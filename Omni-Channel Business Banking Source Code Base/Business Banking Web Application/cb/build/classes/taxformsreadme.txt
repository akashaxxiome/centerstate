Taxforms.xml for Corporate Banking Solutions
(C) Copyright 2016 SAP SE or an SAP affiliate company. All rights reserved.

February 24, 2016

**************************************************************************
General Information
**************************************************************************
Taxforms.xml file contains ACH payment information for making Child Support, Federal, and State tax payments using Corporate Banking Solution's ACH payment capabilities. Additionally, select regional, county, and local ACH payment forms will be added on an ongoing basis.

This Read Me contains a record of changes (revision history) since the original release of taxforms.xml, and basic installation instructions.

**************************************************************************
Installation Instructions
**************************************************************************
Before installing the taxforms.xml file, you should back up your old version. The taxforms.xml file can be found in the WEB-INF/classes directory of the installed product. After saving the old version as a back-up, copy the new version to the same directory. The web server will need to be stopped and started for the installed file to be used.

**************************************************************************
Revision History
**************************************************************************
The taxforms.xml file will be updated every six weeks or as needed.

--------------------------------------------------------------------------------------------
February 24, 2016 Release
--------------------------------------------------------------------------------------------
Enhancement :  (1) VT added forms for tax types 03201, 03200, 02100, 02200, 11002, 07400, 04100, 04040, 01102, 03600, 07102, 01700, 05000, 16000, 07100 and 07101
			   (2) VT updated description for tax types 01101 and 01100
			   (3) CT removed outdated forms for tax types 04603, 05001, 04606 and 04200
               (4) CT updated description for tax type 05200
               (5) CT added forms for tax type 16095, 20005, 20038, 03600, 04901, 07193, 04902, 07200, 20056, 20003, 15086, 16059, 05941,  01750, 16005, 04139, 20004, 16060, 12000, 04607, 07300, 20002, 20007, 03100, 01115, 01106 and 01110

--------------------------------------------------------------------------------------------
February 3, 2016 Release
--------------------------------------------------------------------------------------------
Enhancement :  (1) RI updated the type code for tax type 07308 to 07303
	           (2) RI updated description for tax types 07303, 01102 and 14000
               (3) RI added forms of tax type 06703, 04703, 04403 and 04404
               (4) RI removed form of tax type 01200
               (5) CT updated description for tax types 20001, 01100 and 06400

--------------------------------------------------------------------------------------------
January 13, 2016 Release
--------------------------------------------------------------------------------------------
Enhancement :  (1) FL updated amount type description for all tax types
               
--------------------------------------------------------------------------------------------
December 23, 2015 Release
--------------------------------------------------------------------------------------------
Enhancements :  (1) CT updated description for tax type 20001, 04602, 04604, 05100 and 01100
                (2) GA added forms for tax types 203 and 205


--------------------------------------------------------------------------------------------
December 4, 2015 Release
--------------------------------------------------------------------------------------------
Enhancements :  (1) FL updated description for tax type 00001, 00038

--------------------------------------------------------------------------------------------
November 10, 2015 Release
--------------------------------------------------------------------------------------------
Enhancements:   (1) TX added new form for tax type 56820, 63020, 63820
				(2) TX updated description for tax type 16950
				(3) RI updated description for tax type 21005, 03105, 03106, 03107, 02100, 02200, 02300, 05107, 05106, 14208, 14206, 14207, 14205, 07503, 07105, 07106, 07107, 14108, 20103, 22005, 22006, 22007, 23000, 15103, 15203, 15403, 15303, 24003, 16103

--------------------------------------------------------------------------------------------
October 20, 2015 Release
--------------------------------------------------------------------------------------------
Enhancements:   (1) TN updated descriptions for tax type 01053, 01054, 01055, 01056, 02010, 02011, 02012, 02053, 03002, 04001, 04002, 04003, 04004, 04011, 06002, 07002, 10351, 10355, 10356, 10360, 10368, 10372, 10374, 10377, 11001, 02013, 02014, 02015, 02016
		(2) TX updated descriptions for tax type 26050, 76020, 16050, 90001, 92020, 92820, 13050, 13950

--------------------------------------------------------------------------------------------
September 25, 2015 Release
--------------------------------------------------------------------------------------------
Enhancements:   (1) MS added new forms for tax type 34201, 91001 and 91501
				(2) WI added new form for tax type 15200
                (3) AR updated description for tax type 01100
                (4) VA updated description for tax type 00021 and 00023
                (5) WI updated description for tax type 01212, 01211, 01100 and 06100
                (6) DC updated description for tax type 00300
				(7) CO updated Bank Information
				(8) LA added TPP fields for tax type 13000
				(9) CA added TPP addenda records for form of tax type U
                (10) CA updated TXP07, TXP08, TXP09, TXP10 to be an optional Field for form of tax type code U
				(11) CA updated TXP11 to be an optional Field for form of tax type code U
		
--------------------------------------------------------------------------------------------
August 31, 2015 Release
--------------------------------------------------------------------------------------------
Enhancements:   (1) SC Added new form for tax type 11207 and 12450
                (2) MN updated description for tax type 125
                (3) WV Removed Duplicate form for tax type 08697
                (4) NC added memo information on TXP01 for tax types 01101, 01102, 01103, 02100, 03010, 03012, 04001, 04120, 04111, 04109, 04040, 06201, 06202, 06300, 06500, 07000, 07100
                (5) CO removed outdated form for tax type 121
                (6) IL Added new form for tax type 02020

--------------------------------------------------------------------------------------------
June 1, 2015 Release
--------------------------------------------------------------------------------------------
Enhancements:   (1) NC updated TXP01 for tax types 01101, 01102, 01103, 02100, 03010, 03012, 04001, 07000, 06201, 06202, 06300, 06500, 04120, 04111, 
                   04109, 04040, 07000, 07100, 07200, 07300, 05000, 05101, 05107, 05200, 05400, and 05500
                (2) NC updated descriptions for tax types 05101 and 05200.
                (3) NC removed obsolete form for tax type 07600.
                (4) NC added new form for tax type 05107.
                (5) AL changed TXP07 length, memo, default value, and removed tags.
                (6) AL updated descriptions on several forms.
                (7) AL added new form for tax type 12001.
                (8) GA resolved Formatting inconsistencies for tax types 050 and 011
                (9) MI updated memo field and added forceLastOfQuarter Tag to TXP03 for tax types 13000 and 13010
                (10) MI Added Memo and ForceSemiMonthly_1_15 tags to TXP03 on forms with tax type codes 11000, 11001, and 20000
                (11) HI enabled new style forms and added TPP addenda records
                (12) KY added TPP addenda records to forms for tax types W1, JINS, OL, TRANS, and LINS
                (13) LA added TPP addenda records to tax payment form for Dept of Insurance License Premium, Prepayment, and Fee
                (14) MI added TPP addenda records for forms for tax types 13010 and 13000
                (15) MT added TPP addenda records to Fuels Tax form
                (16) NV enabled NV tax forms and added TPP addenda records
                (17) NY added TPP addenda records to Corporation Tax
                (18) OK added TPP addenda records to all forms
                (19) SC added TPP addenda records to Unemployment Insurance
                (20) IN added TPP addenda records to Cigarette Tax
                (21) Fed added memo to TXP03 instructing user to enter "01" for day of the month.

--------------------------------------------------------------------------------------------
April 15, 2015 Release
--------------------------------------------------------------------------------------------

Enhancements:  (1) MN updated several form descriptions 
               (2) Fed updated description on form for tax type 87527
	       (3) LA updated description on form for tax type 02400
	       (4) Fed added forceAnnual tag to TXP03 for forms for tax types 87521, 87527, and 60697

			   
--------------------------------------------------------------------------------------------
March 1, 2015 Release
--------------------------------------------------------------------------------------------

Enhancements:  (1) MI replaced TXP04 with TXP05 from non-standard forms for tax types 13000 and 13010
               (2) MS removed default value and added memo to TXP04 on forms for tax types 01201, 01301, and 01401
               (3) MI updated Financial Institution account numbers for the forms with tax type codes 01100, 04200, 04400, and 04500

Bug fix: (1) GA corrected memo field for tax type 050

--------------------------------------------------------------------------------------------
January 15, 2015 Release
--------------------------------------------------------------------------------------------

Enhancements:  (1) MS added new forms for tax type codes 01201, 01301, 01401, 04101, 04201, 
                   04401, 04601, 04701, 04901, 05101, 54801, 20001, 64501, 62301, 50101, 50201, 
                   50801, 50701, 50601, 50501, 50401, 50301, 14401, 63601, 63801, 60801, 64301, 
                   64701, 62601, 32501, 63101, 61701, 61301
                (2) MS removed form for tax type code 62301
                (3) MS updated description on form for tax type code 63701
                (4) PA updated tax type codes, TXP02, on forms for tax type codes 1051, 1059, 1062
                (5) PA added new forms for tax type codes 1054, 1052, and 1055
                (6) PA removed form for tax type code 1006
                (7) GA updated bank name
                (8) GA added new forms for tax types 018 and 201
                (9) GA updated description on form for tax type 041
               (10) GA updated format requirements and added memos for TXP01 for forms with tax 
                    types 011, 018, 021, 041, 050, and 201
               (11) MI updated bank account number on forms for tax type 11000, 11001, and 20000
               (12) VA added new forms for tax types 00031, 00032, 00033, 00041 (two forms), 00200, 00771, and 00871

--------------------------------------------------------------------------------------------
November 14, 2014 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) OR updated TXP01 description for forms for tax types 01101 and 01102.
               (2) OR updated TXP05, TXP07, and TXP09 for form for tax type code 01101 to be required
               (3) OR updated TXP05 and TXP07 for form for tax type code 01102 to be required
               (4) NV updated child support form routing and account number

--------------------------------------------------------------------------------------------
October 15, 2014 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) DC added new form for tax type 00380
               (2) PA for tax type UC001 changed description, added trailing spaces to TXP01,
                   Changed TXP05 to left zero fill to length 10.
               (3) Fed added form for tax type 89637
               (4) SC changed sales tax form for tax type 040 to tax type 14701. 
                   Prepended TXP01 with 040 and max length to 10 digits.
               (5) IN changed depository bank name, routing number, and account on all forms
               (6) IN assigned TXP02 the value of 072 for Cigarette Tax
               (7) IN changed account numbers on all forms
               (8) IN removed outdated form for tax type 056
               (9) IN added new forms for tax types 0601, 054, 04001, 051, 053, 0073.
               (10) IN updated descriptions of all forms 
--------------------------------------------------------------------------------------------
September 1, 2014 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) IL Added new forms for tax types 0505 and 5058
               (2) AR Updated descriptions on forms for tax types 05500, 05100, 07200, 02101, 02102, 02103, 02104, 07600, 08600
               (3) OR Added default values on amount fields on forms for tax types 01101 and 01102
               (4) WA Changed banking information on Excise Tax form

--------------------------------------------------------------------------------------------
July 15, 2014 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) Fed changed name of form for tax type 87527 and added a new form for tax type 87521
               (2) RI Child Support changed account number
               (3) MI Added form for tax type 02670, 07300, 07311, and 07331. Removed form for tax type 73
               (4) MN Removed forms for tax types 075, 106, and 108
               (5) LA Changed TXP01 length, name, and added memo field on all forms except Dept of Insurance form and form for tax type 13000.
               (6) LA Updated names on forms for tax types 02100, 05211, 05212, 04101, and 04102
               (7) LA Removed outdated forms for tax types 1, 2, 3, 06502, 07601, and 01102
               (8) LA Updated TXP01, TXP03, TXP04, TXP05 and added TXP06, TXP07, TXP08, TXP09, and TXP10 on form for tax type 13000
               (9) DC Updated descriptions on forms for tax types 00250, 00260, 00350
--------------------------------------------------------------------------------------------
June 1, 2014 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) AR added new tax forms for tax types 05200 (2nd use of this code), 05101, 05102,
                   05201, 05202, 05100 (2nd use of this code), and 05001
               (2) AR updated description of existing form for tax type 05200
               (3) DC changed TXP01 on all forms from variable length upto 15 to exactly 9
               (4) DC added new forms for tax types 00150, 00310, 00370, and 00450
               (5) DC removed form for tax type 00380
               (6) LA added forms for tax types 07301
               (7) LA updated several form descriptions 
               (8) MD updated banking information for Unemployment Insurance payment form

Bug fix: (1) MO fixed mislabled description for form for tax type 0229C
         (2) TX Texas corrected form description spelling error on forms for tax types 08020 and 98020
--------------------------------------------------------------------------------------------
March 1, 2014 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) AL Remove obsolete form for tax type code 02220
               (2) AL Added new form for tax type code 02229
               (3) AL Added local tax form, tax type code 04801, for City of Huntsville. 
               (4) MO Updated descriptions on forms for tax types codes 0115A, 0115P, 0219C, and 0229C.
               (5) Federal restricted TXP03 to valid months on forms for tax type codes 72003, 
                   72004, 72005, 72007, 72008, 72009, and 7200B

--------------------------------------------------------------------------------------------
January 15, 2014 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) ID City of Ketchum added forms for tax types 04040, 21040, and 02540
               (2) ID added forms for tax types 04039, 21039, and 02539.
               (3) ID added tags to force TXP03 to contain 15th of month or last day of month 
                   on forms for tax type codes 01109, 21009, 02509.
               (4) MI Added Memo and ForceLastOfMonth tags to TXP03 on forms with tax type 
                   codes 13000 and 13010
               (5) Federal changed excise tax amount type 133 label on forms for tax types 
                   72003, 72004, 72005, 72005, 72007, 72008, 72009 and 7200B
               (6) CO added new forms for tax types 180, 18001, and 209.

--------------------------------------------------------------------------------------------
December 1, 2013 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) MO removed form for tax type 04198 and added forms for tax types 04598 and 04498.
               (2) KS for form for tax type 04401 changed TXP07 from "Penalty" to "Estimated" and 
                   TXP06 from "P" to "E"
               (3) MI added form for tax codes 13000 and 13010
--------------------------------------------------------------------------------------------
October 15, 2013 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) TX Removed outdated forms for tax types , 38030, 13020, 13820 , 38060, 
				   38040, 32020, 38020
			   (2) TX form for tax type 67501 updated Amount Type options and added ability 
			   	   to enter up to three amount types.
			   (3) TX form for tax type 67401 removed one outdated Amount Type option.
			   (4) TX form for tax type 90200 without E-File number changed description
			   (5) TX form for tax type 90200 with E-File number changed description, added 
			       TXP04 select options added TXP06, and removed TXP10.
               (6) MT Changed the TXP01 to allow 13 characters and prepend two spaces to fill to 15
                    character length.
               (7) MT Removed outdated form Old Fund Liability Tax (01155)
               (8) MT added forms for tax types 02001, 02002, 20102, 20103
			   (9) FED form for tax type 9410B, 94101, 94100, 94105, 94107, 94108, 94109,
			       94103, and 94104 changed TXP04 and TXP05.
--------------------------------------------------------------------------------------------
September 1, 2013 Release
--------------------------------------------------------------------------------------------
Enhancements:  (1) MO Added Child Support Deposit Account info including bank name, routing number, 
                   and account number.
               (2) KS Added Child Support Deposit Account info including bank name, routing number, 
                   and account number.
               (3) Fed updated TXP04 options on forms for tax types 72003, 72004, 72005, 72007, 
                   72008, 72009 and 7200B

--------------------------------------------------------------------------------------------
July 15, 2013 Release
--------------------------------------------------------------------------------------------
Enhancements: (1) Federal forms for tax type codes 72003, 72004, 72005,
                  72007, 72008, 72009, and 7200B added TXP04 select options of
                  13, 133, and 136
              (2) PA Child Support added routing and account number to DepositAccount
              (3) PA added forms for tax types 1009, 1001, 1021, 1022, 1023, 1018,
                  1010, 1014, 1006, 1028, 1030, 1029, 1031, 1024, 1026, 1025, 1027,
                  1019, 1002, 1012, 1011, 1003, 1016, 1013, 1032, 1017, 1033, 1005,
                  1008, 1035, 1015, 1034, 1020, and 1007
              (4) PA changed forms for tax types CN000 to 1004, BL000 to 1003,
                  PU000 to 1020, and LT000 to MJ000
              (5) PA removed form for the following tax types BA000, CS000, LN000,
                  IP000, IS012, IA011, MI000, MO013, MU000, GR000, FT000, OF200

--------------------------------------------------------------------------------------------
May 31, 2013 Release
--------------------------------------------------------------------------------------------
Enhancements: (1) LA added forms for tax types 02400, 04151 and 04152.
--------------------------------------------------------------------------------------------
April 15, 2013 Release
--------------------------------------------------------------------------------------------
Enhancements: (1) NJ added form for tax type 07400.
              (2) CO Removed the requirement for tax id to begin with "3710"
              (3) WI Removed forms for tax types 04810, 04820, and 04830
              (4) WI Added forms for tax types 16000, 02400, 20300, 07300, 04860, 04840,
                  20100, 04940, 04900, 04810, 15090, 15080, 01109, and 01140
              (5) OK Added TXP02 to non-standard form for tax type SVU

Bug fix: (1) IL form for tax type ILUI changed value of TXP04 to 'U'

--------------------------------------------------------------------------------------------
March 15, 2013 Interim Release
--------------------------------------------------------------------------------------------
Enhancements: (1) CA LA forms for tax types 11011, 11021, and 11031 changed TXP01 length, format, and memo.
              (2) OK removed TXP01 from the top of the hierarchy because all of the forms overwrite
                  so it was redundant.

--------------------------------------------------------------------------------------------
March 1, 2013 Release
--------------------------------------------------------------------------------------------
Enhancements: (1) OK tax types WTH and WTR changed TXP02 length from -5 to 3.
              (2) OK removed outdated forms for tax types 06010, 04200, 04500, 04400, 12010, and 04900.
              (3) OK added forms for tax types ATG, STS, SCU, SVU, STW, PWF, ALC, TOB, CIG, MED,
                  STH, STL, STT, STA, STE, AND STW.
              (4) NV- Added Nevada tax forms  section which is commened out by default.
			  for versions CB70 SP1 ESD10 and above, CB70 SP2 ESD9 and above, and CB 8.2 SP01 and beyond,
			  these comments in the taxforms.xml should be deleted to enable State of Nevada taxforms.
			  For all other versions, these forms must be disabled otherwise invalid Nevada tax payments could be submitted.
--------------------------------------------------------------------------------------------
January 15, 2013 Release
--------------------------------------------------------------------------------------------
Enhancements: (1) NH Removed forms for tax types 07112, 07106, 07109
              (2) ME Removed form for tax type 04501
              (3) ME Updated banking information on all forms
              (4) IL Removed outdated forms for tax types 0232, 0233, 0234, 0235, 15052
              (5) AL Added form for tax type 07777
              (6) AL Removed outdated forms for tax types 07776, 02121, 02221, 01201, 12001, 01701
              (7) AL Changed description on form for tax type 02228
              (8) ID Removed forms for tax types 04037 and 20006
              (9) ID Added a form for tax type 20421

Bug fix: (1)  RI Corrected form for tax type 28003. It was previously 02803.

--------------------------------------------------------------------------------------------
December 1, 2012 Release
--------------------------------------------------------------------------------------------
Enhancements: (1) AL added new forms for tax types 04622, 05120, 01005, 05030, 05040, 05050,
                  05060, 05070, 05080, 05090, 07270, 07271, 07272, 07273, and 07274.
              (2) AL removed outdated forms for tax types 07772, 07778, 02223,07210,
                  07260, and 02227
              (3) MI added the following new forms for tax types 02010, 02020, 02170, 02675,
                  04201, and 09100
              (4) CO added new forms for tax types 140, 06510, and 102
              (5) CO removed outdated forms for tax types 04230 and 14221
              (6) AZ changed description on form for tax type 04206

Bug fix: (1) CO Removed "Sales: Prepaid Wireless E911 Surcharge (049)" because it was a
          duplicate of "Fees: Prepaid Wireless E911 Surcharge (049)"
         (2) CO changed tax type code for Income: Estate/Trust Estimated Income from 01712 to 01704
         (3) SC removed tax code 12006, it was repealed in 200
         (4) TN removed Tennessee tax forms 1021, 1022, 1023, 1024 because they were replaced with 1053, 1054, 1055, 1056
         (5) MI Michigan bank deposit record added
--------------------------------------------------------------------------------------------
October 15, 2012 Release
--------------------------------------------------------------------------------------------
Enhancements: (1) KY updated banking information on most forms.
              (2) MA added form for tax type 0120A.

Bug fix: (1) AZ removed forms for tax types 04201, 04202, 04203, 04204, 04205, 04207, 04208,
             04209, 04210, 04211, 04212
--------------------------------------------------------------------------------------------
September 1, 2012 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) OK Updated Bank Name to J P Morgan Chase
                   (2) OK Removed outdated forms for tax types 07500, 20010, 01000, 01200,
                       01410, 01100, 08700, 08401, 08403, 08201, 08402, 08101, 05100, 05201,
                       05202, 05500, 05800, 05203, 05301, 05302, 10100, 04700, 04710.
                   (3) OK updated description of forms 04200, 04500, 04400, 12010.
                   (4) OK added new forms for tax types 06010, 04900, WTH, WTR.
                   (5) DE Removed Tax form for Insurance (71)
--------------------------------------------------------------------------------------------
July 15, 2012 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) NC updated bank name with Bank of North Carolina and
                       routing number with 053112039
                   (2) AR added new forms for tax types 01150, 01170, 02105, 14102, 14100,
                       14101, 05800, and 05900.
                   (3) AR removed outdated forms for tax types 05001, 05101, 05201, and 05202.
                   (4) AR updated descriptions on forms for tax types 08400, 05200, 05500,
                       05100, 04020, 04201, and 04202.
                   (5) AR changed length allowed for Taxpayer ID, TXP01, to 8 on all forms.
                   (6) IA changed length of TXP01 on form for tax type 00050 to require
                       exactly 15 characters.
                   (7) IA changed TXP01 to add leading zeros to fill left on all forms.
                   (8) IA added memo field to TXP01, Taxpayer ID,for tax types 00011, 00300,
                       00050, and 00240.
                       enter field data.
                   (9) IA changed required length of TXP10 from 6 to allow less 6 or less.
                  (10) UT added new forms for tax types 0500, 0630, 0710, 0715, 0720, 0730,
                       0800, 0803, 0840, 0841, 0880, 0881 0900, and 2000.
                  (11) UT updated routing number from 124000012 to 121000248.
                  (12) UT added memo field to TXP01, Taxpayer Identification, on all forms.
                  (13) CO added new forms for tax types 121, 049, 077, 01105, 01106, and 04201.
                  (14) CO removed outdated forms for tax types 08112,08505, 08605, and 08615.
                  (15) NC changed bank name to Bank of North Carolina and routing number to 053112039
                  (16) NC added new forms for tax types 04109, 04040, 04000, 05000, 05200, and 05400
                  (17) NC removed outdated forms for tax types 03011 and 04800.
                  (18) MI added forms for tax types 07100 and 07150.
                  (19) MO on forms for "Q130", "M130", and "R110" changed TXP07 data type from N2 to AN.

     Bug fix: (1) Federal added new form for tax type 11206 after removing existing form
                  with wrong description.
              (2) CO updated the names of forms for tax types 01180 and 01181 because they were
                  labeled backwards.
              (3) TX removed duplicate form for tax type 13080 and another for tax type 67020.
              (4) OH Cincinnati changed tax type, TXP02, from 00226 to 01129.
              (5) Illinois Unemployment routing number & account number correction

--------------------------------------------------------------------------------------------
June 1, 2012 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) MA added forms for tax code 0191E and 0212M
     (2) IN update forms for tax type codes 011, 021, 031, and 040 to have TXP03 to allow YYYYMMDD format. Removed TXP06 and TXP07.
     (3) IN added form for Cigarette Tax (CIG)
     (4) MD added leading zeros to forms for tax types 01100 and 01101
     (5) MD on form for tax type 04100 added leading zeroes to pad TXP05, TXP07, and TXP09 to length of 10.
     (8) MD on forms for tax types 07100 and 07101 added TXP06 containing the value 232601.
     (9) MD added form for tax type 04600
     (10) MD forms for tax type codes 02100, 02200, and 02300 added leading zeros to TXP05.
     (11) FL forms for tax types 00001, 00002, 00016, 00019, 00038, 05425 added leading zeroes padding TXP01, Taxpayer ID, to 15 characters on all forms. Standardized TXP04, Amount Types, for all forms:
     (12) FL Removed the following outdated forms
         Pari-Mutual: Tax on Cardrooms (05355)
         CSE Overpayment recovery (07396)
         CSE Void non-SDU disbursement (07397)
         CSE Retained child support collections (07398)
         CSE Interest (07399)
         Fuel tax - air carrier (00090)
         Fuel tax - terminal supplier (00091)
         Fuel tax - wholesaler/importer (00092)
         Fuel tax - mass transit (00093)
         Fuel tax - local government (00094)
         Fuel tax - blender/retailer of alternate fuels (00095)
         Fuel tax - petroleum operator (00097)
         Fuel tax - exporter (00098)
     (13) GA added forms for tax types 00001 and 00004
     (14) MO Kansas City updated forms for tax types R110, M130, and Q130 to include TXP06 with value of "T" and TXP07 with value of "0" and decreased the size of TXP01 to size 9.
     (15) OH City of Miamisburg added form for tax type 011

--------------------------------------------------------------------------------------------
April 15, 2012 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) CA LA County added six forms for tax type 110.
                   (2) NY CT-400 tax form added.

--------------------------------------------------------------------------------------------
March 1, 2012 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) OH City of Parma added a form for tax type 11.
                   (2) FEDERAL added form for tax type 86132
                   (3) TX removed TXP06, TXP07, TXP08, and TXP09 from form for
                       tax type 67501 and added two selections for TXP04
	               (4) TX removed TXP06, TXP07, TXP08, and TXP09 from form for
	                   tax type 67401 and added two selections for TXP04
	               (5) TX added forms for tax types 67020, 67820, 13080,
	                   75820, 72820, 71820, 94020, and 94820
	               (6) TX forms had several descriptions updated
	               (7) SC Taxpayer Verification made it optional
	               (8) NE TXP06 is Taxpayer Verification field
	               (9) MS Allow the amount type to include a Transaction ID
	               (10) KS Add Penalty & Interest amount fields for (01100, 02000,
	                    04200, 04201, 04400, 04500, 04501) tax forms

--------------------------------------------------------------------------------------------
January 15, 2012 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Federal added form for tax type 9410B
                   (2) Federal forms 94101, 94100, 94105, 94107, 94108, 94109, 94103,
                       94104, 94400, 94405, 94407, 94408, 94409, 94403, 94403, 94404,
                       10005, 10007 all had changes to TXP04, TXP06, and TXP08 to replace
                       text based codes to numeric.
                    (3) ID added many new forms and changed the descriptive labels on others.
                        Some of the forms use the same tax type codes so please see the
                        taxforms.xml file itself for the numerous changes.

--------------------------------------------------------------------------------------------
December 1, 2011 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) FEDERAL added forms for tax types 72003, 72004, 72008,
                       72009, 7200B, 70622, 70632, 09459, 09458, 10662, 11209,
                       11208, 53297, 86122, 87252, 87524, 88042, 88767,
                       88766, 88762
                    (2) FEDERAL changed options for IRS Excise Tax Number on
                        forms for tax types 72005 and 72007
                    (3) NJ updated bank routing and account number.

--------------------------------------------------------------------------------------------
October 15, 2011 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) MS updated bank account number on all forms.
                   (2) MS updated TXP02 to allow CCYYMMDD formatted dates.
                   (3) MS updated TXP04 length to 10.
                   (4) MS removed depreciated forms for tax types 063, 071,
                       073, 081, 082, 086, 141, 04501, and 04801.
                   (5) MS added new forms for tax types: 10401, 14101, 31501,
                       63901, 14201, 62301, 60601, 61001, 60901, 62401, 63701, 31101
                   (6) RI updated bank name, account number, and routing number on all forms.
                   (7) RI updated description on form for tax type 02300
                   (8) RI removed outdated form for tax type 16003.

--------------------------------------------------------------------------------------------
September 1, 2011 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) OH Huber Heights added new form for tax type 011.
                   (2) IA added new form for tax type 13000.
                   (3) IA changed tax type 00300 (Sales and Direct Pay) and 00050 label for
                       TXP01 to Permit Number with variable length up to 15 characters zero
                       left filled.
                   (4) CO updated the description and sequence of all forms.
                   (5) CO added new forms for the following tax type codes: 210, 08405, 01185,
                       05430, 05420, 04840, 04230, 04810, 242, 04202, 049, 1421, 14221, 1422,
                       1423, 01712, 01320, 01220, and 01420.
                   (6) MT updated tax type 01100 with new description and TXP04 value options.
                   (7) MT added new forms for the following tax type codes: 08500, 08400, 02000, 01187,
                       04901, 04500, 16001, 07400, 16002, 04501, 04700, 04900, 04902, 20101, 08001,
                       08002, 08003, 15001, 08004, 08005, 08006, 08007, 15002, 06001, 06002, 07201,
                       06003, 06004, 06005, 06006, 06008, 06009, 06010, 07301, 06011, 06012, 06200,
                       06013, 01700, and 01001

     Bug: (1) OH Kettering form has been fixed such that Amount of Qualifying Wages will
              not be added to the total tax amount.
--------------------------------------------------------------------------------------------
July 15, 2011 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) MA updated tax type code and descriptions
                       for following forms:
                       Old code   New code
                       01683       0168E
                       01673       0167E
                       01667       0166Q
                       01666       0166M
                       01663       0166D
                       01377       0137Q
                       01376       0137M
                       01203       0120E
                   (2) MA added new forms for Jet Fuel Tax - Monthly (0160M)
                       and Aviation Gasoline Tax - Monthly (0112M)
                   (3) AZ added new form for tax type 01100 for those not
                       wishing to submit using the optional two-digit
                       sequence.
                   (4) MT updated tax type 01100 with new description and TXP04 value options.
                   (5) MT added new forms for the following tax types:


--------------------------------------------------------------------------------------------
June 1, 2011 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) OH Cincinnati added form for tax type 00226.
                   (2) OH Kettering added form for tax type 011.
                   (3) CO added memo field to all forms instructing users to append 3710 to TXP01 and
                   changed description to Taxpayer Identification (EFT Number)
                   (4) DE changed TXP01 to required length 11 with a memo reminding user to zero fill
                   to the right to reach length 11.

--------------------------------------------------------------------------------------------
April 15, 2011 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) MD tax types 1100 and 1101 changed TXP01 allowed length
                   from 11 to 8 and updated label to "Maryland Central
                   Registration Number."
                   (2) MD tax types 07100 and 07101 updated TXP01 length from
                   1-15 to a length of 5. Updated description to "NAIC No."
                   (3) MD tax types 02100, 02200, and 02300 TXP01 length
                   changed from 1-15 to length of 9 digits and the description
                   was updated to "Federal Employer ID".
                   (4) MD tax types 05100, 05200, 05300, and 05400 TXP01
                   length updated from a length of 1-15 to a length of 5
                   digits and the description was updated to "Motor Fuel
                   Tax Account Number".
                   (5) MD tax types 05100, 05200, 05300, and 05400 TXP05
                   updated from variable length to be zeroed filled to make
                   the field exactly length 10.
                   (6) MD tax type 04100 TXP01 length updated from length 1-15
                   to length 8 and description updated to "Maryland Tax
                   Account Number".
--------------------------------------------------------------------------------------------
March 1, 2011 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) MN All penalty and interest amount fields have been
                       changed from a default value of three zeros to only
                       one zero.
                   (2) MN Unemployment Insurance payment form has had the tax
                       type code of 888 removed and now uses no value.
                   (3) VA All penalty and interest amount fields have been
                      changed from a default value of three zeros to only
                      one zero.
                   (4) MI added new tax form for tax type 05900, IFTA Fuel.
                   (5) KY Louisville added new tax forms for tax types W1,
                       OL, LINS, JINS, and TRAN.


--------------------------------------------------------------------------------------------
January 15, 2011 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) MO Motor Fuel Tax form updated to match documented
                       non-standard record layout.
                   (2) MO Tobacco Tax form added.
                   (3) MO added memo to TXP05 on form for tax type 0115A.
                   (4) IA changed size of TXP01 from a variable length to an
                       exact length of 15.
                   (5) IA added Taxpayer Verification, TXP10, to all forms.
                   (6) IA updated description for tax type 00300 and removed
                       redundant form with id 1445.
                   (7) NE added two-digit number required as a prefix to each
                       tax type on TXP01. Also added a memo field to remind
                       user of the two-digit prefix requirement.
                       required to be appended to the Tax Payer
                       Identification field.
                   (8) NE Added Taxpayer Verification, TXP10, to all forms.
                   (9) NE Added required two-digit prefix to all forms and
                       and added memo field reminding user of the prefix.
                   (10)NE removed outdated forms for Fuel Tax types 05100,
                       05300, 05400, and 05500. Added form for tax type 05000.
                   (11)FL removed form for tax type code 00096 and added new
                       forms for tax type codes 00090,00091, 00092, 00093,
                       00094, 00095, 00096, 00097, and 00098.
                   (12)FL Changed TXP01 to a length of 09 on forms for tax
                       types 00001, 00003, 00008, 00016, 00022, 00090, 00091,
                       00092, 00093, 00094, 00095, 00096, 00097, and 00098.
                       Also changed TXP01 to a length of 13 for tax type 00001
                       and a length of 07 for tax type 05425.
                   (13)FL Added the following new Child Support Enforcement
                         07396, 07397, 07398, and 07399
                   (14)FL updated description of tax type 05318
                   (15)FL Added the following forms: 05355, 05342, 05365,
                       07221, 07222, 07223, and 07224
                   (16)FL removed outdated forms for tax types 05355, 05365,
                       05342, and 05340.
                   (17)FL Updated the payment codes and payment code
                       descriptions for all forms.
                   (18)HI Added the following tax forms: 02230, 02103

--------------------------------------------------------------------------------------------
November 15, 2010 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) IL added bank name, routing, and account numbers to forms.
                   (2) IL added Unemployment Insurance tax contribution form, tax type ILUI.
                   (3) IN added extension fields, TXP06 and TXP07, to all forms.
                   (4) NC added TXP04 prenote option to all forms.
                   (5) NC made the amount field TXP05 zero filled for all forms.

--------------------------------------------------------------------------------------------
October 1, 2010 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) AZ tax type codes 01101, 01102, 01103, and 01104 reduced size of TXP01 from 15
                       characters or less to exactly 9 and removed zero fill functionality.
                   (2) AZ tax type codes 02101, 02102, 02103, and 02104 reduced size of TXP01 from 15
                       characters or less to exactly 9 and removed zero fill functionality.
                   (3) AZ tax type codes 04101, 04102, 04103, 04104, 04105, 04106, 04107, 04108, 04109,
                       04110, 04111, and 04112 reduced size of TXP01 from length 15 or less to a length
                       of 10 or less with zero fill.
	               (4) AZ tax type codes 04201, 04202, 04203, 04204, 04205, 04206, 04207, 04208, 04209,
	                   04210, 04211, and 04212 reduced size of TXP01 from length 15 or less to a length
	                   of 10 or less with zero fill.
	               (5) CA tax type codes 07130, 07131, 07133, 07134, 07150, 07153, 07270, and 07271
	                   changed length to 8 from 10 or less. Removed TXP06, TXP07, TXP08, and TXP09.
	                   Removed zero fill from TXP05.
	               (6) CA tax type codes 01100, 01101, 01102, 01104, 1300, and 20000 changed to have TXP01 of
	                   length 8 from length 1 to 15.
		           (7) CA tax type codes 02211, 02414, 02124, 02125, 02126, 02127, 02516, 02512, 02028,
		               03620, 03022, 03572, 01190, and 02020 removed leading zero in amount fields.
		           (8) CA added unclaimed property remittance forms.
		           (9) AL added default value for Confirmation Number, TXP07, for all forms.
		          (10) AL removed zero fill function from TXP05 on all forms.
		          (11) ND State Fuel Taxes, Taxpayer ID is now 11 digits instead of 9
		          (12) OH Columbus Ohio Withholding tax and Net Profits tax were added

--------------------------------------------------------------------------------------------
August 15, 2010 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) AR added tax type code 08200
                   (2) TX added forms for tax type codes 47020, 47820, 32640, 32600, 32620, 07020, 06020,
                       17820, 70820, 12020, 12820, and 32660
                   (3) TX added form for tax type 68308
                   (4) TX added options "C", "D", "E", and "F" for tax type code 67501. Changed TXP01 description
                       to "Agency Code" and length from -15 to 3
                   (5) TX for tax type code 67401 changed TXP01 description to "Agency Code" and length from -15 to 3
                   (6) TX tax type code 68307 changed TXP01 description to "TWC Account Number" and length from -15 to 9
                   (7) TX added two forms for tax type code 90200.

    Bug Fixes: (1) ND forms for tax types 052, 053, 054, and 055 change TXP01 description, changed
                   length of TXP01 from 9 digits to 11, and changed length of TXP04 from 4 to 1.

--------------------------------------------------------------------------------------------
July 1, 2010 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) ND type 020 changed TXP01 length from 1-15 to 9.
                       Changed description from Taxpayer Identification Number to Federal
                       Identification Number.
                   (2) ND type 011 changed TXP01 length from 1-15 to 9,
                       added option "C" for TXP04.
                   (3) ND type 041S1 changed length of TXP01 from 1-15 to 9, changed TXP01 description,
                       changed TXP04 from a value of "T" to "F01C".
                   (4) ND type 030 changed TXP01 length from 1-15 to 9.
                       Changed description from Taxpayer Identification Number to Federal
                       Identification Number.
                   (5) ND added forms for type 020F.
                   (6) ND added form for type 020P.
                   (7) ND added form for type 020S.
                   (8) ND added form for type 150.
                   (9) ND added form for type 085.
                   (10) ND added form for type 089.
                   (11) ND added form for type 052.
                   (12) ND added form for type 053.
                   (13) ND added form for type 054.
                   (14) ND added form for type 055.
                   (15) SC Added TXP10 taxpayer verification to all forms.
                   (16) Added default (blank) Child Support form id 999999.

    Bug Fixes: (1) OH tax type 7200 TXP01 length changed from 1-15 to 9 and memo field added.

--------------------------------------------------------------------------------------------
May 15, 2010 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) VA Child Support updated routing number and added FIPS code
                   (2) WA adjusted to newly documented addenda record format.
                       Added ability to pay tax type codes 04102 and 07201.
    Bug Fixes: (1) CA TXP10 is now non-editable and zero filled for tax type codes 01100, 01101, 01102, 01104, 01300, 20000
--------------------------------------------------------------------------------------------
April 1, 2010 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) RI Removed outdated forms for tax types X0013 and X0015
                   (2) WA changed record layout in accordance with new documentation.  Added tax
                       types 04102 and 07201. Updated routing number and account number.
    Bug Fixes:
--------------------------------------------------------------------------------------------
February 15, 2010 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) LA added forms for tax types 06101, 06102, 06201, 06202, 05301, 05302, 05401, 05402,
                       05501, 05502, 05601, 05602, 05701, 05702, 05801, 05802, 05901, 05902, 06001, and 06002.
                   (2) CT added unemployment tax form tax type code CTSUI
                   (3) MI added HMO Use Tax form
                   (4) OH Springfield updated receiving bank information
                   (5) WV added forms for tax types 01085 and 04223.
                   (6) CA added LA City business tax type LACTY
                   (7) CA added tax types 07270 and 07271.
                   (8) UT changed TXP01 to length 11 on all forms.
                   (9) UT added forms for tax type 0408. Removed tax type 0402. Updated most descriptions.
    Bug Fixes: (1) AR changed length of Taxpayer Identification Number to 9 for all forms except 04020, 04201, 04202, which are 8 digits.
--------------------------------------------------------------------------------------------
November 3, 2009 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) WV combined tax types 04201 and 04502 into 04201. Removed 04502.
                   (2) WV combined tax types 07208 and 07215 into 07208. Removed 07215.
                   (3) WV updated name of tax types 16066, 02095, 08689.
                   (4) WV added tax type 16068, 05331, 05050, 05054, 05056, 05057, 05058,
                       05061, 05062, 05063, 05064, 05065, 05069, 01076, 01086, 14114, 14129, 14104
                   (5) WV removed tax type code 01290 and replaced it with 01080 for the form for
                       Personal Income Tax Estimated
                   (6) WV updated descriptions on most of the forms.
                   (7) RI added forms for tax types 04020, 04000, 04010, 15103, 15203, 16403, 16503,
                       04204, 04203, 04030, 04040, 04050, XP001, X0015, X0014, X0013, 08201, 08101,
                       15403, 07303, and 15303.
    Bug Fixes: (1) RI removed duplicate form for tax type code 23000 with id of 1930

--------------------------------------------------------------------------------------------
August 3, 2009 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) OH Springfield added form for tax type code 010
                   (2) OH Dayton City added form for tax type code 011
                   (3) MD added new account numbers to forms for tax type code 07100 and 07101
                   (4) MI added new forms for tax type code 05000, 02355, and 02655
                   (5) MO added TXP06 and TXP07 to form for tax type code 0115A
                   (6) RI added form for tax type code 02803
                   (7) SC added form for tax type code 11009
                   (8) SC removed form for tax type code 11103
                   (9) SC added forms for tax type codes 10804, 10801, and 10807. These forms are intentionally duplicates with different descriptions.
                   (10) TX added forms for tax type codes 67401 and 67501
                   (11) Fed added forms for tax type codes 09401, 94101, 07091, 07097, 07096, 11201
                   (12) LA added forms for tax type codes 04121, 04122, 04131, 04132, 04141, 04142, 08101, 08201, 08601, 08701
                   (13) TX added form for tax type code 68307

--------------------------------------------------------------------------------------------
July 1, 2009 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) CA changed banking information for all State forms to use Citibank.  Account number
                       for the Franchise Tax Board is blank awaiting receipt of that number.
     Bug fixes: (1) AR corrected the size of Taxpayer ID field. It is now 8 characters for tax type
                    codes 04020, 04201, and 04202.  All remain forms returned to 12 characters as
                    they were prior to Feb 1, 2009 release.
                 (2) OH added invoice number field for tax type code 07200 and updated field names.
                 (3) OH Sharonville and Tallmadge forms for tax type code 011 changed TXP07 to type AN.
--------------------------------------------------------------------------------------------
May 01, 2009 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) PA removed forms for tax type code MF000.  Added forms for FT000, LF000,
                       and OF200.
                   (2) PA updated description of tax type code ST301
                   (3) PA added quarter code of 0 to the 11 corporate tax forms.
                   (4) CO added forms for tax type codes 08405, 08505, 08605, and 08615
                   (5) NY updated description of tax type code PT
                   (6) OH added Tallmadge city tax type 011
                   (7) MO added forms for tax type 04198, 04199, 0229C and updated descriptions.
                   (8) UT added option to use Format B in Withholding form.
     Bug fixes: (1) OH Sharonville will now accept TXP10 up to 5 digits plus 1 alpha. 'W' is
                    not automatically appended.
--------------------------------------------------------------------------------------------
February 02, 2009 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) AL added new forms for tax codes 01005, 01006, 01007, 01008, 01201,
                       01701, 02121, and 12001.
                   (2) AR removed form for tax type 04100 and added 04020.
                   (3) AR removed forms for tax type codes 04110, 15000, 04800, 20000.
                   (4) AR changed Taxpayer ID field from 12 character to eight digits.
                   (5) TX added forms for tax types 08020, 98020, 97020, and 96020.
                   (6) TX removed forms for tax types 25020, 24020.
                   (7) TX changed routing number to 111000614
     Bug fixes: (1) CA Franchise Tax Board forms for tax types 02211, 02414, 02124, 02125, 02126, 02127,
                    02516, 02512, 02028, 03620, 03022, 03572, 01190, 02020 now insert leading zeros on amounts
                    and allow the "*" character in the Identification field.
                (2) Fixed a problem with LA (Louisiana) Dept of Insurance License Premium tax form,
                    non-default, but TXP04 inherited the "T" value from parent.
--------------------------------------------------------------------------------------------
November 01, 2008 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) LA DOI has obsoleted form with id 1573, 1574, NS 1575.  These forms will
                       be removed in the next update. We have added the new form, id 3410, which
                       supersedes.
                   (2) MI added new form for tax type code 02671.
                   (3) MI updated bank name to JP Morgan Chase
                   (4) MN Updated names of some forms and added new forms for tax type codes
                       108, 109, 503, and 505
                   (5) OH Akron added "FIRSTMERIT BANK N.A." as bank name in all Akron forms
                   (6) MD added forms for tax types 07100 and 07101.
                   (7) OH Sharonville added form for tax type 011
                   (8) NE Nebraska tax account and routing number changes

     Bug fixes:

--------------------------------------------------------------------------------------------
September 04, 2008 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Updated NM banking information, added companyentrydescription tags
                   (2) removed extra space in New Mexico form name for 040
     Bug fixes:
--------------------------------------------------------------------------------------------
September 01, 2008 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Updated GA banking information with new bank name, routing, and account number
                   (2) Updated NM banking information with new bank name, routing, and account numbers
                       for tax types 046,
                   (3) Added NM form for tax type code 040
     Bug fixes: (1)
--------------------------------------------------------------------------------------------
August 01, 2008 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1)  Added AL Montgomery County tax type 4801.
                   (2) Added IN tax forms for Type II Gaming (Tax type 14101).
     Bug fixes: (1) OR form for tax type code 01101 account number set to 0000015091
--------------------------------------------------------------------------------------------
May 01, 2008 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Remove Michigan SBT Quarterly estimated Tax Code 02100,
                       added MBT (Michigan Business Tax) quarterly estimated Tax Code 02155,
                       added new Bank Account Number for Tax Code 02155.
                   (2) Massachusetts, changed so that the Tax Types are right for the
                       following tax types: 01662, 01663, 01666, 01667,
                       01372, 01373, 01376, 01377, 01362, 01363, 01366, 01367,
                       01673, 01683, 01333, 01343, 01323, 01163, 01193, 01203,
                       01233, 01243, 01703, 01723, 01733, 01743, 01793, 01086,
                       01083, 01082, 01087, 01386, 01383, 01382, 01387, 01126
                       New Tax Types: 01293, 01303, 01043, 01283, 01153, 01136
                   (3) Michigan Beer Tax Type was 63 instead of 06300
                       Added Michigan Wine Tax Type 06200
--------------------------------------------------------------------------------------------
March 06, 2008 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) TX forms updated descriptions on 16020, 16080
                   (2) TX form added for tax type 16950.

--------------------------------------------------------------------------------------------
February 22, 2008 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Updated AL Baldwin County Sales Tax form with tax type 04801 to
                       include the correct routing number and account number
--------------------------------------------------------------------------------------------
January 31, 2008 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Added AL Baldwin County Sales Tax form with tax type 04801
                   (2) Added TX forms 16050, 48020, 48820, 36020, 36820, 32080, 13050,
                       13950, 49020, 49820, 37020, 37820, 32070, 42020, and 42820.
                   (3) Changed TX forms 16850 to 16080 and form 13580 to 13080
                   (4) Deleted TX forms 11020, 10020, 07020, 97020, 96020, 98020, and 08020
                   (5) Updated OR bank account number to 0000015035 from 0000015091

--------------------------------------------------------------------------------------------
October 31, 2007 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) AL added new forms for tax type codes 05710, 07250, 07260.
                       Removed obsolete forms for tax type codes 07120, 07125, 07130, 07145, 07150.
                   (2) IN removed obsolete forms for tax type code MFT and added new form for 050
                       and 04040
                   (3) IN updated tax type codes WTH to 011, RST to 040, TIF to 200,
                       PPD to 056, SFT to 052, COR to 021, FIT to 031, URT to 150.
                   (4) LA updated bank name
                   (5) LA removed obsolete forms for tax type code 02300.
                   (6) LA added new forms for tax type codes 06601 and 06602.
                   (7) VA added DMV forms for tax types 047 and 051.
                   (8) VA removed obsolete forms for tax type codes 05114
                   (9) VA added new form for tax type code 00049
                  (10) WV removed obsolete form for tax type code 05250 and 20099
                  (11) WV added forms for tax type codes 08689, 08687, 04251, 04252,
                       04253, 06225, 06226, 06327, 08697
                  (12) AR added new forms for tax type codes 04110 and 15000

--------------------------------------------------------------------------------------------
August 24, 2007 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) CA tax code 01300 removed the word "Quarterly" from description.
                   (2) CA tax code 01100 added the word "Weekly" to the description
                   (3) MS added tax types 04001, 04301, 04501, and 04801
                   (4) MS added tax type 01101
                   (5) MS added special formating requirements on TXP01, TXP03, and TXP04 for
 		       forms for tax type codes of 01101, 04001, 04301, 04501, and 04801

     Bug fixes: (1) AL added TXP6 as FILLER to all forms
                (2) AL no longer requires TXP07 (Confirmation Number)
                (3) OR changed TXP04 datatype to "AN" from "N" for tax types 20001 and 20201
                (4) NY tax type WT changed TXP10 datatype to "AN" from "ID"

--------------------------------------------------------------------------------------------
March 31, 2007 Release
--------------------------------------------------------------------------------------------
     Enhancements:  This release brings AL into compliance with current ACH requirements.
                        Most of the AL forms where affected.  The major changes included changes
                        to the Addenda format, addition of new forms, and the removal of obsolete forms.
                        Deleted forms include TXP types 01100, 04676, 07105, 07110, 07115, 07135, and 07140.
                        Added forms include: 07775, 07776, 07777, 07778, 04800, 04810, 05200, 05205,
                        08420, 08430, 08440, 07500, 11015, 11020, 11025, 05300, 05301, 01200, 02227,
                        02228, and 04195.
--------------------------------------------------------------------------------------------
January 31, 2007 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Added Federal forms 94400,94403, 94404, 94405, 94407, 94408, and 94409
--------------------------------------------------------------------------------------------
November 8, 2006 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Added PA Lancaster County Quarterly Employer Tax (01150) form
                   (2) Added PA Unemployement Compensation (UC001) form
     Bug fixes:  (1) Removed incorrect depositAccount information from Utah Child Support
                 (2) Removed duplicate incorrect depositAccount information from PA tax forms.
                 (3) North Dakota Corporate Income Tax Form, Amount Type changed to variable length
                 (4) Washington State Child Support Form, Add the routing number for US Bank of Washington

--------------------------------------------------------------------------------------------
June 28, 2006 Release
--------------------------------------------------------------------------------------------
     Bug fixes:  (1) Added support for <noDefaultFields> tag to fix SC Unemployment Insurance bug

--------------------------------------------------------------------------------------------
June 15, 2006 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Federal payments have several updates as follows:
                       - Northern USA and Southern USA will now have payments going to a single location.
                         Removed Northern USA Routing and Account Number pair.  Updated
                         bankName to "Treasury General Account"
                       - Excise Tax (form ids 1003, 1004, and 3311) Subcategories removed: 16-18,
                         53, 54, 67, 70, 72, 85-89, 95-98, and 103 and most of the remaining
                         descriptions have been updated.
                   (2) Added new WV tax payment form with tax type code 08689
                   (3) Added new WV tax payment form with tax type code 08692 and description of
                       "Waste Coal Severance Tax".  This is the 2nd form using this number.  They
                       differ only by description.
                   (4) DC tax forms Updated bankName to Wachovia
                   (5) Added new DC tax forms for tax type codes 00280 and 00380.
                   (6) DC tax forms added fields TXP06, TXP07, TXP08, TXP09, and TXP10 to all tax forms.
                   (7) Added new KY tax form for tax type code 049.

     Bug fixes:  (1) Removed duplicate forms with id 3280 and 1329.
                 (2) NH forms required swapping position of Interest and Penalty Amounts.
                     TXP06 is I, TXP07 is Interest Amount, TXP08 is P, TXP09 is Penalty Amount.
                 (3) DC tax forms changed tax codes 300, 350, 250, 260, and 400 to 00300, 00350,
                     00250, 00260, and 00400, respectively.
                 (4) Tennessee tax forms Taxpayer ID length changed from 8 to 9



--------------------------------------------------------------------------------------------
January 31, 2006 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) WY - Updated Bank from American National Bank to Wells Fargo Bank N.A.,
                            Routing# from 107000071 to 102301092, and account# from 0120927
                            to 5454302091
                   (2) WV - Removed outdated form for tax type 02091.
                   (3) WV - Added new form for tax type 07215.
                   (4) VA - On tax type 05114 changed TXP01 and TXP03 to more closely match
                            the descriptive labels within VA documentation.
                   (5) TN - Right filled account number with nine spaces on DOR forms.
                   (6) TN - DOR forms deleted are for tax type codes 01001, 01003, 01004, 01052,
                            02004, 02005, 02008, 02009, 04006, 04007, 04009, 04010, 06004, 06005,
                            10376, 10378, 12001, 12003, 12004, 12005, 13001, 13002, 13003,
                            13004, 13005, 13006, 14001, 14002, 14003, 15001, 15002, 15003,
                            15004, 15005, 15006, 16001, 16002, 16003, and 16004.
                            DOR forms added are for tax type codes 02013, 02014, 02015, and 02016.
                   (7) PA - Added forms for Tax Type Codes IS012, IA011, and MO013.
                   (8) OH - Added form 09601 Commercial Activity Tax


     Bug fixes:  (1) CO - corrected tax payment account number from 11934489513 to 1193489513.
                 (2) CA - forms containing taxpayer verification field are now editable.
                 (3) Fed forms for tax types 9410B, 94100, 94103, 94104, 94105, 94107, 94108,
                     and 94109 changed TXP04 from "1" to "SOCS", TXP06 from "2" to "MEDI",
                     and changed TXP08 from "3" to "WITH"
                 (4) Fed forms for tax types 10005 and 10007 changed TXP04 from "1" to "RRT1",
                     TXP06 from "2" to "RRT2", and changed TXP08 from "3" to "RRT3"

--------------------------------------------------------------------------------------------
October 15, 2005 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) AL - added newly required "Confirmation Number" TXP07 field to all forms.
                   (2) WI - added optional "Taxpayer Verification" TXP10 field to all forms.
                   (3) MN - updated new account number for Unemployment Insurance
                   (4) Fed - added Monthly Excise Tax form (720M)

--------------------------------------------------------------------------------------------
August 15, 2005 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) KY - added the IndividualName field to shorten the bank display name
                   (2) KS - (global change) added the IndividualName field to shorten the bank
                            display name
                   (3) AK - added the IndividualName field to shorten the bank display name
                   (4) NC - added the IndividualName field to shorten the bank display name
                   (5) DE - shortened bank name to Mellon Bank on forms 1339, 1341, 1342
                   (6) FL - shortened bank name to Bank of America in all forms
                   (7) WI - state taxes have had extensive changes plus 27 new forms as a result
                            of new documentation published 7/05.
                   (8) CA - shortened the bank name
                   (9) CT Child Support shortened the bank name
                   (10) IA Child Support shortened the bank name
                   (11) IN Child Support shortened the bank name
                   (12) AZ Child Support shortened the bank name
                   (13) CO Child Support shortened the bank name
                   (14) AK Child Support shortened the bank name
                   (15) MN Child Support shortened the bank name
                   (16) VA Child Support shortened the bank name

     Bug fixes:  (1) OH - added missing TXP02 values to forms 3140 and 3141
                 (2) WI - child support corrected bank name
                 (3) WY - corrected bank name
                 (4) UT - corrected bank name
                 (5) PA - added TXP06 and TXP07 fields to all corporation tax forms
                 (6) MT - removed value for TXP02 on Fuels Tax form

--------------------------------------------------------------------------------------------
July 15, 2005 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) MO State tax forms have updated descriptions and bank names.
                   (2) Added MO State tax forms for tax types 04198 and 04199.
     Bug fixes:

--------------------------------------------------------------------------------------------
June 15, 2005 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Added IN state tax form TIF.
                   (2) Added MD state tax form for Tax Type 130.
                   (3) Updated MN descriptions and added forms for Tax Types 001, 015, 018, 072,
                       074, 085, 086, 090, 103, 105, 106, 107, 125, 501,502, 503, 504, and 888
     Bug fixes:

--------------------------------------------------------------------------------------------
March 15, 2005 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Added ID state tax forms 04037 and 04038.
     Bug fixes: (1) Corrected "N" datatypes to "AN" on SC Unemployment Insurance form
--------------------------------------------------------------------------------------------
February 15, 2005 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1)
     Bug fixes: (1) Corrected "N" datatypes to "AN" on SC Unemployment Insurance form
--------------------------------------------------------------------------------------------
January 15, 2005 Release
--------------------------------------------------------------------------------------------
     Enhancements: (1) Added AK forms 07142 and 07160.
     Bug fixes: (1) Corrected taxcodes for WY forms 04102 through 4112.

--------------------------------------------------------------------------------------------
November 15, 2004 Release
--------------------------------------------------------------------------------------------
    Bug fixes: (1) Removed incorrect FIPS code from AK child support
               (2) CA State form 20000 added TXP10 field with 000000 as a value and filled amount
                   fields with leading zeros.
               (3) ND State tax form 30 TXP04 fixed to allow 3 or 4 letter values.
               (4) DE State tax forms with TXP02 of T and P, I, ZZ where removed.

    Enhancements: (1) DE published updates to State tax form 071.  As a result, all amount fields
                      are leading zero filled and the AN fields are trailing blank filled.  Added
                      <companyentrydiscription>TAXPAYMENT</companyentrydiscription>.
                  (2) DE State form 071 has three "Amount Type" fields.  They now default to
                      "Premium Tax Payment", "None", and "None", respectively.
                  (3) Added optional TXP10 to CA State forms 07130-07134, 07150, and 07153
                  (4) Made Access Code field required on NY State tax form "ST"
                  (5) AZ State forms all require Taxpayer Identification field be completed. This
                      field will now add leading zeros.

--------------------------------------------------------------------------------------------
October 15, 2004 Release
--------------------------------------------------------------------------------------------
    Bug fixes: (1) Added options for "Amount Types" (TXP04) to ND witholding tax form
               (2) Added to TN withholding form is a new empty field (TXP08) that should cause an
                   extra astrick to be placed in the addenda record following the Company Name field
               (3) Corrected SC state tax form's routing number for Sales Tax (tax code 040).
               (4) Corrected AR child support routing and account numbers.
               (5) Corrected CO child support routing number.
               (6) Corrected AL amount fields to be zero filled.
               (7) Corrected AZ amount fields to be zero filled.
               (8) Removed duplicate AL form for taxcode 02221.
               (9) Added several required fields to all NY state tax forms.
               (10)Removed the default FIPS value from AK child support.
               (11)Removed the default FIPS value from RI child support.
               (12)AR state tax forms will not accept only 12 digits in tax payer id.
               (13)TN state tax forms restricted Tax Payer ID to 8 digits.
               (14)CA state tax forms 01100, 01101, 01102, 01104, and 01300 now have leading
                   zeros added to amount fields.  Added a zero filled TXP10 field to each
                   of these forms.


    Enhancements: (1) Added ND state tax forms for tax codes 052, 053, 054, 055
                  (2) Added ND account number for form 022, 030, 080, 041S1, and 041S2
                  (3) Updated account number for OH state tax form 07200.
                  (4) Added account and routing numbers DE state tax form 1101.
                  (5) Added account and routing numbers to DE state tax forms 051 and 052. Also
                      added Interest and Penalty Payment fields to these forms.
                  (6) Added TX state tax forms 68301, 68302, 68303, 68304, 68305, 68310, 68311,
                      and 68312.
                  (7) Removed drop down options for Penalty, Interest, and Tax payments on SC state
                      tax forms and hard coded them to specific fields.
                  (8) Added the optional "Verification" field to CA state forms 04101, 04102, 04100,
                      05600, 09000, 09001, 09002, 09003.

--------------------------------------------------------------------------------------------
September 20, 2004 Release
--------------------------------------------------------------------------------------------
    Bug fixes: (1) FL state tax forms - corrected spelling of "ammended" to "amended".
               (2) DE Insurance Tax Code 71 - second and third amounts and amount types
                   are not "optional"
               (3) All 2 digit taxcodes have been made 3 digits by adding a zero to the front.
               (4) Corrected TXP02 value for Federal tax forms with taxcodes 88137 and 88317
               (5) Several IL state tax forms required a leading zero in the tax codes, they
                   have all been added.
               (6) To KY state forms added addendum fields for Reference Number, Principle and
                   Interest payments.
               (7) Added "Amount Type" selection options to ND state form with taxcode 080.
               (8) Added "Name Control" field to all NJ state forms.
               (9) Added "Taxpayer Verfication" field to OH state form for taxcode 011.
               (10)Corrected the routing number for SD child support.
               (11)Added "Taxpayer Verification" field to UT state tax forms.


    Enhancements: (1) TN forms 01006 and 02004 had identical descriptions. They are now differentiated.
                  (2) Added optional Taxpayer Verification field to VA State tax forms.
                  (3) Added Fed forms with taxcodes 94100 and 94105.
                  (4) Improved all Fed form descriptions by including full 5 digit taxcodes in
                      description.
                  (5) Added KY state deposit account routing and account numbers to state tax forms.
                  (6) Added optional Interest, Penalty, and Reference Number fields to KY State
                      tax forms.
                  (7) Updated all KY state form descriptions and lengthened 2 digit tax codes to 3.
                  (8) Added KY State form with taxcode 091.
                  (9) Added Federal EIN and Employer Name fields to TN unemployment form, tax
                      type 99999.
                  (10)Added the taxcode to the descriptions of AL state tax forms.
                  (11)Added the following ID state tax forms: 13090, 13091, 13092,and 20180
                  (12)Added Taxpayer Verification field to MN state tax forms.
                  (13)Expanded NH state taxforms to several more descriptive forms.
                  (14)Added PA state tax forms banking account and routing numbers.
                  (15)Added penalty and interest fields to SD state forms
                  (16)Added UT state tax witholding bank acct and routing information.
                  (17)Added VT banking acct and routing info to both state tax forms and child support.
                  (18)Added VA banking acct and routing info to child support form.


--------------------------------------------------------------------------------------------
August 15, 2004 Release
--------------------------------------------------------------------------------------------
    Bug fixes: (1) Colorado State form "Liquour Excise Tax" now uses the correct tax code of "065".
               (2) Several forms had duplicate values in their "id" fields. Consequently, they have
                   been updated with new unique values from 5135 to 5148.
               (3) Removed duplicate form from AL with Tax Code 7150.
               (4) AL taxpayer ID field, TXP01, has been changed to 12 characters from 15.
               (5) Removed duplicate form 065 "Liquor Excise" for CO.
               (6) Added "Amount Type" (TXP04) to FL tax forms.
    Enhancements: (1) California State form with tax code 02516 has an improved description.
                  (2) Added Child Support payment form for California.
                  (3) Added default routing number and bank account number to District of Columbia's
                      tax forms.
                  (4) Added CA Special Tax Forms 09000, 09001, 09002, 09003.
                  (5) Added TXP06, TXP07, TXP08, and TXP09 fields to Alaska State Tax forms.
                  (6) Added DE Insurance Form, 071.
                  (7) Added MO state tax forms for taxcodes 04205, 04206, and 04207
                  (8) Added NC state tax form for taxcode 04111
--------------------------------------------------------------------------------------------
July 15, 2004 Release
--------------------------------------------------------------------------------------------
    Bug fix: Removed duplicate Wyoming state forms
--------------------------------------------------------------------------------------------
June 15, 2004 Release
--------------------------------------------------------------------------------------------
    Initial product release.

**************************************************************************
Enhancements and Bug Reporting
**************************************************************************
With your help, we are pleased to continue expanding ACH payment options.  If you know of ACH payment forms that we are missing or new forms that should be added, please forward your request to support@financialfusion.com and to ACHupdates@earthlink.net.  Include as much of the following information as possible and as applicable:
- Form name
- Form number or tax code
- Routing number
- Account number
- Federal, State, County, and/or City
- Any additional fields or formats that may be required
- Any links to additional information

We make every effort to proactively maintain accurate information, but occasionally ACH payment requirements change without notice.  When you note inaccuracies or required changes, please forward this information to us at support@financialfusion.com.

We appreciate your help!

**************************************************************************
Disclaimer
**************************************************************************
This software is provided "as is" without warranty of any kind. Any use of the taxforms.xml file is at your own risk. Sybase disclaims any implied warranty of merchantability and/or fitness for a particular purpose.
