<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_removestop" className="moduleHelpClass"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<div class="blockWrapper">
	<div  class="blockHead">Stop Payment Details</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					
						<s:text name="jsp.default_21"/>:
				</span>
				<span class="columndata lightBackground">
					<s:property value="stopCheck.accountDisplayText" />
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					
						<s:text name="jsp.default_92"/>
				</span>
				<span class="columndata lightBackground">
					<s:property value="stopCheck.checkNumbers"/> 
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" >
						<s:text name="jsp.default_45"/>:
				</span>
				<span class="columndata" > 
					<s:property value="stopCheck.AmountValue.CurrencyStringNoSymbol"/><s:property value="stopCheck.account.currencyCode"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
						<s:text name="jsp.stops_11"/>
				</span>
				<span class="columndata lightBackground">
					<s:property value="stopCheck.reason"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel" >
				
					<s:text name="jsp.default_316"/>
			</span>
			<span class="columndata" >
				<s:property value="stopCheck.payeeName"/>
			</span>
		</div>
	</div>
</div>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div align="center" class="ui-widget-header customDialogFooter">                                           
<s:url id="deleteStopsUrl" namespace="/pages/jsp/stops" action="deleteStopCheckAction.action">
    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
    </s:url>
<sj:a button="true" cssClass="buttonlink ui-state-default ui-corner-all" title="%{getText('jsp.default_83')}" onClickTopics="closeDeleteStopsDialog"><s:text name="jsp.default_82"/></sj:a>
<sj:a id="deleteStopsLink" href="%{deleteStopsUrl}" targets="resultmessage" button="true"
      title="%{getText('jsp.stops_12')}" onCompleteTopics="cancelStopsFormComplete" onSuccessTopics="cancelStopsSuccessTopics" onErrorTopics="errorDeleteStops"
      effectDuration="1500" cssClass="buttonlink ui-state-default ui-corner-all" ><s:text name="jsp.default_162" /></sj:a>
</div>
