<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.accounts.Accounts,
                 com.ffusion.beans.user.UserLocale" %>
<ffi:help id="payments_placestoppayment" className="moduleHelpClass"/>

<span class="shortcutEntitlementClass" style="display:none;">Stops</span>
<span id="PageHeading" style="display:none;"><s:text name="jsp.stops_7"/></span>
<span class="shortcutPathClass" style="display:none;">pmtTran_stops,stopsLink</span>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<script language="JavaScript">
    <!--
    function checkDate() {
        return true;
    }

    $(function(){
	
			$('#currencyCode').html($('#curCode').val());
	       //$("#accountID").combobox({'size':'65'});
        $("#accountID").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"module":"stopspayment",
			"size":"40",
			"source":"/cb/pages/jsp/stops/StopsPaymentAccountsLookupBoxAction.action"
		});
        
		$('#AddStopCheck_reason_ID').selectmenu({ 'width' : '9.4em' });
		//$('#currencyCode').html($('#accountID :selected').attr("currencycode")); // default value.
		accountSelect();
		$("#accountID").change(accountSelect);
    });
	
	 function accountSelect() {
		var accDis = $('#accountID :selected').text();
		$('#accountDisplayText').val(accDis);
		var selectedVal = this.value;
			if(selectedVal!=null && selectedVal!="" && selectedVal!= "undefined"){
			var currency = selectedVal.split(",")[1];
			$('#currencyCode').html(currency);
			var accntId = selectedVal.split(",", 1);
			$('#acctId').val(accntId);
		} 
	}

    // --></script>
	<s:actionerror />
	<s:form id="StopPaymentForm" namespace="/pages/jsp/stops" validate="false" action="ModStopCheck_verify" method="post" theme="simple" name="FormName">
		<s:fielderror />
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="modStopCheck.accountDisplayText" value='<s:property value="modStopCheck.accountDisplayText"/>' id="accountDisplayText">
		<input type="hidden" name="modStopCheck.accountID" id="acctId" value='<s:property value="modStopCheck.accountID" />'>
		<input type="hidden" name="modStopCheck.currencyCode" id="curCode" value='<s:property value="modStopCheck.currencyCode"/>'>
	
		<ul id="formerrors"></ul>
		<div class="leftPaneWrapper" role="form" aria-labelledby="account">
			<div class="leftPaneInnerWrapper">
				<div class="header"><h2 id="account"><s:text name="jsp.default_15"/> *<h2></div>
				<div class="leftPaneInnerBox leftPaneLoadPanel">
					<div>
						<select id="accountID" class="txtbox"  size="1">
						<option value='<s:property value="modStopCheck.accountID" default="" />' selected>
						<s:property value="modStopCheck.accountDisplayText" default="Default" /></option>
						</select>
					</div>
					<div class="marginTop10"><span id="accountIDError"></span></div>
				</div>
			</div>
		</div>
		<div class="rightPaneWrapper w71">
			<div class="paneWrapper" role="form" aria-labelledby="stopPaymentDetail">
			  	<div class="paneInnerWrapper">
			   		<div class="header"><h2 id="stopPaymentDetail">Stop Payment Details</h2></div>
			   		<div class="paneContentWrapper">
			   			<table width="100%" border="0" cellspacing="2" cellpadding="2">
							<tr>
								<td class="sectionsubhead" width="50%"><label for="PayeeName"><s:text name="jsp.default_314"/></label><span class="required" title="required">*</span></td>
								<td class="sectionsubhead" width="50%"><label for="check#"><s:text name="jsp.default_92"/></label><span class="required" title="required">*</span> <%-- <s:text name="jsp.stops_1"/> --%></td>
							</tr>
							<tr>
								<td class="lightBackground" align="left" valign="middle">					
									<input class="txtbox ui-widget ui-widget-content" type="text" size="18" aria-required="true" name="modStopCheck.payeeName" value='<s:property value="modStopCheck.payeeName"/>' id="PayeeName" maxlength="30">
								</td>
								<td class="lightBackground" align="left" valign="middle">
									<input placeholder="from" class="txtbox ui-widget ui-widget-content" type="text" aria-required="true" name="modStopCheck.fromCheckNumber" id="check#" value='<s:property value="modStopCheck.fromCheckNumber"/>' size="8">
									<span class="sectionsubhead"> <s:text name="jsp.default_423.1"/> </span>
									<input placeholder="to" class="txtbox ui-widget ui-widget-content" type="text" aria-required="true" name="modStopCheck.toCheckNumber" id="check#" value='<s:property value="modStopCheck.toCheckNumber"/>'  size="8">&nbsp;
								</td>
							</tr>
							<tr>
								<td><span id="payeeNameError"></span></td>
								<td><span id="fromCheckNumberError"></span></td>
							</tr>
							<tr>
								
							</tr>
							<tr>
								<td class="sectionsubhead" ><label for="Date"><s:text name="jsp.default_94"/></label><span class="required" title="required">*</span></td>
								<td class="sectionsubhead" ><label for="amount"><s:text name="jsp.default_43"/></label></td>
							</tr>
							<tr>
								<td class="lightBackground" align="left" valign="middle">
									<sj:datepicker  
										id="Date" 
										name="modStopCheck.checkDate" 
										label="%{getText('jsp.default_94')}"
										maxlength="10" buttonImageOnly="true"  
										aria-required="true"
										
										cssClass="ui-widget-content ui-corner-all"/>
									<script>
										var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=false&calDisplay=server&calForm=StopPaymentForm&calMinDate=6M&calMaxDate=2W"/>';
										ns.common.enableAjaxDatepicker("Date", tmpUrl);
										 $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>');
										
	</script>	
						
						
								</td>
								<td align="left"><input class="txtbox  ui-widget ui-widget-content" type="text" id="amount" name="modStopCheck.amountAsString" value='<s:property value="%{modStopCheck.AmountValue.AmountValue}"/>' size="8" maxlength="15">
									<span id="currencyCode"></span>
								</td>
							</tr>
							<tr>
								<td><span id="checkDateError"></span></td>
								<td><span id="amountError"></span></td>
							</tr>
							<tr>
								<td class="columndata lightBackground"  colspan="2"><s:text name="jsp.default_338"/>:</span> <span class="required">*</span></td>
							</tr>
							<tr>
								<td colspan="2">
									<s:select label="Reason"
									   id="AddStopCheck_reason_ID"
									   name="modStopCheck.reason"
									   list="stopReasons"
									   value="modStopCheck.reason"
									   cssClass="txtbox"
									/>
								</td>
							</tr>
							<tr><td colspan="2"><span id="reasonError"></span></td></tr>
						</table>
<div class="required"><s:text name="stops.multiple.checks.note" /></div>
<div align="center" class="marginTop10"><span class="required">* <s:text name="jsp.default_240"/></span></div>
<div class="btn-row">
	<sj:a id="cancelFormButtonOnInput"
	button="true"
	onClickTopics="cancelPaymentStopForm"
	><s:text name="jsp.default_82"/></sj:a>
	&nbsp;
	<sj:a
	  id="editTransferSubmit"
	  formIds="StopPaymentForm"
	  targets="verifyDiv"
	  button="true"
	  validate="true"
	  validateFunction="customValidation"
	  onBeforeTopics="beforeVerifyStopPaymentForm"
	  onCompleteTopics="verifyStopPaymentFormComplete"
	  onErrorTopics="errorVerifyStopPaymentForm"
	  onSuccessTopics="successVerifyStopPaymentForm">
		<s:text name="jsp.default_366" />
	</sj:a>
</div>
		   		</div>
		   	</div>
		  </div>
	</div>
</s:form>


