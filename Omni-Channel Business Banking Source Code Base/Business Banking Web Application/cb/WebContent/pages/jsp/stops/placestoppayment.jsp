<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.accounts.Accounts,
                 com.ffusion.beans.user.UserLocale" %>
                 
<ffi:help id="pyments_add_new_stop_payment" className="moduleHelpClass"/>

<span class="shortcutEntitlementClass" style="display:none;">Stops</span>
<span id="PageHeading" style="display:none;"><s:text name="jsp.stops_9"/></span>
<span class="shortcutPathClass" style="display:none;">pmtTran_stops,stopsLink</span>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<script language="JavaScript">
    <!--
    function checkDate() {
        return true;
    }

    $(function(){
        //$("#accountID").combobox({'size':'65'});
        $("#accountID").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"module":"stopspayment",
			"size":"40",
			"source":"/cb/pages/jsp/stops/StopsPaymentAccountsLookupBoxAction.action"
		});
        
		$('#AddStopCheck_reason_ID').selectmenu({ 'width' : '9.4em' });
		//$('#currencyCode').html($('#accountID :selected').attr("currencycode")); // default value.
		accountSelect();
		$("#accountID").change(accountSelect);
		
		$("#Date").css("width", "113");
    });
	
 
	function accountSelect() {
		var accDis = $('#accountID :selected').text();
		$('#accountDisplayText').val(accDis);
		var selectedVal = this.value;
		if (selectedVal != null && selectedVal != "" && selectedVal != "undefined") {
			var currency = selectedVal.split(",")[1];
			$('#currencyCode').html(currency);
			var accntId = selectedVal.split(",", 1);
			$('#acctId').val(accntId);
		}
	}
// -->
</script>
	<s:actionerror />
	<s:form id="StopPaymentForm" namespace="/pages/jsp/stops" validate="false" action="AddStopCheck_verify" method="post" theme="simple" name="FormName">
		<s:fielderror />
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="stopCheck.accountDisplayText" value='<s:property value="stopCheck.accountDisplayText"/>' id="accountDisplayText">
		<input type="hidden" name="stopCheck.accountID"  id="acctId">
				
		<div class="leftPaneWrapper" role="form" aria-labelledby="account">
			<div class="leftPaneInnerWrapper">
				<div class="header"><h2 id="account"><s:text name="jsp.default_15"/> *<h2></div>
				<div class="leftPaneInnerBox leftPaneLoadPanel">
					<div><select id="accountID" class="txtbox" size="1">
							<option value="" selected>Default</option>
						</select></div>
						<div class="marginTop10"><span id="accountIDError"></span></div>
				</div>
			</div>
		</div>
		<div class="rightPaneWrapper w71">
			<div class="paneWrapper" role="form" aria-labelledby="stopPaymentDetail">
			  	<div class="paneInnerWrapper" >
			   		<div class="header"><h2 id="stopPaymentDetail">Stop Payment Details</h2></div>
			   		<div class="paneContentWrapper">
			   			<table width="100%" border="0" cellspacing="2" cellpadding="2" class="tableData formTablePadding5">
			<tr>
				<td colspan="2" align="center"  style="padding:0"><ul style="margin:0" id="formerrors"></ul></td>
			</tr>
			<tr>
				<td class="sectionsubhead" width="50%">
					<label for="PayeeName"><s:text name="jsp.default_314"/> </label>
				</td>
				<td class="sectionsubhead" width="50%">
					<label for="check#"><s:text name="jsp.default_92"/><span class="required" title="required">*</span> <%-- <s:text name="jsp.stops_1"/> --%>
				</td>
			</tr>
			<tr>
				<td class="lightBackground">				
				   
					<input class="txtbox ui-widget ui-widget-content ui-corner-all" type="text" size="42" aria-required="true" name="stopCheck.payeeName" value='<s:property value="stopCheck.payeeName"/>' id="PayeeName" maxlength="30">
				</td>
				<td class="lightBackground">
					<input placeholder="from" class="txtbox ui-widget ui-widget-content ui-corner-all" type="text" aria-required="true" name="stopCheck.fromCheckNumber" id="check#" value='<s:property value="stopCheck.fromCheckNumber"/>' size="8" maxlength="50">
					<span class="sectionsubhead"> - </span>
					<input placeholder="to" class="txtbox ui-widget ui-widget-content ui-corner-all" type="text" aria-required="true" name="stopCheck.toCheckNumber" id="check#" value='<s:property value="stopCheck.toCheckNumber"/>' size="8" maxlength="50">&nbsp;
				</td>
			</tr>
			<tr>
				<td><span id="payeeNameError" style="display:none"></span></td>
				<td><span id="fromCheckNumberError" style="display:block"></span></td>
			</tr>
			<tr>
				<td class="sectionsubhead" >
						<label for="Date"><s:text name="jsp.default_94"/></label><span class="required"  title="required">*</span>
				</td>
				<td class="sectionsubhead"><label for="amount"><s:text name="jsp.default_43"/></label><span class="required" title="required">*</span></td>
			</tr>
			<tr>
				<td class="lightBackground">
					<sj:datepicker 
						value="%{#session.stopCheck.checkDate}" 
						id="Date" 
						name="stopCheck.checkDate" 
						label="%{getText('jsp.default_94')}"
						maxlength="10" buttonImageOnly="true"  
						aria-required="true"
						cssClass="ui-widget-content ui-corner-all"/>
					
					<script>
						var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=false&calDisplay=server&calForm=StopPaymentForm&calMinDate=6M&calMaxDate=2W"/>';
						ns.common.enableAjaxDatepicker("Date", tmpUrl);
						 $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>');
					</script>	
				  	    
					
				</td>
				<td><input class="txtbox  ui-widget ui-widget-content ui-corner-all" id="amount" type="text" name="stopCheck.amountAsString" value='<s:property value="stopCheck.AmountValue.AmountValue"/>' size="13" maxlength="15">
					<span id="currencyCode"></span>
				</td>
			</tr>
			<tr>
				<td><span id="checkDateError"></span></td>
				<td><span id="amountError"></span></td>
			</tr>
			<tr>
				<td class="sectionsubhead" colspan="2" title="required"><label for="AddStopCheck_reason_ID"><s:text name="jsp.default_338"/></label><span class="required" title="required">*</span></td>
			</tr>
			<tr>
				<td colspan="2">
				<s:if test="%{#session.stopReasons==null}">
					<s:action var="initAddStopCheck"  name="initAddStopCheck" executeResult="false" namespace="/pages/jsp/stops"/>					
					<s:set name="stopReasons" value="%{#initAddStopCheck.stopReasons}" scope="session" /> 
				</s:if>
				<s:else>
					<s:set name="stopReasons" value="%{#session.stopReasons}"/>
				</s:else>
				<s:if test="%{#session.stopReasons==null}">								
					<s:select label="Reason"
					   id="AddStopCheck_reason_ID"
					   name="stopCheck.reason"
					   list="{}"
					   value="stopCheck.reason" 
					   aria-required="true"
					   cssClass="txtbox"
					/>
				</s:if>
				<s:else>
					<s:set name="stopReasons" value="%{#session.stopReasons}"/>
					<s:select label="Reason"
					   id="AddStopCheck_reason_ID"
					   name="stopCheck.reason"
					   list="stopReasons"
					   value="stopCheck.reason" 
					   aria-required="true"
					   cssClass="txtbox"
					/>
				</s:else>
					
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<span id="reasonError"></span>
				</td>
			</tr>
			
		</table>
<div class="required"><s:text name="stops.multiple.checks.note" /></div>
<div align="center" class="marginTop10"><span class="required" title="required">* <s:text name="jsp.default_240"/></span></div>
<div class="btn-row">
	<sj:a id="cancelFormButtonOnInput"
	summaryDivId="stopsSummaryGrid"
	buttonType="cancel"
	button="true"
	onClickTopics="showSummary,cancelPaymentStopForm"
	><s:text name="jsp.default_82"/></sj:a>
	&nbsp;
	<sj:a
	id="editTransferSubmit"
	formIds="StopPaymentForm"
	targets="verifyDiv"
	button="true"
	validate="true"
	validateFunction="customValidation"
	onBeforeTopics="beforeVerifyStopPaymentForm"
	onCompleteTopics="verifyStopPaymentFormComplete"
	onErrorTopics="errorVerifyStopPaymentForm"
	onSuccessTopics="successVerifyStopPaymentForm"
	><s:text name="jsp.default_395" /></sj:a>
</div>
				</div>
			</div>
		</div>
	</div>		
</s:form>


