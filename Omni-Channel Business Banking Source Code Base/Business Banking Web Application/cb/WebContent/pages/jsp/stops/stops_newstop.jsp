<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<s:set var="tmpI18nStr" value="%{getText('jsp.stops_21')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:help id="payments_newstop" className="moduleHelpClass"/>
    <div id="stopPaymentPanel" class="formPanel" align="center">
    	<s:include value="stops_selected.jsp" />
        <s:include value="placestoppayment.jsp" />
    </div><!-- stopPaymentPanel -->

