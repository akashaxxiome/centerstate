<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div class="dashboardUiCls">
<div class="moduleSubmenuItemCls">
	<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-stops"></span>
<span class="moduleSubmenuLbl">
<s:text name="jsp.stops_17" />
</span>
<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>

<!-- dropdown menu include -->    		
<s:include value="/pages/jsp/home/inc/transferSubmenuDropdown.jsp" />
</div>
<!-- dashboard items listing for transfers -->
        <div id="dbStopSummary" class="dashboardSummaryHolderDiv">
        	<s:url id="goBackSummaryUrl" value="/pages/jsp/stops/stops_summary.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
	
			<!-- Stops summary link -->
			<sj:a id="goBackSummaryLink" href="%{goBackSummaryUrl}" targets="summary"
				button="true" cssClass="summaryLabelCls"
				title="%{getText('jsp.billpay_go_back_summary')}"
				onClickTopics="summaryLoadFunction">
				  	<s:text name="jsp.billpay_summary" />
			</sj:a>
			
			
        	<s:url id="singleStopsUrl" value="/pages/jsp/stops/initAddStopCheck.action">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            </s:url>
		    <sj:a id="stopsLink" href="%{singleStopsUrl}" targets="inputDiv" button="true"
		    	title="%{getText('jsp.stops_28')}"
		    	onClickTopics="beforeLoadStopPaymentForm" onCompleteTopics="loadStopPaymentFormComplete" onErrorTopics="errorLoadStopPaymentForm">
			  	&nbsp;<s:text name="jsp.stops_27" /></sj:a>
			  	
			  	<%-- TODO: Get some appdashboard-right items for Stop Payments, - place stop payment, etc. --%>

		        <%-- add single transfer --%>
		        <s:url id="addStopPaymentUrl" value="/pages/jsp/stops/initAddStopCheck.action" escapeAmp="false">
		            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		        </s:url>
		        <sj:a id="addStopPaymentLink" href="%{addStopPaymentUrl}" targets="inputDiv" button="true" title="%{getText('jsp.stops_2')}" cssStyle="display:none"
		            onClickTopics="beforeLoadStopPaymentForm" onCompleteTopics="loadStopPaymentFormComplete" onErrorTopics="errorLoadStopPaymentForm">
		          <span class="ui-icon ui-icon-wrench"></span>
		        </sj:a>
		
				<%-- edit single transfer --%>
			    <s:url id="editStopPaymentUrl" value="/pages/jsp/stops/modstoppayment.jsp" escapeAmp="false">
		            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
			    <sj:a id="editStopPaymentLink" href="%{editStopPaymentUrl}" targets="inputDiv" button="true" title="%{getText('jsp.stops_29')}" cssStyle="display:none"
			    	onClickTopics="beforeLoadStopPaymentForm" onCompleteTopics="loadStopPaymentFormComplete" onErrorTopics="errorLoadStopPaymentForm">
			      <span class="ui-icon ui-icon-wrench"></span>
			    </sj:a>
        </div>
</div>
  <%--   <div id="appdashboard-left" class="formLayout" style="width:100%;">
		<sj:a
			id="stopsquickSearchLink"
			button="true"
			buttonIcon="ui-icon-search"
            onClickTopics="onClickQuickSearchStopsFormTopics" 
		><s:text name="jsp.default_4"/></sj:a>

	    
        
    </div> --%>
	
   <%--  <span class="ui-helper-clearfix">&nbsp;</span> --%>


<%-- Following three dialogs are used for view, delete and inquiry transfer --%>

<sj:dialog id="InquiryStopsDialogID" cssClass="pmtTran_stopsDialog" title="%{getText('jsp.default_249')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
</sj:dialog>

<sj:dialog id="viewStopsDetailsDialogID" cssClass="pmtTran_stopsDialog" title="%{getText('jsp.stops_19')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800">
</sj:dialog>

<sj:dialog id="deleteStopsDialogID" cssClass="pmtTran_stopsDialog" title="%{getText('jsp.stops_5')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800">
</sj:dialog>
<script>

$("#goBackSummaryLink").find("span").addClass("dashboardSelectedItem");

</script>

