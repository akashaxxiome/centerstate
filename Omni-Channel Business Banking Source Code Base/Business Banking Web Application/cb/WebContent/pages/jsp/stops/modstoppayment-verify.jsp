<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_stoppayments-verify" className="moduleHelpClass"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<s:set name="strutsActionName" value="%{'ModStopCheck_execute'}" scope="request"/>
<s:form id="StopsNewVerify" namespace="/pages/jsp/stops" validate="false" action="ModStopCheck_execute" method="post" name="StopsNewVerify" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper" role="form" aria-labelledby="stopPaymentSummary">
			<div class="leftPaneInnerWrapper">
				<div class="header"><h2 id="stopPaymentSummary">Stop Payment Summary</h2></div>
				<div class="leftPaneInnerBox summaryBlock">
					<div style="width:100%">
						<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_15"/>:</span>
						<span class="inlineSection floatleft labelValue"><s:property value="modStopCheck.accountDisplayText" /></span>
					</div>
				</div>
			</div>
		</div>
		<div class="rightPaneWrapper w71">
			<div class="paneWrapper" role="form" aria-labelledby="stopPaymentDetail">
			  	<div class="paneInnerWrapper">
			   		<div class="header"><h2 id="stopPaymentDetail">Stop Payment Details</h2></div>
			   		<div class="paneContentWrapper">
			   			<div class="blockContent">
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel"  >
											 <s:text name="jsp.default_92"/>:
									</span>
									<span class="sectionsubhead sectionLabel"  >
										<s:property value="modStopCheck.checkNumbers"/>
									</span>
								</div>
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel"  >
											<s:text name="jsp.default_43"/>:
									</span>
									<span class="columndata">
										<s:property value="modStopCheck.AmountValue.CurrencyStringNoSymbol"/>&nbsp;<s:property value="modStopCheck.account.currencyCode"/>
									</span>
								</div>
							</div>
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel"  >
											<s:text name="jsp.default_338"/>:
									</span>
									<span class="columndata lightBackground"  >
										<s:property value="modStopCheck.reason"/>
									</span>
								</div>
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel"   >
											<s:text name="jsp.default_313"/>:
									</span>
									<span class="columndata"  >			
										<s:property value="modStopCheck.payeeName"/>
									</span>
								</div>
							</div>
						</div>
			   		</div>
			   	</div>
			   	<div class="btn-row">
					<sj:a id="cancelFormButtonOnVerify"
							button="true"
							onClickTopics="cancelPaymentStopForm"
					><s:text name="jsp.default_82"/></sj:a>
					<sj:a id="backFormButton"
							button="true"
							onClickTopics="stops_backToInput"
					><s:text name="jsp.default_57"/></sj:a>
					<sj:a
						id="sendStopPaymentSubmit"
						formIds="StopsNewVerify"
						targets="confirmDiv"
						button="true"
						onBeforeTopics="beforeSendStopPaymentForm"
						onSuccessTopics="sendStopPaymentFormSuccess"
						onErrorTopics="errorSendStopPaymentForm"
						onCompleteTopics="sendStopPaymentFormComplete"
					><s:text name="jsp.default_107"/></sj:a>
				</div>
			 </div>
		</div>
</s:form>
<div class="hidden" id="stopPaymentResultMessage">
<s:text name="jsp.stops_20"/>
</div>