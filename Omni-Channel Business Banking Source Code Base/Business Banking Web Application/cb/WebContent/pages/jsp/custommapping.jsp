<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%
		session.setAttribute("MappingDefinitionID", request.getParameter("MappingDefinitionID"));
		String mappingID = request.getParameter("MappingDefinitionID");
%>

<ffi:setProperty name="MappingDefinition" property="MappingID" value="<%= mappingID %>"/>

<ffi:cinclude value1="${MappingDefinitionID}" value2="" operator="notEquals">
    <ffi:setProperty name="SetMappingDefinition" property="ID" value="${MappingDefinitionID}"/>
    <ffi:removeProperty name="MappingDefinitionID"/>
</ffi:cinclude>
<ffi:process name="SetMappingDefinition"/>

<ffi:cinclude value1="${MappingDefinition.MappingID}" value2="0" operator="equals">
	<s:include value="/pages/jsp/custommapping_add.jsp"/>
</ffi:cinclude>

<ffi:cinclude value1="${MappingDefinition.MappingID}" value2="0" operator="notEquals">
	<ffi:cinclude value1="${MappingDefinition.InputFormatType}" value2="0">
		<s:include value="/pages/jsp/custommapping_delimited.jsp"/>
	</ffi:cinclude>

	<ffi:cinclude value1="${MappingDefinition.InputFormatType}" value2="1">
		<s:include value="/pages/jsp/custommapping_fixed.jsp"/>
	</ffi:cinclude>
</ffi:cinclude>