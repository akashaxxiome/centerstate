<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="home_leftbar_alertsmallpanel" />
<ffi:setProperty name="Alert-Message" value="Alert"/>

<div id="msgsMasterWrapper">
	<div id="alertsTopActionbar" class="alertsTopActionBarCls">
		<div class="floatleft topActionItemsCls">
			<span  onclick="ns.home.loadUnReadAlerts(5)" class="topActionItemSelected" id="unReadAlert" ><s:text name="alerts.unread" /></span>
			<span onclick="ns.home.loadReadAlerts(5)" id="readAlert"><s:text name="Read" /></span>
		</div>
	</div>
	
	<div class="msgItemHolderDiv">
		<!-- if there's no alert -->
		<s:if test="%{messages.size()==0}">
			<span class="noMsgSpan"><s:text name="jsp.alert_61" /></span>
		</s:if>
		<!-- if there're alerts -->
		<ffi:cinclude value1="0" value2="${messages.Size}" operator="notEquals">
			<% boolean toggle = false;  int counter = 1; %>
			<s:set var="count" value="0" />
			<ffi:list collection="messages" items="Message1">
			<s:if test="%{#count<messageSearchCriteria.pageInfo.pageSize}">
				<% toggle = !toggle; %>
				<ffi:setProperty name="Message1" property="DateFormat" value="${UserLocale.DateTimeFormat}" />
				<div class='alertDataHolder' id='<ffi:getProperty name="Message1" property="ID"/>'>
					<a href="javascript:void(0)" onclick="ns.home.quickViewAlert('<ffi:urlEncrypt url="/cb/pages/jsp/alert/alertQuickViewAction.action?alertId=${Message1.ID}"/>', '<ffi:getProperty name="Message1" property="ID"/>', true)">
						<div class="alertSenderDateDetails">
							<span class="alertSubject">
								<ffi:getProperty name="Message1" property="subject" />
							</span>
							<span id="dateField" class="alertDate"><ffi:getProperty name="Message1" property="DateValue" />
							</span>
						</div>
						<div class="alertDetails">
							<ffi:getProperty name="Message1" property="memo" />
						</div>
					</a>
					
					<div class="alertActionItemHolder">
						<span class="ffiUiIcoMedium ffiUiIco-icon-medium-delete" title="<s:text name='message.delete' />" onclick="ns.home.quickDeleteAlert('<ffi:urlEncrypt url="/cb/pages/jsp/alert/deleteInboxAlertsAction_confirm.action?selectedMsgIDs=${Message1.ID}&quickDelete=true"/>' ,'<ffi:getProperty name="Message1" property="ID"/>' ,false)"></span>
					</div>
				</div>
			</s:if>
			<s:set var="count" value="%{#count+1}" />
			</ffi:list>
			<input type="hidden" id="alertsPageSize" value="<s:property value='%{messageSearchCriteria.pageInfo.pageSize}'/>"/>
			<input type="hidden" id="alertsCount" value="<s:property value="%{messageSearchCriteria.pageInfo.totalRecords}"/>"/>
			<s:if test="%{messageSearchCriteria.pageInfo.totalRecords>5 && messageSearchCriteria.pageInfo.pageSize<messageSearchCriteria.pageInfo.totalRecords}">
				<tr class="boldIt" style="text-align: center!important;cursor: pointer;">
					<span class="alertMoreBtnSpan" onclick="updateAlerts();"><s:text name="message.more" /><span class="alertMoreLinkDownArrow ffiUiIcoSmall ffiUiIco-icon-downArrow marginleft5"></span></span>
				</tr>
			</s:if>
		</ffi:cinclude>
	</div>
</div>

<script type="text/javascript">

	function updateAlerts(){
		var messagesCount = parseInt($('#alertsCount').val());
		var pageSize = parseInt($('#alertsPageSize').val());
		if(messagesCount>pageSize){
			pageSize+=5;
			$('#alertsPageSize').val(pageSize);
			if('<s:property value="%{messageSearchCriteria.unReadOnly}"/>'=='true'){
				ns.home.loadUnReadAlerts($('#alertsPageSize').val());
			}else{
				ns.home.loadReadAlerts($('#alertsPageSize').val());
			}
		}
	}

	$(document).ready(function(){
	  	$("#alertSmallPanelPortlet").find("tr.quickMessageStyle").hover(function() {
	  		$(this).find(".messageActionsWrapper").show();
		},function() {
			$(this).find(".messageActionsWrapper").hide();
		});
	});
</script>