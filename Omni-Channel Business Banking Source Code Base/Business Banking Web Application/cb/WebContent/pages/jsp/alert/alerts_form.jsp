<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<!-- This form is not in use anymore -->
<div id="alertFormPortlet">
<s:form id="alertForm" action="" namespace="/pages/jsp/alert" method="post" name="alertForm" theme="simple">
	<div id="alertPortletTitle">
		<span id="accBalanceThresholdPortletTitle" alertcode="1"><s:text name='alerts.accountBalanceThresholdAlertForm.portlettitle'/></span>
		<span id="accountBalanceSummaryPortletTitle" alertcode="2"><s:text name='alerts.accountBalanceSummaryAlertForm.portlettitle'/></span>
		<span id="insufficentFundsPortletTitle" alertcode="4"><s:text name='alerts.insufficientFundsAlertForm.portlettitle'/></span>
		<span id="transactionPortletTitle" alertcode="3"><s:text name='alerts.transactionAlertForm.portlettitle'/></span>
		<span id="unreadBankMessagePortletTitle" alertcode="6"><s:text name='alerts.unreadBankMessageAlertForm.portlettitle'/></span>		
		<span id="bankEnquiryPortletTitle" alertcode="12"><s:text name='alerts.bankEnquiryResponse.portlettitle'/></span>		
		<span id="failedAttemptPortletTitle" alertcode="13"><s:text name='alerts.failedAttemptLockOut.portlettitle'/></span>
		<span id="passwordChangePortletTitle" alertcode="11"><s:text name='alerts.passwordChangeAlertForm.portlettitle'/></span>
		<span id="stockPortfolioPortletTitle" alertcode="5"><s:text name='alerts.stockPortfolioAlertForm.portlettitle'/></span>
		<span id="invalidAccountAccessPortletTitle" alertcode="14"><s:text name='alerts.invalidAccountAccess.portlettitle'/></span>		
	</div>
	<center>
		<div id="alertHeader">
			<div id="accBalanceThresholdHeader" alertcode="1"><s:text name='alerts.accountBalanceThresholdAlertForm.header'/></div>
			<div id="accountBalanceSummaryHeader" alertcode="2"><s:text name='alerts.accountBalanceSummaryAlertForm.header'/></div>
			<div id="insufficentFundsHeader" alertcode="4"><s:text name='alerts.insufficientFundsAlertForm.header'/></div>
			<div id="transactionHeader" alertcode="3"><s:text name='alerts.transactionAlertForm.header'/></div>
			<div id="unreadBankMessageHeader" alertcode="6"><s:text name='alerts.unreadBankMessageAlertForm.header'/></div>		
			<div id="bankEnquiryHeader" alertcode="12"><s:text name='alerts.bankEnquiryResponseAlertForm.header'/></div>		
			<div id="failedAttemptHeader" alertcode="13"><s:text name='alerts.failedAttemptLockOutAlertForm.header'/></div>
			<div id="passwordChangeHeader" alertcode="11"><s:text name='alerts.passwordChangeAlertForm.header'/></div>
			<div id="stockPortfolioHeader" alertcode="5"><s:text name='alerts.stockPortfolioAlertForm.header'/></div>
			<div id="invalidAccountAccessHeader" alertcode="14"><s:text name='alerts.invalidAccountAccessAlertForm.header'/></div>
		</div>
	</center>
	
	<br>
	<center>
		<div id="alertFormElemnts">
			<div id="accountSelectBox">
				<s:text name='jsp.default_15'/><select name="alertsAccountSelectBox" id="alertsAccountSelectBox"></select>
			</div>
			
				<div id="minMaxAmountBox" alertcode="1">
					<table>
						<tr><td><s:text name='alerts.accountBalanceThresholdAlertForm.minAmount'/></td><td><input class="ui-widget-content ui-corner-all" name="minAmount" id="minAmount"></td></tr>
						<tr><td><s:text name='alerts.accountBalanceThresholdAlertForm.maxAmount'/></td><td><input class="ui-widget-content ui-corner-all" name="maxAmount" id="maxAmount"></td></tr>
					</table>
				</div>	
				<div id="trasactionAlertBox"  alertcode="3">	
					<table cellpadding="3">
						<tr>
							<td><s:text name='jsp.default_439'/></td>
							<td><select name="transactionTypeSelectBox" id="transactionTypeSelectBox"></select></td>
							
						</tr>
						<tr>
							<td><s:text name='jsp.default_43'/></td>
							<td><select name="transactionAmountSelectBox" id="transactionAmountSelectBox"></select><input class="ui-widget-content ui-corner-all" name="transactionAmount" id="transactionAmount"></td>
						</tr>
					</table>	
				</div>	
				<div id="stockBox" alertcode="5">
					<table>
						<tr><td colspan="6"><sj:a id="alertsEditPortPolio" button="true"><s:text name='alerts.stockPortfolioAlertForm.editPortfolio'/></sj:a></td></tr>
					</table>
					<hr>
					<table>						
						<tr><td colspan="6"><s:text name="alerts.stockPortfolioAlertForm.otherStocks"></s:text></td></tr>
						<tr>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
						</tr>
						<tr>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
							<td><input class="ui-widget-content ui-corner-all" name="otherStocks" ></td>
						</tr>						
					</table>
				</div>		           
		</div>
	</center>	
		
	<br>
	<center>
		<div id="alertFrequencey" >
			<table>
				<tr><td align="center">
					<div id="accountBalanceThresholdFrequency" alertcode="1"><s:text name='alerts.accountBalanceThresholdAlertForm.frequency'/></div>
					<div id="insufficentFundsFrequency" alertcode="4"><s:text name='alerts.insufficientFundsAlertForm.frequency'/></div>
					<div id="transactionFrequency" alertcode="3"><s:text name='alerts.transactionAlertForm.frequency'/></div>
				</td></tr>
				<tr><td align="center">
					<input type="radio" checked="" value="false" name="alertFrequency" id="radioEveryTime"><span><b><s:text name='alerts.frequency.everytime'/></b></span>
				</td></tr>
				<tr><td align="center">
					<input type="radio" value="true" name="alertFrequency" id="radioFirstTime"><span><b><s:text name='alerts.frequency.firsttime'/></b></span>
				</td></tr>
			</table>
		</div>
	</center>
	
	<br><hr>	
	<center>
		<div id="alertDeliveryOptions">
			<s:include value="/pages/jsp/alert/inc/delivery_options_grid.jsp"/>
		</div>
	</center>
	
	<br><hr>
	<center>
	<div id="alertFooter">
		<div id="accBalanceThresholdFooter" alertcode="1"><s:text name='alerts.accountBalanceThresholdAlertForm.footer'/></div>
		<div id="accountBalanceSummaryFooter" alertcode="2"><s:text name='alerts.accountBalanceSummaryAlertForm.footer'/></div>
		<div id="insufficentFundsFooter" alertcode="4"><s:text name='alerts.insufficientFundsAlertForm.footer'/></div>
		<div id="transactionFooter" alertcode="3"><s:text name='alerts.transactionAlertForm.footer'/></div>
		<div id="unreadBankMessageFooter" alertcode="6"><s:text name='alerts.unreadBankMessageAlertForm.footer'/></div>		
		<div id="bankEnquiryFooter" alertcode="12"></div>		
		<div id="failedAttemptFooter" alertcode="13"></div>
		<div id="passwordChangeFooter" alertcode="11"></div>
		<div id="stockPortfolioFooter" alertcode="5"><s:text name='alerts.stockPortfolioAlertForm.footer'/></div>
		<div id="invalidAccountAccessFooter" alertcode="14"></div>
	</div>	
	</center>
	
	<br>
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<center>
		  <table>
		  	<tr>
		  		<td><sj:a id="cancelAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a></td>
		  		<td>
		  			<sj:a id="addAlert" button="true" alertbuttontype="add"><s:text name="alerts.addNewAlert"/></sj:a>
		  			<sj:a id="updateAlert" button="true" alertbuttontype="save"><s:text name="jsp.default_366"/></sj:a>
		  		</td>
		  	</tr>
		  </table>
	  </center>
</s:form>	  
</div>