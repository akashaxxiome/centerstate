<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>


 <ffi:setProperty name="AlertsFilter" value="TypeValue=8"/>
<div id="alertTabs" class="portlet" style="float: left; width: 100%;">
	    <div class="portlet-header">
	    	<span class="portlet-title" title="Received Alerts"><s:text name="jsp.alert_51" /></span>
	    </div>
	    <div class="portlet-content">
					<div id="gridContainer" class="summaryGridHolderDivCls">
						<div id="AlertInbox" gridId="inboxAlertsGridId" class="gridSummaryContainerCls" >
							<s:include value="/pages/jsp/alert/alert_inbox.jsp" />
						</div>
					</div>
			</div>
	    	<div id="alertsTabsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       		<ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('alertTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	      		</ul>
			</div>
</div>


<script>
//Initialize portlet with settings & calendar icon
ns.common.initializePortlet("alertTabs");
/* $(document).ready(function() {
alert("111")
	ns.alert.loadAlertInboxGrid();
});
 */
</script>