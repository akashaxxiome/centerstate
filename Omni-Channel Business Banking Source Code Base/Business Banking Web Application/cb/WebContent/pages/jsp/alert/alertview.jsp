<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="alerts_alertview" />
<div id="subjectInViewAlert" style="display:none"><ffi:getProperty name="message" property="Subject" /></div>
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="greenClayBackground tableData">
<tr>
	<td>
		<span><s:text name="jsp.received.on"/>:</span>
		<span><ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateFormat}" /><ffi:getProperty name="Message" property="DateValue" /> <ffi:setProperty name="Message" property="DateFormat" value="H:mm" /><ffi:getProperty name="Message" property="DateValue" /></span>
	</td>
</tr>
<tr><td></td></tr>
<tr>
	<td>
		<span><s:text name="jsp.default_516"/></span>
		<span>
			<ffi:list collection="message.MemoLines" items="line">                                                
			  <ffi:getProperty name="line" encodeLeadingSpaces="true"/><br>
			</ffi:list> 
		</span>
	</td>
</tr>
<%-- <tr>
		<td colspan="3" nowrap width="565"><span class="sectionsubhead"><s:text name="jsp.alert_25"/></span><span class="columndata"><ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateFormat}" /><ffi:getProperty name="Message" property="DateValue" /></span></td>
</tr>
<tr>
	<td colspan="3" nowrap width="565"><span class="sectionsubhead"><s:text name="jsp.alert_63"/></span><span class="columndata"><ffi:setProperty name="Message" property="DateFormat" value="H:mm" /><ffi:getProperty name="Message" property="DateValue" /></span></td>
</tr>
<tr>
	<td colspan="3" nowrap></td>
</tr>
<tr>
	<td class="tbrd_t" colspan="3" nowrap width="565"><span class="columndata">
		<ffi:list collection="message.MemoLines" items="line">                                                
		  <ffi:getProperty name="line" encodeLeadingSpaces="true"/><br>
		</ffi:list> 								
	</span></td>
</tr>
<tr>
	<td colspan="3" nowrap></td>
</tr> --%>
<tr><td></td></tr>
<tr>
	<td align="center">							
		<%-- 
		<input class="submitbutton" type="button" value="DELETE" onClick="location.replace('<ffi:getProperty name="SecureServletPath"/>DeleteMessage')">&nbsp;
		<input class="submitbutton" type="button" value="DONE" onClick="location.replace('<ffi:getProperty name="SecurePath"/>alerts.jsp')">
		--%>
		<sj:a   
				id="closeViewAlertContainerButton"
				href="#"
				button="true" 
				invokerlinkid=""
				onClickTopics="cancelViewAlert"
        ><s:text name="jsp.default_83"/></sj:a>

	    <s:url id="deleteAlertUrl" value="/pages/jsp/alert/deleteInboxAlertsAction_confirm.action" escapeAmp="false">
		    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
		    <s:param name="selectedMsgIDs" value="%{#attr.message.ID}"></s:param>		
		</s:url>
           <sj:a
			id="deleteAlertConfirmSubmit"
			href="%{deleteAlertUrl}"
                        targets="DeleteAlertsConfirmDialogID" 
                        button="true" 
                        onBeforeTopics="beforeDeletingAlertConfirm"
                        onCompleteTopics="deletingAlertComplete"
			onErrorTopics="errorDeletingAlertConfirm"
                        onSuccessTopics="successDeletingAlertConfirm"
                    ><s:text name="jsp.default_163"/></sj:a>

		</td>
	</tr>
</table>
