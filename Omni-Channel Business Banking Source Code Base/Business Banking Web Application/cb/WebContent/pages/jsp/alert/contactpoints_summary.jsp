<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="tempURL" value="/pages/jsp/alert/getContactPoints.action?type=2" URLEncrypt="true"/>	

<s:url id="getContactPointsAction" value="%{#session.tempURL}" escapeAmp="false"/>

<div id="contactPointsPortlet" class="portlet" style="float: left; width: 100%;">
<span class="shortcutPathClass" style="display:none;">home_alert,contactPointsLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>	

	    <div class="portlet-header">
	    	<span class="portlet-title" title="Received Alerts"><s:text name='contactPointsGrid.title' /></span>
	    </div>
<s:form id="saveCPForm" action="modifyContactPoints" namespace="/pages/jsp/alert" method="post" name="saveCPForm" theme="simple">
	    <div class="portlet-content">
	   		 <ffi:help id="contact_point_summary" className="moduleHelpClass"/>
					<div id="gridContainer" class="summaryGridHolderDivCls">
						<sjg:grid  
							id="contactPointsGrid"    
							sortable="true" 
							dataType="json"  
							href="%{getContactPointsAction}"  
							pager="true" 
							gridModel="gridModel" 
							navigator="true"
						    navigatorAdd="false"
						    navigatorDelete="false"
						    navigatorEdit="false"
						    navigatorRefresh="false"
						    navigatorSearch="false"
							shrinkToFit="true"
							onGridCompleteTopics="addGridControlsEvents,ns.contactpoints.gridOnComplete">
							<sjg:gridColumn name="contactPointID" hidedlg="true" index="contactPointID" title="contactPointID" hidden="true" editable="true" edittype="custom" editoptions="{custom_element: ns.contactpoints.addCustomHiddenBox, custom_value:ns.contactpoints.setCustomHiddenValue}" />
							<sjg:gridColumn align="center" width="170" name="name" index="name" title="%{getText('contactPointsGrid.coulmn.name')}" sortable="false" formatter="ns.contactpoints.nameColumnFormatter" editable="true" edittype="text" editoptions="{size:45}"/> 				
							<sjg:gridColumn align="center" width="80" name="contactPointType" index="contactPointType" title="%{getText('contactPointsGrid.coulmn.type')}" sortable="false"  formatter="ns.contactpoints.typeColumnFormatter" editable="true" edittype="select" editoptions="{ value: '-1:%{getText('contactPoints.addNewForm.select.option1')};2:%{getText('contactPoints.addNewForm.select.option2')}'}"/> 				
							<sjg:gridColumn align="center" width="170" name="address" index="address" title="%{getText('contactPointsGrid.coulmn.address')}" sortable="false"  editable="true" edittype="text" editoptions="{size:45}"/>
							<sjg:gridColumn name="deviceID" hidedlg="true" index="deviceID" title="deviceID" hidden="true" editable="true" edittype="custom" editoptions="{custom_element: ns.contactpoints.addCustomHiddenBox, custom_value:ns.contactpoints.setCustomHiddenValue}"/>
							<sjg:gridColumn name="secure" hidedlg="true" index="secure" title="secure"  hidden="true" editable="true" edittype="custom" editoptions="{custom_element: ns.contactpoints.addCustomHiddenBox, custom_value:ns.contactpoints.setCustomHiddenValue}"/>
							<sjg:gridColumn name="action" width="60" title="%{getText('contactPointsGrid.coulmn.action')}" search="false" hidden="true" sortable="false"  formatter="ns.contactpoints.actionColumnFormatter" hidedlg="true" cssClass="__gridActionColumn" />
						</sjg:grid>
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>	
			            <input type="hidden" name="method" value="update"/>
			            <input type="hidden" name="records" id="records" value=""/>
					</div>
			</div>
		
	    	<div id="alertsTabsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       		<ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('contactPointsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	      		</ul>
			</div>

<div class="btn-row">	    
		<sj:a id="saveCPBtn" formIds="saveCPForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="removeErrorsOnBegin"  onSuccessTopics="successUpdatingCP">
			<s:text name="contactPoints.addNewForm.button.save"/>
		</sj:a>				
</div>
</s:form>
</div>

	<div id="targetDiv">
	</div>
<script>
//Initialize portlet with settings & calendar icon
ns.common.initializePortlet("contactPointsPortlet");
$(document).ready(function () {	
/* $('#contactPointsPortlet').portlet({
	bookmark: true,
	bookMarkCallback: function(){
		var path = $('#contactPointsPortlet').find('.shortcutPathClass').html();
		var ent  = $('#contactPointsPortlet').find('.shortcutEntitlementClass').html();
		ns.shortcut.openShortcutWindow( path, ent );
	},
	title: js.contactpoints.CPPortlet.title,
	generateDOM: true,
	helpCallback: function(){
		var helpFile = $('#summary').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	}
}); */
	
	//$.publish("useralerts.closeAddNewCPForm");
});
</script>
