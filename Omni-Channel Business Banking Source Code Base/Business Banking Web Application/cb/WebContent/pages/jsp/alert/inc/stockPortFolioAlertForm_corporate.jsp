<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertStockPortfolioForm" class="portlet">
	<ffi:help id="alerts_stock_portfolio"  className="moduleHelpClass"/>
	<div id="alertStockPortfolioTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="stockPortfolioAccessPortletTitle" alertcode="7"><s:text name='alerts.stockPortfolioAlertForm.portlettitle'/></span>	
	</div>
		
	<div id="alertStockPortfolioFormElemnts" class="portlet-content" align="center">
		<div id="alertStockPortfolioHeader">
			<div id="stockPortfolioAccessHeader" alertcode="7" class="instructions"></div>
		</div>
		<s:form id="stockPortfolioAlertForm" action="AddEditStockAlertAction" namespace="/pages/jsp/alert" method="post" name="StockPortfolioAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			
			<table width="100%" class="tableData">	
				<tr>		
					<td width="50%">Stock Symbol</td>
					<td width="50%"><s:text name='alert.notification.frequency.label'/></td>
				</tr>
				<tr>
					<td>
						<input class="ui-widget-content ui-corner-all txtbox" type="text" id="stockSymbol" name="stockSymbol" size="9">
						<select id="stockCriteria" name="stockCriteria" onchange='ns.useralerts.amountBoxDisabler();'>
						<option value="1"><s:text name="jsp.alert_15"/></option>
						<option value="3"><s:text name="jsp.alert_41"/></option>
						<option value="2"><s:text name="jsp.alert_37"/></option>
						</select>
						<input class="ui-widget-content ui-corner-all txtbox hidden" type="text" id="stockAmount" name="stockAmount" size="15">
					</td>
					<td>
						<input type="radio" checked="" value="false" name="frequency" id="radioEveryTime"> <s:text name='alerts.frequency.everytime'/>
						<input type="radio" value="true" name="frequency" id="radioFirstTime"> <s:text name='alerts.frequency.firsttime'/>
					</td>
						
				</tr>
				<tr>
					<td colspan="2"><span id="stockSymbolError" class="errorLabel"></span> <span id="stockAmountError" class="errorLabel"></span></td>
				</tr>
				<%-- <tr>
					<td align="right" nowrap><span class="sectionsubhead"><s:text name="jsp.alert_54"/></span></td>
					<td>
						<input class="ui-widget-content ui-corner-all txtbox" type="text" id="stockSymbol" name="stockSymbol" size="9">
						<br><span id="stockSymbolError" class="errorLabel"></span>
					</td>
					<td>
						<span class="sectionsubhead"><s:text name="jsp.alert_36"/></span>
						<select id="stockCriteria" name="stockCriteria" onchange='ns.useralerts.amountBoxDisabler();'>
						<option value="1"><s:text name="jsp.alert_15"/></option>
						<option value="3"><s:text name="jsp.alert_41"/></option>
						<option value="2"><s:text name="jsp.alert_37"/></option>
						</select>
						<input class="ui-widget-content ui-corner-all txtbox hidden" type="text" id="stockAmount" name="stockAmount" size="15">
					</td>
					<td>
						<br><span id="stockAmountError" class="errorLabel"></span>								
					</td>		
				</tr> --%>
								
			</table>
				<%-- <div alertcontainer="alertFrequencey" >
					<table>
						<tr><td align="center">
							<div id="stockPortFolioFrequency" alertcode="7" class="instructions"><s:text name='alerts.stockPortfolioAlertForm.frequency'/></div>
						</td></tr>
						<tr><td align="center">
							<input type="radio" checked="" value="false" name="frequency" id="radioEveryTime"><span><b><s:text name='alerts.frequency.everytime'/></b></span>
						</td></tr>
						<tr><td align="center">
							<input type="radio" value="true" name="frequency" id="radioFirstTime"><span><b><s:text name='alerts.frequency.firsttime'/></b></span>
						</td></tr>
					</table>
				</div> --%>
			<div alertcontainer="delOptionsGrid" class="marginTop10" align="center" alertcode="7"></div>
			<div class="btn-row">
		  		<sj:a id="cancelStockPortfolioAlert" button="true" alertbuttontype="cancel" alertcode="7"><s:text name="jsp.default_82"/></sj:a>
	  			<sj:a id="addEditStockPortfolioAlert" alertbuttontype="addNew" formIds="stockPortfolioAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
	  			<sj:a id="updateStockPortfolioAlert" button="true" alertbuttontype="save" formIds="stockPortfolioAlertForm"	targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
		  	</div>
		</s:form>	
	</div>
	<div id="alertStockPortfolioFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('alertStockPortfolioForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
	
	<div>		
		<div id="stockPortfolioFooter" alertcode="7"><s:text name='alerts.stockPortfolioAlertForm.footer'/></div>	
	</div>	
</div>
<script>
//Initialize portlet with settings icon
ns.common.initializePortlet("alertStockPortfolioForm"); 
 $(document).ready(function () {
	 $('#stockCriteria').selectmenu({
		"width":100,
		"change":function(){
			$.publish("removeErrorsOnBegin");
		}
	});
 });
</script>					