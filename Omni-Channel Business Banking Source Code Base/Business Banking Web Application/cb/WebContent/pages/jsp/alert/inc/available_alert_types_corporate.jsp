<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div>
	<table class="tableData availAlertTypeTable tableAlerternateRowColor">
		<tbody>
			<tr>
				<td>
					<span class="sectionsubhead"><s:text name="alerts.unreadBankMessage.header"/></span>
					<span class="columndata"><s:text name="alerts.unreadBankMessage.description"/></span>
				</td>
				<td>
					<sj:a id="unreadBankMessageAddAlertLink" button="true" alertcode="6"><s:text name="jsp.add.label"/></sj:a>
				</td>				
				<td>
					<span class="sectionsubhead"><s:text name="alerts.bankEnquiryResponse.header"/></span>
					<span class="columndata"><s:text name="alerts.bankEnquiryResponse.description"/></span>
				</td>
				<td>
					<sj:a id="bankEnquiryAddAlertLink" button="true" alertcode="12"><s:text name="jsp.add.label"/></sj:a>
				</td>
			</tr>
			<tr>
				<td>
					<span class="sectionsubhead"><s:text name="alerts.passwordChange.header"/></span>
					<span class="columndata"><s:text name="alerts.passwordChange.description"/></span>
				</td>
				<td>
					<sj:a id="passChangeAddAlertLink" button="true" alertcode="11"><s:text name="jsp.add.label"/></sj:a>
				</td>
				<td>
					<span class="sectionsubhead"><s:text name="alerts.failedAttemptLockOut.header"/></span>
					<span class="columndata"><s:text name="alerts.failedAttemptLockOut.description"/></span>
				</td>
				<td>
					<sj:a id="failedAttemptLockoutAddAlertLink" button="true" alertcode="13"><s:text name="jsp.add.label"/></sj:a>
				</td>
			</tr>
			<tr>
				<td>
					<span class="sectionsubhead"><s:text name="alerts.invalidAccountAccess.header"/></span>
					<span class="columndata"><s:text name="alerts.invalidAccountAccess.description"/></span>
				</td>
				<td>
					<sj:a id="invalidAccountAccessAddAlertLink" button="true" alertcode="14"><s:text name="jsp.add.label"/></sj:a>
				</td>
				<td>
					<span class="sectionsubhead"><s:text name="alerts.stockPortfolio.header"/></span>
					<span class="columndata"><s:text name="alerts.stockPortfolio.description"/></span>
				</td>
				<td>
					<sj:a id="stockPortAddAlertLink" button="true" alertcode="7"><s:text name="jsp.add.label"/></sj:a>
				</td>
			</tr>
			<tr>
				<td>
					<span class="sectionsubhead"><s:text name="alerts.accountBalanceThreshold.header"/></span>
					<span class="columndata"><s:text name="alerts.accountBalanceThreshold.description"/></span>
				</td>
				<td>
					<sj:a id="accBalanceAddAlertLink" button="true" alertcode="17"><s:text name="jsp.add.label"/></sj:a>
				</td>
				<td>
					<span class="sectionsubhead"><s:text name="alerts.insufficientFund.header"/></span>
					<span class="columndata"><s:text name="alerts.insufficientFund.description"/></span>
				</td>
				<td>
					<sj:a id="insufficientFundsAddAlertLink" button="true" alertcode="16"><s:text name="jsp.add.label"/></sj:a>
				</td>								
			</tr>
			<tr>
				<td>
					<span class="sectionsubhead"><s:text name="alerts.transactionAlert.header"/></span>
					<span class="columndata"><s:text name="alerts.transactionAlert.description"/></span>
				</td>
				<td>
					<sj:a id="transactionAddAlertLink" button="true" alertcode="18"><s:text name="jsp.add.label"/></sj:a>
				</td>
				<td>
					<span class="sectionsubhead"><s:text name="alerts.panicPay.header"/></span>
					<span class="columndata"><s:text name="alerts.panicPay.description"/></span>
				</td>
				<td>
					<sj:a id="panicPayAddAlertLink" button="true" alertcode="19"><s:text name="jsp.add.label"/></sj:a>
				</td>
			</tr>
			<tr>
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_ENTITLEMENT %>" >
					<td>
						<span class="sectionsubhead"><s:text name="alerts.positivePayAlert.header"/></span>
						<span class="columndata"><s:text name="alerts.positivePayAlert.description"/></span>
					</td>
					<td>
						<sj:a id="positivePayAddAlertLink" button="true" alertcode="15"><s:text name="jsp.add.label"/></sj:a>
					</td>
				</ffi:cinclude>
				<ffi:cinclude value1="${IsApprover}" value2="true" operator="equals">
					<td>
						<span class="sectionsubhead"><s:text name="alerts.paymentApprovalsAlert.header"/></span>
						<span class="columndata"><s:text name="alerts.paymentApprovalsAlert.description"/></span>
					</td>
					<td>
						<sj:a id="paymentApprovalsAddAlertLink" button="true" alertcode="10"><s:text name="jsp.add.label"/></sj:a>
					</td>
				</ffi:cinclude>
			</tr>
			
			
			<tr>
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
					<td>
						<span class="sectionsubhead"><s:text name="alerts.reversePositivePayAlert.header"/></span>
						<span class="columndata"><s:text name="alerts.reversePositivePayAlert.description"/></span>
					</td>
					<td>
						<sj:a id="reversePositivePayAddAlertLink" button="true" alertcode="21"><s:text name="jsp.add.label"/></sj:a>
					</td>
				</ffi:cinclude>
				
					<td>
						<span class="sectionsubhead"></span>
						<span class="columndata"></span>
					</td>
					<td>
						
					</td>
				
			</tr>
		</tbody>
	</table>
</div>