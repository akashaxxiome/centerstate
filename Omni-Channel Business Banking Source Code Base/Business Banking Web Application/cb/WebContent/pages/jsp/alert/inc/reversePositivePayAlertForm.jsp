<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertReversePositivePayForm" class="portlet">
	<ffi:help id="alerts_positive_pay"  className="moduleHelpClass"/>
	<div id="alertReversePositivePayTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="reversePositivePayTitle" alertcode="21"><s:text name='alerts.reversePositivePayAlertForm.portlettitle'/></span>	
	</div>
	<div id="alertReversePositivePayFormElemnts" class="portlet-content">
		<div id="alertReversePositivePayHeader">
			<div id="reversePositivePayHeader" alertcode="21" class="instructions"><s:text name='alerts.reversePositivePayAlertFrom.header'/></div>
		</div>
		<s:form id="reversePositivePayAlertForm" action="AddEditReversePositivePayAlertAction" namespace="/pages/jsp/alert" method="post" name="reversePositivePayAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			<div alertcontainer="delOptionsGrid" alertcode="21" align="center"></div>
			<div class="btn-row">
			  		<sj:a id="cancelReversePositivePayAlertAlert" button="true" alertbuttontype="cancel" alertcode="21"><s:text name="jsp.default_82"/></sj:a>
		  			<sj:a id="addEditReversePositivePayAlert" alertbuttontype="addNew" formIds="reversePositivePayAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
		  			<sj:a id="updateReversePositivePayAlert" button="true" alertbuttontype="save" formIds="reversePositivePayAlertForm"	targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
		  	</div>
		</s:form>	
	</div>
	<div id="alertReversePositivePayFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
      <ul class="portletHeaderMenu" style="background:#fff">
             <li><a href='#' onclick="ns.common.showDetailsHelp('alertReversePositivePayForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      </ul>
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertReversePositivePayForm"); 
</script> 