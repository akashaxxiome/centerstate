<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertFailedAttemptLockoutForm" class="portlet">
	<ffi:help id="alerts_failed_attempt_lockout"  className="moduleHelpClass"/>
	<div id="alertFailedAttemptLockoutTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="FailedAttemptLockoutAccessPortletTitle" alertcode="13"><s:text name='alerts.failedAttemptLockOut.portlettitle'/></span>	</div>
	<div id="alertFailedAttemptLockoutFormElemnts" class="portlet-content">
		<div id="alertFailedAttemptLockoutHeader">
			<div id="FailedAttemptLockoutAccessHeader" alertcode="13" class="instructions"><s:text name='alerts.failedAttemptLockOutAlertForm.header'/></div>
		</div>
		<s:form id="failedAttemptLockoutAlertForm" action="AddEditFailedAttemptLockoutAlertAction" namespace="/pages/jsp/alert" method="post" name="failedAttemptLockoutAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			
			<div alertcontainer="delOptionsGrid" alertcode="13" align="center"></div>				
		  	<div class="btn-row">
		  		<sj:a id="cancelFailedAttemptLockoutAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a>
	  			<sj:a id="addEditFailedAttemptLockoutAlert" alertbuttontype="addNew" formIds="failedAttemptLockoutAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
	  			<sj:a id="updateFailedAttemptLockoutAlert" button="true" alertbuttontype="save" formIds="failedAttemptLockoutAlertForm"	targets="targetDiv" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
	  		</div>
	  
		</s:form>	
	</div>
	<div id="alertFailedAttemptLockoutFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
      <ul class="portletHeaderMenu" style="background:#fff">
             <li><a href='#' onclick="ns.common.showDetailsHelp('alertFailedAttemptLockoutForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      </ul>
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertFailedAttemptLockoutForm"); 
</script> 