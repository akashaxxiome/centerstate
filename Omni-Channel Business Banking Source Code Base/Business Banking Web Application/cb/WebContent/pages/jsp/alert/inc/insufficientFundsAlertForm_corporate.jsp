<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertInsufficientFundsForm" alertcode="16" class="portlet">
	<ffi:help id="alerts_insufficient_funds" className="moduleHelpClass"/>
	<div id="alertInsufficientFundsTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="InsufficientFundsAccessPortletTitle" alertcode="16">
		<s:text name='alerts.insufficientFundsAlertForm.portlettitle'/></span>	</div>
	<div id="alertInsufficientFundsFormElemnts"  class="portlet-content">		
		<s:form id="insufficientFundsAlertForm" action="AddEditInsufficientFundsAlertAction" namespace="/pages/jsp/alert" method="post" name="InsufficientFundsAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			
			<div alertcontainer="delOptionsGrid" alertcode="16" align="center"></div>			
			
			
		<div class="btn-row">
		  	<sj:a id="cancelInsufficientFundsAlert" button="true" alertbuttontype="cancel" alertcode="16"><s:text name="jsp.default_82"/></sj:a></td>
  			<sj:a id="addEditInsufficientFundsAlert" alertbuttontype="addNew" formIds="insufficientFundsAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
  			<sj:a id="updateInsufficientFundsAlert" button="true" alertbuttontype="save" formIds="insufficientFundsAlertForm"	targets="targetDiv"  validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
		 </div>
	  
		</s:form>	
	</div>
	<div id="alertInsufficientFundsFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('alertInsufficientFundsForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
	<div id="InsufficientFundsFooter">
		<span messageFor="footer"><s:text name='alerts.insufficientFundsAlertForm.footer'/></span><br>
		<span messageFor="inactiveAccount"><s:text name='alerts.inactiveAccounts'/></span>			
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertInsufficientFundsForm"); 
</script> 