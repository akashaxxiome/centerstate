<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<sj:dialog id="deleteAlertDialog" title="%{getText('alert.deleteAlertDialog.form.title')}" autoOpen="false" modal="true" cssClass="staticContentDialog" resizable="false">
	<div align="center">
			<span id="spanDelMsg" class="hidden"><s:text name="alert.deleteAlertDialog.form.message"></s:text></span>
			<div id="deleteAlertMessage"></div>
	</div>
	<div align="center" class="ui-widget-header customDialogFooter">
			<sj:a 	id="cancelDeleteAlertBtn" href="#"	button="true">&nbsp;<s:text name='jsp.default_82'/></sj:a>	    
			<sj:a 	id="deleteAlertBtn"	href="#" button="true">&nbsp;<s:text name='jsp.default_162'/></sj:a>	    	    
	</div>		                        	    
</sj:dialog>