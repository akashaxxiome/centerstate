<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<sj:dialog id="deleteCPDialog" title="%{getText('contactPoints.deleteDialog.form.title')}" autoOpen="false" cssClass="staticContentDialog" modal="true">

	    <div>
	    <span class="marginTop10 ffiVisible"><s:text name='contactPoints.deleteDialog.message'/></span>
	    <div align="center" class="ui-widget-header customDialogFooter">
	    	<sj:a 	id="cancelDeleteCPBtn"	button="true">&nbsp;<s:text name='jsp.default_82'/></sj:a>	    
			<sj:a 	id="deleteCPBtn"	button="true">&nbsp;<s:text name='jsp.default_162'/></sj:a>	    	    
	    </div>
	    </div>		                        	    
</sj:dialog>
