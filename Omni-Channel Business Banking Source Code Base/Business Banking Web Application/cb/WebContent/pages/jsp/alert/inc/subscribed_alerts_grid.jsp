<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<div id="alertsGridContainer">
<ffi:help id="current_subscribed_alerts"/>
	<ffi:setGridURL grid="GRID_alertsubScribe" name="DeleteURL" url="/pages/jsp/alert/deleteAlertAction.action?alertId={0}" parm0="alertId"/>
	<ffi:setProperty name="tempURL" value="/pages/jsp/alert/getAlertsAction.action?GridURLs=GRID_alertsubScribe" URLEncrypt="true"/>
	<s:url id="getAlertsUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="alertsGrid"  
		sortable="false"  
		dataType="json"  
		href="%{getAlertsUrl}"  		
		gridModel="gridModel"
		rownumbers="false"
		rowNum="-1"
		viewrecords="true"
		pager="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		onGridCompleteTopics="addGridControlsEvents,useralerts.topics.alertsGridOnComplete"
		> 
		<sjg:gridColumn name="userAlertId" index="userAlertId"	title="%{getText('jsp.default_163')}" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="alertType" index="alertType"	title="%{getText('jsp.default_163')}" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="alertDisplayText" index="alertDisplayText" sortable="false"	title="%{getText('alert.alertsgrid.column.alerts')}"  width="300" hidedlg="false"/>
		<sjg:gridColumn name="primaryContactDisplayText" index="primaryContactDisplayText"	title="%{getText('alerts.deliveryoptionsgrid.column.primary.name')} %{getText('contactPointsGrid.title')}"/>
		<sjg:gridColumn name="secondaryContactDisplayText" index="secondaryContactDisplayText"	title="%{getText('alerts.deliveryoptionsgrid.column.backup.name')} %{getText('contactPointsGrid.title')}"/>
		<sjg:gridColumn name="action" title="%{getText('jsp.default_27')}" sortable="false"  hidden="true" hidedlg="true" cssClass="__gridActionColumn" formatter="ns.useralerts.actionColumnFormatter" width="70"/>
	</sjg:grid>
</div>
