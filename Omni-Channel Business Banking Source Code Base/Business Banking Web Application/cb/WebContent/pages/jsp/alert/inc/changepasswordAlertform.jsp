<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertChangePasswordForm" class="portlet">
	<ffi:help id="alerts_pswd_change"  className="moduleHelpClass"/>
	<div id="alertChangePasswordTitle" class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title" id="passwordChangePortletTitle" alertcode="11"><s:text name='alerts.passwordChangeAlertForm.portlettitle'/></span>	
	</div>
	<div id="alertChangePasswordFormElemnts" class="portlet-content" align="center">
		<div id="alertChangePasswordHeader" align="left">
			<div id="passwordChangeHeader" alertcode="11" class="instructions"><s:text name='alerts.passwordChangeAlertForm.header'/></div>
		</div>
		<s:form id="changePasswordAlertForm" action="AddEditChangePasswordAlertAction" namespace="/pages/jsp/alert" method="post" name="changePasswordAlertForm" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="operation" value=""/>
			<input type="hidden" name="alertId" value=""/>
			<input type="hidden" name="alertType" value=""/>
			<div alertcontainer="delOptionsGrid" alertcode="11">
			</div>
	  		<div class="btn-row">
	  			<sj:a id="cancelChangePasswordAlert" button="true" alertbuttontype="cancel" alertcode="2"><s:text name="jsp.default_82"/></sj:a>
	  			<sj:a id="addEditChangePasswordAlert" alertbuttontype="addNew" formIds="changePasswordAlertForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="alerts.addNewAlert"  /></sj:a>
	  			<sj:a id="updateChangePasswordAlert" button="true" alertbuttontype="save" formIds="changePasswordAlertForm"	targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddEditAlert" onCompleteTopics="onCompleteAddEditAlert" onSuccessTopics="onSuccessAddEditAlert" onClickTopics="ns.useralerts.addNewAlert"><s:text name="jsp.default_366"/></sj:a>
	  		</div>
		</s:form>	
	</div>
	<div id="alertChangePasswordFormFormDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('alertChangePasswordForm')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>
<script> 
//Initialize portlet with settings icon
	ns.common.initializePortlet("alertChangePasswordForm"); 
</script> 