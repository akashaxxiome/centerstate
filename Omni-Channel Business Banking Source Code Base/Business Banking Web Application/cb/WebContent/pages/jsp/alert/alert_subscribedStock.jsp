<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<ffi:setGridURL grid="GRID_alertsubScribedStock" name="DeleteURL" url="/cb/pages/jsp/alert/alert_unsub_confirm.jsp?AlertID={0}" parm0="Alert.ID"/>
	<ffi:setGridURL grid="GRID_alertsubScribedStock" name="EditURL" url="/cb/pages/jsp/alert/AlertCRUDAction.action?method=modify-active&SetAlert.ID={0}" parm0="Alert.ID"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/alert/GetStockAlertsAction.action?alertStocksSessionKey=AlertStocks&GridURLs=GRID_alertsubScribedStock" URLEncrypt="true"/>
    <s:url id="subscribedStockAlertsUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="subscribedStockAlertsGridId"  
		caption="%{getText('jsp.alert_93')}" 
		sortable="true"  
		dataType="json"  
		href="%{subscribedStockAlertsUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		reloadTopics="reloadSubscribedStockAlerts"
		onGridCompleteTopics="subscribedStockAlertsGridCompleteEvents"
		> 
		
		<sjg:gridColumn 
			name="symbol" 
			index="symbol" 
			title="%{getText('jsp.alert_57')}" 
			sortable="true" 
			width="90"
			cssClass="datagrid_textColumn"/>

		<sjg:gridColumn 
			name="map.CRITERIAD" 
			index="criteria" 
			title="%{getText('jsp.alert_24')}" 
			sortable="true" 
			width="105"
			cssClass="datagrid_textColumn"/>

		<sjg:gridColumn 
			name="map.AMOUNTD" 
			index="amount" 
			title="%{getText('jsp.default_43')}" 
			sortable="true" 
			width="90"
			cssClass="datagrid_amountColumn"/>

		<sjg:gridColumn 
			name="alert.ID" 
			index="alertID" 
			title="" 
			sortable="false"
			hidedlg="true" 
			search="false"
			formatter="ns.alert.formatStockAlertIcons" 
			width="15"
			cssClass="datagrid_actionIcons"/>

		<sjg:gridColumn 
			name="map.DeleteURL" 
			index="DeleteURL" 
			title="%{getText('jsp.default_167')}" 
			sortable="true" 
			width="60" 
			hidden="true" 
			hidedlg="true"
			cssClass="datagrid_actionIcons"/>
	
		<sjg:gridColumn 
			name="map.EditURL" 
			index="EditURL" 
			title="%{getText('jsp.default_181')}" 
			sortable="true" 
			width="60" 
			hidden="true" 
			hidedlg="true"
			cssClass="datagrid_actionIcons"/>
						
	</sjg:grid>
	

    <script type="text/javascript">

	    $(document).ready(function () {

	    	//jQuery("#list1").jqGrid('navGrid','#pager1',{edit:false,add:false,del:false,search:false,refresh:false}); 
	    	$("#subscribedStockAlertsGridId").jqGrid('navButtonAdd', "#subscribedStockAlertsGridId_pager", {
	    	    caption: js_search,
	    	    title: js_search,
	    	    buttonicon: 'ui-icon-search',
	    	    onClickButton: function(){
	    			$("#subscribedStockAlertsGridId")[0].toggleToolbar()
	    	    }
	    	});

	    	$("#subscribedStockAlertsGridId").jqGrid('navButtonAdd', "#subscribedStockAlertsGridId_pager", {
	    	    caption: js_clear_search,
	    	    title: js_clear_search,
	    	    buttonicon: 'ui-icon-refresh',
	    	    onClickButton: function(){
	    			$("#subscribedStockAlertsGridId")[0].clearToolbar()
	    	    }
	    	});

	    	$("#subscribedStockAlertsGridId").jqGrid('navButtonAdd', "#subscribedStockAlertsGridId_pager", {
	    	    caption: js_select_column,
	    	    title: js_select_column,
	    	    buttonicon: 'ui-icon-grip-dotted-vertical',
	    	    onClickButton: function(){
	    			$("#subscribedStockAlertsGridId").jqGrid('columnChooser');
	    	    }
	    	});
	    	
	    	$("#subscribedStockAlertsGridId").jqGrid('filterToolbar');
	    	$("#subscribedStockAlertsGridId")[0].toggleToolbar();

	    	if($("#subscribedStockAlertsGridId tbody").data("ui-jqgrid") != undefined){
	    		$("#subscribedStockAlertsGridId tbody").sortable("destroy");
	    	}

	    });

    </script>