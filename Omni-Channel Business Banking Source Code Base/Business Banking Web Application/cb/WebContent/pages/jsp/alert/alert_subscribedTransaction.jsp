<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<ffi:setGridURL grid="GRID_alertsubScribedTran" name="DeleteURL" url="/cb/pages/jsp/alert/alert_unsub_confirm.jsp?AlertID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_alertsubScribedTran" name="EditURL" url="/cb/pages/jsp/alert/AlertCRUDAction.action?method=modify-active&SetAlert.ID={0}" parm0="ID"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/alert/GetTransactionAlertsAction.action?GridURLs=GRID_alertsubScribedTran" URLEncrypt="true"/>
	<s:url id="subscribedTransAlertsUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="subscribedTransAlertsGridId"  
		caption="%{getText('jsp.alert_94')}" 
		sortable="true"  
		dataType="json"  
		href="%{subscribedTransAlertsUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		reloadTopics="reloadSubscribedTransAlerts"
		onGridCompleteTopics="subscribedTransAlertsGridCompleteEvents"
		> 
		
		<sjg:gridColumn 
			name="map.TransactionType" 
			index="transactiontype" 
			title="%{getText('jsp.default_444')}" 
			sortable="true" 
			width="60"
			cssClass="datagrid_textColumn"/>
			
		<sjg:gridColumn 
			name="map.alertname" 
			index="alertname" 
			title="%{getText('jsp.default_15')}" 
			sortable="true" 
			width="135"
			cssClass="datagrid_textColumn"/>

		<sjg:gridColumn 
			name="map.CRITERIA" 
			index="criteria" 
			title="%{getText('jsp.alert_24')}" 
			sortable="true" 
			width="45"
			cssClass="datagrid_textColumn"/>
		
		<sjg:gridColumn 
			name="map.AMOUNT" 
			index="amount" 
			title="%{getText('jsp.default_60')}" 
			sortable="true" 
			width="45"
			cssClass="datagrid_amountColumn"/>

		<sjg:gridColumn 
			name="map.Restricted" 
			index="restricted" 
			title="%{getText('jsp.alert_52')}" 
			hidden="true" 
			hidedlg="true" 
			sortable="false" 
			width="0"
			cssClass="datagrid_textColumn"/>

		<sjg:gridColumn 
			name="ID" 
			index="id" 
			title="" 
			hidedlg="true" 
			search="false"  
			sortable="false" 
			hidden="false" 
			formatter="ns.alert.formatTransactionAlertIcons" 
			width="15"
			cssClass="datagrid_actionIcons"/>

		<sjg:gridColumn 
			name="map.DeleteURL" 
			index="DeleteURL" 
			title="%{getText('jsp.default_167')}" 
			sortable="true" 
			width="60" 
			hidden="true" 
			hidedlg="true"
			cssClass="datagrid_actionIcons"/>
	
		<sjg:gridColumn 
			name="map.EditURL" 
			index="EditURL" 
			title="%{getText('jsp.default_181')}" 
			sortable="true" 
			width="60" 
			hidden="true" 
			hidedlg="true"
			cssClass="datagrid_actionIcons"/>
			
	</sjg:grid>
	

    <script type="text/javascript">

	    $(document).ready(function () {

	    	//jQuery("#list1").jqGrid('navGrid','#pager1',{edit:false,add:false,del:false,search:false,refresh:false}); 
	    	$("#subscribedTransAlertsGridId").jqGrid('navButtonAdd', "#subscribedTransAlertsGridId_pager", {
	    	    caption: js_search,
	    	    title: js_search,
	    	    buttonicon: 'ui-icon-search',
	    	    onClickButton: function(){
	    			$("#subscribedTransAlertsGridId")[0].toggleToolbar()
	    	    }
	    	});

	    	$("#subscribedTransAlertsGridId").jqGrid('navButtonAdd', "#subscribedTransAlertsGridId_pager", {
	    	    caption: js_clear_search,
	    	    title: js_clear_search,
	    	    buttonicon: 'ui-icon-refresh',
	    	    onClickButton: function(){
	    			$("#subscribedTransAlertsGridId")[0].clearToolbar()
	    	    }
	    	});

	    	$("#subscribedTransAlertsGridId").jqGrid('navButtonAdd', "#subscribedTransAlertsGridId_pager", {
	    	    caption: js_select_column,
	    	    title: js_select_column,
	    	    buttonicon: 'ui-icon-grip-dotted-vertical',
	    	    onClickButton: function(){
	    			$("#subscribedTransAlertsGridId").jqGrid('columnChooser');
	    	    }
	    	});
    	
	    	$("#subscribedTransAlertsGridId").jqGrid('filterToolbar');
	    	$("#subscribedTransAlertsGridId")[0].toggleToolbar();

	    	if($("#subscribedTransAlertsGridId tbody").data("ui-jqgrid") != undefined){
	    		$("#subscribedTransAlertsGridId tbody").sortable("destroy");
	    	}

	    });

    </script>