<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
  
  <div id="alertTabs" class="portlet" style="float: left; width: 100%;">
  <span class="shortcutPathClass" style="display:none;">home_alert,showAlertsLogsLink</span>
<span class="shortcutEntitlementClass" style="display:none;">Alerts</span>
<ffi:help id="alerts_alertlog"/>
	    <div class="portlet-header">
		    <div class="searchHeaderCls">
				<span class="searchPanelToggleAreaCls" onclick="$('#quicksearchcriteria').slideToggle();">
					<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
				</span>
				<div class="summaryGridTitleHolder">
					<span class="selectedTabLbl" title="Received Alerts"><s:text name="jsp.alert_11" /></span>
				</div>
			</div>
	    </div>
	     <div class="portlet-content">
	    		<div class="searchDashboardRenderCls">
	    			<s:include value="/pages/jsp/alert/alertLogsSerachCriteria.jsp" />	
	    		</div>
	    		<div id="gridContainer" class="summaryGridHolderDivCls">
						<div id="alertLogInbox" gridId="alertLogGridId" class="gridSummaryContainerCls" >
							<div align="center" id="alertLogDiv">
								<s:url id="alertLogsUrl" value="/pages/jsp/alert/getAlertLogsAction.action" escapeAmp="false">
									<s:param name="alertType" value="%{#request.alertType}"/>
									 <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>	
								</s:url>
								<sjg:grid  
									id="alertLogGridId"  
									dataType="json"  
									sortable="true"  
									href="%{alertLogsUrl}"  
									pager="true"  
									gridModel="gridModel" 
									rowList="%{#session.StdGridRowList}" 
									rowNum="%{#session.StdGridRowNum}" 
									rownumbers="false"
									shrinkToFit="true"
									navigator="true"
									navigatorAdd="false"
									navigatorDelete="false"
									navigatorEdit="false"
									navigatorRefresh="false"
									navigatorSearch="false"
									navigatorView="false"
									scroll="false"
									scrollrows="true"
									viewrecords="true"
									reloadTopics="reloadInboxAlerts"
									onGridCompleteTopics="addGridControlsEvents,inboxAlertsGridCompleteEvents"
									> 
									
									<sjg:gridColumn 
										name="date" 
										index="logdate" 
										title="%{getText('jsp.default_142')}" 
										sortable="true" 
										search="true"  
										width="75"
										cssClass="datagrid_dateColumn"/>
										
								
									<sjg:gridColumn 
										name="map.TypeText" 
										index="alerttype" 
										title="%{getText('jsp.alert_13')}" 
										hidden="false" 
										sortable="true" 
										search="true"  
										width="120"
										cssClass="datagrid_textColumn"/>
						
									<sjg:gridColumn 
										name="deliveryChannel" 
										index="channel" 
										title="%{getText('jsp.alert_27')}" 
										hidden="true" 
										hidedlg="true"
										sortable="true" 
										search="false"  
										width="0"
										cssClass="datagrid_textColumn"/>
						
									<sjg:gridColumn 
										name="map.ChannelText" 
										index="channel" 
										title="%{getText('jsp.alert_27')}" 
										hidden="false" 
										hidedlg="false"
										sortable="true" 
										search="false"  
										width="60"
										cssClass="datagrid_textColumn"/>
						
									<sjg:gridColumn 
										name="status" 
										index="status" 
										title="%{getText('jsp.alert_53')}" 
										hidden="true" 
										hidedlg="true" 
										sortable="true" 
										search="false"  
										width="0"
										cssClass="datagrid_textColumn"/>
						
									<sjg:gridColumn 
										name="map.StatusText" 
										index="status" 
										title="%{getText('jsp.alert_53')}" 
										hidden="false" 
										hidedlg="false" 
										sortable="true" 
										search="false"  
										width="45"
										cssClass="datagrid_textColumn"/>
						
								</sjg:grid>
							</div>
						</div>
				</div>
	    </div>
	    <div id="alertsTabsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       		<ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('alertTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      		</ul>
		</div>



	<%-- <ul>
		<li><a href="#alertLogDiv"><s:text name='jsp.alert_11'/></a></li>		
	</ul> --%>
	
</div>
	 <script type="text/javascript">
	//Initialize portlet with settings & calendar icon
	 ns.common.initializePortlet("alertTabs");
	    $(document).ready(function () {
	    	if($("#alertLogGridId tbody").data("ui-jqgrid") != undefined){
	    		$("#alertLogGridId tbody").sortable("destroy");
	    	}
			
			//Set default date range for date range
			var startDate = 	"<ffi:getProperty name='logMessageSearchCriteria' property='startDate' />";
			var endDate = 	"<ffi:getProperty name='logMessageSearchCriteria' property='endDate' />" ;			
			$("#StartDateID").val(startDate);
			$("#EndDateID").val(endDate);
			var aUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?onlyOneDirection=true&calDirection=back&calForm=LogForm&calTarget=FindLogMessages.StartDate"/>';
	    });
    </script>