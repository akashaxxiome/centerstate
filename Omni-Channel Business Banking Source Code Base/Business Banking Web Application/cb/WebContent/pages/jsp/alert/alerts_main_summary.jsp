<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="alertsMainSummaryContainer" style="display:none">
	
<span class="shortcutPathClass" style="display:none;">home_alert,subscribeAlertsLink</span>
		<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>				
	<div id="availableAlertTypesContainer" class="portlet">
	    <div class="portlet-header">
	    	<span class="portlet-title" title="Alerts"><s:text name='jsp.home_29' /></span>
	    </div>
	    <div class="portlet-content">
	    	<ffi:help id="alerts-main-summary" className="moduleHelpClass"/>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
				<ffi:setProperty name="showAlertTypesForConsumer" value="true"/>		
			</ffi:cinclude>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
				<ffi:setProperty name="showAlertTypesForConsumer" value="true"/>		
			</ffi:cinclude>
			<ffi:cinclude value1="${showAlertTypesForConsumer}" value2="true" operator="equals">
				<s:include value="/pages/jsp/alert/inc/available_alert_types_consumer.jsp"/>		
			</ffi:cinclude>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
				<s:include value="/pages/jsp/alert/inc/available_alert_types_corporate.jsp"/>
			</ffi:cinclude>		
		</div>
    	<div id="addEditAlertsTabsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       		<ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('availableAlertTypesContainer')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      		</ul>
		</div>
</div>

		
		
	<div id="formerrors"></div>
	<div id="addEditAlertContainer" style="display:none;">
		<%-- <s:include value="/pages/jsp/alert/alerts_form.jsp"/>--%>
		
		<ffi:cinclude value1="${showAlertTypesForConsumer}" value2="true" operator="equals">
		<div alertcode="1"><s:include value="/pages/jsp/alert/inc/accountBalanceThresholdAlertForm.jsp"/></div>	
			<div alertcode="2"><s:include value="/pages/jsp/alert/inc/accountBalanceSummaryAlertForm.jsp"/></div>		
			<div alertcode="3"><s:include value="/pages/jsp/alert/inc/transactionAlertForm.jsp"/></div>
			<div alertcode="4"><s:include value="/pages/jsp/alert/inc/insufficientFundsAlertForm.jsp"/></div>
			<div alertcode="5"><s:include value="/pages/jsp/alert/inc/stockPortFolioAlertForm.jsp"/></div>		
			<div alertcode="6"><s:include value="/pages/jsp/alert/inc/unreadBankMessageAlertform.jsp"/></div>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
				<div alertcode="21"><s:include value="/pages/jsp/alert/inc/reversePositivePayAlertForm.jsp"/></div>
			</ffi:cinclude>		
			<div alertcode="300"><s:include value="/pages/jsp/alert/inc/immediateTransactionAlertForm.jsp"/></div>			
		</ffi:cinclude>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<div alertcode="17"><s:include value="/pages/jsp/alert/inc/accountBalanceThresholdAlertForm_corporate.jsp"/></div>		
			<div alertcode="6"><s:include value="/pages/jsp/alert/inc/unreadBankMessageAlertform_corporate.jsp"/></div>
			<div alertcode="7"><s:include value="/pages/jsp/alert/inc/stockPortFolioAlertForm_corporate.jsp"/></div>
			<div alertcode="16"><s:include value="/pages/jsp/alert/inc/insufficientFundsAlertForm_corporate.jsp"/></div>			
			<div alertcode="18"><s:include value="/pages/jsp/alert/inc/transactionAlertForm_corporate.jsp"/></div>
			<ffi:cinclude value1="${IsApprover}" value2="true" operator="equals">			
			<div alertcode="10"><s:include value="/pages/jsp/alert/inc/paymentApprovalsAlertform_corporate.jsp"/></div>		
			</ffi:cinclude>	
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_ENTITLEMENT %>" >
				<div alertcode="15"><s:include value="/pages/jsp/alert/inc/positivePayAlertForm.jsp"/></div>
			</ffi:cinclude>	
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
				<div alertcode="21"><s:include value="/pages/jsp/alert/inc/reversePositivePayAlertForm.jsp"/></div>
			</ffi:cinclude>					
		</ffi:cinclude>		
		<div alertcode="11"><s:include value="/pages/jsp/alert/inc/changepasswordAlertform.jsp"/></div>		
		<div alertcode="12"><s:include value="/pages/jsp/alert/inc/bankInquieryResponseAlertForm.jsp"/></div>
		<div alertcode="14"><s:include value="/pages/jsp/alert/inc/invalidAccountAccessAlertform.jsp"/></div>
		<div alertcode="13"><s:include value="/pages/jsp/alert/inc/failedAttemptLockoutAlertform.jsp"/></div>
		<div alertcode="19"><s:include value="/pages/jsp/alert/inc/panicPayAlertform.jsp"/></div>
		
	</div>	
	
</div>
<ffi:removeProperty name="showAlertTypesForConsumer"/>
<script>
/* $('#alertsGridContainer').portlet({
	title: js_label_current_subscribed_alert,
	helpCallback: function(){
		var fileName = $('#alertsGridContainer').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/','/cb/web/help/' + fileName);
	},
	generateDOM: true
}); */
//Initialize portlet with settings & calendar icon
ns.common.initializePortlet("availableAlertTypesContainer");
</script>