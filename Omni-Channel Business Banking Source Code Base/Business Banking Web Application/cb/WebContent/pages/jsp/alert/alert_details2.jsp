<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="viewAlertPortlet" class="portlet" style="float: left; width: 100%;">
	    <div class="portlet-header">
	    	<span class="portlet-title" title="Alerts"><s:text name='jsp.alert_66' /></span>
	    </div>
	    <div class="portlet-content">
	    	<ffi:help id="alerts_received_view"/>
   			<div id="alertView"></div>
		</div>
    	<div id="addEditAlertsTabsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       		<ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('viewAlertPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      		</ul>
		</div>
</div>



<script>
//Initialize portlet with settings & calendar icon
ns.common.initializePortlet("viewAlertPortlet");

/* $('#viewAlertPortlet').portlet({
	title: js_alert_view_portlet_title,
	close: true,
	closeCallback: function(){
		$('#details2').slideUp();
	},
	generateDOM: true,
	helpCallback: function() {
		var fileName = null;
		if( $('#alertSubscriptionFormDiv').length >0){
			if($('#alertSubscriptionFormDiv').css('display').toLowerCase() == 'block' ){
				fileName = $('#alertSubscriptionFormDiv').find('.moduleHelpClass').html();
			}
		}else{
			fileName = $('#viewAlertPortlet').find('.moduleHelpClass').html();
		}
		callHelp('/cb/web/help/','/cb/web/help/' + fileName);
	}
});

$('#alertView').pane({
	title: js_alert_view_portlet_title,
	showHeader: false
}); */


</script>
    