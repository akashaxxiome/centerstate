<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="quicksearchcriteria" style="display:none; padding: 5px;">
			<s:form action="getAlertLogsAction_filter" namespace="/pages/jsp/alert" method="post" id="FilterAlertLogForm" name="FilterAlertLogForm" theme="simple">
				<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
				<div class="acntDashboard_masterHolder">
					<div class="acntDashboard_itemHolder">
						<span class="sectionsubhead dashboardLabelMargin" style="display:block;"><s:text name="jsp.alert_13"/></br>
						</span>
						
						<ffi:setProperty name="Compare" property="Value1" value="${FindLogMessages.Type}" />
						<select id="FindLogMessages_Type" class="ui-corner-all" name="alertType" size="1">
							<ffi:setProperty name="Compare" property="Value2" value="" />
							<option value="" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.default_39"/></option>
							<ffi:setProperty name="Compare" property="Value2" value="AccountBalance" />
							<option value="AccountBalance" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_3"/></option>							
							<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
								<ffi:setProperty name="Compare" property="Value2" value="ACCOUNTSUMMARY" />
								<option value="ACCOUNTSUMMARY" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_95"/></option>
							</ffi:cinclude>
							<ffi:setProperty name="Compare" property="Value2" value="InquiryResponse" />
							<option value="InquiryResponse" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_19"/></option>
							<ffi:setProperty name="Compare" property="Value2" value="BankMessage" />
							<option value="BankMessage" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_21"/></option>
							<ffi:setProperty name="Compare" property="Value2" value="Lockout" />
							<option value="Lockout" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_30"/></option>
							<ffi:setProperty name="Compare" property="Value2" value="InvalidAccess" />
							<option value="InvalidAccess" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_35"/></option>
							<ffi:setProperty name="Compare" property="Value2" value="NSF" />
							<option value="NSF" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_43"/></option>
							<ffi:setProperty name="Compare" property="Value2" value="PanicPay" />
							<option value="PanicPay" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_96"/></option>
							<ffi:setProperty name="Compare" property="Value2" value="PasswordChange" />
							<option value="PasswordChange" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_49"/></option>
							<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="notEquals">
								<ffi:setProperty name="Compare" property="Value2" value="PaymentApprovals" />
								<option value="PaymentApprovals" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_50"/></option>
			
								<ffi:setProperty name="Compare" property="Value2" value="PositivePay" />
								<option value="PositivePay" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.default_327"/></option>							
								
								<ffi:setProperty name="Compare" property="Value2" value="StockPortfolio" />
								<option value="ProcessStockPortfolio" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_58"/></option>
							</ffi:cinclude>
							<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
								<ffi:setProperty name="Compare" property="Value2" value="StockPortfolio" />
								<option value="STOCK" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_58"/></option>
							</ffi:cinclude>
							<ffi:setProperty name="Compare" property="Value2" value="TermsCondition" />
							<option value="TermsCondition" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.alert_97"/></option>
							<ffi:setProperty name="Compare" property="Value2" value="Transaction" />
							<option value="Transaction" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.default_436"/></option>
			
						</select>
					</div>
					<div class="acntDashboard_itemHolder">
						<span class="sectionsubhead dashboardLabelMargin" style="display:block;"><s:text name="jsp.default.label.date.range"/></span>
						<input type="text" id="alertsDateRangeBox" />
						<input type="hidden" id="StartDateID" name="startDate" value="<ffi:getProperty name='logMessageSearchCriteria' property='startDate' />" />
						<input type="hidden" id="EndDateID" name="endDate" value="<ffi:getProperty name='logMessageSearchCriteria' property='endDate' />" />
						
					</div>
					<div class="acntDashboard_itemHolder">						
						<%-- <span class="sectionsubhead dashboardLabelMargin" style="display:block;">&nbsp;</span> --%>
						<sj:a 
							targets="logsPlaceHolder"
							id="filterAlertLogLink"
							formIds="FilterAlertLogForm"
							validate="true"
							validateFunction="customValidation"
							onclick="removeValidationErrors();"
							button="true"
					    	onSuccessTopics="filterLogs"
					    	onErrorTopics="errorFilteringAlertLogs"
					    	onCompleteTopics="FilteringAlertLogsComplete">
								<s:text name="jsp.default_6" />
						</sj:a>
					</div>
				</div>
				<div class=""><span id="alertsDateRangeBoxError"/></div>
			</s:form>
</div>
<script>
	$(document).ready(function() {
		var aUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?onlyOneDirection=true&calDirection=back&calForm=LogForm&calTarget=FindLogMessages.StartDate"/>';
		var aConfig = {
			url:aUrl,
			startDateBox:$("#StartDateID"),
			endDateBox:$("#EndDateID")
		}
		$("#alertsDateRangeBox").extdaterangepicker(aConfig);
		
		$('#FindLogMessages_Type').selectmenu({width:200});
		ns.alert.successFilteringAlertLogsURL = '<ffi:urlEncrypt url="/cb/pages/jsp/alert/alertLogViewAction.action"/>';
		var type = $("#showAlertsLogsLink").attr("alertType");
		if(type === undefined){
			type="";
		}
		$("#FindLogMessages_Type").selectmenu("value",type);	
		$('#showAlertsLogsLink').removeAttr("alertType");
	});
</script>
