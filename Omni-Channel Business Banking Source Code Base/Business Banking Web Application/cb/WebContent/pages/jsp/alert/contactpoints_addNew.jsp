<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="alerts_newContactPoint"/>
	<s:form id="addNewCPForm" action="addContactPoint" namespace="/pages/jsp/alert" method="post" name="addNewCPForm" theme="simple">
	
	
            <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>	
            <input type="hidden" name="method" value="create"/>
			<table border="0" cellspacing="0" cellpadding="3" class="greenClayBackground tableData" width="100%">
				<tr>
					<td><s:text name="contactPoints.addNewForm.text.name"/><span class="required">*</span></td>
					<td><s:text name="contactPoints.addNewForm.select.type"/><span class="required">*</span></td>
					<td><s:text name="contactPoints.addNewForm.text.address"/><span class="required">*</span></td>
				</tr>
				<tr>
					<td><input class="txtbox ui-widget-content ui-corner-all" type="text" id="name" name="name" size="50"></td>
					<td>
						<select class="form_elements_nowidth" name="contactPointType" size="1" id="contactPointType">					
							<option value="-1"><s:text name="contactPoints.addNewForm.select.option1"/></option>
							<option value="2"><s:text name="contactPoints.addNewForm.select.option2"/></option>              
						</select>	
					</td>
					<td><input class="txtbox ui-widget-content ui-corner-all" type="text" id="address" name="address" size="50" ></td>
				</tr>
				<tr>
					<td><span id="nameError" class="errorLabel"></span></td>
					<td><span id="contactPointTypeError" class="errorLabel"></span>	</td>
					<td><span id="addressError" class="errorLabel"></span></td>
				</tr>
				<%-- <tr>
					<td align="right" nowrap class="greenClayBackground" ><span class="sectionsubhead"><s:text name="contactPoints.addNewForm.text.name"/></span><span class="required">*</span></td>
					<td colspan="3">
						<input class="txtbox ui-widget-content ui-corner-all" type="text" id="name" name="name" size="50">
						<span id="nameError" class="errorLabel"></span>
					</td>
				</tr>
			
				<tr>
					<td align="right" nowrap class="greenClayBackground" ><span class="sectionsubhead"><s:text name="contactPoints.addNewForm.select.type"/></span><span class="required">*</span></td>
					<td colspan="3">
						<select class="form_elements_nowidth" name="contactPointType" size="1" id="contactPointType">					
							<option value="-1"><s:text name="contactPoints.addNewForm.select.option1"/></option>
							<option value="2"><s:text name="contactPoints.addNewForm.select.option2"/></option>              
						</select>	
						<span id="contactPointTypeError" class="errorLabel"></span>						
					</td>
				</tr>

				<tr>
					<td align="right" nowrap class="greenClayBackground" ><span class="sectionsubhead"><s:text name="contactPoints.addNewForm.text.address"/></span><span class="required">*</span></td>
					<td colspan="3">
						<input class="txtbox ui-widget-content ui-corner-all" type="text" id="address" name="address" size="50" >
						<span id="addressError" class="errorLabel"></span>
					</td>
				</tr> --%>

				<%-- <tr>
					<td colspan="4" align="center" nowrap class="greenClayBackground">
							<sj:a id="cancelAddNewContactPoint" button="true"><s:text name="jsp.default_82"/></sj:a>
				            <sj:a id="cpSubmit" formIds="addNewCPForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddingNewCP" onCompleteTopics="addingNewCPComplete" onSuccessTopics="successCreatingCP">
								<s:text name="contactPoints.addNewForm.button.save"/>
							</sj:a>
					</td>
				</tr> --%>
			</table>
			<div class="btn-row">
				<sj:a id="cancelAddNewContactPoint" button="true" summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelNewCp"><s:text name="jsp.default_82"/></sj:a>
	            <sj:a id="cpSubmit" formIds="addNewCPForm"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeAddingNewCP" onCompleteTopics="addingNewCPComplete" onSuccessTopics="successCreatingCP">
					<s:text name="contactPoints.addNewForm.button.save"/>
				</sj:a>
			</div>
	</s:form>
	<div id="targetDiv">
	</div>
	

