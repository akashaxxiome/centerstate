<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<ffi:setGridURL grid="GRID_alertsubScribedBalance" name="DeleteURL" url="/cb/pages/jsp/alert/alert_unsub_confirm.jsp?AlertID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_alertsubScribedBalance" name="EditURL" url="/cb/pages/jsp/alert/AlertCRUDAction.action?method=modify-active&SetAlert.ID={0}" parm0="ID"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/alert/GetBalanceAlertsAction.action?GridURLs=GRID_alertsubScribedBalance" URLEncrypt="true"/>
	<s:url id="subscribedBalanceAlertsUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="subscribedBalAlertsGridId"  
		caption="%{getText('jsp.alert_92')}"  
		sortable="true"  
		dataType="json"  
		href="%{subscribedBalanceAlertsUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		reloadTopics="reloadSubscribedBalanceAlerts"
		onGridCompleteTopics="subscribedBalAlertsGridCompleteEvents"
		> 
		
		<sjg:gridColumn 
			name="map.alertname" 
			index="alertname" 
			title="%{getText('jsp.default_20')}" 
			sortable="true" 
			width="195"
			cssClass="datagrid_textColumn"/>

		<sjg:gridColumn 
			name="map.MINAMOUNT" 
			index="minamount" 
			title="%{getText('jsp.alert_39')}" 
			sortable="true" 
			width="45"
			cssClass="datagrid_numberColumn"/>
			
		<sjg:gridColumn 
			name="map.MAXAMOUNT" 
			index="maxamount" 
			title="%{getText('jsp.alert_33')}" 
			sortable="true" 
			width="45"
			cssClass="datagrid_numberColumn"/>
		
		<sjg:gridColumn 
			name="map.Restricted" 
			index="restricted" 
			title="%{getText('jsp.alert_52')}" 
			sortable="false" 
			hidden="true" 
			hidedlg="true" 
			width="0"
			cssClass="datagrid_textColumn"/>

		<sjg:gridColumn 
			name="ID" 
			index="id" 
			title="" 
			hidedlg="true" 
			search="false"  
			sortable="false" 
			hidden="false" 
			formatter="ns.alert.formatBalanceAlertIcons" 
			width="15"
			cssClass="datagrid_actionIcons"/>
			
		<sjg:gridColumn 
			name="map.DeleteURL" 
			index="DeleteURL" 
			title="%{getText('jsp.default_167')}" 
			sortable="true" 
			width="60" 
			hidden="true" 
			hidedlg="true"
			cssClass="datagrid_actionIcons"/>
	
		<sjg:gridColumn 
			name="map.EditURL" 
			index="EditURL" 
			title="%{getText('jsp.default_181')}" 
			sortable="true" 
			width="60" 
			hidden="true" 
			hidedlg="true"
			cssClass="datagrid_actionIcons"/>
			
	</sjg:grid>
	

    <script type="text/javascript">

	    $(document).ready(function () {

	    	//jQuery("#list1").jqGrid('navGrid','#pager1',{edit:false,add:false,del:false,search:false,refresh:false}); 
	    	$("#subscribedBalAlertsGridId").jqGrid('navButtonAdd', "#subscribedBalAlertsGridId_pager", {
	    	    caption: js_search,
	    	    title: js_search,
	    	    buttonicon: 'ui-icon-search',
	    	    onClickButton: function(){
	    			$("#subscribedBalAlertsGridId")[0].toggleToolbar()
	    	    }
	    	});

	    	$("#subscribedBalAlertsGridId").jqGrid('navButtonAdd', "#subscribedBalAlertsGridId_pager", {
	    	    caption: js_clear_search,
	    	    title: js_clear_search,
	    	    buttonicon: 'ui-icon-refresh',
	    	    onClickButton: function(){
	    			$("#subscribedBalAlertsGridId")[0].clearToolbar()
	    	    }
	    	});

	    	$("#subscribedBalAlertsGridId").jqGrid('navButtonAdd', "#subscribedBalAlertsGridId_pager", {
	    	    caption: js_select_column,
	    	    title: js_select_column,
	    	    buttonicon: 'ui-icon-grip-dotted-vertical',
	    	    onClickButton: function(){
	    			$("#subscribedBalAlertsGridId").jqGrid('columnChooser');
	    	    }
	    	});
	    	
	    	$("#subscribedBalAlertsGridId").jqGrid('filterToolbar');
	    	$("#subscribedBalAlertsGridId")[0].toggleToolbar();

	    	if($("#subscribedBalAlertsGridId tbody").data("ui-jqgrid") != undefined){
	    		$("#subscribedBalAlertsGridId tbody").sortable("destroy");
	    	}

	    });

    </script>
