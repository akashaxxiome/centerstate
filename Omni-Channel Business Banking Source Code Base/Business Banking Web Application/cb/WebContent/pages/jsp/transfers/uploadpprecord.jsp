<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<% 
    request.setAttribute("actionName","/pages/fileupload/transferFileUploadAction.action");
	String fileType = request.getParameter("fileType");
%>

<ffi:setProperty name="fileType" value="<%=fileType %>"/>
<ffi:setProperty name="ProcessImportTask" value="CheckTransferImport"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	<ffi:process name="SetMappingDefinition"/>
</ffi:cinclude>

    <s:include value="/pages/jsp/transfers/transferuploader.jsp"/>

<script>
	ns.common.popupAfterUpload = false;
</script>    