<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="transferfileuploadPortlet">
<div class="portlet-header ui-widget-header ui-corner-all">
  	  <span class="portlet-title"><s:text name="jsp.default_211"/></span>
</div>
<div class="portlet-content">
	<ffi:help id="acctMgmt_fileupload"  />
    <div id="transgerfileUploaderPanelID">
    	<div id="selecttransferImportFormatOfFileUploadID">
		</div>
        <div id="selectFileFormAndStatusID" class="selectFilePane">
			<div id="selectFileOFFileUploadFormID"></div>
        </div>
    </div>
</div>
<div id="transferDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
       		  <li><a href='#' onclick="ns.common.showDetailsHelp('transferfileuploadPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
</div>
</div>
<script>
  //Initialize portlet with settings icon
	ns.common.initializePortlet("transferfileuploadPortlet");
</script>