<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<script type="text/javascript" src="<s:url value='/web/js/transfers/transfer%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/transfers/transfer_grid%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/transfers/external_transfer_account%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/transfers/external_transfer_account_grid%{#session.minVersion}.js'/>"></script>

<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/transfers/transfer%{#session.minVersion}.css'/>"></link>

<div id="desktop" align="center">
	<div id="appdashboard">
    	<s:action namespace="/pages/jsp/transfers" name="TransferDashBoardAction" executeResult="true"/>
	</div>
	<div id="details" style="display:none;">
		<s:include value="/pages/jsp/transfers/transfer_details.jsp"/>
	</div>
	<div id="transferfileupload" style="display:none;">
		<s:include value="transferimportconsole.jsp"/>
	</div>
	<div id="customMappingDetails" style="display:none">
		<s:include value="/pages/jsp/custommapping_workflow.jsp"/>
	</div>
	<div id="mappingDiv">
   	</div>
	<div id="summary">
	<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
		<ffi:cinclude value1="${summaryToLoad}" value2="Template"	operator="notEquals">
				<s:action namespace="/pages/jsp/transfers" name="TransferSummaryAction" executeResult="true"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${summaryToLoad}" value2="Template"	operator="equals">
			<s:action namespace="/pages/jsp/transfers" name="TransferTemplateSummaryAction" executeResult="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	ns.transfer.showTransferDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
});
</script>