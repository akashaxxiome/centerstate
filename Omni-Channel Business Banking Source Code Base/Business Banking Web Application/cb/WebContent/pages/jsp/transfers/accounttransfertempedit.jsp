<%@ page import="com.ffusion.beans.FundsTransactions"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.banking.TransferDefines"%>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<ffi:setProperty name="GetHelpName" property="helpKey" value="paymentsAccounttransfertempedit" />
<ffi:process name="GetHelpName"/>
<div class="moduleHelpClass" style="display:none"><ffi:getProperty name="helpFileName" /></div>

<ffi:setProperty name="TransferAccounts" property="Filter" value="All" />
<% boolean toggle = false; %>


<ffi:removeProperty name="OpenEnded" />
<ffi:removeProperty name="NumberTransfers" />
<ffi:removeProperty name="AddEditTransferTemplate" />
<ffi:removeProperty name="DeleteTransferTemplate" />
<ffi:removeProperty name="LoadFromTransferTemplate" />
<ffi:removeProperty name="templateEdit"/>

<ffi:setProperty name="TransferHomeURL" value="${SecurePath}payments/accounttransfertempedit.jsp"/>


<% if (request.getParameter("SingleTransferTemplateList.ToggleSortedBy") == null && request.getParameter("MultipleTransferTemplateList.ToggleSortedBy") == null) { %>
    <ffi:process name="GetAllTransferTemplates" />
    <ffi:object name="com.ffusion.tasks.banking.SplitFundTransactionTemplates" id="SplitFundTransactionTemplates" scope="session"/>
    <ffi:process name="SplitFundTransactionTemplates" />
<% } %>

<%
    session.setAttribute("DisplayEstimatedFlag", "false");
    FundsTransactions templates = (FundsTransactions)session.getAttribute("SingleTransferTemplateList");
    Iterator it = templates.iterator();
    while (it.hasNext())
    {
        Transfer transfer = (Transfer)it.next(); // this is a transfer since batches were filtered out
        if (transfer.getTemplateName() != null && transfer.getTemplateName().length() > 0) // must have template name
        {
            if (transfer.getIsAmountEstimated() || transfer.getIsToAmountEstimated())
            {
                session.setAttribute("DisplayEstimatedFlag", "true");
                break;
            }
        }
    }
%>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>">
    <ffi:setProperty name="DisplayToAmount" value="true"/>
</ffi:cinclude>

		<div align="center">
			<%-- include page header --%>
			<ffi:include page="${PathExt}payments/inc/nav_header.jsp" />
			<div align="center">
				<table   border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="8"   background="/cb/web/multilang/grafx/cash/sechdr_blank.gif" class="sectiontitle">&nbsp;&nbsp; &gt; <s:text name="jsp.transfers_72"/></td>
					</tr>
<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="SingleTransferTemplateList"/>
<ffi:process name="SortImage"/>
					<tr>
<ffi:setProperty name="SortImage" property="Compare" value="TEMPLATE_NAME"/>
                        <td class="tbrd_ltb sectionsubhead" nowrap><s:text name="jsp.default_283"/><ffi:getProperty name="SortImage" property="Anchor" encode="false"/></td>
<ffi:setProperty name="SortImage" property="Compare" value="TRANSFER_DESTINATION"/>
						<td class="tbrd_tb sectionsubhead" nowrap><s:text name="jsp.default_444"/><ffi:getProperty name="SortImage" property="Anchor" encode="false"/></td>
<ffi:setProperty name="SortImage" property="Compare" value="FROMACCOUNTID"/>
						<td class="tbrd_tb sectionsubhead" nowrap><s:text name="jsp.default_217"/><ffi:getProperty name="SortImage" property="Anchor" encode="false"/></td>
<ffi:setProperty name="SortImage" property="Compare" value="TOACCOUNTID"/>
						<td class="tbrd_tb sectionsubhead" nowrap><s:text name="jsp.default_424"/><ffi:getProperty name="SortImage" property="Anchor" encode="false"/></td>

<ffi:cinclude value1="${DisplayToAmount}" value2="false">
<ffi:setProperty name="SortImage" property="Compare" value="AMOUNT"/>
						<td class="tbrd_tb sectionsubhead" align="right" nowrap><s:text name="jsp.billpay_amount" /><ffi:getProperty name="SortImage" property="Anchor" encode="false"/></td>
</ffi:cinclude>
<ffi:cinclude value1="${DisplayToAmount}" value2="true">
<ffi:setProperty name="SortImage" property="Compare" value="AMOUNT"/>
						<td class="tbrd_tb sectionsubhead" align="right" nowrap><s:text name="jsp.default_489" /><ffi:getProperty name="SortImage" property="Anchor" encode="false"/></td>
<ffi:setProperty name="SortImage" property="Compare" value="TOAMOUNT"/>
						<td class="tbrd_tb sectionsubhead" align="right" nowrap><s:text name="jsp.default_512"/><ffi:getProperty name="SortImage" property="Anchor" encode="false"/></td>
</ffi:cinclude>
<ffi:setProperty name="SortImage" property="Compare" value="STATUSNAME"/>
                        <td class="tbrd_tb sectionsubhead" nowrap>&nbsp;&nbsp;<s:text name="jsp.default_388"/><ffi:getProperty name="SortImage" property="Anchor" encode="false"/></td>
                        <td class="tbrd_trb"><br></td>
					</tr>

				<% toggle = false; %>
                    <!-- Single Templates -->
                    <ffi:list collection="SingleTransferTemplateList" items="TransTemplate">
                    <ffi:cinclude value1="${TransTemplate.TemplateName}" value2="" operator="notEquals">
                <%       toggle = !toggle; %>
					<tr class="<%= toggle ? "" : "ltrow1_color" %>">

						<td class="columndata" nowrap><ffi:getProperty name="TransTemplate" property="TemplateName"/></td>
						<td class="columndata">
                            <ffi:cinclude value1="${TransTemplate.Type}" value2="0" operator="notEquals">
                                <ffi:cinclude value1="${TransTemplate.TransferDestination}" value2="<%= TransferDefines.TRANSFER_ITOI %>" operator="equals">
                                    <s:text name="jsp.transfers_53"/>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${TransTemplate.TransferDestination}" value2="<%= TransferDefines.TRANSFER_ITOE %>" operator="equals">
                                    <s:text name="jsp.transfers_52"/>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${TransTemplate.TransferDestination}" value2="<%= TransferDefines.TRANSFER_ETOI %>" operator="equals">
                                    <s:text name="jsp.transfers_44"/>
                                </ffi:cinclude>
                            </ffi:cinclude>
                        </td>
						<td class="columndata">
							<div align="left">
							<ffi:getProperty name="TransTemplate" property="FromAccount.DisplayText"/>
							<ffi:cinclude value1="${TransTemplate.FromAccount.NickName}" value2="" operator="notEquals" >
								 - <ffi:getProperty name="TransTemplate" property="FromAccount.NickName"/>
					 		</ffi:cinclude>
							 - <ffi:getProperty name="TransTemplate" property="FromAccount.CurrencyCode"/></div>
						</td>
						<td class="columndata">
							<ffi:getProperty name="TransTemplate" property="ToAccount.DisplayText"/>
							<ffi:cinclude value1="${TransTemplate.ToAccount.NickName}" value2="" operator="notEquals" >
								 - <ffi:getProperty name="TransTemplate" property="ToAccount.NickName"/>
					 		</ffi:cinclude>
							 - <ffi:getProperty name="TransTemplate" property="ToAccount.CurrencyCode"/>
						</td>
						<td class="columndata" align="right">
                            <ffi:cinclude value1="${TransTemplate.AmountValue.CurrencyStringNoSymbol}" value2="0.00" operator="equals">
                                --
                            </ffi:cinclude>
                            <ffi:cinclude value1="${TransTemplate.AmountValue.CurrencyStringNoSymbol}" value2="0.00" operator="notEquals">
                                <ffi:cinclude value1="${TransTemplate.IsAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                                <ffi:getProperty name="TransTemplate" property="AmountValue.CurrencyStringNoSymbol"/>
                                <ffi:getProperty name="TransTemplate" property="AmountValue.CurrencyCode"/>
                            </ffi:cinclude>
                        </td>
                        <ffi:cinclude value1="${DisplayToAmount}" value2="true">
                        <td class="columndata" align="right">
                            <ffi:cinclude value1="${TransTemplate.ToAmountValue.CurrencyStringNoSymbol}" value2="0.00" operator="equals">
                                --
                            </ffi:cinclude>
                            <ffi:cinclude value1="${TransTemplate.ToAmountValue.CurrencyStringNoSymbol}" value2="0.00" operator="notEquals">
                                <ffi:cinclude value1="${TransTemplate.IsToAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                                <ffi:getProperty name="TransTemplate" property="ToAmountValue.CurrencyStringNoSymbol"/>
                                <ffi:getProperty name="TransTemplate" property="ToAmountValue.CurrencyCode"/>
                            </ffi:cinclude>
                        </td>
                        </ffi:cinclude>
                        <td class="columndata">
                            &nbsp;&nbsp;<ffi:getProperty name="TransTemplate" property="StatusName"/>
                        </td>
                        <td align="right" nowrap>
                            <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS %>">
                                <ffi:setProperty name="ExternalTransfer" value="false"/>
                                <ffi:cinclude value1="${TransTemplate.TransferDestination}" value2="ITOE">
                                    <ffi:setProperty name="ExternalTransfer" value="true"/>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${TransTemplate.TransferDestination}" value2="ETOI">
                                    <ffi:setProperty name="ExternalTransfer" value="true"/>
                                </ffi:cinclude>

                                <ffi:cinclude value1="${TransTemplate.CanLoad}" value2="true">
                                    <ffi:link url="accounttransfernew.jsp?LoadFromTransferTemplate=${TransTemplate.ID}&checkEffDate=${ExternalTransfer}"><img src="/cb/web/multilang/grafx/i_use.gif" alt="<s:text name="jsp.default_263"/>" height="14" width="15" border="0"></ffi:link>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${TransTemplate.CanLoad}" value2="false">
                                    <img src="/cb/web/multilang/grafx/payments/i_use_dim.gif" alt="<s:text name="jsp.transfers_24"/>" height="14" width="15" border="0">
                                </ffi:cinclude>
                            </ffi:cinclude>

                            <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_TRANSFERS_TEMPLATE %>">
                                <ffi:cinclude value1="${TransTemplate.CanEdit}" value2="true">
                                    <ffi:link url="accounttransferedit.jsp?SetFundsTran.Name=SingleTransferTemplateList&TemplateID=${TransTemplate.ID}&templateEdit=true"><img src="/cb/web/multilang/grafx/payments/i_edit.gif" alt="<s:text name="jsp.default_179"/>" height="14" width="19" border="0"></ffi:link>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${TransTemplate.CanEdit}" value2="false">
                                    <img src="/cb/web/multilang/grafx/payments/i_edit_dim.gif" alt="<s:text name="jsp.transfers_23"/>" height="14" width="19" border="0">
                                </ffi:cinclude>
                                <ffi:cinclude value1="${TransTemplate.CanDelete}" value2="true">
                                    <ffi:link url="accounttransferdelete.jsp?SetFundsTran.Name=SingleTransferTemplateList&TemplateID=${TransTemplate.ID}&templateEdit=true"><img src="/cb/web/multilang/grafx/payments/i_trash.gif" alt="<s:text name="jsp.default_163"/>" height="14" width="14" border="0"></ffi:link>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${TransTemplate.CanDelete}" value2="false">
                                    <img src="/cb/web/multilang/grafx/payments/i_trash_dim.gif" alt="<s:text name="jsp.default_84"/>" height="14" width="14" border="0">
                                </ffi:cinclude>
                            </ffi:cinclude>
						</td>
					</tr>
                </ffi:cinclude>
                </ffi:list>
				    <tr>
                        <td class="rquired tbrd_full" colspan="8" align="right">&nbsp;
                            <ffi:cinclude value1="${DisplayEstimatedFlag}" value2="true">
                                &#8776; <s:text name="jsp.default_241"/>&nbsp;&nbsp;
                            </ffi:cinclude>
                        </td>
			       </tr>
                   <tr>
						<td colspan="8"><img src="/cb/web/multilang/grafx/payments/sechdr_pymnts_btm.gif" height="11"  ></td>
					</tr>
                </table>
                <br>
                <table   border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="8"   background="/cb/web/multilang/grafx/cash/sechdr_blank.gif" class="sectiontitle">&nbsp;&nbsp; &gt; <s:text name="jsp.transfers_56"/></td>
					</tr>

<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage1" scope="session"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage1" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="SortImage1" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage1" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage1" property="Collection" value="MultipleTransferTemplateList"/>
<ffi:process name="SortImage1"/>
					<tr>
<ffi:setProperty name="SortImage1" property="Compare" value="TEMPLATENAME"/>
                        <td class="tbrd_ltb sectionsubhead" nowrap><s:text name="jsp.default_283"/><ffi:getProperty name="SortImage1" property="Anchor" encode="false"/></td>
<ffi:setProperty name="SortImage1" property="Compare" value="TRANSFER_DESTINATION"/>
						<td class="tbrd_tb sectionsubhead" nowrap><s:text name="jsp.default_444"/><ffi:getProperty name="SortImage1" property="Anchor" encode="false"/></td>
<ffi:setProperty name="SortImage1" property="Compare" value="FROMACCOUNTID"/>
						<td class="tbrd_tb sectionsubhead" nowrap><s:text name="jsp.default_217"/><ffi:getProperty name="SortImage1" property="Anchor" encode="false"/></td>
<ffi:setProperty name="SortImage1" property="Compare" value="TOACCOUNTID"/>
						<td class="tbrd_tb sectionsubhead" nowrap><s:text name="jsp.default_424"/><ffi:getProperty name="SortImage1" property="Anchor" encode="false"/></td>
<ffi:setProperty name="SortImage1" property="Compare" value="AMOUNT"/>
						<td class="tbrd_tb sectionsubhead" align="right" nowrap><s:text name="jsp.default_43"/><ffi:getProperty name="SortImage1" property="Anchor" encode="false"/></td>
<ffi:setProperty name="SortImage" property="Compare" value="STATUS"/>
                        <td class="tbrd_tb sectionsubhead" nowrap>&nbsp;&nbsp;<s:text name="jsp.default_388"/><ffi:getProperty name="SortImage" property="Anchor" encode="false"/></td>
                        <td class="tbrd_trb" colspan="2"><br></td>
					</tr>

				<% toggle = false; %>
                    <!-- Batch Templates -->
                    <ffi:list collection="MultipleTransferTemplateList" items="TransTemplate">
                <%       toggle = !toggle; %>
					<tr class="<%= toggle ? "" : "ltrow1_color" %>">

						<td class="columndata" nowrap><ffi:getProperty name="TransTemplate" property="TemplateName"/></td>
						<td class="columndata">
                            <ffi:cinclude value1="${TransTemplate.CommonDestination}" value2="">
                                --
                            </ffi:cinclude>
                            <ffi:cinclude value1="${TransTemplate.CommonDestination}" value2="" operator="notEquals">
                                <ffi:cinclude value1="${TransTemplate.CommonDestination}" value2="<%= TransferDefines.TRANSFER_ITOI %>" operator="equals">
                                    <s:text name="jsp.transfers_53"/>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${TransTemplate.CommonDestination}" value2="<%= TransferDefines.TRANSFER_ITOE %>" operator="equals">
                                    <s:text name="jsp.transfers_52"/>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${TransTemplate.CommonDestination}" value2="<%= TransferDefines.TRANSFER_ETOI %>" operator="equals">
                                    <s:text name="jsp.transfers_44"/>
                                </ffi:cinclude>
                            </ffi:cinclude>
                        </td>
						<td class="columndata">
							<div align="left">
                            <ffi:cinclude value1="${TransTemplate.CommonFromAccount.ID}" value2="">
                                --
                            </ffi:cinclude>
                            <ffi:cinclude value1="${TransTemplate.CommonFromAccount.ID}" value2="" operator="notEquals">
                                <ffi:getProperty name="TransTemplate" property="CommonFromAccount.DisplayText"/>
                                <ffi:cinclude value1="${TransTemplate.CommonFromAccount.NickName}" value2="" operator="notEquals" >
                                     - <ffi:getProperty name="TransTemplate" property="CommonFromAccount.NickName"/>
                                </ffi:cinclude>
                                 - <ffi:getProperty name="TransTemplate" property="CommonFromAccount.CurrencyCode"/>
                            </ffi:cinclude>
                            </div>
						</td>
						<td class="columndata">
                            <ffi:cinclude value1="${TransTemplate.CommonToAccount.ID}" value2="">
                                --
                            </ffi:cinclude>
                            <ffi:cinclude value1="${TransTemplate.CommonToAccount.ID}" value2="" operator="notEquals">
                                <ffi:getProperty name="TransTemplate" property="CommonToAccount.DisplayText"/>
                                <ffi:cinclude value1="${TransTemplate.CommonToAccount.NickName}" value2="" operator="notEquals" >
                                     - <ffi:getProperty name="TransTemplate" property="CommonToAccount.NickName"/>
                                </ffi:cinclude>
                                 - <ffi:getProperty name="TransTemplate" property="CommonToAccount.CurrencyCode"/>
                            </ffi:cinclude>
                        </td>
						<td class="columndata" align="right">
                            <ffi:cinclude value1="${TransTemplate.AmountValue.IsZero}" value2="true">
                                --
                            </ffi:cinclude>
                            <ffi:cinclude value1="${TransTemplate.AmountValue.IsZero}" value2="false">
                                <ffi:getProperty name="TransTemplate" property="AmountValue.CurrencyStringNoSymbol"/>
                                <ffi:getProperty name="TransTemplate" property="AmountValue.CurrencyCode"/>
                            </ffi:cinclude>
                        </td>
                        <td class="columndata">
                            <ffi:cinclude value1="${TransTemplate.CommonStatusName}" value2="">
                                &nbsp;&nbsp;--
                            </ffi:cinclude>
                            <ffi:cinclude value1="${TransTemplate.CommonStatusName}" value2="" operator="notEquals">
                                &nbsp;&nbsp;<ffi:getProperty name="TransTemplate" property="CommonStatusName"/>
                            </ffi:cinclude>
                        </td>
                        <td align="right" nowrap>
                            <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS %>">
                                <ffi:setProperty name="CanLoadTemplate" value="true"/>
                                <ffi:list collection="TransTemplate.Transfers" items="Transfer">
                                    <ffi:cinclude value1="${Transfer.CanLoad}" value2="false">
                                        <ffi:setProperty name="CanLoadTemplate" value="false"/>
                                    </ffi:cinclude>
                                </ffi:list>

                                <ffi:cinclude value1="${CanLoadTemplate}" value2="true">
                                    <ffi:link url="accounttransfermultiplenew-wait.jsp?LoadFromTransferTemplate=MULTI_${TransTemplate.ID}"><img src="/cb/web/multilang/grafx/i_use.gif" alt="<s:text name="jsp.default_263"/>" height="14" width="15" border="0"></ffi:link>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${CanLoadTemplate}" value2="false">
                                    <img src="/cb/web/multilang/grafx/payments/i_use_dim.gif" alt="<s:text name="jsp.transfers_24"/>" height="14" width="15" border="0">
                                </ffi:cinclude>
                                <ffi:removeProperty name="CanLoadTemplate"/>
                            </ffi:cinclude>

                            <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_TRANSFERS_TEMPLATE %>">
                                <ffi:setProperty name="CanEditTemplate" value="true"/>
                                <ffi:list collection="TransTemplate.Transfers" items="Transfer">
                                    <ffi:cinclude value1="${Transfer.CanEdit}" value2="false">
                                        <ffi:setProperty name="CanEditTemplate" value="false"/>
                                    </ffi:cinclude>
                                </ffi:list>

                                <ffi:cinclude value1="${CanEditTemplate}" value2="true">
                                    <ffi:link url="accounttransfermultipletempedit-wait.jsp?SetFundsTran.Name=MultipleTransferTemplateList&TemplateID=${TransTemplate.ID}&templateEdit=true"><img src="/cb/web/multilang/grafx/payments/i_edit.gif" alt="<s:text name="jsp.default_179"/>" height="14" width="19" border="0"></ffi:link>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${CanEditTemplate}" value2="false">
                                    <img src="/cb/web/multilang/grafx/payments/i_edit_dim.gif" alt="<s:text name="jsp.transfers_23"/>" height="14" width="19" border="0">
                                </ffi:cinclude>
                                <ffi:removeProperty name="CanEditTemplate"/>

                                <ffi:setProperty name="CanDeleteTemplate" value="true"/>
                                <ffi:list collection="TransTemplate.Transfers" items="Transfer">
                                    <ffi:cinclude value1="${Transfer.CanDelete}" value2="false">
                                        <ffi:setProperty name="CanDeleteTemplate" value="false"/>
                                    </ffi:cinclude>
                                </ffi:list>

                                <ffi:cinclude value1="${CanDeleteTemplate}" value2="true">
                                    <ffi:link url="accounttransfermultipletempdelete.jsp?SetFundsTran.Name=MultipleTransferTemplateList&TemplateID=${TransTemplate.ID}"><img src="/cb/web/multilang/grafx/payments/i_trash.gif" alt="<s:text name="jsp.default_163"/>" height="14" width="14" border="0"></ffi:link>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${CanDeleteTemplate}" value2="false">
                                    <img src="/cb/web/multilang/grafx/payments/i_trash_dim.gif" alt="<s:text name="jsp.default_84"/>" height="14" width="14" border="0">
                                </ffi:cinclude>
                                <ffi:removeProperty name="CanDeleteTemplate"/>
                            </ffi:cinclude>
						</td>
					</tr>
                </ffi:list>
				    <tr>
                        <td class="rquired tbrd_full" colspan="8" align="right">&nbsp;</td>
			       </tr>
                   <tr>
						<td colspan="8"><img src="/cb/web/multilang/grafx/payments/sechdr_pymnts_btm.gif" height="11"  ></td>
					</tr>
                </table>
                <table   border="0" cellspacing="0" cellpadding="0">
                    <tr>
						<td align="center" class="sectionsubhead" nowrap>
                            <br><br>
                            <input class="submitbutton" type="button" value="<s:text name="jsp.default_82"/>" onclick="document.location='<ffi:urlEncrypt url="index.jsp"/>';return false;">&nbsp;
                            <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_TRANSFERS_TEMPLATE %>">
                            <input class="submitbutton" type="button" value="<s:text name="jsp.transfers_16"/>" onclick="document.location='<ffi:urlEncrypt url='accounttransfernew.jsp?templateEdit=true'/>'">&nbsp;
                            <input class="submitbutton" type="button" value="<s:text name="jsp.transfers_13"/>" onclick="document.location='<ffi:urlEncrypt url='accounttransfermultiplenew-wait.jsp?templateEdit=true'/>'">
                            </ffi:cinclude>
                        </td>
					</tr>
                </table>
            </div>
		</div>
