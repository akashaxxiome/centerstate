<%@ page import="com.ffusion.beans.banking.TransferDefines" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


<%
	session.setAttribute("templateEdit", request.getParameter("templateEdit"));
	String fromPortalPage = request.getParameter("fromPortalPage");
	session.setAttribute("fromPortalPage",fromPortalPage);
	String amtValue="";
	String amtCurrency="";
	String toAmtValue="";
	String toAmtCurrency="";
	String userAssignedAmountFlag="";
	String estimatedAmountFlag="";
%>

<ffi:object name="com.ffusion.tasks.util.ResourceList" id="TransferFrequencies" scope="session"/>
	<ffi:setProperty name="TransferFrequencies" property="ResourceFilename" value="com.ffusion.beansresources.banking.resources"/>
	<ffi:setProperty name="TransferFrequencies" property="ResourceID" value="RecTransferFrequencies"/>
<ffi:process name="TransferFrequencies" />
<ffi:removeProperty name="TransferFrequencies" />

<%	boolean recurring = (request.getAttribute("Transfer") instanceof com.ffusion.beans.banking.RecTransfer) ? true : false; %>
<s:set var="tmpI18nStr" value="%{getText('jsp.transfers_34')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
							
<s:form action="/pages/jsp/transfers/skipTransferAction.action" method="post" name="TransferSkip" id="skipTransfeFormId" >
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<input type="hidden" name="transferID" value="<s:property value='transfer.ID'/>"/>
	<input type="hidden" name="recTransferID" value="<s:property value='transfer.recTransferID'/>"/>
	<input type="hidden" name="transferType" value="<s:property value='transfer.transferType'/>"/>
	<input type="hidden" id="isSkipInstance" name="isSkipInstance" value="<s:property value='%{isSkipInstance}'/>" />

<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
<div class="blockContent">
<s:set var="DisplayEstimatedAmountKey" value="false" scope="request" />
<s:set var="userAssignedAmountFlagName" value="transfer.UserAssignedAmountFlagName"/>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="skipTransfer_fromAccountLabelId" class="sectionLabel"><s:text name="jsp.default_217"/>: </span>
            <span id="skipTransfer_fromAccountValueId"><s:property value="transfer.FromAccount.AccountDisplayText"/></span>                     
		</div>
		<div class="inlineBlock">
			<s:if test="%{#userAssignedAmountFlagName=='single'}">
					<span id="skipTransfer_amountLabelId" class="sectionLabel"><s:text name="jsp.default_43"/>: </span>
					<span id="skipTransfer_amountValueId">
						<s:property value="transfer.AmountValue.CurrencyStringNoSymbol"/>
	                    <s:property value="transfer.AmountValue.CurrencyCode"/>
	                </span>
			</s:if>
			<s:elseif test="%{#userAssignedAmountFlagName !='single'}">
	               <span id="skipTransfer_fromAmountLabelId" class="sectionLabel"><s:text name="jsp.default_489"/>: </span>
	               <span id="skipTransfer_fromAmountValueId">
	               	<s:if test="transfer.IsAmountEstimated=='true'">
	                       &#8776;<s:set var="DisplayEstimatedAmountKey" value="true" scope="request" />
	                   </s:if>
	                   <s:property value="transfer.AmountValue.CurrencyStringNoSymbol"/>
	                   <s:property value="transfer.AmountValue.CurrencyCode"/>
	               </span>
	          </s:elseif>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="skipTransfer_toAccountLabelId" class="sectionLabel"><s:text name="jsp.default_424"/>: </span>
            <span id="skipTransfer_toAccountValueId">
            	<s:property value="transfer.ToAccount.AccountDisplayText"/>
    		</span>
		</div>
		<div class="inlineBlock">
			<s:if test="%{#userAssignedAmountFlagName=='single'}">
			</s:if>
			<s:elseif test="%{#userAssignedAmountFlagName !='single'}">
	               <span id="skipTransfer_toAmountLabelId" class="sectionLabel"><s:text name="jsp.default_512"/>: </span>
	               <span id="skipTransfer_toAmountValueId">
	                   <s:if test="transfer.IsAmountEstimated=='true'">
	                       &#8776;<s:set var="DisplayEstimatedAmountKey" value="true" scope="request" />
	                   </s:if>
	                   <s:property value="transfer.ToAmountValue.CurrencyStringNoSymbol"/>
	                   <s:property value="transfer.ToAmountValue.CurrencyCode"/>
	               </span>
	          </s:elseif>
		</div>
	</div>
	<div class="blockRow">
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
            <span id="skipTransfer_TypeLabelId" class="sectionLabel"><s:text name="jsp.default_444"/>: </span>
            <span id="skipTransfer_TypeValueId">
                <s:set var="transferDestination" value="transfer.TransferDestination" /> 
                <s:set var="TRANS_BOOK" value="@com.ffusion.beans.banking.TransferDefines@TRANSFER_BOOK" />
                <s:set var="TRANS_ITOI" value="@com.ffusion.beans.banking.TransferDefines@TRANS_ITOI"/>
                <s:set var="TRANS_ITOE" value="@com.ffusion.beans.banking.TransferDefines@TRANS_ITOE"/>
                <s:set var="TRANS_ETOI" value="@com.ffusion.beans.banking.TransferDefines@TRANS_ETOI"/>
                 <s:if test="%{#transferDestination==#TRANS_BOOK}">
					<s:text name="jsp.transfers_53" />
				</s:if>
				<s:elseif test="%{#transferDestination==#TRANS_ITOI}">
					<s:text name="jsp.transfers_53" />
				</s:elseif>
				<s:elseif test="%{#transferDestination==#TRANS_ITOE}">
					<s:text name="jsp.transfers_52" />
				</s:elseif>
				<s:elseif test="%{#transferDestination==#TRANS_ETOI}">
					<s:text name="jsp.transfers_44" />
				</s:elseif>
            </span>
       </ffi:cinclude>
	</div>
	
	<s:if test="%{#request.istemplateflow!='true'}">
		<%--Display Transfer Date Section --%>
		<s:if test="%{#request.transfer.templateName== null || #request.transfer.templateName=='' 
			|| #request.transfer.transferType== null
			|| #request.transfer.transferType!='TEMPLATE' 
			|| #request.transfer.transferType!='RECTEMPLATE'}">
				<div class="blockRow">
					<span id="skipTransfer_dateLabelId" class="sectionLabel"><s:text name="jsp.default_137" />: </span>
					<span id="skipTransfer_dateValueId"><s:property value="transfer.date" /></span>
				</div>
		</s:if>
	</s:if>
	
	
	<% if (recurring) { %>
      <div class="blockRow">
           <span id="skipTransfer_frequencyLabelId" class="sectionLabel"><s:text name="jsp.default_351"/>: </span>
           <span id="skipTransfer_frequencyValueId"><s:property value="transfer.Frequency" /></span>
      </div>
      <div class="blockRow">
           <span id="skipTransfer_noOfTransfersLabelId" class="sectionLabel"><s:text name="jsp.default_443"/>: </span>
           <span id="skipTransfer_noOfTransfersValueId">
			   <s:if test="transfer.isOpenEnded()">
					<s:text name="jsp.default_446"/>
			   </s:if>
			   <s:else>
					<s:property value="transfer.NumberTransfers"/>
			   </s:else>
			</span>
   	 </div>
    <% } %>
    <div class="blockRow">
		<span id="skipTransfer_memoLabelId" class="sectionLabel"><s:text name="jsp.default_279"/>: </span>
		<span id="skipTransfer_memoValueId"><s:property value="transfer.memo"/></span>
	</div>
</div>
<tr>
<s:if test="%{#DisplayEstimatedAmountKey=='true'}">
	<div class="mltiCurrenceyMessage">
    	<span id="skipTransfer_estimatedAmountLabelId" class="required" colspan="2" align="center">&#8776; <s:text name="jsp.default_241"/></span>
    </div>
</s:if>

</s:form>

<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div  class="ui-widget-header customDialogFooter">
	<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals">
		<sj:a id="cancelSkipSingleTransferLink" button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" >
			<s:text name="jsp.default_82" />
		</sj:a>
		<sj:a id="skipSingleTransferLink" formIds="skipTransfeFormId" targets="resultmessage" button="true"   
			  title="%{getText('jsp.transfers_37.1')}" onCompleteTopics="skipSingleTransferComplete" onSuccessTopics="skipTransferSuccessTopics" onErrorTopics="errorSkipTransfer" 
			  effectDuration="1500" ><s:text name="jsp.default_Skip" /></sj:a>
	</ffi:cinclude>
	<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
		<sj:a id="cancelSkipSingleTransferLinkPortal" button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" >
			<s:text name="jsp.default_82" />
		</sj:a>
		<sj:a id="skipSingleTransferLinkPortal" formIds="skipTransfeFormId" targets="loadingstatus" button="true"   
			  title="%{getText('jsp.transfers_37.1')}" onSuccessTopics="homeTransferDeleteSuccessTopics" 
			  effectDuration="1500" ><s:text name="jsp.default_Skip" /></sj:a>
	</ffi:cinclude>
</div>