<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="TransfersUpdated" value="true"/> <%-- For Register Integration --%>

<ffi:cinclude value1="${EditTransfer}" value2="" operator="notEquals" >
	<ffi:setProperty name="EditTransfer" property="Process" value="true"/>
	<ffi:process name="EditTransfer"/>
</ffi:cinclude>

<ffi:cinclude value1="${AddTransfer}" value2="" operator="notEquals" >
	<ffi:setProperty name="AddTransfer" property="Process" value="true"/>
	<ffi:process name="AddTransfer"/>
</ffi:cinclude>

<ffi:cinclude value1="${DeleteTransfer}" value2="" operator="notEquals" >
	<ffi:process name="DeleteTransfer"/>
</ffi:cinclude>

<ffi:removeProperty name="OpenEnded" />
<ffi:removeProperty name="AddTransfer" />
<ffi:removeProperty name="DeleteTransfer" />
<ffi:removeProperty name="EditTransfer" />
<ffi:removeProperty name="ConfirmTransfer" />
<script>document.location='<ffi:urlEncrypt url="${SecurePath}payments/index.jsp?Refresh=true" />'</script>
