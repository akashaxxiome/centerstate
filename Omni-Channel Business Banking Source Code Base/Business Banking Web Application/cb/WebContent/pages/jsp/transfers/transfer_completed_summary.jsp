<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_accounttransferCompletedSummary" />

	<ffi:setGridURL grid="GRID_transferCompletedSummary" name="ViewURL" url="/cb/pages/jsp/transfers/viewTransferAction.action?transferID={0}&transferType={1}&recTransferID={2}" parm0="ID" parm1="TransferType" parm2="RecTransferID" />
	<ffi:setGridURL grid="GRID_transferCompletedSummary" name="InquireURL" url="/cb/pages/jsp/transfers/sendTransferInquiryAction_initTransferInquiry.action?transferID={0}&transferType={1}&recTransferID={2}" parm0="ID" parm1="TransferType" parm2="RecTransferID" />
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/transfers/getTransfersAction_getCompletedTransfers.action?collectionName=CompletedTransfers&GridURLs=GRID_transferCompletedSummary" URLEncrypt="true"/>
    <s:url id="completedTransfersUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="completedTransfersGrid"  
		caption=""  
		dataType="local"  
		href="%{completedTransfersUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		sortable="true"
		sortname="date"
		sortorder="desc"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,completedTransfersGridCompleteEvents"> 
		
		<sjg:gridColumn name="date" index="date" title="%{getText('jsp.default_137')}" sortable="true" width="55"/>
		<sjg:gridColumn name="type" index="type" title="%{getText('jsp.default_444')}" formatter="ns.transfer.customTypeColumn" sortable="true" width="90"/>
		
		<sjg:gridColumn name="combinedAccount" index="Accounts" title="%{getText('jsp.transfers_108')}" sortable="false" width="250" formatter="ns.transfer.formatCombinedAccount"/>
		<ffi:cinclude value1="${UserType}" value2="Corporate" operator="equals">
			<sjg:gridColumn name="fromAccount.accountDisplayText" index="fromAccountNickName" title="%{getText('jsp.transfers_46')}" sortable="true" width="150"  hidden="true" hidedlg="false"/>
			<sjg:gridColumn name="toAccount.accountDisplayText" index="toAccountNickName" title="%{getText('jsp.transfers_77')}" sortable="true" width="150"  hidden="true" hidedlg="false"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${UserType}" value2="Consumer" operator="equals">
			<sjg:gridColumn name="fromAccount.accountDisplayText" index="consumerFromAccount" title="%{getText('jsp.transfers_46')}" sortable="true" width="150"  hidden="true" hidedlg="false"/>
			<sjg:gridColumn name="toAccount.accountDisplayText" index="consumerToAccount" title="%{getText('jsp.transfers_77')}" sortable="true" width="150"  hidden="true" hidedlg="false"/>
		</ffi:cinclude>
		
		<sjg:gridColumn name="map.Frequency" index="mapFrequencyValue" title="%{getText('jsp.default_215')}" formatter="ns.transfer.formatTransferFrequencyColumn" sortable="true" width="70"/>
		<sjg:gridColumn name="statusName" index="statusName" title="%{getText('jsp.default_388')}" sortable="true" width="60"/>
		
			<sjg:gridColumn name="combinedAmount" index="amount" title="%{getText('jsp.transfers_text_amount')}" sortable="true" width="120" formatter="ns.transfer.formatCombinedAmount"/>
		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_489')}" formatter="ns.transfer.formatFromAmountColumn" sortable="true" width="80" align="right"  hidden="true" hidedlg="false"/>
		<sjg:gridColumn name="toAmountValue.currencyStringNoSymbol" index="toAmountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_512')}" formatter="ns.transfer.formatToAmountColumn" sortable="true" width="60" align="right"  hidden="true" hidedlg="false"/>
		<sjg:gridColumn name="isAmountEstimated" index="isAmountEstimated" title="" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="isToAmountEstimated" index="isToAmountEstimated" title="" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="memo" index="memo" title="Memo" sortable="false" width="120" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.transfer.formatCompletedTransfersActionLinks" width="90" search="false" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/> 
		
		<sjg:gridColumn name="trackingID" index="trackingID" title="" width="80" hidden="true" hidedlg="true"/>	
		<sjg:gridColumn name="fromAccount.displayText" index="fromAccountDisplayText" title="" width="80" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="fromAccount.currencyCode" index="fromAccountCurrencyCode" title="" width="80" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="amount" index="amount" title="" sortable="true" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="toAccount.displayText" index="toAccountDisplayText" title="" width="80" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="toAccount.currencyCode" index="toAccountCurrencyCode" title="" width="80" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="transferDestination" index="transferDestination" title="" width="80" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="transferType" index="transferType" title="" width="80" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="frequency" index="frequency" title="" hidden="true" hidedlg="true"/>		
		
		<sjg:gridColumn name="map.InquireURL" index="InquireURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		
	</sjg:grid>
	
	<script>
		$("#completedTransfersGrid").jqGrid('setColProp','ID',{title:false});
	</script>
	
