<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	String templateEdit = request.getParameter("templateEdit");
	session.setAttribute("templateEdit", templateEdit);
	String collectionName = request.getParameter("SetTransfer.Name");
	String transferID = request.getParameter("SetTransfer.ID");
    String transactionIndex = request.getParameter("SetTransfer.TransactionIndex");
%>
<ffi:cinclude value1="${templateEdit}" value2="true">

	<ffi:setProperty name="SetFundsTran" property="BeanSessionName" value="Transfer"/>
	<ffi:setProperty name="SetFundsTran" property="Name" value="SingleTransferTemplateList"/>
	<ffi:setProperty name="SetFundsTran" property="ID" value="<%= transferID%>" />
	<ffi:process name="SetFundsTran"/>
</ffi:cinclude>

<ffi:cinclude value1="${templateEdit}" value2="true" operator="notEquals">
	<ffi:setProperty name="SetTransfer" property="Name" value="<%= collectionName%>"/>
	<ffi:setProperty name="SetTransfer" property="ID" value="<%= transferID%>"/>
    <ffi:setProperty name="SetTransfer" property="TransactionIndex" value="<%= transactionIndex%>"/>
	<ffi:process name="SetTransfer"/>
</ffi:cinclude>



