<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<s:if test="%{#request.transfer.templateName!= null && #request.transfer.templateName!=''}">
 	<ffi:help id="payments_transferTemplateDetailsView" />
</s:if>
<s:else>
	<ffi:help id="payments_accounttransferview" />	
</s:else>

<s:include value="inc/include-view-transaction-details-constants.jsp" />
<s:include value="inc/include-view-accounttransfer-details.jsp" />

<div class="ui-widget-header customDialogFooter">
	<s:if test="%{#request.transfer.templateName== null || #request.transfer.templateName==''}">
		<sj:a id="printTransactionDetail"	button="true" 
					onClick="ns.common.printPopup();" cssClass="hiddenWhilePrinting"><s:text name="jsp.default_331"/></sj:a>
	</s:if>
	<s:if test="%{#frequency!=null}">
	<s:if test="%{!#attr.IsRecModel.equals('true') && (#request.transfer.templateName== null || #request.transfer.templateName=='')}">
	<sj:a 
			id="viewModelLink"
			button="true"
			onClick="viewModel()"
	><s:text name="jsp.default.view_orig_trans"/></sj:a>
	</s:if> 
	</s:if> 
	<sj:a id="transferViewDoneBtn" button="true"
		title="%{getText('jsp.default_102.1')}" onClickTopics="closeDialog" cssClass="hiddenWhilePrinting">
		<s:text name="jsp.default_175" />
	</sj:a>
	
</div>


<script type="text/javascript">

function viewModel() {
	var urlString = "<ffi:urlEncrypt url='/cb/pages/jsp/transfers/viewTransferAction.action?IsRecModel=true&transferType=Recurring&recTransferID=${transfer.RecTransferID}&IsVirtualInstance=${transfer.IsVirtualInstance}'/>";

	$.ajax({
		url: urlString,
		success: function(data){
			ns.common.closeDialog();
			$('#viewTransferDetailsDialogID').html(data).dialog('open');
		}
	});
}

</script>