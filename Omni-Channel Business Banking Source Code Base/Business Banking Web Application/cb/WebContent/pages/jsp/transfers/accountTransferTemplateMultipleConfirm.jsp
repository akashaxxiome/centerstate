<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.ffusion.tasks.banking.Task"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.beans.banking.TransferBatch"%>
<%@ page import="com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<s:if test="%{#request.flowType=='NewTransfer'}">
	<s:set var="transferBatch" value="#session.transferTemplateBatch" />
	<s:set var="strutsAction"
		value="%{ 'AddTransferTemplateBatchAction_execute'}" />
</s:if>
<s:else>
	<s:set var="transferBatch" value="#session.editTransferTemplateBatch" />
	<s:set var="strutsAction"
		value="%{ 'EditTransferTemplateBatchAction_execute'}" />
</s:else>

<div class="formPanel">
		<ffi:help id="payments_transferTemplateEditVerify" />
		<s:set var="hasMultiCurrency"
			value="%{''+#transferBatch.hasMultiCurrency}" />
		<s:set var="batchTotalZero" value="#transferBatch.amountValue.isZero" />
<s:form id="MultipleTransferNewVerify" namespace="/pages/jsp/transfers" validate="false" action="%{#strutsAction}" method="post" name="MultipleTransferNewVerify" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
<input type="Hidden" name="OpenEnded" value="false">
<input type="Hidden" name="AddTransfer.ClearAccounts" value="">
<div class="leftPaneWrapper" role="form" aria-labelledby="templateSummary">
	<div id="addTransfer_transferTemplateName" class="templatePane leftPaneInnerWrapper leftPaneLoadPanel">
		<div class="header"><h2 id="templateSummary"><s:text name="jsp.default.templates.summary" /></h2></div>
			<div class="summaryBlock"  style="padding: 15px 10px;">
				<div style="width:100%" class="floatleft">
					<span id="verifyTransfer_templateLabelId" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_416" />:</span>
					<span id="verifyTransfer_templateValueId" class="inlineSection floatleft labelValue"><s:property value="%{#transferBatch.templateName}" /></span>
				</div>
			</div>
	</div>
	<%--Transfer Batch Total + Buttons Section --%>
	<s:if test="%{!#batchTotalZero}">
		<div id="TotalSection" class="leftPaneInnerWrapper">
			<span id="TotalLabel">
				<s:text name="jsp.default_431" />:
			</span>
			<span id="Total"> 
				<s:property value="#transferBatch.amountValue.currencyStringNoSymbol" />
				<s:property value="#transferBatch.amountValue.currencyCode" />
			</span>
		</div>
	</s:if>
</div>

<div class="confirmPageDetails">
<s:set var="rowCount" value="0" />
			<s:iterator var="transfer" value="#transferBatch.transfers">
			<div class="blockWrapper">
				<s:if test="%{#transfer.fromAccount!=null}">	
					<h3 class="transactionHeading">Transfer<s:property value="#rowCount+1"/></h3>
				
				<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
						<div class="blockContent">
							<div class="blockRow">
								<div class="inlineBlock"  style="width: 50%">
									<span id="verifyMultipleTransfer_fromAccountLabelId" class="sectionLabel"><s:text name="jsp.default_217" />: </span>
									<span id="verifyMultipleTransfer_fromAccountValueId<s:property value='#rowCount'/>">
										<s:if test="%{#transfer.fromAccount!=null}">
													<s:property value="#transfer.fromAccount.accountDisplayText"/>
										</s:if>
									</span>
								</div>
								<div class="inlineBlock">
									<s:if test="%{#hasMultiCurrency=='false'}">
										<span id="verifyMultipleTransfer_amountLabelId" class="sectionLabel"><s:text name="jsp.default_43" />: </span>
									</s:if>
									<s:if test="%{#hasMultiCurrency=='true'}">
										<span id="verifyMultipleTransfer_fromAmountLabelId" class="sectionLabel"><s:text name="jsp.default_489" />: </span>
									</s:if>
									<s:if test="%{#hasMultiCurrency=='false'}">
										<span id="verifyMultipleTransfer_amountValueId<s:property value='#rowCount'/>">
											<s:property value="#transfer.amountValue.currencyStringNoSymbol" /> 
											<s:property value="#transfer.amountValue.currencyCode" />
										</span>
									</s:if>
									<s:if test="%{#hasMultiCurrency=='true'}">
										<span id="verifyMultipleTransfer_fromAmountValueId<s:property value='#rowCount'/>">
											<s:if test="%{#transfer.isAmountEstimated}">&#8776;
                                      		</s:if> 
                                      		<s:if test="%{#transfer.amountValue.currencyStringNoSymbol=='0.00'}">--</s:if>
											<s:else>
												<s:property value="#transfer.amountValue.currencyStringNoSymbol" />
												<s:property value="#transfer.amountValue.currencyCode" />&nbsp;&nbsp;&nbsp;&nbsp;
                                       		</s:else>
                                       </span>
									</s:if>
								</div>
							</div>
							<div class="blockRow">
								<div class="inlineBlock"  style="width: 50%">
									<span id="verifyMultipleTransfer_toAccountLabelId" class="sectionLabel"><s:text name="jsp.default_424" />: </span>
									<span id="verifyMultipleTransfer_toAccountValueId<s:property value='#rowCount'/>">
										<s:if test="%{#transfer.toAccount!=null}">
													<s:property value="#transfer.toAccount.accountDisplayText"/>
										</s:if>
									</span>
								</div>
								<div class="inlineBlock">
									<s:if test="%{#hasMultiCurrency=='true'}">
										<span id="verifyMultipleTransfer_toAmountLabelId" class="sectionLabel"><s:text name="jsp.default_512" />: </span>
									</s:if>
									<s:if test="%{#hasMultiCurrency=='true'}">
										<span id="verifyMultipleTransfer_toAmountValueId<s:property value='#rowCount'/>">
											<s:if test="%{#transfer.isToAmountEstimated}">&#8776;
                                      		</s:if> 
                                      		<s:if test="%{#transfer.toAmountValue.currencyStringNoSymbol=='0.00'}">--</s:if>
											<s:else>
												<s:property value="#transfer.toAmountValue.currencyStringNoSymbol" />
												<s:property value="#transfer.toAmountValue.currencyCode" />&nbsp;&nbsp;&nbsp;&nbsp;
                                       		</s:else>
                                       	</span>
									</s:if>
								</div>
							</div>
						
							<s:if test="%{memo!=''}">
								<div class="blockRow">
									<s:if test="%{#hasMultiCurrency=='false'}">
										<span id="verifyMultipleTransfer_memoLabelId" class="sectionLabel"><s:text name="jsp.default_279" />: </span>
									</s:if>
									<s:if test="%{#hasMultiCurrency=='true'}">
										<span id="verifyMultipleTransfer_memoLabelId" class="sectionLabel"><s:text name="jsp.default_279" />: </span>
									</s:if>
									<span id="verifyMultipleTransfer_memoValueId<s:property value='%{#rowCount-1}'/>"><s:property value="#transfer.memo" /></span>
								</div>
							</s:if>	
							<s:set var="Transfer" value="#transfer" scope="session"></s:set>
							<s:if test="%{#request.showCategoryColumn=='true'}">
								<div class="blockRow">
									<span class="sectionLabel"><s:text name="jsp.default_88" />: </span>
									<%-- <!-- ====================== BEGIN REGISTER INTEGRATION ===================== -->--%>
									<ffi:cinclude value1="${Transfer.REGISTER_CATEGORY_ID}"
										value2="0" operator="notEquals">
										<span class="columndata ltrow2_color">
											<ffi:setProperty name="RegisterCategories" property="Current" value="${Transfer.REGISTER_CATEGORY_ID}" /> 
											<ffi:setProperty name="Parentfalse" value="" /> 
											<ffi:setProperty name="Parenttrue" value=": " /> 
											<ffi:getProperty name="RegisterCategories" property="ParentName" /> 
											<ffi:getProperty name="Parent${RegisterCategories.HasParent}" /> 
											<ffi:getProperty name="RegisterCategories" property="Name" /></span>
									</ffi:cinclude>
									<%-- <!-- ====================== END REGISTER INTEGRATION ===================== -->--%>
									</tr>
								</div>
							</s:if>
						</div>
					</s:if>	
				<s:set var="rowCount" value="%{#rowCount + 1}" />
			</div>
			</s:iterator>
<s:if test="%{#hasMultiCurrency=='true'}">
<div class="estimatedAmountMessage">
	<span id="verifyMultipleTransfer_messageIndicator" class="required" colspan="7" align="center">&#8776; <s:text name="jsp.default_241" /></span>
</div>
</s:if>
<div class="btn-row">
 <sj:a id="cancelFormButtonOnVerify" button="true" summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelMultipleTransferTemplateNewForm"><s:text name="jsp.default_82" /></sj:a> 
 <s:if test="%{#request.flowType=='NewTransfer'}">
		<s:url action="AddTransferTemplateBatchAction_init"
			escapeAmp="false" namespace="/pages/jsp/transfers"
			id="intializeTaskUrl"></s:url>
	</s:if> <s:else>
		<s:url action="EditTransferTemplateBatchAction_init"
			escapeAmp="false" namespace="/pages/jsp/transfers"
			id="intializeTaskUrl">
			<s:param name="transferBatchID"><s:property value="#transferBatch.iD"/></s:param>
			</s:url>
	</s:else> <ffi:cinclude value1="${TransferFileUpload}" value2="true"
		operator="equals">
		<sj:a id="backFormButton" button="true"
			onClickTopics="backToInput,hideOperationResult">
			<s:text name="jsp.default_57" />
		</sj:a>
	</ffi:cinclude> <ffi:cinclude value1="${TransferFileUpload}" value2="true"
		operator="notEquals">
		<s:hidden id="hiddenInitTaskUrl" value="%{intializeTaskUrl}"></s:hidden>
		<sj:a id="backFormButton" button="true"
			onClickTopics="initializeBatchTask">
			<s:text name="jsp.default_57" />
		</sj:a>
	</ffi:cinclude> <sj:a id="saveMultipleTemplateSubmit"
		formIds="MultipleTransferNewVerify" targets="confirmDiv"
		button="true" onBeforeTopics="beforeSendTransferTemplate"
		onSuccessTopics="sendTransferTemplateSuccess"
		onErrorTopics="errorSendTransferTemplate"
		onCompleteTopics="sendTransferTemplateComplete">
		<s:text name="jsp.default_367" />
	</sj:a>
</div></div>
</s:form>
</div>