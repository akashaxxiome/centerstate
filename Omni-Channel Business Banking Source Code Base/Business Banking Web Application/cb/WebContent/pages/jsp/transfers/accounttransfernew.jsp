<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page contentType="text/html; charset=UTF-8" import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%--<%@ page import="com.ffusion.beans.accounts.Accounts"%>--%>


<%
session.setAttribute("LoadTemplateFromTransfer", request.getParameter("LoadTemplateFromTransfer"));
session.setAttribute("LoadFromTransferTemplate", request.getParameter("LoadFromTransferTemplate"));

%>
<style type="text/css">
.formPane, .templatePane{width:100%; float:left; padding:0}
</style>
	 <s:if test="%{#request.istemplateflow!='true'}">     
        <ffi:cinclude ifEntitled="<%=EntitlementsDefines.ENT_TRANSFERS_TEMPLATE%>">
        	<div id="templateForSingleTransfer" class="templatePane leftPane" role="form" aria-labelledby="loadTemplate">
        	<div class="header"><h2 id="loadTemplate"><s:text name="jsp.default_264" /></h2></div>
				<div id="templateLoadID" class="leftPaneInnerBox">
					 <s:url id="ajax" value="accounttransfertempload.jsp">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					</s:url>
					<s:include value="%{ajax}" />
				</div>
			</div>
		</ffi:cinclude>
	 </s:if>


    <div id="singleTransferPanel" class="formPanel formLeftMargin">
        
        <s:if test="%{#request.istemplateflow=='true'}">
        <span class="shortcutPathClass" style="display: none;">pmtTran_transfer,addSingleTemplateLink</span>
        <span class="shortcutEntitlementClass" style="display: none;">Transfers Template</span>
       	<div id="singleTransferForm">
	         <s:url id="ajax" value="accountTransferTemplateForm.jsp">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	   		</s:url>
   		
        </s:if>
        <s:else>
        <span class="shortcutPathClass" style="display: none;">pmtTran_transfer,addSingleTransferLink</span>
        <span class="shortcutEntitlementClass" style="display: none;">Transfers</span>
        
        <div id="singleTransferForm" class="rightPaneWrapper rightPaneConfirmDvc">
	        <s:url id="ajax" value="accounttransferform.jsp">
			    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		    </s:url>
		 
        </s:else>
		<s:include value="%{ajax}" />
        </div><!-- singleTransferForm -->
       
      
    </div><!-- singleTransferPanel -->