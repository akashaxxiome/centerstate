<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="${EditTransfer}" value2="" operator="notEquals" >
    <ffi:setProperty name="EditTransfer" property="Process" value="true"/>
    <ffi:process name="EditTransfer"/>
</ffi:cinclude>

<ffi:cinclude value1="${AddTransfer}" value2="" operator="notEquals" >
    <ffi:setProperty name="AddTransfer" property="Process" value="true"/>
    <ffi:process name="AddTransfer"/>
</ffi:cinclude>

<%-- Clear paging session ID so that BPW will refresh the list and display the added transfer template on the summary page --%>
<ffi:setProperty name="GetAllTransferTemplates" property="PagingContext.SessionId" value=""/>


