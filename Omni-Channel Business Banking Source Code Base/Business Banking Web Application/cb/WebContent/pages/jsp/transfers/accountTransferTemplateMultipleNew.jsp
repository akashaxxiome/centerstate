<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page
	import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.beans.banking.TransferBatch"%>
<%@ page import="com.ffusion.beans.banking.Transfers"%>

<%@ page contentType="text/html; charset=UTF-8"
	import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<ffi:help id="payments_newmultipletemp" />
	

<% boolean canImport = false; %>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
 		<% canImport = true; %>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS_SWIFT_FILE_UPLOAD %>">
			 <% canImport = true; %>
</ffi:cinclude>

<%-- if Logged in user is consumer then dont show file import --%>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			 <% canImport = false; %>
</ffi:cinclude>

<script>
		$.subscribe('onSubmitCheckTopics', function(event,data) {
			onSubmitCheck(false);
		});
</script>

<span id="PageHeading" style="display: none;"><h1 id="multipleTransfersTemplateTitle" class="portlet-title"><s:text
		name="jsp.transfers_15" /></h1></span> <span class="shortcutPathClass"
	style="display: none;">pmtTran_transfer,addMultipleTemplateLink</span>
<span class="shortcutEntitlementClass" style="display: none;">Transfers</span>




<ffi:help id="payments_newmultipletemp" />
<s:include
	value="%{#session.PagesPath}/common/commonUploadIframeCheck_js.jsp" />
<s:if test="flowType=='NewTransfer'">
	<s:set var="transferBatch" value="#session.transferTemplateBatch" />
</s:if>
<script>
	var transferSize = <s:property value="#transferBatch.transferCount" />;
	if(transferSize < 2) transferSize = 2;
</script>
<%
	String sizeByRequest = com.ffusion.util.HTMLUtil.encode(request.getParameter("transferSize"));
	if(sizeByRequest!=null){
%>
<script>
		transferSize = <%= sizeByRequest %>;
	</script>
<%}%>
<script>
	for(var i = 2; i < transferSize; i++){
		$(".columndataauto" + i).show();
	}
</script>
<!-- Check AppType of user -->
<ffi:setProperty name="IsConsumerUser" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>

<s:include value="/pages/jsp/inc/trans-from-to-js.jsp" />
<style type="text/css">
.columndataauto0 {
	padding: 3px
}

.columndataauto1 {
	padding: 3px
}

.columndataauto2 {
	padding: 3px;
	display: none
}

.columndataauto3 {
	padding: 3px;
	display: none
}

.columndataauto4 {
	padding: 3px;
	display: none;
}
</style>
<s:form id="TransferMultipleNew" namespace="/pages/jsp/transfers" validate="false" action="AddTransferTemplateBatchAction_verify" method="post" name="TransferMultipleNew" theme="simple">
<div class="leftPaneWrapper" role="form" aria-labelledby="saveTemplate">
	<div id="addTransfer_transferTemplateName" class="templatePane leftPaneInnerWrapper leftPaneLoadPanel">
		<div class="header"><h2 id="saveTemplate"><s:text name="jsp.default_371" /></h2></div>
			<div id="" class="leftPaneInnerBox">
				<span style="display:block" class="marginBottom10"><label for="templateName"><s:text name="jsp.default_416" /></label> <span class="required">*</span></span>
				<s:textfield name="transferTemplateBatch.templateName" id="templateName" size="32" maxlength="32" value="%{#session.transferTemplateBatch.templateName}" cssClass="ui-widget-content ui-corner-all" aria-labelledby="templateName" aria-required="true" />
				<div class="marginTop10"><span id="templateNameError"></span></div>
			</div>
			
	</div>
	
	<% if (canImport) { %>
	<div id="" class="templatePane leftPaneInnerWrapper" role="form" aria-labelledby="fileImport">
	<div class="header"><h2 id="fileImport"><s:text name="jsp.default_539" /></h2></div>
		<div class="leftPaneInnerBox" align="center">
			<sj:a name="fileImportBtn" id="fileImportBtn" onclick="ns.common.setModuleType('transferTemplate',function(){$('#transferFileImportLink').trigger('click');})" button="true" cssStyle="margin-left:10px; margin-bottom: 10px;">
				<s:text name="jsp.default_539.1" />
			</sj:a>
		</div>
	
	</div>
	<% } %>			

	<%--Transfer Batch Total + Buttons Section --%>
	<div id="TotalSection" class="leftPaneInnerWrapper" role="section" aria-labelledby="Total">
		<span id="TotalTitle" align="right">
			<label for="Total"><s:text name="jsp.default_431" /></label>:
		</span>
		<span id="TotalValue">--------</span>
	</div>
</div>

<div id="multipleTransferPanel" class="rightPaneWrapper w71" align="center">
<div class="paneWrapper" role="form" aria-labelledby="transferDetails">
  	<div class="paneInnerWrapper">
   		<div class="header"><h2 id="transferDetails"><s:text name="jsp.transfer.details.label" /></h2></div>
<div id="MultipleTransferForm">
<ul id="formerrors"><li class="errorLabel"></li></ul>
<s:set name="isTemplate" value="true" scope="request"/>
<s:include value="/pages/jsp/transfers/inc/accounttransfermultiplenewform-js.jsp" />
<div class="paneContentWrapper tempMultiCls">
	<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData">

		<tr>
			<td colspan="3" align="center" style="padding:0"><span id="formerrors"></span></td>
		</tr>
	<s:set var="count" value="0" />
	
	<s:set var="transfers" value="#transferBatch.transfers" />
	<s:iterator value="#transfers" var="transfer">
		<tr  class="columndataauto<s:property value="#count" />" >
			<td colspan="3"><h3 class="transactionHeading" id="templateId">Template <s:property value="#count+1"/></h3></td>
		</tr>
		<tr>
			<td  colspan="3"  align="center"><span id="transfers_<s:property value="#count" />_Error"></span></td>
		</tr>
		<tr class="columndataauto<s:property value='#categoryCount' />">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="columndataauto<s:property value="#count" />" >
			<td id="addMultipleTransfer_fromAccountLabelId"	class="sectionsubhead ltrow2_color" align="left" width="45%">
				<label for="fromAccount"><s:text name="jsp.default_217" /></label> <span class="required" title="required">*</span>
			</td>
			<td class="sectionsubhead ltrow2_color" width="10%"></td>
			<td id="addMultipleTransfer_toAccountLabelId" class="sectionsubhead ltrow2_color" align="left" width="45%">
				<label for="toAccount"><s:text name="jsp.default_424" /></label> <span class="required">*</span>
			</td>
		</tr>
<input type="Hidden" name="transferTemplateBatch.transfers[<s:property value="#count" />].transferDestination" id="TransferDest<s:property value="#count" />" value="<s:property value='#transfer.transferDestination'/>">
			<tr>
				<td class="columndataauto<s:property value="#count" />" align="left" nowrap>
					<select class="txtbox" style="width: 30%" name="transferTemplateBatch.transfers[<s:property value="#count" />].fromAccountID" id="FromAccount<s:property value="#count" />"
						onChange="onAccountChangeCheck('from',<s:property value="#count" />);" aria-labelledby="addMultipleTransfer_fromAccountLabelId"  aria-required="true">
						<s:if test="%{#transfer.fromAccountID!=null}">
							<option value="<s:property value='%{#transfer.fromAccountID}' />:<s:property value="#transfer.fromAccount.currencyCode" />"
								selected><s:property value="#transfer.fromAccount.accountDisplayText" />
							</option>
						</s:if>
					</select>
				</td>
				<td class="columndataauto<s:property value="#count" />" width="10%">&nbsp;</td>
				<td class="columndataauto<s:property value="#count" />" align="left" nowrap>
					<select class="txtbox" style="width: 30%" name="transferTemplateBatch.transfers[<s:property value="#count" />].toAccountID"
					id="ToAccount<s:property value="#count" />" onChange="onAccountChangeCheck('to',<s:property value="#count" />);" aria-labelledby="addMultipleTransfer_toAccountLabelId" aria-required="true">
						<s:if test="%{#transfer.toAccountID!=null}">
							<option
								value="<s:property value="%{#transfer.ToAccountID}" />:<s:property value="#transfer.toAccount.currencyCode" />"
								selected><s:property value="#transfer.toAccount.accountDisplayText" />
							</option>
						</s:if>
					</select>
				</td>
			</tr>
			<tr class="columndataauto<s:property value="#count" />" >
				<td><span id="transfers_<s:property value="#count" />_fromAccountIDError"></span></td>
				<td width="10%">&nbsp;</td>
				<td><span id="transfers_<s:property value="#count" />_toAccountIDError"></span></td>
			</tr>
			<tr class="columndataauto<s:property value="#count" />" >
				<td id="addMultipleTransfer_amountLabelId" class="sectionsubhead ltrow2_color" align="left">
					<span style="display:inline-block; width:125px;"><label for="Amount"><s:text name="jsp.default_43" /></label></span>
					<span id="CurrencyColumnHeader<s:property value="#count" />" style="display: none">
					<label for="curreuncy"><s:text name="jsp.default_125" /></label> <span class="required">*</span></span>
				</td>
				<td class="sectionsubhead ltrow2_color" width="10%"></td>
				<td id="addMultipleTransfer_memoLabelId" class="columndata ltrow2_color" class="columndataauto<s:property value="#count" />">
					<label for="memo"><s:text name="jsp.default_279" /></label>
				</td>
			</tr>
			
			<tr>	
				<td class="columndataauto<s:property value="#count" />" align="left" nowrap><input class="txtbox ui-widget-content ui-corner-all" 
				name="transferTemplateBatch.transfers[<s:property value="#count" />].amount" size="12" maxlength="15" class="form_fields" value="<s:if test="%{!#transfer.userAssignedAmountValue.isZero}"><s:property value='#transfer.userAssignedAmount'/></s:if>" id="Amount<s:property value="#count" />"
					onChange="calculateTotal();" onBlur="calculateTotal();"
					onkeyup="calculateTotal();" aria-labelledby="addMultipleTransfer_amountLabelId">
					<span
					id="CurrencyLabel<s:property value="#count" />"
					style="display: none">&nbsp;</span>&nbsp; <span
					id="CurrencySection<s:property value="#count" />"
					style="display: none"> <select
						class="ui-widget-content ui-corner-all"
						name="transferTemplateBatch.transfers[<s:property value="#count" />].userAssignedAmountFlagName"
						id="CurrencySelect<s:property value="#count" />"
						onChange="calculateTotal();" >
							<option value="single" selected><s:text
									name="jsp.transfers_70" /></option>
					</select> <input type="hidden"
						name="transferTemplateBatch.transfers[<s:property value="#count" />].toAmount"
						id="OtherAmount<s:property value="#count" />" value="0.00">
					</span> &nbsp;&nbsp;
					</td>
				<td class="columndataauto<s:property value="#count" />" align="left" nowrap></td>
				<td id="addMultipleTransfer_memoLabelId" class="columndataauto<s:property value="#count" />">
					<input class="txtbox ui-widget-content ui-corner-all" name="transferTemplateBatch.transfers[<s:property value="#count" />].memo" id="Memo<s:property value="#count" />" size="35" maxlength="100" class="form_fields"
					value="<s:property value="#transfer.memo"/>" aria-labelledby="addMultipleTransfer_memoLabelId">&nbsp;&nbsp;&nbsp;&nbsp;
					<span id="Category<s:property value="#count" />" style="display: none"> Category: 
					<select
									class="ui-widget-content ui-corner-all"
									name="transferTemplateBatch.transfers[<s:property value="#count" />].transferCategory"
									id="CategorySelect<s:property value="#count" />"
									class="form_elements">
										<option value="0">Unassigned</option>
										<ffi:setProperty name="RegisterCategories"
											property="Filter" value="PARENT_CATEGORY=-1,ID!0,AND" />
										<ffi:setProperty name="RegisterCategories"
											property="FilterSortedBy" value="NAME" />
										<ffi:list collection="RegisterCategories" items="Category">
											<option
												value="<ffi:getProperty name='Category' property='Id'/>"
												<ffi:cinclude value1="${Transfer.REGISTER_CATEGORY_ID}" value2="${Category.Id}">selected</ffi:cinclude>><ffi:getProperty
													name="Category" property="Name" /></option>
											<ffi:setProperty name="RegisterCategoriesCopy"
												property="Filter" value="PARENT_CATEGORY=${Category.Id}" />
											<ffi:setProperty name="RegisterCategoriesCopy"
												property="FilterSortedBy" value="NAME" />
											<ffi:list collection="RegisterCategoriesCopy"
												items="Category1">
												<ffi:setProperty name="Compare" property="Value2"
													value="${Category1.Id}" />
												<option
													value="<ffi:getProperty name="Category1" property="Id"/>"
													<ffi:getProperty name="selected${Compare.Equals}"/>>&nbsp;&nbsp;
													<ffi:getProperty name="Category" property="Name" />:
													<ffi:getProperty name="Category1" property="Name" /></option>
											</ffi:list>
										</ffi:list>
								</select>
							</span>
				</td>
			</tr>
			
			<tr class="columndataauto<s:property value="#count" />" >
				<td>
					<span id="transfers_<s:property value="#count" />_amountError"></span>
					<span id="transfers_<s:property value="#count" />_toAmountError"></span>
					<span id="transfers_<s:property value="#count" />_userAssignedAmountError"></span> 
				</td>
				<td width="10%">&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" class="columndataauto<s:property value="#count" />" align="right">

				<sj:a id="clearRow%{#count}" button="true" title="%{getText('Clear')}" onClick="return clearRow('%{#count}');">
						<s:text name="Clear" />
				</sj:a>
				</td>	
			</tr>
			
			<s:set var="count" value="%{#count+1}" />
		</s:iterator>
		<%--Reset TransferAccount collection. Remove all filters --%>
		<ffi:setProperty name="TransferAccounts" property="Filter" value="All" />
		<tr>
			<td colspan="3" align="right">
			<hr style="border:1px solid #b0b0b0; margin:10px 0" width="100%" height="1">

			<div class="deleteRow" style="display: inline-block;"><sj:a id="deleteRowID"
					button="true">
					<s:text name="jsp.transfers_6" />
				</sj:a></div>
			<div class="addRow"  style="display: inline-block;" align="left"><sj:a id="addRowID"
					button="true">
					<s:text name="jsp.transfers_3" />
				</sj:a></div>
			</td>
		</tr>
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		<tr class="ltrow_color">
			<td class="columndata ltrow2_color" align="center" colspan="7">
				<sj:a id="cancelMultipleTransferSubmit" button="true" summaryDivId="summary" buttonType="cancel"
					onClickTopics="showSummary,cancelMultipleTransferTemplateNewForm">
					<s:text name="jsp.default_82" />
				</sj:a> <sj:a id="verifyTransferSubmit1" formIds="TransferMultipleNew"
					targets="verifyDiv" button="true" validate="true"
					validateFunction="customValidation"
					onBeforeTopics="beforeVerifyTransferForm"
					onCompleteTopics="verifyTransferFormComplete"
					onClickTopics="onSubmitCheckTopics"
					onErrorTopics="errorVerifyTransferForm"
					onSuccessTopics="successVerifyTransferForm">
					<s:text name="jsp.default_395" />
				</sj:a>
			</td>
		</tr>
</table></div>
</div></div>
<ffi:removeProperty name="fromPortalPage" />
<ffi:removeProperty name="IsConsumerUser" />

	</div>
	<!-- MultipleTransferForm -->
</div>
<!-- MultipleTransferPanel -->
</s:form>
