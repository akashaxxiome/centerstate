<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.beans.banking.TransferStatus"%>
<%@ page import="java.lang.Integer"%>
<%@ page import="com.ffusion.beans.banking.TransferDefines"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%--Set Variables to be used on JSP --%>
<s:set var="transferBatch" value="#session.transferBatch" />
<s:set var="hasMultiCurrency"
	value="%{''+#transferBatch.hasMultiCurrency}" />
<s:set var="batchTotalZero" value="#transferBatch.amountValue.isZero" />

<ffi:help id="payments_accounttransfermultipleconfirm" />

<div class="leftPaneWrapper" role="form" aria-labelledby="saveTemplate">
	<ffi:cinclude ifEntitled="<%=EntitlementsDefines.ENT_TRANSFERS_TEMPLATE%>">
		<div id="templateForMultipleTransferVerify" class="templatePane leftPaneInnerWrapper">
			<div class="header"><h2 id="saveTemplate"><s:text name="jsp.default_371" /></h2></div>
			<div style="padding: 15px 10px;">
				<s:include value="accounttransfermultitempnewname.jsp" />
			</div>
			<div id="templateForMultipleTransferVerifyResult"></div>
		</div>
	</ffi:cinclude>
	<%--Transfer Batch Total + Buttons Section --%>
	
	<%--Transfer Batch Total Section  --%>
	<s:if test="%{!#batchTotalZero}">
		<div class="totalAmountWrapper leftPaneInnerWrapper">
				<div id="confirmMultipleTransfer_totalLabel" class="inlineSection">
					<span id="TotalLabel">
						<s:text name="jsp.default_431" /> :
					</span>
				</div>
				<div id="confirmMultipleTransfer_totalValue" class="inlineSection">
					<span id="Total"> 
						<s:property value="#transferBatch.amountValue.currencyStringNoSymbol" /> 
						<s:property value="#transferBatch.amountValue.currencyCode" />
					</span>
				</div>
		</div>
	</s:if>
</div>

<div class="confirmPageDetails">
	<s:set var="rowCount" value="0" />
	<s:iterator var="transfer" value="#transferBatch.transfers">
		<s:if test="%{#transfer.fromAccount!=null}">
			<s:if test="%{#transfer.statusName!='Cancelled'}">	
			<h3 class="transactionHeading">Transfer <s:property value="#rowCount+1"/>
					&#91;
						<s:if test="%{#hasMultiCurrency=='false'}">
							<span class="confirmMultipleTransfer_referenceLabelId" style="font-weight:bold;"><s:text name="jsp.default_347" /> :</span>
						</s:if>
						<s:if test="%{#hasMultiCurrency=='true'}">
								<span class="confirmMultipleTransfer_referenceLabelId sectionLabel" style="font-weight:bold;"><s:text name="jsp.default_347" /></span>
						</s:if>
						<%--Transfer Tracking ID Section --%>
						<span id="confirmMultipleTransfer_referenceValueId<s:property value='#rowCount'/>" style="font-weight:bold;"><s:property value="#transfer.TrackingID" /></span>
					&#93;
			</h3>
			<div  class="blockHead"><s:text name="jsp.transaction.status.label" /> :</div>
			<div class="blockContent">
				<s:property value="%{transfer.status}"/>
				<s:set var="TRS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_TO_TRANSFER==#transfer.status}" />
				<s:set var="TRS_INSUFFICIENT_FUNDS" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_INSUFFICIENT_FUNDS==#transfer.status}" />
				<s:set var="TRS_BPW_APPROVAL_FAILED" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BPW_APPROVAL_FAILED==#transfer.status}" />
				<s:set var="TRS_BPW_LIMITCHECK_FAILED" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BPW_LIMITCHECK_FAILED==#transfer.status}" />
				<s:set var="TRS_SCHEDULED" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_SCHEDULED==#transfer.status}" />
				<s:set var="TRS_TRANSFERED"	value="%{@com.ffusion.beans.banking.TransferStatus@TRS_TRANSFERED==#transfer.status}" />
				<s:set var="TRS_PENDING_APPROVAL"	value="%{@com.ffusion.beans.banking.TransferStatus@TRS_PENDING_APPROVAL==#transfer.status}" />
				<s:set var="TRS_BACKEND_FAILED" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BACKEND_FAILED==#transfer.status}" />
				<s:set var="HighlightStatus" value="%{'false'}" />
				<s:if test="#TRS_FAILED_TO_TRANSFER">
					<s:set var="HighlightStatus" value="%{'true'}" />
				</s:if> 
				<s:if test="#TRS_INSUFFICIENT_FUNDS">
					<s:set var="HighlightStatus" value="%{'true'}" />
				</s:if> 
				<s:if test="#TRS_BPW_LIMITCHECK_FAILED">
					<s:set var="HighlightStatus" value="%{'true'}" />
				</s:if> 
				<s:if test="#TRS_BPW_APPROVAL_FAILED">
					<s:set var="HighlightStatus" value="%{'true'}" />
				</s:if> 
				<s:if test="#TRS_BACKEND_FAILED">
					<s:set var="HighlightStatus" value="%{'true'}" />
				</s:if> 
				
				
				<s:if test="#HighlightStatus=='true'">
					<div class="blockRow failed">
						
						<%--Transfer Status Section --%>
						<span id="confirmMultipleTransfer_statusValueId<s:property value='#rowCount'/>">
							<span class="sapUiIconCls icon-decline"></span> 
							
						<s:if test="%{#transfer.backendErrorDesc=='' || #transfer.backendErrorDesc==null }">
								<s:property	value="%{#transfer.statusName}" />
								</s:if> 
								<s:else>
								<s:property	value="%{#transfer.statusName}" /> - <s:property value="%{#transfer.backendErrorDesc}"/>
								</s:else>
						</span>
						<!--<span id="transferResultMessage" class="floatRight"><s:text name="jsp.transfers_94" /></span>-->
					</div>
				</s:if> 
				<s:else>
					<s:if test="%{#TRS_SCHEDULED || #TRS_PENDING_APPROVAL}">
						<div class="blockRow pending">
							
							<%--Transfer Status Section --%>
							<span id="confirmMultipleTransfer_statusValueId<s:property value='#rowCount'/>">
								<span class="sapUiIconCls icon-future"></span><s:property value="%{#transfer.statusName}" />
							</span>
							<span id="transferResultMessage" class="floatRight"><s:text name="jsp.transfers_94" /></span>
						</div>
					</s:if>
					<s:elseif test="%{#TRS_TRANSFERED}">
						<div class="blockRow completed">
							
							<%--Transfer Status Section --%>
							<span id="confirmMultipleTransfer_statusValueId<s:property value='#rowCount'/>">
							<span class="sapUiIconCls icon-accept"></span>
							 <s:property value="%{#transfer.statusName}" />
							</span>
							<span id="transferResultMessage" class="floatRight"><s:text name="jsp.transfers_94" /></span>
						</div>
					</s:elseif>
				</s:else>
				</div>
				<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
					<div class="blockContent">
						<div class="blockRow">
							<div class="inlineBlock"  style="width: 50%">
								<span class="confirmMultipleTransfer_fromAccountLabelId sectionLabel"><s:text name="jsp.default_217" /> :</span>
								<%--From Account Section --%>
								<span id="confirmMultipleTransfer_fromAccountValueId<s:property value='#rowCount'/>" >
									<s:if test="%{#transfer.fromAccount!=null}">
										<s:property value="#transfer.fromAccount.accountDisplayText"/>
									</s:if>
								</span>
							</div>
							<div class="inlineBlock">
								<s:if test="%{#hasMultiCurrency=='false'}">
									<span class="confirmMultipleTransfer_amountLabelId sectionLabel"><s:text name="jsp.default_43" /> :</span>
								</s:if>
								<s:if test="%{#hasMultiCurrency=='true'}">
									<span class="confirmMultipleTransfer_fromAmountLabelId sectionLabel" ><s:text name="jsp.default_489" /> :</span>
								</s:if>
								<%--Amount  Section (In case of Single Currency Transfer)--%>
								<s:if test="%{#hasMultiCurrency=='false'}">
									<span id="confirmMultipleTransfer_amountValueId<s:property value='#rowCount'/>">
										<s:property value="#transfer.amountValue.currencyStringNoSymbol" /> <s:property value="#transfer.amountValue.currencyCode" />
									</span>
								</s:if>
								<%--Amount  Section (In case of Mutli Currency Transfer)--%>
								<s:if test="%{#hasMultiCurrency=='true'}">
									<%--Transfer Amount Section --%>
									<span id="confirmMultipleTransfer_fromAmountValueId<s:property value='#rowCount'/>">
										<s:if test="%{#transfer.isAmountEstimated}">
	                                        &#8776;
	                                    </s:if> 
	                                    <s:if test="%{#transfer.amountValue.currencyStringNoSymbol=='0.00'}">--</s:if>
										<s:else>
											<s:property value="#transfer.amountValue.currencyStringNoSymbol" />
											<s:property value="#transfer.amountValue.currencyCode" />
	                                     </s:else>
									</span>
									<%--Estimated Amount Section --%>
								</s:if>
							</div>
						</div>
						<div class="blockRow">
							<div class="inlineBlock"  style="width: 50%">
								<span class="confirmMultipleTransfer_toAccountLabelId sectionLabel"><s:text name="jsp.default_424" /> :</span>
								<%--To Account Section --%>
								<span id="confirmMultipleTransfer_toAccountValueId<s:property value='#rowCount'/>">
									<s:if test="%{#transfer.toAccount!=null}">
										<s:property value="#transfer.toAccount.accountDisplayText"/>
									</s:if>
								</span>
							</div>
							<div class="inlineBlock">
								<s:if test="%{#hasMultiCurrency=='true'}">
									<span class="confirmMultipleTransfer_toAmountLabelId sectionLabel" ><s:text name="jsp.default_512" /> :</span>
									<s:if test="%{#hasMultiCurrency=='true'}">
									<%--Transfer Amount Section --%>
									<%--Estimated Amount Section --%>
									<span id="confirmMultipleTransfer_toAmountValueId<s:property value='#rowCount'/>">
										<s:if test="%{#transfer.isToAmountEstimated}">
	                                        &#8776;
	                                    </s:if>
	                                    <s:if test="%{#transfer.toAmountValue.currencyStringNoSymbol=='0.00'}">--</s:if>
										<s:else>
											<s:property value="#transfer.toAmountValue.currencyStringNoSymbol" />
											<s:property value="#transfer.toAmountValue.currencyCode" />
	                                     </s:else>
									</span>
								</s:if>
								</s:if>
							</div>
						</div>
						<div class="blockRow">
							<s:if test="%{#hasMultiCurrency=='false'}">
								<span class="confirmMultipleTransfer_dateLabelId sectionLabel"><s:text name="jsp.default_137" /> :</span>
							</s:if>
							<s:if test="%{#hasMultiCurrency=='true'}">
									<span class="confirmMultipleTransfer_dateLabelId sectionLabel"><s:text name="jsp.default_137" /> :</span>
							</s:if>
							<%--Transfer Date Section --%>
							<span id="confirmMultipleTransfer_dateValueId<s:property value='#rowCount'/>"><s:property value="#transfer.date" /></span>
						</div>
						<s:if test="%{memo!=''}">
							<div class="blockRow">
								<s:if test="%{#hasMultiCurrency=='false'}">
									<span class="confirmMultipleTransfer_memoLabelId sectionLabel"><s:text name="jsp.default_279" /> :</span>
								</s:if>
								<s:if test="%{#hasMultiCurrency=='true'}">
										<span class="confirmMultipleTransfer_memoLabelId sectionLabel"><s:text name="jsp.default_279" /> :</span>
								</s:if>
								<%--Transfer Memo Section --%>
								<span id="confirmMultipleTransfer_memoValueId<s:property value='#rowCount'/>"><s:property value="#transfer.memo" /></span>
							</div>
						</s:if>
					</div>
			</s:if>
		</s:if>
		<s:set var="rowCount" value="%{#rowCount + 1}" />
	</s:iterator>							
	<s:if test="%{#hasMultiCurrency=='true'}">
		<div class="estimatedAmountMessage">
			<span class="required">&#8776; <s:text name="jsp.default_241" /></span>
		</div>
	</s:if>
	<div class="btn-row" id="confirmMultipleTransfer_confirmDoneBtn">
			<s:if test="%{#request.isTemplateLoad}">
				<sj:a button="true" summaryDivId="summary" buttonType="done"
					onClickTopics="showSummary,cancelTemplatesForm">
					<s:text name="jsp.default_175" />
				</sj:a>
			</s:if>
			<s:else>
				<sj:a button="true"
					summaryDivId="summary" buttonType="done"
					onClickTopics="showSummary,cancelForm,reloadSelectedGridTab">
					<s:text name="jsp.default_175" />
				</sj:a>
			</s:else>
	</div>	
</div>
