<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="com.ffusion.tasks.banking.Task"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.beans.banking.TransferBatch"%>
<%@ page import="com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<div class="formPanel">

	<%--Set variablees to be used on the form --%>
		<s:set var="transferBatch" value="#session.transferBatch" />
		<ffi:help id="payments_accountMultipleTransferVerify" />
		<s:set var="hasMultiCurrency"
			value="%{''+#transferBatch.hasMultiCurrency}" />
		<s:set var="batchTotalZero" value="#transferBatch.amountValue.isZero" />


		<%--Verify Mutliple Transfer Form Start --%>
		<s:form id="MultipleTransferNewVerify" namespace="/pages/jsp/transfers" validate="false" action="%{ 'AddTransferBatchAction_execute'}" method="post" name="MultipleTransferNewVerify" theme="simple">
			<%--Hidden Field section --%>
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
			<input type="Hidden" name="OpenEnded" value="false">
			<input type="Hidden" name="AddTransfer.ClearAccounts" value="">
<div class="leftPaneWrapper" role="form" aria-labelledby="transferSummary">
	<div class="leftPaneInnerWrapper">
		<div class="header"><h2 id="transferSummary"><s:text name="jsp.transfer.summary.label" /></h2></div>
		<div class="leftPaneInnerBox">
			<div style="width:100%" class="floatleft">
				<span id="" class="sectionsubhead sectionLabel floatleft inlineSection">Total Transfers: </span>
				<span id="" class="inlineSection floatleft labelValue">
					<s:set var="totalTransferCount" value="0" />
					<s:iterator var="transfer" value="#transferBatch.transfers">
					<s:if test="%{#transfer.fromAccount!=null}">
					 	<s:set var="totalTransferCount" value="%{#totalTransferCount + 1}" />
					</s:if>
					</s:iterator>
					<s:property value='#totalTransferCount'/>
				</span>
			</div>
		</div>	
	</div>
		
	<%--Transfer Batch Total Section --%>
	<s:if test="%{!#batchTotalZero}">
		<div class="totalAmountWrapper leftPaneInnerWrapper">
			<div class="inlineSection"><span id="TotalLabel">  <s:text name="jsp.default_431" />  :</span></div>
			<div class="inlineSection">
				<span id="Total"> 
					<s:property value="#transferBatch.amountValue.currencyStringNoSymbol" />
					<s:property value="#transferBatch.amountValue.currencyCode" />
				</span>
			</div>
		</div>
	</s:if>
</div>
<div class="confirmPageDetails">
			<!-- New Div Layout -->
			<%--If Date Change Display warning message --%>
			<s:if test="%{#request.isBatchDateChanged=='true'}">
				<div id="verifyMultipleTransfer_warningMessageId" class="sectionsubhead">
					<s:text name="jsp.transfers_91" />
				</div>
			</s:if>
			<s:set var="rowCount" value="0" />
			<s:iterator var="transfer" value="#transferBatch.transfers">
				
				<div class="blockWrapper" role="form" aria-labelledby="teansactionSummary">
				<s:if test="%{#transfer.fromAccount!=null}">
				<h3 class="transactionHeading">Transfer <s:property value="#rowCount+1"/></h3>
					<div  class="blockHead"><h2 id="transactionSummary"><s:text name="jsp.transaction.summary.label" /></h2></div>
						<div class="blockContent">
							<div class="blockRow">
								<div class="inlineBlock"  style="width: 50%">
									<span id="verifyMultipleTransfer_fromAccountLabelId"  class="sectionLabel"><s:text name="jsp.default_217" /> :</span>
									<%--From Account Section --%>
									<span id="verifyMultipleTransfer_fromAccountValueId<s:property value='#rowCount'/>">
										<s:if test="%{#transfer.fromAccount!=null}">
											<s:property value="#transfer.fromAccount.accountDisplayText"/>
										</s:if>
									</span>
								</div>
								<div class="inlineBlock">
									<s:if test="%{#hasMultiCurrency=='false'}">
										<span id="verifyMultipleTransfer_amountLabelId" class="sectionLabel">
											<s:text name="jsp.default_43" /> :
										</span>
									</s:if>
									<%--Amount  Section for Single Currency Transfer--%>
									<s:if test="%{#hasMultiCurrency=='false'}">
										<span id="verifyMultipleTransfer_amountValueId<s:property value='#rowCount'/>">
											<s:property value="#transfer.amountValue.currencyStringNoSymbol" /> 
											<s:property value="#transfer.amountValue.currencyCode" />
										</span>
									</s:if>
									
									<s:if test="%{#hasMultiCurrency=='true'}">
										<span class="verifyMultipleTransfer_fromAmountLabelId sectionLabel">
											<s:text name="jsp.default_489" /> :
										</span>
									</s:if>
									<%--Amount and To Amount  Section For Multicurrency Transfer --%>
									<s:if test="%{#hasMultiCurrency=='true'}">
									<%--Amount Section --%>
									<span id="verifyMultipleTransfer_fromAmountValueId<s:property value='#rowCount'/>">
										<s:if test="%{#transfer.isAmountEstimated}">
                                            &#8776;
                                        </s:if> <s:if test="%{#transfer.amountValue.currencyStringNoSymbol=='0.00'}">--</s:if>
										<s:else>
											<s:property
												value="#transfer.amountValue.currencyStringNoSymbol" />
											<s:property value="#transfer.amountValue.currencyCode" />
                                     	</s:else>
                                     </span>
								</s:if>
								</div>
							</div>
							<div class="blockRow">
								<div class="inlineBlock"  style="width: 50%">
									<span id="" class="verifyMultipleTransfer_toAccountLabelId sectionLabel"><s:text name="jsp.default_424" /> :</span>
									<%--To Account Section --%>
									<span id="verifyMultipleTransfer_toAccountValueId<s:property value='#rowCount'/>">
										<s:if test="%{#transfer.toAccount!=null}">
											<s:property value="#transfer.toAccount.accountDisplayText"/>
										</s:if>
									</span>
								</div>
								<div class="inlineBlock">
									<s:if test="%{#hasMultiCurrency=='true'}">
										<span class="verifyMultipleTransfer_toAmountLabelId sectionLabel"><s:text name="jsp.default_512" /> :
										</span>
									</s:if>
									<%--Amount and To Amount  Section For Multicurrency Transfer --%>
									<s:if test="%{#hasMultiCurrency=='true'}">
                                        <%--Estimated Amount Section --%>
										<span id="verifyMultipleTransfer_toAmountValueId<s:property value='#rowCount'/>">
										    <s:if test="%{#transfer.isToAmountEstimated}">
	                                            &#8776;
	                                        </s:if> 
	                                        <s:if test="%{#transfer.toAmountValue.currencyStringNoSymbol=='0.00'}">--</s:if>
											<s:else>
													<s:property
														value="#transfer.toAmountValue.currencyStringNoSymbol" />
													<s:property value="#transfer.toAmountValue.currencyCode" />
	                                         </s:else>
                                        </span>
									</s:if>
								</div>
							</div>
							<div class="blockRow">
								<span class="verifyMultipleTransfer_dateLabelId sectionLabel">
									<s:text name="jsp.default_137" /> :
								</span>
								<span id="verifyMultipleTransfer_dateValueId<s:property value='#rowCount'/>">
									<s:property value="#transfer.date" /> 
									<s:if test="%{#request.dateChanged[#rowCount]=='true'}">&nbsp;<span class="sectionsubhead"><s:text name="jsp.transfers_8" /></span>
									</s:if>
								</span>
							</div>
					<s:if test="%{memo!=''}">
					<div class="blockRow">
						<span class="verifyMultipleTransfer_memoLabelId sectionLabel">
							<s:text name="jsp.default_279" /> :
						</span>
						<span  id="verifyMultipleTransfer_memoValueId<s:property value='%{#rowCount-1}'/>">
							<s:property value="#transfer.memo" />
						</span>
					</div>
					</s:if>		
					<s:if test="%{#request.showCategoryColumn=='true'}">
					<div class="blockRow">
						<span class="sectionLabel"><s:text name="jsp.default_88" /> :</span>
						<s:set var="Transfer" value="#transfer" scope="session"></s:set>
						<%-- <!-- ====================== BEGIN REGISTER INTEGRATION ===================== -->--%>
						<ffi:cinclude value1="${Transfer.REGISTER_CATEGORY_ID}" value2="0" operator="notEquals">
							<span><ffi:setProperty
									name="RegisterCategories" property="Current"
									value="${Transfer.REGISTER_CATEGORY_ID}" /> <ffi:setProperty
									name="Parentfalse" value="" /> <ffi:setProperty
									name="Parenttrue" value=": " /> <ffi:getProperty
									name="RegisterCategories" property="ParentName" /> <ffi:getProperty
									name="Parent${RegisterCategories.HasParent}" /> <ffi:getProperty
									name="RegisterCategories" property="Name" /></span>
						</ffi:cinclude>
					</div>
					<s:if test="%{#transfer.transferDestination == 'ITOE'}">
						<s:set var="mfa" value="true"/>
					</s:if>
					
					<s:if test="%{#mfa != 'true'}">
						<input type="hidden" name="ignoreMfa" value="true"/>
					</s:if>
					
					</s:if>
					
				</div>
				</s:if>
				
				</div>
				<s:set var="rowCount" value="%{#rowCount + 1}" />
			</s:iterator>
			
			<s:if test="%{#hasMultiCurrency=='true'}">
				<div class="estimatedAmountMessage">
						<span id="verifyMultipleTransfer_messageIndicator" class="required">&#8776; <s:text name="jsp.default_241" />
						</span>
				</div>
			</s:if>
		<s:if test="isSameDayTransfer">
			<div id="sameDayMsg" class="sectionsubhead" align="left">
				<strong><s:text name="jsp.transfers_115" /></strong>
			</div>
		</s:if>
			
			<div class="btn-row">
				<sj:a id="cancelFormButtonOnVerify" button="true" summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelMultipleTransferNewForm">
					<s:text name="jsp.default_82" />
				</sj:a> 
				<s:url action="AddTransferBatchAction_init" escapeAmp="false" namespace="/pages/jsp/transfers" id="intializeTaskUrl"></s:url> 
				<ffi:cinclude value1="${TransferFileUpload}" value2="true" operator="equals">
					<sj:a id="backFormButton" button="true"
						onClickTopics="backToInput,hideOperationResult">
						<s:text name="jsp.default_57" />
					</sj:a>
				</ffi:cinclude> 
				<ffi:cinclude value1="${TransferFileUpload}" value2="true" operator="notEquals">
					<s:hidden id="hiddenInitTaskUrl" value="%{intializeTaskUrl}"></s:hidden>
					<sj:a id="backFormButton" button="true"
						onClickTopics="initializeBatchTask">
						<s:text name="jsp.default_57" />
					</sj:a>
				</ffi:cinclude> 
				<sj:a id="sendMultipleTransferSubmit" formIds="MultipleTransferNewVerify" targets="confirmDiv" button="true" onBeforeTopics="beforeSendTransferForm" 
					onSuccessTopics="sendTransferFormSuccess"
					onErrorTopics="errorSendTransferForm"
					onCompleteTopics="sendTransferFormComplete">
					<s:text name="jsp.default_378" />
				</sj:a>
			</div>
			<!--  New Div Layout -->
			
</div>
</s:form>
</div>
