<%--
Name: 			accounttransferconfirmform.jsp

Flow(s):		Add Single Transfer
				Add Recurring Transfer
				Edit Single Transfer
				Edit Recurring Transfer
 --%>
 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ffusion.beans.banking.TransferDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="java.lang.Integer"%>

<%--Set the bean(s) that is to be used on the JSP --%>


<s:if test="flowType=='NewTransfer'">
	<s:set var="transfer" value="%{#session.newTransfer}" />
</s:if>
<s:else>
	<s:set var="transfer" value="#session.editTransfer" />
</s:else>


<ffi:help id="payments_accounttransferconfirm" />

<div class="leftPaneWrapper" role="form" aria-labelledby="transferSummary"><div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="transferSummary"><s:text name="jsp.transfer.summary.label" /></h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%" class="floatleft">
			<span id="confirmTransfer_fromAccountLabelId" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_217" /> :</span>
			<span id="confirmTransfer_fromAccountValueId" class="inlineSection floatleft labelValue">
				<s:property value="#transfer.fromAccount.accountDisplayText" />
			</span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
			<span id="confirmTransfer_toAccountLabelId" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text	name="jsp.default_424" /> :</span>
			<span id="confirmTransfer_toAccountValueId" class="inlineSection floatleft labelValue">
				<s:property value="#transfer.toAccount.accountDisplayText" />
			</span>
		</div>
	</div>
</div>

<ffi:cinclude ifEntitled="<%=EntitlementsDefines.ENT_TRANSFERS_TEMPLATE%>">
	<div id="templateForSingleTransferVerify" class="leftPaneInnerWrapper" role="form" aria-labelledby="saveTemplate">
			<div class="header"><h2 id="saveTemplate"><s:text name="jsp.default_371" /></h2></div>
			   <div id="templateSaveAsID" style="padding: 15px 10px;">
					<s:include value="accounttransfertempnewname.jsp" />
		       </div>
		    <div id="templateForSingleTransferVerifyResult"></div>
	</div>
</ffi:cinclude>

</div>
<form action="accounttransfersend.jsp" method="post" name="TransferNewSent">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
<div class="confirmPageDetails">
	
		<div class="confirmationDetails">
			<span><s:text name="jsp.default_347" /></span>
			<span id="confirmTransfer_refernceValueId"><s:property value="%{#transfer.trackingID}" /></span>
		</div>
		<div class="blockWrapper">
			<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
			<div class="blockContent">
					<s:set var="TRS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_TO_TRANSFER==#transfer.status}" />
					<s:set var="TRS_INSUFFICIENT_FUNDS" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_INSUFFICIENT_FUNDS==#transfer.status}" />
					<s:set var="TRS_BPW_LIMITCHECK_FAILED" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BPW_LIMITCHECK_FAILED==#transfer.status}" />
					<s:set var="TRS_SCHEDULED" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_SCHEDULED==#transfer.status}" />
					<s:set var="TRS_TRANSFERED" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_TRANSFERED==#transfer.status}" />
					<s:set var="TRS_PENDING_APPROVAL" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_PENDING_APPROVAL==#transfer.status}" />
					<s:set var="TRS_UNKNOWN" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_UNKNOWN==#transfer.status}" />
					<s:set var="TRS_FUNDSPROCESSED" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FUNDSPROCESSED==#transfer.status}" />
					<s:set var="TRS_BACKEND_FAILED" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BACKEND_FAILED==#transfer.status}" />
					
					<s:if
						test="%{#TRS_FAILED_TO_TRANSFER}">
						<s:set var="HighlightStatus" value="true" />
					</s:if> 
					<s:if test="%{#TRS_INSUFFICIENT_FUNDS}">
						<s:set var="HighlightStatus" value="true" />
					</s:if> 
					<s:if test="%{#TRS_BPW_LIMITCHECK_FAILED}">
						<s:set var="HighlightStatus" value="true" />
					</s:if>
					<s:if test="%{#TRS_BACKEND_FAILED}">
						<s:set var="HighlightStatus" value="true" />
					</s:if> 
					<s:if test="%{#HighlightStatus==true}">
						<div class="blockRow failed">
							<%-- Display Transfer Status --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
									<span class="sapUiIconCls icon-decline"></span> <!-- failed mean give red here -->
								<s:if test="%{#transfer.backendErrorDesc=='' || #transfer.backendErrorDesc==null }">
								<s:property	value="%{#transfer.statusName}" />
								</s:if> 
								<s:else>
								<s:property	value="%{#transfer.statusName}" /> - <s:property value="%{#transfer.backendErrorDesc}"/>
								</s:else>
							</span>
					</s:if> 
					<s:elseif test="%{#TRS_SCHEDULED}">
						<div class="blockRow pending">
							<%-- Display Transfer Status --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
									<span class="sapUiIconCls icon-future"></span><!-- this is scheduled/pending giv orange -->
									<s:property value="%{#transfer.statusName}" />
							</span>
					</s:elseif>
					<s:elseif test="%{#TRS_TRANSFERED}">
						<div class="blockRow completed">
							<%-- Display Transfer Status --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-accept"></span><!-- this is transfered give it green --> <s:property value="%{#transfer.statusName}" />
							</span>
					</s:elseif>
					<s:elseif test="%{#TRS_FUNDSPROCESSED}">
						<div class="blockRow completed">
							<%-- Display Transfer Status --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-accept"></span><!-- this is transfered give it green --> <s:property value="%{#transfer.statusName}" />
							</span>
					</s:elseif>
					<s:elseif test="%{#TRS_PENDING_APPROVAL}">
						<div class="blockRow pending">
							<%-- Display Transfer Status --%>
							<span id="confirmTransfer_statusValueId"><s:set var="HighlightStatus" value="false" />
								<span class="sapUiIconCls icon-pending"></span> <!-- this is pending approval give color orange icon different -->
								<s:property value="%{#transfer.statusName}" />
							</span>
					</s:elseif>
					<span id="transferResultMessage" class="floatRight"> <s:if
							test="%{#TRS_TRANSFERED}">
							<s:text name="jsp.transfers_93" />
						</s:if> <s:elseif test="%{#TRS_FAILED_TO_TRANSFER}">
							<s:text name="jsp.transfers_104" />
						</s:elseif> <s:elseif test="%{#TRS_INSUFFICIENT_FUNDS}">
							<s:text name="jsp.transfers_105" />
						</s:elseif> <s:elseif test="%{#TRS_BPW_LIMITCHECK_FAILED}">
							<s:text name="jsp.transfers_106" />
						</s:elseif> <s:elseif test="%{#TRS_SCHEDULED}">
							<s:text name="jsp.transfers_93" />
						</s:elseif> <s:elseif test="%{#TRS_PENDING_APPROVAL}">
							<s:text name="jsp.transfers_93" />
						</s:elseif> <s:elseif test="%{#TRS_UNKNOWN}">
							<s:text name="jsp.transfers_107" />
						</s:elseif>
						<s:elseif test="%{#TRS_FUNDSPROCESSED}">
							<s:text name="jsp.transfers_93" />
						</s:elseif>
					</span>	</div>	
				</div>
			</div>
			<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
			<div class="blockContent">
			
				<%--Transfer Date --%>
				<div class="blockRow">
					<span id="confirmTransfer_dateLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_137" /> :</span>
					<span id="confirmTransfer_dateValueId"><s:property value="#transfer.date" /></span>
				</div>
				<div class="blockRow">
					<s:set var="DisplayEstimatedAmountKey" value="false" />
					<s:if test="%{#transfer.UserAssignedAmountFlagName=='single'}">
						<div class="inlineBlock" style="width: 50%">
							<span id="confirmTransfer_amountLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43" /> :</span>
							<span id="confirmTransfer_amountValueId"><s:property value="#transfer.AmountValue.CurrencyStringNoSymbol" /> 
							<s:property value="#transfer.AmountValue.CurrencyCode" /></span>
						</div>
					</s:if>
					<s:else>
						<div class="inlineBlock" style="width: 50%">
							<span id="confirmTransferFrom_amountLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_489" />
							: </span>
							<span id="confirmTransferFrom_amountValueId">
								<s:if test="%{#transfer.isAmountEstimated}">
		                                        &#8776;<s:set
										var="DisplayEstimatedAmountKey" value="true" />
								</s:if> 
								<s:property value="#transfer.AmountValue.CurrencyStringNoSymbol" />
								<s:property value="#transfer.AmountValue.CurrencyCode" />
							</span>
						</div>
					</s:else>
					<s:if test="%{#transfer.UserAssignedAmountFlagName=='single'}">
					</s:if>
					<s:else>
						<div class="inlineBlock">
							<span id="confirmTransferTo_amountLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_512" />
							: </span>
							<span id="confirmTransferTo_amountValueId">
								<s:if test="%{#transfer.isToAmountEstimated}">&#8776;<s:set var="DisplayEstimatedAmountKey" value="true" />
								</s:if> <s:property value="#transfer.ToAmountValue.CurrencyStringNoSymbol" />
								<s:property value="#transfer.ToAmountValue.CurrencyCode" />
							</span>
						</div>
					</s:else>
				</div>
			</div>
			
			<%-- Status of Transfer after submission --%>
				<!-- <div class="blockRow"> -->
				
					<s:set
						var="TRS_FAILED_TO_TRANSFER"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_TO_TRANSFER==#transfer.status}" />
					<s:set var="TRS_INSUFFICIENT_FUNDS"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_INSUFFICIENT_FUNDS==#transfer.status}" />
					<s:set var="TRS_BPW_LIMITCHECK_FAILED"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BPW_LIMITCHECK_FAILED==#transfer.status}" />
					<s:set var="TRS_SCHEDULED"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_SCHEDULED==#transfer.status}" />
					<s:set var="TRS_TRANSFERED"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_TRANSFERED==#transfer.status}" />
					<s:set var="TRS_PENDING_APPROVAL"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_PENDING_APPROVAL==#transfer.status}" />
					<s:set var="TRS_UNKNOWN"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_UNKNOWN==#transfer.status}" />			
					
				<!-- </div> -->
				
			<div  class="blockHead"><s:text name="jsp.miscellaneous.label" /></div>
			<div class="blockContent">
				<%--Display the Transfer Destinition.If the Transfer is Internal to Internal, External to Internal or Internal to External --%>
				
				<ffi:cinclude
					ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
					<s:set var="TRANSFER_BOOK" value="%{'ITOI'}" />
					<%--Values to be in sync with com.ffusion.beans.banking.TransferDefines --%>
					<s:set var="TRANSFER_ITOE" value="%{'ITOE'}" />
					<s:set var="TRANSFER_ETOI" value="%{'ETOI'}" />
					<div class="blockRow">
						<span id="verifyTransfer_typeLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_444" /> :</span>
						<span id="verifyTransfer_typeValueId">
						<s:if test="%{#transfer.transferDestination==#TRANSFER_BOOK || #transfer.transferDestination=='INTERNAL'}">
								<s:text name="jsp.transfers_53" />
							</s:if>  <s:if test="%{#transfer.transferDestination==#TRANSFER_ITOE}">
								<s:text name="jsp.transfers_52" />
							</s:if> <s:if test="%{#transfer.transferDestination==#TRANSFER_ETOI}">
								<s:text name="jsp.transfers_44" />
							</s:if></span>
					</div>
				</ffi:cinclude>
			
				<%--Transfer Memo --%>
				<s:if test="%{memo!=''}">
					<div class="blockRow">
						<span id="confirmTransfer_memoLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279" /> :</span>
						<span id="confirmTransfer_memoValueId"><s:property value="#transfer.memo" /></span>
					</div>
				</s:if>
				
			</div>
			<s:if test="isRecurring=='true'">
				<div  class="blockHead"><s:text name="jsp.recurrence.label" /></div>
				<div class="blockContent">
					
						<%-- Display Transfer Frequency --%>
						<div class="blockRow">
							<span id="confirmTransfer_recuringLabelId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_351" /> :</span>
							<span id="confirmTransfer_recuringValueId"><s:property value="#transfer.frequency" /></span>
						</div>
					
						<%-- Display Transfer Count --%>
						<div class="blockRow">
							<span id="confirmTransfer_noOfTransfersLabelId"	class="sectionsubhead sectionLabel"><s:text name="jsp.default_443" /> :</span>
							<span id="confirmTransfer_noOfTransfersValueId"><s:if
									test="%{#transfer.isOpenEnded()}">
									<s:text name="jsp.default_446" />
								</s:if> <s:else>
									<s:property value="#transfer.numberTransfers" />
								</s:else></span>
					</div>
			</div>
			</s:if>
		
		<div class="mltiCurrenceyMessage">
			<s:if test="%{#DisplayEstimatedAmountKey}">
				 <span class="required">&#8776; <s:text name="jsp.default_241" /></span>
			</s:if>
			<s:else>
			</s:else>
		</div>
		<div class="btn-row">
				<s:if test="%{fromPortal=='true'}">
						<sj:a id="transferDoneButton" button="true"
							onclick="$('#Grid_pendingTransfersSummaryID').trigger('reloadGrid');"
							onClickTopics="cancelEditPendingTransferForm">
							<s:text name="jsp.default_175" />
						</sj:a>
				</s:if>
			  <s:else>
				<s:if test="%{isLoadFromTemplate=='true'}">				
					<sj:a id="transferDoneBtn" button="true" summaryDivId="summary" buttonType="done" onClickTopics="showSummary,cancelForm">
						<s:text name="jsp.default_175" />
					</sj:a>
				</s:if>
				<s:else>				
					<sj:a id="transferDoneBtn" button="true" summaryDivId="summary" buttonType="done" onClickTopics="showSummary,showSelectedGridTabOnCancel,cancelForm">
						<s:text name="jsp.default_175" />
					</sj:a>
				</s:else>
			</s:else>
		</div>
</div>
</form>