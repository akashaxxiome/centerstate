<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page import="com.ffusion.beans.banking.TransferDefines"%>

<s:set var="transferBatch" value="#request.transferBatch"/>

<ffi:help id="payments_accounttransfertempdelete" />
<div class="approvalDialogHt2">
<s:form action="/pages/jsp/transfers/deleteTransferTemplateBatchAction.action" method="post" name="deleteMultipleTemplateForm" id="deleteMultipleTemplateFormId" >
<input type="hidden" name="transferBatchID" value="<s:property value='#transferBatch.ID'/>"/>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

<div class="templateConfirmationWrapper">
	<div class="confirmationDetails">
		<span id="deleteMultipleTransferTemplate_NameLabelId" class="sectionLabel"><s:text name="jsp.default_416" />:</span>
		<span id=""><s:property value="#transferBatch.templateName"/></span>
	</div>
</div>
<s:set var="idCount" value="0"/>
	<s:iterator var="transfer" value="#transferBatch.transfers">
    	<s:if test="%{#transfer.fromAccount!=null}" >
    		<h3 class="transactionHeading">Transfer<s:property value="#idCount+1"/></h3>
    		<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
			<div class="blockContent">
							<s:set var="TRS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_TO_TRANSFER==#transfer.status}" />
							<s:set var="TRS_INSUFFICIENT_FUNDS"
								value="%{@com.ffusion.beans.banking.TransferStatus@TRS_INSUFFICIENT_FUNDS==#transfer.status}" />
							<s:set var="TRS_BPW_LIMITCHECK_FAILED"
								value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BPW_LIMITCHECK_FAILED==#transfer.status}" />
							<s:set var="TRS_SCHEDULED"
								value="%{@com.ffusion.beans.banking.TransferStatus@TRS_SCHEDULED==#transfer.status}" />
							
							<s:set var="TRS_PENDING_APPROVAL"
								value="%{@com.ffusion.beans.banking.TransferStatus@TRS_PENDING_APPROVAL==#transfer.status}" />
								
							<s:if test="%{#TRS_FAILED_TO_TRANSFER}">
								<s:set var="HighlightStatus" value="true" />
							</s:if> 
							<s:if test="%{#TRS_INSUFFICIENT_FUNDS}">
								<s:set var="HighlightStatus" value="true" />
							</s:if> 
							<s:if test="%{#TRS_BPW_LIMITCHECK_FAILED}">
								<s:set var="HighlightStatus" value="true" />
							</s:if>
							<s:if test="#HighlightStatus=='true'">
								<div class="blockRow failed"><span id="confirmMultipleTransfer_statusValueId<s:property value='#rowCount'/>">
									<input type="hidden" id="isValid" value="false"/>
									<span class="sapUiIconCls icon-decline "></span> <span class="required"><s:property value="#transfer.statusName" /></span>
								</span></div>
							</s:if>
							<s:else>
								<s:if test="%{#TRS_PENDING_APPROVAL}">
								<div class="blockRow pending"><span id="confirmMultipleTransfer_statusValueId<s:property value='#rowCount'/>">
									<input type="hidden" id="isValid" value="false"/>
									<span class="sapUiIconCls icon-pending "></span>
									<s:property value="%{#transfer.statusName}" />
								</span></div>
								</s:if>
								<s:else>
									<div class="blockRow completed"><span id="confirmMultipleTransfer_statusValueId<s:property value='#rowCount'/>">
										<input type="hidden" id="isValid" value="true"/>
										<span class="sapUiIconCls icon-accept"></span>
										<s:property value="%{#transfer.statusName}" />
									</span></div>
								</s:else>  
							</s:else>
				</div>
			<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span id="deleteMultipleTransferTemplate_fromAccountLabelId" class="sectionLabel"><s:text name="jsp.default_217"/>: </span>
                        <span id="deleteMultipleTransferTemplate_fromAccountValueId<s:property value='#idCount'/>">
                             <s:if test="%{#transfer.fromAccount!=null}">
                                 <s:if test="%{#transfer.fromAccountID=='Deleted-0'}" >
                                  <s:property value="#transfer.fromAccount.accountDisplayText"/>                         
								</s:if>	
                                 <s:elseif test="%{#transfer.fromAccountID=='Restricted-0'}">
                                    <s:property value="#transfer.fromAccount.accountDisplayText"/>                     
								</s:elseif>	
								<s:else>
								 <s:property value="#transfer.fromAccount.accountDisplayText"/>                     
								</s:else>
                              </s:if>
                         </span>        
					</div>
					<div class="inlineBlock">
							<s:if test="%{!#transferBatch.hasMultiCurrency}" >
                                <span id="deleteMultipleTransferTemplate_amountLabelId" class="sectionLabel"><s:text name="jsp.default_43"/>: </span>
                                    <span id="deleteMultipleTransferTemplate_amountLabelId<s:property value='#idCount'/>">
	                                    <s:property value="#transfer.amountValue.currencyStringNoSymbol"/>
	                                    <s:property value="#transfer.amountValue.currencyCode"/>
                                    </span>
                                
                             </s:if>
                                <s:else>
                                <span id="deleteMultipleTransferTemplate_fromAmountLabelId" class="sectionLabel"><s:text name="jsp.default_489"/>: </span>
                                                                  <span id="deleteMultipleTransferTemplate_fromAmountLabelId<s:property value='#idCount'/>">
                                  <s:if test="%{#transfer.IsAmountEstimated}">&#8776;</s:if>
                                    
                                      <s:if test="%{#transfer.amountValue.currencyStringNoSymbol=='0.00'}" >
                                          --
                                      </s:if>
                                      <s:else>
                                           <s:property value="#transfer.amountValue.currencyStringNoSymbol"/>
                                  		<s:property value="#transfer.amountValue.currencyCode"/>
                                      </s:else>
                                  </span>
                             </s:else>
						</div>
				</div>
				
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span id="deleteMultipleTransferTemplate_toAccountLabelId" class="sectionLabel"><s:text name="jsp.default_424"/>: </span>
						<span id="deleteMultipleTransferTemplate_toAccountValueId<s:property value='#idCount'/>">
                                   <s:if test="%{#transfer.toAccount!=null}">
	                                     <s:if test="%{#transfer.toAccountID=='Deleted-0'}" >
                                    <s:property value="#transfer.toAccount.accountDisplayText"/>                                            
								</s:if>	

                                 <s:elseif test="%{#transfer.toAccountID=='Restricted-0'}">
                                      <s:property value="#transfer.toAccount.accountDisplayText"/>                      
								</s:elseif>	
								<s:else>
								   <s:property value="#transfer.toAccount.accountDisplayText"/>                      
								</s:else>
	                                </s:if>
                                    </span>
					</div>
					<div class="inlineBlock">
						<s:if test="%{!#transferBatch.hasMultiCurrency}">
                         </s:if>
                         <s:else>
                         	<span id="deleteMultipleTransferTemplate_toAmountLabelId"><s:text name="jsp.default_512"/>: </span>
                         </s:else>
                         <s:if test="%{!#transferBatch.hasMultiCurrency}">
                         </s:if>
                         <s:else>
                         	<span id="viewMultipleTransferTemplate_toAmountValueId <s:property value='#idCount'/>">
                                <s:if test="%{#transfer.isToAmountEstimated}">&#8776;</s:if>
                                
                                  <s:if test="%{#transfer.amountValue.currencyStringNoSymbol=='0.00'}" >
                                      --
                                  </s:if>
                                  <s:else>
                                       <s:property value="#transfer.toAmountValue.currencyStringNoSymbol"/>
                              		<s:property value="#transfer.toAmountValue.currencyCode"/>
                                  </s:else>
                              </span>
                         </s:else>
					</div>
				</div>
				<div class="blockRow">
					<span id="deleteMultipleTransferTemplate_memoLableId" class="sectionLabel"><s:text name="jsp.default_279"/></span>
					<span id="viewMultipleTransferTemplate_memoId<s:property value='#idCount'/>">
                    	<s:property value="#transfer.memo"/>
                    </span>
				</div>
			</div>
    		<s:set var="idCount" value="%{#idCount+1}"/>
		</s:if>
	</s:iterator>
<s:if test="%{!#transferBatch.hasMultiCurrency}">
                           
<div class="totalAmountWrapper">
        <span id="TotalLabel"><s:text name="jsp.default_553"/></span>
        <span id="Total">
            <s:property value="#transferBatch.amountValue.currencyStringNoSymbol"/>
            <s:property value="#transferBatch.amountValue.currencyCode"/>
        </span>
</div>
</s:if>
<s:else>
<div class="mltiCurrenceyMessage">
    <span class="required">&#8776; <s:text name="jsp.default_241"/></span>
</div>
</s:else>
</s:form>

<div class="ffivisible" style="height:100px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
	<sj:a id="cancelDeleteMultipleTemplateLink" button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}"><s:text name="jsp.default_82" /></sj:a>
	
	<sj:a id="deleteMultipleTemplateLink" formIds="deleteMultipleTemplateFormId" targets="resultmessage" button="true"   
			  title="%{getText('Delete_Template')}" onCompleteTopics="cancelMultipleTransferTemplateComplete" onSuccessTopics="cancelMultipleTransferTemplateSuccessTopics" onErrorTopics="errorDeleteTransfer" 
			  effectDuration="1500" ><s:text name="jsp.default_162" /></sj:a>
</div> 
</div>