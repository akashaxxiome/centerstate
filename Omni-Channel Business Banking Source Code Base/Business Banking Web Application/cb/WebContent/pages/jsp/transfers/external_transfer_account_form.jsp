<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ffusion.beans.exttransfers.ExtTransferAccount,
				 com.ffusion.csil.core.common.EntitlementsDefines,
				 com.ffusion.beans.exttransfers.ExtTransferAccountDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="pmTran_external_account"  className="moduleHelpClass" />

<s:set var="extTransferAccount" value="#session.extTransferAccount" />
<s:set name="strutsActionName" value="%{'AddExtTransferAccount'}" />
	
<span class="shortcutPathClass" style="display:none;">pmtTran_transfer,addExternalTransferAccountLink</span>
<span class="shortcutEntitlementClass" style="display: none;">External Transfers</span>

<span id="PageHeading" style="display:none;"><s:text name="jsp.transfers_add_external_transfer_account"/></span>
 
<ffi:cinclude value1="${SecureUser.LocaleLanguage}" value2="en_US" operator="equals">
    <ffi:setProperty name="externalXferURL" value="/web/forms/Manage-External-Enrollment.pdf" />
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.LocaleLanguage}" value2="en_US" operator="notEquals">
    <ffi:setProperty name="externalXferURL" value="/web/${UserLocale.Language}forms/Manage-External-Enrollment.pdf" />
</ffi:cinclude>

<ffi:cinclude value1="${baSBackendEnabledFlag}" value2="TRUE" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.GetCountryList" id="GetCountryList" scope="session"/>
		<ffi:setProperty name="GetCountryList" property="CollectionSessionName" value="CountryList"/>
	<ffi:process name="GetCountryList"/>
	<ffi:removeProperty name="GetCountryList"/>
</ffi:cinclude>
<%
session.setAttribute("FFIAddExtTransferAccount", session.getAttribute("AddExtTransferAccount"));
%>

<%
	String bankID = "";
    String maxLengthRoutingNumber = "9";
    boolean isRegionalBank = false;
%>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_ACH %>">
	<% bankID = "ETFACH";
	%>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_REGIONAL_BANK_IDENTIFIER %>">
		<%
        if (bankID.length() == 0)           // can only support ACH or Regional Bank, not both
        {
            bankID = "ETFACH";
            isRegionalBank = true;
            maxLengthRoutingNumber = "37";
        }
        %>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_SWIFT %>">
	<%
		if (bankID.equals("ETFACH")) {
			bankID = "ETFBOTH";
		} else {
			bankID = "ETFSWIFT";
		}
	%>
</ffi:cinclude>

<ffi:removeProperty name="ComingBack"/>

<script language="JavaScript">

$(function(){
	if($("#countrySelectID").length == 1) {
		$("#countrySelectID").selectmenu({width:'19.4em'});
	}
});

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function onSubmitForm() {
	updateRouting();
	document.formAddExternalTransferAccount.submit();
}

function updateRouting()
{
    if (document.formAddExternalTransferAccount["extTransferAccount.acctBankIDType"][0].checked) {
        document.formAddExternalTransferAccount["extTransferAccount.routingNumber"].value = document.formAddExternalTransferAccount.FedABARoutingNumber.value;
    } else {
        document.formAddExternalTransferAccount["extTransferAccount.routingNumber"].value = document.formAddExternalTransferAccount.SWIFTRoutingNumber.value;
    }
}

function updateCurrencyAndType()
{
	curSel = document.formAddExternalTransferAccount['extTransferAccount.currencyCode'];
	typeSel = document.formAddExternalTransferAccount['extTransferAccount.type'];
	if (curSel == null || typeSel == null)
        return;
    
	curSelectedText = "<s:property value="extTransferAccount.currencyCode"/>";
    typeSelectedText = "<s:property value="extTransferAccount.type"/>";
	
    if ( curSel.selectedIndex >= 0 )
        curSelectedText = curSel.options[ curSel.selectedIndex ].text;
    if ( typeSel.selectedIndex >= 0 )
        typeSelectedText = typeSel.options[ typeSel.selectedIndex ].value;
		
    isNotACH = <%=isRegionalBank%>;

    if (document.formAddExternalTransferAccount["extTransferAccount.acctBankIDType"][1].checked) {
    	isNotACH = true;
    }

	//clear out entries in the lists
	curSel.length = 0;
	idx = 0;
	if (isNotACH == true) {
		<s:iterator value = "targetCurrenciesList">
			curSel.options[idx] = new Option("<s:property value="getCurrencyCode()"/>", "<s:property value="getCurrencyCode()"/>");
			if (curSelectedText == "<s:property value="getCurrencyCode()"/>")
				curSel.options[idx].selected = true;     // reselect the previously selected option
            idx++;
		</s:iterator>
	} else {
		curSel.options[0] = new Option("USD","USD");
	}
	idx = 0;
	typeSel.length = 0;
	if (isNotACH == true) {
		<s:iterator value = "accountTypesIdList">
            typeValue = "<s:property/>";
			<s:set var="typeValueTextId">AccountType<s:property/></s:set>
			typeValueText = "<s:property value="getResourceByResourceId(#typeValueTextId)"/>";
			typeSel.options[idx] = new Option(typeValueText, typeValue);
			if (typeSelectedText == typeValue)
				typeSel.options[idx].selected = true;     // reselect the previously selected option
            idx++;
		</s:iterator>
	} else {
		<ffi:setProperty name="AccountTypes" property="ResourceID" value="AccountType1"/>
		typeSel.options[0] = new Option("<ffi:getProperty name="AccountTypes" property="Resource" />", "1");
		<ffi:setProperty name="AccountTypes" property="ResourceID" value="AccountType2"/>
		typeSel.options[1] = new Option("<ffi:getProperty name="AccountTypes" property="Resource" />", "2");
        if (typeSelectedText == typeSel.options[0].value)
            typeSel.options[0].selected = true;     // reselect the previously selected option
        if (typeSelectedText == typeSel.options[1].value)
            typeSel.options[1].selected = true;     // reselect the previously selected option
	}
	$('#accountType').selectmenu({width:150});
	$('#currencyCode').selectmenu({width:90});
}

$.subscribe('postFormTopic', function(event, data) {
	var $currentTarget = $(event.originalEvent.currentTarget);
	var aPostAction = $currentTarget.attr("postaction");
	var formId = $currentTarget.attr("formid");
	var field1 = $currentTarget.attr("field1");
	var value1 = $currentTarget.attr("value1");
	
	if (document.formAddExternalTransferAccount["extTransferAccount.acctBankIDType"][0].checked) {
        document.formAddExternalTransferAccount["extTransferAccount.routingNumber"].value = document.formAddExternalTransferAccount.FedABARoutingNumber.value;
    } else {
        document.formAddExternalTransferAccount["extTransferAccount.routingNumber"].value = document.formAddExternalTransferAccount.SWIFTRoutingNumber.value;
    }
	
	value1 = document.formAddExternalTransferAccount["extTransferAccount.routingNumber"].value;
	
	if(aPostAction){
		$('#'+ field1).val(value1);
		$('#'+formId).attr('action', aPostAction);
	}
});

//-->
</script>

<%-- ================ MAIN CONTENT START ================ --%>
<ffi:setProperty name="BackURL" value="${SecurePath}transfers/manage-external-add.jsp?ComingBack=true" URLEncrypt="true"/>

<s:form id="formAddExternalTransferAccount" namespace="/pages/jsp/transfers" validate="false" action="addExtTransferAccount" method="post" name="formAddExternalTransferAccount" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="extTransferAccount.locale" value="<ffi:getProperty name="${UserLocale.Locale}"/>"/>

<ffi:cinclude value1="${User.CustomerType}" value2="1" operator="equals"> <%-- small business user --%>
	<input type="hidden" name="extTransferAccount.businessId" value="<ffi:getProperty name="${SecureUser.BusinessID}"/>"/>
</ffi:cinclude>
<ffi:cinclude value1="${User.CustomerType}" value2="1" operator="notEquals"> <%-- small business user --%>
	<input type="hidden" name="extTransferAccount.userId" value="<ffi:getProperty name="${SecureUser.ProfileID}"/>"/>	
</ffi:cinclude>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_ADD_ACCOUNT %>">
<div class="instructionsDivWrapper">
<p class="instructions"><strong>External Account Transfer Instructions:</strong></p>
<ul id="formerrors"></ul>
<p class="instructions">
	<s:text name="jsp.exttransferaccount_msg_1"/>
</p>
<p class="instructions">
	<s:text name="jsp.exttransferaccount_msg_2"/>
</p>
<p class="instructions">
	<s:text name="jsp.exttransferaccount_msg_3"/>
</p>
<p class="instructions">
<s:url id="termsURL" value="%{#session.externalXferURL}" escapeAmp="false" />
<s:a href="%{termsURL}" target="_blank"><s:text name="jsp.exttransferaccount_download_enrollment_form"/></s:a>
</p></div>
	<div class="btn-row">
		<sj:a id="cancelFormButtonOnInput" summaryDivId="summary" buttonType="cancel" button="true" onClickTopics="showSummary,cancelExternalTransferAccountForm"><s:text name="jsp.default_82"/></sj:a>
	</div>
<script>
	$('#detailsPortletWizardStatusId').hide();
</script>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_ADD_ACCOUNT %>" >
<div class="instructionsDivWrapper">
<p class="instructions"><strong>External Account Transfer Instructions:</strong></p>
<p class="instructions">
			<s:text name="jsp.exttransferaccount_msg_4"/>
		</p>
<p class="instructions">
			<s:text name="jsp.exttransferaccount_msg_3"/>
		</p>
<p class="instructions">
		<s:url id="termsURL" value="%{#session.externalXferURL}" escapeAmp="false" />
		<s:a href="%{termsURL}" target="_blank"><s:text name="jsp.exttransferaccount_download_enrollment_form"/></s:a>
		</p>
</div>
<div class="marginTop20"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header">External Account Transfer Details</div>
   		<div class="paneContentWrapper">

	<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">
		<tr>
			<td width="50%" class="sectionsubhead ltrow2_color" nowrap>
				<s:text name="jsp.exttransferaccount_primary_account_holder"/> <span class="required">*</span>
			</td>
			<td width="50%" class="sectionsubhead ltrow2_color" nowrap>
				<s:text name="jsp.exttransferaccount_joint_account_holder"/>
			</td>
		</tr>
		<tr>
			<td class="sectionsubhead ltrow2_color" nowrap>
				<input id="primaryAcctHolder" type="text" name="extTransferAccount.primaryAcctHolder" size="37" class="txtbox ui-widget-content ui-corner-all" value="<s:property value="extTransferAccount.primaryAcctHolder"/>">
			</td>
			<td class="sectionsubhead ltrow2_color" nowrap>
				<input id="secondaryAcctHolder" type="text" name="extTransferAccount.secondaryAcctHolder" size="37" class="txtbox ui-widget-content ui-corner-all" value="<s:property value="extTransferAccount.secondaryAcctHolder"/>">
			</td>
		</tr>
		<tr>
			<td><span id="AddExtTransferAccount.PrimaryAcctHolderError"></span></td>
			<td></td>
		</tr>
		<input type="hidden" name="extTransferAccount.routingNumber">
		<tr>
			<td class="sectionsubhead ltrow2_color" nowrap>
				<s:text name="jsp.exttransferaccount_financial_institution"/><span class="required">*</span>
			</td>
			<td class="sectionsubhead ltrow2_color" nowrap>
				<div style="margin:0 50px 0 0; display:inline-block; width:110px"><s:text name="jsp.default_20"/><span class="required">*</span></div>
				<s:text name="jsp.default_125"/><span class="required">*</span>
			</td>
		</tr>
		<tr>
			<td class="sectionsubhead ltrow2_color" nowrap valign="top">
				<input id="bankName" type="text" name="extTransferAccount.bankName" size="37" class="txtbox ui-widget-content ui-corner-all" value="<s:property value="extTransferAccount.bankName"/>">
			</td>
			<td class="sectionsubhead ltrow2_color" nowrap>
				<div style="margin:0 50px 0 0; display:inline-block; width:110px"><select id="accountType" class="txtbox" name="extTransferAccount.type"></select></div>
				<select id="currencyCode" class="txtbox" name="extTransferAccount.currencyCode"></select>
			</td>
		</tr>
		<tr>
			<td ><span id="AddExtTransferAccount.BankNameError"></span></td>
			<td></td>
		</tr>

		<tr>
			<td class="sectionsubhead ltrow2_color" nowrap>
				<s:text name="jsp.default_19"/><span class="required">*</span>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_ACH %>"><s:text name="jsp.exttransferaccount_3_to_17_digits"/></ffi:cinclude>
			</td>
			<td class="sectionsubhead ltrow2_color" nowrap>
				<s:text name="jsp.exttransferaccount_confirm_account_number"/><span class="required">*</span>
			</td>
		</tr>
		<tr>
			<td class="sectionsubhead ltrow2_color" nowrap>
				<input id="accountNumber" type="text" name="extTransferAccount.number" size="37" class="txtbox ui-widget-content ui-corner-all" value="<s:property value="extTransferAccount.number"/>">
			</td>
			<td class="sectionsubhead ltrow2_color" nowrap>
				<input id="accountNumberVerify" type="text" name="extTransferAccount.accountNumberVerify" size="37" class="txtbox ui-widget-content ui-corner-all" value="<s:property value="extTransferAccount.accountNumberVerify"/>">
			</td>
		</tr>
		<%-- QTS 477640: we only want to display this message if Routing Number or Bank Identifier because it has to do with displaying
    a check image and the text includes a graphic for where the account number should be on the check --%>
             <ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_ACH %>">
					<ffi:cinclude value1="ETFSWIFT" value2="<%=bankID%>" operator="notEquals">
						<tr>
							<td colspan="2">
									<span style="font-size:11px; font-style:italic"><s:text name="jsp.exttransferaccount_account_number_msg">
										<s:param value="%{'<img src=\"/cb/web/multilang/grafx/account_number.gif\" width=\"8\" height=\"13\">'}"></s:param>
									</s:text></span>
							</td>
						</tr>
                 </ffi:cinclude>
             </ffi:cinclude>
		<tr>
			<td><span id="AddExtTransferAccount.NumberError"></span></td>
			<td><span id="AddExtTransferAccount.AccountNumberVerifyError"></span></td>
		</tr>
		
		<tr>
			<ffi:cinclude value1="ETFACH" value2="<%=bankID%>" operator="equals">
				<td class="sectionsubhead ltrow2_color" nowrap><s:property value="bankIdentifierDisplayText"/><span class="required">*</span>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_ACH %>">
				
					<s:if test="%{extTransferAccount.bankIdentifierFlag == false}">
							<span style="font-size:11px; font-style:italic">&nbsp;(
								<s:text name="jsp.exttransferaccount_routing_number_msg">
									<s:param value="%{'<img src=\"/cb/web/multilang/grafx/routing_number.gif\" width=\"8\" height=\"13\">'}"></s:param>
								</s:text>
							)</span>
					</s:if>
           	   </ffi:cinclude></td>
			</ffi:cinclude>
			<ffi:cinclude value1="ETFSWIFT" value2="<%=bankID%>" operator="equals">
				<td class="sectionsubhead ltrow2_color" nowrap>
					<s:text name="jsp.exttransferaccount_swift_bic"/><span class="required">*</span> <span><s:text name="jsp.exttransferaccount_8_or_11_characters"/></span>
				</td>
			</ffi:cinclude>
			<ffi:cinclude value1="ETFBOTH" value2="<%=bankID%>" operator="equals">
				<td class="sectionsubhead ltrow2_color" nowrap>
					<s:text name="jsp.exttransferaccount_bank_identifier"/><span class="required">*</span>
				</td>
		</ffi:cinclude>
		</tr>
		<tr>
			<ffi:cinclude value1="ETFACH" value2="<%=bankID%>" operator="equals">
			<td class="sectionsubhead ltrow2_color" nowrap colspan="1">
				<input type="radio" name="extTransferAccount.acctBankIDType" value="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_FEDABA%>" checked style="display:none">
                <input type="hidden" name="extTransferAccount.isBankIdentifier" value="<%="" + isRegionalBank%>">
				<input type="radio" name="extTransferAccount.acctBankIDType" value="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%>" style="display:none">
				<input type="text" name="FedABARoutingNumber" size="37" maxlength="37" class="txtbox ui-widget-content ui-corner-all" value="<s:property value="extTransferAccount.routingNumber"/>">		
				<s:text name="jsp.exttransferaccount_nine_digits"/>
			</td>
			</ffi:cinclude>
			<ffi:cinclude value1="ETFSWIFT" value2="<%=bankID%>" operator="equals">
				<td class="sectionsubhead ltrow2_color" nowrap colspan="2">
					<input type="radio" name="extTransferAccount.acctBankIDType" value="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_FEDABA%>" style="display:none">
					<input type="radio" name="extTransferAccount.acctBankIDType" value="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%>" checked style="display:none">
					<input type="text" name="SWIFTRoutingNumber" size="16" maxlength="11" class="txtbox ui-widget-content ui-corner-all" value="<s:property value="extTransferAccount.routingNumber"/>">
				</td>
			</ffi:cinclude>
			<ffi:cinclude value1="ETFBOTH" value2="<%=bankID%>" operator="equals">
				<td>
					<input type="radio" name="extTransferAccount.acctBankIDType" value="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_FEDABA%>" checked onClick="updateCurrencyAndType();" />
                             <input type="hidden" name="extTransferAccount.isBankIdentifier" value="<%="" + isRegionalBank%>"/>
					 <s:property value="bankIdentifierDisplayText"/>
					&nbsp;
					<s:set var="acctBankIDTypeText"><%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%> </s:set>
					<s:set var="acctBankIDType" value="extTransferAccount.acctBankIDType" />
					<s:if test="%{#acctBankIDType != #acctBankIDTypeText}">
						<s:set var="displayValue" value="extTransferAccount.routingNumber" />
                        <s:if test="%{#displayValue == null}">
							<s:set var="displayValue" value=""/>
						</s:if>
					</s:if>
					
					<input maxlength="37" class="txtbox ui-widget-content ui-corner-all" type="text" name="FedABARoutingNumber" value="<s:property value="#displayValue"/>" size="37" border="0">
<%-- QTS 477640: we only want to display this message if Routing Number or Bank Identifier because it has to do with displaying
    a check image and the text includes a graphic for where the account number should be on the check --%>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_ACH %>">
			<s:if test="%{extTransferAccount.bankIdentifierFlag == false}">
				<div class="marginTop10 marginBottom10">
						<span style="font-size:11px; font-style:italic"><s:text name="jsp.exttransferaccount_routing_number_msg">
							<s:param value="%{'<img src=\"/cb/web/multilang/grafx/routing_number.gif\" width=\"8\" height=\"13\">'}"></s:param>
						</s:text></span>
				</div>
			</s:if>
             </ffi:cinclude>
            	</td>
            	<td>
					<input type="radio" name="extTransferAccount.acctBankIDType" value="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%>" onClick="updateCurrencyAndType();" />
					<s:text name="jsp.exttransferaccount_swift_bic"/>&nbsp;
					
					<s:set var="acctBankIDTypeText"><%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%> </s:set>
					<s:set var="acctBankIDType" value="extTransferAccount.acctBankIDType" />
					<s:if test="%{#acctBankIDType == #acctBankIDTypeText}">
						<s:set var="displayValue" value="extTransferAccount.routingNumber" />
                        <s:if test="%{#displayValue == null}">
							<s:set var="displayValue" value=""/>
						</s:if>
					</s:if>
					
					<input maxlength="11" class="txtbox ui-widget-content ui-corner-all" type="text" name="SWIFTRoutingNumber" value="<s:property value="#displayValue"/>" size="18" border="0" />
					<span>&nbsp;<s:text name="jsp.exttransferaccount_8_or_11_characters"/></span>
				</td>
			</ffi:cinclude>
		</tr>
		
		<ffi:cinclude value1="${baSBackendEnabledFlag}" value2="TRUE" operator="equals">
			<tr>
				<td class="greenClayBackground" align="left" valign="baseline"><s:text name="jsp.user_97"/><span class="required">*</span></td>
			</tr>
			<tr>
				<td class="columndata" valign="baseline" align="left">
					<select id="countrySelectID" class="txtbox" name="extTransferAccount.countryCode" size="1" >
						<ffi:setProperty name="SelectedUserCountry" value="${extTransferAccount.CountryCode}"/>
						<ffi:cinclude value1="${SelectedUserCountry}" value2="">
							<ffi:setProperty name="SelectedUserCountry" value="${SecureUser.Locale.ISO3Country}"/>
						</ffi:cinclude>
						<ffi:list collection="CountryList" items="item">
							<option <ffi:cinclude value1="${SelectedUserCountry}" value2="${item.CountryCode}">selected</ffi:cinclude> 
							value="<ffi:getProperty name="item" property="CountryCode"/>"><ffi:getProperty name='item' property='Name'/></option>
						</ffi:list>
						<ffi:removeProperty name="SelectedUserCountry"/>
					</select>
				</td>
			</tr>
		</ffi:cinclude>
		
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_UNVERIFIED %>">
			<tr>
				<td colspan="2" nowrap ><s:text name="jsp.exttransferaccount_ownership"/> <span class="required">*</span></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="radio" name="extTransferAccount.verifyStatusValue" value="<%=ExtTransferAccountDefines.VERIFYSTATUS_NOT_DEPOSITED%>" checked />
					<s:text name="jsp.exttransferaccount_this_is_my_own_account"/>&nbsp;
					<input type="radio" name="extTransferAccount.verifyStatusValue" value="<%=ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED%>" />
					<s:text name="jsp.exttransferaccount_this_account_belongs_to_someone_else"/>
					<span id="AddExtTransferAccount.VerifyStatusValueError"></span>
				</td>
			</tr>
		</ffi:cinclude>
		<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_UNVERIFIED %>">
			<input type="hidden" name="extTransferAccount.verifyStatusValue" value="<%=ExtTransferAccountDefines.VERIFYSTATUS_NOT_DEPOSITED%>">
		</ffi:cinclude>
		<tr>
			<td align="center" colspan="2" class="required">* <s:text name="jsp.default_240"/>
			</td>
		</tr>
<%-- QTS 477640: we only want to display this message if Routing Number or Bank Identifier because it has to do with displaying
    a check image and the text includes a graphic for where the account number should be on the check --%>
	    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_ACH %>">
			<ffi:cinclude value1="ETFSWIFT" value2="<%=bankID%>" operator="notEquals">
			<tr>
				<td colspan='2' align="center">
	 				<img src="/cb/web/grafx/check_sample.gif" >
				</td>
			</tr>
	        </ffi:cinclude>
		</ffi:cinclude>
	</table></div>
</div></div>
<div class="clearBoth"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper" style="padd">
		<div class="header"><s:text name="jsp.exttransferaccount_authorization"/></div>
		<div class="paneContentWrapper">
				<div class="txt_normal">
                             <p><!--L10NStart-->By electing to establish an external account transfer link, I understand that Fusion Bank will establish a link between my account at the other financial institution and my Fusion Bank accounts as I have designated. I authorize Fusion Bank to set up this service. In giving my authorization, I certify that I've reviewed my request carefully, and that all of the information I have provided is correct. In addition I acknowledge that I have authority over that account and authorize Fusion Bank to debit or credit my accounts as I indicate. I further acknowledge that Fusion Bank will originate <% if (isRegionalBank) { %>a<% } else { %>an ACH<% } %> transfer between my account at another financial institution to move funds into or out of my Fusion Bank accounts complying with the provisions of <% if (isRegionalBank) { %>national<% } else { %>U.S.<% } %> law.<!--L10NEnd-->
						<br>
						<s:text name="jsp.exttransferaccount_msg_6"/>
					</p>
					<p><s:text name="jsp.exttransferaccount_msg_7"/></p>
				</div>
				<div align="center">
					<input type="radio" name="extTransferAccount.IAgree" value="true">
					<s:text name="jsp.exttransferaccount_i_agree"/>&nbsp;
					<input type="radio" name="extTransferAccount.IAgree" value="false">
					<s:text name="jsp.exttransferaccount_i_disagree"/>
				</div>
				<p class="filter_totals_row">&nbsp;</p>
		</div>
	</div>
</div>
	<div class="btn-row">
		<input type="hidden" name="extTransferAccount.recipientType" value="<%= com.ffusion.beans.exttransfers.ExtTransferAccount.RECIPIENT_TYPE_PERSONAL %>" >
		<sj:a id="cancelFormButtonOnInput" buttonType="cancel"  button="true" onClickTopics="showSummary,cancelExternalTransferAccountForm"><s:text name="jsp.default_82"/></sj:a>
		&nbsp;
		<sj:a
			id="addExtTransferAccountSubmit"
			formIds="formAddExternalTransferAccount"
			formid="formAddExternalTransferAccount"
			field1="extTransferAccount.routingNumber"
			value1=""
			postaction="/cb/pages/jsp/transfers/addExtTransferAccount.action"
			targets="inputDiv"
			button="true"
			validate="true"
			validateFunction="customValidation"
			onClickTopics="postFormTopic"
			onCompleteTopics="addExtTransferAccountFormComplete"
			onSuccessTopics="successAddExtTransferAccountForm"><s:text name="jsp.default_395"/></sj:a> 
	</div>
</ffi:cinclude>
</s:form>
<script type="text/javascript">
<!--
updateCurrencyAndType();
//-->
</script>
<%-- ================= MAIN CONTENT END ================= --%>

<ffi:removeProperty name="SetExtTransferAccount"/>
<ffi:removeProperty name="externalXferURL" />
<ffi:removeProperty name="baSBackendEnabledFlag" />