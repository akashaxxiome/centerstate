<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<ffi:cinclude value1="${AddEditTransferTemplate}" value2="" operator="notEquals" >
    <ffi:cinclude value1="${isRecurring}" value2="true">
        <ffi:setProperty name="AddEditTransferTemplate" property="Validate" value="TEMPLATENAME,AMOUNT,FROMACCOUNT,TOACCOUNT,FREQUENCY,NUMBERTRANSFERS"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${isRecurring}" value2="false">
        <ffi:setProperty name="AddEditTransferTemplate" property="Validate" value="TEMPLATENAME,AMOUNT,FROMACCOUNT,TOACCOUNT"/>
    </ffi:cinclude>
    <ffi:setProperty name="AddEditTransferTemplate" property="Process" value="true"/>
    <ffi:process name="AddEditTransferTemplate" />
</ffi:cinclude>

<ffi:cinclude value1="${DeleteTransferTemplate}" value2="" operator="notEquals" >
    <ffi:process name="DeleteTransferTemplate"/>
</ffi:cinclude>

<ffi:removeProperty name="AddEditTransferTemplate" />
<ffi:removeProperty name="DeleteTransferTemplate" />
<ffi:cinclude value1="${ProcessAndClose}" value2="true" operator="notEquals">
    <ffi:include page="${PathExt}payments/accounttransfertempedit.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${ProcessAndClose}" value2="true" operator="equals">
    <ffi:removeProperty name="ProcessAndClose" />
    
	<%-- Clear paging session ID so that BPW will refresh the list and display the added transfer template on the summary page --%>
	<ffi:setProperty name="GetAllTransferTemplates" property="PagingContext.SessionId" value=""/>


    <script>self.close();</script>
</ffi:cinclude>
