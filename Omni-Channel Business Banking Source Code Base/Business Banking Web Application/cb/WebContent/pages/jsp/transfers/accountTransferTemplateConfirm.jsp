<%@ page import="com.ffusion.beans.banking.TransferDefines"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="java.lang.Integer"%>

<script type="text/javascript">
	function loadTemplate(){
		var id = $('#id').val();
		var recId = $('#recId').val();
		if($.trim(recId) ==""){
			recId = id;
		}
		var templateType = $('#transferType').val();
		var urlString = "/cb/pages/jsp/transfers/AddTransferAction_initTemplateLoad.action?LoadFromTransferTemplate="+id+":"+recId+":"+templateType;
		ns.transfer.loadSingleTransferTemplate(urlString);
	}
	
	$(document).ready(function(){
		// if status is not active disable load template
		var isValid = $('#isValid').val();
		if(isValid!='true'){
			$('#disabledTemplateSave').show();
			$('#templateSaveAsID').hide();
		}else{
			$('#disabledTemplateSave').hide();
			$('#templateSaveAsID').show();
		}
	});
	
</script>
<%--Set the variables to be used on the JSP --%>
<s:set var="fromPortalPage" value="#parameters.fromPortalPage" />
<s:if test="%{#request.flowType=='NewTransfer'}">
	<s:set var="transfer" value="%{#session.newTransferTemplate}" />
</s:if>
<s:else>
	<s:set var="transfer" value="#session.editTransferTemplate" />
</s:else>

<div class="leftPaneWrapper" id="templateForSingleTransferVerify" role="form" aria-labelledby="templateSummary">
<div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="templateSummary"><s:text name="jsp.default.templates.summary" /></h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%" class="floatleft">
			<span id="confirmTransfer_templateNameLabelId" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_416" />: </span>
			<span id="confirmTransfer_templateNameValueId" class="inlineSection floatleft labelValue"><s:property value="#transfer.templateName" /></span>
		</div>
	</div>
</div>
</div>
<div class="confirmPageDetails">
<ffi:help id="payments_accounttransferconfirm" />
<%--Transfer Template Confirm Form --%>
<input type="hidden" value="<s:property value="%{#transfer.ID}"/>" id="id">
<input type="hidden" value="<s:property value="%{#transfer.recTransferID"/>" id="recId">
<input type="hidden" value="<s:property value="%{#transfer.transferType}"/>" id="transferType">
<form action="accounttransfersend.jsp" method="post" name="TransferNewSent">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />

	<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
	<div class="blockContent">
		
				<s:set var="HighlightStatus" value="false" /> 
				<s:set var="TRS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.banking.TransferStatus@TRS_FAILED_TO_TRANSFER==#transfer.status}" />
				<s:set var="TRS_INSUFFICIENT_FUNDS"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_INSUFFICIENT_FUNDS==#transfer.status}" />
				<s:set var="TRS_BPW_LIMITCHECK_FAILED"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_BPW_LIMITCHECK_FAILED==#transfer.status}" />
				<s:set var="TRS_SCHEDULED"
					value="%{@com.ffusion.beans.banking.TransferStatus@TRS_SCHEDULED==#transfer.status}" />
				<s:if test="%{#TRS_FAILED_TO_TRANSFER}">
						<s:set var="HighlightStatus" value="true" />
				</s:if> <s:if test="%{#TRS_INSUFFICIENT_FUNDS}">
						<s:set var="HighlightStatus" value="true" />
				</s:if> <s:if test="%{#TRS_BPW_LIMITCHECK_FAILED}">
						<s:set var="HighlightStatus" value="true" />
				</s:if>
				<s:set var="TRS_PENDING_APPROVAL"
						value="%{@com.ffusion.beans.banking.TransferStatus@TRS_PENDING_APPROVAL==#transfer.status}" />

				<s:if test="%{#HighlightStatus=='true'}">
					<div class="blockRow failed"><span id="confirmTransfer_statusValueId">
						<span class="sapUiIconCls icon-decline"></span>
						<s:property	value="#transfer.statusName" />
						<input type="hidden" id="isValid" value="false"/>
					</span><span id="transferResultMessage" class="floatRight"><s:text name="jsp.transfers_92" /></span></div>
				</s:if> 
				<s:else>
					<s:if test="%{#TRS_PENDING_APPROVAL}">
						<input type="hidden" id="isValid" value="false"/>
						<div class="blockRow pending"><span id="confirmTransfer_statusValueId">
							<span class="sapUiIconCls icon-pending "></span>
							<s:property value="%{#transfer.statusName}" />
						</span><span id="transferResultMessage" class="floatRight"><s:text name="jsp.transfers_92" /></span></div>
					</s:if>
					<s:else>
					<div class="blockRow completed"><span id="confirmTransfer_statusValueId">
						<input type="hidden" id="isValid" value="true"/>
						<span class="sapUiIconCls icon-accept "></span>
							<s:property value="%{#transfer.statusName}" />
						</span><span id="transferResultMessage" class="floatRight"><s:text name="jsp.transfers_92" /></span></div>
					</s:else>
				</s:else>
		</div>
	<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>	
	<s:set var="DisplayEstimatedAmountKey" value="false" />
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmTransfer_toAccountLabelId" class="sectionLabel"><s:text name="jsp.default_217" />: </span>
				<span id="confirmTransfer_fromAccountValueId"><s:property value="#transfer.fromAccount.accountDisplayText"/></span>
			</div>
			<div class="inlineBlock">
				<%--Amount Section in case of Single Currency Transfer--%>
				<s:if test="%{#transfer.UserAssignedAmountFlagName=='single'}">
						<span id="confirmTransfer_amountLabelId" class="sectionLabel"><s:text name="jsp.default_43" />: </span>
						<span id="confirmTransfer_amountValueId">
							<s:property value="#transfer.AmountValue.CurrencyStringNoSymbol" /> 
							<s:property value="#transfer.AmountValue.CurrencyCode" />
						</span>
				</s:if>
		
				<%--Amount Section in case of Mutli Currency Transfer--%>
				<s:else>
						<span id="confirmTransferFrom_amountLabelId" class="sectionLabel"><s:text name="jsp.default_489" />: </span>
						<span id="confirmTransferFrom_amountValueId">
							<s:if test="%{#transfer.IsAmountEstimated=='true'}">
                                 &#8776;<s:set var="DisplayEstimatedAmountKey" value="true" />
							</s:if> 
							<s:property value="#transfer.AmountValue.CurrencyStringNoSymbol" />
							<s:property value="#transfer.AmountValue.CurrencyCode" /></span>
				</s:else>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmTransfer_toAccountLabelId" class="sectionLabel"><s:text name="jsp.default_424" />: </span>
				<span id="confirmTransfer_toAccountValueId"><s:property value="#transfer.toAccount.accountDisplayText"/></span>
			</div>
			<div class="inlineBlock">
				<s:if test="%{#transfer.UserAssignedAmountFlagName=='single'}">
				</s:if>
				<s:else>
						<span id="confirmTransferTo_amountLabelId" class="sectionLabel"><s:text name="jsp.default_512" />: </span>
						<span id="confirmTransferTo_amountValueId">
							<s:if test="%{#transfer.IsAmountEstimated=='true'}">
                                     &#8776;<s:set var="DisplayEstimatedAmountKey" value="true" />
							</s:if> 
							<s:property value="#transfer.AmountValue.CurrencyStringNoSymbol" />
							<s:property value="#transfer.AmountValue.CurrencyCode" /></span>
				</s:else>
			</div>
		</div>
	</div>
	<div class="blockHead"><s:text name="jsp.miscellaneous.label" /></div>
	<div class="blockContent">
		<div class="blockRow"><%--Transfer Type Section(Internal to Internal, External to Internal,Internal to External) --%>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
				<span id="confirmTransfer_typeLabelId" class="sectionLabel">
					<s:set var="TRANSFER_BOOK" value="%{@com.ffusion.beans.banking.TransferDefines@TRANSFER_BOOK==#transfer.transferDestination}" />
					<s:set var="TRANSFER_ITOE" value="%{@com.ffusion.beans.banking.TransferDefines@TRANSFER_ITOE==#transfer.transferDestination}" />
					<s:set var="TRANSFER_ETOI" value="%{@com.ffusion.beans.banking.TransferDefines@TRANSFER_ETOI==#transfer.transferDestination}" />
					<s:set var="TRANSFER_ITOI" value="%{@com.ffusion.beans.banking.TransferDefines@TRANSFER_ITOI==#transfer.transferDestination}" />
					<s:text name="jsp.default_444" />: 
				</span>
				<span id="confirmTransfer_typeValueId">
					<s:if test="%{#TRANSFER_ITOI}">
						<s:text name="jsp.transfers_53" />
					</s:if> <s:if test="%{#TRANSFER_BOOK}">
						<s:text name="jsp.transfers_53" />
					</s:if> <s:if test="%{#TRANSFER_ITOE}">
						<s:text name="jsp.transfers_52" />
					</s:if> <s:if test="%{#TRANSFER_ETOI}">
						<s:text name="jsp.transfers_44" />
					</s:if>
				</span>
		</ffi:cinclude>
		</div>
		<s:if test="%{memo!=''}">
			<div class="blockRow">
				<%--Transfer Memo Section --%>
				<s:set var="TransferMemo" value="%{#transfer.memo}" />
						<span id="confirmTransfer_memoLabelId" class="sectionLabel"><s:text name="jsp.default_279" /></span>
						<span id="confirmTransfer_memoValueId"><s:property value="#transfer.memo" /></span>
			</div>
		</s:if>
	</div>
	<s:if test="%{#request.isRecurring=='true'}">
		<div  class="blockHead"><s:text name="jsp.recurrence.label" /></div>
		<div class="blockContent">
			<div class="blockRow">
				<%--Rec Transfer Frequency + Transfer Count --%>
				<%--Transfer Frequency --%>
					<span id="confirmTransfer_recuringLabelId" class="sectionLabel"><s:text name="jsp.default_351" />: </span>
					<span id="confirmTransfer_recuringValueId"><s:property value="#transfer.frequency" /></span>
			</div>
			<div class="blockRow">
				<%--Rec Transfer Frequency + Transfer Count --%>
				<%--Transfer Count --%>
					<span id="confirmTransfer_noOfTransfersLabelId" class="sectionLabel"><s:text name="jsp.default_443" />: </span>
					<span id="confirmTransfer_noOfTransfersValueId">
						<s:if test="%{#transfer.NumberTransfers==999}">
							<s:text name="jsp.default_446" />
						</s:if> 
						<s:else>
							<s:property value="#transfer.numberTransfers" />
						</s:else>
					</span>
			</div>
		</div>
	</s:if>
	<%--Show message if the amount is estimated --%>
	<s:if test="%{#DisplayEstimatedAmountKey=='true'}">
		<div class="mltiCurrenceyMessage">
					<span class="required">&#8776; <s:text name="jsp.default_241" /></span>
		</div>
	</s:if>	
	<%--Confirm Transfer Template Form Buttons --%>
	<div class="btn-row">
			<s:if test="%{#fromPortalPage=='true'}">
					<%-- All topics subscribed here can be found in pendingTransactionPortal.js --%>
					<%-- onClickTopics="closeConfirmTransferDialog"  commenting this topic, as it will be called
												on closeDialog topic to call functionality on X button click of dialog--%>
					<sj:a id="transferDoneButton" button="true"
						onClickTopics="closeConfirmTransferDialog">
						<s:text name="jsp.default_175" />
					</sj:a>
			</s:if> <s:else>
					<sj:a id="transferTemplateDoneBtn" button="true" summaryDivId="summary" buttonType="done" onClickTopics="showSummary,cancelTemplatesForm">
						<s:text name="jsp.default_175" />
					</sj:a>
			</s:else>
	</div>
</form>
</div>