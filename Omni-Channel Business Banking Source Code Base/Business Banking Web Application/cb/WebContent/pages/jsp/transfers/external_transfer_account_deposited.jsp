<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="com.ffusion.beans.exttransfers.ExtTransferAccount,
				 com.ffusion.csil.core.common.EntitlementsDefines,
				 com.ffusion.beans.exttransfers.ExtTransferAccountDefines"%>
<ffi:help id="payments_agg-add-account-step3"  className="moduleHelpClass" />
<span id="PageHeading" style="display:none;"><s:text name="jsp.exttransferaccount_deposit_verification_underway" /></span>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<%-- ================ MAIN CONTENT START ================ --%>

<s:form id="formExtTransferAccountDeposited" name="formExtTransferAccountDeposited" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<p align="center">
	<ffi:cinclude value1="${ExternalTransferACCOUNT.VerifyStatusString}" value2="<%=String.valueOf(ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED)%>" operator="equals">
		<ffi:setProperty name="isExtTransferAdded" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${ExternalTransferACCOUNT.VerifyStatusString}" value2="<%=String.valueOf(ExtTransferAccountDefines.VERIFYSTATUS_NOT_REQUIRED)%>" operator="equals">
		<ffi:setProperty name="isExtTransferAdded" value="true"/>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${isExtTransferAdded}" value2="true" operator="equals">
		<s:text name="jsp.exttransferaccount_successfully_added" /><br>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${isExtTransferAdded}" value2="true" operator="notEquals">
		<s:text name="jsp.exttransferaccount_msg_9" /><br>
		<s:text name="jsp.exttransferaccount_msg_10" />
	</ffi:cinclude>
	
	<%-- <ffi:cinclude value1="${ExternalTransferACCOUNT.VerifyStatusString}" value2="<%=String.valueOf(ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED)%>" operator="equals">
	<s:text name="jsp.exttransferaccount_successfully_added" /><br>
	</ffi:cinclude>
	<ffi:cinclude value1="${ExternalTransferACCOUNT.VerifyStatusString}" value2="<%=String.valueOf(ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED)%>" operator="notEquals">
	<s:text name="jsp.exttransferaccount_msg_9" /><br>
	<s:text name="jsp.exttransferaccount_msg_10" />
	</ffi:cinclude> --%>
</p>
<div class="leftPaneWrapper">
<div class="leftPaneInnerWrapper">
		<div class="header">External Account Transfer Summary</div>
		<div id="" class="paneContentWrapper summaryBlock">
			<div>
				<span class="sectionsubhead sectionLabel floatleft inlineSection">
					<s:text name="jsp.default_16" />:</span>
				<span class="inlineSection floatleft labelValue"> <ffi:getProperty name="ExternalTransferACCOUNT" property="ConsumerMenuDisplayText"/>
					&nbsp;<ffi:getProperty name="ExternalTransferACCOUNT" property="CurrencyCode"/>
				</span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection">
					<s:text name="jsp.exttransferaccount_primary_account_holder"/>:</span>
				<span class="inlineSection floatleft labelValue"> <ffi:getProperty name="ExternalTransferACCOUNT" property="PrimaryAcctHolder"/>
				</span>
			</div>
		</div>
	</div>
</div>

<div class="confirmPageDetails">
<div class="blockWrapper label150">
	<div  class="blockHead">External Account Transfer Details</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<ffi:cinclude value1="${ExternalTransferACCOUNT.AcctBankIDType}" value2="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%>" operator="notEquals">
					<span class="sectionsubhead sectionLabel">
						<s:text name="jsp.default_63" />/
						  <ffi:getProperty name="TempBankIdentifierDisplayText"/>
						:
					</span>
				</ffi:cinclude>
				<ffi:cinclude value1="${ExternalTransferACCOUNT.AcctBankIDType}" value2="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%>" operator="equals">
					<span class="sectionsubhead sectionLabel">
						<s:text name="jsp.default_63" />/<s:text name="jsp.exttransferaccount_swift_bic" />:
					</span>
				</ffi:cinclude>
				<span><ffi:getProperty name="ExternalTransferACCOUNT" property="BankName"/> / <ffi:getProperty name="ExternalTransferACCOUNT" property="RoutingNumber"/></span>
			
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_294" />:
				</span>
				<span> <ffi:getProperty name="ExternalTransferACCOUNT" property="Nickname"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_20" />:
				</span>
				<span> <ffi:getProperty name="ExternalTransferACCOUNT" property="Type"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.exttransferaccount_ownership" />:</span>
				<ffi:cinclude value1="${ExternalTransferACCOUNT.VerifyStatusString}" value2="<%=String.valueOf(ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED)%>" operator="notEquals">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.exttransferaccount_this_is_my_own_account" /></span>
				</ffi:cinclude>
				<ffi:cinclude value1="${ExternalTransferACCOUNT.VerifyStatusString}" value2="<%=String.valueOf(ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED)%>" operator="equals">
					<span><s:text name="jsp.exttransferaccount_this_account_belongs_to_someone_else" /></span>
				</ffi:cinclude>
			</div>
		</div>
	</div>
</div>
<div class="btn-row">
	<sj:a id="amountDepositedButton" button="true" onClickTopics="cancelExternalTransferAccountForm"><s:text name="jsp.default_303"/></sj:a>
</div></div>
</s:form>

<%-- ================= MAIN CONTENT END ================= --%>

<ffi:removeProperty name="SetExtTransferAccount"/>

				 