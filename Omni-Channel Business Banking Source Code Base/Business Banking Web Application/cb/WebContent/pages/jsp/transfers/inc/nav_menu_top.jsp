<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="TabSection" value="pmtTran"/>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="menuBackgroundDarkBlue">
    <tr>
        <s:include value="/cb/pages/jsp/inc/nav_menu_tabs.jsp"/>
        <td width="100%">&nbsp;</td>
    </tr>
</table>
