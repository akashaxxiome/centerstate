<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page
	import="com.ffusion.beans.accounts.Accounts,
					com.ffusion.csil.core.common.EntitlementsDefines,
					com.ffusion.beans.banking.TransferStatus"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<script language="JavaScript" type="text/javascript">
<!--


var textarea_transfer_maxlen = {
		  isMax : function (){

			var textarea =
				document.getElementById("transferTemplateComment");
			var max_length = 985;
			if(textarea.value.length > max_length){
			  textarea.value =
				textarea.value.substring(0, max_length);
				document.getElementById("EditTransfer.TemplateCommentError").innerHTML=js_template_comment_error;
			}
		  },
		  disabledRightMouse : function (){
			document.oncontextmenu =
			  function (){ return false; }
		  },
		  enabledRightMouse : function (){
			document.oncontextmenu = null;
		  }
		};
		
		
$(function() {
	
	$("#FromAccount").lookupbox({
		"source":"/cb/pages/common/TransferAccountsLookupBoxAction.action",
		"controlType":"server",
		"collectionKey":"1",
		"size":"65",
		"pairTo":"ToAccount",
		"module":"transfer",
		"boxType":"from",
	});
	
	$("#ToAccount").lookupbox({
		"source":"/cb/pages/common/TransferAccountsLookupBoxAction.action",
		"controlType":"server",
		"collectionKey":"1",
		"size":"65",
		"pairTo":"FromAccount",
		"module":"transfer",
		"boxType":"to"
	});
	onLoadCheck();
	// disable Repeating frequency by default, enable only if rec model is being edited and hide once rec model editing is selected
	if($('#isRecModelEdit').val()=='false'){
		 $("#single_transfer_frequency_edit").attr('disabled',true);
		 /* var repeatingWeeks = $("#single_transfer_frequency_edit").attr('value');
		/*var repeatingNum = */
			
		/*$("#repaetingTransfers").html(repeatingWeeks + " &#45; " );
		$("#editOrginalRecModel").hide();
		$("#editRecModel").show();*/
	}else{
		$('#loadRecModelFormButton').hide();
		/* $("#editRecModel").hide();
		$("#editOrginalRecModel").show(); */
	}
 });

function tempHistoryDisplay(show)
{
    if (show)
    {
       document.getElementById("tempHistory").style.display = "inline";
       document.getElementById("viewTempHistory").style.display = "none";
       document.getElementById("hideTempHistory").style.display = "inline";
	   $("#hideTempHistory").focus();
    }
    else
    {
       document.getElementById("tempHistory").style.display = "none";
       document.getElementById("viewTempHistory").style.display = "inline";
       document.getElementById("hideTempHistory").style.display = "none";
	   $("#viewTempHistory").focus();
    }
}

function toggleRecurring(){
//debugger;
	<s:if test="%{#isRecurring}">
		if (document.TransferEdit.openEnded[1].checked == true){
			// disable number of payment field by default, enable only if rec model is being edited
			if($('#isRecModelEdit').val()=='true'){
				document.TransferEdit.numberTransfers.disabled = false;	
			}else {
				<s:if test="%{#isTemplate!='true'}">
					document.TransferEdit.numberTransfers.disabled = true;
					document.TransferEdit.openEnded[0].disabled = true;
					document.TransferEdit.openEnded[1].disabled = true;
				</s:if>
				
				<s:if test="%{#isTemplate=='true'}">
					document.TransferEdit.numberTransfers.disabled = false;
					document.TransferEdit.openEnded[0].disabled = false;
					document.TransferEdit.openEnded[1].disabled = false;
				</s:if>
			}
		} else {
			document.TransferEdit.numberTransfers.disabled = true;
		}
	</s:if>
}

function modifyFrequencyOnLoad() {
	<s:if test="%{#isRecurring}">
		document.TransferEdit.openEnded[1].checked = true;
		if (document.TransferEdit['numberTransfers'].value=="999") {
			document.TransferEdit.openEnded[0].checked = true;
			document.TransferEdit.openEnded[1].checked = false;
			document.TransferEdit.numberTransfers.value = "";
		}
		modifyFrequency();
	</s:if>
}

function modifyFrequency( ){
	<s:if test="%{#isRecurring}">
		if (document.TransferEdit.frequency.value == "None"){
			document.TransferEdit.openEnded[0].disabled = true;
			document.TransferEdit.openEnded[1].disabled = true;
			document.TransferEdit.openEnded[0].checked = false;
			document.TransferEdit.openEnded[1].checked = true;
			document.TransferEdit.numberTransfers.disabled = true;
			document.TransferEdit.numberTransfers.value="1";
		} else {
			document.TransferEdit.openEnded[0].disabled = false;
			document.TransferEdit.openEnded[1].disabled = false;
			toggleRecurring();
		}
	</s:if>
}

<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
function checkCurrency(event)
{
    if (event == "onload" || event == "onchange")
    {
        var fromSel = document.getElementById("FromAccount");
        var toSel = document.getElementById("ToAccount");
        var currencies = getCurrencies(fromSel, toSel);
        


        if ((currencies[0] != null) && (currencies[1] != null)) // both accounts selected
        {
        	
            if (currencies[0] != currencies[1]) // multi-currency
            {
               // build currency menu based on selected account currencies
                var currencySel = document.getElementById("CurrencySelect");
                currencySel.options[1] = new Option(currencies[0], "from");
                currencySel.options[2] = new Option(currencies[1], "to");

                if (event == "onload") // select currency in drop-down menu for onload event
                {
                    var userAssigned = '<s:property value="#transfer.UserAssignedAmountFlagName"/>';
                    if (userAssigned == "from")
                        currencySel.options[1].selected = true;
                    else if (userAssigned == "to")
                        currencySel.options[2].selected = true;
                }

              	//Create selectmenu widget
				 if(ns.common.isInitialized($('#CurrencySelect'),'ui-selectmenu')){
                	$("#CurrencySelect").selectmenu('destroy');
                }
                $("#CurrencySelect").selectmenu({'width':80});
                
                // hide currency label
                document.getElementById("CurrencyLabel").style.display = "none";

                // display currency drop-down menu
                document.getElementById("CurrencySection").style.display = "";
                document.getElementById("CurrencySectionLabel").style.display = "";
            }
            else // single currency
            {
                // hide currency drop-down menu
                document.getElementById("CurrencySection").style.display = "none";
                document.getElementById("CurrencySectionLabel").style.display = "none";

                // set and display single currency label
                document.getElementById("CurrencyLabel").firstChild.nodeValue = currencies[0];
                document.getElementById("CurrencyLabel").style.display = "";
            }
        }
        else // both accounts not selected
        {
            // hide currency label and drop-down menu
            document.getElementById("CurrencyLabel").style.display = "none";
            document.getElementById("CurrencySection").style.display = "none";
            document.getElementById("CurrencySectionLabel").style.display = "none";
        }
    }
    else if (event == "onsubmit")
    {
        var currencySel = document.getElementById("CurrencySelect");

        if (document.getElementById("CurrencySection").style.display == "") // currency section displayed
        {
        	if (currencySel.selectedIndex == 1)
            {
                document.getElementById("Amount1").name = "amount";
                document.getElementById("OtherAmount").name = "toAmount";
            }
            else if (currencySel.selectedIndex == 2)
            {
                document.getElementById("Amount1").name = "toAmount";
                document.getElementById("OtherAmount").name = "amount";
            }
        }
        else // currency section hidden, single-currency transfer
        {
			document.getElementById("Amount1").name = "amount";
            document.getElementById("OtherAmount").name = "toAmount";
            // select first option in currency drop-down menu which has a value of single
            currencySel.options[0].selected = true;
        }
    }
}
</ffi:cinclude>

function onLoadCheck()
{
    var fromSel = document.getElementById("FromAccount");
    var toSel = document.getElementById("ToAccount");

    modifyFrequencyOnLoad();

    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
    checkCurrency('onload');
    </ffi:cinclude>
}

function onChangeCheck()
{
	var fromAccountDisplayText = $.trim($('select#FromAccount option:selected').html());
	var toAccountDisplayText = $.trim($('select#ToAccount option:selected').html());
	
    if(document.getElementById("FromAccount").getAttribute("name")=="fromAccountID"){
    	$("#editFromAcct").text(fromAccountDisplayText);
    }
    
    if(document.getElementById("ToAccount").getAttribute("name")=="toAccountID"){
    	$("#editToAcct").text(toAccountDisplayText);
    }

    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
    checkCurrency('onchange');
    </ffi:cinclude>

    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
    setTransferDestination();
    </ffi:cinclude>
}

<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
function setTransferDestination()
{
    var fromAcct = document.getElementById("FromAccount").value;
    var toAcct = document.getElementById("ToAccount").value;

    var marker = fromAcct.indexOf(":");
    marker = fromAcct.indexOf(":", marker+1);
    var fromExternal = (marker != -1);

    marker = toAcct.indexOf(":");
    marker = toAcct.indexOf(":", marker+1);
    var toExternal = (marker != -1);

    if (fromExternal)
    {
        document.getElementById("TransferDest").value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>";
    }
    else if (toExternal)
    {
        document.getElementById("TransferDest").value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>";
    }
    else // internal transfer
    {
        var transfertype = "<ffi:getProperty name='transfer' property='TransferType'/>";
        if (transfertype == "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_TYPE_TEMPLATE %>" ||
			transfertype == "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_TYPE_RECTEMPLATE %>")
        {
            document.getElementById("TransferDest").value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOI %>";
        }
        else // actual transfer
        {
            document.getElementById("TransferDest").value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_BOOK %>";
        }
    }
}
</ffi:cinclude>

function onSubmitCheck(submitRefresh)
{
	 if (submitRefresh)
	 {
	     document.TransferEdit.submit();
	 }
	 else
	 {
	 	<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
	    checkCurrency('onsubmit');
	    </ffi:cinclude>

	    return true;
	}
}

function loadRecModel(){
	//debugger
	// /cb/pages/jsp/transfers/editTransferAction_input.action?transferID={0}&transferType={1}&recTransferID={2}" parm0="ID" parm1="TransferType" parm2="RecTransferID
	var fromPortal = false;
	<s:if test="fromPortal=='true'">
		fromPortal = true;
	</s:if>	
	if(!fromPortal) {
		var url = "/cb/pages/jsp/transfers/editTransferAction_input.action?isRecModel=true&transferType=" + $('#transferType').val() + "&recTransferID="+$('#recTransferID').val();
		ns.transfer.editSingleTransfer(url);
	} else {
		var url = "/cb/pages/jsp/transfers/editTransferAction_input.action?isRecModel=true&fromPortal=true&fromPendingTransfers=true&transferType=" + $('#transferType').val() + "&recTransferID="+$('#recTransferID').val();
		ns.pendingTransactionsPortal.editSingleTransfer(url);
	}
}
// --></script>
