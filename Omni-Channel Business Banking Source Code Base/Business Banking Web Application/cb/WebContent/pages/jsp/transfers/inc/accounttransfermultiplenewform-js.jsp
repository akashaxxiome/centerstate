<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page
	import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.beans.banking.TransferBatch"%>
<%@ page import="com.ffusion.beans.banking.Transfers"%>

<% String dateFormatName = ""; %>
<ffi:getProperty name="UserLocale" property="DateFormatName"
	assignTo="dateFormatName" />
<script language="JavaScript" type="text/javascript">
<!--
<s:if test="%{#request.istemplateflow=='true'}">
	var TOTAL_ROWS = <s:property value="#session.transferTemplateBatch.transfers.size"/>;
</s:if>
<s:else>
	var TOTAL_ROWS = <s:property value="#session.transferBatch.transfers.size"/>;
</s:else>



<s:if test="%{#request.istemplateflow=='true'}">
	var assignedAmounts = [
	<ffi:list collection="transferTemplateBatch.Transfers" items="Transfer">
	    "<ffi:getProperty name='Transfer' property='UserAssignedAmountFlagName'/>",
	</ffi:list>""
	];
</s:if>
<s:else>
	var assignedAmounts = [
	<ffi:list collection="transferBatch.Transfers" items="Transfer">
	    "<ffi:getProperty name='Transfer' property='UserAssignedAmountFlagName'/>",
	</ffi:list>""
	];
</s:else>

<s:if test="%{#request.isTemplate==true}">
var module = "template";
</s:if>
<s:else>
var module = "transfer";
</s:else>

$(function(){
   	for(var i = 0; i < TOTAL_ROWS; i++){
		
   		lookupboxFromAccount(i);
   		lookupboxToAccount(i);
		
		onLoadCheck();	
   	}
});

function lookupboxFromAccount(i) {
	$("#FromAccount"+ i).lookupbox({
		"source":"/cb/pages/common/TransferAccountsLookupBoxAction.action",
		"controlType":"server",
		"collectionKey":"1",
		"size":"40",
		"pairTo":"ToAccount"+i,
		"module":module,
		"accounts":"from",
	});
}

function lookupboxToAccount(i) {
	$("#ToAccount"+ i).lookupbox({
		"source":"/cb/pages/common/TransferAccountsLookupBoxAction.action",
		"controlType":"server",
		"collectionKey":"1",
		"size":"40",
		"pairTo":"FromAccount"+i,
		"module":module,
		"accounts":"to",
	});
}
   

$(document).ready(function() {
	
		if(transferSize==2) $("#deleteRowID").attr('style','display:none');
		if(transferSize>=TOTAL_ROWS) $("#addRowID").attr('style','display:none');

		$("#addRowID").click(function() {
			if(transferSize < TOTAL_ROWS){
				$(".columndataauto" + transferSize).show();
				$(".columnLabel" + transferSize).show();
				$(".columnError" + transferSize).show();
				$(".columnHeading" + transferSize).show();
				transferSize++;
				if(transferSize>2)
				{
				$("#deleteRowID").show();
				$("#deleteRowID").focus();
				}
				if(transferSize==TOTAL_ROWS) {
					$("#addRowID").attr('style','display:none');
				}
			}
		});

		$("#deleteRowID").click(function() {
			if(transferSize > 2){
				
				$(".columndataauto" + (transferSize-1)).hide();
				$(".columnLabel" + (transferSize-1)).hide();
				$(".columnError" + (transferSize-1)).hide();
				$(".columnHeading" + (transferSize-1)).hide();
				clearRow(transferSize-1);
				
				transferSize--;
				if(transferSize==2) {
					$("#deleteRowID").attr('style','display:none');
				}
				if(transferSize<TOTAL_ROWS){
					$("#addRowID").show();
					$("#addRowID").focus();
				}
			}
		});

});


function importFile()
{
   // we want to SUBMIT so that any changes to the payments in the collection will be saved
   document.MultipleTransferNew.action = "<ffi:getProperty name="SecureServletPath"/>AddTransferBatch";
   document.MultipleTransferNew["AddTransferBatch.SuccessURL"].value = "<ffi:getProperty name="SecurePath"/>payments/transferimport.jsp";
   onSubmitCheck(true);
}
function clearRow(index)
{
	$('#FromAccount' + index).lookupbox("destroy");
	$("#FromAccount"+ index).append('<option value="" selected="selected"></option>');
	lookupboxFromAccount(index);
	
	setTimeout(function(){
		$('#ToAccount' + index).lookupbox("destroy");
		$("#ToAccount"+ index).append('<option value="" selected="selected"></option>');
		lookupboxToAccount(index);
		},500);
	
    document.getElementById("Amount" + index).value = "";
	
    if (document.getElementById("Date" + index) != null)
    {
    		document.getElementById("Date" + index).value = "<%=dateFormatName%>";
    }
  
    document.getElementById("Memo" + index).value = "";
    document.getElementById("CategorySelect" + index).selectedIndex = 0;
    document.getElementById("CurrencyLabel" + index).style.display = "none";
    document.getElementById("CurrencySection" + index).style.display = "none";
    document.getElementById("CurrencyColumnHeader"+ index).style.display = "none";
    if (allCurrencyHidden())
        document.getElementById("CurrencyColumnHeader" + index).style.display = "none";

    calculateTotal();
    //reset date
    var defaultTransferDate = $("#hiddenDefaultDateId"+index).val();
    $("#Date"+index).val(defaultTransferDate);
    
    return false;
}

function calculateTotal()
{
    var total = 0;
    var hideTotal = false;
    var currency = null;

    for (var i = 0; i < TOTAL_ROWS; i++)
    {
        var amount = document.getElementById("Amount" + i).value;
        if (amount == "")
        {
            continue;
        }
        else // amount not empty string
        {
            amount = amount.replace(/^\s+/g, '').replace(/\s+$/g, ''); // trim whitespace
            amount = amount.replace(/^\$/, '').replace(/,/g, ''); // remove leading $ and commas

            if (isNaN(amount))
            {
                hideTotal = true; // hide total if an invalid transfer amount exists
                break;
            }
        }

        var fromSel = document.getElementById("FromAccount" + i);
        var toSel = document.getElementById("ToAccount" + i);		
        var currencies = getCurrencies(fromSel, toSel);

        if ((currencies[0] == null) || (currencies[1] == null)) // both accounts NOT selected
        {
            continue;
        }
        else // both accounts selected
        {
            if (currencies[0] != currencies[1]) // multi-currency
            {
                var currencySel = document.getElementById("CurrencySelect" + i);
                var currencyValue = currencySel.options[currencySel.selectedIndex].value;
                var currencyText = currencySel.options[currencySel.selectedIndex].text;

                if (currencyValue == "single") // currency not selected
                {
                    continue; // don't include amount in total
                }
                else if (currencyText != "USD")
                {
                    hideTotal = true; // hide total if a non-USD transfer exists
                    break;
                }
            }
            else // single currency
            {
                if (currencies[0] != "USD")
                {
                    hideTotal = true; // hide total if a non-USD transfer exists
                    break;
                }
            }
        }

        total += parseFloat(amount);
    }

    if (hideTotal)
    {
		$('#TotalTitle').hide();
		$('#TotalValue').hide();
    }
    else // all transfers same currency so display a total
    {
        total = ns.common.formatTotal(total, "USD");
        $('#TotalValue').html(total);
		$('#TotalTitle').show();
		$('#TotalValue').show();
    }
}

<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
function checkAllCurrency(event)
{
	
    if (event == "onload")
    {
        var index = 0;
        var currencySel = document.getElementById("CurrencySelect" + index);
       
        while (currencySel != null)
        {
            var fromSel = document.getElementById("FromAccount" + index);
            var toSel = document.getElementById("ToAccount" + index);
            var currencies = getCurrencies(fromSel, toSel);
          
            if ((currencies[0] != null) && (currencies[1] != null)) // both accounts selected
            {
            	
            	
                if (currencies[0] != currencies[1]) // multi-currency
                {
                   // build currency menu based on selected account currencies
                    currencySel.options[1] = new Option(currencies[0], "from");
                    currencySel.options[2] = new Option(currencies[1], "to");

                    if (event == "onload") // select currency in drop-down menu for onload event
                    {
                        var userAssigned = assignedAmounts[index];
                        if (userAssigned == "from")
                            currencySel.options[1].selected = true;
                        else if (userAssigned == "to")
                            currencySel.options[2].selected = true;
                    }
                    
                  	//Create selectmenu widget
					 if(ns.common.isInitialized($("#"+currencySel.id),'ui-selectmenu')){
                	$("#"+currencySel.id).selectmenu('destroy');
                }
                  	
                    $("#"+currencySel.id).selectmenu({'width':80});

                    // hide currency label
                    document.getElementById("CurrencyLabel" + index).style.display = "none";

                    // display currency drop-down menu and column header
                    document.getElementById("CurrencySection" + index).style.display = "";
                    document.getElementById("CurrencyColumnHeader" + index).style.display = "";
                }
                else // single currency
                {
                	 
                    // hide currency drop-down menu
                    document.getElementById("CurrencySection" + index).style.display = "none";

                    // hide currency column header
                    if (allCurrencyHidden())
                        document.getElementById("CurrencyColumnHeader"+ index).style.display = "none";

                    // set and display single currency label
                    document.getElementById("CurrencyLabel" + index).firstChild.nodeValue = currencies[0];
                    document.getElementById("CurrencyLabel" + index).style.display = "";
                }
            }
            else // both accounts not selected
            {
            	
                // hide currency label, drop-down menu, and column header
                document.getElementById("CurrencyLabel" + index).style.display = "none";
                document.getElementById("CurrencySection" + index).style.display = "none";
                if (allCurrencyHidden())
                    document.getElementById("CurrencyColumnHeader"+ index).style.display = "none";
            }

            index = index + 1;
            currencySel = document.getElementById("CurrencySelect" + index);
        }
    }
    else if (event == "onsubmit")
    {
        var index = 0;
        var currencySel = document.getElementById("CurrencySelect" + index);

        while (currencySel != null)
        {
		
                var amountInput = document.getElementById("Amount" + index);
                var amountIndex = amountInput.name.indexOf("&");
                var otherAmountInput = document.getElementById("OtherAmount" + index);
                var otherAmountIndex = otherAmountInput.name.indexOf("&");
                var bean="transferBatch";
                <s:if test="%{#request.istemplateflow=='true'}">
                bean="transferTemplateBatch";
                </s:if>
            if (document.getElementById("CurrencySection" + index).style.display == "") // currency section displayed
            {

                if (currencySel.selectedIndex == 1)
                {
                    amountInput.name =bean+ '.transfers['+index+'].amount';
                    otherAmountInput.name = bean+'.transfers['+index+'].toAmount';
                }
                else if (currencySel.selectedIndex == 2)
                {
                    amountInput.name = bean+'.transfers['+index+'].toAmount';
                    otherAmountInput.name =bean+ '.transfers['+index+'].amount';
                }
            }
            else // currency section hidden, single-currency transfer
            {
                // select first option in currency drop-down menu which has a value of single
                currencySel.options[0].selected = true;
                amountInput.name =bean+'.transfers['+index+'].amount';
                otherAmountInput.name =bean+'.transfers['+index+'].toAmount';
            }

            index = index + 1;
            currencySel = document.getElementById("CurrencySelect" + index);
        }
    }
}

function checkSingleCurrency(index)
{
    var fromSel = document.getElementById("FromAccount" + index);
	var toSel = document.getElementById("ToAccount" + index);
    var currencies = getCurrencies(fromSel, toSel);
	var tokenString=$("[name='CSRF_TOKEN']").val();
    if ((currencies[0] != null) && (currencies[1] != null)) // both accounts selected
    {
        if (currencies[0] != currencies[1]) // multi-currency
        {
		
        	// build currency menu based on selected account currencies
            var currencySel = document.getElementById("CurrencySelect" + index);
			      var currArr = [currencies[0], currencies[1]];
			
			$.getJSON('/cb/pages/utils/GetCurrencyDescriptionAction.action', {
				currencyCode : currArr[0]
			  }, function(jsonResponse1) {
			  
					 $.getJSON('/cb/pages/utils/GetCurrencyDescriptionAction.action', {
					currencyCode : currArr[1]
				  }, function(jsonResponse2) {
				  
				currencySel.options[1] = new Option(currArr[0]+' - '+jsonResponse1.currencyDesc, "from");
						currencySel.options[2] = new Option(currArr[1]+' - '+jsonResponse2.currencyDesc, "to");
							
						$("#CurrencySelect"+index).selectmenu({'width':180});
					
				  });
		  });

          	//Create selectmenu widget
			 if(ns.common.isInitialized($("#"+currencySel.id),'ui-selectmenu')){
				$("#"+currencySel.id).selectmenu('destroy');
			}
            
		        
            // hide currency label
            document.getElementById("CurrencyLabel" + index).style.display = "none";

            // display currency drop-down menu and column header
            document.getElementById("CurrencySection" + index).style.display = "";
            document.getElementById("CurrencyColumnHeader"+ index).style.display = "";
        }
        else // single currency
        {
            // hide currency drop-down menu
            document.getElementById("CurrencySection" + index).style.display = "none";

            // hide currency column header
            if (allCurrencyHidden())
                document.getElementById("CurrencyColumnHeader"+ index).style.display = "none";

            // set and display single currency label
            document.getElementById("CurrencyLabel" + index).firstChild.nodeValue = currencies[0];
            document.getElementById("CurrencyLabel" + index).style.display = "";
        }
    }
    else // both accounts not selected
    {
        // hide currency label, drop-down menu, and column header
        document.getElementById("CurrencyLabel" + index).style.display = "none";
        document.getElementById("CurrencySection" + index).style.display = "none";
        if (allCurrencyHidden())
            document.getElementById("CurrencyColumnHeader"+ index).style.display = "none";
    }
}


</ffi:cinclude>

function allCurrencyHidden()
{
    var allHidden = true;
    for (i=0; i < TOTAL_ROWS; i++)
    {
        if (document.getElementById("CurrencySection" + i).style.display == "")
            allHidden = false;
    }
    return allHidden;
}


<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
<%--
/* If the user is entitled to external transfers, and the user selects an external account
(indicated by second ":" marker in the account's option value, these scripts will automatically
set the date to two days in the future, if it's not already. */
--%>
function checkAccount(acctSel, index)
{
    var acctValue = null;

    if (acctSel == "from")
        acctValue = document.getElementById("FromAccount"+index).value;
    else
        acctValue = document.getElementById("ToAccount"+index).value;

    var marker = acctValue.indexOf(":");
    marker = acctValue.indexOf(":", marker+1);

    if (marker != -1) // external account selected
    {
        if (acctSel == "from")
            document.getElementById("TransferDest"+index).value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>";
        else
            document.getElementById("TransferDest"+index).value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>";

		<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
		removeCurrencyFromAcctIDs("FromAccount", "ToAccount", TOTAL_ROWS);
		checkAllCurrency('onsubmit');
		</ffi:cinclude>
		submitTransferFormPreProcessingExternalAccounts();
		submmitTransferForm();
	}
    else
    {
    	
        var transferDest = document.getElementById("TransferDest"+index).value;
        if (transferDest == "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>" ||
            transferDest == "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>")
        {

			<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${transferBatch.DateFormat}"/>

			// QTS 679527 solve currency select issue in new multiple transfer template page
			// validation of date
			var displayCurrencySelect = document.getElementById("Date"+index);
			if(displayCurrencySelect)
			document.getElementById("Date"+index).value = "<ffi:getProperty name="GetCurrentDate" property="Date"/>";

			document.getElementById("TransferDest"+index).value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_BOOK %>";
		}
	}
}

//Submit the form for pre-processing of external account for the batch transfer.
//Set accountFromId for the external account which is used to do transaction.
function submitTransferFormPreProcessingExternalAccounts(){
	$.ajax({
	  type: "POST",
	  url: "/cb/pages/jsp/transfers/AddTransferBatchAction_processingExternalTransfer.action",
	  data: $("#TransferMultipleNew").serialize(),
	  async: false,
	  success: function() {
	  }
	});
}

//Submit the form again and for the additional processing(external transfer)
function submmitTransferForm(){
	var urlString = "/cb/pages/jsp/transfers/accounttransfermultiplenewform.jsp?ExtAcctSelected=true&NoCreate=false";
	urlString = urlString.concat('&transferSize=',transferSize);
	$.ajax({
	  type: "POST",
	  url: urlString,
	  data: $("#TransferMultipleNew").serialize(),
	  success: function(data) {
		 $("#multipleTransferForm").html(data);
	  }
	});
}

</ffi:cinclude>

function onLoadCheck()
{ 

    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
    checkAllCurrency('onload');
    </ffi:cinclude>
    calculateTotal();
  
}

function onAccountChangeCheck(acctSel, index)
{
    var fromSel = document.getElementById("FromAccount" + index);
    var toSel = document.getElementById("ToAccount" + index);

    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>'>
    checkAccount(acctSel, index);
    </ffi:cinclude>

    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
    checkSingleCurrency(index);
    </ffi:cinclude>

    calculateTotal();
}

function onSubmitCheck(manualSubmit)
{
	//QTS 685850 : 	no need to remove currencies, handled in EditTransferBatchAction.java
    //removeCurrencyFromAcctIDs("FromAccount", "ToAccount", TOTAL_ROWS);

    if (manualSubmit)
    {
        document.MultipleTransferNew.submit();
    }
    else
    {
        <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
        checkAllCurrency('onsubmit');
        </ffi:cinclude>

        return true;
    }
}


//-->
</script>
