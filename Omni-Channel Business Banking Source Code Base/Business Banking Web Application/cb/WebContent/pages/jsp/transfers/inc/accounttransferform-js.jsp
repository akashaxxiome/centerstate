<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.bcreport.ReportLogRecord,
				 com.ffusion.beans.banking.Transfer" %>
<%@ page import="com.ffusion.beans.accounts.*" %>
<%@ page import="com.ffusion.util.enums.UserAssignedAmount, com.ffusion.beans.banking.TransferDefines"%>

<script language="JavaScript" type="text/javascript">
<!--

$(function(){
	
	var isTemplate = "<s:property value='#request.isTemplate' />"
	
	var module = "transfer";
	if(isTemplate){
		module = "template";
	}
	
	$("#FromAccount").lookupbox({
		"source":"/cb/pages/common/TransferAccountsLookupBoxAction.action",
		"controlType":"server",
		"collectionKey":"1",
		"size":"65",
		"pairTo":"ToAccount",
		"module":module,
		"accounts":"from"
		});
	
	$("#ToAccount").lookupbox({
		"source":"/cb/pages/common/TransferAccountsLookupBoxAction.action",
		"controlType":"server",
		"collectionKey":"1",
		"size":"65",
		"pairTo":"FromAccount",
		"module":module,
		"accounts":"to"
	});
	
	onLoadCheck();
	
});

function toggleRecurring(){
	if (document.TransferNew.openEnded[1].checked == true){
		document.TransferNew.numberTransfers.readOnly = false;
	} else {
		document.TransferNew.numberTransfers.readOnly = true;
	}
}

function modifyFrequencyOnLoad() {
	document.TransferNew.openEnded[1].checked = true;
	if (document.TransferNew['numberTransfers'].value=="999") {
		document.TransferNew.openEnded[0].checked = true;
		document.TransferNew.openEnded[1].checked = false;
		document.TransferNew.numberTransfers.value = "";
	}
	modifyFrequency();
}

function modifyFrequency(){

	if (document.TransferNew.frequency.value == "None"){
		document.TransferNew.openEnded[0].disabled = true;
		document.TransferNew.openEnded[1].disabled = true;
		document.TransferNew.openEnded[0].checked = false;
		document.TransferNew.openEnded[1].checked = true;
		document.TransferNew.numberTransfers.readOnly = true;
		document.TransferNew.numberTransfers.value="1";
	} else {
		document.TransferNew.openEnded[0].disabled = false;
		document.TransferNew.openEnded[1].disabled = false;
		if (document.TransferNew.numberTransfers.value == "0" ||  document.TransferNew.numberTransfers.value == "1" 
			|| document.TransferNew.numberTransfers.value == "")
			document.TransferNew.numberTransfers.value="2";
		toggleRecurring();
	}
}



<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
function checkCurrency(event)
{
	var tokenString=$("[name='CSRF_TOKEN']").val();
	
    if (event == "onload" || event == "onchange")
    {
        var fromSel = document.getElementById("FromAccount");
        var toSel = document.getElementById("ToAccount");
        var currencies = getCurrencies(fromSel, toSel);

		if ((currencies[0] != null) && (currencies[1] != null)) // both accounts selected
        {
		
            if (currencies[0] != currencies[1]) // multi-currency
            {

              // build currency menu based on selected account currencies
			 
            var currencySel = document.getElementById("CurrencySelect");
			
			      var currArr = [currencies[0], currencies[1]];
			
			$.getJSON('/cb/pages/utils/GetCurrencyDescriptionAction.action', {
				currencyCode : currArr[0]
			  }, function(jsonResponse1) {
			
					 $.getJSON('/cb/pages/utils/GetCurrencyDescriptionAction.action', {
					currencyCode : currArr[1]
				  }, function(jsonResponse2) {
				  
				  
				currencySel.options[1] = new Option(currArr[0]+' - '+jsonResponse1.currencyDesc, "from");
						currencySel.options[2] = new Option(currArr[1]+' - '+jsonResponse2.currencyDesc, "to");
							
						$("#CurrencySelect").selectmenu({'width':180});
					
				  });
		  });
		
                if (event == "onload") // select currency in drop-down menu for onload event
                {
                	 var userAssigned = '<s:property value="#newTransfer.UserAssignedAmountFlagName"/>';
                    if (userAssigned == "from")
                        currencySel.options[1].selected = true;
                    else if (userAssigned == "to")
                        currencySel.options[2].selected = true;
                }

                //Create selectmenu widget
                if(ns.common.isInitialized($('#CurrencySelect'),'ui-selectmenu')){
                	$("#CurrencySelect").selectmenu('destroy');
                }
                
               // $("#CurrencySelect").selectmenu({'width':130}); commented this line on 22 sept 2016
                
                // hide currency label
                document.getElementById("CurrencyLabel").style.display = "none";

                // display currency drop-down menu
                document.getElementById("CurrencySection").style.display = "";
                document.getElementById("CurrencySectionLabel").style.display = "";
            }
            else // single currency
            {
                // hide currency drop-down menu
                document.getElementById("CurrencySection").style.display = "none";
                document.getElementById("CurrencySectionLabel").style.display = "none";

                // set and display single currency label
                document.getElementById("CurrencyLabel").firstChild.nodeValue = currencies[0];
                document.getElementById("CurrencyLabel").style.display = "";
            }
        }
        else // both accounts not selected
        {
        	//alert("nothing selected")
            // hide currency label and drop-down menu
            document.getElementById("CurrencyLabel").style.display = "none";
            document.getElementById("CurrencySection").style.display = "none";
            document.getElementById("CurrencySectionLabel").style.display = "none";
        }
    }
    else if (event == "onsubmit")
    {
        var currencySel = document.getElementById("CurrencySelect");

        if (document.getElementById("CurrencySection").style.display == "") // currency section displayed
        {
            if (currencySel.selectedIndex == 1)
            {
            	document.getElementById("Amount1").name = "amount";
                document.getElementById("OtherAmount").name = "toAmount";
            }
            else if (currencySel.selectedIndex == 2)
            {
                document.getElementById("Amount1").name = "toAmount";
                document.getElementById("OtherAmount").name = "amount";
            }
        }
        else // currency section hidden, single-currency transfer
        {
            // select first option in currency drop-down menu which has a value of single
            currencySel.options[0].selected = true;
            document.getElementById("Amount1").name = "amount";
            document.getElementById("OtherAmount").name = "toAmount";
        }
    }
}
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>">
<%--
/* If the user is entitled to external transfers, and the user selects an external account
(indicated by second ":" marker in the account's option value, these scripts will automatically
set the date to two days in the future, if it's not already. */
--%>
function checkAccount(acctSel)
{
    var acctValue = null;

    if (acctSel == "from")
        acctValue = document.getElementById("FromAccount").value;
    else
        acctValue = document.getElementById("ToAccount").value;

    var marker = acctValue.indexOf(":");
    marker = acctValue.indexOf(":", marker+1);

    var transferDest = document.getElementById("TransferDest").value;

    if (marker != -1) // external account selected, determine if destination/date should be updated
    {
        // update destination and date if previous transfer was internal
        if (transferDest == "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_BOOK %>" ||
            transferDest == "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOI %>")
        {
            if (acctSel == "from")
                document.getElementById("TransferDest").value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>";
            else
                document.getElementById("TransferDest").value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>";
        }
    }
    else // internal account selected, determine if destination/date should be updated
    {
        // check other selected account type
        if (acctSel == "from")
            acctValue = document.getElementById("ToAccount").value;
        else
            acctValue = document.getElementById("FromAccount").value;

        marker = acctValue.indexOf(":");
        if (marker == -1)
            return;  // other account not selected so return

        marker = acctValue.indexOf(":", marker+1);
        if (marker != -1)
            return;  // other account is external so return

        // both selected accounts are internal, update destination and date if previous transfer was external
        if (transferDest == "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ETOI %>" ||
            transferDest == "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_ITOE %>")
        {
			document.getElementById("TransferDest").value = "<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_BOOK %>";
        }
	}
}
</ffi:cinclude>

function onLoadCheck()
{
    var fromSel = document.getElementById("FromAccount");
    var toSel = document.getElementById("ToAccount");

    modifyFrequencyOnLoad();

    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
		checkCurrency('onload');
    </ffi:cinclude>
}

function onChangeCheck(acctSel)
{
    var fromSel = document.getElementById("FromAccount");
    var toSel = document.getElementById("ToAccount");

    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>'>
    
    </ffi:cinclude>

    <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
		checkCurrency('onchange');
    </ffi:cinclude>
}

function onSubmitCheck(submitRefresh)
{
    if (submitRefresh)
    {
        document.TransferNew.submit();
    }
    else
    {
        <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>'>
			checkCurrency('onsubmit');
        </ffi:cinclude>

        return true;
    }
}

// --></script>