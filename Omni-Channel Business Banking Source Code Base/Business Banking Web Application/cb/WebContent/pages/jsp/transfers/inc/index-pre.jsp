<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<%-- remove template related session attributes --%>
<ffi:removeProperty name="templateEdit"/>
<ffi:removeProperty name="LoadFromTransferTemplate"/>

<%
	boolean initializeFlag = false;
	if(request.getParameter("Initialize") != null){
		initializeFlag = true;
	} // In case of portal page this will be request attribute.
	else if(request.getAttribute("Initialize") != null){
		initializeFlag = true;
	}
%>

<% if (initializeFlag){ %>
    <ffi:removeProperty name="Initialize" />



    <ffi:object id="GetTransferAccounts" name="com.ffusion.tasks.banking.GetTransferAccounts" scope="session"/>
        <ffi:setProperty name="GetTransferAccounts" property="AccountsName" value="BankingAccounts"/>
        <ffi:setProperty name="GetTransferAccounts" property="Reload" value="true"/>
    <ffi:process name="GetTransferAccounts"/>
    <ffi:removeProperty name="GetTransferAccounts"/>
    
    <ffi:object id="SetAccountsCollectionDisplayText"  name="com.ffusion.tasks.util.SetAccountsCollectionDisplayText" />
	<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CollectionName" value="<%= SessionNames.TRANSFERACCOUNTS %>"/>
	<ffi:process name="SetAccountsCollectionDisplayText"/>
	<ffi:removeProperty name="SetAccountsCollectionDisplayText"/>
	
	<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
	<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.TRANSFERACCOUNTS %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.TRANSFERACCOUNTS_INDEX %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
	<ffi:setProperty name="BuildIndex" property="BeanProperty" value="AccountDisplayText"/>
	<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
	<ffi:process name="BuildIndex"/>
	<ffi:removeProperty name="BuildIndex"/>

	
	<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
	<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.TRANSFERACCOUNTS %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.TRANSFERACCOUNTS_INDEX_BY_TYPE %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
	<ffi:setProperty name="BuildIndex" property="BeanProperty" value="Type"/>
	<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
	<ffi:process name="BuildIndex"/>
	<ffi:removeProperty name="BuildIndex"/>
	
	<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
	<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.TRANSFERACCOUNTS %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.TRANSFERACCOUNTS_INDEX_BY_CURRENCY %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
	<ffi:setProperty name="BuildIndex" property="BeanProperty" value="CurrencyCode"/>
	<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
	<ffi:process name="BuildIndex"/>
	<ffi:removeProperty name="BuildIndex"/>

    <ffi:object name="com.ffusion.tasks.banking.SetTransfer" id="SetTransfer" scope="session" />
	<ffi:object name="com.ffusion.tasks.banking.SetFundsTransaction" id="SetFundsTran" scope="session" />

	<%-- Use AllBankingAccounts collection rather than TransferAccounts so that account names/numbers are shown even if user doesn't have access to account --%>
    <ffi:object id="GetPagedPendingTransfers" name="com.ffusion.tasks.banking.GetPagedTransfers" scope="session"/>
    	<ffi:setProperty name="GetPagedPendingTransfers" property="DateFormat" value="${UserLocale.DateFormat}" />
    	<ffi:setProperty name="GetPagedPendingTransfers" property="DateFormatParam" value="${UserLocale.DateFormat}" />
    	<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedPendingTransfers" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
    	<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetPagedPendingTransfers" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
    	<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedPendingTransfers" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
    	<ffi:setProperty name="GetPagedPendingTransfers" property="ClearSortCriteria" value="" />
    	<ffi:setProperty name="GetPagedPendingTransfers" property="SortCriteriaOrdinal" value="1" />
    	<ffi:setProperty name="GetPagedPendingTransfers" property="SortCriteriaName" value="<%= com.ffusion.beans.banking.TransferDefines.SORT_CRITERIA_DATETOPOST %>" />
    	<ffi:setProperty name="GetPagedPendingTransfers" property="SortCriteriaAsc" value="False" />
    	<ffi:setProperty name="GetPagedPendingTransfers" property="ClearSearchCriteria" value="" />
    	<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals">
    		<ffi:setProperty name="GetPagedPendingTransfers" property="PageSize" value="5" />
    	</ffi:cinclude>
    	<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
			<ffi:setProperty name="GetPagedPendingTransfers" property="PageSize" value="0"/>
    	</ffi:cinclude>
    	<ffi:setProperty name="GetPagedPendingTransfers" property="BankingServiceName" value="<%= com.ffusion.tasks.banking.Task.BANKING %>" />
    	<ffi:setProperty name="GetPagedPendingTransfers" property="AccountsCollection" value="TransferAccounts" />
    	<ffi:setProperty name="GetPagedPendingTransfers" property="CollectionSessionName" value="PendingTransfers" />
    	<ffi:setProperty name="GetPagedPendingTransfers" property="Status" value="<%= com.ffusion.beans.banking.TransferDefines.TRANSFER_STATUS_PENDING %>" />
        <ffi:setProperty name="GetPagedPendingTransfers" property="DateType" value="<%= com.ffusion.beans.banking.TransferDefines.DATE_TYPE_DATE_DUE %>" />
        <ffi:setProperty name="GetPagedPendingTransfers" property="DateFormat" value="${UserLocale.DateFormat}"/>
        <ffi:setProperty name="GetPagedPendingTransfers" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
        <ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<ffi:setProperty name="GetPagedPendingTransfers" property="Type" value="CURRENT,RECURRING,REPETITIVE"/>        
        </ffi:cinclude>
        <ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
        	<ffi:setProperty name="GetPagedPendingTransfers" property="Type" value="CURRENT,RECMODEL"/>
        </ffi:cinclude>

	<%-- Check if request is from Home -> Pending Transfer portal. If so, just process task and return --%>
	<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
		<ffi:process name="GetPagedPendingTransfers"/>
		
		<ffi:object id="GetAllTransferTemplates" name="com.ffusion.tasks.banking.GetAllTransferTemplates" scope="session"/>
			<ffi:setProperty name="GetAllTransferTemplates" property="AccountsCollection" value="TransferAccounts" />
			<ffi:setProperty name="GetAllTransferTemplates" property="CollectionSessionName" value="TransferTemplates" />
			<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetAllTransferTemplates" property="NoSortImage" value='<img src="/efs/efs/multilang/grafx/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
			<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetAllTransferTemplates" property="DescendingSortImage" value='<img src="/efs/efs/multilang/grafx/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
			<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetAllTransferTemplates" property="AscendingSortImage" value='<img src="/efs/efs/multilang/grafx/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
			<ffi:setProperty name="GetAllTransferTemplates" property="ClearSortCriteria" value="" />
			<ffi:setProperty name="GetAllTransferTemplates" property="SortCriteriaOrdinal" value="1" />
			<ffi:setProperty name="GetAllTransferTemplates" property="SortCriteriaName" value="<%= com.ffusion.beans.banking.TransferDefines.SORT_CRITERIA_TEMPLATE_NICKNAME %>" />
			<ffi:setProperty name="GetAllTransferTemplates" property="SortCriteriaAsc" value="True" />
			<ffi:setProperty name="GetAllTransferTemplates" property="ClearSearchCriteria" value="" />
		    <ffi:setProperty name="GetAllTransferTemplates" property="PageSize" value="" />
		    <ffi:setProperty name="GetAllTransferTemplates" property="DateFormat" value="${UserLocale.DateFormat}"/>
		    <ffi:setProperty name="GetAllTransferTemplates" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		    <ffi:setProperty name="GetAllTransferTemplates" property="BankingServiceName" value="<%= com.ffusion.tasks.banking.Task.BANKING %>" />
		<ffi:process name="GetAllTransferTemplates" />
	</ffi:cinclude>
	<% if(null != session.getAttribute("fromPortalPage")){
		session.setAttribute("FFIGetAllTransferTemplates", session.getAttribute("GetAllTransferTemplates"));
		return;// return back to calling page without processing anything.
	}
	%>

<% } %>

		<%-- Required till Load Templates lookupbox action is migrated --%>
		<ffi:object id="GetAllTransferTemplates" name="com.ffusion.tasks.banking.GetAllTransferTemplates" scope="session"/>
			<ffi:setProperty name="GetAllTransferTemplates" property="AccountsCollection" value="TransferAccounts" />
			<ffi:setProperty name="GetAllTransferTemplates" property="CollectionSessionName" value="TransferTemplates" />
			<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetAllTransferTemplates" property="NoSortImage" value='<img src="/efs/efs/multilang/grafx/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
			<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetAllTransferTemplates" property="DescendingSortImage" value='<img src="/efs/efs/multilang/grafx/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
			<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetAllTransferTemplates" property="AscendingSortImage" value='<img src="/efs/efs/multilang/grafx/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">' />
			<ffi:setProperty name="GetAllTransferTemplates" property="ClearSortCriteria" value="" />
			<ffi:setProperty name="GetAllTransferTemplates" property="SortCriteriaOrdinal" value="1" />
			<ffi:setProperty name="GetAllTransferTemplates" property="SortCriteriaName" value="<%= com.ffusion.beans.banking.TransferDefines.SORT_CRITERIA_TEMPLATE_NICKNAME %>" />
			<ffi:setProperty name="GetAllTransferTemplates" property="SortCriteriaAsc" value="True" />
			<ffi:setProperty name="GetAllTransferTemplates" property="ClearSearchCriteria" value="" />
		    <ffi:setProperty name="GetAllTransferTemplates" property="PageSize" value="" />
		    <ffi:setProperty name="GetAllTransferTemplates" property="DateFormat" value="${UserLocale.DateFormat}"/>
		    <ffi:setProperty name="GetAllTransferTemplates" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		    <ffi:setProperty name="GetAllTransferTemplates" property="BankingServiceName" value="<%= com.ffusion.tasks.banking.Task.BANKING %>" />
		<ffi:process name="GetAllTransferTemplates" />
