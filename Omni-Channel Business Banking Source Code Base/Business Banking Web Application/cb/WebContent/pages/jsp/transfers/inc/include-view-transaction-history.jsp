<%--
Name: 			include-view-transaction-history.jsp

Flow(s):		View Single Transfer
				View Recurring Transfer
				View Single Transfer Template
				View Recurring Transfer Template
 --%>
 
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<div class="clearBoth"></div>
<s:set var="TransactionHistory" value="#request.logs" />
<s:if test="%{#TransactionHistory!=null}">
	<div class="transactionHistoryDataHolderDiv marginTop20">
	<%-- ---- TRANSACTION HISTORY ---- --%>
	<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableAlerternateRowColor tdWithPadding">
		<tr class="resetTableRowBGColor">
			<td class="sectionsubhead" style="padding:10px 3px" nowrap><s:text name="jsp.default_142" /></td>
			<td class="sectionsubhead" style="padding:10px 3px" nowrap><s:text name="jsp.default_438" /></td>
			<td class="sectionsubhead" style="padding:10px 3px" nowrap><s:text name="jsp.default_333" /></td>
			<td class="sectionsubhead" style="padding:10px 3px" nowrap><s:text name="jsp.default_170" /></td>
		</tr>
		
		<s:iterator value="#TransactionHistory" var="historyItem">
			<tr>
	
			<%--Transaction Date Column --%>
				<td class="columndata"><s:property
						value="#historyItem.tranDate" /></td>
	
			<%--State Column --%>
				<td class="columndata"><s:property value="#historyItem.state" /></td>
	
			<%--Processed By Column --%>
				<td class="columndata"><s:property
						value="#historyItem.processedBy" /></td>
	
			<%--Description  Column --%>
				<td class="columndata">
					<div class="description">
						<s:property value="%{#historyItem.message}" />
					<div>
				</td>
				
			</tr>
		</s:iterator>
	</table>
	</div>
</s:if>

