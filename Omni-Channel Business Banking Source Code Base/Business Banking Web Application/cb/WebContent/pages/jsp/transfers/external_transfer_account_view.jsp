<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ffusion.beans.exttransfers.ExtTransferAccount,
				 com.ffusion.csil.core.common.EntitlementsDefines,
				 com.ffusion.beans.exttransfers.ExtTransferAccountDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
 <%@ page contentType="text/html; charset=UTF-8" %>

  <ffi:help id="payments_externalaccounts" />

<s:set var="extTransferAccount" value="#session.extTransferAccount" />
<span id="PageHeading" style="display:none;"><s:text name="jsp.transfers_view_external_transfer_account"/></span>

<ffi:cinclude value1="${SecureUser.LocaleLanguage}" value2="en_US" operator="equals">
    <ffi:setProperty name="externalXferURL" value="/web/forms/Manage-External-Enrollment.pdf" />
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.LocaleLanguage}" value2="en_US" operator="notEquals">
    <ffi:setProperty name="externalXferURL" value="/web/${SecureUser.LocaleLanguage}/forms/Manage-External-Enrollment.pdf" />
</ffi:cinclude>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<s:if test="%{#request.isBaSBackendEnable == true}">
	<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
		<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
	<ffi:process name="CountryResource" />
</s:if>

<%-- ================ MAIN CONTENT START ================ --%>
<ffi:setProperty name="BackURL" value="${SecurePath}transfers/manage-external-view.jsp"/>
<div class="tableData">
<s:form id="formViewExternalTransferAccountForm" namespace="/pages/jsp/transfers" name="formViewExternalTransferAccountForm" theme="simple">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="blockWrapper marginTop10">
	<div class="blockContent label140">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.exttransferaccount_primary_account_holder"/>:
				</span>
				<span class="sectionsubhead valueCls">
					<s:property value="extTransferAccount.primaryAcctHolder"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.exttransferaccount_joint_account_holder"/>:
				</span>
				<span class="sectionsubhead valueCls">
					<s:property value="extTransferAccount.secondaryAcctHolder"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.exttransferaccount_financial_institution"/>:
				</span>
				<span class="sectionsubhead valueCls">
					<s:property value="extTransferAccount.bankName"/>
				</span>
			</div>
			<s:set var="acctBankIDType" value="extTransferAccount.acctBankIDType"/>
			<s:set var="actTypeSwift"><%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%></s:set>
			<s:if test = "%{#acctBankIDType == #actTypeSwift}">
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel">
						<s:text name="jsp.exttransferaccount_swift_bic"/>:
					</span>
					<span class="sectionsubhead valueCls">
						<s:property value="extTransferAccount.routingNumber"/>
					</span>
				</div>			
			</s:if>

			<s:if test = "%{#acctBankIDType != #actTypeSwift}">
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><ffi:getProperty name="TempBankIdentifierDisplayText"/>:</span>
					<span class="sectionsubhead valueCls">
						<s:property value="extTransferAccount.routingNumber"/>
					</span>
				</div>
			</s:if>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_20"/>:
				</span>
				<span class="sectionsubhead valueCls">
					<span class="txt_normal"><s:property value="extTransferAccount.type"/></span>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_19"/>:
				</span>
				<span class="sectionsubhead valueCls">
					<s:property value="extTransferAccount.consumerMenuDisplayText"/> <s:property value="extTransferAccount.currencyCode"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.exttransferaccount_ownership"/>:
				</span>
				<s:set var="verifyStatusString" value="extTransferAccount.verifyStatusString"/>
				<s:set var="verifyStatusNotVerifiedValue"><%=String.valueOf(ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED)%></s:set>
				<s:if test="%{#verifyStatusString != #verifyStatusNotVerifiedValue}">
					<span class="sectionsubhead valueCls"><s:text name="jsp.exttransferaccount_this_is_my_own_account"/></span>
				</s:if>
				
				<s:if test="%{#verifyStatusString == #verifyStatusNotVerifiedValue }">
					<span class="sectionsubhead valueCls"><s:text name="jsp.exttransferaccount_this_account_belongs_to_someone_else"/></span>
				</s:if>
			</div>
			<s:if test="%{#request.isBaSBackendEnable == true}">
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel">
						<s:text name="jsp.user_97"/>
					</span>
					<span class="sectionsubhead valueCls">
						<s:set var="countryResourceId">Country<s:property value="extTransferAccount.countryCode"/></s:set>
						<ffi:setProperty name="CountryResource" property="ResourceID" value="${countryResourceId}" />
						<ffi:getProperty name='CountryResource' property='Resource'/>
					</span>
				</div>
			</s:if>
		</div>
		<div class="blockRow">	
			<div class="inlineBlock" style="width: 100%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.exttransferaccount_msg_14"/>:</span> 
				<span class="sectionsubhead valueCls"><s:property value="extTransferAccount.createDate"/>
			</div>
		</div>
	</div>
</div>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_ACH %>">
<s:if test="%{#acctBankIDType != #actTypeSwift }">
	<div align="center" class="marginTop20"><img src="/cb/web/grafx/check_sample.gif" ></div>
</s:if>
</ffi:cinclude>

<div style="margin-top: 40px;">&nbsp;</div>
<div  class="ui-widget-header customDialogFooter">
	<sj:a button="true" title="%{getText('jsp.default_102.1')}" onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a>
</div>
</s:form>
</div>
<%-- ================= MAIN CONTENT END ================= --%>
<ffi:removeProperty name="externalXferURL" />
<ffi:removeProperty name="CountryResource" />