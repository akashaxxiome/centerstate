<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page
	import="com.ffusion.beans.accounts.Accounts,
					com.ffusion.csil.core.common.EntitlementsDefines,
					com.ffusion.beans.banking.TransferStatus"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>



<s:set var="transfer" value="#session.editTransferTemplate" />

<s:set var="isRecurring" value="#request.isRecurring" />

<s:set var="isTemplate" value="#request.istemplateflow" />

<ffi:help id="payments_transferTemplateEdit" />

<%

session.setAttribute("fromPortalPage",request.getAttribute("fromPortalPage"));

%>


	<span id="PageHeading" style="display:none;"><h1 id="editTransferTemplateTitle" class="portlet-title"><s:text name="jsp.transfers_40"/></h1></span>



<s:set name="OpenEnded" value="#request.isOpenEnded" />
<s:set  name="Frequency" value="None" />
<style type="text/css">
#TransferEdit input.combobox{width:360px}
#TransferEdit_templateName{width:90%}
</style>

<!-- Check AppType of user -->
<ffi:setProperty name="IsConsumerUser" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>

<s:include value="/pages/jsp/inc/trans-from-to-js.jsp" />

<s:include value="/pages/jsp/transfers/inc/accounttransferedit-js.jsp" />

<div align="center">
<s:form id="TransferEdit" namespace="/pages/jsp/transfers" validate="false" action="EditTransferTemplateAction_verify" method="post" name="TransferEdit" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
<input type="Hidden" name="transferDestination" id="TransferDest" value="<s:property value="#transfer.transferDestination"/>">
<ffi:removeProperty name="tmp_url" />
<div class="leftPaneWrapper" role="form" aria-labelledby="save">
	<div id="editTransferTemplateName" class="templatePane leftPaneInnerWrapper">
		<div class="header"><h2 id="save"><s:text name="jsp.default_366" /></h2></div>
			<div id="" class="leftPaneInnerBox">
				<span style="margin:0 0 10px; display:block" id="editTransferTemplateName"><label for="templateName"><s:text name="jsp.default_416" /></label> <span class="required" title="required">*</span></span>
				<div class="transferTemplateName">
					<s:textfield name="templateName" size="32" maxlength="32" value="%{#transfer.templateName}" cssClass="ui-widget-content ui-corner-all " aria-labelledby="editTransferTemplateName" aria-required="true"/>
				</div>
				<div style="margin:10px 0 0;"><span id="templateNameError"></span></div>
			</div>
			
	</div>
</div>

<div id="multipleTransferPanel" class="rightPaneWrapper">
<div class="paneWrapper" role="form" aria-labelledby="templateSummary">
  <div class="paneInnerWrapper">
	<div class="header"><h2 id="templateSummary"><s:text name="jsp.default.templates.summary" /></h2></div>
	<div class="paneContentWrapper">
		<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tblFixedLayout">
			<tr>
				<td colspan="3" align="center"><ul id="formerrors"></ul></td>
			</tr>
			<tr class="ltrow_color">
				<td id="editTransferFromAccountLabel" class="sectionsubhead ltrow2_color" width="48%">
					<label for="fromAccount"><s:text name="jsp.default_217" /></label> <span class="required" title="required">*</span></td>
				<td width="4%"></td>
				<td id="editTransferToAccountLabel" class="sectionsubhead ltrow2_color" width="48%"><label for="toAccount"><s:text
						name="jsp.transfers_76" /></label><span class="required" title="required">*</span></td>
			</tr>
			
			<tr>
				<td nowrap class="ltrow2_color" align="left">
					<select class="txtbox" name="fromAccountID" id="FromAccount"
					size="1" onChange="onChangeCheck()" aria-labelledby="editTransferFromAccountLabel" aria-required="true">
						<s:if test="%{#transfer.FromAccountID!=''}">
						<s:if test="%{#transfer.fromAccountID!=null}">
											
						<option
							value="<s:property value='%{#transfer.fromAccountID}' />:<s:property value="#transfer.fromAccount.currencyCode" />"
							selected><s:property value="#transfer.fromAccount.accountDisplayText" />
						</option>
						
					</s:if>
							
						</s:if>
				</select>
				</td>
				<td></td>
				<td nowrap class="ltrow2_color" align="left"><select
					class="txtbox" name="toAccountID" id="ToAccount" size="1"
					onchange="onChangeCheck()" aria-labelledby="editTransferToAccountLabel" aria-required="true">
						<s:if test="%{#transfer.ToAccountID!=''}">

							<option
							value="<s:property value='%{#transfer.toAccountID}' />:<s:property value="#transfer.toAccount.currencyCode" />"
							selected><s:property value="#transfer.toAccount.accountDisplayText" />
						</option>
						</s:if>
				</select></td>
			</tr>
			
			<tr>
				<td><span id="fromAccountIDError"></span></td>
				<td></td>
				<td><span id="toAccountIDError"></span></td>
			</tr>
			
			<tr class="ltrow_color">
				<td id="editTransferAmountLabel" class="sectionsubhead ltrow2_color">
					<span style="width:130px; display:inline-block"><label for="Amount"><s:text name="jsp.default_43" /></label></span> 
					<span id="CurrencySectionLabel">
						<span id="editTransferCurranyLabel"
						class="sectionsubhead ltrow2_color"><label for="currency"><s:text name="jsp.default_125" /></label></span> <span class="required" title="required">*</span>
					</span>
				</td>
				<td></td>
				<td id="editTransferMemoLabel" class="sectionsubhead ltrow2_color"
					><label for="memo"><s:text name="jsp.default_279" /></label></td>
			</tr>

			<tr class="ltrow_color">
				<td class="ltrow2_color" align="left"><s:textfield
						name="amount" id="Amount1" size="12" maxlength="15"
						value="%{#transfer.userAssignedAmount}"
						cssClass="ui-widget-content ui-corner-all" aria-labelledby="editTransferAmountLabel"/> <span
					id="CurrencyLabel" class="columndata" style="display: none">&nbsp;</span>&nbsp;
					<span id="CurrencySection" style="display: none">
					 <select
						class="txtbox" name="userAssignedAmountFlagName"
						id="CurrencySelect" aria-labelledby="editTransferCurranyLabel">
							<option value="single" selected><s:text
									name="jsp.transfers_70" /></option>
					</select> <input type="hidden" id="OtherAmount" value="0.00">
				</span></td>
				<td></td>
				<td class="columndata ltrow2_color" align="left">
					<s:textfield name="memo" size="47" maxlength="100"
						value="%{#transfer.memo}"
						cssClass="ui-widget-content ui-corner-all" style="width:70%" aria-labelledby="editTransferMemoLabel"/>
				</td>
				
			</tr>
			
			<tr>
				<td><span id="amountError"></span>
					<span id="toAmountError"></span>
					<span id="userAssignedAmountError"></span></td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			
			<s:if test="%{#isRecurring}">
				<tr>
					<td id="editTransferFrequencyLabel" class="sectionsubhead ltrow2_color" colspan="3"><s:text
							name="jsp.default_351" /></td>
				</tr>
				<tr>
					<td valign="top" align="left" colspan="3"><s:select
							id="single_transfer_frequency_edit"
							cssClass="txtbox ui-corner-all" onchange="modifyFrequency();"
							list="%{#request.frequencyList}" name="frequency"
							value="#transfer.frequency" />  <script>
							$('#single_transfer_frequency_edit').selectmenu({
								width : 110
							});
						</script>
					<span id="editTransferNoOfTransfersLabel" valign="middle"
						align="left" class="columndata ltrow2_color" colspan="2">
						
						<input
						onclick="toggleRecurring();" type="radio" align="middle"
						name="openEnded" value="true" <ffi:cinclude value1="${OpenEnded}" value2="true">checked</ffi:cinclude>
						border="0"> <s:text name="jsp.default_446" /> 
						
						<input
						onclick="toggleRecurring();" type="radio" align="middle"
						name="openEnded" value="false" <ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals" >checked</ffi:cinclude>
						border="0"> <s:text name="jsp.transfers_1" />
						
						&nbsp; <input id="editTransferNoOfTransfersValue" type="text"
						name="numberTransfers"
						value="<s:property value="#transfer.numberTransfers"/>" size="3"
						maxlength="3" class="ui-widget-content ui-corner-all"> </span></span>
					</td>
				</tr>
				<tr>
					<td colspan="3"><span id="frequencyValueError"></span><span id="numberTransfersError"></span></td>
				</tr>
			</s:if>
			
		</table>

<div style="clear:both"></div>
<ffi:cinclude value1="${IsConsumerUser}" value2="true" operator="notEquals">
<table border="0" cellspacing="0" cellpadding="3" width="100%">										
  <tr>
	<td colspan="3" height="20"></td>
  </tr>
  <tr>
	<td id="editTransferTemplateLifeCycle" class="sectionhead ltrow2_color tbrd_b" colspan="2"><h3 id="templateLifecycle"><s:text name="jsp.default_414"/></h3></td>
	<td class="ltrow2_color tbrd_b" align="right">&nbsp;&nbsp;</td>
  </tr>
  <tr>
	<td colspan="3" height="15"></td>
  </tr>
  <tr>
	<td id="editTransferCurrentStatus" class="sectionsubhead ltrow2_color" align="right"><label for="currentStatus"><s:text name="jsp.default_130"/></label></td>
	<td class="columndata ltrow2_color" align="left" colspan="2"><s:property value="#transfer.statusName"/></td>
  </tr>
  <tr>
	<td id="editTransferAddCommentLabel" align="right" valign="top" style="padding-top:5px"><span class="sectionsubhead"><label for="AddComment"><s:text name="jsp.default_33"/></label></span></td>
	<td class="columndata" align="left" colspan="2">
		<textarea  id="transferTemplateComment" name="templateComment" onkeyup="textarea_transfer_maxlen.isMax()"
		onfocus="textarea_transfer_maxlen.disabledRightMouse()" onblur="textarea_transfer_maxlen.enabledRightMouse()" rows="2" cols="40" class="ui-widget-content ui-corner-all" aria-labelledby="editTransferAddCommentLabel"><s:property value="#transfer.templateComment"/></textarea>
		<span id="EditTransfer.TemplateCommentError" style="font-weight:bold; color:red"></span>
	</td>
  </tr>
  <tr>
	<td id="editTransferTemplateHistoryLabel" class="sectionsubhead ltrow2_color" align="right"><s:text name="jsp.default_411"/></td>
	<td class="columndata ltrow2_color" align="left" colspan="2">
		<a href="javascript:tempHistoryDisplay(true)" onclick="tempHistoryDisplay(true);" id="viewTempHistory">
		   <span class="sapUiIconCls icon-positive"></span>
		</a>
		<a href="javascript:tempHistoryDisplay(false)" onclick="tempHistoryDisplay(false);" id="hideTempHistory" style="display:none">
		   <span class="sapUiIconCls icon-negative"></span>
		</a>
	</td>
  </tr>
	<tr>
	<td colspan="3">
		<span id="tempHistory" style="display:none">
			<%-- ---- TRANSACTION HISTORY ---- --%>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
            		<td colspan="4" class="tbrd_b sectionhead"><s:text name="jsp.default_411"/></td>
            </tr>
            <tr>
                <td class="sectionsubhead" nowrap><strong><s:text name="jsp.default_142"/></strong></td>
                <td class="sectionsubhead" nowrap><strong><s:text name="jsp.default_438"/></strong></td>
                <td class="sectionsubhead" nowrap><strong><s:text name="jsp.default_333"/></strong></td>
                <td class="sectionsubhead" nowrap><strong><s:text name="jsp.default_170"/></strong></td>
            </tr>
           <s:set var="TransactionHistory" value="#request.logs" />
           <s:if test="%{#TransactionHistory!=null}">
            <s:iterator value="#TransactionHistory" var="historyItem">
            	<tr>
                <td class="columndata"><s:property value="#historyItem.tranDate"/></td>
                <td class="columndata"><s:property value="#historyItem.state"/></td>
                <td class="columndata"><s:property value="#historyItem.processedBy"/></td>
                <td class="columndata"><div class="description"><s:property value="#historyItem.message"/></div></td> 
            </tr>
            </s:iterator>
            </s:if>
        </table>
		</span>
	</td>
	 </tr>
</table>									
</ffi:cinclude>
<div class="btn-row">
					<ffi:cinclude
						value1="${fromPortalPage}" value2="true" operator="equals">
						<%-- All topics subscribed here can be found in pendingTransactionPortal.js --%>
						<sj:a id="cancelFormButtonOnInput" button="true" summaryDivId="summary" buttonType="cancel"
							onClickTopics="showSummary,cancelEditPendingTransferForm">
							<s:text name="jsp.default_82" />
						</sj:a>

						<sj:a id="editTransferSubmit" formIds="TransferEdit"
							targets="verifyDiv" button="true" validate="true"
							validateFunction="customValidation"
							onclick="onSubmitCheck(false)"
							onBeforeTopics="beforeVerifyPortalForm"
							onErrorTopics="errorHandlingEditTransfer"
							onSuccessTopics="updateVerifySectionEditTransfer">
							<s:text name="jsp.transfers_74" />
						</sj:a>
						<input type="hidden" name="fromPortalPage" value="true"></input>
					</ffi:cinclude> 
					<ffi:cinclude value1="${fromPortalPage}" value2="true"
						operator="notEquals">

						<sj:a id="cancelFormButtonOnInput" button="true"  summaryDivId="summary" buttonType="cancel"
														onClickTopics="showSummary,cancelTemplatesForm"
										        ><s:text name="jsp.default_82"/></sj:a>

									 			<sj:a
													id="editTransferSubmit"
													formIds="TransferEdit"
					                                targets="verifyDiv"
					                                button="true"
					                                validate="true"
					                                validateFunction="customValidation"
					                                onBeforeTopics="beforeVerifyTransferForm"
					                                onCompleteTopics="verifyTransferFormComplete"
													onErrorTopics="errorVerifyTransferForm"
					                                onSuccessTopics="successVerifyTransferForm"
													onClick="onSubmitCheck(false)"
						                        ><s:text name="jsp.default_366"/></sj:a>
					</ffi:cinclude>
</div></div></div></div></div>
	</s:form>
</div>



<s:set name="deleteDialogTitle" value="%{'Delete Account Transfer Template Confirm'}" scope="request"/>

<ffi:cinclude value1="${fromPortalPage}" value2="true"
	operator="notEquals">
	<sj:dialog id="innerdeletetransferdialog" autoOpen="false" modal="true"
		cssClass="pmtTran_transferDialog staticContentDialog"
		title="%{#request.deleteDialogTitle}" height="180" width="450"
		overlayColor="#000" overlayOpacity="0.7" showEffect="fold"
		hideEffect="clip">
		<div align="center">
			<s:url id="formUrl"
				value="/pages/jsp/transfers/cancelTransferAction.action" />
			<s:form validate="false"
				action="/pages/jsp/transfers/cancelTransferAction.action"
				method="post" name="innerDeleteTransferForm"
				id="innerDeleteTransferForm" theme="simple">
				<input type="hidden" name="CSRF_TOKEN"
					value="<ffi:getProperty name='CSRF_TOKEN'/>" />
				<p id="deleteDialogMessage">
					<s:text name="jsp.transfers_21" />
				</p>

				
				<div id="deleteDialogReturnMessage"></div>
				<sj:a id="deleteDialogCancelBtn" button="true"
					onClickTopics="closeDialog" title="%{getText('jsp.default_83')}">
					<s:text name="jsp.default_82" />
				</sj:a>
				<ffi:cinclude value1="${fromPortalPage}" value2="true"
					operator="equals">
					<sj:a id="deleteSingleTransferLink"
						formIds="innerDeleteTransferForm" targets="loadingstatus"
						button="true" title="%{getText('jsp.default_162')}"
						onSuccessTopics="closeDialog,homeTransferDeleteSuccessTopics"
						effectDuration="1500">
						<s:text name="jsp.default_162" />
					</sj:a>
				</ffi:cinclude>
				<ffi:cinclude value1="${fromPortalPage}" value2="true"
					operator="notEquals">
					<sj:a id="deleteDialogSubmitBtn" button="true"
						formIds="innerDeleteTransferForm" targets="resultmessage"
						onBeforeTopics="beforeDeleteTransfer"
						onCompleteTopics="deleteTransferComplete"
						onErrorTopics="errorDeleteTransfer"
						onSuccessTopics="successDeleteTransfer">
						<s:text name="jsp.default_162" />
					</sj:a>
				</ffi:cinclude>
			</s:form>
		</div>

	</sj:dialog>
</ffi:cinclude>
<ffi:removeProperty name="fromPortalPage" />
<ffi:removeProperty name="IsConsumerUser" />
