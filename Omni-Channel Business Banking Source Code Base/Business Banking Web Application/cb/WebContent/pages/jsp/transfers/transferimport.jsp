<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
				 java.util.ArrayList,
				 com.ffusion.beans.fileimporter.MappingDefinition"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>


<%  if (session.getAttribute("AddEditTransferTemplate") != null) { %>
<s:set var="tmpI18nStr" value="%{getText('jsp.transfers_83')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<% } else { %>
<s:set var="tmpI18nStr" value="%{getText('jsp.transfers_80')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<% } %>
	<ffi:setProperty name='PageText' value=''/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
        <ffi:setProperty name="PageText2" value="${SecurePath}fileuploadcustom.jsp?Section=TRANSFER" URLEncrypt="true"/>
		<ffi:setProperty name='PageText' value='<a href="${PageText2}"><img src="/cb/pages/${ImgExt}grafx/account/i_custommappings.gif" alt="" width="91" height="16" border="0"></a>'/>
        <ffi:removeProperty name="PageText2"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:setProperty name="saveBackURL" value=""/>
<ffi:object id="FileUpload" name="com.ffusion.tasks.fileImport.FileUploadTask" scope="session"/>
<ffi:setProperty name="FileUpload" property="FileType" value="Transfer Import" />

<%  // default values are used by import
	String defaultUpdateBy = "" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING_NEW;
	String defaultMatchBy = "" + MappingDefinition.MATCH_RECORDS_BY_ID_NAME_ACCOUNT;
%>
<ffi:object id="CheckTransferImport" name="com.ffusion.tasks.fileImport.CheckTransferImportTask" scope="session"/>
<ffi:object id="ProcessTransferImport" name="com.ffusion.tasks.fileImport.ProcessTransferImportTask" scope="session"/>
    <%
		session.setAttribute("FFIProcessTransferImport", session.getAttribute("ProcessTransferImport"));
	%>
<ffi:setProperty name="ProcessTransferImport" property="DefaultUpdateRecordsBy" value="<%= defaultUpdateBy %>"/>
<ffi:setProperty name="ProcessTransferImport" property="DefaultMatchRecordsBy" value="<%= defaultMatchBy %>"/>

<ffi:object id="GetMappingDefinitions" name="com.ffusion.tasks.fileImport.GetMappingDefinitionsTask" scope="session"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	<ffi:process name="GetMappingDefinitions"/>
</ffi:cinclude>

<ffi:object id="SetMappingDefinition" name="com.ffusion.tasks.fileImport.SetMappingDefinitionTask" scope="session"/>
<% session.setAttribute("FFISetMappingDefinition", session.getAttribute("SetMappingDefinition")); %>

<ffi:object id="GetOutputFormat" name="com.ffusion.tasks.fileImport.GetOutputFormatTask" scope="session"/>
<ffi:setProperty name="CheckTransferImport" property="ImportErrorsURL" value="confirm"/>

<%  if (session.getAttribute("AddEditTransferTemplate") != null) { %>
<title><ffi:getProperty name="product"/>: <s:text name="jsp.transfers_84"/></title>
<% } else { %>
<title><ffi:getProperty name="product"/>: <s:text name="jsp.transfers_19"/></title>
<% } %>

<script type="text/javascript"><!--
	function updateCustomMapping(fileType, customFileType)
	{
		if (fileType == null || customFileType == null)
			return;
		if (fileType.value == 'Custom')
		{   
			$("#transferSetMappingDefinitionID").selectmenu('enable');
		}
		else
		{   
			$("#transferSetMappingDefinitionID").val('0');
			$("#transferSetMappingDefinitionID").selectmenu('destroy').selectmenu({width: 220});
			$("#transferSetMappingDefinitionID").selectmenu('disable');
		}
	}

    $(function(){
		$("#transferFileTypeID").selectmenu({width: 220});
		$("#transferSetMappingDefinitionID").selectmenu({width: 220});

     });
    $(document).ready(function(){
	   updateCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID']);
	});
	   
// --></script>
<fieldset id="fileUploadSelectFormatFieldSetID">
        <legend><s:text name="jsp.default_539"/></legend> 
		<div>
			
			<form id="TransferFileTypeFormID" name="FileType" action="/cb/pages/fileupload/transferFileUploadAction_verify.action" method="post" target="FileUpload">
                <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData">
                    <tr>
                        <td class="lightBackground" align="center">
                            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                                <tr>
                                    <td class="columndata" width="480">
                                        <span class="sectionsubhead"><s:text name="jsp.default_236"/></span>
                                    </td>
                                    <td width="30"></td>
                                    <td><span class="sectionsubhead"><s:text name="jsp.default_14"/></span></td>
                                </tr>
                                <tr>
                                    <td class="columndata" width="480" rowspan="3"><s:text name="jsp.default_223"/></td>
                                    <td class="columndata" nowrap width="30"></td>

                                <td class="columndata" valign="top">
                                <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS_SWIFT_FILE_UPLOAD %>">
                                        <select id="transferFileTypeID" name="fileType" class="txtbox" onChange="updateCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID'])">
                                            <option value="Transfer Import"><s:text name="jsp.transfers_81"/></option>
                                            <option value="Custom"><s:text name="jsp.default_132"/></option>
                                        </select>
                                    </ffi:cinclude>
                                </ffi:cinclude>
                                <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS_SWIFT_FILE_UPLOAD %>">
                                        <input type="Hidden" name="fileType" value="Transfer Import">
                                        <s:text name="jsp.transfers_81"/>
                                    </ffi:cinclude>
                                </ffi:cinclude>
                                <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.TRANSFERS_SWIFT_FILE_UPLOAD %>">
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                        <input type="Hidden" name="fileType" value="Custom">
                                        <s:text name="jsp.default_132"/>
                                    </ffi:cinclude>
                                </ffi:cinclude>
                                </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td valign="bottom"><span class="sectionsubhead">
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                        <s:text name="jsp.default_135"/>
                                    </ffi:cinclude>
                                    </span></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td valign="top" class="columndata" nowrap>
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                        <select id="transferSetMappingDefinitionID" name="SetMappingDefinition.ID" class="txtbox">
                                            <option value="0">- <s:text name="jsp.default_377"/> -</option>
                                            <ffi:list collection="MappingDefinitions" items="MappingDefinition">
                                                <% String entitlementName = ""; %>
                                                <ffi:cinclude value1="${MappingDefinition.OutputFormatName}" value2="" operator="notEquals">
                                                    <ffi:setProperty name="GetOutputFormat" property="Name" value="${MappingDefinition.OutputFormatName}"/>
                                                    <ffi:process name="GetOutputFormat"/>
                                                    <ffi:setProperty name="OutputFormat" property="CurrentCategory" value="TRANSFER"/>

                                                    <ffi:getProperty name="OutputFormat" property="EntitlementName" assignTo="entitlementName" />

                                                    <ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
                                                        <% boolean displayMap = true;
                                                        if (entitlementName != null && entitlementName.length() > 0) { %>
                                                            <ffi:cinclude ifNotEntitled="<%=entitlementName%>" >
                                                                <% displayMap = false; %>
                                                            </ffi:cinclude>
                                                        <% }
                                                        String updateRecordsBy = null; %>
                                                        <ffi:getProperty name="MappingDefinition" property="UpdateRecordsBy" assignTo="updateRecordsBy" />
                                                        <%
                                                        if (updateRecordsBy != null && updateRecordsBy.equals("" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING_AMOUNTS_ONLY))
                                                            displayMap = false;
                                                        if (displayMap)
                                                        { %>
                                                        <option value="<ffi:getProperty name='MappingDefinition' property='MappingID'/>"><ffi:getProperty name="MappingDefinition" property="Name"/></option>
                                                        <% } %>
                                                    </ffi:cinclude>
                                                </ffi:cinclude>
                                            </ffi:list>
                                        </select><br/>
                                        <span id="mappingTypeError"></span>
                                    </ffi:cinclude>
                                    <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                        <input type="Hidden" name="SetMappingDefinition.ID" value="0">
                                    </ffi:cinclude>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="center" nowrap>
									<ffi:cinclude value1="${templateEdit}" value2="true" operator="equals">
                                        <sj:a 
			                                button="true" 
			                                onClickTopics="cancelTransferTemplateFileImportForm"
		    	                            cssClass="cancelImportFormButton"
					                        ><s:text name="jsp.default_82"/></sj:a>
									</ffi:cinclude>
									<ffi:cinclude value1="${templateEdit}" value2="true" operator="notEquals">
										<sj:a 
			                                button="true" 
			                                onClickTopics="cancelTransferFileImportForm"
		    	                            cssClass="cancelImportFormButton"
					                        ><s:text name="jsp.default_82"/></sj:a>
									</ffi:cinclude>
                                        &nbsp;&nbsp;
                                        <sj:a 
                                            id="transferFileUploadID"
											formIds="TransferFileTypeFormID"
											timeout="0" 
											button="true"
											validate="true"
											validateFunction="customValidation"
											targets="openFileImportDialogID" 
											onclick="removeValidationErrors();"
											onErrorTopics="errorOpenFileUploadTopics"
											onSuccessTopics="openTransferFileUploadTopics"
											><s:text name="jsp.transfers_5"/></sj:a>
                                       
                                </tr>
                            </table>
                        </td>
                     
                    </tr>
                  
                </table>
			</form>
		</div>
</fieldset>
<ffi:removeProperty name="GetOutputFormat"/>
<ffi:setProperty name="actionNameNewReportURL" value="NewReportBaseAction.action?NewReportBase.ReportName=${ReportName}&flag=generateReport" URLEncrypt="true" />
<ffi:setProperty name="actionNameGenerateReportURL" value="GenerateReportBaseAction_display.action?doneCompleteDirection=${doneCompleteDirection}" URLEncrypt="true" />