
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page import="com.ffusion.beans.exttransfers.ExtTransferAccount,
				 com.ffusion.csil.core.common.EntitlementsDefines,
				 com.ffusion.beans.exttransfers.ExtTransferAccountDefines"%>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_externalaccounttransfer"  className="moduleHelpClass" />
<span id="PageHeading" style="display:none;"><s:text name="jsp.exttransferaccount_deposit_verification_underway" /></span>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_ADD_ACCOUNT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<%-- check if passed the Security Check yet --%>
<ffi:cinclude value1="${manageExternalVerified}" value2="" operator="equals">

</ffi:cinclude>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<% 
	boolean retrying = false;
	if(request.getParameter("Retrying") != null) {
		session.setAttribute("Retrying", request.getParameter("Retrying"));
	}
	if(request.getParameter("ComingBack") != null) {
		session.setAttribute("ComingBack", request.getParameter("ComingBack"));
	}
%>
<ffi:cinclude value1="${Retrying}" value2="" operator="notEquals" >
	<% retrying = true; %>
</ffi:cinclude>
<ffi:removeProperty name="Retrying"/>

<ffi:removeProperty name="VerifyExtTransferAccount" />
<ffi:object name="com.ffusion.tasks.exttransfers.VerifyExtTransferAccount" id="VerifyExtTransferAccount"  scope="session"/>
	<ffi:setProperty name="VerifyExtTransferAccount" property="Initialize" value="true"/>
	<ffi:setProperty name="VerifyExtTransferAccount" property="VerifyFailedURL" value="${SecurePath}transfers/manage-external-verify-failed.jsp"/>
	<% if (session.getAttribute("accountID") != null) {	%>
		<ffi:setProperty name="VerifyExtTransferAccount" property="ExtAccountBPWID" value="${accountID}"/>
		<ffi:removeProperty name="accountID"/>
	<% } %>
<%-- check if this is a secondary user --%>
<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="notEquals">
	    <ffi:setProperty name="VerifyExtTransferAccount" property="SourceAccounts" value="PrimaryUserAccounts" />
</ffi:cinclude>

<%-- only do this if this is a primary user --%>
<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="equals">
	    <ffi:setProperty name="VerifyExtTransferAccount" property="SourceAccounts" value="BankingAccounts" />
</ffi:cinclude>
<ffi:setProperty name="VerifyExtTransferAccount" property="Process" value="false"/>
<ffi:process name="VerifyExtTransferAccount" />
<ffi:removeProperty name="ComingBack"/>

<ffi:cinclude value1="${VerifyExtTransferAccount.isBaSBackendEnable}" value2="true" operator="equals">
	<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
		<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
	<ffi:process name="CountryResource" />
</ffi:cinclude>

<% session.setAttribute("FFIVerifyExtTransferAccount", session.getAttribute("VerifyExtTransferAccount")); %>

<%-- ================ MAIN CONTENT START ================ --%>
<ffi:setProperty name="BackURL" value="${SecurePath}transfers/manage-external-verify.jsp?ComingBack=true"/>

<s:form id="formVerifyExternalTransferAccount" namespace="/pages/jsp/transfers" validate="false" action="verifyExtTransferAccount" method="post" name="formVerifyExternalTransferAccount" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_ADD_ACCOUNT %>" >
<div class="instructions">
	<p><s:text name="jsp.exttransferaccount_msg_11" /></p>
	<p><s:text name="jsp.exttransferaccount_msg_12" /></p>
	<% if (retrying) { %>
		<p>
			<s:text name="jsp.exttransferaccount_msg_13" />
		</p>
	<% } %>
		<ffi:cinclude value1="0" value2="${ExternalTransferACCOUNT.VerifyFailedCount}" operator="notEquals">
			<p>
				<span class="sectionsubhead">
					<s:text name="jsp.exttransferaccount_verify_account_msg">
						<s:param name="CSRF_TOKEN" value="%{#session.ExternalTransferACCOUNT.VerifyFailedCount}"></s:param>
					</s:text>
				</span>
			</p>
		</ffi:cinclude>
</div>

<div class="leftPaneWrapper">
<div class="leftPaneInnerWrapper">
		<div class="header">External Account Transfer Deposit</div>
		<div id="" class="paneContentWrapper">
			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">
				<% int amountCount = 0; %>
				<ffi:list collection="VerifyExtTransferAccount.Amounts" items="Amount">
					<% amountCount ++; %>
					<tr>
						<td width="8%" class="sectionsubhead ltrow2_color" nowrap valign="middle">
							<s:text name="jsp.exttransferaccount_deposit" />&nbsp;<%=amountCount%> (<ffi:getProperty name="Amount" property="AmountValue.CurrencyCode"/>):
						</td>
					</tr>
					<tr>
						<td class="sectionsubhead ltrow2_color" nowrap valign="middle">
						<input type="text" name="VerifyExtTransferAccount.CurrentAmount=<ffi:getProperty name="Amount" property="ID"/>&VerifyExtTransferAccount.Amount" size="8" class="txtbox ui-widget-content ui-corner-all" value="">
						</td>
					</tr>
					<tr><td><span id="Amount<ffi:getProperty name="Amount" property="ID"/>Error"></span></td></tr>
				</ffi:list>
			</table>
		</div>
	</div>
</div>
<div class="rightPaneWrapper w71">
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header">External Account Transfer Details</div>
	<div class="blockWrapper">
		<div class="blockContent label150">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<ffi:cinclude value1="${ExternalTransferACCOUNT.AcctBankIDType}" value2="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%>" operator="notEquals">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_63" />/
						  <ffi:getProperty name="TempBankIdentifierDisplayText"/>:
					</span>
				</ffi:cinclude>
				<ffi:cinclude value1="${ExternalTransferACCOUNT.AcctBankIDType}" value2="<%=ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_SWIFT%>" operator="equals">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_63" />/<s:text name="jsp.exttransferaccount_swift_bic" />:</span>
				</ffi:cinclude>
				<span><ffi:getProperty name="ExternalTransferACCOUNT" property="BankName"/> / <ffi:getProperty name="ExternalTransferACCOUNT" property="RoutingNumber"/></span>
					
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_16" />:</span>
				<span><ffi:getProperty name="ExternalTransferACCOUNT" property="ConsumerMenuDisplayText"/> <ffi:getProperty name="ExternalTransferACCOUNT" property="CurrencyCode"/>
				</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">	
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_294" />:</span>
					<span><ffi:getProperty name="ExternalTransferACCOUNT" property="Nickname"/></span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_20" />:</span>
					<span><ffi:getProperty name="ExternalTransferACCOUNT" property="Type"/></span>
				</div>
			</div>
			<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.exttransferaccount_ownership" />:</span>
					<ffi:cinclude value1="${ExternalTransferACCOUNT.VerifyStatusString}" value2="<%=String.valueOf(ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED)%>" operator="notEquals">
						<span class="sectionsubhead"><s:text name="jsp.exttransferaccount_this_is_my_own_account" /></span>
					</ffi:cinclude>
					<ffi:cinclude value1="${ExternalTransferACCOUNT.VerifyStatusString}" value2="<%=String.valueOf(ExtTransferAccountDefines.VERIFYSTATUS_NOT_VERIFIED)%>" operator="equals">
						<span><s:text name="jsp.exttransferaccount_this_account_belongs_to_someone_else" /></span>
					</ffi:cinclude>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.exttransferaccount_deposit_date" />:</span>
				<span>
					<ffi:setProperty name="ExternalTransferACCOUNT" property="DateFormat" value="${UserLocale.DateFormat}" />  
					<ffi:getProperty name="ExternalTransferACCOUNT" property="DepositDate"/>
				</span>
				</div>
			</div>
			<ffi:cinclude value1="${VerifyExtTransferAccount.isBaSBackendEnable}" value2="true" operator="equals">
				<div class="blockRow">
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel">
							<s:text name="jsp.user_97"/>
						</span>
						<span class="sectionsubhead valueCls">
							<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${ExternalTransferACCOUNT.CountryCode}" />
							<ffi:getProperty name='CountryResource' property='Resource'/>
						</span>
					</div>
				</div>
			</ffi:cinclude>
		</div>
	</div>
	<div class="btn-row">
		<sj:a id="cancelFormButtonOnInput" button="true" onClickTopics="cancelExternalTransferAccountForm"><s:text name="jsp.default_82"/></sj:a>
		&nbsp;
		<sj:a
			id="verifyExtTransferAccountSubmit"
			formIds="formVerifyExternalTransferAccount"
			targets="inputDiv"
			button="true"
			validate="true"
			validateFunction="customValidation"
			onBeforeTopics="beforeVerifyExtTransferAccountForm"
			onCompleteTopics="verifyExtTransferAccountFormComplete"
			onErrorTopics="errorVerifyExtTransferAccountForm"
			onSuccessTopics="successVerifyExtTransferAccountForm"><s:text name="jsp.default_395"/></sj:a>
	</div></div></div></div>
</ffi:cinclude>
</s:form>
<%-- ================= MAIN CONTENT END ================= --%>
<ffi:removeProperty name="SetExtTransferAccount"/>
<ffi:removeProperty name="CountryResource" />
