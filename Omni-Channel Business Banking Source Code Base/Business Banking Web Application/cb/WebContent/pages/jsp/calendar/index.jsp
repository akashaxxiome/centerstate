<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<%@ page import="com.ffusion.beans.user.UserLocale,
                 com.ffusion.beans.accounts.Accounts,
				 com.ffusion.efs.tasks.SessionNames,
					com.ffusion.beans.SecureUser,com.ffusion.util.DateConsts" %>
		
<%
session.setAttribute("moduleNameForCalendar", request.getParameter("moduleName"));
%>
<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
<ffi:process name="GetCurrentDate"/>			
<%-- Format and display the current date/time --%>


<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="<%= DateConsts.FULL_CALENDAR_DATE_FORMAT %>"/>


<ffi:setGridURL grid="Transfer_Calendar_Event_URL" name="Transfer_Calendar_Event_URL" url="/cb/pages/jsp/transfers/viewTransferAction_calendarData.action?transferID={0}&transferType={1}&recTransferID={2}" parm0="transferID" parm1="transferType" parm2="recTransferID"  />

	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar-extra.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.overrides.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.overrides.css' media='print' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.print.css' media='print' />
	
	<script type='text/javascript'>
	
		$(document).ready(function() { 
			
			var date = new Date();
			var d = date.getDate();
			var m = date.getMonth();
			var y = date.getFullYear();
			var dateArray=[];
			
			$('#calendar').fullCalendar({
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,basicWeek,basicDay'
				},			
				editable: true,
				disableDragging:true,
				events: "<ffi:urlEncrypt url='/cb/pages/jsp/calendar/getCalendarEvents.action?moduleName=TransferCalendar&GridURLs=Transfer_Calendar_Event_URL' />",

				eventClick: function(calEvent, jsEvent, view){
					if(view.name == "month"){
						moveToDay(calEvent.start);	
					}else if(view.name == "basicDay"){
						var isLoaded = false;						
						var temp = $(this);
						for(var i =0;i<$.parseHTML(this.innerHTML).length;i++){
							if($.parseHTML(this.innerHTML)[i].localName =="div"
									&& $.parseHTML(this.innerHTML)[i].className.indexOf("transactionHistoryDataHolderDiv")!="-1"){
								isLoaded = true;
								break;
							}
						}
						if(!isLoaded){
		                /* if($(this).find('div.transactionHistoryDataHolderDiv').length==0){ */
		                	clearData();
							var isCorporateUser='<%= ((SecureUser)session.getAttribute(SessionNames.SECURE_USER)).isCorporateUser()%>';
							
							if(isCorporateUser=='true'){
								$.ajax({
								   url: "/cb/pages/jsp/transfers/viewTransferAction_calendarData.action",
								   data:{
										transferID:calEvent.transferID,
										transferType:calEvent.transferType,
										recTransferID:calEvent.recTransferID
								   },
								   success: function(data){
									   temp.append(data);
									   var lHeight = $(temp).parent().height();
									   $("div .fc-content").height(lHeight+50);
								   },
								});
							}
						}
					}else{
						moveToDay(calEvent.start);	
					}
				},
				eventRender:function(event, element){
					element.removeClass("fc-event fc-event-skin");
					element.find("div").removeClass("fc-event fc-event-skin");
					var view  =  $('#calendar').fullCalendar('getView');
					
					var holderDiv = "";
					var text = '';
					
					if(view.name == "month"){
						var y = event.start;
						var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
						var cal = $("#calendar")[0];
						var data = $.data(cal,d.toString());
						
						if(dateArray.indexOf(d)=='-1'){
						dateArray.push(d);
						text = "";
						for (var key in event.calendarDateData) {
							  if (event.calendarDateData.hasOwnProperty(key)) {
								  if(event.calendarDateData[key] > 0) {
									  if(key == "Pending" || key == "Skipped") {
										  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarPendingApprovalRecordCls sapUiIconCls icon-history'>";
									  } else if(key == "Pending Approval") {
										  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarPendingRecordCls sapUiIconCls icon-pending'>";
									  } else if (key == "Completed") {
										  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
									  }
	
									  text = text + holderDiv;
									  text = text+" " +event.calendarDateData[key] + " "+ key;
									  text += "</div>";
								  }
							  }
						}
						$(".completedevent").css("position", "absolute");
						element.find('.fc-event-title').html(text);
						$.data(cal,d.toString(),data);
						}else{
							return false;
						}
					}
					if(view.name == "basicWeek") {

						if (dateArray.indexOf(event.referenceNumber)=='-1') {
							text = "";
							dateArray.push(event.referenceNumber);
							key = event.statusName;
							
							if(key!=null && key!=''){
								  if(key.trim() == "Pending" || key.trim() == "Scheduled" || key.trim() == "Skipped") {
									  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewPendingApprovalRecordCls sapUiIconCls icon-history'>";
								  } else if(key.trim() == "Pending Approval"|| event.statusName.trim() == "Approval Pending" ) {
									  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewPendingRecordCls sapUiIconCls icon-pending'>";
								  } else if (key.trim() == "Completed") {
									  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
								  }
								  text = text + holderDiv;
								  text = text+" " +ns.common.HTMLEncode(this.title); 
								  text += "</div>";
			
								$(".completedevent").css("position", "absolute");
								element.find('.fc-event-title').html(text);	
							}
							
						}
					}
					if(view.name == "basicDay"){
						text = "";
						var statusName = ""; 
						if(event.statusName.trim() == "Pending" || event.statusName.trim() == "Scheduled" || "In Process"== event.statusName.trim() || event.statusName.trim() == "Skipped") {
							statusName = "<div class='marginRight20 marginleft10 floatleft'>"+event.statusName+"<span class='marginleft5 calendarPendingApprovalRecordCls sapUiIconCls icon-history'></span></div>";
						} else if(event.statusName.trim() == "Pending Approval" || "Approval Pending" == event.statusName.trim()) {
							statusName = "<div class='marginRight20 floatleft'>"+event.statusName+"<span class='marginleft5 calendarPendingRecordCls sapUiIconCls icon-pending'></span></div>";
						} else if (event.statusName.trim() == "Completed") {
							statusName = "<div class='marginRight20 floatleft'>"+event.statusName+"<span class='marginleft5 calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'></span></div>";
						}
						var referenceNumber = "<div class='marginRight20 floatleft heading'>"+js_reference_number+": <span class='boldIt'>"+event.referenceNumber+"</span></div>";
						var fromAccount = ns.common.HTMLEncode(event.fromAccount);
						var toAccount = ns.common.HTMLEncode(event.toAccount);
						var account = "<div class='marginRight20 floatleft'>"+js_account+":  <span class='boldIt'>"+ fromAccount +"<span class='marginleft10 sapUiIconCls icon-arrow-right'></span>"+ toAccount +"</span></div>";
						var amount = "<div class='marginRight20 floatleft'>"+js_amount+":  <span class='boldIt'>"+event.amount+ " " +event.currencyCode +"</span></div>";
						
						var existingTitle = event.title;
						
						//var newTitle = statusName + "Reference Number: " + referenceNumber + existingTitle;
						var newTitle = "<span class='heading'>"+statusName+referenceNumber+account+amount;+"</span>";
						
						//$(".completedevent").css("position", "relative");
						//$(".completedevent").addClass("completedeventDayView");
						var isCorporateUser='<%= ((SecureUser)session.getAttribute(SessionNames.SECURE_USER)).isCorporateUser()%>';
						if(isCorporateUser == "true") {
							$("div[class*='fc-view-basicDay'] div .fc-event-hori").addClass("completedeventDayView");	
						} else {
							// don't show hand cursor for consumer
							$("div[class*='fc-view-basicDay'] div .fc-event-hori").addClass("completedeventDayView resetHandCursor");
						}
						
						element.find('.fc-event-title').html(newTitle);
					}
				},
				
				eventAfterRender :function( event, element, view ) { 
					var y = event.start;
					var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
					var cal = $("#calendar")[0];
					$.removeData(cal,d.toString());
					dateArray.length=0;
				},
				
				windowResize: function(view) {
					//$('#calendar').fullCalendar('refetchEvents');
					//$('#calendar').fullCalendar('render');
				}
			});	
		});
		function moveToDay(date){
			var toDate = new Date(date);
			$('#calendar').fullCalendar( 'changeView', 'basicDay' );
			$('#calendar').fullCalendar( 'gotoDate', toDate );
		}
		
				
	    function getServerDate(){
		    var date = new Date('<ffi:getProperty name="GetCurrentDate" property="Date" />');
			return date;
		} 
		
		function clearData(){
			$('.transactionHistoryDataHolderDiv').each(function(){
					$(this).slideToggle();
					$(this).remove();
				});
		}
	</script>
	<style type='text/css'>

		label {
			margin-top: 40px;
			text-align: left;
			font-size: 14px;
			font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
			}

		#calendar {
			width: 95%;
			margin: 0 auto;
			
			overflow-x: hidden;
			overflow-y: auto;
		}
		
		.fc-view {
			overflow-x: hidden;
			overflow-y: auto;
		}
		
		/* style added to increase the height of the content when a transaction is expanded for DayView */
		
		.fc-view-basicDay {
			height: 100%;
		}
	
		.fc-border-separate {
		     height: 100%;
		}
		
		.transactionHistoryDataHolderDiv .tableAlerternateRowColor{
			cursor: default;
		}
		
		.tableAlerternateRowColor .description{
			width:100%;
		}
	</style>
	<div id='calendar'></div>