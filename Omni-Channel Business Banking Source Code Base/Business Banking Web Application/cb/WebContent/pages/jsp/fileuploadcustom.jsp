<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%@ page import="com.ffusion.beans.fileimporter.MappingDefinition"%>

<s:set var="tmpI18nStr" value="%{getText('jsp.default_134')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='Temp1' value='${SecurePath}custommapping.jsp?SetMappingDefinition.ID=0' URLEncrypt="true"/>
<ffi:setProperty name='PageText' value='<a href="${Temp1}"><img src="/cb/pages/${ImgExt}grafx/account/i_addcustommapping.gif" alt="" width="115" height="16" border="0"></a>'/>
<ffi:setProperty name='Temp1' value='${SecurePath}fileuploadcustom.jsp?Section=${Section}' URLEncrypt="true"/>
<ffi:setProperty name="BackURL" value="${Temp1}"/>

<%
	if(request.getParameter("Section") != null)
		session.setAttribute("Section", request.getParameter("Section"));
%>

				<script>
				$(function(){
					$("#customMappingGridTabs").tabs();
				});

				</script>
	


<%-- include javascripts for top navigation menu bar
Since Payments, Transfers, ACH and Wire all are in the SubDirectory payments, just make that the default. --%>
<% request.setAttribute("SubDirectory", "payments"); %>
<ffi:cinclude value1="${Section}" value2="PPAY">
	<% request.setAttribute("SubDirectory", "cash"); %>
</ffi:cinclude>

<%--
<BODY BGCOLOR=#FFFFFF> 
--%>
<div align="center">
<%-- include page header --%>
<ffi:flush/>


            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="display:none">
				<tr>
					<td colspan="4" align="left" width="750" height="14" background="/cb/web/multilang/grafx/<ffi:getProperty name="SubDirectory"/>/sechdr_blank.gif"><span class="sectionsubhead" style="color: white">&nbsp; > <s:text name="jsp.default_134"/></span></td>
				</tr>
				<tr>
					<td class="sectionsubhead tbrd_ltb"> <s:text name="jsp.default_284"/>&nbsp;
						<%-- no sorting on this field set, use sort_off.gif, set the link to sorting in ASC order --%>
						<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2="<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_NAME %>" operator="notEquals" >
							<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2='<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_NAME + ",REVERSE" %>' operator="notEquals" >
								<ffi:link url="?MappingDefinitions.SortedBy=NAME" >
								<img src="/cb/web/multilang/grafx/<ffi:getProperty name='SubDirectory'/>/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								</ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>
						<%-- sorted in asc order, set the link to sort in desc order --%>
						<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2="<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_NAME %>" operator="equals" >
							<ffi:link url='?MappingDefinitions.SortedBy=NAME,REVERSE' >
								<img src="/cb/web/multilang/grafx/<ffi:getProperty name='SubDirectory'/>/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
							</ffi:link>
						</ffi:cinclude>
						<%-- sorted in desc order, set link to sort in asc order --%>
						<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2='<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_NAME + ",REVERSE" %>' operator="equals" >
							<ffi:link url="?MappingDefinitions.SortedBy=NAME" >
								<img src="/cb/web/multilang/grafx/<ffi:getProperty name='SubDirectory'/>/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
							</ffi:link>
						</ffi:cinclude>
					</td>
					<td class="sectionsubhead tbrd_tb"> <s:text name="jsp.default_170"/>&nbsp;
						<%-- no sorting on this field set, use sort_off.gif, set the link to sorting in ASC order --%>
						<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2="<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_DESCRIPTION %>" operator="notEquals" >
							<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2='<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_DESCRIPTION + ",REVERSE" %>' operator="notEquals" >
								<ffi:link url="?MappingDefinitions.SortedBy=DESCRIPTION" >
									<img src="/cb/web/multilang/grafx/<ffi:getProperty name='SubDirectory'/>/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								</ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>
						<%-- sorted in asc order, set the link to sort in desc order --%>
						<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2="<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_DESCRIPTION %>" operator="equals" >
							<ffi:link url='?MappingDefinitions.SortedBy=DESCRIPTION,REVERSE' >
								<img src="/cb/web/multilang/grafx/<ffi:getProperty name='SubDirectory'/>/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
							</ffi:link>
						</ffi:cinclude>
						<%-- sorted in desc order, set link to sort in asc order --%>
						<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2='<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_DESCRIPTION + ",REVERSE" %>' operator="equals" >
							<ffi:link url="?MappingDefinitions.SortedBy=DESCRIPTION" >
								<img src="/cb/web/multilang/grafx/<ffi:getProperty name='SubDirectory'/>/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
							</ffi:link>
						</ffi:cinclude>
					</td>
					<td class="sectionsubhead tbrd_tb"> <s:text name="jsp.default_308"/>&nbsp;
						<%-- no sorting on this field set, use sort_off.gif, set the link to sorting in ASC order --%>
						<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2="<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_OUTPUT_FORMAT %>" operator="notEquals" >
							<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2='<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_OUTPUT_FORMAT + ",REVERSE" %>' operator="notEquals" >
								<ffi:link url="?MappingDefinitions.SortedBy=OUTPUT_FORMAT" >
									<img src="/cb/web/multilang/grafx/<ffi:getProperty name='SubDirectory'/>/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								</ffi:link>
							</ffi:cinclude>
						</ffi:cinclude>
						<%-- sorted in asc order, set the link to sort in desc order --%>
						<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2="<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_OUTPUT_FORMAT %>" operator="equals" >
							<ffi:link url='?MappingDefinitions.SortedBy=OUTPUT_FORMAT,REVERSE' >
								<img src="/cb/web/multilang/grafx/<ffi:getProperty name='SubDirectory'/>/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
							</ffi:link>
						</ffi:cinclude>
						<%-- sorted in desc order, set link to sort in asc order --%>
						<ffi:cinclude value1="${MappingDefinitions.SortedBy}" value2='<%= com.ffusion.beans.fileimporter.MappingDefinition.FIELD_OUTPUT_FORMAT + ",REVERSE" %>' operator="equals" >
							<ffi:link url="?MappingDefinitions.SortedBy=OUTPUT_FORMAT" >
								<img src="/cb/web/multilang/grafx/<ffi:getProperty name='SubDirectory'/>/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
							</ffi:link>
						</ffi:cinclude>
					</td>
          <td class="sectionsubhead tbrd_trb">&nbsp;</td>
				</tr>
				

				<ffi:object id="GetOutputFormat" name="com.ffusion.tasks.fileImport.GetOutputFormatTask" scope="session"/>
				<ffi:object id="GetOutputFormatInfo" name="com.ffusion.tasks.fileImport.GetOutputFormatInfoTask" scope="session"/>
				<ffi:list collection="MappingDefinitions" items="MappingDefinition">
				<% String entitlementName = ""; %>
				<tr class="whiteBackground">
                    <td class="columndata"><ffi:getProperty name="MappingDefinition" property="Name"/></td>
                    <td class="columndata"><ffi:getProperty name="MappingDefinition" property="Description"/></td>
                    <td class="columndata">
						<ffi:cinclude value1="${MappingDefinition.OutputFormatName}" value2="" operator="notEquals">
							<ffi:setProperty name="GetOutputFormat" property="Name" value="${MappingDefinition.OutputFormatName}"/>
							<ffi:process name="GetOutputFormat"/>
							
								<%
								  session.setAttribute("FFIGetOutputFormat", session.getAttribute("GetOutputFormat"));
								%>
                
							<ffi:setProperty name="GetOutputFormatInfo" property="Name" value="OutputFormat"/>
							<ffi:process name="GetOutputFormatInfo" />
								<%
								  session.setAttribute("FFIGetOutputFormatInfo", session.getAttribute("GetOutputFormatInfo"));
								%>
								
							<ffi:getProperty name="GetOutputFormatInfo" property="LocaleSpecificName" />
							
								<% String updateRecordsBy = null;
								   String matchRecordsBy = null; %>
								<ffi:getProperty name="MappingDefinition" property="UpdateRecordsBy" assignTo="updateRecordsBy" />
								<ffi:getProperty name="MappingDefinition" property="MatchRecordsBy" assignTo="matchRecordsBy" />
								<%
								if (updateRecordsBy != null) {
									%>
									<br>
									<%
									if (updateRecordsBy.equals("" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING)) { %>
									<s:text name="jsp.default_450"/>
									<% }
									if (updateRecordsBy.equals("" + MappingDefinition.UPDATE_RECORDS_BY_NEW)) { %>
									<s:text name="jsp.default_31"/>
									<% }
									if (updateRecordsBy.equals("" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING_NEW)) { %>
									<s:text name="jsp.default_449"/>
									<% }
									if (updateRecordsBy.equals("" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING_AMOUNTS_ONLY)) { %>
									<s:text name="jsp.default_447"/>
									<% }
								}%>
								<ffi:getProperty name="OutputFormat" property="EntitlementName" assignTo="entitlementName" />
							<ffi:cinclude value1="${OutputFormat.ContainsMatchRecordOptions}" value2="true" operator="equals">
								<%
								if (matchRecordsBy != null && !matchRecordsBy.equals("" + MappingDefinition.MATCH_RECORDS_BY_NONE)) {
									%>
									&nbsp;&nbsp;&nbsp;
									<%
									if (matchRecordsBy.equals("" + MappingDefinition.MATCH_RECORDS_BY_ACCOUNT)) { %>
									<s:text name="jsp.default_272"/>
									<% }
									if (matchRecordsBy.equals("" + MappingDefinition.MATCH_RECORDS_BY_ID)) { %>
									<s:text name="jsp.default_273"/>
									<% }
									if (matchRecordsBy.equals("" + MappingDefinition.MATCH_RECORDS_BY_ID_NAME)) { %>
									<s:text name="jsp.default_274"/>
									<% }
									if (matchRecordsBy.equals("" + MappingDefinition.MATCH_RECORDS_BY_ID_NAME_ACCOUNT)) { %>
									<s:text name="jsp.default_275"/>
									<% }
									if (matchRecordsBy.equals("" + MappingDefinition.MATCH_RECORDS_BY_NAME)) { %>
									<s:text name="jsp.default_276"/>
									<% }
								}
								%>
							</ffi:cinclude>
						</ffi:cinclude>
					</td>
          <td class="columndata" align="right">
					<% if (entitlementName != null && entitlementName.length() > 0) { %>
						<ffi:cinclude ifEntitled="<%=entitlementName%>" >
							<ffi:link url="${SecurePath}custommapping.jsp?MappingDefinitionID=${MappingDefinition.MappingID}"><img src="/cb/web/multilang/grafx/account/i_edit.gif" width="19" height="14" hspace="3" border="0"></ffi:link>
							<ffi:link url="${SecurePath}custommapping_delete_confirm.jsp?MappingDefinitionID=${MappingDefinition.MappingID}"><img src="/cb/web/multilang/grafx/account/i_trash.gif" width="14" height="14" hspace="3" border="0"></ffi:link>
						</ffi:cinclude>
						<ffi:cinclude ifNotEntitled="<%=entitlementName%>" >
							<img src="/cb/web/multilang/grafx/payments/i_edit_dim.gif" alt="<s:text name="jsp.default_298"/>" width="19" height="14" hspace="3" border="0">
							<img src="/cb/web/multilang/grafx/payments/i_trash_dim.gif" alt="<s:text name="jsp.default_297"/>" width="14" height="14" hspace="3" border="0">
						</ffi:cinclude>
					<% } else { %>
						<ffi:link url="${SecurePath}custommapping.jsp?MappingDefinitionID=${MappingDefinition.MappingID}"><img src="/cb/web/multilang/grafx/account/i_edit.gif" width="19" height="14" hspace="3" border="0"></ffi:link>
						<ffi:link url="${SecurePath}>custommapping_delete_confirm.jsp?MappingDefinitionID=${MappingDefinition.MappingID}"><img src="/cb/web/multilang/grafx/account/i_trash.gif" width="14" height="14" hspace="3" border="0"></ffi:link>
					<% } %>
                    </td>
                </tr>
				</ffi:list>
				<ffi:removeProperty name="GetOutputFormatInfo" />

				<tr>
					<ffi:cinclude value1="${Section}" value2="ACH">
						<td class="tbrd_full" colspan="9" align="center"><input class="submitbutton" type="button" name="submitButtonName" value="<s:text name="jsp.default_175"/>" onclick="window.location='<ffi:urlEncrypt url="${SecurePath}payments/achimport.jsp?UseLastRequest=TRUE"/>'"></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="WIRE">
						<td class="tbrd_full" colspan="9" align="center"><input class="submitbutton" type="button" name="submitButtonName" value="<s:text name="jsp.default_175"/>" onclick="window.location='<ffi:urlEncrypt url="${SecurePath}payments/wiretransferimport.jsp?UseLastRequest=TRUE"/>'"></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="PAYMENT">
						<td class="tbrd_full" colspan="9" align="center"><input class="submitbutton" type="button" name="submitButtonName" value="<s:text name="jsp.default_175"/>" onclick="window.location='<ffi:urlEncrypt url="${SecurePath}payments/paymentimport.jsp?UseLastRequest=TRUE"/>'"></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="TRANSFER">
						<td class="tbrd_full" colspan="9" align="center"><input class="submitbutton" type="button" name="submitButtonName" value="<s:text name="jsp.default_175"/>" onclick="window.location='<ffi:urlEncrypt url="${SecurePath}payments/transferimport.jsp?UseLastRequest=TRUE"/>'"></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="PPAY">
						<td class="tbrd_full" colspan="9" align="center"><input class="submitbutton" type="button" name="submitButtonName" value="<s:text name="jsp.default_175"/>" onclick="window.location='<ffi:urlEncrypt url="${SecurePath}cash/cashppupload.jsp?UseLastRequest=TRUE"/>'"></td>
					</ffi:cinclude>
				</tr>
				<tr>
					<ffi:cinclude value1="${Section}" value2="PPAY" operator="notEquals" >
						<td colspan="4"><img src="/cb/web/multilang/grafx/payments/sechdr_pymnts_btm.gif" width="750" height="11"></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="PPAY">
						<td colspan="4"><img src="/cb/web/multilang/grafx/cash/sechdr_cash_btm.gif" width="750" height="11"></td>
					</ffi:cinclude>
				</tr>
			</table>
		</div>
		<%-- Remove properties that may have been added by the add/edit pages --%>
		<ffi:removeProperty name="prevName"/>
		<ffi:removeProperty name="origMapping"/>
<%-- </BODY> --%>
		<s:include value="/pages/jsp/custom_mapping_grid.jsp"/>
		
		<td class="tbrd_full" colspan="9" align="center">
		<div id="submitButtonsInACHID" class="submitButtonsDiv">	
		<ffi:cinclude value1="${Section}" value2="PPAY">
								
							<s:url id="mappingListDoneURL" value="/pages/jsp/cash/cashppupload.jsp" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								<s:param name="UseLastRequest" value="TRUE"/>
							</s:url>
							<sj:a 
								id="mappingListDoneID"
								href="%{mappingListDoneURL}" 
								targets="firstDiv" 
								indicator="indicator" 
								button="true" 
								onSuccessTopics="mappingListDoneSuccessCash"
								onCompleteTopics="tabifyNotes"
							><s:text name="jsp.default_175"/></sj:a>             
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="WIRE">
							
							 <s:url id="mappingListDoneURL" value="/pages/jsp/wires/wiretransferimport.jsp" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
								<s:param name="UseLastRequest" value="TRUE"/>
							</s:url>
							<sj:a 
								id="mappingListDoneID"
								href="%{mappingListDoneURL}" 
								targets="selectImportFormatOfFileUploadID" 
								indicator="indicator" 
								button="true" 
								onSuccessTopics="mappingListDoneSuccessWire"
								onCompleteTopics="tabifyNotes"
							><s:text name="jsp.default_175"/></sj:a> 
						              
							
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="TRANSFER">

							
							<s:url id="mappingListDoneURL" value="/pages/jsp/transfers/transferimport.jsp" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
								<s:param name="UseLastRequest" value="TRUE"/>
							</s:url>
							<sj:a 
								href="%{mappingListDoneURL}" 
								targets="selecttransferImportFormatOfFileUploadID" 
								indicator="indicator" 
								button="true" 
								onClickTopics="onClickUploadTransferFormTopics" 
								onCompleteTopics="tabifyNotes"
							><s:text name="jsp.default_175"/></sj:a> 
						              
							
		</ffi:cinclude>				
		
		<ffi:cinclude value1="${Section}" value2="PAYMENT">
		<br>
							
							<s:url id="mappingListDoneURL" value="/pages/jsp/billpay/paymentimport.jsp" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
								<s:param name="UseLastRequest" value="TRUE"/>
							</s:url>
							<sj:a 
								href="%{mappingListDoneURL}" 
								targets="selectImportFormatOfFileUploadID" 
								indicator="indicator" 
								button="true" 
								onClickTopics="onClickUploadBillpayFormTopics" 
								onCompleteTopics="tabifyNotes"
							><s:text name="jsp.default_175"/></sj:a> 
						              
							
		</ffi:cinclude>	
		
		<ffi:cinclude value1="${Section}" value2="ACH">
							 <s:url id="mappingListDoneURL" value="/pages/jsp/ach/achimport.jsp" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
								<s:param name="UseLastRequest" value="TRUE"/>
							</s:url>
							<sj:a 
								id="mappingListDoneID"
								href="%{mappingListDoneURL}" 
								targets="achBatchImportDiv" 
								indicator="indicator" 
								button="true" 
								onSuccessTopics="mappingListDoneSuccessACH"
								onClickTopics="mappingListDoneOnClickTopicsACH"
								onCompleteTopics="tabifyNotes"
							><s:text name="jsp.default_175"/></sj:a> 
						              
							
		</ffi:cinclude>	
		</div>
				           </td>	
		
<ffi:removeProperty name="GetOutputFormat"/>
	
<script>

	$('#deleteMappingDialogID').addHelp(function(){
		var helpFile = $('#deleteMappingDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});

	$.subscribe('mappingListDoneOnClickTopicsACH', function(event,data) {
		cancelImportACHEntries();
	});
	function cancelImportACHEntries()
	{
		var importACHEntriesUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achimport.jsp?PartialImport=false"/>';
        if (ns.ach.doPartialImport == true)
            importACHEntriesUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achimport.jsp?PartialImport=true"/>';

            $.ajax({
    	        url: importACHEntriesUrl,
    	        success: function(data) {
    	        	ns.ach.showImportSection(data);
					$("#detailsPortlet").show();
    	    		
    	        }
    	    });
	}
</script>
