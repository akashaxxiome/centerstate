<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<ffi:setL10NProperty name='PageHeading' value='Approve Company Changes'/>

<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

<s:include value="inc/da-business-confirm-pre.jsp" />
<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-business-confirm.jsp-1"/>


