<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees"%>


<% 
	session.setAttribute("FFIClonePermissions", session.getAttribute("ClonePermissions"));
%>
		<div align="center">
			<%-- <table width="80%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
						<div align="center">
							<span class="sectionsubhead">
							<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminusers.jsp-1" parm0="${BusinessEmployeeForClone.FullName}"/>
							(<s:text name="jsp.default_576"/>)
							</span>
						</div>
					</td>
				</tr>
			</table> --%>
			
			<s:form id="SourceAndTargetAccountSelect" namespace="/pages/user" action="cloneAccountPermissions-verify" validate="false" theme="simple" method="post" name="SourceAndTargetAccountSelect">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<table width="90%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" class="adminBackground">
						<table border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="adminBackground" align="left" colspan="3">
								</td>
							</tr>
							<tr>
								<td class="adminBackground" align="center" colspan="3">
									<span class="sectionsubhead">
									<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/clone-account-permissions.jsp-1" parm0="${BusinessEmployeeForClone.FullName}"/>
									</span>
								</td>
							</tr>
							<tr><td class="adminBackground" colspan="3">&nbsp;</td>
							</tr>
							<tr>
								<td class="adminBackground" align="center">
									<span class="sectionsubhead">
									<s:text name="jsp.user_298"/>
									</span>
								</td>
								<td>&nbsp;</td>
								<td class="adminBackground" align="center">
									<span class="sectionsubhead">
									<s:text name="jsp.user_114.1"/>
									</span>
								</td>
							</tr>
							<tr>
								<td class="adminBackground" align="center">
									<ffi:setProperty name="Compare" property="Value1" value="${ClonePermissions.SourceObjectId}"/>
									<select name="ClonePermissions.SourceObjectId" id="sourceCloneAccounts" class="txtbox" style="width:450px" >
										<ffi:list collection="AccountListForClone" items="account">
											<ffi:setProperty name="Compare" property="Value2" value="${account.ID}"/>
											<option value="<ffi:getProperty name="account" property="ID"/>" <ffi:getProperty name="Compare" property="SelectedIfEquals"/>> 
												<ffi:getProperty name="account" property="RoutingNum"/> : <ffi:getProperty name="account" property="DisplayText"/> - <ffi:getProperty name="account" property="CurrencyCode"/> - <ffi:getProperty name="account" property="NickName"/>
											</option>
										</ffi:list>
									</select>
								</td>
								<td>&nbsp;</td>
								<td class="adminBackground" align="center">
									<%
										 com.ffusion.efs.tasks.entitlements.ClonePermissions clonePermissions = (com.ffusion.efs.tasks.entitlements.ClonePermissions)session.getAttribute("ClonePermissions");
										 java.util.List targetObjects = (clonePermissions == null)?null:clonePermissions.getTargetObjectIds();
										 String accountId = null;
									%>
									<select name="ClonePermissions.TargetObjectId" id="targetCloneAccounts" class="" multiple="multiple" style="width:450px;">
										<ffi:list collection="AccountListForClone" items="account">
											<ffi:getProperty name="account" property="ID" assignTo="accountId"/>
											<option value="<ffi:getProperty name="account" property="ID"/>" <%=((targetObjects != null)&&(targetObjects.contains(accountId)))?"selected":""%> >
												<ffi:getProperty name="account" property="RoutingNum"/> : <ffi:getProperty name="account" property="DisplayText"/> - <ffi:getProperty name="account" property="CurrencyCode"/> - <ffi:getProperty name="account" property="NickName"/>
											</option>
										</ffi:list>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="3">&nbsp;</td>
							</tr>
							<tr><td colspan="3" align="center"><span id="CloneAccountError"></span></td></tr>
							<tr>
								<td colspan="3">&nbsp;</td>
							</tr>
							<tr>
								<td class="adminBackground" align="center" colspan="3">
									<sj:a
										button="true"
										onClickTopics="cancelCloneUserPermLoad"
										><s:text name="jsp.default_82"/></sj:a>
									<sj:a
										formIds="SourceAndTargetAccountSelect"
										targets="verifyDiv"
										button="true"
										validate="true"
										validateFunction="customValidation"
										onBeforeTopics="beforeVerify"
										onCompleteTopics="completeVerify"
										><s:text name="jsp.user_66"/></sj:a>
								</td>
							</tr>
							<tr>
								<td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
								<td><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</s:form>
		</div>

<ffi:setProperty name="ClonePermissions" property="ClearTargetObjectIds" value=""/>

<script language="javascript">
	$(document).ready(function() {
		$("#targetCloneAccounts").extmultiselect().multiselectfilter();
		
		$("#sourceCloneAccounts").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"size":"50",
			"module":"cloneAccountPermissions",
			"source":"/cb/pages/user/cloneAcctPermAccountsLookupBoxAction.action"
		});
	});
	
	
	
</script>	
	

