<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setProperty name="PageTitle" value="Manage Users"/>
<ffi:setProperty name="topMenu" value="home"/>
<ffi:setProperty name="subMenu" value="preferences"/>
<ffi:setProperty name="sub2Menu" value="manageusers"/>

<ffi:setProperty name="SkipPage" value="TRUE"/>

<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_FROM %>' checkParent="true">
    <ffi:setProperty name="SkipPage" value="FALSE"/>
</ffi:cinclude>
<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_TO %>' checkParent="true">
    <ffi:setProperty name="SkipPage" value="FALSE"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_ADD_ACCOUNT %>' checkParent="true">
    <ffi:setProperty name="SkipPage" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS %>' checkParent="true">
    <ffi:setProperty name="SkipPage" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude value1="${EnableInternalTransfers}" value2="FALSE" operator="equals">
    <ffi:setProperty name="SkipPage" value="TRUE"/>
</ffi:cinclude>

<ffi:cinclude value1="${SkipPage}" value2="TRUE" operator="equals">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><ffi:getProperty name="PageTitle"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="<ffi:getProperty name="ServletPath"/>FFretail.css" type="text/css">
</head>
<ffi:setProperty name="TempURL" value="${SecurePath}user/user_addedit_exttransfers_save.jsp?Target=${SecurePath}user/user_addedit_payments.jsp" URLEncrypt="true"/>
<ffi:cinclude value1="${BackButton}" value2="" operator="notEquals">
    <ffi:setProperty name="TempURL" value="${SecurePath}user/user_addedit_exttransfers_save.jsp?Target=${SecurePath}user/user_addedit_transfers.jsp" URLEncrypt="true"/>
</ffi:cinclude>
<body onload="document.location.href='<ffi:getProperty name="TempURL"/>';">
</body>
</html>
</ffi:cinclude>
<ffi:cinclude value1="${SkipPage}" value2="TRUE" operator="notEquals">
<ffi:cinclude value1="${AddEditMode}" value2="Edit" operator="equals">
    <ffi:cinclude value1="${IsExternalTransfersPermissionsInitialized}" value2="" operator="equals">
        <ffi:setProperty name="IncludeFromSection" value="FALSE"/>
        <ffi:setProperty name="IncludeToSection" value="FALSE"/>
        <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_FROM %>' checkParent="true">
            <ffi:setProperty name="IncludeFromSection" value="TRUE"/>
        </ffi:cinclude>
        <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_TO %>' checkParent="true">
            <ffi:setProperty name="IncludeToSection" value="TRUE"/>
        </ffi:cinclude>
        <ffi:object name="com.ffusion.beans.util.StringList" id="AccountsCollectionNames" scope="session"/>
        <ffi:setProperty name="AccountsCollectionNames" property="Add" value="ExternalTransferAccounts"/>
        <ffi:setProperty name="FromOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_FROM %>'/>
        <ffi:setProperty name="ToOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_TO %>'/>

        <ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsFromFilters" scope="session"/>
        <ffi:setProperty name="FromFilters" value='<%= com.ffusion.beans.accounts.AccountFilters.EXTERNAL_TRANSFER_FROM_FILTER %>'/>
		<ffi:setProperty name="FromFilters" value="${FromFilters},VERIFYSTATUS!5,BPWSTATUS=1,AND"/>
        <ffi:setProperty name="AccountsFromFilters" property="Key" value="ExternalTransferAccounts"/>
        <ffi:setProperty name="AccountsFromFilters" property="Value" value="${FromFilters}"/>

        <ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsToFilters" scope="session"/>
        <ffi:setProperty name="ToFilters" value='<%= com.ffusion.beans.accounts.AccountFilters.EXTERNAL_TRANSFER_TO_FILTER %>'/>
		<ffi:setProperty name="ToFilters" value="${ToFilters},VERIFYSTATUS!5,BPWSTATUS=1,AND"/>

        <ffi:setProperty name="AccountsToFilters" property="Key" value="ExternalTransferAccounts"/>
        <ffi:setProperty name="AccountsToFilters" property="Value" value="${ToFilters}"/>

        <ffi:setProperty name="FeatureAccessAndLimitsTaskName" value="FFIExternalTransferAccessAndLimits"/>
        <ffi:setProperty name="TransferAccessAndLimitsTaskName" value="SecondaryUserTransferAccessAndLimits"/>
		<s:include value="/pages/jsp/user/inc/user_edit_exttransfers_permissions_init.jsp" />
    </ffi:cinclude>

    <ffi:setProperty name="IsExternalTransfersPermissionsInitialized" value="true"/>
</ffi:cinclude>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><ffi:getProperty name="PageTitle"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="<ffi:getProperty name="ServletPath"/>FFretail.css" type="text/css">
</head>
<body>
<div class="marginTop10" style="position:relative; overflow:hidden">
<ffi:help id="user_user_addedit_exttransfers" className="moduleHelpClass"/>
<p class="transactionHeading"><ffi:getProperty name="FFIEditSecondaryUser" property="FullNameWithLoginId"/> <%-- <ffi:getProperty name="SecondaryUser" property="FullNameWithLoginId"/> --%></p>
<form name="UserExternalTransfersForm" id="userExternalTransfersForm" action="/cb/pages/jsp/user/saveSecondaryUserExTransfers.action" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header"><s:text name="user.exTransferInformation"/></div>
   		<div class="paneContentWrapper">
   			<div class="instructions">
            	<s:text name="secondaryUser.exTransferTab.instructions"/>
            </div>
            <div class="instructions">
                <input name="EnableExternalTransfers" togglepermissions="true" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value="TRUE"  <ffi:cinclude value1="${EnableExternalTransfers}" value2="TRUE" operator="equals">checked</ffi:cinclude>> <span style="padding-right: 20px;" class="txt_normal_bold">Yes</span> <input name="EnableExternalTransfers" togglepermissions="true" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value="FALSE"  <ffi:cinclude value1="${EnableExternalTransfers}" value2="FALSE" operator="equals">checked</ffi:cinclude>> <span class="txt_normal_bold">No</span>
            </div>
            <div class="instructions" containerType="permissionsBlock" <ffi:cinclude value1="${EnableExternalTransfers}" value2="TRUE" operator="notEquals">style="display: none;"</ffi:cinclude>>
            	<s:text name="secondaryUser.exTransferTab.instructions2"/>
            </div>
   		</div>
   	</div>
</div>
<div containerType="permissionsBlock" <ffi:cinclude value1="${EnableExternalTransfers}" value2="TRUE" operator="notEquals">style="display: none;"</ffi:cinclude>>
<ffi:setProperty name="PermissionsTitle" value="External Transfer Permissions"/>
<ffi:setProperty name="EnableOperationDisplayValue" value="Transfers"/>
<ffi:setProperty name="NoAccountsMessage" value="(No accounts authorized for external transfers are available.)"/>
<ffi:setProperty name="IncludeFromSection" value="FALSE"/>
<ffi:setProperty name="IncludeToSection" value="FALSE"/>
<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_FROM %>' checkParent="true">
    <ffi:setProperty name="IncludeFromSection" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_TO %>' checkParent="true">
    <ffi:setProperty name="IncludeToSection" value="TRUE"/>
</ffi:cinclude>
<ffi:object name="com.ffusion.beans.util.StringList" id="AccountsCollectionNames" scope="session"/>
<ffi:setProperty name="AccountsCollectionNames" property="Add" value="ExternalTransferAccounts"/>
<ffi:setProperty name="FromOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_FROM %>'/>
<ffi:setProperty name="ToOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.EXTERNAL_TRANSFERS_TO %>'/>

<ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsFromFilters" scope="session"/>
<ffi:setProperty name="FromFilters" value='<%= com.ffusion.beans.accounts.AccountFilters.EXTERNAL_TRANSFER_FROM_FILTER %>'/>
<ffi:setProperty name="FromFilters" value="${FromFilters},VERIFYSTATUS!5,BPWSTATUS=1,AND"/>
<ffi:setProperty name="AccountsFromFilters" property="Key" value="ExternalTransferAccounts"/>
<ffi:setProperty name="AccountsFromFilters" property="Value" value="${FromFilters}"/>

<ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsToFilters" scope="session"/>
<ffi:setProperty name="ToFilters" value='<%= com.ffusion.beans.accounts.AccountFilters.EXTERNAL_TRANSFER_TO_FILTER %>'/>
<ffi:setProperty name="ToFilters" value="${ToFilters},VERIFYSTATUS!5,BPWSTATUS=1,AND"/>
<ffi:setProperty name="AccountsToFilters" property="Key" value="ExternalTransferAccounts"/>
<ffi:setProperty name="AccountsToFilters" property="Value" value="${ToFilters}"/>

<ffi:setProperty name="FeatureAccessAndLimitsTaskName" value="FFIExternalTransferAccessAndLimits"/>
<ffi:setProperty name="TransferAccessAndLimitsTaskName" value="SecondaryUserTransferAccessAndLimits"/>
<s:include value="/pages/jsp/user/inc/user_addedit_exttransfers_permissions.jsp" />
</div>
    <div class="btn-row">
    	<sj:a id="secUserExTransfersCancel" 
    	button="true" 
		summaryDivId="summary" 
		buttonType="cancel" 
		onClickTopics="showSummary,secondaryUsers.cancel">
    	<s:text name="jsp.default_82"/></sj:a>        
		<sj:a id="secUserExTransfersBack" button="true" buttontype="back" backstep="2"><s:text name="jsp.default_57"/></sj:a>        
		<sj:a id="secUserExTransfersNext" button="true"  formIds="userExternalTransfersForm" targets="targetDiv"	validate="true" validateFunction="customValidation" onBeforeTopics="secondaryUsers.clearErrors" onSuccessTopics="secondaryUsers.saveExTransfersSuccess"><s:text name="jsp.default_291"/></sj:a>        
    </div>
</form>
</div>
</body>
</html>
<%--
<ffi:removeProperty name="TempURL"/>
<ffi:removeProperty name="PermissionsTitle"/>
<ffi:removeProperty name="EnableOperationDisplayValue"/>
<ffi:removeProperty name="IncludeFromSection"/>
<ffi:removeProperty name="IncludeToSection"/>
<ffi:removeProperty name="AccountsCollectionNames"/>
<ffi:removeProperty name="FromOperationName"/>
<ffi:removeProperty name="ToOperationName"/>
<ffi:removeProperty name="FromFilters"/>
<ffi:removeProperty name="ToFilters"/>
<ffi:removeProperty name="AccountsFromFilters"/>
<ffi:removeProperty name="AccountsToFilters"/>
<ffi:removeProperty name="FeatureAccessAndLimitsTaskName"/>
<ffi:removeProperty name="NoAccountsMessage"/>
<ffi:removeProperty name="TransferAccessAndLimitsTaskName"/>
<ffi:removeProperty name="CancelButton"/>
<ffi:removeProperty name="BackButton"/>
<ffi:removeProperty name="NextButton"/>

<ffi:setProperty name="BackURL" value="${SecurePath}user/user_addedit_exttransfers.jsp" URLEncrypt="true"/>
--%>
</ffi:cinclude>

<ffi:removeProperty name="SkipPage"/>