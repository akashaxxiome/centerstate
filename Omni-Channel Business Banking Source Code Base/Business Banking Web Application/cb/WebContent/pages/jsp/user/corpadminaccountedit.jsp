<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:help id="user_corpadminaccountedit" className="moduleHelpClass"/>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>


<div id='PageHeading' style="display:none;"><s:text name="jsp.user_136"/></div>

<ffi:setProperty name="disableEdit" value=""/>
<ffi:setProperty name="disableStyle" value="class=\"submitbutton\""/>


<%
	session.setAttribute("FFIModifyAccountById", session.getAttribute("ModifyAccount"));
    String accountState = null;
    String accountCountry = null;
	
%>


<ffi:setProperty name="RegAvailableFlag" value="false"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
	<ffi:setProperty name="RegAvailableFlag" value="true"/>
	<ffi:cinclude value1="${ModifyAccount.TypeValue}" value2="1" operator="notEquals">
		<ffi:cinclude value1="${ModifyAccount.TypeValue}" value2="2" operator="notEquals">
			<ffi:cinclude value1="${ModifyAccount.TypeValue}" value2="12" operator="notEquals">
				<ffi:setProperty name="RegAvailableFlag" value="false"/>
			</ffi:cinclude>`
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>


<%-- include javascript for top navigation menu bar --%>


<script type="text/javascript">

	$(function(){
		$("#CountrySelect").combobox({'size':'22',searchFromFirst:true});
		$("#StateSelect").combobox({'size':'22',searchFromFirst:true});
		});	
		
    ns.admin.currentCountry = "<ffi:getProperty name='ModifyAccount' property='Contact.Country'/>";

    ns.admin.showStateForAcctConfig = function(obj) {
    var urlString = '/cb/pages/jsp/user/corpadminaccountcountrystate.jsp';
        if (ns.admin.currentCountry != obj.value)
        {
            ns.admin.currentCountry = obj.value;

            $.ajax({
                   type: 'POST',
                   url: urlString,
                   data: "countryCode=" + obj.value + "&CSRF_TOKEN=" + '<ffi:getProperty name='CSRF_TOKEN'/>',
                   success: function(data){
                    if(data.indexOf("option") != -1) {
                        $("#stateLabelNameId").html(js_user_state).append('<span class="required">*</span>');
                    } else {
                        $("#stateLabelNameId").html('<span></span>');
                    }
                    $("#stateSelectId").html(data);
               }
            });
        }
	/*
	$.post( urlString, { countryCode: objValue}, function(data) {

	    if(data.indexOf("option") != -1) {
	    	$("#stateLabelNameId").html('State:');
	    } else {
	    	$("#stateLabelNameId").html('<span></span>');
	    }
	   	$("#stateSelectId").html(data);
	});*/
}

function validateData( formName )
{
    var form = document.forms[formName];
<ffi:cinclude value1="${RegAvailableFlag}" value2="true">
    if (document.frmEditAccount.registerEnabled.checked) {
        document.frmEditAccount["ModifyAccount.REG_ENABLED"].value = "true";
    } else {
        document.frmEditAccount["ModifyAccount.REG_ENABLED"].value = "false";
    }
</ffi:cinclude>
    
}

</script>
<s:form id="frmEditAccount" namespace="/pages/user" name="frmEditAccount" action="modifyAccountById-verify" validate="false" method="post" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="ModifyAccount.REG_ENABLED" value="false">
<input type="hidden" name="RegAvailableFlag" value="<ffi:getProperty name="RegAvailableFlag"/>">
<div class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header">Account Information Summary</div>
		<div id="" class="paneContentWrapper summaryBlock label130">
			<div>
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_52"/></span>
				<span id="" class="inlineSection floatleft labelValue" style="width:auto">
					<ffi:getProperty name="ModifyAccount" property="BankName"/>
				</span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_98"/></span>
				<span id="" class="inlineSection floatleft labelValue" style="width:auto">
					<ffi:getProperty name="ModifyAccount" property="CurrencyCode" />
				</span>
			</div>
			
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_10"/></span>
				<span id="" class="inlineSection floatleft labelValue" style="width:auto">
					<ffi:getProperty name="ModifyAccount" property="DisplayText" />
				</span>
			</div>
			
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_11"/></span>
				<span id="" class="inlineSection floatleft labelValue" style="width:auto">
					<ffi:getProperty name="ModifyAccount" property="Type" />
				</span>
			</div>
		</div>
	</div>
</div>

<div class="rightPaneWrapper w71">

  <%-- include page header, PageHeading and PageText should be set --%>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
  	<div class="header"><s:text name="jsp.user_136"/></div>
        <table width="100%" border="0" cellspacing="3" cellpadding="0" class="tableData tdWithPadding">
                         <tr>
                            <td width="50%" class='<ffi:getPendingStyle fieldname="nickName" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><s:text name="jsp.user_204"/></td>
                            <ffi:cinclude value1="${ModifyAccount.ContactId}" value2="-1" operator="notEquals">
                                <td width="50%" class='<ffi:getPendingStyle fieldname="street" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><s:text name="jsp.user_31"/><span class="required">*</span></td>
                            </ffi:cinclude>
                         </tr>
                         <tr>
                             <td valign="middle" class="columndata" align="left">
                                 <input name="ModifyAccount.NickName" type="text" size="24" maxlength="40" class="ui-widget-content ui-corner-all" value='<ffi:getProperty name="ModifyAccount" property="NickName" />' <ffi:getProperty name="disableEdit"/>>
                             	<span class="sectionhead_greyDA"><ffi:getProperty name="oldNickName" /></span>
                             </td>
                             <ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
         							<ffi:cinclude value1="${RegAvailableFlag}" value2="true">
                                         <% String regEnabled = ""; %>
                                         <ffi:getProperty name="ModifyAccount" property="REG_ENABLED" assignTo="regEnabled" />
                                         <% if( regEnabled == null ) { regEnabled = ""; } %>
                                         <td class="sectionhead" <ffi:getProperty name="disableEdit"/>><input type="checkbox" name="registerEnabled" value="checkboxValue" border="0" <ffi:getProperty name="disableEdit"/> <%= ( regEnabled.equals("T") || regEnabled.equals("true") )  ? "checked" : "" %>><s:text name="jsp.user_275"/></td>
         							</ffi:cinclude>
         							<ffi:cinclude value1="${RegAvailableFlag}" value2="true" operator="notEquals">
         							</ffi:cinclude>
     							</ffi:cinclude>
               					<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" ></ffi:cinclude>
               					<ffi:cinclude value1="${ModifyAccount.ContactId}" value2="-1" operator="notEquals">
               						<td   valign="middle" class="columndata" align="left">
	                                    <input name="Contact.Street" type="text" size="24" maxlength="40" class="ui-widget-content ui-corner-all" value='<ffi:getProperty name="ModifyAccount" property="Contact.Street" />' <ffi:getProperty name="disableEdit"/>>
	                                    <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Street" /></span>
	                                </td>
               					</ffi:cinclude>
                        </tr>
                        <tr>
                        	<td><span id="ModifyAccount.NickNameError"></span></td>
                        	<ffi:cinclude value1="${ModifyAccount.ContactId}" value2="-1" operator="notEquals">
                        		<td><span id="Contact.StreetError"></span></td>
                        	</ffi:cinclude>
                        </tr>         
                        <ffi:cinclude value1="${ModifyAccount.ContactId}" value2="-1" operator="notEquals">
                             <tr>
                                 <td class='<ffi:getPendingStyle fieldname="street2" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><s:text name="jsp.user_30"/></td>
                                 <td class='<ffi:getPendingStyle fieldname="city" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap><s:text name="jsp.user_65"/><span class="required">*</span></td>
							 </tr>
							 <tr>
                                 <td   valign="middle" class="columndata" align="left">
                                     <input name="Contact.Street2" type="text" size="24" maxlength="40" class="ui-widget-content ui-corner-all" value='<ffi:getProperty name="ModifyAccount" property="Contact.Street2" />' <ffi:getProperty name="disableEdit"/>>
                                     <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Street2" /></span>
                                 </td>
                                 <td   valign="middle" class="columndata" align="left">
                                     <input name="Contact.City" type="text" size="24" maxlength="20" class="ui-widget-content ui-corner-all" value='<ffi:getProperty name="ModifyAccount" property="Contact.City" />' <ffi:getProperty name="disableEdit"/>>
                                     <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="City" /></span>
                                 </td>
                             </tr>
                             <tr>
                             	<td><span id="Contact.Street2Error"></span></td>
                             	<td><span id="Contact.CityError"></span></td>
                             </tr>
                             <tr>
                                
                                <!-- default selected country and state -->
                                    <ffi:getProperty name="ModifyAccount" property="Contact.Country" assignTo="accountCountry"/>
                                    <% if (accountCountry == null) { accountCountry = ""; } %>
                                    <ffi:getProperty name="ModifyAccount" property="Contact.State" assignTo="accountState"/>
                                    <% if (accountState == null) { accountState = ""; } %>

                                    <ffi:object id="GetStatesForCountry" name="com.ffusion.tasks.util.GetStatesForCountry" scope="session" />
                                        <ffi:setProperty name="GetStatesForCountry" property="CountryCode" value="<%= accountCountry %>" />
                                    <ffi:process name="GetStatesForCountry" />
                                     <%-- if there's no state by default country, we have to keep  stateLabelNameId and stateSelectId exist --%>
                                    <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
                                       <td id="stateLabelNameId" class='<ffi:getPendingStyle fieldname="state" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><span class="required">*</span></td>
                                     </ffi:cinclude>
                                     <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
                                          <td id="stateLabelNameId" class='<ffi:getPendingStyle fieldname="state" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><s:text name="jsp.user_299"/><span class="required">*</span></td>
                                    </ffi:cinclude>    
                                    <td class='<ffi:getPendingStyle fieldname="zipCode" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><s:text name="jsp.user_391"/><span class="required">*</span></td>
                             </tr>
                             <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
                                 <ffi:setProperty name="ModifyAccount" property="Contact.State" value=""/>
                             </ffi:cinclude>
                             <tr>
                                 
                                 <%-- if there's no state by default country, we have to keep  stateLabelNameId and stateSelectId exist --%>
                                    <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
                                            <td valign="middle" class="columndata" align="left">
                                                <div id="stateSelectId"></div>
                                            </td>
                                    </ffi:cinclude>
                                    <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
                                                <td valign="middle" class="columndata" align="left">
                                                    <div id="stateSelectId">
                                                        <div class="selectBoxHolder">
                                                            <select class="txtbox" id="StateSelect" name="Contact.State" size="1" <ffi:getProperty name="disableEdit"/>>
                                                                <option value="" <ffi:cinclude value1="<%= accountState %>" value2="" operator="equals">selected</ffi:cinclude>><s:text name="jsp.default_376"/></option>
                                                                <ffi:list collection="GetStatesForCountry.StateList" items="item">
                                                                    <option value='<ffi:getProperty name="item" property="StateKey"/>' <ffi:cinclude value1="<%= accountState %>" value2="${item.StateKey}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name='item' property='Name'/></option>
                                                                </ffi:list>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <ffi:cinclude value1="${oldDAObject.state}" value2="" operator="notEquals">
                                                        <ffi:list collection="GetStatesForCountry.StateList" items="item">
                                                            <ffi:cinclude value1="${oldDAObject.state}" value2="${item.StateKey}" operator="equals">
                                                                <ffi:setProperty name="oldState" value="${item.Name}"/>
                                                            </ffi:cinclude>
                                                        </ffi:list>
                                                    </ffi:cinclude>
                                               	<span class="sectionhead_greyDA"><ffi:getProperty name="oldState"/></span>
                                    </ffi:cinclude>
                                    <td   valign="middle" class="columndata" align="left">
	                                    <input name="Contact.ZipCode" type="text" size="24" maxlength="11" class="ui-widget-content ui-corner-all" value='<ffi:getProperty name="ModifyAccount" property="Contact.ZipCode" />' <ffi:getProperty name="disableEdit"/>>
	                                    <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="ZipCode" /></span>
	                                </td>
                             </tr>
                             <tr>
                             	<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals"><td><span id="Contact.StateError"></span></td></ffi:cinclude>
                             	<td><span id="Contact.ZipCodeError"></span></td>
                             </tr>
                             
                             <tr>
                               <td class='<ffi:getPendingStyle fieldname="country" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><s:text name="jsp.user_97"/><span class="required">*</span></td>
                               <td class='<ffi:getPendingStyle fieldname="phone" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><s:text name="jsp.user_252"/></td>
                             </tr>
                             <tr>
                                <td valign="middle" class="columndata" align="left">
                                   <div class="selectBoxHolder">
                                       <select class="ui-widget-content ui-corner-all" id="CountrySelect" name="Contact.Country" size="1" onchange="ns.admin.showStateForAcctConfig(this);" <ffi:getProperty name="disableEdit"/>>
                                           <option value="" <ffi:cinclude value1="<%= accountCountry %>" value2="" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_283"/></option>
                                           <ffi:list collection="Country_List" items="item">
                                               <option value='<ffi:getProperty name="item" property="CountryCode"/>' <ffi:cinclude value1="<%= accountCountry %>" value2="${item.CountryCode}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name='item' property='Name'/></option>
                                           </ffi:list>
                                       </select>
                                   </div>
                                   <ffi:cinclude value1="${oldDAObject.Country}" value2="" operator="notEquals">
                                       <ffi:list collection="Country_List" items="item">
                                           <ffi:cinclude value1="${oldDAObject.Country}" value2="${item.CountryCode}" operator="equals">
                                               <ffi:setProperty name="oldCountry" value="${item.Name}"/>
                                           </ffi:cinclude>
                                       </ffi:list>
                                   </ffi:cinclude>
                                   <span class="sectionhead_greyDA"><ffi:getProperty name="oldCountry"/></span>
                                 </td>    
                                 <td   valign="middle" class="columndata" align="left">
                                     <input name="Contact.Phone" type="text" size="24" maxlength="14" class="ui-widget-content ui-corner-all" value='<ffi:getProperty name="ModifyAccount" property="Contact.Phone" />' <ffi:getProperty name="disableEdit"/>>
                                     <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Phone" /></span>
                                 </td> 
                             </tr>
                             <tr>
                             	<td><span id="Contact.CountryError"></span></td>
                             	<td><span id="Contact.PhoneError"></span></td>
                             </tr>
                             <tr>
                                <td class='<ffi:getPendingStyle fieldname="faxPhone" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><s:text name="jsp.user_157"/></td>
                                <td class='<ffi:getPendingStyle fieldname="email" dacss="sectionheadDA" defaultcss="sectionhead" />' nowrap height="18"><s:text name="jsp.user_148"/></td>
                             </tr>
                             <tr>
                                 <td   valign="middle" class="columndata" align="left">
                                    <input name="Contact.FaxPhone" type="text" size="24" maxlength="14" class="ui-widget-content ui-corner-all" value='<ffi:getProperty name="ModifyAccount" property="Contact.FaxPhone" />' <ffi:getProperty name="disableEdit"/>>
                                    <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="FaxPhone" /></span>
                                 </td>
                                 <td  valign="middle" class="columndata" align="left">
                                   <input name="Contact.Email" type="text" size="24" maxlength="40" class="ui-widget-content ui-corner-all" value='<ffi:getProperty name="ModifyAccount" property="Contact.Email" />' <ffi:getProperty name="disableEdit"/>>
                                   <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Email" /></span>
                            	 </td>
                             </tr>
                             <tr>
                             	<td><span id="Contact.FaxPhoneError"></span></td>
                             	<td ><span id="Contact.EmailError"></span></td>
                          	</tr>
                                </ffi:cinclude>
			<%-- <tr>
			    <td align="center"><span class="required">* <s:text name="jsp.default_240"/></span></td>
			</tr> --%>
			<!-- <tr>
			    <td><br></td>
			</tr> -->
			
                       <%--  <tr>
                            <td align="center" nowrap>
		    						<ffi:setProperty name="tmpURL" value="/cb/pages/jsp/user/corpadminaccountedit-selected.jsp?aid=${ModifyAccount.ID}&rnum=${ModifyAccount.RoutingNum}" URLEncrypt="true" />
    								<!--<input class="submitbutton" type="reset" value="<s:text name="jsp.default_358"/>" border="0" onclick="document.frmEditAccount.action='<ffi:getProperty name="tmpURL"/>';document.frmEditAccount.submit();">-->
									<a class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon' 
										title='<s:text name="jsp.default_358.1" />' 
										href='#' 
										onClick="ns.admin.editAccountConfig('<ffi:getProperty name='tmpURL'/>')">
											<span class='ui-icon ui-icon-refresh'></span><span class="ui-button-text"><s:text name="jsp.default_358"/></span>
									</a>
									

									<sj:a 
										title='<s:text name="jsp.default_358.1" />'
										button="true"
										buttonIcon="ui-icon-refresh"
										href="#"
										onclick="ns.admin.editAccountConfig(%{tmpURL})">
											<s:text name="jsp.default_358.1"/>
									</sj:a>
										
		    						<ffi:removeProperty name="tmpURL"/>
									<sj:a 
										button="true"
										buttonIcon="ui-icon-close"
										onClickTopics="cancelCompanyForm"
										><s:text name="jsp.default_82"/></sj:a>
									<sj:a
										id="editAccountConfigSubmit"
										formIds="frmEditAccount"
										targets="verifyDiv"
										button="true"
										buttonIcon="ui-icon-check"
										validate="true"
										validateFunction="customValidation"
										onBeforeTopics="beforeVerify"
										onCompleteTopics="completeVerify"
										onErrorTopics="errorVerify"
										onclick="validateData( frmEditAccount );"
										><s:text name="jsp.default_366"/></sj:a>
                            </td>
                        </tr> --%>
 </table>
    
 <div align="center"><span class="required">* <s:text name="jsp.default_240"/></span></div>
 <div class="btn-row">
 <ffi:setProperty name="tmpURL" value="/cb/pages/jsp/user/corpadminaccountedit-selected.jsp?aid=${ModifyAccount.ID}&rnum=${ModifyAccount.RoutingNum}" URLEncrypt="true" />
<sj:a 
	title='<s:text name="jsp.default_358.1" />'
	button="true"
	href="#"
	onclick="ns.admin.editAccountConfig(%{tmpURL})">
	<s:text name="jsp.default_358.1"/>
</sj:a>

		<ffi:removeProperty name="tmpURL"/>
<sj:a 
	button="true"
	onClickTopics="cancelCompanyForm,cancelAccConfEditForm"
	><s:text name="jsp.default_82"/></sj:a>
<sj:a
	id="editAccountConfigSubmit"
	formIds="frmEditAccount"
	targets="verifyDiv"
	button="true"
	validate="true"
	validateFunction="customValidation"
	onBeforeTopics="beforeVerify"
	onCompleteTopics="completeVerify"
	onErrorTopics="errorVerify"
	onclick="validateData( frmEditAccount );"
	><s:text name="jsp.default_366"/></sj:a>
 </div>
  </div></div></div>
 </s:form>
<ffi:removeProperty name="GetStatesForCountry"/>
