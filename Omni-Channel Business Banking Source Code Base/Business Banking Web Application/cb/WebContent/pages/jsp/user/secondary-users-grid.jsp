<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<div id="secUsersGridContainer" width="150px">
<form id="secondaryUsersForm" id="secondaryUsersForm">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="secondaryUsersCount" id="secondaryUsersCount" value=""/>
<ffi:setGridURL grid="GRID_secUsers" name="DeleteURL" url="/pages/jsp/user/confirmDeleteSecondaryUser.action?userId={0}" parm0="alertId"/>
<ffi:setProperty name="tempURL" value="/pages/jsp/user/getSecondaryUsers.action?GridURLs=GRID_secUsers" URLEncrypt="true"/>
<s:url id="getSecondaryUsersUrl" value="%{#session.tempURL}" escapeAmp="false"/>
<h1 class="headerTitle"><s:text name="secondaryUser.gridPanelName" /></h1>
	<sjg:grid  
		id="secondaryUsersGrid"  
		sortable="true"  
		dataType="json"  
		href="%{getSecondaryUsersUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rownumbers="false"
		viewrecords="true"
		sortname="name"
		sortorder="asc"
		onGridCompleteTopics="secondaryUsers.secUsersGridOnComplete"
		> 
		<sjg:gridColumn name="active" index="active"	title="%{getText('jsp.user_Active')}" formatter="ns.secondaryUsers.activeColumnFormatter" />
		<sjg:gridColumn name="fullNameWithLoginId" index="name"	title="%{getText('jsp.default_283')}" sortable="true" />
		<sjg:gridColumn name="action" title="%{getText('jsp.default_27')}" hidden="true" hidedlg="true" cssClass="__gridActionColumn" formatter="ns.secondaryUsers.actionColumnFormatter" />
	</sjg:grid>
	
	<div class="btn-row" id="activeResetControlBox">
		<span class="ui-helper-clearfix">&nbsp;</span>	
		<sj:a 	id="resetUserStatus"	button="true"  onClickTopics="secondaryUsers.resetUsers" ><s:text name="jsp.default_358"/></sj:a>	
		<sj:a 	id="submitUserStatus"	button="true"  onClickTopics="secondaryUsers.activateUsers" ><s:text name="jsp.default_366"/></sj:a>			
	</div>
	</form>
	
</div>	