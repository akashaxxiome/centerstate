<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.beans.accounts.Accounts" %>
<ffi:help id="user_corpsummary" className="moduleHelpClass"/>

<%
session.setAttribute("entGroupId", request.getParameter("entGroupId"));
%>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>

<ffi:object id="IsValidGroupDescendant" name="com.ffusion.efs.tasks.entitlements.IsValidGroupDescendant" scope="request"/>
<ffi:setProperty name="IsValidGroupDescendant" property="AncestorEntGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="IsValidGroupDescendant" property="AncestorEntGroupType" value="Business"/>
<ffi:setProperty name="IsValidGroupDescendant" property="DescendantEntGroupId" value="${entGroupId}"/>
<ffi:process name="IsValidGroupDescendant"/>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>



<%-- include page header --%>


<div class="approvalDialogHt2" style="margin-bottom:40px;">		
	<div align="center" class="sectionhead marginBottom20">	
	
		<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${entGroupId}"/>
		<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value="EntGroupItem1"/>
		<ffi:process name="GetEntitlementGroup"/>
		<ffi:removeProperty name="entGroupId"/>

		<ffi:object id="GetNumMembers" name="com.ffusion.efs.tasks.entitlements.GetNumMembers" scope="session"/>
		<ffi:setProperty name="GetNumMembers" property="EntitlementGroupId" value="${EntGroupItem1.GroupId}"/>
		<ffi:process name="GetNumMembers"/>
		<ffi:removeProperty name="GetNumMembers" />
																											
		<ffi:getProperty name="EntGroupItem1" property="GroupName"/> 
		<br>
		<ffi:getProperty name="Entitlement_Group_Num_Members"/> <s:text name="jsp.user_365"/></td>
	</div>
	
	<div align="center" class="sectionhead" style="width:957px;">
		<ffi:object id="DisplayCurrency" name="com.ffusion.beans.common.Currency"/>
		
		<ffi:object id="GetAccountsForGroup" name="com.ffusion.tasks.admin.GetAccountsForGroup" scope="session"/>
			<ffi:setProperty name="GetAccountsForGroup" property="GroupID" value="${EntGroupItem1.groupId}"/>
			<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="${EntGroupItem1.GroupName}Accounts"/>
		<ffi:process name="GetAccountsForGroup"/>
		<ffi:removeProperty name="GetAccountsForGroup"/>
	
		<ffi:setProperty name="entGrpAccountsSessName" value="${EntGroupItem1.GroupName}Accounts"/>
		
		<%
			String sessionName = (String)session.getAttribute("entGrpAccountsSessName");
			com.ffusion.beans.accounts.Accounts accounts = (com.ffusion.beans.accounts.Accounts) session.getAttribute(sessionName);
			if(accounts == null) {
		%>
		
		<h4>Internal server error. No accounts found in session. </h4>
		
		<%		
			} // end if
			else if( accounts != null && accounts.size() == 0 ) {
		%>
			<hr><b><s:text name="jsp.user_228"/></b>
		<%
		}
		else {
		%>
			<ffi:setProperty name="adminSummaryURL" value="/pages/user/AdminSummaryGrid.action?accountsSessName=${EntGroupItem1.GroupName}Accounts&entGroupSessName=EntGroupItem1" URLEncrypt="true"/>
			<s:url id="adminSummaryURL" value="%{#session.adminSummaryURL}" escapeAmp="false"/>
			
			<sjg:grid
			id="adminAccountSummaryGrid"
			caption=""
			sortable="true"
			dataType="json"
			href="%{adminSummaryURL}"
			pager="true"
			gridModel="gridModel"
			rowList="10,15,20"
			rowNum="10"
			rownumbers="false"
			navigator="true"
			navigatorAdd="false"
			sortname="date"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			viewrecords="true"
			shrinkToFit="true"
			scroll="false"
			scrollrows="true"
			onGridCompleteTopics="addGridControlsEvents"			
			>		
			<sjg:gridColumn name="bankName" index="bankName" title="%{getText('jsp.default_63')}" sortable="false" width="80"/>
			<sjg:gridColumn name="routingNum" index="routingNum" title="%{getText('jsp.default_365')}" sortable="false" width="50"/>
			<sjg:gridColumn name="displayText" index="displayText" title="%{getText('jsp.default_16')}" sortable="false" width="70"/>
			<sjg:gridColumn name="accountDisplayText" index="accountType" title="%{getText('jsp.default_444')}" sortable="false" width="40" hidden="true"/>
			<sjg:gridColumn name="currency" index="currency" title="%{getText('jsp.default_125')}" sortable="false" width="30"/>
			<sjg:gridColumn name="nickName" index="nickName" title="%{getText('jsp.default_294')}" sortable="false" width="60" hidden="true"/>
			<sjg:gridColumn name="limitsString" index="limitsString" title="%{getText('jsp.default_588')}" sortable="false" width="100"/>
		</sjg:grid>
		<%
			}	// end else
		%>
	
	<br>

</div>
</div>

<ffi:removeProperty name="entGrpAccountsSessName"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/index.jsp"/>
<ffi:removeProperty name="PageHeading2"/>
<ffi:removeProperty name="EmptyAccountsCollection" />
<ffi:removeProperty name="DisplayCurrency" />
