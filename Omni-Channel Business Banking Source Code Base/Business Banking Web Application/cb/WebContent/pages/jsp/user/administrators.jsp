<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
		<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
		<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
		<ffi:process name="ThrowException"/>
</ffi:cinclude>

<div id="administratorsDiv">
<ffi:help id="user_administrators" className="moduleHelpClass"/>
<s:include value="inc/corpadmininfo-pre.jsp" />

<span id="pageHeading" style="display:none;"><s:text name="jsp.user_36"/></span>

<%-- Non Dual Approval processing starts --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals"> 
	<div class="paneWrapper">
	 	<div class="paneInnerWrapper">
	   		<table width="100%" class="tableData" border="0" cellspacing="0" cellpadding="3">
	   			<tr class="header">
					<td class="sectionsubhead adminBackground"><s:text name="jsp.user_292"/></td>
				<tr>
				<tr>
					<td class="columndata">
						<%
								boolean isFirst= true;
								String temp = "";
							%>
							<ffi:list collection="BusinessEmployees" items="emp">
								<ffi:setProperty name="empName" value="${emp.Name}"/>
								<%
								if( isFirst ) {
									isFirst = false;
									temp = temp + session.getAttribute( "empName" );
								} else {
									temp = temp + ", " + session.getAttribute( "empName" );
								}
								%>
							</ffi:list>
						 	<%
								session.setAttribute( "AdministratorStr", temp );
							%>
							<ffi:getProperty name="AdministratorStr"/>
						<ffi:cinclude value1="${AdministratorStr}" value2="" operator="equals">
							<s:text name="jsp.default_296"/>
						</ffi:cinclude>
							<ffi:removeProperty name="AdministratorStr"/>
							<ffi:removeProperty name="empName"/>
					</td>
				</tr>
	   		</table>
	 	</div>
	</div>
</ffi:cinclude> <%-- Non Dual Approval processing ends --%>

<%-- Dual Approval processing starts --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>"> 
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
	<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories"/>
	
	<ffi:object name="com.ffusion.tasks.dualapproval.ProcessDACategories" id="ProcessDACategories" />
	<ffi:process name="ProcessDACategories" />
	
	<%-- Administrators section has changed starts --%>
	<ffi:cinclude value1="${isAdministratorChanged}" value2="Y"> 
		<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
		<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}"/>
		<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="Admins"/>
		<ffi:process name="GetAdminsForGroup"/>
		
		<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
		<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>
		
		
		<ffi:removeProperty name="GetBusinessEmployees"/>
		<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
		<ffi:process name="GetBusinessEmployees"/>
		<ffi:removeProperty name="GetBusinessEmployees"/>
		<ffi:removeProperty name="TempBusinessEmployee"/>
		
		<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
		<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="Admins"/>
		<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
		<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
		<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
		<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
		<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
		<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
		<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
		<ffi:process name="GenerateUserAndAdminLists"/>
		<ffi:removeProperty name="adminIds"/>
		<ffi:removeProperty name="userIds"/>
		<ffi:removeProperty name="GenerateUserAndAdminLists"/>
		
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
		<ffi:process name="GetDACategoryDetails"/>
		
		<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
			<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
			<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
			<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
			<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
		<ffi:process name="GenerateListsFromDAAdministrators"/>
		
		<ffi:object id="GenerateCommaSeperatedAdminList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedAdminList"/>
		<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminEmployees" value="OldAdminEmployees"/>
		<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminGroups" value="OldAdminGroups"/>
		<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="sessionName" value="OldAdminStringList"/>
		<ffi:process name="GenerateCommaSeperatedAdminList"/>
		
		<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminEmployees" value="AdminEmployees"/>
		<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminGroups" value="AdminGroups"/>
		<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="sessionName" value="NewAdminStringList"/>
		<ffi:process name="GenerateCommaSeperatedAdminList"/>
			
		<div class="paneWrapper">
		 	<div class="paneInnerWrapper">
		   		<table width="100%" class="tableData" border="0" cellspacing="0" cellpadding="3">
		   			<tr class="header">
						<td class="sectionsubhead adminBackground" width="50%">Old Administrators</td>
						<td class="sectionsubhead adminBackground" >New Administrators</td>
					<tr>
					<tr>
						<td class="columndata">
							<ffi:getProperty name="OldAdminStringList"/>
						</td>
						<td class="columndata sectionheadDA">
							<ffi:getProperty name="NewAdminStringList"/>
						</td>
					</tr>
		   		</table>
		 	</div>
		</div>
			
		<ffi:removeProperty name="GetDACategoryDetails"/>
		<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
		<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>
	</ffi:cinclude> <%-- Administrators section has changed ends --%>
	
	<%-- Administrators section hasn't changed starts --%>
	<ffi:cinclude value1="${isAdministratorChanged}" value2="Y" operator="notEquals"> 
		<div class="paneWrapper">
		 	<div class="paneInnerWrapper">
		   		<table width="100%" class="tableData" border="0" cellspacing="0" cellpadding="3">
		   			<tr class="header">
						<td class="sectionsubhead adminBackground"><s:text name="jsp.user_292"/></td>
					<tr>
					<tr>
						<td class="columndata">
							<%
									boolean isFirst= true;
									String temp = "";
								%>
								<ffi:list collection="BusinessEmployees" items="emp">
									<ffi:setProperty name="empName" value="${emp.Name}"/>
									<%
									if( isFirst ) {
										isFirst = false;
										temp = temp + session.getAttribute( "empName" );
									} else {
										temp = temp + ", " + session.getAttribute( "empName" );
									}
									%>
								</ffi:list>
							 	<%
									session.setAttribute( "AdministratorStr", temp );
								%>
								<ffi:getProperty name="AdministratorStr"/>
							<ffi:cinclude value1="${AdministratorStr}" value2="" operator="equals">
								<s:text name="jsp.default_296"/>
							</ffi:cinclude>
								<ffi:removeProperty name="AdministratorStr"/>
								<ffi:removeProperty name="empName"/>
						</td>
					</tr>
		   		</table>
		 	</div>
		</div>
	</ffi:cinclude> <%-- Administrators section hasn't changed ends--%>
</ffi:cinclude>	<%-- Dual Approval processing ends --%>		
			
</div>


