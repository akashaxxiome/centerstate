<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<select class="txtbox" id="selectEntitlementGroup_X" name="X" size="1">
	<option
	value="-1" selected>
		Select a Profile						
	</option>
	<s:iterator value="UserProfiles"  var="profile">
	
	<s:if test="%{#profile.profileId == SelectedBizProfileID}">
		<option scope="<s:property value='#profile.profileScope'/>" selected="selected" value='<s:property value="#profile.profileId"/>'><s:property value="#profile.profileName"/></option>
	</s:if>
	<s:else>
		<option scope="<s:property value='#profile.profileScope'/>" value='<s:property value="#profile.profileId"/>'><s:property value="#profile.profileName"/></option>
	</s:else>
</s:iterator>
</select>
<div >
	<ol id="crossChannelChildId"></ol>
</div>
<span id="XError"></span>

