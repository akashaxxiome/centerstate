<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:setL10NProperty name='PageHeading' value='Confirm Approve Division'/>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<%		
	if( request.getParameter("itemId") != null ) { 
		String itemId = request.getParameter("itemId");
		session.setAttribute("itemId", itemId); 
	}

%>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.businessID}"/>
<ffi:process name="GetDAItem" />

<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE %>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:object name="com.ffusion.tasks.admin.AddBusinessGroup" id="AddBusinessGroup" />
	<ffi:setProperty name="AddBusinessGroup" property="CheckboxesAvailable" value="false"/>
	<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
		<ffi:cinclude value1="${details.fieldName}" value2="groupName">
			<ffi:setProperty name="AddBusinessGroup" property="GroupName" value="${details.newValue}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${details.fieldName}" value2="parentId">
			<ffi:setProperty name="AddBusinessGroup" property="ParentGroupId" value="${details.newValue}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${details.fieldName}" value2="entGroupType">
			<ffi:setProperty name="AddBusinessGroup" property="GroupType" value="${details.newValue}"/>
		</ffi:cinclude>
	</ffi:list>
	
	<ffi:object name="com.ffusion.tasks.dualapproval.GetAdminListFromDA" id="GetAdminListFromDA" />
		<ffi:setProperty name="GetAdminListFromDA" property="itemId" value="${itemId}"/>
		<ffi:setProperty name="GetAdminListFromDA" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		<ffi:setProperty name="GetAdminListFromDA" property="adminListName" value="AdminList"/>
	<ffi:process name="GetAdminListFromDA"/>
	
	<ffi:setProperty name="AddBusinessGroup" property="AdminListName" value="AdminList"/>
	<ffi:setProperty name="AddBusinessGroup" property="InitAutoEntitle" value="True"/>
	<ffi:process name="AddBusinessGroup"/>
	<ffi:process name="AddBusinessGroup"/>
	<%--
		<ffi:include page="${PathExt}user/inc/da-user-approve-permission.jsp" />
	--%>
	<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChanges" />
		<ffi:setProperty name="ApprovePendingChanges" property="businessId" value="${SecureUser.businessID}" />
		<ffi:setProperty name="ApprovePendingChanges" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>" />
		<ffi:setProperty name="ApprovePendingChanges" property="itemId" value="${itemId}" />
		<ffi:setProperty name="ApprovePendingChanges" property="approveDAItem" value="TRUE" />
	<ffi:process name="ApprovePendingChanges"/>
	
	<ffi:setProperty name="tempDisplayName" value="${AddBusinessGroup.GroupName}"/> 
</ffi:cinclude>

<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-division-add-confirm.jsp-1" parm0="${tempDisplayName}"/>


<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="AddBusinessGroup"/>
<ffi:removeProperty name="AdminList"/>
<ffi:removeProperty name="ApprovePendingChanges"/>
<ffi:removeProperty name="itemId" />
<ffi:removeProperty name="CATEGORY_BEAN" />
<ffi:removeProperty name="DAItem" />