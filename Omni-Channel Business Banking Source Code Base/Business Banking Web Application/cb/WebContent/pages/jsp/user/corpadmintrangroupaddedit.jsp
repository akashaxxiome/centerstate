<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<ffi:help id="user_corpadmintrangroupaddedit" className="moduleHelpClass"/>
<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web'/>/css/ui.supermultiselect.css" />
<script type="text/javascript" src="<s:url value='/web'/>/js/ui.supermultiselect.js"></script>


<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<%

String addflagStr = request.getParameter("addFlag")==null ? "": request.getParameter("addFlag");
String editTransactionGroup_GroupName = request.getParameter("EditTransactionGroup_GroupName")==null ? "": request.getParameter("EditTransactionGroup_GroupName");

	session.setAttribute("addFlag", addflagStr);
	session.setAttribute("EditTransactionGroup_GroupName", editTransactionGroup_GroupName);
%>
<ffi:cinclude value1="${addFlag}" value2="true" operator="equals">
	<div id='PageHeading' style="display:none;"><s:text name="jsp.user.addTranGroupTitle" /></div>
</ffi:cinclude>
<ffi:cinclude value1="${addFlag}" value2="true" operator="notEquals">
	<div id='PageHeading' style="display:none;"><s:text name="jsp.user.editTranGroupTitle" /></div>
</ffi:cinclude>

<ffi:object name="com.ffusion.tasks.business.AddTransactionGroup" id="AddTransactionGroup" />

<ffi:object name="com.ffusion.tasks.business.EditTransactionGroup" id="EditTransactionGroup" scope="session" />

<%
session.setAttribute("FFIAddTransactionGroup", session.getAttribute("AddTransactionGroup"));
session.setAttribute("FFIEditTransactionGroup", session.getAttribute("EditTransactionGroup"));
%>

<%-- Dual Approval for Transaction Groups --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="AddTransactionGroupToDA" />
	<ffi:removeProperty name="DA_TRANSACTION_GROUP" />
	<ffi:object id="AddTransactionGroupToDA" name="com.ffusion.tasks.dualapproval.AddTransactionGroupToDA" />
	<ffi:setProperty name="AddTransactionGroupToDA" property="AddFlag" value="${addFlag}" />
	<ffi:cinclude value1="${addFlag}" value2="true" operator="notEquals">
		<ffi:setProperty name="AddTransactionGroupToDA" property="ReadFromDA" value="true" />
		<ffi:setProperty name="AddTransactionGroupToDA" property="OldTransactionGroupName" value="${EditTransactionGroup_GroupName}" />
		<ffi:process name="AddTransactionGroupToDA" />
	</ffi:cinclude>
</ffi:cinclude>
<%-- END Dual Approval for Transaction Groups --%>

<%
session.setAttribute("FFIAddTransactionGroupToDA", session.getAttribute("AddTransactionGroupToDA"));
%>

<%
	String _addPage = ( String )session.getAttribute( "addFlag" );
	String _formSubmit = "";
	String _errorPage = ( String )session.getAttribute( "errorPage" );

	String _strTransactionGroupName = "";
	String _originalTransactionGroupName = "";

	if( _addPage.equals( "true" ) ) {
		// values for Add Transaction Group page
		_strTransactionGroupName = ( String )session.getAttribute( "transactionGroupName" );
		if( _strTransactionGroupName == null ) {
			_strTransactionGroupName = "";
		}
        _strTransactionGroupName = com.ffusion.util.HTMLUtil.encode(_strTransactionGroupName);
		_formSubmit = "/pages/user/addTransactionGroup.action";
	} else {
		// values for Edit Transaction Group page %>
		<ffi:getProperty name="EditTransactionGroup_GroupName" assignTo="_originalTransactionGroupName"/>
		<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="" operator="notEquals">
			<ffi:setProperty name="_originalTransactionGroupName" value="${AddTransactionGroupToDA.TransactionGroupName}" />
		</ffi:cinclude>
<%
		if( _originalTransactionGroupName == null ) {
			_originalTransactionGroupName = "";
		}
        _originalTransactionGroupName = com.ffusion.util.HTMLUtil.encode(_originalTransactionGroupName);
		_strTransactionGroupName = _originalTransactionGroupName;

		_formSubmit = "/pages/user/editTransactionGroup.action";
	}
	request.setAttribute("transactionGroupFormAction", _formSubmit);

%>

<%-- Retrieve the available typecodes --%>
<ffi:object name="com.ffusion.tasks.util.GetBAICodes" id="GetBAICodes" scope="request"/>
<ffi:setProperty name="GetBAICodes" property="Levels" value="<%= Integer.toString( com.ffusion.dataconsolidator.constants.DCConstants.BAI_TYPE_CODE_LEVEL_DETAIL ) %>"/>
<ffi:process name="GetBAICodes"/>

<ffi:cinclude value1="${addFlag}" value2="true" operator="notEquals">
	<%-- Retrieve the original type codes for the transaction group --%>
	<ffi:object name="com.ffusion.tasks.business.GetTypeCodesForGroup" id="GetTypeCodesForGroup" scope="request"/>
	<ffi:setProperty name="GetTypeCodesForGroup" property="TransactionGroup" value="${EditTransactionGroup_GroupName}"/>
	<ffi:process name="GetTypeCodesForGroup"/>
</ffi:cinclude>

<SCRIPT language=JavaScript type="text/javascript"><!--

	(function($){
		removeItemsFromCurrentList( document.TransactionGroupForm );
		
	})(jQuery);
	
	(function($){		
		// init supermultiselect i18n texts
		$.extend($.ui.supermultiselect, {
			locale: {
				addAll: js_multiSelect_addAll,
				removeAll: js_multiSelect_removeAll,
				itemsCount: js_multiSelect_itemsRemain
			}
		});

		$("#assignedTypecodesListId").supermultiselect({ 
					width: "100%",
				 	height: "200px",
				 	containedPool: $("#transactionGroupSelectPool"),
				 	sortable: true,
					beforeApplyItem: beforeApplyItemForTranGrp,
					afterRemove:function(){						
						var $superSelectMenu = $("#assignedTypecodesListId_sms");						
						ns.common.superSelectFocusHandler($superSelectMenu);
					},
					afterUpdate:function(){
						var $superSelectMenu = $("#assignedTypecodesListId_sms");						
						ns.common.superSelectFocusHandler($superSelectMenu);						
					}
						
		});
		$("#availableTypecodesListId").supermultiselect({ 
					width: "100%",
				 	height: "200px",
				   	toIds: [ "assignedTypecodesListId" ],
				 	containedPool: $("#transactionGroupSelectPool"),
					afterAdd:function(){
						var $superSelectMenu = $("#availableTypecodesListId_sms");						
						ns.common.superSelectFocusHandler($superSelectMenu);
					}

		});
		
		
	})(jQuery);
	
// this function will remove the selected items from a list
// and move them to the assigned typecodes list

$.subscribe('adminGroupValidateSaveTopic', function(event,data){
	return validateSave( document.TransactionGroupForm );
});

function transferOptionRight( form, list )
{
	var removeArray = new Array();

	// add the selected items to the assigned typecodes list
	// mark the indices to be removed from this list
	for( i = 0 ; i < list.length ; i++ ) {
		if( list.options[ i ].selected ) {
			// add the number and space to the text
			var tempText = list.options[ i ].text;
			
			var newoption = new Option( tempText, list.options[ i ].value, true, true);
			form.assignedTypecodesList.options[ form.assignedTypecodesList.length ] = newoption;
			removeArray[ removeArray.length ] = i;
		}
	}

	// then remove the items from the remove list in reverse order to preserve the indices
	for( i = removeArray.length - 1 ; i >= 0 ; i-- ) {
		var index = removeArray[ i ];
		list.options[ index ] = null;
	}

	// select the first list item
	list.selectedIndex = 0;
}

// this function will remove the desired items from the assigned typecodes list
// and move them to the available typecodes list
function transferOptionLeft( form, removeAll )
{
	var removeArray = new Array();

	for( i = 0 ; i < form.assignedTypecodesList.length ; i++ ) {
		if( removeAll || form.assignedTypecodesList.options[ i ].selected ) {

			// decide to which list to add the item
			// we use the first 5 characters of string so that we are certain that the indexOf() method
			// will return the correct result
			var listName = form.assignedTypecodesList.options[ i ].value.substr( 0, 5 );
			/*if( listName.indexOf( "user" ) != -1 ) {
				// remove the word (user) from the text
				tempText = tempText.substr( 0, tempText.length - 7 );

				var newoption = new Option( tempText, form.assignedTypecodesList.options[ i ].value, true, true);
				form.userList.options[ form.userList.length ] = newoption;
			}*/

			removeArray[ removeArray.length ] = i;
		}
	}

	// then remove the items from the remove list in reverse order to preserve the indices
	for( i = removeArray.length - 1 ; i >= 0 ; i-- ) {
		var index = removeArray[ i ];
		form.assignedTypecodesList.options[ index ] = null;

		// if we removed the last element, select the one above it
		if( form.assignedTypecodesList.length == index ) {
			form.assignedTypecodesList.selectedIndex = index - 1;
		} else {
			form.assignedTypecodesList.selectedIndex = index;
		}
	}
}

// this function will remove the desired items from the admin list
// and move them to the available typecodes list
function transferOptionLeft( form, removeAll )
{
	var removeArray = new Array();

	for( i = 0 ; i < form.assignedTypecodesList.length ; i++ ) {
		if( removeAll || form.assignedTypecodesList.options[ i ].selected ) {
			var tempText = form.assignedTypecodesList.options[ i ].text;

			var newoption = new Option( tempText, form.assignedTypecodesList.options[ i ].value, true, true);
			newoption.selected = false;
			form.availableTypecodesList.options[ form.availableTypecodesList.length ] = newoption;

			removeArray[ removeArray.length ] = i;
		}
	}

	// then remove the items from the remove list in reverse order to preserve the indices
	for( i = removeArray.length - 1 ; i >= 0 ; i-- ) {
		var index = removeArray[ i ];
		form.assignedTypecodesList.options[ index ] = null;

		// if we removed the last element, select the one above it
		if( form.assignedTypecodesList.length == index ) {
			form.assignedTypecodesList.selectedIndex = index - 1;
		} else {
			form.assignedTypecodesList.selectedIndex = index;
		}
	}
}

function callCancel()
{
	// return back to the previous page
	window.location.href="corpadmininfo.jsp";
}

function validateSave( form )
{       
	if( form.assignedTypecodesList.length > 0 ) {

		// mark all items as selected
		for( i = 0 ; i < form.assignedTypecodesList.options.length ; i++ ){
		    form.assignedTypecodesList.options[ i ].selected = "true";    
		}
  }
	return true;
}

// remove items from the available typecodes list that appear in the assigned typecodes list
function removeItemsFromCurrentList( form )
{
	for( i = 0 ; i < form.assignedTypecodesList.length ; i++ ) {
		var tempItem = form.assignedTypecodesList.options[ i ].value;

		// delete the elements of the user list that are in the chain item list
		for( j = 0 ; j < form.availableTypecodesList.length ; j++ ) {
			if( form.availableTypecodesList.options[ j ].value == tempItem ) {
				form.availableTypecodesList.options[ j ] = null;
				break;
			}
		}
	}
}

function beforeApplyItemForTranGrp(item, assignedTypecodesListId)
{
	item.data('optionLink').attr('mother', 'availableTypecodesListId');
	return true;
}

//--></SCRIPT>

	<s:form id="TransactionGroupForm" namespace="/pages/user" name="TransactionGroupForm" action="%{#request.transactionGroupFormAction}" validate="false" method="post" theme="simple">
    <ffi:removeProperty name="PageScope_FormSubmit" />
    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
    <ffi:cinclude value1="${addFlag}" value2="true" operator="notEquals">
    	<input type="hidden" name="originalGroupName" value="<%=_originalTransactionGroupName%>">
    </ffi:cinclude>
    <div align="center" id="transactionGroupSelectPool">
    <%-- include page header --%>
    <!-- 3 column table for icons -->
			<%-- <ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="" operator="notEquals">
				<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="${AddTransactionGroupToDA.OldTransactionGroupName}" operator="notEquals">
					<td colspan="2" width="35%" align="left" class="sectionheadDA">					
				</ffi:cinclude>
			    <ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="${AddTransactionGroupToDA.OldTransactionGroupName}" operator="equals">
			    	<td colspan="2" width="35%" align="left" class="sectionsubhead">
			    </ffi:cinclude>
			</ffi:cinclude> --%>
			<%-- <ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="" operator="equals"> --%>
				
			<%-- </ffi:cinclude> --%>
		    	<div class="marginleft10" style="width:100%" align="left"><span><s:text name="jsp.user_344"/><span class="required">*</span>&nbsp;&nbsp;</span>
		    	<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="" operator="notEquals">
		    		<input class="ui-widget-content ui-corner-all" type="text" name="transactionGroupName" value="<ffi:getProperty name="AddTransactionGroupToDA" property="TransactionGroupName" />" size="24" maxlength="50" style="width:180px;">
		    		<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="${AddTransactionGroupToDA.OldTransactionGroupName}" operator="notEquals">
		    			&nbsp;&nbsp;<span class="sectionhead_greyDA"><ffi:getProperty name="AddTransactionGroupToDA" property="OldTransactionGroupName" /></span>
		    		</ffi:cinclude>
		    	</ffi:cinclude>
		    	<ffi:cinclude value1="${AddTransactionGroupToDA.TransactionGroupName}" value2="" operator="equals">
			        <input class="ui-widget-content ui-corner-all" type="text" size="36" maxlength="50" style="width:180px;" name="transactionGroupName" value="<%=_strTransactionGroupName%>" size="24">
		        </ffi:cinclude>
		        <span id="transactionGroupNameError"></span></div>
            <table border="0" class="marginTop20 adminAccordianCls customAccordianBgClass" style="width:65% !important">

                <tr>
                    <td valign="top" align="right" width="48%">
                        <!-- Available Typecodes -->
                        <table border="0" class="tblborder" width="100%">
                            <tr>
                                <td align="center" class="sectionsubhead pane-header ui-state-default ui-corner-top ui-state-active"><s:text name="jsp.user_50"/></td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <SELECT id="availableTypecodesListId" name="availableTypecodesList" class="txtbox" multiple size="20">
										<%-- List the available typecodes --%>
										<%  com.ffusion.beans.dataconsolidator.BAITypeCodeInfo typeCode = null; 
											String lan = null; %>
										<ffi:getProperty name="UserLocale" property="Locale" assignTo="lan" />
										<ffi:list collection="GetBAICodes.CodeInfoList" items="typeCode">
										<% typeCode = (com.ffusion.beans.dataconsolidator.BAITypeCodeInfo)pageContext.findAttribute("typeCode"); %>
											<option value="<ffi:getProperty name="typeCode" property="Code"/>"><ffi:getProperty name="typeCode" property="Code"/>: <%= typeCode.getDescription(lan) %></option>
										</ffi:list>
                                    </SELECT>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <%-- <td>
                        <!-- central buttons -->
                        <table border=0>
                            <tr>
                            	<td>
                            		<sj:a button="true"	cssStyle="width:10em"	onClick="ns.admin.removeAllFromAssignedTypecodesList()"><s:text name="jsp.common_200" /></sj:a>
								</td>
							</tr>
                        </table>
                    </td> --%>
                    <td width="4%" valign="top"></td>
                    <td width="48%" valign="top">
                        <!-- Assigned Typecodes -->
                        <table border="0" class="tblborder" width="100%">
                            <tr>
                                <td align="center" class="pane-header ui-state-default ui-corner-top ui-state-active <ffi:getPendingStyle fieldname="typeCodes" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="DA_TRANSACTION_GROUP" />"><s:text name="jsp.user_48"/><span class="required">*</span></td>
                            </tr>
                            <tr>
                                <td valign="middle" align="left"><div style="position:relative">
                               <span class="removeAllLinkAdmin" style="position:absolute; right:0; top:0;"><sj:a  cssStyle="width:10em"	onClick="ns.admin.removeAllFromAssignedTypecodesList()"><s:text name="user.admin.grid.removeAll" /></sj:a></span>
                                <ffi:cinclude value1="${AddTransactionGroupToDA.TypeCodes}" value2="" operator="notEquals">
                                	<SELECT id="assignedTypecodesListId" name="assignedTypecodesList" class="txtbox" multiple size="20">
									<ffi:cinclude value1="${addFlag}" value2="true" operator="notEquals">
										<%-- List the assigned typecodes --%>
										<ffi:list collection="AddTransactionGroupToDA.TypeCodeList" items="typeCode">
											<% typeCode = (com.ffusion.beans.dataconsolidator.BAITypeCodeInfo)pageContext.findAttribute("typeCode"); %>
											<option value="<ffi:getProperty name="typeCode" property="Code"/>"><ffi:getProperty name="typeCode" property="Code"/>: <%= typeCode.getDescription(lan) %></option>
										</ffi:list>
									</ffi:cinclude>
								    </SELECT><span id="assignedTypecodesListError"></span>
                                </ffi:cinclude>                                
                                <ffi:cinclude value1="${AddTransactionGroupToDA.TypeCodes}" value2="" operator="equals">
                                	<ffi:object name="com.ffusion.tasks.business.GetTypeCodesForGroup" id="GetTypeCodesForGroup" scope="request"/>
                                	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="notEquals">
									<ffi:setProperty name="GetTypeCodesForGroup" property="TransactionGroup" value="${EditTransactionGroup_GroupName}"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">
										<ffi:setProperty name="GetTypeCodesForGroup" property="TransactionGroup" value="${AddTransactionGroupToDA.OldTransactionGroupName}"/>
									</ffi:cinclude>
									<ffi:process name="GetTypeCodesForGroup"/>
									
				    <SELECT id="assignedTypecodesListId" name="assignedTypecodesList" class="txtbox" multiple size="20" style="width:200px" >
					<ffi:cinclude value1="${addFlag}" value2="true" operator="notEquals">
						<%-- List the assigned typecodes --%>
						<ffi:list collection="GetTypeCodesForGroup.CodeInfoList" items="typeCode">
							<% typeCode = (com.ffusion.beans.dataconsolidator.BAITypeCodeInfo)pageContext.findAttribute("typeCode"); %>
							<option value="<ffi:getProperty name="typeCode" property="Code"/>"><ffi:getProperty name="typeCode" property="Code"/>: <%= typeCode.getDescription(lan) %></option>
						</ffi:list>
					</ffi:cinclude>
				    </SELECT><span id="assignedTypecodesListError"></span>
								</ffi:cinclude>
                              </div>  </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <ffi:cinclude value1="${AddTransactionGroupToDA.TypeCodes}" value2="" operator="notEquals">
	                <ffi:cinclude value1="${AddTransactionGroupToDA.OldTypeCodes}" value2="" operator="notEquals">
		               	<tr><td colspan="3">&nbsp;</td></tr>
		                <tr>
		                	<td></td>
		                	<td></td>
		                	 
							    <td valign="top">
							    	<table width="100%">
							    		<tr>
							    			<td>Old <s:text name="jsp.user_48"/>:</td>
							    			<td><ffi:list collection="AddTransactionGroupToDA.OldTypeCodeList" items="typeCode">
							    		<span class="sectionhead_greyDA"><ffi:getProperty name="typeCode" property="Code"/>: <ffi:getProperty name="typeCode" property="Description"/></span>, 
							    	</ffi:list></td>
							    		</tr>
							    	</table>
							    </td>
						</tr>
					</ffi:cinclude>
				</ffi:cinclude>
            </table>
            
<div align="center"><span class="required">* <s:text name="jsp.default_240"/></span></div>
<div class="btn-row">
<sj:a
	button="true" summaryDivId="companySummaryTabsContent" buttonType="cancel"
	onClickTopics="showSummary,cancelAddEditTransGrpForm"
	><s:text name="jsp.default_82"/></sj:a>
<sj:a
	formIds="TransactionGroupForm"
	button="true" 
	targets="resultmessage"
	validate="true" 
	validateFunction="customValidation"
	onSuccessTopics="successTGAdd,successTgGrpAdded"
	onErrorTopics="errorTGAdd"
	onBeforeTopics="beforeVerify"
	onCompleteTopics="completedTGAdd"
	onClickTopics="adminGroupValidateSaveTopic"
	><s:text name="jsp.default_366"/></sj:a>

</div>
</div>
</s:form>


<%-- Housekeeping --%>
<ffi:removeProperty name="GetBAICodes"/>
<ffi:cinclude value1="${addFlag}" value2="true" operator="notEquals"><ffi:removeProperty name="GetTypeCodesForGroup"/></ffi:cinclude>

<%-- Set appropriate BackURL --%>
<ffi:cinclude value1="${addFlag}" value2="true" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmintrangroupaddedit.jsp?addFlag=true" URLEncrypt="true" />
</ffi:cinclude>
<ffi:cinclude value1="${addFlag}" value2="true" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmintrangroupaddedit.jsp?addFlag=false&EditTransactionGroup_GroupName=${EditTransactionGroup_GroupName}" URLEncrypt="true" />
</ffi:cinclude>
