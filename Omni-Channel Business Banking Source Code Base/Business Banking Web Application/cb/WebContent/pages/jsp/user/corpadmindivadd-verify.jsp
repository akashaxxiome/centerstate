<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<%
	String ItemId = request.getParameter("ItemId");
	if(ItemId!= null){
		session.setAttribute("ItemId", ItemId);
	}
%>
<ffi:help id="user_corpadmindivadd-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_54')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="AddBusinessGroup" property="InitAutoEntitle" value="True"/>
	<ffi:process name="AddBusinessGroup"/>

	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:setProperty name="AddBusinessGroup" property="SuccessURL" value=""/>
	</ffi:cinclude>

	<ffi:setProperty name="AddBusinessGroup" property="ValidateOnly" value="true"/>	
	<ffi:process name="AddBusinessGroup"/>
</ffi:cinclude>
<table cellspacing="0" cellpadding="0" border="0" width="100%" class="marginBottom10 marginTop20">
	<tr>
		<td width="25%">
		
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				<div class="paneWrapper">
				<div class="paneInnerWrapper">
					<div class="header">
						<s:text name="admin.division.new.summary"/>
					</div>
					<div class="paneContentWrapper">
						<table cellspacing="0" cellpadding="3" border="0" width="100%">
							<tr>
								<td><s:text name="jsp.user_121"/>:&nbsp; <ffi:getProperty name="AddBusinessGroup" property="GroupName"/></td>
							</tr>
							<tr>
								<td>
								<table cellspacing="0" cellpadding="3" border="0" width="100%">
									<tr>
										<td valign="top" width="100"><s:text name="jsp.user_38"/>:&nbsp;</td>
										<td><ffi:getProperty name="AdministratorStr"/></td>
									</tr>
							</table>
						</tr>
					</table>
				</div>
				</div>
			</div>
			</ffi:cinclude>
		
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<div class="paneWrapper">
				<div class="paneInnerWrapper">
					<div class="header">
						<s:text name="admin.division.new.summary"/>
					</div>
					<div class="paneContentWrapper">
						<table cellspacing="0" cellpadding="3" border="0" width="100%">
							<tr>
								<td><s:text name="jsp.user_121"/>:&nbsp; <s:property value="%{#session.division.name}" /></td>
							</tr>
							<tr>
								<td>
								<table cellspacing="0" cellpadding="3" border="0" width="100%">
									<tr>
										<td valign="top" width="100"><s:text name="jsp.user_38"/>:&nbsp;</td>
										<td><s:property value="%{#session.division.businessEmployeeNames}"/></td>
									</tr>
								</table>
							</tr>
						</table>
					</div>
					</div>
				</div>
			</ffi:cinclude>
			</td>
			<td width="75%">
			<!--  
			   <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<s:url id="tmpRedirectURL" value="%{'addBusinessDivision-daExecute'}" escapeAmp="false" namespace="/pages/user">
						<s:param name="ItemId" value="%{#parameters.ItemId}"/>
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
						<s:param name="doAutoEntitle" value="%{'false'}"/>
					</s:url>
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
					<s:url id="tmpRedirectURL" value="%{'addBusinessDivision-execute'}" escapeAmp="false" namespace="/pages/user"/>
				</ffi:cinclude>
			-->
			<!--BO Migration:: Added condition to Support DA business for Add division -->
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				<s:url id="tmpRedirectURL" value="%{'addBusinessDivision-daExecute'}" escapeAmp="false" namespace="/pages/user">
					<s:param name="ItemId" value="%{#parameters.ItemId}"/>
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
					<s:param name="doAutoEntitle" value="%{'false'}"/>
				</s:url>
				<ffi:setProperty name="actionVerifyName" value="${tmpRedirectURL}" />
			</ffi:cinclude>					
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">					
				<ffi:setProperty name="actionVerifyName" value="AddDivision_confirm" />
			</ffi:cinclude>
			
			   <s:form id="divisionAutoEntitleFormId" namespace="/pages/user" action="%{#session.actionVerifyName}" theme="simple" method="post" name="autoEntConfirm">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<table cellspacing="0" cellpadding="3" border="0" width="70%" id="adminTblWidth">
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
						
						<s:if test="%{#session.division.AutoEntitle}">
							<tr>
								<td align="center" >
									&nbsp;&nbsp;&nbsp;&nbsp;
									<span class="columndata marginRight20 paddingLeft10"><s:text name="jsp.user_124"/></span>
									<input type="radio" name="autoEntitle" value="true" checked="checked"/><s:text name="jsp.default_467"/>&nbsp;&nbsp;
									<input type="radio" name="autoEntitle" value="false"/><s:text name="jsp.default_295"/>
								</td>
								<td>&nbsp;</td>
							</tr>
						</s:if>
					
					</ffi:cinclude>
					<tr>
						<td align="center" class="sectionhead"><s:text name="jsp.user_378"/></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center" >
							<br><br><sj:a id="backBtn2"
								   button="true" 
								   onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
								<sj:a id="cancelBtn2"
								   button="true" 
								   summaryDivId="summary" buttonType="cancel"
								   onClickTopics="cancelDivisionForm"><s:text name="jsp.default_82"/></sj:a>
								<sj:a
									formIds="divisionAutoEntitleFormId"
									targets="confirmDiv"
									button="true" 
									validate="false"
									onBeforeTopics="beforeSubmit" onCompleteTopics="completeSubmit" onErrorTopics="errorSubmit" onSuccessTopics="successSubmit"
									><s:text name="jsp.default_366"/></sj:a>
						</td>
						<td>&nbsp;</td>
					</tr>
				</table>
				</s:form>
			</td>
		</tr>	
	</table>
