<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<s:include value="/pages/jsp/user/inc/user_delete_init.jsp" />

<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/accounts/accounts%{#session.minVersion}.css'/>"></link>

<div align="center">
<form name="UserDeleteForm" action="" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="blockWrapper">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><!-- L10NStart -->First Name:<!-- L10NEnd --></span>
                <span><ffi:getProperty name="TempSecondaryUser" property="FirstName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><!-- L10NStart -->Last Name:<!-- L10NEnd --></span>
                <span class=""><ffi:getProperty name="TempSecondaryUser" property="LastName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionLabel"><!-- L10NStart -->User Name:<!-- L10NEnd --></span>
            <span><ffi:getProperty name="TempSecondaryUser" property="UserName"/></span>
		</div>
	</div>
</div>
<ffi:setProperty name="SecondaryUserToModifyID" value="${TempSecondaryUser.Id}"/>
<ffi:setProperty name="TransferOrCancelTransactionsInputName" value='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_TRANSACTIONS_TRANSFER_OR_CANCEL %>'/>
<s:include value="/pages/jsp/user/inc/user_pending_transactions.jsp" />

<ffi:cinclude value1="${HasExpiredTransaction}" value2="true" operator="notEquals">
    <!-- <div style="text-align: center;">
        L10NStartAre you sure you want to delete this user?L10NEnd
    </div> -->
</ffi:cinclude>
    <div class="marginTop30">&nbsp;</div>
    <div  class="ui-widget-header customDialogFooter">
		<sj:a id="cancelDeleteUser" button="true"><s:text name="jsp.default_82"/></sj:a>
		<ffi:cinclude value1="${HasExpiredTransaction}" value2="true" operator="notEquals">
			<sj:a id="deleteUser" button="true"><s:text name="jsp.default_162" /></sj:a>
		</ffi:cinclude>
    </div>
</form>

</div>

    <ffi:removeProperty name="HasExpiredTransaction"/>
