<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines"%>
<%@page import="com.ffusion.beans.business.Business"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<s:url id="usersProfileLink" value="/cb/pages/jsp/user/corpadminprofiles.jsp"/>
<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>
<ffi:setProperty name="Business" property="EntitlementGroup.Properties.CurrentProperty" value="<%=ProfileDefines.BUSINESS_MIGRATION_FLAG %>"/>
<div class="dashboardUiCls">
   	<div class="moduleSubmenuItemCls">
   		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-adminUsers"></span>
   		<span class="moduleSubmenuLbl">
   			<s:text name="jsp.home_229" />
   		</span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
		
		<!-- dropdown menu include -->    		
   		<s:include value="/pages/jsp/home/inc/adminSubmenuDropdown.jsp" />
  	</div>
   	
   	<!-- dashboard items listing for admin -->
	<div id="dbUsersSummary" class="dashboardSummaryHolderDiv">
		<!-- Users summary link -->
		
			<s:url id="usersUrl" value="/pages/jsp/user/admin_users_summary.jsp" escapeAmp="false">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a
				id="usersLink"
				href="%{usersUrl}"
				targets="summary"
				indicator="indicator"
				button="true"
				cssClass="summaryLabelCls"
				onSuccessTopics="onLoadUsersSummaryLoadedSuccess">
			<s:text name="jsp.user_366"/></sj:a>
			
		<!-- Add Users link -->
			<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
			<ffi:process name="CanAdministerAnyGroup"/>
            <ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
                <s:url id="addUserURL" value="/pages/user/addBusinessUser_init.action" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="Init" value="true"/>
				</s:url>
                <sj:a id="addUserLink"
                   href="%{addUserURL}"
                   targets="inputDiv"
                   onClickTopics="beforeLoad"
                   button="true"
                   onCompleteTopics="completeUserLoad"
                   onSuccessTopics="successUserLoad"><s:text name="jsp.user_28"/></sj:a>
            </ffi:cinclude>
            
            
		    <s:url id="editUserUrl" value="/pages/jsp/user/corpadminuseredit.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		    </s:url>
		    
		    <sj:a id="editUserLink" href="%{editUserUrl}" targets="inputDiv" button="true" title="%{getText('jsp.user_146')}"
		        onClickTopics="beforeLoad" onCompleteTopics="completeUserLoad" onErrorTopics="errorLoad" cssStyle="display:none;">
		      <span class="ui-icon ui-icon-wrench"></span>
		    </sj:a>
		
			<s:url id="userPermissionsUrl" value="/pages/jsp/user/admin_permissions.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			
			<sj:a id="userPermissionsLink" href="%{userPermissionsUrl}" targets="permissionsDiv" button="true" title="%{getText('jsp.default_569')}"
				onClickTopics="beforePermLoad" onCompleteTopics="completeUserPermLoad" onErrorTopics="errorLoad" cssStyle="display:none;">
			  <span class="ui-icon ui-icon-wrench"></span>
			</sj:a>
		
			<s:url id="approvalGroupAssignUrl" value="/pages/jsp/user/corpadminapprovalgroupassign.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="approvalGroupAssignLink" href="%{approvalGroupAssignUrl}" targets="inputDiv" button="true" title="%{getText('jsp.user_47')}"
				onClickTopics="beforeLoad" onCompleteTopics="completeUserLoad" onErrorTopics="errorLoad" cssStyle="display:none;">
			  <span class="ui-icon ui-icon-wrench"></span>
			</sj:a>
	</div>
	
	<!-- dashboard items listing for requested images, hidden by default -->
	<div id="dbProfilesSummary" class="dashboardSummaryHolderDiv  hideDbOnLoad">
		<ffi:cinclude value1="${Business.EntSysType}" value2="<%= ProfileDefines.ENT_SYS_TYPE_PROFILE%>" operator="equals">
			<s:url id="profileUrl" value="/pages/jsp/user/businessuserprofiles_summary.jsp" escapeAmp="false">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a
				id="profilesLink"
				href="%{profileUrl}"
				targets="profileSummary"
				indicator="indicator"
				button="true"
				cssClass="summaryLabelCls"
				onSuccessTopics="onLoadProfileSummaryLoadedSuccess"
			><s:text name="user.profiles.summary"/></sj:a>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.EntSysType}" value2="<%= ProfileDefines.ENT_SYS_TYPE_PROFILE%>" operator="notEquals">
			<s:url id="profileUrl" value="/pages/jsp/user/admin_profiles_summary.jsp" escapeAmp="false">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a
				id="profilesLink"
				href="%{profileUrl}"
				targets="profileSummary"
				indicator="indicator"
				button="true"
				cssClass="summaryLabelCls"
				onSuccessTopics="onLoadProfileSummaryLoadedSuccess"
			><s:text name="user.profiles.summary"/></sj:a>		
		</ffi:cinclude>
		
		<!--
		<ffi:cinclude value1="${Business.EntitlementGroup.Properties.ValueOfCurrentProperty}" value2="true" operator="equals">
			<s:url id="BusinessProfileUrl" value="/pages/jsp/user/businessuserprofiles_summary.jsp" escapeAmp="false">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a
				id="businessProfilesLink"
				href="%{BusinessProfileUrl}"
				targets="profileSummary"
				indicator="indicator"
				button="true"
				onSuccessTopics="onLoadProfileSummaryLoadedSuccess"
			><s:text name="user.business.profiles.summary"/></sj:a>
		</ffi:cinclude>-->
		<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
			<ffi:cinclude value1="${Business.EntSysType}" value2="<%= ProfileDefines.ENT_SYS_TYPE_PROFILE%>" operator="equals">
				<s:url id="addBusinessProfileURL" value="/pages/jsp/businessuserprofile/addBusinessUserProfile_init.action">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				<sj:a id="addBusinessProfileLink"
			   href="%{addBusinessProfileURL}"
			   targets="inputDiv"
			   onClickTopics="beforeLoad"
			   onCompleteTopics="completeUserLoad"
			   onSuccessTopics="successProfileLoad"
			   button="true"><s:text name="jsp.user_26"/></sj:a>
          </ffi:cinclude>
		  <ffi:cinclude value1="${Business.EntSysType}" value2="<%= ProfileDefines.ENT_SYS_TYPE_PROFILE%>" operator="notEquals">
				<s:url id="addProfileURL" value="/pages/user/addUserProfile_init.action">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				<sj:a id="addProfileLink"
			   href="%{addProfileURL}"
			   targets="inputDiv"
			   onClickTopics="beforeLoad"
			   onCompleteTopics="completeUserLoad"
			   onSuccessTopics="successProfileLoad"
			   button="true"><s:text name="jsp.user_26"/></sj:a>
          </ffi:cinclude>
          <ffi:cinclude value1="${Business.EntitlementGroup.Properties.ValueOfCurrentProperty}" value2="true" operator="equals">
			<s:url id="addBusinessProfileURL" value="/pages/jsp/businessuserprofile/addBusinessUserProfile_init.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="addBusinessProfileLink"
		   href="%{addBusinessProfileURL}"
		   targets="inputDiv"
		   onClickTopics="beforeLoad"
		   onCompleteTopics="completeUserLoad"
		   onSuccessTopics="successBusinessProfileLoad"
		   button="true"><s:text name="jsp.user.profile.add.biz.profile.link"/></sj:a>
          </ffi:cinclude>     
		</ffi:cinclude>

			<!-- edit links -->
			<s:url id="editProfileUrl" value="/pages/jsp/user/corpadminprofileedit.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		    </s:url>
		    <sj:a id="editProfileLink" href="%{editProfileUrl}" targets="inputDiv" button="true" title="%{getText('jsp.user_146')}"
		        onClickTopics="beforeLoad" onCompleteTopics="completeUserLoad" cssStyle="display:none;"  onErrorTopics="errorLoad">
		      <span class="ui-icon ui-icon-wrench"></span>
		    </sj:a>
		    
		    <s:url id="profilePermissionsUrl" value="/pages/jsp/user/admin_permissions.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="profilePermissionsLink" href="%{profilePermissionsUrl}" targets="permissionsDiv" button="true" title="%{getText('jsp.default_569')}"
				onClickTopics="beforePermLoad" onCompleteTopics="completeUserPermLoad" onErrorTopics="errorLoad" cssStyle="display:none;">
			  <span class="ui-icon ui-icon-wrench"></span>
			</sj:a>
		    
			
	</div>
	
	<div id="dbCloneUserPermissionSummary" class="dashboardSummaryHolderDiv  hideDbOnLoad">
		<%-- user permission (Clone user permissions) button, it would be shown only in user permission, otherwise hidden. --%>
	    <s:url id="cloneUserPermissionsUrl" value="%{#session.PagesPath}user/clone-user-permissions-init.jsp">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
	    <sj:a 
			id="cloneUserPermissionsButton" 
			href="%{cloneUserPermissionsUrl}" 
			targets="inputDiv" 
			button="true" 
	    	title="%{getText('jsp.user_71')}"
			onClickTopics="beforeCloneUserAndAccountPermLoad"
			onCompleteTopics="completeLoad" 
			onErrorTopics="errorLoad"
			>
		  	&nbsp;<s:text name="jsp.user_71.1"/></sj:a>
		<%-- user permission (Clone account permissions) button, it would be shown only in user permission, otherwise hidden. --%>
	    <s:url id="cloneAccountPermissionsUrl" value="%{#session.PagesPath}user/clone-account-permissions-init.jsp">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
		</s:url>
	    <sj:a 
			id="cloneAccountPermissionsButton" 
			href="%{cloneAccountPermissionsUrl}" 
			targets="inputDiv" 
			button="true" 
	    	title="%{getText('jsp.user_392')}"
			onClickTopics="beforeCloneUserAndAccountPermLoad"
			onCompleteTopics="completeLoad" 
			onErrorTopics="errorLoad"
			>
		  	&nbsp;<s:text name="jsp.user_392.1" /></sj:a>
	</div>
	
	
	<!-- More list option listing -->
	<div class="dashboardSubmenuItemCls">
		<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
	
		<!-- UL for rendering tabs -->
		<ul class="dashboardSubmenuItemList">
			<li class="dashboardSubmenuItem"><a id="showdbUsersSummary" onclick="ns.common.refreshDashboard('showdbUsersSummary',true)"><s:text name="jsp.home_229"/></a></li>
			<li class="dashboardSubmenuItem"><a id="showdbProfilesSummary" onclick="ns.common.refreshDashboard('showdbProfilesSummary',true)"><s:text name="jsp.user_269"/></a></li>
			<!-- <li class="dashboardSubmenuItem"><a id="showdbCloneUserPermissionSummary" onclick="ns.common.refreshDashboard('showdbCloneUserPermissionSummary',true)">Clone Permissions</a></li> -->
		</ul>
	</div>

</div> 


	
<script>
	$("#usersLink").find("span").addClass("dashboardSelectedItem");
</script>