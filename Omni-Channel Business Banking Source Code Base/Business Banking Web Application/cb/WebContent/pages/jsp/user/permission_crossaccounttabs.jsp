<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
    <ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

	<%-- cross account sub tabs --%>
	<s:include value="%{#session.PagesPath}user/inc/checkentitled-crossaccountcomponents.jsp" />
	<div class="marginTop20"></div>
	<%
		int xcount = 0;
	%>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
	
		<sj:tabbedpanel id="XAcctTabs" selectedTab="%{#parameters.secondLavelPermissionTabIndex}" collapsible="false" onBeforeTopics="changeCrossAccountTab"
		onCompleteTopics="crossAcctTabsCompleteTopics">
			
				<ffi:list collection="ComponentNamesCross" items="currComponentName">
					<% String localizedComponentName = null; %>
					<ffi:getProperty name="ComponentNameLocaleNameMap" property="${currComponentName}" assignTo="localizedComponentName"/>
					<% session.setAttribute("xcount", String.valueOf(xcount)); %>	
					<ffi:setProperty name="tempComponentName" value="${localizedComponentName}"/>
					<ffi:setProperty name="Targets" value="user_crossaccountpermissions${xcount}"/>
					<ffi:setProperty name="componentName" value="${currComponentName}"/>
					<% 
						// The componentName must not have special characters. If new tabs are added, set-page-da-css.jsp must be updated to highlight them.
						String componentName = (String)session.getAttribute("componentName");
					 	if(componentName.length() > 0) {
					 		componentName = componentName.replaceAll("[!@#$%^*(){};:/_? ]+", "");
							componentName = componentName.replace("&", "And");
					 		componentName = componentName.substring(0, 1).toLowerCase() + componentName.substring(1, componentName.length());
					 	}
					 	session.setAttribute("componentName", componentName);
					%>					
					<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
							<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/crossaccountpermissions.jsp?runSearch=&ComponentIndex=${xcount}&perm_target=${Targets}&completedTopics=success${Targets}Topics&nextPermission=${Targets}Next" URLEncrypt="true"/>
							<sj:tab id="%{#session.componentName}" href="%{#session.tempURL}" label="%{#session.tempComponentName}"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
							<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/crossaccountpermissions.jsp?Init=TRUE&ComponentIndex=${xcount}&perm_target=${Targets}&completedTopics=success${Targets}Topics&nextPermission=${Targets}Next" URLEncrypt="true"/>
							<sj:tab id="%{#session.componentName}" href="%{#session.tempURL}" label="%{#session.tempComponentName}"/>
						</ffi:cinclude>
					</s:if>
					<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
						<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
								<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/crossaccountpermissions.jsp?Init=TRUE&ComponentIndex=${xcount}&perm_target=${Targets}&completedTopics=success${Targets}Topics&nextPermission=${Targets}Next" URLEncrypt="true"/>
								<sj:tab id="%{#session.componentName}" href="%{#session.tempURL}" label="%{#session.tempComponentName}"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
								<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/crossaccountpermissions.jsp?Init=TRUE&ComponentIndex=${xcount}&perm_target=${Targets}&completedTopics=success${Targets}Topics&nextPermission=${Targets}Next" URLEncrypt="true"/>
								<sj:tab id="%{#session.componentName}" href="%{#session.tempURL}" label="%{#session.tempComponentName}"/>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
								<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/crossaccountpermissions.jsp?runSearch=&ComponentIndex=${xcount}&perm_target=${Targets}&completedTopics=success${Targets}Topics&nextPermission=${Targets}Next" URLEncrypt="true"/>
								<sj:tab id="%{#session.componentName}" href="%{#session.tempURL}" label="%{#session.tempComponentName}"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
								<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/crossaccountpermissions.jsp?Init=TRUE&ComponentIndex=${xcount}&perm_target=${Targets}&completedTopics=success${Targets}Topics&nextPermission=${Targets}Next" URLEncrypt="true"/>
								<sj:tab id="%{#session.componentName}" href="%{#session.tempURL}" label="%{#session.tempComponentName}"/>
							</ffi:cinclude>
						</ffi:cinclude>
					</s:if>
					<%  xcount++;  %>
				</ffi:list>

		</sj:tabbedpanel>
		
	<ffi:removeProperty name="currComponentName"/>
	<ffi:removeProperty name="LocalizedComponentNamesCross"/>
	<ffi:removeProperty name="tempComponentName"/>
	<ffi:removeProperty name="componentName"/>	
<script type="text/javascript">
	if(accessibility){
		$("#XAcctTabs" ).tabs({
			show: function(event,ui){
					$.publish("common.topics.tabifyNotes");
					$("#XAcctTabs").setFocusOnTab();
				  }		
		});
	}
	$("#XAcctTabs").addClass('marginleft5');
	
	$(document).ready(function(){
		//Reset the second level tab index to 0, since it affects further navigation from dropdown menu.
		ns.admin.secondLavelPermissionTab = 0;	
	});
</script>

	