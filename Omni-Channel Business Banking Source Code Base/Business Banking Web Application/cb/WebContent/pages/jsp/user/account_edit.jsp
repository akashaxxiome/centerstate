<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<ffi:help id="userprefs_accounts"/>

<script type="text/javascript" src="<s:url value='/web/js/userPreferences/userPreferences%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/userPreferences/userPreferences_Account_grid%{#session.minVersion}.js'/>"></script>
<% session.setAttribute("displayCurrencyCode",request.getParameter("displayCurrencyCode")); %>

<%-- This page allows users to set which accounts are active, register enabled and statement enabled.--%>
<%-- It has both Internal and External accounts--%>

<%-- <div id="dashboard" align="left">
	<sj:a id="addInternalAcctBtn" button="true" title="%{getText('jsp.user.preferences.account.add.internal.account.tooltip')}"
	onclick="ns.userPreference.addInternalAccount('/cb/pages/jsp/user/add_internal_account_input.jsp','serviceCenter_services,addAccountsLink')">
		<span class="sapUiIconCls icon-add"></span><s:text name="jsp.user.preferences.account.add.internal.account"/>
	</sj:a>
	<sj:a id="addInternalAcctBtn" button="true" buttonIcon="ui-icon-info" title="%{getText('jsp.user.preferences.account.add.internal.account.tooltip')}"
	onclick="ns.userPreference.addInternalAccount('/cb/pages/jsp/user/add_internal_account_input.jsp?backLinkURL=userPreferencesVirtualMenu,accountPreferencesID','serviceCenter_services,addAccountsLink')"><s:text name="jsp.user.preferences.account.add.internal.account"/></sj:a>
	Temporarily commented this code block to hide "ADD EXTERNAL ACCOUNT" Button
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_AGGREGATION %>">
	    <sj:a id="addExternalAcctBtn" button="true" buttonIcon="ui-icon-info">ADD EXTERNAL ACCOUNT</sj:a>
	</ffi:cinclude>
	
</div>
<br>
<br> --%>
	<!-- <div id="accountSettingSection" class="portlet" style="float: left; width: 100%;"> -->
	   <%--  <div class="portlet-header" style="display:inline-block">
	    	<span class="portlet-title"><s:text name="jsp.default_Account_Preferences" /></span>
	    </div> --%>
	  <div id="accountSettingSection" class="accountPrefHeight">
	   <!--  <div class="portlet-content"> -->
	    	<s:form name="accountsPrefSetup" method="post" id="accountsPrefSetup" theme="simple" >
			    <input id="CSRF_TOKEN" type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<p class="instructions_noindent"><s:text name="jsp.user.preferences.accountGrid.UsageInfo"/></p>
				<p class="instructions_noindent"><span class="required">*</span> <s:text name="jsp.user.preferences.nickName.length.Note"/></p>
				<div style="width: 100%;">			
					<script type="text/javascript">
						<!--
						$.ajaxSetup({
						    async: false
						});
						//-->
						
					function changeDispCurrency()
					{
						var displayCurrCode = $("#displayCurrCode").val();
						var urlString = '/cb/pages/jsp/user/account_edit.jsp?displayCurrencyCode='+displayCurrCode;
						$.ajax({
							url: urlString,
							success: function(data){
							    $('#accountSettingSection').html(data);
						}
						});
					}

					$("#displayCurrCode").selectmenu({width: 200});
					</script>
					<ffi:cinclude value1="${GetTargetCurrencies}" value2="" operator="equals">
					<ffi:object name="com.ffusion.tasks.fx.GetTargetCurrencies" id="GetTargetCurrencies" scope="session"/>
					</ffi:cinclude>
					<ffi:process name="GetTargetCurrencies"/>

					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
						<div id="ChangeDisplayCurrencyFormDivID" class="acntDashboard_itemHolder">
		<s:form name="ChangeDisplayCurrencyForm"
			method="post" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		    <table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap align="left"><span class="sectionsubhead" >
					<s:text name="jsp.account_189"/>&nbsp;</span>
					<div style="margin-bottom:5px"></div>
					<select id="displayCurrCode" name="displayCurrCode" class="txtbox" onchange="changeDispCurrency();">
							<option <ffi:cinclude value1='' value2='${displayCurrencyCode}' operator='equals'> selected </ffi:cinclude> value=''><s:text name="jsp.default_127"/></option>
							<ffi:list collection="FOREIGN_EXCHANGE_TARGET_CURRENCIES" items="currency">
								<option <ffi:cinclude value1='${currency.CurrencyCode}' value2='${displayCurrencyCode}' operator='equals'> selected </ffi:cinclude> value='<ffi:getProperty name="currency" property="CurrencyCode"/>'><ffi:getProperty name="currency" property="Description" /></option>
							</ffi:list>
						</select> 
						<!-- <br/> -->
					</td>
				</tr>
			</table>
		</s:form>
		</div>
					</ffi:cinclude>
					<%-- Internal Accounts --%>
					<ffi:setProperty name="gridType" value="Internal"/>
					<%-- Include Internal Accounts Grid--%>
					<h1 class="headerTitle"><s:text name="jsp.account.internalAccount"/>
					<select id="selectBoxInternalAccounts" class="floatRight" >
						<option value="-1">-- <s:text name="jsp.user.preferences.account.add.internal.account"/> --</option>
						<option value="jsp.user.preferences.account.add.internal.account.addBillPay"><s:text name="jsp.user.preferences.account.add.internal.account.addBillPay"/></option>
						<option value="jsp.user.preferences.account.add.internal.account.addOnlineBankingAccounts"><s:text name="jsp.user.preferences.account.add.internal.account.addOnlineBankingAccounts"/></option>
					</select>
					</h1>
					<script>
						$("#selectBoxInternalAccounts").selectmenu({width: 250});

						$("#selectBoxInternalAccounts").change(function(e){
							var value = $(this).val();
							ns.userPreference.addInternalAccount(value);
						});
					</script>
					
					<div class="marginTop20"></div>
					<s:include value="/pages/jsp/user/userprefs_account_grid.jsp"/>
					<div class="marginTop20"></div>
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_AGGREGATION%>">
						<ffi:setProperty name="gridType" value="External"/>
						<%-- Include Internal Accounts Grid--%>
						<h1 class="headerTitle"><s:text name="jsp.account.externalAccount"/></h1>
						<s:include value="/pages/jsp/user/userprefs_account_grid.jsp"/>
					</ffi:cinclude>
					<script type="text/javascript">
						<!--
						$.ajaxSetup({
						    async: true
						});
						//-->
					</script>
					</div>
					<div  class="ui-widget-header customDialogFooter">
							<%-- HTML Buttons --%>
							<%-- <sj:a id="resetAccountPrefBtn"
								  onclick="ns.userPreference.resetPreferences('ACCOUNT_TAB');"
								  button="true"><s:text name="jsp.default_358"/></sj:a> --%>
							<sj:a id="cancelAccountPrefBtn"  button="true"
								  onclick="$.publish('closeDialog');"><s:text name="jsp.default_82"/></sj:a>
							<sj:a id="saveAccountPrefBtn" button="true"
								onclick="ns.userPreference.saveAccountPreference();"><s:text name="jsp.default_366"/></sj:a>
					</div>
			</s:form>
<!-- 	</div> -->
	
 </div>
<!-- <div id="accountSettingSection" style="overflow:hidden;">
	
</div> -->


<script type="text/javascript">
	//Show Account listing section.
	/* $('#accountSettingSection').portlet({
		generateDOM: true,
		title : js_account_portlet_title
	}); */
	
	//ns.common.initializePortlet("accountSettingSection", false);
</script>


<sj:dialog id="resultDialog" autoOpen="false" modal="true" title="%{getText('jsp.user.preferences.save.dialog.title')}" height="auto" width="250" overlayColor="#000" overlayOpacity="0.7" 
	buttons="{'%{getText(\"jsp.default_175\")}': function(){ $('#resultDialog').dialog('close');$.publish('quickSaveAccountPreferencesSetting')}}" cssClass="home_prefsDialog staticContentDialog"
	resizable="false">
		<div id="userpreferenceConfirmTextID" align="center">
			<table style="width:100%; height:100%;" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata" style="text-align: center;"><s:text name="jsp.user_390"/></td>
				</tr>	
			</table>
		</div>
</sj:dialog>