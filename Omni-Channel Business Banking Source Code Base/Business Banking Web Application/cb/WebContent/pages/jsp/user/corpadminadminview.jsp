<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.beans.business.Business" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroup" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroups" %>
<%@ page import="com.sap.banking.web.util.entitlements.EntitlementsUtil" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<div id='PageHeading' style="display: none;"><s:text name="jsp.user_View_Adminer"/></div>

<%	
	String divAdd = request.getParameter("divAdd");
	if(divAdd!= null){
		session.setAttribute("divAdd", divAdd);
	}
		
	String temp = request.getParameter("AdminEditType");
	if (temp != null) {
		session.setAttribute("AdminEditType", temp);
	}

	//Keep original business employee value
	session.setAttribute("OrigBusinessEmployee",(BusinessEmployee) session.getAttribute("BusinessEmployee"));

	SecureUser su = ( SecureUser )session.getAttribute( "SecureUser" );
	BusinessEmployee be = new BusinessEmployee();
	be.set( ProfileDefines.BANK_ID, su.getBankID() );
	be.setBusinessId( su.getBusinessID() );
	session.setAttribute( "BusinessEmployee", be );
	
	//the variable groupType will be passed to javascript function validateSave
	//so the correct alert message can be displayed
	String groupType = "Business";
%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:cinclude value1="${onCancelGoto}" value2="da-new-division-edit.jsp" operator="equals">
		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
			<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
			<ffi:setProperty name="GetListsForNewGroup" value="TRUE"/>
		</ffi:cinclude>
		<% groupType = "Division"; %>
	</ffi:cinclude>
	<ffi:cinclude value1="${onCancelGoto}" value2="da-new-group-edit.jsp" operator="equals">
		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
			<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
			<ffi:setProperty name="GetListsForNewGroup" value="TRUE"/>
		</ffi:cinclude>
		<% groupType = "Group"; %>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${onCancelGoto}" value2="corpadmindivview.jsp" operator="equals">
	<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
		<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
	</ffi:cinclude>
	<% groupType = "Division"; %>
</ffi:cinclude>
<ffi:cinclude value1="${onCancelGoto}" value2="corpadmingroupview.jsp" operator="equals">
	<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
		<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
	</ffi:cinclude>
	<% groupType = "Group"; %>
</ffi:cinclude>
<ffi:cinclude value1="${BackReload}" value2="TRUE" operator="equals">
	<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
	<ffi:setProperty name="GetListsForNewGroup" value="FALSE"/>
</ffi:cinclude>



<%-- Retrieve the list of admins on this group --%>
<%-- commented the old code --%>

<ffi:cinclude value1="${GetListsForNewGroup}" value2="TRUE" operator="notEquals"> 
   <ffi:cinclude value1="${EditGroupAmins}" value2="TRUE" operator="notEquals">
	<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
	<ffi:cinclude value1="${onCancelGoto}" value2="corpadmininfo.jsp" operator="equals">
		<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${onCancelGoto}" value2="corpadmininfo.jsp" operator="notEquals">
		<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	</ffi:cinclude>
	<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="Admins"/>
	<ffi:process name="GetAdminsForGroup"/>
   </ffi:cinclude>
</ffi:cinclude>



<%-- Retrieve the list of admins on this group --%>
<%-- copied from corpadminadminedit.jsp --%>
<%--
<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="equals">
	<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}" />
</ffi:cinclude>
<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="notEquals">
	<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
</ffi:cinclude>
<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="Admins" />
<ffi:process name="GetAdminsForGroup" />
--%>

<%-- based on the business employee object, get the business employees for the business --%>
<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="BusinessEmployee"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>

<%-- Generate the non-admin and admin lists --%>
<%-- commented the old code --%>
<ffi:cinclude value1="${GetListsForNewGroup}" value2="TRUE" operator="notEquals"> 
   <ffi:cinclude value1="${EditGroupAmins}" value2="TRUE" operator="notEquals">
	<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="Admins"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
	<ffi:process name="GenerateUserAndAdminLists"/>
	<ffi:removeProperty name="adminIds"/>
	<ffi:removeProperty name="userIds"/>
	<ffi:removeProperty name="GenerateUserAndAdminLists"/>
   </ffi:cinclude>
</ffi:cinclude>


<%-- Generate the non-admin and admin lists --%>
<%-- copied from corpadminadminedit.jsp --%>
<%--
<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists" />
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="Admins" />
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees" />
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees" />
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups" />
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees" />
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups" />
	<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds" />
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds" />
<ffi:process name="GenerateUserAndAdminLists" />
<ffi:removeProperty name="adminIds" />
<ffi:removeProperty name="userIds" />
<ffi:removeProperty name="GenerateUserAndAdminLists" />
--%>

<ffi:cinclude value1="${GetListsForNewGroup}" value2="TRUE" operator="equals">

	<%-- we have to supply the AdminEmployees, NonAdminEmployees and the NonAdminGroups ArrayLists --%>
<%
	SecureUser thisUser = (SecureUser)session.getAttribute( "SecureUser" );
	BusinessEmployees allUsers = (BusinessEmployees)session.getAttribute( com.ffusion.tasks.Task.BUSINESS_EMPLOYEES );
	BusinessEmployee currentUser = (BusinessEmployee)allUsers.getByID( Integer.toString( thisUser.getProfileID() ) );
	allUsers.removeByID( Integer.toString( thisUser.getProfileID() ) );
	session.setAttribute( "NonAdminEmployees", allUsers );

	java.util.Locale tempLocale = thisUser.getLocale();
	BusinessEmployees defalutAdmin = new BusinessEmployees( tempLocale );
	defalutAdmin.add( currentUser );
	session.setAttribute( "AdminEmployees", defalutAdmin );
	
	EntitlementGroup businessEntGroup = (EntitlementGroup)( (Business)session.getAttribute( EntitlementsDefines.ENT_GROUP_BUSINESS ) ).getEntitlementGroup();
	java.util.ArrayList groupList = new java.util.ArrayList();
	// each entry will contain a name, a type and a groupId
	java.util.ArrayList bizEntry = new java.util.ArrayList();
	bizEntry.add( businessEntGroup.getGroupName() );
	bizEntry.add( businessEntGroup.getEntGroupType() );
	bizEntry.add( Integer.toString( businessEntGroup.getGroupId() ) );
	groupList.add( bizEntry );
     EntitlementGroups divs = EntitlementsUtil.getChildrenEntitlementGroups( businessEntGroup.getGroupId() ); 
     for( int i = 0; i < divs.size(); i++ ) {
          EntitlementGroup division = divs.getGroup( i );
	  // each entry will contain a name, a type and a groupId
	  java.util.ArrayList divEntry = new java.util.ArrayList();
	  divEntry.add( division.getGroupName() );
	  divEntry.add( division.getEntGroupType() );
	  divEntry.add( Integer.toString( division.getGroupId() ) );
          groupList.add( divEntry );

          EntitlementGroups groups = EntitlementsUtil.getChildrenEntitlementGroups( division.getGroupId() ); 
          for( int j = 0; j < groups.size(); j++ ) {
              EntitlementGroup group = groups.getGroup( j );
              // each entry will contain a name, a type and a groupId
	      java.util.ArrayList groupEntry = new java.util.ArrayList();
              groupEntry.add( group.getGroupName() );
	      groupEntry.add( group.getEntGroupType() );
              groupEntry.add( Integer.toString( group.getGroupId() ) );

	      groupList.add( groupEntry );
          }                
      }
	session.setAttribute( "NonAdminGroups", groupList );

%>
</ffi:cinclude>

<!-- Dual Approval starts-->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:cinclude value1="${divAdd}" value2="true" operator="notEquals">
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
				<ffi:cinclude value1="${onCancelGoto}" value2="corpadmininfo.jsp" operator="equals">
					<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${onCancelGoto}" value2="corpadmininfo.jsp" operator="notEquals">
					<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
				</ffi:cinclude>
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%= groupType %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
				<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
			<ffi:process name="GetDACategoryDetails"/>
			
			<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
				<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
				<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
				<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
				<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
				<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
				<ffi:process name="GenerateListsFromDAAdministrators"/>
			</ffi:cinclude>
	</ffi:cinclude>
	<ffi:removeProperty name="divAdd"/>
	
	<ffi:removeProperty name="GetDACategoryDetails"/>
	<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
</ffi:cinclude>
<!-- Dual Approval ends -->
<div class="ffivisible"  style="max-height:650px; overflow-y:auto; width:100%;">
	<div align="center" id="selectPoolView" class="customAccordianBgClass adminAccordianCls">

		<!-- 3 column table for icons -->
		<table width="700" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" class="adminBackground">
					<!-- table containing data -->
					<table align="center" border="0" class="tableData">	
						<tr>
							<td align="right">
								<table border="0" cellspacing="0" cellpadding="0" class="tblborder">
									<tr>
										<td align="left" class="pane-header ui-state-default ui-corner-top ui-state-active"><s:text name="jsp.user_363"/></td>
									</tr>
									<tr>
										<td align="left">	
											<select name="userListView" id="userListView"  class="ui-widget-content" multiple size="14" style="width:300px">

											<ffi:list collection="NonAdminEmployees" items="businessEmployee">
												<ffi:setProperty name="_userID" value="${businessEmployee.Id}"/>
												<ffi:setProperty name="_entGroupID" value="${businessEmployee.EntitlementGroupId}"/>

												<%-- add the number and one space --%>
												<%-- we can assume that business employees are SecureUser.TYPE_CUSTOMER --%>
												<option value="<%= EntitlementsDefines.ENT_MEMBER_TYPE_USER %>-<%= SecureUser.TYPE_CUSTOMER %>-<ffi:getProperty name="_userID"/>-<ffi:getProperty name="_entGroupID"/>"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1" parm0="${businessEmployee.FullName}"/></option>
											</ffi:list>
											</select>
										</td>
									</tr>
									</table>
									
									<table border="0" cellspacing="0" cellpadding="0"  class="tblborder marginTop10">
									<tr>
										<td align="left" class="pane-header ui-state-default ui-corner-top ui-state-active"><s:text name="jsp.user_164"/></td>
									</tr>
									<tr>
									 	<td align="left">
											<select name="groupListView" id="groupListView"  class="ui-widget-content" multiple size="14"  style="width:300px">
										 	<ffi:list collection="NonAdminGroups" items="group">
												<ffi:list collection="group" items="groupName, groupType, groupId">
						
												<%-- add the Groups to the list --%>
												<option value="---<ffi:getProperty name="groupId"/>"><ffi:getProperty name="groupName"/> (
												<ffi:setProperty name="ENTResource" property="ResourceID" value="Ent_Group_Type.${groupType}" />
		                                        <ffi:getProperty name="ENTResource" property="Resource"/>
												)</option>
												</ffi:list>
											</ffi:list>							
											</select>
										</td>
									</tr>
								</table>
							</td>
							<td align="center">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td align="left" style="vertical-align:top">
								<table border="0" class="tableData">
									<tr>
										<ffi:cinclude value1="${CATEGORY_BEAN}" value2="">
											<td colspan="2" class="pane-header ui-state-default ui-corner-top ui-state-active" align="left"><s:text name="jsp.user_292"/></td>
										</ffi:cinclude>
										<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals" >
											<td colspan="2" class="sectionheadDA" align="left"><s:text name="jsp.user_292"/></td>
										</ffi:cinclude>
									</tr>
									<tr>
										<td valign="middle" align="left">
											<select name="adminListView" id="adminListView"  class="ui-widget-content" multiple size="32" style="width:300px" >
											<%
												// number the items in the list
												int approvalIndex = 1;
											%>
											<ffi:list collection="AdminEmployees" items="businessEmployee">
												<ffi:setProperty name="_userID" value="${businessEmployee.Id}"/>
												<ffi:setProperty name="_entGroupID" value="${businessEmployee.EntitlementGroupId}"/>
												<option value="<%= EntitlementsDefines.ENT_MEMBER_TYPE_USER %>-<%= SecureUser.TYPE_CUSTOMER %>-<ffi:getProperty name="_userID"/>-<ffi:getProperty name="_entGroupID"/>"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1" parm0="${businessEmployee.FullName}"/></option>
											</ffi:list>
											<ffi:cinclude value1="${GetListsForNewGroup}" value2="TRUE" operator="notEquals">
											   <ffi:list collection="AdminGroups" items="group">
												<ffi:list collection="group" items="groupName, groupType, groupId">
						
												<%-- add the Groups to the list --%>
												<option value="---<ffi:getProperty name="groupId"/>"><ffi:getProperty name="groupName"/> (
												<ffi:setProperty name="ENTResource" property="ResourceID" value="Ent_Group_Type.${groupType}" />
		                                        <ffi:getProperty name="ENTResource" property="Resource"/>
												)</option>
												</ffi:list>
											   </ffi:list>	
											</ffi:cinclude>
											</select>
										</td>
										<%-- Show old administrator values as highlighted. --%>
										<%-- <ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals" >
											<td class="sectionhead_greyDA" valign="top">
												<table class="tableData">
												<ffi:list collection="OldAdminEmployees" items="businessEmployee">
													<tr><td class="sectionhead_greyDA" valign="top">
														<ffi:setProperty name="_userID" value="${businessEmployee.Id}"/>
														<ffi:setProperty name="_entGroupID" value="${businessEmployee.EntitlementGroupId}"/>
														<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1" parm0="${businessEmployee.FullName}"/>
													</td></tr>
												</ffi:list>
												<ffi:list collection="OldAdminGroups" items="group">
													<ffi:list collection="group" items="groupName, groupType, groupId">
														<tr><td class="sectionhead_greyDA" valign="top">
														add the Groups to the list
														<ffi:getProperty name="groupName"/> (
														<ffi:setProperty name="ENTResource" property="ResourceID" value="Ent_Group_Type.${groupType}" />
				                                        <ffi:getProperty name="ENTResource" property="Resource"/>
														)
														</td></tr>
													</ffi:list>
												</ffi:list>	
												</table>
											</td>
										</ffi:cinclude> --%>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<%-- Show old administrator values as highlighted. --%>
							<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals" >
								<td class="sectionhead_greyDA" valign="top" colspan="3">
									<span class="labelCls"><s:text name="jsp.user_38" /> ( <s:text name="jsp.da_approvals_pending_user_OldValue_column" /> ) :</span>
									<!-- <table class="tableData"  class="marginTop20"> -->
									<ffi:list collection="OldAdminEmployees" items="businessEmployee">
										<!-- <tr><td class="sectionhead_greyDA" valign="top"> -->
										<span class="valueCls">
											<ffi:setProperty name="_userID" value="${businessEmployee.Id}"/>
											<ffi:setProperty name="_entGroupID" value="${businessEmployee.EntitlementGroupId}"/>
											<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1" parm0="${businessEmployee.FullName}"/>, 
										<!-- </td></tr> -->
										</span>
									</ffi:list>
									<ffi:list collection="OldAdminGroups" items="group">
										<ffi:list collection="group" items="groupName, groupType, groupId">
											<!-- <tr><td class="sectionhead_greyDA" valign="top"> -->
											<%-- add the Groups to the list --%>
											<span class="valueCls">
											<ffi:getProperty name="groupName"/> (
											<ffi:setProperty name="ENTResource" property="ResourceID" value="Ent_Group_Type.${groupType}" />, 
	                                        <ffi:getProperty name="ENTResource" property="Resource"/>
											)
											<!-- </td></tr> -->
											</span>
										</ffi:list>
									</ffi:list>	
									<!-- </table> -->
								</td>
							</ffi:cinclude>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="100" colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td align="center" class="ui-widget-header customDialogFooter">
					<sj:a button="true" title="Close" onclick="ns.common.closeDialog('viewAdminInViewDiv');ns.common.closeDialog('editAdminerDialogId');"><s:text name="jsp.default_175" /></sj:a>
				</td>
			</tr>
			<tr>
				<td height="50" colspan="3">&nbsp;</td>
			</tr>
		</table>
	</div>
</div>
<ffi:removeProperty name="BusinessEmployee"/>
<ffi:removeProperty name="BusinessEmployees"/>
<ffi:removeProperty name="Admins"/>
<ffi:removeProperty name="UserEmployees"/>
<ffi:removeProperty name="GetListsForNewGroup"/>
<ffi:removeProperty name="EditGroupAmins"/>
<ffi:removeProperty name="Back"/>
<ffi:removeProperty name="OldAdminGroups"/>
<ffi:removeProperty name="OldAdminEmployees"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminadminview.jsp"/>