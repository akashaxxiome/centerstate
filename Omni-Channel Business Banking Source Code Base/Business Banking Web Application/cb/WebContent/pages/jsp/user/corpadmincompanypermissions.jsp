<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setProperty name="EditGroup_GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="Section" value="Company"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.reports_526')}" scope="request" />
<ffi:setProperty name="Context" value="${Business.BusinessName} (${tmpI18nStr})"/>
<ffi:removeProperty name="BusinessEmployeeId"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<span id="pageHeading" style="display:none;"><s:text name="jsp.default_179"/> <ffi:getProperty name="Context"/> <s:text name="jsp.default_569"/></span>
<div align="left" id="corpadmincompanypermissionsDiv">	
<ffi:help id="user_corpadmincompanypermissions" className="moduleHelpClass"/>
<s:include value="inc/permissions-init.jsp"/>
	<table border="0" cellspacing="0" cellpadding="3">
		<tr>
			<%-- <td class="sectionhead" valign="baseline" align="right" height="17" >
				&gt; <s:text name="jsp.default_179"/> <span class="sectionhead"><ffi:getProperty name="Context"/></span> <s:text name="jsp.default_569"/>
			</td> --%>
			<td colspan="3" class="columndata" align="left" valign="baseline">
				<s:url id="companyPermissionsURL" value="%{#session.PagesPath}user/admin_permissions.jsp" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
				</s:url>
				<sj:a
					id="companyPermissionsLink"
					href="%{companyPermissionsURL}"
					targets="permissionsDiv"
					button="true"
					buttonIcon="ui-icon-pencil"
					onClickTopics="beforePermLoad"
					onCompleteTopics="completeCompanyPermLoad"
					onErrorTopics="errorLoad"
					cssStyle="display:none;"
					><s:text name="jsp.default_178"/></sj:a>
			</td>
		</tr>
	</table>
</div>
<s:include value="inc/permissionsmenu.jsp"/>
