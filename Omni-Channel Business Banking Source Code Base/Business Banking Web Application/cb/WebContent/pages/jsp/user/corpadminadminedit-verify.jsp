<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<%@ page import="com.ffusion.beans.business.Business" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroup" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroups" %>
<%@ page import="com.sap.banking.web.util.entitlements.EntitlementsUtil" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%--save the information from the corpadminadminedit.jsp page--%>

<%@ page import="com.ffusion.csil.beans.entitlements.*" %>

<ffi:help id="user_corpadminadminedit-verify" className="moduleHelpClass"/>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="CanAdministerAnyGroup" />

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>

<%
// Retrieve the selected admins and build an EntitlementGroupMember for each.
// the value of the adminListItems is: MemberType-MemberSubType-MemberID-EntGroupID

java.util.ArrayList adminIds = new java.util.ArrayList();
java.util.ArrayList userIds = new java.util.ArrayList();
java.util.ArrayList groupIds = new java.util.ArrayList();
java.util.ArrayList bizEmpIds = new java.util.ArrayList();
java.util.ArrayList adminMembers = new java.util.ArrayList();
String[] userListData = request.getParameterValues( "userList" );
String[] groupListData = request.getParameterValues( "groupList" );
String[] adminListData = request.getParameterValues( "adminList" );
EntitlementGroupMembers userMembers = new EntitlementGroupMembers();

try {
%>
<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="notEquals">
<%
SecureUser thisUser = (SecureUser)session.getAttribute( "SecureUser" );
java.util.Locale tempLocale = thisUser.getLocale();
if( adminListData != null ) {
	BusinessEmployees userAdmins = new BusinessEmployees( tempLocale );
	java.util.ArrayList groupAdmins = new java.util.ArrayList();
	// create the EntitlementGroupMembers from the posted data
	for( int i = 0 ; i < adminListData.length ; i++ ) {
		// create a decision object
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= adminListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		String tempId = null;
		int firstIndex = itemData.indexOf( '-' );
		int secondIndex = itemData.indexOf( '-', firstIndex + 1 );
		int lastIndex = itemData.lastIndexOf( '-' );

		if( firstIndex == 0 ){
			// the element is a group or a division
			adminMembers.add( itemData.substring( lastIndex + 1 ) );
			tempId = itemData.substring( lastIndex + 1 );

			session.setAttribute( "tempEntId", tempId );
%>
			<%-- get the BusinessEmployees for a specific EntitlementGroup --%>
			<ffi:object id="GetEmployeesInEntGroup" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroupId"/>
			<ffi:setProperty name="GetEmployeesInEntGroup" property="BusinessEmployeesSessionName" value="tempBizEmployees"/>
			<ffi:setProperty name="GetEmployeesInEntGroup" property="EntitlementGroupId" value="<%= tempId %>"/>
			<ffi:process name="GetEmployeesInEntGroup"/>
<%
			BusinessEmployees tempEmployees = (BusinessEmployees)session.getAttribute( "tempBizEmployees" );
			//check that the employees in the EntitlementGroup is not already in bizEmpIds
			for( int j=0; j<tempEmployees.size(); j++ ) {
				BusinessEmployee emp = (BusinessEmployee)tempEmployees.get(j);
				boolean foundId = false;
				for( int w=0; w<bizEmpIds.size(); w++ ) {
					if( ( (String)bizEmpIds.get(w) ).equals( emp.getId() ) ) {
						foundId = true;
						break;
					}
				}
				if( !foundId ) {
					bizEmpIds.add( emp.getId() );
				}
			}
			//add information about this group to groupAdmins, in case of later retreival
			EntitlementGroup group = EntitlementsUtil.getEntitlementGroup( Integer.parseInt( tempId ) );
			java.util.ArrayList Entry = new java.util.ArrayList();
			Entry.add( group.getGroupName() );
		 	Entry.add( group.getEntGroupType() );
		 	Entry.add( Integer.toString( group.getGroupId() ) );
	         	groupAdmins.add( Entry );

			adminIds.add( itemData.substring( lastIndex + 1 ) );
		} else {
			// the element is a user
			tempId =  itemData.substring( secondIndex + 1, lastIndex );
			EntitlementGroupMember member = new EntitlementGroupMember();
			member.setMemberType( itemData.substring( 0, firstIndex ) );
			member.setMemberSubType( itemData.substring( firstIndex + 1, secondIndex ) );
			member.setId( tempId );
			member.setEntitlementGroupId( Integer.parseInt( itemData.substring( lastIndex + 1 ) ) );
			adminMembers.add( member );
			adminIds.add( tempId );
			
			//check that this employee is not already in the bizEmpIds ArrayList
			boolean foundId = false;
			for( int j=0; j<bizEmpIds.size(); j++ ) {
			if( ( (String)bizEmpIds.get(j) ).equals( tempId ) ) {
					foundId = true;
					break;
				}
			}
			if( !foundId ) {
				bizEmpIds.add( tempId );
			}
%>
			<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
			<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="<%= tempId %>"/>
			<ffi:process name="GetBusinessEmployee"/>
			<ffi:removeProperty name="GetBusinessEmployee" />
<%
			BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
			userAdmins.add( emp );
		}
	}
	session.setAttribute( "AdminEmployees", userAdmins );
	session.setAttribute( "AdminGroups", groupAdmins );
}
session.setAttribute( "BizEmployeeIds", bizEmpIds );
BusinessEmployees tempAdminEmps = new BusinessEmployees( tempLocale );
%>
<ffi:removeProperty name="tempEntId"/>
<ffi:removeProperty name="tempBizEmployees"/>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
<ffi:list collection="BizEmployeeIds" items="Id">
	<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${Id}"/>
	<ffi:process name="GetBusinessEmployee"/>
<%
	BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
	tempAdminEmps.add( emp );
%>
</ffi:list>
<%-- set BusinessEmployee object back to secured user --%>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>
<ffi:removeProperty name="GetBusinessEmployee" />
<%
session.setAttribute( "AdminsSelected", "TRUE");
session.setAttribute( "tempAdminEmps", tempAdminEmps);

if( userListData != null ) {
	BusinessEmployees userNonAdmins = new BusinessEmployees( tempLocale );
	// create the EntitlementGroupMembers from the posted data
	for( int i = 0 ; i < userListData.length ; i++ ) {
		// create a decision object
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= userListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		int firstIndex = itemData.indexOf( '-' );
		int secondIndex = itemData.indexOf( '-', firstIndex + 1 );
		int lastIndex = itemData.lastIndexOf( '-' );
		String tempId = itemData.substring( secondIndex + 1, lastIndex );
%>
		<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="<%= tempId %>"/>
		<ffi:process name="GetBusinessEmployee"/>
		<ffi:removeProperty name="GetBusinessEmployee" />
<%
		BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
		userNonAdmins.add( emp );
		
		EntitlementGroupMember member = new EntitlementGroupMember();
		member.setMemberType( itemData.substring( 0, firstIndex ) );
		member.setMemberSubType( itemData.substring( firstIndex + 1, secondIndex ) );
		member.setId( tempId );
		member.setEntitlementGroupId( Integer.parseInt( itemData.substring( lastIndex + 1 ) ) );
		userMembers.add( member );
		userIds.add( tempId );
	}
	session.setAttribute( "NonAdminEmployees", userNonAdmins );
}

if( groupListData != null ) {
	java.util.ArrayList groupNonAdmins = new java.util.ArrayList();
	// create the groupId array from the posted data
	for( int i = 0 ; i < groupListData.length ; i++ ) {
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= groupListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		int lastIndex = itemData.lastIndexOf( '-' );
		String tempId = itemData.substring( lastIndex + 1 );
		//add information about this group to groupAdmins, in case of later retreival
		EntitlementGroup group = EntitlementsUtil.getEntitlementGroup( Integer.parseInt( tempId ) );
		java.util.ArrayList Entry = new java.util.ArrayList();
		Entry.add( group.getGroupName() );
	 	Entry.add( group.getEntGroupType() );
	 	Entry.add( Integer.toString( group.getGroupId() ) );
         	groupNonAdmins.add( Entry );
		groupIds.add( tempId );
	}
	session.setAttribute( "NonAdminGroups", groupNonAdmins );
}
session.setAttribute( "GroupDivAdminEdited", "true" );
%>
</ffi:cinclude>
<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="equals">
<%
SecureUser thisUser = (SecureUser)session.getAttribute( "SecureUser" );
java.util.Locale tempLocale = thisUser.getLocale();
if( adminListData != null ) {
	BusinessEmployees userAdmins = new BusinessEmployees( tempLocale );
	java.util.ArrayList groupAdmins = new java.util.ArrayList();
	// create the EntitlementGroupMembers from the posted data
	for( int i = 0 ; i < adminListData.length ; i++ ) {
		// create a decision object
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= adminListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		String tempId = null;
		int firstIndex = itemData.indexOf( '-' );
		int secondIndex = itemData.indexOf( '-', firstIndex + 1 );
		int lastIndex = itemData.lastIndexOf( '-' );

		if( firstIndex == 0 ) {
			// the element is a group or a division
			adminMembers.add( itemData.substring( lastIndex + 1 ) );
			tempId = itemData.substring( lastIndex + 1 );


			//add information about this group to groupAdmins, in case of later retreival
			EntitlementGroup group = EntitlementsUtil.getEntitlementGroup( Integer.parseInt( tempId ) );
			java.util.ArrayList Entry = new java.util.ArrayList();
			Entry.add( group.getGroupName() );
		 	Entry.add( group.getEntGroupType() );
		 	Entry.add( Integer.toString( group.getGroupId() ) );
	         	groupAdmins.add( Entry );

			adminIds.add( itemData.substring( lastIndex + 1 ) );
		} else {
			// the element is a user
			tempId =  itemData.substring( secondIndex + 1, lastIndex );
			EntitlementGroupMember member = new EntitlementGroupMember();
			member.setMemberType( itemData.substring( 0, firstIndex ) );
			member.setMemberSubType( itemData.substring( firstIndex + 1, secondIndex ) );
			member.setId( itemData.substring( secondIndex + 1, lastIndex ) );
			adminIds.add( member.getId() );
			member.setEntitlementGroupId( Integer.parseInt( itemData.substring( lastIndex + 1 ) ) );
			adminMembers.add( member );
%>
			<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
			<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="<%= tempId %>"/>
			<ffi:process name="GetBusinessEmployee"/>
			<ffi:removeProperty name="GetBusinessEmployee" />
<%
			BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
			userAdmins.add( emp );
			
		}
	}

	session.setAttribute( "AdminEmployees", userAdmins );
	session.setAttribute( "AdminGroups", groupAdmins );
}

if( userListData != null ) {
	BusinessEmployees userNonAdmins = new BusinessEmployees( tempLocale );
	// create the EntitlementGroupMembers from the posted data
	for( int i = 0 ; i < userListData.length ; i++ ) {
		// create a decision object
		EntitlementGroupMember member = new EntitlementGroupMember();
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= userListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%
		int firstIndex = itemData.indexOf( '-' );
		int secondIndex = itemData.indexOf( '-', firstIndex + 1 );
		int lastIndex = itemData.lastIndexOf( '-' );
		String tempId = itemData.substring( secondIndex + 1, lastIndex );
%>
		<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="<%= tempId %>"/>
		<ffi:process name="GetBusinessEmployee"/>
		<ffi:removeProperty name="GetBusinessEmployee" />
<%
		BusinessEmployee emp = (BusinessEmployee)session.getAttribute( "BusinessEmployee" );
		userNonAdmins.add( emp );
		
		member.setMemberType( itemData.substring( 0, firstIndex ) );
		member.setMemberSubType( itemData.substring( firstIndex + 1, secondIndex ) );
		member.setId( itemData.substring( secondIndex + 1, lastIndex ) );
		userIds.add( member.getId() );
		member.setEntitlementGroupId( Integer.parseInt( itemData.substring( lastIndex + 1 ) ) );
		userMembers.add( member );
	}
	session.setAttribute( "NonAdminEmployees", userNonAdmins );

}

if( groupListData != null ) {
	java.util.ArrayList groupNonAdmins = new java.util.ArrayList();
	// create the groupId array from the posted data
	for( int i = 0 ; i < groupListData.length ; i++ ) {
		String itemData = null;
%>
    <ffi:setProperty name="tempVar" value="<%= groupListData[ i ] %>"/>
	  <ffi:getProperty name="tempVar" assignTo="itemData"/>
<%		
		int lastIndex = itemData.lastIndexOf( '-' );
		String tempId = itemData.substring( lastIndex + 1 );
		//add information about this group to groupAdmins, in case of later retreival
		EntitlementGroup group = EntitlementsUtil.getEntitlementGroup( Integer.parseInt( tempId ) );
		java.util.ArrayList Entry = new java.util.ArrayList();
		Entry.add( group.getGroupName() );
	 	Entry.add( group.getEntGroupType() );
	 	Entry.add( Integer.toString( group.getGroupId() ) );
         	groupNonAdmins.add( Entry );
		groupIds.add( itemData.substring( lastIndex + 1 ) );
	}
	session.setAttribute( "NonAdminGroups", groupNonAdmins );
}
%>
</ffi:cinclude>
<%
// put the data into the session
session.setAttribute( "adminMembers", adminMembers );
session.setAttribute( "userMembers", userMembers );
session.setAttribute( "adminIds", adminIds );
session.setAttribute( "userIds", userIds );
session.setAttribute( "groupIds", groupIds );
}
catch ( Exception e ) {
	e.printStackTrace();
}

%>

<%-- call the SetAdministrators task only if we are editing business or business groups --%>
<%-- remove adminMemebers only if we do not need them for adding new business groups --%>
<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="equals">
	<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembers"/>
	<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembers"/>
	<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
	<ffi:setProperty name="SetAdministrators" property="Commit" value="false"/>
	<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="SetAdministrators" property="HistoryId" value="${Business.Id}"/>
	<ffi:cinclude value1="${SetAdministrators.AutoEntitle}" value2="true" operator="equals">
		<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg15')}" scope="request" />
		<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
		<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg16')}" scope="request" />
		<ffi:setProperty name="autoEntitleConfirmMsg2" value="${tmpI18nStr}"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="notEquals">
	<%-- validate the admin settings, make sure we have at least 1 user in it --%>
	<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembers"/>
	<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembers"/>
	<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
	<ffi:setProperty name="SetAdministrators" property="Commit" value="false"/>
	<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:setProperty name="SetAdministrators" property="EntitlementGroupType" value="${Entitlement_EntitlementGroup.EntGroupType}"/>
	<ffi:setProperty name="SetAdministrators" property="HistoryId" value="${Business.Id}"/>
	<ffi:cinclude value1="${SetAdministrators.AutoEntitle}" value2="true" operator="equals">
		<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg15')}" scope="request" />
		<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
		<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg16')}" scope="request" />
		<ffi:setProperty name="autoEntitleConfirmMsg2" value="${tmpI18nStr}"/>
	</ffi:cinclude>

</ffi:cinclude>
<ffi:setProperty name="SetAdministrators" property="Commit" value="true"/>

<div align="center">

	<s:form id="editAdministratorVerifyForm" namespace="/pages/user" name="editAdministratorVerifyForm" action="setAdministrators-execute" method="post" validate="false" theme="simple">
    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<!-- 3 column table for icons -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left">
					<table border="0" >
						<ffi:cinclude value1="${SetAdministrators.AutoEntitle}" value2="true" operator="equals">
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<TR>
		                        <TD><img src="/cb/web/multilang/grafx/spacer.gif" width="180" height="1"></TD>
		                        <TD><img src="/cb/web/multilang/grafx/spacer.gif" width="170" height="1"></TD>
	                        </TR>
							<TR>
								<TD colspan="2" style="text-align:center" nowrap>
									<span class="sectionsubhead">
										<ffi:getProperty name="autoEntitleConfirmMsg2"/>
									</span>
								</TD>		
							</TR>
							<TR>
								<TD style="text-align:right"><input type="radio" name="AutoEntitleAdministrators" value="true" checked="checked"/></TD>
								<TD class="columndata"><s:text name="jsp.default_467"/></TD>
							</TR>
							<TR>
								<TD style="text-align:right"><input type="radio" name="AutoEntitleAdministrators" value="false"/></TD>
								<TD class="columndata"><s:text name="jsp.default_295"/></TD>
							</TR>
							</ffi:cinclude>
						</ffi:cinclude>
						<tr>
							<td colspan="2" style="text-align:center" align="center" nowrap><span class="sectionsubhead"><s:text name="jsp.user_377"/></span></td>
						</tr>
						<TR>
                               <TD><img src="/cb/web/multilang/grafx/spacer.gif" width="40%" height="1"></TD>
                               <TD><img src="/cb/web/multilang/grafx/spacer.gif" width="30%" height="1"></TD>
                           </TR>
						<tr>
							
							<td valign="top" width="60%" class="sectionsubhead" align="right"><s:text name="jsp.user_292"/></td>
						

							<td align="left" >
								
								<SELECT name="adminList" class="ui-widget-content" disabled="disabled" multiple size="10" style="width:200px" >
									<%
										// number the items in the list
										int approvalIndex = 1;
									%>
									<ffi:list collection="AdminEmployees" items="businessEmployee">
										<ffi:setProperty name="_userID" value="${businessEmployee.Id}"/>
										<ffi:setProperty name="_entGroupID" value="${businessEmployee.EntitlementGroupId}"/>
										<option value="<%= EntitlementsDefines.ENT_MEMBER_TYPE_USER %>-<%= SecureUser.TYPE_CUSTOMER %>-<ffi:getProperty name="_userID"/>-<ffi:getProperty name="_entGroupID"/>"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1" parm0="${businessEmployee.FullName}"/></option>
									</ffi:list>
									<ffi:cinclude value1="${GetListsForNewGroup}" value2="TRUE" operator="notEquals">
									   <ffi:list collection="AdminGroups" items="group">
										<ffi:list collection="group" items="groupName, groupType, groupId">
				
										<%-- add the Groups to the list --%>
										<option value="---<ffi:getProperty name="groupId"/>"><ffi:getProperty name="groupName"/> (
										<ffi:setProperty name="ENTResource" property="ResourceID" value="Ent_Group_Type.${groupType}" />
		                                      <ffi:getProperty name="ENTResource" property="Resource"/>
										)</option>
										</ffi:list>
									   </ffi:list>	
									</ffi:cinclude>
								</SELECT>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="3"><hr noshade></td>
			</tr>
			<tr>
				<td colspan="3"><br></td>
			</tr>
			<tr>
				<td colspan="3"><br></td>
			</tr>
			<tr>
				<td colspan="3" align="center">
					<sj:a 
						button="true" 
						buttonIcon="ui-icon-arrowreturnthick-1-w"
						onClickTopics="backToInput"
						><s:text name="jsp.default_57"/></sj:a>
					<sj:a
						button="true" 
						onClickTopics="cancelCompanyForm"
						><s:text name="jsp.default_82"/></sj:a>
					<sj:a 
						formIds="editAdministratorVerifyForm"
						targets="confirmDiv" 
						button="true"
						onBeforeTopics="beforeSubmit"
						onErrorTopics="errorSubmit"
						onSuccessTopics="completeAdministratorsSubmit"
						><s:text name="jsp.default_366"/></sj:a>
				</td>
			</tr>
		</table>
	</s:form>
</div>
