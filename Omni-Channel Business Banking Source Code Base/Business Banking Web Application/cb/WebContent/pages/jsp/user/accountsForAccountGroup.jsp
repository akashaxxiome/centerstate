<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page import="com.ffusion.beans.accountgroups.BusinessAccountGroup" %>
<%@ page import="com.ffusion.beans.accounts.Accounts" %>
<%@ page import="com.ffusion.beans.accounts.Account" %>

<% session.setAttribute("Init", request.getParameter("Init"));%>
<% session.setAttribute("grpid", request.getParameter("grpid"));%>
<div class="externallDialogHt" style="min-height:150px; ">
<%-- load the account group info from the accounts collection --%>
<ffi:cinclude value1="${Init}" value2="true" operator="equals">
<ffi:object name="com.ffusion.tasks.accountgroups.SetAccountGroupById" id="SetAccountGroupById" scope="session" />
<ffi:setProperty name="SetAccountGroupById" property="Id" value="${grpid}"/>
<ffi:setProperty name="SetAccountGroupById" property="AccountGroupsName" value="ParentsAccountGroups"/>
<ffi:process name="SetAccountGroupById"/>

<%
	BusinessAccountGroup acctGroup = (BusinessAccountGroup) session.getAttribute( "AccountGroup" );
	Accounts accountsToEdit = new Accounts();
	accountsToEdit.set(acctGroup.getAccounts());
	session.setAttribute( "FilteredAccounts", accountsToEdit );
%>
</ffi:cinclude>

<ffi:setProperty name="tempURL" value="setAccountGroupById.action?collectionName=FilteredAccounts&groupName=${AccountGroup.Name}" URLEncrypt="true"/>
<s:url namespace="/pages/user" id="aremoteurl" action="%{#session.tempURL}"/>
     <sjg:grid
       id="adminPermAcctGrpGrid"
       caption=""
	   sortable="true"  
       dataType="json"
       href="%{aremoteurl}"
       pager="true"
       viewrecords="true"
       gridModel="gridModel"
	   rowList="%{#session.StdGridRowList}" 
	   rowNum="%{#session.StdGridRowNum}" 
       rownumbers="false"
       scroll="false"
       altRows="true"
       sortname="bankName"
       sortorder="asc"
       shrinkToFit="true"
       navigator="true"
	   navigatorSearch="false"
       navigatorAdd="false"
       navigatorDelete="false"
       navigatorEdit="false"
       navigatorRefresh="false"
       onGridCompleteTopics="addGridControlsEvents,adminPermAcctGrpGridCompleteEvents"
     >
		<sjg:gridColumn name="map.gName" width="300" index="gName" title="%{getText('jsp.user_6')}" sortable="false" />
		<sjg:gridColumn name="bankName" index="BANKNAME" title="%{getText('jsp.default_63')}" sortable="true" width="125"/>
		<sjg:gridColumn name="displayText" index="NUMBER" title="%{getText('jsp.default_19')}" sortable="true" width="175" formatter="ns.admin.formatActGrpAcctNum"/>
		<sjg:gridColumn name="nickName" index="NICKNAME" title="%{getText('jsp.default_294')}" sortable="true" width="125"/>
		<sjg:gridColumn name="primaryAccount" index="primary" title="Primary Account" sortable="false" width="175" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="coreAccount" index="core" title="Core Account" sortable="false" width="125" hidden="true" hidedlg="true"/>

 </sjg:grid>

 <table border="0" width="100%">
    <tr>
        <td width="50%"><br></td>
        <td align="left">
         <span class="columndata">*&nbsp;Primary Account</span>
         <br>
         <span class="columndata">&#8226;&nbsp;External Account</span>
         <br>
        </td>
        <td width="5%"><br></td>
    </tr>
 </table>
</div> 
<script type="text/javascript">
	$("#adminPermAcctGrpGrid").data("supportSearch",true);
</script>

<%-- Tidy up the session. --%>
<ffi:setProperty name="Init" value=""/>

<ffi:removeProperty name="grpid"/>

<ffi:removeProperty name="SortImage"/>
<ffi:removeProperty name="AccountGroup"/>

