<%@ page import="java.util.Locale"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.beans.ach.ACHReportConsts"%>

<style>

</style>
<script src="<s:url value='/web'/>/js/reports/reports.js" type="text/javascript"></script>


<%-- This task will not be referenced in pages, so we can add prefix "FFI" directly. --%>
<ffi:object id="FFIUpdateReportCriteria" name="com.ffusion.tasks.reporting.UpdateReportCriteria"/>
<ffi:setProperty name="FFIUpdateReportCriteria" property="ReportName" value="ReportData"/>

<ffi:object id="FFIGenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
<%-- Adding the followed statement is because that the first print report function won't work well without it. --%>
<% session.setAttribute("GenerateReportBase", session.getAttribute("FFIGenerateReportBase")); %>


<%--NOTES:
	1.	The first sort condition for this report MUST be processing date (in descending order) for this report
		to work correctly.
	2.	The maximum display size is set in this JSP (this report's maximum display size will not change
		with changes to the setting in CSIL).  The display size is set here manually, because you MUST
		ensure that it is larger than the number of actions a user typically completes during a session.
		If the max display size is less than the number of actions a user performs, then the report will
		display incomplete data.
--%>


    <%
	   java.util.Calendar today = java.util.Calendar.getInstance();
       java.util.Calendar yesterday = java.util.Calendar.getInstance();
       today.add( java.util.Calendar.DAY_OF_YEAR, 1 );
       yesterday.add( java.util.Calendar.DAY_OF_YEAR, -1 );
	   String dateFormat = com.ffusion.util.DateConsts.REPORT_CRITERIA_INTERNAL_FORMAT_DATE_STR;
       String todayString = com.ffusion.util.DateFormatUtil.getFormatter( dateFormat ).format( today.getTime() );
       String yesterdayString = com.ffusion.util.DateFormatUtil.getFormatter( dateFormat ).format( yesterday.getTime() );
       String reportTitle = com.ffusion.beans.reporting.ReportConsts.getReportName( 
    		   com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_SESSION_ACTIVITY, Locale.getDefault());
       pageContext.setAttribute("reportTitle", reportTitle);
    %>

    <ffi:object id="GenerateReport" name="com.ffusion.tasks.reporting.GenerateReport"/>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<%-- Set a value for the identification name.  The GenerateReport task expects a name for its validation.
	     The session activity report cannot be saved, so this name is arbitrary. --%>
	<ffi:setProperty name="GenerateReport" property="IdentificationName" value="Session Activity Report" />

    <%-- The default sort order (of ordinal one) by Date should NOT be changes as the backend of this report
	 depends on the order. --%>
        <ffi:setProperty name="GenerateReport" property="SortCritName" value="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_PROCESS_DATE %>" />
        <ffi:setProperty name="GenerateReport" property="SortCritOrdinal" value="1" />
        <ffi:setProperty name="GenerateReport" property="SortCritAsc" value="FALSE" />

	<ffi:setProperty name="GenerateReport" property="SearchCritName" value="User" />
	<ffi:setProperty name="GenerateReport" property="SearchCritValue" value="${User.Id}"/>

	<%-- these criteria control how many days the report will go back (looking for the last login) --%>
	<ffi:setProperty name="GenerateReport" property="SearchCritName" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_DATE %>" />
	<ffi:setProperty name="GenerateReport" property="SearchCritValue" value="<%= yesterdayString %>"/>
	<ffi:setProperty name="GenerateReport" property="SearchCritName" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_DATE %>" />
	<ffi:setProperty name="GenerateReport" property="SearchCritValue" value="<%= todayString %>"/>

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value='<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_CATEGORY %>' />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value='<%= com.ffusion.tasks.admin.AdminTask.CATEGORY_NAME %>' />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value='<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>' />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value='<%= com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_SESSION_ACTIVITY %>' />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_TITLE %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value='<%=pageContext.getAttribute("reportTitle").toString()%>'></ffi:setProperty>
	
	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TITLE %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="SHOWCOMPANYNAME" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="SHOWDATE" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="SHOWTIME" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="SHOWHEADER" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="ALLPAGES" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="PAGENUMBERFORMAT" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="SHOWFOOTER" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="ALLPAGES" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DATE_FORMAT %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="${UserLocale.DateFormat}" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_TIME_FORMAT %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="HH:mm" />

<%-- This value must be set appropriately so that at the very least all of the items that may
     have been performed by the user during this session is less than the maximum display size --%>
	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_MAXDISPLAYSIZE %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="250" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="900" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH_TEXT %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="150" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_ORIENTATION %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_LANDSCAPE %>" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:setProperty name="GenerateReport" property="IdentificationName" value="<%= com.ffusion.beans.efsreport.EFSReportConsts.RPT_TYPE_SESSION_RECEIPT %>" />

	<ffi:setProperty name="GenerateReport" property="SortCritName" value="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_PROCESS_DATE %>" />
	<ffi:setProperty name="GenerateReport" property="SortCritOrdinal" value="1" />
	<ffi:setProperty name="GenerateReport" property="SortCritAsc" value="true" />

	<ffi:setProperty name="StringUtil" property="Value1" value="<%= com.ffusion.tasks.Task.SESSION_LOGIN_TIME %>" />
	<ffi:setProperty name="StringUtil" property="Value2" value="<%= com.ffusion.beans.reporting.ReportCriteria.HIDE_SUFFIX %>" />

	<ffi:setProperty name="GenerateReport" property="SearchCritName" value="${StringUtil.Concatenate}" />
	<ffi:setProperty name="GenerateReport" property="SearchCritValue" value="${SessionLoginTime.TimeInMillis}" />

	<ffi:setProperty name="GenerateReport" property="SearchCritName" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_USER %>" />
	<ffi:setProperty name="GenerateReport" property="SearchCritValue" value="${SecureUser.ProfileID}"/>

	<ffi:setProperty name="GenerateReport" property="SearchCritName" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>" />
	<ffi:setProperty name="GenerateReport" property="SearchCritValue" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>"/>

	<ffi:setProperty name="GenerateReport" property="SearchCritName" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>" />
	<ffi:setProperty name="GenerateReport" property="SearchCritValue" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_SESSION %>"/>

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value='<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>' />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value='<%= com.ffusion.beans.efsreport.EFSReportConsts.RPT_TYPE_SESSION_RECEIPT %>' />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_DATE %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TITLE %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_TITLE %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value='<%=pageContext.getAttribute("reportTitle").toString()%>'></ffi:setProperty>

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TIME %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_HEADER %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGE_NUMBER_FORMAT %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="PAGE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_PAGE_NUMBER_FORMAT %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_FOOTER %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="TRUE" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DATE_FORMAT %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="MM/dd/yyyy" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_TIME_FORMAT %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="HH:mm" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_MAXDISPLAYSIZE %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="250" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="741" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH_TEXT %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="175" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_ORIENTATION %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_LANDSCAPE %>" />
</ffi:cinclude>

	<ffi:setProperty name="GenerateReport" property="ReportName" value="ReportData" />

	<ffi:setProperty name="GenerateReport" property="ReportOptionName" value="<%=com.ffusion.beans.reporting.ReportCriteria.OPT_GENERATE_TASK %>" />
	<ffi:setProperty name="GenerateReport" property="ReportOptionValue" value="com.ffusion.tasks.admin.GetReportData"/>
    <ffi:process name="GenerateReport" />

    <ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
    <ffi:process name="GenerateReportBase"/>


<div id="sessionActivityReportID">
	<div id="exportReortID" >
		<tr>
			<s:form id="exportSessionActivityReportFormID"  theme="simple" cssStyle="text-align:right">
			<input type="hidden" name="GenerateReportBase.Destination" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>">
			<input type="hidden" name="CSRF_TOKEN"	value="<ffi:getProperty name='CSRF_TOKEN'/>" />
			<input type="hidden" name="returnjsp"   value="false" />
			<s:hidden key="reportID" />

				<td>
					<select id="exportReportSelectSessionActID" class="txtbox" name="GenerateReportBase.Format"
						onchange="{ns.report.exportReport('/cb/pages/jsp/reports/UpdateReportCriteriaAction_generateReport.action', $('#exportSessionActivityReportFormID')[0]);}">
					
						<option value="0">
							<s:text name="jsp.default_195"/>
						</option>
						
						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>">
							<s:text name="jsp.default_231"/>
						</option>

						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>">
							<s:text name="jsp.default_105"/>
						</option>

						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>">
							<s:text name="jsp.default_402"/>
						</option>

						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>">
							<s:text name="jsp.default_323"/>
						</option>

						<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>">
							<s:text name="jsp.default_325"/>
						</option>
					</select>&nbsp;&nbsp;
					<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>">
				</td>
					<%-- <sj:a id="exportSessionActivityReportButtonId"
						  button="true"
						  formIds="exportSessionActivityReportFormID"
						>
						  EXPORT
					</sj:a> --%>

						<%-- <sj:a id="exportSessionActivityReportButtonId"
						onclick="{ns.report.exportReport('/cb/pages/jsp/reports/UpdateReportCriteriaAction_generateReport.action', $('#exportSessionActivityReportFormID')[0]);}"
						effect="pulsate" value="Export" indicator="indicator" button="true" buttonIcon="ui-icon-print"
						><s:text name="jsp.default_195"/></sj:a> --%>

					<%--<td align="left" nowrap>
						<input class="submitbutton" type="submit" name="Submit2" value="EXPORT">
					</td>--%>
			</tr>
			</s:form>
		</tr>
	</div>
	
	<div id="sessionActivityReportTableID" class="tableData sessionActivityReportTable marginTop10">
		<tr>
			<td class="tbrd_ltr"><br>
				<ffi:object id="exportForDisplay" name="com.ffusion.tasks.reporting.ExportReport"/>
				<ffi:setProperty name="exportForDisplay" property="ReportName" value="ReportData"/>
				<ffi:setProperty name="exportForDisplay" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
				<ffi:process name="exportForDisplay"/>
				<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_NAME %>" encode="false"/>
			</td>
		</tr>
	</div>
	
</div>


<s:form id="otherPrefs" name="otherPrefs" method="post" action="QuickEditPreferenceAction_updateSessionSettings.action" namespace="/pages/jsp/user" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" id="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

	<div id="PreferenceDesk" style="width:100%;">
	<!-- Session report area -->
	<div id="sessionReportPortletId" class="sessionReportCls marginTop10" width="99%" style="font-size: 13px;">
		<s:text name="jsp.user_163"/>&nbsp;
			<input type="checkbox" name="test" onClick="setSessionActivityFlag()" id="sessionActivityCheck" style="vertical-align: middle;"
			<s:if test="#session.DISPLAY_SESSION_SUMMARY_REPORT">checked</s:if>>
			<input type="hidden" name="DisplaySessionSummary" id="sessionActivity"> 
			<!-- bDisplaySessionSummary -->
	</div>
	<div id="otherPreferenceBtn" class="ui-widget-header customDialogFooter">
		<%-- <sj:a id="resetOtherpreferenceButtonId" onclick="ns.userPreference.resetPreferences('MISC_TAB');" button="true">
			<s:text name="jsp.default_358"/>
		</sj:a> --%>
		<sj:a id="cancelOtherpreferenceButtonId" button="true" onclick="ns.common.closeDialog('sessionActivityDialogId')"  cssClass="hiddenWhilePrinting">
			<s:text name="jsp.default_102"/>
		</sj:a>
		<sj:a id="printSessionActivityDialog" button="true" onclick="ns.common.printPopup();" cssClass="hiddenWhilePrinting">
			<s:text name="jsp.default_331"/>
		</sj:a>
		<sj:a id="saveOtherPrefButton" formIds="otherPrefs" button="true" validate="false" targets="Miscellaneous" 
			onCompleteTopics="quickSessionOtherPreferences" cssClass="hiddenWhilePrinting">
			<s:text name="jsp.default_366"/>
		</sj:a>
	</div>
	</div>
</s:form>

<script>
	$("#exportReportSelectSessionActID").selectmenu({width:'12em'});
	
	$("#sessionActivityReportID").parent().addClass("noXOverflow");
	
	function setSessionActivityFlag(){
		if($('#sessionActivityCheck').is(":checked")){
			$('#sessionActivity').val(true);
		}else{
			$('#sessionActivity').val(false);
		}
	}
</script>