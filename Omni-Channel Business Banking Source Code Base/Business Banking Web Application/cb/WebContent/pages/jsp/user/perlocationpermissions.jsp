<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<div id="Per_Locations">
<div id="perlocationpermissionsDiv" class="perlocationpermissionsHelpCSS" >
<ffi:help id="user_perlocationpermissions" className="moduleHelpClass"/>
<%
	request.setAttribute("runSearch", request.getParameter("runSearch"));
	request.setAttribute("FromBack", request.getParameter("FromBack"));
	request.setAttribute("UseLastRequest", request.getParameter("UseLastRequest"));
%>

<% boolean includeNEXTPage = false;
// if NOT ENTITLED to WIRE TEMPLATES, don't show the
// NEXT PERMISSION button
%>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>">
    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE %>">
        <% includeNEXTPage = true; %>
    </ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${runSearch}" value2="true" operator="equals">
	<%
	request.setAttribute("SetBusinessEmployee.Id",request.getParameter("SetBusinessEmployee.Id"));
	request.setAttribute("EditGroup_GroupName",request.getParameter("EditGroup_GroupName"));
	request.setAttribute("EditGroup_GroupId",request.getParameter("EditGroup_GroupId"));
	request.setAttribute("EditGroup_DivisionName",request.getParameter("EditGroup_DivisionName"));
	request.setAttribute("PermissionsWizard",request.getParameter("PermissionsWizard"));
	// needs to be session for verify page back button to work properly
	session.setAttribute("DivisionId",request.getParameter("DivisionId"));
	%>
</ffi:cinclude>

<s:include value="inc/perlocationpermissions-pre.jsp" />

<s:include value="inc/parentchild_js.jsp"/>

<script>
$(function(){
	$("#perLocDivisionSelect").combobox({'size':'26'});
	$("#locationOptionSelect").selectmenu({width: 250});
	
	//perLocationLimitFrmSubmit
});
function locationOptionSelection(){
	$("#perLocationLimitFrmSubmit").trigger('click');
}
</script>
<%-- <span><s:text name="jsp.user_413"/></span> --%>
<div align="center">
<ffi:cinclude value1="${displayCashConcentation}" value2="false">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="adminBackground">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td>
                <div align="center">
                <span class="sectionsubhead"><s:text name="jsp.user_215"/></span>
                </div>
            </td>
            </tr>
            <tr>
            <td style="padding: 6px;">
                <div align="center">
                    <sj:a
                        button="true"
                        onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
                        ><s:text name="jsp.default_102"/></sj:a>
                </div>
            </td>
            </tr>
        </table>
        </td>
    </tr>
    </table>
</ffi:cinclude>
<ffi:cinclude value1="${displayCashConcentation}" value2="true">
<ffi:cinclude value1="${DivisionId}" value2="" operator="equals">
			    <div align="center">
					<span class="sectionsubhead"><s:text name="jsp.user_212"/></span>
			    </div>
			    <div align="center" class="marginTop10">
					<sj:a
						button="true"
						onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
						><s:text name="jsp.default_102"/></sj:a>
                          <% if (includeNEXTPage) { %>
					<sj:a
						id="perLocationNextButton0"
						button="true"
						onclick="return ns.admin.nextPermission('#perLocationNextButton0', '#permTabs');"
						><s:text name="jsp.user_203"/></sj:a>
                          <% } %>
			    </div>
</ffi:cinclude>
<ffi:cinclude value1="${DivisionId}" value2="" operator="notEquals">
<div class="errortext"><ul id="formerrors"></ul></div>				
<div class="sectionhead marginBottom10">
<ffi:getProperty name="Context"/>
</div>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<ffi:cinclude value1="${showSearch}" value2="TRUE">
					<td class="adminBackground" valign="top">
						<table height="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="400" align="left" class="adminBackground">
									
									<s:form id="formSearchLocations" action="<ffi:getProperty name='SecureServletPath'/>GetLocations" method="post" name="formSearchLocations">
											<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
											<input type="hidden" name="runSearch" value="false">
											<input type="hidden" name="GetLocations.DivisionID" value="<ffi:getProperty name="DivisionId"/>">
										<table width="100%" border="0">										
											<tr>
												<td colspan="2" class="sectionsubhead" height="20">&nbsp;&nbsp;&gt; <s:text name="jsp.default_548"/></td>
											</tr>
											<tr>
												<td colspan="2" class="columndata">&nbsp;
													<s:text name="jsp.user_342"/>
												</td>
											</tr>
											<tr>
												<td class="sectionsubhead" height="20" width="50%" align="right"><s:text name="jsp.default_267"/></td>
												<td class="sectionsubhead"><input class="ui-widget-content ui-corner-all" type="text" name="GetLocations.LocationName" size="17" maxlength="40" class="txtbox" value="<ffi:getProperty name="GetLocations" property="LocationName"/>"></td>
											</tr>
											<tr>
												<td class="sectionsubhead" align="right"><s:text name="jsp.user_189"/></td>
												<td class="sectionsubhead"><input class="ui-widget-content ui-corner-all" type="text" name="GetLocations.LocationID" size="17" maxlength="17" class="txtbox" value="<ffi:getProperty name="GetLocations" property="LocationID"/>"></td>
											</tr>
											<tr>
												<td class="sectionsubhead" colspan="2" height="20"  align="center"><input class="submitbutton" type="submit" value="<s:text name="jsp.default_373"/>"></td>
											</tr>
										</table>
									</s:form>
								</td>
							</tr>
						</table>
					</td>
					<td>&nbsp;</td>
				</ffi:cinclude>
				<td class="adminBackground" valign="top" align="center">
					<s:form id="LocationDivisionSelect" name="LocationDivisionSelect">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<table border="0" cellspacing="5" cellpadding="0">
							<tr>
								<ffi:cinclude value1="${Section}" value2="Divisions" operator="notEquals">
									<td align="right" class="sectionsubhead"><s:text name="jsp.user_122"/></td>
								</ffi:cinclude>
								<td align="left" class="sectionsubhead">
									<ffi:cinclude value1="${UseDivisionList}" value2="true" operator="equals">
										<input type="hidden" name="runSearch" value="true">
										<input type="hidden" name="Section" value="<ffi:getProperty name="Section"/>">
										<input type="hidden" name="SetBusinessEmployee.Id" value="<ffi:getProperty name="BusinessEmployee" property="Id"/>">
										<input type="hidden" name="EditGroup_GroupId" value="<ffi:getProperty name="EditGroup_GroupId"/>">
										<input type="hidden" name="EditGroup_GroupName" value="<ffi:getProperty name="EditGroup_GroupName"/>">
										<input type="hidden" name="EditGroup_DivisionName" value="<ffi:getProperty name="EditGroup_DivisionName"/>">
										<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">
											<div class="selectBoxHolder">
												<select id="perLocDivisionSelect" name="DivisionId" class="txtbox" onChange="ns.admin.viewPerLocations();">
													<ffi:list collection="Descendants" items="Division">
														<option value="<ffi:getProperty name="Division" property="GroupId"/>" <ffi:cinclude value1="${DivisionId}" value2="${Division.GroupId}" operator="equals">selected<%--L10NEnd--%></ffi:cinclude>>
															<ffi:getProperty name="Division" property="GroupName"/>
														</option>
													</ffi:list>
												</select>
											</div>
									</ffi:cinclude>
									<ffi:cinclude value1="${UseDivisionList}" value2="true" operator="notEquals">
										<input type="hidden" name="DivisionId" value="<ffi:getProperty name="DivisionId"/>">
										<ffi:cinclude value1="${Section}" value2="Divisions" operator="notEquals">
											<ffi:getProperty name="DivisionName"/>
										</ffi:cinclude>
									</ffi:cinclude>
								</td>
							</tr>
						</table>
					</s:form>
						<ffi:cinclude value1="${Locations.size}" value2="0" operator="equals">
							<tr>
								<td class="adminBackground" align="center" colspan="2">
									<div class="sectionsubhead marginTop10 marginBottom10">
										<ffi:cinclude value1="${showSearch}" value2="TRUE" operator="equals">
										   <s:text name="jsp.user_225"/>
												<sj:a
													button="true"
													onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
													><s:text name="jsp.default_102"/></sj:a>
                                            <% if (includeNEXTPage) { %>
												<sj:a
													id="perLocationNextButton1"
													button="true"
													onclick="return ns.admin.nextPermission('#perLocationNextButton1', '#permTabs');"
													><s:text name="jsp.user_203"/></sj:a>
                                            <% } %>
										</ffi:cinclude>
										<ffi:cinclude value1="${showSearch}" value2="TRUE" operator="notEquals">
											<s:text name="jsp.user_328"/><br><br>
												<sj:a
													button="true"
													onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
                                            <% if (includeNEXTPage) { %>
												<sj:a
													id="perLocationNextButton2"
													button="true"
													onclick="return ns.admin.nextPermission('#perLocationNextButton2', '#permTabs');"
													><s:text name="jsp.user_203"/></sj:a>
                                            <% } %>
												
										</ffi:cinclude>
									</div>
								</td>
							</tr>
						</ffi:cinclude>
						<ffi:cinclude value1="${Locations.size}" value2="0" operator="notEquals">
							<tr>
								<td class="adminBackground" colspan="2">
									<s:form id="LocationLimitSelect" namespace="/pages/user" action="perlocationpermissions_localimitform"  validate="false" theme="simple" method="post" name="LocationLimitSelect">
										<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
										<input type="hidden" name="runSearch" value="false">
										<input type="hidden" name="Section" value="<ffi:getProperty name="Section"/>">
										<input type="hidden" name="SetBusinessEmployee.Id" value="<ffi:getProperty name="BusinessEmployee" property="Id"/>">
										<input type="hidden" name="EditGroup_GroupId" value="<ffi:getProperty name="EditGroup_GroupId"/>">
										<input type="hidden" name="EditGroup_GroupName" value="<ffi:getProperty name="EditGroup_GroupName"/>">
										<input type="hidden" name="EditGroup_DivisionName" value="<ffi:getProperty name="EditGroup_DivisionName"/>">
										<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">
										<table border="0" cellspacing="5" cellpadding="0" align="center">
											<tr>
												<td class="columndata" height="20" align="left" colspan="2"><s:text name="jsp.user_284"/></td>
											</tr>
											<tr>
												<td align="right" class="sectionsubhead"><s:text name="jsp.user_190"/></td>
												<td align="left" class="sectionsubhead">&nbsp;&nbsp;&nbsp;
													<select name="Location_BPWID" class="txtbox" size="5" id="locationOptionSelect" onchange="locationOptionSelection()">
														<ffi:list collection="Locations" items="LocationItem">
														<option value="<ffi:getProperty name="LocationItem" property="LocationBPWID"/>"
																<ffi:cinclude value1="${Location_BPWID}" value2="${LocationItem.LocationBPWID}" operator="equals">selected</ffi:cinclude>
																<ffi:list collection="daObjectIds" items="daLocationId">
																	<ffi:cinclude value1="${daLocationId}" value2="${LocationItem.LocationBPWID}" operator="equals"> class="columndataDA"</ffi:cinclude>
																</ffi:list>
																>
																<ffi:getProperty name="LocationItem" property="LocationName"/>
																<ffi:cinclude value1="${LocationItem.LocationID}" value2="" operator="notEquals">
																	- <ffi:getProperty name="LocationItem" property="LocationID"/>
																</ffi:cinclude>
														</option>
														</ffi:list>
													</select>
												</td>
											</tr>
											<tr>
												<td class="sectionsubhead" colspan="2" height="0" align="center">
													<sj:a
														href="#"
														formIds="LocationLimitSelect"
														targets="perLocationLimitFrm"
														button="true"
														id="perLocationLimitFrmSubmit"
														validate="false"
														onCompleteTopics="onCompleteTabifyAndFocusTopic"
														cssStyle="display:none"
														><s:text name="jsp.default_459"/></sj:a>
												</td>
											</tr>
										</table>
									</s:form>
								</td>
							</tr>
						</ffi:cinclude>
					</table>
				</td>
			</tr>
		<div id="perLocationLimitFrm">
			<s:include value="perlocationpermissions_localimitform.jsp" />
		</div>
</ffi:cinclude>
</ffi:cinclude>
		</div>
</div>
</div>
<ffi:removeProperty name="GetMaxLimitForPeriod"/>
<ffi:removeProperty name="GetGroupLimits"/>
<ffi:removeProperty name="Entitlement_Limits"/>

<ffi:removeProperty name="limitIndex"/>

<ffi:removeProperty name="LastRequest"/>
<ffi:removeProperty name="PageHeading2"/>
<ffi:removeProperty name="DisplayErrors"/>
<ffi:removeProperty name="CheckEntitlementObjectType"/>
<ffi:removeProperty name="CheckEntitlementObjectId"/>
<%--
<ffi:removeProperty name="Descendants"/>
--%>
<ffi:removeProperty name="CheckOperationEntitlement"/>
<ffi:removeProperty name="GetLocations"/>
<ffi:removeProperty name="ParentChild"/>
<ffi:removeProperty name="HasAdmin"/>
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>
<ffi:removeProperty name="HandleParentChildDisplay"/>
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:removeProperty name="SavePermissionsWizard"/>
<ffi:removeProperty name="dependentDaEntitlements"/>
<ffi:setProperty name="FromBack" value=""/>
<ffi:removeProperty name="runSearch"/>
<ffi:removeProperty name="DivisionId"/>

<s:include value="%{#session.PagesPath}user/inc/set-page-da-css.jsp" />