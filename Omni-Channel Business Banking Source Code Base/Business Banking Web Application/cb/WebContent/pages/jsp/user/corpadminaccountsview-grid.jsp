<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
    
<div align="center">
		
	<ffi:setGridURL grid="GRID_accountConfig" name="ViewURL" url="/cb/pages/jsp/user/corpadminaccount-view.jsp?aid={0}&did={1}&rnum={2}" parm0="ID" parm1="viewBusinessID" parm2="RoutingNum"/>
	
	<ffi:setProperty name="accountConfigGridTempURL" value="/pages/user/getACHAccounts.action?collectionName=FilteredAccounts&GridURLs=GRID_accountConfig" URLEncrypt="true"/>
	<s:url id="getSortImageUrl" value="%{#session.accountConfigGridTempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="accountConfigGrid"
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{getSortImageUrl}"  
		pager="true"  
		viewrecords="true" 
		gridModel="gridModel" 
 		rowList="%{#session.StdGridRowList}" 
 		rowNum="%{#session.StdGridRowNum}" 
		shrinkToFit="true"
		scroll="false"
		altRows="true"
		sortname="BANKNAME"
		sortorder="asc"
		navigator="true"
		navigatorSearch="false"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		onGridCompleteTopics="addGridControlsEvents,completeAccountCongiGridLoad"> 
		
		<sjg:gridColumn name="bankName" index="BANKNAME" title="%{getText('jsp.default_61')}" sortable="true" width="125"/>
		<sjg:gridColumn name="routingNum" index="ROUTINGNUM" title="%{getText('jsp.user_280')}" sortable="true" width="108"/>
		<sjg:gridColumn name="displayText" index="NUMBER" title="%{getText('jsp.default_16')}" formatter="formatAccountColumn" sortable="true" width="175"/>
		<sjg:gridColumn name="type" index="TYPE" title="%{getText('jsp.default_444')}" sortable="true" width="108"/> 
		<sjg:gridColumn name="currencyCode" index="CURRENCY_CODE" title="%{getText('jsp.default_125')}" sortable="true" width="108"/> 
		<sjg:gridColumn name="nickName" index="NICKNAME" title="%{getText('jsp.default_294')}" sortable="true" width="125"/>
		<sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.default_27')}" formatter="formatAccountConfigActionViewLinks" sortable="false"  search="false" width="60" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
		
		<sjg:gridColumn name="primaryAccount" index="primaryAccount" title="%{getText('jsp.user_263')}" sortable="false" width="10" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="coreAccount" index="coreAccount" title="%{getText('jsp.user_96')}" sortable="false" width="10" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.highLightField" index="highLightField" title="highLightField" sortable="false"  width="10" hidden="true" hidedlg="true"/>
	</sjg:grid>
	</div>
	<script type="text/javascript">
		$("#accountConfigGrid").data("supportSearch",true);
		
		var fnCustomAccountConfigRefresh = function (event) {
			ns.common.forceReloadCurrentSummary();
		};
		
		$("#accountConfigGrid").data('fnCustomRefresh', fnCustomAccountConfigRefresh);
	</script>
	