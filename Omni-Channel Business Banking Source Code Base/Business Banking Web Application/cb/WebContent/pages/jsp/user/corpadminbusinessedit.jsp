<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:help id="user_corpadminbusinessedit" className="moduleHelpClass"/>
<div id='PageHeading' style="display:none;"><s:text name="jsp.user_140"/></div>
<% request.setAttribute("goback", request.getParameter("goback")); %>

<ffi:cinclude value1="${goback}" value2="true" operator="notEquals">
	<ffi:object id="ModifyBusiness" name="com.ffusion.tasks.user.ModifyBusiness" scope="session"/>
		<ffi:setProperty name="ModifyBusiness" property="Initialize" value="true" />
	<ffi:process name="ModifyBusiness"/>
	<ffi:setProperty name="ModifyBusiness" property="Street" value="${Business.Street}"/>
	<ffi:setProperty name="ModifyBusiness" property="Street2" value="${Business.Street2}"/>
	<ffi:setProperty name="ModifyBusiness" property="City" value="${Business.City}"/>
	<ffi:setProperty name="ModifyBusiness" property="TaxId" value="${Business.TaxId}"/>
	<ffi:setProperty name="ModifyBusiness" property="State" value="${Business.State}"/>
	<ffi:setProperty name="ModifyBusiness" property="ZipCode" value="${Business.ZipCode}"/>
	<ffi:setProperty name="ModifyBusiness" property="Country" value="${Business.Country}"/>
	<ffi:setProperty name="ModifyBusiness" property="Phone" value="${Business.Phone}"/>
	<ffi:setProperty name="ModifyBusiness" property="FaxPhone" value="${Business.FaxPhone}"/>
	<ffi:setProperty name="ModifyBusiness" property="Email" value="${Business.Email}"/>
</ffi:cinclude>
<ffi:cinclude value1="${goback}" value2="true" operator="equals">
	<ffi:cinclude value1="${ModifyBusiness}" value2="" operator="equals">
		<!-- ModifyBusiness not found - try getting a new one -->
		<ffi:object id="ModifyBusiness" name="com.ffusion.tasks.user.ModifyBusiness" scope="session"/>
			<ffi:setProperty name="ModifyBusiness" property="Initialize" value="true" />
		<ffi:process name="ModifyBusiness"/>
		<ffi:setProperty name="ModifyBusiness" property="Street" value="${Business.Street}"/>
		<ffi:setProperty name="ModifyBusiness" property="Street2" value="${Business.Street2}"/>
		<ffi:setProperty name="ModifyBusiness" property="City" value="${Business.City}"/>
		<ffi:setProperty name="ModifyBusiness" property="State" value="${Business.State}"/>
		<ffi:setProperty name="ModifyBusiness" property="ZipCode" value="${Business.ZipCode}"/>
    	<ffi:setProperty name="ModifyBusiness" property="TaxId" value="${Business.TaxId}"/>
		<ffi:setProperty name="ModifyBusiness" property="Country" value="${Business.Country}"/>
		<ffi:setProperty name="ModifyBusiness" property="Phone" value="${Business.Phone}"/>
		<ffi:setProperty name="ModifyBusiness" property="FaxPhone" value="${Business.FaxPhone}"/>
		<ffi:setProperty name="ModifyBusiness" property="Email" value="${Business.Email}"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:removeProperty name="goback"/>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="Business"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
	<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="ModifyBusiness"/>
<ffi:process name="PopulateObjectWithDAValues"/>

<% session.setAttribute("EditBusiness", session.getAttribute("ModifyBusiness") ); %>
<% session.setAttribute("FFIEditBusiness", session.getAttribute("ModifyBusiness") ); %>

<script type="text/javascript">

	$(function(){
		$("#CountrySelect").combobox({'size':'25'});
		$("#StateSelect").combobox({'size':'25',searchFromFirst:true});
		/* $("#StateSelect").selectmenu({width: 250}); */
		$("#corpadminResetButton").button({
			
		});
	});	
	
	ns.admin.showState = function(obj) {
		var urlString = '/cb/pages/jsp/user/corpadmininfocountrystate.jsp';
		$.ajax({
			   type: 'POST',
			   url: urlString,
			   data: "countryCode=" + obj.value + "&CSRF_TOKEN=" + '<ffi:getProperty name='CSRF_TOKEN'/>',
			   success: function(data){
			    if(data.indexOf("option") != -1) {
			    	$("#stateLabelNameId").html('<s:text name="jsp.user_299" /><span class="required">*</span>');
			    } else {
			    	$("#stateLabelNameId").html('<span></span>');
			    }
			    $("#stateSelectId").html(data);
		   }
		});
	}

</script>
<s:form id="editBusinessForm" namespace="/pages/user" name="editBusinessForm" action="modifyBusiness" validate="false" method="post" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">
		<tr>
			<td class="sectionhead" width="33%"><s:text name="jsp.user_201"/></td>
			<td width="33%" class='<ffi:getPendingStyle fieldname="street" defaultcss="sectionhead"  dacss="sectionheadDA"/>' valign="middle"><s:text name="jsp.user_31"/><span class="required">*</span></td>
			<td width="33%" class='<ffi:getPendingStyle fieldname="street2" defaultcss="sectionhead"  dacss="sectionheadDA"/>' valign="middle"><s:text name="jsp.user_30"/></td>
		</tr>
		  <tr>
		    <td class="columndata" align="left" valign="middle">
				<ffi:getProperty name="EditBusiness" property="BusinessName"/>
		    </td>
		    <td class="columndata" valign="middle" align="left">
				<s:textfield name="EditBusiness.Street" size="36" maxlength="40" style="width:200px;" value="%{#session.EditBusiness.street}" cssClass="ui-widget-content ui-corner-all"/>
				
				<span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Street" /></span>
		    </td>
		    <td class="columndata" valign="middle" align="left">
				<s:textfield name="EditBusiness.Street2" size="36" maxlength="40" style="width:200px;" value="%{#session.EditBusiness.street2}" cssClass="ui-widget-content ui-corner-all"/>
				<span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Street2" /></span>
		    </td>
		  </tr>
		  <tr>
		  	<td></td>
		  	<td><span id="EditBusiness.StreetError"></span></td>
		  	<td><span id="EditBusiness.Street2Error"></span></td>
		  </tr>
		  <tr>
		    <td class='<ffi:getPendingStyle fieldname="city" defaultcss="sectionhead"  dacss="sectionheadDA"/>' valign="middle"><s:text name="jsp.user_65"/><span class="required">*</span></td>
		    <td id="stateLabelNameId" class='<ffi:getPendingStyle fieldname="state" defaultcss="sectionhead"  dacss="sectionheadDA"/>'  valign="middle"><s:text name="jsp.user_299"/><span class="required">*</span></td>
		    <td class='<ffi:getPendingStyle fieldname="country" defaultcss="sectionhead"  dacss="sectionheadDA"/>' valign="middle"><s:text name="jsp.user_97"/><span class="required">*</span></td>
		  </tr>
		  <tr>
		    <td class="columndata" valign="middle" align="left">
				<s:textfield name="EditBusiness.City" size="36" maxlength="20" style="width:200px;" value="%{#session.EditBusiness.city}" cssClass="ui-widget-content ui-corner-all"/>
				<span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="City" /></span>
		    </td>
			<ffi:setProperty name="State_ResourceFilename" value="com.ffusion.utilresources.states"/>
			<ffi:setProperty name="State_ResourceID" value="StateList"/>
			<!-- default selected country and state -->
			<%
			    String businessState = null;
			    String businessCountry = null;
			%>
		    <ffi:getProperty name="EditBusiness" property="Country" assignTo="businessCountry"/>
		    <% if (businessCountry == null) { businessCountry = ""; } %>
		    <ffi:getProperty name="EditBusiness" property="State" assignTo="businessState"/>
		    <% if (businessState == null) { businessState = ""; } %>
		
			<ffi:object id="GetStatesForCountry" name="com.ffusion.tasks.util.GetStatesForCountry" scope="session" />
			    <ffi:setProperty name="GetStatesForCountry" property="CountryCode" value="<%= businessCountry %>" />
			<ffi:process name="GetStatesForCountry" />
			
			<!-- if there's no state by default country, we have to keep  stateLabelNameId and stateSelectId exist -->		
			<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
			<!--  if there's no state by default country, do not need to validate state, here set checkCompanyProfileState to be FALSE -->
			<ffi:setProperty name="checkCompanyProfileState" value="FALSE"/>
            	<td class="columndata" valign="middle" align="left">
         			<div id="stateSelectId"></div>
            	</td>
	        </ffi:cinclude>
			<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
			<!--  if there's state by default country, need to validate state, here set checkCompanyProfileState to be TRUE -->
			<ffi:setProperty name="checkCompanyProfileState" value="TRUE"/>
				<td class="columndata" valign="middle" align="left">
				
					<div id="stateSelectId">
						<div class="selectBoxHolder">
							<select class="txtbox" id="StateSelect" name="EditBusiness.State" value='<ffi:getProperty name="EditBusiness" property="State" />' size="1">
								<option<ffi:cinclude value1="<%= businessState %>" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.default_376"/></option>
								<ffi:list collection="GetStatesForCountry.StateList" items="item">
									<option <ffi:cinclude value1="<%= businessState %>" value2="${item.StateKey}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="StateKey"/>"><ffi:getProperty name='item' property='Name'/></option>
								</ffi:list>
							</select>
						</div>
					</div>
					<ffi:list collection="GetStatesForCountry.StateList" items="item">
					 	<ffi:cinclude value1="${oldDAObject.State}" value2="${item.StateKey}">
							<span class="sectionhead_greyDA"><ffi:getProperty name='item' property='Name'/></span>
						</ffi:cinclude>
					</ffi:list>
				</td>
			  
	        </ffi:cinclude>
	        <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
	            <ffi:setProperty name="EditBusiness" property="State" value=""/>
	        </ffi:cinclude>
		    <td class="columndata" valign="middle" align="left">
				<%--
					The resource filename and ID used for the abbreviated country names which will be the values stored.
					These properties are also used to verify the country values to be stored;
					the values are needed in corpadminbusinessedit-confirm.jsp for the ModifyBusiness task
					(with id "ModifyBusiness") to enable the verification.
				--%>
				<ffi:setProperty name="Country_ResourceFilename" value="com.ffusion.utilresources.states"/>
				<ffi:setProperty name="Country_ResourceID" value="CountryList"/>

				<ffi:object name="com.ffusion.tasks.util.GetCountryList" id="GetCountryList" scope="session"/>
				<ffi:setProperty name="GetCountryList" property="CollectionSessionName" value="Country_List"/>
				<ffi:process name="GetCountryList"/>
				<ffi:removeProperty name="GetCountryList"/>    

					<div class="selectBoxHolder">
						<select class="txtbox" id="CountrySelect" name="EditBusiness.Country" value='<ffi:getProperty name="EditBusiness" property="Country" />' size="1" onchange="ns.admin.showState(this);">
						<option<ffi:cinclude value1="<%= businessCountry %>" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.user_283"/></option>
						<ffi:list collection="Country_List" items="item">
							<option <ffi:cinclude value1="<%= businessCountry %>" value2="${item.CountryCode}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="CountryCode"/>"><ffi:getProperty name='item' property='Name'/></option>
						</ffi:list>
						</select>
					</div>	
					<ffi:list collection="Country_List" items="item">
						 	<ffi:cinclude value1="${oldDAObject.Country}" value2="${item.CountryCode}">
						<span class="sectionhead_greyDA"><ffi:getProperty name='item' property='Name'/></span>
						</ffi:cinclude>
					</ffi:list>
		    </td>
		  </tr>
		  <tr>
		  	<td><span id="EditBusiness.CityError"></span></td>
		  	<td><span id="EditBusiness.StateError"></span></td>
		  	<td><span id="EditBusiness.CountryError"></span></td>
		  </tr>
		  <tr>
		  	<td class='<ffi:getPendingStyle fieldname="zipCode" defaultcss="sectionhead"  dacss="sectionheadDA"/>'><s:text name="jsp.user_391"/><span class="required">*</span></td>
		  	<td class='<ffi:getPendingStyle fieldname="phone" defaultcss="sectionhead"  dacss="sectionheadDA"/>' valign="middle"><s:text name="jsp.user_252"/><span class="required">*</span></td>
		  	<td class='<ffi:getPendingStyle fieldname="faxPhone" defaultcss="sectionhead"  dacss="sectionheadDA"/>' valign="middle"><s:text name="jsp.user_157"/></td>
		  </tr>
		  <tr>
		    <td class="columndata" valign="middle" align="left">
				<s:textfield name="EditBusiness.ZipCode" size="10" maxlength="11" style="width:100px;" value="%{#session.EditBusiness.zipCode}" cssClass="ui-widget-content ui-corner-all"/>
				<span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="ZipCode" /></span>
			</td>
		    <td class="columndata" valign="top" align="left">
				<s:textfield name="EditBusiness.Phone" size="20" maxlength="14" style="width:100px;" value="%{#session.EditBusiness.phone}" cssClass="ui-widget-content ui-corner-all"/>
				<span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Phone" /></span>
		    </td>
		    <td class="columndata" valign="middle" align="left">
				<s:textfield name="EditBusiness.FaxPhone" size="20" maxlength="14" style="width:100px;" value="%{#session.EditBusiness.faxPhone}" cssClass="ui-widget-content ui-corner-all"/>
				<span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="FaxPhone" /></span>
		    </td>
		  </tr>
		  <tr>
		  	<td><span id="EditBusiness.ZipCodeError"></span></td>
		  	<td><span id="EditBusiness.PhoneError"></span></td>
		  	<td><span id="EditBusiness.FaxPhoneError"></span></td>
		  </tr>
		  <tr>
		    <td colspan="3" class='<ffi:getPendingStyle fieldname="email" defaultcss="sectionhead"  dacss="sectionheadDA"/>' valign="middle"><s:text name="jsp.user_133"/></td>
		  </tr>
		  <tr>
	    	<td class="columndata" valign="middle" align="left" colspan="3">
				<s:textfield name="EditBusiness.Email" size="36" maxlength="40" style="width:200px;" value="%{#session.EditBusiness.email}" cssClass="ui-widget-content ui-corner-all"/>
				 <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Email" /></span>
		    </td>
	        <ffi:cinclude value1="${EditBusiness.ACH_IAT_UseTaxID}" value2="Y" operator="equals">
			    <td class="sectionhead" align="right" valign="top"><s:text name="jsp.user_303"/></td>
			    <td class="columndata" valign="top" align="left">
				<input class="ui-widget-content ui-corner-all" type="text" name="EditBusiness.TaxId" value='<ffi:getProperty name="EditBusiness" property="TaxId" />' size="36" maxlength="10" border="0">
	            <br>(<s:text name="jsp.user_277"/>)
			    </td>
	        </ffi:cinclude>
        </tr>
        <tr>
        	<td colspan="3"><span id="EditBusiness.EmailError"></span></td>
        </tr>
   </table>
 </s:form>
<div align="center">
    <span class="required">* <s:text name="jsp.default_240"/></span>
</div>
<div class="btn-row">
    <ffi:setProperty name="tmpURL" value="/cb/pages/jsp/user/corpadminbusinessedit.jsp?goback=false" URLEncrypt="TRUE" />
	
	<a id="corpadminResetButton" 
		title='<s:text name="jsp.default_358.1" />' 
		href='#' 
		button="true"
		onClick="ns.admin.reset('<ffi:getProperty name='tmpURL'/>','#companyContent')">
		<s:text name="jsp.default_358"/>
	</a>
	
    <ffi:removeProperty name="tmpURL"/>
	<sj:a
		button="true"
		onClickTopics="cancelCompanyProfileForm"
		><s:text name="jsp.default_82"/></sj:a>
		
	<sj:a
		id="saveEditBusiness"
		formIds="editBusinessForm"
		targets="companyContent"
		button="true"
		validate="true"
		validateFunction="customValidation"
		onBeforeTopics="beforeVerify"
		onErrorTopics="errorVerify"
		onSuccessTopics="onSuccessCompanyProfileTopic"
		onCompleteTopics="onCompleteCompanyProfileTopic"
		><s:text name="jsp.default_366"/></sj:a>
</div>
<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
	<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="ModifyBusiness"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="restoredObject" value="y"/>
<ffi:process name="PopulateObjectWithDAValues"/>
<ffi:removeProperty name="DAITEM"/>
