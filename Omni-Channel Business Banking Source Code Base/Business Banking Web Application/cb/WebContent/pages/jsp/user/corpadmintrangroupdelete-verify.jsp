<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:help id="user_corpadmintrangroupdelete-verify" className="moduleHelpClass"/>

<% session.setAttribute("DeleteTransactionGroup_GroupName", request.getParameter("DeleteTransactionGroup_GroupName")); %>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="AddTransactionGroupToDA" />
	<ffi:removeProperty name="DA_TRANSACTION_GROUP" />
	<ffi:object id="AddTransactionGroupToDA" name="com.ffusion.tasks.dualapproval.AddTransactionGroupToDA" />
	<% session.setAttribute("FFIAddTransactionGroupToDA", session.getAttribute("AddTransactionGroupToDA")); %>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
<ffi:object name="com.ffusion.tasks.business.DeleteTransactionGroup" id="DeleteTransactionGroup" scope="session" />
<% session.setAttribute("FFIDeleteTransactionGroup", session.getAttribute("DeleteTransactionGroup")); %>
</ffi:cinclude>

<div align="center">
			<%-- include page header --%>
	<s:form id="verifyTransactionGroupForm" namespace="/pages/user" name="verifyTransactionGroupForm" action="deleteTransactionGroup" validate="false" method="post" theme="simple">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<TABLE class="adminBackground" width="100%" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmintrangroupdelete-verify.jsp-1" parm0="${DeleteTransactionGroup_GroupName}"/></TD>
				<td></td>
			</TR>
			<TR>
				<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15"></TD>
				<td></td>
			</TR>
			<TR>
				<TD align="center">
					<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>
					<sj:a 
						formIds="verifyTransactionGroupForm"
						targets="resultmessage"
						button="true" 
						onSuccessTopics="completeTGDel" 
						onErrorTopics="errorTGAdd"
						effectDuration="1500" 
						><s:text name="jsp.default_162"/></sj:a>
				</TD>
				<td></td>
			</TR>
		</TABLE>
	</s:form>
	<p></p>
</div>
