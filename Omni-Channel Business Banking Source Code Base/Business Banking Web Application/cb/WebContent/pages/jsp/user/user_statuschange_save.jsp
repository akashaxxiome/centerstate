<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<%-- Initialize required common services.--%>
<ffi:object name="com.ffusion.tasks.banking.Initialize" id="UsersEditBankingInitialize" scope="session"/>
<ffi:setProperty name="UsersEditBankingInitialize" property="ClassName" value="com.ffusion.services.banking.BankingServiceImpl"/>
<ffi:setProperty name="UsersEditBankingInitialize" property="InitURL" value="bankingService.xml"/>
<ffi:setProperty name="UsersEditBankingInitialize" property="ServiceName" value="UsersEditBankingService"/>
<ffi:process name="UsersEditBankingInitialize"/>

<ffi:object name="com.ffusion.tasks.banking.SignOn" id="UsersEditBankingSignOn" scope="session"/>
<ffi:setProperty name="UsersEditBankingSignOn" property="ServiceName" value="UsersEditBankingService"/>
<ffi:setProperty name="UsersEditBankingSignOn" property="UserName" value="${TransID}"/>
<ffi:setProperty name="UsersEditBankingSignOn" property="Password" value="${TransPassword}"/>
<ffi:process name="UsersEditBankingSignOn"/>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>

<ffi:object name="com.ffusion.beans.util.IntegerMath" id="Counter" scope="request"/>
<ffi:setProperty name="Counter" property="Value1" value="0"/>
<ffi:setProperty name="Counter" property="Value2" value="1"/>

<ffi:list collection="ModifiedSecondaryUsers" items="TempSecondaryUser">
    <%--
    Filter all the pending transactions collections so that only the transactions created by the secondary user will
    be modified.
    --%>
    <ffi:setProperty name="PaymentPendingStatus" value='<%= "" + com.ffusion.beans.billpay.PaymentStatus.PMS_SCHEDULED %>'/>
    <ffi:setProperty name="RecPayments" property="Filter" value="SUBMITTED_BY==${TempSecondaryUser.Id},STATUS==${PaymentPendingStatus},AND"/>

    <ffi:object name="com.ffusion.tasks.billpay.FindRecPayments" id="FindRecPayments" scope="request"/>
    <ffi:setProperty name="FindRecPayments" property="RemovePending" value="true"/>
    <ffi:process name="FindRecPayments"/>

    <ffi:setProperty name="Payments" property="Filter" value="SUBMITTED_BY==${TempSecondaryUser.Id},STATUS==${PaymentPendingStatus},AND"/>

    <ffi:setProperty name="TransferPendingStatus" value='<%= "" + com.ffusion.beans.banking.TransferStatus.TRS_SCHEDULED %>'/>
    <ffi:setProperty name="RecTransfers" property="Filter" value="SUBMITTED_BY==${TempSecondaryUser.Id},STATUS=${TransferPendingStatus},AND"/>

    <ffi:object name="com.ffusion.tasks.banking.FindRecTransfers" id="FindRecTransfers" scope="request"/>
    <ffi:setProperty name="FindRecTransfers" property="RemovePending" value="true"/>
    <ffi:process name="FindRecTransfers"/>

    <ffi:setProperty name="Transfers" property="Filter" value="SUBMITTED_BY==${TempSecondaryUser.Id},STATUS=${TransferPendingStatus},AND"/>

    <%-- Set the existing user in the session so that it can be used to initialize the appropriate task(s). --%>
    <ffi:object name="com.ffusion.tasks.user.SetUserInSession" id="SetUserInSession" scope="request"/>
    <ffi:setProperty name="SetUserInSession" property="UsersSessionName" value="SecondaryUsers"/>
    <ffi:setProperty name="SetUserInSession" property="UserSessionName" value="OriginalUser"/>
    <ffi:setProperty name="SetUserInSession" property="Id" value="${TempSecondaryUser.Id}"/>
    <ffi:process name="SetUserInSession"/>

    <ffi:setProperty name="TransferOrCancelTransactionsInputName" value='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_TRANSACTIONS_TRANSFER_OR_CANCEL %>'/>
    <ffi:setProperty name="TransferOrCancelTransactionsInputName" value="${TransferOrCancelTransactionsInputName}_${Counter.Value1}"/>

    <%-- Create the task used to hold all the existing user's profile information. --%>
    <ffi:object name="com.ffusion.tasks.multiuser.EditSecondaryUser" id="SecondaryUser" scope="session"/>
    <ffi:setProperty name="SecondaryUser" property="SecondaryUserSessionName" value="OriginalUser"/>
    <ffi:setProperty name="SecondaryUser" property="FieldsToValidate" value="FIRST_NAME,LAST_NAME,USER_NAME"/>
    <ffi:setProperty name="SecondaryUser" property="FieldsToVerify" value="PHONE,EMAIL,ZIPCODE"/>
    <ffi:setProperty name="SecondaryUser" property="SecondaryUsersSessionName" value="NewSecondaryUsers"/>
    <ffi:setProperty name="SecondaryUser" property="BankingServiceName" value="UsersEditBankingService"/>
    <ffi:setProperty name="SecondaryUser" property="RecPaymentsSessionName" value="RecPayments"/>
    <ffi:setProperty name="SecondaryUser" property="PaymentsSessionName" value="Payments"/>
    <ffi:setProperty name="SecondaryUser" property="RecTransfersSessionName" value="RecTransfers"/>
    <ffi:setProperty name="SecondaryUser" property="TransfersSessionName" value="Transfers"/>
    <ffi:setProperty name="SecondaryUser" property="PrimaryUserAccountsSessionName" value="BankingAccounts"/>
    <ffi:setProperty name="SecondaryUser" property="PendingTransactionsActionInputName" value="${TransferOrCancelTransactionsInputName}"/>
    <ffi:setProperty name="SecondaryUser" property="Initialize" value="TRUE"/>
    <ffi:setProperty name="SecondaryUser" property="Process" value="FALSE"/>
    <ffi:process name="SecondaryUser"/>

    <ffi:setProperty name="SecondaryUser" property="Initialize" value="FALSE"/>
    <ffi:setProperty name="SecondaryUser" property="Process" value="TRUE"/>
    <ffi:setProperty name="SecondaryUser" property="AccountStatus" value="${TempSecondaryUser.AccountStatus}"/>
    <ffi:process name="SecondaryUser"/>

    <ffi:setProperty name="Counter" property="Value1" value="${Counter.Add}"/>
</ffi:list>
<ffi:removeProperty name="TempURL"/>
