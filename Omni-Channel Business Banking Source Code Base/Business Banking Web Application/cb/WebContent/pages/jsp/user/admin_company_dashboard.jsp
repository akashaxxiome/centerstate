<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<s:url id="remoteProfile" value="/pages/jsp/user/corpadmininfo.jsp"/>
<s:url id="remoteBaiSettings" value="/pages/jsp/user/baiexportsettings.jsp"/>
<s:url id="remoteAdmin" value="/pages/jsp/user/administrators.jsp"/>
<s:url id="remoteAcctConfig" value="/pages/jsp/user/accountconfiguration.jsp"/>
<s:url id="remoteAcctGrps" value="/pages/jsp/user/corpadminaccountgroups_grid.jsp"/>
<s:url id="remoteTranGrps" value="/pages/jsp/user/corpadmintransactiongroups.jsp"/>
<s:url id="remotePerms" value="/pages/jsp/user/corpadmincompanypermissions.jsp"/>
<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-adminCompany"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_58" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/adminSubmenuDropdown.jsp" />
    	</div>
    	
    	<!-- dashboard items listing for transfers -->
        <div id="dbAdminCompanyProfileSummary" class="dashboardSummaryHolderDiv">
				<sj:a id="profile" href="%{remoteProfile}" targets="companySummaryTabsContent" button="true" onClickTopics="onProfileClick,onSummaryLinkClick" onCompleteTopics="onProfileComplete" cssClass="summaryLabelCls" title="%{getText('jsp.user_404')}">
					<s:text name="jsp.user_404"/>
				</sj:a>
				
				<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
					<ffi:setProperty name="viewOnly" value="false" />
					<ffi:removeProperty name="ViewUserPermissions" />
					<ffi:cinclude value1="${DAItem.status}" value2="Pending Approval">
						<ffi:setProperty name="viewOnly" value="true" />
						<ffi:setProperty name="ViewUserPermissions" value="TRUE" />
					</ffi:cinclude>
					<ffi:cinclude value1="${DAItem.status}" value2="Rejected">
						<ffi:setProperty name="viewOnly" value="true" />
						<ffi:setProperty name="ViewUserPermissions" value="TRUE" />
					</ffi:cinclude>
					<ffi:cinclude value1="${viewOnly}" value2="true">
						<s:url id="CompanyViewURL" value="%{#session.PagesPath}user/corpadminbusinessview.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a 
							href="%{CompanyViewURL}"
							targets="DACompanyProfileViewDialogId"
							button="true" 
							onClickTopics="beforeLoad"
							onSuccessTopics="openDACompanyProfileViewDialog"
							id="viewCompany"
							><s:text name="jsp.default_459"/> <s:text name="jsp.user_404"/></sj:a>			
											
					</ffi:cinclude>
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">

						<s:url id="CompanyEditURL" value="%{#session.PagesPath}user/corpadminbusinessedit.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a 
							id="editCompany"
							href="%{CompanyEditURL}"
							targets="companyContent"
							button="true" 
							onClickTopics="beforeLoad,beforeEditLinkClick"
							onCompleteTopics="completeCompanyProfileLoad"
							><s:text name="jsp.default_178"/> <s:text name="jsp.user_404"/></sj:a>

					</ffi:cinclude>
				</ffi:cinclude>
		</div>
		
		<div id="dbAdminCompanyBAISummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
			<sj:a id="baiExportSettings" href="%{remoteBaiSettings}" targets="companySummaryTabsContent" button="true" onClickTopics="onBAIClick,onSummaryLinkClick" onCompleteTopics="onBAIComplete" cssClass="summaryLabelCls" title="%{getText('jsp.user_404')}">
				<s:text name="jsp.user_405"/>
			</sj:a>
			
			<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
				<ffi:cinclude value1="AddBAIExportSettingsToDA" value2="" operator="notEquals">
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">


						<s:url id="BAIEditURL" value="/pages/jsp/user/baiexportsettingsedit.jsp">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a id="BAIEditLink" href="%{BAIEditURL}" targets="inputDiv" button="true" 
						onClickTopics="beforeLoad,beforeEditLinkClick,beforeEditBAILinkClick" onCompleteTopics="completeCompanyLoad"><s:text name="jsp.default_178"/> <s:text name="jsp.user_405"/></sj:a>

					</ffi:cinclude>
					
					<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
						<s:url id="CompanyBaiViewURL" value="%{#session.PagesPath}user/baiexportsettingsview.jsp">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a 
							id= "editCompanyBAI"
							href="%{CompanyBaiViewURL}"
							targets="DACompanyBaiViewDialogId"
							button="true" 
							onClickTopics="beforeLoad"
							onSuccessTopics="openDABaiViewDialog"
							><s:text name="jsp.default_459"/> <s:text name="jsp.user_405"/></sj:a>
					</ffi:cinclude>
					
				</ffi:cinclude>
				<ffi:cinclude value1="AddBAIExportSettingsToDA" value2="" operator="equals">
					<a href="<ffi:getProperty name="SecurePath" />user/baiexportsettingsedit.jsp"><img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="<s:text name="jsp.default_179"/>" border="0" hspace="3"></a>
				</ffi:cinclude>
			</ffi:cinclude>
		</div>
		
		<div id="dbAdminCompanyAdministratorSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
			<sj:a id="administrators" href="%{remoteAdmin}" targets="companySummaryTabsContent" button="true" onClickTopics="onAdministratorsClick,onSummaryLinkClick" onCompleteTopics="onAdministratorsComplete" cssClass="summaryLabelCls" title="%{getText('jsp.user_404')}">
				<s:text name="jsp.user_36"/>
			</sj:a>
				<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
				<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
					<s:url id="AdministratorsEditURL" value="%{#session.PagesPath}user/corpadminadminedit.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
						<s:param name="AdminEditType" value="%{'Company'}"/>
					</s:url>
					<sj:a
						id="adminitratorsEditLink"
						href="%{AdministratorsEditURL}" 
						targets="editAdminerDialogId" 
						button="true" 
					    onClickTopics="beforeLoad"
                                    onCompleteTopics="openEditAdminerDialog"
					><s:text name="jsp.default_178"/> <s:text name="jsp.user_36"/>
					</sj:a>
				</ffi:cinclude>
				<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
				<%--
				<s:url id="AdministratorsEditURL" value="/cb/jsp/user/corpadminadminview.jsp">
				--%>
					<s:url id="AdministratorsViewURL" value="%{#session.PagesPath}user/corpadminadminview.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="AdminEditType" value="%{'Company'}"/>
					</s:url>
					<sj:a id="AdminitratorsViewLink" 
						href="%{AdministratorsViewURL}" 
						targets="editAdminerDialogId" 
						button="true" 
					    onClickTopics="beforeLoad,beforeOpenEditAdminerDialog" onCompleteTopics="viewAdminDialogSuccessTopics"><s:text name="jsp.default_459"/>&nbsp;<s:text name="jsp.user_36"/></sj:a>
				</ffi:cinclude>
				</ffi:cinclude>
		</div>
		
		<div id="dbAdminCompanyAccConfSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
			<sj:a id="accountConfiguration" href="%{remoteAcctConfig}" targets="companySummaryTabsContent" button="true" onClickTopics="onAccConfClick,onSummaryLinkClick" onCompleteTopics="onAccConfComplete" cssClass="summaryLabelCls" title="%{getText('jsp.user_404')}">
				<s:text name="jsp.user_406"/>
			</sj:a>
		</div>
		
		<div id="dbAdminCompanyAccGrpSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
			<sj:a id="accountGroups" href="%{remoteAcctGrps}" targets="companySummaryTabsContent" button="true" onClickTopics="onCompanyAccGrpClick,onSummaryLinkClick" onCompleteTopics="onCompanyAccGrpComplete" cssClass="summaryLabelCls" title="%{getText('jsp.user_404')}">
				<s:text name="jsp.default_17"/>
			</sj:a>
		</div>
		
		<div id="dbAdminCompanyTransactionGrpSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
			<sj:a id="tranGroups" href="%{remoteTranGrps}" targets="companySummaryTabsContent" button="true" onClickTopics="onAdminCompanyTransactionGrpClick,onSummaryLinkClick" onCompleteTopics="onAdminCompanyTransactionGrpComplete" cssClass="summaryLabelCls" title="%{getText('jsp.user_404')}">
				<s:text name="jsp.user_407"/>
			</sj:a>
			<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
				<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
					<s:url id="addTransactionGroupURL" value="%{#session.PagesPath}user/corpadmintrangroupaddedit.jsp" escapeAmp="false">
						<s:param name="addFlag" value="%{'true'}" />
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
					</s:url>
					<sj:a
						id="addTransactionGroupBtn"
						href="%{addTransactionGroupURL}"
						targets="companyContent"
						onClickTopics="beforeLoad,beforeEditLinkClick,beforeAddTransGrpClick" onCompleteTopics="completeCompanyProfileLoad" onErrorTopics="errorLoad"
						button="true"><s:text name="jsp.user_18"/></sj:a>
				</ffi:cinclude>
			</ffi:cinclude>
		</div>
		
		
		<div id="dbAdminCompanyPermissionsSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
			<sj:a id="permissions" href="%{remotePerms}" targets="companySummaryTabsContent" button="true" onClickTopics="onAdminCompanyPermClick" onCompleteTopics="onAdminCompanyPermissionsComplete" cssClass="summaryLabelCls" title="%{getText('jsp.user_404')}">
				<s:text name="jsp.common_119"/>
			</sj:a>
		</div>
		
		<!-- More list option listing -->
		<div class="dashboardSubmenuItemCls">
			<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
		
			<!-- UL for rendering tabs -->
			<ul id="dashboardSubmenuItemListId" class="dashboardSubmenuItemList">
				<li class="dashboardSubmenuItem"><a id="showdbAdminCompanyProfileSummary" onclick="ns.common.refreshDashboard('showdbAdminCompanyProfileSummary',true)"><span><s:text name="jsp.user_404"/></span></a></li>
			
				<li class="dashboardSubmenuItem"><a id="showdbAdminCompanyBAISummary" onclick="ns.common.refreshDashboard('showdbAdminCompanyBAISummary',true)"><span><s:text name="jsp.user_405"/></span></a></li>
				
				<li class="dashboardSubmenuItem"><a id="showdbAdminCompanyAdministratorSummary" onclick="ns.common.refreshDashboard('showdbAdminCompanyAdministratorSummary',true)"><span><s:text name="jsp.user_36"/></span></a></li>
				<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
					<li class="dashboardSubmenuItem"><a id="showdbAdminCompanyAccConfSummary" onclick="ns.common.refreshDashboard('showdbAdminCompanyAccConfSummary',true)"><span><s:text name="jsp.user_406"/></span></a></li>
				</ffi:cinclude>
				<li class="dashboardSubmenuItem"><a id="showdbAdminCompanyAccGrpSummary" onclick="ns.common.refreshDashboard('showdbAdminCompanyAccGrpSummary',true)"><span><s:text name="jsp.default_17"/></span></a></li>
				<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
					<li class="dashboardSubmenuItem"><a id="showdbAdminCompanyTransactionGrpSummary" onclick="ns.common.refreshDashboard('showdbAdminCompanyTransactionGrpSummary',true)"><span><s:text name="jsp.user_407"/></span></a></li>
				</ffi:cinclude>
				<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
					<li class="dashboardSubmenuItem"><a id="showdbAdminCompanyPermissionsSummary" onclick="ns.common.refreshDashboard('showdbAdminCompanyPermissionsSummary',true)"><span><s:text name="jsp.common_119"/></span></a></li>
				</ffi:cinclude>
			</ul>
		</div>
</div>
<script>    
	$("#profile").find("span").addClass("dashboardSelectedItem");
</script>

