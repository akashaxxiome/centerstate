<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>



<ffi:setProperty name="PageTitle" value="Manage Users"/>
<ffi:setProperty name="topMenu" value="home"/>
<ffi:setProperty name="subMenu" value="preferences"/>
<ffi:setProperty name="sub2Menu" value="manageusers"/>

<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_SETUP %>'>

<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_CORE_ACCOUNT %>' startsWith="true"/>
<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_REGISTER_ACCOUNT %>' startsWith="true"/>
<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_STATEMENT_ACCOUNT %>' startsWith="true"/>
<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_ACCOUNT %>' startsWith="true"/>
<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_REGISTER_ACCOUNT %>' startsWith="true"/>

<%-- Set variables to display records in alternating colors. --%>
<ffi:setProperty name="pband1" value="class=\"tabledata_white\""/>
<ffi:setProperty name="pband2" value="class=\"tabledata_gray\""/>

    
    <script type="text/javascript">
    <!--
    <%--
    /**
     * This function enables/disables the account register and online statement checkboxes for a given core account
     * depending on the state of the account's corresponding active checkbox. If the active checkbox is checked, then
     * the account register and online statement checkboxes are enabled. Otherwise, they are cleared and disabled.
     *
     * @param ordinal the position of the row on the page
     */
    --%>
    function toggleRegisterAndStatementCheckboxes(ordinal) {
        enableCheckboxes = document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_CORE_ACCOUNT %>' + ordinal].checked;

        if (document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_REGISTER_ACCOUNT %>' + ordinal]) {
            if (enableCheckboxes) {
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_REGISTER_ACCOUNT %>' + ordinal].disabled = false;
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_REGISTER_ACCOUNT %>' + ordinal].checked = false;
            } else {
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_REGISTER_ACCOUNT %>' + ordinal].disabled = true;
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_REGISTER_ACCOUNT %>' + ordinal].checked = false;
            }
        }

        if (document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_STATEMENT_ACCOUNT %>' + ordinal]) {
            if (enableCheckboxes) {
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_STATEMENT_ACCOUNT %>' + ordinal].disabled = false;
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_STATEMENT_ACCOUNT %>' + ordinal].checked = false;
            } else {
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_STATEMENT_ACCOUNT %>' + ordinal].disabled = true;
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_STATEMENT_ACCOUNT %>' + ordinal].checked = false;
            }
        }
    }

    <%--
    /**
     * This function enables/disables the account register checkbox for a given external account depending on the state
     * of the account's corresponding active checkbox. If the active checkbox is checked, then the account register
     * checkbox is enabled. Otherwise, it is cleared and disabled.
     *
     * @param ordinal the position of the row on the page
     */
    --%>
    function toggleRegisterCheckboxesForExternalAccounts(ordinal) {
        enableCheckboxes = document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_ACCOUNT %>' + ordinal].checked;

        if (document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_REGISTER_ACCOUNT %>' + ordinal]) {
            if (enableCheckboxes) {
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_REGISTER_ACCOUNT %>' + ordinal].disabled = false;
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_REGISTER_ACCOUNT %>' + ordinal].checked = false;
            } else {
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_REGISTER_ACCOUNT %>' + ordinal].disabled = true;
                document.UserAccountsForm.elements['<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_REGISTER_ACCOUNT %>' + ordinal].checked = false;
            }
        }
    }
    //-->
	function submitBackButton(url) {
		document.UserAccountsForm.action=url;
		document.UserAccountsForm.submit();
	}
    </script>

<div class="marginTop10" style="position:relative; overflow:hidden">
<ffi:help id="user_user_addedit_accounts" className="moduleHelpClass"/>
<p class="transactionHeading"><ffi:getProperty name="FFIEditSecondaryUser" property="FullNameWithLoginId"/></p>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header"><s:text name="user.accountsInformation"/></div>
   		<div class="paneContentWrapper">
   			<div class="instructions">
            		<s:text name="secondaryUser.accountsTab.instructions"/>
            </div>
   		</div>
   	</div>
</div>
<div id="formerrors"></div>
<form name="UserAccountsForm" id="userAccountsForm" action="/cb/pages/jsp/user/saveSecondaryUserAccounts.action" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="marginTop20"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="paneContentWrapper">
   		<div class="nameSubTitle"><s:text name="user.fusionBankAccounts"/></div>
		    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData tdTopBottomPadding5 marginTop10">
		        <tr class="header">
		            <td><div align="center">Active</div></td>
		            <td><ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_REGISTER %>'><div align="center">Register</div></ffi:cinclude></td>
		            <td><ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ONLINE_STATEMENT %>'><div align="center">Statement</div></ffi:cinclude></td>
		            <td></td>
		            <td><s:text name="user.account"/></td>
		            <td><s:text name="jsp.default_294"/></td>
		            <td><s:text name="jsp.default_20"/></td>
		            <td></td>
		        </tr>
		<ffi:object name="com.ffusion.beans.util.IntegerMath" id="Calculator" scope="request"/>
		<ffi:setProperty name="Calculator" property="Value1" value="3"/>
		<ffi:setProperty name="Calculator" property="Value2" value="1"/>
		
		<ffi:object name="com.ffusion.beans.util.IntegerMath" id="Counter" scope="request"/>
		<ffi:setProperty name="Counter" property="Value1" value="0"/>
		<ffi:setProperty name="Counter" property="Value2" value="1"/>
		
		<ffi:setProperty name="BankingAccounts" property="Filter" value="COREACCOUNT=1,HIDE!1,AND"/>
		<ffi:setProperty name="BankingAccounts" property="FilterSortedBy" value="NICKNAME,ID"/>
		<ffi:setProperty name="SecondaryUserEntitledCoreAccounts" property="Filter" value="All"/>
		
		<ffi:list collection="BankingAccounts" items="CoreAccount">
		    <ffi:setProperty name="IsSecondaryUserCoreAccount" value=""/>
		    <ffi:setProperty name="IsPrimaryUserRegisterAccount" value=""/>
		    <ffi:setProperty name="IsSecondaryUserRegisterAccount" value=""/>
		    <ffi:setProperty name="IsPrimaryUserStatementAccount" value=""/>
		    <ffi:setProperty name="IsSecondaryUserStatementAccount" value=""/>
		
		    <%-- Determine if the secondary user is entitled to the current account. --%>
		    <ffi:list collection="SecondaryUserEntitledCoreAccounts" items="TempSecondaryUserCoreAccount">
		        <ffi:cinclude value1="${TempSecondaryUserCoreAccount.ID}" value2="${CoreAccount.ID}" operator="equals">
		            <ffi:cinclude value1="${TempSecondaryUserCoreAccount.RoutingNum}" value2="${CoreAccount.RoutingNum}" operator="equals">
		                <ffi:cinclude value1="${TempSecondaryUserCoreAccount.BankID}" value2="${CoreAccount.BankID}" operator="equals">
		                    <ffi:setProperty name="IsSecondaryUserCoreAccount" value="true"/>
		                </ffi:cinclude>
		            </ffi:cinclude>
		        </ffi:cinclude>
		    </ffi:list>
		
		    <%-- Determine if the current account is enabled for account register for the primary user. --%>
		    <ffi:cinclude value1="${CoreAccount.REG_ENABLED}" value2="true" operator="equals">
		        <ffi:setProperty name="IsPrimaryUserRegisterAccount" value="true"/>
		    </ffi:cinclude>
		
		    <%-- Determine if the current account is enabled for account register for the secondary user. --%>
		    <ffi:list collection="SecondaryUserEntitledRegisterAccounts" items="TempSecondaryUserRegisterAccount">
		        <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.ID}" value2="${CoreAccount.ID}" operator="equals">
		            <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.RoutingNum}" value2="${CoreAccount.RoutingNum}" operator="equals">
		                <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.BankID}" value2="${CoreAccount.BankID}" operator="equals">
		                    <ffi:setProperty name="IsSecondaryUserRegisterAccount" value="true"/>
		                </ffi:cinclude>
		            </ffi:cinclude>
		        </ffi:cinclude>
		    </ffi:list>
		
		    <%-- Determine if the current account is enabled for online statements for the primary user. --%>
		    <ffi:list collection="PrimaryUserStatementEnabledAccounts" items="TempPrimaryUserStatementAccount">
		        <ffi:cinclude value1="${TempPrimaryUserStatementAccount.ID}" value2="${CoreAccount.ID}" operator="equals">
		            <ffi:cinclude value1="${TempPrimaryUserStatementAccount.RoutingNum}" value2="${CoreAccount.RoutingNum}" operator="equals">
		                <ffi:cinclude value1="${TempPrimaryUserStatementAccount.BankID}" value2="${CoreAccount.BankID}" operator="equals">
		                    <ffi:setProperty name="IsPrimaryUserStatementAccount" value="true"/>
		                </ffi:cinclude>
		            </ffi:cinclude>
		        </ffi:cinclude>
		    </ffi:list>
		
		    <%-- Determine if the current account is enabled for online statements for the secondary user. --%>
		    <ffi:list collection="SecondaryUserEntitledStatementAccounts" items="TempSecondaryUserStatementAccount">
		        <ffi:cinclude value1="${TempSecondaryUserStatementAccount.ID}" value2="${CoreAccount.ID}" operator="equals">
		            <ffi:cinclude value1="${TempSecondaryUserStatementAccount.RoutingNum}" value2="${CoreAccount.RoutingNum}" operator="equals">
		                <ffi:cinclude value1="${TempSecondaryUserStatementAccount.BankID}" value2="${CoreAccount.BankID}" operator="equals">
		                    <ffi:setProperty name="IsSecondaryUserStatementAccount" value="true"/>
		                </ffi:cinclude>
		            </ffi:cinclude>
		        </ffi:cinclude>
		    </ffi:list>
		
		    <%-- Reset various flags depending on whether or not the current account is accesible to the secondary user. --%>
		    <ffi:cinclude value1="${IsSecondaryUserCoreAccount}" value2="" operator="equals">
		        <ffi:setProperty name="IsSecondaryUserRegisterAccount" value=""/>
		        <ffi:setProperty name="IsSecondaryUserStatementAccount" value=""/>
		    </ffi:cinclude>
		
		        <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
		            <td><div align="center"><input name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_CORE_ACCOUNT %><ffi:getProperty name="Counter" property="Value1"/>' type="checkbox" onclick="toggleRegisterAndStatementCheckboxes('<ffi:getProperty name="Counter" property="Value1"/>');" <ffi:cinclude value1="${IsSecondaryUserCoreAccount}" value2="true" operator="equals">checked</ffi:cinclude>></div></td>
		            <td><ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_REGISTER %>'><ffi:cinclude value1="${IsPrimaryUserRegisterAccount}" value2="true" operator="equals"><div align="center"><input name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_REGISTER_ACCOUNT %><ffi:getProperty name="Counter" property="Value1"/>' type="checkbox" <ffi:cinclude value1="${IsSecondaryUserRegisterAccount}" value2="true" operator="equals">checked</ffi:cinclude> <ffi:cinclude value1="${IsSecondaryUserCoreAccount}" value2="" operator="equals">disabled</ffi:cinclude>></div></ffi:cinclude></ffi:cinclude></td>
		            <td><ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ONLINE_STATEMENT %>'><ffi:cinclude value1="${IsPrimaryUserStatementAccount}" value2="true" operator="equals"><div align="center"><input name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_STATEMENT_ACCOUNT %><ffi:getProperty name="Counter" property="Value1"/>' type="checkbox" <ffi:cinclude value1="${IsSecondaryUserStatementAccount}" value2="true" operator="equals">checked</ffi:cinclude> <ffi:cinclude value1="${IsSecondaryUserCoreAccount}" value2="" operator="equals">disabled</ffi:cinclude>></div></ffi:cinclude></ffi:cinclude></td>
		            <td></td>
		            <td style="padding-left: 0px;"><ffi:getProperty name="CoreAccount" property="NumberMasked"/></td>
		            <td style="padding-left: 0px;"><ffi:getProperty name="CoreAccount" property="NickName"/></td>
		            <td style="padding-left: 0px;"><ffi:getProperty name="CoreAccount" property="Type"/></td>
		            <td></td>
		        </tr>
		    <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
		    <ffi:setProperty name="Counter" property="Value1" value="${Counter.Add}"/>
		</ffi:list>
		
		<ffi:setProperty name="ExternalAccounts" property="Filter" value="COREACCOUNT=0,HIDE!1,AND"/>
		<ffi:setProperty name="ExternalAccounts" property="FilterSortedBy" value="ConsumerDisplayText,ID"/>
		<ffi:setProperty name="SecondaryUserEntitledExternalAccounts" property="Filter" value="All"/>
		
		<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_AGGREGATION %>'>
		    <ffi:cinclude value1="${ExternalAccounts.Size}" value2="0" operator="notEquals">
		    </table></div></div></div>
<div class="marginTop20"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
  		<div class="paneContentWrapper">
   			<div class="nameSubTitle"><s:text name="user.externalAccounts"/></div>
	   		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tableData tdTopBottomPadding5 marginTop10">
	        <tr class="header">
	            <td><div align="center"><s:text name="jsp.default_28"/></div></td>
	            <td><ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_REGISTER %>'><div align="center">Register</div></ffi:cinclude></td>
	            <td><s:text name="user.institution"/></td>
	            <td><s:text name="jsp.default_16"/></td>
	            <td><s:text name="jsp.default_294"/></td>
	            <td><s:text name="jsp.default_20"/></td>
	            <td></td>
	        </tr>
	        <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Calculator" scope="request"/>
	        <ffi:setProperty name="Calculator" property="Value1" value="3"/>
	        <ffi:setProperty name="Calculator" property="Value2" value="1"/>
	
	        <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Counter" scope="request"/>
	        <ffi:setProperty name="Counter" property="Value1" value="0"/>
	        <ffi:setProperty name="Counter" property="Value2" value="1"/>
	
	        <ffi:list collection="ExternalAccounts" items="ExternalAccount">
	            <ffi:setProperty name="IsSecondaryUserExternalAccount" value=""/>
	            <ffi:setProperty name="IsPrimaryUserRegisterAccount" value=""/>
	            <ffi:setProperty name="IsSecondaryUserRegisterAccount" value=""/>
	
	            <%-- Determine if the secondary user is entitled to the current account. --%>
	            <ffi:list collection="SecondaryUserEntitledExternalAccounts" items="TempSecondaryUserExternalAccount">
	                <ffi:cinclude value1="${TempSecondaryUserExternalAccount.ID}" value2="${ExternalAccount.ID}" operator="equals">
	                    <ffi:cinclude value1="${TempSecondaryUserExternalAccount.RoutingNumber}" value2="${ExternalAccount.RoutingNumber}" operator="equals">
	                        <ffi:cinclude value1="${TempSecondaryUserExternalAccount.BankId}" value2="${ExternalAccount.BankId}" operator="equals">
	                            <ffi:setProperty name="IsSecondaryUserExternalAccount" value="true"/>
	                        </ffi:cinclude>
	                    </ffi:cinclude>
	                </ffi:cinclude>
	            </ffi:list>
	
	            <%-- Determine if the current account is enabled for account register for the primary user. --%>
	            <ffi:cinclude value1="${ExternalAccount.REG_ENABLED}" value2="true" operator="equals">
	                <ffi:setProperty name="IsPrimaryUserRegisterAccount" value="true"/>
	            </ffi:cinclude>
	
	            <%-- Determine if the current account is enabled for account register for the secondary user. --%>
	            <ffi:list collection="SecondaryUserEntitledRegisterAccounts" items="TempSecondaryUserRegisterAccount">
	                <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.ID}" value2="${ExternalAccount.ID}" operator="equals">
	                    <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.RoutingNum}" value2="${ExternalAccount.RoutingNum}" operator="equals">
	                        <ffi:cinclude value1="${TempSecondaryUserRegisterAccount.BankID}" value2="${ExternalAccount.BankID}" operator="equals">
	                            <ffi:setProperty name="IsSecondaryUserRegisterAccount" value="true"/>
	                        </ffi:cinclude>
	                    </ffi:cinclude>
	                </ffi:cinclude>
	            </ffi:list>
	
	            <%-- Reset various flags depending on whether or not the current account is accesible to the secondary user. --%>
	            <ffi:cinclude value1="${IsSecondaryUserExternalAccount}" value2="" operator="equals">
	                <ffi:setProperty name="IsSecondaryUserRegisterAccount" value=""/>
	            </ffi:cinclude>
	
	        <tr <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
	            <td><div align="center"><input name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_ACCOUNT %><ffi:getProperty name="Counter" property="Value1"/>' type="checkbox" onclick="toggleRegisterCheckboxesForExternalAccounts('<ffi:getProperty name="Counter" property="Value1"/>');" <ffi:cinclude value1="${IsSecondaryUserExternalAccount}" value2="true" operator="equals">checked</ffi:cinclude>></div></td>
	            <td><ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_REGISTER %>'><ffi:cinclude value1="${IsPrimaryUserRegisterAccount}" value2="true" operator="equals"><div align="center"><input name='<%= com.ffusion.tasks.multiuser.MultiUserTask.ENABLE_EXTERNAL_REGISTER_ACCOUNT %><ffi:getProperty name="Counter" property="Value1"/>' type="checkbox" <ffi:cinclude value1="${IsSecondaryUserRegisterAccount}" value2="true" operator="equals">checked</ffi:cinclude> <ffi:cinclude value1="${IsSecondaryUserExternalAccount}" value2="" operator="equals">disabled</ffi:cinclude>></div></ffi:cinclude></ffi:cinclude></td>
	            <td style="padding-left: 0px;"><ffi:getProperty name="ExternalAccount" property="BankName"/></td>
	            <td style="padding-left: 0px;"><ffi:getProperty name="ExternalAccount" property="NumberMasked"/></td>
	            <td style="padding-left: 0px;"><ffi:getProperty name="ExternalAccount" property="NickName"/></td>
	            <td style="padding-left: 0px;"><ffi:getProperty name="ExternalAccount" property="Type"/></td>
	            <td></td>
	        </tr>
	            <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
	            <ffi:setProperty name="Counter" property="Value1" value="${Counter.Add}"/>
	        </ffi:list>
	    </ffi:cinclude>
	</ffi:cinclude>
	    </table></div></div></div>
    <div class="btn-row">    
    	<sj:a id="secUserAccountsCancel" 
    	button="true" 
		summaryDivId="summary" 
		buttonType="cancel" 
		onClickTopics="showSummary,secondaryUsers.cancel">
    	<s:text name="jsp.default_82"/></sj:a>        
		<sj:a id="secUserAccountsBack" button="true" buttontype="back" backstep="0" ><s:text name="jsp.default_57"/></sj:a>        
		<sj:a id="secUserAccountsNext" button="true" formIds="userAccountsForm" targets="targetDiv" onSuccessTopics="secondaryUsers.saveAccountsSuccess"> <s:text name="jsp.default_291"/> </sj:a>        
    </div>
</form>

</div>
</body>
</html>

<ffi:removeProperty name="IsPrimaryUserRegisterAccount"/>
<ffi:removeProperty name="IsPrimaryUserStatementAccount"/>
<ffi:removeProperty name="IsSecondaryUserCoreAccount"/>
<ffi:removeProperty name="IsSecondaryUserExternalAccount"/>
<ffi:removeProperty name="IsSecondaryUserRegisterAccount"/>
<ffi:removeProperty name="IsSecondaryUserStatementAccount"/>
<ffi:removeProperty name="TempURL"/>
<ffi:removeProperty name="CancelButton"/>
<ffi:removeProperty name="BackButton"/>
<ffi:removeProperty name="NextButton"/>

<ffi:setProperty name="BackURL" value="${SecurePath}user/user_addedit_accounts.jsp" URLEncrypt="true"/>

</ffi:cinclude>