<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:help id="user_corpadminlocdelete-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_375')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<%
	String locationName = request.getParameter("DeleteLocation_LocationName");
	if(locationName!= null){
		session.setAttribute("DeleteLocation_LocationName", locationName);
	}

	String locationBPWID = request.getParameter("DeleteLocation_LocationBPWID");
	if(locationBPWID!= null){
		session.setAttribute("DeleteLocation_LocationBPWID", locationBPWID);
	}
	
	String childSequence = request.getParameter("childSequence");
	if(childSequence != null) {
		session.setAttribute("childSequence", childSequence);
	}
	
%>

<ffi:setProperty name="DeleteLocation_LocationBPWID" value="<%=locationBPWID%>"/>
<ffi:setProperty name='PageHeadingForLocation' value='Delete Location'/>
<script>
	ns.admin.PageHeadingForLocation="<ffi:getProperty name='PageHeadingForLocation' />";
</script>
<div id="locationwholeworld">
		<div align="center">
		<TABLE class="adminBackground" width="480" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminlocdelete-verify.jsp-1" parm0="<%=locationName%>"/></TD>
							<td></td>
						</TR>
						<TR>
							<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15"></TD>
							<td></td>
						</TR>
						<TR>
							<TD height="30">&nbsp;</TD>
						</TR>
						<TR>
							<TD>
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a id="cancelDeleteLocLink" button="true" onClickTopics="closeDialog"
									  title="%{getText('jsp.default_83')}" 
									  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_82" /></sj:a>
								<s:url id="deleteLocationUrl" value="/pages/jsp/user/corpadminlocdelete-confirm.jsp">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								</s:url>
								<sj:a id="deleteLocationLink" href="%{deleteLocationUrl}"
									  targets="deleteLocationDialogID"
									  button="true" 
									  title="%{getText('jsp.user_396')}"
									  effectDuration="1500"
									  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_162" /></sj:a>

							</div>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
			<p></p>
		</div>
</div>
