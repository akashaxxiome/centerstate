<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<span id="pageHeading" style="display:none">Account Group</span>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
		<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
		<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
		<ffi:process name="ThrowException"/>
</ffi:cinclude>

<div id="corpadminaccountgroups_gridDiv" class="corpadminaccountgroups_gridDiv">
<ffi:help id="user_corpadminaccountgroups_grid" className="moduleHelpClass"/>
<s:include value="inc/corpadminaccountgroups-pre.jsp" />

	<ffi:setProperty name="corpAdminAccountGroupsTempURL" value="/pages/user/getAccountGroups.action?collectionName=AccountGroups" URLEncrypt="true"/>
    <s:url id="accountGroupsUrl" value="%{#session.corpAdminAccountGroupsTempURL}"/>
	<s:url id="accountUrl" value="/pages/user/getAccountsByGroup.action" />
	<sjg:grid  
		id="accountGroupsGridId"  
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{accountGroupsUrl}"  
		pager="true" 
		viewrecords="true"	
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		scroll="false"
		altRows="true"
		navigator="false"
		navigatorSearch="false"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		onGridCompleteTopics="completeAccountGroupsGridLoad"
		> 
			<sjg:grid
				id="accountSubgrid"
				subGridUrl="%{accountUrl}"
				gridModel="gridModel"
				pager="true"
				viewrecords="true"	
				rowList="4,8,10"
				rowNum="10"
				rownumbers="false"
				shrinkToFit="true"
				scroll="false"
				altRows="true"
				navigator="true"
				navigatorAdd="false"
				navigatorDelete="false"
				navigatorEdit="false"
				navigatorRefresh="true"
				>
				<sjg:gridColumn name="bankName" index="bankName" title="%{getText('jsp.default_61')}" sortable="false" width="100"/>
				<sjg:gridColumn name="routingNum" index="rountingNum" title="%{getText('jsp.default_365')}"  width="100" sortable="false"/>
				<sjg:gridColumn name="displayText" index="accountDisplayText" title="%{getText('jsp.default_16')}" formatter="formatAccountColumn" sortable="false" width="130"/>
				<sjg:gridColumn name="type" index="type" title="%{getText('jsp.default_444')}" sortable="false" width="90"/>
				<sjg:gridColumn name="currencyCode" index="accountCurrencyCode" title="%{getText('jsp.default_125')}" width="80" sortable="false"/>
				<sjg:gridColumn name="nickName" index="accountNickName" title="%{getText('jsp.default_293')}" sortable="false" width="150"/>
			</sjg:grid>
		<sjg:gridColumn name="name" index="groupName" title="%{getText('jsp.default_17')}" sortable="false" width="750"/>

	</sjg:grid>
</div>

<script>
$(document).ready(function () {
	$("#accountSubgrid").data("supportSearch",false);
	
	if($("#accountGroupsGridId tbody").data("ui-jqgrid") != undefined){
		$("#accountGroupsGridId tbody").sortable("destroy");
	}
	if($("#accountSubgrid tbody").data("ui-jqgrid") != undefined){
		$("#accountSubgrid tbody").sortable("destroy");
	}
});
</script>