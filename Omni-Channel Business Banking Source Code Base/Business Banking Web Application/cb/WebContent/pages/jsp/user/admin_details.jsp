<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="detailsPortlet" class="portlet wizardSupportCls ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	<div class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title"><s:text name="jsp.user_72"/></span>
	</div>
	<div class="portlet-content">
       <sj:tabbedpanel id="crudTabs" >

            <sj:tab id="inputTab" target="inputDiv" label="%{getText('jsp.default_390')}"/>
            <sj:tab id="verifyTab" target="verifyDiv" label="%{getText('jsp.default_392')}"/>
            <sj:tab id="confirmTab" target="confirmDiv" label="%{getText('jsp.default_393')}"/>
            <div id="inputDiv" style="border:1px;overflow:auto;">
                <s:text name="jsp.user_149"/>
                <br><br>
            </div>
           <div id="verifyDiv" style="border:1px">
                <s:text name="jsp.default_578"/>
                <br><br>
            </div>
            <div id="confirmDiv" style="border:1px">
                <s:text name="jsp.default_555"/>
                <br><br>
            </div>
        </sj:tabbedpanel> 
    </div>
    <div id="adminDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
			  <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>
<script type="text/javascript">

	/* $('#detailsPortlet').portlet({
		close: true,
		bookmark: true,
		closeCallback: function(){
			$('#details').slideUp();
		},
		helpCallback: function(){
			
			var helpFile = $('#detailsPortlet').find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		},
		bookMarkCallback: function(){
			var path = $('#detailsPortlet').find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.shortcutPathClass').html();
			var ent  = $('#detailsPortlet').find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.shortcutEntitlementClass').html();
			ns.shortcut.openShortcutWindow( path, ent );
		}
	}); */
	
	ns.common.initializePortlet("detailsPortlet");
	
</script>

<%-- Admin Permission Account Group View dialog --%>
<sj:dialog 
	id="viewPermAcctGrpDialog"
	buttons="{ 
    		'%{getText(\"jsp.default_102\")}':function() { ns.common.closeDialog(); }
    		}"
	cssClass="adminDialog" 
	title="%{getText('jsp.user_5')}"
	modal="true"
	resizable="false"
	autoOpen="false"
	closeOnEscape="true"
	showEffect="fold"
	hideEffect="clip"
	width="1200"   
	height="auto"  
	cssStyle="overflow-y:hidden;">
</sj:dialog>