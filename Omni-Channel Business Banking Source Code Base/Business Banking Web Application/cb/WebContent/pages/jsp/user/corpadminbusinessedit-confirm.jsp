<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_corpadminbusinessedit-confirm" className="moduleHelpClass"/>
<s:include value="inc/corpadminbusinessedit-confirm-pre.jsp"/>
<div align="center">
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>"  operator="notEquals">
		<s:text name="jsp.user_59"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<s:text name="jsp.user_60"/>
	</ffi:cinclude>
</div>
<div class="btn-row">
	<sj:a button="true" onClickTopics="completeCompanyProfileSubmit"><s:text name="jsp.default_175"/></sj:a>
</div>
<ffi:removeProperty name="GetStatesForCountry"/>
<ffi:removeProperty name="checkCompanyProfileState"/>
