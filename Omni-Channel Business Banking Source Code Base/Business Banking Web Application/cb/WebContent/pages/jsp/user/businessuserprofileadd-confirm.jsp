<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminprofileadd-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_93')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>


<div align="center" class="marginTop20">

	<div align="center"><s:property value="BusinessEmployee.UserName"/>&nbsp<s:text name="jsp.profile.label.confirm.add"/></div>
	<div class="btn-row">
			<sj:a button="true" onClickTopics="cancelProfileForm,refreshProfileGrid"><s:text name="jsp.default_175"/></sj:a>
	</div>
</div>

<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadminprofiles-pre.jsp"/>
