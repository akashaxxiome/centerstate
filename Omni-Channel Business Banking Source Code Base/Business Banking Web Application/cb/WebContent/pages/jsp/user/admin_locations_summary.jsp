<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<s:include value="inc/corpadmindiv-pre.jsp"/>


<%-- The following div tempDivIDForMethodNsAdminReloadDivisions is used to hold temparory variables of division grids.--%>
<div id="tempDivIDForMethodNsAdminReloadDivisions" style="display: none;"></div>

<%-- Pending Approval Company island start --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
  <div id="admin_pendingDivisionsTab" class="portlet hidden">
	  <div class="portlet-header">
	  		<span class="portlet-title"><s:text name="jsp.default.label.pending.approval.changes"/></span>
			<%-- <div class="searchHeaderCls toggleClick expandArrow">
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<a id="adminDivisionPendingApprovalLink" href="javascript:void(0);" class="nameTitle"><s:text name="jsp.default.label.pending.approval.changes"/></a>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-negative floatRight"></span>
				</div>
			</div> --%>
	    </div>
	    
	    <div class="portlet-content">
	    	<!-- <div class="toggleBlock marginTop10"> -->
				<s:include value="/pages/jsp/user/inc/da-division-island-grid.jsp"/>
			<!-- </div> -->
		</div>
		
		<div id="adminDivisionSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('admin_pendingDivisionsTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
		<!-- <div class="clearBoth portletClearnace hidden" style="padding: 10px 0"></div>	 -->
  </div>
  <div class="clearBoth" id="divisionBlankDiv" style="padding: 10px 0"></div>
</ffi:cinclude>
<%-- Pending Approval Company island end --%>


<div id="locationSummaryTabs" class="portlet" style="float: left; width: 100%;">
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="jsp.default_563"/></span>
	</div>
	<div class="portlet-content" id="locationSummaryTabsContent">
			<s:include value="/pages/jsp/user/corpadminlocations.jsp" />
	</div>
	<div id="divisionSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('locationSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>
<input type="hidden" id="dashBoardDivisionIdNew" value="<ffi:getProperty name='EditDivision_GroupId'/>"/>
<input type="hidden" id="dashBoardDivisionNameNew" value="<ffi:getProperty name='EditGroup_DivisionName'/>"/>

<script type="text/javascript">	
	//Initialize portlet with settings icon
	ns.common.initializePortlet("locationSummaryTabs");
	ns.common.initializePortlet("admin_pendingDivisionsTab");
	
	$(document).ready(function(){
		$.publish("selectDefaultDivision")
	});		
</script>
