<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<ffi:help id="user_corpadminprofiles" className="moduleHelpClass"/>
<s:include value="inc/businessuserprofiles-pre.jsp"/>

<ffi:removeProperty name="AddButton"/>
<form id="inputFormId" >
 <input type="hidden" id="gridUrlId" name="GridURLs" value="GRID_Profile">
 </input>
</form>

<ffi:setGridURL grid="GRID_BusinessProfile" name="DeleteURL" url="/cb/pages/jsp/businessuserprofile/deleteBusinessUserProfile_init.action?employeeID={0}&selectedEntGroupId={1}" parm0="ProfileEntGroupId"  parm1="ProfileEntGroupId"/>
<ffi:setGridURL grid="GRID_BusinessProfile" name="EditURL" url="/cb/pages/jsp/businessuserprofile/modifyBusinessUserProfile_init.action?employeeID={0}&OneAdmin={1}&UsingEntProfiles={2}&ChannelId={3}" parm0="ProfileEntGroupId" parm1="OneAdmin" parm2="UsingEntProfiles" parm3="ChannelId"/>
<ffi:setGridURL grid="GRID_BusinessProfile" name="CloneURL" url="/cb/pages/jsp/businessuserprofile/cloneBusinessUserProfile_init.action?employeeID={0}&OneAdmin={1}&UsingEntProfiles={2}&ChannelId={3}" parm0="ProfileEntGroupId" parm1="OneAdmin" parm2="UsingEntProfiles" parm3="ChannelId"/>
<ffi:setGridURL grid="GRID_BusinessProfile" name="PermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=UserProfile&BusinessEmployeeId={0}&OneAdmin={1}&UsingEntProfiles={2}&ChannelId={3}&EditGroup_GroupId={4}" parm0="ProfileEntGroupId" parm1="OneAdmin" parm2="UsingEntProfiles" parm3="ChannelId" parm4="ProfileEntGroupId"/>

<ffi:setGridURL grid="GRID_Profile" name="PermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=UserProfile&BusinessEmployeeId={0}&OneAdmin={1}&UsingEntProfiles={2}&ChannelId={3}&ParentProfileName={4}&EditGroup_GroupId={5}" parm0="ProfileEntGroupId" parm1="OneAdmin" parm2="UsingEntProfiles" parm3="ChannelId" parm4="ParentProfileName" parm5="ProfileEntGroupId"/>

<ffi:setProperty name="adminProfileGridTempURL" value="getBusinessUserProfiles?CollectionName=entitlementProfiles&GridURLs=GRID_BusinessProfile" URLEncrypt="true"/>
<ffi:setProperty name="adminProfileSubGridTempURL" value="getBusinessUserProfilesChild?CollectionName=entitlementProfiles" URLEncrypt="false"/>
<s:url namespace="/pages/jsp/businessuserprofile" id="remoteurlProfile" action="%{#session.adminProfileGridTempURL}" escapeAmp="false"/>
<s:url namespace="/pages/jsp/businessuserprofile" id="remoteurlSubProfile" action="%{#session.adminProfileSubGridTempURL}" escapeAmp="false"/>
     <sjg:grid
       id="businessProfileGrid"
       caption=""
	   sortable="true"  
       dataType="json"
       href="%{remoteurlProfile}"
       pager="true"
       viewrecords="true"
       gridModel="gridModel"
	   rowList="%{#session.StdGridRowList}" 
	   rowNum="%{#session.StdGridRowNum}" 
       rownumbers="false"
       altRows="true"
       sortname="ProfileName"
       sortorder="asc"
       shrinkToFit="true"
       navigator="true"
       groupCollapse="true"
       navigatorAdd="false"
       navigatorDelete="false"
       navigatorEdit="false"
       navigatorRefresh="false"
       navigatorSearch="false"
       onGridCompleteTopics="addGridControlsEvents,removeSubGridItems"
     >
     	<sjg:grid
				id="adminProfileSubGrid"
				subGridUrl="%{remoteurlSubProfile}"
				gridModel="gridModel"
				sortable="false"
				rowList="4,8,10"
				formIds="inputFormId"
				rowNum="10"
				rownumbers="false"
				shrinkToFit="true"
				scroll="false"
				altRows="true"
				
				>
				<sjg:gridColumn name="profileName" width="300" index="ProfileName" title="%{getText('jsp.user_267')}" sortable="false" hidden="true" />
			     <sjg:gridColumn name="map.Group" width="300" index="Group" title="%{getText('jsp.default_225')}" sortable="false" hidden="true"/>
			     <sjg:gridColumn name="profileEntGroupId" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatProfileActionLinks" search="false" width="150" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
			     <sjg:gridColumn name="map.ChannelType" width="150" index="ChannelType" title="%{getText('jsp.reports.Criteria_Channel')}" sortable="true" />
			     <sjg:gridColumn name="map.IsAnAdminOf" width="0" index="IsAnAdminOf" title="%{getText('jsp.user_184')}" hidden="true" sortable="false" hidedlg="true"/>
				 <sjg:gridColumn name="map.OneAdmin" width="0" index="OneAdmin" title="%{getText('jsp.user_231')}" hidden="true" sortable="false" hidedlg="true"/>
				  <sjg:gridColumn name="map.ParentProfileName" width="0" index="ParentProfileName" title="%{getText('jsp.user_231')}" hidden="true" sortable="false" hidedlg="true"/>
			</sjg:grid>
     <sjg:gridColumn name="profileName" width="80" index="ProfileName" title="%{getText('jsp.user_267')}" sortable="true" />
     <sjg:gridColumn name="map.Group" width="40" index="Group" title="%{getText('jsp.default_225')}" sortable="true" />
     <sjg:gridColumn name="profileEntGroupId" index="action" key="true" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatProfileActionLinks" search="false" width="150" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
     <sjg:gridColumn name="map.ChannelType" width="0" index="ChannelType" title="%{getText('jsp.reports.Criteria_Channel')}" sortable="true" hidden="true" />
     <sjg:gridColumn name="map.IsAnAdminOf" width="0" index="IsAnAdminOf" title="%{getText('jsp.user_184')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="map.OneAdmin" width="0" index="OneAdmin" title="%{getText('jsp.user_231')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="channelId" width="0" index="ChannelId" title="%{getText('jsp.user_231')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="status" width="25" index="Status" title="Status"  hidden="false" sortable="false" hidedlg="true" formatter="formatProfileStatus"/>
	 <sjg:gridColumn name="description" width="0" index="Description" title="%{getText('jsp.default_170')}" hidden="false" sortable="false" hidedlg="true"/>
	 
 </sjg:grid>
<script type="text/javascript">
//Code to remove the expand subgrid icon from the grid which is not applicable.
//Only cross channel profiles have child profiles so removing the icon from the rest of the per channel profiles.
	$.subscribe('removeSubGridItems', function(event,data) {	
		var gridID = data.id;
		if (!gridID.startsWith('#'))
			gridID = '#' + gridID;
			var data = $(gridID).getDataIDs();
			for(a=0;a<data.length;a++){
				row=jQuery(gridID).getRowData(data[a]);
				
				var group = row['map.Group'];
				var userName = row['profileName'];
				var id = data[a];
				var channelId = row['channelId'];
				if(channelId != "X") {
					 $('tr#'+id, $(gridID)).children("td.sgcollapsed")
	                 .html("")
	                 .removeClass('ui-sgcollapsed sgcollapsed');
			}
		}
	});
	var fnCustomProfilesRefresh = function (event) {
		ns.common.forceReloadCurrentSummary();
	};
	
	$("#adminProfileGrid").data('fnCustomRefresh', fnCustomProfilesRefresh);
	
	$("#adminProfileGrid").data("supportSearch",true);

</script>
