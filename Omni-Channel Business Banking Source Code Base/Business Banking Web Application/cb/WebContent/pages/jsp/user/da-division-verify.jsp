<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<%	
	if( request.getParameter("rejectFlag") != null ) { 
		String rejectFlag = request.getParameter("rejectFlag");
		session.setAttribute("rejectFlag", rejectFlag); 
	}
	
	if( request.getParameter("itemId") != null ) { 
		String itemId = request.getParameter("itemId");
		session.setAttribute("itemId", itemId); 
	}
	
	if( request.getParameter("GroupName") != null ) { 
		String GroupName = request.getParameter("GroupName");
		session.setAttribute("GroupName", GroupName); 
	}
	
	if( request.getParameter("userAction") != null ) { 
		String userAction = request.getParameter("userAction");
		session.setAttribute("userAction", userAction); 
	}
	
	session.setAttribute("Section", request.getParameter("Section"));
%>


<ffi:setL10NProperty name='PageHeading' value='Pending Approval Summary'/>
<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<s:include value="/pages/jsp/inc/init/admin-init.jsp"/>
</ffi:cinclude>

<s:include value="inc/da-summary-common.jsp" />
<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetCategories" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
	<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetCategories"/>

<ffi:cinclude value1="${categories}" value2="">
	<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
	<ffi:setProperty name="GetDAItem" property="Validate" value="itemId,ItemType" />
	<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
	<ffi:process name="GetDAItem"/>
</ffi:cinclude>

<ffi:removeProperty name="DISABLE_APPROVE"/>

<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >
<ffi:setProperty name="SuccessUrl" value="${successUrl}" />
<input type="hidden" name="userAction" value="<ffi:getProperty name='userAction'/>" >

<div align="center">
	<ffi:setProperty name="subMenuSelected" value="divisions"/>
	<ffi:removeProperty name="isPendingIsland"/>
	<ffi:object name="com.ffusion.tasks.dualapproval.ProcessDACategories" id="ProcessDACategories" />
	<ffi:process name="ProcessDACategories" />
	<s:include value="inc/da-processwizardmenu.jsp" />
	<br>
</div>

<ffi:setProperty name="BackURL" value="${SecurePath}user/da-division-verify.jsp"/>
<ffi:removeProperty name="categories"/>
<ffi:removeProperty name="SubmittedUserName"/>
