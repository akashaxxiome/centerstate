<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminapprovalgroupassign" className="moduleHelpClass"/>
<%
	com.ffusion.beans.approvals.ApprovalsGroups apprGroupsForUser
		= ( com.ffusion.beans.approvals.ApprovalsGroups )session.getAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_USER_APPROVALS_GROUPS );
	com.ffusion.beans.approvals.ApprovalsGroup apprGroup;
	java.util.HashMap apprGroupsForUserMap = new java.util.HashMap();
	Integer apprGroupID;
	Integer keyValue;

	if ( apprGroupsForUser != null ) {
		for ( int a = 0; a < apprGroupsForUser.size(); a++ ) {
			apprGroup = ( com.ffusion.beans.approvals.ApprovalsGroup )apprGroupsForUser.get( a );
			apprGroupID = new Integer( apprGroup.getApprovalsGroupID() );
			keyValue = new Integer( a );
			apprGroupsForUserMap.put( keyValue, apprGroupID );
		}
	}
%>

<ffi:object id="GetApprovalGroups" name="com.ffusion.tasks.approvals.GetApprovalGroups" scope="session"/>
<ffi:process name="GetApprovalGroups"/>

<%
	com.ffusion.beans.approvals.ApprovalsGroups apprGroups
		= ( com.ffusion.beans.approvals.ApprovalsGroups )session.getAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_GROUPS );
    if (apprGroups != null)
	    apprGroups.setSortedBy("APPROVALGROUPNAME");
%>


<ffi:object id="AssignApprovalGroups" name="com.ffusion.tasks.approvals.AssignApprovalGroups" scope="session"/>
<% session.setAttribute("FFIAssignApprovalGroups", session.getAttribute("AssignApprovalGroups")); %>

<span id="PageHeading" style="display:none;"><s:text name="jsp.user_47"/></span>

<div align="center" class="marginTop20">
			<div align="center" class="marginBottom10">
           			<span class="sectionsubhead">
           			        <ffi:getProperty name="Context"/>
					</span>
			</div>
			<div>
						<ul id="formerrors"></ul>
			</div>
			<div align="left" class="adminBackground">
			<s:form id="assignApprovalGroupsID" namespace="/pages/user" action="assignApprovalGroups-verify" theme="simple" name="FormName" method="post">
                 	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<% String checkBoxName;
						   Integer tempGroupID; %>
						   
						<% if ( apprGroups != null && apprGroups.size() > 0) {
						   	for ( int a = 0; a < apprGroups.size(); a++ ) {
						        	apprGroup = ( com.ffusion.beans.approvals.ApprovalsGroup )apprGroups.get( a );
								checkBoxName = "apprGroup" + apprGroup.getApprovalsGroupID();
								tempGroupID = new Integer( apprGroup.getApprovalsGroupID() ); %>
							<%-- <tr>
								<td class="adminBackground"><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0"></td>
								<td class="adminBackground" valign="top">
	                                            <ffi:setProperty name="ApprovalsGroupName" value="<%=apprGroup.getGroupName()%>" />
									<input name="<%=checkBoxName%>" type="checkbox" <% if ( apprGroupsForUserMap.containsValue( tempGroupID ) ) { %> checked <% } %> border="0"><span class="sectionsubhead">&nbsp;<ffi:getProperty name="ApprovalsGroupName"/></span>
                                    							</td>
								<td class="adminBackground" align="right" width="150"></td>

							</tr> --%>
							<div class="approvalCheckDiv">
								<ffi:setProperty name="ApprovalsGroupName" value="<%=apprGroup.getGroupName()%>" />
								<input name="<%=checkBoxName%>" type="checkbox" <% if ( apprGroupsForUserMap.containsValue( tempGroupID ) ) { %> checked <% } %> border="0"><span class="sectionsubhead">&nbsp;<ffi:getProperty name="ApprovalsGroupName"/></span>
							</div>
							<% } %>
						<% } else {%> 
						<div class="sectionsubhead" align="center">
						        <s:text name="jsp.user_208"/>
						</div>
						<% } %>
						<% if ( apprGroups != null && apprGroups.size() > 0) { %>
						<div class="btn-row">
									<sj:a id="cancelBtn"
									   button="true" 
									   onClickTopics="cancelUserForm"><s:text name="jsp.default_82"/></sj:a>
									<sj:a id="addSubmit"
										  	formIds="assignApprovalGroupsID"
											targets="verifyDiv"
											button="true"
											validate="true"
											validateFunction="customValidation"
											onBeforeTopics="beforeVerify"
											onCompleteTopics="completeVerify"
											onErrorTopics="errorSubmit"
											><s:text name="jsp.user_2"/></sj:a>
						</div>
						<% } else { %>
						<div class="btn-row">
							<sj:a button="true"  onClickTopics="cancelUserForm"><s:text name="jsp.default_57"/></sj:a>
						</div>
						<% } %>
			</s:form>
</div></div>
