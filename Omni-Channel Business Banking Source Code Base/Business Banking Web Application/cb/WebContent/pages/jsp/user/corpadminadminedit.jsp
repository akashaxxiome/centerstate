<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>

<%@ page import="com.ffusion.beans.business.Business"%>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroup"%>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroups"%>
<%@ page import="com.ffusion.beans.SecureUser"%>
<%@ page import="com.ffusion.beans.user.BusinessEmployees"%>
<%@ page import="com.ffusion.beans.user.BusinessEmployee"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember" %>
<%@ page import="com.ffusion.csil.beans.entitlements.Entitlement" %>
<%@ page import="com.sap.banking.web.util.entitlements.EntitlementsUtil"%>

<ffi:help id="user_corpadminadminedit" className="moduleHelpClass"/>
<link type="text/css" rel="stylesheet" media="all"
	href="<s:url value='/web'/>/css/ui.supermultiselect.css" />
<script type="text/javascript"
	src="<s:url value='/web'/>/js/ui.supermultiselect.js"></script>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="CanAdministerAnyGroup" />

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>


<div id='PageHeading' style="display: none;"><s:text name="jsp.user_137"/></div>

<%
	String UseLastRequest = request.getParameter("UseLastRequest");
 	if(UseLastRequest!= null){
 		session.setAttribute("UseLastRequest", UseLastRequest);
 	}

	String temp = request.getParameter("AdminEditType");
	if (temp != null) {
		session.setAttribute("AdminEditType", temp);
	}

	/* kaijies: GetListsForNewGroup marks to add new division or group */
	String GetListsForNewGroup = request.getParameter("GetListsForNewGroup");
	if(GetListsForNewGroup!= null){
		session.setAttribute("GetListsForNewGroup", GetListsForNewGroup);
	}

	String EditGroupAmins = request.getParameter("EditGroupAmins");
	if(EditGroupAmins != null){
		session.setAttribute("EditGroupAmins", EditGroupAmins);
	}

	String divAdd = request.getParameter("divAdd");
	if(divAdd != null){
		session.setAttribute("divAdd", divAdd);
	}

	String AddDiv = request.getParameter("AddDiv");
	if(AddDiv != null){
		session.setAttribute("AddDiv", AddDiv);
	}

	String EditDiv = request.getParameter("EditDiv");
	if(EditDiv != null){
		session.setAttribute("EditDiv", EditDiv);
	}

	String AddGroup = request.getParameter("AddGroup");
	if(AddGroup != null){
		session.setAttribute("AddGroup", AddGroup);
	}

	String EditGroup = request.getParameter("EditGroup");
	if(EditGroup != null){
		session.setAttribute("EditGroup", EditGroup);
	}

	String EditAddDiv = request.getParameter("EditAddDiv");
	if(EditAddDiv != null){
		session.setAttribute("EditAddDiv", EditAddDiv);
	}

	String BackReload = request.getParameter("BackReload");
	if(BackReload != null){
		session.setAttribute("BackReload", BackReload);
	}

	String EditAddGroup = request.getParameter("EditAddGroup");
	if(EditAddGroup != null){
		session.setAttribute("EditAddGroup", EditAddGroup);
	}
%>


<ffi:object id="SetAdministrators" name="com.ffusion.tasks.user.SetAdministrators"/>
<%
session.setAttribute( "FFISetAdministrators", session.getAttribute("SetAdministrators"));
%>

<%
	//Keep original business employee value
	session.setAttribute("OrigBusinessEmployee",(BusinessEmployee) session.getAttribute("BusinessEmployee"));

	SecureUser su = ( SecureUser )session.getAttribute( "SecureUser" );
	BusinessEmployee be = new BusinessEmployee();
	be.set( ProfileDefines.BANK_ID, su.getBankID() );
	be.setBusinessId( su.getBusinessID() );
	session.setAttribute( "BusinessEmployee", be );

	//the variable groupType will be passed to javascript function validateSave
	//so the correct alert message can be displayed
	String groupType = "Business";
%>


<ffi:cinclude value1="${onCancelGoto}" value2="corpadmindivadd.jsp" operator="equals">
	<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
		<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
		<ffi:setProperty name="GetListsForNewGroup" value="TRUE"/>
	</ffi:cinclude>
	<% groupType = "Division"; %>
</ffi:cinclude>
<ffi:cinclude value1="${onCancelGoto}" value2="corpadmingroupadd.jsp" operator="equals">
	<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
		<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
		<ffi:setProperty name="GetListsForNewGroup" value="TRUE"/>
	</ffi:cinclude>
	<% groupType = "Group"; %>
</ffi:cinclude>
<ffi:cinclude value1="${onCancelGoto}" value2="corpadmindivedit.jsp" operator="equals">
	<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
		<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
	</ffi:cinclude>
	<% groupType = "Division"; %>
</ffi:cinclude>
<ffi:cinclude value1="${onCancelGoto}" value2="corpadmingroupedit.jsp" operator="equals">
	<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
		<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
	</ffi:cinclude>
	<% groupType = "Group"; %>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:cinclude value1="${onCancelGoto}" value2="da-new-division-edit.jsp" operator="equals">
		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
			<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
			<ffi:setProperty name="GetListsForNewGroup" value="TRUE"/>
		</ffi:cinclude>
		<% groupType = "Division"; %>
	</ffi:cinclude>
	<ffi:cinclude value1="${onCancelGoto}" value2="da-new-group-edit.jsp" operator="equals">
		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
			<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
			<ffi:setProperty name="GetListsForNewGroup" value="TRUE"/>
		</ffi:cinclude>
		<% groupType = "Group"; %>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${BackReload}" value2="TRUE" operator="equals">
	<ffi:setProperty name="EditGroupAmins" value="TRUE"/>
	<ffi:setProperty name="GetListsForNewGroup" value="FALSE"/>
</ffi:cinclude>

<ffi:cinclude value1="${BackReload}" value2="TRUE" operator="notEquals">
	<ffi:cinclude value1="${onDoneGoto}" value2="corpadmininfo.jsp" operator="notEquals">
<%		// Copy these collections in case the user presses "Cancel" on the auto-entitle page
		// Do this when not reloading and when not from corp admin
		session.setAttribute( "NonAdminEmployeesCopy", session.getAttribute("NonAdminEmployees"));
		session.setAttribute( "AdminEmployeesCopy", session.getAttribute("AdminEmployees"));
		session.setAttribute( "NonAdminGroupsCopy", session.getAttribute("NonAdminGroups"));
		session.setAttribute( "AdminGroupsCopy", session.getAttribute("AdminGroups"));
		session.setAttribute( "adminMembersCopy", session.getAttribute("adminMembers"));
		session.setAttribute( "userMembersCopy", session.getAttribute("userMembers"));
		session.setAttribute( "groupIdsCopy", session.getAttribute("groupIds"));
		session.setAttribute( "tempAdminEmpsCopy", session.getAttribute("tempAdminEmps") );
		session.setAttribute( "GroupDivAdminEditedCopy", session.getAttribute("GroupDivAdminEdited"));
%>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:removeProperty name="BackReload"/>

<%-- Retrieve the list of admins on this group --%>
<ffi:cinclude value1="${GetListsForNewGroup}" value2="TRUE" operator="notEquals">
   <ffi:cinclude value1="${EditGroupAmins}" value2="TRUE" operator="notEquals">
	<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
	<ffi:cinclude value1="${onCancelGoto}" value2="corpadmininfo.jsp" operator="equals">
		<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${onCancelGoto}" value2="corpadmininfo.jsp" operator="notEquals">
		<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	</ffi:cinclude>
	<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="Admins"/>
	<ffi:process name="GetAdminsForGroup"/>
   </ffi:cinclude>
</ffi:cinclude>

<%-- based on the business employee object, get the business employees for the business --%>
<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="BusinessEmployee" />
<ffi:process name="GetBusinessEmployees" />
<ffi:removeProperty name="GetBusinessEmployees" />

<ffi:cinclude value1="${AdminEditType}" value2="Company" operator="equals">
	<ffi:setProperty name="EntToCheck" value="<%=EntitlementsDefines.BUSINESS_ADMIN%>"/>
</ffi:cinclude>
<ffi:cinclude value1="${AdminEditType}" value2="Division" operator="equals">
	<ffi:setProperty name="EntToCheck" value="<%=EntitlementsDefines.DIVISION_MANAGEMENT%>"/>
</ffi:cinclude>
<ffi:cinclude value1="${AdminEditType}" value2="Group" operator="equals">
	<ffi:setProperty name="EntToCheck" value="<%=EntitlementsDefines.GROUP_MANAGEMENT%>"/>
</ffi:cinclude>

<%-- Generate the non-admin and admin lists --%>
<ffi:cinclude value1="${GetListsForNewGroup}" value2="TRUE" operator="notEquals">
   <ffi:cinclude value1="${EditGroupAmins}" value2="TRUE" operator="notEquals">
	<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="Admins"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
	<ffi:setProperty name="GenerateUserAndAdminLists" property="EntToCheck" value="${EntToCheck}"/>
	<ffi:process name="GenerateUserAndAdminLists"/>
	<ffi:removeProperty name="adminIds"/>
	<ffi:removeProperty name="userIds"/>
	<ffi:removeProperty name="GenerateUserAndAdminLists"/>
   </ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${GetListsForNewGroup}" value2="TRUE" operator="equals">
	<%-- we have to supply the AdminEmployees, NonAdminEmployees and the NonAdminGroups ArrayLists --%>
	<%
	SecureUser thisUser = (SecureUser)session.getAttribute( "SecureUser" );
	BusinessEmployees allUsers = (BusinessEmployees)session.getAttribute( com.ffusion.tasks.Task.BUSINESS_EMPLOYEES );
	BusinessEmployee currentUser = (BusinessEmployee)allUsers.getByID( Integer.toString( thisUser.getProfileID() ) );
	allUsers.removeByID( Integer.toString( thisUser.getProfileID() ) );
	com.ffusion.csil.beans.entitlements.Entitlement ent = null;
	String entToCheck = (String)session.getAttribute("EntToCheck");
	if (entToCheck != null) {
		ent = new com.ffusion.csil.beans.entitlements.Entitlement(entToCheck, null, null);
	}
	for (int i = 0; i < allUsers.size(); i++) {
		BusinessEmployee nextEmp = (BusinessEmployee)allUsers.get(i);
		com.ffusion.csil.beans.entitlements.EntitlementGroupMember member = EntitlementsUtil.getEntitlementGroupMemberForBusinessEmployee(nextEmp);
		nextEmp.remove("CANADMIN");
		try {
			if (ent != null && !EntitlementsUtil.checkEntitlement(member, ent)) {
				nextEmp.put("CANADMIN", "false");
			}
		} catch (Exception ex) {}

	}
	session.setAttribute("NonAdminEmployees", allUsers);

	java.util.Locale tempLocale = thisUser.getLocale();
	BusinessEmployees defaultAdmin = new BusinessEmployees( tempLocale );
	defaultAdmin.add( currentUser );
	session.setAttribute( "AdminEmployees", defaultAdmin );

	EntitlementGroup businessEntGroup = (EntitlementGroup)( (Business)session.getAttribute( EntitlementsDefines.ENT_GROUP_BUSINESS ) ).getEntitlementGroup();
	java.util.ArrayList groupList = new java.util.ArrayList();
	// each entry will contain a name, a type and a groupId
	java.util.ArrayList bizEntry = new java.util.ArrayList();
	bizEntry.add( businessEntGroup.getGroupName() );
	bizEntry.add( businessEntGroup.getEntGroupType() );
	bizEntry.add( Integer.toString( businessEntGroup.getGroupId() ) );
	groupList.add( bizEntry );
	try{
	     EntitlementGroups divs = EntitlementsUtil.getChildrenEntitlementGroups( businessEntGroup.getGroupId() );
	     for( int i = 0; i < divs.size(); i++ ) {
	          EntitlementGroup division = divs.getGroup( i );
	          //UserProfile group? continue. We dont need to display userprofile group in the group list.
	          if(EntitlementsDefines.ENT_GROUP_USER_PROFILE.equalsIgnoreCase(division.getEntGroupType())){
	        	  continue;
	          }
		  // each entry will contain a name, a type and a groupId
		  java.util.ArrayList divEntry = new java.util.ArrayList();
		  divEntry.add( division.getGroupName() );
		  divEntry.add( division.getEntGroupType() );
		  divEntry.add( Integer.toString( division.getGroupId() ) );
	          groupList.add( divEntry );

	          EntitlementGroups groups = EntitlementsUtil.getChildrenEntitlementGroups( division.getGroupId() );
	          for( int j = 0; j < groups.size(); j++ ) {
	              EntitlementGroup group = groups.getGroup( j );
	           	  //UserProfile group? continue. We dont need to display userprofile group in the group list.
		          if(EntitlementsDefines.ENT_GROUP_USER_PROFILE.equalsIgnoreCase(group.getEntGroupType())){
		        	  continue;
		          }
	              // each entry will contain a name, a type and a groupId
		      java.util.ArrayList groupEntry = new java.util.ArrayList();
	              groupEntry.add( group.getGroupName() );
		      groupEntry.add( group.getEntGroupType() );
	              groupEntry.add( Integer.toString( group.getGroupId() ) );

		      groupList.add( groupEntry );
	          }
	      }
	} catch ( Exception e ) {
           //exception occured while calling getChildrenEntitlementGroups, no error handling necessary
	}
	session.setAttribute( "NonAdminGroups", groupList );
%>
</ffi:cinclude>

<ffi:cinclude value1="${Business.dualApprovalMode}"
	value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:cinclude value1="${divAdd}" value2="true" operator="notEquals">
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:cinclude value1="${onCancelGoto}" value2="corpadmininfo.jsp" operator="equals">
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${onCancelGoto}" value2="corpadmininfo.jsp" operator="notEquals">
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}" />
		</ffi:cinclude>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=groupType%>" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}" />
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>" />
		<ffi:process name="GetDACategoryDetails" />

		<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
			<ffi:object id="GenerateListsFromDAAdministrators" name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session" />
			<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees" />
			<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups" />
			<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups" />
			<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees" />
			<ffi:process name="GenerateListsFromDAAdministrators" />
		</ffi:cinclude>
	</ffi:cinclude>

	<ffi:removeProperty name="divAdd" />
	<ffi:removeProperty name="GetDACategoryDetails" />
	<ffi:removeProperty name="GenerateListsFromDAAdministrators" />
</ffi:cinclude>




<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:object id="SetAdministratorsInDA" name="com.ffusion.tasks.dualapproval.SetAdministratorsInDA"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="AddedMemberIds" value="addedMemberIds"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="RemovedMemberIds" value="removedMemberIds"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="ItemId" value="${Business.Id}"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
	<ffi:setProperty name="SetAdministratorsInDA" property="UserAction" value="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>"/>

	<% session.setAttribute("FFISetAdministratorsInDA", session.getAttribute("SetAdministratorsInDA")); %>
</ffi:cinclude>




<!-- Dual Approval ends -->

<%
	request.setAttribute("action","setAdministrators-confirm");
%>
<div class="ffivisible"  style="max-height:650px; overflow-y:auto; width:100%;">
<div align="center" id="selectPool" class="customAccordianBgClass adminAccordianCls"><!-- <FORM name=adminForm action="corpadminadminedit-confirm.jsp" method="post"> -->
	<s:form id="editAdministratorForm" namespace="/pages/user"
		name="editAdministratorForm" action="%{#request.action}"
		method="post" validate="false" theme="simple">
		<input type="hidden" name="CSRF_TOKEN"
			value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		<!-- 3 column table for icons -->

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" class="adminBackground"><!-- table containing data -->
				<table align="center" border="0" width="100%">
					<tr>
						<td align="right">
						<table border="0" cellspacing="0" cellpadding="0"  class="tblborder" width="350">
							<tr>
								<td align="left" class="pane-header ui-state-default ui-corner-top ui-state-active"><s:text name="jsp.user_363"/></td>
							</tr>
							<tr>
								<td align="left"><SELECT name="userList" id="userList"
									class="txtbox tableData" multiple size="15" style="width: 250px">

									<ffi:list collection="NonAdminEmployees" items="businessEmployee">
										<ffi:cinclude value1="${businessEmployee.CANADMIN}" value2="false" operator="notEquals">
											<ffi:setProperty name="_userID" value="${businessEmployee.Id}" />
											<ffi:setProperty name="_entGroupID" value="${businessEmployee.EntitlementGroupId}" />

											<%-- add the number and one space --%>
											<%-- we can assume that business employees are SecureUser.TYPE_CUSTOMER --%>
											<option value="<%= EntitlementsDefines.ENT_MEMBER_TYPE_USER %>-<%= SecureUser.TYPE_CUSTOMER %>-<ffi:getProperty name="_userID"/>-<ffi:getProperty name="_entGroupID"/>"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1" parm0="${businessEmployee.FullName}" /></option>
										</ffi:cinclude>
									</ffi:list>
								</SELECT>
								</td>
							</tr>
						</table>
						<table border="0" cellspacing="0" cellpadding="0" class="tblborder marginTop10">	
							<tr>
								<td align="left" class="pane-header ui-state-default ui-corner-top ui-state-active"><s:text name="jsp.user_164"/></td>
							</tr>
							<tr>
								<td align="left">
								<SELECT name="groupList" id="groupList"
									class="txtbox" multiple size="15" style="width: 250px">
									<ffi:list collection="NonAdminGroups" items="group">
										<ffi:list collection="group"
											items="groupName, groupType, groupId">

											<%-- add the Groups to the list --%>
											<ffi:setProperty name="ENTResource" property="ResourceID" value="Ent_Group_Type.${groupType}" />
											<option value="---<ffi:getProperty name="groupId"/>"><ffi:getProperty name="groupName" /> ( <ffi:getProperty name="ENTResource" property="Resource" /> )</option>
										</ffi:list>
									</ffi:list>
								</SELECT>
								</td>
							</tr>
						</table>
						</td>
						<td align="center" width="40">
							&nbsp;<%-- <sj:a button="true"	cssStyle="width:90px"	onClick="ns.admin.removeAllFromAdminList()"><s:text name="jsp.common_200" /></sj:a> --%>
						</td>
						<td align="left" valign="top">
						<table border="0" cellspacing="0" cellpadding="0"  class="tblborder">
							<tr>
								<ffi:cinclude value1="${CATEGORY_BEAN}" value2="">
									<td colspan="2" class="pane-header ui-state-default ui-corner-top ui-state-active" align="left"><s:text name="jsp.user_292"/><span class="required">*</span></td>
								</ffi:cinclude>
								<ffi:cinclude value1="${CATEGORY_BEAN}" value2=""
									operator="notEquals">
									<ffi:cinclude value1="${onCancelGoto}" value2="corpadmindivadd.jsp" >
										<td colspan="2" class="sectionsubhead" align="left"><s:text name="jsp.user_292"/><span class="required">*</span></td>
									</ffi:cinclude>
									<ffi:cinclude value1="${onCancelGoto}" value2="corpadmindivadd.jsp" operator="notEquals">
										<td colspan="2" class="sectionheadDA" align="left"><s:text name="jsp.user_292"/><span class="required">*</span></td>
									</ffi:cinclude>
								</ffi:cinclude>
							</tr>
							<tr>
								<td>
									<span id="adminListNameError"></span>
								</td>
							</tr>
							<tr>
								<td valign="middle" align="left">
									<span class="removeAllLinkAdmin"><sj:a button="false" onClick="ns.admin.removeAllFromAdminList()"><s:text name="user.admin.grid.removeAll"/></sj:a></span>
									<SELECT name="adminListName"
									id="adminList" class="txtbox" multiple size="32"
									style="width: 250px">
									<%
													// number the items in the list
													int approvalIndex = 1;
												%>
									<ffi:list collection="AdminEmployees" items="businessEmployee">
										<ffi:setProperty name="_userID" value="${businessEmployee.Id}" />
										<ffi:setProperty name="_entGroupID"
											value="${businessEmployee.EntitlementGroupId}" />
										<option value="<%= EntitlementsDefines.ENT_MEMBER_TYPE_USER %>-<%= SecureUser.TYPE_CUSTOMER %>-<ffi:getProperty name="_userID"/>-<ffi:getProperty name="_entGroupID"/>"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1" parm0="${businessEmployee.FullName}" /></option>
									</ffi:list>
									<ffi:cinclude value1="${GetListsForNewGroup}" value2="TRUE"
										operator="notEquals">
										<ffi:list collection="AdminGroups" items="group">
											<ffi:list collection="group"
												items="groupName, groupType, groupId">

												<%-- add the Groups to the list --%>
												<option value="---<ffi:getProperty name="groupId"/>"><ffi:getProperty name="groupName" /> ( <ffi:setProperty name="ENTResource" property="ResourceID" value="Ent_Group_Type.${groupType}" /> <ffi:getProperty name="ENTResource" property="Resource" /> )</option>
											</ffi:list>
										</ffi:list>
									</ffi:cinclude>
								</SELECT>
								</td>
								<%-- Show old administrator values as highlighted. --%>
								<%-- <ffi:cinclude value1="${CATEGORY_BEAN}" value2=""
									operator="notEquals">
									<td class="sectionhead_greyDA" valign="top">
									<table>
										<ffi:list collection="OldAdminEmployees"
											items="businessEmployee">
											<tr>
												<td class="sectionhead_greyDA" valign="top">ssss<ffi:setProperty
													name="_userID" value="${businessEmployee.Id}" /> <ffi:setProperty
													name="_entGroupID"
													value="${businessEmployee.EntitlementGroupId}" /> <ffi:getL10NString
													rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1"
													parm0="${businessEmployee.FullName}" /></td>
											</tr>
										</ffi:list>
										<ffi:list collection="OldAdminGroups" items="group">
											<ffi:list collection="group"
												items="groupName, groupType, groupId">
												<tr>
													<td class="sectionhead_greyDA" valign="top">ffffadd the Groups to the list
													<ffi:getProperty name="groupName" /> ( <ffi:setProperty
														name="ENTResource" property="ResourceID"
														value="Ent_Group_Type.${groupType}" /> <ffi:getProperty
														name="ENTResource" property="Resource" /> )</td>
												</tr>
											</ffi:list>
										</ffi:list>
									</table>
									</td>
								</ffi:cinclude> --%>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<table  width="96%" align="center" border="0" cellspacing="0" cellpadding="0" class="marginTop20">
								<tr>
									<%-- Show old administrator values as highlighted. --%>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2=""
										operator="notEquals">
										<td class="" valign="top">
										<span class="labelCls"><s:text name="jsp.user_38" /> ( <s:text name="jsp.da_approvals_pending_user_OldValue_column" /> ) :</span>
										
										<ffi:list collection="OldAdminEmployees"
												items="businessEmployee">
										
										<span class="valueCls" >
											<ffi:setProperty
												name="_userID" value="${businessEmployee.Id}" /> <ffi:setProperty
												name="_entGroupID"
												value="${businessEmployee.EntitlementGroupId}" /> <ffi:getL10NString
												rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1"
												parm0="${businessEmployee.FullName}" />,
										</span>
										</ffi:list>
										
										
										<ffi:list collection="OldAdminGroups" items="group">
												<ffi:list collection="group"
													items="groupName, groupType, groupId">
										
										<span class="valueCls" >
											<ffi:getProperty name="groupName" /> ( <ffi:setProperty
															name="ENTResource" property="ResourceID"
															value="Ent_Group_Type.${groupType}" /> <ffi:getProperty
															name="ENTResource" property="Resource" /> )
										</span>
										</ffi:list>
										</ffi:list>
										
										<%-- <table>
											<ffi:list collection="OldAdminEmployees"
												items="businessEmployee">
												<tr>
													<td class="sectionhead_greyDA" valign="top">
													<span><s:text name="jsp.user_38" /> ( <s:text name="jsp.da_approvals_pending_user_OldValue_column" /> ) :</span>
													<ffi:setProperty
														name="_userID" value="${businessEmployee.Id}" /> <ffi:setProperty
														name="_entGroupID"
														value="${businessEmployee.EntitlementGroupId}" /> <ffi:getL10NString
														rsrcFile="cb" msgKey="jsp/user/corpadminadminedit.jsp-1"
														parm0="${businessEmployee.FullName}" />
													</td>
												</tr>
											</ffi:list>
											<ffi:list collection="OldAdminGroups" items="group">
												<ffi:list collection="group"
													items="groupName, groupType, groupId">
													<tr>
														<td class="sectionhead_greyDA" valign="top">ffffadd the Groups to the list
														<ffi:getProperty name="groupName" /> ( <ffi:setProperty
															name="ENTResource" property="ResourceID"
															value="Ent_Group_Type.${groupType}" /> <ffi:getProperty
															name="ENTResource" property="Resource" /> )</td>
													</tr>
												</ffi:list>
											</ffi:list>
										</table> --%>
										</td>
									</ffi:cinclude>
								</tr>							
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" colspan="3" height="30">
						<span class="required">* <s:text name="jsp.default_240"/></span>
						</td>
					</tr>
					<!-- <tr>
						<td height="100" colspan="3">&nbsp;</td>
					</tr> -->
					<tr>
						<td colspan="3" align="center"><%-- the proper link for the cancel action need to be set --%>
						<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE"
							operator="equals">
							<ffi:setProperty name="tmp_url"
								value="${SecurePath}user/${onCancelGoto}?AdminsSelected=TRUE&CancelReload=TRUE"
								URLEncrypt="true" />
							<script>
								ns.admin.editAdminerCancelUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/user/${onCancelGoto}?AdminsSelected=TRUE&CancelReload=TRUE"/>';
							</script>
						</ffi:cinclude>
						<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
							<ffi:setProperty name="tmp_url"
								value="${SecurePath}user/${onCancelGoto}?AdminsSelected=FALSE&CancelReload=TRUE"
								URLEncrypt="true" />
							<script>
								ns.admin.editAdminerCancelUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/user/${onCancelGoto}?AdminsSelected=FALSE&CancelReload=TRUE"/>';
							</script>
						</ffi:cinclude>
						<div align="center" class="ui-widget-header customDialogFooter">
						<sj:a
							id="editAdminerCancelButtonId"
							button="true"
							onClickTopics="editAdminerOnCancel"
							><s:text name="jsp.default_82"/></sj:a>
						<ffi:removeProperty name="tmp_url" />
						<%--<INPUT type=submit class="submitbutton" value="SAVE" style="width:50px" onclick="return validateSave( this.form);" > --%>
						<sj:a
							formIds="editAdministratorForm"
							targets="editAdminerAutoEntitleDialogId"
							button="true"
							validate="true"
							validateFunction="customValidation"
							onBeforeTopics="beforeVerify3"
							onErrorTopics="errorVerify"
							validatesaveform="editAdministratorForm"
							onClickTopics="validateSaveOnClickTopic"
							onSuccessTopics="editAdminerSaveOnComplete"
							onCompleteTopics="onCompleteEditAdminTopic,tabifyNotes"
							><s:text name="jsp.default_366"/></sj:a>
							</div>
						</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
	</s:form>
</div>
</div>
<script type="text/javascript">

	(function($){

		$("#adminList").supermultiselect({
					width: "350px",
				 	height: "440px",
				 	containedPool: $("#selectPool"),
				 	sortable: true,
					beforeApplyItem: ns.admin.beforeApplyRightItem,
					afterRemove:function(){
						var $superSelectMenu = $("#adminList_sms");
						ns.common.superSelectFocusHandler($superSelectMenu);
					},

		});
		$("#userList").supermultiselect({
					width: "350px",
				 	height: "180px",
				   	toIds: [ "adminList" ],
				 	containedPool: $("#selectPool"),
					afterAdd:function(){
						var $superSelectMenu = $("#userList_sms");						
						ns.common.superSelectFocusHandler($superSelectMenu);
					}

		});
		$("#groupList").supermultiselect({
					width: "350px",
				 	height: "180px",
				 	toIds: [ "adminList" ],
				 	containedPool: $("#selectPool"),
					afterAdd:function(){
						var $superSelectMenu = $("#groupList_sms");
						ns.common.superSelectFocusHandler($superSelectMenu);
					}

		});
	})(jQuery);
</script>
<%

	BusinessEmployee be1 = (BusinessEmployee)session.getAttribute("OrigBusinessEmployee");
	session.setAttribute( "BusinessEmployee", be1 );

%>
<ffi:removeProperty name="BusinessEmployees" />
<ffi:removeProperty name="Admins" />
<ffi:removeProperty name="UserEmployees" />
<ffi:removeProperty name="GetListsForNewGroup" />
<ffi:removeProperty name="EditGroupAmins" />
<ffi:removeProperty name="Back" />
<ffi:removeProperty name="CATEGORY_BEAN" />
<ffi:removeProperty name="cancelCompanyTopic" />
<ffi:removeProperty name="EntToCheck"/>
<ffi:setProperty name="BackURL" value="/cb${SecurePath}user/corpadminadminedit.jsp" />
