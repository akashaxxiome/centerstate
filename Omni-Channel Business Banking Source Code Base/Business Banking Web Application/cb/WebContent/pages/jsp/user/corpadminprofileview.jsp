<%--
This is a common view admin profile information page.

Pages that request this page
----------------------------
Pages this page requests
------------------------
DONE button requests corpadminprofiles.jsp

Pages included in this page
---------------------------
--%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%-- Need to change help file id name after adding help file for admin profile view --%>
<ffi:help id="payments_wirepayeeview" className="moduleHelpClass"/>

<%
	session.setAttribute("itemId", request.getParameter("itemId"));
%>
<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>

<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminprofiles.jsp"/>

<%-- set BusinessEmployee to be the current logged in user --%>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>
<ffi:removeProperty name="GetBusinessEmployee" />

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchEntitlementProfiles" value="TRUE"/>
<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployees"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>

<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployees" />
<ffi:process name="SetBusinessEmployee"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="BusinessEmployee"/>
	<ffi:process name="PopulateObjectWithDAValues"/>

	<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
	<ffi:setProperty name="GetDAItem" property="Validate" value="itemId,ItemType" />
	<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
	<ffi:process name="GetDAItem"/>
	<ffi:cinclude value1="${DAItem.UserAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED%>">
		<ffi:setProperty name="isUserDeleted" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>

<%-- for localizing state name --%>
<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${BusinessEmployee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${BusinessEmployee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>

<div align="center">
	<%-- <table class="adminBackground" width="676" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2">
				<div align="center">
					<span class="sectionsubhead"><br><ffi:getProperty name="BusinessName"/> <ffi:cinclude value1="DivisionName" value2="" operator="equals"> / <ffi:getProperty name="DivisionName"/></ffi:cinclude>&nbsp;<br>
						<br>
					</span></div>
			</td>
		</tr>
	</table> --%>
	<div class="blockWrapper">
		<div class="blockHead"><s:text name="Profile Summary" /></div>
		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock">
					<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
						<span class='<ffi:getPendingStyle fieldname="userName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_267" /></span>
						<span class="columndata" width="180" >
							<ffi:getProperty name="BusinessEmployee" property="UserName"/>
							<span class="sectionhead_greyDA">
							<br>
								<ffi:getProperty name="oldDAObject" property="UserName"/>
							</span>
						</span>
					</ffi:cinclude>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock">
					<span class='<ffi:getPendingStyle fieldname="entitlementGroupId" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Group" /></span>
					<span class="columndata" width="200" valign="baseline">
						<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
											<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
											<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value="NewEntitlementGroup" />
						<ffi:process name="GetEntitlementGroup"/>												
						<ffi:getProperty name="NewEntitlementGroup" property="GroupName"/>
						<span class="sectionhead_greyDA">
							<br>
                                     <ffi:cinclude value1="${oldDAObject.EntitlementGroupId}" value2="" operator="notEquals">
                                         <ffi:cinclude value1="${oldDAObject.EntitlementGroupId}" value2="0" operator="notEquals">
                                             <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${oldDAObject.EntitlementGroupId}" />
                                             <ffi:process name="GetEntitlementGroup"/>
                                             <ffi:getProperty name="NewEntitlementGroup" property="GroupName"/>
                                         </ffi:cinclude>
                                     </ffi:cinclude>
						</span>
                        <ffi:removeProperty name="NewEntitlementGroup"/>
					</span>	
				</div>
			</div>
			<s:if test="#session.BusinessEmployee.UsingEntProfiles">
				<div class="blockRow">
					<div class="inlineBlock">
						<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
							<span class='<ffi:getPendingStyle fieldname="channelList" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline">Supported Channels</span>
							<span class="columndata" width="180" >
								<ffi:getProperty name="BusinessEmployee" property="channelList"/>
								<span class="sectionhead_greyDA">
								<br>
									<ffi:getProperty name="oldDAObject" property="channelList"/>
								</span>
							</span>
						</ffi:cinclude>
					</div>
				</div>
			</s:if>
		</div>
	</div>		
	<table>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td class="columndata" colspan="6" nowrap align="center">
			<form method="post">   <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/><sj:a button="true" title="Close"  onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a></form>
			</td>
		</tr>
	</table>
</div>
	
<ffi:removeProperty name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>
<ffi:removeProperty name="GetStateProvinceDefnForState"/>
<ffi:removeProperty name="GroupAdmins"/>
<ffi:removeProperty name="isUserDeleted"/>