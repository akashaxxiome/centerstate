<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadminsummary" className="moduleHelpClass"/>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.COMPANY_SUMMARY %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
		<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
		<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
		<ffi:process name="ThrowException"/>
</ffi:cinclude>

<div class="paneWrapper">
  	<div class="paneInnerWrapper">
	<table border="0" cellspacing="0" cellpadding="0" width="100%" class="tableData">
	<%--
		<tr>
			<td colspan="4" width="750" class="sectionsubhead">
				&nbsp;&nbsp;&gt;<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/index.jsp-1" parm0="${BusinessEmployee.FullName}" parm1="${Business.BusinessName}"/>
			</td>
		</tr>
	--%>
		<% int band = 1; %>
		<ffi:list collection="GroupSummaries" items="group">
			<% if ( band == 1 ) {
				out.println( "<tr height=\"20\" class=\"header\">" );
				out.println( "\t<td height=\"1\" >&nbsp;</td>" );
				out.println( "\t<td height=\"1\" class=\"sectionsubhead\" align=\"left\" width=\"50%\">" + com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_415", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) + "</td>" );
				out.println( "\t<td height=\"1\" class=\"sectionsubhead\" align=\"left\">" + com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_416", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) + "</td>" );
				out.println( "\t<td height=\"1\" >&nbsp;</td>" );
				out.println( "</tr>" );
		           }
		        %>

			<ffi:list collection="group" items="spaces, groupName, groupId, numUsers, groupType">
				<tr <%= band % 2 == 0 ? "class=\"columndata dkrow\"" : "class=\"columndata ltrow3\"" %>" >
					<td>&nbsp;</td>
					<ffi:cinclude value1="${groupId}" value2="0" operator="equals" >
						<td class="columndata"><ffi:getProperty name="spaces" encode="false"/><ffi:getProperty name="groupName"/></td>
						<td class="columndata">&nbsp;&nbsp;<ffi:getProperty name="numUsers"/> <%-- <s:text name="jsp.user_367"/> --%><br></td>
					</ffi:cinclude>
					<ffi:cinclude value1="${groupId}" value2="0" operator="notEquals" >
						<td class="columndata">
							<ffi:getProperty name="spaces" encode="false"/>
							<ffi:setProperty name="tempURL" value="/cb/pages/jsp/user/corpsummary.jsp?entGroupId=${groupId}" URLEncrypt="true"/>
							<a title='<s:text name="jsp.default_460" />' href='#' class="anchorText" onClick=
								"ns.admin.viewAdminSummaryByGroupId('<ffi:getProperty name="tempURL"/>')">
								<ffi:getProperty name="groupName"/>
							</a>
						</td>
						<td class="columndata">&nbsp;&nbsp;<ffi:getProperty name="numUsers"/><br></td>
					</ffi:cinclude>
					<td>&nbsp;</td>
				</tr>
			</ffi:list>
			<% band++; %>
		</ffi:list>

		<% if ( band == 1 ) {
		       out.println( "<tr height=\"20\">" );
		       out.println( "<td height=\"1\" class=\"tbrd_ltb\">&nbsp;</td>" );
		       out.println( "<td height=\"1\" class=\"sectionsubhead tbrd_trb\" colspan=\"3\" align=\"left\">" + com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.user_417", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) + "</td>" );
		       out.println( "</tr>" );
	   	   }
	   	%>
	</table></div></div>
	
