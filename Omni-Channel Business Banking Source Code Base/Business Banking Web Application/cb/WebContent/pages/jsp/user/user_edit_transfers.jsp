<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setProperty name="PageTitle" value="Manage Users"/>
<ffi:setProperty name="topMenu" value="home"/>
<ffi:setProperty name="subMenu" value="preferences"/>
<ffi:setProperty name="sub2Menu" value="manageusers"/>

<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'>
<ffi:setProperty name="TempURL" value="${SecurePath}user/user_addedit_transfers_save.jsp?Target=${SecurePath}user/user_addedit_exttransfers.jsp" URLEncrypt="true"/>
<ffi:cinclude value1="${BackButton}" value2="" operator="notEquals">
    <ffi:setProperty name="TempURL" value="${SecurePath}user/user_addedit_transfers_save.jsp?Target=${SecurePath}user/user_addedit_accounts.jsp" URLEncrypt="true"/>
</ffi:cinclude>

</ffi:cinclude>
<ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.TRANSFERS %>'>
<ffi:cinclude value1="${AddEditMode}" value2="Edit" operator="equals">
    <ffi:cinclude value1="${IsTransfersPermissionsInitialized}" value2="" operator="equals">
        <ffi:setProperty name="IncludeFromSection" value="TRUE"/>
        <ffi:setProperty name="IncludeToSection" value="TRUE"/>
        <ffi:object name="com.ffusion.beans.util.StringList" id="AccountsCollectionNames" scope="session"/>
        <ffi:setProperty name="AccountsCollectionNames" property="Add" value="SecondaryUserEntitledCoreAccounts"/>
        <ffi:setProperty name="FromOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS_FROM %>'/>
        <ffi:setProperty name="ToOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS_TO %>'/>

        <ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsFromFilters" scope="session"/>
        <ffi:setProperty name="AccountsFromFilters" property="Key" value="SecondaryUserEntitledCoreAccounts"/>
        <ffi:setProperty name="AccountsFromFilters" property="Value" value='<%= com.ffusion.beans.accounts.AccountFilters.TRANSFER_FROM_FILTER %>'/>

        <ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsToFilters" scope="session"/>
        <ffi:setProperty name="AccountsToFilters" property="Key" value="SecondaryUserEntitledCoreAccounts"/>
        <ffi:setProperty name="AccountsToFilters" property="Value" value='<%= com.ffusion.beans.accounts.AccountFilters.TRANSFER_TO_FILTER %>'/>

        <ffi:setProperty name="FeatureAccessAndLimitsTaskName" value="FFITransferAccessAndLimits"/>
		<s:include value="/pages/jsp/user/inc/user_edit_permissions_init.jsp" />
    </ffi:cinclude>

    <ffi:setProperty name="IsTransfersPermissionsInitialized" value="true"/>
</ffi:cinclude>

<div class="marginTop10" style="position:relative; overflow:hidden">
<ffi:help id="user_user_addedit_transfers" className="moduleHelpClass"/>
<p class="transactionHeading"><ffi:getProperty name="FFIEditSecondaryUser" property="FullNameWithLoginId"/> <%-- <ffi:getProperty name="SecondaryUser" property="FullNameWithLoginId"/> --%></p>
<form name="UserTransfersForm" id="userTransfersForm" action="/cb/pages/jsp/user/saveSecondaryUserTransfers.action" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header"><s:text name="user.transferInformation"/></div>
   		<div class="paneContentWrapper">
   			<div class="instructions">
             	<s:text name="secondaryUser.transferTab.instructions"/>
             </div>
             <div class="instructions">
                 <input name="EnableInternalTransfers" togglepermissions="true" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value="TRUE"  <ffi:cinclude value1="${EnableInternalTransfers}" value2="TRUE" operator="equals">checked</ffi:cinclude>> <span style="padding-right: 20px;" class="txt_normal_bold">Yes</span> <input name="EnableInternalTransfers" togglepermissions="true" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value="FALSE"  <ffi:cinclude value1="${EnableInternalTransfers}" value2="FALSE" operator="equals">checked</ffi:cinclude>> <span class="txt_normal_bold">No</span>
             </div>
             <div class="instructions" containerType="permissionsBlock" <ffi:cinclude value1="${EnableInternalTransfers}" value2="TRUE" operator="notEquals">style="display: none;"</ffi:cinclude>>
                 <s:text name="secondaryUser.transferTab.instructions2"/>
             </div>
   		</div>
   	</div>
</div>
    <div containerType="permissionsBlock" <ffi:cinclude value1="${EnableInternalTransfers}" value2="TRUE" operator="notEquals">style="display: none;"</ffi:cinclude>>
<ffi:setProperty name="PermissionsTitle" value="Transfer Permissions"/>
<ffi:setProperty name="EnableOperationDisplayValue" value="Transfers"/>
<ffi:setProperty name="NoAccountsMessage" value="(No accounts authorized for transfers are available.)"/>
<ffi:setProperty name="IncludeFromSection" value="TRUE"/>
<ffi:setProperty name="IncludeToSection" value="TRUE"/>
<ffi:object name="com.ffusion.beans.util.StringList" id="AccountsCollectionNames" scope="session"/>
<ffi:setProperty name="AccountsCollectionNames" property="Add" value="SecondaryUserEntitledCoreAccounts"/>
<ffi:setProperty name="FromOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS_FROM %>'/>
<ffi:setProperty name="ToOperationName" value='<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS_TO %>'/>

<ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsFromFilters" scope="session"/>
<ffi:setProperty name="AccountsFromFilters" property="Key" value="SecondaryUserEntitledCoreAccounts"/>
<ffi:setProperty name="AccountsFromFilters" property="Value" value='<%= com.ffusion.beans.accounts.AccountFilters.TRANSFER_FROM_FILTER %>'/>

<ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsToFilters" scope="session"/>
<ffi:setProperty name="AccountsToFilters" property="Key" value="SecondaryUserEntitledCoreAccounts"/>
<ffi:setProperty name="AccountsToFilters" property="Value" value='<%= com.ffusion.beans.accounts.AccountFilters.TRANSFER_TO_FILTER %>'/>

<ffi:setProperty name="FeatureAccessAndLimitsTaskName" value="FFITransferAccessAndLimits"/>
	<s:include value="/pages/jsp/user/inc/user_addedit_permissions.jsp" />
    </div>
    <div class="btn-row">
    	<sj:a id="secUserTransfersCancel" 
    	button="true" 
		summaryDivId="summary" 
		buttonType="cancel" 
		onClickTopics="showSummary,secondaryUsers.cancel">
    	<s:text name="jsp.default_82"/></sj:a>        
		<sj:a id="secUserTransfersBack" button="true" buttontype="back" backstep="1"><s:text name="jsp.default_57"/></sj:a>        
		<sj:a id="secUserTransfersNext" button="true" formIds="userTransfersForm" targets="targetDiv" validate="true" validateFunction="customValidation" onBeforeTopics="secondaryUsers.clearErrors" onSuccessTopics="secondaryUsers.saveTransfersSuccess"><s:text name="jsp.default_291"/></sj:a>        
    </div>
</form>

</div>

<%--
<ffi:removeProperty name="TempURL"/>
<ffi:removeProperty name="PermissionsTitle"/>
<ffi:removeProperty name="EnableOperationDisplayValue"/>
<ffi:removeProperty name="IncludeFromSection"/>
<ffi:removeProperty name="IncludeToSection"/>
<ffi:removeProperty name="AccountsCollectionNames"/>
<ffi:removeProperty name="FromOperationName"/>
<ffi:removeProperty name="ToOperationName"/>
<ffi:removeProperty name="AccountsFromFilters"/>
<ffi:removeProperty name="AccountsToFilters"/>
<ffi:removeProperty name="FeatureAccessAndLimitsTaskName"/>
<ffi:removeProperty name="NoAccountsMessage"/>
<ffi:removeProperty name="CancelButton"/>
<ffi:removeProperty name="BackButton"/>
<ffi:removeProperty name="NextButton"/>
--%>
<ffi:setProperty name="BackURL" value="${SecurePath}user/user_addedit_transfers.jsp" URLEncrypt="true"/>
</ffi:cinclude>