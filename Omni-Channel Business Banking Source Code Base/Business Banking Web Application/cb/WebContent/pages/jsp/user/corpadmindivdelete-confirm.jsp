<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:help id="user_corpadmindivdelete-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_82')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	
	<ffi:object id="CanDeleteEntitlementGroup" name="com.ffusion.tasks.admin.CanDeleteEntitlementGroup" scope="session"/>
	<ffi:process name="CanDeleteEntitlementGroup"/>
	<ffi:object id="DeleteAutoEntitleSettings" name="com.ffusion.tasks.autoentitle.DeleteAutoEntitleSettings" scope="session"/>
	<ffi:setProperty name="DeleteAutoEntitleSettings" property="EntitlementGroupSessionKey" value="<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>"/>
	<ffi:process name="DeleteAutoEntitleSettings"/>
	<ffi:process name="DeleteBusinessGroup"/>

</ffi:cinclude>


<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:object id="AddBusinessGroupToDA" name="com.ffusion.tasks.dualapproval.AddBusinessGroupToDA" scope="session"/>
	<ffi:setProperty name="AddBusinessGroupToDA" property="UserAction" value="<%= String.valueOf(IDualApprovalConstants.USER_ACTION_DELETED) %>"/>
	<ffi:setProperty name="AddBusinessGroupToDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
	<ffi:setProperty name="AddBusinessGroupToDA" property="ItemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:process name="AddBusinessGroupToDA"/>
	<ffi:removeProperty name="AddBusinessGroupToDA"/>
</ffi:cinclude>

<div align="center">

	<TABLE class="adminBackground" width="480" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD>
				<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
					<br>
					<TR>
						<TD class="columndata" align="center">
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmindivdelete-confirm.jsp-1" parm0="${DivisionToDeleteName}"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmindivdelete-confirm.jsp-2" parm0="${DivisionToDeleteName}"/>
							</ffi:cinclude>
						</TD>
					</TR>
					<TR>
						<TD align="center">
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a id="closeDeleteDivLinkConfirm" button="true"
								  onClickTopics="closeDialog,closeDeleteDivisionDialog" title="%{getText('Close_Window')}"
								  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_175" /></sj:a>
							</div>
						</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>
	<p></p>
</div>

<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadmindiv-pre.jsp"/>
