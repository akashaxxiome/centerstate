<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.COMPANY_SUMMARY %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
		<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
		<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
		<ffi:process name="ThrowException"/>
</ffi:cinclude>

<s:include value="inc/index-pre.jsp"/>
<script src="<s:url value='/web/js/user/admin%{#session.minVersion}.js'/>" type="text/javascript"></script>
<script src="<s:url value='/web/js/user/admin_grid%{#session.minVersion}.js'/>" type="text/javascript"></script>

<div id="desktop" align="center">

    <style type="text/css">
        .indent1 { padding-left: 2em; }
        .indent2 { padding-left: 4em; }
        .indent3 { padding-left: 6em; }
        .indent4 { padding-left: 8em; }
    </style>

	 
	<div id="appdashboard">
		<s:include value="admin_summary_dashboard.jsp"/>
	</div>
	
	<div id="operationresult">
		<div id="resultmessage"></div>
	</div>

	<div id="details">
		<s:include value="admin_details.jsp"/>
	</div>
	<div id="summary">
	<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
		<s:include value="admin_summary.jsp"/>
	</ffi:cinclude>
	</div>
</div>
<%-- Admin Summary View dialog --%>
<sj:dialog 
	id="viewSummaryByGroupIdDialog"
	cssClass="adminDialog" 
	title="%{getText('jsp.user_33')}"
	modal="true"
	resizable="false"
	autoOpen="false"
	closeOnEscape="true"
	showEffect="fold"
	hideEffect="clip"
	width="auto"
	height="600" 
	cssStyle="overflow:hidden;">
</sj:dialog>

<script>
	$('#viewSummaryByGroupIdDialog').addHelp(function(){
		var helpFile = $('#viewSummaryByGroupIdDialog').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	$(document).ready(function(){
		ns.admin.showAdminSummaryDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	});
</script>