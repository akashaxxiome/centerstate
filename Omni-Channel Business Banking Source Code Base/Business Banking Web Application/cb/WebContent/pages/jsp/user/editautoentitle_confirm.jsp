<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroup"%>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<div id="editautoentitleConfirmDiv" class="editautoentitleHelpCSS">
<ffi:help id="user_editautoentitle_confirm" className="moduleHelpClass"/>
<ffi:setProperty name='PageHeading' value='Verify Auto-Entitlement Options'/>


<%
    boolean disableACH = false;
    boolean disableWires = false;
    boolean disableCashCon = false;
    int viewCashConCount = 0;
    String checked = "";
%>
	<%-- get the restrictive entitlements for EFS level --%>
	<ffi:object id="RestrictiveEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session" />

    <s:if test="#session.BusinessEmployee.UsingEntProfiles">
    	<s:if test="%{#session.Section == 'Profiles'}">
    		<ffi:object id="RestrictiveEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedAutoEntitlements" scope="session" />
    		<ffi:setProperty name="RestrictiveEntitlements" property="UsingEntProfiles" value="true"/>
    		<ffi:setProperty name="RestrictiveEntitlements" property="ChannelId" value="${ChannelId}"/>
    	</s:if>
    	<s:if test="%{#session.Section == 'UserProfile'}">
    		<ffi:object id="RestrictiveEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedAutoEntitlements" scope="session" />
    		<ffi:setProperty name="RestrictiveEntitlements" property="UsingEntProfiles" value="true"/>
    		<ffi:setProperty name="RestrictiveEntitlements" property="ChannelId" value="${ChannelId}"/>
    	</s:if>
    </s:if>

    <ffi:setProperty name="RestrictiveEntitlements" property="GroupId" value="1"/>
    <ffi:process name="RestrictiveEntitlements"/>
    <ffi:list collection="Entitlement_Restricted_list" items="items">
        <ffi:cinclude value1="${items.OperationName}" value2="<%=EntitlementsDefines.WIRES%>">
            <% disableWires = true; %>
        </ffi:cinclude>
        <ffi:cinclude value1="${items.OperationName}" value2="<%=EntitlementsDefines.ACH_BATCH%>">
            <% disableACH = true; %>
        </ffi:cinclude>
        <ffi:cinclude value1="${items.OperationName}" value2="<%=EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY%>">
            <% viewCashConCount++; %>
        </ffi:cinclude>
        <ffi:cinclude value1="${items.OperationName}" value2="<%=EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST%>">
            <% viewCashConCount++; %>
        </ffi:cinclude>
    </ffi:list>
    <% if (viewCashConCount == 2) disableCashCon = true; %>


<%!
public boolean isNullOrEmpty(String s) {
	if (s == null || s.length() == 0) return true;
	else return false;
}
%>

<%-- Validate and process the AddEdit task here --%>


<%-- Validate and process the AddEdit task here --%>
<%
// handle the checkboxes.  Unchecked checkboxes are not submitted at all, and will be null on the request... not "false".
String EnableAccounts = "true"; String EnableAccountGroups = "true"; String EnablePermissions = "true"; String EnableLocations = "true";
String EnableACHCompanies = "true"; String EnableWireTemplates = "true"; String holidays = "true"; String EnableProfiles = "true";
if (isNullOrEmpty(request.getParameter("ModifyAutoEntitleSettings.EnableAccounts"))) EnableAccounts = "false";
if (isNullOrEmpty(request.getParameter("ModifyAutoEntitleSettings.EnableAccountGroups"))) EnableAccountGroups = "false";
if (isNullOrEmpty(request.getParameter("ModifyAutoEntitleSettings.EnablePermissions"))) EnablePermissions = "false";
if (isNullOrEmpty(request.getParameter("ModifyAutoEntitleSettings.EnableLocations"))) EnableLocations = "false";
if (isNullOrEmpty(request.getParameter("ModifyAutoEntitleSettings.EnableACHCompanies"))) EnableACHCompanies = "false";
if (isNullOrEmpty(request.getParameter("ModifyAutoEntitleSettings.EnableWireTemplates"))) EnableWireTemplates = "false";
if (isNullOrEmpty(request.getParameter("ModifyAutoEntitleSettings.EnableProfiles"))) EnableProfiles = "false";
%>

<ffi:setProperty name="ModifyAutoEntitleSettings" property="EnableAccounts" value="<%= EnableAccounts %>"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EnableAccountGroups" value="<%= EnableAccountGroups %>"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EnablePermissions" value="<%= EnablePermissions %>"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EnableLocations" value="<%= EnableLocations %>"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EnableACHCompanies" value="<%= EnableACHCompanies %>"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EnableWireTemplates" value="<%= EnableWireTemplates %>"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EnableProfiles" value="<%= EnableProfiles %>"/>
 <% com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings ModifyAutoEntitleSettings = (com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings)session.getAttribute( "ModifyAutoEntitleSettings" ); %>
<div align="center"><ffi:getProperty name="Context"/></div>
<div class="marginleft5"><s:text name="jsp.user_259"/></div>
	<s:form id="EditAutoEntitleFormVerify" name="EditAutoEntitleFormVerify" namespace="/pages/user" action="modifyAutoEntitleSettings-execute" validate="false" theme="simple" method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<table width="100%" border="0"  cellspacing="0" cellpadding="2" class="tableData marginTop10">	
					<ffi:setProperty name="displayAutoEntitlement" value="true"/>
			    	 <s:if test="#session.BusinessEmployee.UsingEntProfiles">
				    	<%
					       checked = (ModifyAutoEntitleSettings.getEnableProfiles().equals( "true" ) ) ? "checked" : "";
					    %>
					    <tr>
					    	<ffi:setProperty name="displayAutoEntitlement" value="false"/>
							<td class="sectionsubhead" align="center"><div><input type="checkbox" disabled <%= checked %> name="ModifyAutoEntitleSettings.EnableProfiles" value="true" border="0">&nbsp;Auto Entitlement</div></td>
					    </tr>
					</s:if>
				    <ffi:cinclude value1="${displayAutoEntitlement}" value2="true">
					    <% 
					       if(ModifyAutoEntitleSettings.getParentEnableAccounts().equals( "true" ) ) { 
						    checked = (ModifyAutoEntitleSettings.getEnableAccounts().equals( "true" ) ) ? "checked" : ""; %>
							<td width="16%" ><input type="checkbox" disabled <%= checked %> name="ModifyAutoEntitleSettings.EnableAccounts" value="true" border="0">&nbsp;<s:text name="jsp.user_13"/></td>
					    <% } %>
					    <% 
					       if(ModifyAutoEntitleSettings.getParentEnableAccountGroups().equals( "true" ) ) { 
						    checked = (ModifyAutoEntitleSettings.getEnableAccountGroups().equals( "true" ) ) ? "checked" : ""; %>
							<td width="16%" ><input type="checkbox" disabled <%= checked %> name="ModifyAutoEntitleSettings.EnableAccountGroups" value="true"  border="0">&nbsp;<s:text name="jsp.user_7"/></td>
					    <% } %>
					    <% 
					       if(ModifyAutoEntitleSettings.getParentEnableACHCompanies().equals( "true" ) && !disableACH) {
						    checked = (ModifyAutoEntitleSettings.getEnableACHCompanies().equals( "true" ) ) ? "checked" : ""; %>
							<td width="16%" ><input type="checkbox" disabled <%= checked %> name="ModifyAutoEntitleSettings.EnableACHCompanies" value="true"  border="0">&nbsp;<s:text name="jsp.user_14"/></td>
					    <% } %>
					    <% if(ModifyAutoEntitleSettings.getParentEnableLocations().equals( "true" ) && !disableCashCon ) {
						     checked = (ModifyAutoEntitleSettings.getEnableLocations().equals( "true" ) ) ? "checked" : ""; %>
							<td width="16%" ><input type="checkbox" disabled <%= checked %> name="ModifyAutoEntitleSettings.EnableLocations" value="true"  border="0">&nbsp;<s:text name="jsp.default_563"/></td>
					    <% } %>
					    <% if(ModifyAutoEntitleSettings.getParentEnablePermissions().equals( "true" ) ) { 
						    checked = (ModifyAutoEntitleSettings.getEnablePermissions().equals( "true" ) ) ? "checked" : ""; %>
							<td width="16%" ><input type="checkbox" disabled <%= checked %> name="ModifyAutoEntitleSettings.EnablePermissions" value="true"  border="0">&nbsp;<s:text name="jsp.default_569"/></td>
					    <% } %>
					    <% if(ModifyAutoEntitleSettings.getParentEnableWireTemplates().equals( "true" ) && !disableWires ) {
						     checked = (ModifyAutoEntitleSettings. getEnableWireTemplates().equals( "true" ) ) ? "checked" : ""; %>
							<td width="16%" ><input type="checkbox" disabled <%= checked %> name="ModifyAutoEntitleSettings.EnableWireTemplates" value="true"  border="0">&nbsp;<s:text name="jsp.default_580"/></td>
					    <% } %>
					</ffi:cinclude>
					</table>
		<%-- 	<tr><td><hr size="1" width="50%"></hr></td></tr>
		    <tr>
				<td>
				<div align="center" style="padding-top: 6px; padding-bottom: 16px;" class="permissionButtonAlign">
					<s:url id="inputUrl" value="%{#session.PagesPath}user/editautoentitle.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="InitAE" value="%{'skip'}"/>
					</s:url>
					<sj:a
						href="%{inputUrl}"
						targets="Auto_Entitlement"
						button="true"
						buttonIcon="ui-icon-arrowreturnthick-1-w"
						onCompleteTopics="backButtonTopic"
						><s:text name="jsp.default_57"/></sj:a>
					<sj:a
						button="true" 
						buttonIcon="ui-icon-close"
						onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
						><s:text name="jsp.default_102"/></sj:a>
					<sj:a
						formIds="EditAutoEntitleFormVerify"
						targets="resultmessage"
						button="true" 
						buttonIcon="ui-icon-check"
						validate="false"
						onBeforeTopics="beforeSubmit" 
						onErrorTopics="errorSubmit"
						onSuccessTopics="completedAutoEntitleEdit"
						><s:text name="jsp.default_366"/></sj:a>
				</div>
				</td>
		    </tr> --%>
<div class="marginTop10"></div>
<div class="btn-row">
<s:url id="inputUrl" value="%{#session.PagesPath}user/editautoentitle.jsp" escapeAmp="false">
<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
<s:param name="InitAE" value="%{'skip'}"/>
<s:param name="UseLastRequest" value="%{'true'}"/>
</s:url>
<sj:a
	href="%{inputUrl}"
	targets="Auto_Entitlement"
	button="true"
	onCompleteTopics="backButtonTopic"
	><s:text name="jsp.default_57"/></sj:a>
<sj:a
	button="true" 
	onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
	><s:text name="jsp.default_102"/></sj:a>
<sj:a
	formIds="EditAutoEntitleFormVerify"
	targets="resultmessage"
	button="true" 
	validate="false"
	onBeforeTopics="beforeSubmit" 
	onErrorTopics="errorSubmit"
	onSuccessTopics="completedAutoEntitleEdit"
	><s:text name="jsp.default_366"/></sj:a>
</div>
	</s:form>
</div>
