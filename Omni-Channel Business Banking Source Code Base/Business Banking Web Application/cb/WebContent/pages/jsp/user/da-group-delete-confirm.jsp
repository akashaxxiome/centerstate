<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:setL10NProperty name='PageHeading' value='Confirm Delete Group'/>

<%		
	if( request.getParameter("itemId") != null ) { 
		String itemId = request.getParameter("itemId");
		session.setAttribute("itemId", itemId); 
	}
	
	if( request.getParameter("GroupName") != null ) { 
		String GroupName = request.getParameter("GroupName");
		session.setAttribute("GroupName", GroupName); 
	}

%>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.businessID}"/>
<ffi:process name="GetDAItem" />

<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
	<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${itemId}'/>
	<ffi:process name="GetEntitlementGroup"/>
	
	<ffi:object id="CanDeleteEntitlementGroup" name="com.ffusion.tasks.admin.CanDeleteEntitlementGroup" scope="session"/>
	<ffi:process name="CanDeleteEntitlementGroup"/>
	
	<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${itemId}" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="approveDAItem" value="true" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="groupName" value="${GroupName}" />
	<ffi:process name="ApprovePendingChangesTask"/>
	<ffi:removeProperty name="ApprovePendingChangesTask"/>
	
	
	<ffi:object id="DeleteAutoEntitleSettings" name="com.ffusion.tasks.autoentitle.DeleteAutoEntitleSettings" scope="session"/>
	<ffi:setProperty name="DeleteAutoEntitleSettings" property="EntitlementGroupSessionKey" value="<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>"/>
	<ffi:setProperty name="DeleteBusinessGroup" property="SuccessURL" value=""/>
	<ffi:process name="DeleteAutoEntitleSettings"/>
	<ffi:process name="DeleteBusinessGroup"/>
	

</ffi:cinclude>

<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-group-delete-confirm.jsp-1" parm0="${GroupName}"/>

<ffi:removeProperty name="Entitlement_EntitlementGroup"/>
<ffi:removeProperty name="DeleteAutoEntitleSettings"/>
<ffi:removeProperty name="DAItem"/>