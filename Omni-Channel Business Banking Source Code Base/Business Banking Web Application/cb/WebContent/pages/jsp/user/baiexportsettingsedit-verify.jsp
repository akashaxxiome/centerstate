<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:help id="user_baiexportsettingsedit-verify" className="moduleHelpClass"/>
<% 
	request.setAttribute("senderIDType", request.getParameter("senderIDType"));
	request.setAttribute("senderIDCustom", request.getParameter("senderIDCustom"));
	request.setAttribute("receiverIDType", request.getParameter("receiverIDType"));
	request.setAttribute("receiverIDCustom", request.getParameter("receiverIDCustom"));
	request.setAttribute("ultimateReceiverIDType", request.getParameter("ultimateReceiverIDType"));
	request.setAttribute("ultimateReceiverIDCustom", request.getParameter("ultimateReceiverIDCustom"));
	request.setAttribute("originatorIDType", request.getParameter("originatorIDType"));
	request.setAttribute("originatorIDCustom", request.getParameter("originatorIDCustom"));
	request.setAttribute("customerAccountNumberType", request.getParameter("customerAccountNumberType"));
	request.setAttribute("senderIDCustomValue", request.getParameter("senderIDCustomValue"));
	request.setAttribute("receiverIDCustomValue", request.getParameter("receiverIDCustomValue"));
	request.setAttribute("ultimateReceiverIDCustomValue", request.getParameter("ultimateReceiverIDCustomValue"));
	request.setAttribute("originatorIDCustomValue", request.getParameter("originatorIDCustomValue"));
	request.setAttribute("oldSenderIDType", request.getParameter("oldSenderIDType"));
	request.setAttribute("oldSenderIDCustomValue", request.getParameter("oldSenderIDCustomValue"));
	request.setAttribute("oldReceiverIDType", request.getParameter("oldReceiverIDType"));
	request.setAttribute("oldReceiverIDCustomValue", request.getParameter("oldReceiverIDCustomValue"));
	request.setAttribute("oldUltimateReceiverIDType", request.getParameter("oldUltimateReceiverIDType"));
	request.setAttribute("oldUltimateReceiverIDCustomValue", request.getParameter("oldUltimateReceiverIDCustomValue"));
	request.setAttribute("oldOriginatorIDType", request.getParameter("oldOriginatorIDType"));
	request.setAttribute("oldOriginatorIDCustomValue", request.getParameter("oldOriginatorIDCustomValue"));
	request.setAttribute("oldCustomerAccountNumberType", request.getParameter("oldCustomerAccountNumberType"));
%>
<ffi:object id="SetBAIExportSettings" name="com.ffusion.tasks.user.SetBAIExportSettings" scope="session"/>
<ffi:setProperty name="SetBAIExportSettings" property="BusinessSessionName" value="Business" />
<ffi:setProperty name="SetBAIExportSettings" property="SenderIDType" value="${senderIDType}"/>
<ffi:setProperty name="SetBAIExportSettings" property="SenderIDCustom" value="${senderIDCustom}"/>
<ffi:setProperty name="SetBAIExportSettings" property="ReceiverIDType" value="${receiverIDType}"/>
<ffi:setProperty name="SetBAIExportSettings" property="ReceiverIDCustom" value="${receiverIDCustom}"/>
<ffi:setProperty name="SetBAIExportSettings" property="UltimateReceiverIDType" value="${ultimateReceiverIDType}"/>
<ffi:setProperty name="SetBAIExportSettings" property="UltimateReceiverIDCustom" value="${ultimateReceiverIDCustom}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OriginatorIDType" value="${originatorIDType}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OriginatorIDCustom" value="${originatorIDCustom}"/>
<ffi:setProperty name="SetBAIExportSettings" property="CustomerAccountNumberType" value="${customerAccountNumberType}"/>
<%-- Dual Approval Processing --%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:object id="NewBAIExportSettings" name="com.ffusion.tasks.user.GetBAIExportSettings" scope="session"/>
	<ffi:setProperty name="NewBAIExportSettings" property="BusinessSessionName" value="Business" />
	<ffi:setProperty name="NewBAIExportSettings" property="SenderIDType" value="${senderIDType}"/>
	<ffi:setProperty name="NewBAIExportSettings" property="SenderIDCustom" value="${senderIDCustom}"/>
	<ffi:setProperty name="NewBAIExportSettings" property="ReceiverIDType" value="${receiverIDType}"/>
	<ffi:setProperty name="NewBAIExportSettings" property="ReceiverIDCustom" value="${receiverIDCustom}"/>
	<ffi:setProperty name="NewBAIExportSettings" property="UltimateReceiverIDType" value="${ultimateReceiverIDType}"/>
	<ffi:setProperty name="NewBAIExportSettings" property="UltimateReceiverIDCustom" value="${ultimateReceiverIDCustom}"/>
	<ffi:setProperty name="NewBAIExportSettings" property="OriginatorIDType" value="${originatorIDType}"/>
	<ffi:setProperty name="NewBAIExportSettings" property="OriginatorIDCustom" value="${originatorIDCustom}"/>
	<ffi:setProperty name="NewBAIExportSettings" property="CustomerAccountNumberType" value="${customerAccountNumberType}"/>
</ffi:cinclude>

<%-- END Dual Approval Processing --%>
<ffi:removeProperty name="senderIDType"/>
<ffi:removeProperty name="senderIDCustomValue"/>
<ffi:removeProperty name="receiverIDType"/>
<ffi:removeProperty name="receiverIDCustomValue"/>
<ffi:removeProperty name="ultimateReceiverIDType"/>
<ffi:removeProperty name="ultimateReceiverIDCustomValue"/>
<ffi:removeProperty name="originatorIDType"/>
<ffi:removeProperty name="originatorIDCustomValue"/>
<ffi:removeProperty name="customerAccountNumberType"/>
<ffi:setProperty name="SetBAIExportSettings" property="OldSenderIDType" value="${oldSenderIDType}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OldSenderIDCustom" value="${oldSenderIDCustomValue}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OldReceiverIDType" value="${oldReceiverIDType}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OldReceiverIDCustom" value="${oldReceiverIDCustomValue}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OldUltimateReceiverIDType" value="${oldUltimateReceiverIDType}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OldUltimateReceiverIDCustom" value="${oldUltimateReceiverIDCustomValue}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OldOriginatorIDType" value="${oldOriginatorIDType}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OldOriginatorIDCustom" value="${oldOriginatorIDCustomValue}"/>
<ffi:setProperty name="SetBAIExportSettings" property="OldCustomerAccountNumberType" value="${oldCustomerAccountNumberType}"/>
<ffi:removeProperty name="oldSendIDType"/>
<ffi:removeProperty name="oldSenderIDCustomValue"/>
<ffi:removeProperty name="oldReceiverIDType"/>
<ffi:removeProperty name="oldReceiverIDCustomValue"/>
<ffi:removeProperty name="oldUltimateReceiverIDType"/>
<ffi:removeProperty name="oldUltimateReceiverIDCustomValue"/>
<ffi:removeProperty name="oldOriginatorIDType"/>
<ffi:removeProperty name="oldOriginatorIDCustomValue"/>
<ffi:removeProperty name="oldCustomerAccountNumberType"/>

<ffi:setProperty name="SetBAIExportSettings" property="Validate" value="true"/>
<ffi:process name="SetBAIExportSettings"/>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<% session.setAttribute("FFIBAIExportSettings", session.getAttribute("SetBAIExportSettings"));%>
<% session.setAttribute("FFIAddBAIExportSettingsToDA", session.getAttribute("AddBAIExportSettingsToDA"));%>

	<div align="center">
	
		<s:form id="editBAIExportSettingsVerifyForm" namespace="/pages/user" name="editBAIExportSettingsVerifyForm" action="setBAIExportSettings-execute" validate="false" method="post" theme="simple">
        <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminBackground formTablePadding10 tableData">
		<tr>
			<td width="50%">
			<span class="sectionhead sectLabel" >
				<s:text name="jsp.user_295"/>:
			</span>
			<span class="sectValue">
				<ffi:cinclude value1="${SetBAIExportSettings.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					<s:text name="jsp.user_357"/> 
					<ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${SetBAIExportSettings.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
					<ffi:getProperty name="SetBAIExportSettings" property="SenderIDCustom"/>
				</ffi:cinclude>
			</span>
			</td>
			<td width="50%">
				<span class="sectionhead sectLabel" >
					<s:text name="jsp.user_270"/>:
				</span>
				<span class="sectValue columndata">
					<ffi:cinclude value1="${SetBAIExportSettings.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
						<s:text name="jsp.user_358"/>
						<ffi:getProperty name="TempBankIdentifierDisplayText"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${SetBAIExportSettings.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
						<ffi:getProperty name="SetBAIExportSettings" property="ReceiverIDCustom"/>
					</ffi:cinclude>
				</span>
			</td>
		</tr>
		<tr>
			<td width="50%">
				<span class="sectionhead sectLabel" >
					<s:text name="jsp.user_348"/>:
				</span>
				<span class="sectValue columndata">
					<ffi:cinclude value1="${SetBAIExportSettings.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
						<s:text name="jsp.user_357"/> 
						<ffi:getProperty name="TempBankIdentifierDisplayText"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${SetBAIExportSettings.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
						<ffi:getProperty name="SetBAIExportSettings" property="UltimateReceiverIDCustom"/>
					</ffi:cinclude>
				</span>
			</td>
			<td width="50%">
				<span class="sectionhead sectLabel" >
					<s:text name="jsp.user_232"/>:
				</span>
				<span class="sectValue columndata">
					<ffi:cinclude value1="${SetBAIExportSettings.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
						<s:text name="jsp.user_355"/>
						<ffi:getProperty name="TempBankIdentifierDisplayText"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${SetBAIExportSettings.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
						<ffi:getProperty name="SetBAIExportSettings" property="OriginatorIDCustom"/>
					</ffi:cinclude>
				</span>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<span class="sectionhead sectLabel" >
					<s:text name="jsp.user_100"/>:
				</span>
				<span class="sectValue columndata">
					<ffi:cinclude value1="${SetBAIExportSettings.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" operator="equals">
						<s:text name="jsp.user_356"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${SetBAIExportSettings.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" operator="equals">										
						<s:text name="jsp.user_352"/> 
						<ffi:getProperty name="TempBankIdentifierDisplayText"/>
						 <s:text name="jsp.user_43"/>
					</ffi:cinclude>
				</span>
			</td>
		</tr>
	</table>
						<%-- <tr>
							<td align="center" colspan="5"><br>
							<sj:a
                                button="true"
                                onClickTopics="backToInput"
                                ><s:text name="jsp.default_57"/></sj:a>
							<sj:a 
								button="true"
								onClickTopics="cancelCompanyForm"
								><s:text name="jsp.default_82"/></sj:a>
							<sj:a id="editBAIExportSettingsSubmit"
                                formIds="editBAIExportSettingsVerifyForm"
                                targets="confirmDiv"
								button="true"
								validateFunction="customValidation"
								onBeforeTopics="beforeSubmit"
								onSuccessTopics="completeBAIExportSettingsSubmit"
								onErrorTopics="errorSubmit"
                                ><s:text name="jsp.default_107"/></sj:a>
						</TR> --%>
<div class="btn-row">
<sj:a
button="true"
onClickTopics="backToInput"
><s:text name="jsp.default_57"/></sj:a>
<sj:a 
	button="true" summaryDivId="companySummaryTabsContent" buttonType="cancel"
	onClickTopics="showSummary,cancelCompanyForm,cancelEditBAICompanyForm"
	><s:text name="jsp.default_82"/></sj:a>
<sj:a id="editBAIExportSettingsSubmit"
                         formIds="editBAIExportSettingsVerifyForm"
                         targets="confirmDiv"
	button="true"
	validateFunction="customValidation"
	onBeforeTopics="beforeSubmit"
	onSuccessTopics="completeBAIExportSettingsSubmit"
	onErrorTopics="errorSubmit"
                         ><s:text name="jsp.default_107"/></sj:a>
</div>
		</s:form>
	</DIV>
