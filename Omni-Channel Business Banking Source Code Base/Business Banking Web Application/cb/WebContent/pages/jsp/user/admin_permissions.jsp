<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroup"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.sap.banking.web.util.entitlements.EntitlementsUtil"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<%-- remove objects used by the edit ACH company forms from the session --%>
<ffi:removeProperty name="EditACHCompanyAccess"/>
<ffi:removeProperty name="ApprovalAdminByBusiness"/>
<ffi:removeProperty name="CheckForRedundantACHLimits"/>
<ffi:removeProperty name="crossACHEntLists"/>
<ffi:removeProperty name="perACHEntLists"/>
<ffi:removeProperty name="limitInfoList"/>
<ffi:removeProperty name="ACHCompanyID"/>
<ffi:removeProperty name="ACHCompany"/>
<ffi:removeProperty name="perBatchMax"/>
<ffi:removeProperty name="dailyMax"/>
<ffi:removeProperty name="ChildParentMap"/>

<% boolean showAutoEntitle = true; %>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
    <ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<%
	if(session.getAttribute(com.ffusion.struts.admin.GetAccountAccessPermissions.REQ_SYNC) == null) 
		session.setAttribute(com.ffusion.struts.admin.GetAccountAccessPermissions.REQ_SYNC, new Object());
	
%>
<ffi:object id="ModifyAutoEntitleSettings" name="com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings" scope="session"/>

<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EntitlementGroupSessionKey" value="<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>"/>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
<ffi:process name="GetEntitlementGroup"/>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	
	<!-- Get Business Employees -->
	<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
	<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
	<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
	<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
	<ffi:setProperty name="GetBusinessEmployees" property="SearchEntitlementProfiles" value="TRUE"/>
	<ffi:setProperty name="GetBusinessEmployees" property="BusinessEmployeesSessionName" value="fullEmployeeList"/>
	<ffi:process name="GetBusinessEmployees"/>
	<ffi:setProperty name="GetBusinessEmployees" property="BusinessEmployeesSessionName" value="entitlementProfiles"/>
	<ffi:process name="GetBusinessEmployees"/>
	<ffi:removeProperty name="GetBusinessEmployees"/>
	<ffi:removeProperty name="SearchBusinessEmployee"/>
	<!-- Get Business Employees Ends -->
	
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" scope="session" />
	<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${BusinessEmployeeId}"/>
	<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeesSessionName" value="fullEmployeeList"/>
	<ffi:process name="SetBusinessEmployee"/>
	<ffi:setProperty name="ModifyAutoEntitleSettings" property="EntitlementGroupMemberSessionKey" value="ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE"/>
	
	<% com.ffusion.beans.user.BusinessEmployee businessEmployee = (com.ffusion.beans.user.BusinessEmployee) session.getAttribute("BusinessEmployee");
	    EntitlementGroupMember member = EntitlementsUtil.getEntitlementGroupMemberForBusinessEmployee(businessEmployee);
	    session.setAttribute( "ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE", member );
	%>
</s:if>
<ffi:process name="ModifyAutoEntitleSettings"/>
<%
 com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings ModifyAutoEntitleSettings = (com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings)session.getAttribute( "ModifyAutoEntitleSettings" ); 
 if( ModifyAutoEntitleSettings.getParentAnyPermissionEnabled().equals( "false" ) ) {
     showAutoEntitle = false; 
 }
 %>
 <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
 <s:if test="%{#session.Section == 'Users'}">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.Id}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>" />
		<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="true" />
		<ffi:setProperty name="GetDACategoryDetails" property="MultipleCategoriesSessionName" value="EntitlementBean" />
	<ffi:process name="GetDACategoryDetails"/>
 </s:if>
 <ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.Id}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>" />
		<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="true" />
		<ffi:setProperty name="GetDACategoryDetails" property="MultipleCategoriesSessionName" value="EntitlementBean" />
	<ffi:process name="GetDACategoryDetails"/>
 </ffi:cinclude>
 <ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
 </ffi:cinclude>
  <ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
 </ffi:cinclude>
  <ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
 </ffi:cinclude>
 </ffi:cinclude>

<%-- Permission remote links of tabs defined here. --%>
	<%-- auto entitlement link --%>
<s:url id="remoteAutoEntitle" value="%{#session.PagesPath}user/editautoentitle.jsp" escapeAmp="false"/>
	<%-- account access edit link --%>
<s:url id="remoteAcctAccessEdit" value="%{#session.PagesPath}user/accountaccess.jsp" escapeAmp="false"/>
	<%-- account access view link --%>
<s:url id="remoteAcctAccessView" value="%{#session.PagesPath}user/accountaccess-view.jsp" escapeAmp="false"/>
	<%-- account group view link --%>
<s:url id="remoteAcctGroupAccessView" value="%{#session.PagesPath}user/accountgroupaccess-view.jsp" escapeAmp="false"/>
	<%-- account group edit link --%>
<s:url id="remoteAcctGroupAccessEdit" value="%{#session.PagesPath}user/accountgroupaccess.jsp" escapeAmp="false"/>
	<%-- cross ach company view link --%>
<s:url id="remoteXACHCompanyView" value="%{#session.PagesPath}user/crossachcompanyaccess-view.jsp" escapeAmp="false"/>
	<%-- cross ach company edit link --%>
<s:url id="remoteXACHCompanyEdit" value="%{#session.PagesPath}user/crossachcompanyaccess.jsp" escapeAmp="false"/>
	<%-- per ach company view link --%>
<s:url id="remoteACHCompanyView" value="%{#session.PagesPath}user/achcompanyaccess-view.jsp" escapeAmp="false"/>
	<%-- per ach company edit link --%>
<s:url id="remoteACHCompanyEdit" value="%{#session.PagesPath}user/achcompanyaccess.jsp" escapeAmp="false"/>
	<%-- cross account tab link --%>
<s:url id="remoteCrossAccountTabs" value="%{#session.PagesPath}user/permission_crossaccounttabs.jsp" escapeAmp="false"/>
	<%-- per account tab link --%>
<s:url id="remotePerAccountTabs" value="%{#session.PagesPath}user/permission_peraccounttabs.jsp" escapeAmp="false"/>
	<%-- per account group tab link --%>
<s:url id="remotePerAccountGroupTabs" value="%{#session.PagesPath}user/permission_peraccountgroupstabs.jsp" escapeAmp="false"/>
	<%-- per location tab link --%>
<s:url id="remotePerLocation" value="%{#session.PagesPath}user/perlocationpermissions.jsp"/>
<%-- wire template tab link --%>
<s:url id="remoteWireTemp" value="%{#session.PagesPath}user/wiretemplatesaccess.jsp"/>

<% int permcount = 0; %>
<div id="permissionTabsSummary" class="portlet" style="float: left; width: 100%; position:absolute;">
    <div class="portlet-header">
	    <div class="searchHeaderCls">
			<div class="summaryGridTitleHolder">
				<span id="selectedPermissionTab" class="selectedTabLbl">
					<%-- auto entitlement tab --%>
				    <% if( showAutoEntitle ) { %>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="notEquals">
						<s:text name="jsp.user_49"/>
					</ffi:cinclude>
					<%-- account access tab --%>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="equals">
						<s:text name="jsp.user_13"/>
					</ffi:cinclude>
					<% }else{ %>
						<%-- account access tab --%>
						<s:text name="jsp.user_13"/>
					<% } %>
				</span>
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
				
				<div class="gridTabDropdownHolder">
				
					<%-- auto entitlement tab --%>
				    <% if( showAutoEntitle ) { %>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="notEquals">
						<% permcount ++; %>
						<span class="summaryTabDropdownItem" id='autoEntitleTab' onclick="ns.common.showTabSummary('autoEntitleTab','<s:property value="#remoteAutoEntitle"/>')" >
							<s:text name="jsp.user_49"/>
						</span>
					</ffi:cinclude>
					<% } %>
				
					<%-- account access tab --%>
					<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
							<span class="summaryTabDropdownItem" id='acctAccessTab' onclick="ns.common.showTabSummary('acctAccessTab','<s:property value="#remoteAcctAccessView"/>')">
								<s:text name="jsp.user_13"/>
							</span>
							<% permcount ++; %>
						</ffi:cinclude>
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
							<span class="summaryTabDropdownItem" id='acctAccessTab' onclick="ns.common.showTabSummary('acctAccessTab','<s:property value="#remoteAcctAccessEdit"/>')">
								<s:text name="jsp.user_13"/>
							</span>
							<% permcount ++; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
						<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
							<span class="summaryTabDropdownItem" id='acctAccessTab' onclick="ns.common.showTabSummary('acctAccessTab','<s:property value="#remoteAcctAccessView"/>')">
								<s:text name="jsp.user_13"/>
							</span>
							<% permcount ++; %>
						</ffi:cinclude>
						<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
							<span class="summaryTabDropdownItem" id='acctAccessTab' onclick="ns.common.showTabSummary('acctAccessTab','<s:property value="#remoteAcctAccessEdit"/>')">
								<s:text name="jsp.user_13"/>
							</span>
							<% permcount ++; %>
						</ffi:cinclude>
					</ffi:cinclude>
					
					<%-- account group access tab --%>
					<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
							<sj:tab id="acctGroupAccess" href="%{remoteAcctGroupAccessView}" key="Account Groups" label="%{getText('jsp.user_7')}" />
							<span class="summaryTabDropdownItem" id='acctGroupAccessTab' onclick="ns.common.showTabSummary('acctGroupAccessTab','<s:property value="#remoteAcctGroupAccessView"/>')">
								<s:text name="jsp.user_7"/>
							</span>
							<% permcount ++; %>
						</ffi:cinclude>
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
							<sj:tab id="acctGroupAccess" href="%{remoteAcctGroupAccessEdit}" key="Account Groups" label="%{getText('jsp.user_7')}" />
							<span class="summaryTabDropdownItem" id='acctGroupAccessTab' onclick="ns.common.showTabSummary('acctGroupAccessTab','<s:property value="#remoteAcctGroupAccessEdit"/>')">
								<s:text name="jsp.user_7"/>
							</span>
							<% permcount ++; %>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
						<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
							<sj:tab id="acctGroupAccess" href="%{remoteAcctGroupAccessView}" key="Account Groups" label="%{getText('jsp.user_7')}" />
							<span class="summaryTabDropdownItem" id='acctGroupAccessTab' onclick="ns.common.showTabSummary('acctGroupAccessTab','<s:property value="#remoteAcctGroupAccessView"/>')">
								<s:text name="jsp.user_7"/>
							</span>
							<% permcount ++; %>
						</ffi:cinclude>
						<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
							<sj:tab id="acctGroupAccess" href="%{remoteAcctGroupAccessEdit}" key="Account Groups" label="%{getText('jsp.user_7')}" />
							<span class="summaryTabDropdownItem" id='acctGroupAccessTab' onclick="ns.common.showTabSummary('acctGroupAccessTab','<s:property value="#remoteAcctGroupAccessEdit"/>')">
								<s:text name="jsp.user_7"/>
							</span>
							<% permcount ++; %>
						</ffi:cinclude>
					</ffi:cinclude>
					
					<%-- DETERMINE WHETHER TO DISPLAY ACH OR NOT --%>
					<s:include value="%{#session.PagesPath}user/inc/checkdisplayach.jsp" />
					
					<%-- cross ach company and per ach company tabs --%>
					<ffi:cinclude value1="${displayACH}" value2="true" operator="equals">
						<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
								<span class="summaryTabDropdownItem" id='xACHCompanyTab' onclick="ns.common.showTabSummary('xACHCompanyTab','<s:property value="#remoteXACHCompanyView"/>')">
									<s:text name="jsp.user_408"/>
								</span>
								<% permcount ++; %>
							</ffi:cinclude>
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
								<span class="summaryTabDropdownItem" id='xACHCompanyTab' onclick="ns.common.showTabSummary('xACHCompanyTab','<s:property value="#remoteXACHCompanyEdit"/>')">
									<s:text name="jsp.user_408"/>
								</span>
								<% permcount ++; %>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
								<% permcount ++; %>
								<span class="summaryTabDropdownItem" id='xACHCompanyTab' onclick="ns.common.showTabSummary('xACHCompanyTab','<s:property value="#remoteXACHCompanyView"/>')">
									<s:text name="jsp.user_408"/>
								</span>
							</ffi:cinclude>
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
								<span class="summaryTabDropdownItem" id='xACHCompanyTab' onclick="ns.common.showTabSummary('xACHCompanyTab','<s:property value="#remoteXACHCompanyEdit"/>')">
									<s:text name="jsp.user_408"/>
								</span>
								<% permcount ++; %>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
								<span class="summaryTabDropdownItem" id='aCHCompanyTab' onclick="ns.common.showTabSummary('aCHCompanyTab','<s:property value="#remoteACHCompanyView"/>')">
									<s:text name="jsp.user_409"/>
								</span>
								<% permcount ++; %>
							</ffi:cinclude>
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
								<span class="summaryTabDropdownItem" id='aCHCompanyTab' onclick="ns.common.showTabSummary('aCHCompanyTab','<s:property value="#remoteACHCompanyEdit"/>')">
									<s:text name="jsp.user_409"/>
								</span>
								<% permcount ++; %>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
								<span class="summaryTabDropdownItem" id='aCHCompanyTab' onclick="ns.common.showTabSummary('aCHCompanyTab','<s:property value="#remoteACHCompanyView"/>')">
									<s:text name="jsp.user_409"/>
								</span>
								<% permcount ++; %>
							</ffi:cinclude>
							<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
								<span class="summaryTabDropdownItem" id='aCHCompanyTab' onclick="ns.common.showTabSummary('aCHCompanyTab','<s:property value="#remoteACHCompanyEdit"/>')">
									<s:text name="jsp.user_409"/>
								</span>
								<% permcount ++; %>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>		
					
					<% session.setAttribute("permcount", String.valueOf(permcount)); %>	
					
					<%-- cross account sub tabs --%>
					<s:include value="%{#session.PagesPath}user/inc/checkentitled-crossaccountcomponents.jsp" />
					<s:if test="%{!#session.LocalizedComponentNamesCross.isEmpty()}">
						<%-- cross account feature access and limits tab --%>
						<span class="summaryTabDropdownItem" id='crossAccountTab' onclick="ns.common.showTabSummary('crossAccountTab','<s:property value="#remoteCrossAccountTabs"/>')">
							<s:text name="jsp.user_410"/>
						</span>
					</s:if>
					
					<%-- per account sub tabs --%>
					<s:include value="%{#session.PagesPath}user/inc/checkentitled-peraccountcomponents.jsp" />
					<s:if test="%{!#session.LocalizedComponentNamesPer.isEmpty()}">
						<%-- per account feature access and limits --%>
						<span class="summaryTabDropdownItem" id='perAccountTab' onclick="ns.common.showTabSummary('perAccountTab','<s:property value="#remotePerAccountTabs"/>')">
							<s:text name="jsp.user_411"/>
						</span>
					</s:if>
					
					<%-- per account groups sub tabs --%>
					<s:include value="%{#session.PagesPath}user/inc/checkentitled-peraccountgroupcomponents.jsp" />
					<s:if test="%{!#session.LocalizedComponentNamesPer.isEmpty()}">
						<%-- per account group feature access and limits --%>
						<span class="summaryTabDropdownItem" id='perAccountGroupTab' onclick="ns.common.showTabSummary('perAccountGroupTab','<s:property value="#remotePerAccountGroupTabs"/>')">
							<s:text name="jsp.user_412"/>
						</span>
					</s:if>
					
					<%-- Feature Access and Limits (per location) --%>
					<s:include value="%{#session.PagesPath}user/inc/checkentitledlocations.jsp" />
					<ffi:cinclude value1="${CheckEntitlementByGroupCB.Entitled}" value2="TRUE" operator="equals">
						<ffi:cinclude value1="${displayPerLocation}" value2="TRUE" operator="equals">
							<span class="summaryTabDropdownItem" id='perLocationTab' onclick="ns.common.showTabSummary('perLocationTab','<s:property value="#remotePerLocation"/>')">
								<s:text name="jsp.user_413"/>
							</span>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:removeProperty name="CheckEntitlementByGroupCB"/>							
				
					<%  String catSubType = null; %>
	
					<%-- Wire Template Access and Limits --%>
					<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>">
							<span class="summaryTabDropdownItem" id='wireTemplateTab' onclick="ns.common.showTabSummary('wireTemplateTab','<s:property value="#remoteWireTemp"/>')">
								<s:text name="jsp.user_414"/>
							</span>
						</ffi:cinclude>
					</s:if>
					<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>">
				
								<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
								<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
														value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT %>" />
								<ffi:setProperty name="GetDACategorySubType" property="OperationName" 
																					value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>" />
								<ffi:process name="GetDACategorySubType"/>
								<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
					
								<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMemberCB" id="CheckEntitlementByMember" scope="session"/>
								<ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>"/>
								<ffi:setProperty name="CheckEntitlementByMember" property="GroupId" value="${ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE.EntitlementGroupId}"/>
								<ffi:setProperty name="CheckEntitlementByMember" property="MemberId" value="${ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE.Id}"/>
								<ffi:setProperty name="CheckEntitlementByMember" property="MemberType" value="${ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE.MemberType}"/>
								<ffi:setProperty name="CheckEntitlementByMember" property="MemberSubType" value="${ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE.MemberSubType}"/>
								<ffi:setProperty name="CheckEntitlementByMember" property="AttributeName" value="IsEntitledForWires"/>
								<s:if test="%{#session.UsingEntProfiles == 'true'}">
									<ffi:setProperty name="CheckEntitlementByMember" property="UsingEntProfiles" value="true"/>
									<ffi:setProperty name="CheckEntitlementByMember" property="ChannelId" value="${ChannelId}"/>
								</s:if>
								<ffi:setProperty name="CheckEntitlementByMember" property="DaCategorySessionName" value="EntitlementBean"/>
								<ffi:setProperty name="CheckEntitlementByMember" property="DaCategorySubType" value="${catSubType}"/>
								<ffi:process name="CheckEntitlementByMember"/>   	
				
								<ffi:setProperty name="GetDACategorySubType" property="OperationName" value="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_APPROVAL_OVERALL %>" />
								<ffi:process name="GetDACategorySubType"/>
								<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
				
								<ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_APPROVAL_OVERALL%>"/>
								<ffi:setProperty name="CheckEntitlementByMember" property="AttributeName" value="IsEntitledForWireApproval"/>
								<ffi:setProperty name="CheckEntitlementByMember" property="DaCategorySubType" value="${catSubType}"/>
								<ffi:process name="CheckEntitlementByMember"/>
				
								<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
				
								<s:if test="#session.IsEntitledForWires.equals('TRUE') || #session.IsEntitledForWireApproval.equals('TRUE')">
									<span class="summaryTabDropdownItem" id='wireTemplateTab' onclick="ns.common.showTabSummary('wireTemplateTab','<s:property value="#remoteWireTemp"/>')">
										<s:text name="jsp.user_414"/>
									</span>
								</s:if>
								<ffi:removeProperty name="GetDACategorySubType,IsEntitledForWires,IsEntitledForWireApproval"/>
							</ffi:cinclude>
					</s:if>

					<s:if test="%{#session.Section == 'UserProfile'">
						<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" id="checkWireEntitlement"/>
						<ffi:setProperty name="checkWireEntitlement" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
						<ffi:setProperty name="checkWireEntitlement" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>"/>
						<ffi:setProperty name="checkWireEntitlement" property="AttributeName" value="IsEntitledForWires"/>
						<ffi:process name="checkWireEntitlement"/>
						<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" id="checkWireEntitlementForApproval"/>
						<ffi:setProperty name="checkWireEntitlementForApproval" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
						<ffi:setProperty name="checkWireEntitlementForApproval" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_APPROVAL_OVERALL %>"/>
						<ffi:setProperty name="checkWireEntitlementForApproval" property="AttributeName" value="IsEntitledForWireApproval"/>
						<ffi:process name="checkWireEntitlementForApproval"/>
						
						<s:if test="#session.IsEntitledForWires.equals('TRUE') || #session.IsEntitledForWireApproval.equals('TRUE')">
									<span class="summaryTabDropdownItem" id='wireTemplateTab' onclick="ns.common.showTabSummary('wireTemplateTab','<s:property value="#remoteWireTemp"/>')">
										<s:text name="jsp.user_414"/>
									</span>
						</s:if>
						<ffi:removeProperty name="IsEntitledForWires,IsEntitledForWireApproval"/>
					</s:if>


				</div>
			</div>
		</div>
    </div>
    
    <% boolean showAutoEntitleTab = false; %>
    <div class="portlet-content">
		<div id="tabSummaryContainer" class="summaryTabHolderDivCls">
			<% if(showAutoEntitle){%>
				<%-- show auto entitlement tab as first--%>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="notEquals">
					<% showAutoEntitleTab = true; %>
				</ffi:cinclude>
				<%-- account access tab as first--%>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="equals">
					<% showAutoEntitleTab = false; %>
				</ffi:cinclude>
			<% }else{  //account access tab as first
				 showAutoEntitleTab = false;
			} %>
			
			<% if(showAutoEntitleTab){%>
				<%-- auto entitlement tab --%>
				<s:set var="initialTopLevelPermissionTab" value="%{'autoEntitleTab'}" scope="request" />
			<% }else{ %>
				<%-- account access tab --%>
				<s:set var="initialTopLevelPermissionTab" value="%{'acctAccessTab'}" scope="request" />
			<% } %>
		</div>
    </div>
	
	<div id="adminPermissionDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('permissionTabsSummary')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
	
</div>

<script type="text/javascript">
$(document).ready(function(){
	ns.common.initializePortlet("permissionTabsSummary");
	var initialTopLevelPermissionTab = "<s:property value='#request.initialTopLevelPermissionTab' />";
	if(ns.admin.topLavelPermissionTab && ns.admin.topLavelPermissionTab != initialTopLevelPermissionTab){
		$("#"+ns.admin.topLavelPermissionTab).click();
	} else {
		$("#"+initialTopLevelPermissionTab).click();
	}
});
	
</script>

<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>
