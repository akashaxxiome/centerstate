<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadmingroups" className="moduleHelpClass"/>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<div id="hidden"  style="display:none;">
    <s:url id="editGroupUrl" value="/pages/jsp/user/corpadmingroupedit.jsp" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
    </s:url>
    <sj:a id="editGroupLink" href="%{editGroupUrl}" targets="inputDiv" button="true" title="%{getText('jsp.user_143')}"
        onClickTopics="beforeLoad" onCompleteTopics="completeGroupLoad" onErrorTopics="errorLoad">
      <span class="ui-icon ui-icon-wrench"></span>
    </sj:a>
	
	<s:url id="groupPermissionsUrl" value="/pages/jsp/user/admin_permissions.jsp" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	</s:url>
	<sj:a id="groupPermissionsLink" href="%{groupPermissionsUrl}" targets="permissionsDiv" button="true" title="%{getText('jsp.default_569')}"
		onClickTopics="beforePermLoad" onCompleteTopics="completeGroupPermLoad" onErrorTopics="errorLoad">
	  <span class="ui-icon ui-icon-wrench"></span>
	</sj:a>
</div>


<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right" valign="middle" class="columndata">
            <ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
                <s:url id="addGroupURL" value="/pages/jsp/user/corpadmingroupadd.jsp" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="UseLastRequest">FALSE</s:param>
				</s:url>
                <sj:a id="addGroupLink"
                   href="%{addGroupURL}"
                   targets="inputDiv"
                   onClickTopics="beforeLoad" onCompleteTopics="completeGroupLoad"
                   button="true" buttonIcon="ui-icon-add"><s:text name="jsp.user_18"/></sj:a>
            </ffi:cinclude>
        </td>
	</tr>
</table> --%>

<ffi:setGridURL grid="GRID_adminGroup" name="DeleteURL" url="/cb/pages/jsp/user/corpadmingroupdelete-verify.jsp?DeleteGroup_GroupId={0}" parm0="GroupId"/>
<ffi:setGridURL grid="GRID_adminGroup" name="EditURL" url="/cb/pages/jsp/user/corpadmingroupedit.jsp?EditGroup_GroupId={0}&EditGroup_GroupName={1}&EditGroup_DivisionName={2}&EditGroup_Supervisor={3}" parm0="GroupId" parm1="GroupName" parm2="Division" parm3="Supervisor"/>
<ffi:setGridURL grid="GRID_adminGroup" name="PermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Groups&EditGroup_GroupId={0}" parm0="GroupId"/>
<ffi:setGridURL grid="GRID_adminGroup" name="AdminEditURL" url="/cb/pages/jsp/user/corpadminadminedit-selected.jsp?EditDivision_GroupId={0}&AdminEditType=Group" parm0="GroupId"/>

<ffi:setProperty name="adminGroupGridTempURL" value="getBusinessGroups.action?CollectionName=Descendants&GridURLs=GRID_adminGroup" URLEncrypt="true"/>
<s:url namespace="/pages/user" id="remoteurl" action="%{#session.adminGroupGridTempURL}" escapeAmp="false"/>
     <sjg:grid
       id="adminGroupGrid"
       caption=""
	   sortable="true"  
       dataType="json"
       href="%{remoteurl}"
       pager="true"
       viewrecords="true"
       gridModel="gridModel"
	   rowList="%{#session.StdGridRowList}" 
	   rowNum="%{#session.StdGridRowNum}" 
       rownumbers="false"
       scroll="false"
       altRows="true"
       sortname="NAME"
       sortorder="asc"
       shrinkToFit="true"
       navigator="true"
	   navigatorSearch="false"
       navigatorAdd="false"
       navigatorDelete="false"
       navigatorEdit="false"
       navigatorRefresh="false"
       onGridCompleteTopics="addGridControlsEvents"
     >
     <sjg:gridColumn name="groupName" width="300" index="NAME" title="%{getText('jsp.default_283')}" sortable="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.Administrator" width="150" index="administrator" title="%{getText('jsp.user_38')}" sortable="false" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.Division" width="150" index="Division" title="%{getText('jsp.default_174')}" sortable="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatGroupActionLinks" search="false" width="100"  hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
	 <sjg:gridColumn name="map.IsAnAdminOf" index="IsAnAdminOf" title="%{getText('jsp.user_184')}" sortable="false" hidden="true" hidedlg="true" width="0" cssClass="datagrid_actionIcons"/>
	 <sjg:gridColumn name="map.Supervisor" index="Supervisor" title="%{getText('jsp.user_300')}" sortable="false" hidden="true" hidedlg="true" width="0" cssClass="datagrid_actionIcons"/>


 </sjg:grid>


<script type="text/javascript">
	var fnCustomGroupRefresh = function (event) {
		ns.common.forceReloadCurrentSummary();
	};
	
	$("#adminGroupGrid").data('fnCustomRefresh', fnCustomGroupRefresh);
	
	$("#adminGroupGrid").data("supportSearch",true);

	//disable sortable rows
	if(ns.common.isInitialized($("#adminGroupGrid tbody"),"ui-jqgrid")){
		$("#adminGroupGrid tbody").sortable("destroy");	
	}
	

	$("#adminGroupGrid").jqGrid('setColProp','ID',{title:false});
</script>
