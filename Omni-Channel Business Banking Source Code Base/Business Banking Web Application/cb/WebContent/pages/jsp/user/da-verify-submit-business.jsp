<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<ffi:setL10NProperty name='PageHeading' value='Verify Submit User for Approval'/>

<% session.setAttribute("itemId",request.getParameter("itemId"));%>
<% session.setAttribute("itemType",request.getParameter("itemType"));%>

<ffi:object name="com.ffusion.tasks.dualapproval.SubmitForApproval" id="SubmitForApprovalTask" />
<ffi:setProperty name="SubmitForApprovalTask" property="itemType" value="${itemType}" />
<ffi:setProperty name="SubmitForApprovalTask" property="itemId" value="${itemId}" />
<% session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("SubmitForApprovalTask") ); %>


<div align="center" class="marginTop10">
    	    	
    	    	
    		<ffi:setProperty name="messageKey" value="jsp/user/da-business-submit-confirm.jsp-1" />
    		<s:text name="%{#session.Business.BusinessName}" var="parameter0"></s:text>
		   	<ffi:getL10NString rsrcFile="cb" msgKey="${messageKey}" parm0="${parameter0}" />
    	
</div>
<div class="ui-widget-header customDialogFooter">
	<%--
		<input class="submitbutton" tabindex="15" type="button" value="Confirm" onclick="document.location='<ffi:urlEncrypt url="da-submit-approval.jsp?itemId=${itemId}&successUrl=${successUrl}&itemType=${itemType}"/>'">
	--%>
	<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>

	<s:url id="submitForApprovalUrl" escapeAmp="false" value="/pages/dualapproval/dualApprovalAction-submitForApproval.action?module=Business&daAction=Pending Approval">
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>  
	</s:url>

	<sj:a id="submitForApprovalLink"
	href="%{submitForApprovalUrl}"
	targets="resultmessage"
	button="true"
	title="%{getText('jsp.default_tile_confirm')}"
	onClickTopics=""
	onSuccessTopics="DACompanySubmitForApprovalCompleteTopics"
	onCompleteTopics="submitPendingApprovalForCompany"
	onErrorTopics="">
	<s:text name="jsp.default_107" /> 
	</sj:a>
</div>

<ffi:removeProperty name="messageKey" />
<ffi:removeProperty name="parameter0" />