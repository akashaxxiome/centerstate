<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">

<%
	String groupId = request.getParameter("EditGroup_GroupId");
	String groupName = request.getParameter("EditGroup_GroupName");
	String supervisor = request.getParameter("EditGroup_Supervisor");
	String divisionName = request.getParameter("EditGroup_DivisionName");
%>

<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
	<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='<%=groupId%>'/>
	<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>'/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:object id="EditBusinessGroup" name="com.ffusion.tasks.admin.EditBusinessGroup" scope="session"/>
   <ffi:setProperty name="EditBusinessGroup" property="GroupName" value="<%=groupName%>"/>
   <ffi:setProperty name="EditBusinessGroup" property="SupervisorId" value="<%=supervisor%>"/>
   <ffi:setProperty name="EditBusinessGroup" property="ParentGroupName" value="<%=divisionName%>"/>
   <ffi:setProperty name="EditBusinessGroup" property="ParentGroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>

 </ffi:cinclude>