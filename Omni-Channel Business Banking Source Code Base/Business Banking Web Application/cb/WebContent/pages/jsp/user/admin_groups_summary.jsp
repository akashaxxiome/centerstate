<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<s:include value="inc/corpadmingroups-pre.jsp"/>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT%>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<%-- The following div tempDivIDForMethodNsAdminReloadGroups is used to hold temparory variables of group grids.--%>
<div id="tempDivIDForMethodNsAdminReloadGroups" style="display: none;"></div>

	<ffi:object id="admingrouptabs" name="com.ffusion.tasks.util.TabConfig"/>
	<ffi:setProperty name="admingrouptabs" property="TransactionID" value="AdminGroup"/>
	
	<ffi:setProperty name="admingrouptabs" property="Tab" value="group"/>
	<ffi:setProperty name="admingrouptabs" property="Href" value="/cb/pages/jsp/user/corpadmingroups.jsp"/>
	<ffi:setProperty name="admingrouptabs" property="LinkId" value="group"/>
	<ffi:setProperty name="admingrouptabs" property="Title" value="jsp.user.Groups"/>
	
	<ffi:process name="admingrouptabs"/>


<%-- Pending Approval Company island start --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
<div id="admin_groupsPendingTab" class="portlet hidden">
	  <div class="portlet-header">
	  		<span class="portlet-title"><s:text name="jsp.default.label.pending.approval.changes"/></span>
			<%-- <div class="searchHeaderCls toggleClick expandArrow">
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<a id="adminGroupsPendingApprovalLink" href="javascript:void(0);" class="nameTitle"><s:text name="jsp.default.label.pending.approval.changes"/></a>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-negative floatRight"></span>
				</div>
			</div> --%>
	    </div>
	    
	    <div class="portlet-content">
	    	<!-- <div class="toggleBlock marginTop10"> -->
				<s:include value="/pages/jsp/user/inc/da-group-island-grid.jsp"/>
			<!-- </div> -->
		</div>
		
		<div id="admingroupsummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('admin_groupsPendingTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
		<!-- <div class="clearBoth portletClearnace hidden" style="padding: 10px 0"></div> -->	
</div>
<div class="clearBoth"  id="groupBlankDiv" style="padding: 10px 0"></div>
</ffi:cinclude>
<%-- Pending Approval Company island end --%>


<div id="groupsSummaryTabs" class="portlet" style="float: left; width: 100%;">
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="jsp.user.Groups"/></span>
	</div>
	<div class="portlet-content" id="groupsSummaryTabsContent">
		<s:include value="/pages/jsp/user/corpadmingroups.jsp" />
	</div>
	<div id="groupsSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('groupsSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<script type="text/javascript">
	//Initialize portlet with settings icon
	ns.common.initializePortlet("groupsSummaryTabs");
	ns.common.initializePortlet("admin_groupsPendingTab");
	
</script>
