<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%-- <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<s:include value="%{#session.PagesPath}user/inc/da-pending-approval.jsp" />
       <br>
</ffi:cinclude> --%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
<div id="company_PendingApprovalTab"  class="portlet hidden" style="float: left; width: 100%;">
	    <div class="portlet-header">
			<%-- <div class="searchHeaderCls toggleClick expandArrow">
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<a id="adminCompanyPendingApprovalLink" href="#companyPendingApprovalChangeDiv" class="nameTitle"><s:text name="jsp.user_558"/></a>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-negative floatRight"></span>
				</div>
			</div> --%>
			<span id="adminCompanyPendingApprovalLink" class="portlet-title"><s:text name="jsp.user_558"/></span>
		</div>
		<div class="portlet-content">
				<s:include value="/pages/jsp/user/inc/da-pending-approval.jsp"/>
				<%-- <s:include value="%{#session.PagesPath}user/inc/da-pending-approval.jsp" /> --%>
		</div>
		<div id="pendingSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('company_PendingApprovalTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
		<div class="clearBoth portletClearnace hidden" style="padding: 10px 0"></div>
	</div>
</ffi:cinclude>
<div class="clearBoth" style="padding: 10px 0"></div>
<div id="admin_SummaryTab" class="portlet">
	 <div class="portlet-header">
		 	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
		 		<span class="portlet-title">
					<s:text name="administrable.areas.for.admin">
	                      	<s:param >
	                      		<ffi:getProperty name='BusinessEmployee' property='FullName'/> <ffi:getProperty name='Business' property='BusinessName'/>
	                      	</s:param>
	                </s:text>	
				</span>
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			   	<span class="portlet-title"><s:text name="administrable.areas.for.admin">
                   	<s:param >
                   		<ffi:getProperty name='BusinessEmployee' property='FullName'/> <ffi:getProperty name='Business' property='BusinessName'/>
                   	</s:param>
           	   </s:text></span>
           	</ffi:cinclude>
		</div>
	    <div class="portlet-content">
			<div id="gridContainer" class="summaryGridHolderDivCls">
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
					<div id="companySummaryLabelSummaryGrid" gridId="" class="gridSummaryContainerCls" >
						<s:include value="/pages/jsp/user/corpadminsummary.jsp" />
					</div>
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
					<div id="companySummaryLabelSummaryGrid" gridId="" class="gridSummaryContainerCls" >
						<s:include value="/pages/jsp/user/corpadminsummary.jsp" />
					</div>
	    		</ffi:cinclude>
			</div>
	    </div>
		
		<div id="adminSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('admin_SummaryTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
</div>	
<input type="hidden" id="getTransID" value="<ffi:getProperty name='admintabs' property='TransactionID'/>" />	 
<script>    
	
	//Initialize portlet with settings icon
	ns.common.initializePortlet("company_PendingApprovalTab");
	
	ns.common.initializePortlet("admin_SummaryTab");
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(".toggleBlock").css('display') == 'none'){
				$(".toggleBlock").slideDown();
				$(".portletClearnace").hide();
				$(this).find(".moduleSubmenuLblDownArrow").addClass("icon-negative").removeClass("icon-positive");
			}else{
				$(".toggleBlock").slideUp();
				$(".portletClearnace").show();
				$(this).find(".moduleSubmenuLblDownArrow").addClass("icon-positive").removeClass("icon-negative");
			}
		});
	});		
	</script>