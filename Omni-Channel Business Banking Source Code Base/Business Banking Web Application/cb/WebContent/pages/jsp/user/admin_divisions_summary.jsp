<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>


<s:include value="inc/corpadmindiv-pre.jsp"/>


<%-- The following div tempDivIDForMethodNsAdminReloadDivisions is used to hold temparory variables of division grids.--%>

<%-- Pending Approval Company island start --%>
<s:if test="%{#parameters.dashboardElementId[0] == 'addDivisionLink'}">
<%-- do nothing 2 this is a negative check for else condition --%>
</s:if>
<s:elseif test="%{#parameters.dashboardElementId[0] == 'showdbLocationSummary'}">
<%-- do nothing 2 this is a negative check for else condition --%>
</s:elseif>
<s:else>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
  <div id="admin_pendingDivisionsTab" class="portlet hidden">
	  <div class="portlet-header">
	  		<span class="portlet-title"><s:text name="jsp.default.label.pending.approval.changes"/></span>
			<%-- <div class="searchHeaderCls toggleClick expandArrow">
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<a id="adminDivisionPendingApprovalLink" href="javascript:void(0);" class="nameTitle"><s:text name="jsp.default.label.pending.approval.changes"/></a>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-negative floatRight"></span>
				</div>
			</div> --%>
	    </div>
	    
	    <div class="portlet-content">
	    	<%-- <div class="toggleBlock marginTop10"> --%>
				<s:include value="/pages/jsp/user/inc/da-division-island-grid.jsp"/>
			<%-- </div> --%>
		</div>
		
		<div id="adminDivisionSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('admin_pendingDivisionsTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
		<%-- <div class="clearBoth portletClearnace hidden" style="padding: 10px 0"></div>	--%>
  </div>
  <div class="clearBoth" id="divisionBlankDiv" style="padding: 10px 0"></div>
</ffi:cinclude>
</s:else>
<%-- Pending Approval Company island end --%>

<div id="divisionSummaryTabs" class="portlet" style="float: left; width: 100%;">
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="jsp.user_562"/></span>
	</div>
	<div class="portlet-content" id="divisionSummaryTabsContent">
			<s:include value="/pages/jsp/user/corpadmindiv.jsp" />
	</div>
	
	<div id="divisionSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('divisionSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<%
com.ffusion.csil.beans.entitlements.EntitlementGroups divisionGroups = (com.ffusion.csil.beans.entitlements.EntitlementGroups)session.getAttribute("Descendants");
int divisionCount = 0;
for(int i=0;i<divisionGroups.size();i++){
	com.ffusion.csil.beans.entitlements.EntitlementGroup eg = (com.ffusion.csil.beans.entitlements.EntitlementGroup)divisionGroups.get(i);
	if(eg.getMap().get("IsAnAdminOf") == "true"){
		divisionCount+=1;
	}
}
%>

<script type="text/javascript">	
ns.common.selectDashboardItem('divisionsLink')
	ns.common.initializePortlet("divisionSummaryTabs");
	ns.common.initializePortlet("admin_pendingDivisionsTab");
	$(function(){
		var divisionCount = '<%= divisionCount%>';
		if(divisionCount == 'undefined' || divisionCount == '' || divisionCount == 0){
			$('#divisionMoreOptions').css('display','none');
			$('#locationSummaryMenuLink').hide();
			$('#addLocationMenuLink').hide();
		}else {
			$('#divisionMoreOptions').css('display','inline');
			$('#locationSummaryMenuLink').show();
			$('#addLocationMenuLink').show();
		}
	});
</script>
