<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
    String userID = request.getParameter("SetBusinessEmployee.Id");
%>

<%-- populate BusinessEmployees with employees for the business --%>
<%-- this is not necessary the first time the page is displayed, but is important for refresh the page --%>
<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:setProperty name="GetBusinessEmployee" property="SearchEntitlementProfile" value="TRUE"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchEntitlementProfiles" value="TRUE"/>
<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployees"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>

<ffi:object id="GetEntitlementGroupSummaries" name="com.ffusion.tasks.user.GetEntitlementGroupSummaries" scope="session" />
<ffi:setProperty name="GetEntitlementGroupSummaries" property="NumSpaces" value="3" />
<ffi:process name="GetEntitlementGroupSummaries" />
<ffi:removeProperty name="GetEntitlementGroupSummaries" />

<%-- set BusinessEmployee to be the user being edited --%>
<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" scope="request"/>
<ffi:setProperty name="SetBusinessEmployee" property="Id" value="<%=userID%>"/>
<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployees" />
<ffi:process name="SetBusinessEmployee"/>