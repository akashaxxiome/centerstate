<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.tasks.dualapproval.IDACategoryConstants" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setL10NProperty name='PageHeading' value='Pending Approval Summary'/>

<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<s:include value="/pages/jsp/inc/init/admin-init.jsp"/>
</ffi:cinclude>

<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetCategories" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
	<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetCategories"/>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="Validate" value="itemId,ItemType" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
<ffi:process name="GetDAItem"/>

<ffi:object id="SubmittedUser" name="com.ffusion.tasks.user.GetUserById"/>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="equals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.createdBy}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="notEquals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.modifiedBy}" />
	</ffi:cinclude>
	<ffi:setProperty name="SubmittedUser" property="userSessionName" value="SubmittedUserName"/>
<ffi:process name="SubmittedUser"/>


<%@page import="com.ffusion.tasks.dualapproval.IDACategoryConstants"%>

<script type="text/javascript">
function reject()
{
	document.frmDualApproval.action = "rejectDAChanges.action";
	document.frmDualApproval.submit();
}

// Handle text area maxlength
$(document).ready(function(){
	ns.common.handleTextAreaMaxlength("#RejectReason");
});

</script>

<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >
<input type="hidden" name="userAction" value="<ffi:getProperty name='userAction'/>" >

<ffi:cinclude value1="${tempSuccessUrl}" value2="">
	<ffi:setProperty name="tempSuccessUrl" value="${SuccessUrl}"/>
</ffi:cinclude>

<ffi:cinclude value1="${SuccessUrl}" value2="">
	<ffi:setProperty name='SuccessUrl' value="${tempSuccessUrl}"/>
</ffi:cinclude>

<div align="center" class="approvalDialogHt">
	<span id="formError"></span>
	<ffi:setProperty name="subMenuSelected" value="divisions"/>
	<%-- include page header --%>
	<ffi:setProperty name="isPendingIsland" value="TRUE"/>
	<ffi:removeProperty name="isPendingIsland"/>
	
	<div class="blockWrapper">
		<div class="blockHead"><s:text name="jsp.da_approvals_pending_user_verify_approvalchanges" /></div>
		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock" style="width:32%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_122" /></span>
					<span><ffi:getProperty name="GroupName"/></span>
				</div>
				<div class="inlineBlock" style="width:32%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.da_approvals_pending_user_SubmittedBy_column" />: </span>
					<span><ffi:getProperty name="SubmittedUserName" property="userName"/></span>
				</div>
				<div class="inlineBlock" style="width:32%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.da_approvals_pending_user_SubmittedOn_column" />:</span>
					<span><ffi:getProperty name="DAItem" property="formattedSubmittedDate"/></span>
				</div>
			</div>
		</div>
		
		
		<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>" operator="notEquals">
			<div class="blockHead"><s:text name="jsp.da_approvals_pending_user_Profile_column" /></div>			
			<s:include value="inc/da-verify-division-profile.jsp" />
			<s:include value="inc/da-verify-division-administrator.jsp" />
			<s:include value="inc/da-verify-division-locations.jsp" />

		</ffi:cinclude>
	</div>
	<table width="750" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="adminBackground">
				<table width="708" border="0" cellspacing="0" cellpadding="3">
					
					
					<ffi:setProperty name="entitlementTypeOrder" property="Filter" value="EnableMenu=true" />
					<ffi:object id="DAWizardUtil" name="com.ffusion.tasks.dualapproval.DAWizardUtil" />
					<ffi:setProperty name="DAWizardUtil" property="currentPage" value="<%=IDACategoryConstants.IS_PROFILE_CHANGED %>" />
					<ffi:process name="DAWizardUtil" />
					<ffi:removeProperty name="DAWizardUtil"/>
					<tr><td align="left" >&nbsp;</td></tr>
					<tr><td align="left" colspan="7">&nbsp;</td></tr>
					<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>">
						<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							<tr>
								<td align="left" width="100%" >
								<table width="100%" >
									<tr>
										<td width="15%" class="columndata" align="center">
											<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-division-verify.jsp-1" parm0="${GroupName}"/>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr><td>&nbsp;</td></tr>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${rejectFlag}" value2="y" >
						<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
							<tr>
								<td align="left" width="100%" >
<%--									
									<form name="frmDualApproval" method="post" >
--%>
									<s:form id="frmDualApproval1" name="frmDualApproval" namespace="/pages/jsp/user" validate="false" method="post" theme="simple">

										<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
										<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>"/>
										<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>"/>
										<input type="hidden" name="successUrl" value="<ffi:getProperty name="SecurePath"/>user/corpadmininfo.jsp"/> 
										<table width="100%" >
											<tr>
												<td align="left" valign="top" width="15%" class="sectionsubhead adminBackground" >
												<s:text name="jsp.da_approvals_pending_user_reject_reason"/><span class="required">*</span></td> 
											</tr>
											<tr>	
												<td align="left">
													<textarea align="left" id="RejectReason" name="REJECTREASON" rows="3" cols="80" maxlength="120"></textarea>
												</td>
											</tr>
											<tr>
												<td align="left" colspan="2"><span class="required"><s:text name="jsp.da_approvals_pending_user_require_field"/></span></td>
											</tr>
										</table>
									<%--</form>--%>
									</s:form>
								</td>
							</tr>
						</ffi:cinclude>
					</ffi:cinclude>
					<tr><td height="20">&nbsp;</td></tr>
					<tr><td align="center" class="ui-widget-header customDialogFooter">
					<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>

					<ffi:cinclude value1="${previouspage}" value2="" operator="notEquals">
						<sj:a 
							id="previousPageID2"
							button="true"
							onclick="previousPage();"
						><s:text name="jsp.default_58" /></sj:a>
					</ffi:cinclude>
					<ffi:cinclude value1="${nextpage}" value2="" operator="notEquals">
						<sj:a
							id="nextPageID2"
							button="true"
							onclick="nextPage();"
						><s:text name="jsp.default_112" /></sj:a>
					</ffi:cinclude>
					<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>" operator="notEquals">
						<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_ADDED %>">
								<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
									<s:url id="approveUrl" value="/pages/jsp/user/da-division-add-confirm.jsp">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param> 
										<s:param name="itemId" value="%{#session.itemId}"></s:param>  										 
									</s:url>
									<sj:a id="approveDivisionLink"
										href="%{approveUrl}"
										targets="resultmessage"
										button="true"
										title="Approve Changes"
										onClickTopics="closeDialog"
										onSuccessTopics="DADivisionApprovalReviewCompleteTopics"
										onCompleteTopics=""
										onErrorTopics="">
										<s:text name="jsp.da_company_approval_button_text"/>
									</sj:a>
								</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
									<s:url id="approveUrl" value="/pages/jsp/user/da-division-modify-confirm.jsp">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param> 
										<s:param name="itemId" value="%{#session.itemId}"></s:param>  										 
									</s:url>
									<sj:a id="approveDivisionLink"
										href="%{approveUrl}"
										targets="resultmessage"
										button="true"
										title="Approve Changes"
										onClickTopics="closeDialog"
										onSuccessTopics="DADivisionApprovalReviewCompleteTopics"
										onCompleteTopics=""
										onErrorTopics="">
										<s:text name="jsp.da_company_approval_button_text"/>
									</sj:a>
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${rejectFlag}" value2="y" >
						<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
									<s:url id="rejectDivisionUrl" value="/pages/dualapproval/rejectDualApprovalChanges.action" escapeAmp="false">
										<s:param name="itemId" value="%{#session.itemId}"/>
										<s:param name="itemType" value="%{#session.itemType}"/> 
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>								 
									</s:url>
									<sj:a id="rejectDivisionLink"
										button="true"
										title="Reject Changes"
										onClickTopics="rejectDAiD"
										onSuccessTopics=""
										onCompleteTopics=""
										onErrorTopics="">
										<s:text name="jsp.da_company_reject_button_text"/>
									</sj:a>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>">
						<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
								<s:url id="approveUrl" value="/pages/jsp/user/da-division-delete-confirm.jsp">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param> 
									<s:param name="itemId" value="%{#session.itemId}"></s:param>  	
									<s:param name="GroupName" value="%{#session.GroupName}"></s:param>  									
								</s:url>
								<sj:a id="approveDivisionLink"
									href="%{approveUrl}"
									targets="resultmessage"
									button="true"
									title="Approve Changes"
									onClickTopics="closeDialog"
									onSuccessTopics="DADivisionApprovalReviewCompleteTopics"
									onCompleteTopics=""
									onErrorTopics="">
									<s:text name="jsp.da_company_approval_button_text"/>
								</sj:a>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>					
					</td></tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<ffi:flush/>
</div>

<ffi:setProperty name="url" value="${SecurePath}user/da-summary-division-profile.jsp?PermissionsWizard=TRUE"  URLEncrypt="true" />
<ffi:setProperty name="BackURL" value="${url}"/>

<ffi:removeProperty name="DAItem"/>
<ffi:removeProperty name="categories"/>
<ffi:removeProperty name="DISABLE_APPROVE"/>
<ffi:removeProperty name="SubmittedUserName"/>

<script>
	
	$.subscribe('rejectDAiD', function(event,data) { 
			$.ajax({
			url: '/cb/pages/dualapproval/rejectDualApprovalChanges.action',
			async: false,
			type:"POST",
			data: $("#frmDualApproval1").serialize(),
			success: function(data){
				var statusMessage = $('#resultmessage').html();
				ns.common.showStatus(statusMessage);
				$.publish("DADivisionApprovalReviewCompleteTopics");
			}
		});
		
	});
	function nextPage(){
		$.ajax({
			url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${nextpage.MenuUrl}&ParentMenu=${nextpage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${nextpage.PageKey}&SortKey=${nextpage.NextSortKey}"/>',

			success: function(data)
			{
				$('#approvalWizardDialogID').html(data);
				ns.admin.checkResizeApprovalDialog();
			}
		});
	}

	function previousPage(){
		$.ajax({
			url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${previouspage.MenuUrl}&ParentMenu=${previouspage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${previouspage.PageKey}&SortKey=${previouspage.NextSortKey}"/>',
			success: function(data)
			{
				$('#approvalWizardDialogID').html(data);
				ns.admin.checkResizeApprovalDialog();
			}
		});
	}
</script>