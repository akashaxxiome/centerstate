<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_banklistitem" className="moduleHelpClass"/>

<ffi:setProperty name="Edit" value=""/>

	<div>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		    <td class="adminBackground">
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
			    
			    <tr>
				<td>
					<s:form name="banklistitem.jsp" method="post" id="user_insertSelectedDestinationBankFormID">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<table width="98%" border="0" cellspacing="0" cellpadding="3" align="center">
						   <tr>
							<td colspan="4" nowrap align="center">
								<span class="headerTitle">
									<ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="InstitutionName"/>
								</span>
							</td>
						    </tr>
						    <tr>
						    	<td colspan="4"><p class="transactionHeading"><s:text name="jsp.user_31"/></p></td>
						    </tr>
						    <tr>
						    	<td>
						    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
							    		<tr>
									    	<td width="25%"><s:text name="jsp.user_31"/></td>
									    	<td width="25%"><s:text name="jsp.default_101"/></td>
									    	<td width="25%"><s:text name="jsp.default_386"/></td>
									    	<td width="25%"><s:text name="jsp.default_115"/></td>
									    </tr>
								    </table>	
						    	</td>
						    </tr>
						    <tr>
						    	<td>
						    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
							    		<tr>
								    		<td width="25%">
												<span class="columndata"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="Street"/></span>
												<span class="columndata"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="ZipCode"/></span>
											</td>
									    	<td width="25%">
												<span class="columndata"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="City"/></span>
											</td>
									    	<td width="25%">
												<span class="columndata"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="State"/></span>
											</td>
									    	<td width="25%">
												<span class="columndata"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="Country"/></span>
											</td>
										</tr>
								    </table>	
						    	</td>	
						    </tr>
						    <tr>
						    	<td colspan="4" height="20">&nbsp;</td>
						    </tr>
						    <tr>
						    	<td colspan="4"><p class="transactionHeading"><s:text name="admin.banklookup.identification.codes"/></p></td>
						    </tr>
						    <tr>
						    	<td colspan="4">
							    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
								    	<tr>
									    	<td width="33%"><s:text name="jsp.default_201"/></td>
									    	<td width="33%"><s:text name="jsp.default_99"/></td>
									    	<td width="33%"><s:text name="jsp.default_289"/></td>
									    </tr>
								    </table>
							    </td>	
						    </tr>
						    <tr>
						    	<td colspan="4">
							    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
								    	<tr>
									    	<td width="33%"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="AchRoutingNumber"/></td>
									    	<td width="33%">&nbsp;</td>
									    	<td width="33%">&nbsp;</td>
									    </tr>
								    </table>
							    </td>	
						    </tr>
						    
						    <%-- <tr>
								<td nowrap width="49%">
								    <div align="left">
									<span class="columndata"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="Street"/></span></div>
								</td>
								<td rowspan="4" nowrap width="1"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="1" height="50" border="0"></td>
								<td align="right" nowrap width="49%"><span class="columndata"><s:text name="jsp.default_201"/>: <ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="AchRoutingNumber"/></span></td>
								<td>&nbsp;</td>
						    </tr>
						    <tr>
								<td width="49%">
								    <div align="left">
										<span class="columndata"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="City"/>,&nbsp;<ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="State"/></span></div>
								</td>
						    </tr>
						    <tr>
								<td width="49%"><span class="columndata"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="ZipCode"/></span></td>
						    </tr>
						    <tr>
								<td width="49%"><span class="columndata"><ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="Country"/></span></td>
						    </tr>
						    <tr>
								<td width="49%"></td>
								<td width="1"></td>
								<td align="right" width="49%"><br></td>
						    </tr> --%>
						    <tr>
						    	<td colspan="4" height="50">&nbsp;</td>
						    </tr>
						    <tr>
								<td colspan="4" align="center">
								<div align="center" class="ui-widget-header customDialogFooter">
									<sj:a id="user_insertSelectedBankForm"
										button="true"
										onClickTopics="location_insertSelectedBankTopics"><s:text name="jsp.default_250"/></sj:a>
									&nbsp;&nbsp;
									<sj:a id="user_cancelSelectedBankForm"
										button="true"
										onClickTopics="location_closeSelectBankTopics"
									><s:text name="jsp.default_102"/></sj:a>
									<input type="hidden" name="bankName" value="<ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="InstitutionName"/>"/>
									<input type="hidden" name="rtNum" value="<ffi:getProperty name="<%= com.ffusion.tasks.banklookup.BankLookupTask.FINANCIAL_INSTITUTION %>" property="AchRoutingNumber"/>"/>
								</div>
								</td>
						   </tr>
					</table>
				    </s:form>
				</td>
			    </tr>
			</table>
		    </td>
		</tr>
	    </table>
	</div>
