<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%
	session.setAttribute("itemId", request.getParameter("itemId"));
    session.setAttribute("itemType", request.getParameter("itemType"));
    session.setAttribute("category", request.getParameter("category"));
%>
<ffi:object name="com.ffusion.tasks.dualapproval.ModifyDAItemStatus" id="ModifyDAItemStatus" />
<ffi:setProperty name="ModifyDAItemStatus" property="itemId" value="${itemId}"/>
<ffi:setProperty name="ModifyDAItemStatus" property="itemType" value="${itemType}"/>
<ffi:setProperty name="ModifyDAItemStatus" property="successURL" value="${successUrl}" />
<ffi:process name="ModifyDAItemStatus"/>
<ffi:removeProperty name="ModifyDAItemStatus"/>
