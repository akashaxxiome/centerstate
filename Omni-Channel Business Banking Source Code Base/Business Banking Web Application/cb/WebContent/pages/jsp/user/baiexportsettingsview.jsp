<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setProperty name='PageHeading' value='Company Profile'/>

<%-- initializing if necessary --%>
<ffi:removeProperty name="GroupDivAdminEdited"/>
<ffi:removeProperty name="AdminsSelected"/>
<ffi:removeProperty name="AdminEmployees"/>
<ffi:removeProperty name="AdminGroups"/>
<ffi:removeProperty name="NonAdminEmployees"/>
<ffi:removeProperty name="NonAdminGroups"/>
<ffi:removeProperty name="tempAdminEmps"/>
<ffi:removeProperty name="modifiedAdmins"/>
<ffi:removeProperty name="adminMembers"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="groupIds"/>
<ffi:removeProperty name="userMembers"/>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="SetAdministrators"/>


<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<ffi:include page="${PathExt}inc/init/admin-init.jsp" />
</ffi:cinclude>

<ffi:setProperty name='PageText' value=''/>

<%--get company profile info from the business bean--%>
<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>

<%--get the company's BAI export preferences--%>
<ffi:object id="GetBAIExportSettings" name="com.ffusion.tasks.user.GetBAIExportSettings" scope="request"/>
<ffi:setProperty name="GetBAIExportSettings" property="BusinessSessionName" value="Business"/>
<ffi:process name="GetBAIExportSettings"/>

<%--get service package info from GetEntitlementGroup, which stores info in session under Entitlement_EntitlementGroup--%>
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
    <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${Business.ServicesPackageId}" />
<ffi:process name="GetEntitlementGroup"/>

<%--get contact using GetPrimaryBusinessEmployees, which stores info for primary contact in session under PrimaryBusinessEmployee--%>
<ffi:object id="GetPrimaryBusinessEmployees" name="com.ffusion.tasks.user.GetPrimaryBusinessEmployees" scope="session"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="GetPrimaryBusinessEmployees" property="BankId" value="${Business.BankId}"/>
<ffi:process name="GetPrimaryBusinessEmployees"/>
<ffi:removeProperty name="GetPrimaryBusinessEmployees" />

<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Business.EntitlementGroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>

<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
<ffi:process name="GetBusinessEmployeesByEntGroups"/>
<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
 								
<%--get accounts info from GetAccountsByBusinessEmployee, stores results in session as BusEmpAccounts--%>
<ffi:object id="GetAccountsByBusinessEmployee" name="com.ffusion.tasks.accounts.GetAccountsByBusinessEmployee" scope="session"/>
	<ffi:setProperty name="GetAccountsByBusinessEmployee" property="AccountsName" value="BusEmpAccounts"/>
<ffi:process name="GetAccountsByBusinessEmployee"/>
<ffi:removeProperty name="GetAccountsByBusinessEmployee"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="CountryResource" />

<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>


<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<%-- Read BAI Export settings from DA --%>
<ffi:object name="com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA" id="AddBAIExportSettingsToDA" scope="request" />
<ffi:setProperty name="AddBAIExportSettingsToDA" property="ReadFromDA" value="true" />
<ffi:setProperty name="AddBAIExportSettingsToDA" property="TmpBankIdDisplayText" value="${TempBankIdentifierDisplayText}" /> 
<ffi:process name="AddBAIExportSettingsToDA" />	

<div align="center">
	<ffi:setProperty name="subMenuSelected" value="company"/>

	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding10">
		<tr>
			<td width="50%">
			<span class="<ffi:getPendingStyle fieldname="senderIDType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" ><s:text name="jsp.user_295"/>:
			</span>									
			<span class="columndata <ffi:getPendingStyle fieldname="SenderIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline">
				<ffi:cinclude value1="${AddBAIExportSettingsToDA.SenderIDType}" value2="" operator="notEquals">
					<ffi:cinclude value1="${AddBAIExportSettingsToDA.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					 <s:text name="jsp.user_358"/>
					 <ffi:getProperty name="TempBankIdentifierDisplayText"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${AddBAIExportSettingsToDA.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
						<ffi:getProperty name="AddBAIExportSettingsToDA" property="SenderIDCustom"/>
					</ffi:cinclude>
					<br />
				</ffi:cinclude>
				<span class="<ffi:getPendingStyle fieldname="senderIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">			
					<ffi:cinclude value1="${GetBAIExportSettings.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
						<s:text name="jsp.user_358"/>
						<ffi:getProperty name="TempBankIdentifierDisplayText"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${GetBAIExportSettings.SenderIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
						<ffi:getProperty name="GetBAIExportSettings" property="SenderIDCustom"/>
					</ffi:cinclude>
				</span>
			</span>
			</td>
			<td width="50%">
			<span class="<ffi:getPendingStyle fieldname="receiverIDType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" >
				<s:text name="jsp.user_270"/>:
			</span>
			<span class="columndata" valign="baseline">
			<ffi:cinclude value1="${AddBAIExportSettingsToDA.ReceiverIDType}" value2="" operator="notEquals">
				<ffi:cinclude value1="${AddBAIExportSettingsToDA.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					  <s:text name="jsp.user_358"/>
					  <ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${AddBAIExportSettingsToDA.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
					<ffi:getProperty name="AddBAIExportSettingsToDA" property="ReceiverIDCustom"/>
				</ffi:cinclude>
				<br />
			</ffi:cinclude>	
			<span class="<ffi:getPendingStyle fieldname="receiverIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">
				<ffi:cinclude value1="${GetBAIExportSettings.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					  <s:text name="jsp.user_358"/>
					  <ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${GetBAIExportSettings.ReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
					<ffi:getProperty name="GetBAIExportSettings" property="ReceiverIDCustom"/>
				</ffi:cinclude>
			</span>
			</span>
			</td>
		</tr>
		<tr>
			<td width="50%">
				<span class="<ffi:getPendingStyle fieldname="ultimateReceiverIDType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" >
					<s:text name="jsp.user_348"/>:
				</span>
				<span class="columndata" valign="baseline">
					<ffi:cinclude value1="${AddBAIExportSettingsToDA.UltimateReceiverIDType}" value2="" operator="notEquals">
					<ffi:cinclude value1="${AddBAIExportSettingsToDA.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
						 <s:text name="jsp.user_357"/>
						 <ffi:getProperty name="TempBankIdentifierDisplayText"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${AddBAIExportSettingsToDA.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
						<ffi:getProperty name="AddBAIExportSettingsToDA" property="UltimateReceiverIDCustom"/>
					</ffi:cinclude>
					<br />
					</ffi:cinclude>
					<span class="<ffi:getPendingStyle fieldname="ultimateReceiverIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">
					<ffi:cinclude value1="${GetBAIExportSettings.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
						 <s:text name="jsp.user_358"/>
						 <ffi:getProperty name="TempBankIdentifierDisplayText"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${GetBAIExportSettings.UltimateReceiverIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
						<ffi:getProperty name="GetBAIExportSettings" property="UltimateReceiverIDCustom"/>
					</ffi:cinclude>
					</span>
				</span>
			</td>
			<td width="50%">
				<span class="<ffi:getPendingStyle fieldname="originatorIDType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" >
					<s:text name="jsp.user_232"/>:
				</span>
				<span class="columndata" valign="baseline">
					<ffi:cinclude value1="${AddBAIExportSettingsToDA.OriginatorIDType}" value2="" operator="notEquals">
						<ffi:cinclude value1="${AddBAIExportSettingsToDA.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
							<s:text name="jsp.user_355"/>
							<ffi:getProperty name="TempBankIdentifierDisplayText"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${AddBAIExportSettingsToDA.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
							<ffi:getProperty name="AddBAIExportSettingsToDA" property="OriginatorIDCustom"/>
						</ffi:cinclude>
						<br/>
					</ffi:cinclude>			
					<span class="<ffi:getPendingStyle fieldname="originatorIDType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">							
						<ffi:cinclude value1="${GetBAIExportSettings.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
							<s:text name="jsp.user_355"/>
							<ffi:getProperty name="TempBankIdentifierDisplayText"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${GetBAIExportSettings.OriginatorIDType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">										
							<ffi:getProperty name="GetBAIExportSettings" property="OriginatorIDCustom"/>
						</ffi:cinclude>
					</span>
				</span>
			  </td>
			</tr>
			<tr>
				<td width="50%">
					<span class="<ffi:getPendingStyle fieldname="customerAccountNumberType" dacss="sectionheadDA" defaultcss="sectionsubhead" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />" valign="baseline" align="right" height="17" >
						<s:text name="jsp.user_100"/>:
					</span>
					<span class="columndata" valign="baseline">
						<ffi:cinclude value1="${AddBAIExportSettingsToDA.CustomerAccountNumberType}" value2="" operator="notEquals">
							<ffi:cinclude value1="${AddBAIExportSettingsToDA.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" operator="equals">
							<s:text name="jsp.user_356"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${AddBAIExportSettingsToDA.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" operator="equals">										
								<s:text name="jsp.user_352"/>
								  <ffi:getProperty name="TempBankIdentifierDisplayText"/>
								 <s:text name="jsp.user_43"/>
							</ffi:cinclude>
							<br />
						</ffi:cinclude>
						<span class="<ffi:getPendingStyle fieldname="customerAccountNumberType" dacss="sectionhead_greyDA" defaultcss="" sessionCategoryName="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS%>" />">
							<ffi:cinclude value1="${GetBAIExportSettings.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" operator="equals">
							<s:text name="jsp.user_356"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${GetBAIExportSettings.CustomerAccountNumberType}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" operator="equals">										
								  <s:text name="jsp.user_353"/>	
								  <ffi:getProperty name="TempBankIdentifierDisplayText"/>
								  <s:text name="jsp.user_42"/>
							</ffi:cinclude>
						</span>
					</span>
				</td>
			</tr>
	</table>
	<div class="ffivisible" style="height:50px;">&nbsp;</div>
	<div class="ui-widget-header customDialogFooter">
		<sj:a button="true" title="Close" onClickTopics="closeDialog,closeViewBAIDialog"><s:text name="jsp.default_175" /></sj:a>
	</div>
	<ffi:flush/>
	<!-- Restore Business Employee in session to the original values -->
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="Business"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="restoredObject" value="y"/>
 	<ffi:process name="PopulateObjectWithDAValues"/>
</div>


<ffi:setProperty name="onCancelGoto" value="corpadmininfo.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmininfo.jsp"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmininfo.jsp"/>
