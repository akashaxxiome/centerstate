<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.tasks.dualapproval.IDACategoryConstants" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<ffi:setL10NProperty name='PageHeading' value='Pending Approval Summary'/>

<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<s:include value="/pages/jsp/inc/init/admin-init.jsp"/>
</ffi:cinclude>

<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetCategories" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
	<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetCategories"/>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="Validate" value="itemId,ItemType" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
<ffi:process name="GetDAItem"/>

<ffi:object id="SubmittedUser" name="com.ffusion.tasks.user.GetUserById"/>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="equals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.createdBy}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="notEquals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.modifiedBy}" />
	</ffi:cinclude>
	<ffi:setProperty name="SubmittedUser" property="userSessionName" value="SubmittedUserName"/>
<ffi:process name="SubmittedUser"/>

<script type="text/javascript">
function reject()
{
	document.frmDualApproval.action = "rejectDualApprovalChanges.action";
	document.frmDualApproval.submit();
}

// Handle text area maxlength
$(document).ready(function(){
	ns.common.handleTextAreaMaxlength("#RejectReason");
});
</script>

<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >
<input type="hidden" name="userAction" value="<ffi:getProperty name='userAction'/>" >

<ffi:cinclude value1="${tempSuccessUrl}" value2="">
	<ffi:setProperty name="tempSuccessUrl" value="${SuccessUrl}"/>
</ffi:cinclude>

<ffi:cinclude value1="${SuccessUrl}" value2="">
	<ffi:setProperty name='SuccessUrl' value="${tempSuccessUrl}"/>
</ffi:cinclude>

<div align="center" class="approvalDialogHt">
	<span id="formError"></span>
	<ffi:setProperty name="subMenuSelected" value="groups"/>
	<ffi:setProperty name="isPendingIsland" value="TRUE"/>

	<ffi:removeProperty name="isPendingIsland"/>
	<table width="750" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" class="adminBackground">
				<table width="708" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td>	
							<div class="blockHead"><s:text name="admin.group.summary"/></div>
							<div class="paneWrapper">
						  		<div class="paneInnerWrapper">
						  		<div class="header">
								<table width="750" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110" >
											<s:text name="jsp.user_Group" />
										</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110" >
											<s:text name="jsp.da_approvals_pending_user_SubmittedBy_column" />
										</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110" >
											<s:text name="jsp.da_approvals_pending_user_SubmittedOn_column" />
										</td>
									</tr>
								</table>
								</div>
								<table width="750" border="0" cellspacing="0" cellpadding="3">	
									<tr>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110">
											&nbsp;<ffi:getProperty name="GroupName"/>
										</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110">
											&nbsp;<ffi:getProperty name="SubmittedUserName" property="userName"/>
										</td>
										<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110">
											&nbsp;<ffi:getProperty name="DAItem" property="formattedSubmittedDate"/>
										</td>
									</tr>
								</table>
								</div>
							</div>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>		
					<table width="708" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td  colspan="3">
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>" operator="notEquals">
								<s:include value="inc/da-verify-group-profile.jsp"/>
								<%-- <s:include value="inc/da-verify-group-administrator.jsp"/> --%>
							</ffi:cinclude>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>	
					<table width="708" border=0" cellspacing="0" cellpadding="3">
					<tr>
						<td  colspan="3">
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>" operator="notEquals">
								<%-- <s:include value="inc/da-verify-group-profile.jsp"/> --%>
								<s:include value="inc/da-verify-group-administrator.jsp"/>
							</ffi:cinclude>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>			
					<table width="708" border="0" cellspacing="0" cellpadding="3">
					<tr><td colspan="3">&nbsp;</td></tr>
					<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>">
						<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							<tr>
								<td width="100%"  colspan="3">
								<table width="100%" >
									<tr>
										<td width="15%" class="columndata" align="center">
											<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-group-verify.jsp-1" parm0="${GroupName}"/>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr><td>&nbsp;</td></tr>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:setProperty name="entitlementTypeOrder" property="Filter" value="EnableMenu=true" />
					<ffi:object id="DAWizardUtil" name="com.ffusion.tasks.dualapproval.DAWizardUtil" />
					<ffi:setProperty name="DAWizardUtil" property="currentPage" value="<%=IDACategoryConstants.IS_PROFILE_CHANGED %>" />
					<ffi:process name="DAWizardUtil" />
					<ffi:removeProperty name="DAWizardUtil"/>
					<ffi:cinclude value1="${rejectFlag}" value2="y" >
						<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
							<tr>
								<td width="100%"  colspan="3">
							<s:form id="frmDualApproval1" name="frmDualApproval" namespace="/pages/jsp/user" validate="false" method="post" theme="simple">
	                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	                    	<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
							<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
								<table width="100%" >
									<tr>
										<td width="15%" class="sectionsubhead adminBackground" ><s:text name="jsp.da_approvals_pending_user_reject_reason" />
											<span class="required">*</span>
										</td>
									</tr>
									<tr>	
										<td>
											<textarea id="RejectReason" name="REJECTREASON" rows="3" cols="80" maxlength="120"></textarea>
										</td>
									</tr>
									<tr>
										<td><span class="required"><s:text name="jsp.da_approvals_pending_user_require_field" /></span></td>
									</tr>
								</table>
							</s:form>
								</td>
							</tr>
						</ffi:cinclude>
					</ffi:cinclude>
					<tr><td align="center" colspan="3" class="ui-widget-header customDialogFooter">
						<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>
						<ffi:cinclude value1="${previouspage}" value2="" operator="notEquals">
							<sj:a
								id="previousPageID2"
								button="true"
								onclick="previousPage();"
							><s:text name="jsp.default_58" /></sj:a>
						</ffi:cinclude>
						<ffi:cinclude value1="${nextpage}" value2="" operator="notEquals">
							<sj:a
								id="nextPageID2"
								button="true"
								onclick="nextPage();"
							><s:text name="jsp.default_112" /></sj:a>
						</ffi:cinclude>
					<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>" operator="notEquals">
						<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_ADDED %>">
								<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
									<s:url id="approveUrl" value="/pages/jsp/user/da-group-add-confirm.jsp">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param> 
										<s:param name="itemId" value="%{#session.itemId}"></s:param>  										 
									</s:url>
									<sj:a id="approveGroupLink"
										href="%{approveUrl}"
										targets="resultmessage"
										button="true"
										title="Approve Changes"
										onClickTopics=""
										onSuccessTopics="DAGroupApprovalReviewCompleteTopics"
										onCompleteTopics="GroupCompleteTopic"
										onErrorTopics="">
										<s:text name="jsp.da_company_approval_button_text"/>
									</sj:a>
								</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
									<s:url id="approveUrl" value="/pages/jsp/user/da-group-modify-confirm.jsp">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param> 
										<s:param name="itemId" value="%{#session.itemId}"></s:param>  
										<s:param name="groupName" value="%{#session.GroupName}"></s:param> 
									</s:url>
									<sj:a id="approveGroupLink"
										href="%{approveUrl}"
										targets="resultmessage"
										button="true"
										title="Approve Changes"
										onClickTopics=""
										onSuccessTopics="DAGroupApprovalReviewCompleteTopics"
										onCompleteTopics="GroupCompleteTopic"
										onErrorTopics="">
										<s:text name="jsp.da_company_approval_button_text"/>
									</sj:a>
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${rejectFlag}" value2="y" >
						<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
							<s:url id="rejectGroupUrl" value="/pages/dualapproval/rejectDualApprovalChanges.action">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>	
							</s:url>
							<sj:a id="rejectGroupLink"
								href="%{rejectGroupUrl}"
								formIds="frmDualApproval1" 
								targets="resultmessage"
								button="true"
								title="Reject Changes"
								onClickTopics=""
								onSuccessTopics="DAGroupApprovalReviewCompleteTopics"
								onCompleteTopics=""
								onErrorTopics="">
								<s:text name="jsp.da_company_reject_button_text"/>
							</sj:a>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>">
						<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
								<s:url id="approveUrl" value="/pages/jsp/user/da-group-delete-confirm.jsp">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									<s:param name="itemId" value="%{#session.itemId}"/>
									<s:param name="GroupName" value="%{#session.GroupName}"/>									  									
								</s:url>
								<sj:a id="approveDivisionLink"
									href="%{approveUrl}"
									targets="resultmessage"
									button="true"
									title="Approve Changes"
									onClickTopics=""
									onSuccessTopics="DAGroupApprovalReviewCompleteTopics"
									onCompleteTopics="GroupCompleteTopic"
									onErrorTopics="">
									<s:text name="jsp.da_company_approval_button_text"/>
								</sj:a>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
					</td></tr>
				</table>
			 </td>
		</tr>
	</table>
	<br>
	<ffi:flush/>
</div>

<ffi:setProperty name="url" value="${SecurePath}user/da-summary-group-profile.jsp?PermissionsWizard=TRUE"  URLEncrypt="true" />
<ffi:setProperty name="BackURL" value="${url}"/>

<ffi:removeProperty name="DAItem"/>
<ffi:removeProperty name="categories"/>
<ffi:removeProperty name="DISABLE_APPROVE"/>
<ffi:removeProperty name="SubmittedUserName"/>

<script>
	function nextPage(){
		$.ajax({
			url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${nextpage.MenuUrl}&ParentMenu=${nextpage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${nextpage.PageKey}&SortKey=${nextpage.NextSortKey}"/>',
			success: function(data)
			{
				$('#approvalWizardDialogID').html(data);
				ns.admin.checkResizeApprovalDialog();
			}
		});
	}

	function previousPage(){
		$.ajax({
			url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${previouspage.MenuUrl}&ParentMenu=${previouspage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${previouspage.PageKey}&SortKey=${previouspage.NextSortKey}"/>',
			success: function(data)
			{
				$('#approvalWizardDialogID').html(data);
				ns.admin.checkResizeApprovalDialog();
			}
		});
	}
</script>
