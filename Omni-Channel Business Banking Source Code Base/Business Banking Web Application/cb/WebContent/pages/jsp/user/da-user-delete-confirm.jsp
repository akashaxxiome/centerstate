<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setL10NProperty name='PageHeading' value='Confirm Approve User'/>
<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.businessID}"/>
<ffi:process name="GetDAItem" />

<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
	<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
	<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
	<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
	<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
	<ffi:process name="GetBusinessEmployees"/>
	<ffi:removeProperty name="GetBusinessEmployees"/>
	<ffi:removeProperty name="SearchBusinessEmployee"/>
	
	<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
		<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:process name="SetBusinessEmployee"/>
	<ffi:removeProperty name="SetBusinessEmployee"/>
	
	<ffi:object id="RemoveBusinessUser" name="com.ffusion.tasks.admin.RemoveBusinessUser" scope="session"/>
	<ffi:setProperty name="RemoveBusinessUser" property="DAApprove" value="true"/>
	<ffi:process name="RemoveBusinessUser"/>
	<ffi:removeProperty name="RemoveBusinessUser"/>
	
	<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${itemId}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="approveDAItem" value="true" />
	<ffi:process name="ApprovePendingChangesTask"/>
	<ffi:removeProperty name="ApprovePendingChangesTask"/>
	
	<ffi:setProperty name="tempDisplayName" value="${BusinessEmployee.UserName}"/> 
</ffi:cinclude>

	<div align="center">		
		<table class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">			
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<br>
						<tr>
							<td class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-user-add-confirm.jsp-1" parm0="${tempDisplayName}"/></td>
						</tr>
						<br>
						<tr>
							<td align="center">
									<sj:a 
										id="doneWizardID"
										button="true" 
										onclick="doneWizard();"
									><s:text name="jsp.default_538" /></sj:a></td>
						</tr>
					</table>
				</td>

			</tr>
			
		</table>
			<p></p>
		</div>

<script>
function doneWizard(){
	$('#approvalWizardDialogID').dialog('close');;
}
</script>
<ffi:removeProperty name="BusinessEmployee"/>
<ffi:removeProperty name="DAItem"/>
<ffi:removeProperty name="BusinessEmployees"/>