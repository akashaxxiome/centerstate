<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>	
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${EditDivision_GroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:help id="user_corpadmindivedit-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_372')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:setProperty name="EditBusinessGroup" property="ValidateOnly" value="true"/>	
<ffi:process name="EditBusinessGroup"/>	
	
<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:setProperty name="divisionEditDualApprovalMode" value="${Business.dualApprovalMode}"/>

<SCRIPT language=JavaScript><!--
function callBack()
{
	// return back to the previous page
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
	window.location.href = "<ffi:urlEncrypt url="corpadmindivedit.jsp?UseLastRequest=TRUE&AdminsSelected=TRUE" />";
</ffi:cinclude>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
	window.location.href = "<ffi:urlEncrypt url="corpadmindivedit.jsp?UseLastRequest=TRUE" />";
</ffi:cinclude>

}
//--></SCRIPT>

<table cellspacing="0" cellpadding="0" border="0" width="100%" class="marginBottom10 marginTop20">
	<tr>
		<td width="25%">
			<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<s:text name="admin.division.new.summary"/>
				</div>
				<div class="paneContentWrapper">
					<table cellspacing="0" cellpadding="3" border="0" width="100%">
						<tr>
							<td><s:text name="jsp.user_121"/>:&nbsp; <ffi:getProperty name="EditBusinessGroup" property="GroupName"/></td>
						</tr>
						<tr>
							<td>
							<table cellspacing="0" cellpadding="3" border="0" width="100%">
								<tr>
									<td valign="top" width="100"><s:text name="jsp.user_38"/>:&nbsp;</td>
									<td><ffi:getProperty name="AdministratorStr"/>
										<ffi:cinclude value1="${AdministratorStr}" value2="" operator="equals">
											<s:text name="jsp.default_296"/>
										</ffi:cinclude>
									</td>
								</tr>
							</table>
						</tr>
					</table>
				</div>
				</div>
			</div>
			</td>
			<td width="75%">
			   <s:form id="divisionEditVerifyFormId" namespace="/pages/user" action="editBusinessDivision-execute" theme="simple" name="divisionEditVerifyFormName" method="post">
					<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<input type="hidden" name="DeleteBusinessGroup.SuccessURL" value="<ffi:urlEncrypt url="${SecurityPath}redirect.jsp?target=${SecurityPath}user/corpadmingroups.jsp" />">
					<input type="hidden" name="EditBusinessGroup.CheckboxesAvailable" value="false">
					<input type="hidden" name="EditBusinessGroup.SessionGroupName" value="Entitlement_EntitlementGroup"/>
				<table cellspacing="0" cellpadding="3" border="0" width="70%" >
					<tr>
						<td align="center">
							&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="columndata marginRight20 paddingLeft10"><s:text name="jsp.user_378"/></span>
						</td>
					</tr>
					<tr>
						<td align="center">
							<br><br><sj:a
										  button="true" 
										  onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
									<sj:a
										  button="true" 
										  summaryDivId="summary" buttonType="cancel"
										  onClickTopics="cancelDivisionForm"><s:text name="jsp.default_82"/></sj:a>
									<sj:a
										formIds="divisionEditVerifyFormId"
										targets="confirmDiv"
										button="true" 
										validate="false"
										onBeforeTopics="beforeSubmit" onCompleteTopics="completeSubmit" onErrorTopics="errorSubmit" onSuccessTopics="successSubmit"
										><s:text name="jsp.default_366"/></sj:a>
						</td>
						<td>&nbsp;</td>
					</tr>
				</table>
				</s:form>
			</td>
		</tr>	
	</table>

<ffi:removeProperty name="BusinessEmployees"/>
<%-- <ffi:removeProperty name="AdministratorStr"/> --%>
