<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%
	String EditLocation_LocationBPWID = request.getParameter("EditLocation_LocationBPWID");
	if(EditLocation_LocationBPWID!= null){
		session.setAttribute("EditLocation_LocationBPWID", EditLocation_LocationBPWID);
	}
	
	String editlocationreload = request.getParameter("editlocationreload");
	if(editlocationreload!= null){
		session.setAttribute("editlocation-reload", editlocationreload);
	}
	
	String addlocationreload = request.getParameter("addlocationreload");
	if(addlocationreload!= null){
		session.setAttribute("addlocation-reload", addlocationreload);
	}
	
	String childSequence = request.getParameter("childSequence");
	if(childSequence!= null){
		session.setAttribute("childSequence", childSequence);
	}
	String locationBpwdId = request.getParameter("EditLocation_LocationBPWID");
	if(EditLocation_LocationBPWID!= null){
		session.setAttribute("locationBpwdId", locationBpwdId);
	}
%>

<script language="javascript">
$(function(){
	$("#selectBankID").selectmenu({width: 200});
	$("#selectAccountTypeID").selectmenu({width: 150});
	$("#selectAccountID").selectmenu({width: 200});
	$("#selectCashConCompanyID").selectmenu({width: 150});
	$("#selectConcAccountID").selectmenu({width: 150});
	$("#selectDisbAccountID").selectmenu({width: 150});
	
    $("#selectCashConCompanyID").change(function() {
    	showHideSameDayPrenoteFields($("option:selected", this));
    });
});
</script>

<style>
.displayNone{display:none;}
.displayVisible{display:table-cell;}
</style>

<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayCashConEnabled"/>
<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.cashcon.sameday.support"/>
<ffi:process name="GetBPWProperty"/>

<ffi:setProperty name='PageHeadingForLocation' value='Edit New Location'/>
<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name='verifyEditAddLocation' value='true'/>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${EditDivision_GroupId}"/>
<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value="divisionEntitlementGroup" />
<ffi:process name="GetEntitlementGroup"/>
<script>
	ns.admin.PageHeadingForLocation="<ffi:getProperty name='PageHeadingForLocation' />";
</script>
<ffi:object id="GetCumulativeSettings" name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" scope="session" />
<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupSessionKey" value="divisionEntitlementGroup" />
<ffi:process name="GetCumulativeSettings" />
<ffi:setProperty name="autoEntitleLocations" value="${GetCumulativeSettings.EnableLocations}" />
<ffi:removeProperty name="GetCumulativeSettings" />

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">
	<ffi:setProperty name="autoEntitleLocations" value="false" />
</ffi:cinclude>

<ffi:object id="AddLocation" name="com.ffusion.tasks.cashcon.AddLocation" scope="session" />
<ffi:setProperty name="AddLocation" property="DivisionID" value="${EditDivision_GroupId}"/>
<ffi:cinclude value1="${autoEntitleLocations}" value2="true" operator="equals">
	<ffi:setProperty name="AddLocation" property="AutoEntitleLocation" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${autoEntitleLocations}" value2="true" operator="notEquals">
	<ffi:setProperty name="AddLocation" property="AutoEntitleLocation" value="false" />
</ffi:cinclude>

<% session.setAttribute("FFIAddLocation", session.getAttribute("AddLocation")); %>

<ffi:removeProperty name="addlocation-reload"/>

<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
<ffi:process name="GetAffiliateBanks"/>
<ffi:removeProperty name="GetAffiliateBanks"/>
<ffi:setProperty name="AffiliateBanks" property="SortedBy" value="BANK_NAME" />

<%-- get cashcon company list --%>
<ffi:object id="GetCashConCompanies" name="com.ffusion.tasks.cashcon.GetCashConCompanies"/>
<ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${EditDivision_GroupId}"/>
<ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
<ffi:process name="GetCashConCompanies"/>
<ffi:removeProperty name="GetCashConCompanies"/>

<ffi:setProperty name="CashConCompanies" property="Filter" value="ACTIVE=true"/>
<ffi:setProperty name="CashConCompanies" property="FilterOnFilter" value="CONC_ENABLED=true,DISB_ENABLED=true"/>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="objectId" value="${EditLocation_LocationBPWID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="objectType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION%>"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="AddLocation"/>
	<ffi:process name="PopulateObjectWithDAValues"/>
</ffi:cinclude>

<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="PopulateObjectWithDAValues"/>

<ffi:cinclude value1="${AddLocation.LocalBankID}" value2="0" operator="equals">
	<% boolean tempStart = true; %>
	<ffi:list collection="AffiliateBanks" items="Bank">
		<% if (tempStart) {
			tempStart = false; %>
			<ffi:setProperty name="AddLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
		<% } %>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${AddLocation.LocalBankID}" value2="0" operator="notEquals">
	<ffi:object id="SetAffiliateBank" name="com.ffusion.tasks.affiliatebank.SetAffiliateBank"/>
	<ffi:setProperty name="SetAffiliateBank" property="CollectionSessionName" value="AffiliateBanks"/>
	<ffi:setProperty name="SetAffiliateBank" property="AffiliateBankSessionName" value="CurrentBank"/>
	<ffi:setProperty name="SetAffiliateBank" property="AffiliateBankID" value="${AddLocation.LocalBankID}"/>
	<ffi:process name="SetAffiliateBank"/>
</ffi:cinclude>

<ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="" operator="equals">
	<% boolean tempStart = true; %>
	<ffi:list collection="CashConCompanies" items="tempCompany">
		<% if (tempStart) {
			tempStart = false; %>
			<ffi:setProperty name="AddLocation" property="CashConCompanyBPWID" value="${tempCompany.BPWID}"/>
		<% } %>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="" operator="notEquals">
	<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
	<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${AddLocation.CashConCompanyBPWID}"/>
	<ffi:process name="SetCashConCompany"/>
</ffi:cinclude>

<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
<%-- Set the concentration account and disbursemnt account. Select first ones as selected if none was already selected. --%>
	<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="notEquals">
	<% boolean tempStart = true;
	   boolean accountFound = false;
	   String firstAccount = null; %>
		<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
			<% if (tempStart) {
				tempStart = false; %>
				<ffi:getProperty name="ConcAccount" property="BPWID" assignTo="firstAccount" />
			<% } %>
			<ffi:cinclude value1="${ConcAccount.BPWID}" value2="${AddLocation.ConcAccountBPWID}" operator="equals">
				<% accountFound = true; %>
			</ffi:cinclude>
		</ffi:list>
		<% if (!accountFound) { %>
			<ffi:setProperty name="AddLocation" property="ConcAccountBPWID" value="<%= firstAccount %>"/>
		<% } %>
	</ffi:cinclude>
	<ffi:cinclude value1="${CashConCompany.ConcAccounts}" value2="" operator="equals">
		<ffi:setProperty name="AddLocation" property="ConcAccountBPWID" value=""/>
	</ffi:cinclude>
	<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="notEquals">
	<% boolean tempStart = true;
	   boolean accountFound = false;
	   String firstAccount = null; %>
		<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
			<% if (tempStart) {
				tempStart = false; %>
				<ffi:getProperty name="DisbAccount" property="BPWID" assignTo="firstAccount" />
			<% } %>
			<ffi:cinclude value1="${DisbAccount.BPWID}" value2="${AddLocation.DisbAccountBPWID}" operator="equals">
				<% accountFound = true; %>
			</ffi:cinclude>
		</ffi:list>
		<% if (!accountFound) { %>
			<ffi:setProperty name="AddLocation" property="DisbAccountBPWID" value="<%= firstAccount %>"/>
		<% } %>
	</ffi:cinclude>
	<ffi:cinclude value1="${CashConCompany.DisbAccounts}" value2="" operator="equals">
		<ffi:setProperty name="AddLocation" property="DisbAccountBPWID" value=""/>
	</ffi:cinclude>
</ffi:cinclude>

<%-- based on selected concentration account, set DepositEnabled --%>
<%-- based on selected concentration account, set DepositEnabled --%>
<% boolean depositEnabled = false;
   boolean disbursementEnabled = false; %>
<ffi:cinclude value1="${AddLocation.ConcAccountBPWID}" value2="" operator="notEquals">
	<% depositEnabled = true; %>
</ffi:cinclude>

<%-- based on selected disbursement account, set DisbursementEnabled --%>
<ffi:cinclude value1="${AddLocation.DisbAccountBPWID}" value2="" operator="notEquals">
	<% disbursementEnabled = true; %>
</ffi:cinclude>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

		<SCRIPT language=JavaScript><!--

<ffi:setProperty name="AffiliateBanks" property="Filter" value=""/>
/* Account lists for each of the "pre-defined" banks. */
var bankAccounts = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
<% int bankAccountsCounter = 0; %>
<ffi:list collection="AffiliateBanks" items="affiliateBank">
    <ffi:setProperty name="BankingAccounts" property="Filter" value="ROUTINGNUM=${affiliateBank.AffiliateRoutingNum}"/>
bankAccounts[<%= Integer.toString(bankAccountsCounter) %>] = new Array(
<ffi:setProperty name="spaceOrComma" value=" "/>
    <ffi:list collection="BankingAccounts" items="bankAccount">
	<ffi:cinclude value1="${bankAccount.TypeValue}"
		value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING ) %>"
		operator="equals">
	    <ffi:getProperty name="spaceOrComma"/>
new Array("<ffi:getProperty name='bankAccount' property='ID'/><ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="bankAccount" property="NickName"/></ffi:cinclude>", "<ffi:getProperty name='bankAccount' property='ID'/>")
	    <ffi:setProperty name="spaceOrComma" value=","/>
	</ffi:cinclude>
	<ffi:cinclude value1="${bankAccount.TypeValue}"
		value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS ) %>"
		operator="equals">
	    <ffi:getProperty name="spaceOrComma"/>
new Array("<ffi:getProperty name='bankAccount' property='ID'/><ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="bankAccount" property="NickName"/></ffi:cinclude>", "<ffi:getProperty name='bankAccount' property='ID'/>")
	    <ffi:setProperty name="spaceOrComma" value=","/>
	</ffi:cinclude>
    </ffi:list>
	);
<% bankAccountsCounter++; %>
</ffi:list>

/* Mappings of banks to indices into the account lists.*/
var bankIndexMapping = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
var bankIndexMappingByID = new Array(<ffi:getProperty name="AffiliateBanks" property="Size"/>);
<% int bankIndexMappingCounter = 0; %>
<ffi:list collection="AffiliateBanks" items="affiliateBank">
bankIndexMapping[<%= Integer.toString(bankIndexMappingCounter) %>] = "<ffi:getProperty name='affiliateBank' property='AffiliateRoutingNum'/>";
bankIndexMappingByID[<%= Integer.toString(bankIndexMappingCounter) %>] = "<ffi:getProperty name='affiliateBank' property='AffiliateBankID'/>";
<% bankIndexMappingCounter++; %>
</ffi:list>

<ffi:setProperty name="BankingAccounts" property="Filter" value=""/>

/* concentration and disbursement Account lists for each of the CC companies. */
var ccAccounts = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var disbAccounts = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var ccEnabled = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var disbEnabled = new Array(<ffi:getProperty name="CashConCompanies" property="Size"/>);
var ccSize = <ffi:getProperty name="CashConCompanies" property="size"/>;

<% int ccCounter = 0; %>
<ffi:list collection="CashConCompanies" items="ccCompany">
	ccEnabled[<%= Integer.toString(ccCounter) %>] = <ffi:getProperty name="ccCompany" property="ConcEnabledString"/>;
	ccAccounts[<%= Integer.toString(ccCounter) %>] = new Array(
		<ffi:setProperty name="spaceOrComma" value=" "/>
    	<ffi:list collection="ccCompany.ConcAccounts" items="ccAccount">
			<ffi:getProperty name="spaceOrComma"/>
			new Array("<ffi:getProperty name="ccAccount" property="DisplayText"/><ffi:cinclude value1="${ccAccount.Nickname}" value2="" operator="notEquals"> - <ffi:getProperty name="ccAccount" property="Nickname"/></ffi:cinclude>", "<ffi:getProperty name='ccAccount' property='BPWID'/>")
			<ffi:setProperty name="spaceOrComma" value=","/>
		</ffi:list>
	);
	disbEnabled[<%= Integer.toString(ccCounter) %>] = <ffi:getProperty name="ccCompany" property="DisbEnabledString"/>;
	disbAccounts[<%= Integer.toString(ccCounter) %>] = new Array(
		<ffi:setProperty name="spaceOrComma" value=" "/>
		<ffi:list collection="ccCompany.DisbAccounts" items="disbAccount">
			<ffi:getProperty name="spaceOrComma"/>
			new Array("<ffi:getProperty name="disbAccount" property="DisplayText"/><ffi:cinclude value1="${disbAccount.Nickname}" value2="" operator="notEquals"> - <ffi:getProperty name="disbAccount" property="Nickname"/></ffi:cinclude>", "<ffi:getProperty name='disbAccount' property='BPWID'/>")
			<ffi:setProperty name="spaceOrComma" value=","/>
		</ffi:list>
	);
<% ccCounter++; %>
</ffi:list>

<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals" >
	<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="true" operator="equals" >
		var cashConcAccountsCurrencyCode = new Array ();
		<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount" >
			cashConcAccountsCurrencyCode[ "<ffi:getProperty name="ConcAccount" property="BPWID" />" ] = "<ffi:getProperty name="ConcAccount" property="Currency" />";
		</ffi:list>
	</ffi:cinclude>
</ffi:cinclude>

function updateAccountsList(routingNum)
{
	var updated = false;
    var accountIDSel = document.getElementById("selectAccountID");
    var selectedValue = accountIDSel.options[accountIDSel.selectedIndex].value;
    var selectedAccountID = "<ffi:getProperty name="AddLocation" property="LocalAccountID" />";
    
    for (var i = 0; i < bankIndexMapping.length; i++) {
        if (bankIndexMapping[i] == routingNum) {

            if (bankAccounts[i].length == 0) {
                accountIDSel.options.length = 1;
                accountIDSel.options[0] = new Option("None", "");
            } else {
                accountIDSel.options.length = bankAccounts[i].length;

                for (var j = 0; j < bankAccounts[i].length; j++) {
                    var defaultSelected = (selectedValue == bankAccounts[i][j][1] || selectedAccountID == bankAccounts[i][j][1]) ? true : false;
                    accountIDSel.options[j] = new Option(bankAccounts[i][j][0],
                            bankAccounts[i][j][1], defaultSelected, defaultSelected);
                }
            }

            updated = true;
            break;
        }
	}

    if (!updated) {
        accountIDSel.options.length = 1;
        accountIDSel.options[0] = new Option("None", "");
    }
    $("#selectAccountID").selectmenu('destroy');
    $('#selectAccountID').selectmenu({width: 200});
}

function updateAccountsListByID(id) {
    for (var i = 0; i < bankIndexMappingByID.length; i++) {
	if (bankIndexMappingByID[i] == id) {
	    updateAccountsList(bankIndexMapping[i]);
	    break;
	}
    }
}

function callCancel()
{
	// return back to the previous page
	window.location.href="corpadmindivedit.jsp";
}
//--></SCRIPT>

<SCRIPT language=JavaScript><!--

function validateLocation() {
  updateFields();  
  document.AddEditLocationForm.action='<ffi:urlEncrypt url="${SecurePath}user/corpadminlocadd-confirm.jsp?locationBpwdId=${EditLocation_LocationBPWID}" />';
  return true;
}

function selectDestBank() {
	frm = document.AddEditLocationForm;
	destURL = "banklistselect.jsp";
	bankName = frm.elements['AddLocation.LocalBankName'].value;
	routingNumber = frm.elements['AddLocation.LocalRoutingNumber'].value;

	theChild = window.open(destURL+"?SearchBankName="+bankName+"&SearchRoutingNumber="+routingNumber+"&TargetExternalAccountForm=AddEditLocationForm&BankNameFieldName=AddLocation.LocalBankName&RoutingNumberFieldName=AddLocation.LocalRoutingNumber&AddingFreeFormAccount=true","BankList","width=360,height=420,resizable=yes,scrollbars=yes,menubar=no,toolbar=no,directories=no,location=no,status=no");
}

function updateFields()
{
	frm = document.AddEditLocationForm;
	// Active or not	
	
	if( frm.elements["AddLocation.Active"].checked ) {
		frm.elements[ "AddLocation.Active" ].value = "true";
	} else {
		frm.elements[ "AddLocation.Active" ].value = "false";
	}

	// ConsolidateDeposits
	
	if( frm.elements["AddLocation.ConsolidateDeposits"].checked ) {
		frm.elements[ "AddLocation.ConsolidateDeposits" ].value = "true";
	} else {
		frm.elements[ "AddLocation.ConsolidateDeposits" ].value = "false";
	}

	// DepositPrenote
	if( frm.elements["DepositPrenote"].checked || frm.elements["regularDepositPrenote"].checked) {
		frm.elements[ "AddLocation.DepositPrenote" ].value = "true";
		frm.elements[ "AddLocation.DepositSameDayPrenote" ].value = "false";
	} else {
		frm.elements[ "AddLocation.DepositPrenote" ].value = "false";
	}

	// DisbursementPrenote
	if( frm.elements["DisbursementPrenote"].checked || frm.elements["regularDisbursementPrenote"].checked) {
		frm.elements[ "AddLocation.DisbursementPrenote" ].value = "true";
		frm.elements[ "AddLocation.DisbursementSameDayPrenote" ].value = "false";
	} else {
		frm.elements[ "AddLocation.DisbursementPrenote" ].value = "false";
	}

	return true;
}

var bConc = true;
var bDisb = true;

function updateCCAccountsList(cccIndex)
{
	var concIDSel = document.getElementById("selectConcAccountID");
	var disbIDSel = document.getElementById("selectDisbAccountID");
	var selectedValue = concIDSel.options[concIDSel.selectedIndex].value;

	bConc = true;
	bDisb = true;
	
	if (ccSize == 0 || ccEnabled[cccIndex] == false || ccAccounts[cccIndex].length == 0) {
		concIDSel.options.length = 1;
		concIDSel.options[0] = new Option("None", "");
		bConc = false;
	} else {
		concIDSel.options.length = ccAccounts[cccIndex].length;
		for (var j = 0; j < ccAccounts[cccIndex].length; j++) {
			var defaultSelected = (selectedValue == ccAccounts[cccIndex][j][1]) ? true : false;
			concIDSel.options[j] = new Option(ccAccounts[cccIndex][j][0],
					ccAccounts[cccIndex][j][1], defaultSelected, defaultSelected);
		}
	}

	selectedValue = disbIDSel.options[disbIDSel.selectedIndex].value;
	if (ccSize == 0 || disbEnabled[cccIndex] == false || disbAccounts[cccIndex].length == 0) {
		disbIDSel.options.length = 1;
		disbIDSel.options[0] = new Option("None", "");
		bDisb = false;
	} else {
		disbIDSel.options.length = disbAccounts[cccIndex].length;
		for (var j = 0; j < disbAccounts[cccIndex].length; j++) {
			var defaultSelected = (selectedValue == disbAccounts[cccIndex][j][1]) ? true : false;
			disbIDSel.options[j] = new Option(disbAccounts[cccIndex][j][0],
					disbAccounts[cccIndex][j][1], defaultSelected, defaultSelected);
		}
	}
	$("#selectConcAccountID").selectmenu('destroy');
	$('#selectConcAccountID').selectmenu({width: 150});
	$("#selectDisbAccountID").selectmenu('destroy');
	$('#selectDisbAccountID').selectmenu({width: 150});
	disableFields(bConc, bDisb);
}

function showHideSameDayPrenoteFields(element) {
	
    var sameDayCashConEnabled = element.attr("SameDayCashConEnabled");
    var concSameDayCutOffDefined = element.attr("ConcSameDayCutOffDefined");
    var concCutOffsPassed = element.attr("ConcCutOffsPassed");
    var depositSameDayPrenote = element.attr("DepositSameDayPrenote");
    var disbSameDayCutOffDefined = element.attr("DisbSameDayCutOffDefined");
    var disbCutOffsPassed = element.attr("DisbCutOffsPassed");
    var disbursementSameDayPrenote = element.attr("DisbursementSameDayPrenote");
    var depositPrenote = element.attr("DepositPrenote");
    var disbursementPrenote = element.attr("DisbursementPrenote");
    
    if (sameDayCashConEnabled == "true") {
		if (bConc) {			
			if (concSameDayCutOffDefined == "true") {
				 $('#regularDepositPrenoteLabelClass').removeClass("displayVisible");
				 $('#regularDepositPrenoteCheckboxClass').removeClass("displayVisible");
				 $('#depositSameDayPrenoteClass').removeClass("displayNone");
				 $('#regularDepositPrenoteLabelClass').addClass("displayNone");
				 $('#regularDepositPrenoteCheckboxClass').addClass("displayNone");
				 $('#depositSameDayPrenoteClass').addClass("displayVisible");
				 $('#regularDepositPrenote').removeAttr("name");
				 $('#__checkbox_regularDepositPrenote').removeAttr("name");
				 $('#DepositPrenote').attr("name", "AddLocation.DepositPrenote" );
				 $('#__checkbox_DepositPrenote').attr("name", "__checkbox_AddLocation.DepositPrenote" );
				if (concCutOffsPassed == "true") {
					$('#showConcNote').show();
					$('#DepositSameDayPrenote').attr("disabled", true);
				} else {
					$('#showConcNote').hide();
					$('#DepositSameDayPrenote').removeAttr("disabled");
				}
				$('#DepositPrenote').removeAttr("disabled");
				 if (depositSameDayPrenote == "true") {
					$('#DepositSameDayPrenote').val(true);
					$('#DepositSameDayPrenote').attr('checked', true);
					$('#hiddenDepositSameDayPrenote').attr("name", "AddLocation.DepositSameDayPrenote" );
				 }
			} else {
				disableDepositSameDayFields();
			}
		}
		if (bDisb) {
			if (disbSameDayCutOffDefined == "true") {
				
				 $('#regularDisbursementPrenoteCheckboxClass').removeClass("displayVisible");
				 $('#regularDisbursementPrenoteLabelClass').removeClass("displayVisible");
				 $('#regularDisbursementPrenoteCheckboxClass').addClass("displayNone");
				 $('#regularDisbursementPrenoteLabelClass').addClass("displayNone");
				 $('#disbursementSameDayPrenoteClass').addClass("displayVisible");
				 $('#disbursementSameDayPrenoteClass').removeClass("displayNone");
				 $('#regularDisbursementPrenote').removeAttr("name");
				 $('#__checkbox_regularDisbursementPrenote').removeAttr("name");
				 $('#DisbursementPrenote').attr("name", "AddLocation.DisbursementPrenote" );
				 $('#__checkbox_DisbursementPrenote').attr("name", "__checkbox_AddLocation.DisbursementPrenote" );
				if (disbCutOffsPassed == "true") {
					$('#showDisbNote').show();
					$('#DisbursementSameDayPrenote').attr("disabled", true);
				} else {
					$('#showDisbNote').hide();
					$('#DisbursementSameDayPrenote').removeAttr("disabled");
				}
				$('#DisbursementPrenote').removeAttr("disabled");
				if (disbursementSameDayPrenote == "true") {
					$('#DisbursementSameDayPrenote').val(true);
					$('#DisbursementSameDayPrenote').attr('checked', true);
					$('#hiddenDisbursementSameDayPrenote').attr("name", "AddLocation.DisbursementSameDayPrenote" );
				}
			} else {
				disableDisbursementSameDayFields();
			}
		}
    } else {
    	disableDepositSameDayFields();
    	disableDisbursementSameDayFields();
    }
    
    disableCommonFields(depositSameDayPrenote, disbursementSameDayPrenote, depositPrenote, disbursementPrenote);
}

function disableCommonFields(depositSameDayPrenote, disbursementSameDayPrenote, depositPrenote, disbursementPrenote) {
	
	if (depositSameDayPrenote == "true") {
		$('#DepositPrenote').attr('checked', false);
		$('#DepositPrenote').removeAttr('checked');
		$('#regularDepositPrenote').attr('checked', false);
		$('#regularDepositPrenote').removeAttr('checked');
	}
	
	if (disbursementSameDayPrenote == "true") {
		$('#DisbursementPrenote').attr('checked', false);
		$('#DisbursementPrenote').removeAttr('checked');
		$('#regularDisbursementPrenote').attr('checked', false);
		$('#regularDisbursementPrenote').removeAttr('checked');
	}
	
	if (depositSameDayPrenote == "true") {
		 document.getElementById('hiddenDepositPrenote').value = false;
		 document.getElementById('hiddenDepositSameDayPrenote').value = true;
		
	} else if (depositPrenote == "true") {	
		document.getElementById('hiddenDepositPrenote').value = true;
		document.getElementById('hiddenDepositSameDayPrenote').value = false;
	}
	
	if (disbursementSameDayPrenote == "true") {
		
		document.getElementById('hiddenDisbursementPrenote').value = false;
		document.getElementById('hiddenDisbursementSameDayPrenote').value = true;
		
	} else if (disbursementPrenote == "true") {
		document.getElementById('hiddenDisbursementPrenote').value = true;
		document.getElementById('hiddenDisbursementSameDayPrenote').value = false;
	}
}

function disableDepositSameDayFields () {
	 $('#hiddenDepositPrenote').removeAttr("name");
	 $('#hiddenDepositSameDayPrenote').removeAttr("name");
	 frm = document.AddEditLocationForm;
	 frm.elements[ "AddLocation.DepositSameDayPrenote" ].value = "false";
	
	 $('#DepositSameDayPrenote').attr('checked', false);
	 $('#DepositSameDayPrenote').removeAttr('checked');
	 $('#regularDepositPrenoteLabelClass').removeClass("displayNone");
	 $('#regularDepositPrenoteCheckboxClass').removeClass("displayNone");
	 $('#depositSameDayPrenoteClass').removeClass("displayVisible");
	 $('#regularDepositPrenoteLabelClass').addClass("displayVisible");
	 $('#regularDepositPrenoteCheckboxClass').addClass("displayVisible");
	 $('#depositSameDayPrenoteClass').addClass("displayNone");
	 $('#regularDepositPrenote').removeAttr("disabled");
	 $('#DepositPrenote').removeAttr("name");
	 $('#__checkbox_DepositPrenote').removeAttr("name");
	 $('#regularDepositPrenote').attr("name", "AddLocation.DepositPrenote" );
	 $('#__checkbox_regularDepositPrenote').attr("name", "__checkbox_AddLocation.DepositPrenote" );
	 
	 $('#showConcNote').hide();
}

function disableDisbursementSameDayFields () {
	 $('#hiddenDisbursementPrenote').removeAttr("name");
	 $('#hiddenDisbursementSameDayPrenote').removeAttr("name");
	 frm = document.AddEditLocationForm;
	 frm.elements[ "AddLocation.DisbursementSameDayPrenote" ].value = "false";
	 $('#DisbursementSameDayPrenote').attr('checked', false);
	 $('#DisbursementSameDayPrenote').removeAttr('checked');
	 $('#regularDisbursementPrenoteCheckboxClass').removeClass("displayNone");
	 $('#regularDisbursementPrenoteLabelClass').removeClass("displayNone");
	 $('#disbursementSameDayPrenoteClass').removeClass("displayVisible");
	 $('#regularDisbursementPrenoteCheckboxClass').addClass("displayVisible");
	 $('#regularDisbursementPrenoteLabelClass').addClass("displayVisible");
	 $('#disbursementSameDayPrenoteClass').addClass("displayNone");
	 $('#regularDisbursementPrenote').removeAttr("disabled");
	 $('#DisbursementPrenote').removeAttr("name");
	 $('#__checkbox_DisbursementPrenote').removeAttr("name");
	 $('#regularDisbursementPrenote').attr("name", "AddLocation.DisbursementPrenote" );
	 $('#__checkbox_regularDisbursementPrenote').attr("name", "__checkbox_AddLocation.DisbursementPrenote" );
	 $('#showDisbNote').hide();
}

function disableFields(depositEnabled, disbursementEnabled)
{
	if (depositEnabled) {
		$('#ConsolidateDeposits').removeAttr("disabled");
		$('#DepositMinimum').removeAttr("disabled");
		$('#DepositMaximum').removeAttr("disabled");
		$('#AnticDeposit').removeAttr("disabled");
		$('#ThreshDeposit').removeAttr("disabled");
	} else {
		$('#ConsolidateDeposits').attr("disabled", true);
		$('#DepositPrenote').attr("disabled", true);
		$('#regularDepositPrenote').attr("disabled", true);
		$('#DepositSameDayPrenote').attr("disabled", true);
		$('#DepositMinimum').attr("disabled", true);
		$('#DepositMaximum').attr("disabled", true);
		$('#AnticDeposit').attr("disabled", true);
		$('#ThreshDeposit').attr("disabled", true);
		$('#ConsolidateDeposits').attr('checked', false);
		$('#DepositPrenote').attr('checked', false);
		$('#regularDepositPrenote').attr('checked', false);
		$('#DepositSameDayPrenote').attr('checked', false);
		$('#DepositMinimum').val('');
		$('#DepositMaximum').val('');
		$('#AnticDeposit').val('');
		$('#ThreshDeposit').val('');

	}
	if (!disbursementEnabled) {
		$('#regularDisbursementPrenote').attr("disabled", true);
		$('#regularDisbursementPrenote').checked = false;
		$('#DisbursementPrenote').attr("disabled", true);
		$('#DisbursementPrenote').checked = false;
		$('#DisbursementSameDayPrenote').attr("disabled", true);
		$('#DisbursementSameDayPrenote').checked = false;
	}
}

$(document).ready(function() {
	
	updateCCAccountsList($("#selectCashConCompanyID option:selected").index());
	showHideSameDayPrenoteFields($("#selectCashConCompanyID option:selected"));

	<ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="false" operator="equals">
			updateAccountsList('<ffi:getProperty name="AddLocation" property="LocalRoutingNumber"/>');
	</ffi:cinclude>
	<ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="true" operator="equals">
			updateAccountsList('<ffi:getProperty name="CurrentBank" property="AffiliateRoutingNum"/>');
	</ffi:cinclude>
});


function removeDepositSameDayPrenoteChecked(prenoteId) {
	if (prenoteId.checked) {
		document.getElementById('DepositSameDayPrenote').checked = false;
		document.getElementById('DepositPrenote').value = true;
		document.getElementById('DepositSameDayPrenote').value = false;
		document.getElementById('hiddenDepositPrenote').value = true;
		document.getElementById('hiddenDepositSameDayPrenote').value = false;
	} else {
		document.getElementById('DepositPrenote').value = false;
		document.getElementById('hiddenDepositPrenote').value = false;
	}
}

function removeDepositNormalPrenoteChecked(sameDayPrenoteId) {
    if(sameDayPrenoteId.checked) {
    	document.getElementById('DepositPrenote').checked = false;
    	document.getElementById('DepositPrenote').value = false;
    	document.getElementById('DepositSameDayPrenote').value = true;
    	document.getElementById('hiddenDepositSameDayPrenote').value = true;
    	document.getElementById('hiddenDepositPrenote').value = false;
    } else {
    	document.getElementById('DepositSameDayPrenote').value = false;
    	document.getElementById('hiddenDepositSameDayPrenote').value = false;
    }
    
}

function removeDisbursementSameDayPrenoteChecked(prenoteId) {
	if (prenoteId.checked) {
		document.getElementById('DisbursementSameDayPrenote').checked = false;
		document.getElementById('DisbursementSameDayPrenote').value = false;
		document.getElementById('DisbursementPrenote').value = true;
		document.getElementById('hiddenDisbursementPrenote').value = true;
		document.getElementById('hiddenDisbursementSameDayPrenote').value = false;
	} else {
		document.getElementById('DisbursementPrenote').value = false;
		document.getElementById('hiddenDisbursementPrenote').value = false;
	}
}

function removeDisbursementNormalPrenoteChecked(sameDayPrenoteId) {
    if(sameDayPrenoteId.checked) {
    	document.getElementById('DisbursementPrenote').checked = false;
    	document.getElementById('DisbursementPrenote').value = false;
    	document.getElementById('DisbursementSameDayPrenote').value = true;
    	document.getElementById('hiddenDisbursementSameDayPrenote').value = true;
    	document.getElementById('hiddenDisbursementPrenote').value = false;

    } else {
    	document.getElementById('DisbursementSameDayPrenote').value = false;
    	document.getElementById('hiddenDisbursementSameDayPrenote').value = false;
    }
}

//--></SCRIPT>

		<div align="center">

			<%-- <table width="676" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
						<div align="center">
							<span class="sectionhead"><ffi:getProperty name="EditDivision_DivisionName"/><br>
								<br>
							</span>
						</div>
					</td>
				</tr>
			</table> --%>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" align="center">
						<ul id="formerrors"></ul>
					</td>
				</tr>
				<tr>
					<td align="left" class="adminBackground">
						<s:form id="AddEditLocationFormID" namespace="/pages/user" action="addLocation-verify" theme="simple" name="AddEditLocationForm" method="post">
						<%--
						<form action="<ffi:urlEncrypt url="${SecurePath}user/da-location-edit.jsp?addlocation-reload=false" />" name="AddEditLocationForm" method="post" onsubmit="return updateFields();">
						--%>
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="locationBpwdId" value="<ffi:getProperty name='EditLocation_LocationBPWID'/>"/>
						<input type="hidden" id="hiddenDepositSameDayPrenote" name="AddLocation.DepositSameDayPrenote" value="${AddLocation.DepositSameDayPrenote}"/>
						<input type="hidden" id="hiddenDepositPrenote" name="DepositPrenote" value="${AddLocation.DepositPrenote}"/>
						<input type="hidden" id="hiddenDisbursementSameDayPrenote" name="AddLocation.DisbursementSameDayPrenote" value="${AddLocation.DisbursementSameDayPrenote}"/>
						<input type="hidden" id="hiddenDisbursementPrenote" name="DisbursementPrenote" value="${AddLocation.DisbursementPrenote}"/>
						
							<div align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td><p class="transactionHeading"><s:text name="admin.location.summary"/></p></td>
									</tr>
								</table>
								<table border="0" cellspacing="0" cellpadding="3" align="left">	
									<tr class="columndataauto">
										<td class="sectionsubhead ltrow2_color" align="left">
											<s:text name="jsp.default_267"/>:<span class="required">*</span>
										</td>
										<td width="70">&nbsp;</td>
										<td class="sectionsubhead ltrow2_color" align="left">
											<s:text name="jsp.user_189"/>:<span class="required">*</span>
										</td>
										<td width="20">&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td>
											<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocationName" size="24" maxlength="16" border="0" value="<ffi:getProperty name="AddLocation" property="LocationName"/>">
											<span id="AddLocation.locationNameError"></span>
										</td>
										<td>&nbsp;</td>
										<td>
											<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocationID" size="24" maxlength="15" border="0" value="<ffi:getProperty name="AddLocation" property="LocationID"/>">
											<span id="AddLocation.locationIDError"></span>
										</td>
										<td>&nbsp;</td>
										<td>
											<s:checkbox id="ActiveEditNewLocation" name="AddLocation.Active" value="%{#session.AddLocation.Active}" fieldValue="true"/>&nbsp;&nbsp;<s:text name="jsp.default_28"/>
										</td>
									</tr>
								</table>
								
								<table width="100%" border="0" cellspacing="0" cellpadding="3"><!-- Bank -->
									<tr  class="columndataauto" >
										<td colspan="4"><p class="transactionHeading"><s:text name="jsp.user_188"/></p></td>
									</tr>
									<tr class="columndataauto">
										<td class="sectionsubhead ltrow2_color" align="left" height="50" width="180">
											<input type="radio" name="AddLocation.CustomLocalBank" value="false" onclick="updateAccountsList(AddEditLocationForm['AddLocation.LocalRoutingNumber'].value);" <ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="false" operator="equals">checked</ffi:cinclude>>
											&nbsp;<s:text name="jsp.default_61"/>&nbsp;<ffi:getProperty name="TempBankIdentifierDisplayText"/>:<span class="required">*</span>
										</td>
										<td class="sectionsubhead ltrow2_color" width="230" height="50" valign="middle">
											<input class="ui-widget-content ui-corner-all" type="text" id="ROUTINGNUMBER" name="AddLocation.LocalRoutingNumber" size="18" maxlength="9" border="0" value="<ffi:getProperty name="AddLocation" property="LocalRoutingNumber"/>" onchange="updateAccountsList(AddEditLocationForm['AddLocation.LocalRoutingNumber'].value); AddEditLocationForm['AddLocation.CustomLocalBank'][0].checked = true;">
										</td>
										<td class="sectionsubhead ltrow2_color" width="270">
											<span class="ffivisible marginRight10"><s:text name="jsp.default_306" /><span>   &nbsp; &nbsp; &nbsp; &nbsp;
											<input id="BANKNAME" class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalBankName" size="18" maxlength="<%= Integer.toString( com.ffusion.tasks.Task.MAX_BANK_NAME_LENGTH ) %>" border="0" value="<ffi:getProperty name="AddLocation" property="LocalBankName"/>">
											<span id="AddLocation.localBankNameError"></span>
										</td>
										<td class="sectionsubhead ltrow2_color">
											<sj:a button="true" title="%{getText('jsp.default_373')}" onClickTopics="location_selectBankTopics"><s:text name="jsp.default_373" /></sj:a>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&nbsp;<span id="AddLocation.localRoutingNumberError"></span></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td colspan="4" height="50">
											<input type="radio" name="AddLocation.CustomLocalBank" value="true" onclick="updateAccountsListByID($('#selectBankID').val());" <ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="true" operator="equals">checked</ffi:cinclude>>
											<select id="selectBankID" class="txtbox" name="AddLocation.LocalBankID" onchange="updateAccountsListByID($('#selectBankID').val()); AddEditLocationForm['AddLocation.CustomLocalBank'][1].checked = true;">
												<ffi:list collection="AffiliateBanks" items="Bank">
													<option value="<ffi:getProperty name="Bank" property="AffiliateBankID"/>" <ffi:cinclude value1="${AddLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">selected</ffi:cinclude>>
														<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
													</option>
												</ffi:list>
											</select>
										</td>
									</tr>
									<!-- Bank  -->
									<tr>
										<td>&nbsp;</td>
									</tr>
									<!-- Account Information  -->
									<tr class="columndataauto">
										<td colspan="4">
										<table  width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="sectionsubhead ltrow2_color" align="left"  height="50" width="150">
												<input type="radio" name="AddLocation.CustomLocalAccount" value="false" <ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="false" operator="equals">checked</ffi:cinclude>>
												&nbsp;<span class="sectionsubhead"><s:text name="jsp.default_19"/><span class="required">*</span>:</span>
											</td>
											<td class="sectionsubhead ltrow2_color" width="250">
												<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalAccountNumber" size="18" maxlength="17" border="0" value="<ffi:getProperty name="AddLocation" property="LocalAccountNumber"/>" onchange="AddEditLocationForm['AddLocation.CustomLocalAccount'][0].checked = true;">
											</td>
											<td class="sectionsubhead ltrow2_color"  width="100">
												<span class="sectionsubhead"><s:text name="jsp.default_20"/>:</span>
											</td>
											<td class="sectionsubhead ltrow2_color">
												<select id="selectAccountTypeID" class="txtbox" name="AddLocation.LocalAccountType">
													<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING %>" <ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_64"/></option>
													<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS %>" <ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_281"/></option>
												</select>
											</td>
										</tr>
										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;<span id="AddLocation.localAccountNumberError"></span></td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td colspan="4" height="50">
												<input type="radio" name="AddLocation.CustomLocalAccount" value="true" <ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="true" operator="equals">checked</ffi:cinclude>>
												&nbsp;&nbsp;
												<select id="selectAccountID" class="txtbox" name="AddLocation.LocalAccountID" onclick="AddEditLocationForm['AddLocation.CustomLocalAccount'][1].checked = true;">
												    <option value=""><s:text name="jsp.default_296"/></option>
												</select><span id="AddLocation.LocalAccountIDError"></span>
											</td>
										</tr>
										</table>
										</td>
									</tr>
									<!-- Account Information  -->
								</table>
								
								<!-- Remote Bank information -->
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_276"/></p></td>
									</tr>
									<tr class="columndataauto" >
										<td><span class="sectionsubhead"><s:text name="jsp.user_55"/>:</span></td>
										<td><span class="sectionsubhead"><s:text name="jsp.user_75"/>:</span></td>
										<td><span class="sectionsubhead"><s:text name="jsp.user_117"/>:</span></td>
									</tr>
									<tr class="columndataauto" >
										<td>
										<select id="selectCashConCompanyID" class="txtbox" name="AddLocation.CashConCompanyBPWID" onchange="updateCCAccountsList(this.selectedIndex);updateFields();" <ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">disabled</ffi:cinclude>>
											<ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">
										    	<option value=""><s:text name="jsp.default_296"/></option>
											</ffi:cinclude>
											<ffi:list collection="CashConCompanies" items="tempCompany">
												<option value="<ffi:getProperty name="tempCompany" property="BPWID"/>" 
												
												SameDayCashConEnabled="<ffi:getProperty name="SameDayCashConEnabled"/>"  
												ConcSameDayCutOffDefined="<ffi:getProperty name="tempCompany" property="ConcSameDayCutOffDefined"/>"
												ConcCutOffsPassed="<ffi:getProperty name="tempCompany" property="ConcCutOffsPassed"/>"
												DepositSameDayPrenote="<ffi:getProperty name="AddLocation" property="DepositSameDayPrenote"/>"
												DisbSameDayCutOffDefined="<ffi:getProperty name="tempCompany" property="DisbSameDayCutOffDefined"/>"
												DisbCutOffsPassed="<ffi:getProperty name="tempCompany" property="DisbCutOffsPassed"/>"
												DisbursementSameDayPrenote="<ffi:getProperty name="AddLocation" property="DisbursementSameDayPrenote"/>"
												DepositPrenote="<ffi:getProperty name="AddLocation" property="DepositPrenote"/>"
												DisbursementPrenote="<ffi:getProperty name="AddLocation" property="DisbursementPrenote"/>"
												
												<ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">selected</ffi:cinclude>>
												
												<ffi:getProperty name="tempCompany" property="CompanyName"/>	
												</option>
											</ffi:list>
										</select>
										</td>
										<td>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
												<select id="selectConcAccountID" class="txtbox" name="AddLocation.ConcAccountBPWID" disabled>
													<option value="" selected><s:text name="jsp.default_296"/></option>
												</select>
											</ffi:cinclude>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
													<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="false" operator="equals">
												<select id="selectConcAccountID" class="txtbox" name="AddLocation.ConcAccountBPWID" onchange="updateFields();" disabled>
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="true" operator="equals">
												<select id="selectConcAccountID" class="txtbox" name="AddLocation.ConcAccountBPWID" onchange="updateFields();">
														<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
															<option value="<ffi:getProperty name="ConcAccount" property="BPWID"/>" <ffi:cinclude value1="${AddLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																<ffi:getProperty name="ConcAccount" property="DisplayText"/>
																<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
														 		</ffi:cinclude>	
															</option>
														</ffi:list>
													</ffi:cinclude>
												</select>
											</ffi:cinclude>
										</td>
										<td>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
												<select id="selectDisbAccountID" class="txtbox" name="AddLocation.DisbAccountBPWID" disabled>
													<option value="" selected><s:text name="jsp.default_296"/></option>
												</select>
											</ffi:cinclude>
											<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:cinclude value1="${CashConCompany.DisbEnabledString}" value2="false" operator="equals">
												<select id="selectDisbAccountID" class="txtbox" name="AddLocation.DisbAccountBPWID" onchange="updateFields();" disabled>
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</ffi:cinclude>
													<ffi:cinclude value1="${CashConCompany.DisbEnabledString}" value2="true" operator="equals">
												<select id="selectDisbAccountID" class="txtbox" name="AddLocation.DisbAccountBPWID" onchange="updateFields();">
														<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
															<option value="<ffi:getProperty name="DisbAccount" property="BPWID"/>" <ffi:cinclude value1="${AddLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																<ffi:getProperty name="DisbAccount" property="DisplayText"/>
																<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
																	 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
														 		</ffi:cinclude>	
															</option>
														</ffi:list>
													</ffi:cinclude>
												</select>
											</ffi:cinclude>
										</td>
									</tr>
								</table>
								<!-- Remote Bank information -->
								
								<!-- Deposit-specific information -->
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_113"/></p></td>
									</tr>
									<tr class="columndataauto" >
										<td><span class="sectionsubhead"><s:text name="jsp.user_112"/>:</span></td>
										<td><span class="sectionsubhead"><s:text name="jsp.user_111"/>:</span></td>
										<td><span class="sectionsubhead"><s:text name="jsp.user_44"/>:</span></td>
									</tr>
									<tr class="columndataauto" >
										<td>
											<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.DepositMinimum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="AddLocation" property="DepositMinimum"/>" <% } else { %> disabled <% } %>>
											<br><span id="depositMinimumError"></span>
										</td>
										<td>
											<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.DepositMaximum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="AddLocation" property="DepositMaximum"/>" <% } else { %> disabled <% } %>>
											<br><span id="depositMaximumError"></span>
										</td>
										<td>
											<input class="ui-widget-content ui-corner-all" type="text" id="AnticDeposit" name="AddLocation.AnticDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="AddLocation" property="AnticDeposit"/>" <% } else { %> disabled <% } %>>
											<br><span id="anticDepositError"></span>
										</td>
									</tr>
									<tr>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr class="columndataauto" >
										<td><span class="sectionsubhead"><s:text name="jsp.user_337"/>:</span></td>
										<td></td>
										<td></td>
									</tr>
									<tr class="columndataauto" >
										<td>
											<input class="ui-widget-content ui-corner-all" type="text" id="ThreshDeposit" name="AddLocation.ThreshDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="AddLocation" property="ThreshDeposit"/>" <% } else { %> disabled <% } %>>
											<br><span id="threshDepositError"></span></td>
										<td>
											<s:checkbox id="ConsolidateDepositsEditNewLocation" name="AddLocation.ConsolidateDeposits" value="%{#session.AddLocation.ConsolidateDeposits}" fieldValue="true"/>
											&nbsp;<span class="sectionsubhead"><s:text name="jsp.user_94"/></span>
										</td>
										<td id="regularDepositPrenoteCheckboxClass" class="displayNone">
											 <div id="regularDepositPrenoteCheckbox">
											     <s:checkbox id="regularDepositPrenote" name="AddLocation.DepositPrenote" value="%{#session.AddLocation.DepositPrenote}" fieldValue="true"/>
												 <span class="sectionsubhead"><s:text name="jsp.user_110"/></span>
											 </div>
										</td>
									</tr>
									<tr class="columndataauto" >
										<td id="depositSameDayPrenoteClass" colspan="3" class="displayNone">
											<table width="84%">
												<tr class="columndataauto">
													<td>
														<div id="depositPrenoteLabel" >
															<span class="sectionsubhead">
																<s:text name="jsp.user_110"/>
															</span>
														</div>	
													</td>
													<td>
														<div id="depositPrenoteCheckbox" >
															<span>
																<s:checkbox id="DepositPrenote" name="AddLocation.DepositPrenote" 
																value="%{#session.AddLocation.DepositPrenote}" fieldValue="true" 
																onchange="removeDepositSameDayPrenoteChecked(this);"/>
															</span><span class="sectionsubhead"><s:text name="jsp.user_571"/></span>
														</div>		
													</td>
													 <td>
														<div id="depositSameDayPrenoteCheckbox" >
															<span>
																<s:checkbox id="DepositSameDayPrenote" name="AddLocation.DepositSameDayPrenote" 
																value="%{#session.AddLocation.DepositSameDayPrenote}"
																fieldValue="true" onchange="removeDepositNormalPrenoteChecked(this);"/>
															</span> <span class="sectionsubhead"><s:text name="jsp.user_572"/></span>
														 </div>
													 </td>
												</tr>
												<tr class="columndataauto">
													<td colspan="3">
														<table width="100%">
															<tr>
																<td>
																	<div id="showConcNote" >
																		<span class="required">&nbsp;&nbsp;<s:text name="jsp.common.note.sameDayCutOffPassed"/></span>
																	</div>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>	
								</table>
								<!-- Deposit-specific information -->
								
								<!-- Disbursement-Request-Specific-Information -->
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr  class="columndataauto" >
										<td><p class="transactionHeading"><s:text name="jsp.user_119"/></p></td>
									</tr>
									<tr class="columndataauto" >
										<td id="regularDisbursementPrenoteCheckboxClass" class="displayNone">
											 <div id="regularDisbursementPrenoteCheckbox" >
										     	<span> <s:checkbox id="regularDisbursementPrenote" name="AddLocation.DisbursementPrenote" value="%{#session.AddLocation.DisbursementPrenote}" fieldValue="true"/>
											 </span><span class="sectionsubhead"><s:text name="jsp.user_118"/></span>
											 </div>
									    </td>
									</tr>
									<tr class="columndataauto">
										<td id="disbursementSameDayPrenoteClass" colspan="3" class="displayNone">
									    	<table width="70%">
									    	<tr class="columndataauto">
										    	<td>
													<div id="disbursementPrenoteLabel" >
														<span class="sectionsubhead"><s:text name="jsp.user_118"/></span>
													</div>
												</td>
												<td>
													 <div id="disbursementPrenoteCheckbox" >
													 	<span><s:checkbox id="DisbursementPrenote" name="AddLocation.DisbursementPrenote" 
																value="%{#session.AddLocation.DisbursementPrenote}" fieldValue="true" 
																onchange="removeDisbursementSameDayPrenoteChecked(this);"/>
														</span><span class="sectionsubhead"><s:text name="jsp.user_571"/></span>		
					 								 </div>	
												</td>
												
												
												<td>
													  <div id="disbursementSameDayPrenoteCheckbox" >
													  <span>
													  	<s:checkbox id="DisbursementSameDayPrenote" name="AddLocation.DisbursementSameDayPrenote" 
														value="%{#session.AddLocation.DisbursementSameDayPrenote}" fieldValue="true" 
														onchange="removeDisbursementNormalPrenoteChecked(this);"/>
													  </span><span class="sectionsubhead"><s:text name="jsp.user_572"/></span>
												  </div>
												</td>
												
											</tr>
											<tr class="columndataauto">
												<td colspan="3">
													<table width="100%">
														<tr>
															<td>
																<div id="showDisbNote" >
																<span class="required">&nbsp;&nbsp;<s:text name="jsp.common.note.sameDayCutOffPassed"/></span></div>
															</td>
														</tr>
													</table>
												</td>
											</tr>
									    	</table>
									    </td>
									</tr>
								</table>
								<!-- Disbursement-Request-Specific-Information -->
								
								<%-- <table border="0" cellspacing="0" cellpadding="3" width="710">
									<tr>
										<td colspan="4" class="adminBackground tbrd_b">
											<span class="sectionhead"><s:text name="jsp.user_188"/></span><br>
										</td>
										<td class="adminBackground">&nbsp;</td>
										<td class="adminBackground">&nbsp;</td>
										<td class="tbrd_b adminBackground" colspan="4">
											<span class="sectionhead"><s:text name="jsp.user_113"/></span>
										</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="4">
											<div align="left"><span class="sectionsubheadDA"><s:text name="jsp.default_61"/></span></div>
										</td>
										<td valign="top" class="tbrd_r adminBackground" rowspan="13">&nbsp;</td>
										<td valign="top" class="adminBackground" rowspan="13">&nbsp;</td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.user_112"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.DepositMinimum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="AddLocation" property="DepositMinimum"/>" <% } else { %> disabled <% } %>>
												<span id="depositMinimumError"></span>
											</div>
										</td>
									</tr>
									<tr valign="middle">
										<td>
											<div align="left">
												&nbsp;&nbsp;<input type="radio" name="AddLocation.CustomLocalBank" value="false" onclick="updateAccountsList(AddEditLocationForm['AddLocation.LocalRoutingNumber'].value);" <ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="false" operator="equals">checked</ffi:cinclude>>
											</div>
										</td>
										<td nowrap>
											<div align="right"><span class="sectionsubheadDA">
											  <ffi:getProperty name="TempBankIdentifierDisplayText"/>
											:</span></div>
										</td>
										<td>
											<div align="left">
												<input class="ui-widget-content ui-corner-all" type="text" id="ROUTINGNUMBER" name="AddLocation.LocalRoutingNumber" size="18" maxlength="9" border="0" value="<ffi:getProperty name="AddLocation" property="LocalRoutingNumber"/>" onchange="updateAccountsList(AddEditLocationForm['AddLocation.LocalRoutingNumber'].value); AddEditLocationForm['AddLocation.CustomLocalBank'][0].checked = true;">
											</div>
										</td>
										<td>
											<sj:a button="true" title="%{getText('jsp.default_373')}" onClickTopics="location_selectBankTopics"><s:text name="jsp.default_373" /></sj:a>
										</td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.user_111"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.DepositMaximum" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="AddLocation" property="DepositMaximum"/>" <% } else { %> disabled <% } %>>
											</div>
										</td>
									</tr>
									<tr valign="middle">
										<td>&nbsp;</td>
										<td nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.default_63"/>:</span></div>
										</td>
										<td>
											<input id="BANKNAME" class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalBankName" size="18" maxlength="<%= Integer.toString( com.ffusion.tasks.Task.MAX_BANK_NAME_LENGTH ) %>" border="0" value="<ffi:getProperty name="AddLocation" property="LocalBankName"/>">
											<span id="AddLocation.localBankNameError"></span>
										</td>
										<td></td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.user_44"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<input class="ui-widget-content ui-corner-all" type="text" id="AnticDeposit" name="AddLocation.AnticDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="AddLocation" property="AnticDeposit"/>" <% } else { %> disabled <% } %>>
												<span id="anticDepositError"></span>
											</div>
										</td>
									</tr>
									<tr valign="middle">
										<td>
											&nbsp;&nbsp;<input type="radio" name="AddLocation.CustomLocalBank" value="true" onclick="updateAccountsListByID(AddEditLocationForm['AddLocation.LocalBankID'].value);" <ffi:cinclude value1="${AddLocation.CustomLocalBank}" value2="true" operator="equals">checked</ffi:cinclude>>
										</td>
										<td colspan="3">
											<div align="left">
												<select id="selectBankID" class="txtbox" name="AddLocation.LocalBankID" onchange="updateAccountsListByID(AddEditLocationForm['AddLocation.LocalBankID'].value); AddEditLocationForm['AddLocation.CustomLocalBank'][1].checked = true;">
													<ffi:list collection="AffiliateBanks" items="Bank">
														<option value="<ffi:getProperty name="Bank" property="AffiliateBankID"/>" <ffi:cinclude value1="${AddLocation.LocalBankID}" value2="${Bank.AffiliateBankID}" operator="equals">selected</ffi:cinclude>>
															<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
														</option>
													</ffi:list>
												</select>
											</div>
										</td>
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.user_337"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<input class="ui-widget-content ui-corner-all" type="text" id="ThreshDeposit" name="AddLocation.ThreshDeposit" size="18" maxlength="11" border="0" <% if (depositEnabled) { %> value="<ffi:getProperty name="AddLocation" property="ThreshDeposit"/>" <% } else { %> disabled <% } %>>
												<span id="threshDepositError"></span>
											</div>
										</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="4">
											<div align="left"> <span class="sectionsubheadDA"><s:text name="jsp.default_15"/></span></div>
										</td>
										<td>
											<s:checkbox id="ConsolidateDepositsEditNewLocation" name="AddLocation.ConsolidateDeposits" value="%{#session.AddLocation.ConsolidateDeposits}" fieldValue="true"/>
										</td>
										<td colspan="3"><span class="sectionsubheadDA"><s:text name="jsp.user_94"/></span></td>
									</tr>
									<tr valign="middle">
										<td>
											<div align="left">
												&nbsp;&nbsp;<input type="radio" name="AddLocation.CustomLocalAccount" value="false" <ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="false" operator="equals">checked</ffi:cinclude>>
											</div>
										</td>
										<td nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.default_19"/>:</span></div>
										</td>
										<td>
											<div align="left">
												<input class="ui-widget-content ui-corner-all" type="text" name="AddLocation.LocalAccountNumber" size="18" maxlength="17" border="0" value="<ffi:getProperty name="AddLocation" property="LocalAccountNumber"/>" onchange="AddEditLocationForm['AddLocation.CustomLocalAccount'][0].checked = true;">
												<span id="AddLocation.localAccountNumberError"></span>
											</div>
										</td>
										<td>&nbsp; </td>
										<td>
											<s:checkbox id="DepositPrenoteEditNewLocationID" name="AddLocation.DepositPrenote" value="%{#session.AddLocation.DepositPrenote}" fieldValue="true"/>
										</td>
										<td colspan="3"><span class="sectionsubheadDA"><s:text name="jsp.user_110"/></span></td>
									</tr>
									<tr valign="middle">
										<td>&nbsp;</td>
										<td nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.default_20"/>:</span></div>
										</td>
										<td>
											<select id="selectAccountTypeID" class="txtbox" name="AddLocation.LocalAccountType">
												<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING %>" <ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_64"/></option>
												<option value="<%= com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS %>" <ffi:cinclude value1="${AddLocation.LocalAccountType}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_281"/></option>
											</select>
										</td>
										<td></td>
										<td colspan="4">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td>
											&nbsp;&nbsp;<input type="radio" name="AddLocation.CustomLocalAccount" value="true" <ffi:cinclude value1="${AddLocation.CustomLocalAccount}" value2="true" operator="equals">checked</ffi:cinclude>>
										</td>
										<td colspan="3">
											<div align="left">
												<select id="selectAccountID" class="txtbox" name="AddLocation.LocalAccountID" onclick="AddEditLocationForm['AddLocation.CustomLocalAccount'][1].checked = true;">
												    <option value=""><s:text name="jsp.default_296"/></option>
												</select><span id="AddLocation.LocalAccountIDError"></span>
											</div>
										</td>
										<td colspan="4" class="tbrd_b"><span class="sectionhead"><s:text name="jsp.user_119"/></span></td>
									</tr>
									<tr valign="middle">
										<td colspan="4">&nbsp;</td>
										<td>
											<s:checkbox id="DisbursementPrenoteEditAddNewLocationID" name="AddLocation.DisbursementPrenote" value="%{#session.AddLocation.DisbursementPrenote}" fieldValue="true"/>
										</td>
										<td colspan="3" nowrap><span class="sectionsubheadDA"><s:text name="jsp.user_118"/></span></td>
									</tr>
									<tr valign="middle">
										<td class="tbrd_b adminBackground" colspan="4"><span class="sectionhead"><s:text name="jsp.user_276"/></span></td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.user_55"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<select id="selectCashConCompanyID" class="txtbox" name="AddLocation.CashConCompanyBPWID" onchange="updateCCAccountsList(this.selectedIndex);updateFields();" <ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">disabled</ffi:cinclude>>
													<ffi:cinclude value1="${CashConCompanies.Size}" value2="0" operator="equals">
												    	<option value=""><s:text name="jsp.default_296"/></option>
													</ffi:cinclude>
													<ffi:list collection="CashConCompanies" items="tempCompany">
														<option value="<ffi:getProperty name="tempCompany" property="BPWID"/>" <ffi:cinclude value1="${AddLocation.CashConCompanyBPWID}" value2="${tempCompany.BPWID}" operator="equals">selected</ffi:cinclude>>
															<ffi:getProperty name="tempCompany" property="CompanyName"/>
														</option>
													</ffi:list>
												</select>
											</div>
										</td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.user_75"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
													<select id="selectConcAccountID" class="txtbox" name="AddLocation.ConcAccountBPWID" disabled>
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</select>
												</ffi:cinclude>
												<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
														<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="false" operator="equals">
													<select id="selectConcAccountID" class="txtbox" name="AddLocation.ConcAccountBPWID" onchange="updateFields();" disabled>
															<option value="" selected><s:text name="jsp.default_296"/></option>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany.ConcEnabledString}" value2="true" operator="equals">
													<select id="selectConcAccountID" class="txtbox" name="AddLocation.ConcAccountBPWID" onchange="updateFields();">
															<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">
																<option value="<ffi:getProperty name="ConcAccount" property="BPWID"/>" <ffi:cinclude value1="${AddLocation.ConcAccountBPWID}" value2="${ConcAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																	<ffi:getProperty name="ConcAccount" property="DisplayText"/>
																	<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
																		 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
															 		</ffi:cinclude>	
																</option>
															</ffi:list>
														</ffi:cinclude>
													</select>
												</ffi:cinclude>
											</div>
										</td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
									<tr valign="middle">
										<td class="adminBackground" colspan="2" nowrap>
											<div align="right"><span class="sectionsubheadDA"><s:text name="jsp.user_117"/>:</span></div>
										</td>
										<td class="adminBackground" colspan="2">
											<div align="left">
												<ffi:cinclude value1="${CashConCompany}" value2="" operator="equals">
													<select id="selectDisbAccountID" class="txtbox" name="AddLocation.DisbAccountBPWID" disabled>
														<option value="" selected><s:text name="jsp.default_296"/></option>
													</select>
												</ffi:cinclude>
												<ffi:cinclude value1="${CashConCompany}" value2="" operator="notEquals">
															<ffi:cinclude value1="${CashConCompany.DisbEnabledString}" value2="false" operator="equals">
													<select id="selectDisbAccountID" class="txtbox" name="AddLocation.DisbAccountBPWID" onchange="updateFields();" disabled>
															<option value="" selected><s:text name="jsp.default_296"/></option>
														</ffi:cinclude>
														<ffi:cinclude value1="${CashConCompany.DisbEnabledString}" value2="true" operator="equals">
													<select id="selectDisbAccountID" class="txtbox" name="AddLocation.DisbAccountBPWID" onchange="updateFields();">
															<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">
																<option value="<ffi:getProperty name="DisbAccount" property="BPWID"/>" <ffi:cinclude value1="${AddLocation.DisbAccountBPWID}" value2="${DisbAccount.BPWID}" operator="equals">selected</ffi:cinclude>>
																	<ffi:getProperty name="DisbAccount" property="DisplayText"/>
																	<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
																		 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
															 		</ffi:cinclude>	
																</option>
															</ffi:list>
														</ffi:cinclude>
													</select>
												</ffi:cinclude>
											</div>
										</td>
										<td>&nbsp;</td>
										<td colspan="3">&nbsp;</td>
									</tr>
								</table> --%>
								<br><br>
								<s:url id="locationEditURL" value="%{#session.PagesPath}user/da-location-edit.jsp" escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
										<s:param name="addlocationreload" value="%{'false'}"/>
								</s:url>
								<sj:a
									href="%{locationEditURL}" 
									targets="viewLocationDialogID" 
									button="true"
									title="Reset"
									onClickTopics=""
									onCompleteTopics="openViewLocationDialog"
								><s:text name="jsp.default_358"/></sj:a>
								<sj:a
								   button="true"
								   onClickTopics="closeDialog"><s:text name="jsp.default_82"/></sj:a>
								<sj:a id="addEditSubmitLocation"
									formIds="AddEditLocationFormID"
									targets="viewLocationDialogID"
									button="true"
									validate="true"
									validateFunction="customValidation"
									onBeforeTopics="beforeVerify"
									onCompleteTopics="completeVerifyEditAddLocation"
									onErrorTopics="errorVerify"
									onSuccessTopics="successVerify"
									><s:text name="jsp.user_1"/></sj:a>
								<br><br>
							</div>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
<ffi:setProperty name="BackURL" value="${SecurePath}user/da-location-edit.jsp?addlocation-reload=false UseLastRequest=TRUE" URLEncrypt="true"/>
<ffi:removeProperty name="AddingFreeFormAccount"/>
<ffi:removeProperty name="autoEntitleLocations" />
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="oldDAObject"/>

<ffi:removeProperty name="editlocation-reload"/>
<ffi:removeProperty name="locationBpwdId"/>
