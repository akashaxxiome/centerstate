<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>

<script type="text/javascript" src="<s:url value='/web/js/userPreferences/userPreferences%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/userPreferences/userPreferences_Account_grid%{#session.minVersion}.js'/>"></script>

<ffi:help id="userprefs" className="moduleHelpClass"/>
<%-- Check if attribute 'ActualStartPage' available or not. 
This attribute will be there when we change menu settings and click on save. --%>
<s:if test="#session.ActualStartPage != null">
	<ffi:setProperty name="StartPage" value="${ActualStartPage}"/>
	<ffi:removeProperty name="ActualStartPage"/>
</s:if>


<s:url id="myProfileURL" action="ModifyUserProfileAction_init" namespace="/pages/jsp/user" escapeAmp="false">	
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
</s:url>
<%--This button will be used only in case of SecurityCheck dialog --%>
<sj:a
	button="true"
	id="reloadUserProfileBtn"
	onClickTopics="reloadUserProfileTab"
	cssStyle="display:none;"></sj:a>

<s:url id="menuURL" value="/pages/jsp/user/EditPreferenceAction_initializeMenuPreferences.action"/>
<s:url id="miscSettingURL" value="/pages/jsp/user/EditPreferenceAction_initOtherSettings.action"/>
<s:url id="accountSettingURL" value="/pages/jsp/user/AccountPrefSetting_init.action"/>
<s:url id="manageUsersSettingsURL" value="/pages/jsp/user/manageSecondaryUsers_init.action"/>


<div id="operationresult">
		<div id="resultmessage"><s:text name="jsp.default_498"/></div>
</div>
<sj:tabbedpanel id="preferencesTabs" collapsible="true"
	cache="false" cssStyle="overflow:visible;" onCompleteTopics="preferencesTabsCompleteTopics">
	<%-- Since this action is available in quick actions on Control Toolbar, will be removed from this page --%>
	<%--
	<sj:tab id="myProfileTab" href="%{myProfileURL}" key="MyProfileTab" label="%{getText('jsp.user.preferences.myProfileTabName')}" ></sj:tab>
     --%>
    <%-- Decide to show Accounts tab or not. Only consumer or small business user should be able to see this tab. --%>
<%--     <ffi:setProperty name="ShowAccountTab" value="False"/>
    <ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
		<ffi:setProperty name="ShowAccountTab" value="True"/>
	</ffi:cinclude>	
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>" operator="equals">
		<ffi:setProperty name="ShowAccountTab" value="True"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${ShowAccountTab}" value2="True" operator="equals">
		 <ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_SETUP%>" >
	    		<sj:tab id="accountSettingTab" href="%{accountSettingURL}" key="AccountSettingTab"
	    			label="%{getText('jsp.user.preferences.accountTabName')}"></sj:tab>
	    </ffi:cinclude>	
    </ffi:cinclude>  --%>
    <%-- Since this action is available in quick actions on Control Toolbar, will be removed from this page --%>
    <%--
    <sj:tab id="menuTab" href="%{menuURL}" key="MenuTab" label="%{getText('jsp.user.preferences.navigationTabName')}"></sj:tab>
     --%>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_USERS%>" >
		<sj:tab id="manageUsersTab" href="%{manageUsersSettingsURL}" key="ManageUsersTab" label="%{getText('jsp.user.preferences.manageUsersTabName')}"/>
	</ffi:cinclude>	
    <%-- <sj:tab id="otherSettingTab" href="%{miscSettingURL}" key="MiscSettingTab" label="%{getText('jsp.user.preferences.otherSettingTabName')}"></sj:tab> --%>
    <!-- Label needs to be pulled from prop file -->
    <%-- Since this action is available in quick actions on Control Toolbar, will be removed from this page --%>
    <%-- 
    <sj:tab id="otherSettingTab" href="%{miscSettingURL}" key="MiscSettingTab" label="Settings"></sj:tab>
    --%>
</sj:tabbedpanel>
<s:hidden value="home_prefs,a My_Profile" id="userPrefShortcutPath"></s:hidden>
<s:hidden value="NO_ENTITLEMENT_CHECK" id="userPrefShortcutEntitlement"></s:hidden>

<script type="text/javascript">
	$(document).ready(function(){
		
	
	$('#preferencesTabs').find('ul').addClass('portlet-header');
	$('#preferencesTabs').portlet({
		bookmark: true,
		helpCallback: function(){			
			var helpFile = "";
			$('#preferencesTabs').find('.ui-tabs-panel').each(function(){
			    if($(this).attr('aria-hidden') == "false"){
			    	helpFile = $(this).find('.moduleHelpClass').html();
			    }
			});
			
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		},
		bookMarkCallback: function(){
			var path = $('#userPrefShortcutPath').val();
			var ent  = $('#userPrefShortcutEntitlement').val();
			ns.shortcut.openShortcutWindow( path, ent );
		},
		duration:300
	});
	$("#preferencesTabs").sortable({ items: 'li'});
	
	if(accessibility){
		$("#preferencesTabs" ).tabs();
	}
	});
</script>


<%-- Check if tab to be selected is set in the session or not, if selected then value set in session should be considered --%>
<s:if test="#session.TabToBeSelected  != null">
	
	<script type="text/javascript">
	$(document).ready(function(){
		var tabToBeSelected = parseInt('<ffi:getProperty name="TabToBeSelected"/>', 10);		
		$("#preferencesTabs").tabs( "option", "active", tabToBeSelected);
	});
	</script>
	
	<ffi:removeProperty name="TabToBeSelected"/>
</s:if>
<s:else>
	<script type="text/javascript">	
		<!--
		$(document).ready(function(){
			
		//This code will take care of selecting 1st tab if none is selected in Preferences tabs.		
		//var currentlySelected = $("#preferencesTabs").tabs( "option", "active");		
		if(ns.userPreference.tabToBeSelected !=-1){
			$("#preferencesTabs").tabs( "option", "active", ns.userPreference.tabToBeSelected);
			ns.userPreference.tabToBeSelected =-1 ;
		}
	});
		//-->
	</script>
</s:else>
	
	
<sj:dialog id="resultDialog" autoOpen="false" modal="true" title="%{getText('jsp.user.preferences.save.dialog.title')}" height="auto" width="250" overlayColor="#000" overlayOpacity="0.7" 
	buttons="{'%{getText(\"jsp.default_175\")}': function(){ $('#resultDialog').dialog('close');}}" cssClass="home_prefsDialog staticContentDialog"
	resizable="false">
		<div id="userpreferenceConfirmTextID" align="center">
			<table style="width:100%; height:100%;" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata" style="text-align: center;"><s:text name="jsp.user_390"/></td>
				</tr>	
			</table>
		</div>
</sj:dialog>

<ffi:removeProperty name="ShowAccountTab"/>
<script type="text/javascript" src="<s:url value='/web/js/user/secondaryUsers%{#session.minVersion}.js'/>"></script>
<script>
	$(document).ready(function(){
		$( "#preferencesTabs" ).tabs({
			load:function(event,ui){
				var responseText = ui.panel.html();
				if(responseText){
					responseText = $.trim(responseText);
				}
				if(responseText == "[[Exception:601]]"){//Fix for exception getting dumped in container
					ui.panel.html('');
				}
			},
			activate: function( event, ui ) {
				if(ui.newTab.attr("id") == "myProfileTab"){
					var $myProfileTab = $(event.currentTarget);
					$myProfileTab.attr("enableglobaltrack","false");//This escapes the track event
					ns.common.activeSubmitButton = "reloadUserProfileBtn";
				}
			}
		});
	});
</script>