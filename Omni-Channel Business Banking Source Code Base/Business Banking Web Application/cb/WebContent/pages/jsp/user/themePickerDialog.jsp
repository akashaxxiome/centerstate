<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<div style="width:100%; margin:20px auto; text-align:center;">
	<div id="themePickerContainer" class="themePickerCls" onclick="ns.home.isThemeChanged();$('body').layout().resizeAll();">
	</div>
</div>
		<div class="ui-widget-header customDialogFooter">
			<sj:a id="cancelOtherpreferenceButtonId" button="true" onclick="$.publish('closeDialog');">
				<s:text name="jsp.default_102"/>
			</sj:a>
		</div>
		
<script type="text/javascript">

$.subscribe('ns.home.saveThemeTopic', function(event,data) {
	ns.home.saveCurrentTheme();
});

ns.home.saveCurrentTheme = function(){
	var currentTheme = $.themes.currentTheme;
	if(currentTheme != undefined || currentTheme != ''){
	    $.ajax({
	        url: "/cb/pages/jsp/home/manageTheme_saveTheme.action",
	        type: "POST",
	        async: false,
	        global:false,
	        data: {themeName: currentTheme,CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
	        success: function(data){
            	ns.home.userCustomThemeTitle = currentTheme;
				// Sets the saved Theme ID in a cookie variable 
				ns.common.setCookie("UserTheme", ns.common.getCookie("themeID"));
	        }
	    });
	    $('body').layout().resizeAll();
	}
};

$(document).ready(function(){
	var ids = new Array();
	var counter = 0;
	var display;
	var iconUrl;
	var spriteTag;
	var cssUrl;
	var cssPatchUrl;
	<ffi:list collection="UserThemes" items="theme">
		ids[counter] = '<ffi:getProperty name="theme" property="id"/>';
		display = '<ffi:getProperty name="theme" property="display"/>';
		iconUrl = '<ffi:getProperty name="theme" property="iconUrl"/>';
		spriteTag = iconUrl.substring(0,1);
		if (spriteTag == '#') {
			var spriteNum = iconUrl.substring(1,iconUrl.length);
			iconUrl = spriteNum - 0;
		}
		cssUrl = '<ffi:getProperty name="theme" property="cssUrl"/>';
		cssPatchUrl = '<ffi:getProperty name="theme" property="cssPatchUrl"/>';
		//$.themes.addTheme( ids[counter], display, iconUrl, '#', cssUrl );
		$.themes._themes[ids[counter]] = {display: display, icon: iconUrl, preview: spriteTag, url: cssUrl , cssPatchUrl: cssPatchUrl};
		counter++;
	</ffi:list>
			
	$('#themePickerContainer').themes({
		themes : ids
	});
	
});
</script>
<span id="themeresultmessage" style="display:none">
	<s:text name="jsp.user_390"/>
</span>