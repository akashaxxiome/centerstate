<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<div id="accountaccessVerifyDiv" class="remoteAcctAccess">
<ffi:help id="user_accountaccess-verify" className="moduleHelpClass"/>
<ffi:setProperty name="SavePermissionsWizard" value="${PermissionsWizard}"/>		

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:removeProperty name="DARequest" />
	<ffi:removeProperty name="Entitlement_Limits" />
	<ffi:object id="GetLimitsFromDA" name="com.ffusion.tasks.dualapproval.GetLimitsFromDA" scope="session"/>
	<ffi:object id="GetEntitlementsFromDA" name="com.ffusion.tasks.dualapproval.GetEntitlementsFromDA" scope="session"/>
	<ffi:setProperty name="GetLimitsFromDA" property="limitListSessionName" value="Entitlement_Limits"/>
	<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits" scope="session"/>
 	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
 	<ffi:setProperty name="GetGroupLimits" property="GroupId" value="${EditGroup_GroupId}"/>
    <ffi:setProperty name="GetGroupLimits" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
    <ffi:setProperty name="GetGroupLimits" property="ObjectType" value="Account"/>
</ffi:cinclude>

<ffi:setProperty name='PageHeading' value='Verify Account Access'/>
<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="session"/>
<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${SecureUser.BaseCurrency}"/>
<%--
<ffi:include page="${PathExt}user/inc/SetPageText.jsp"/>
--%>

<ffi:object id="GetEntitlementTypePropertyList" name="com.ffusion.efs.tasks.entitlements.GetEntitlementTypePropertyList"/>
<ffi:setProperty name="GetEntitlementTypePropertyList" property="ListName" value="Access"/>
<ffi:setProperty name="GetEntitlementTypePropertyList" property="TypeToLookup" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
<ffi:process name="GetEntitlementTypePropertyList"/>

<%-- we need to do auto entitlement checking for company, division and group being modified --%>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">

<%-- get the auto entitle settings for the business --%>
<%-- assuming the correct Entitlement_EntitlementGroup is in session --%>
<ffi:object name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" id="GetCumulativeSettings" scope="session"/>
<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupSessionKey" value="Entitlement_EntitlementGroup"/>
<ffi:process name="GetCumulativeSettings"/>

</s:if>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">

<%-- get the auto entitle settings for the user being edited --%>
<ffi:object name="com.ffusion.efs.tasks.entitlements.GetMember" id="GetMember" scope="session" />
<ffi:setProperty name="GetMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
<ffi:setProperty name="GetMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
<ffi:setProperty name="GetMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
<ffi:setProperty name="GetMember" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>

<ffi:process name="GetMember"/>
<ffi:removeProperty name="GetMember"/>

<ffi:object name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" id="GetCumulativeSettings" scope="session"/>
<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupMemberSessionKey" value="Entitlement_Group_Member"/>
<ffi:process name="GetCumulativeSettings"/>

<ffi:removeProperty name="Entitlement_Group_Member"/>

</s:if>


<ffi:setProperty name="NumTotalColumns" value="10"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="9"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
	<s:if test="#session.BusinessEmployee.UsingEntProfiles && #session.Section == 'Profiles'}">
	</s:if>
	<s:else>	 
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="9"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="9"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	</s:else>
</s:if>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="9"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="inc/disableAdminCheckBoxForProfiles.jsp" />

<%-- set action --%>
<% String action = "editAccountAccessPermissions-execute" ;%>

<ffi:removeProperty name="doAutoEntitle"/>
<%-- Initialize -- make available number of granted accounts --%>
<%-- (init only mode set for task) --%>
<ffi:setProperty name="EditAccountAccessPermissions" property="InitOnly" value="true"/>
<ffi:process name="EditAccountAccessPermissions"/>

<%-- do the autoentitle action if account access has been granted and: --%>
<%-- non DA: auto entitle is on --%>
<%-- DA: permissions being edited are not for a user --%>
<ffi:cinclude value1="${GetCumulativeSettings.EnableAccounts}" value2="true" operator="equals">
	<ffi:cinclude value1="${EditAccountAccessPermissions.NumGrantedAccounts}" value2="0" operator="notEquals">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<% action = "editAccountAccessPermissions-autoentitle"; %>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
				<% action = "editAccountAccessPermissions-autoentitle"; %>
			</s:if>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<% 
	request.setAttribute("action", action);
	session.setAttribute("action_execute", "editAccountAccessPermissions-execute");
%>

<!--<form action="<ffi:getProperty name='SuccessURL'/>" name="FormName" method="post">-->
<s:form namespace="/pages/user" action="%{#request.action}" validate="false" theme="simple" method="post" name="permLimitFrmVerify" id="permLimitFrmVerify">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
 <!--
      <input type="hidden" name="EditAccountAccessPermissions.ParentGroupId" value='<ffi:getProperty name="Entitlement_EntitlementGroup" property="ParentId"/>'>
      <input type="hidden" name="EditAccountAccessPermissions.CheckboxesAvailable" value="true">
      <input type="hidden" name="EditAccountAccessPermissions.SessionGroupName" value="Entitlement_EntitlementGroup">
      <input type="hidden" name="EditAccountAccessPermissions.GroupName" value='<ffi:getProperty name="Entitlement_EntitlementGroup" property="GroupName"/>'>
-->
<div align="center">
<div class="marginTop20 marginleft5" style="width:90%" align="left"><s:text name="jsp.user_260"/></div></div>
<div align="center" class="marginTop10"><ffi:getProperty name="Context"/></div>
		<div align="center">
			<table width="90%" border="0" cellspacing="0" cellpadding="3">
                 <tr>
                     <td colspan="6">
                     	<s:include value="inc/accountaccessverify_grid.jsp" />
                     </td>
                 </tr>
			</table>
<div class="btn-row" style="margin-bottom:25px !important;">
		<s:url id="inputUrl" value="%{#session.PagesPath}user/accountaccess.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="FromBack" value="%{'TRUE'}"></s:param>
			<s:param name="UseLastRequest" value="%{'TRUE'}"></s:param>
			<s:param name="PermissionsWizard" value="%{#session.PermissionsWizard}"></s:param>
		</s:url>
		<sj:a
			link="aa4"
			href="%{inputUrl}"
			targets="Accounts"
			button="true"
			onCompleteTopics="backButtonTopic"
			><s:text name="jsp.default_57"/></sj:a>
		
		<sj:a
			link="aa3"
			button="true" 
			onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
			><s:text name="jsp.default_102"/></sj:a>

		<ffi:cinclude value1="<%=action%>" value2="editAccountAccessPermissions-autoentitle" operator="equals">
			<ffi:setProperty name="perm_target" value="Accounts"/>
			<ffi:setProperty name="completedTopics" value="completedAccountAccessEdit"/>
				<sj:a
					link="aa2"
					formIds="permLimitFrmVerify"
					targets="Accounts"
					button="true"
					validate="false"
					onBeforeTopics="beforeVerify"
					onErrorTopics="errorSubmit"
					><s:text name="jsp.default_111"/></sj:a>
				<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
						<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg1')}" scope="request" />
						<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
						<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg2')}" scope="request" />
						<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
						<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg3')}" scope="request" />
						<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
				</ffi:cinclude>
				<s:if test="%{#session.Section == 'Users'}">
						<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg4')}" scope="request" />
						<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
				</s:if>
				<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
						<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg4')}" scope="request" />
						<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
				</ffi:cinclude>
				<ffi:setProperty name="autoEntitleBackURL" value="${PagesPath}user/accountaccess-verify.jsp"/>
				<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
					<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
					<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
				</ffi:cinclude>
				<ffi:setProperty name="autoEntitleFormAction" value="${SuccessURL}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="<%=action%>" value2="editAccountAccessPermissions-autoentitle" operator="notEquals">
			<sj:a
				link="aa1"
				formIds="permLimitFrmVerify"
				targets="resultmessage"
				button="true"
				validate="false"
				onBeforeTopics="beforeSubmit"
				onErrorTopics="errorSubmit"
				onSuccessTopics="completedAccountAccessEdit,reloadCompanyApprovalGrid"
				><s:text name="jsp.default_366"/></sj:a>
		</ffi:cinclude>
	</div>
</div>
</s:form>
</div>
