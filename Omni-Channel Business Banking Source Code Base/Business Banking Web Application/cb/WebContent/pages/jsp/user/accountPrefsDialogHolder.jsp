<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<s:url id="pageurl" value="/pages/jsp/user/AccountPrefSetting_init.action" escapeAmp="false">
</s:url>

<sj:dialog
	id="accountPrefsDialogId"
	autoOpen="false"
	modal="true"
	title="%{getText('jsp.default_Account_Preferences')}"
	height="550"
	width="1050"
	href="%{pageurl}"
	showEffect="fold"
	hideEffect="clip"
	resizable="false"  
	cssStyle="overflow:hidden;"
	>
</sj:dialog>