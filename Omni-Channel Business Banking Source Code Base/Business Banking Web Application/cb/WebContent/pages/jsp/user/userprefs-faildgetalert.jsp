<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="userprefs-confirm" className="moduleHelpClass"/>
<%
	session.setAttribute("NewsFrontpageSettings_MaxHeadlines", request.getParameter("NewsFrontpageSettings_MaxHeadlines"));
	session.setAttribute("NewsBusinessSettings_MaxHeadlines", request.getParameter("NewsBusinessSettings_MaxHeadlines"));
	session.setAttribute("NewsSportsSettings_MaxHeadlines", request.getParameter("NewsSportsSettings_MaxHeadlines"));
	session.setAttribute("DISPLAY_SESSION_SUMMARY_REPORT", request.getParameter("DISPLAY_SESSION_SUMMARY_REPORT"));
	session.setAttribute("new_display_session_summary_value", request.getParameter("new_display_session_summary_value"));
	session.setAttribute("BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES", request.getParameter("BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES"));
	session.setAttribute("BankLookupMaximumMatches", request.getParameter("BankLookupMaximumMatches"));
	session.setAttribute("TRANSACTION_SEARCH_MAXIMUM_MATCHES", request.getParameter("TRANSACTION_SEARCH_MAXIMUM_MATCHES"));
	session.setAttribute("TransactionSearchMaximumMatches", request.getParameter("TransactionSearchMaximumMatches"));
	session.setAttribute("Timeout", request.getParameter("Timeout"));
	session.setAttribute("NewTimeout", request.getParameter("NewTimeout"));
	session.setAttribute("DeliveryInfo2", request.getParameter("DeliveryInfo2"));
	session.setAttribute("DeliveryInfo1", request.getParameter("DeliveryInfo1"));
	session.setAttribute("DeliveryInfo1To", request.getParameter("DeliveryInfo1To"));
	session.setAttribute("DeliveryInfo2To", request.getParameter("DeliveryInfo2To"));
	session.setAttribute("SecureAlert", request.getParameter("SecureAlert"));
	session.setAttribute("UpdateAlertSettings", request.getParameter("UpdateAlertSettings"));
%>

<%-- Update the user's session delivery settings. --%>
<ffi:cinclude value1="${DISPLAY_SESSION_SUMMARY_REPORT}" value2="${new_display_session_summary_value}" operator="notEquals">
<ffi:setProperty name="DISPLAY_SESSION_SUMMARY_REPORT" value="${new_display_session_summary_value}"/>
<ffi:object id="SetSessionSettings" name="com.ffusion.tasks.user.SetSessionSettings" scope="session"/>
<ffi:process name="SetSessionSettings"/>
</ffi:cinclude>

<ffi:process name="FFISetNavigationSettings"/>
<ffi:process name="GetNavigationSettings"/>
<ffi:removeProperty name="FFISetNavigationSettings"/>



<%-- Update the banklookup settings. --%>
<ffi:cinclude value1="${BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES}" value2="${BankLookupMaximumMatches}" operator="notEquals">
<ffi:object id="SetBankLookupSettings" name="com.ffusion.tasks.user.SetBankLookupSettings" scope="session"/>
<ffi:setProperty name="SetBankLookupSettings" property="OldValue" value="${BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES}"/>
<ffi:setProperty name="BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES" value="${BankLookupMaximumMatches}"/>
<ffi:process name="SetBankLookupSettings"/>
</ffi:cinclude>


<%-- Update the Transaction Search settings. --%>
<ffi:cinclude value1="${TRANSACTION_SEARCH_MAXIMUM_MATCHES}" value2="${TransactionSearchMaximumMatches}" operator="notEquals">
<ffi:object id="SetTransactionSearchSettings" name="com.ffusion.tasks.user.SetTransactionSearchSettings" scope="session"/>
<ffi:setProperty name="SetTransactionSearchSettings" property="OldValue" value="${TRANSACTION_SEARCH_MAXIMUM_MATCHES}"/>
<ffi:setProperty name="SetTransactionSearchSettings" property ="NewValue" value="${TransactionSearchMaximumMatches}"/>
<ffi:process name="SetTransactionSearchSettings"/>
</ffi:cinclude>


<%-- Update the user's timeout settings. --%>
<ffi:object id="ModifyUser" name="com.ffusion.tasks.user.EditUser" scope="session"/>
	<ffi:setProperty name="ModifyUser" property="Init" value="true" />
<ffi:process name="ModifyUser"/>


<ffi:setProperty name="NewTimeout" value="${Timeout}"/>
<ffi:setProperty name="ModifyUser" property="Timeout" value="${NewTimeout}"/>
<ffi:setProperty name="ModifyUser" property="Process" value="true" />
<ffi:process name="ModifyUser"/>

<ffi:removeProperty name="ModifyUser"/>

<ffi:setProperty name="UpdateAlertUserPrefs" property="Process" value="true" />
<ffi:process name="UpdateAlertUserPrefs"/>
<ffi:removeProperty name="UpdateAlertUserPrefs"/> 



		<DIV id="noAlertsEngineConfirmID" align="center">
			<br>
			<div id="noAlertsEngineConfirmTextID" align="center">
				<table>
				<TD class="columndata" style="text-align: left"><s:text name="jsp.user.FaildGetAlertsEngine"/></TD>
				</table>
			</div>
			<br>			
		</DIV>

