<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<ffi:help id="userprefs_misc"/>



<s:form id="otherPrefs" name="otherPrefs" method="post" action="QuickEditPreferenceAction_updateOtherSettings.action" namespace="/pages/jsp/user" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" id="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

	<div id="PreferenceDesk" style="width:100%;">
	
	<!-- Misc area item holder -->
	<div id="sessionActivityPortID" class="sessionActivityItemCls" width="99%">
		<!-- Account Display Text-->
		<div class="miscTabItem">
			<span style="display:inline;float:left;"><s:text name="jsp.default.account.display.text.format"/></span>
			<div class="floatleft">
				<select id="accountDisplayTextFormat" name="accountDisplayTextFormat" class="txtbox" size="1">
					<s:if test="%{accountDisplayTextFormat!=null}">
						<option
							value="<ffi:getProperty name="accountDisplayTextFormat"/>"
							selected><ffi:getProperty name="formattedAccountDisplayText"/>
						</option>
					</s:if>
				</select>
			</div>
		</div>
		
		<!-- Grid Page Record Size-->
		<div class="miscTabItem">
			<span style="display:inline;float:left;"><s:text name="jsp.default.records.on.grid.page"/></span>
			<div class="floatleft">
			  <s:select id="recordsOnGridPage" style="width:100px;" name="recordsOnGridPage" cssClass="txtbox ui-corner-all" 
				list="%{#request.recordsOnGridPageList}" headerValue="Select size" headerKey="-1" value="#attr.recordsOnGridPage" >
			 </s:select>
		    </div>	 
		</div>
		
		<!-- Max matches to return -->
		<div class="settingItemHolder">
			<div class="miscTabItem">
				<span><s:text name="jsp.user_196"/></span>
				<input id="bankLookupID" name="bankLookupMatches" readonly="readonly" value="<s:property value="#request.bankLookupMatches"/>">
			</div>
		</div>

		<!-- System time out -->
		<div class="settingItemHolder">
			<div class="miscTabItem">
				<span><s:text name="jsp.user_301"/></span>
				<!-- if check added to satisfy the step size added while creating the box -->
				<input id="timeOutID" name="timeout1" readonly="readonly">
				<!-- this holds the acutal value that is passed and stored in db -->
				<input type="hidden" name="timeout" id="timeout">
			</div>
		</div>

		<s:if test="%{!autoRefreshPanelsInterval.equals('-1')}">
			<!-- Mail notifications -->
			<div class="settingItemHolder">
				<div class="miscTabItem">
					<span><s:text name="jsp.user.showMailNotification"/></span>
					<s:if test="%{showMailNotification == 'true'}">
						<input type="checkbox" name="showMail" id="showMail" checked="checked" onchange="updateNotificationFlag('showMail');" style="vertical-align: middle;">
					</s:if>
					<s:else>
						<input type="checkbox" name="showMail" id="showMail" onchange="updateNotificationFlag('showMail');" style="vertical-align: middle;">
					</s:else>
				</div>
			</div>
			
			
	
			<!-- Alert notifications -->
			<div class="settingItemHolder">
				<div class="miscTabItem">
					<span><s:text name="jsp.user.showAlertNotification"/></span>
					<s:if test="%{showAlertNotification == 'true'}">
						<input type="checkbox" name="showAlert" id="showAlert" checked="checked" onchange="updateNotificationFlag('showAlert');" style="vertical-align: middle;">
					</s:if>
					<s:else>
						<input type="checkbox" name="showAlert" id="showAlert" onchange="updateNotificationFlag('showAlert');" style="vertical-align: middle;">
					</s:else>
				</div>
			</div>
	
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
				<!-- Approval notifications -->
				<div class="settingItemHolder">
					<div class="miscTabItem">
						<span><s:text name="jsp.user.showApprovalNotification"/></span>
						<s:if test="%{showApprovalNotification == 'true'}">
							<input type="checkbox" name="showApproval" id="showApproval" checked="checked" onchange="updateNotificationFlag('showApproval');" style="vertical-align: middle;">
						</s:if>
						<s:else>
							<input type="checkbox" name="showApproval" id="showApproval" onchange="updateNotificationFlag('showApproval');" style="vertical-align: middle;">
						</s:else>
					</div>
				</div>
			</ffi:cinclude>
		</s:if>
	</div>
	
	<input type="hidden" name="showMailNotification" id="showMailNotification" value="<ffi:getProperty name="showMailNotification"/>">
	<input type="hidden" name="showAlertNotification" id="showAlertNotification" value="<ffi:getProperty name="showAlertNotification"/>">
	<input type="hidden" name="showApprovalNotification" id="showApprovalNotification" value="<ffi:getProperty name="showApprovalNotification"/>">
	
	<div id="otherPreferenceBtn" class="ui-widget-header customDialogFooter">
		<sj:a id="cancelOtherpreferenceButtonId" button="true" onclick="ns.common.closeDialog('otherSettingDialogId')">
			<s:text name="jsp.default_102"/>
		</sj:a>
		<sj:a id="saveOtherPrefButton" formIds="otherPrefs" button="true" validate="false" targets="resultmessage"
		 onCompleteTopics="quickSaveOtherPreferencesSetting">
			<s:text name="jsp.default_366"/>
		</sj:a>
	</div>
</s:form>
<script>
	var selectedThemeRef = "themeSAP";
	var selectedAccountDisplayTextFormat = '<ffi:getProperty name="accountDisplayTextFormat"/>';
	var formattedAccountDisplayText = '<ffi:getProperty name="formattedAccountDisplayText"/>';
	
	$(function()
	{
		$("#bankLookupID").spinner({
			min: 10,
			max: 50,
			step: 10,
			start: 10,
	    }).width(60);
		
		$("#timeOutID").spinner({
			min: 5,
			max: 60,
			step: 5,
			start: 5,
			change: function( event, ui ) {
				var timeInMinutes = $('#timeOutID').val();
				$('#timeout').val(timeInMinutes*60);
			}
	    }).width(60);
		
		var time = '<s:property value="#request.timeout"/>';
		var timeIMinutes = time/60;
		$( "#timeOutID" ).spinner( "value", timeIMinutes );
	});
	
	$(document).ready(function() {
		updateNotificationFlag = function(checkboxName){
			console.log(1);
			var isChecked = $("#"+ checkboxName).is(":checked");
			$("#"+checkboxName + "Notification").val(isChecked);
		};
		
		$("#accountDisplayTextFormat").lookupbox({
			"source":"/cb/pages/jsp/personalization/getAccountDisplayTextFormats.action",
			"controlType":"server",
			"collectionKey":"1",
			"size":"30",
			"module":"preferences"
		}); 
		
		
		$("#recordsOnGridPage").combobox({size:10});
		
	});
</script>

 <div id="operationresult">
<span id="resultmessage" style="display:none">
	<s:text name="jsp.user_390"/>
</span>
</div>