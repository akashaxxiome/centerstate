<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees"%>

<ffi:help id="user_clone-account-permissions-verify" className="moduleHelpClass"/>
<%
	request.setAttribute("ClonePermissions.SourceObjectId",request.getParameter("ClonePermissions.SourceObjectId"));
	request.setAttribute("ClonePermissions.TargetObjectId",request.getParameter("ClonePermissions.TargetObjectId"));
%>

<div align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<%-- <tr>
			<td colspan="2">
				<div align="center">
					<span class="sectionsubhead">
					<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminusers.jsp-1" parm0="${BusinessEmployeeForClone.FullName}"/>(<s:text name="jsp.default_576"/>)
					</span>
				</div>
			</td>
		</tr> --%>
		<tr>
			<td colspan="2">&nbsp;
			</td>
		</tr>
	</table>
	<s:form id="SourceAndTargetAccountSelectVerify" namespace="/pages/user" action="cloneAccountPermissions-execute" validate="false" theme="simple" method="post" name="SourceAndTargetAccountSelectVerify">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<table width="60%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td align="center" class="adminBackground">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td class="adminBackground" colspan="2" align="center">
							<span class="columndata">
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminusers.jsp-1" parm0="${BusinessEmployeeForClone.FullName}"/>
							</span>
						</td>
						<!-- <td class="adminBackground">&nbsp;</td> -->
					</tr>
					<tr>
						<td class="adminBackground" width="30%" align="center"><span class="columndata"><s:text name="jsp.user_67"/></span></td>
						<td class="adminBackground" width="30%" align="center"><span class="columndata"><s:text name="jsp.user_339"/></span></td>
					</tr>
					<tr>
						
						<td class="adminBackground" align="center" >
							<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" scope="request"/>	
							<ffi:setProperty name="SetAccount" property="AccountsName" value="${ClonePermissions.SourceObjectsSessionName}"/>
							<ffi:setProperty name="SetAccount" property="AccountName" value="account"/>
							<ffi:setProperty name="SetAccount" property="ID" value="${ClonePermissions.SourceObjectId}"/>
							<ffi:process name="SetAccount"/>
							<span class="columndata">
							<ffi:getProperty name="account" property="RoutingNum"/> : <ffi:getProperty name="account" property="DisplayText"/> - <ffi:getProperty name="account" property="CurrencyCode"/> - <ffi:getProperty name="account" property="NickName"/>
							</span>
						</td>
						
						<td align="center">
							<table border="0" cellspacing="0" cellpadding="0" align="center">
								<ffi:list collection="ClonePermissions.TargetObjectIds" items="accountId">
									<tr>
										<td>
											<ffi:setProperty name="SetAccount" property="AccountsName" value="${ClonePermissions.TargetObjectsSessionName}"/>
											<ffi:setProperty name="SetAccount" property="AccountName" value="account"/>
											<ffi:setProperty name="SetAccount" property="ID" value="${accountId}"/>
											<ffi:process name="SetAccount"/>
											<span class="columndata">
											<ffi:getProperty name="account" property="RoutingNum"/> : <ffi:getProperty name="account" property="DisplayText"/> - <ffi:getProperty name="account" property="CurrencyCode"/> - <ffi:getProperty name="account" property="NickName"/>
											</span>
										</td>
									</tr>
								</ffi:list>
							</table>
						</td>
						
					</tr>
					<tr>
						<td class="adminBackground" colspan="2">&nbsp;</td>
					</tr>
					<tr>
						<td class="adminBackground" align="center" colspan="2">
							<sj:a
								button="true"
								onClickTopics="backToInput"
								><s:text name="jsp.default_57"/></sj:a>
							<sj:a
								formIds="SourceAndTargetAccountSelectVerify"
								targets="confirmDiv"
								button="true" 
								validate="false"
								onBeforeTopics="beforeSubmit" 
								onErrorTopics="errorSubmit"
								onCompleteTopics="completeSubmit"
								><s:text name="jsp.user_66"/></sj:a>
						</td>
					</tr>
					<tr><td class="adminBackground" colspan="3">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</s:form>
</div>
