<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<s:include value="inc/corpadminusers-pre.jsp"/>

<%-- <script type="text/javascript">

	$(function(){
		$("#admin_usersPendingApprovalTab").tabs();
		$("#admin_usersTab").tabs();	
		$("#admin_usersTab").sortable({ items: 'li'});
		$("#admin_usersTab").sortable({ update: function(){
			ns.common.tabs.tabUpdate("#admin_usersTab");
		} });   
		ns.common.tabs.tabInit("#admin_usersTab");
	});
	
</script> --%>

<%-- The following div tempDivIDForMethodNsAdminReloadEmployees is used to hold temparory variables of user grids.--%>
<div id="tempDivIDForMethodNsAdminReloadEmployees" style="display: none;"></div>


<%-- Pending Approval Company island start --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
  <div id="admin_pendingUsersTab" class="portlet" style="display:none;">
	  <div class="portlet-header">
	  		<span class="portlet-title"><s:text name="jsp.default.label.pending.approval.changes"/></span>
			<%-- <div class="searchHeaderCls toggleClick expandArrow">
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<a id="adminUserPendingApprovalLink" href="javascript:void(0);" class="nameTitle"><s:text name="jsp.default.label.pending.approval.changes"/></a>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-negative floatRight"></span>
				</div>
			</div> --%>
	    </div>
	    
	    <div class="portlet-content">
	    	<!-- <div class="toggleBlock marginTop10"> -->
				<s:include value="/pages/jsp/user/inc/da-user-island-grid.jsp"/>
			<!-- </div> -->
		</div>
		
		<div id="adminUsersSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('admin_pendingUsersTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
		<!-- <div class="clearBoth portletClearnace hidden" style="padding: 10px 0"></div>	 -->
  </div>
	<div class="clearBoth" id="userBlankDiv" style="padding: 10px 0"></div>
</ffi:cinclude>
<%-- Pending Approval Company island end --%>
  

<div id="usersSummaryTabs" class="portlet" style="float: left; width: 100%;">
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="jsp.user_366"/></span>
	</div>
	<div class="portlet-content" id="usersSummaryTabsContent">
		<s:include value="/pages/jsp/user/corpadminusers.jsp" />
	</div>
	<div id="userSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('usersSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<script type="text/javascript">	
	//Initialize portlet with settings icon
	ns.common.initializePortlet("usersSummaryTabs");
	ns.common.initializePortlet("admin_pendingUsersTab");
	
</script>


<%-- Pending Approval Users changes island start --%>
<%-- <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<div id="admin_usersPendingApprovalTab" class="portlet" >
		<ul class="portlet-header">
	   	    <li>
		        <a id="adminUsersPendingApprovalLink" href="#adminUsersPendingApprovalChangeDiv"><s:text name="jsp.default.label.pending.approval.changes"/></a>
		    </li>
		</ul>
	
		<div id="adminUsersPendingApprovalChangeDiv" class="portlet-content">
			<s:include value="/pages/jsp/user/inc/da-user-island-grid.jsp"/>
		</div>	
	</div>
</ffi:cinclude>
Pending Approval Users changes island end
<br/>

	<ffi:object id="adminusertabs" name="com.ffusion.tasks.util.TabConfig"/>
	<ffi:setProperty name="adminusertabs" property="TransactionID" value="AdminUser"/>
	
	<ffi:setProperty name="adminusertabs" property="Tab" value="user"/>
	<ffi:setProperty name="adminusertabs" property="Href" value="/cb/pages/jsp/user/corpadminusers.jsp"/>
	<ffi:setProperty name="adminusertabs" property="LinkId" value="user"/>
	<ffi:setProperty name="adminusertabs" property="Title" value="jsp.user_366"/>
	
	<ffi:setProperty name="adminusertabs" property="Tab" value="profile"/>
	<ffi:setProperty name="adminusertabs" property="Href" value="/cb/pages/jsp/user/corpadminprofiles.jsp"/>
	<ffi:setProperty name="adminusertabs" property="LinkId" value="profile"/>
	<ffi:setProperty name="adminusertabs" property="Title" value="jsp.user_269"/>
	
	<ffi:process name="adminusertabs"/>

<div id="admin_usersTab" class="portlet">
	<ul class="portlet-header">
		<ffi:list collection="adminusertabs.Tabs" items="tab">
	        <li id='<ffi:getProperty name="tab" property="LinkId"/>Link'>
	            <a href='<ffi:getProperty name="tab" property="Href"/>' id='<ffi:getProperty name="tab" property="LinkId"/>'>
	            	<ffi:setProperty name="TabTitle" value="${tab.Title}"/><s:text name="%{#session.TabTitle}"/>
	            </a>
	        </li>
		</ffi:list>
		<a id="tabChangeNote" style="height:0px;  position:relative; top:3px; left:10px; "></a>
		<a id="tabRevertNote" style="height:0px;  position:relative; top:3px; left:20px; "></a>
	</ul>
  	<input type="hidden" id="getTransID" value="<ffi:getProperty name='adminusertabs' property='TransactionID'/>" />
</div>

<script type="text/javascript">
	$('#admin_usersTab').portlet({
		duration:300,
		helpCallback: function(){
			var helpFile = $('#admin_usersTab').find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
	});
	$('#admin_usersPendingApprovalTab').portlet({
		duration:300,
		helpCallback: function(){
			var helpFile = null;

			helpFile = $('#adminUsersPendingApprovalChangeDiv').find('.moduleHelpClass').html();
			
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
	});
</script> --%>
