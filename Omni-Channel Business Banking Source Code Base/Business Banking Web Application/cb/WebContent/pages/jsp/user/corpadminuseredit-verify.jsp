<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminuseredit-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_385')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:setProperty name="EditBusinessUser" property="InitAutoEntitle" value="True"/>
<ffi:setProperty name="EditBusinessUser" property="SourcePage" value="UserInfo"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<ffi:setProperty name="EditBusinessUser" property="OldBusinessName" value="BusinessEmployee"/>
	<ffi:process name="EditBusinessUser"/>
</ffi:cinclude>

<%-- set BusinessEmployee to be the current logged in user --%>
<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>
<ffi:removeProperty name="GetBusinessEmployee" />

<%
	Object be = session.getAttribute("BusinessEmployee");
	if( be != null && be instanceof com.ffusion.beans.ExtendABean ){
		Object oldBe = ((com.ffusion.beans.ExtendABean)be).clone();
		session.setAttribute( "OldBusinessEmployee", oldBe );
		((com.ffusion.beans.user.BusinessEmployee)be).setPassword(null);
	}
%>

<%-- populate BusinessEmployees with employees for the business --%>
<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>

<%-- set BusinessEmployee to be the user being edited --%>
<ffi:process name="SetBusinessEmployee"/>

<%-- for localizing state name --%>
<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${BusinessEmployee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${BusinessEmployee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>

		<div align="center" class="marginTop10">
			<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
						<div align="center">
							<span class="sectionsubhead"><br><ffi:getProperty name="BusinessName"/> <ffi:cinclude value1="DivisionName" value2="" operator="equals"> / <ffi:getProperty name="DivisionName"/></ffi:cinclude>&nbsp;<br>
								<br>
							</span></div>
					</td>
				</tr>
			</table> --%>
				<div class="leftPaneWrapper" id="userConfirmLeftPane">
					<div class="leftPaneInnerWrapper ffivisible">		
									<div class="header">
										<s:text name="user.login.summary"/>
									</div>
									<div style="padding: 15px 10px;" class="summaryBlock">
										<div style="width:100%" class="floatleft">
											<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_455"/>:</span>
											<span id="" class="inlineSection floatleft labelValue"><ffi:getProperty name="BusinessEmployee" property="UserName"/></span>
										</div>
										<div style="width:100%" class="marginTop10 clearBoth floatleft">
											<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_364"/>:</span>
											<span id="" class="inlineSection floatleft labelValue">
												<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.AccountStatus}" />
												<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE ) %>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.default_28"/></ffi:cinclude>
												<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.default_238"/></ffi:cinclude>
												<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE ) %>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_306"/></ffi:cinclude>
											</span>
										</div>
									</div>
					</div>
			    </div>	
				<div class="confirmPageDetails adminBackground">
						<s:form namespace="/pages/user" action="editBusinessUser_execute" theme="simple" name="EditBusinessUserForm" id="EditBusinessUserFormVerify" method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<div align="center">
								<div class="blockWrapper label130">
									<div class="blockHead"><s:text name="user.details" /></div>
									<div class="blockContent">
										<s:if test="%{#request.DoAutoEntitle}"> 
										<div class="blockRow">
											<div class="inlineBlock" style="width:100%;">
												<span class="sectionsubhead labelCls"><s:text name="jsp.user_128"/>:</span>
												<span class="sectionsubhead valueCls" style="width:20% !important;">
												<input type="radio" name="BusinessEmployee.AutoEntitle" value="true" checked="checked"/>&nbsp;<s:text name="jsp.default_467"/>
												<input type="radio" name="BusinessEmployee.AutoEntitle" value="false"/>&nbsp;<s:text name="jsp.default_295"/>
												</span>
											</div>
										</div>
										</s:if>
										<div class="blockRow">
											<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
											<div class="inlineBlock">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_161"/>:</span>
												<span class="sectionsubhead valueCls"><ffi:getProperty name="BusinessEmployee" property="FirstName"/></span>
											</div>
											</ffi:cinclude>
											<div class="inlineBlock">
												<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
														<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_185"/>:</span>
												</ffi:cinclude>
												<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
														<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_283"/>:</span>
												</ffi:cinclude>
												<span class="sectionsubhead valueCls"><ffi:getProperty name="BusinessEmployee" property="LastName"/></span>
											</div>
										</div>
										<div class="blockRow">
											<div class="inlineBlock">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_225"/>:</span>
												<span class="sectionsubhead valueCls">
													<ffi:list collection="GroupSummaries" items="group" >
														<ffi:list collection="group" items="spaces, groupName, groupId, numUsers" >
														  <s:if test="BusinessEmployee.UsingEntProfiles">
															<ffi:cinclude value1="${groupId}" value2="0" operator="notEquals" >
																<ffi:cinclude value1="${groupId}" value2="${BusinessEmployee.map.PARENT_ENT_GROUP_ID}">
																	<ffi:getProperty name="groupName" />
																</ffi:cinclude>														
															</ffi:cinclude>
														  </s:if>
														  <s:else>
														  	<ffi:cinclude value1="${groupId}" value2="0" operator="notEquals" >
																<ffi:cinclude value1="${groupId}" value2="${BusinessEmployee.entitlementGroupId}">
																	<ffi:getProperty name="groupName" />
																</ffi:cinclude>														
															</ffi:cinclude>
														  </s:else>
														</ffi:list>
													</ffi:list>
												</span>
											</div>
											<div class="inlineBlock">
												<span class="sectionsubhead sectionLabel">
		                                            <ffi:cinclude value1="${GroupAdmins.Size}" value2="0" operator="notEquals">
		                                               <s:text name="jsp.user_264"/>:
		                                            </ffi:cinclude>
		                                        </span>
												<span class="sectionsubhead valueCls">
		                                            <ffi:cinclude value1="${GroupAdmins.Size}" value2="0" operator="notEquals">
		                                                <ffi:list collection="GroupAdmins" items="employee">
		                                                    <ffi:cinclude value1="${employee.Id}" value2="${BusinessEmployee.PrimaryAdmin}">
		                                                        <ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
		                                                    </ffi:cinclude>
		                                                </ffi:list>
		                                            </ffi:cinclude>
		                                        </span>
										</div>
										<s:if test="%{#session.Business.usingEntProfiles == true || migration == true}">		
											<div class="blockRow">
												<!-- <div class="inlineBlock"> -->
												<span class="sectionsubhead sectionLabel">Profile Assignments:</span>
												<s:iterator value="BusinessEmployee.EntitlementProfiles"  var="profile">
												  <span class="sectionsubhead valueCls"><s:property value="#profile.profileName"/></span>
												    <div class="blockRow">
												      <span class="sectionsubhead sectionLabel">Supported Channels:</span>
												  	</div> 
												  <s:iterator var="channel" value="#profile.ChildProfiles">
													  <div class="blockRow">
														<span class="sectionsubhead valueCls"><s:property value="#channel.channelId"/></span>
													  </div>
												  	</s:iterator>
												</s:iterator>
												<!-- </div> -->
											</div> 
										</s:if>	
										<div class="blockRow">
											<div class="inlineBlock">
												<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_193"/>:</span>
												<span class="sectionsubhead valueCls">
												<ffi:cinclude value1="${TempLockoutStatus}" value2="" operator="equals">
														<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="equals">
															<s:text name="jsp.user_155"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="equals">
															<s:text name="jsp.user_192"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="equals">
															<s:text name="jsp.user_192"/>
														</ffi:cinclude>
														
														<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="notEquals">
															<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="notEquals">
																<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="notEquals">
																<s:text name="jsp.user_351"/>
																</ffi:cinclude>
															</ffi:cinclude>
														</ffi:cinclude>
													</ffi:cinclude>
													<ffi:cinclude value1="${TempLockoutStatus}" value2="" operator="notEquals">
														<ffi:getProperty name="TempLockoutStatus"/>
													</ffi:cinclude>
												</span>
												</ffi:cinclude>
											</div>
											<div class="inlineBlock">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_131"/>:</span>
												<span class="sectionsubhead valueCls"><ffi:getProperty name="BusinessEmployee" property="Email"/></span>
											</div>
										</div>
										
										<div class="blockRow">
											<div class="inlineBlock">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_250"/>:</span>
												<span class="sectionsubhead valueCls">
													<ffi:getProperty name="BusinessEmployee" property="Phone"/>
												</span>
											</div>
											<div class="inlineBlock">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_103"/>:</span>
												<span class="sectionsubhead valueCls">
													<ffi:getProperty name="BusinessEmployee" property="DataPhone"/>
												</span>
											</div>
										</div>
										
										<div class="blockRow">
											<div class="inlineBlock">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_156"/>:</span>
												<span class="sectionsubhead valueCls">
													<ffi:getProperty name="BusinessEmployee" property="FaxPhone"/>
												</span>
											</div>
											<div class="inlineBlock">
												<ffi:object id="GetLanguageList" name="com.ffusion.tasks.util.GetLanguageList" scope="session" />
												<ffi:process name="GetLanguageList" />
		
												<ffi:cinclude value1="${GetLanguageList.LanguagesList.size}" value2="1" operator="notEquals">
		
												<ffi:object id="LanguageResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
												<ffi:setProperty name="LanguageResource" property="ResourceFilename" value="com.ffusion.utilresources.languages" />
												<ffi:process name="LanguageResource" />
												<ffi:setProperty name="LanguageResource" property="ResourceID" value="${BusinessEmployee.PreferredLanguage}"/>
													<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_261"/>:</span>
													<span class="sectionsubhead valueCls"><ffi:getProperty name="LanguageResource" property="Resource"/></span>
												</ffi:cinclude>
		
												<ffi:removeProperty name="GetLanguageList" />
												<ffi:removeProperty name="LanguageResource" />
											</div>
										</div>
										
										<div class="blockRow">
											<div class="inlineBlock">
												<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_35"/>:</span>
												<span class="sectionsubhead valueCls">
		
													<ffi:getProperty name="BusinessEmployee" property="Street"/>&nbsp;
													<ffi:cinclude value1="${BusinessEmployee.Street2}" value2="" operator="notEquals">
														<ffi:getProperty name="BusinessEmployee" property="Street2"/>,
													</ffi:cinclude>
													<ffi:cinclude value1="${BusinessEmployee.City}" value2="" operator="notEquals">
														<ffi:getProperty name="BusinessEmployee" property="City"/>,
													</ffi:cinclude>
													<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
														<ffi:getProperty name="BusinessEmployee" property="State"/>
													</ffi:cinclude>
													<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
														<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
													</ffi:cinclude>
		
													<ffi:cinclude value1="${BusinessEmployee.State}" value2="" operator="notEquals">
													  <ffi:cinclude value1="${BusinessEmployee.ZipCode}" value2="" operator="notEquals">
														,
													  </ffi:cinclude>
													</ffi:cinclude>
		
													<ffi:getProperty name="BusinessEmployee" property="ZipCode"/>
													<ffi:cinclude value1="${BusinessEmployee.State}${BusinessEmployee.ZipCode}" value2="" operator="notEquals">
														
													</ffi:cinclude>
		
		
													<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
													<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
													<ffi:process name="CountryResource" />
		
							  						<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${BusinessEmployee.Country}" />
													<ffi:getProperty name='CountryResource' property='Resource'/>
												</span>
											</div>
										</div> 
										
										<!-- <div class="blockRow">
											<div class="inlineBlock">
												ss
											</div>
											<div class="inlineBlock">
												ddd
											</div>
										</div> -->
									</div>
								</div>		
				</div>				
				
									<%
						java.util.ArrayList adminGroups = new java.util.ArrayList();
					%>
					
					<!-- Retrieve a list of groups the new user can administer -->
					<ffi:object id="GetCanAdminGroups" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdminByEditAddUser" scope="session"/>
					<ffi:setProperty name="GetCanAdminGroups" property="EntGroupsName" value="CanAdminEntGroups"/>
					<!-- if the page is a reload of the last page, get data from the last page -->
					<ffi:setProperty name="GetCanAdminGroups" property="EntGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
					<ffi:process name="GetCanAdminGroups"/>
					<ffi:removeProperty name="GetCanAdminGroups"/>
					<div id="userAdminValue" class="required floatleft marginTop10">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.user_53"/>:</span>
							<!-- display name of each employee in BusinessEmployees -->
						<%
							boolean isFirst = true;
							String temp = "";
						%>
						<ffi:setProperty name="CanAdminEntGroups" property="SortedBy" value="<%= com.ffusion.csil.beans.entitlements.EntitlementGroup.NAME %>"/>
						<ffi:list collection="CanAdminEntGroups" items="GroupItem">
							<!-- display separator if this is not the first one -->
							<ffi:setProperty name="groupName" value="${GroupItem.GroupName}"/>
							<ffi:setProperty name="groupType" value="${GroupItem.EntGroupType}"/>
							<%
								if (isFirst) {
									isFirst = false;
									temp = temp + session.getAttribute("groupName") + " (" + session.getAttribute("groupType") + ")";
								} else {
									temp = temp + ", " + session.getAttribute("groupName") + " (" + session.getAttribute("groupType") + ")";
								}
							%>
						</ffi:list>
						<ffi:removeProperty name="CanAdminEntGroups"/>
						<%
							if (temp.length() > 100) {
								int index;
								String remainderStr = new String(temp);
								do {
									String headStr = remainderStr.substring(0, 100);
									index = headStr.lastIndexOf(',');
									headStr = remainderStr.substring(0, index);
									if (headStr.length() > 0) {
										adminGroups.add(headStr);
									}
									remainderStr = remainderStr.substring(index + 2);
								} while (remainderStr.length() > 100);
								if (remainderStr.length() > 0) {
									adminGroups.add(remainderStr);
								}
							} else {
								if (temp.length() > 0) {
									adminGroups.add(temp);
								}
							}
							//CanAdminGroupStr are used for displaying can admin groups on both the corpadminuseredit.jsp
							//and the corpadminueredit-verify.jsp
							if (adminGroups.size() > 0) {
								String adminStrLine = (String) adminGroups.get(0);
								session.setAttribute("adminStrLine", adminStrLine);
						%>
						<ffi:getProperty name="adminStrLine"/>
						<%
						} else {
						%>
						<s:text name="jsp.user_322"/>
						<%
							}
						%>
						<ffi:removeProperty name="groupName"/>
						<ffi:removeProperty name="groupType"/>
						<ffi:removeProperty name="adminStrList"/>
					<!-- </td>
				</tr> -->
				 <%
				if (adminGroups.size() > 1) {
					int trackingNum = 1;
					do {
				%>
				<!-- <tr>
				
					<td class="columndata required" valign="baseline"> -->
						<%
						String adminStrLine = (String) adminGroups.get(trackingNum++);
						session.setAttribute("adminStrLine", adminStrLine);
					%>
					<ffi:getProperty name="adminStrLine"/>
					
				<%
				} while (trackingNum < adminGroups.size());
				%>
				<ffi:removeProperty name="adminStrLine"/>
				<%
				}
				%> </div>
				<div class="btn-row">					
											<sj:a
												  button="true"
												  onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
											<sj:a
												  button="true"
												  summaryDivId="summary" 
												  buttonType="cancel"
												  onClickTopics="showSummary,cancelUserForm"><s:text name="jsp.default_82"/></sj:a>
											<sj:a
												formIds="EditBusinessUserFormVerify"
												targets="confirmDiv"
												button="true"
												validate="false"
												onBeforeTopics="beforeSubmit" onErrorTopics="errorSubmit" onSuccessTopics="successSubmitUserChanges"
												><s:text name="jsp.default_366"/></sj:a>
									
							</div>
						</s:form>
		</div>
</div>
<ffi:removeProperty name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>
<ffi:removeProperty name="GetStateProvinceDefnForState"/>
<ffi:removeProperty name="TempLockoutStatus"/>
