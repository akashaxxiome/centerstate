<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadminaccountedit-verify" className="moduleHelpClass"/>
<ffi:setProperty name="disableEdit" value="disabled"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<%
	String aid = (String)session.getAttribute("aid");
	String rnum = (String)session.getAttribute("rnum");
    String accountState = null;
    String accountCountry = null;
	request.setAttribute("RegAvailableFlag", request.getParameter("RegAvailableFlag"));
	request.setAttribute("ModifyAccount_REG_ENABLED", request.getParameter("ModifyAccount.REG_ENABLED"));
	request.setAttribute("ModifyAccount_NickName", request.getParameter("ModifyAccount.NickName"));
%>

<ffi:setProperty name="ModifyAccount" property="REG_ENABLED" value="${ModifyAccount_REG_ENABLED}" />
<ffi:setProperty name="ModifyAccount" property="NickName" value="${ModifyAccount_NickName}" />


<script type="text/javascript"><!--


function validateData( formName )
{

<ffi:cinclude value1="${RegAvailableFlag}" value2="true">
    if (document.frmEditAccountVerify.registerEnabled.checked) {
        document.frmEditAccountVerify["ModifyAccount.REG_ENABLED"].value = "true";
    } else {
        document.frmEditAccountVerify["ModifyAccount.REG_ENABLED"].value = "false";
    }
</ffi:cinclude>

}


//--></script>
  <%-- include page header, PageHeading and PageText should be set --%>
   <!--<form action="" method="post" name="frmEditAccountVerify">-->
<s:form id="frmEditAccountVerify" namespace="/pages/user" name="frmEditAccountVerify" action="modifyAccountById-execute" validate="false" method="post" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header">Account Information Summary</div>
		<div id="" class="paneContentWrapper summaryBlock label130">
			<div>
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_52"/></span>
				<span id="" class="inlineSection floatleft labelValue" style="width:auto">
					<ffi:getProperty name="ModifyAccount" property="BankName" />
				</span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_98"/></span>
				<span id="" class="inlineSection floatleft labelValue" style="width:auto">
					<ffi:getProperty name="ModifyAccount" property="CurrencyCode" />
				</span>
			</div>
			
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_10"/></span>
				<span id="" class="inlineSection floatleft labelValue" style="width:auto">
					<ffi:getProperty name="ModifyAccount" property="DisplayText" />
				</span>
			</div>
			
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_11"/></span>
				<span id="" class="inlineSection floatleft labelValue" style="width:auto">
					<ffi:getProperty name="ModifyAccount" property="Type" />
				</span>
			</div>
		</div>
	</div>
</div>

<div class="confirmPageDetails">
<div class="blockWrapper">
	<div  class="blockHead">Account Information</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
                <span class="sectionLabel" class='<ffi:getPendingStyle fieldname="nickName" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_204"/></span>
                <span>
                 <ffi:getProperty name="ModifyAccount" property="NickName" />
                	<span class="sectionhead_greyDA"><ffi:getProperty name="oldNickName" /></span>
                </span>
       			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
					<ffi:cinclude value1="${RegAvailableFlag}" value2="true">
                              <% String regEnabled = ""; %>
                              <ffi:getProperty name="ModifyAccount" property="REG_ENABLED" assignTo="regEnabled" />
                              <% if( regEnabled == null ) { regEnabled = ""; } %>
                              <span  class="sectionhead" <ffi:getProperty name="disableEdit"/>><input type="checkbox" name="registerEnabled" value="checkboxValue" border="0" <ffi:getProperty name="disableEdit"/> <%= regEnabled.equals("true") ? "checked" : "" %>><s:text name="jsp.user_275"/></span>
					</ffi:cinclude>
					<ffi:cinclude value1="${RegAvailableFlag}" value2="true" operator="notEquals">
					</ffi:cinclude>
				</ffi:cinclude>
         		<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" ></ffi:cinclude>
			</div>
			<div class="inlineBlock">
				<ffi:cinclude value1="${ModifyAccount.ContactId}" value2="-1" operator="notEquals">
					<span class="sectionLabel" class='<ffi:getPendingStyle fieldname="street" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_31"/></span>
                     <span>
                         <ffi:getProperty name="ModifyAccount" property="Contact.Street" />
                         <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Street" /></span>
                     </span>
				</ffi:cinclude>
			</div>
		</div>
		<ffi:cinclude value1="${ModifyAccount.ContactId}" value2="-1" operator="notEquals">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
               <span class="sectionLabel" class='<ffi:getPendingStyle fieldname="street2" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_30"/></span>
               <span>
                   <ffi:getProperty name="ModifyAccount" property="Contact.Street2" />
                   <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Street2" /></span>
               </span>
			</div>
			<div class="inlineBlock">
               <span class="sectionLabel" class='<ffi:getPendingStyle fieldname="city" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_65"/></span>
               <span>
                 <ffi:getProperty name="ModifyAccount" property="Contact.City" />
                   <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="city" /></span>
               </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<!-- selected country and state -->
                  <ffi:getProperty name="ModifyAccount" property="Contact.Country" assignTo="accountCountry"/>
                  <% if (accountCountry == null) { accountCountry = ""; } %>
                  <ffi:getProperty name="ModifyAccount" property="Contact.State" assignTo="accountState"/>
                  <% if (accountState == null) { accountState = ""; } %>

                  <ffi:object id="GetStatesForCountry" name="com.ffusion.tasks.util.GetStatesForCountry" scope="session" />
                      <ffi:setProperty name="GetStatesForCountry" property="CountryCode" value="${AccountContact.Country}" />
                  <ffi:process name="GetStatesForCountry" />
                  <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
				 <span class="sectionLabel" class='<ffi:getPendingStyle fieldname="state" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_299"/></span>
			     <span>
                     <%--  <select class="ui-widget-content ui-corner-all" name="Contact.State" disabled="disabled" size="1" <ffi:getProperty name="disableEdit"/>>
                              <option value="" <ffi:cinclude value1="<%= accountState %>" value2="" operator="equals">selected</ffi:cinclude>><s:text name="jsp.default_376"/></option> --%>
                          <ffi:list collection="GetStatesForCountry.StateList" items="item">
                               <ffi:cinclude value1="<%= accountState %>" value2="${item.StateKey}" operator="equals"><ffi:getProperty name='item' property='Name'/></ffi:cinclude>
                          </ffi:list>
                      <%-- </select> --%>
                      <span class="sectionhead_greyDA"><ffi:getProperty name="oldState" /></span>
                  </span>
                </ffi:cinclude>
                <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
                    <%-- validate zip code only if country is defined --%>
                    <ffi:object id="IsRegisteredCountry" name="com.ffusion.tasks.util.IsRegisteredCountry" scope="session" />
                        <ffi:setProperty name="IsRegisteredCountry" property="CountryCode" value="<%= accountCountry %>" />
                    <ffi:process name="IsRegisteredCountry" />
                    <ffi:removeProperty name="IsRegisteredCountry"/>
                </ffi:cinclude>
			</div>
			<div class="inlineBlock">
                 <span class="sectionLabel" class='<ffi:getPendingStyle fieldname="zipCode" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_391"/></span>
                 <span>
                     <ffi:getProperty name="ModifyAccount" property="Contact.ZipCode" />
                     <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="zipCode" /></span>
                 </span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
                  <span class="sectionLabel" class='<ffi:getPendingStyle fieldname="country" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_97"/></span>
                  <span>
                 <%--  <select class="ui-widget-content ui-corner-all" name="Contact.Country" disabled="disabled" size="1" <ffi:getProperty name="disableEdit"/>>
                      <option value="" <ffi:cinclude value1="<%= accountCountry %>" value2="" operator="equals">selected</ffi:cinclude>><s:text name="jsp.user_283"/></option> --%>
                      <ffi:list collection="Country_List" items="item">
                          <ffi:cinclude value1="<%= accountCountry %>" value2="${item.CountryCode}" operator="equals"><ffi:getProperty name='item' property='Name'/></ffi:cinclude>
                      </ffi:list>
                  <%-- </select> --%>
                  </span><span class="sectionhead_greyDA"><ffi:getProperty name="oldCountry" /></span>
			</div>
			<div class="inlineBlock">
              <span class="sectionLabel" class='<ffi:getPendingStyle fieldname="phone" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_252"/></span>
              <span>
                  <ffi:getProperty name="ModifyAccount" property="Contact.Phone" />
                  <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Phone" /></span>
              </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
                <span class="sectionLabel" class='<ffi:getPendingStyle fieldname="faxPhone" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_157"/></span>
                <span>
                    <ffi:getProperty name="ModifyAccount" property="Contact.FaxPhone" />
                    <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="faxPhone" /></span>
                </span>
			</div>
			<div class="inlineBlock">
                <span class="sectionLabel" class='<ffi:getPendingStyle fieldname="email" dacss="sectionheadDA" defaultcss="sectionhead" />' ><s:text name="jsp.user_148"/></span>
                <span>
                    <ffi:getProperty name="ModifyAccount" property="Contact.Email" />
                    <span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="email" /></span>
                </span>
			</div>
		</div>
	 </ffi:cinclude>
	</div>
</div>
<div class="btn-row">
<sj:a
	button="true"
	onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
<sj:a 
button="true"
onClickTopics="cancelCompanyForm,cancelAccConfEditForm"
><s:text name="jsp.default_82"/></sj:a>
<sj:a
	id="submitAccountConfigVerify"
	formIds="frmEditAccountVerify"
	targets="confirmDiv"
	button="true"
	validate="false"
	onBeforeTopics="beforeSubmit" 
	onSuccessTopics="accountEditSuccessTopic"
	onCompleteTopics="completeSubmit" 
	onErrorTopics="errorSubmit"
	><s:text name="jsp.default_366"/></sj:a>
</div>
               
</div>    
   </s:form>
<ffi:removeProperty name="GetStatesForCountry"/>
<%
session.setAttribute("aid", null);
session.setAttribute("rnum", null);
%>
