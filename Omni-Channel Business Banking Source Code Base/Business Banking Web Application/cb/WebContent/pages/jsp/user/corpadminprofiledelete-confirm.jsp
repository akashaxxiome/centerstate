<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminprofiledelete-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_80')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
		<div align="center">

		<TABLE class="adminBackground" width="480" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminprofiledelete-confirm.jsp-1" parm0="${BusinessEmployee.UserName}"/></TD>
						</TR>
						<TR>
							<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15"></TD>
						</TR>
						<TR>
							<TD align="center">
								<sj:a id="closeDeleteGroupLinkConfirm" button="true"
									  onClickTopics="closeDeleteProfileDialog" title="%{getText('Close_Window')}"
									  buttonIcon="ui-icon-check"
									  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_175" /></sj:a>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
			<p></p>
		</div>

<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadminprofiles-pre.jsp"/>
