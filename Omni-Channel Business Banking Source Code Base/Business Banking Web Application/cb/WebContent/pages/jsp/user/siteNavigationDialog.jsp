<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script>
	$.subscribe('quickSaveSiteNavigationSetting', function(event,data) {
		ns.common.showStatus($("#resultmessage").html().trim());
		ns.common.closeDialog("siteNavigationDialogId");
		setTimeout(function(){
			window.location.hash = ns.home.currentMenuId;
			window.location.reload()
			},1000);
	});
</script>

<div style="width:100%; margin:0 auto; text-align:center;">
	 <s:action namespace="/pages/jsp/user" name="QuickEditPreferenceAction_initializeMenuPreferences" executeResult="true">
	 </s:action>
	 
	 

</div>