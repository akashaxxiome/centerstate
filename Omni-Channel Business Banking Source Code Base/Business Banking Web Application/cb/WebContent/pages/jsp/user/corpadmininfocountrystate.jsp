<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<script language="javascript">
$(function(){
	$("#selectUserStateID").combobox({'size':'31',searchFromFirst:true});
});
</script>

<%
    String userState = null;
%>

<!-- default selected country and state -->
<ffi:getProperty name="EditBusiness" property="State" assignTo="userState"/>
<% if (userState == null) { userState = ""; } %>

<ffi:object id="GetStatesForCountry" name="com.ffusion.tasks.util.GetStatesForCountry" scope="session" />
    <ffi:setProperty name="GetStatesForCountry" property="CountryCode" value='<%=request.getParameter("countryCode") %>' />
<ffi:process name="GetStatesForCountry" />

<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
	<!--  if there's state by default country, need to validate state, here set checkCompanyProfileState to be TRUE -->
	<ffi:setProperty name="checkCompanyProfileState" value="TRUE"/>
	<select class="txtbox" tabindex="6" id="selectUserStateID" name="EditBusiness.State" value="<ffi:getProperty name="EditBusiness" property="State"/>">
		<option<ffi:cinclude value1="<%= userState %>" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.default_376"/></option>
		<ffi:list collection="GetStatesForCountry.StateList" items="item">
			<option <ffi:cinclude value1="<%= userState %>" value2="${item.StateKey}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="StateKey"/>"><ffi:getProperty name='item' property='Name'/></option>
		</ffi:list>
	</select><span id="EditBusiness.StateError"></span>
</ffi:cinclude>
<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
	<!--  if there's no state by default country, need to validate state, here set checkCompanyProfileState to be FALSE -->
	<ffi:setProperty name="checkCompanyProfileState" value="FALSE"/>
	<ffi:setProperty name="EditBusiness" property="State" value=""/>
</ffi:cinclude>