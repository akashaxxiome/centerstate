<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<ffi:setL10NProperty name='PageHeading' value='Confirm Approve Group'/>

<%		
	if( request.getParameter("itemId") != null ) { 
		String itemId = request.getParameter("itemId");
		session.setAttribute("itemId", itemId); 
	}
	
	if( request.getParameter("groupName") != null ) { 
		String GroupName = request.getParameter("groupName");
		session.setAttribute("GroupName", GroupName); 
	}
%>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.businessID}"/>
<ffi:process name="GetDAItem" />

<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
	<ffi:setProperty name="EntitlementGroupParentId" value="${Business.EntitlementGroup.ParentId}" />
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE %>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
	
		<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
		<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${itemId}"/>
		<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>'/>
	    <ffi:process name="GetEntitlementGroup"/>
	
		<ffi:object name="com.ffusion.tasks.admin.EditBusinessGroup" id="EditBusinessGroup" />
		<ffi:setProperty name="EditBusinessGroup" property="SuccessURL" value=""/>
		<ffi:setProperty name="EditBusinessGroup" property="CheckboxesAvailable" value="false"/>
		<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
			<ffi:cinclude value1="${details.fieldName}" value2="parentId">
				<ffi:setProperty name="EditBusinessGroup" property="ParentGroupId" value="${details.newValue}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="groupName">
				<ffi:setProperty name="EditBusinessGroup" property="GroupName" value="${details.newValue}"/>
			</ffi:cinclude>
		</ffi:list>
		<ffi:cinclude value1="${EditBusinessGroup.ParentGroupId}" value2="0" operator="equals">
			<ffi:setProperty name="EditBusinessGroup" property="ParentGroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${EditBusinessGroup.GroupName}" value2="" operator="equals">
			<ffi:setProperty name="EditBusinessGroup" property="GroupName" value="${GroupName}"/>
		</ffi:cinclude>
		<ffi:setProperty name="EditBusinessGroup" property="AdminListName" value="AdminList"/>
		<ffi:setProperty name="EditBusinessGroup" property="SessionGroupName" value="Entitlement_EntitlementGroup"/>
		<ffi:setProperty name="EditBusinessGroup" property="InitAutoEntitle" value="True"/>
		<ffi:process name="EditBusinessGroup"/>
		<ffi:process name="EditBusinessGroup"/>
		
		<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${itemId}" />
		<ffi:setProperty name="ApprovePendingChangesTask" property="category" value="<%=IDualApprovalConstants.CATEGORY_BEAN %>" />
		<ffi:process name="ApprovePendingChangesTask"/>
		<ffi:removeProperty name="Entitlement_EntitlementGroup"/>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${isPermissionChanged}" value2="Y">
		<s:include value="inc/da-user-approve-permission.jsp" />
		<ffi:removeProperty name="approveGroupPermission" />
	</ffi:cinclude>

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR %>"/>
	<ffi:process name="GetDACategoryDetails"/>

	<%-- Check if administrators are modified. If yes, then approve the administrator changes and save it into system tables. --%>
	<%-- As the administrator who is approving the change may be getting removed, do adminstrators very last --%>
	<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
		<ffi:object name="com.ffusion.tasks.dualapproval.CreateAdministratorInputs" id="CreateAdministratorInputs"/>
		<ffi:setProperty name="CreateAdministratorInputs" property="adminEmployees" value="AdminEmployees"/>
		<ffi:setProperty name="CreateAdministratorInputs" property="adminGroups" value="AdminGroups"/>
		<ffi:setProperty name="CreateAdministratorInputs" property="nonAdminGroups" value="NonAdminGroups"/>
		<ffi:setProperty name="CreateAdministratorInputs" property="nonAdminEmployees" value="NonAdminEmployees"/>
		<ffi:setProperty name="CreateAdministratorInputs" property="memberType" value="<%= EntitlementsDefines.ENT_MEMBER_TYPE_USER %>"/>
		<ffi:setProperty name="CreateAdministratorInputs" property="memberSubType" value="<%= Integer.toString(SecureUser.TYPE_CUSTOMER) %>"/>
		<ffi:process name="CreateAdministratorInputs"/>
		<%-- call SetAdministrators to do all the validation, with commit = false --%>
		<ffi:object id="SetAdministrators" name="com.ffusion.tasks.user.SetAdministrators"/>
		<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembers"/>
		<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembers"/>
		<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
		<ffi:setProperty name="SetAdministrators" property="Commit" value="false"/>
		<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${itemId}"/>
		<ffi:setProperty name="SetAdministrators" property="EntitlementGroupType" value="<%=EntitlementsDefines.ENT_GROUP_GROUP%>"/>
		<ffi:setProperty name="SetAdministrators" property="HistoryId" value="${Business.Id}"/>
		<ffi:process name="SetAdministrators"/>

		<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChanges" />
		<ffi:setProperty name="ApprovePendingChanges" property="businessId" value="${SecureUser.businessID}" />
		<ffi:setProperty name="ApprovePendingChanges" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>" />
		<ffi:setProperty name="ApprovePendingChanges" property="itemId" value="${itemId}" />
		<ffi:setProperty name="ApprovePendingChanges" property="category" value="<%=IDualApprovalConstants.CATEGORY_BEAN %>" />
		<ffi:process name="ApprovePendingChanges"/>

		<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChanges" />
			<ffi:setProperty name="ApprovePendingChanges" property="businessId" value="${SecureUser.businessID}" />
			<ffi:setProperty name="ApprovePendingChanges" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>" />
			<ffi:setProperty name="ApprovePendingChanges" property="itemId" value="${itemId}" />
			<ffi:setProperty name="ApprovePendingChanges" property="approveDAItem" value="TRUE" />
		<ffi:process name="ApprovePendingChanges"/>
		<%-- now commit the administrator changes after everything else --%>
		<ffi:setProperty name="SetAdministrators" property="Commit" value="true"/>
		<ffi:process name="SetAdministrators"/>

		<ffi:removeProperty name="AdminEmployees"/>
		<ffi:removeProperty name="AdminGroups"/>
		<ffi:removeProperty name="NonAdminGroups"/>
		<ffi:removeProperty name="NonAdminEmployees"/>
		<ffi:removeProperty name="userMembers"/>
		<ffi:removeProperty name="adminMembers"/>
		<ffi:removeProperty name="groupIds"/>
		<ffi:removeProperty name="CreateAdministratorInputs"/>
		<ffi:removeProperty name="SetAdministrators"/>
	</ffi:cinclude>
	<%-- if there were administrator changes, the ApprovePendingChange for the group was called in the block above --%>
	<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
		<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChanges" />
			<ffi:setProperty name="ApprovePendingChanges" property="businessId" value="${SecureUser.businessID}" />
			<ffi:setProperty name="ApprovePendingChanges" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>" />
			<ffi:setProperty name="ApprovePendingChanges" property="itemId" value="${itemId}" />
			<ffi:setProperty name="ApprovePendingChanges" property="approveDAItem" value="TRUE" />
		<ffi:process name="ApprovePendingChanges"/>
	</ffi:cinclude>
	
	<ffi:setProperty name="tempDisplayName" value="${EditBusinessGroup.GroupName}"/> 
</ffi:cinclude>

<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-group-modify-confirm.jsp-1" parm0="${tempDisplayName}"/>

<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="AdminList"/>
<ffi:removeProperty name="ApprovePendingChanges"/>
<ffi:removeProperty name="Entitlement_EntitlementGroup"/>
<ffi:removeProperty name="DAItem"/>