<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>

<ffi:cinclude value1="${runSearch}" value2="">

	<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
	<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="BusEmpAccounts"/>
	<ffi:setProperty name="SearchResultsPage" value="/pages/jsp/user/corpadminaccountsedit.jsp"/>
	<%-- Only default to search only page if there are more than the specified number of accounts (Value2).	--%>
	<ffi:setProperty name="Math" property="Value1" value="${BusEmpAccounts.Size}"/>

	<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount"/>
	<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%= com.ffusion.tasks.GetDisplayCount.ACCOUNT %>"/>
	<ffi:setProperty name="GetDisplayCount" property="DisplayCountName" value="accountDisplaySizeStr"/>
	<ffi:process name="GetDisplayCount"/>
	<ffi:removeProperty name="GetDisplayCount"/>

	<ffi:setProperty name="Math" property="Value2" value="${accountDisplaySizeStr}"/>
	<ffi:removeProperty name="accountDisplaySizeStr"/>


	<ffi:cinclude value1="${Math.LessEquals}" value2="true">
		<ffi:setProperty name="runSearch" value="true"/>
		<ffi:process name="SearchAcctsByNameNumType" />
	</ffi:cinclude>
</ffi:cinclude>
<% session.setAttribute("FFISearchAcctsByNameNumType", session.getAttribute("SearchAcctsByNameNumType") ); %>

<%
	String accountDisplaySizeStr;
	String numAccountsStr;
%>
<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount"/>
<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%= com.ffusion.tasks.GetDisplayCount.ACCOUNT %>"/>
<ffi:setProperty name="GetDisplayCount" property="DisplayCountName" value="accountDisplaySizeStr"/>
<ffi:process name="GetDisplayCount"/>
<ffi:removeProperty name="GetDisplayCount"/>

<ffi:getProperty name="accountDisplaySizeStr" assignTo="accountDisplaySizeStr"/>
<ffi:getProperty name="${SearchAcctsByNameNumType.AccountsName}" property="size" assignTo="numAccountsStr"/>
<%
	int accountDisplaySize = Integer.parseInt( accountDisplaySizeStr );
	int numAccounts = Integer.parseInt( numAccountsStr );
	
	if( accountDisplaySize < numAccounts ) {
		pageContext.setAttribute( "showSearch", "TRUE" );
	} else {
		pageContext.setAttribute( "showSearch", "FALSE" );
	}
	
	pageContext.setAttribute( "accountDisplaySizeStr", String.valueOf( accountDisplaySize ) );
	pageContext.setAttribute( "numAccountsStr", String.valueOf( numAccounts ) );
%>

<s:set var="searchActionName" value="%{'accountAccess'}"></s:set>
<s:if test="#session.viewOnly == 'true'">
<s:set var="searchActionName" value="%{'searchAcctsByNameNumType-executeViewOnly'}"></s:set>
</s:if>

<script type="text/javascript">
<!--
	$(function(){
		$("#accountConfigAccountType").selectmenu({width: 140});
		
		var value = {value:"All Accounts", label: "All Accounts"};
        $("#accountConfigAccountID").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"module":"accountConfiguration",
			"size":"50",
			"source":"/cb/pages/common/AccountConfigAccountsLookupBoxAction.action",
			"defaultOption":value
		});
		accountSelect();
		$("#accountConfigAccountID").change(accountSelect);
		if($("#accountConfigAccountID").val()=='' && $.trim($('#companyAccountConfigGrid').html())==''){
			setTimeout(function() {
			      $('#ACHAccountsSearchButton').click();
			}, 1000);
		}
		
    });
	
	function accountSelect() {
		//$('#currencyCode').html($('#accountConfigAccountID :selected').attr("currencycode"));
		var selectedVal = $('#accountConfigAccountID :selected').text();
		if(selectedVal!=null && selectedVal!="" && selectedVal!= "undefined" && selectedVal!="All Accounts"){
			var currency = selectedVal.split("-")[3];
			$('#currencyCode').html(currency);
		}
		getAccounts();
	}
	
	function getAccounts(){
		var accountVal = $.trim($("#accountConfigAccountID option:selected").val());
		if("All Accounts"==accountVal){
			$("#accountConfigAccountID option:selected").val('');
		}
		$('#ACHAccountsSearchButton').click();
	}
	
-->
</script>

<ffi:object id="SetAccountsCollectionDisplayText"  name="com.ffusion.tasks.util.SetAccountsCollectionDisplayText" />
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CollectionName" value="<%= SessionNames.BUSINESS_EMP_ACCOUNTS %>"/>
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
<ffi:process name="SetAccountsCollectionDisplayText"/>
<ffi:removeProperty name="SetAccountsCollectionDisplayText"/>

<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.BUSINESS_EMP_ACCOUNTS %>"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.BUSINESS_EMP_ACCOUNTS_INDEX %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="AccountDisplayText"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>
									
										
<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.BUSINESS_EMP_ACCOUNTS %>"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.BUSINESS_EMP_ACCOUNTS_INDEX_BY_TYPE %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="Type"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>
										
<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.BUSINESS_EMP_ACCOUNTS %>"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.BUSINESS_EMP_ACCOUNTS_INDEX_BY_CURRENCY %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="CurrencyCode"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>

<ffi:cinclude value1="${showSearch}" value2="TRUE" operator="equals">
	<!-- <form action="<ffi:getProperty name='SecureServletPath'/>SearchAcctsByNameNumType" method="post" name="formSearchACHAccounts">-->
	<s:form id="formSearchACHAccounts" namespace="/pages/user" name="formSearchACHAccounts" action="%{searchActionName}" validate="false" method="post" theme="simple">
    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<input type="hidden" name="runSearch" value="true">
	<table width="100%" class="tableData">
		<tr>
			<td width="10%">Filter <s:text name="jsp.default_16"/></td>
			<td width="500px">
				<div class="selectBoxHolder">
				<!-- <option selected>All Accounts</option> -->
					<select id="accountConfigAccountID" class="txtbox" name="SearchAcctsByNameNumType.Id" size="1">
						<option selected="selected" value="All Accounts">All Accounts</option>
				                 <ffi:cinclude value1="${SearchAcctsByNameNumType.Id}" value2="" operator="notEquals">
				                 	<ffi:cinclude value1="${SearchAcctsByNameNumType.Id}" value2="-1" operator="notEquals">
								<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
								<ffi:setProperty name="SetAccount" property="AccountsName" value="BusEmpAccounts"/>
								<ffi:setProperty name="SetAccount" property="ID" value="${SearchAcctsByNameNumType.Id}"/>
								<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount"/>
								<ffi:process name="SetAccount"/>
								
				
								<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
								<s:set var="AccountDisplayTextTaskBean" value="#session.SelectedAccount" scope="request" />
								<ffi:setProperty name="selectedId" value="${SelectedAccount.ID}"  />
								<ffi:setProperty name="AccountDisplayTextTask" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
								<ffi:process name="AccountDisplayTextTask"/>
								<ffi:setProperty name="permissionsAccDisText" value="${AccountDisplayTextTask.AccountDisplayText}" />
								<option value="<ffi:getProperty name='selectedId'/>" selected ><ffi:getProperty name="permissionsAccDisText"/></option>
							</ffi:cinclude>
						</ffi:cinclude>
            		 </select>
         		</div>
			</td>
			<td>
				<sj:a
									id="ACHAccountsSearchButton"
									formIds="formSearchACHAccounts"
									targets="accountConfigDiv"
									button="true"
									cssStyle="display:none"
									buttonIcon="ui-icon-search"
									validate="true"
									validateFunction="customValidation"
									><s:text name="jsp.default_373"/></sj:a>
			</td>
		</tr>
	</table>
		<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" class="adminBackground">
					<table width="100%" border="0">
						<tr>
							<td class="sectionsubhead" colspan="2" height="20">&nbsp;&nbsp;&gt; <s:text name="jsp.user_13"/></td>
						</tr>
						<tr>
							<td class="sectionsubhead" height="20" align="right"><s:text name="jsp.default_294"/></td>
							<td class="sectionsubhead">
								<!-- <input type="text" name="SearchAcctsByNameNumType.NickName" size="17" maxlength="40" class="txtbox"> -->
								<s:textfield name="SearchAcctsByNameNumType.NickName" size="17" maxlength="40" value="%{#session.SearchAcctsByNameNumType.nickName}" cssClass="ui-widget-content ui-corner-all"/>
							</td>
						</tr>
						<tr>
							<td class="sectionsubhead" align="right"><s:text name="jsp.default_16"/></td>
							
							<td class="sectionsubhead">
								<div class="selectBoxHolder">
									<select id="accountConfigAccountID" class="txtbox" name="SearchAcctsByNameNumType.Id" size="1">
										<!-- <option value=""></option> -->
					                    <ffi:cinclude value1="${SearchAcctsByNameNumType.Id}" value2="" operator="notEquals">
					                    	<ffi:cinclude value1="${SearchAcctsByNameNumType.Id}" value2="-1" operator="notEquals">
												<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
												<ffi:setProperty name="SetAccount" property="AccountsName" value="BusEmpAccounts"/>
												<ffi:setProperty name="SetAccount" property="ID" value="${SearchAcctsByNameNumType.Id}"/>
												<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount"/>
												<ffi:process name="SetAccount"/>
												
							
												<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
												<s:set var="AccountDisplayTextTaskBean" value="#session.SelectedAccount" scope="request" />
												<ffi:setProperty name="selectedId" value="${SelectedAccount.ID}"  />
												<ffi:setProperty name="AccountDisplayTextTask" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
												<ffi:process name="AccountDisplayTextTask"/>
												<ffi:setProperty name="permissionsAccDisText" value="${AccountDisplayTextTask.AccountDisplayText}" />
												<option value="<ffi:getProperty name='selectedId'/>" selected ><ffi:getProperty name="permissionsAccDisText"/></option>
											</ffi:cinclude>
										</ffi:cinclude>
					                </select>
					            </div>
							</td>
						</tr>
						<tr>
							<td class="sectionsubhead" align="right"><s:text name="jsp.default_20"/></td>
							<td class="sectionsubhead">
								<select name="SearchAcctsByNameNumType.Type" class="txtbox" id="accountConfigAccountType">
									<option value="" selected><s:text name="jsp.user_563"/></option>
										<ffi:list collection="AccountTypesList" items="AccountType">
											<option value="<ffi:getProperty name="AccountType"/>" <ffi:cinclude value1="${AccountType}" value2="${SearchAcctsByNameNumType.Type}">selected</ffi:cinclude>><ffi:getProperty name="AccountType"/></option>
										</ffi:list>
								</select>
							</td>
						</tr>
						<tr><td colspan="2" ><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td></tr>
						<tr>
							<td colspan="2" align="center">
								<sj:a
									id="ACHAccountsSearchButton"
									formIds="formSearchACHAccounts"
									targets="accountConfigDiv"
									button="true"
									buttonIcon="ui-icon-search"
									validate="true"
									validateFunction="customValidation"
									><s:text name="jsp.default_373"/></sj:a>
							</td>
						</tr>
						<tr><td colspan="2" ><img src="/cb/web/multilang/grafx/user/spacer.gif" border="0" height="1"></td></tr>
					</table>
				</td>
			</tr>
		</table> --%>
	</s:form>
</ffi:cinclude>					
