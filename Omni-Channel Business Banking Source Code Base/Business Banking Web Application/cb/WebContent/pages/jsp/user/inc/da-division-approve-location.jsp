<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION %>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="<%=IDualApprovalConstants.TRUE%>"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:list collection="MultipleCategories" items="category">
	<ffi:setProperty name="locationBPWID" value="${category.objectId}"/>
	
	<%-- now that the account's may have been activated or deactivated, reload account lists --%>
	<ffi:object id="GetBusinessAccounts" name="com.ffusion.tasks.accounts.GetBusinessAccounts" scope="session"/>
	<ffi:setProperty name="GetBusinessAccounts" property="AccountsName" value="BankingAccounts"/>
	<ffi:process name="GetBusinessAccounts"/>
	<ffi:removeProperty name="GetBusinessAccounts"/>
	
	<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
	<ffi:process name="GetAffiliateBanks"/>
	<ffi:removeProperty name="GetAffiliateBanks"/>
	<ffi:setProperty name="AffiliateBanks" property="SortedBy" value="BANK_NAME" />
	
	<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED %>">
		<ffi:object id="AddLocation" name="com.ffusion.tasks.cashcon.AddLocation" scope="session"/>
		<ffi:list collection="category.daItems" items="details">
			<ffi:cinclude value1="${details.fieldName}" value2="locationName">
				<ffi:setProperty name="AddLocation" property="locationName" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="locationID">
				<ffi:setProperty name="AddLocation" property="locationID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="active">
				<ffi:setProperty name="AddLocation" property="active" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="depositMinimum">
				<ffi:setProperty name="AddLocation" property="depositMinimum" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="customLocalBank">
				<ffi:setProperty name="AddLocation" property="customLocalBank" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localRoutingNumber">
				<ffi:setProperty name="AddLocation" property="localRoutingNumber" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localBankName">
				<ffi:setProperty name="AddLocation" property="localBankName" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localRoutingNumber">
				<ffi:setProperty name="AddLocation" property="localBankId" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="depositMaximum">
				<ffi:setProperty name="AddLocation" property="depositMaximum" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="anticDeposit">
				<ffi:setProperty name="AddLocation" property="anticDeposit" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="threshDeposit">
				<ffi:setProperty name="AddLocation" property="threshDeposit" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="customLocalAccount">
				<ffi:setProperty name="AddLocation" property="customLocalAccount" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="consolidateDeposits">
				<ffi:setProperty name="AddLocation" property="consolidateDeposits" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="depositPrenote">
				<ffi:setProperty name="AddLocation" property="depositPrenote" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="depositSameDayPrenote">
				<ffi:setProperty name="AddLocation" property="depositSameDayPrenote" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="customLocalAccount">
				<ffi:setProperty name="AddLocation" property="customLocalAccount" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localAccountNumber">
				<ffi:setProperty name="AddLocation" property="localAccountNumber" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localAccountType">
				<ffi:setProperty name="AddLocation" property="localAccountType" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localAccountID">
				<ffi:setProperty name="AddLocation" property="localAccountID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="disbursementPrenote">
				<ffi:setProperty name="AddLocation" property="disbursementPrenote" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="disbursementSameDayPrenote">
				<ffi:setProperty name="AddLocation" property="disbursementSameDayPrenote" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="cashConCompanyBPWID">
				<ffi:setProperty name="AddLocation" property="cashConCompanyBPWID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="concAccountBPWID">
				<ffi:setProperty name="AddLocation" property="concAccountBPWID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="disbAccountBPWID">
				<ffi:setProperty name="AddLocation" property="disbAccountBPWID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localBankID">
				<ffi:setProperty name="AddLocation" property="localBankID" value="${details.newValue}" />
			</ffi:cinclude>
		</ffi:list>
		<ffi:setProperty name="DivLocationName" value="${AddLocation.LocationName}" />
		<ffi:setProperty name="AddLocation" property="divisionID" value="${itemId}" />
		<ffi:setProperty name="AddLocation" property="ProcessFlag" value="true" />
		<%-- Restrict newly added location to the division only. --%>
		<ffi:setProperty name="AddLocation" property="AutoEntitleLocation" value="false" />
		<ffi:process name="AddLocation"/>
		<ffi:removeProperty name="AddLocation"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
		<ffi:object id="EditLocation" name="com.ffusion.tasks.cashcon.EditLocation" scope="session"/>
		
		<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount" scope="session" />
		<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%=com.ffusion.tasks.GetDisplayCount.CASHCON %>"/>
		<ffi:process name="GetDisplayCount"/>
		<ffi:removeProperty name="GetDisplayCount"/>
		
		<ffi:object id="GetLocations" name="com.ffusion.tasks.cashcon.GetLocations"/>
		<ffi:setProperty name ="GetLocations" property="DivisionID" value="${itemId}"/>
		<ffi:setProperty name="GetLocations" property="MaxResults" value="${DisplayCount}"/>
		<ffi:process name="GetLocations"/>
		<ffi:removeProperty name="GetLocations"/>
		
		<ffi:object id="SetLocation" name="com.ffusion.tasks.cashcon.SetLocation" scope="session" />
			<ffi:setProperty name="SetLocation" property="BPWID" value="${locationBPWID}"/>
		<ffi:process name="SetLocation"/>
		
		<ffi:setProperty name="EditLocation" property="locationName" value="${Location.LocationName}" />
		<ffi:setProperty name="EditLocation" property="locationID" value="${Location.LocationID}" />
		<ffi:setProperty name="EditLocation" property="active" value="${Location.Active}" />
		<ffi:setProperty name="EditLocation" property="depositMinimum" value="${Location.DepositMinimum}" />
		<ffi:setProperty name="EditLocation" property="localRoutingNumber" value="${Location.LocalRoutingNumber}" />
		<ffi:setProperty name="EditLocation" property="localBankName" value="${Location.LocalBankName}" />
		<ffi:setProperty name="EditLocation" property="depositMaximum" value="${Location.DepositMaximum}" />
		<ffi:setProperty name="EditLocation" property="anticDeposit" value="${Location.AnticDeposit}" />
		<ffi:setProperty name="EditLocation" property="threshDeposit" value="${Location.ThreshDeposit}" />
		<ffi:setProperty name="EditLocation" property="consolidateDeposits" value="${Location.ConsolidateDeposits}" />
		<ffi:setProperty name="EditLocation" property="depositPrenote" value="${Location.DepositPrenote}" />
		<ffi:setProperty name="EditLocation" property="depositSameDayPrenote" value="${Location.DepositSameDayPrenote}" />
		<ffi:cinclude value1="${Location.LocalAccountID}" value2="" operator="notEquals">
			<ffi:setProperty name="EditLocation" property="customLocalAccount" value="true" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Location.LocalAccountID}" value2="">
			<ffi:setProperty name="EditLocation" property="customLocalAccount" value="false" />
		</ffi:cinclude>
		<ffi:setProperty name="EditLocation" property="localAccountNumber" value="${Location.LocalAccountNumber}" />
		<ffi:setProperty name="EditLocation" property="localAccountType" value="${Location.LocalAccountType}" />
		<ffi:setProperty name="EditLocation" property="localAccountID" value="${Location.LocalAccountID}" />
		<ffi:setProperty name="EditLocation" property="disbursementPrenote" value="${Location.DisbursementPrenote}" />
		<ffi:setProperty name="EditLocation" property="disbursementSameDayPrenote" value="${Location.DisbursementSameDayPrenote}" />
		<ffi:setProperty name="EditLocation" property="cashConCompanyBPWID" value="${Location.CashConCompanyBPWID}" />
		<ffi:setProperty name="EditLocation" property="concAccountBPWID" value="${Location.ConcAccountBPWID}" />
		<ffi:setProperty name="EditLocation" property="disbAccountBPWID" value="${Location.DisbAccountBPWID}" />
		<ffi:setProperty name="EditLocation" property="LogId" value="${Location.LogId}"/>
		<ffi:setProperty name="EditLocation" property="SubmittedBy" value="${Location.SubmittedBy}"/>
		<ffi:setProperty name="EditLocation" property="DisPrenoteStatus" value="${Location.DisPrenoteStatus}" />
		<ffi:setProperty name="EditLocation" property="DepPrenoteStatus" value="${Location.DepPrenoteStatus}" />
		
		<ffi:setProperty name="EditLocation" property="CustomLocalBank" value="false" />
		<ffi:list collection="AffiliateBanks" items="Bank">
			<ffi:cinclude value1="${Bank.AffiliateRoutingNum}" value2="${Location.LocalRoutingNumber}" operator="equals">
				<ffi:setProperty name="EditLocation" property="CustomLocalBank" value="true" />
				<ffi:setProperty name="EditLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
			</ffi:cinclude>
		</ffi:list>
		<ffi:cinclude value1="${EditLocation.CustomLocalBank}" value2="false" operator="equals">
			<ffi:setProperty name="EditLocation" property="LocalRoutingNumber" value="${Location.LocalRoutingNumber}"/>
			<ffi:setProperty name="EditLocation" property="LocalBankName" value="${Location.LocalBankName}"/>
		</ffi:cinclude>
		
		<ffi:cinclude value1="${EditLocation.LocalBankID}" value2="0" operator="equals">
			<ffi:setProperty name="tempStart" value="true" />
			<ffi:list collection="AffiliateBanks" items="Bank">
				<ffi:cinclude value1="${tempStart}" value2="true" >
					<ffi:setProperty name="tempStart" value="false" />
					<ffi:setProperty name="EditLocation" property="LocalBankID" value="${Bank.AffiliateBankID}"/>
				</ffi:cinclude>
			</ffi:list>
	    </ffi:cinclude>
	    <ffi:removeProperty name="tempStart"/>
		
		<ffi:setProperty name="EditLocation" property="locationBPWID" value="${locationBPWID}" />
		<ffi:list collection="category.daItems" items="details">
			<ffi:cinclude value1="${details.fieldName}" value2="locationName">
				<ffi:setProperty name="EditLocation" property="locationName" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="locationID">
				<ffi:setProperty name="EditLocation" property="locationID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="active">
				<ffi:setProperty name="EditLocation" property="active" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="depositMinimum">
				<ffi:setProperty name="EditLocation" property="depositMinimum" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="customLocalBank">
				<ffi:setProperty name="EditLocation" property="customLocalBank" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localRoutingNumber">
				<ffi:setProperty name="EditLocation" property="localRoutingNumber" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localBankName">
				<ffi:setProperty name="EditLocation" property="localBankName" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localBankId">
				<ffi:setProperty name="EditLocation" property="localBankId" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="depositMaximum">
				<ffi:setProperty name="EditLocation" property="depositMaximum" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="anticDeposit">
				<ffi:setProperty name="EditLocation" property="anticDeposit" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="threshDeposit">
				<ffi:setProperty name="EditLocation" property="threshDeposit" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="customLocalAccount">
				<ffi:setProperty name="EditLocation" property="customLocalAccount" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="consolidateDeposits">
				<ffi:setProperty name="EditLocation" property="consolidateDeposits" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="depositPrenote">
				<ffi:setProperty name="EditLocation" property="depositPrenote" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="depositSameDayPrenote">
				<ffi:setProperty name="EditLocation" property="depositSameDayPrenote" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="customLocalAccount">
				<ffi:setProperty name="EditLocation" property="customLocalAccount" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localAccountNumber">
				<ffi:setProperty name="EditLocation" property="localAccountNumber" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localAccountType">
				<ffi:setProperty name="EditLocation" property="localAccountType" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localAccountID">
				<ffi:setProperty name="EditLocation" property="localAccountID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="disbursementPrenote">
				<ffi:setProperty name="EditLocation" property="disbursementPrenote" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="disbursementSameDayPrenote">
				<ffi:setProperty name="EditLocation" property="disbursementSameDayPrenote" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="cashConCompanyBPWID">
				<ffi:setProperty name="EditLocation" property="cashConCompanyBPWID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="concAccountBPWID">
				<ffi:setProperty name="EditLocation" property="concAccountBPWID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="disbAccountBPWID">
				<ffi:setProperty name="EditLocation" property="disbAccountBPWID" value="${details.newValue}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${details.fieldName}" value2="localBankID">
				<ffi:setProperty name="EditLocation" property="localBankID" value="${details.newValue}" />
			</ffi:cinclude>
		</ffi:list>
		<ffi:setProperty name="EditLocation" property="divisionID" value="${itemId}" />
		<ffi:setProperty name="EditLocation" property="ProcessFlag" value="true" />
		<ffi:setProperty name="EditLocation" property="SuccessURL" value="" />
		<ffi:setProperty name="DivLocationName" value="${Location.LocationName}" />
		<ffi:process name="EditLocation"/>
		<ffi:removeProperty name="EditLocation"/>
		<ffi:removeProperty name="AffiliateBanks"/>
		<ffi:removeProperty name="Locations"/>
		<ffi:removeProperty name="Location"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_DELETED %>">
	
		<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount" scope="session" />
		<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%=com.ffusion.tasks.GetDisplayCount.CASHCON %>"/>
		<ffi:process name="GetDisplayCount"/>
		<ffi:removeProperty name="GetDisplayCount"/>
	
		<ffi:object id="GetLocations" name="com.ffusion.tasks.cashcon.GetLocations"/>
		<ffi:setProperty name ="GetLocations" property="DivisionID" value="${itemId}"/>
		<ffi:setProperty name="GetLocations" property="MaxResults" value="${DisplayCount}"/>
		<ffi:process name="GetLocations"/>
		<ffi:removeProperty name="GetLocations"/>
		
		<ffi:object id="SetLocation" name="com.ffusion.tasks.cashcon.SetLocation" scope="session" />
			<ffi:setProperty name="SetLocation" property="BPWID" value="${locationBPWID}"/>
		<ffi:process name="SetLocation"/>
		<ffi:removeProperty name="SetLocation"/>
	
		<ffi:setProperty name="DivLocationName" value="${Location.LocationName}" />
		<ffi:object id="DeleteLocation" name="com.ffusion.tasks.cashcon.DeleteLocation"/>
		<ffi:setProperty name="DeleteLocation" property="LocationBPWID" value="${locationBPWID}"/>
		<ffi:process name="DeleteLocation"/>
		<ffi:removeProperty name="DeleteLocation"/>
		<ffi:removeProperty name="Location"/>
	</ffi:cinclude>
	
	<% 
		session.setAttribute("CATEGORY_BEAN", (com.ffusion.beans.dualapproval.DAItemCategory) request.getAttribute("category"));	
	%>
	
	<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${itemId}" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="category" value="<%=IDualApprovalConstants.CATEGORY_LOCATION %>" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="categoryBeanName" value="CATEGORY_BEAN" />
	<ffi:setProperty name="ApprovePendingChangesTask" property="groupName" value="${DivLocationName}" />
	<ffi:process name="ApprovePendingChangesTask"/>
	
	<ffi:removeProperty name="CATEGORY_BEAN"/>
	
</ffi:list>

<ffi:removeProperty name="MultipleCategories"/>
<ffi:removeProperty name="locationBPWID"/>
<ffi:removeProperty name="locationName"/>