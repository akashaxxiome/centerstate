<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<div  class="blockHead tableData"><span><strong><!--L10NStart-->Transaction Groups<!--L10NEnd--></strong></span></div>
<div class="paneWrapper">
<div class="paneInnerWrapper">
  		<table width="100%" class="tableData" border="0" cellspacing="0" cellpadding="3">
			<tr class="header">
				<td class="sectionsubhead adminBackground" >Field Name</td>
				<td class="sectionsubhead adminBackground" >Old Value</td>
				<td class="sectionsubhead adminBackground" >New Value</td>
				<td class="sectionsubhead adminBackground" >Action</td>
			</tr>
<ffi:list collection="DA_TRANSACTION_GROUPS" items="da_tran_group">
	<ffi:list collection="da_tran_group.DaItems" items="details">
		<ffi:cinclude value1="${details.fieldName}" value2="transactionGroupName" operator="equals">
		<ffi:setProperty name="newTGDA" value="${details.NewValue}" />
		<ffi:setProperty name="oldTGDA" value="${details.OldValue}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${details.fieldName}" value2="typeCodes" operator="equals">
		<ffi:setProperty name="newTCDA" value="${details.NewValue}" />
		<ffi:setProperty name="oldTCDA" value="${details.OldValue}" />
		</ffi:cinclude>	
	</ffi:list>
	<ffi:cinclude value1="${newTGDA}" value2="${oldTGDA}" operator="notEquals">
		<tr valign="top">
			<td class="columndata" >Transaction Group Name</td>
			<td class="columndata">
				<ffi:getProperty name="oldTGDA" />
			</td>
			<td class="columndata sectionheadDA">
				<ffi:getProperty name="newTGDA" />
			</td>
			<td  class="columndata"><ffi:getProperty name="da_tran_group" property="UserAction" /></td>
		</tr>
	</ffi:cinclude>
	<ffi:cinclude value1="${newTCDA}" value2="" operator="notEquals">
	<tr valign="top">
		<td class="columndata" >Assigned Typecodes</td>
		<td class="columndata" valign="top">
			<ffi:setProperty name="GetCompanyTransGroupsDA" property="CurrentTypeCodes" value="${oldTCDA}" />
			<ffi:list collection="GetCompanyTransGroupsDA.CurrentTypeCodeList" items="typeCode">
				<span><ffi:getProperty name="typeCode" property="Code"/>: <ffi:getProperty name="typeCode" property="Description"/></span><br />
			</ffi:list>
		</td>
		<td class="columndata sectionheadDA" valign="top">
			<ffi:setProperty name="GetCompanyTransGroupsDA" property="CurrentTypeCodes" value="${newTCDA}" />
			<ffi:list collection="GetCompanyTransGroupsDA.CurrentTypeCodeList" items="typeCode">
				<span><ffi:getProperty name="typeCode" property="Code"/>: <ffi:getProperty name="typeCode" property="Description"/></span><br />
			</ffi:list>
		</td>		
		<td  class="columndata"><ffi:getProperty name="da_tran_group" property="UserAction" /></td>
	</tr>
	</ffi:cinclude>
	<ffi:removeProperty name="newTGDA" />
	<ffi:removeProperty name="oldTGDA" />
	<ffi:removeProperty name="newTCDA" />
	<ffi:removeProperty name="oldTCDA" />
</ffi:list>
</table></div></div>