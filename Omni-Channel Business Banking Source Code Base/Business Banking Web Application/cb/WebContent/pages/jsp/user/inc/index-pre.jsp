<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:set var="tmpI18nStr" value="%{getText('jsp.default_400')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageHeading2' value=''/>

<ffi:setProperty name='PageText' value=''/>

<%-- initializing if necessary --%>
<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
    <s:include value="admin-init.jsp" />
</ffi:cinclude>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
    <ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>
<ffi:removeProperty name="GetBusinessEmployee" />

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="session"/>
	<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>
<ffi:removeProperty name="GetBusinessByEmployee" />

<ffi:object id="BankEmployeeById" name="com.ffusion.tasks.bankemployee.GetBankEmployeeById" scope="session" />
<ffi:cinclude value1="${Business.PersonalBanker}" value2="0" operator="notEquals">
	<ffi:setProperty name="BankEmployeeById" property="BankEmployeeSessionName" value="PersonalBanker" />
	<ffi:setProperty name="BankEmployeeById" property="Id" value="${Business.PersonalBanker}" />
	<ffi:process name="BankEmployeeById" />
</ffi:cinclude>
<ffi:cinclude value1="${Business.AccountRep}" value2="0" operator="notEquals">
	<ffi:setProperty name="BankEmployeeById" property="BankEmployeeSessionName" value="AccountRep" />
	<ffi:setProperty name="BankEmployeeById" property="Id" value="${Business.AccountRep}" />
	<ffi:process name="BankEmployeeById" />
</ffi:cinclude>
<ffi:removeProperty name="BankEmployeeById" />

<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
    <ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="BusinessEmployee"/>
<ffi:process name="GetGroupsAdministeredBy"/>
<ffi:removeProperty name="GetGroupsAdministeredBy" />

<ffi:object id="GetGroupSummaries" name="com.ffusion.tasks.user.GetEntitlementGroupSummaries"/>
<ffi:setProperty name="GetGroupSummaries" property="NumSpaces" value="8"/>
<ffi:process name="GetGroupSummaries"/>
<ffi:removeProperty name="GetGroupSummaries"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>
