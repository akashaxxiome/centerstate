<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<ffi:setProperty name="Compare" property="Value1" value="TRUE"/>
<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request"/>
<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${SecureUser.BaseCurrency}"/>

<% int limitIndex = ( ( Integer )session.getAttribute( "limitIndex" ) ).intValue(); %>

<ffi:setProperty name="DisplayedLimit" value="TRUE"/>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:removeProperty name="DARequest" />
	<ffi:removeProperty name="Entitlement_Limits" />
	<ffi:object id="GetLimitsFromDA" name="com.ffusion.tasks.dualapproval.GetLimitsFromDA" scope="session"/>
	<ffi:setProperty name="GetLimitsFromDA" property="limitListSessionName" value="Entitlement_Limits"/>
	<ffi:setProperty name="GetLimitsFromDA" property="operationName" value="${LimitType.OperationName}"/>
	<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits" scope="session"/>
	<ffi:setProperty name="GetGroupLimits" property="OperationName" value="${LimitType.OperationName}"/>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="GetGroupLimits" property="GroupId" value="${Business.EntitlementGroupId}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
		<ffi:setProperty name="GetGroupLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	</ffi:cinclude>
	<ffi:object id="GetRequireApprovalFromDA" name="com.ffusion.tasks.dualapproval.GetRequireApprovalFromDA" scope="session"/>
	<ffi:setProperty name="GetRequireApprovalFromDA" property="operationName" value="${LimitType.OperationName}"/>
	
</ffi:cinclude>
<tr>
	<% String checked = ""; %>
	<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
		<td class="tbrd_t" valign="middle">
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ADMIN_PARTNER %>"/>
			
			<ffi:cinclude value1="${CanAdminRow}" value2="TRUE" operator="equals">
				<div align="center">
					<ffi:getProperty name="admin${limitIndex}${TemplateIndex}" assignTo="checked"/>
					<input type="<ffi:getProperty name="AdminCheckBoxType"/>" disabled name=name value="value" <ffi:cinclude value2="<%=checked%>" value1="${LimitType.Value}" operator="equals"> checked </ffi:cinclude> border="0">
				</div>
			</ffi:cinclude>
			<ffi:cinclude value1="${CanAdminRow}" value2="TRUE" operator="notEquals">
				<div align="center">
					<%--&nbsp;--%>
				</div>
			</ffi:cinclude>
		</td>
		</ffi:cinclude>
	</ffi:cinclude>
	
	<ffi:setProperty name="CheckEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:setProperty name="CheckEntitlementByGroup" property="OperationName" value="${LimitType.OperationName}"/>

	<ffi:cinclude value1="${CheckEntitlementObjectType}" value2="" operator="notEquals">
		<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectType" value="${CheckEntitlementObjectType}"/>
	</ffi:cinclude>

	<ffi:cinclude value1="${CheckEntitlementObjectId}" value2="" operator="notEquals">
		<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectId" value="${CheckEntitlementObjectId}"/>
	</ffi:cinclude>

	<ffi:process name="CheckEntitlementByGroup"/>
	<ffi:setProperty name="Compare" property="Value2" value="${CheckEntitlement}"/>
	<td class="tbrd_t" valign="middle">
		<div align="center">
			<% checked = ""; %>
			<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
			<ffi:getProperty name="entitlement${limitIndex}" assignTo="checked"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
			<ffi:getProperty name="entitlement${limitIndex}_${TemplateIndex}" assignTo="checked"/>
			</ffi:cinclude>
				<ffi:cinclude value1="${CanInitRow}" value2="TRUE" operator="equals">
					<input type="checkbox" disabled name=name value="value" <ffi:cinclude value2="<%=checked%>" value1="${LimitType.OperationName}" operator="equals"> checked </ffi:cinclude> border="0">
				</ffi:cinclude>
				<ffi:cinclude value1="${CanInitRow}" value2="TRUE" operator="notEquals">
					&nbsp;
				</ffi:cinclude>
		</div>
	</td>
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL %>"/>
            <%  String classParam = "tbrd_t columndata"; %>
			<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">

				<% String indentLevel; %>
				<ffi:getProperty name="LimitType" property="Value" assignTo="indentLevel"/>
				<%
					int indentLevelAsInt = Integer.parseInt( indentLevel );
					if (indentLevelAsInt > 0) {
                       classParam = "tbrd_t indent" + Integer.toString(indentLevelAsInt) + " columndata";
					}
				%>
			</ffi:cinclude>
	<td class='<ffi:getPendingStyle fieldname="${LimitType.OperationName}" defaultcss="<%=classParam %>" 
						dacss='<%=classParam + " columndataDA" %>' 
						sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'  valign="middle" align="left">
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
			<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
				<ffi:process name="GetEntitlementPropertyValues"/>
				<ffi:getProperty name="GetEntitlementPropertyValues" property="PropertyValue"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
				<ffi:getProperty name="LimitType" property="OperationName"/>
			</ffi:cinclude>
	</td>
	<%-- Requires Approval Processing --%>
	<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
		<ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
			<ffi:setProperty name="EditRequiresApproval" property="OperationName" value="${LimitType.OperationName}" />
			<ffi:setProperty name="EditRequiresApproval" property="ExecOpSelVerify" value="true" />
			<ffi:process name="EditRequiresApproval" />
			<td class="tbrd_t" valign="middle">&nbsp;&nbsp;
				<ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="true" operator="equals">
					<input type="checkbox"  disabled <ffi:getProperty name="EditRequiresApproval" property="OpSelected" />	/>
				</ffi:cinclude>
				<ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="true" operator="notEquals">
					&nbsp;
				</ffi:cinclude>
			</td>
		</ffi:cinclude>
	</ffi:cinclude>
	<%--  END Requires Approval Processing --%>
	<% String limitValue = ""; %>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
	<ffi:getProperty name="transaction_limit${limitIndex}" assignTo="limitValue"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
	<ffi:getProperty name="transaction_limit${limitIndex}_${TemplateIndex}" assignTo="limitValue"/>
	</ffi:cinclude>
	<ffi:cinclude value2="<%=checked%>"  value1="${LimitType.OperationName}" operator="notEquals">
		<td colspan="2" class="tbrd_t columndata" valign="middle" align="left" nowrap>
			<s:text name="jsp.user_222"/>
		</td>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:setProperty name="GetGroupLimits" property="Period" value="1"/>
			<ffi:process name="GetGroupLimits"/>
			<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="1"/>
			<ffi:process name="GetLimitsFromDA" />
	</ffi:cinclude>
	<ffi:cinclude value2="<%=checked%>"  value1="${LimitType.OperationName}" operator="equals">
		<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
			<td colspan="2" class="tbrd_t columndata" valign="middle" align="left" nowrap>
				<s:text name="jsp.user_222"/>
			</td>
		</ffi:cinclude>
		
		<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
			<td colspan="2" class="tbrd_t" valign="middle">
				<div align="left" class="columndata">
					 <ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
					 <ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
					 <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
						<s:text name="jsp.user_244"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals" >
						<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="notEquals" >
							<span class="columndata"><s:text name="jsp.user_244"/></span>
						</ffi:cinclude>
						<ffi:cinclude value1="${DARequest.PerTransactionChange}" value2="Y" operator="equals" >
							<span class="columndataDA"><s:text name="jsp.user_244"/></span>
							<span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerTransaction" /></span>
						</ffi:cinclude>
					</ffi:cinclude>
				</div>
			</td>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:setProperty name="GetGroupLimits" property="Period" value="2"/>
		<ffi:process name="GetGroupLimits"/>
		<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="2"/>
		<ffi:process name="GetLimitsFromDA" />
	</ffi:cinclude>
	<% String x = ""; %>
	<td class="tbrd_t" valign="middle" align="center">
		<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
		<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
			<ffi:setProperty name="allowApproval" value="${LimitType.Value}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${LimitType.Value}" value2="" operator="equals">
			<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
			<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
				<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
				<ffi:getProperty name="transaction_exceed${limitIndex}" assignTo="x"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
				<ffi:getProperty name="transaction_exceed${limitIndex}_${TemplateIndex}" assignTo="x"/>
				</ffi:cinclude>
				<input type="checkbox" disabled name=name value="value" <ffi:cinclude value2="<%=x%>"  value1="true" operator="equals"> checked </ffi:cinclude> border="0">
				<% pageContext.setAttribute("x", ""); %>
			</ffi:cinclude>
			<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
				&nbsp;
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="notEquals">
			&nbsp;
		</ffi:cinclude>
	</td>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
	<ffi:getProperty name="day_limit${limitIndex}" assignTo="limitValue"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
	<ffi:getProperty name="day_limit${limitIndex}_${TemplateIndex}" assignTo="limitValue"/>
	</ffi:cinclude>
	<ffi:cinclude value2="<%=checked%>"  value1="${LimitType.OperationName}" operator="notEquals">
		<td colspan="2" class="tbrd_t columndata" valign="middle" align="left" nowrap>
			<s:text name="jsp.user_220"/>
		</td>
	</ffi:cinclude>
	<ffi:cinclude value2="<%=checked%>"  value1="${LimitType.OperationName}" operator="equals">
		<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
			<td colspan="2" class="tbrd_t columndata" valign="middle" align="left" nowrap>
				<s:text name="jsp.user_220"/>
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
			<td colspan="2" class="tbrd_t" valign="middle">
				<div align="left" class="columndata">
					 <ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
					 <ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
					  <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
						<s:text name="jsp.user_242"/>
					  </ffi:cinclude>
					  <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
						<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="notEquals" >
							<span class="columndata"><s:text name="jsp.user_242"/></span>
						</ffi:cinclude>
						<ffi:cinclude value1="${DARequest.PerDayChange}" value2="Y" operator="equals" >
							<span class="columndataDA"><s:text name="jsp.user_242"/></span>
							<span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerDay" /></span>
						</ffi:cinclude>
					</ffi:cinclude>
				</div>
			</td>
		</ffi:cinclude>
	</ffi:cinclude>
	<td class="tbrd_t" valign="middle" align="center">
		<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
		<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
			<ffi:setProperty name="allowApproval" value="${LimitType.Value}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${LimitType.Value}" value2="" operator="equals">
			<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
			<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
				<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
				<ffi:getProperty name="day_exceed${limitIndex}" assignTo="x"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
				<ffi:getProperty name="day_exceed${limitIndex}_${TemplateIndex}" assignTo="x"/>
				</ffi:cinclude>
				<input type="checkbox" disabled name=name value="value" <ffi:cinclude value2="<%=x%>"  value1="true" operator="equals"> checked </ffi:cinclude> border="0">
				<% pageContext.setAttribute("x", ""); %>
			</ffi:cinclude>
			<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
				&nbsp;
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="notEquals">
			&nbsp;
		</ffi:cinclude>
	</td>
</tr>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:setProperty name="GetGroupLimits" property="Period" value="3"/>
		<ffi:process name="GetGroupLimits"/>
		<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="3"/>
		<ffi:process name="GetLimitsFromDA" />
	</ffi:cinclude>
<tr>
	<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
			<td></td>
		</ffi:cinclude>
	</ffi:cinclude>
	<td></td>
	<td></td>
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
	<ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
	<td>&nbsp;</td>
        </ffi:cinclude>
</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
	<ffi:getProperty name="week_limit${limitIndex}" assignTo="limitValue"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
	<ffi:getProperty name="week_limit${limitIndex}_${TemplateIndex}" assignTo="limitValue"/>
	</ffi:cinclude>
	<ffi:cinclude value2="<%=checked%>"  value1="${LimitType.OperationName}" operator="notEquals">
		<td colspan="2" class="columndata" valign="middle" align="left" nowrap>
			<s:text name="jsp.user_223"/>
		</td>
	</ffi:cinclude>
	<ffi:cinclude value2="<%=checked%>"  value1="${LimitType.OperationName}" operator="equals">
		<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
			<td colspan="2" class="columndata" valign="middle" align="left" nowrap>
				<s:text name="jsp.user_223"/>
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
			<td colspan="2" valign="middle">
				<div align="left" class="columndata">
					 <ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
					 <ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
					 <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
						<s:text name="jsp.user_245"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
						 <ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="notEquals" >
							<span class="columndata"><s:text name="jsp.user_245"/></span>
						</ffi:cinclude>
						<ffi:cinclude value1="${DARequest.PerWeekChange}" value2="Y" operator="equals" >
							<span class="columndataDA"><s:text name="jsp.user_245"/></span>
							<span class="sectionhead_greyDA"><ffi:getProperty name="DARequest" property="PerWeek" /></span>
						</ffi:cinclude>
					</ffi:cinclude>
				</div>
			</td>
		</ffi:cinclude>
	</ffi:cinclude>
	<td valign="middle" align="center">
		<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
		<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
			<ffi:setProperty name="allowApproval" value="${LimitType.Value}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${LimitType.Value}" value2="" operator="equals">
			<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
			<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
				<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
				<ffi:getProperty name="week_exceed${limitIndex}" assignTo="x"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
				<ffi:getProperty name="week_exceed${limitIndex}_${TemplateIndex}" assignTo="x"/>
				</ffi:cinclude>
				<input type="checkbox" disabled name=name value="value" <ffi:cinclude value2="<%=x%>"  value1="true" operator="equals"> checked </ffi:cinclude> border="0">
				<% pageContext.setAttribute("x", ""); %>
			</ffi:cinclude>
			<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
				&nbsp;
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="notEquals">
			&nbsp;
		</ffi:cinclude>
	</td>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:setProperty name="GetGroupLimits" property="Period" value="4"/>
		<ffi:process name="GetGroupLimits"/>
		<ffi:setProperty name="GetLimitsFromDA" property="PeriodId" value="4"/>
		<ffi:process name="GetLimitsFromDA" />
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
	<ffi:getProperty name="month_limit${limitIndex}" assignTo="limitValue"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
	<ffi:getProperty name="month_limit${limitIndex}_${TemplateIndex}" assignTo="limitValue"/>
	</ffi:cinclude>
	<ffi:cinclude value2="<%=checked%>"  value1="${LimitType.OperationName}" operator="notEquals">
		<td colspan="2" class="columndata" valign="middle" align="left" nowrap>
			<s:text name="jsp.user_221"/>
		</td>
	</ffi:cinclude>
	<ffi:cinclude value2="<%=checked%>"  value1="${LimitType.OperationName}" operator="equals">
		<ffi:cinclude value1="${limitValue}" value2="" operator="equals">
			<td colspan="2" class="columndata" valign="middle" align="left" nowrap>
				<s:text name="jsp.user_221"/>
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${limitValue}" value2="" operator="notEquals">
			<td colspan="2" valign="middle" >
				<div align="left" class="columndata">
					 <ffi:setProperty name="CurrencyObject" property="Amount" value="${limitValue}"/>
					 <ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/>
					  <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
					 	<s:text name="jsp.user_243"/>
					 </ffi:cinclude>
					 <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
						<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="notEquals" >
							<span class="columndata"><s:text name="jsp.user_243"/></span>
						</ffi:cinclude>
						<ffi:cinclude value1="${DARequest.PerMonthChange}" value2="Y" operator="equals" >
							<span class="columndataDA"><s:text name="jsp.user_243"/></span>
						</ffi:cinclude>
					</ffi:cinclude>
				</div>
			</td>
		</ffi:cinclude>
	</ffi:cinclude>
	<td valign="middle" align="center">
		<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ALLOW_APPROVAL %>"/>
		<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
			<ffi:setProperty name="allowApproval" value="${LimitType.Value}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${LimitType.Value}" value2="" operator="equals">
			<ffi:setProperty name="allowApproval" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
			<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
				<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
				<ffi:getProperty name="month_exceed${limitIndex}" assignTo="x"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
				<ffi:getProperty name="month_exceed${limitIndex}_${TemplateIndex}" assignTo="x"/>
				</ffi:cinclude>
				<input type="checkbox" disabled name=name value="value" <ffi:cinclude value2="<%=x%>"  value1="true" operator="equals"> checked </ffi:cinclude> border="0">
				<% pageContext.setAttribute("x", ""); %>
			</ffi:cinclude>
			<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
				&nbsp;
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${allowApproval}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="notEquals">
			&nbsp;
		</ffi:cinclude>
	</td>
</tr>

<ffi:removeProperty name="RAChecked" />
<ffi:removeProperty name="showReqApp"/>