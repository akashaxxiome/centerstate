<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroup"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.sap.banking.web.util.entitlements.EntitlementsUtil"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%-- remove objects used by the edit ACH company forms from the session --%>
<ffi:removeProperty name="EditACHCompanyAccess"/>
<ffi:removeProperty name="ApprovalAdminByBusiness"/>
<ffi:removeProperty name="CheckForRedundantACHLimits"/>
<ffi:removeProperty name="crossACHEntLists"/>
<ffi:removeProperty name="perACHEntLists"/>
<ffi:removeProperty name="limitInfoList"/>
<ffi:removeProperty name="ACHCompanyID"/>
<ffi:removeProperty name="ACHCompany"/>
<ffi:removeProperty name="perBatchMax"/>
<ffi:removeProperty name="dailyMax"/>
<ffi:removeProperty name="ChildParentMap"/>

<% boolean showAutoEntitle = true; %>

<ffi:setProperty name="confirmURL" value='${SecurePath}user/editautoentitle_confirm.jsp'/>
<ffi:object id="ModifyAutoEntitleSettings" name="com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings" scope="session"/>
<ffi:setProperty name="backURL" value="${SecurePath}user/editautoentitle.jsp"/>

<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EntitlementGroupSessionKey" value="<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>"/>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
<ffi:process name="GetEntitlementGroup"/>
</s:if>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">

<!-- Get Business Employees -->
<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchEntitlementProfiles" value="TRUE"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:setProperty name="GetBusinessEmployees" property="BusinessEmployeesSessionName" value="entitlementProfiles"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>
<!-- Get Business Employees Ends -->

<%-- SetBusinessEmployee.Id should be passed in the request --%>
<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${BusinessEmployeeId}"/>
<ffi:process name="SetBusinessEmployee"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EntitlementGroupMemberSessionKey" value="ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE"/>

<% com.ffusion.beans.user.BusinessEmployee businessEmployee = (com.ffusion.beans.user.BusinessEmployee) session.getAttribute("BusinessEmployee");
    EntitlementGroupMember member = EntitlementsUtil.getEntitlementGroupMemberForBusinessEmployee(businessEmployee);
    session.setAttribute( "ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE", member );
%>
</s:if>
<ffi:process name="ModifyAutoEntitleSettings"/>
<%
 com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings ModifyAutoEntitleSettings = (com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings)session.getAttribute( "ModifyAutoEntitleSettings" ); 
 if( ModifyAutoEntitleSettings.getParentAnyPermissionEnabled().equals( "false" ) ) {
     showAutoEntitle = false; 
 }
 %>
 <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
 <s:if test="%{#session.Section == 'Users'}">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.Id}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>" />
		<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="true" />
		<ffi:setProperty name="GetDACategoryDetails" property="MultipleCategoriesSessionName" value="EntitlementBean" />
	<ffi:process name="GetDACategoryDetails"/>
 </s:if>
 <ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.Id}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>" />
		<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="true" />
		<ffi:setProperty name="GetDACategoryDetails" property="MultipleCategoriesSessionName" value="EntitlementBean" />
	<ffi:process name="GetDACategoryDetails"/>
 </ffi:cinclude>
 <ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
 </ffi:cinclude>
  <ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
 </ffi:cinclude>
  <ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
 </ffi:cinclude>
 </ffi:cinclude>

<% if( showAutoEntitle ) { %>
<%-- Do not show the auto-entitlement selection if DA mode is enabled. --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="notEquals">
<div id="permmenu_autoentitle" class="permissionMenuWrapper">
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
	<table width="100%" border="0" cellspacing="0" cellpadding="2" class="">

		<%	int band = 0; %>
		<tr class="header">
			<td width="250"><s:text name="jsp.admin.company.auto.entitlement" /></td>
			<td width="475"></td>
			<td width="55">Action</td>
		</tr>
		<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
			<td id="autoEntitlement" width="250"><s:text name="jsp.admin.company.auto.entitlement.options" /></td>
			<td id="autoEntitlementDesc" width="475"><s:text name="jsp.admin.company.enable.disable.auto.entitlement.options" /></td>
			<td width="55">
				<a id="editAutoEntitlement" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' onClick=
                   	"ns.admin.gotoPermission('autoEntitleTab')">
                   	<s:text name="jsp.default_178" />
                </a>
				<%-- 
				<ffi:link url="editautoentitle.jsp?&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
				</ffi:link>
				--%>
			</td>
		</tr>
		<%	band++;	%>
	</table>	
	</div></div>
</div>
<%-- <script type="text/javascript">
	$('#permmenu_autoentitle').pane({
		title: 'Auto-Entitlement',
		minmax: true,
		close: false
	});
</script> --%>
</ffi:cinclude>
<% } %>












<!-- Admin Permissions Menu -->
<div id="permmenu_menus" class="permissionMenuWrapper">
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
<table width="100%" border="0" cellspacing="0" cellpadding="2">
	<tr class="header">
		<td class="" width="250">
			<span class="sectionsubhead"><s:text name="jsp.admin.company.permission.types" /></span>
		</td>
		<td width="475" >
			<span class="sectionsubhead"><s:text name="jsp.admin.company.description" /></span>
		</td>
		<td class="" width="55">Action</td>
	</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="10" class="tableAlerternateRowColor">

	<%	int band = 0; %>

	<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
		<td id="accountAccess" width="250">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:list collection="categories" items="category">
					<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>" operator="equals">
						<ffi:setProperty name="highLight" value="true" />
					</ffi:cinclude>
				</ffi:list>
				<ffi:cinclude value1="${highLight}" value2="true">
					<span class="columndataDA"><s:text name="jsp.admin.company.account.access" /></span>
				</ffi:cinclude>
				<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
					<s:text name="jsp.admin.company.account.access" />
				</ffi:cinclude>
				<ffi:removeProperty name="highLight" />
			</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<s:text name="jsp.admin.company.account.access" />
		</ffi:cinclude>
	</td>
	<td id="accountAccessDesc" width="475"><s:text name="jsp.admin.company.grant.permission.account.groups" /></td>
	<td width="25">
<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">	
	<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
		<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
			onClick="ns.admin.gotoPermission('acctAccessTab')">
	        View
        </a>
		<%-- 
		<ffi:link url="accountaccess-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
			<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
		</ffi:link>
		--%>
	</ffi:cinclude>
	<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
		<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
			onClick="ns.admin.gotoPermission('acctAccessTab')">
	        <s:text name="jsp.default_178" />
        </a>
		<%-- 
		<ffi:link url="accountaccess.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
			<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
		</ffi:link>
		--%>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
		<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
			onClick="ns.admin.gotoPermission('acctAccessTab')">
	        View
        </a>
		<%-- 
		<ffi:link url="accountaccess-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
			<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
		</ffi:link>
		--%>
	</ffi:cinclude>
	<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
		<a id="editAccountAccess" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
			onClick="ns.admin.gotoPermission('acctAccessTab')">
	        <s:text name="jsp.default_178" />
        </a>
		<%-- 
		<ffi:link url="accountaccess.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
			<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
		</ffi:link>
		--%>
	</ffi:cinclude>
	
</ffi:cinclude>	
	</td>
	</tr>
	<%	band++;	%>

	<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
		<td id="accountAccessGroup" width="250">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
				<ffi:list collection="categories" items="category">
					<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" operator="equals">
						<ffi:setProperty name="highLight" value="true" />
					</ffi:cinclude>
				</ffi:list>
				<ffi:cinclude value1="${highLight}" value2="true">
					<span class="columndataDA"><s:text name="jsp.admin.company.account.group.access" /></span>
				</ffi:cinclude>
				<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
					<s:text name="jsp.admin.company.account.group.access" />
				</ffi:cinclude>
				<ffi:removeProperty name="highLight" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<s:text name="jsp.admin.company.account.group.access" />
			</ffi:cinclude>
		</td>
		<td id="accountAccessGroupDesc" width="475"><s:text name="jsp.admin.company.grant.permission.account.groups" /></td>
		<td width="25">
	<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">		
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
		<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
			onClick="ns.admin.gotoPermission('acctGroupAccessTab')">
	        View
        </a>
		<%-- 
			<ffi:link url="accountgroupaccess-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
			</ffi:link>
		--%>
		</ffi:cinclude>
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
			<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
				onClick="ns.admin.gotoPermission('acctGroupAccessTab')">
		        <s:text name="jsp.default_178" />
	        </a>
			<%-- 
			<ffi:link url="accountgroupaccess.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
			</ffi:link>
			--%>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
			<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
				onClick="ns.admin.gotoPermission('acctGroupAccessTab')">
		        View
	        </a>
			<%-- 
			<ffi:link url="accountgroupaccess-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
			</ffi:link>
			--%>
		</ffi:cinclude>
		<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
			<a id="editAccountAccessGroup" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
				onClick="ns.admin.gotoPermission('acctGroupAccessTab')">
		        <s:text name="jsp.default_178" />
	        </a>
			<%-- 
			<ffi:link url="accountgroupaccess.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
			</ffi:link>
			--%>
		</ffi:cinclude>	
	</ffi:cinclude>
		</td>
	</tr>
	<%-- DETERMINE WHETHER TO DISPLAY ACH OR NOT --%>
	<s:include value="%{#session.PagesPath}user/inc/checkdisplayach.jsp" />

	<ffi:cinclude value1="${displayACH}" value2="true" operator="equals">
		<%	band++;	%>
		<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
		
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:list collection="categories" items="category">
				<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_CROSS_ACH_COMPANY_ACCESS %>">
					<ffi:setProperty name="highLight" value="true" />
				</ffi:cinclude>
			</ffi:list>
			<ffi:cinclude value1="${highLight}" value2="true">
				<td width="250" class="columndataDA"><s:text name="jsp.admin.company.cross.ach.company.access" /></td>
			</ffi:cinclude>
			<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
				<td width="250" ><s:text name="jsp.admin.company.cross.ach.company.access" /></td>
			</ffi:cinclude>
			<ffi:removeProperty name="highLight" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<td id="crossAchCompanyAccess" width="250"><s:text name="jsp.admin.company.cross.ach.company.access" /></td>
		</ffi:cinclude>
			<td id="crossAchCompanyAccessDesc" width="475"><s:text name="jsp.admin.company.administer.access.limits.all.ach.company" /></td>
			<td width="25">
		<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">	
			<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
				<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
					onClick="ns.admin.gotoPermission('xACHCompanyTab')">
			        View
		        </a>
				<%-- 
				<ffi:link url="crossachcompanyaccess-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
				</ffi:link>
				--%>
			</ffi:cinclude>
			<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
				<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
					onClick="ns.admin.gotoPermission('xACHCompanyTab')">
			        <s:text name="jsp.default_178" />
		        </a>
				<%-- 
				<ffi:link url="crossachcompanyaccess.jsp?Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
				</ffi:link>
				--%>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
				<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
					onClick="ns.admin.gotoPermission('xACHCompanyTab')">
			        View
		        </a>
				<%-- 
				<ffi:link url="crossachcompanyaccess-view.jsp?Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
				</ffi:link>
				--%>
			</ffi:cinclude>
			<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
				<a id="editCrossAchCompanyAccess" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
					onClick="ns.admin.gotoPermission('xACHCompanyTab')">
			        <s:text name="jsp.default_178" />
		        </a>
				<%-- 
				<ffi:link url="crossachcompanyaccess.jsp?Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
				</ffi:link>
				--%>
			</ffi:cinclude>
		</ffi:cinclude>
			</td>
		</tr>
		<%	band++;	%>

		<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:list collection="categories" items="category">
				<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_PER_ACH_COMPANY_ACCESS %>">
					<ffi:setProperty name="highLight" value="true" />
				</ffi:cinclude>
			</ffi:list>
			<ffi:cinclude value1="${highLight}" value2="true">
				<td width="250" class="columndataDA"> <s:text name="jsp.admin.company.per.ach.company.access" /></td>
			</ffi:cinclude>
			<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
				<td width="250" ><s:text name="jsp.admin.company.per.ach.company.access" /></td>
			</ffi:cinclude>
			<ffi:removeProperty name="highLight" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<td id="perAchCompanyAccess" width="250"><s:text name="jsp.admin.company.per.ach.company.access" /></td>
		</ffi:cinclude>
			<td id="perAchCompanyAccessDesc" width="475"><s:text name="jsp.admin.company.administer.access.limits.ach.company" /></td>
			<td width="25">
		<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">	
			<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
				<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
					onClick="ns.admin.gotoPermission('aCHCompanyTab')">
			        View
		        </a>
				<%-- 
				<ffi:link url="achcompanyaccess-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
				</ffi:link>
				--%>
			</ffi:cinclude>
			<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
				<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
					onClick="ns.admin.gotoPermission('aCHCompanyTab')">
			        <s:text name="jsp.default_178" />
		        </a>
				<%-- 
				<ffi:link url="achcompanyaccess.jsp?Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
				</ffi:link>
				--%>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
				<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
					onClick="ns.admin.gotoPermission('aCHCompanyTab')">
			        View
		        </a>
				<%-- 
				<ffi:link url="achcompanyaccess-view.jsp?Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
				</ffi:link>
				--%>
			</ffi:cinclude>
			<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
				<a id="editPerAchCompanyAccess" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
					onClick="ns.admin.gotoPermission('aCHCompanyTab')">
			        <s:text name="jsp.default_178" />
		        </a>
				<%-- 
				<ffi:link url="achcompanyaccess.jsp?Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
				</ffi:link>
				--%>
			</ffi:cinclude>		
		</ffi:cinclude>
			</td>
		</tr>
	</ffi:cinclude>
	<%	band++;	%>

	<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
		<td id="XAccountAccessLimit" width="250"><s:text name="jsp.admin.company.feature.access.limits.cross.account" /></td>
		<td id="XAccountAccessLimitDesc" width="475"><s:text name="jsp.admin.company.grant.limit.account" /></td>
		<td width="25">
			<!-- <img src="/cb/web/multilang/grafx/user/spacer.gif" alt="Edit" hspace="3" border="0" height="14" width="19"> -->
		</td>
	</tr>
	<%	band++;	%>
	
	<s:include value="%{#session.PagesPath}user/inc/checkentitled-crossaccountcomponents.jsp" />
	<%  int count = 0;  %>

<%String catSubType = null; %>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>	
<!-- Dual approval highlighting for cross account-->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:list collection="LocalizedComponentNamesCross" items="currComponentName">
			<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
					<ffi:list collection="categories" items="crossAccCategory">
						<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
						<ffi:setProperty name="GetDACategorySubType" property="DaItemCategoryName" value="crossAccCategory"/>
						<ffi:setProperty name="GetDACategorySubType" property="ComponentName" value="${currComponentName}"/>
						<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
										value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT %>" />
						<ffi:process name="GetDACategorySubType"/>
						<ffi:removeProperty name="GetDACategorySubType"/>
						
						<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
						<ffi:cinclude value1="${catSubType}" value2="" operator="notEquals">
							<ffi:setProperty name="highLight" value="true" />
						</ffi:cinclude>
						<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
						<ffi:removeProperty name="catSubType"/>
					</ffi:list>
				<ffi:cinclude value1="${highLight}" value2="true">
					<td width="250" class="columndataDA">
				</ffi:cinclude>
				<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
					<td width="250">
				</ffi:cinclude>
				<ffi:removeProperty name="highLight" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="currComponentName"/></td>
				<td width="475">&nbsp;</td>
				<td width="25">
					<% session.setAttribute("count", String.valueOf(count)); %>
				<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
					<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
						<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
							onClick="ns.admin.gotoPermission('crossAccountTab',<ffi:getProperty name="count"/>)">
					        View
				        </a>
						<%-- 
						<ffi:link url="crossaccountpermissions.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
							<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
						</ffi:link>
						--%>
					</ffi:cinclude>
					<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
						<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
							onClick="ns.admin.gotoPermission('crossAccountTab',<ffi:getProperty name="count"/>)">
					        <s:text name="jsp.default_178" />
				        </a>
						<%-- 
						<ffi:link url="crossaccountpermissions.jsp?Section=${Section}&Init=TRUE&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
							<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
						</ffi:link>
						--%>
					</ffi:cinclude>
				</s:if>
				<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
					<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
						<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
							<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
								onClick="ns.admin.gotoPermission('crossAccountTab',<ffi:getProperty name="count"/>)">
						        View
					        </a>
							<%-- 
							<ffi:link url="crossaccountpermissions.jsp?Section=${Section}&Init=TRUE&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
								<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
							</ffi:link>
							--%>
						</ffi:cinclude>
						<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
							<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
								onClick="ns.admin.gotoPermission('crossAccountTab',<ffi:getProperty name="count"/>)">
						        <s:text name="jsp.default_178" />
					        </a>
							<%-- 
							<ffi:link url="crossaccountpermissions.jsp?Section=${Section}&Init=TRUE&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
								<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
							</ffi:link>
							--%>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
					<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
						<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
							onClick="ns.admin.gotoPermission('crossAccountTab',<ffi:getProperty name="count"/>)">
					        View
				        </a>
						<%-- 
						<ffi:link url="crossaccountpermissions.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
							<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
						</ffi:link>
						--%>
					</ffi:cinclude>
					<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
						<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
							onClick="ns.admin.gotoPermission('crossAccountTab',<ffi:getProperty name="count"/>)">
					        <s:text name="jsp.default_178" />
				        </a>
						<%-- 
						<ffi:link url="crossaccountpermissions.jsp?Section=${Section}&Init=TRUE&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
							<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
						</ffi:link>
						--%>
					</ffi:cinclude>
					</ffi:cinclude>
				</s:if>
				</td>
			</tr>
			<%  count++;  %>
			<%  band++;  %>
		</ffi:list>
	</ffi:cinclude>
	
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals" >
    <%  int xcount = 0;   %>
	<ffi:list collection="LocalizedComponentNamesCross" items="currComponentName">
			<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
				<td id="XAccountAccessLimit<%= xcount%>" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="currComponentName"/></td>
				<td width="475">&nbsp;</td>
				<td width="25">
					<% session.setAttribute("count", String.valueOf(count)); %>
					<a id="editXAccountAccessLimit<%= xcount++%>" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
						onClick="ns.admin.gotoPermission('crossAccountTab',<ffi:getProperty name="count"/>)">
				        <s:text name="jsp.default_178" />
			        </a>
					<%-- 
					<ffi:link url="crossaccountpermissions.jsp?Section=${Section}&Init=TRUE&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
						<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
					</ffi:link>
					--%>
				</td>
			</tr>
			<%  count++;  %>
			<%  band++;  %>
		</ffi:list>
</ffi:cinclude>

<ffi:removeProperty name="LocalizedComponentNamesCross"/>


	<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
		<td id="perAccountAccessLimit" width="250"><s:text name="jsp.admin.company.feature.access.per.account" /></td>
		<td id="perAccountAccessLimitDesc" width="475"><s:text name="jsp.admin.company.grant.use.particular.accounts" /></td>
		<td width="25">
			<!-- <img src="/cb/web/multilang/grafx/user/spacer.gif" alt="Edit" hspace="3" border="0" height="14" width="19"> -->
		</td>
	</tr>
	<%	band++;	%>
	<s:include value="%{#session.PagesPath}user/inc/checkentitled-peraccountcomponents.jsp" />
	<%  count = 0;  %>
<ffi:removeProperty name="currComponentName"/>

<!-- Dual approval highlighting for per account-->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:list collection="LocalizedComponentNamesPer" items="currComponentName">
		<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
		<ffi:list collection="categories" items="perAccountCategory">
				<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
				<ffi:setProperty name="GetDACategorySubType" property="DaItemCategoryName" value="perAccountCategory"/>
				<ffi:setProperty name="GetDACategorySubType" property="ComponentName" value="${currComponentName}"/>
				<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
								value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_ACCOUNT %>" />
				<ffi:process name="GetDACategorySubType"/>
				<ffi:removeProperty name="GetDACategorySubType"/>
				
				<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
				<ffi:cinclude value1="${catSubType}" value2="" operator="notEquals">
					<ffi:setProperty name="highLight" value="true" />
				</ffi:cinclude>
				<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
				<ffi:removeProperty name="catSubType"/>

					</ffi:list>
				<ffi:cinclude value1="${highLight}" value2="true">
					<td width="250" class="columndataDA ">
				</ffi:cinclude>
				<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
					<td width="250">
				</ffi:cinclude>
				<ffi:removeProperty name="highLight" />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="currComponentName"/></td>
			<td width="475">&nbsp;</td>
			<td width="25">
			<% session.setAttribute("count", String.valueOf(count)); %>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
				<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
					onClick="ns.admin.gotoPermission('perAccountTab',<ffi:getProperty name="count"/>)">
			        View
		        </a>
				<%-- 
				<ffi:link url="peraccountpermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
					<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
				</ffi:link>
				--%>
			</ffi:cinclude>
			<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
				<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
					onClick="ns.admin.gotoPermission('perAccountTab',<ffi:getProperty name="count"/>)">
			        <s:text name="jsp.default_178" />
		        </a>
				<%-- 
				<ffi:link url="peraccountpermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
					<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
				</ffi:link>
				--%>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
			<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
				onClick="ns.admin.gotoPermission('perAccountTab',<ffi:getProperty name="count"/>)">
		        View
	        </a>
			<%-- 
			<ffi:link url="peraccountpermissions.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
				<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
			</ffi:link>
			--%>
		</ffi:cinclude>
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
			<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
				onClick="ns.admin.gotoPermission('perAccountTab',<ffi:getProperty name="count"/>)">
		        <s:text name="jsp.default_178" />
	        </a>
			<%-- 
			<ffi:link url="peraccountpermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
				<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
			</ffi:link>
			--%>
		</ffi:cinclude>
		</ffi:cinclude>
			</td>
		</tr>
		
		<%  count++;  %>
		<%  band++;  %>
	</ffi:list>
</ffi:cinclude>
	
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals" >
 <% int pcount=0; %>
	<ffi:list collection="LocalizedComponentNamesPer" items="currComponentName">
		<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
			<td id="perAccountAccessLimit<%= pcount%>" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="currComponentName"/></td>
			<td width="475">&nbsp;</td>
			<td width="25">
			<% session.setAttribute("count", String.valueOf(count)); %>
			<a id="editPerAchCompanyAccess<%= pcount++%>" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
				onClick="ns.admin.gotoPermission('perAccountTab',<ffi:getProperty name="count"/>)">
		        <s:text name="jsp.default_178" />
	        </a>
			<%-- 
			<ffi:link url="peraccountpermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
				<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
			</ffi:link>
			--%>
			</td>
		</tr>
		<%  count++;  %>
		<%  band++;  %>
	</ffi:list>
</ffi:cinclude>
	
	<ffi:removeProperty name="currComponentName"/>
	<ffi:removeProperty name="LocalizedComponentNamesPer"/>

 	<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
		<td id="perGroupAccountAccessLimit" width="250"><s:text name="jsp.admin.company.feature.access.per.account.group" /></td>
		<td id="perGroupAccountAccessLimitDesc" width="475"><s:text name="jsp.admin.company.grant.particular.account.groups" /></td>
		<td width="25">
			<!-- <img src="/cb/web/multilang/grafx/user/spacer.gif" alt="Edit" hspace="3" border="0" height="14" width="19"> -->
		</td>
	</tr>
	<%	band++;	%>
	<s:include value="%{#session.PagesPath}user/inc/checkentitled-peraccountgroupcomponents.jsp" />
	<%  count = 0;  %>

<!-- Dual approval highlighting for per account group-->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:list collection="LocalizedComponentNamesPer" items="currComponentName">
		<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
				<ffi:list collection="categories" items="perAccountGrpCategory">
						<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
						<ffi:setProperty name="GetDACategorySubType" property="DaItemCategoryName" value="perAccountGrpCategory"/>
						<ffi:setProperty name="GetDACategorySubType" property="ComponentName" value="${currComponentName}"/>
						<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
										value="<%=com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>" />
						<ffi:process name="GetDACategorySubType"/>
						<ffi:removeProperty name="GetDACategorySubType"/>
						<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
						<ffi:cinclude value1="${catSubType}" value2="" operator="notEquals">
							<ffi:setProperty name="highLight" value="true" />
						</ffi:cinclude>
						<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
						<ffi:removeProperty name="catSubType"/>
				</ffi:list>
				<ffi:cinclude value1="${highLight}" value2="true">
					<td width="250" class="columndataDA ">
				</ffi:cinclude>
				<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
					<td width="250">
				</ffi:cinclude>
				<ffi:removeProperty name="highLight" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="currComponentName"/></td>
			<td width="475">&nbsp;</td>
			<td width="25">
			<% session.setAttribute("count", String.valueOf(count)); %>
			<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
				<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
					<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
						onClick="ns.admin.gotoPermission('perAccountGroupTab',<ffi:getProperty name="count"/>)">
				        View
			        </a>
					<%-- 
					<ffi:link url="peraccountgrouppermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
						<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
					</ffi:link>
					--%>
				</ffi:cinclude>
				<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
					<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
						onClick="ns.admin.gotoPermission('perAccountGroupTab',<ffi:getProperty name="count"/>)">
				        <s:text name="jsp.default_178" />
			        </a>
					<%-- 
					<ffi:link url="peraccountgrouppermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
						<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
					</ffi:link>
					--%>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
				<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
					<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
						onClick="ns.admin.gotoPermission('perAccountGroupTab',<ffi:getProperty name="count"/>)">
				        View
			        </a>
					<%-- 
					<ffi:link url="peraccountgrouppermissions.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
						<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
					</ffi:link>
					--%>
				</ffi:cinclude>
				<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
					<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
						onClick="ns.admin.gotoPermission('perAccountGroupTab',<ffi:getProperty name="count"/>)">
				        <s:text name="jsp.default_178" />
			        </a>
					<%-- 
					<ffi:link url="peraccountgrouppermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
						<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
					</ffi:link>
					--%>
				</ffi:cinclude>
			</ffi:cinclude>
			</td>
		</tr>
		<%  count++;  %>
		<%  band++;  %>
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
    <% int pgcount=0;%>
	<ffi:list collection="LocalizedComponentNamesPer" items="currComponentName">
		<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
			<td id="perGroupAccountAccessLimit<%=pgcount%>" width="250">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="currComponentName"/></td>
			<td width="475">&nbsp;</td>
			<td width="25">
			<% session.setAttribute("count", String.valueOf(count)); %>
			<a id="editPerGroupAccountAccessLimit<%=pgcount++%>" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
				onClick="ns.admin.gotoPermission('perAccountGroupTab',<ffi:getProperty name="count"/>)">
		        <s:text name="jsp.default_178" />
	        </a>
			<%-- 
			<ffi:link url="peraccountgrouppermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&ComponentIndex=${count}">
				<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
			</ffi:link>
			--%>
			</td>
		</tr>
		<%  count++;  %>
		<%  band++;  %>
	</ffi:list>
</ffi:cinclude>
	<ffi:removeProperty name="currComponentName"/>
	<ffi:removeProperty name="LocalizedComponentNamesPer"/>

	<s:include value="%{#session.PagesPath}user/inc/checkentitledlocations.jsp" />
	<ffi:cinclude value1="${CheckEntitlementByGroupCB.Entitled}" value2="TRUE" operator="equals">
	   <ffi:cinclude value1="${displayPerLocation}" value2="TRUE" operator="equals">
		<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
			<td id="perLocAccessLimit" width="250">

<!-- Dual approval highlighting for per location-->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:list collection="categories" items="category">
		<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_PER_LOCATION %>" operator="equals">
				<ffi:setProperty name="highLight" value="true" />
		</ffi:cinclude>
	</ffi:list>
	<ffi:cinclude value1="${highLight}" value2="true">
		<span class="columndataDA">Feature Access and Limits (per location)</span>
	</ffi:cinclude>
	<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
		<s:text name="jsp.admin.company.per.locations" />
	</ffi:cinclude>
	<ffi:removeProperty name="highLight" />
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<s:text name="jsp.admin.company.per.locations" />
</ffi:cinclude>
			</td>
			<td id="perLocAccessLimitDesc" width="475"><s:text name="jsp.admin.company.grant.particular.locations" /></td>
			<td width="25">
		<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
			<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
				<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
					onClick="ns.admin.gotoPermission('perLocationTab')">
			        View
		        </a>
				<%-- 
				<ffi:link url="perlocationpermissions-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
					<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
				</ffi:link>
				--%>
			</ffi:cinclude>
			<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
				<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
					onClick="ns.admin.gotoPermission('perLocationTab')">
			        <s:text name="jsp.default_178" />
		        </a>
				<%-- 
				<ffi:link url="perlocationpermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&DivisionId=">
					<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
				</ffi:link>
				--%>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
				<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
					onClick="ns.admin.gotoPermission('perLocationTab')">
			        View
		        </a>
				<%-- 
				<ffi:link url="perlocationpermissions-view.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&DivisionId=">
					<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
				</ffi:link>
				--%>
			</ffi:cinclude>
			<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
				<a id="editPerLocAccessLimitDesc" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
					onClick="ns.admin.gotoPermission('perLocationTab')">
			        <s:text name="jsp.default_178" />
		        </a>
				<%-- 
				<ffi:link url="perlocationpermissions.jsp?Section=${Section}&runSearch=&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}&DivisionId=">
					<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
				</ffi:link>
				--%>
			</ffi:cinclude>
		</ffi:cinclude>
			</td>
		</tr>
		<%	band++;	%>
	   </ffi:cinclude>
	</ffi:cinclude>	
<ffi:removeProperty name="CheckEntitlementByGroupCB"/>
	
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>">
		
	<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
<td id="wireAccessLimit" width="250">
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">	
		<ffi:list collection="categories" items="category">
			<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_WIRE_TEMPLATE_ACCESS_LIMITS %>" operator="equals">
					<ffi:setProperty name="highLight" value="true" />
			</ffi:cinclude>
		</ffi:list>
		<ffi:cinclude value1="${highLight}" value2="true">
			<span class="columndataDA"><s:text name="jsp.admin.company.wire.templates.limits" /></span>
		</ffi:cinclude>
		<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
			<s:text name="jsp.admin.company.wire.templates.limits" />
		</ffi:cinclude>
		<ffi:removeProperty name="highLight" />
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<s:text name="jsp.admin.company.wire.templates.limits" />
</ffi:cinclude>
</td>
		<td id="wireAccessLimitDesc" width="475"><s:text name="jsp.admin.company.grant.restrict.wire" /></td>
		<td width="25">
	<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
			<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
				onClick="ns.admin.gotoPermission('wireTemplateTab')">
		        View
	        </a>
			<%-- 
			<ffi:link url="wiretemplatesaccess-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
			</ffi:link>
			--%>
		</ffi:cinclude>
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
			<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
				onClick="ns.admin.gotoPermission('wireTemplateTab')">
		        <s:text name="jsp.default_178" />
	        </a>
			<%-- 
			<ffi:link url="wiretemplatesaccess.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
			</ffi:link>
			--%>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:cinclude value1="${viewOnly}" value2="true" operator="equals">
			<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
				onClick="ns.admin.gotoPermission('wireTemplateTab')">
		        View
	        </a>
			<%-- 
			<ffi:link url="wiretemplatesaccess-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
			</ffi:link>
			--%>
		</ffi:cinclude>
		<ffi:cinclude value1="${viewOnly}" value2="true" operator="notEquals">
			<a id="editWireAccessLimit" class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
				onClick="ns.admin.gotoPermission('wireTemplateTab')">
		        <s:text name="jsp.default_178" />
	        </a>
			<%-- 
			<ffi:link url="wiretemplatesaccess.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
			</ffi:link>
			--%>
		</ffi:cinclude>
	</ffi:cinclude>
		</td>
	</tr>
	<%	band++;	%>
	</ffi:cinclude>
	</s:if>
	
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>">

	<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
		<ffi:setProperty name="GetDACategorySubType" property="PerCategory" 
										value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT %>" />
		<ffi:setProperty name="GetDACategorySubType" property="OperationName" 
																	value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>" />
	<ffi:process name="GetDACategorySubType"/>
	<ffi:removeProperty name="GetDACategorySubType"/>
	<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" assignTo="catSubType"/>
	
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMemberCB" id="CheckEntitlementByMember" scope="session"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="GroupId" value="${ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE.EntitlementGroupId}"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="MemberId" value="${ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE.Id}"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="MemberType" value="${ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE.MemberType}"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="MemberSubType" value="${ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE.MemberSubType}"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="AttributeName" value="IsEntitledForWires"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="DaCategorySessionName" value="EntitlementBean"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="DaCategorySubType" value="${catSubType}"/>
   	<ffi:process name="CheckEntitlementByMember"/>   	
	<ffi:removeProperty name="catSubType"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"/>
	    	
   	<ffi:cinclude value1="${IsEntitledForWires}" value2="TRUE" operator="equals">

	<tr class="columndata <%= ( band % 2 == 0 ) ? "ui-state-zebra" : "ltrow3" %>">
<td width="250">
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">	
		<ffi:list collection="categories" items="category">
			<ffi:cinclude value1="${category.categorySubType}" value2="<%=IDualApprovalConstants.CATEGORY_SUB_WIRE_TEMPLATE_ACCESS_LIMITS %>" operator="equals">
					<ffi:setProperty name="highLight" value="true" />
			</ffi:cinclude>
		</ffi:list>
		<ffi:cinclude value1="${highLight}" value2="true">
			<span class="columndataDA">Wire Template Access and Limits</span>
		</ffi:cinclude>
		<ffi:cinclude value1="${highLight}" value2="true" operator="notEquals">
			Wire Template Access and Limits
		</ffi:cinclude>
		<ffi:removeProperty name="highLight" />
	</s:if>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			Wire Template Access and Limits
	</s:if>
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			Wire Template Access and Limits
</ffi:cinclude>
</td>
		<td width="475">Grant and restrict access to wire templates that are defined as Bank Templates or Business Templates</td>
		<td width="25">
	<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
			<a class='ui-button ui-state-default ui-corner-all' title='<s:text name="jsp.default_460" />' href='#' 
				onClick="ns.admin.gotoPermission('wireTemplateTab')">
		        View
	        </a>
			<%-- 
			<ffi:link url="wiretemplatesaccess-view.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_view.gif" title="View" border="0" hspace="3">
			</ffi:link>
			--%>
		</ffi:cinclude>
		<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
			<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
				onClick="ns.admin.gotoPermission('wireTemplateTab')">
		        <s:text name="jsp.default_178" />
	        </a>
			<%-- 
			<ffi:link url="wiretemplatesaccess.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
				<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
			</ffi:link>
			--%>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' 
			onClick="ns.admin.gotoPermission('wireTemplateTab')">
	        <s:text name="jsp.default_178" />
        </a>
		<%-- 
		<ffi:link url="wiretemplatesaccess.jsp?runSearch=&Section=${Section}&EditGroup_GroupId=${EditGroup_GroupId}&SetBusinessEmployee.Id=${BusinessEmployeeId}">
			<img src="/cb/web/multilang/grafx/user/i_edit.gif" alt="Edit" hspace="3" border="0">
		</ffi:link>
		--%>
	</ffi:cinclude>
		</td>
	</tr>
	<%	band++;	%>
	</ffi:cinclude>
	</ffi:cinclude>
	</s:if>
</table>
</div>
<script type="text/javascript">
	/* $('#permmenu_menus').pane({
		title: 'Permissions',
		minmax: true,
		close: false
	}); */
</script>




<ffi:removeProperty name="DoneURL" />
<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
	<ffi:setProperty name="DoneURL" value="${SecurePath}user/corpadmindiv.jsp" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<ffi:setProperty name="DoneURL" value="${SecurePath}user/corpadmingroups.jsp" URLEncrypt="true"/>
</ffi:cinclude>
<s:if test="%{#session.Section == 'Users'}">
	<ffi:setProperty name="DoneURL" value="${SecurePath}user/corpadminusers.jsp" URLEncrypt="true"/>
</s:if>
<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
	<ffi:setProperty name="DoneURL" value="${SecurePath}user/corpadminprofiles.jsp" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td>&nbsp;</td> </tr>
<tr>
	<td  colspan="7" align="center"><input class="submitbutton" type="button" value="Done" onclick="location.href='<ffi:getProperty name="DoneURL" />'"> 	</td>
</tr>
</table>
</ffi:cinclude>
<ffi:removeProperty name="categories"/>
<ffi:removeProperty name="ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE"/>
<ffi:removeProperty name="IsEntitledForWires"/>
<ffi:removeProperty name="EntitlementBean"/>
<ffi:removeProperty name="DoneURL" />
</div>
</div>