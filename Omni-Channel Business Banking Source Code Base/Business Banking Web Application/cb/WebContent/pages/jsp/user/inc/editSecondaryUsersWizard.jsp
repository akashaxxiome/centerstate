<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	String editedSecondaryUserId = request.getParameter("SecondaryUserID");
	session.setAttribute("SecondaryUserID",editedSecondaryUserId);
%>

	




<s:include value="/pages/jsp/user/user_edit_init.jsp" />

	<div id="addEditSecondaryUsersPortlet" class="bigWizardSupportCls portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
        <div class="portlet-header ui-widget-header ui-corner-all">
        	<span class="portlet-title"><s:text name="secondaryUser.addEditSecondaryUsers"/></span>
		</div>
        <div class="portlet-content">
		<div id="addEditSecondaryUsersTabs">
			<ul>
				<li><a href="#personalInformationContainer"><s:text name="secondaryUser.wizardTab.tab0.title"/></a></li>
				<li><a href="#accountSetupContainer"><s:text name="secondaryUser.wizardTab.tab1.title"/></a></li>
				<li><a href="#transferSetupContainer"><s:text name="secondaryUser.wizardTab.tab2.title"/></a></li>
				<li><a href="#exTransferSetupContainer"><s:text name="secondaryUser.wizardTab.tab3.title"/></a></li>
				<li><a href="#billPaymentSetupContainer"><s:text name="secondaryUser.wizardTab.tab4.title"/></a></li>
			</ul>
			<div id="personalInformationContainer">
				<p><s:include value="/pages/jsp/user/inc/user_edit_info.jsp" /></p>
			</div>
			<div id="accountSetupContainer">
				<p>accountSetupContainer</p>
			</div>
			<div id="transferSetupContainer">
				<p>transferSetupContainer</p>		
			</div>
			<div id="exTransferSetupContainer">
				<p>exTransferSetupContainer</p>		
			</div>
			<div id="billPaymentSetupContainer">
				<p>billPaymentSetupContainer</p>		
			</div>
		</div>
	</div>
	<div id="addEditSecondaryUsersDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('addEditSecondaryUsersPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
  </div>

  <script>    
 /*  $(document).ready(function(){
		$('#addEditSecondaryUsersPortlet').portlet({
			close: true
		});
  }); */
  ns.common.initializePortlet("addEditSecondaryUsersPortlet", false);
</script>