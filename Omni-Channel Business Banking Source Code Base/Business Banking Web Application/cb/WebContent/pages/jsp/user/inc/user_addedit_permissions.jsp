<%--
| The purpose of this page is to generate the permissions table for the secondary user internal transfers, external
| transfers, and bill payment administration pages. Inputs that this page expects are:
|
|       - the title to be displayed in the header of the table set in the session variable called "PermissionsTitle"
|       - the operation display name to be included in the "Enable ... From This Account" and
|         "Enable ... To This Account" column headers in the session variable called "EnableOperationDisplayValue"
|       - the session variable called "IncludeFromSection" set to "TRUE" if and only if the "From ..." section should
|         be rendered
|       - the session variable called "IncludeToSection" set to "TRUE" if and only if the "To ..." section should be
|         rendered
|       - the session variable called "AccountsCollectionNames" set to a StringList of session names of Accounts
|         collections to which the corresponding collections of accounts the user can be granted access to for the
|         purposes of initiating transactions of the given operation type(s) is bound
|       - the session variable called "FromOperationName" set to the entitlement operation to include in the names of
|         the form input elements in the "From ..." section
|       - the session variable called "ToOperationName" set to the entitlement operation to include in the names of the
|         form input elements in the "To ..." section
|       - the session variable called "AccountsFromFilters" set to a table of filters to be applied against each
|         individual Accounts bean. Each account in a particular collection must satisfy the corresponding filter in
|         order to be included in the list of candidate accounts in the "From ..." section of the page. The key of each
|         pair in the table is the session name of the collection to which the value, i.e., the filter, will be applied.
|       - the session variable called "AccountsToFilters" set to a table of filters to be applied against each
|         individual Accounts bean. Each account in a particular collection must satisfy the corresponding filter in
|         order to be included in the list of candidate accounts in the "To ..." section of the page. The key of each
|         pair in the table is the session name of the collection to which the value, i.e., the filter, will be applied.
|       - the session variable called "FeatureAccessAndLimitsTaskName" set to the session name of the
|         FeatureAccessAndLimits task used to process and persist the permissions settings
--%>


<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>' startsWith="true"/>
<ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_MAX_LIMIT_PREFIX %>' startsWith="true"/>

<%
String IsObjectEntitled = null;
String ObjectLimitValue = null;
String ErrorMessage = null;
String ErrorsDetected = null;
%>

<%-- Set variables to display records in alternating colors. --%>
<ffi:setProperty name="pband1" value="class=\"tabledata_white\""/>
<ffi:setProperty name="pband2" value="class=\"tabledata_gray\""/>

<%-- Create the task used to generate entitlement object identifiers for accounts. --%>
<ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetAccountEntitlementObjectID" scope="request"/>

<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="Reset" value=""/>
<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddLimitPeriodToList" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
<ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddLimitPeriodToList" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>

<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroup" id="CheckParentEntitlements" scope="request"/>
<ffi:setProperty name="CheckParentEntitlements" property="AttributeName" value="IsParentEntitled"/>

<ffi:removeProperty name="Errors Detected"/>
<ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="HasDetectedRedundantLimits" assignTo="ErrorsDetected"/>
<div class="marginTop10"></div>
<ffi:cinclude value1="${ErrorsDetected}" value2="true" operator="equals">
                    <div style="padding-left: 10px; padding-right: 10px;" class="limitCheckError">
                        Error: One or more of the limits you are attempting to create is less restrictive than existing limits and/or other limits that you have specified. The invalid limits in question are indicated below and must be reduced or removed.
                    </div>
</ffi:cinclude>
<div class="marginTop10"></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="paneContentWrapper">
   			<div class="nameSubTitle"><ffi:getProperty name="PermissionsTitle"/></div>
                    <table border="0" cellpadding="2" cellspacing="0" class="tableData tdTopBottomPadding5 marginTop10" width="100%">
<ffi:cinclude value1="${IncludeFromSection}" value2="TRUE" operator="equals">
                        <tr class="header">
                            <td><div align="center"><ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_addedit_permissions.jsp-1" parm0="${EnableOperationDisplayValue}"/></div></td>
                            <td style="padding-left: 20px; padding-right: 20px;">Account</td>
                            <td colspan="2">Limit Per Transaction</td>
                            <td colspan="2">Limit Per Day</td>
                        </tr>
    <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Calculator" scope="request"/>
    <ffi:setProperty name="Calculator" property="Value1" value="3"/>
    <ffi:setProperty name="Calculator" property="Value2" value="1"/>

    <ffi:setProperty name="HasAccount" value=""/>

    <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementOperationName" value="${FromOperationName}"/>

    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="OperationName" value="${FromOperationName}"/>
    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>

    <ffi:list collection="AccountsCollectionNames" items="AccountsCollectionName">
        <ffi:setProperty name="AccountsFromFilters" property="Key" value="${AccountsCollectionName}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="Filter" value="${AccountsFromFilters.Value}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="FilterSortedBy" value="ConsumerDisplayText,ID"/>
        <ffi:list collection="${AccountsCollectionName}" items="TempAccount">
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.ID}"/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.RoutingNum}"/>
            <ffi:process name="GetAccountEntitlementObjectID"/>

            <ffi:setProperty name="CheckParentEntitlements" property="GroupId" value="${SecureUser.EntitlementID}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
            <ffi:setProperty name="CheckParentEntitlements" property="OperationName" value="${FromOperationName}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectId" value="${GetAccountEntitlementObjectID.ObjectID}"/>
            <ffi:process name="CheckParentEntitlements"/>

            <%-- QTS 756842. If primary user is not entitled to account setup, don't show accounts. --%>
			<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_SETUP %>' checkParent="TRUE">
				<ffi:setProperty name="IsParentEntitled" value="FALSE"/>
			</ffi:cinclude>
			
            <ffi:cinclude value1="${IsParentEntitled}" value2="TRUE" operator="equals">
                <ffi:setProperty name="HasAccount" value="true"/>

                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddObjectIDToList" value='${GetAccountEntitlementObjectID.ObjectID}'/>

                <ffi:setProperty name="SessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>'/>
                <ffi:setProperty name="FAALMLSessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_MAX_LIMIT_PREFIX %>'/>
                <ffi:setProperty name="AccountEntitlementObject" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
                <ffi:setProperty name="FAALBaseName" value="${SessionPrefix}!${FromOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:setProperty name="FAALMLBaseName" value="${FAALMLSessionPrefix}!${FromOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>

                        <tr style="padding: 0;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:removeProperty name="IsObjectEntitled"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="IsEntitledObject" assignTo="IsObjectEntitled"/>
                            <td><div align="center"><input name='<ffi:getProperty name="FAALBaseName"/>' enabledisable="true" type="checkbox" value="TRUE" <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="equals">checked</ffi:cinclude>></div></td>
                            <td style="padding-left: 20px; padding-right: 20px;"><ffi:getProperty name="TempAccount" property="ConsumerDisplayText"/></td>
                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                            <td><input inputfor='<ffi:getProperty name="FAALBaseName"/>' name='<ffi:getProperty name="FAALBaseName"/>!<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>' type="text" class="txtbox ui-widget-content ui-corner-all" size="15" maxlength="15" value="<ffi:getProperty name='ObjectLimitValue'/>" <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="notEquals">disabled</ffi:cinclude>></td>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="ObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="notEquals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value="${GetSecondaryUserMaxLimits.MaxLimit}"/>
                            <td style="padding-left: 5px;"><ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_addedit_permissions.jsp-3" parm0="${GetSecondaryUserMaxLimits.MaxLimit}"/></td>
                </ffi:cinclude>
                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="equals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value=""/>
                            <td style="padding-left: 5px;">(no maximum)</td>
                </ffi:cinclude>
                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                            <td><input inputfor='<ffi:getProperty name="FAALBaseName"/>' name='<ffi:getProperty name="FAALBaseName"/>!<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>' type="text" class="txtbox ui-widget-content ui-corner-all" size="15" maxlength="15" value="<ffi:getProperty name='ObjectLimitValue'/>" <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="notEquals">disabled</ffi:cinclude>></td>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="ObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="notEquals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value="${GetSecondaryUserMaxLimits.MaxLimit}"/>
                            <td style="padding-left: 5px;"><ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_addedit_permissions.jsp-3" parm0="${GetSecondaryUserMaxLimits.MaxLimit}"/></td>
                </ffi:cinclude>
                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="equals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value=""/>
                            <td style="padding-left: 5px;">(no maximum)</td>
                </ffi:cinclude>
                        </tr>

                <ffi:object name="com.ffusion.beans.util.StringList" id="ErrorMessages" scope="request"/>
                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALMLBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALMLBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:cinclude value1="${ErrorMessages.Size}" value2="0" operator="notEquals">
                        <tr style="padding: 0px;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                            <td></td>
                            <td colspan="5" style="padding-left: 20px; padding-right: 20px;">
                                <div class="limitCheckError">
                                    <br>
                    <ffi:list collection="ErrorMessages" items="TempErrorMessage">
                                    <ffi:getProperty name="TempErrorMessage"/><br>
                    </ffi:list>
                                    <br>
                                </div>
                            </td>
                        </tr>
                </ffi:cinclude>

                <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
            </ffi:cinclude>
        </ffi:list>
    </ffi:list>
    <ffi:cinclude value1="${HasAccount}" value2="" operator="equals">
                        <tr style="padding: 0px;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                            <td colspan="6"><div style="text-align: center"><ffi:getProperty name="NoAccountsMessage"/></div></td>
                        </tr>
    </ffi:cinclude>
                        <!--
                        <tr style="padding: 0;" class="tabledata_white">
                            <td><div align="center"><input name="" type="checkbox"></div></td>
                            <td style="padding-left: 20px; padding-right: 20px;">Checking Very Very Very Long Nickname xxx-xx063-0</td>
                            <td><input name="" type="text" class="form_fields" size="15" maxlength="15"></td>
                            <td style="padding-left: 5px;">($5,000 Maximum)</td>
                            <td><input name="" type="text" class="form_fields" size="15" maxlength="15"></td>
                            <td style="padding-left: 5px;">($12,100,000 Maximum)</td>
                        </tr>
                        -->
</ffi:cinclude>
<ffi:cinclude value1="${IncludeFromSection}" value2="TRUE" operator="equals">
    <ffi:cinclude value1="${IncludeToSection}" value2="TRUE" operator="equals">
                        <tr>
                            <td colspan="6"><img src="/efs/efs/multilang/grafx/spacer.gif" width="1" height="20"></td>
                        </tr>
    </ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${IncludeToSection}" value2="TRUE" operator="equals">
                        <tr class="header">
                            <td><div align="center"><ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_addedit_permissions.jsp-2" parm0="${EnableOperationDisplayValue}"/></div></td>
                            <td style="padding-left: 20px; padding-right: 20px;">Account</td>
                            <td colspan="2">Limit Per Transaction</td>
                            <td colspan="2">Limit Per Day</td>
                        </tr>
    <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Calculator" scope="request"/>
    <ffi:setProperty name="Calculator" property="Value1" value="3"/>
    <ffi:setProperty name="Calculator" property="Value2" value="1"/>

    <ffi:setProperty name="HasAccount" value=""/>

    <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementOperationName" value="${ToOperationName}"/>

    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="AllowApproval" value="FALSE"/>
    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="OperationName" value="${ToOperationName}"/>
    <ffi:setProperty name="GetSecondaryUserMaxLimits" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>

    <ffi:list collection="AccountsCollectionNames" items="AccountsCollectionName">
        <ffi:setProperty name="AccountsToFilters" property="Key" value="${AccountsCollectionName}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="Filter" value="${AccountsToFilters.Value}"/>
        <ffi:setProperty name="${AccountsCollectionName}" property="FilterSortedBy" value="ConsumerDisplayText,ID"/>
        <ffi:list collection="${AccountsCollectionName}" items="TempAccount">
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.ID}"/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>'/>
            <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${TempAccount.RoutingNum}"/>
            <ffi:process name="GetAccountEntitlementObjectID"/>

            <ffi:setProperty name="CheckParentEntitlements" property="GroupId" value="${SecureUser.EntitlementID}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectType" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
            <ffi:setProperty name="CheckParentEntitlements" property="OperationName" value="${ToOperationName}"/>
            <ffi:setProperty name="CheckParentEntitlements" property="ObjectId" value="${GetAccountEntitlementObjectID.ObjectID}"/>
            <ffi:process name="CheckParentEntitlements"/>

            <%-- QTS 756842. If primary user is not entitled to account setup, don't show accounts. --%>
			<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_SETUP %>' checkParent="TRUE">
				<ffi:setProperty name="IsParentEntitled" value="FALSE"/>
			</ffi:cinclude>
			
            <ffi:cinclude value1="${IsParentEntitled}" value2="TRUE" operator="equals">
                <ffi:setProperty name="HasAccount" value="true"/>

                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="AddObjectIDToList" value='${GetAccountEntitlementObjectID.ObjectID}'/>

                <ffi:setProperty name="SessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_PREFIX %>'/>
                <ffi:setProperty name="FAALMLSessionPrefix" value='<%= com.ffusion.tasks.multiuser.FeatureAccessAndLimits.SESSION_MAX_LIMIT_PREFIX %>'/>
                <ffi:setProperty name="AccountEntitlementObject" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.ACCOUNT %>'/>
                <ffi:setProperty name="FAALBaseName" value="${SessionPrefix}!${ToOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:setProperty name="FAALMLBaseName" value="${FAALMLSessionPrefix}!${ToOperationName}!${AccountEntitlementObject}!${GetAccountEntitlementObjectID.ObjectID}"/>
                        <tr style="padding: 0;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="EntitlementObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:removeProperty name="IsObjectEntitled"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="IsEntitledObject" assignTo="IsObjectEntitled"/>
                            <td><div align="center"><input name='<ffi:getProperty name="FAALBaseName"/>' enabledisable="true" type="checkbox" value="TRUE"  <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="equals">checked</ffi:cinclude>></div></td>
                            <td style="padding-left: 20px; padding-right: 20px;"><ffi:getProperty name="TempAccount" property="ConsumerDisplayText"/></td>
                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                            <td><input inputfor='<ffi:getProperty name="FAALBaseName"/>' name='<ffi:getProperty name="FAALBaseName"/>!<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>' type="text" class="txtbox ui-widget-content ui-corner-all" size="15" maxlength="15" value="<ffi:getProperty name='ObjectLimitValue'/>" <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="notEquals">disabled</ffi:cinclude>></td>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="ObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="notEquals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value="${GetSecondaryUserMaxLimits.MaxLimit}"/>
                            <td style="padding-left: 5px;"><ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_addedit_permissions.jsp-3" parm0="${GetSecondaryUserMaxLimits.MaxLimit}"/></td>
                </ffi:cinclude>
                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="equals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value=""/>
                            <td style="padding-left: 5px;">(no maximum)</td>
                </ffi:cinclude>
                <ffi:removeProperty name="ObjectLimitValue"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="LimitPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="ObjectLimit" assignTo="ObjectLimitValue"/>
                            <td><input inputfor='<ffi:getProperty name="FAALBaseName"/>' name='<ffi:getProperty name="FAALBaseName"/>!<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>' type="text" class="txtbox ui-widget-content ui-corner-all" size="15" maxlength="15" value="<ffi:getProperty name='ObjectLimitValue'/>" <ffi:cinclude value1="${IsObjectEntitled}" value2="true" operator="notEquals">disabled</ffi:cinclude>></td>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="ObjectID" value="${GetAccountEntitlementObjectID.ObjectID}"/>
                <ffi:setProperty name="GetSecondaryUserMaxLimits" property="Period" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="notEquals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value="${GetSecondaryUserMaxLimits.MaxLimit}"/>
                            <td style="padding-left: 5px;"><ffi:getL10NString rsrcFile="efs" msgKey="jsp/user/inc/user_addedit_permissions.jsp-3" parm0="${GetSecondaryUserMaxLimits.MaxLimit}"/></td>
                </ffi:cinclude>
                <ffi:cinclude value1="${GetSecondaryUserMaxLimits.MaxLimit}" value2="" operator="equals">
                    <ffi:setProperty name="${FAALMLBaseName}!${TempPeriod}" value=""/>
                            <td style="padding-left: 5px;">(no maximum)</td>
                </ffi:cinclude>
                        </tr>

                <ffi:object name="com.ffusion.beans.util.StringList" id="ErrorMessages" scope="request"/>
                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALMLBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:setProperty name="TempPeriod" value='<%= "" + com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY %>'/>
                <ffi:removeProperty name="ErrorMessage"/>
                <ffi:setProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessageKey" value="${FAALMLBaseName}!${TempPeriod}"/>
                <ffi:getProperty name="${FeatureAccessAndLimitsTaskName}" property="RedundantLimitErrorMessage" assignTo="ErrorMessage"/>
                <ffi:cinclude value1="${ErrorMessage}" value2="" operator="notEquals">
                    <ffi:setProperty name="ErrorMessages" property="Add" value="${ErrorMessage}"/>
                </ffi:cinclude>

                <ffi:cinclude value1="${ErrorMessages.Size}" value2="0" operator="notEquals">
                        <tr style="padding: 0px;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                            <td></td>
                            <td colspan="5" style="padding-left: 20px; padding-right: 20px;">
                                <div class="limitCheckError">
                                    <br>
                    <ffi:list collection="ErrorMessages" items="TempErrorMessage">
                                    <ffi:getProperty name="TempErrorMessage"/><br>
                    </ffi:list>
                                    <br>
                                </div>
                            </td>
                        </tr>
                </ffi:cinclude>

                <ffi:setProperty name="Calculator" property="Value2" value="${Calculator.Subtract}"/>
            </ffi:cinclude>
        </ffi:list>
    </ffi:list>
    <ffi:cinclude value1="${HasAccount}" value2="" operator="equals">
                        <tr style="padding: 0px;" <ffi:getProperty name="pband${Calculator.Value2}" encode="false"/>>
                            <td colspan="6"><div style="text-align: center"><ffi:getProperty name="NoAccountsMessage"/></div></td>
                        </tr>
    </ffi:cinclude>
</ffi:cinclude>
                    </table>
   		</div>
   	</div>
</div>
        

<ffi:removeProperty name="SessionPrefix"/>
<ffi:removeProperty name="FAALMLSessionPrefix"/>
<ffi:removeProperty name="AccountEntitlementObject"/>
<ffi:removeProperty name="FAALBaseName"/>
<ffi:removeProperty name="FAALMLBaseName"/>
<ffi:removeProperty name="IsObjectEntitled"/>
<ffi:removeProperty name="ObjectLimitValue"/>
<ffi:removeProperty name="ErrorMessage"/>
<ffi:removeProperty name="Errors Detected"/>
<ffi:removeProperty name="TempPeriod"/>
<ffi:removeProperty name="HasAccount"/>
<ffi:removeProperty name="IsParentEntitled"/>
