<%@page import="com.ffusion.csil.beans.entitlements.Channel"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="IsValidGroupDescendant" name="com.ffusion.efs.tasks.entitlements.IsValidGroupDescendant" scope="request"/>
<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>

<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:setProperty name="GetBusinessEmployee" property="SearchEntitlementProfile" value="TRUE"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>

<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${EditGroup_GroupId}"/>
	<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>'/>
	<ffi:process name="GetEntitlementGroup"/>
</s:if>

<s:if test="%{#session.Section == 'Users'}">
	<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${BusinessEmployeeId}"/>
	<ffi:setProperty name="GetBusinessEmployee" property="SearchEntitlementProfile" value="TRUE"/>
	<ffi:process name="GetBusinessEmployee"/>

	<ffi:setProperty name="EditGroup_GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_576')}" scope="request" />
	<ffi:setProperty name="Context" value="${BusinessEmployee.FullName} (${tmpI18nStr})"/>
		<ffi:cinclude value1="3" value2="${BusinessEmployee.CustomerType}" operator="equals">
			<ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Profile)"/>
		</ffi:cinclude>
		<ffi:cinclude value1="4" value2="${BusinessEmployee.CustomerType}" operator="equals">
			<ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Profile)"/>
		</ffi:cinclude>
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminusers.jsp"/>
</s:if>
<s:if test="%{#session.Section == 'Profiles'}">
	<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${BusinessEmployeeId}"/>
	<ffi:setProperty name="GetBusinessEmployee" property="SearchEntitlementProfile" value="TRUE"/>
	<ffi:process name="GetBusinessEmployee"/>

	<ffi:setProperty name="EditGroup_GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	 <ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Profile)"/>
		<ffi:cinclude value1="3" value2="${BusinessEmployee.CustomerType}" operator="equals">
			<ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Profile)"/>
		</ffi:cinclude>
		<ffi:cinclude value1="true" value2="${BusinessEmployee.usingEntProfiles}" operator="equals">
			<ffi:cinclude value1="4" value2="${BusinessEmployee.CustomerType}" operator="equals">
				<ffi:cinclude value1="${ChannelId}" value2="<%=Channel.X_CHANNEL%>" operator="equals">
					<ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Profile)"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${ChannelId}" value2="<%=Channel.X_CHANNEL%>" operator="notEquals">
					<ffi:setProperty name="Context" value="${ParentProfileName} (${ChannelId})"/>
				</ffi:cinclude>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="false" value2="${BusinessEmployee.usingEntProfiles}" operator="equals">
			<ffi:cinclude value1="4" value2="${BusinessEmployee.CustomerType}" operator="equals">
				<ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Profile)"/>
			</ffi:cinclude>
		</ffi:cinclude>
		
		<ffi:cinclude value1="5" value2="${BusinessEmployee.CustomerType}" operator="equals">
			<ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Primary Administrator Profile)"/>
		</ffi:cinclude>
		<ffi:cinclude value1="6" value2="${BusinessEmployee.CustomerType}" operator="equals">
			<ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Secondary Administrator Profile)"/>
		</ffi:cinclude>
		<ffi:cinclude value1="7" value2="${BusinessEmployee.CustomerType}" operator="equals">
			<ffi:setProperty name="Context" value="${BusinessEmployee.UserName} (Cross Channel Profile)"/>
		</ffi:cinclude>

	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminprofiles.jsp"/>
</s:if>
<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<ffi:setProperty name="IsValidGroupDescendant" property="DescendantEntGroupType" value="Group"/>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_225')}" scope="request" />
	<ffi:setProperty name="Context" value="${Entitlement_EntitlementGroup.GroupName} (${tmpI18nStr})"/>

	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmingroups.jsp"/>
</ffi:cinclude>

<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
	<ffi:setProperty name="IsValidGroupDescendant" property="DescendantEntGroupType" value="Division"/>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_174')}" scope="request" />
	<ffi:setProperty name="Context" value="${Entitlement_EntitlementGroup.GroupName} (${tmpI18nStr})"/>
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmindiv.jsp"/>
</ffi:cinclude>

<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
	<ffi:setProperty name="IsValidGroupDescendant" property="DescendantEntGroupType" value="UserProfile"/>
	<ffi:cinclude value1="${ChannelId}" value2="X" operator="notEquals">
		<ffi:setProperty name="IsValidGroupDescendant" property="DescendantEntGroupType" value="ChannelProfile"/>
	</ffi:cinclude>
	<s:set var="tmpI18nStr" value="%{getText('jsp.user_404')}" scope="request" />
	<ffi:setProperty name="Context" value="${Entitlement_EntitlementGroup.GroupName} (${tmpI18nStr})"/>
	<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminprofiles.jsp"/>
</ffi:cinclude>

<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
	<ffi:setProperty name="IsValidGroupDescendant" property="AncestorEntGroupId" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="IsValidGroupDescendant" property="AncestorEntGroupType" value="Business"/>
	<ffi:setProperty name="IsValidGroupDescendant" property="DescendantEntGroupId" value="${EditGroup_GroupId}"/>
	<ffi:process name="IsValidGroupDescendant"/>
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="request" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="notEquals" >
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployeeId}" operator="equals">
				<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
					<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
					<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
					<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
					<ffi:process name="ThrowException"/>
				</ffi:cinclude>
			</ffi:cinclude>
		</ffi:cinclude>
	</ffi:cinclude>
</s:if>
<ffi:removeProperty name="runSearch"/>
