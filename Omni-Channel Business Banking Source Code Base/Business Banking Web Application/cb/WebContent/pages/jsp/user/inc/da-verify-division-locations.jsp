<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayCashConEnabled"/>
<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.cashcon.sameday.support"/>
<ffi:process name="GetBPWProperty"/>

<ffi:cinclude value1="${isLocationChanged}" value2="Y">

	<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount" scope="session" />
	<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%=com.ffusion.tasks.GetDisplayCount.CASHCON %>"/>
	<ffi:process name="GetDisplayCount"/>
	<ffi:removeProperty name="GetDisplayCount"/>

	<ffi:object id="GetLocations" name="com.ffusion.tasks.cashcon.GetLocations"/>
	<ffi:setProperty name ="GetLocations" property="DivisionID" value="${itemId}"/>
	<ffi:setProperty name="GetLocations" property="MaxResults" value="${DisplayCount}"/>
	<ffi:process name="GetLocations"/>
	<ffi:removeProperty name="GetLocations"/>
	
	<%-- Get the affiliate bank list --%>
	<ffi:object id="GetAffiliateBanks" name="com.ffusion.tasks.affiliatebank.GetAffiliateBanks"/>
	<ffi:process name="GetAffiliateBanks"/>
	<ffi:removeProperty name="GetAffiliateBanks"/>
	<ffi:setProperty name="AffiliateBanks" property="SortedBy" value="BANK_NAME" />
	<ffi:setProperty name="NormalPrenote" value="false" />

	<%-- Get cashcon company list --%>
	<ffi:object id="GetCashConCompanies" name="com.ffusion.tasks.cashcon.GetCashConCompanies"/>
	<ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${itemId}"/>
	<ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
	<ffi:process name="GetCashConCompanies"/>
	<ffi:removeProperty name="GetCashConCompanies"/>
		
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="<%=IDualApprovalConstants.TRUE%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<table border="0" width="100%">
	<tr>
		<td><div class="blockHead"><s:text name="user.location.summary" /></div></td>
	</tr>
	</table>
	<table border="0"  width="100%">
	<ffi:list collection="MultipleCategories" items="category">
		<!-- <tr>
			<td colspan="6">&nbsp;</td>
		</tr> -->
		<tr>
			<td colspan="6">
				<div class="blockWrapper">
					<%-- <div class="blockHead"><s:text name="user.location.summary" /></div> --%>
					<div class="blockContent">
						<div class="blockRow">
							<div class="inlineBlock" style="width:49%">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_267" /></span>
								<span><ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED %>" operator="notEquals">
									<ffi:object id="SetLocation" name="com.ffusion.tasks.cashcon.SetLocation"/>
										<ffi:setProperty name="SetLocation" property="BPWID" value="${category.objectId}"/>
									<ffi:process name="SetLocation"/>
									<ffi:getProperty name="Location" property="locationName" />						
									<ffi:setProperty name="locationCashConCompanyBPWID" value="${Location.cashConCompanyBPWID}"/>						
									<ffi:removeProperty name="Location" />						
								</ffi:cinclude>
								<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED %>">
									<ffi:list collection="category.daItems" items="details">
										<ffi:cinclude value1="${details.fieldName}" value2="locationName">
											<ffi:getProperty name="details" property="newValue" />
										</ffi:cinclude>										
										<ffi:cinclude value1="${details.fieldName}" value2="cashConCompanyBPWID">											
											<ffi:setProperty name="NewCashConCompanyBPWID" value="${details.newValue}"/>											
										</ffi:cinclude>
									</ffi:list>
								</ffi:cinclude></span>
							</div>
							<div class="inlineBlock" style="width:49%">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_27" />: </span>
								<span><ffi:getProperty name="category" property="userAction" /></span>
							</div>
							
						</div>
					</div>
				</div>
			</td>
		</tr>
		
		<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_DELETED %>" operator="notEquals">
		<tr>
			<td colspan="6">
				<div class="paneWrapper">
				   	<div class="paneInnerWrapper">
						<div class="header">
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td class="sectionsubhead adminBackground" width="33%"><s:text name="jsp.da_approvals_pending_user_FieldName_column" /></td>
									<td class="sectionsubhead adminBackground" width="33%"><s:text name="jsp.da_approvals_pending_user_OldValue_column" /></td>
									<td class="sectionsubhead adminBackground" width="33%"><s:text name="jsp.da_approvals_pending_user_NewValue_column" /></td>
								</tr>
							</table>
						</div>
						<div class="paneContentWrapper">
							<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tableData">
								
							
		<!-- <tr>
			<td class="sectionsubhead adminBackground" colspan="2">L10NStartField NameL10NEnd</td>
			<td colspan="1" class="sectionsubhead adminBackground">L10NStartOld ValueL10NEnd</td>
			<td colspan="3" class="sectionsubhead adminBackground">L10NStartNew ValueL10NEnd</td>
		</tr> -->
		<%-- Find out customLocalBank old/new and customLocalAccount old/new.  We need these to know which fields should --%>
		<%-- be displayed. Note that these are really tri-state values - they may be true, false or null.  On a null value --%>
		<%-- neither version of the property would be displayed --%>
		<ffi:list collection="category.daItems" items="details">
			<s:if test="%{#request.details.fieldName == 'customLocalBank'}">
				<s:if test="%{#request.details.oldValue == 'true'}">
					<s:set var="isCustomLocalBankOld" value="%{'T'}" scope="page"/>
				</s:if>
				<s:elseif test="%{#request.details.oldValue == 'false'}">
					<s:set var="isCustomLocalBankOld" value="%{'F'}" scope="page"/>
				</s:elseif>
				<s:else>
					<s:set var="isCustomLocalBankOld" value="%{'E'}" scope="page"/>
				</s:else>
				<s:if test="%{#request.details.newValue == 'true'}">
					<s:set var="isCustomLocalBankNew" value="%{'T'}" scope="page"/>
				</s:if>
				<s:elseif test="%{#request.details.newValue == 'false'}">
					<s:set var="isCustomLocalBankNew" value="%{'F'}" scope="page"/>
				</s:elseif>
				<s:else>
					<s:set var="isCustomLocalBankNew" value="%{'E'}" scope="page"/>
				</s:else>
			</s:if>
			<s:elseif test="%{#request.details.fieldName == 'customLocalAccount'}">
				<s:if test="%{#request.details.oldValue == 'true'}">
					<s:set var="isCustomLocalAccountOld" value="%{'T'}" scope="page"/>
				</s:if>
				<s:elseif test="%{#request.details.oldValue == 'false'}">
					<s:set var="isCustomLocalAccountOld" value="%{'F'}" scope="page"/>
				</s:elseif>
				<s:else>
					<s:set var="isCustomLocalAccountOld" value="%{'E'}" scope="page"/>
				</s:else>
				<s:if test="%{#request.details.newValue == 'true'}">
					<s:set var="isCustomLocalAccountNew" value="%{'T'}" scope="page"/>
				</s:if>
				<s:elseif test="%{#request.details.newValue == 'false'}">
					<s:set var="isCustomLocalAccountNew" value="%{'F'}" scope="page"/>
				</s:elseif>
				<s:else>
					<s:set var="isCustomLocalAccountNew" value="%{'E'}" scope="page"/>
				</s:else>
			</s:elseif>
		</ffi:list>
				
		<ffi:setProperty name="isConcSameDayPrenoteNewValue" value="false" />
		<ffi:setProperty name="isDisbSameDayPrenoteNewValue" value="false" />
		<ffi:setProperty name="isConcSameDayPrenoteOldValue" value="false" />
		<ffi:setProperty name="isDisbSameDayPrenoteOldValue" value="false" />
		
		<ffi:setProperty name="isConcSameDayPrenoteModified" value="false" />
		<ffi:setProperty name="isDisbSameDayPrenoteModified" value="false" />
		<ffi:setProperty name="isConcPrenoteModified" value="false" />
		<ffi:setProperty name="isDisbPrenoteModified" value="false" />
		
		<ffi:list collection="category.daItems" items="details">
					
			<ffi:cinclude value1="${details.fieldName}" value2="depositSameDayPrenote" operator="equals">
				<ffi:setProperty name="isConcSameDayPrenoteOldValue" value="${details.oldValue}" />
				<ffi:setProperty name="isConcSameDayPrenoteNewValue" value="${details.newValue}" />
				<ffi:setProperty name="isConcSameDayPrenoteModified" value="true" />
			</ffi:cinclude>
			
			<ffi:cinclude value1="${details.fieldName}" value2="disbursementSameDayPrenote" operator="equals">
				<ffi:setProperty name="isDisbSameDayPrenoteOldValue" value="${details.oldValue}" />
				<ffi:setProperty name="isDisbSameDayPrenoteNewValue" value="${details.newValue}" />
				<ffi:setProperty name="isDisbSameDayPrenoteModified" value="true" />
			</ffi:cinclude>
			
			<ffi:cinclude value1="${details.fieldName}" value2="depositPrenote" operator="equals">
				<ffi:setProperty name="isConcPrenoteModified" value="true" />
			</ffi:cinclude>
			
			<ffi:cinclude value1="${details.fieldName}" value2="disbursementPrenote" operator="equals">
				<ffi:setProperty name="isDisbPrenoteModified" value="true" />
			</ffi:cinclude>
		</ffi:list>
		
		<ffi:setProperty name="displayConcSameDayField" value="false" />
		<ffi:setProperty name="displayDisbSameDayField" value="false" />
		
		<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
		     <ffi:cinclude value1="${isConcSameDayPrenoteModified}" value2="true" operator="equals">
			    <ffi:cinclude value1="${isConcPrenoteModified}" value2="false" operator="equals">
				    <ffi:setProperty name="displayConcSameDayField" value="true" />
				</ffi:cinclude>
			</ffi:cinclude>
		
			<ffi:cinclude value1="${isDisbSameDayPrenoteModified}" value2="true" operator="equals">
				<ffi:cinclude value1="${isDisbPrenoteModified}" value2="false" operator="equals">
					<ffi:setProperty name="displayDisbSameDayField" value="true" />
				</ffi:cinclude>
			</ffi:cinclude>
		</ffi:cinclude>
		
		
		<ffi:setProperty name="isConcSameDayOn" value="false" />
		<ffi:setProperty name="isDisbSameDayOn" value="false" />
		<ffi:list collection="category.daItems" items="details">
			
			<s:if test="%{#request.details.fieldName == 'locationBPWID' || #request.details.fieldName == 'customLocalBank' || #request.details.fieldName == 'customLocalAccount'}">
				<%-- these fields will never display --%>
			</s:if>
			<s:else>
				<%-- determine which of the bank ID and account ID fields should be displayed --%>
				<s:set var="displayThisField" value="false" scope="page"/>
				<s:if test="%{#request.details.fieldName == 'localBankID'}">
					<s:if test="%{#attr.isCustomLocalBankOld == 'T' || #attr.isCustomLocalBankNew == 'T'}">
						<s:set var="displayThisField" value="true" scope="page"/>
					</s:if>
				</s:if>
				<s:elseif test="%{#request.details.fieldName == 'localRoutingNumber' || #request.details.fieldName == 'localBankName'}">
					 <s:if test="%{#attr.isCustomLocalBankOld == 'F' || #attr.isCustomLocalBankNew == 'F'}">
						 <s:set var="displayThisField" value="true" scope="page"/>
					 </s:if>
				</s:elseif>
				<s:elseif test="%{#request.details.fieldName == 'localAccountID'}">
					 <s:if test="%{#attr.isCustomLocalBankOld == 'T' || #attr.isCustomLocalBankNew}">
						 <s:set var="displayThisField" value="true" scope="page"/>
					 </s:if>
				</s:elseif>
				<s:elseif test="%{(#request.details.fieldName == 'localAccountNumber' || #request.details.fieldName == 'localAccountType')}">
					 <s:if test="%{#attr.isCustomLocalAccountOld == 'F' || #attr.isCustomLocalAccountNew == 'F'}">
						 <s:set var="displayThisField" value="true" scope="page"/>
					 </s:if>
				</s:elseif>
				<s:else>
				
					<ffi:setProperty name="isSameDayPrenoteField" value="false" />
					
					<ffi:cinclude value1="${details.fieldName}" value2="depositSameDayPrenote" operator="equals">
						<ffi:cinclude value1="${displayConcSameDayField}" value2="true" operator="equals">
							<ffi:setProperty name="isSameDayPrenoteField" value="false" />
						</ffi:cinclude>
						<ffi:cinclude value1="${displayConcSameDayField}" value2="true" operator="notEquals">
							<ffi:setProperty name="isSameDayPrenoteField" value="true" />
						</ffi:cinclude>
					</ffi:cinclude>
					
					<ffi:cinclude value1="${details.fieldName}" value2="disbursementSameDayPrenote" operator="equals">
						<ffi:cinclude value1="${displayDisbSameDayField}" value2="true" operator="equals">
							<ffi:setProperty name="isSameDayPrenoteField" value="false" />
						</ffi:cinclude>
						<ffi:cinclude value1="${displayDisbSameDayField}" value2="true" operator="notEquals">
							<ffi:setProperty name="isSameDayPrenoteField" value="true" />
						</ffi:cinclude>
					</ffi:cinclude>
					
					<ffi:cinclude value1="${isSameDayPrenoteField}" value2="true" operator="equals">
						<s:set var="displayThisField" value="false" scope="page"/>
					</ffi:cinclude>
					
					<ffi:cinclude value1="${isSameDayPrenoteField}" value2="true" operator="notEquals">
						<s:set var="displayThisField" value="true" scope="page"/>
					</ffi:cinclude>
					
				</s:else>
				<s:if test="%{#attr.displayThisField}">
					<tr>
						<td width="33%" class="columndata" colspan="2">
						
							<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								<s:set var="displayedModifiedField" value="false" scope="page"/>
								
								<ffi:cinclude value1="${details.fieldName}" value2="depositSameDayPrenote" operator="equals">
									<ffi:cinclude value1="${displayConcSameDayField}" value2="true" operator="equals">
										   <ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.depositPrenote" />
										   <s:set var="displayedModifiedField" value="true" scope="page"/>
									</ffi:cinclude>
								</ffi:cinclude>
								
								<ffi:cinclude value1="${details.fieldName}" value2="disbursementSameDayPrenote" operator="equals">
									<ffi:cinclude value1="${displayDisbSameDayField}" value2="true" operator="equals">
											<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.disbursementPrenote" />
											<s:set var="displayedModifiedField" value="true" scope="page"/>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${displayedModifiedField}" value2="false" operator="equals">
									<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.${details.fieldName}" />
								</ffi:cinclude>
								
							</ffi:cinclude>
							<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>" operator="notEquals">
								<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.${details.fieldName}" />
							</ffi:cinclude>
						</td>
						<td width="33%" class="columndata">
							<s:if test="%{#request.details.fieldName == 'localBankID'}">
								<ffi:list collection="AffiliateBanks" items="Bank">
									<ffi:cinclude value1="${Bank.AffiliateBankID}" value2="${details.oldValue}" operator="equals">
										<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
									</ffi:cinclude>
								</ffi:list>
							</s:if>
							<s:elseif test="%{#request.details.fieldName == 'localAccountType'}">
								<ffi:cinclude value1="${details.oldValue}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">
									<!--L10NStart-->Checking<!--L10NEnd-->
								</ffi:cinclude>
								<ffi:cinclude value1="${details.oldValue}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">
									<!--L10NStart-->Savings<!--L10NEnd-->
								</ffi:cinclude>
							</s:elseif>
							<s:elseif test="%{#request.details.fieldName == 'cashConCompanyBPWID'}">																						
								<ffi:cinclude value1="${details.oldValue}" value2="" operator="notEquals">								
									<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
									<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
									<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>									
									<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${details.oldValue}"/>								
									<ffi:process name="SetCashConCompany"/>	
									<ffi:list collection="CashConCompanies" items="tempCompany">									
										<ffi:cinclude value1="${details.oldValue}" value2="${tempCompany.BPWID}">
																				
											<ffi:setProperty name="oldConcSameDayCutOffDefined" value="${tempCompany.ConcSameDayCutOffDefined}" />
											<ffi:setProperty name="oldConcCutOffsPassed" value="${tempCompany.ConcCutOffsPassed}" />
											<ffi:setProperty name="oldDisbSameDayCutOffDefined" value="${tempCompany.DisbSameDayCutOffDefined}" />
											<ffi:setProperty name="oldDisbCutOffsPassed" value="${tempCompany.DisbCutOffsPassed}" />
										    
											<ffi:getProperty name="tempCompany" property="CompanyName"/>
										</ffi:cinclude>
									</ffi:list>
								</ffi:cinclude>
							</s:elseif>
							<s:elseif test="%{#request.details.fieldName == 'concAccountBPWID'}">
									<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED %>" operator="notEquals">
									<s:if test="{#session.OldCashConCompany == null || #session.OldCashConCompany.size == 0}">
										<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
										<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>									
										<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${locationCashConCompanyBPWID}"/>								
										<ffi:process name="SetCashConCompany"/>
									</s:if>	
									</ffi:cinclude>							
								<ffi:list collection="OldCashConCompany.ConcAccounts" items="ConcAccount">
									<ffi:cinclude value1="${details.oldValue}" value2="${ConcAccount.BPWID}">
											<ffi:getProperty name="ConcAccount" property="DisplayText"/>
												<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
													 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
													</ffi:cinclude>
									</ffi:cinclude>
								</ffi:list>
							</s:elseif>
							<s:elseif test="%{#request.details.fieldName == 'disbAccountBPWID'}">
								<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED %>" operator="notEquals">
									<s:if test="{#session.OldCashConCompany == null || #session.OldCashConCompany.size == 0}">
										<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
										<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="OldCashConCompany"/>									
										<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${locationCashConCompanyBPWID}"/>								
										<ffi:process name="SetCashConCompany"/>
									</s:if>	
								</ffi:cinclude>
								<ffi:list collection="OldCashConCompany.DisbAccounts" items="DisbAccount">
									<ffi:cinclude value1="${details.oldValue}" value2="${DisbAccount.BPWID}">
										<ffi:getProperty name="DisbAccount" property="DisplayText"/>
										<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
											 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
								 		</ffi:cinclude>
									</ffi:cinclude>
								</ffi:list>
							</s:elseif>
							<s:else>
								<ffi:setProperty name="isOldFieldDisplayed" value="false" />
									<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
										<ffi:cinclude value1="${details.fieldName}" value2="depositSameDayPrenote" operator="equals">
											<ffi:cinclude value1="${isConcSameDayPrenoteModified}" value2="true" operator="equals">
												<ffi:cinclude value1="${isConcPrenoteModified}" value2="false" operator="equals">
												    <ffi:cinclude value1="${details.oldValue}" value2="" operator="notEquals">
													    <ffi:cinclude value1="${details.oldValue}" value2="true" operator="equals">
													    	<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.sameDayRequested" />
													    </ffi:cinclude>
													     <ffi:cinclude value1="${details.oldValue}" value2="true" operator="notEquals">
													     	<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
													     </ffi:cinclude>
													     <ffi:setProperty name="isOldFieldDisplayed" value="true" />
												    </ffi:cinclude>
												</ffi:cinclude>
											</ffi:cinclude>
										</ffi:cinclude>
										
										<ffi:cinclude value1="${details.fieldName}" value2="disbursementSameDayPrenote" operator="equals">
											<ffi:cinclude value1="${isDisbSameDayPrenoteModified}" value2="true" operator="equals">
												<ffi:cinclude value1="${isDisbPrenoteModified}" value2="false" operator="equals">
												 	<ffi:cinclude value1="${details.oldValue}" value2="" operator="notEquals">
														<ffi:cinclude value1="${details.oldValue}" value2="true" operator="equals">
													    	<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.sameDayRequested" />
													    </ffi:cinclude>
													     <ffi:cinclude value1="${details.oldValue}" value2="true" operator="notEquals">
													     	<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
													     </ffi:cinclude>
													     <ffi:setProperty name="isOldFieldDisplayed" value="true" /> 
													</ffi:cinclude>     
												</ffi:cinclude>
											</ffi:cinclude>
										</ffi:cinclude>
									</ffi:cinclude>
								
									<ffi:cinclude value1="${details.fieldName}" value2="depositPrenote" operator="equals">
										
											<ffi:cinclude value1="${isConcSameDayPrenoteOldValue}" value2="true" operator="equals">
												<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.sameDayRequested" />
												<ffi:setProperty name="isOldFieldDisplayed" value="true" />
											</ffi:cinclude>
											
										<ffi:cinclude value1="${isConcSameDayPrenoteOldValue}" value2="true" operator="notEquals">
											<ffi:cinclude value1="${details.oldValue}" value2="" operator="notEquals">
												<ffi:cinclude value1="${details.oldValue}" value2="true" operator="equals">
													<ffi:cinclude value1="${oldConcSameDayCutOffDefined}" value2="true" operator="equals">
														<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
													</ffi:cinclude>
													<ffi:cinclude value1="${oldConcSameDayCutOffDefined}" value2="true" operator="notEquals">
														<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
													</ffi:cinclude>
												</ffi:cinclude>
											
												<ffi:cinclude value1="${details.oldValue}" value2="true" operator="notEquals">
													<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.notRequested" />
												</ffi:cinclude>
												<ffi:setProperty name="isOldFieldDisplayed" value="true" />
											</ffi:cinclude>
										</ffi:cinclude>
									</ffi:cinclude>
								
								
								<ffi:cinclude value1="${details.fieldName}" value2="disbursementPrenote" operator="equals">
									<ffi:cinclude value1="${isDisbSameDayPrenoteOldValue}" value2="true" operator="equals">
										<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.sameDayRequested" />
										<ffi:setProperty name="isOldFieldDisplayed" value="true" />
									</ffi:cinclude>
									<ffi:cinclude value1="${isDisbSameDayPrenoteOldValue}" value2="true" operator="notEquals">
										<ffi:cinclude value1="${details.oldValue}" value2="" operator="notEquals">
											<ffi:cinclude value1="${details.oldValue}" value2="true" operator="equals">
											   <ffi:cinclude value1="${oldDisbSameDayCutOffDefined}" value2="true" operator="equals">
													<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
												</ffi:cinclude>
												<ffi:cinclude value1="${oldDisbSameDayCutOffDefined}" value2="true" operator="notEquals">
													<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
												</ffi:cinclude>
											</ffi:cinclude>
										
											<ffi:cinclude value1="${details.oldValue}" value2="true" operator="notEquals">
												<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.notRequested" />
											</ffi:cinclude>
											<ffi:setProperty name="isOldFieldDisplayed" value="true" />
										</ffi:cinclude>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${isOldFieldDisplayed}" value2="false" operator="equals">
									<ffi:getProperty name="details" property="oldValue" />	
								</ffi:cinclude>
							</s:else>
						</td>
						<td width="33%"  class="columndata">
							<s:if test="%{#request.details.fieldName == 'localBankID'}">
								<ffi:list collection="AffiliateBanks" items="Bank">
									<ffi:cinclude value1="${Bank.AffiliateBankID}" value2="${details.newValue}" operator="equals">
										<ffi:getProperty name="Bank" property="AffiliateBankName"/> - <ffi:getProperty name="Bank" property="AffiliateRoutingNum"/>
									</ffi:cinclude>
								</ffi:list>
							</s:if>
							<s:elseif test="%{#request.details.fieldName == 'localAccountType'}">
								<ffi:cinclude value1="${details.newValue}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_CHECKING )%>" operator="equals">
									<!--L10NStart-->Checking<!--L10NEnd-->
								</ffi:cinclude>
								<ffi:cinclude value1="${details.newValue}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountTypes.TYPE_SAVINGS )%>" operator="equals">
									<!--L10NStart-->Savings<!--L10NEnd-->
								</ffi:cinclude>
							</s:elseif>
							<s:elseif test="%{#request.details.fieldName == 'cashConCompanyBPWID'}">
								<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
								<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
								<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
								<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${details.newValue}"/>
								<ffi:process name="SetCashConCompany"/>
								<ffi:list collection="CashConCompanies" items="tempCompany">
									<ffi:cinclude value1="${details.newValue}" value2="${tempCompany.BPWID}">
													
										<ffi:setProperty name="newConcSameDayCutOffDefined" value="${tempCompany.ConcSameDayCutOffDefined}" />
										<ffi:setProperty name="newConcCutOffsPassed" value="${tempCompany.ConcCutOffsPassed}" />
										<ffi:setProperty name="newDisbSameDayCutOffDefined" value="${tempCompany.DisbSameDayCutOffDefined}" />
										<ffi:setProperty name="newDisbCutOffsPassed" value="${tempCompany.DisbCutOffsPassed}" />
									
										<ffi:getProperty name="tempCompany" property="CompanyName"/>
									</ffi:cinclude>
								</ffi:list>
							</s:elseif>
							<s:elseif test="%{#request.details.fieldName == 'concAccountBPWID'}">							 
							<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED %>" operator="notEquals">							 
								<s:if test="{#session.CashConCompany == null || #session.CashConCompany.size == 0}">								
									<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
									<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
									<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
									<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${locationCashConCompanyBPWID}"/>
									<ffi:process name="SetCashConCompany"/>
								</s:if>
							</ffi:cinclude>
							<s:if test="{#session.CashConCompany == null || #session.CashConCompany.size == 0}">															
									<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
									<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
									<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
									<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${NewCashConCompanyBPWID}"/>
									<ffi:process name="SetCashConCompany"/>
							</s:if>
								<ffi:list collection="CashConCompany.ConcAccounts" items="ConcAccount">								
									<ffi:cinclude value1="${details.newValue}" value2="${ConcAccount.BPWID}">									
											<ffi:getProperty name="ConcAccount" property="DisplayText"/>
												<ffi:cinclude value1="${ConcAccount.Nickname}" value2="" operator="notEquals" >
													 - <ffi:getProperty name="ConcAccount" property="Nickname"/>
												</ffi:cinclude>
									</ffi:cinclude>
								</ffi:list>								
							</s:elseif>
							<s:elseif test="%{#request.details.fieldName == 'disbAccountBPWID'}">
								<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED %>" operator="notEquals">								
									<s:if test="{#session.CashConCompany == null || #session.CashConCompany.size == 0}">								
										<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
										<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
										<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${locationCashConCompanyBPWID}"/>
										<ffi:process name="SetCashConCompany"/>
									</s:if>						 
								</ffi:cinclude>
								<s:if test="{#session.CashConCompany == null || #session.CashConCompany.size == 0}">								
									<ffi:object id="SetCashConCompany" name="com.ffusion.tasks.cashcon.SetCashConCompany"/>
									<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
									<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
									<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${NewCashConCompanyBPWID}"/>
									<ffi:process name="SetCashConCompany"/>
								</s:if>
								<ffi:list collection="CashConCompany.DisbAccounts" items="DisbAccount">													
									<ffi:cinclude value1="${details.newValue}" value2="${DisbAccount.BPWID}">
										<ffi:getProperty name="DisbAccount" property="DisplayText"/>
										<ffi:cinclude value1="${DisbAccount.Nickname}" value2="" operator="notEquals" >
											 - <ffi:getProperty name="DisbAccount" property="Nickname"/>
								 		</ffi:cinclude>
									</ffi:cinclude>
								</ffi:list>								
							</s:elseif>
							<s:else>
								<ffi:setProperty name="isNewFieldDisplayed" value="false" />
									<ffi:cinclude value1="${category.userAction}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
										<ffi:cinclude value1="${details.fieldName}" value2="depositSameDayPrenote" operator="equals">
											<ffi:cinclude value1="${isConcSameDayPrenoteModified}" value2="true" operator="equals">
												<ffi:cinclude value1="${isConcPrenoteModified}" value2="false" operator="equals">
												    <ffi:cinclude value1="${details.newValue}" value2="" operator="notEquals">
													    <ffi:cinclude value1="${details.newValue}" value2="true" operator="equals">
													    	<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.sameDayRequested" />
													    </ffi:cinclude>
													     <ffi:cinclude value1="${details.newValue}" value2="true" operator="notEquals">
													     	<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
													     </ffi:cinclude>
													     <ffi:setProperty name="isNewFieldDisplayed" value="true" />
												    </ffi:cinclude>
												</ffi:cinclude>
											</ffi:cinclude>
										</ffi:cinclude>
										
										<ffi:cinclude value1="${details.fieldName}" value2="disbursementSameDayPrenote" operator="equals">
											<ffi:cinclude value1="${isDisbSameDayPrenoteModified}" value2="true" operator="equals">
												<ffi:cinclude value1="${isDisbPrenoteModified}" value2="false" operator="equals">
												 	<ffi:cinclude value1="${details.newValue}" value2="" operator="notEquals">
														<ffi:cinclude value1="${details.newValue}" value2="true" operator="equals">
													    	<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.sameDayRequested" />
													    </ffi:cinclude>
													     <ffi:cinclude value1="${details.newValue}" value2="true" operator="notEquals">
													     	<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
													     </ffi:cinclude>
													     <ffi:setProperty name="isNewFieldDisplayed" value="true" /> 
													</ffi:cinclude>     
												</ffi:cinclude>
											</ffi:cinclude>
										</ffi:cinclude>
									</ffi:cinclude>
								
								<ffi:cinclude value1="${details.fieldName}" value2="depositPrenote" operator="equals">
									<ffi:cinclude value1="${isConcSameDayPrenoteNewValue}" value2="true" operator="equals">
										<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.sameDayRequested" />
										<ffi:setProperty name="isConcSameDayOn" value="true" />
										<ffi:setProperty name="isNewFieldDisplayed" value="true" />
									</ffi:cinclude>
									<ffi:cinclude value1="${isConcSameDayPrenoteNewValue}" value2="true" operator="notEquals">
										<ffi:cinclude value1="${details.newValue}" value2="" operator="notEquals">
											<ffi:cinclude value1="${details.newValue}" value2="true" operator="equals">
												<ffi:cinclude value1="${newConcSameDayCutOffDefined}" value2="true" operator="equals">
													<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
												</ffi:cinclude>
												<ffi:cinclude value1="${newConcSameDayCutOffDefined}" value2="true" operator="notEquals">
													<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
												</ffi:cinclude>
											</ffi:cinclude>
											<ffi:cinclude value1="${details.newValue}" value2="true" operator="notEquals">
												<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.notRequested" />
											</ffi:cinclude>
											<ffi:setProperty name="isNewFieldDisplayed" value="true" />
										</ffi:cinclude>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${details.fieldName}" value2="disbursementPrenote" operator="equals">
									<ffi:cinclude value1="${isDisbSameDayPrenoteNewValue}" value2="true" operator="equals">
										<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.sameDayRequested" />
										<ffi:setProperty name="isDisbSameDayOn" value="true" />
										<ffi:setProperty name="isNewFieldDisplayed" value="true" />
									</ffi:cinclude>
									<ffi:cinclude value1="${isDisbSameDayPrenoteNewValue}" value2="true" operator="notEquals">
										<ffi:cinclude value1="${details.newValue}" value2="" operator="notEquals">
											<ffi:cinclude value1="${details.newValue}" value2="true" operator="equals">
											 	<ffi:cinclude value1="${newDisbSameDayCutOffDefined}" value2="true" operator="equals">
													<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
												</ffi:cinclude>
												<ffi:cinclude value1="${newDisbSameDayCutOffDefined}" value2="true" operator="notEquals">
													<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.requested" />
												</ffi:cinclude>
											</ffi:cinclude>
											<ffi:cinclude value1="${details.newValue}" value2="true" operator="notEquals">
												<ffi:getL10NString rsrcFile="cb" msgKey="da.field.division.location.notRequested" />
											</ffi:cinclude>
										    <ffi:setProperty name="isNewFieldDisplayed" value="true" />
										</ffi:cinclude>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${isNewFieldDisplayed}" value2="false" operator="equals">
									<ffi:getProperty name="details" property="newValue" />	
								</ffi:cinclude>
							</s:else>
						</td>
					</tr>
				</s:if>
			</s:else>
		</ffi:list>
					<ffi:setProperty name="showNote" value="false" />
			<ffi:cinclude value1="${isConcSameDayOn}" value2="true" operator="equals">
				<ffi:cinclude value1="${newConcSameDayCutOffDefined}" value2="true" operator="equals">
					<ffi:cinclude value1="${newConcCutOffsPassed}" value2="true" operator="equals">
								<ffi:setProperty name="showNote" value="true" />
					</ffi:cinclude>	
				</ffi:cinclude>	
			</ffi:cinclude>
			<ffi:cinclude value1="${isDisbSameDayOn}" value2="true" operator="equals">
				<ffi:cinclude value1="${newDisbSameDayCutOffDefined}" value2="true" operator="equals">
					<ffi:cinclude value1="${newDisbCutOffsPassed}" value2="true" operator="equals">
						<ffi:setProperty name="showNote" value="true" />
					</ffi:cinclude>	
				</ffi:cinclude>		
			</ffi:cinclude>	
			<ffi:cinclude value1="${showNote}" value2="true" operator="equals">
				<tr>
					<td colspan="5">
						<table align="left">
							<tr>
								<td>
									<div id="showNote" >
										<span class="required">&nbsp;&nbsp;<s:text name="jsp.common.note.sameDayCutOffPassed"/></span>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</ffi:cinclude>
		</table>
	</div>
	</div>
	</div>
	</td>
</tr>
<tr>
			<td colspan="6">&nbsp;</td>
		</tr>
		</ffi:cinclude>
	</ffi:list>
	</table>
</ffi:cinclude>
<ffi:removeProperty name="MultipleCategories"/>
<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="DisplayCount"/>
<ffi:removeProperty name="Locations"/>
<ffi:removeProperty name="customLocalAccount"/>
<ffi:removeProperty name="customLocalBankNew"/>
<ffi:removeProperty name="customLocalAccountOld"/>
<ffi:removeProperty name="customLocalAccountNew"/>
<ffi:removeProperty name="AffiliateBanks"/>
<ffi:removeProperty name="OldCashConCompany"/>
<ffi:removeProperty name="CashConCompany"/>
<ffi:removeProperty name="locationCashConCompanyBPWID"/>
<ffi:removeProperty name="NewCashConCompanyBPWID"/>

