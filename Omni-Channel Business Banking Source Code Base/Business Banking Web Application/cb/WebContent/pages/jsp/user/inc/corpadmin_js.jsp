<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script language="JavaScript" type="text/javascript"><!--
// the variable used in javascripts looks like default as
// global, then all loop variables need to be different name

function divAddEditValidator(theForm, groupNameFormElement) {
	if( !isNonEmpty(groupNameFormElement.value) ) {
	   alert( "<s:text name="jsp.user_309"/>");
	   return false;
	} else if( groupNameFormElement.value.length > 255 ) {
	   alert( "<s:text name="jsp.user_311"/>");
	   return false;
	}

	if( !chkName( groupNameFormElement.value ) ) {
		alertStr = "<s:text name="i18nNotFound_1"/>";
		alertStr = alertStr + "<s:text name="jsp.user_246"/>";
		alert(alertStr);
		return false;
	}
	return true;
}

function groupAddEditValidator(groupNameFormElement) {
	if( !isNonEmpty(groupNameFormElement.value) ) {
	   alert( "<s:text name="jsp.user_314"/>");
	   return false;
	} else if( groupNameFormElement.value.length > 255 ) {
	   alert( "<s:text name="jsp.user_316"/>");
	   return false;
	}

	if( !chkName( groupNameFormElement.value ) ) {
		alertStr = "<s:text name="i18nNotFound_2"/>";
		alertStr = alertStr + "<s:text name="jsp.user_246"/>";
		alert(alertStr);
		return false;
	}
	return true;
}

function isNonEmpty( fieldvalue ) {
    if( fieldvalue == null || fieldvalue.length ==0 ) return false;
	for(  i1 = 0; i1 < fieldvalue.length; i1 ++ ) {
	    // if there is non white space character, it is non empty string
	    if( fieldvalue.charAt( i1 ) != ' ' ) {
		     return true;
			 }
	}
	return false;
}

function deleteEntGroup(form) {

	form.action = form.SecurePath.value + "CanDeleteEntitlementGroup";
	form.submit();

}


function userAddEditValidator( theForm, objName, password_len, userid_len, max_password_len, allowEmptyPassword ) {
	passwordField=objName + ".Password";
	confirmPasswordField="ConfirmPassword";
	firstNameField=objName + ".FirstName";
	lastNameField=objName + ".LastName";
	userField=objName + ".UserName";
	groupField=objName + ".EntitlementGroupId";
	clueField=objName+".PasswordClue";
	reminderField=objName+".PasswordReminder";
	phoneField = objName+".Phone";
	emailField = objName+".Email";

	confirmPassword="";
	password="";
	clueVal = "";
	passwordReminder="";
	phone="";
	email="";
	group="";

	for (i2 = 0; i2 < theForm.length; i2 ++) {

		if( ( theForm.elements[i2].name == firstNameField ) ) {

		    if( !isNonEmpty( theForm.elements[i2].value ) ) {
				alert("<s:text name="jsp.user_312"/>");
				return false;
			}
		 	if( !chkFirstLastName( theForm.elements[i2].value ) ) {
				alertStr = "<s:text name="i18nNotFound_3"/>";
				alertStr = alertStr + "<s:text name="jsp.user_247"/>";
				alert(alertStr);
				return false;
			}
		}

		if( ( theForm.elements[i2].name == lastNameField ) ) {
		    if( !isNonEmpty( theForm.elements[i2].value ) ) {
				alert("<s:text name="jsp.user_317"/>");
				return false;
			}
		 	if( !chkFirstLastName( theForm.elements[i2].value ) ) {
				alertStr = "<s:text name="i18nNotFound_4"/>";
				alertStr = alertStr + "<s:text name="jsp.user_248"/>";
				alert(alertStr);
				return false;
			}
		}

		// save password field
		if( theForm.elements[i2].name == passwordField ) {
			password =  theForm.elements[i2].value;

			if ( !allowEmptyPassword && !isNonEmpty( password ) ) {
				alert("<s:text name="jsp.user_319"/>");
				return false;
			}
		 	if( password.length>0 ) {
				if ( !chkName( password ) ) {
					alertStr = "<s:text name="i18nNotFound_5"/>";
					alertStr = alertStr + "<s:text name="jsp.user_246"/>";
					alert(alertStr);
					return false;
				}
	
				// validate the length of the password
				if ( password.length < password_len ) {
					if( password.length>0 || !allowEmptyPassword ) {
						alertStr = "<s:text name="jsp.user_253"/> " + password_len + " <s:text name="jsp.user_63"/>";
						alert( alertstr );
						return false;
					}
				}
	
				if ( password.length > max_password_len ) {
					alertStr = "<s:text name="jsp.user_254"/> " + max_password_len + " <s:text name="jsp.user_63"/>";
					alert( alertstr );
					return false;
				}
			}
		}

		//save confirm password field
		if( theForm.elements[i2].name == confirmPasswordField ) {
			confirmPassword =  theForm.elements[i2].value;
		}

		// save user field
		if( theForm.elements[i2].name == userField ) {
			user =  theForm.elements[i2].value;

			if ( !isNonEmpty( user ) ) {
				alert("<s:text name="jsp.user_323"/>");
				return false;
			}

		 	if( !chkUserName( user ) ) {
				alertStr = "<s:text name="i18nNotFound_6"/>";
				alertStr = alertStr + "<s:text name="jsp.user_246"/>";
				alert(alertStr);
				return false;
			}

			//validate the length of user name
			if ( user.length < userid_len ) {
				alertStr = "<s:text name="jsp.user_258"/> " + userid_len + ".";
				alert( alertstr );
				return false;
			}
		}


		if( theForm.elements[i2].name == clueField ) {
			clueVal = theForm.elements[i2].value;
		}

		// confirm password
		if( ( theForm.elements[i2].name == confirmPasswordField ) ) {
			if( confirmPassword.length <= 0 && password.length>0 ) {
				alert("<s:text name="jsp.user_255"/>");
				return false;
			}else if ( password !== confirmPassword ) {
				alert("<s:text name="jsp.user_308"/>");
				return false;
			}
		}

		// check reminder and questions
		if( ( theForm.elements[i2].name == reminderField ) ) {
			passwordReminder=theForm.elements[i2].value;
			if( ( passwordReminder.length>0 ) && ( clueVal.length<=0 ) ) {
				alert("<s:text name="jsp.user_335"/>");
				return false;
			} else if( ( passwordReminder.length<=0 ) && ( clueVal.length>0 ) ) {
				alert("<s:text name="jsp.user_334"/>");
				return false;
			}
		}

		// Make sure that phone# is all numeric
		if( ( theForm.elements[i2].name == phoneField ) ) {
			phone=theForm.elements[i2].value;
			if( phone.length>0 && !chkPhone( phone ) ){
				alert( "<s:text name="jsp.user_251"/>" );
				return false;
			}
		}

		// Make sure email format is valid
		if( ( theForm.elements[i2].name == emailField ) ) {
			email=theForm.elements[i2].value;

			if( email.length>0 && !chkEmail( email ) ) {
				alert( "<s:text name="jsp.user_132"/>" );
				return false;
			}
		}

		// save group field
		if( theForm.elements[i2].name == groupField ) {
			if ( theForm.elements[i2].value == "-1" ) {
				group = "-1";
				//alert("<s:text name="jsp.user_3"/>");
				//return false;
			}
		}

	}	// end for-loop


	// check to make sure at least one character in the password is alpha and one numeric
	if( password.length>0 || !allowEmptyPassword ) {
		if( !chkPassword(password) ) {
			return false;
		}
	}

	if ( group == "-1" ) {
		alert("<s:text name="jsp.user_3"/>");
		return false;
	}


	return true;
}



// Validate phone number
function chkPhone( fieldValue )
{
	// Numeric numbers, plus a few more accepted char's
	var validInputNumbers = "0123456789 -()";

	var match=false;
	for( i=0; i<fieldValue.length; i++ ) {
		ch = fieldValue.charAt( i );
		match=false;
		for( j=0; j<validInputNumbers.length; j++ ) {
			if( ch== validInputNumbers.charAt(j) ) {
				match=true;
				break;
			}
		}
		if( !match ) return false;
	}

	return true;
}

// Validate E-mail entry
function chkEmail( email )
{
	// Boundary conditions: email address must be at least 5 char's long, i.e. X@Y.Z
	if( email.length<5
		|| email.charAt(0)=='@'
		|| email.charAt(0)=='.'
		|| email.charAt( email.length-1 )=='@'
		|| email.charAt( email.length-2 )=='@'
		|| email.charAt( email.length-3 )=='@'
		|| email.charAt( email.length-1 )=='.' ) {
		return false;
	}

	var count=0;
	for( i=1; i<email.length-1; i++ ) {
		if( email.charAt(i)=='@' ) count++;
	}

	if( count!=1 ) return false;

	return true;
}

// validate to check if all characters of password is in validInput string.
function chkPassword(validfield)
{
	// only allow 0-9 be entered,
	var validInputNumbers = "0123456789";
	var validInputAlphas = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

	var numFound = false;
	var alphaFound = false;
	for (i4 = 0; i4 < validfield.length; i4++)
	{
		ch = validfield.charAt(i4);
		for (j4 = 0; j4 < validInputNumbers.length;  j4++)
		{
			if (ch == validInputNumbers.charAt(j4))
			{
				numFound = true;
				break;
			}
		}

		for (k4 = 0; k4 < validInputAlphas.length; k4++)
		{
			if (ch == validInputAlphas.charAt(k4))
			{
				alphaFound = true;
				break;
			}
		}

		if (!numFound && !alphaFound)
		{
			alertStr = "<s:text name="jsp.user_321"/>";
			alertStr = alertStr + "<s:text name="jsp.user_229"/>";
			alert(alertStr);
			return (false);
		}
	}

	if (!numFound || !alphaFound)
	{
		alertStr = "<s:text name="jsp.user_321"/>";
		alertStr = alertStr + "<s:text name="jsp.user_230"/>";
		alert(alertStr);
		return (false);
	}
	return true;
}

// validate to check if a string is a valid name
function chkString(validfield, validInputChars) {
	var validFound = false;
	for (i4 = 0; i4 < validfield.length; i4++)
	{
		validFound = false;
		ch = validfield.charAt(i4);
		for (k4 = 0; k4 < validInputChars.length; k4++)
		{
			if (ch == validInputChars.charAt(k4))
			{
				validFound = true;
				break;
			}
		}

		if(!validFound)
		{
			return false;
		}
	}

	return true;
}

function chkName(validField) {
	// only allow alpha, numbers, space, period and underscore
  	var validInputChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ._";

	return chkString(validField, validInputChars);
}

function chkFirstLastName(validField) {
	// only allow alpha, numbers, space, period, underscore and hyphen for first and last name
	var validInputChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ._-'";

	return chkString(validField, validInputChars);
}

function chkUserName(validField) {
	//only allow lowercase alpha, numbers, space, period and underscore
	var validInputChars = "abcdefghijklmnopqrstuvwxyz0123456789 ._";
	return chkString(validField, validInputChars);
}

// validate to check if all characters of password is in validInput string.
function chkNumeric(validfield, validfieldname, validInput )
{

	// only allow 0-9 be entered,
	if( validInput == null || validInput == "" || validInput.length < 1 ) {
	    validInput = "0123456789";
	}
	var allValid = true;
	for (i4 = 0;  i4 < validfield.length;  i4++)
	{
		ch = validfield.charAt(i4);
		for (j4 = 0;  j4 < validInput.length;  j4++)
		{
			if (ch == validInput.charAt(j4))
			break;

		    // could not find the match character in the validInput with ch
			if (j4 == validInput.length - 1 )
			{
				allValid = false;
				break;
			}
		}
	}

	if (!allValid)
	{
		alertStr = "<s:text name="jsp.user_257"/> \""
		alertStr = alertStr + validInput + "\" <s:text name="jsp.user_176"/> \"" + validfieldname + "\" <s:text name="jsp.user_159"/>"
		alert(alertStr);
		return (false);
	}
	return true;
}

function validateACH( theForm, achid, achname )
{
   achidvalue="";
   achnamevalue="";

   for (i5 = 0; i5 < theForm.length; i5++) {

			// save achid field
			if( theForm.elements[i5].name == achid ) {
				achidvalue =  theForm.elements[i5].value;

			}

			// save achname field
			if( theForm.elements[i5].name == achname ) {
				achnamevalue =  theForm.elements[i5].value;
			}
	}

    // only return true if the values of both fields are non empty
	// or both fields are empty
	if( (  isNonEmpty( achidvalue )  &&   isNonEmpty( achnamevalue ) )
	   ||  ( !isNonEmpty( achidvalue )  &&  !isNonEmpty( achnamevalue ) ) ) {
		return true;
	}
	alert( "<s:text name="jsp.user_194"/> " + achid + " <s:text name="jsp.user_40"/> " + achname + " <s:text name="jsp.user_46"/>");
	return false;
}

function validateLimitAmount( theForm )
{
	// suffix of limit text boxes
	var validChars="0123456789";

	for( i=0; i < theForm.length; i++ ) {
		curr_name=theForm.elements[i].name;
		if( curr_name.indexOf( "_limit" ) > 0
		 && ( curr_name.indexOf( "transaction" ) >= 0
		   || curr_name.indexOf( "day" ) >= 0
		   || curr_name.indexOf( "week" ) >= 0
		   || curr_name.indexOf( "month" ) >= 0 ) ) {

			points=0;
			amount_str = theForm.elements[i].value;
			// trivial amount
			if( amount_str.length<1 ) continue;
			// get rid of the leading +/-. e.g. +100, -10000.00
			if( amount_str.length>1 ) {
				if( amount_str.charAt(0) == '+' || amount_str.charAt(0) == '-' ) {
					amount_str = amount_str.substring(1);
				}
			}
			for( j=0; j<amount_str.length; j++ ) {
				if( amount_str.charAt(j)=='.' ) {
					points++;
					if( points>1 ) {
						alert( theForm.elements[i].value + " <s:text name="jsp.user_178"/>" );
						return false;
					}
				} else {
					if( validChars.indexOf(amount_str.charAt(j)) < 0) {
						alert( theForm.elements[i].value + " <s:text name="jsp.user_178"/>" );
						return false;
					}
				}
			}	// end for-loop
		}
	} // end for-loop

	return true;
}

/**
 * Checks for negative values, and values that are made redundant by other limits
 */
function validateLimits(form, prefix) {
		var limitName = new Array('transaction_limit', 'day_limit', 'week_limit', 'month_limit');
		var limitPeriod = new Array('transaction', 'day', 'week', 'month');
		for (i = 0; i < form.length; i ++) {
			name = form.elements[i].name;
			if (name.indexOf(prefix) == 0 && form.elements[i].checked) {
				suffix = name.substring(prefix.length, name.length);
				// we should ALWAYS have limits after the name -- ACH and TAX have prefixes for the Limit Names
				elemPrefix = "";
				if (form.elements[i+1] != null && form.elements[i+1].name.indexOf(limitName[0] + suffix) != -1)
				{
					name = form.elements[i+1].name;
					elemPrefix = name.substring(0, name.indexOf( limitName[0] + suffix ));
				}
				if (form.elements[elemPrefix + limitName[0] + suffix] != null) {
					prevMin = 0;
					prevMinPeriod = -1;
					for (j = 0; j < 4; j++) {
						amountString = form.elements[elemPrefix + limitName[j] + suffix].value;
						if (amountString.length > 0) {
							amount = parseFloat( amountString );
							if (isNaN(amount)) {
								alert( "<s:text name="jsp.user_237"/> " + limitPeriod[j] + ((elemPrefix != "")?" ("+elemPrefix+")":"") + " limit for " + form.elements[i].value + " <s:text name="jsp.user_179"/>" );
								return false;
							}
							if (amount < 0) {
								alert( "<s:text name="jsp.user_237"/> " + limitPeriod[j] + ((elemPrefix != "")?" ("+elemPrefix+")":"") + " limit for " + form.elements[i].value + " <s:text name="jsp.user_180"/>" );
								return false;
							}

							if (prevMinPeriod == -1 || amount > prevMin) { //this is first one with data, or this amount > previous amount
								prevMinPeriod = j;
								prevMin = amount;
							} else {
								if (amount < prevMin) {
									alert( "<s:text name="jsp.user_237"/> " + limitPeriod[prevMinPeriod] + ((elemPrefix != "")?" ("+elemPrefix+")":"") + " limit for " + form.elements[i].value + " <s:text name="jsp.user_182"/> " + limitPeriod[j] + ((elemPrefix != "")?" ("+elemPrefix+")":"") + " <s:text name="jsp.user_187"/>" );
									return false;
								}
							}
						}
					}
				}
			}
		}

		return true;
	}

function getAmountAsNum ( amt, currencyCode ) 
{
	var currRe="";
	var ch;
	var currencySymbol="";
	var index;

	if ( amt.length <= 0 ) {
		return "0";
	}

	if ( currencyCode == "JPY" ) {
		currRe = /^(|\+|-)(|¥)([1-9]((\d)*|(|\d|\d\d)(,\d\d\d)*)|0)$/;
		currencySymbol = "¥";
	} else {
		currRe = /^(|\+|-)(|\$)(|[1-9]((\d)*|(|\d|\d\d)(,\d\d\d)*)|0)(|\.\d(|\d))$/;
		currencySymbol = "$";
	}

	if ( !currRe.test ( amt ) ) {
		return NaN;
	} else {
		// Remove currency symbol
		index = amt.indexOf( currencySymbol );
		if ( index >= 0 ) {
			if ( index == 0 ) {
				amt = amt.substring (1);
			} else {
				amt = amt.substring ( 0, index ) + amt.substring ( index+1 );
			}
		}

		// Remove a leading '+'
		if ( amt.charAt(0) == '+' ) {
			amt = amt.substring(1);
		}

		//Make the valid amount into a number without ',' in it
		for ( var i = 0; i < amt.length; i++ ) {
			ch = amt.charAt (i);
			
			if ( ch == ',' ) {
				amt = amt.substring(0,i) + amt.substring(i+1);
			}
		}
	}

	return parseFloat ( amt );
}

function invalidAmountMessage ( amountType, currencyCode )
{
	if ( currencyCode == "JPY" ) {
		alert ( amountType + " <s:text name="jsp.user_183"/>" );
	} else {
		alert ( amountType + " <s:text name="jsp.default_253"/>" );
	}
}

function negativeAmountMessage ( amountType )
{
	alert ( amountType + " <s:text name="jsp.user_200"/>" );
}
// --></script>
