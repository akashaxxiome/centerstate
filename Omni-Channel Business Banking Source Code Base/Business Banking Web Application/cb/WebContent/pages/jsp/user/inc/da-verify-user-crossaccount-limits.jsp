<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<ffi:object name="com.ffusion.tasks.admin.GetMaxCrossAccountLimits" id="GetMaxCrossAccountLimits" scope="session"/>
	<ffi:setProperty name="GetMaxCrossAccountLimits" property="EntitlementTypePropertyListsName" value="NonAccountEntitlementsWithLimits"/>
	<ffi:setProperty name="GetMaxCrossAccountLimits" property="DualApprovalMode" value="${Business.dualApprovalMode}"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetMaxCrossAccountLimits" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	    <ffi:setProperty name="GetMaxCrossAccountLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	    <ffi:setProperty name="GetMaxCrossAccountLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	    <ffi:setProperty name="GetMaxCrossAccountLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			<ffi:setProperty name="GetMaxCrossAccountLimits" property="GroupId" value="${EditGroup_GroupId}" />
	</s:if>
	<ffi:setProperty name="GetMaxCrossAccountLimits" property="PerTransactionMapName" value="PerTransactionLimits"/>
	<ffi:setProperty name="GetMaxCrossAccountLimits" property="PerDayMapName" value="PerDayLimits"/>
	<ffi:setProperty name="GetMaxCrossAccountLimits" property="PerWeekMapName" value="PerWeekLimits"/>
	<ffi:setProperty name="GetMaxCrossAccountLimits" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxCrossAccountLimits"/>
<ffi:removeProperty name="GetMaxCrossAccountLimits"/>

<ffi:object id="GetMaxLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxLimitForPeriod" scope="session" />
	<ffi:setProperty name="GetMaxLimitForPeriod" property="PerTransactionMapName" value="PerTransactionLimits"/>
	<ffi:setProperty name="GetMaxLimitForPeriod" property="PerDayMapName" value="PerDayLimits"/>
	<ffi:setProperty name="GetMaxLimitForPeriod" property="PerWeekMapName" value="PerWeekLimits"/>
	<ffi:setProperty name="GetMaxLimitForPeriod" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxLimitForPeriod"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="NoLimitString" value="no"/>

<ffi:object id="CheckForRedundantDALimits" name="com.ffusion.tasks.dualapproval.CheckForRedundantDALimits" scope="session" />
	<ffi:setProperty name="CheckForRedundantDALimits" property="LimitListName" value="NonAccountEntitlementsWithLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MaxPerTransactionLimitMapName" value="PerTransactionLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MaxPerDayLimitMapName" value="PerDayLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MaxPerWeekLimitMapName" value="PerWeekLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MaxPerMonthLimitMapName" value="PerMonthLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MergedListName" value="NonAccountEntitlementsMerged"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="categorySessionName" value="CATEGORY_BEAN"/>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="CheckForRedundantDALimits" property="GroupId" value="${EditGroup_GroupId}"/>
	</s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="CheckForRedundantDALimits" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
		<ffi:setProperty name="CheckForRedundantDALimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="CheckForRedundantDALimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="CheckForRedundantDALimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	</s:if>

<ffi:process name="CheckForRedundantDALimits"/>

<%-- Requires Approval validation --%>
<ffi:object id="DASettings" name="com.ffusion.tasks.dualapproval.GetDASettings" />
<ffi:process name="DASettings" />
<ffi:removeProperty name="DASettings" />
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
	<ffi:object id="ValidateRequiresApprovalDA" name="com.ffusion.tasks.dualapproval.ValidateRequiresApprovalDA" />
	<ffi:setProperty name="ValidateRequiresApprovalDA" property="CategorySessionName" value="<%=com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.CATEGORY_BEAN %>"/>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="ValidateRequiresApprovalDA" property="GroupId" value="${EditGroup_GroupId}"/>
	</s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="ValidateRequiresApprovalDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
		<ffi:setProperty name="ValidateRequiresApprovalDA" property="ModifyUserOnly" value="true"/>
		<ffi:setProperty name="ValidateRequiresApprovalDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="ValidateRequiresApprovalDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="ValidateRequiresApprovalDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	</s:if>
	
	<ffi:process name="ValidateRequiresApprovalDA" />
	<ffi:removeProperty name="ValidateRequiresApprovalDA" />
</ffi:cinclude>
<%-- END Requires Approval validation --%>

