<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%@page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<ffi:object id="EditCrossAccountPermissionsDA" name="com.ffusion.tasks.dualapproval.EditCrossAccountPermissionsDA" scope="session"/>
<ffi:object id="EditRequiresApprovalDA" name="com.ffusion.tasks.dualapproval.EditRequiresApprovalDA" scope="session"/>

<ffi:setProperty name="GetCategories" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetCategories" property="itemType" value="${itemType}"/>
<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>

<ffi:setProperty name="EditCrossAccountPermissionsDA" property="approveAction" value="true" />
<ffi:setProperty name="EditCrossAccountPermissionsDA" property="EntitlementTypesMerged" value="AccountEntitlementsMerged"/>
<ffi:setProperty name="EditCrossAccountPermissionsDA" property="EntitlementTypePropertyLists" value="LimitsList"/>
<ffi:setProperty name="EditCrossAccountPermissionsDA" property="DirectoryIdForAccounts" value="${Business.Id}"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditCrossAccountPermissionsDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditCrossAccountPermissionsDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditCrossAccountPermissionsDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditCrossAccountPermissionsDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="EditCrossAccountPermissionsDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>

<ffi:setProperty name="EditRequiresApprovalDA" property="approveAction" value="true" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditRequiresApprovalDA" property="ModifyUserOnly" value="true" />
	<ffi:setProperty name="EditRequiresApprovalDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="EditRequiresApprovalDA" property="ModifyUserOnly" value="false" />
	<ffi:setProperty name="EditRequiresApprovalDA" property="GroupId" value="${EditGroup_GroupId}" />
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberType" value="${itemType}"/>
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberId" value="${itemId}"/>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="EditRequiresApprovalDA" property="GroupId" value="${Business.EntitlementGroupId}" />
	</ffi:cinclude>
</s:if>

<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithLimitsName" value="AccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithoutLimitsName" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithLimitsName" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithoutLimitsName" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountMerged" value="NonAccountEntitlementsMergedBeforeFilter"/>

<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="NonAccountEntitlementsMergedBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="NonAccountEntitlementsMerged"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${EntitlementGroupParentId}"/>

<ffi:setProperty name="GetCategories" property="ObjectId" value=""/>
<ffi:setProperty name="GetCategories" property="ObjectType" value=""/>

<ffi:cinclude value1="${CheckRequiresApproval}" value2="">
	<%--CheckRequiresApproval instance is required in session and is used by EditRequiresApprovalDA task when approving permissions --%>
	<ffi:object id="CheckRequiresApproval" name="com.ffusion.tasks.dualapproval.CheckRequiresApproval" />
</ffi:cinclude>

<%--Get the list of component names for cross account category.  --%>
<ffi:object id="GetComponentsPerCategoryDA" name="com.ffusion.tasks.dualapproval.GetComponentsPerCategoryDA" />
	<ffi:setProperty name="GetComponentsPerCategoryDA" property="perCategory" 
									value="<%=EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT %>"/>
<ffi:process name="GetComponentsPerCategoryDA"/>

<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
	<ffi:setProperty name="GetDACategorySubType" property="perCategory" 
							value="<%=EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT %>"/>

<ffi:setProperty name="approveCategory" property="ObjectId" value="" />
<ffi:setProperty name="approveCategory" property="ObjectType" value="" />
<%String catSubType; %>		
<%-- ComponentsPerCategoryNames is a list in session set using GetComponentsPerCategoryDA task.--%>
<ffi:list collection="ComponentsPerCategoryNames" items="component" >
	<%--Get the categorySubType using perCategory and component value. --%>
	<ffi:setProperty name="GetDACategorySubType" property="componentName" value="${component}"/>
	<ffi:process name="GetDACategorySubType"/>
	
	<ffi:setProperty name="GetTypesForEditingLimits" property="ComponentValue" value="${component}"/>
	<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME %>" assignTo="catSubType"/>
	<ffi:setProperty name="GetCategories" property="categorySubType" value="${catSubType}"/>
	<ffi:setProperty name="approveCategory" property="categorySubType" value="" />
	<ffi:process name="GetTypesForEditingLimits"/>
	<ffi:removeProperty name="categories" />
	<ffi:process name="GetCategories" />
	<ffi:cinclude value1="${categories}" value2="" operator="notEquals">
		<ffi:process name="FilterEntitlementsForBusiness"/>
		<ffi:process name="EditCrossAccountPermissionsDA" />
		<ffi:process name="EditRequiresApprovalDA" />
		<ffi:list collection="categories" items="category" startIndex="1" endIndex="1">
			<ffi:setProperty name="ApprovePendingChanges" property="CategorySubType" value="${catSubType}"/>
			<ffi:setProperty name="ApprovePendingChanges" property="ApproveBySubType" value="true"/>
			<ffi:process name="ApprovePendingChanges"/>
		</ffi:list>
	</ffi:cinclude>
</ffi:list>

<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" />
<ffi:removeProperty name="catSubType" />
<ffi:removeProperty name="EditCrossAccountPermissionsDA" />
<ffi:removeProperty name="EditRequiresApprovalDA" />