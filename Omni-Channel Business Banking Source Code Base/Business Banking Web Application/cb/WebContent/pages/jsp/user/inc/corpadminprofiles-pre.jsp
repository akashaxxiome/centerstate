<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:action namespace="/pages/user" name="getPendingApprovalBusinessEntitlementProfiles_getPendingRecCount"/>      

<% if (request.getAttribute("pendingDAProfileCount") != null &&  request.getAttribute("pendingDAProfileCount").equals("0")) {
	%>
	 <script>
	  $('#admin_profilesPendingApprovalTab').hide();</script>
<%} else {%>
 <script>
 	 $('#admin_profilesPendingApprovalTab').show();
	</script>
<%}%>
