<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- SESSION CLEANUP --%>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="account" operator="equals">
	<ffi:include page="${PathExt}inc/accountSessionCleanup.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="cash" operator="equals">
	<ffi:include page="${PathExt}inc/cashSessionCleanup.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="home" operator="equals">
	<ffi:include page="${PathExt}inc/homeSessionCleanup.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="payments" operator="equals">
	<ffi:include page="${PathExt}inc/paymentsSessionCleanup.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="reports" operator="equals">
	<ffi:include page="${PathExt}inc/reportsSessionCleanup.jsp"/>
</ffi:cinclude>

<%
/**
 * Used to clear the 'verified' session variable if user navigates away from the User Profile page after having
 * authentiacated. This will ensure that that user is prompted for authentication each time they try to go back to 
 * the User Profile page after being on any other page.
 */
%> 
<ffi:cinclude value1="${profileSubMenu}" value2="true" operator="equals">
    <ffi:cinclude value1="${subMenuSelected}" value2="user profile" operator="notEquals">	
        <ffi:include page="${PathExt}inc/profileSessionCleanup.jsp" />
    </ffi:cinclude>	
</ffi:cinclude>

<ffi:setL10NProperty name="SessionCleanupCurrTab" value="admin"/>
<%-- END OF SESSION CLEANUP --%>

<s:include value="/pages/jsp/inc/init/menu-init.jsp"/>

<table border="0" cellpadding="0" cellspacing="0" width="750">
    
    <ffi:cinclude value1="${nav_menu_sub2}" value2="" operator="notEquals">
    <tr>
        <td colspan="3" valign="top" style="width: 750; height: 23;"><s:include value="/pages/jsp/user/inc/%{#session.nav_menu_sub2}"/></td>
    </tr>
    </ffi:cinclude>
        <ffi:cinclude value1="${nav_menu_sub3}" value2="" operator="notEquals">
        <tr>
            <td colspan="3" valign="top" style="width: 750; height: 23;"><ffi:include page="${PathExt}user/inc/${nav_menu_sub3}"/></td>
        </tr>
    </ffi:cinclude>
</table>
<ffi:removeProperty name="sub-menu-payments" />
<ffi:removeProperty name="sub-menu-user" />
<ffi:removeProperty name="sub-menu-reports" />
<ffi:removeProperty name="sub-menu-home" />
<ffi:removeProperty name="sub-menu-cash" />
<ffi:removeProperty name="sub-menu-account" />
<ffi:removeProperty name="PageText2" />
<ffi:removeProperty name="PageText_clone_user_permission" />
<ffi:removeProperty name="PageText_clone_account_permission" />
<br>

