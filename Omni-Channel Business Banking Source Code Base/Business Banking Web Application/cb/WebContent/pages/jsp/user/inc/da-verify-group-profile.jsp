
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<ffi:cinclude value1="${isProfileChanged}" value2="Y">
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP%>" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}" />
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>" />
	<ffi:process name="GetDACategoryDetails" />

<div class="blockHead"><s:text name="user.profiles.summary"/></div>
	<div class="paneWrapper">
  		<div class="paneInnerWrapper">
  		<div class="header">
			<table width="750" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td class="sectionsubhead adminBackground" width="110"><s:text name="jsp.da_approvals_pending_user_FieldName_column" /></td>
					<td class="sectionsubhead adminBackground" width="110"><s:text name="jsp.da_approvals_pending_user_OldValue_column" /></td>
					<td class="sectionsubhead adminBackground" width="110"><s:text name="jsp.da_approvals_pending_user_NewValue_column" /></td>
				</tr>
			</table>
			</div>
			<table width="750" border="0" cellspacing="0" cellpadding="3">
				
				<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
					<ffi:cinclude value1="${details.fieldName}" value2="parentId">
						<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
						<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
						    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${details.newValue}" />
						<ffi:process name="GetAdministratorsForGroups"/>
						<ffi:removeProperty name="GetAdministratorsForGroups"/>
						
						<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
						  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
						<ffi:process name="GetBusinessEmployeesByEntGroups"/>
						<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
						
						<ffi:object id="GroupProfileValidationDA" name="com.ffusion.tasks.dualapproval.GroupProfileValidationDA" />
						<ffi:setProperty name="GroupProfileValidationDA" property="UserAdminEmployeesOfNewDiv" value="BusinessEmployees"/>
						<ffi:setProperty name="GroupProfileValidationDA" property="CategoryBeanSessionName" value="CATEGORY_BEAN"/>
						<ffi:process name="GroupProfileValidationDA"/>
						<ffi:removeProperty name="GroupProfileValidationDA"/>
						
						<ffi:removeProperty name="BusinessEmployees"/>
						<ffi:removeProperty name="EntitlementAdmins"/>
					</ffi:cinclude>
				</ffi:list>
				
				<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
						<ffi:cinclude value1="${details.fieldName}" value2="groupName">
							<tr>
								<td class="columndata" width="110">
									<ffi:getL10NString rsrcFile="cb" msgKey="da.field.group.${details.fieldName}" /></td>
								<td  class="columndata" width="110">
									<ffi:getProperty name="details" property="oldValue" />
								</td>
								<td  class="columndata" width="110">
									<span class="errorcolor"><ffi:getProperty name="details" property="newValue" /></span>
								</td>
							</tr>
						</ffi:cinclude>
						<ffi:cinclude value1="${details.fieldName}" value2="parentId">
							<tr>
								<td class="columndata" width="110">
									<ffi:getL10NString rsrcFile="cb" msgKey="da.field.group.division" /></td>
								<td class="columndata" width="110">
									<ffi:cinclude value1="${details.oldValue}" value2="" operator="notEquals">
										<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
										<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${details.oldValue}'/>
										<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='ParentEntitlementGroup'/>
										<ffi:process name="GetEntitlementGroup"/>
										<ffi:getProperty name="ParentEntitlementGroup" property="GroupName" />
										<ffi:removeProperty name="GetEntitlementGroup"/>
									</ffi:cinclude>
								</td>
								<td class="columndata" width="110">
									<span class="errorcolor"><ffi:cinclude value1="${details.newValue}" value2="" operator="notEquals">
										<ffi:object id="GetEntitlementGroup" name="com.ffusion.tasks.dualapproval.GetEntitlementGroupDA" />
										<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${details.newValue}'/>
										<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='ParentEntitlementGroup'/>
										<ffi:setProperty name="GetEntitlementGroup" property="DetailSessionName" value="details" />
										<ffi:process name="GetEntitlementGroup"/>
										<ffi:getProperty name="ParentEntitlementGroup" property="GroupName" />
										<ffi:removeProperty name="GetEntitlementGroup"/>
									</ffi:cinclude></span>
								</td>
								<ffi:removeProperty name="ParentEntitlementGroup"/>
							</tr>
							<%-- Validation Errors --%>
							<ffi:cinclude value1="${details.error}" value2="" operator="notEquals">
							<ffi:setProperty name="DISABLE_APPROVE" value="true"/>
							<tr>
								<td class="columndataDANote" colspan="3"><ffi:getProperty name="details" property="error" /></td>
							</tr>
						</table>
					</div>
				</div>			
				</ffi:cinclude>
				<%-- END Validation Errors --%>
			</ffi:cinclude>
	</ffi:list>
</ffi:cinclude>
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="GetDACategoryDetails"/>
