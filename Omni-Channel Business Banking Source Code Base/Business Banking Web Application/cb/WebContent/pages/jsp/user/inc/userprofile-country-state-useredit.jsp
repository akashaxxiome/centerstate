<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<script language="javascript">
$(function(){
	$("#stateForUserSelectID").selectmenu({width:'23em'});
});
</script>

<%
    String userState = null;
%>

<!-- default selected country and state -->
<ffi:getProperty name="NewBusinessEmployee" property="State" assignTo="userState"/>
<% if (userState == null) { userState = ""; } %>

<ffi:cinclude value1="${StatesExists}" value2="true" operator="equals">
	<select class="txtbox" id="stateForUserSelectID" name="State" value="<ffi:getProperty name="NewBusinessEmployee" property="State"/>">
		<option<ffi:cinclude value1="<%= userState %>" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.default_376"/></option>
		<ffi:list collection="StateList" items="item">
			<option <ffi:cinclude value1="<%= userState %>" value2="${item.StateKey}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="StateKey"/>"><ffi:getProperty name='item' property='Name'/></option>
		</ffi:list>
	</select>
	<input type="hidden" name="stateHidden" value="false"/>
</ffi:cinclude>
<ffi:cinclude value1="${StatesExists}" value2="true" operator="notEquals">
	<ffi:setProperty name="NewBusinessEmployee" property="State" value=""/>
	<input type="hidden" name="stateHidden" value="true"/>
</ffi:cinclude>