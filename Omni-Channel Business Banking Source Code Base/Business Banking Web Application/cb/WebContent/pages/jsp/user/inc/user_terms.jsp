<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>


<div id="secondaryUsersTermsDialog" class="staticContentDialog">
<form name="TermsForm" action="" method="post">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<div  class="instructions">
		<s:text name="secondaryUser.termsDialog.message"/>
	</div>
	<br/><br/><br/>
    <div  class="ui-widget-header customDialogFooter">
		<sj:a id="cancelTermsSecondaryUsers" 
		button="true" 
		summaryDivId="summary" 
		buttonType="cancel" 
		onClickTopics="showSummary"><s:text name="termsAndConditions.buttonDecline"/></sj:a>	
		<sj:a id="acceptTermsSecondaryUsers" button="true"><s:text name="termsAndConditions.buttonAccept"/></sj:a>	
		<!--
        <input name="CancelButton" type="submit" value="CANCEL" class="form_buttons" onclick="document.TermsForm.action='<ffi:getProperty name="SecurePath"/>user/user_list.jsp';">
        <input name="AgreeButton" type="submit" value="I AGREE" class="form_buttons" onclick="document.TermsForm.action='<ffi:getProperty name="SecurePath"/>user/user_terms_save.jsp';">
		-->
    </div>
</form>
</div>