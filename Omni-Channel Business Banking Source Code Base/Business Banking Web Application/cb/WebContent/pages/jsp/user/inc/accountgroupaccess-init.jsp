<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- This file was removed from accountgroupaccess.jsp to keep the page from getting too big to compile --%>

<%-- Get the BusinessEmployee  --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:process name="SetBusinessEmployee"/>
</s:if>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_REQUIRE_APPROVAL %>"/>
</ffi:cinclude>
<%-- Dual Approval Processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<s:if test="%{#session.Section == 'Users'}">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</s:if>
	<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${Business.Id}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Business.Id}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
		<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}"/>
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetCategories"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_GROUP_ACCESS %>" />
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="${GetDACategoryDetails.categorySubType}"/>
	</ffi:cinclude>
</ffi:cinclude>


<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
 	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	    <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupAccessPermissionsDA" scope="session"/>
			<ffi:setProperty name="EditAccountGroupAccessPermissions" property="approveAction" value="false"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.admin.EditAccountGroupAccessPermissions" scope="session"/>
		</ffi:cinclude>
    </s:if>

	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="EditAccountGroupAccessPermissions" property="ProfileId" value="${Business.Id}"/>
	</ffi:cinclude>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
				<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupAccessPermissionsDA" scope="session"/>
				<ffi:setProperty name="EditAccountGroupAccessPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
				<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.admin.EditAccountGroupAccessPermissions" scope="session"/>
			</ffi:cinclude>			
			<!--  4 for normal profile, 5 for primary profile, 6t for sec profile, 7 for cross profile -->
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupAccessPermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.admin.EditAccountGroupAccessPermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditAccountGroupAccessPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupAccessPermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.admin.EditAccountGroupAccessPermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditAccountGroupAccessPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="5" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupAccessPermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.admin.EditAccountGroupAccessPermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditAccountGroupAccessPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="6" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.dualapproval.EditAccountGroupAccessPermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.admin.EditAccountGroupAccessPermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditAccountGroupAccessPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:object id="EditAccountGroupAccessPermissions" name="com.ffusion.tasks.admin.EditAccountGroupAccessPermissions" scope="session"/>
		</ffi:cinclude>
		<ffi:setProperty name="EditAccountGroupAccessPermissions" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="EditAccountGroupAccessPermissions" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="EditAccountGroupAccessPermissions" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
	<ffi:setProperty name="EditAccountGroupAccessPermissions" property="GroupId" value="${EditGroup_GroupId}"/>
	<ffi:setProperty name="EditAccountGroupAccessPermissions" property="SaveLastRequest" value="false"/>
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>

<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
    <ffi:setProperty name="GetGroupLimits" property="GroupId" value="${EditGroup_GroupId}"/>
    <ffi:setProperty name="GetGroupLimits" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
    <ffi:setProperty name="GetGroupLimits" property="ObjectType" value="AccountGroup"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>

<ffi:cinclude value1="${runSearch}" value2="">
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:object id="GetRestrictedEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:process name="GetRestrictedEntitlements"/>
		<ffi:removeProperty name="GetRestrictedEntitlements"/>

		<ffi:object id="GetAccountGroupsForGroup" name="com.ffusion.tasks.admin.GetAccountGroupsForGroup" scope="session"/>
		<ffi:setProperty name="GetAccountGroupsForGroup" property="GroupID" value="${EditGroup_GroupId}"/>
		<ffi:setProperty name="GetAccountGroupsForGroup" property="BusDirectoryId" value="${Business.Id}"/>
		<ffi:setProperty name="GetAccountGroupsForGroup" property="GroupAccountGroupsName" value="ParentsAccountGroups"/>
		<ffi:setProperty name="GetAccountGroupsForGroup" property="CheckAdminAccess" value="true"/>
		<ffi:process name="GetAccountGroupsForGroup"/>
	</s:if>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
		<ffi:process name="GetEntitlementGroup"/>

		<ffi:object id="GetRestrictedEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:process name="GetRestrictedEntitlements"/>
		<ffi:removeProperty name="GetRestrictedEntitlements"/>

		<ffi:object id="GetAccountGroupsForGroup" name="com.ffusion.tasks.admin.GetAccountGroupsForGroup" scope="session"/>
		<ffi:setProperty name="GetAccountGroupsForGroup" property="GroupID" value="${Entitlement_EntitlementGroup.ParentId}"/>
		<ffi:setProperty name="GetAccountGroupsForGroup" property="BusDirectoryId" value="${Business.Id}"/>
		<ffi:setProperty name="GetAccountGroupsForGroup" property="CheckAdminAccess" value="true"/>
		<ffi:setProperty name="GetAccountGroupsForGroup" property="GroupAccountGroupsName" value="ParentsAccountGroups"/>

		<ffi:process name="GetAccountGroupsForGroup"/>
	</s:if>

	<ffi:setProperty name="EditAccountGroupAccessPermissions" property="AccountGroupsName" value="ParentsAccountGroups"/>

	<ffi:setProperty name="GetAccountGroupsForGroup" property="GroupAccountGroupsName" value="ParentsAccountGroups"/>

	<ffi:setProperty name="ParentsAccountGroups" property="SortedBy" value="NAME"/>
</ffi:cinclude>

<ffi:object id="ApprovalAdminByBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
<ffi:setProperty name="ApprovalAdminByBusiness" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="ApprovalAdminByBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.APPROVALS_ADMIN %>"/>
<ffi:process name="ApprovalAdminByBusiness"/>

<ffi:cinclude value1="${ParentsAccountGroups}" value2="" operator="notEquals">
	<ffi:object name="com.ffusion.tasks.admin.GetMaxAccountGroupAccessLimits" id="GetMaxAccountGroupAccessLimits" scope="session"/>
	<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="AccountGroupsName" value="ParentsAccountGroups"/>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	</s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
		<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
	<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="PerTransactionMapName" value="PerTransactionLimits"/>
	<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="PerDayMapName" value="PerDayLimits"/>
	<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="PerWeekMapName" value="PerWeekLimits"/>
	<ffi:setProperty name="GetMaxAccountGroupAccessLimits" property="PerMonthMapName" value="PerMonthLimits"/>
	<ffi:process name="GetMaxAccountGroupAccessLimits"/>
	<ffi:removeProperty name="GetMaxAccountGroupAccessLimits"/>
</ffi:cinclude>

<ffi:object id="GetMaxLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxLimitForPeriod" scope="session" />
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerTransactionMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerDayMapName" value="PerDayLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerWeekMapName" value="PerWeekLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxLimitForPeriod"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="NoLimitString" value="no"/>

<ffi:object id="CheckForRedundantLimits" name="com.ffusion.efs.tasks.entitlements.CheckForRedundantLimits" scope="session" />
<ffi:setProperty name="CheckForRedundantLimits" property="LimitListName" value="ParentsAccountGroups"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerTransactionLimitMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerDayLimitMapName" value="PerDayLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerWeekLimitMapName" value="PerWeekLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerMonthLimitMapName" value="PerMonthLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="EntitlementPrefix" value="account_group"/>
<ffi:setProperty name="CheckForRedundantLimits" property="SuccessURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/accountgroupaccess-verify-init.jsp?PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
<ffi:setProperty name="CheckForRedundantLimits" property="BadLimitURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/accountgroupaccess.jsp&UseLastRequest=TRUE&DisplayErrors=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/>
<ffi:setProperty name="CheckForRedundantLimits" property="GroupId" value="${EditGroup_GroupId}"/>
<ffi:setProperty name="CheckForRedundantLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
</s:if>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"/>
	<ffi:process name="GetDACategoryDetails" />

	<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>"/>
	<ffi:process name="GetDACategoryDetails" />
</ffi:cinclude>


<ffi:object id="GetEntitlementTypePropertyList" name="com.ffusion.efs.tasks.entitlements.GetEntitlementTypePropertyList"/>
<ffi:setProperty name="GetEntitlementTypePropertyList" property="ListName" value="Access"/>
<ffi:setProperty name="GetEntitlementTypePropertyList" property="TypeToLookup" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
<ffi:process name="GetEntitlementTypePropertyList"/>

<ffi:cinclude value1="${ParentsAccountGroups}" value2="" operator="notEquals">
	<ffi:object id="HandleAccountGroupAccessRowDisplay" name="com.ffusion.tasks.admin.HandleAccountGroupAccessRowDisplay"/>
	<ffi:setProperty name="HandleAccountGroupAccessRowDisplay" property="AccountGroupsName" value="ParentsAccountGroups"/>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="HandleAccountGroupAccessRowDisplay" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	</s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="HandleAccountGroupAccessRowDisplay" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
		<ffi:setProperty name="HandleAccountGroupAccessRowDisplay" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="HandleAccountGroupAccessRowDisplay" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="HandleAccountGroupAccessRowDisplay" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
	<ffi:process name="HandleAccountGroupAccessRowDisplay"/>
</ffi:cinclude>