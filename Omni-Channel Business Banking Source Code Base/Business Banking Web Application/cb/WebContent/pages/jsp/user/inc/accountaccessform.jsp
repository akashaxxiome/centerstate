<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function applyAll(updateId)
	{
		$("#applyAllId").val(updateId);
		$('#accountAccessPermissionsApplyAllButtonID').click();

	}
</script>
<div id="applyAllAccountAccessPermissionsDiv">
<s:form id="accountAccessPermissionApplyAllForm" namespace="/pages/user" action="accountAccessPermissionsApplyAll" theme="simple" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input id="applyAllId" type="hidden" name="applyAllId" value=""/>
<div align="left" class="marginBottom10 langSelectionSeperator marginTop10">Apply Limits to all - <ffi:getProperty name="Context"/></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">
			<%-- <tr>
				<td colspan="6" class="sectionsubhead" height="20"><s:text name="jsp.user.apply.all.camel"/></td>
			</tr> --%>
			<tr class="header">
			<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
				<td class="sectionsubhead" width="5%"><s:text name="jsp.user_32"/></td>
			</ffi:cinclude>
			<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="equals">
				<td class="sectionsubhead" width="5%">&nbsp;</td>
			</ffi:cinclude>
				<td class="sectionsubhead" width="5%"><s:text name="jsp.user_177"/></td>
				<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
				<ffi:process name="GetLimitBaseCurrency" />
				<td class="sectionsubhead" align="" width="25%"><s:text name="jsp.default_261"/>(<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)</td>
				<td class="sectionsubhead" align="center" width="20%">
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
						<s:text name="jsp.user_151"/> <s:text name="jsp.user_386"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
						&nbsp;
					</ffi:cinclude>
				</td>
				<td class="sectionsubhead" align="" width="25%">
					<s:text name="jsp.default_261"/>(<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
				</td>
				<td class="sectionsubhead" align="center" width="20%">
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
						<s:text name="jsp.user_151"/> <s:text name="jsp.user_386"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
						&nbsp;
					</ffi:cinclude>
				</td>
			</tr>
			<tr>
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
					<td class="sectionsubhead"><input id="adminCheck" name="admin" type="checkbox" value="true" onclick="applyAll('admin');"></td>
				</ffi:cinclude>
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="equals">
					<td class="sectionsubhead">&nbsp;</td>
				</ffi:cinclude>
				<td class="sectionsubhead" align=""><input id="initCheck" name="init" type="checkbox" value="true" onclick="applyAll('init');"></td>
				<td class="sectionsubhead">
					<input name="transactionLimit" class="ui-widget-content ui-corner-all" type="text" style="width:60px;" size="12" maxlength="17" onchange="applyAll('transactionLimit');">
					<span align="left" nowrap class="columndata"><s:text name="jsp.user_244"/></span>
				</td>
				<td class="sectionsubhead" align="center">
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
						<input name="transactionExceed" type="checkbox" value="true" onclick="applyAll('transactionExceed');">
					</ffi:cinclude>
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
						&nbsp;
					</ffi:cinclude>
				</td>
				<td class="sectionsubhead">
					<input name="dayLimit" class="ui-widget-content ui-corner-all" type="text" style="width:60px;" size="12" maxlength="17" onchange="applyAll('dayLimit');">
					<span align="left" nowrap class="columndata"><s:text name="jsp.user_242"/></span>
				</td>
				<td class="sectionsubhead" align="center">
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
						<input name="dayExceed" type="checkbox" value="true" onclick="applyAll('dayExceed');">
					</ffi:cinclude>
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
						&nbsp;
					</ffi:cinclude>
				</td>
			</tr>
			<tr>
				<td class="sectionsubhead" colspan="2">&nbsp;</td>
				<td class="sectionsubhead">
					<input name="weekLimit" class="ui-widget-content ui-corner-all" type="text" style="width:60px;" size="12" maxlength="17" onchange="applyAll('weekLimit');">
					<span align="left" nowrap class="columndata"><s:text name="jsp.user_245"/></span>
				</td>
				<td class="sectionsubhead" align="center">
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
						<input name="weekExceed" type="checkbox" value="true" onclick="applyAll('weekExceed');">
					</ffi:cinclude>
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
						&nbsp;
					</ffi:cinclude>
				</td>
				<td class="sectionsubhead">
					<input name="monthLimit" class="ui-widget-content ui-corner-all" type="text" style="width:60px;" size="12" maxlength="17" onchange="applyAll('monthLimit');">
					<span align="left" nowrap class="columndata"><s:text name="jsp.user_243"/></span>
				</td>
				<td class="sectionsubhead" align="center">
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
						<input name="monthExceed" type="checkbox" value="true" onchange="applyAll('monthExceed');">
					</ffi:cinclude>
					<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
						&nbsp;
					</ffi:cinclude>
				</td>
			</tr>
			<tr>
				<td colspan="6" class="sectionsubhead" align="center">
					<div class="hidden">
					<sj:a
						id="accountAccessPermissionsApplyAllButtonID"
						formIds="accountAccessPermissionApplyAllForm"
						targets="resultmessage"
						button="true"
						validate="true"
						validateFunction="customValidation" 
						onBeforeTopics="beforeAccountAccessPermissionsApplyAll"
						onCompleteTopics="confirmAccountAccessPermissionsApplyAll"
						onErrorTopics="errorAccountAccessPermissionsApplyAll"
						onSuccessTopics="successAccountAccessPermissionsApplyAll">
						<s:text name="jsp.user.apply.all"/></sj:a>
					</div>
				</td>
			</tr>
		</table></div></div>
	</s:form>
</div>

<ffi:removeProperty name="GetLimitBaseCurrency"/>
