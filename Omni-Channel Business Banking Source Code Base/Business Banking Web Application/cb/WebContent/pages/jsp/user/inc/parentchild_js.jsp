<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script language="JavaScript" type="text/javascript"><!--

function handleClick( theForm, checkboxClicked )
{
	if( checkboxClicked.checked ) {
		// A box was checked.  Check it's parents.
		var posInArray = findInArray( childParentArray, checkboxClicked.name );

		if( posInArray == -1 ) return;

		// Find the element on the form.
		for( var j = 0; j < theForm.length; j++ ) {

			var foundPrefix = false;
			for( var prefixIndex = 0; prefixIndex < prefixes.length; prefixIndex++ ) {
				if( theForm.elements[j].name.indexOf( prefixes[prefixIndex] ) == 0   ) {
					foundPrefix = true;
					break;
				}
			}

			if( !foundPrefix ) continue;

			for( var i = 1; i < childParentArray[posInArray].length; i++ ) {

				if( ( theForm.elements[j].name == childParentArray[posInArray][i] ) ) {
					// Found it.  Now check it.
					theForm.elements[j].checked = true;

					// Now that the parent has been checked, we need to recursively
					// check to see if that checkbox's parents also need to be checked.
					handleClick( theForm, theForm.elements[j] );

					break;
				}
			}
		}
	} else {
		// A box was unchecked.  Uncheck it's children.
		var posInArray = findInArray( parentChildArray, checkboxClicked.name );

		if( posInArray == -1 ) return;

		// Find the element on the form.
		for( var j = 0; j < theForm.length; j++ ) {

			var foundPrefix = false;
			for( var prefixIndex = 0; prefixIndex < prefixes.length; prefixIndex++ ) {
				if( theForm.elements[j].name.indexOf( prefixes[prefixIndex] ) == 0   ) {
					foundPrefix = true;
					break;
				}
			}

			if( !foundPrefix ) continue;
			for( var i = 1; i < parentChildArray[posInArray].length; i++ ) {

				if( ( theForm.elements[j].name == parentChildArray[posInArray][i] ) ) {
					// Found it.  Now clear it.
					theForm.elements[j].checked = false;

					// Now that the parent has been cleared, we need to recursively
					// check to see if that checkbox's parents also need to be cleared.
					handleClick( theForm, theForm.elements[j] );

					break;
				}
			}
		}
	}
}

//**************************************************************
// Requires Approval handling ACH Parent Child relationships
//**************************************************************

function handleRAClick( theForm, checkboxClicked )
{
	disableExceedLimitACH (theForm, checkboxClicked);

	if( checkboxClicked.checked ) {
		// A box was unchecked.  Uncheck it's children.
		var posInArray = findInArray( parentChildArray, appr_names[checkboxClicked.name] );

		if( posInArray == -1 ) return;

		// Find the element on the form.
		for( var j = 0; j < theForm.length; j++ ) {
			for( var i = 1; i < parentChildArray[posInArray].length; i++ ) {

				if( ( theForm.elements[j].name == init_names[parentChildArray[posInArray][i]] ) ) {
					// Found it.  Now clear it.
					theForm.elements[j].checked = true;

					// Now that the parent has been cleared, we need to recursively
					// check to see if that checkbox's parents also need to be cleared.
					handleRAClick( theForm, theForm.elements[j] );
					break;
				}
			}
		}

	} else {
	// A box was checked.  Check it's parents.
		var posInArray = findInArray( childParentArray, appr_names[checkboxClicked.name] );
		if( posInArray == -1 ) return;

		// Find the element on the form.
		for( var j = 0; j < theForm.length; j++ ) {
			for( var i = 1; i < childParentArray[posInArray].length; i++ ) {

				if( ( theForm.elements[j].name == init_names[childParentArray[posInArray][i]] ) ) {
					// Found it.  Now check it.
					theForm.elements[j].checked = false;

					// Now that the parent has been checked, we need to recursively
					// check to see if that checkbox's parents also need to be checked.
					handleRAClick( theForm, theForm.elements[j] );

					break;
				}
			}
		}
	}
}



function findInArray( array, toFind )
{
	for( var i = 0; i < array.length; i++ ) {
		if( array[i] == null ) continue;

		if( array[i][0] == toFind ) return i;
	}

	// It's not present.
	return -1;
}

//**************************************************
// 			Requires Approval Scripts
//**************************************************
function disableExceedLimitACH(thisForm, reqApprCheckbox) {
	var limitNames = ach_ra_limits[reqApprCheckbox.name];
	if(limitNames) {
		for(var i=0; i < limitNames.length; i++) {
			var limitName = limitNames[i];
			if(thisForm[limitName]) {
				if(reqApprCheckbox.checked) {
					thisForm[limitName].checked = false;
					thisForm[limitName].disabled = true;
				} else {
					thisForm[limitName].checked = false;
					thisForm[limitName].disabled = false;
				}
			}
		}
	}
}

function disableExceedLimitById(thisForm, reqApprCheckbox, limitId) {
	//handle the wires hierarchy selection
	reqApprCheckboxClicked(thisForm, reqApprCheckbox);
	//disable exceed with approval
	disableExceedLimitByName(thisForm, reqApprCheckbox, limitId);
}

//**************************************************
// Requires Approval disables exceed with approval
//**************************************************
function disableExceedLimitByName(thisForm, reqApprCheckbox, limitId) {
	var name_array = new Array("day_exceed", "transaction_exceed", "week_exceed", "month_exceed");
	for(var i=0; i < name_array.length; i++) {
		var limitName = (name_array[i]+limitId);
		if(thisForm[limitName]) {
			if(thisForm[reqApprCheckbox.name].checked) {
				thisForm[limitName].checked = false;
				thisForm[limitName].disabled = true;
			} else {
				thisForm[limitName].checked = false;
				thisForm[limitName].disabled = false;
			}
		}
	}
}

//**************************************************
//Requires Approval hierarchy selection handler
//**************************************************
function reqApprCheckboxClicked(thisForm, reqApprCheckbox) {
	//if this is parent clicked
	var child_ops = ra_parent_child[reqApprCheckbox.name];
	if(child_ops) {
		//enable all the children
		if(reqApprCheckbox.checked) {
			selectReqApprChildren(thisForm, child_ops);
			selectReqApprParents(thisForm, reqApprCheckbox);
		} //disable all the children
		else if(!reqApprCheckbox.checked) {
			deselectReqApprChildren(thisForm, child_ops);
			deselectReqApprParents(thisForm, reqApprCheckbox);
		}
	} else { //if child clicked
		//select parent if all children are selected
		if(reqApprCheckbox.checked) {
			selectReqApprParents(thisForm, reqApprCheckbox);
		} //deselect parent hierarchy if any of child is deselected
		else if(!reqApprCheckbox.checked) {
			deselectReqApprParents(thisForm, reqApprCheckbox);
		}
	}
}

//**************************************************
//Requires approval select children handler
//**************************************************
function selectReqApprChildren(thisForm, reqApprChildArr) {
	var child_ops = reqApprChildArr;
	for(var i = 0; i < child_ops.length; i++) {
		if(thisForm[child_ops[i]]) {
			if((!thisForm[child_ops[i]].checked) && (!thisForm[child_ops[i]].disabled)) {
				thisForm[child_ops[i]].checked = true;
				disableExceedLimitByName(thisForm, thisForm[child_ops[i]], ra_limits[child_ops[i]]);
			}
			//call recursive if this one has further children
			if(ra_parent_child[child_ops[i]]) {
				selectReqApprChildren(thisForm, ra_parent_child[child_ops[i]]);
			}
		}
	}
}

//**************************************************
//Requires approval deselection children handler
//**************************************************
function deselectReqApprChildren(thisForm, reqApprChildArr) {
	var child_ops = reqApprChildArr;
	for(var i = 0; i < child_ops.length; i++) {
		if(thisForm[child_ops[i]]) {
			if((thisForm[child_ops[i]].checked) && (!thisForm[child_ops[i]].disabled)) {
				thisForm[child_ops[i]].checked = false;
				disableExceedLimitByName(thisForm, thisForm[child_ops[i]], ra_limits[child_ops[i]]);
			}
			//call recursive if this one has further children
			if(ra_parent_child[child_ops[i]]) {
				deselectReqApprChildren(thisForm, ra_parent_child[child_ops[i]]);
			}
		}
	}
}

//**************************************************
//Requires Approval deselection parent handler
//**************************************************
function deselectReqApprParents(thisForm, reqApprCheckbox) {
	var parent_ops = ra_child_parent[reqApprCheckbox.name];
	while(parent_ops) {
		if(thisForm[parent_ops]) {
			if(thisForm[parent_ops].checked) {
				thisForm[parent_ops].checked = false;
				disableExceedLimitByName(thisForm, thisForm[parent_ops], ra_limits[parent_ops]);
			}
		}
		parent_ops = ra_child_parent[parent_ops];
	}
}

//**************************************************
//Requires Approval Select Parent Handler
//**************************************************
function selectReqApprParents(thisForm, reqApprCheckbox) {
	//find current parent
	var parent_ops = ra_child_parent[reqApprCheckbox.name];
	var select_parent = true;

	//if all children selected select parent
	while(parent_ops) {
		//search all the children
		var child_ops = ra_parent_child[parent_ops];
		if(child_ops) {
			for(var j = 0; j < child_ops.length; j++) {
				if(thisForm[child_ops[j]]) {
					if(!thisForm[child_ops[j]].checked) {
						select_parent = false;
					}
				}
			}
		}

		if(select_parent) {
			if(thisForm[parent_ops]) {
				if(!thisForm[parent_ops].checked) {
					thisForm[parent_ops].checked = true;
					disableExceedLimitByName(thisForm, reqApprCheckbox, ra_limits[parent_ops]);
				}
			}
		}

		parent_ops = ra_child_parent[parent_ops];
	}
}

//**************************************************
//	Requires Approval Operation Hierarchy Generator
//  ra_child_parent - child to parent array
//  ra_parent_child - parent to child array
//**************************************************
function genReqApprParentChildList() {
	//generate child/parent list
	//if(!ra_child_parent||ra_child_parent) {
		ra_child_parent = new Array();
		for(var i=0; i < ra_disp_props.length; i++) {
			var ra_disp_prop = ra_disp_props[i];
			if(ra_disp_prop) {
				var parents_list = name_id_map[ ra_disp_prop[ 1 ] ];
				ra_child_parent[ra_disp_prop[0]] = parents_list;
			}
		}
	//}

	//generate parent child list
	//if(!ra_parent_child||ra_parent_child) {
	ra_parent_child = new Array();
		for(var i=0; i < ra_disp_props.length; i++) {
			var ra_disp_prop = ra_disp_props[i];
			if(ra_disp_prop) {
				//control parents list
				if(ra_parent_child[ name_id_map[ ra_disp_prop[ 1 ] ] ]) {
					//add child only
					var ra_children = ra_parent_child[ name_id_map[ ra_disp_prop[ 1 ] ] ];
					ra_children[ra_children.length] = ra_disp_prop[0];
				} else {
					//add parent and child both
					ra_parent_child[name_id_map[ ra_disp_prop[ 1 ] ] ] = new Array(ra_disp_prop[0]);
				}
			}
		}
	//}
}

//******************************************************************************
//Reset for disabled exceed with approval checkboxes
//******************************************************************************
function selectReqApprChildrensForReset(thisForm, limitId) {
	// Reset the form
	thisForm.reset();

	for(var i=1; i <= limitId; i++) {
		var reqApprCheckbox = "req_appr_" + i;
		if (thisForm[reqApprCheckbox] != null) {
			enableExceedLimitByName(thisForm, thisForm[reqApprCheckbox].checked, ra_limits[reqApprCheckbox]);
		}
	}
}

//******************************************************************************
//Enable exceed with approval checkboxes if respective RA checkbox is unchecked
//******************************************************************************
function enableExceedLimitByName(thisForm, reqApprCheckboxValue, limitId) {
	var name_array = new Array("day_exceed", "transaction_exceed", "week_exceed", "month_exceed");

	for(var k=0; k < name_array.length; k++) {
		var limitName = (name_array[k]+limitId);
		if(thisForm[limitName]) {
			if (!reqApprCheckboxValue) {
				if(thisForm[limitName].disabled) {
					thisForm[limitName].disabled = false;
				}
			} else {
				if(!thisForm[limitName].disabled) {
					thisForm[limitName].disabled = true;
				}
			}
		}
	}
}

// --></script>
