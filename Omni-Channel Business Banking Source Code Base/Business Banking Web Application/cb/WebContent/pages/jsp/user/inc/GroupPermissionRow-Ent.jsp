<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<% int limitIndex = ( ( Integer )session.getAttribute( "limitIndex" ) ).intValue(); %>

<ffi:setProperty name="EntitlementsDisplayedNames" property="Add" value="${LimitType.OperationName}"/>
<ffi:setProperty name="DisplayedLimit" value="TRUE"/>
<ffi:setProperty name="hideEnt" value="false" />
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:cinclude value1="${CanInitRow}" value2="FALSE">
		<ffi:setProperty name="hideEnt" value="true" />
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
	<ffi:cinclude value1="${hideEnt}" value2="true">
		<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
				<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ADMIN_PARTNER %>"/>
				<ffi:cinclude value1="${CanAdminRow}" value2="TRUE" operator="equals">

					<ffi:setProperty name="CheckEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
					<ffi:setProperty name="CheckEntitlementByGroup" property="OperationName" value="${LimitType.Value}"/>
					<ffi:cinclude value1="${CheckEntitlementObjectType}" value2="" operator="notEquals">
						<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectType" value="${CheckEntitlementObjectType}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${CheckEntitlementObjectId}" value2="" operator="notEquals">
						<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectId" value="${CheckEntitlementObjectId}"/>
					</ffi:cinclude>
					<ffi:process name="CheckEntitlementByGroup"/>
					<ffi:setProperty name="Compare" property="Value2" value="${CheckEntitlement}"/>

					<ffi:setProperty name="LastRequest" property="Name" value="admin${limitIndex}${TemplateIndex}"/>

					<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${LimitType.Value}"/>
					<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
					<ffi:removeProperty name="admin${limitIndex}${TemplateIndex}"/>
					<input type="<ffi:getProperty name="AdminCheckBoxType"/>" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0" <ffi:cinclude value1="${ParentChild}" value2="true" operator="equals">onClick="handleClick( FormName, this )"</ffi:cinclude>>
				</ffi:cinclude>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${hideEnt}" value2="false">
<tr>
	<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
		<td class="tbrd_t" valign="middle">
		</ffi:cinclude>
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ADMIN_PARTNER %>"/>
			<ffi:cinclude value1="${CanAdminRow}" value2="TRUE" operator="equals">

				<ffi:setProperty name="CheckEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
				<ffi:setProperty name="CheckEntitlementByGroup" property="OperationName" value="${LimitType.Value}"/>
				<ffi:cinclude value1="${CheckEntitlementObjectType}" value2="" operator="notEquals">
					<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectType" value="${CheckEntitlementObjectType}"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${CheckEntitlementObjectId}" value2="" operator="notEquals">
					<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectId" value="${CheckEntitlementObjectId}"/>
				</ffi:cinclude>
				<ffi:process name="CheckEntitlementByGroup"/>
				<ffi:setProperty name="Compare" property="Value2" value="${CheckEntitlement}"/>
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
					<div align="center">
				</ffi:cinclude>
					<ffi:setProperty name="LastRequest" property="Name" value="admin${limitIndex}${TemplateIndex}"/>

					<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${LimitType.Value}"/>
					<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
					<ffi:removeProperty name="admin${limitIndex}${TemplateIndex}"/>
					<input type="<ffi:getProperty name="AdminCheckBoxType"/>" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0" <ffi:cinclude value1="${ParentChild}" value2="true" operator="equals">onClick="handleClick( permLimitFrm, this )"</ffi:cinclude>>
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
					</div>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${CanAdminRow}" value2="TRUE" operator="notEquals">
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
				<div align="center">
				</ffi:cinclude>
					<ffi:setProperty name="LastRequest" property="Name" value="admin${limitIndex}${TemplateIndex}"/>

					<ffi:setProperty name="LastRequest" property="CheckboxValue" value=""/>
					<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
					<ffi:removeProperty name="admin${limitIndex}${TemplateIndex}"/>
					<input type="hidden" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" <ffi:getProperty name="LastRequest" property="Checked"/> border="0">
				<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
				</div>
				</ffi:cinclude>
			</ffi:cinclude>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
		</td>
		</ffi:cinclude>
	</ffi:cinclude>

	<ffi:setProperty name="CheckEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:setProperty name="CheckEntitlementByGroup" property="OperationName" value="${LimitType.OperationName}"/>

	<ffi:cinclude value1="${CheckEntitlementObjectType}" value2="" operator="notEquals">
		<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectType" value="${CheckEntitlementObjectType}"/>
	</ffi:cinclude>

	<ffi:cinclude value1="${CheckEntitlementObjectId}" value2="" operator="notEquals">
		<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectId" value="${CheckEntitlementObjectId}"/>
	</ffi:cinclude>

	<ffi:process name="CheckEntitlementByGroup"/>

	<ffi:setProperty name="Compare" property="Value2" value="${CheckEntitlement}"/>
	<td class="tbrd_t" valign="middle">
		<div align="center">
			<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
				<ffi:setProperty name="LastRequest" property="Name" value="entitlement${limitIndex}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
				<ffi:setProperty name="LastRequest" property="Name" value="entitlement${limitIndex}_${TemplateIndex}"/>
			</ffi:cinclude>
			<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${LimitType.OperationName}"/>
			<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Compare.Equals}"/>
			<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
				<ffi:removeProperty name="entitlement${limitIndex}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
				<ffi:removeProperty name="entitlement${limitIndex}_${TemplateIndex}"/>
			</ffi:cinclude>

			<ffi:cinclude value1="${CanInitRow}" value2="TRUE" operator="equals">
				<input type="checkbox" <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">disabled</ffi:cinclude> <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" border="0" <ffi:getProperty name="LastRequest" property="Checked"/> <ffi:cinclude value1="${ParentChild}" value2="true" operator="equals">onClick="handleClick( permLimitFrm, this )"</ffi:cinclude>>
			</ffi:cinclude>
			<ffi:cinclude value1="${CanInitRow}" value2="TRUE" operator="notEquals">
				<input type="hidden" <ffi:getProperty name="LastRequest" property="NameTag" encode="false"/> value="<ffi:getProperty name="LastRequest" property="CheckboxValue"/>" border="0">
			</ffi:cinclude>
		</div>
	</td>
    <ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL %>"/>
    	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
            <%  String classParam = "class = 'tbrd_t columndata'"; %>
            	<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
					<% String indentLevel; %>
					<ffi:getProperty name="LimitType" property="Value" assignTo="indentLevel"/>
					<%
						int indentLevelAsInt = Integer.parseInt( indentLevel );
					    if (indentLevelAsInt > 0) {
	                          classParam = "class = 'tbrd_t indent" + Integer.toString(indentLevelAsInt) + " columndata'";
	                    }
					%>
				</ffi:cinclude>

			<%-- Requires Approval Column addition --%>
			<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
				<td <%=classParam%> valign="middle" style="text-align: left;">
					<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
					<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
						<ffi:process name="GetEntitlementPropertyValues"/>
						<ffi:getProperty name="GetEntitlementPropertyValues" property="PropertyValue"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
						<ffi:getProperty name="LimitType" property="OperationName"/>
					</ffi:cinclude>
				</td>
				<ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
					<ffi:setProperty name="CheckRequiresApproval" property="OperationName" value="${LimitType.OperationName}" />
					<ffi:process name="CheckRequiresApproval" />
					<td class="tbrd_t" valign="middle"  colspan="7" style="text-align: left;">&nbsp;&nbsp;
						<ffi:cinclude value1="TRUE" value2="${CanInitRow}" operator="equals">
							<ffi:cinclude value1="${CheckRequiresApproval.ValidReqApprOperation}" value2="true" operator="equals">
								<input name="req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />" type="checkbox" <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">disabled</ffi:cinclude> value="<ffi:getProperty name="CheckRequiresApproval" property="OperationName" />"
								 <ffi:getProperty name="CheckRequiresApproval" property="Enabled" />
								 <ffi:getProperty name="CheckRequiresApproval" property="Selected" />
								  onclick="disableExceedLimitById(this.form, this, '<ffi:getProperty name="limitIndex" />')"
								 />
								<ffi:setProperty name="EditRequiresApproval" property="ReqApprOpName" value="${LimitType.OperationName}" />
								<ffi:setProperty name="DisableExceed" value="${CheckRequiresApproval.Selected}" />
								<%-- Set display properties into javascript --%>
								<script type="text/javascript">
									var ra_indx = (<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" /> - 1);
									ra_disp_props[ra_indx] = new Array('req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />', '<ffi:getProperty name="CheckRequiresApproval" property="DisplayParent" />', '<ffi:getProperty name="CheckRequiresApproval" property="ControlParent" />');
									name_id_map['<ffi:getProperty name="CheckRequiresApproval" property="OperationName" />'] = 'req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />';
								</script>
			        		</ffi:cinclude>
							<ffi:cinclude value1="${CheckRequiresApproval.ValidReqApprOperation}" value2="false" operator="equals">
								&nbsp;
							</ffi:cinclude>
						</ffi:cinclude>
					</td>
			    </ffi:cinclude>
			</ffi:cinclude>
		<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="notEquals">
			<td <%=classParam%> valign="middle" colspan="7" style="text-align: left;">
				<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
				<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
					<ffi:process name="GetEntitlementPropertyValues"/>
					<ffi:getProperty name="GetEntitlementPropertyValues" property="PropertyValue"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
					<ffi:getProperty name="LimitType" property="OperationName"/>
				</ffi:cinclude>
			</td>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
            <%  String classParam = "tbrd_t columndata"; %>
				<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">

					<% String indentLevel; %>
					<ffi:getProperty name="LimitType" property="Value" assignTo="indentLevel"/>
					<%
						int indentLevelAsInt = Integer.parseInt( indentLevel );
					    if (indentLevelAsInt > 0) {
	                          classParam = "tbrd_t indent" + Integer.toString(indentLevelAsInt) + " columndata";
	                    }
					%>
				</ffi:cinclude>
				<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
					<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
						<ffi:process name="GetEntitlementPropertyValues"/>
						<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${GetEntitlementPropertyValues.PropertyValue}" />
					</ffi:cinclude>
					<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
					<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${LimitType.OperationName}" />
					</ffi:cinclude>
			<%-- Requires Approval Column addition --%>
			<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
				<td class='<ffi:getPendingStyle fieldname="${LimitType.OperationName}" defaultcss="<%=classParam %>"
						dacss='<%=classParam + " columndataDA" %>'
						sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'
						valign="middle" style="text-align: left;">
						<ffi:getProperty name="LastRequest" property="CheckboxValue" />
					</td>
				<ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
					<ffi:setProperty name="CheckRequiresApproval" property="OperationName" value="${LimitType.OperationName}" />
					<ffi:process name="CheckRequiresApproval" />
					<td class="tbrd_t" valign="middle"  colspan="7" style="text-align: left;">&nbsp;&nbsp;
						<ffi:cinclude value1="TRUE" value2="${CanInitRow}" operator="equals">
							<ffi:cinclude value1="${CheckRequiresApproval.ValidReqApprOperation}" value2="true" operator="equals">
								<input name="req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />" type="checkbox" <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">disabled</ffi:cinclude> value="<ffi:getProperty name="CheckRequiresApproval" property="OperationName" />"
								 <ffi:getProperty name="CheckRequiresApproval" property="Enabled" />
								 <ffi:getProperty name="CheckRequiresApproval" property="Selected" />
								  onclick="disableExceedLimitById(this.form, this, '<ffi:getProperty name="limitIndex" />')"
								 />
								<ffi:setProperty name="EditRequiresApproval" property="ReqApprOpName" value="${LimitType.OperationName}" />
								<ffi:setProperty name="DisableExceed" value="${CheckRequiresApproval.Selected}" />
								<%-- Set display properties into javascript --%>
								<script type="text/javascript">
									var ra_indx = (<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" /> - 1);
									ra_disp_props[ra_indx] = new Array('req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />', '<ffi:getProperty name="CheckRequiresApproval" property="DisplayParent" />', '<ffi:getProperty name="CheckRequiresApproval" property="ControlParent" />');
									name_id_map['<ffi:getProperty name="CheckRequiresApproval" property="OperationName" />'] = 'req_appr_<ffi:getProperty name="CheckRequiresApproval" property="OpCounter" />';
								</script>
			        		</ffi:cinclude>
							<ffi:cinclude value1="${CheckRequiresApproval.ValidReqApprOperation}" value2="false" operator="equals">
								&nbsp;
							</ffi:cinclude>
						</ffi:cinclude>
					</td>
			    </ffi:cinclude>
			</ffi:cinclude>
		<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="notEquals">
			<td class='<ffi:getPendingStyle fieldname="${LimitType.OperationName}" defaultcss="<%=classParam %>"
						dacss='<%=classParam + " columndataDA" %>'
						sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'
						valign="middle" colspan="7" style="text-align: left;">
						<ffi:getProperty name="LastRequest" property="CheckboxValue" />
				</td>
		</ffi:cinclude>
	</ffi:cinclude>
</tr>
</ffi:cinclude>
