<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="EditLocationPermissionsDA" name="com.ffusion.tasks.dualapproval.EditLocationPermissionsDA" scope="session"/>

<ffi:setProperty name="EditLocationPermissionsDA"  property="approveAction" value="true" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditLocationPermissionsDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditLocationPermissionsDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditLocationPermissionsDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditLocationPermissionsDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="EditLocationPermissionsDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>
<ffi:setProperty name="EditLocationPermissionsDA" property="EntitlementTypesMerged" value="AccountEntitlementsMerged"/>
<ffi:setProperty name="EditLocationPermissionsDA" property="EntitlementTypePropertyLists" value="LimitsList"/>

<ffi:setProperty name="GetTypesForEditingLimits" property="LocationWithLimitsName" value="LocationEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="LocationWithoutLimitsName" value="LocationEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="LocationMerged" value="LocationEntitlementsMergedBeforeFilter"/>

<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="LocationEntitlementsMergedBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="LocationEntitlementsMerged"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${EntitlementGroupParentId}"/>
		
<!-- Cash Management -->
<ffi:setProperty name="GetCategories" property="ObjectId" value=""/>
<ffi:setProperty name="GetCategories" property="ObjectType" value=""/>
<ffi:setProperty name="GetCategories" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_PER_LOCATION %>"/>
<ffi:process name="GetTypesForEditingLimits"/>

<ffi:setProperty name="approveCategory" property="ObjectId" value="" />
<ffi:setProperty name="approveCategory" property="ObjectType" value="" />
<ffi:setProperty name="approveCategory" property="categorySubType" value="" />
<ffi:removeProperty name="categories" />
<ffi:process name="GetCategories" />
<ffi:cinclude value1="${categories}" value2="" operator="notEquals">
	<ffi:object id="FilterDACategories" name="com.ffusion.tasks.dualapproval.FilterDACategories" />
	<ffi:setProperty name="FilterDACategories" property="FilteredUnique" value="true" />
	<ffi:removeProperty name="dacategories" />
	<ffi:process name="FilterDACategories" />
	<ffi:list collection="dacategories" items="category">
		<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectType" value="${category.ObjectType}"/>
		<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectId" value="${category.ObjectId}"/>
		<ffi:setProperty name="GetCategories" property="ObjectId" value="${category.ObjectId}"/>
		<ffi:setProperty name="GetCategories" property="ObjectType" value="${category.ObjectType}"/>
		<ffi:process name="FilterEntitlementsForBusiness"/>
		<ffi:process name="EditLocationPermissionsDA" />
	</ffi:list>
	<ffi:list collection="categories" items="category" startIndex="1" endIndex="1">
		<ffi:setProperty name="ApprovePendingChanges" property="ApproveBySubType" value="true"/>
		<ffi:setProperty name="ApprovePendingChanges" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_PER_LOCATION %>"/>
		<ffi:process name="ApprovePendingChanges"/>
	</ffi:list>
</ffi:cinclude>

<ffi:removeProperty name="EditLocationPermissionsDA"/>
<ffi:removeProperty name="dacategories" />