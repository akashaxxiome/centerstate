<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

	<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
	<ffi:process name="GetLimitBaseCurrency" />

	<ffi:setProperty name="accountAccessPermissionsVerifyGridTempURL" value="/pages/user/getVerifyAccountAccessPermissions.action?collectionName=FilteredAccounts" URLEncrypt="true"/>
	<s:url id="accountAccessPermissionsVerifyGridURL" value="%{#session.accountAccessPermissionsVerifyGridTempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="accountAccessPermissionsVerifyGrid"
		caption=""  
		sortable="false"  
		dataType="json"  
		href="%{accountAccessPermissionsVerifyGridURL}"  
		pager="true"  
		viewrecords="true" 
		gridModel="gridModel" 
 		rowList="%{#session.StdGridRowList}" 
 		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		altRows="true"
		sortorder="asc"
		navigator="true"
		navigatorSearch="false"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		onGridCompleteTopics="completeaccountAccessPermissionsVerifyGridLoad"
		>
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
			<sjg:gridColumn name="map.CanAdminRow" index="ADMIN" title="%{getText('jsp.user_32')}" formatter="ns.admin.formatVerifyAdminColumn" sortable="false" align="center" width="40" />
		</ffi:cinclude> 
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="equals">
			<sjg:gridColumn name="map.CanAdminRow" index="ADMIN" title="%{getText('jsp.user_32')}" formatter="ns.admin.formatVerifyAdminColumn" sortable="false" hidden="true" hidedlg="true" />
		</ffi:cinclude>
		<sjg:gridColumn name="map.CanInitRow" index="INIT" title="%{getText('jsp.user_177')}" formatter="ns.admin.formatVerifyInitColumn" sortable="false" align="center" width="40" />
		<sjg:gridColumn name="displayText" index="NUMBER" title="%{getText('jsp.default_15')}" formatter="ns.admin.formatAccountColumn" sortable="true" width="200"/>
		<sjg:gridColumn name="type" index="TYPESTRING" title="%{getText('jsp.default_20')}" sortable="true" width="75" />
		<sjg:gridColumn name="LIMIT1" index="LIMIT1" title="%{getText('jsp.default_261') + '(' + getText('jsp.user_173') + ' ' + #session.BaseCurrency + ')'}" formatter="ns.admin.formatVerifyLimit1Column" sortable="false" width="170" />
		<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			<sjg:gridColumn name="EXCEED_LIMIT_WITH_APPROVAL1" index="EXCEED_LIMIT_WITH_APPROVAL1" title="%{getText('jsp.user_152')}" formatter = "ns.admin.formatVerifyExceedWithApproval1Column" sortable="false" align="center" width="140" />
		</ffi:cinclude>
		<sjg:gridColumn name="LIMIT2" index="LIMIT2" title="%{getText('jsp.default_261') + '(' + getText('jsp.user_173') + ' ' + #session.BaseCurrency + ')'}" formatter="ns.admin.formatVerifyLimit2Column" sortable="false" width="170"/>
		<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
			<sjg:gridColumn name="EXCEED_LIMIT_WITH_APPROVAL2" index="EXCEED_LIMIT_WITH_APPROVAL2" title="%{getText('jsp.user_152')}" formatter = "ns.admin.formatVerifyExceedWithApproval2Column" sortable="false" align="center" width="140" />
		</ffi:cinclude>
		<sjg:gridColumn name="currencyCode" index="CURRENCY_CODE" title="%{getText('jsp.default_125')}" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="primaryAccount" index="primaryAccount" title="%{getText('jsp.user_263')}" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="coreAccount" index="coreAccount" title="%{getText('jsp.user_96')}" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="routingNum" index="routingNum" title="%{getText('jsp.user_96')}" sortable="false" hidden="true" hidedlg="true"/>
		
	</sjg:grid>
	
<script type="text/javascript">
	//Custom headers
	var exceedWithApprovalTitle = js_user_exceedLimit + '<br>' + js_user_withoutAppr;
	$exceedWithApprovalheaderTR1 = $('#accountAccessPermissionsGrid_EXCEED_LIMIT_WITH_APPROVAL1');
	$exceedWithApprovalheaderTR1.attr("style", "height:50px;");
	$exceedWithApprovalheader1 = $('#accountAccessPermissionsVerifyGrid_EXCEED_LIMIT_WITH_APPROVAL1 #jqgh_EXCEED_LIMIT_WITH_APPROVAL1');
	$exceedWithApprovalheader1.html(exceedWithApprovalTitle);
	$exceedWithApprovalheader1.attr("style", "height:40px;");
	
	$exceedWithApprovalheaderTR2 = $('#accountAccessPermissionsGrid_EXCEED_LIMIT_WITH_APPROVAL2');
	$exceedWithApprovalheaderTR2.attr("style", "height:50px;");
	$exceedWithApprovalheader2 = $('#accountAccessPermissionsVerifyGrid_EXCEED_LIMIT_WITH_APPROVAL2 #jqgh_EXCEED_LIMIT_WITH_APPROVAL2');
	$exceedWithApprovalheader2.html(exceedWithApprovalTitle);
	$exceedWithApprovalheader2.attr("style", "height:40px;");
	
	//disable sortable rows
		if($("#accountAccessPermissionsVerifyGrid tbody").data("ui-jqgrid") != undefined){
			$("#accountAccessPermissionsVerifyGrid" + " tbody").sortable("destroy");
		}
</script>

<ffi:removeProperty name="GetLimitBaseCurrency"/>