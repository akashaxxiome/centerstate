<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:setProperty name="entitlementTypeOrder" property="Filter" value="ParentMenu=top" />
        <table width='100%' cellpadding='0' cellspacing='0' border='0' class="tableRightBorderClass tableData">
        <tr>
        
        <ffi:list collection="entitlementTypeOrder" items="PermissionTypeAdmin">  
	    <%-- Highlight the menu item if it is the menu item for the current page.	--%>
			
            <td width='82' align='center' style="padding:0px 2px; <ffi:cinclude value1="${CurrentWizardPage}" value2="${PermissionTypeAdmin.PageKey}">background-color: #27AE60;color:#fff</ffi:cinclude>" >
            	<ffi:cinclude value1="${PermissionTypeAdmin.MenuUrl}" value2="" operator="notEquals">
            		<ffi:setProperty name="tmpurl" value="${PermissionTypeAdmin.MenuUrl}&ParentMenu=${PermissionTypeAdmin.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${PermissionTypeAdmin.PageKey}&SortKey=${PermissionTypeAdmin.NextSortKey}" URLEncrypt="true"/>
   
                 </ffi:cinclude>
		<ffi:cinclude value1="${PermissionTypeAdmin.MenuUrl}" value2="" operator="equals">
			<ffi:setProperty name="tmpurl" value="${PermissionTypeAdmin.NextPageUrl}&ParentMenu=${PermissionTypeAdmin.MenuDisplayName}&PermissionsWizard=TRUE&CurrentWizardPage=${PermissionTypeAdmin.NextPageKey}&SortKey=${PermissionTypeAdmin.NextSortKey}" URLEncrypt="true"/>            	
                </ffi:cinclude>   
             		<ffi:cinclude value1="${PermissionTypeAdmin.EnableLink}" value2="false" >
             		            <span class="sectionhead adminBackground"><ffi:getProperty name="PermissionTypeAdmin" property="menuDisplayName"/>
             		            </span>
             		     </ffi:cinclude>
          
             		<ffi:cinclude value1="${PermissionTypeAdmin.EnableLink}" value2="false" operator="notEquals" >
                		<a class="menuSecondaryContainer" href='<ffi:getProperty name="tmpurl" />' ><span class="menuSecondaryText"><ffi:getProperty name="PermissionTypeAdmin" property="menuDisplayName"/></span></a>
                	</ffi:cinclude>       
            </td>
            <%--td width='1' bgcolor="#000000"/--%>
       </ffi:list>          

        </tr>
    </table>
<div style="border-bottom:1px solid #ccc"></div>
<ffi:removeProperty name="tmpurl" />
