<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="corpadmininfo-pre.jsp" />


<ffi:object name="com.ffusion.tasks.accountgroups.GetAccountGroups" id="GetAccountGroups"  scope="session"/>
<ffi:setProperty name="GetAccountGroups" property="BusDirectoryId" value="${Business.Id}"/>
<ffi:process name="GetAccountGroups" />
<ffi:removeProperty name="GetAccountGroups"/>
<ffi:setProperty name="AccountGroups" property="SortedBy" value="NAME"/>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>