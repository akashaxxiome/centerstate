<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request"/>
<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${SecureUser.BaseCurrency}"/>
<!-- <tr>
	<td class="sectionsubhead">&gt;L10NStartPermissionL10NEnd</td>
</tr> -->
<tr class="header">
	<td class="sectionsubhead adminBackground">&nbsp;<s:text name="jsp.default.permission.type" /></td>
	<td class="sectionsubhead adminBackground"><s:text name="jsp.default.activity" /></td>
	<td class="sectionsubhead adminBackground"><s:text name="jsp.default.activity.type" /></td>
	<td class="sectionsubhead adminBackground"><s:text name="jsp.default.activity.type.id" /></td>
	<td class="sectionsubhead adminBackground"><s:text name="jsp.default.activity.old.value" /></td>
	<td class="sectionsubhead adminBackground"><s:text name="jsp.default.activity.new.value" /></td>
	<td class="sectionsubhead adminBackground"><s:text name="jsp.default.activity.old.limit.exceed" /></td>
	<td class="sectionsubhead adminBackground"><s:text name="jsp.default.activity.new.limit.exceed" /></td>
	<td class="sectionsubhead adminBackground"><s:text name="jsp.default.activity.limit.period" /></td>
</tr>
