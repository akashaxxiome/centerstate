<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="EditACHCompanyAccessDA" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>

<%-- Get the entitlementGroup, which the permission or its member's permission is currently being edited --%>
<ffi:object id="GetEntitlementGroupId" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:setProperty name="EditACHCompanyAccessDA" property="approveAction" value="true" />
<ffi:setProperty name="EditACHCompanyAccessDA" property="EntsListSessionName" value="perACHEntLists"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditACHCompanyAccessDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditACHCompanyAccessDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditACHCompanyAccessDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditACHCompanyAccessDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="EditACHCompanyAccessDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>

<ffi:object id="GetACHCompanyAccess" name="com.ffusion.tasks.admin.GetACHCompanyAccess" scope="session"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
</ffi:cinclude>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="ParentGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="GetACHCompanyAccess" property="ParentGroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
</s:if>
<ffi:setProperty name="GetACHCompanyAccess" property="EntsListSessionName" value="perACHEntListsBeforeFilter"/>
<ffi:setProperty name="GetACHCompanyAccess" property="ParentChildName" value="ParentChildLists"/>
<ffi:setProperty name="GetACHCompanyAccess" property="ChildParentName" value="ChildParentLists"/>
<ffi:setProperty name="GetACHCompanyAccess" property="EntsLimitsSessionName" value="limitInfoList"/>
	
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="perACHEntListsBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="perACHEntLists"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${EntitlementGroupParentId}"/>
			
<!-- Per ach company account -->
<ffi:setProperty name="GetCategories" property="ObjectId" value=""/>
<ffi:setProperty name="GetCategories" property="ObjectType" value=""/>
<ffi:setProperty name="GetCategories" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_PER_ACH_COMPANY_ACCESS %>"/>
<ffi:setProperty name="approveCategory" property="ObjectId" value="" />
<ffi:setProperty name="approveCategory" property="ObjectType" value="" />
<ffi:setProperty name="approveCategory" property="categorySubType" value="" />

<ffi:removeProperty name="categories" />
<ffi:process name="GetCategories" />
<ffi:cinclude value1="${categories}" value2="" operator="notEquals">
	<ffi:removeProperty name="FilterDACategories" />
	<ffi:object id="FilterDACategories" name="com.ffusion.tasks.dualapproval.FilterDACategories" />
	<ffi:setProperty name="FilterDACategories" property="FilteredUnique" value="true" />
	<ffi:removeProperty name="dacategories" />
	<ffi:process name="FilterDACategories" />
	<ffi:list collection="dacategories" items="category">
		<ffi:setProperty name="GetACHCompanyAccess" property="ACHCompanyID" value="${category.ObjectId}"/>
		<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectType" value="${category.ObjectType}"/>
		<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectId" value="${category.ObjectId}"/>
		<ffi:setProperty name="GetCategories" property="ObjectId" value="${category.ObjectId}"/>
		<ffi:setProperty name="GetCategories" property="ObjectType" value="${category.ObjectType}"/>
		<ffi:process name="GetACHCompanyAccess"/>
		<ffi:process name="FilterEntitlementsForBusiness"/>
		<ffi:setProperty name="EditACHCompanyAccessDA" property="ACHCompanyID" value="${category.ObjectId}"/>
		<ffi:process name="EditACHCompanyAccessDA" />
	</ffi:list>
	
	<ffi:list collection="categories" items="category" startIndex="1" endIndex="1">
	<ffi:setProperty name="ApprovePendingChanges" property="ApproveBySubType" value="true"/>
		<ffi:setProperty name="ApprovePendingChanges" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_PER_ACH_COMPANY_ACCESS %>"/>
		<ffi:setProperty name="ApprovePendingChanges" property="categoryType" value="${category.categoryType}"/>
		<ffi:process name="ApprovePendingChanges"/>
	</ffi:list>
</ffi:cinclude>

<ffi:removeProperty name="GetACHCompanyAccess" />
<ffi:removeProperty name="EditACHCompanyAccessDA" />
<ffi:removeProperty name="FilterDACategories" />
<ffi:removeProperty name="dacategories" />
<ffi:removeProperty name="Entitlement_EntitlementGroup" />
