<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="GetMaxACHCompanyLimits" name="com.ffusion.tasks.admin.GetMaxACHCompanyLimits" scope="session" />
<ffi:setProperty name="GetMaxACHCompanyLimits" property="LimitInfoListSessionName" value="limitInfoList"/>
<ffi:setProperty name="GetMaxACHCompanyLimits" property="PerTransactionMapName" value="perBatchMax"/>
<ffi:setProperty name="GetMaxACHCompanyLimits" property="PerDayMapName" value="dailyMax"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="GetMaxACHCompanyLimits" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>
<ffi:setProperty name="GetMaxACHCompanyLimits" property="ACHCompanyID" value="${ACHCompanyID}"/>
<ffi:process name="GetMaxACHCompanyLimits"/>
<ffi:removeProperty name="GetMaxACHCompanyLimits"/>

<ffi:object id="GetMaxACHLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxACHLimitForPeriod" scope="session" />
<ffi:setProperty name="GetMaxACHLimitForPeriod" property="PerTransactionMapName" value="perBatchMax"/>
<ffi:setProperty name="GetMaxACHLimitForPeriod" property="PerDayMapName" value="dailyMax"/>
<ffi:process name="GetMaxACHLimitForPeriod"/>
<ffi:setProperty name="GetMaxACHLimitForPeriod" property="NoLimitString" value="no"/>

<ffi:object id="CheckForRedundantACHDALimits" name="com.ffusion.tasks.dualapproval.CheckForRedundantACHDALimits" scope="session" />
<ffi:setProperty name="CheckForRedundantACHDALimits" property="PerTransactionMapName" value="perBatchMax"/>
<ffi:setProperty name="CheckForRedundantACHDALimits" property="PerDayMapName" value="dailyMax"/>
<ffi:setProperty name="CheckForRedundantACHDALimits" property="LimitInfoListSessionName" value="limitInfoList"/>
<ffi:setProperty name="CheckForRedundantACHDALimits" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_BEAN %>"/>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="CheckForRedundantACHDALimits" property="GroupId" value="${EditGroup_GroupId}"/>
</s:if>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckForRedundantACHDALimits" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>	
	<ffi:setProperty name="CheckForRedundantACHDALimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckForRedundantACHDALimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckForRedundantACHDALimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
</s:if>
<ffi:process name="CheckForRedundantACHDALimits"/>
<ffi:removeProperty name="CheckForRedundantDALimits"/>
<ffi:removeProperty name="GetMaxLimitForPeriod"/>
