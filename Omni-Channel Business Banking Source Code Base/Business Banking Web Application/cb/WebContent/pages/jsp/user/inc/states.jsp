<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<ffi:object id="GetStatesForCountry" name="com.ffusion.tasks.util.GetStatesForCountry" scope="session" />
    <ffi:setProperty name="GetStatesForCountry" property="CountryCode" value='<%=request.getParameter("countryCode") %>' />
<ffi:process name="GetStatesForCountry" />
<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">	
		<option selected value=""><s:text name="jsp.default_376"/></option>
		<ffi:list collection="GetStatesForCountry.StateList" items="item">
			<option  value="<ffi:getProperty name="item" property="StateKey"/>"><ffi:getProperty name='item' property='Name'/></option>
		</ffi:list>
</ffi:cinclude>