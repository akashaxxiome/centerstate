<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:object id="EditACHCompanyAccessDA" name="com.ffusion.tasks.dualapproval.EditACHCompanyAccessDA" scope="session"/>
<ffi:object id="EditRequiresApprovalDA" name="com.ffusion.tasks.dualapproval.EditRequiresApprovalDA" scope="session"/>
<ffi:object id="CheckRequiresApproval" name="com.ffusion.tasks.dualapproval.CheckRequiresApproval" />

<%-- Get the entitlementGroup, which the permission or its member's permission is currently being edited --%>
<ffi:object id="GetEntitlementGroupId" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:setProperty name="EditACHCompanyAccessDA" property="approveAction" value="true" />
<ffi:setProperty name="EditACHCompanyAccessDA" property="EntsListSessionName" value="crossACHEntLists"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditACHCompanyAccessDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditACHCompanyAccessDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditACHCompanyAccessDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditACHCompanyAccessDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="EditACHCompanyAccessDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>
<ffi:setProperty name="EditRequiresApprovalDA" property="approveAction" value="true" />
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditRequiresApprovalDA" property="ModifyUserOnly" value="true" />
	<ffi:setProperty name="EditRequiresApprovalDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="EditRequiresApprovalDA" property="ModifyUserOnly" value="false" />
	<ffi:setProperty name="EditRequiresApprovalDA" property="GroupId" value="${EditGroup_GroupId}" />
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberType" value="${itemType}"/>
	<ffi:setProperty name="EditRequiresApprovalDA" property="MemberId" value="${itemId}"/>
</s:if>

<ffi:object id="GetACHCompanyAccess" name="com.ffusion.tasks.admin.GetACHCompanyAccess" scope="session"/>

<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Division" operator="equals">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${Section}" value2="Group" operator="equals">
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
</ffi:cinclude>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">	
	<ffi:setProperty name="GetACHCompanyAccess" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="ParentGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="GetACHCompanyAccess" property="ParentGroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	<ffi:setProperty name="GetACHCompanyAccess" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
</s:if>
<ffi:setProperty name="GetACHCompanyAccess" property="EntsListSessionName" value="crossACHEntListsBeforeFilter"/>
<ffi:setProperty name="GetACHCompanyAccess" property="ParentChildName" value="ParentChildLists"/>
<ffi:setProperty name="GetACHCompanyAccess" property="ChildParentName" value="ChildParentLists"/>
<ffi:setProperty name="GetACHCompanyAccess" property="EntsLimitsSessionName" value="limitInfoList"/>
	
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="crossACHEntListsBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="crossACHEntLists"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${EntitlementGroupParentId}"/>
			
<!-- Per ach company account -->
<ffi:setProperty name="GetCategories" property="ObjectId" value=""/>
<ffi:setProperty name="GetCategories" property="ObjectType" value=""/>
<ffi:setProperty name="GetCategories" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_CROSS_ACH_COMPANY_ACCESS %>"/>
<ffi:setProperty name="approveCategory" property="ObjectId" value="" />
<ffi:setProperty name="approveCategory" property="ObjectType" value="" />

<ffi:removeProperty name="categories" />
<ffi:process name="GetCategories" />
<ffi:cinclude value1="${categories}" value2="" operator="notEquals">
	<ffi:process name="GetACHCompanyAccess"/>
	<ffi:process name="FilterEntitlementsForBusiness"/>
	<ffi:process name="EditACHCompanyAccessDA" />
	<ffi:process name="EditRequiresApprovalDA" />
	
	<ffi:list collection="categories" items="category" startIndex="1" endIndex="1">
	<ffi:setProperty name="ApprovePendingChanges" property="CategorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_CROSS_ACH_COMPANY_ACCESS %>"/>
	<ffi:setProperty name="ApprovePendingChanges" property="ApproveBySubType" value="true"/>
	<ffi:process name="ApprovePendingChanges"/>		
</ffi:list>
	
</ffi:cinclude>

<ffi:removeProperty name="CheckRequiresApproval" />
<ffi:removeProperty name="EditCrossAccountPermissionsDA" />
<ffi:removeProperty name="EditRequiresApprovalDA" />
<ffi:removeProperty name="GetACHCompanyAccess" />
<ffi:removeProperty name="Entitlement_EntitlementGroup" />