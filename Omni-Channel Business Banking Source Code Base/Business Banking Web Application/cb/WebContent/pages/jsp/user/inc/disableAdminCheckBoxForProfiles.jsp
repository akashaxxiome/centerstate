<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
	<s:if test="%{#session.SecureUser.usingEntProfiles == true}">
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</s:if>
	<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
</ffi:cinclude>