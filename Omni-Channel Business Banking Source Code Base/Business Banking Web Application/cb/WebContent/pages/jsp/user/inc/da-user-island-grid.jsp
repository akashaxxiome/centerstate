<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<ffi:help id="user_corpadminusers_da_pendingapproval" className="moduleHelpClass"/>

<%-- <div id="hidden"  style="display:none;">
    <s:url id="editDAUserUrl" value="/pages/jsp/user/corpadminuseredit.jsp" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
    </s:url>
    <sj:a id="editDAUserLink" href="%{editDAUserUrl}" targets="inputDiv" button="true" title="%{getText('jsp.user_146')}"
        onClickTopics="beforeLoad" onCompleteTopics="completeUserLoad" onErrorTopics="errorLoad">
      <span class="ui-icon ui-icon-wrench"></span>
    </sj:a>	
</div> --%>

<%-- 
<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="ApproveURL" url="/cb/pages/jsp/user/da-user-verify.jsp?Section=Users&itemId=${pendingApprovalEmployee.Id}&rejectFlag=n&userName=${pendingApprovalEmployee.UserName}&userAction=${pendingApprovalEmployee.Action}" />
--%>
<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="ApproveURL" url="/cb/pages/jsp/user/da-user-verify.jsp?Section=Users&itemId={0}&rejectFlag=n&userName={1}&userAction={2}" parm0="Id" parm1="UserName" parm2="Action"/>

<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="PermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Users&BusinessEmployeeId={0}&OneAdmin={1}" parm0="Id" parm1="OneAdmin"/>

<%--
<ffi:link url="da-verify-discard-user.jsp?itemId=${pendingApprovalEmployee.Id}&successUrl=${SecurePath}user/corpadminusers.jsp&itemType=User&category=Profile"><img src="/cb/cb/multilang/grafx/user/i_stop.gif" title="Discard Changes" border="0" hspace="3"></ffi:link>
--%>
<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="DiscardURL" url="/cb/pages/jsp/user/da-verify-discard-user.jsp?itemId={0}&userAction={1}&successUrl=/SecurePath/user/corpadminusers.jsp&itemType=User&category=Profile" parm0="Id" parm1="Action"/>
				
<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="SubmitForApprovalURL" url="/cb/pages/jsp/user/da-verify-submit-user.jsp?itemId={0}&successUrl=/SecurePath/user/corpadminusers.jsp&itemType=User&category=Profile" parm0="Id"/>

<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="ModifyChangeURL" url="/cb/pages/dualapproval/dualApprovalAction-revokingApproval.action?itemId={0}&successUrl=/SecurePath/user/corpadminusers.jsp&module=User&category=Profile" parm0="Id"/>
			
<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="RejectURL" url="/cb/pages/jsp/user/da-user-verify.jsp?Section=Users&itemId={0}&successUrl=/SecurePath/user/corpadminusers.jsp&itemType=User&category=Profile&rejectFlag=y&userName={1}&userAction={2}" parm0="Id" parm1="UserName" parm2="Action"/>

<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="ViewUserPermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Users&BusinessEmployeeId={0}&OneAdmin={1}&ViewUserPermissions=TRUE" parm0="Id" parm1="OneAdmin"/>

<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="EditURL" url="/cb/pages/user/editBusinessUser_init.action?employeeID={0}" parm0="Id"/>

<ffi:setGridURL grid="GRID_adminUsersPendingApprovalChanges" name="ViewURL" url="/cb/pages/jsp/user/corpadminuserview.jsp?itemId={0}" parm0="Id"/>
		
<ffi:setProperty name="usersWithPendingChangesGridTempURL" value="/pages/user/getPendingApprovalBusinessEmployees.action?GridURLs=GRID_adminUsersPendingApprovalChanges&CollectionName=PendingApprovalEmployees&dualApprovalMode=1" URLEncrypt="true"/>
<s:url id="getBusinessEmployeesUrl" value="%{#session.usersWithPendingChangesGridTempURL}" escapeAmp="false"/>
     <sjg:grid
       id="usersWithPendingChangesGrid"
       caption=""
	   sortable="true"  
       dataType="json"
       href="%{getBusinessEmployeesUrl}"
       pager="true"
       viewrecords="true"
       gridModel="gridModel"
	   rowList="%{#session.StdGridRowList}" 
	   rowNum="%{#session.StdGridRowNum}" 
       rownumbers="false"
       altRows="true"
       sortname="USERNAME"
       sortorder="asc"
       shrinkToFit="true"
       navigator="true"
       navigatorAdd="false"
       navigatorDelete="false"
       navigatorEdit="false"
       navigatorRefresh="false"
       navigatorSearch="false"
       onGridCompleteTopics="addGridControlsEvents,tabifyNotes,pendingDAChangesGridCompleteEventsForUsers"
     >
     <sjg:gridColumn name="fullName" width="200" index="NAME" title="%{getText('jsp.default_283')}" sortable="true" />
     <sjg:gridColumn name="userName" width="200" index="USERNAME" title="%{getText('jsp.default_455')}" sortable="true" />
     <sjg:gridColumn name="map.Group" width="200" index="Group" title="%{getText('jsp.default_225')}" sortable="true" />
     <sjg:gridColumn name="map.Admin" width="75" index="CanAdministerAnyGroup" title="%{getText('jsp.user_32')}" sortable="false" search="false" formatter="formatDAPendingChangesUserAdminColumn"/>
     <sjg:gridColumn name="action" index="action" title="%{getText('jsp.default_388')}" sortable="false" hidedlg="false" width="90" search="false"/>
     <sjg:gridColumn name="ID" index="operation" title="%{getText('jsp.default_27')}" sortable="false" width="90" search="false" formatter="formatDAPendingChangesUserActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
	 <sjg:gridColumn name="map.IsAnAdminOf" width="0" index="IsAnAdminOf" title="%{getText('jsp.user_184')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="map.OneAdmin" width="0" index="OneAdmin" title="%{getText('jsp.user_231')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="map.Self" width="0" index="Self" title="%{getText('jsp.user_294')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="rejectReason" width="0" index="rejectReason" title="rejectReason" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="rejectedBy" index="rejectedBy" title="rejectedBy" sortable="false" width="0" hidden="true" hidedlg="true"/>
	 <sjg:gridColumn name="status" index="status" title="status" sortable="false" width="0" hidden="true" hidedlg="true"/>

 </sjg:grid>

<script type="text/javascript">

	var fnCustomPendingUsersRefresh = function (event) {
		ns.common.forceReloadCurrentSummary();
	};
	
	$("#usersWithPendingChangesGrid").data('fnCustomRefresh', fnCustomPendingUsersRefresh);
	
	$("#usersWithPendingChangesGrid").data("supportSearch",true);

	//disable sortable rows
	if(ns.common.isInitialized($("#usersWithPendingChangesGrid tbody"),"jqGrid")){
		$("#usersWithPendingChangesGrid tbody").sortable("destroy");
	};
	$("#usersWithPendingChangesGrid").jqGrid('setColProp','ID',{title:false});	
</script>
