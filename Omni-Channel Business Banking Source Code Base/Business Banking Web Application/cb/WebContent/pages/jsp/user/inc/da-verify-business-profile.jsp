<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:cinclude value1="${isProfileChanged}" value2="Y">

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="Business"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	<s:if test="%{#session.CATEGORY_BEAN != null}">
	<div  class="blockHead tableData"><span><strong><!--L10NStart-->Profile<!--L10NEnd--></strong></span></div>
	
<div class="paneWrapper">
 	<div class="paneInnerWrapper">
   		<table width="100%" class="tableData" border="0" cellspacing="0" cellpadding="3">
   			<tr class="header">
				<td class="sectionsubhead adminBackground" width="25%">Field Name</td>
				<td class="sectionsubhead adminBackground" width="25%">Old Value</td>
				<td class="sectionsubhead adminBackground" width="25%">New Value</td>
				<td class="sectionsubhead adminBackground" width="25%">Action</td>
			<tr>
			<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
				<tr>
					<td class="columndata" ><ffi:getProperty name="details" property="fieldName" /></td>
					<td class="columndata"><ffi:getProperty name="details" property="oldValue" /></td>
					<td class="columndata"><span class="sectionheadDA"><ffi:getProperty name="details" property="newValue" /></span></td>
					<td class="columndata"><ffi:getProperty name="CATEGORY_BEAN" property="UserAction" /></td>
				</tr>
			</ffi:list>
   		</table>
 </div>
</div>  
</s:if>
</ffi:cinclude>
