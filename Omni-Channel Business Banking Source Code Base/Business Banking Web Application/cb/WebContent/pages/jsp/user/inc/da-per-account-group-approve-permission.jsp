<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@page import="com.ffusion.csil.core.common.EntitlementsDefines"%>

<ffi:object id="EditAccountGroupPermissionsDA" name="com.ffusion.tasks.dualapproval.EditAccountGroupPermissionsDA" scope="session"/>

<ffi:setProperty name="EditAccountGroupPermissionsDA" property="approveAction" value="true" />
<ffi:setProperty name="EditAccountGroupPermissionsDA" property="EntitlementTypesMerged" value="AccountEntitlementsMerged"/>
<ffi:setProperty name="EditAccountGroupPermissionsDA" property="EntitlementTypePropertyLists" value="LimitsList"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="EditAccountGroupPermissionsDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
	<ffi:setProperty name="EditAccountGroupPermissionsDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="EditAccountGroupPermissionsDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="EditAccountGroupPermissionsDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="EditAccountGroupPermissionsDA" property="GroupId" value="${EditGroup_GroupId}" />
</s:if>

<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithLimitsName" value="AccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithoutLimitsName" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="AccountMerged" value="AccountEntitlementsMergedBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithLimitsName" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithoutLimitsName" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>


<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsMergedBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsMerged"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${EntitlementGroupParentId}"/>

<%--Get the list of component names for cross account category.  --%>
<ffi:object id="GetComponentsPerCategoryDA" name="com.ffusion.tasks.dualapproval.GetComponentsPerCategoryDA" />
	<ffi:setProperty name="GetComponentsPerCategoryDA" property="perCategory" 
									value="<%=EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_ACCOUNT %>"/>
<ffi:process name="GetComponentsPerCategoryDA"/>

<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
	<ffi:setProperty name="GetDACategorySubType" property="perCategory" 
													value="<%=EntitlementsDefines.ACCOUNT_GROUP %>"/>
<%String catSubType; %>
<%-- ComponentsPerCategoryNames is a list in session set using GetComponentsPerCategoryDA task.--%>
<ffi:list collection="ComponentsPerCategoryNames" items="component" >
	<%--Get the categorySubType using perCategory and component value. --%>
	<ffi:setProperty name="GetDACategorySubType" property="componentName" value="${component}"/>
	<ffi:process name="GetDACategorySubType"/>
	<ffi:setProperty name="GetCategories" property="ObjectId" value=""/>
	<ffi:setProperty name="GetCategories" property="ObjectType" value=""/>
	<ffi:setProperty name="GetTypesForEditingLimits" property="ComponentValue" value="${component}"/>
	<ffi:getProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME %>" assignTo="catSubType"/>
	<ffi:setProperty name="GetCategories" property="categorySubType" value="${catSubType}"/>
	
	<ffi:process name="GetTypesForEditingLimits"/>
	
	<ffi:setProperty name="approveCategory" property="ObjectId" value="" />
	<ffi:setProperty name="approveCategory" property="ObjectType" value="" />
	<ffi:setProperty name="approveCategory" property="categorySubType" value="" />
		
	<ffi:removeProperty name="categories" />	
	<ffi:process name="GetCategories" />
	<ffi:cinclude value1="${categories}" value2="" operator="notEquals">
	<ffi:removeProperty name="FilterDACategories" />
		<ffi:object id="FilterDACategories" name="com.ffusion.tasks.dualapproval.FilterDACategories" />
		<ffi:setProperty name="FilterDACategories" property="FilteredUnique" value="true" />
		<ffi:removeProperty name="dacategories" />
		<ffi:process name="FilterDACategories" />
		<ffi:list collection="dacategories" items="category">
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectType" value="${category.ObjectType}"/>
			<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectId" value="${category.ObjectId}"/>
			<ffi:setProperty name="GetCategories" property="ObjectId" value="${category.ObjectId}"/>
			<ffi:setProperty name="GetCategories" property="ObjectType" value="${category.ObjectType}"/>
			<ffi:process name="FilterEntitlementsForBusiness"/>
			<ffi:process name="EditAccountGroupPermissionsDA" />
		</ffi:list>
		<ffi:list collection="categories" items="category" startIndex="1" endIndex="1">
			<ffi:setProperty name="ApprovePendingChanges" property="ApproveBySubType" value="true"/>
			<ffi:setProperty name="ApprovePendingChanges" property="categorySubType" value="${catSubType}"/>
			<ffi:process name="ApprovePendingChanges"/>
		</ffi:list>
	</ffi:cinclude>
</ffi:list>

<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" />
<ffi:removeProperty name="catSubType" />
<ffi:removeProperty name="EditAccountGroupPermissionsDA"/>
<ffi:removeProperty name="dacategories" />