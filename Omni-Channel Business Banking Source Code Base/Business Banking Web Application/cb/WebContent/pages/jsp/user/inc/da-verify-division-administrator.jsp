<%-- This page displays old values and new values for administrator section in da-business-verify.jsp --%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:cinclude value1="${isAdministratorChanged}" value2="Y">

<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_MODIFIED%>" operator="equals">
	<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
	<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${EditGroup_GroupId}"/>
	<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="Admins"/>
	<ffi:process name="GetAdminsForGroup"/>
</ffi:cinclude>

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>

<%-- based on the business employee object, get the business employees for the business --%>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="TempBusinessEmployee"/>

<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="Admins"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
<ffi:process name="GenerateUserAndAdminLists"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="GenerateUserAndAdminLists"/>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
	<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
<ffi:process name="GenerateListsFromDAAdministrators"/>

<ffi:object id="GenerateCommaSeperatedAdminList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedAdminList"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminEmployees" value="OldAdminEmployees"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminGroups" value="OldAdminGroups"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="sessionName" value="OldAdminStringList"/>
<ffi:process name="GenerateCommaSeperatedAdminList"/>

<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminEmployees" value="AdminEmployees"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="adminGroups" value="AdminGroups"/>
<ffi:setProperty name="GenerateCommaSeperatedAdminList" property="sessionName" value="NewAdminStringList"/>
<ffi:process name="GenerateCommaSeperatedAdminList"/>
	<div class="blockHead"><s:text name="jsp.user_36" /></div>
	<div class="paneWrapper">
	   	<div class="paneInnerWrapper">
			<div class="header">
				<table cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr>
						<td class="sectionsubhead adminBackground" width="50%"><s:text name="admin.approval.old.administrators" /></td>
						<td class="sectionsubhead adminBackground" width="50%"><s:text name="admin.approval.new.administrators" /></td>
					</tr>
				</table>
			</div>
			<div class="paneContentWrapper">
				<table cellspacing="0" cellpadding="0" border="0" width="100%" class="tableData">
					<tr>
						<td class="columndata" width="50%">
							<ffi:getProperty name="OldAdminStringList"/>
						</td>
						<td class="columndata" width="50%">
							<span class="sectionheadDA"><ffi:getProperty name="NewAdminStringList"/></span>
						</td>
					</tr>
				</table>
			</div>
			</div>
		</div>


</ffi:cinclude>

<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>
<ffi:removeProperty name="OldAdminStringList"/>
<ffi:removeProperty name="NewAdminStringList"/>
<ffi:removeProperty name="OldAdminEmployees"/>
<ffi:removeProperty name="OldAdminGroups"/>
<ffi:removeProperty name="Admins"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
