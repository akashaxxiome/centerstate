<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<ffi:object name="com.ffusion.tasks.admin.GetMaxPerAccountLimits" id="GetMaxPerAccountLimits" scope="session"/>
	<ffi:setProperty name="GetMaxPerAccountLimits" property="EntitlementTypePropertyListsName" value="AccountEntitlementsWithLimits"/>
	<ffi:setProperty name="GetMaxPerAccountLimits" property="AccountId" value="${category.objectId}"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetMaxPerAccountLimits" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	    <ffi:setProperty name="GetMaxPerAccountLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	    <ffi:setProperty name="GetMaxPerAccountLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	    <ffi:setProperty name="GetMaxPerAccountLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
			<ffi:setProperty name="GetMaxPerAccountLimits" property="GroupId" value="${EditGroup_GroupId}" />
	</s:if>
	<ffi:setProperty name="GetMaxPerAccountLimits" property="PerTransactionMapName" value="PerTransactionLimits"/>
	<ffi:setProperty name="GetMaxPerAccountLimits" property="PerDayMapName" value="PerDayLimits"/>
	<ffi:setProperty name="GetMaxPerAccountLimits" property="PerWeekMapName" value="PerWeekLimits"/>
	<ffi:setProperty name="GetMaxPerAccountLimits" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxPerAccountLimits"/>
<ffi:removeProperty name="GetMaxPerAccountLimits"/>

<ffi:object id="GetMaxLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxLimitForPeriod" scope="session" />
	<ffi:setProperty name="GetMaxLimitForPeriod" property="PerTransactionMapName" value="PerTransactionLimits"/>
	<ffi:setProperty name="GetMaxLimitForPeriod" property="PerDayMapName" value="PerDayLimits"/>
	<ffi:setProperty name="GetMaxLimitForPeriod" property="PerWeekMapName" value="PerWeekLimits"/>
	<ffi:setProperty name="GetMaxLimitForPeriod" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxLimitForPeriod"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="NoLimitString" value="no"/>

<ffi:object id="CheckForRedundantDALimits" name="com.ffusion.tasks.dualapproval.CheckForRedundantDALimits" scope="session" />
	<ffi:setProperty name="CheckForRedundantDALimits" property="LimitListName" value="AccountEntitlementsWithLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MaxPerTransactionLimitMapName" value="PerTransactionLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MaxPerDayLimitMapName" value="PerDayLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MaxPerWeekLimitMapName" value="PerWeekLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MaxPerMonthLimitMapName" value="PerMonthLimits"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="MergedListName" value="AccountEntitlementsMerged"/>
	
	<ffi:setProperty name="CheckForRedundantDALimits" property="categorySessionName" value="CATEGORY_BEAN"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
	<ffi:setProperty name="CheckForRedundantDALimits" property="ObjectId" value="${category.objectId}"/>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="CheckForRedundantDALimits" property="GroupId" value="${EditGroup_GroupId}"/>
	</s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="CheckForRedundantDALimits" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
		<ffi:setProperty name="CheckForRedundantDALimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="CheckForRedundantDALimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="CheckForRedundantDALimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	</s:if>

<ffi:process name="CheckForRedundantDALimits"/>
<ffi:removeProperty name="CheckForRedundantDALimits"/>
<ffi:removeProperty name="GetMaxLimitForPeriod"/>
