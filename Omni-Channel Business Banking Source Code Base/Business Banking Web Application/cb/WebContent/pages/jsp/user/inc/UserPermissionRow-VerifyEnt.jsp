<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="Compare" property="Value1" value="TRUE"/>
<ffi:object id="CurrencyObject" name="com.ffusion.beans.common.Currency" scope="request"/>
<ffi:setProperty name="CurrencyObject" property="CurrencyCode" value="${SecureUser.BaseCurrency}"/>

<% int limitIndex = ( ( Integer )session.getAttribute( "limitIndex" ) ).intValue(); %>

<ffi:object id="CheckEntitlementByMember" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" scope="session" />
    <ffi:setProperty name="CheckEntitlementByMember" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
    <ffi:setProperty name="CheckEntitlementByMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
    <ffi:setProperty name="CheckEntitlementByMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
    <ffi:setProperty name="CheckEntitlementByMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>

<ffi:setProperty name="EntitlementsDisplayedNames" property="Add" value="${LimitType.OperationName}"/>
<ffi:setProperty name="DisplayedLimit" value="TRUE"/>
<ffi:setProperty name="Compare" property="Value2" value="${CheckEntitlement}"/>
<ffi:setProperty name="hideEnt" value="false" />
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
<ffi:cinclude value1="${CanInitRow}" value2="FALSE">
	<ffi:setProperty name="hideEnt" value="true" />
</ffi:cinclude> 
</ffi:cinclude>
<ffi:cinclude value1="${hideEnt}" value2="false">
<ffi:removeProperty name="hideEnt" />
<tr>
	<ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
		<ffi:cinclude value1="${HasAdmin}" value2="true" operator="equals">
			<td class="tbrd_t" valign="middle">
				<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_ADMIN_PARTNER %>"/>
				
				<ffi:cinclude value1="${CanAdminRow}" value2="TRUE" operator="equals">
					<div align="center">
						<% String checked = ""; %>
						<ffi:getProperty name="admin${limitIndex}${TemplateIndex}" assignTo="checked"/>
						<input type="checkbox" disabled name=name value="value" <ffi:cinclude value2="<%=checked%>" value1="${LimitType.Value}" operator="equals"> checked </ffi:cinclude> border="0">
					</div>
				</ffi:cinclude>
				<ffi:cinclude value1="${CanAdminRow}" value2="TRUE" operator="notEquals">
					<div align="center">
						&nbsp;
					</div>
				</ffi:cinclude>
			</td>
		</ffi:cinclude>
	</ffi:cinclude>
	
	<ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="${LimitType.OperationName}"/>
	<ffi:cinclude value1="${CheckEntitlementObjectType}" value2="" operator="notEquals">
		<ffi:setProperty name="CheckEntitlementByMember" property="ObjectType" value="${CheckEntitlementObjectType}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${CheckEntitlementObjectId}" value2="" operator="notEquals">
		<ffi:setProperty name="CheckEntitlementByMember" property="ObjectId" value="${CheckEntitlementObjectId}"/>
	</ffi:cinclude>
	<ffi:process name="CheckEntitlementByMember"/>
	
	<td class="tbrd_t" valign="middle">
		<div align="center">
					<% String z = ""; %>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="equals">
					<ffi:getProperty name="entitlement${limitIndex}" assignTo="z"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TemplateIndex}" value2="" operator="notEquals">
					<ffi:getProperty name="entitlement${limitIndex}_${TemplateIndex}" assignTo="z"/>
					</ffi:cinclude>
				<ffi:cinclude value1="${CanInitRow}" value2="TRUE" operator="equals">
					<input type="checkbox" disabled name=name value="value" <ffi:cinclude value2="<%=z%>"  value1="${LimitType.OperationName}" operator="equals"> checked </ffi:cinclude> border="0">
				</ffi:cinclude>
				<ffi:cinclude value1="${CanInitRow}" value2="TRUE" operator="notEquals">
					&nbsp;
				</ffi:cinclude>
			 <% pageContext.setAttribute("z", ""); %>
		</div>
	</td>
			<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL %>"/>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	            <%  String classParam = "class = 'tbrd_t columndata'"; %>
				<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">
	
					<% String indentLevel; %>
					<ffi:getProperty name="LimitType" property="Value" assignTo="indentLevel"/>
					<%
						int indentLevelAsInt = Integer.parseInt( indentLevel );
						if (indentLevelAsInt > 0) {
	                        classParam = "class = 'tbrd_t indent" + Integer.toString(indentLevelAsInt) + " columndata'";
						}
					%>
				</ffi:cinclude>
				<%-- Requires Approval Column addition --%>
              <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
              	<td <%=classParam%> valign="middle" align="left">
						<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
						<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
							<ffi:process name="GetEntitlementPropertyValues"/>
							<ffi:getProperty name="GetEntitlementPropertyValues" property="PropertyValue"/>&nbsp;
						</ffi:cinclude>
						<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
							<ffi:getProperty name="LimitType" property="OperationName"/>&nbsp;
						</ffi:cinclude>
				</td>	
	           <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
	           	<ffi:cinclude value1="TRUE" value2="${CanInitRow}" operator="equals">
	               <ffi:setProperty name="EditRequiresApproval" property="OperationName" value="${LimitType.OperationName}" />
	               <ffi:setProperty name="EditRequiresApproval" property="ExecOpSelVerify" value="true" />	
	               <ffi:process name="EditRequiresApproval" /> 	              
	               <td class="tbrd_t" valign="middle" colspan="7" >&nbsp;&nbsp;
	         	    <ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="true" operator="equals">
		             <input type="checkbox"  disabled <ffi:getProperty name="EditRequiresApproval" property="OpSelected" />	/>		
		            </ffi:cinclude>
		            <ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="false" operator="equals">
			      	&nbsp;
		            </ffi:cinclude>
	               </td>
	             </ffi:cinclude>
	            </ffi:cinclude>
               </ffi:cinclude>
	        	<%-- END Requires Approval Column addition --%>
	        	<%-- Non Requires Approval column --%>
	           <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="notEquals">
				<td <%=classParam%> valign="middle" colspan="7" align="left">
						<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
						<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
							<ffi:process name="GetEntitlementPropertyValues"/>
							<ffi:getProperty name="GetEntitlementPropertyValues" property="PropertyValue"/>&nbsp;
						</ffi:cinclude>
						<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
							<ffi:getProperty name="LimitType" property="OperationName"/>&nbsp;
						</ffi:cinclude>
				</td>
			</ffi:cinclude>
			  <%-- END Non Requires Approval column --%>
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				<%  String classParam = "tbrd_t columndata"; %>
				<ffi:cinclude value1="${LimitType.Value}" value2="" operator="notEquals">

				<% String indentLevel; %>
				<ffi:getProperty name="LimitType" property="Value" assignTo="indentLevel"/>
				<%
					int indentLevelAsInt = Integer.parseInt( indentLevel );
					if (indentLevelAsInt > 0) {
						classParam = "tbrd_t indent" + Integer.toString(indentLevelAsInt) + " columndata";
					}
				%>
				</ffi:cinclude>
			  <%-- Requires Approval Column addition --%>
              <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
              	<td class='<ffi:getPendingStyle fieldname="${LimitType.OperationName}" defaultcss="<%=classParam %>" 
						dacss='<%=classParam + " columndataDA" %>' 
						sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />' 
						valign="middle" align="left">
					<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
					<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
						<ffi:process name="GetEntitlementPropertyValues"/>
						<ffi:getProperty name="GetEntitlementPropertyValues" property="PropertyValue"/>&nbsp;
					</ffi:cinclude>
					<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
						<ffi:getProperty name="LimitType" property="OperationName"/>&nbsp;
					</ffi:cinclude>
				</td>	
	           <ffi:cinclude value1="${EditRequiresApproval}" value2="" operator="notEquals">
	            <ffi:cinclude value1="TRUE" value2="${CanInitRow}" operator="equals">
	               <ffi:setProperty name="EditRequiresApproval" property="OperationName" value="${LimitType.OperationName}" />
	               <ffi:setProperty name="EditRequiresApproval" property="ExecOpSelVerify" value="true" />	
	               <ffi:process name="EditRequiresApproval" /> 	              
	               <td class="tbrd_t" valign="middle" colspan="7" >&nbsp;&nbsp;
	         	    <ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="true" operator="equals">
		             <input type="checkbox"  disabled <ffi:getProperty name="EditRequiresApproval" property="OpSelected" />	/>		
		            </ffi:cinclude>
		            <ffi:cinclude value1="${EditRequiresApproval.DisplayOp}" value2="false" operator="equals">
			      	&nbsp;
		            </ffi:cinclude>
	               </td>
	             </ffi:cinclude>
	            </ffi:cinclude>
               </ffi:cinclude>
	        	<%-- END Requires Approval Column addition --%>
	        	<%-- Non Requires Approval column --%>
	           <ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="notEquals">
				<td class='<ffi:getPendingStyle fieldname="${LimitType.OperationName}" defaultcss="<%=classParam %>" 
						dacss='<%=classParam + " columndataDA" %>' 
						sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />' 
						valign="middle" colspan="7" align="left">
					<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
					<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
						<ffi:process name="GetEntitlementPropertyValues"/>
						<ffi:getProperty name="GetEntitlementPropertyValues" property="PropertyValue"/>&nbsp;
					</ffi:cinclude>
					<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
						<ffi:getProperty name="LimitType" property="OperationName"/>&nbsp;
					</ffi:cinclude>
				</td>
			</ffi:cinclude>
			  <%-- END Non Requires Approval column --%>
        	</ffi:cinclude>
</tr>
</ffi:cinclude>
