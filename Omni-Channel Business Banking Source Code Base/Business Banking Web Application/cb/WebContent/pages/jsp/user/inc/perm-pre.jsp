<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroup"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page import="com.sap.banking.web.util.entitlements.EntitlementsUtil"%>

<%-- remove objects used by the edit ACH company forms from the session --%>
<ffi:removeProperty name="EditACHCompanyAccess"/>
<ffi:removeProperty name="ApprovalAdminByBusiness"/>
<ffi:removeProperty name="CheckForRedundantACHLimits"/>
<ffi:removeProperty name="crossACHEntLists"/>
<ffi:removeProperty name="perACHEntLists"/>
<ffi:removeProperty name="limitInfoList"/>
<ffi:removeProperty name="ACHCompanyID"/>
<ffi:removeProperty name="ACHCompany"/>
<ffi:removeProperty name="perBatchMax"/>
<ffi:removeProperty name="dailyMax"/>

<ffi:setProperty name="confirmURL" value='${SecurePath}user/editautoentitle_confirm.jsp'/>
<ffi:object id="ModifyAutoEntitleSettings" name="com.ffusion.tasks.autoentitle.ModifyAutoEntitleSettings" scope="session"/>
<ffi:setProperty name="backURL" value="${SecurePath}user/editautoentitle.jsp"/>

<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EntitlementGroupSessionKey" value="<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>"/>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
<ffi:process name="GetEntitlementGroup"/>
</s:if>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">

<!-- Get Business Employees -->
<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchEntitlementProfiles" value="TRUE"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:setProperty name="GetBusinessEmployees" property="BusinessEmployeesSessionName" value="entitlementProfiles"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>
<!-- Get Business Employees Ends -->

<%-- SetBusinessEmployee.Id should be passed in the request --%>
<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${BusinessEmployeeId}"/>
<ffi:process name="SetBusinessEmployee"/>
<ffi:setProperty name="ModifyAutoEntitleSettings" property="EntitlementGroupMemberSessionKey" value="ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE"/>

<% com.ffusion.beans.user.BusinessEmployee businessEmployee = (com.ffusion.beans.user.BusinessEmployee) session.getAttribute("BusinessEmployee");
    EntitlementGroupMember member = EntitlementsUtil.getEntitlementGroupMemberForBusinessEmployee(businessEmployee);
    session.setAttribute( "ENTITLEMENT_GROUP_MEMBER_AUTOENTITLE", member );
%>
</s:if>
<ffi:process name="ModifyAutoEntitleSettings"/>

 <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
 <s:if test="%{#session.Section == 'Users'}">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.Id}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>" />
		<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="true" />
		<ffi:setProperty name="GetDACategoryDetails" property="MultipleCategoriesSessionName" value="EntitlementBean" />
	<ffi:process name="GetDACategoryDetails"/>
 </s:if>
 <ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.Id}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>" />
		<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="true" />
		<ffi:setProperty name="GetDACategoryDetails" property="MultipleCategoriesSessionName" value="EntitlementBean" />
	<ffi:process name="GetDACategoryDetails"/>
 </ffi:cinclude>
 <ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
 </ffi:cinclude>
  <ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
 </ffi:cinclude>
  <ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories"/>
		<ffi:setProperty name="GetCategories" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCategories" />
	<ffi:removeProperty name="GetCategories"/>
 </ffi:cinclude>
 </ffi:cinclude>