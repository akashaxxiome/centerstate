<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:setProperty name="confirmation_done" value="false"/>

<%-- Set the page heading --%>
<ffi:setProperty name='PageHeading' value='Edit Feature Access and Limits (per location)'/>
<ffi:removeProperty name='PageHeading2'/>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>
<%--
<ffi:include page="${PathExt}user/inc/SetPageText.jsp" />
--%>
<ffi:setProperty name="SavePermissionsWizard" value="${PermissionsWizard}"/>


<%-- Initialize Request --%>
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>
<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<%-- Get the BusinessEmployee  --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:process name="SetBusinessEmployee"/>
</s:if>

<%-- get the entitlementgroup for this new user back to session --%>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
<ffi:process name="GetEntitlementGroup"/>


<%-- Dual Approval Processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<s:if test="%{#session.Section == 'Users'}">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</s:if>
		<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
	<ffi:process name="GetCategories"/>
	<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="<%=IDualApprovalConstants.CATEGORY_SUB_PER_LOCATION %>"/>
	<ffi:object name="com.ffusion.tasks.dualapproval.GetPendingObjectsByTypeSubType" id="getPendingObjects"/>
		<ffi:setProperty name="getPendingObjects" property="objectIdsSessionName" value="daObjectIds"/>
		<ffi:setProperty name="getPendingObjects" property="objectType" value="<%=EntitlementsDefines.LOCATION%>"/>
		<ffi:setProperty name="getPendingObjects" property="categorySubType" value="${Category_Sub_Session_Name}"/>
	<ffi:process name="getPendingObjects"/>

	<s:if test="%{#session.Section == 'Users'}">
		<ffi:removeProperty name="CROSS_ACC_CAT_SUBTYPE" />
		<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
			<ffi:setProperty name="GetDACategorySubType" property="operationName" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>"/>
			<ffi:setProperty name="GetDACategorySubType" property="perCategory" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT%>" />
			<ffi:setProperty name="GetDACategorySubType" property="categorySubType" value="CROSS_ACC_CAT_SUBTYPE"/>
		<ffi:process name="GetDACategorySubType"/>	
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${CROSS_ACC_CAT_SUBTYPE}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoriesSessionName" value="dependentDaEntitlements"/>
		<ffi:process name="GetDACategoryDetails"/>
		<ffi:removeProperty name="CROSS_ACC_CAT_SUBTYPE" />
		<ffi:removeProperty name="GetDACategorySubType" />	
	</s:if>
	<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
		<ffi:removeProperty name="CROSS_ACC_CAT_SUBTYPE" />
		<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
			<ffi:setProperty name="GetDACategorySubType" property="operationName" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>"/>
			<ffi:setProperty name="GetDACategorySubType" property="perCategory" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT%>" />
			<ffi:setProperty name="GetDACategorySubType" property="categorySubType" value="CROSS_ACC_CAT_SUBTYPE"/>
		<ffi:process name="GetDACategorySubType"/>	
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${CROSS_ACC_CAT_SUBTYPE}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoriesSessionName" value="dependentDaEntitlements"/>
		<ffi:process name="GetDACategoryDetails"/>
		<ffi:removeProperty name="CROSS_ACC_CAT_SUBTYPE" />
		<ffi:removeProperty name="GetDACategorySubType" />	
	</ffi:cinclude>
</ffi:cinclude>

<%-- Determine if we need to have a list of divisions
	 We get a division list if we came from Divisions, or if we
	 came from Users and the user is a member of the entitlement
	 group representing the business.
--%>
<ffi:setProperty name="UseDivisionList" value="false"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="UseDivisionList" value="true"/>
</ffi:cinclude>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Business">
		<ffi:setProperty name="UseDivisionList" value="true"/>
	</ffi:cinclude>
</s:if>

<s:if test="%{#session.Section == 'UserProfile'}">
                <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
                <ffi:process name="GetEntitlementGroup"/>
                <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Business">
                                <ffi:setProperty name="UseDivisionList" value="true"/>
                </ffi:cinclude>
                
                <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="UserProfile">
                                <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
                                <ffi:process name="GetEntitlementGroup"/>
                                <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Business">
                                                <ffi:setProperty name="UseDivisionList" value="true"/>
                                </ffi:cinclude>
                </ffi:cinclude>

                <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${EditGroup_GroupId}"/>
                <ffi:process name="GetEntitlementGroup"/>   
</s:if>

<%-- Get the list of divisions, if necessary --%>
<ffi:cinclude value1="${UseDivisionList}" value2="true" operator="equals">
	<ffi:object id="GetDescendantsByType" name="com.ffusion.efs.tasks.entitlements.GetDescendantsByType" scope="session"/>
	<ffi:setProperty name="GetDescendantsByType" property="GroupID" value="${Business.EntitlementGroupId}"/>
	<ffi:setProperty name="GetDescendantsByType" property="GroupType" value="Division"/>
	<ffi:process name="GetDescendantsByType"/>
	<ffi:removeProperty name="GetDescendantsByType" />
</ffi:cinclude>
<%-- Determine which division we have --%>
<ffi:cinclude value1="${DivisionId}" value2="" operator="equals">
	<ffi:cinclude value1="${UseDivisionList}" value2="true" operator="equals">
		<%-- We have a business, or a user belonging to the Business' entitlement group.
		     Take the first division from the Divisions list --%>
		<ffi:list collection="Descendants" items="Descendant" startIndex="1" endIndex="1">
			<ffi:setProperty name="DivisionId" value="${Descendant.GroupId}" />
		</ffi:list>
	</ffi:cinclude>
	<ffi:cinclude value1="${UseDivisionList}" value2="true" operator="notEquals">
		<%-- The entilement group of the user is either Division or Group --%>
		<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Division" operator="equals">
			<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Division" operator="notEquals">
			<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.ParentId}"/>
		</ffi:cinclude>
		<s:if test="%{#session.Section == 'UserProfile'}">
            <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
            <ffi:process name="GetEntitlementGroup"/>
            <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Division" operator="equals">
				<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			</ffi:cinclude>
            <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="equals">
            	<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.ParentId}"/>
            </ffi:cinclude>
            <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="UserProfile">
				<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${Entitlement_EntitlementGroup.ParentId}"/>
                <ffi:process name="GetEntitlementGroup"/>
	            <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Division" operator="equals">
					<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.GroupId}"/>
				</ffi:cinclude>
	            <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="equals">
	            	<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	            </ffi:cinclude>
            </ffi:cinclude>
            <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${EditGroup_GroupId}"/>
            <ffi:process name="GetEntitlementGroup"/>   
		</s:if>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${DivisionId}" value2="" operator="notEquals">
<%-- Get the info for the Division --%>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${DivisionId}"/>
<ffi:process name="GetEntitlementGroup"/>
<ffi:setProperty name="DivisionName" value="${Entitlement_EntitlementGroup.GroupName}"/>

<%-- get the entitlementgroup for this new user back in the session, it's used by the user limits pages --%>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
<ffi:process name="GetEntitlementGroup"/>

<%-- Get the list of locations for the division --%>
<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount" scope="session" />
<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%=com.ffusion.tasks.GetDisplayCount.CASHCON %>"/>
<ffi:process name="GetDisplayCount"/>
<ffi:removeProperty name="GetDisplayCount"/>

<ffi:cinclude value1="${runSearch}" value2="" operator="equals">
	<ffi:setProperty name="runSearch" value="true"/>
</ffi:cinclude>

<ffi:cinclude value1="${runSearch}" value2="true" operator="equals">
	<ffi:object id="GetLocations" name="com.ffusion.tasks.cashcon.GetLocations"/>
	<ffi:setProperty name ="GetLocations" property="DivisionID" value="${DivisionId}"/>
	<ffi:setProperty name="GetLocations" property="MaxResults" value="${DisplayCount}"/>
	<ffi:process name="GetLocations"/>

	<ffi:cinclude value1="${SortedBy}" value2="" operator="notEquals">
		<ffi:setProperty name="Locations" property="SortedBy" value="${SortedBy}"/>
	</ffi:cinclude>

	<%-- Make sure the currently selected location is in the list --%>
	<ffi:setProperty name="Locations" property="Filter" value="LocationBPWID=${Location_BPWID}"/>
	<ffi:cinclude value1="${Locations.Size}" value2="0" operator="equals">
		<ffi:setProperty name="Location_BPWID" value=""/>
	</ffi:cinclude>
	<ffi:setProperty name="Locations" property="Filter" value=""/>

	<ffi:object id="GetTypesForEditingLimits" name="com.ffusion.tasks.admin.GetTypesForEditingLimits" scope="session"/>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_COMPANY %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_DIVISION %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="UserProfile" operator="equals">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_GROUP %>"/>
		</ffi:cinclude> 
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:setProperty name="GetTypesForEditingLimits" property="CategoryValue" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_PER_USER %>"/>
		</s:if>
		<ffi:setProperty name="GetTypesForEditingLimits" property="LocationWithLimitsName" value="LocationEntitlementsWithLimitsBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="LocationWithoutLimitsName" value="LocationEntitlementsWithoutLimitsBeforeFilter"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="LocationMerged" value="LocationEntitlementsMergedBeforeFilter"/>
	<ffi:process name="GetTypesForEditingLimits"/>
	<ffi:removeProperty name="GetTypesForEditingLimits"/>

	<%-- Initialize the task which will store the changes --%>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.dualapproval.EditLocationPermissionsDA" scope="session"/>
		  	<ffi:setProperty name="EditLocationPermissions" property="approveAction" value="false" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	   		<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.admin.EditLocationPermissions" scope="session"/>
	    </ffi:cinclude>
    </s:if>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
				<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.dualapproval.EditLocationPermissionsDA" scope="session"/>
				<ffi:setProperty name="EditLocationPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
				<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.admin.EditLocationPermissions" scope="session"/>
			</ffi:cinclude>
			<!--  4 for normal profile, 5 for primary profile, 6t for sec profile, 7 for cross profile -->
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.dualapproval.EditLocationPermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.admin.EditLocationPermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditLocationPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.dualapproval.EditLocationPermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.admin.EditLocationPermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditLocationPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="5" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.dualapproval.EditLocationPermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.admin.EditLocationPermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditLocationPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="6" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.dualapproval.EditLocationPermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.admin.EditLocationPermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditLocationPermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:object id="EditLocationPermissions" name="com.ffusion.tasks.admin.EditLocationPermissions" scope="session"/>
		</ffi:cinclude>
		<ffi:setProperty name="EditLocationPermissions" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="EditLocationPermissions" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="EditLocationPermissions" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
    
	<ffi:setProperty name="EditLocationPermissions" property="LocationBPWID" value="${Location_BPWID}"/>
	<ffi:setProperty name="EditLocationPermissions" property="GroupId" value="${EditGroup_GroupId}"/>
	<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
		<ffi:setProperty name="EditLocationPermissions" property="ProfileId" value="${Business.Id}"/>
	</ffi:cinclude>
	<ffi:setProperty name="EditLocationPermissions" property="EntitlementTypesWithLimits" value="LocationEntitlementsWithLimits"/>
	<ffi:setProperty name="EditLocationPermissions" property="EntitlementTypesWithoutLimits" value="LocationEntitlementsWithoutLimits"/>
	<ffi:setProperty name="EditLocationPermissions" property="EntitlementTypesMerged" value="LocationEntitlementsMerged"/>
	<ffi:setProperty name="EditLocationPermissions" property="SaveLastRequest" value="false"/>
	<ffi:setProperty name="EditLocationPermissions" property="EntitlementTypePropertyLists" value="LimitsList"/>
</ffi:cinclude>

<%-- Determine if the location search should be on or off --%>
<ffi:setProperty name="showSearch" value="TRUE"/>
<ffi:cinclude value1="${GetLocations.LocationName},${GetLocations.LocationID}" value2="," operator="equals">
	<ffi:cinclude value1="${DisplayCount}" value2="${Locations.Size}" operator="notEquals">
		<ffi:setProperty name="showSearch" value="FALSE"/>
	</ffi:cinclude>
</ffi:cinclude>

<%-- If no location is selected, pick the first one in the list --%>
<ffi:cinclude value1="${Location_BPWID}" value2="" operator="equals">
	<ffi:removeProperty name="Location"/>
	<ffi:list collection="Locations" items="Location" startIndex="1" endIndex="1">
		<ffi:setProperty name="Location_BPWID" value="${Location.LocationBPWID}"/>
	</ffi:list>
</ffi:cinclude>

<%-- Set the currently selected location in session --%>
<ffi:cinclude value1="${Locations.size}" value2="0" operator="notEquals">
	<ffi:cinclude value1="${Location_BPWID}" value2="" operator="notEquals">
		<ffi:object id="SetLocationSearchResult" name="com.ffusion.tasks.cashcon.SetLocationSearchResult"/>
		<ffi:setProperty name="SetLocationSearchResult" property="BPWID" value="${Location_BPWID}"/>
		<ffi:process name="SetLocationSearchResult"/>
		<ffi:removeProperty name="SetLocationSearchResult"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="CheckOperationEntitlement" name="com.ffusion.tasks.admin.CheckOperationEntitlement"/>

<ffi:setProperty name="GetLocations" property="SuccessURL" value="${SecurePath}user/perlocationpermissions.jsp"/>

<%-- set variables used to interchange row colors --%>

<ffi:object id="ApprovalAdminByBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
<ffi:setProperty name="ApprovalAdminByBusiness" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="ApprovalAdminByBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.APPROVALS_ADMIN %>"/>
<ffi:process name="ApprovalAdminByBusiness"/>

<%-- filter the entitlements so that those entitlements restricted by the BusinessAdmin entitlement group 
	(as well as its parents) would not be displayed, along with the restricted entitlements' control children --%>
<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="LocationEntitlementsMergedBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="LocationEntitlementsMerged"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroup.ParentId}"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.LOCATION %>"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectId" value="${Location_BPWID}"/>
<ffi:process name="FilterEntitlementsForBusiness"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="LocationEntitlementsWithLimitsBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="LocationEntitlementsWithLimits"/>
<ffi:process name="FilterEntitlementsForBusiness"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="LocationEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="LocationEntitlementsWithoutLimits"/>
<ffi:process name="FilterEntitlementsForBusiness"/>
<ffi:removeProperty name="FilterEntitlementsForBusiness"/>

<ffi:object id="HandleParentChildDisplay" name="com.ffusion.tasks.admin.HandleParentChildDisplay"/>
<ffi:setProperty name="HandleParentChildDisplay" property="EntitlementTypePropertyLists" value="LocationEntitlementsMerged"/>
<ffi:setProperty name="HandleParentChildDisplay" property="PrefixName" value="entitlement"/>
<ffi:setProperty name="HandleParentChildDisplay" property="AdminPrefixName" value="admin"/>
<ffi:setProperty name="HandleParentChildDisplay" property="ParentChildName" value="ParentChildLists"/>
<ffi:setProperty name="HandleParentChildDisplay" property="ChildParentName" value="ChildParentLists"/>
<ffi:process name="HandleParentChildDisplay"/>

<ffi:object name="com.ffusion.tasks.admin.GetMaxPerLocationLimits" id="GetMaxPerLocationLimits" scope="session"/>
<ffi:setProperty name="GetMaxPerLocationLimits" property="EntitlementTypePropertyListsName" value="LocationEntitlementsWithLimits"/>
<ffi:setProperty name="GetMaxPerLocationLimits" property="LocationId" value="${Location_BPWID}"/>
<ffi:setProperty name="GetMaxPerLocationLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
    <ffi:setProperty name="GetMaxPerLocationLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
    <ffi:setProperty name="GetMaxPerLocationLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
    <ffi:setProperty name="GetMaxPerLocationLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<ffi:setProperty name="GetMaxPerLocationLimits" property="PerTransactionMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="GetMaxPerLocationLimits" property="PerDayMapName" value="PerDayLimits"/>
<ffi:setProperty name="GetMaxPerLocationLimits" property="PerWeekMapName" value="PerWeekLimits"/>
<ffi:setProperty name="GetMaxPerLocationLimits" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxPerLocationLimits"/>
<ffi:removeProperty name="GetMaxPerLocationLimits"/>

<ffi:object id="GetMaxLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxLimitForPeriod" scope="session" />
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerTransactionMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerDayMapName" value="PerDayLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerWeekMapName" value="PerWeekLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxLimitForPeriod"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="NoLimitString" value="no"/>

<ffi:object id="CheckForRedundantLimits" name="com.ffusion.efs.tasks.entitlements.CheckForRedundantLimits" scope="session" />
<ffi:setProperty name="CheckForRedundantLimits" property="LimitListName" value="LocationEntitlementsWithLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerTransactionLimitMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerDayLimitMapName" value="PerDayLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerWeekLimitMapName" value="PerWeekLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerMonthLimitMapName" value="PerMonthLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MergedListName" value="LocationEntitlementsMerged"/>
<ffi:setProperty name="CheckForRedundantLimits" property="SuccessURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/perlocationpermissions-verify-init.jsp?PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/> 
<ffi:setProperty name="CheckForRedundantLimits" property="BadLimitURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/perlocationpermissions.jsp&UseLastRequest=TRUE&DisplayErrors=TRUE&PermissionsWizard=${PermissionsWizard}" URLEncrypt="TRUE"/> 
<ffi:setProperty name="CheckForRedundantLimits" property="GroupId" value="${EditGroup_GroupId}"/>
<ffi:setProperty name="CheckForRedundantLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.LOCATION %>"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
</s:if>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:cinclude value1="${Location_BPWID}" value2="" operator="notEquals">
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
			<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${Location_BPWID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.LOCATION %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<s:if test="%{#session.Section == 'Users'}">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			</s:if>
			<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
			</ffi:cinclude>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_PER_LOCATION %>" />
		<ffi:process name="GetDACategoryDetails" />
	
		<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>"/>
		<ffi:process name="GetDACategoryDetails" />
	</ffi:cinclude>
</ffi:cinclude>

<script type="text/javascript"><!--

	(function($){
		<ffi:cinclude value1='${DivisionId}' value2='' operator='notEquals'>
			initializeParentChild();
		</ffi:cinclude>
	})(jQuery);
	/* sets the checkboxes on a form to checked or unchecked.
	 * form - the form to operate on
	 * value - to set the checkbox.checked property
	 * exp - set only checkboxes matching exp to value
	 */
	function setCheckboxes( form, value, exp ) {
		for( i=0; i < form.elements.length; i++ ){
			if( form.elements[i].type == "checkbox" &&
				form.elements[i].name.indexOf( exp ) != -1 ) {
				form.elements[i].checked = value;
			}
		}
	}
	
	function initializeParentChild() {
		parentChildArray = new Array();
		childParentArray = new Array();
		
		prefixes = new Array( "<ffi:getProperty name="HandleParentChildDisplay" property="PrefixName"/>","<ffi:getProperty name="HandleParentChildDisplay" property="AdminPrefixName"/>" );
		
<ffi:object id="counterMath" name="com.ffusion.beans.util.IntegerMath"/>		
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:setProperty name="counterMath" property="Value2" value="1"/>

		// Populate the parentChildArray
<ffi:list collection="ParentChildLists" items="childList">
		parentChildArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array( 
	<ffi:list collection="childList" items="entry" startIndex="1" endIndex="1">
			"<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="childList" items="entry" startIndex="2">
			, "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);
	
	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />	
</ffi:list>	

		// Populate the childParentArray
<ffi:setProperty name="counterMath" property="Value1" value="0"/>
<ffi:list collection="ChildParentLists" items="parentList">
		childParentArray[<ffi:getProperty name="counterMath" property="Value1"/>] = new Array( 
	<ffi:list collection="parentList" items="entry" startIndex="1" endIndex="1">
			"<ffi:getProperty name="entry"/>"
	</ffi:list>
	<ffi:list collection="parentList" items="entry" startIndex="2">
			, "<ffi:getProperty name="entry"/>"
	</ffi:list>
		);
	
	<ffi:setProperty name="counterMath" property="Value1" value="${counterMath.Add}" />	
</ffi:list>	
	}
	
	ns.admin.resetFormForPerLocation = function ()
	{
		$('#permLimitFrm').get(0).reset();
	}
// --></script>

</ffi:cinclude>

<ffi:setProperty name="NumTotalColumns" value="9"/>
<ffi:setProperty name="AdminCheckBoxType" value="checkbox"/>
<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- We need to determine if this user is able to administer any group.
		 Only if the user being edited is an administrator do we show the Admin checkbox. --%>
		 
	<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	<ffi:setProperty name="CanAdministerAnyGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
	
	<ffi:process name="CanAdministerAnyGroup"/>
	<ffi:removeProperty name="CanAdministerAnyGroup"/>
	
	<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="FALSE" operator="equals">
		<ffi:setProperty name="NumTotalColumns" value="8"/>
		<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${OneAdmin}" value2="TRUE" operator="equals">
		<ffi:cinclude value1="${SecureUser.ProfileID}" value2="${BusinessEmployee.Id}" operator="equals">
			<ffi:setProperty name="NumTotalColumns" value="8"/>
			<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		<ffi:cinclude value1="${AdminCheckBoxType}" value2="checkbox" >
			<ffi:setProperty name="AdminCheckBoxTypeDA" value="hidden"/>
		</ffi:cinclude>
	</ffi:cinclude>
	
</s:if>

<!-- Hide admin checkbox if dual approval mode is set -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
	<ffi:setProperty name="NumTotalColumns" value="8"/>
	<ffi:setProperty name="AdminCheckBoxType" value="hidden"/>
</ffi:cinclude>

<s:include value="/pages/jsp/user/inc/disableAdminCheckBoxForProfiles.jsp" />

<%-- Requires Approval --%>
<ffi:removeProperty name="REQUIRES_APPROVAL" />
<ffi:removeProperty name="CheckRequiresApprovalPerAcct" />
