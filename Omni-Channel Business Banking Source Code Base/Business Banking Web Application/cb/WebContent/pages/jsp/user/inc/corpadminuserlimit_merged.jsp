<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:setProperty name="Compare" property="Value1" value="TRUE"/>

<% int limitIndex = 0; %>
<% session.setAttribute( "limitIndex", new Integer( 0 ) ); %>

<ffi:object id="CheckParentEnt" name="com.ffusion.tasks.admin.CheckOperationEntitlement"/>

<ffi:object id="CheckEntitlementByMember" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" scope="session" />
    <ffi:setProperty name="CheckEntitlementByMember" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
    <ffi:setProperty name="CheckEntitlementByMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
    <ffi:setProperty name="CheckEntitlementByMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
    <ffi:setProperty name="CheckEntitlementByMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>

<ffi:object id="GetEntitlementPropertyValues" name="com.ffusion.efs.tasks.entitlements.GetEntitlementPropertyValues" scope="session"/>
<ffi:setProperty name="GetEntitlementPropertyValues" property="PropertyName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
<ffi:setProperty name="GetEntitlementPropertyValues" property="EntTypePropertyListName" value="LimitType"/>

<ffi:object id="CheckEntitlement" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" scope="session" />

<ffi:object id="HandleAccountRowDisplay" name="com.ffusion.tasks.admin.HandleAccountRowDisplay"/>
<ffi:setProperty name="HandleAccountRowDisplay" property="EntitlementTypePropertyLists" value="LimitsList"/>
<ffi:setProperty name="HandleAccountRowDisplay" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:setProperty name="HandleAccountRowDisplay" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
<ffi:setProperty name="HandleAccountRowDisplay" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
<ffi:setProperty name="HandleAccountRowDisplay" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
<ffi:setProperty name="HandleAccountRowDisplay" property="DaCategorySessionName" value="dependentDaEntitlements"/>
<ffi:setProperty name="HandleAccountRowDisplay" property="CoreAccount" value="${Account.coreAccount}"/>
<ffi:setProperty name="HandleAccountRowDisplay" property="OneAdmin" value="${OneAdmin}"/>
<ffi:cinclude value1="${CheckEntitlementObjectType}" value2="" operator="notEquals">
	<ffi:setProperty name="HandleAccountRowDisplay" property="ObjectType" value="${CheckEntitlementObjectType}"/>
</ffi:cinclude>

<ffi:cinclude value1="${CheckEntitlementObjectId}" value2="" operator="notEquals">
	<ffi:setProperty name="HandleAccountRowDisplay" property="ObjectId" value="${CheckEntitlementObjectId}"/>
</ffi:cinclude>
<ffi:process name="HandleAccountRowDisplay"/>

<ffi:list collection="LimitsList" items="LimitType">
	<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CAN_ADMIN_ROW %>"/>
	<ffi:setProperty name="CanAdminRow" value="${LimitType.Value}"/>

	<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.CAN_INIT_ROW %>"/>
	<ffi:setProperty name="CanInitRow" value="${LimitType.Value}"/>

	<%-- checking if LimitType is a limit or just an entitlement --%>
	<ffi:setProperty name="GetGroupLimits" property="OperationName" value="${LimitType.OperationName}"/>

	<ffi:setProperty name="LimitType" property="CurrentProperty" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.DISPLAY_ROW %>"/>
	<ffi:cinclude value1="${LimitType.Value}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.YES %>" operator="equals">
		<ffi:setProperty name="LimitType" property="CurrentProperty" value="isLimit"/>
		<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="equals">
			<ffi:cinclude value1="TRUE" value2="${CanInitRow}" operator="equals">
				<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
					<s:include value="UserPermissionRow-Limit-View.jsp"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
						<s:include value="UserPermissionRow-Limit.jsp"/>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="TRUE" value2="${CanInitRow}" operator="notEquals">
				<s:include value="UserPermissionRow-Ent.jsp"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:setProperty name="LimitType" property="CurrentProperty" value="isLimit"/>
		<ffi:cinclude value1="${LimitType.IsCurrentPropertySet}" value2="true" operator="notEquals">
			<s:include value="UserPermissionRow-Ent.jsp"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<% limitIndex++; %>
	<% session.setAttribute( "limitIndex", new Integer( limitIndex ) ); %>
</ffi:list>

<ffi:removeProperty name="GetEntitlementPropertyValues"/>
<ffi:removeProperty name="CheckParentEnt"/>
<ffi:removeProperty name="CheckEntitlement"/>
<ffi:removeProperty name="CheckEntitlementByMember"/>
<ffi:removeProperty name="allowApproval"/>
<ffi:removeProperty name="CanInitRow"/>
<ffi:removeProperty name="CanAdminRow"/>
