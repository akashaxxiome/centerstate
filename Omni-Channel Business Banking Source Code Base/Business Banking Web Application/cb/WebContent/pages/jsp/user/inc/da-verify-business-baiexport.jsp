<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<div  class="blockHead tableData"><span><strong>BAI Export Settings</strong></span></div>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
  		<table width="100%" class="tableData" border="0" cellspacing="0" cellpadding="3">
			<tr class="header">
				<td width="25%" class="sectionsubhead adminBackground" >Field Name</td>
				<td width="25%"  class="sectionsubhead adminBackground" >Old Value</td>
				<td  width="40%" class="sectionsubhead adminBackground" >New Value</td>
				<td  width="10%"class="sectionsubhead adminBackground" >Action</td>
			</tr>
			<ffi:list collection="DA_BAIEXPORT_SETTINGS.daItems" items="details">
	<ffi:cinclude value1="${details.fieldName}" value2="senderIDType" operator="equals">	
		<tr>
			<td class="columndata" >Sender Identification</td>
			<td class="columndata" >
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					<!--L10NStart-->Use affiliate bank <!--L10NEnd--><ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
					<ffi:setProperty name="AddBAIExportSettingsToDA" property="CurrentProperty" value="senderIDCustom" />										
					<ffi:getProperty name="AddBAIExportSettingsToDA" property="CurrentPropertyOldValue"/>
				</ffi:cinclude>
			</td>
			<td class="columndata sectionheadDA" >
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					<!--L10NStart-->Use affiliate bank <!--L10NEnd--><ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_SENDER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">															
					<ffi:getProperty name="AddBAIExportSettingsToDA" property="SenderIDCustom"/>
				</ffi:cinclude>
			</td>
			<td  class="columndata"><ffi:getProperty name="DA_BAIEXPORT_SETTINGS" property="userAction" /></td>
		</tr>
	</ffi:cinclude>
	<ffi:cinclude value1="${details.fieldName}" value2="receiverIDType" operator="equals">	
		<tr>
			
			<td class="columndata" >Receiver Identification</td>
			<td class="columndata" >
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					<!--L10NStart-->Use affiliate bank <!--L10NEnd--><ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
					<ffi:setProperty name="AddBAIExportSettingsToDA" property="CurrentProperty" value="receiverIDCustom" />										
					<ffi:getProperty name="AddBAIExportSettingsToDA" property="CurrentPropertyOldValue"/>
				</ffi:cinclude>
			</td>
			<td class="columndata sectionheadDA" >
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					<!--L10NStart-->Use affiliate bank <!--L10NEnd--><ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">															
					<ffi:getProperty name="AddBAIExportSettingsToDA" property="receiverIDCustom"/>
				</ffi:cinclude>
			</td>
			<td  class="columndata"><ffi:getProperty name="DA_BAIEXPORT_SETTINGS" property="userAction" /></td>
		</tr>
	</ffi:cinclude>
	<ffi:cinclude value1="${details.fieldName}" value2="ultimateReceiverIDType" operator="equals">	
		<tr>
			
			<td class="columndata" >Ultimate Receiver Identification</td>
			<td class="columndata" >
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					Use affiliate bank <ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
					<ffi:setProperty name="AddBAIExportSettingsToDA" property="CurrentProperty" value="ultimateReceiverIDCustom" />										
					<ffi:getProperty name="AddBAIExportSettingsToDA" property="CurrentPropertyOldValue"/>
				</ffi:cinclude>
			</td>
			<td class="columndata sectionheadDA" >
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					Use affiliate bank <ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ULTIMATE_RECEIVER_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">															
					<ffi:getProperty name="AddBAIExportSettingsToDA" property="ultimateReceiverIDCustom"/>
				</ffi:cinclude>
			</td>
			<td  class="columndata"><ffi:getProperty name="DA_BAIEXPORT_SETTINGS" property="userAction" /></td>
		</tr>
	</ffi:cinclude>
	<ffi:cinclude value1="${details.fieldName}" value2="originatorIDType" operator="equals">	
		<tr>
			
			<td class="columndata" >Originator Identification</td>
			<td class="columndata" >
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					<!--L10NStart-->Use account <!--L10NEnd--><ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">
					<ffi:setProperty name="AddBAIExportSettingsToDA" property="CurrentProperty" value="originatorIDCustom" />										
					<ffi:getProperty name="AddBAIExportSettingsToDA" property="CurrentPropertyOldValue"/>
				</ffi:cinclude>
			</td>
			<td class="columndata sectionheadDA" >
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_BANK_ROUTING_NUMBER %>" operator="equals">
					<!--L10NStart-->Use account <!--L10NEnd--><ffi:getProperty name="TempBankIdentifierDisplayText"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_ORIGINATOR_IDENTIFICATION_OPTION_CUSTOM_VALUE %>" operator="equals">															
					<ffi:getProperty name="AddBAIExportSettingsToDA" property="originatorIDCustom"/>
				</ffi:cinclude>
			</td>
			<td  class="columndata"><ffi:getProperty name="DA_BAIEXPORT_SETTINGS" property="userAction" /></td>
		</tr>
	</ffi:cinclude>
	<ffi:cinclude value1="${details.fieldName}" value2="customerAccountNumberType" operator="equals">	
		<tr>
			
			<td class="columndata" >Customer Account Number Format</td>
			<td class="columndata" >
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" operator="equals">
					Use account number only
				</ffi:cinclude>
				<ffi:cinclude value1="${details.oldValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" operator="equals">
					Use <ffi:getProperty name="TempBankIdentifierDisplayText"/> and account number (eg. 567483920:50000-1)
				</ffi:cinclude>
			</td>
			<td class="columndata sectionheadDA" >
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_NUMBER %>" operator="equals">
					Use account number only
				</ffi:cinclude>
				<ffi:cinclude value1="${details.newValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.BAI_EXPORT_CUSTOMER_ACCOUNT_NUMBER_OPTION_ACCOUNT_AND_ROUTING_NUMBER %>" operator="equals">															
					Use <ffi:getProperty name="TempBankIdentifierDisplayText"/> and account number (eg. 567483920:50000-1)
				</ffi:cinclude>
			</td>
			<td  class="columndata"><ffi:getProperty name="DA_BAIEXPORT_SETTINGS" property="userAction" /></td>
		</tr>
	</ffi:cinclude>
</ffi:list>
		</table>
  	</div>
</div>
