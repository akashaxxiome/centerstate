<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>

<script language="javascript">
<!--
	$("#searchAccountSelectId").selectmenu({width: 138});
	
    $(function(){

    	setInitDetails();
    	
    });
	
	function accountSelect() {
		//$('#currencyCode').html($('#accountID :selected').attr("currencycode"));
		var selectedVal = $('#accountID :selected').text();
		if(selectedVal!=null && selectedVal!="" && selectedVal!= "undefined" && selectedVal !="All Accounts"){
			var currency = selectedVal.split("-")[3];
			$('#currencyCode').html(currency);
			$("#searchFormButtonId").trigger('click');
		}else if(selectedVal ="All Accounts"){
			$('#accountID :selected').val('')
			$("#searchFormButtonId").trigger('click');
		}
	}
	
//-->
</script>

<ffi:object id="SetAccountsCollectionDisplayText"  name="com.ffusion.tasks.util.SetAccountsCollectionDisplayText" />
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CollectionName" value="<%= SessionNames.PARENTS_ACCOUNTS %>"/>
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
<ffi:process name="SetAccountsCollectionDisplayText"/>
<ffi:removeProperty name="SetAccountsCollectionDisplayText"/>

<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.PARENTS_ACCOUNTS %>"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.PERMISSIONACCOUNTS_INDEX %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="AccountDisplayText"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>
									
										
<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.PARENTS_ACCOUNTS %>"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.PERMISSIONACCOUNTS_INDEX_BY_TYPE %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="Type"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>
										
<ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.PARENTS_ACCOUNTS %>"/>
<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.PERMISSIONACCOUNTS_INDEX_BY_CURRENCY %>"/>
<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_HASH %>"/>
<ffi:setProperty name="BuildIndex" property="BeanProperty" value="CurrencyCode"/>
<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>

<%-- <ffi:cinclude value1="${showSearch}" value2="TRUE" operator="equals"> --%>
<div id="searchAccountAccessPermissionsDiv" >
	<s:form id="searchForm" namespace="/pages/user" action="accountAccess" theme="simple" method="post">
   	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<input type="hidden" name="runSearch" value="true">
		<table border="0" cellspacing="1" cellpadding="1" class="tableData marginTop20" align="center">
			<%-- <tr>
				<td class="sectionsubhead" colspan="2" height="20">&nbsp;&nbsp;&gt; <s:text name="jsp.default_548"/></td>
			</tr>
			<tr>
				<td class="sectionsubhead" height="20" align="right"><s:text name="jsp.default_294"/></td>
				<td class="sectionsubhead"><input type="text" name="SearchAcctsByNameNumType.NickName" style="width:100px;" size="17" maxlength="40" class="ui-widget-content ui-corner-all"></td>
			</tr> --%>
			<tr>
				<td class="sectionsubhead"  ><s:text name="jsp.default_16"/></td>
				<td class="sectionsubhead">
					<div class="selectBoxHolder">
						<select id="accountID" class="txtbox" name="SearchAcctsByNameNumType.Id" size="1" style="width:300px">
						<option>
							All Accounts
						</option>
		                    <ffi:cinclude value1="${SearchAcctsByNameNumType.Id}" value2="" operator="notEquals">
		                    	<ffi:cinclude value1="${SearchAcctsByNameNumType.Id}" value2="-1" operator="notEquals">
		                    	
									<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
									<ffi:setProperty name="SetAccount" property="AccountsName" value="FilteredAccounts"/>
									<ffi:setProperty name="SetAccount" property="ID" value="${SearchAcctsByNameNumType.Id}"/>
									<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount"/>
									<ffi:process name="SetAccount"/>
									
									
									<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
									<s:set var="AccountDisplayTextTaskBean" value="#session.SelectedAccount" scope="request" />
									<ffi:setProperty name="AccountDisplayTextTask" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
									<ffi:process name="AccountDisplayTextTask"/>
									<ffi:setProperty name="permissionsAccDisText" value="${AccountDisplayTextTask.AccountDisplayText}" />
								</ffi:cinclude>
							</ffi:cinclude>
		                </select>
		            </div>
				</td>
				<td class="sectionsubhead" height="20">
						<sj:a
							id="searchFormButtonId"
							formIds="searchForm"
							targets="accountaccessDiv"
							button="true"
							validate="false"
							cssStyle="display:none"
							><span class="sapUiIconCls icon-sys-find"></span><s:text name="jsp.default_373"/></sj:a>
				</td>
			</tr>
			<%-- <tr>							
				<td class="sectionsubhead" align="right"><s:text name="jsp.default_20"/></td>
				<td class="sectionsubhead">
					<select id="searchAccountSelectId" name="SearchAcctsByNameNumType.Type" class="ui-widget-content ui-corner-all">
						<option value=""><s:text name="jsp.user_563"/></option>
						<ffi:list collection="AccountTypesList" items="AccountType">
							<option value="<ffi:getProperty name="AccountType"/>" <ffi:cinclude value1="${AccountType}" value2="${SearchAcctsByNameNumType.Type}">selected</ffi:cinclude>><ffi:getProperty name="AccountType"/></option>
						</ffi:list>
					</select>
				</td>
			</tr> --%>
				
		</table>
	</s:form>
</div>
<%-- </ffi:cinclude> --%>
