<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<script language="javascript">
$(function(){
	$("#selectUserStateID").combobox({width: 200});
});
</script>

<%
    String userState = null;
%>

<!-- default selected country and state -->
<ffi:getProperty name="BusinessEmployee" property="State" assignTo="userState"/>
<% if (userState == null) { userState = ""; } %>

<ffi:object id="GetStatesForCountry" name="com.ffusion.tasks.util.GetStatesForCountry" scope="session" />
    <ffi:setProperty name="GetStatesForCountry" property="CountryCode" value='<%=request.getParameter("countryCode") %>' />
<ffi:process name="GetStatesForCountry" />

<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
	<select class="txtbox" tabindex="6" id="selectUserStateID" name="BusinessEmployee.State" value="<ffi:getProperty name="BusinessEmployee" property="State"/>">
		<option<ffi:cinclude value1="<%= userState %>" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.default_376"/></option>
		<ffi:list collection="GetStatesForCountry.StateList" items="item">
			<option <ffi:cinclude value1="<%= userState %>" value2="${item.StateKey}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="StateKey"/>"><ffi:getProperty name='item' property='Name'/></option>
		</ffi:list>
	</select>
</ffi:cinclude>
<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
	<ffi:setProperty name="BusinessEmployee" property="State" value=""/>
</ffi:cinclude>