<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<ffi:setL10NProperty name='PageHeading' value='Confirm Approve Profile'/>

<%
	Object be = session.getAttribute("BusinessEmployee");
	if( be != null && be instanceof com.ffusion.beans.ExtendABean ){
		Object oldBe = ((com.ffusion.beans.ExtendABean)be).clone();
		Object tmpBe = ((com.ffusion.beans.ExtendABean)be).clone();
		session.setAttribute( "tmpBusinessEmployee", tmpBe );
		session.setAttribute( "OldBusinessEmployee", oldBe );
		((com.ffusion.beans.user.BusinessEmployee)be).setPassword(null);
	}
%>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.businessID}"/>
<ffi:process name="GetDAItem" />

<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
	<ffi:setProperty name="EntitlementGroupParentId" value="${Business.EntitlementGroup.ParentId}" />
	
	<s:include value="/pages/jsp/user/inc/da-profile-approve-permission.jsp" />
	
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE %>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
		<s:action namespace="/pages/user" name="approveBusinessEntitlementProfileChanges">            
			<s:param name="ItemId" value="%{#session.itemId}"></s:param>			
        </s:action>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${CATEGORY_BEAN}" value2="">
		<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
			<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.businessID}" />
			<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>" />
			<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="${itemId}" />
			<ffi:setProperty name="ApprovePendingChangesTask" property="approveDAItem" value="TRUE" />
		<ffi:process name="ApprovePendingChangesTask"/>
	</ffi:cinclude>
	
	<ffi:setProperty name="tempDisplayName" value="${BusinessEmployee.UserName}"/> 
</ffi:cinclude>

		<div align="center">
		
		<table class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">
			
			<tr>

				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<br>
						<tr>
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_ADDED %>">
								<td class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-profile-add-confirm.jsp-2" parm0="${userName}"/></td>
							</ffi:cinclude>
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								<td class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-profile-add-confirm.jsp-1" parm0="${userName}"/></td>
							</ffi:cinclude>
							<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED %>">							
								<td class="columndata" align="center"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminprofiledelete-confirm.jsp-1" parm0="${userName}"/></td>
							</ffi:cinclude>
						</tr>
						<br>
						<tr>
							<td align="center">
									<sj:a 
										id="doneWizardID"
										button="true" 
										onclick="doneWizard();"
									><s:text name="jsp.default_538" /></sj:a></td>
						</tr>
					</table>
				</td>

			</tr>
			
		</table>
			<p></p>
		</div>

<script>
function doneWizard(){
	$('#approvalWizardDialogID').dialog('close');;
}
</script>
<ffi:removeProperty name="EntitlementGroupParentId"/>
<ffi:removeProperty name="ApprovePendingChangesTask"/>
<ffi:removeProperty name="OldBusinessEmployee"/>
<ffi:removeProperty name="tmpBusinessEmployee"/>
<ffi:removeProperty name="BusinessEmployee"/>
<ffi:removeProperty name="BusinessEmployees"/>
<ffi:removeProperty name="oldDAObject"/>
<ffi:removeProperty name="DAItem"/>
<ffi:removeProperty name="EditBusinessUser"/>
<ffi:removeProperty name="SetBusinessEmployee"/>
