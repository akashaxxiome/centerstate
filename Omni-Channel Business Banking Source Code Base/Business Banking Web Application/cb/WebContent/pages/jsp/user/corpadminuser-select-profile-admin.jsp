<%@page import="com.ffusion.beans.user.User"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
String userName1=null;
String userName2=null;
%>
<ffi:getProperty name="BusinessEmployee" property="UserName" assignTo="userName1" />
<ffi:getProperty name="<%= com.ffusion.efs.tasks.SessionNames.SECURE_USER %>" property="UserName" assignTo="userName2" />
         <ffi:cinclude value1="${GroupAdmins.Size}" value2="0" operator="notEquals">
             <%-- Allow changing primary admin only if the user is not changing its own primary admin --%>
			<ffi:cinclude value1="<%= userName1 %>" value2="<%= userName2 %>" operator="notEquals">
                 <select id="selectAdminID" class="txtbox" name="BusinessEmployee.PrimaryAdmin">
                     <option value="0"><s:text name="jsp.default_375"/></option>
                     <ffi:setProperty name="GroupAdmins" property="SortedBy" value="NAME"/>
                     <ffi:list collection="GroupAdmins" items="employee">
                         <ffi:cinclude value1="${employee.AccountStatus}" value2="<%= User.STATUS_ACTIVE %>">
                         <option value="<ffi:getProperty name='employee' property='Id'/>"
                                 <ffi:cinclude value1="${BusinessEmployee.PrimaryAdmin}" value2="${employee.Id}">selected</ffi:cinclude>
                                 ><ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
                         </option>
                         </ffi:cinclude>
                     </ffi:list>
                 </select>
                 <span class="sectionhead_greyDA">
                 	<br>
				<ffi:list collection="GroupAdmins" items="employee">
					<ffi:cinclude value1="${BusinessEmployee.MasterBusinessEmployee.PrimaryAdmin}" value2="${employee.Id}">
						<ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
					</ffi:cinclude>
				</ffi:list>
			</span>
             </ffi:cinclude>
             <ffi:cinclude value1="<%= userName1 %>" value2="<%= userName2 %>" operator="equals">
                 <ffi:list collection="GroupAdmins" items="employee">
                         <ffi:cinclude value1="${BusinessEmployee.PrimaryAdmin}" value2="${employee.Id}">
                             <ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
                         </ffi:cinclude>
                 </ffi:list>
             </ffi:cinclude>
         </ffi:cinclude>