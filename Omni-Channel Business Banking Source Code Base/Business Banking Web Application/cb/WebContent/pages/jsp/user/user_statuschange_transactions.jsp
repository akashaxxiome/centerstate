<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<%-- Determine if any of the secondary users marked as inactive have any pending transactions. --%>
<ffi:setProperty name="IncludePendingTXNotice" value=""/>
<ffi:setProperty name="FoundUserWithExpiredTransaction" value=""/>

<ffi:list collection="ModifiedSecondaryUsers" items="TempSecondaryUser">
    <ffi:cinclude value1="${TempSecondaryUser.AccountStatus}" value2='<%= com.ffusion.beans.user.User.STATUS_ACTIVE %>' operator="notEquals">
        <%--
        Filter all the pending transactions collections so that only the transactions created by the secondary user will
        be displayed on the page.
        --%>
        <ffi:setProperty name="PaymentPendingStatus" value='<%= "" + com.ffusion.beans.billpay.PaymentStatus.PMS_SCHEDULED %>'/>
        <ffi:setProperty name="RecPayments" property="Filter" value="SUBMITTED_BY==${TempSecondaryUser.Id},STATUS==${PaymentPendingStatus},AND"/>

        <ffi:object name="com.ffusion.tasks.billpay.FindRecPayments" id="FindRecPayments" scope="request"/>
        <ffi:setProperty name="FindRecPayments" property="RemovePending" value="true"/>
        <ffi:process name="FindRecPayments"/>

        <ffi:setProperty name="Payments" property="Filter" value="SUBMITTED_BY==${TempSecondaryUser.Id},STATUS==${PaymentPendingStatus},AND"/>

        <ffi:setProperty name="TransferPendingStatus" value='<%= "" + com.ffusion.beans.banking.TransferStatus.TRS_SCHEDULED %>'/>
        <ffi:setProperty name="RecTransfers" property="Filter" value="SUBMITTED_BY==${TempSecondaryUser.Id},STATUS=${TransferPendingStatus},AND"/>

        <ffi:object name="com.ffusion.tasks.banking.FindRecTransfers" id="FindRecTransfers" scope="request"/>
        <ffi:setProperty name="FindRecTransfers" property="RemovePending" value="true"/>
        <ffi:process name="FindRecTransfers"/>

        <ffi:setProperty name="Transfers" property="Filter" value="SUBMITTED_BY==${TempSecondaryUser.Id},STATUS=${TransferPendingStatus},AND"/>

        <ffi:cinclude value1="${RecPayments.Size}" value2="0" operator="notEquals">
            <ffi:setProperty name="IncludePendingTXNotice" value="true"/>
        </ffi:cinclude>

        <ffi:cinclude value1="${Payments.Size}" value2="0" operator="notEquals">
            <ffi:setProperty name="IncludePendingTXNotice" value="true"/>
        </ffi:cinclude>

        <ffi:cinclude value1="${RecTransfers.Size}" value2="0" operator="notEquals">
            <ffi:setProperty name="IncludePendingTXNotice" value="true"/>
        </ffi:cinclude>

        <ffi:cinclude value1="${Transfers.Size}" value2="0" operator="notEquals">
            <ffi:setProperty name="IncludePendingTXNotice" value="true"/>
        </ffi:cinclude>

        <%-- Check for any outstanding transactions that may have expired. --%>
        <ffi:object name="com.ffusion.tasks.multiuser.GetExpiredTransactions" id="GetExpiredRecPayments" scope="request"/>
        <ffi:setProperty name="GetExpiredRecPayments" property="SourceCollectionSessionName" value="RecPayments"/>
        <ffi:setProperty name="GetExpiredRecPayments" property="DestinationCollectionSessionName" value="ExpiredRecPayments"/>
        <ffi:setProperty name="GetExpiredRecPayments" property="Initialize" value="TRUE"/>
        <ffi:setProperty name="GetExpiredRecPayments" property="Process" value="TRUE"/>
        <ffi:process name="GetExpiredRecPayments"/>

        <ffi:object name="com.ffusion.tasks.multiuser.GetExpiredTransactions" id="GetExpiredPayments" scope="request"/>
        <ffi:setProperty name="GetExpiredPayments" property="SourceCollectionSessionName" value="Payments"/>
        <ffi:setProperty name="GetExpiredPayments" property="DestinationCollectionSessionName" value="ExpiredPayments"/>
        <ffi:setProperty name="GetExpiredPayments" property="Initialize" value="TRUE"/>
        <ffi:setProperty name="GetExpiredPayments" property="Process" value="TRUE"/>
        <ffi:process name="GetExpiredPayments"/>

        <ffi:object name="com.ffusion.tasks.multiuser.GetExpiredTransactions" id="GetExpiredRecTransfers" scope="request"/>
        <ffi:setProperty name="GetExpiredRecTransfers" property="SourceCollectionSessionName" value="RecTransfers"/>
        <ffi:setProperty name="GetExpiredRecTransfers" property="DestinationCollectionSessionName" value="ExpiredRecTransfers"/>
        <ffi:setProperty name="GetExpiredRecTransfers" property="Initialize" value="TRUE"/>
        <ffi:setProperty name="GetExpiredRecTransfers" property="Process" value="TRUE"/>
        <ffi:process name="GetExpiredRecTransfers"/>

        <ffi:object name="com.ffusion.tasks.multiuser.GetExpiredTransactions" id="GetExpiredTransfers" scope="request"/>
        <ffi:setProperty name="GetExpiredTransfers" property="SourceCollectionSessionName" value="Transfers"/>
        <ffi:setProperty name="GetExpiredTransfers" property="DestinationCollectionSessionName" value="ExpiredTransfers"/>
        <ffi:setProperty name="GetExpiredTransfers" property="Initialize" value="TRUE"/>
        <ffi:setProperty name="GetExpiredTransfers" property="Process" value="TRUE"/>
        <ffi:process name="GetExpiredTransfers"/>

        <ffi:cinclude value1="${ExpiredRecPayments.Size}" value2="0" operator="notEquals">
            <ffi:setProperty name="FoundUserWithExpiredTransaction" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${ExpiredPayments.Size}" value2="0" operator="notEquals">
            <ffi:setProperty name="FoundUserWithExpiredTransaction" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${ExpiredRecTransfers.Size}" value2="0" operator="notEquals">
            <ffi:setProperty name="FoundUserWithExpiredTransaction" value="true"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${ExpiredTransfers.Size}" value2="0" operator="notEquals">
            <ffi:setProperty name="FoundUserWithExpiredTransaction" value="true"/>
        </ffi:cinclude>

    </ffi:cinclude>
</ffi:list>

<ffi:cinclude value1="${IncludePendingTXNotice}" value2="true" operator="notEquals">
	<s:include value="/pages/jsp/user/user_list_save.jsp" />
</ffi:cinclude>


<ffi:cinclude value1="${IncludePendingTXNotice}" value2="true" operator="equals">
    <ffi:removeProperty name='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_TRANSACTIONS_TRANSFER_OR_CANCEL %>' startsWith="true"/>

    <ffi:setProperty name="PageTitle" value="Manage Users"/>
    <ffi:setProperty name="topMenu" value="home"/>
    <ffi:setProperty name="subMenu" value="preferences"/>
    <ffi:setProperty name="sub2Menu" value="manageusers"/>

<div align="center">
<form name="UserStatusChangeForm" id="transferCancelSaveStatusUserForm" action="" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
    <table border="0" cellpadding="0" cellspacing="0" width="721" class="tableData tableAlerternateRowColor tdTopBottomPadding5">
        <tr>
            <td><img src="/efs/efs/multilang/grafx/spacer.gif" height="1"></td>
            <!-- <td><img src="/efs/efs/multilang/grafx/spacer.gif" width="26" height="1"></td> -->
        </tr>
        <tr class="ui-widget-header">
            <td colspan="2">Edit Secondary Users - Pending Transactions</td>
            <!-- <td class="header"><img src="/efs/efs/multilang/grafx/header_corner.gif" width="21" height="19"></td> -->
        </tr>
        <tr>
            <td colspan="2">
                <div style="padding-left: 10px; padding-right: 10px;" class="instructions">
                    <br>
    <ffi:cinclude value1="${FoundUserWithExpiredTransaction}" value2="true" operator="equals">
                    You have chosen to deactivate one or more of your secondary users. However, the following selected users have outstanding transactions that have expired. This operation cannot be completed  until the following expired transactions are cancelled or updated so that they have valid dates.
    </ffi:cinclude>
    <ffi:cinclude value1="${FoundUserWithExpiredTransaction}" value2="true" operator="notEquals">
                    You have chosen to deactivate one or more of your secondary users. However, the following selected users have outstanding transactions. Please select the treatment of these transactions.
    </ffi:cinclude>
                </div>
            </td>
        </tr>
    </table>
    <ffi:flush/>
    <ffi:object name="com.ffusion.beans.util.IntegerMath" id="Counter" scope="request"/>
    <ffi:setProperty name="Counter" property="Value1" value="0"/>
    <ffi:setProperty name="Counter" property="Value2" value="1"/>

    <ffi:list collection="ModifiedSecondaryUsers" items="TempSecondaryUser">
        <ffi:cinclude value1="${TempSecondaryUser.AccountStatus}" value2='<%= com.ffusion.beans.user.User.STATUS_ACTIVE %>' operator="notEquals">
            <ffi:setProperty name="SecondaryUserToModifyID" value="${TempSecondaryUser.Id}"/>
            <ffi:setProperty name="TransferOrCancelTransactionsInputName" value='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_TRANSACTIONS_TRANSFER_OR_CANCEL %>'/>
            <ffi:setProperty name="TransferOrCancelTransactionsInputName" value="${TransferOrCancelTransactionsInputName}_${Counter.Value1}"/>
            <ffi:setProperty name="${TransferOrCancelTransactionsInputName}" value='<%= com.ffusion.tasks.multiuser.MultiUserTask.MODIFY_ACTION_TRANSFER %>'/>
			<s:include value="/pages/jsp/user/inc/user_pending_transactions.jsp" />
        </ffi:cinclude>
        <ffi:setProperty name="Counter" property="Value1" value="${Counter.Add}"/>
    </ffi:list>
    <!-- <table border="0" cellpadding="0" cellspacing="0" width="721" class="tableData tdTopBottomPadding5">
        <tr>
            <td class="table_border_bottom"><img src="/efs/efs/multilang/grafx/spacer.gif" width="721" height="1"></td>
        </tr>
    </table> -->
    <div align="center">
		<sj:a 	id="cancelStatusChange"	button="true"  onClickTopics="secondaryUsers.closePendingTxPayementsDialog" >&nbsp;<s:text name="jsp.default_82"/></sj:a>			
    <ffi:cinclude value1="${FoundUserWithExpiredTransaction}" value2="true" operator="notEquals">
			<sj:a 	id="transferCancelTxSaveStatus"	button="true"  onClickTopics="secondaryUsers.TransferOrCancelTransactions" >&nbsp;<s:text name="jsp.default_366"/></sj:a>			
    </ffi:cinclude>
    </div>
</form>
    
</div>
    <ffi:removeProperty name="TempURL"/>
    <ffi:removeProperty name="CancelButton"/>
    <ffi:removeProperty name="SaveButton"/>

    <ffi:setProperty name="BackURL" value="${SecurePath}user/user_statuschange_transactions.jsp" URLEncrypt="true"/>
</ffi:cinclude>

<ffi:removeProperty name="IncludePendingTXNotice"/>
<ffi:removeProperty name="FoundUserWithExpiredTransaction"/>
