<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_peraccountgrouppermissions" className="moduleHelpClass"/>
<% 
	if(request.getParameter("runSearch") != null)
		request.setAttribute("runSearch", request.getParameter("runSearch"));
	session.setAttribute("ComponentIndex", request.getParameter("ComponentIndex")); 
	session.setAttribute("perm_target", request.getParameter("perm_target"));
	session.setAttribute("completedTopics", request.getParameter("completedTopics"));
	request.setAttribute("nextPermission", request.getParameter("nextPermission"));
	if(request.getParameter("perAccountGroupInit") != null){
		session.setAttribute("perAccountGroupInit",request.getParameter("perAccountGroupInit"));
	}
%>

<div id="<ffi:getProperty name="perm_target"/>">
<s:include value="%{#session.PagesPath}user/inc/checkentitled-peraccountgroupcomponents.jsp" />
<%-- Getting the name of component from the ArrayList that should be in session from previous
	 page, based on the index that is passed in through the url --%>
<%
	if (session.getAttribute("ComponentNamesPer")!=null) {
		int index = Integer.valueOf((String)(session.getAttribute("ComponentIndex"))).intValue();
		String ComponentName = (String)(((ArrayList)(session.getAttribute("ComponentNamesPer"))).get(index));
		session.setAttribute("ComponentName", ComponentName);
	}
%>

<ffi:cinclude value1="${runSearch}" value2="" operator="equals">
	<s:include value="inc/peraccountgrouppermissions-pre.jsp" />
</ffi:cinclude>
<div class="marginTop10">
<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
	<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/peraccountgrouppermissions.jsp-1" parm0="${ComponentName}"/></TITLE>
</ffi:cinclude>
<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
	<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/peraccountgrouppermissions-view.jsp-1" parm0="${ComponentName}"/></TITLE>
</ffi:cinclude>
</div>
<%-- include javascripts for top navigation menu bar --%>
<s:include value="inc/parentchild_js.jsp"/>

<div id="perm_peraccountgroup" align="center">
<ul id="formerrors"></ul>
<div align="center" class="marginTop10"><ffi:getProperty name="Context"/></div>
	<!-- group view form -->
	<div id="acctGroupViewDiv">
		<s:include value="peraccountgrouppermissions_groupform.jsp" />
	</div>
	<!-- The Account limit Table -->
	<div id="acctLimitDiv">
		<s:include value="peraccountgrouppermissions_accountlimitform.jsp?nextPermission=%{#attr.nextPermission}" />
	</div>
	<br>
</div>
</div>
<%-- Set Top level CSS for DA mode, highlight those categories which have pending approval changes --%>
<s:include value="%{#session.PagesPath}user/inc/set-page-da-css.jsp" />

<ffi:removeProperty name="GetMaxLimitForPeriod"/>
<ffi:removeProperty name="GetGroupLimits"/>
<ffi:removeProperty name="Entitlement_Limits"/>

<ffi:removeProperty name="NonAccountEntitlementsWithLimits"/>
<ffi:removeProperty name="NonAccountEntitlementsWithoutLimits"/>
<ffi:removeProperty name="limitIndex"/>
<ffi:removeProperty name="CheckRequiresApprovalPerAcct" />

<ffi:removeProperty name="accountGroupDisplaySize"/>
<ffi:removeProperty name="numAccountGroups"/>
<ffi:removeProperty name="LastRequest"/>
<ffi:removeProperty name="PageHeading2"/>
<ffi:removeProperty name="DisplayErrors"/>
<ffi:removeProperty name="CheckEntitlementObjectType"/>
<ffi:removeProperty name="CheckEntitlementObjectId"/>
<ffi:removeProperty name="HandleParentChildDisplay"/>
<ffi:removeProperty name="ParentChild"/>
<ffi:removeProperty name="HasAdmin"/>
<ffi:removeProperty name="PerAccount"/>
<ffi:removeProperty name="AdminCheckBoxType"/>
<ffi:removeProperty name="NumTotalColumns"/>
<ffi:removeProperty name="ChildParentLists"/>
<ffi:removeProperty name="ParentChildLists"/>

<ffi:removeProperty name="counterMath"/>
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:removeProperty name="PermissionsWizardSubMenu"/>
<ffi:removeProperty name="SavePermissionsWizard"/>
<ffi:removeProperty name="GetCategories"/>
<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="dependentDaEntitlements"/>
<ffi:setProperty name="FromBack" value=""/>
