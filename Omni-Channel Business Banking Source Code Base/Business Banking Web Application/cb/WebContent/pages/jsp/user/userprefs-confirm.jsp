<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="userprefs-confirm" className="moduleHelpClass"/>
<%
	session.setAttribute("DISPLAY_SESSION_SUMMARY_REPORT", request.getParameter("DISPLAY_SESSION_SUMMARY_REPORT"));
	session.setAttribute("new_display_session_summary_value", request.getParameter("new_display_session_summary_value"));
	session.setAttribute("BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES", request.getParameter("BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES"));
	session.setAttribute("BankLookupMaximumMatches", request.getParameter("BankLookupMaximumMatches"));
	session.setAttribute("Timeout", request.getParameter("Timeout"));
	session.setAttribute("NewTimeout", request.getParameter("NewTimeout"));
%>

<%-- Update the user's session delivery settings. --%>
<ffi:cinclude value1="${DISPLAY_SESSION_SUMMARY_REPORT}" value2="${new_display_session_summary_value}" operator="notEquals">
	<ffi:setProperty name="DISPLAY_SESSION_SUMMARY_REPORT" value="${new_display_session_summary_value}"/>
	<ffi:object id="SetSessionSettings" name="com.ffusion.tasks.user.SetSessionSettings" scope="session"/>
	<ffi:process name="SetSessionSettings"/>
</ffi:cinclude>
<ffi:removeProperty name="SetSessionSettings"/>
<ffi:removeProperty name="new_display_session_summary_value"/>


<%-- Update the banklookup settings. --%>
<ffi:cinclude value1="${BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES}" value2="${BankLookupMaximumMatches}" operator="notEquals">
	<ffi:object id="SetBankLookupSettings" name="com.ffusion.tasks.user.SetBankLookupSettings" scope="session"/>
	<ffi:setProperty name="SetBankLookupSettings" property="OldValue" value="${BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES}"/>
	<ffi:setProperty name="BANK_LOOKUP_SETTINGS_MAXIMUM_MATCHES" value="${BankLookupMaximumMatches}"/>
	<ffi:process name="SetBankLookupSettings"/>
</ffi:cinclude>
<ffi:removeProperty name="SetBankLookupSettings"/>
<ffi:removeProperty name="BankLookupMaximumMatches"/>

<%-- Update the user's timeout settings. --%>
<ffi:object id="ModifyUser" name="com.ffusion.tasks.user.EditUser" scope="session"/>
<ffi:setProperty name="ModifyUser" property="Init" value="true" />
<ffi:process name="ModifyUser"/>
<ffi:setProperty name="NewTimeout" value="${Timeout}"/>
<ffi:setProperty name="ModifyUser" property="Timeout" value="${NewTimeout}"/>
<ffi:setProperty name="ModifyUser" property="Process" value="true" />
<ffi:process name="ModifyUser"/>
<ffi:removeProperty name="ModifyUser"/>
<ffi:removeProperty name="Timeout"/>

<s:set var="ActualStartPage" value="#session.StartPage" scope="session"></s:set>
