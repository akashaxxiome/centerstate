<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
	<style>
	 .ui-jqgrid tr.jqgrow td:first-child input{margin-left:15px;}
	 .ui-jqgrid-btable tr.jqgrow td input[type="text"]{width:90% !important;}
	</style>
	<ffi:help id="user_pref_accounts"/>
	<ffi:cinclude value1="${gridType}" value2="Internal" operator="equals">
		<ffi:setProperty name="tempGridURL" value="/pages/jsp/user/AccountsSettingGrid_getInternalAccounts.action" URLEncrypt="true"/>
		<ffi:setProperty name="gridID" value="internalAccountsGrid"/>
		<ffi:setProperty name="gridCompleteTopic" value="ns-userPreference-makeInternalAccGridRowsEditable"/>
		<%-- <s:text name="jsp.account.internalAccount"/> --%>
	</ffi:cinclude>
	<ffi:cinclude value1="${gridType}" value2="External" operator="equals">
		<ffi:setProperty name="tempGridURL" value="/pages/jsp/user/AccountsSettingGrid_getExternalAccounts.action" URLEncrypt="true"/>
		<ffi:setProperty name="gridID" value="externalAccountsGrid"/>
		<ffi:setProperty name="gridCompleteTopic" value="ns-userPreference-makeExternalAccGridRowsEditable"/>
		<%-- <s:text name="jsp.account.externalAccount"/> --%>
	</ffi:cinclude>
	<s:url id="gridUrl" value="%{#session.tempGridURL}" escapeAmp="false"/>
	<sjg:grid id="%{#session.gridID}"
		dataType="json"
		href="%{gridUrl}"
		sortable="false"
		pager="false"
		gridModel="gridModel"
		rowNum="-1"
		rownumbers="false"
		shrinkToFit="false"
		scroll="false"
		scrollrows="false"
		onGridCompleteTopics="%{#session.gridCompleteTopic}"
		>
		<sjg:gridColumn name="map.ActiveAcct" index="activAccts"
			title="%{getText('jsp.user.preferences.accountGrid.Header.ActiveCol')}"	width="40" align="center"
			sortable="false" edittype="checkbox" editable="true" editoptions="{value:'on:off'}" formatoptions="{disabled : 'false'}"/>
		<ffi:cinclude value1="${gridType}" value2="Internal" operator="equals">	
			<sjg:gridColumn name="map.OnlineStatAcct" index="onlineAccts"
				title="%{getText('jsp.user.preferences.accountGrid.Header.StatementCol')}"  width="65" align="center"
				sortable="false" edittype="checkbox" editable="true" editoptions="{value:'on:off'}" formatoptions="{disabled : 'false'}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${gridType}" value2="External" operator="equals">
			<sjg:gridColumn name="bankName" index="bankName" title="%{getText('jsp.user.preferences.accountGrid.Header.InstitutionCol')}"
				width="65" align="left"	sortable="false"/>
		</ffi:cinclude>
		<sjg:gridColumn name="numberMasked" index="NumberMasked"
			title="%{getText('jsp.user.preferences.accountGrid.Header.AccNoCol')}" sortable="false" width="85" align="left"/>
		<sjg:gridColumn name="nickName" index="nickName" title="%{getText('jsp.user.preferences.accountGrid.Header.NickNameCol')}"
			width="145" align="left" sortable="false" edittype="text" editable="true" editoptions="{maxlength: 40}"/>
		<sjg:gridColumn name="type" index="type" width="145" align="left" sortable="false"
			title="%{getText('jsp.user.preferences.accountGrid.Header.AccTypeCol')}" />
		<sjg:gridColumn name="map.CurrencyCode" index="CurrencyCode" title="%{getText('jsp.user.preferences.accountGrid.Header.CurrencyCode')}"
				width="65" align="left"	sortable="false"/>	
		<ffi:cinclude value1="${gridType}" value2="Internal" operator="equals">	
			<sjg:gridColumn name="map.displayCurrentBalance" index="CurrentBalance" width="150" align="right" sortable="false"
				title="%{getText('jsp.user.preferences.accountGrid.Header.CurrBalCol')}" formatter ="ns.userPreference.formatCurrentBalance"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${gridType}" value2="External" operator="equals">
			<sjg:gridColumn name="map.displayCurrentBalance" index="CurrentBalance" width="150" align="right" sortable="false"
				title="%{getText('jsp.user.preferences.accountGrid.Header.CurrBalCol')}" formatter ="ns.userPreference.formatCurrentBalance"/>
		</ffi:cinclude>		
	</sjg:grid>
	
	<script type="text/javascript">
		// add Grid control id to maintain it on the page
		//ns.common.addGridControls('#<ffi:getProperty name="gridID"/>', false);
		
		// Add event handler so that no scroll bar will be shown on click of grid hide/unhide button.
		// This is done to fix 
		// CR #705953 vertical and horizontal scroll bars are getting displayed for Internal and External accounts section.
		$('#<ffi:getProperty name="gridID"/>')
			.parents('.ui-jqgrid')
				.find('.ui-jqgrid-titlebar-close').click(function(){
					var jqGrid = $('#<ffi:getProperty name="gridID"/>').parents('.ui-jqgrid')[0];
					if($(this).children('.ui-icon').hasClass('ui-icon-circle-triangle-s')){
						jqGrid.style.overflow = 'hidden';
					} else {
						jqGrid.style.overflow = '';
					}
				});
		
		
	</script>
	<ffi:removeProperty name="gridID"/>
	<ffi:removeProperty name="tempGridURL"/>
	<ffi:removeProperty name="gridCompleteTopic"/>