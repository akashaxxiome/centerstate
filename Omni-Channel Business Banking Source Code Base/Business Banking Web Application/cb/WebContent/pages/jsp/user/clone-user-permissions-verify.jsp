<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees"%>

<ffi:help id="user_clone-user-permissions-verify" className="moduleHelpClass"/>
<% session.setAttribute("SourceBusinessEmployeeId", request.getParameter("SourceBusinessEmployeeId")); %>

<ffi:setProperty name='PageHeading' value='Verify Clone User Permissions'/>

<ffi:setProperty name="BackURL" value="${SecurePath}user/clone-user-permissions.jsp" URLEncrypt="true"/>

<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" scope="request"/>
<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${SourceBusinessEmployeeId}"/>
<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeeSessionName" value="SourceBusinessEmployee"/>
<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeesSessionName" value="BusinessEmployeesForClone"/>
<ffi:setProperty name="SetBusinessEmployee" property="TaskErrorURL" value=""/>
<ffi:process name="SetBusinessEmployee"/>

<ffi:cinclude value1="${SourceBusinessEmployee}" value2="" operator="equals">
	<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${SourceBusinessEmployeeId}"/>
	<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeeSessionName" value="SourceBusinessEmployee"/>
	<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeesSessionName" value="EntitlementProfilesForClone"/>
	<ffi:setProperty name="SetBusinessEmployee" property="TaskErrorURL" value="<%=com.ffusion.tasks.BaseTask.TASK_ERROR%>"/>
	<ffi:process name="SetBusinessEmployee"/>
</ffi:cinclude>

<ffi:setProperty name="ClonePermissions" property="OnlyValidate" value="true"/>
<ffi:process name="ClonePermissions"/>

<div align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<div align="center">
					<span class="sectionsubhead">
						<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminusers.jsp-1" parm0="${TargetBusinessEmployee.FullName}"/>
						(<s:text name="jsp.default_576"/>)
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td>&nbsp;
			</td>
		</tr>
	</table>
	<s:form id="SourceUserSelectVerify" namespace="/pages/user" action="clonePermissions-execute" theme="simple" validate="false" method="post" name="SourceUserSelectVerify">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="adminBackground">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<%-- <tr>
						<td class="adminBackground" align="left">
							<span class="columndata">
							<s:text name="jsp.user_260"/>
							</span>
						</td>
					</tr> --%>
					<tr>
						<td class="adminBackground" align="left">
						&nbsp;
						</td>
					</tr>
					<tr>
						<td class="adminBackground" align="center">
							
							<span class="columndata">
							<s:text name="jsp.user_67"/>
							</span>
							<span class="sectionsubhead">
							<ffi:cinclude value1="${SourceBusinessEmployee.CustomerType}" value2="4" operator="notEquals">
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminusers.jsp-1" parm0="${SourceBusinessEmployee.FullName}"/>
								(<s:text name="jsp.default_576"/>)
							</ffi:cinclude>
							<ffi:cinclude value1="${SourceBusinessEmployee.CustomerType}" value2="4" operator="equals">
								<ffi:getProperty name="SourceBusinessEmployee" property="UserName"/>
								(<s:text name="jsp.user_150"/>)
							</ffi:cinclude>
							</span>
							<span class="columndata">
							<s:text name="jsp.user_339"/>
							</span>
							<span class="sectionsubhead">
							<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminusers.jsp-1" parm0="${TargetBusinessEmployee.FullName}"/>(<s:text name="jsp.default_576"/>)
							</span>
						</td>
					</tr>
					<tr><td class="adminBackground" >&nbsp;</td></tr>
					<tr>
						<td class="adminBackground" align="center">
							<sj:a
							    id="backUserPermission"
								button="true" 
								onClickTopics="backToInput"
								><s:text name="jsp.default_57"/></sj:a>
							<ffi:setProperty name="targetURL" value="${SecurePath}user/wait.jsp?target=${PathExt}user/clone-user-permissions-clone.jsp" URLEncrypt="true"/>
							<sj:a
							    id="verifyCloneUserPermission"
								formIds="SourceUserSelectVerify"
								targets="confirmDiv"
								button="true" 
								validate="false"
								onBeforeTopics="beforeSubmit" 
								onErrorTopics="errorSubmit"
								onCompleteTopics="completeSubmit"
								><s:text name="jsp.user_66"/></sj:a>
							<ffi:removeProperty name="targetURL"/>
						</td>
					</tr>
					<tr><td class="adminBackground">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</s:form>
</div>
