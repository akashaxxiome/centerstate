<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.util.DateFormatUtil" %>

<div id="Wire_Templates">
<div id="wiretemplatesaccessDiv" class="wiretemplatesaccessHelpCSS">
<ffi:help id="user_wiretemplateaccess" className="moduleHelpClass"/>
<%
	request.setAttribute("runSearch", request.getParameter("runSearch"));
	request.setAttribute("SearchWireName", request.getParameter("SearchWireName"));
	request.setAttribute("SearchWireType", request.getParameter("SearchWireType"));
	request.setAttribute("SearchWireCategory", request.getParameter("SearchWireCategory"));
	request.setAttribute("FromBack", request.getParameter("FromBack"));
	request.setAttribute("UseLastRequest", request.getParameter("UseLastRequest"));
%>

<ffi:removeProperty name="PageHeading2"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
	<ffi:removeProperty name="WireTemplatesList"/>
</ffi:cinclude>

<ffi:setProperty name="SavePermissionsWizard" value="${PermissionsWizard}"/>


<%-- Initialize Request --%>
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>
<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<%-- Get the BusinessEmployee  --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<%-- SetBusinessEmployee.Id should be passed in the request --%>
	<ffi:process name="SetBusinessEmployee"/>
</s:if>

<%-- get the entitlementgroup for this new user back to session --%>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='${EditGroup_GroupId}'/>
<ffi:process name="GetEntitlementGroup"/>

<%-- Dual Approval Processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
		<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
		<s:if test="%{#session.Section == 'Users'}">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</s:if>
		<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
			<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
			<ffi:setProperty name="GetCategories" property="itemId" value="${EditGroup_GroupId}" />
		</ffi:cinclude>
	<ffi:process name="GetCategories"/>
	<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" value="<%=IDualApprovalConstants.CATEGORY_SUB_WIRE_TEMPLATE_ACCESS_LIMITS %>"/>

	<s:if test="%{#session.Section == 'Users'}">
		<ffi:removeProperty name="CROSS_ACC_CAT_SUBTYPE" />
		<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
			<ffi:setProperty name="GetDACategorySubType" property="operationName" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE %>"/>
			<ffi:setProperty name="GetDACategorySubType" property="perCategory" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT%>" />
			<ffi:setProperty name="GetDACategorySubType" property="categorySubType" value="CROSS_ACC_CAT_SUBTYPE"/>
		<ffi:process name="GetDACategorySubType"/>

		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${CROSS_ACC_CAT_SUBTYPE}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoriesSessionName" value="dependentDaEntitlements"/>
		<ffi:process name="GetDACategoryDetails"/>
		<ffi:removeProperty name="CROSS_ACC_CAT_SUBTYPE" />
		<ffi:removeProperty name="GetDACategorySubType" />
	</s:if>
	<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
		<ffi:removeProperty name="CROSS_ACC_CAT_SUBTYPE" />
		<ffi:object id="GetDACategorySubType" name="com.ffusion.tasks.dualapproval.GetDACategorySubType" />
			<ffi:setProperty name="GetDACategorySubType" property="operationName" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE %>"/>
			<ffi:setProperty name="GetDACategorySubType" property="perCategory" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_CATEGORY_CROSS_ACCOUNT%>" />
			<ffi:setProperty name="GetDACategorySubType" property="categorySubType" value="CROSS_ACC_CAT_SUBTYPE"/>
		<ffi:process name="GetDACategorySubType"/>

		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${CROSS_ACC_CAT_SUBTYPE}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoriesSessionName" value="dependentDaEntitlements"/>
		<ffi:process name="GetDACategoryDetails"/>
		<ffi:removeProperty name="CROSS_ACC_CAT_SUBTYPE" />
		<ffi:removeProperty name="GetDACategorySubType" />
	</ffi:cinclude>
	
</ffi:cinclude>



<ffi:cinclude value1="${templateStartDate}" value2="" operator="equals">
	<ffi:setProperty name="templateStartDate" value="01/01/2000" />
</ffi:cinclude>
<ffi:cinclude value1="${templateEndDate}" value2="" operator="equals">
	<ffi:setProperty name="templateEndDate" value="01/01/2050" />
</ffi:cinclude>
<%-- Get all wire templates - should not matter whether group/user/division --%>
<ffi:object name="com.ffusion.tasks.wiretransfers.GetAllWireTemplates" id="GetAllWireTemplates" scope="session"/>
	<ffi:setProperty name="GetAllWireTemplates" property="businessID" value="${Business.EntitlementGroupId}"/>
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:setProperty name="GetAllWireTemplates" property="GroupID" value="${BusinessEmployee.EntitlementGroupId}"/>
		<ffi:setProperty name="GetAllWireTemplates" property="UserType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="GetAllWireTemplates" property="MemberID" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
		<ffi:setProperty name="GetAllWireTemplates" property="DateFormat" value="<%= DateFormatUtil.DATE_FORMAT_MMDDYYYY %>" />
		<ffi:setProperty name="GetAllWireTemplates" property="StartDate" value="${templateStartDate}" />
		<ffi:setProperty name="GetAllWireTemplates" property="EndDate" value="${templateEndDate}" />
	</s:if>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:setProperty name="GetAllWireTemplates" property="GroupID" value="${Entitlement_EntitlementGroup.ParentId}"/>
		<ffi:setProperty name="GetAllWireTemplates" property="DateFormat" value="<%= DateFormatUtil.DATE_FORMAT_MMDDYYYY %>" />
		<ffi:setProperty name="GetAllWireTemplates" property="StartDate" value="${templateStartDate}" />
		<ffi:setProperty name="GetAllWireTemplates" property="EndDate" value="${templateEndDate}" />
	</s:if>
<ffi:process name="GetAllWireTemplates"/>
<ffi:removeProperty name="GetAllWireTemplates"/>

<ffi:object name="com.ffusion.tasks.wiretransfers.SetWireTransfer" id = "SetWireTransfer" scope="session"/>
	<ffi:setProperty name="SetWireTransfer" property="collectionSessionName" value="AllWireTemplates"/>
	<ffi:setProperty name="SetWireTransfer" property="beanSessionName" value="WireTemplate"/>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>
<ffi:cinclude value1="${runSearch}" value2="">

	<ffi:getProperty name="Entitlement_Entitlements" property="Size"/>

	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.dualapproval.EditWireTemplatePermissionsDA" scope="session"/>
			<ffi:setProperty name="EditWireTemplatePermissions" property="approveAction" value="false" />
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.admin.EditWireTemplatePermissions" scope="session"/>
		</ffi:cinclude>
	</s:if>		
	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="1" operator="equals">
				<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.dualapproval.EditWireTemplatePermissionsDA" scope="session"/>
				<ffi:setProperty name="EditWireTemplatePermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="3" operator="equals">
				<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.admin.EditWireTemplatePermissions" scope="session"/>
			</ffi:cinclude>
			<!--  4 for normal profile, 5 for primary profile, 6t for sec profile, 7 for cross profile -->
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="4" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.dualapproval.EditWireTemplatePermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.admin.EditWireTemplatePermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditWireTemplatePermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="7" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.dualapproval.EditWireTemplatePermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.admin.EditWireTemplatePermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditWireTemplatePermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="5" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.dualapproval.EditWireTemplatePermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.admin.EditWireTemplatePermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditWireTemplatePermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${BusinessEmployee.CustomerType}" value2="6" operator="equals">
				<s:if test="#session.BusinessEmployee.UsingEntProfiles">
					<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.dualapproval.EditWireTemplatePermissionsDA" scope="session"/>
				</s:if>
				<s:else>
					<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.admin.EditWireTemplatePermissions" scope="session"/>
				</s:else>
				<ffi:setProperty name="EditWireTemplatePermissions" property="approveAction" value="false"/>
			</ffi:cinclude>
			
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:object id="EditWireTemplatePermissions" name="com.ffusion.tasks.admin.EditWireTemplatePermissions" scope="session"/>
		</ffi:cinclude>
		<ffi:setProperty name="EditWireTemplatePermissions" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
		<ffi:setProperty name="EditWireTemplatePermissions" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		<ffi:setProperty name="EditWireTemplatePermissions" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	</s:if>
		
		<ffi:setProperty name="EditWireTemplatePermissions" property="GroupId" value="${EditGroup_GroupId}"/>
		<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
			<ffi:setProperty name="EditWireTemplatePermissions" property="ProfileId" value="${Business.Id}"/>
		</ffi:cinclude>
		<ffi:setProperty name="EditWireTemplatePermissions" property="EntitlementTypesWithLimits" value="NonAccountEntitlementsWithLimits"/>
		<ffi:setProperty name="EditWireTemplatePermissions" property="EntitlementTypesWithoutLimits" value="NonAccountEntitlementsWithoutLimits"/>
		<ffi:setProperty name="EditWireTemplatePermissions" property="SaveLastRequest" value="false"/>

	<ffi:object id="GetRestrictedEntitlements" name="com.ffusion.efs.tasks.entitlements.GetRestrictedEntitlements" scope="session" />
		<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			<ffi:setProperty name="GetRestrictedEntitlements" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
		</s:if>
		<ffi:setProperty name="GetRestrictedEntitlements" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:process name="GetRestrictedEntitlements"/>
	<ffi:removeProperty name="GetRestrictedEntitlements"/>

	<ffi:object id="GetTypesForEditingLimits" name="com.ffusion.tasks.admin.GetTypesForEditingLimits" scope="session"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="ComponentValue" value="Payments & Transfers"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="WireTemplateWithLimitsName" value="NonAccountEntitlementsWithLimits"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="WireTemplateWithoutLimitsName" value="NonAccountEntitlementsWithoutLimits"/>
		<ffi:setProperty name="GetTypesForEditingLimits" property="WireTemplateMerged" value="NonAccountEntitlementsMerged"/>

	<ffi:process name="GetTypesForEditingLimits"/>
	<ffi:removeProperty name="GetTypesForEditingLimits"/>

	<ffi:setProperty name="AllWireTemplates" property="Filter" value="All"/>
</ffi:cinclude>


<ffi:cinclude value1="${runSearch}" value2="true" operator="equals">
	<ffi:cinclude value1="${SearchWireName}" value2="" operator="equals">
		<ffi:cinclude value1="${SearchWireType}" value2="" operator="equals">
			<ffi:cinclude value1="${SearchWireCategory}" value2="" operator="equals">
				<ffi:setProperty name="AllWireTemplates" property="Filter" value="All"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${SearchWireCategory}" value2="" operator="notEquals">
				<ffi:setProperty name="AllWireTemplates" property="Filter" value="WIRE_CATEGORY=${SearchWireCategory}"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${SearchWireType}" value2="" operator="notEquals">
			<ffi:cinclude value1="${SearchWireCategory}" value2="" operator="equals">
				<ffi:setProperty name="AllWireTemplates" property="Filter" value="WIRE_DESTINATION=${SearchWireType}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${SearchWireCategory}" value2="" operator="notEquals">
				<ffi:setProperty name="AllWireTemplates" property="Filter" value="WIRE_DESTINATION=${SearchWireType},AND,WIRE_CATEGORY=${SearchWireCategory}"/>
			</ffi:cinclude>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${SearchWireName}" value2="" operator="notEquals">
		<ffi:cinclude value1="${SearchWireType}" value2="" operator="equals">
			<ffi:cinclude value1="${SearchWireCategory}" value2="" operator="equals">
				<ffi:setProperty name="AllWireTemplates" property="Filter" value="WIRE_NAME=${SearchWireName}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${SearchWireCategory}" value2="" operator="notEquals">
				<ffi:setProperty name="AllWireTemplates" property="Filter" value="WIRE_NAME=${SearchWireName},AND,WIRE_CATEGORY=${SearchWireCategory}"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${SearchWireType}" value2="" operator="notEquals">
			<ffi:cinclude value1="${SearchWireCategory}" value2="" operator="equals">
				<ffi:setProperty name="AllWireTemplates" property="Filter" value="WIRE_NAME=${SearchWireName},AND,WIRE_DESTINATION=${SearchWireType}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${SearchWireCategory}" value2="" operator="notEquals">
				<ffi:setProperty name="AllWireTemplates" property="Filter" value="WIRE_NAME=${SearchWireName},AND,WIRE_DESTINATION=${SearchWireType},AND,WIRE_CATEGORY=${SearchWireCategory}"/>
			</ffi:cinclude>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="CheckOperationEntitlement" name="com.ffusion.tasks.admin.CheckOperationEntitlement"/>
<%-- set variables used to interchange row colors --%>

<%-- Figure out display settings for the account search tables --%>
<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount"/>
<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%= com.ffusion.tasks.GetDisplayCount.WIRETEMPLATE %>"/>
<ffi:setProperty name="GetDisplayCount" property="DisplayCountName" value="WireTemplateDisplaySize"/>
<ffi:process name="GetDisplayCount"/>
<ffi:removeProperty name="GetDisplayCount"/>
<ffi:setProperty name="numAccounts" value="${AllWireTemplates.Size}"/>
<%
	int WireTemplateDisplaySize = Integer.parseInt( (String)session.getAttribute("WireTemplateDisplaySize") );
	int numAccounts = Integer.parseInt( (String)session.getAttribute("numAccounts") );
	session.setAttribute( "showSearch", "FALSE" );
	if (numAccounts > WireTemplateDisplaySize) {
		session.setAttribute( "showSearch", "TRUE" );
	}
	WireTemplateDisplaySize = (numAccounts < WireTemplateDisplaySize)? numAccounts : WireTemplateDisplaySize;
	session.setAttribute( "WireTemplateDisplaySize", String.valueOf( WireTemplateDisplaySize) );
%>

<ffi:object id="ApprovalAdminByBusiness" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
<ffi:setProperty name="ApprovalAdminByBusiness" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="ApprovalAdminByBusiness" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.APPROVALS_ADMIN %>"/>
<ffi:process name="ApprovalAdminByBusiness"/>

<% session.setAttribute("FFIEditWireTemplatePermissions", session.getAttribute("EditWireTemplatePermissions")); %>
<% session.setAttribute("FFISetWireTransfer", session.getAttribute("SetWireTransfer")); %>

<script type="text/javascript"><!--

	$(document).ready(function(){
		
		$("#editGroupWireTemplateID").extmultiselect().multiselectfilter();
		initializeParentChild();
	});

	$(function(){
		$("#SearchWireType").selectmenu({width: 120});
		$("#SearchWireCategory").selectmenu({width: 120});
	});

	/* sets the checkboxes on a form to checked or unchecked.
	 * form - the form to operate on
	 * value - to set the checkbox.checked property
	 * exp - set only checkboxes matching exp to value
	 */
	function setCheckboxes( form, value, exp1, exp2 ) {
		for( i=0; i < form.elements.length; i++ ){
			if( form.elements[i].type == "checkbox" &&
				(form.elements[i].name.indexOf( exp1 ) != -1 || form.elements[i].name.indexOf( exp2 ) != -1)) {
					form.elements[i].checked = value;
			}
		}
	}


	<%
		session.setAttribute( "NumLimitsPerTemplate", String.valueOf( ((ArrayList)session.getAttribute("NonAccountEntitlementsMerged")).size() ) );
		session.setAttribute( "ChildParentArraySize", String.valueOf( ((ArrayList)session.getAttribute("NonAccountEntitlementsMerged")).size() * Integer.parseInt( (String)session.getAttribute("WireTemplateDisplaySize") ) ) );
	%>
	function initializeParentChild() {
		parentChildArray = new Array(<ffi:getProperty name="WireTemplateDisplaySize"/>);
		childParentArray = new Array(<ffi:getProperty name="ChildParentArraySize"/>);

		prefixes = new Array( "entitlement", "WireTemplateNumber" );

		// Populate the parentChildArray
<ffi:object id="templateMath" name="com.ffusion.beans.util.IntegerMath"/>
<ffi:setProperty name="templateMath" property="Value1" value="0"/>
<ffi:setProperty name="templateMath" property="Value2" value="1"/>

<ffi:object id="limitMath" name="com.ffusion.beans.util.IntegerMath"/>
<ffi:setProperty name="limitMath" property="Value1" value="0"/>
<ffi:setProperty name="limitMath" property="Value2" value="1"/>

<ffi:list collection="AllWireTemplates" items="wireTemplate">
	<ffi:setProperty name="limitMath" property="Value1" value="0"/>
		parentChildArray[<ffi:getProperty name="templateMath" property="Value1"/>] = new Array( "WireTemplateNumber<ffi:getProperty name="templateMath" property="Value1"/>"
	<ffi:list collection="NonAccountEntitlementsMerged" items="ent">
			, "entitlement<ffi:getProperty name="limitMath" property="Value1"/>_<ffi:getProperty name="templateMath" property="Value1"/>"

		<ffi:setProperty name="limitMath" property="Value1" value="${limitMath.Add}" />
	</ffi:list>
		);

	<ffi:setProperty name="templateMath" property="Value1" value="${templateMath.Add}" />
</ffi:list>

		// Populate the childParentArray
<ffi:setProperty name="templateMath" property="Value1" value="0"/>
<ffi:setProperty name="limitMath" property="Value1" value="0"/>

<ffi:object id="childIndex" name="com.ffusion.beans.util.IntegerMath"/>
<ffi:setProperty name="childIndex" property="Value1" value="0"/>
<ffi:setProperty name="childIndex" property="Value2" value="1"/>

<ffi:list collection="NonAccountEntitlementsMerged" items="ent">
	<ffi:setProperty name="templateMath" property="Value1" value="0"/>
	<ffi:list collection="AllWireTemplates" items="wireTemplate">
		childParentArray[<ffi:getProperty name="childIndex" property="Value1"/>] = new Array( "entitlement<ffi:getProperty name="limitMath" property="Value1"/>_<ffi:getProperty name="templateMath" property="Value1"/>", "WireTemplateNumber<ffi:getProperty name="templateMath" property="Value1"/>" );

		<ffi:setProperty name="childIndex" property="Value1" value="${childIndex.Add}" />
		<ffi:setProperty name="templateMath" property="Value1" value="${templateMath.Add}" />
	</ffi:list>

	<ffi:setProperty name="limitMath" property="Value1" value="${limitMath.Add}" />
</ffi:list>
	}
	
	ns.admin.resetWireTemplateAccessForm = function()
	{
		$("#wireTemplateTab").click();
	}
// --></script>


<s:include value="inc/parentchild_js.jsp"/>
<%-- <span><s:text name="jsp.user_414"/></span> --%>
<ul id="formerrors"></ul>
<div align="center" class="marginBottom20"><ffi:getProperty name="Context"/></div>
<div align="center">
			
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<!-- Inner Tables -->
			<tr>
				<%-- <ffi:cinclude value1="${showSearch}" value2="TRUE">
					<td class="adminBackground" valign="top" align="center">
						<table height="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="400" align="left" class="adminBackground">
									<s:form id="wireSearchForm" namespace="/pages/user" action="wiretemplatesaccess" theme="simple" method="post">
									<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<table width="100%" border="0">
											<input type="hidden" name="runSearch" value="true">
											<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">
											<tr>
												<td colspan="2" class="sectionsubhead" height="20">&nbsp;&nbsp;&gt; <s:text name="jsp.default_548"/></td>
											</tr>
											<tr>
												<td colspan="2" class="columndata">&nbsp;
													<s:text name="jsp.user_343"/>
												</td>
											</tr>
											<tr>
												<td class="sectionsubhead" height="20" width="50%" align="right"><s:text name="jsp.default_283"/></td>
												<td class="sectionsubhead"><input class="ui-widget-content ui-corner-all" type="text" name="SearchWireName" size="17" maxlength="40" class="txtbox"></td>
											</tr>
											<tr>
												<td class="sectionsubhead" align="right"><s:text name="jsp.default_581"/></td>
													<td class="sectionsubhead"><select id="SearchWireType" name="SearchWireType" class="txtbox">
															<option value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_DOMESTIC %>"><s:text name="jsp.default_560"/></option>
															<option value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_INTERNATIONAL %>"><s:text name="jsp.default_541"/></option>
															<option value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_HOST %>"><s:text name="jsp.default_229"/></option>
															<option value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_DRAWDOWN %>"><s:text name="jsp.default_176"/></option>
															<option value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_FED %>"><s:text name="jsp.user_158"/></option>
															<option value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_BOOK %>"><s:text name="jsp.default_75"/></option>
													</select>
												</td>
											</tr>
											<tr>
												<td class="sectionsubhead" align="right"><s:text name="jsp.user_388"/></td>
													<td class="sectionsubhead"><select id="SearchWireCategory" name="SearchWireCategory" class="txtbox">
															<option value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_CATEGORY_FREEFORM %>"><s:text name="jsp.default_561"/></option>
															<option value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_CATEGORY_REPETITIVE %>"><s:text name="jsp.default_570"/></option>
															<option value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_CATEGORY_SEMIREPETITIVE %>"><s:text name="jsp.default_572"/></option>
													</select>
												</td>
											</tr>
											<tr>
												<td class="sectionsubhead" colspan="2" height="20"  align="center">
													<sj:a
														formIds="wireSearchForm"
														targets="wiretemplatesaccessDiv"
														button="true"
														validate="false"
														><s:text name="jsp.default_373"/></sj:a>
												</td>
											</tr>
									</table>
									</s:form>
								</td>
							</tr>
						</table>
					</td>
					<td align="right">&nbsp;</td>
					<td>&nbsp;</td>
				</ffi:cinclude> --%>
				<td class="adminBackground" valign="top" align="center">
					<table border="0" cellspacing="5" cellpadding="0">
						<tr>
							<ffi:cinclude value1="${AllWireTemplates.size}" value2="0" operator="equals">
								<td class="adminBackground sectionsubhead" align="center">
									<ffi:cinclude value1="${runSearch}" value2="" operator="equals">
										<s:text name="jsp.user_329"/><br><br>
										<sj:a button="true" onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
									</ffi:cinclude>
									<ffi:cinclude value1="${runSearch}" value2="false" operator="equals">
										<s:text name="jsp.user_329"/><br><br>
										<sj:a button="true" onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
									</ffi:cinclude>
									<ffi:cinclude value1="${runSearch}" value2="" operator="notEquals">
										&nbsp;<s:text name="jsp.user_227"/>
										<ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="notEquals">
												<sj:a button="true" onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.user_160"/></sj:a>
										</ffi:cinclude>

									</ffi:cinclude>
								</td>
							</ffi:cinclude>
							<ffi:cinclude value1="${AllWireTemplates.size}" value2="0" operator="notEquals">
								<td class="adminBackground">
									<s:form id="viewForm" namespace="/pages/user" action="wiretemplatesaccess" theme="simple" method="post">
									<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<table border="0">
										<input type="hidden" name="runSearch" value="false">
										<input type="hidden" name="Section" value="<ffi:getProperty name="Section"/>">
									<input type="hidden" name="EditGroup_GroupId" value="<ffi:getProperty name="EditGroup_GroupId"/>">
									<input type="hidden" name="SetBusinessEmployee.Id" value="<ffi:getProperty name="BusinessEmployee" property="Id"/>">
									<input type="hidden" name="EditGroup_GroupName" value="<ffi:getProperty name="EditGroup_GroupName"/>">
									<input type="hidden" name="EditGroup_Supervisor" value="<ffi:getProperty name="EditGroup_Supervisor"/>">
									<input type="hidden" name="EditGroup_DivisionName" value="<ffi:getProperty name="EditGroup_DivisionName"/>">
									<input type="hidden" name="EditGroup_Supervisor" value="<ffi:getProperty name="EditGroup_Supervisor"/>">
									<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">
										<tr>
											<td width="120" class="columndata" height="20" align="center"><%-- <s:text name="jsp.user_290"/> --%> 
												<s:text name="jsp.default_375" />
												<s:text name="jsp.default_5"/>:</td>
											<td align="center" class="sectionsubhead" width="380">
													<select id="editGroupWireTemplateID" name="EditGroup_WireTemplateID"  class="" size="<ffi:getProperty name="WireTemplateDisplaySize"/>" multiple="multiple" style="width:370px;">
													<ffi:list collection="AllWireTemplates" items="WireTemplateItem">
														<option value="<ffi:getProperty name="WireTemplateItem" property="ID"/>"><ffi:getProperty name="WireTemplateItem" property="WireName"/></option>
													</ffi:list>
												</select>
											</td>
											<td class="sectionsubhead" colspan="2" height="20" align="center">
												<sj:a
													formIds="viewForm"
													targets="wiretemplatesaccessDiv"
													button="true"
													validate="false"
													value="VIEW"
													onCompleteTopics="onCompleteTabifyAndFocusTopic"
													><s:text name="jsp.default_459"/></sj:a>
											</td>
										</tr>
								</table>
								</s:form>
							</td>
						</ffi:cinclude>
						</tr>
					</table>
				</td>
			</tr>
		</table>

	<!-- The Account limit Table -->

<ffi:cinclude value1="${AllWireTemplates.size}" value2="0" operator="notEquals">

			<ffi:object id="GetGroupLimits" name="com.ffusion.efs.tasks.entitlements.GetGroupLimits"/>
			<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
				<ffi:setProperty name="GetGroupLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
				<ffi:setProperty name="GetGroupLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
				<ffi:setProperty name="GetGroupLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
			</s:if>
			<ffi:setProperty name="GetGroupLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			<ffi:setProperty name="GetGroupLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATE %>"/>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center" class="adminBackground">
                        <div align="center">
						<s:form id="WireFormName" name="permLimitFrm" namespace="/pages/user" action="editWireTemplatePermissions-verify" validate="false" theme="simple" method="post">
						 <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                                    <input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">
                                    <tr>
                                        <td align="center" colspan="5">
                                        	<div class="paneWrapper">
  												<div class="paneInnerWrapper">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminBackground permissionTblWithBorder">
                                                <tr class="header">
                                                    <td valign="middle" align="center" nowrap class="sectionsubhead">
                                                        <%-- hide if read only --%>
														<input type="checkbox" value="ACTIVATE ALL" 
														<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
																disabled
														</ffi:cinclude>
														onclick="setCheckboxes( this.form, this.checked, 'entitlement', 'WireTemplate' );"> <s:text name="jsp.user_177"/>
                                                    </td>
                                                    <td valign="middle" nowrap align="left" class="sectionsubhead">
                                                        <s:text name="jsp.user_346"/>
                                                    </td>
                                                    <td colspan="2" valign="middle" align="center" nowrap class="sectionsubhead">
						    <ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
						    <ffi:process name="GetLimitBaseCurrency" />
                                                        <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                                    </td>
                                                    <td valign="middle" align="center" nowrap class="sectionsubhead">
																											<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                                        <s:text name="jsp.user_153"/>
																											</ffi:cinclude>
																											<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																												&nbsp;
																											</ffi:cinclude>
                                                    </td>
                                                    <td colspan="2" valign="middle" align="center" nowrap class="sectionsubhead">
                                                        <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                                    </td>
                                                    <td valign="middle" align="center" nowrap class="sectionsubhead">
																											<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                                        <s:text name="jsp.user_153"/>
																											</ffi:cinclude>
																											<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																												&nbsp;
																											</ffi:cinclude>
                                                    </td>
                                                </tr>
<%-- Flag so that the included pages that actually display the limits and entitlements
	 will include the javascript function that makes parent/child work --%>
<ffi:setProperty name="ParentChild" value="true"/>

<%

String[] arrayTemplate = request.getParameterValues( "EditGroup_WireTemplateID" );
ArrayList wireTemplatesList = null;
ArrayList allWireTemplates = null;

if (arrayTemplate == null ) {
	wireTemplatesList = new ArrayList();
	String wireTemplate=null;
%>
<ffi:list collection="AllWireTemplates" items="WireTemplateItem" startIndex="0" endIndex="0" >
<ffi:getProperty name="WireTemplateItem" property="ID" assignTo="wireTemplate"/>
</ffi:list>
<%
	wireTemplatesList.add(wireTemplate);
	}

if (arrayTemplate != null) {
	wireTemplatesList = new ArrayList();
	for (int i = 0; i < arrayTemplate.length; i++) {
		String wireTemplate = null;  %>
    <ffi:setProperty name="tempVar" value="<%= arrayTemplate[i] %>"/>
    <ffi:getProperty name="tempVar" assignTo="wireTemplate"/>
<%
		wireTemplatesList.add( wireTemplate );
	}
}
	if (wireTemplatesList != null) {
	session.setAttribute( "WireTemplatesList", wireTemplatesList );


%>
<ffi:object name="com.ffusion.tasks.admin.GetMaxWireTemplateLimits" id="GetMaxWireTemplateLimits" scope="session"/>
<ffi:setProperty name="GetMaxWireTemplateLimits" property="EntitlementTypePropertyListsName" value="NonAccountEntitlementsWithLimits"/>
<ffi:setProperty name="GetMaxWireTemplateLimits" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
    <ffi:setProperty name="GetMaxWireTemplateLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
    <ffi:setProperty name="GetMaxWireTemplateLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
    <ffi:setProperty name="GetMaxWireTemplateLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
</s:if>
<ffi:setProperty name="GetMaxWireTemplateLimits" property="PerTransactionMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="GetMaxWireTemplateLimits" property="PerDayMapName" value="PerDayLimits"/>
<ffi:setProperty name="GetMaxWireTemplateLimits" property="PerWeekMapName" value="PerWeekLimits"/>
<ffi:setProperty name="GetMaxWireTemplateLimits" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:setProperty name="GetMaxWireTemplateLimits" property="WireTemplateIDListName" value="WireTemplatesList"/>
<ffi:process name="GetMaxWireTemplateLimits"/>

<ffi:object id="GetMaxLimitForPeriod" name="com.ffusion.tasks.admin.GetMaxLimitForPeriod" scope="session" />
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerTransactionMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerDayMapName" value="PerDayLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerWeekMapName" value="PerWeekLimits"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="PerMonthMapName" value="PerMonthLimits"/>
<ffi:process name="GetMaxLimitForPeriod"/>
<ffi:setProperty name="GetMaxLimitForPeriod" property="NoLimitString" value="no"/>

<ffi:object id="CheckForRedundantLimits" name="com.ffusion.efs.tasks.entitlements.CheckForRedundantLimits" scope="session" />
<ffi:setProperty name="CheckForRedundantLimits" property="LimitListName" value="NonAccountEntitlementsWithLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerTransactionLimitMapName" value="PerTransactionLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerDayLimitMapName" value="PerDayLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerWeekLimitMapName" value="PerWeekLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MaxPerMonthLimitMapName" value="PerMonthLimits"/>
<ffi:setProperty name="CheckForRedundantLimits" property="MergedListName" value="NonAccountEntitlementsMerged"/>
<ffi:setProperty name="CheckForRedundantLimits" property="WireTemplatesListName" value="WireTemplatesList"/>
<ffi:setProperty name="CheckForRedundantLimits" property="SuccessURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/wiretemplatesaccess-verify-init.jsp?PermissionsWizard=${SavePermissionsWizard}" URLEncrypt="TRUE"/>
<ffi:setProperty name="CheckForRedundantLimits" property="BadLimitURL" value="${SecurePath}redirect.jsp?target=${SecurePath}user/wiretemplatesaccess.jsp&UseLastRequest=TRUE&DisplayErrors=TRUE&PermissionsWizard=${SavePermissionsWizard}" URLEncrypt="TRUE"/>
<ffi:setProperty name="CheckForRedundantLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATE %>"/>
<ffi:setProperty name="CheckForRedundantLimits" property="GroupId" value="${EditGroup_GroupId}"/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
	<ffi:setProperty name="CheckForRedundantLimits" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
</s:if>





<%
	for (int i=0; i < wireTemplatesList.size(); i++) {
		String thisTemplate = (String) wireTemplatesList.get(i);
		session.setAttribute( "TemplateIndex", new Integer(i) );
		String wireTemplateNumber = "WireTemplateNumber" + i;
%>
<ffi:removeProperty name="<%= wireTemplateNumber %>"/>
<ffi:setProperty name="SetWireTransfer" property="ID" value="<%= thisTemplate %>"/>
<ffi:process name="SetWireTransfer"/>
<%-- set the object ID for each wireTemplate of the list --%>
<ffi:setProperty name="GetGroupLimits" property="ObjectId" value="${WireTemplate.TemplateID}"/>

<%-- check for each wire template entitlement --%>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:object id="CheckEntitlementByMember" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMember" scope="session" />
	    <ffi:setProperty name="CheckEntitlementByMember" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
	    <ffi:setProperty name="CheckEntitlementByMember" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>

		<ffi:setProperty name="CheckEntitlementByMember" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATE %>"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="ObjectId" value="${WireTemplate.TemplateID}"/>
        <ffi:process name="CheckEntitlementByMember"/>
	<ffi:removeProperty name="CheckEntitlementByMember"/>
</s:if>
   <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
			<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_WIRE_TEMPLATE_ACCESS_LIMITS %>" />
			<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATE%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${WireTemplate.TemplateID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<s:if test="%{#session.Section == 'Users'}">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			</s:if>
			<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${BusinessEmployee.EntitlementGroupMember.Id}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${SecureUser.BusinessID}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
				<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_GROUP %>"/>
				<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${EditGroup_GroupId}" />
			</ffi:cinclude>
			<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"/>
		<ffi:process name="GetDACategoryDetails" />

		<ffi:setProperty name="GetDACategoryDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>"/>
		<ffi:process name="GetDACategoryDetails" />
	</ffi:cinclude>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:setProperty name="CheckEntitlementByGroup" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>

	<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATE %>"/>
	<ffi:setProperty name="CheckEntitlementByGroup" property="ObjectId" value="${WireTemplate.TemplateID}"/>
    <ffi:process name="CheckEntitlementByGroup"/>
</s:if>
												<tr>
												<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
													<td class="tbrd_t" valign="middle" align="center" >
														<ffi:cinclude value1="TRUE" value2="${CheckEntitlement}" operator="equals">
										    	            <input type="checkbox" name="WireTemplateNumber<%= i %>" value="<ffi:getProperty name="WireTemplate" property="ID"/>" checked border="0" onClick="handleClick( permLimitFrm, this )" >
										    	         </ffi:cinclude>
														<ffi:cinclude value1="TRUE" value2="${CheckEntitlement}" operator="notEquals">
										    	             <input type="checkbox" name="WireTemplateNumber<%= i %>" value="<ffi:getProperty name="WireTemplate" property="ID"/>" border="0" onClick="handleClick( permLimitFrm, this )">
										    	         </ffi:cinclude>
										    	    </td>
										    	    <td class="tbrd_t" valign="middle" colspan="7" style="text-align: left;">
										                <span class="sectionsubhead">
											                <ffi:getProperty name="WireTemplate" property="WireName"/>
														</span>
													</td>
												</ffi:cinclude>
												<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
												    <ffi:getPendingStyle fieldname="" defaultcss="" dacss=""
													sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />
												    <td class="tbrd_t" valign="middle" align="center">
														<ffi:cinclude value1="TRUE" value2="${CheckEntitlement}" operator="equals">
															<%-- hide if read only --%>
										    	            <input type="checkbox" name="WireTemplateNumber<%= i %>" value="<ffi:getProperty name="WireTemplate" property="ID"/>" checked border="0" 
										    	            <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
																disabled
															</ffi:cinclude>
										    	            onClick="handleClick( permLimitFrm, this )" >
										    	         </ffi:cinclude>
														<ffi:cinclude value1="TRUE" value2="${CheckEntitlement}" operator="notEquals">
										    	             <%-- hide if read only --%>
										    	             <input type="checkbox" name="WireTemplateNumber<%= i %>" value="<ffi:getProperty name="WireTemplate" property="ID"/>" border="0" 
										    	             <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
																disabled
															</ffi:cinclude>
										    	             onClick="handleClick( permLimitFrm, this )">
										    	         </ffi:cinclude>
										    	    </td>
										    	    <td class="tbrd_t" valign="middle" colspan="7">
														<span class="'<ffi:getPendingStyle fieldname="" defaultcss=" sectionsubhead " dacss=" sectionsubheadDA "
													sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'">
											                <ffi:getProperty name="WireTemplate" property="WireName"/>
											            </span>
													</td>
										    	</ffi:cinclude>
												</tr>
<%
   	session.setAttribute( "limitIndex", new Integer( 0 ) );

	// Increment the indent level of the per wire template entitlements by 1, so that they will appear indented under the wire template name.
	com.ffusion.csil.beans.entitlements.EntitlementTypePropertyLists lists = (com.ffusion.csil.beans.entitlements.EntitlementTypePropertyLists)session.getAttribute( "NonAccountEntitlementsMerged" );
	java.util.Iterator iter = lists.iterator();
	while( iter.hasNext() ) {
		com.ffusion.csil.beans.entitlements.EntitlementTypePropertyList curList = (com.ffusion.csil.beans.entitlements.EntitlementTypePropertyList)iter.next();

		if( curList.isPropertySet( com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL ) ) {
			int curLevel = Integer.parseInt( curList.getPropertyValue( com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL, 0 ) );
			curLevel++;
			curList.addProperty( com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL, String.valueOf( curLevel ) );
		} else {
			curList.addProperty( com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_INDENT_LEVEL, "1" );
		}
	}
	session.setAttribute( "LimitsList", lists );
%>
<ffi:setProperty name="DisplayedLimit" value="FALSE"/>
<ffi:setProperty name="CheckEntitlementObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATE %>"/>
<ffi:setProperty name="CheckEntitlementObjectId" value="${WireTemplate.TemplateID}"/>
<ffi:setProperty name="WireID" value="<%= (String) wireTemplatesList.get(i) %>"/>
<ffi:flush/>

<ffi:cinclude value1="${AllWireTemplates.size}" value2="0" operator="notEquals">

	<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">

		<s:include value="inc/corpadminuserlimit_merged.jsp"/>
	</s:if>
	<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
		<s:include value="inc/corpadmingrouplimit_merged.jsp"/>
	</s:if>

</ffi:cinclude>

		<ffi:cinclude value1="${DisplayedLimit}" value2="FALSE" operator="equals">
			                                                <tr>
			                                                    <td colspan="8" valign="middle" align="center" style="padding-top:8px;">
			                                                        <span class="columndata"><s:text name="jsp.user_214"/></span>
			                                                    </td>
			                                                </tr>
		</ffi:cinclude>
<%
	}
}
%>
											</table></div></div>
										</td>
									</tr>

									<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
									<tr>
										<td colspan="5" align="center" class="adminBackground sectionsubhead">
											<sj:a  id="wireTampletePermissionResetButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon' 
												title='<s:text name="jsp.default_358.1" />' 
												href='#' 
												onClick="ns.admin.resetWireTemplateAccessForm(); removeValidationErrors();"
												button="true">
												<s:text name="jsp.default_358"/>
											</sj:a>
											<sj:a
												href='#' 
												 button="true"
												 onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
												 ><s:text name="jsp.default_102"/></sj:a>
											<sj:a
												href='#' 
												formIds="WireFormName"
												targets="wiretemplatesaccessDiv"
												button="true"
												validate="true"
												validateFunction="customValidation"
												onBeforeTopics="beforeVerify"
												onCompleteTopics="onCompleteTabifyAndFocusTopic"
												><s:text name="jsp.default_366"/></sj:a>
										</td>
									</tr>
							</ffi:cinclude>
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
											<tr>
											<td align="center" colspan="6">
												<sj:a
													href='#' 
													button="true" 
													onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
													><s:text name="jsp.default_102"/>
												</sj:a>
												
											</td>
											</tr>
							</ffi:cinclude>
									<tr>
										<td height="10">&nbsp;</td>
									</tr>
                            </table>
						</s:form>
                        </div>
                    </td>
                </tr>
            </table>
	</ffi:cinclude>
	<!-- End of The Account limit table -->
		</div>
</div>

<!-- reset the list of wiretemplates to its original -->
<ffi:setProperty name="AllWireTemplates" property="Filter" value="All"/>

<ffi:removeProperty name="WireID"/>

<ffi:removeProperty name="GetMaxLimitForPeriod"/>
<ffi:removeProperty name="GetGroupLimits"/>
<ffi:removeProperty name="Entitlement_Limits"/>

<ffi:removeProperty name="LimitsList"/>
<ffi:removeProperty name="limitIndex"/>
<ffi:removeProperty name="TemplateIndex"/>
<ffi:removeProperty name="EditGroup_WireTemplateID"/>

<ffi:removeProperty name="SearchWireType"/>
<ffi:removeProperty name="SearchWireName"/>
<ffi:removeProperty name="SearchWireCategory"/>

<ffi:removeProperty name="WireTemplateDisplaySize"/>
<ffi:removeProperty name="numAccounts"/>
<ffi:removeProperty name="LastRequest"/>
<ffi:removeProperty name="PageHeading2"/>
<ffi:removeProperty name="DisplayErrors"/>
<ffi:removeProperty name="CheckEntitlementObjectType"/>
<ffi:removeProperty name="CheckEntitlementObjectId"/>
<ffi:removeProperty name="CheckOperationEntitlement"/>

<ffi:removeProperty name="ParentChild"/>

<ffi:removeProperty name="templateMath"/>
<ffi:removeProperty name="limitMath"/>
<ffi:removeProperty name="childIndex"/>

<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:setProperty name="FromBack" value=""/>
<ffi:removeProperty name="dependentDaEntitlements"/>

<s:include value="%{#session.PagesPath}user/inc/set-page-da-css.jsp" />
