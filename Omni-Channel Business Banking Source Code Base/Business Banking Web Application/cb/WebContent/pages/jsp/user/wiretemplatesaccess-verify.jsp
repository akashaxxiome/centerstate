<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="java.util.ArrayList" %>
<div id="wiretemplatesaccessVerifyDiv" class="wiretemplatesaccessHelpCSS">
<ffi:help id="user_wiretemplateaccess-verify" className="moduleHelpClass"/>
<ffi:setProperty name='PageHeading' value='Verify Wire Templates Access and Limits'/>
<ffi:removeProperty name='PageHeading2'/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:setProperty name="SavePermissionsWizard" value="${PermissionsWizard}"/>

<ffi:object id="CheckOperationEntitlement" name="com.ffusion.tasks.admin.CheckOperationEntitlement"/>
<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>

<%-- we need to do auto entitlement checking for company, division and group being modified --%>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">

<%-- get the auto entitle settings for the business --%>
<%-- assuming the correct Entitlement_EntitlementGroup is in session --%>
<ffi:object name="com.ffusion.tasks.autoentitle.GetCumulativeSettings" id="GetCumulativeSettings" scope="session"/>
<ffi:setProperty name="GetCumulativeSettings" property="EntitlementGroupSessionKey" value="Entitlement_EntitlementGroup"/>
<ffi:process name="GetCumulativeSettings"/>


</s:if>

<%-- set action start--%>
<% String action = "editWireTemplatePermissions-execute" ;%>

<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">

	<ffi:cinclude value1="${GetCumulativeSettings.EnableWireTemplates}" value2="true" operator="equals">

		<%-- Determine if we should show a "CONTINUE" button or a "SAVE" button. --%>
		<ffi:setProperty name="ShowContinueButton" value="false" />
		<%
			ArrayList wireTemplatesList = (ArrayList) session.getAttribute("WireTemplatesList" );
			if( wireTemplatesList != null ) {
				for( int i=0; i < wireTemplatesList.size(); i++ ) {
					String thisTemplate = (String) wireTemplatesList.get(i);
					//String WireTemplateNumber = "WireTemplateNumber" + i;
					String templateIndex = (new Integer(i)).toString();
		%>
					<ffi:setProperty name="EditWireTemplatePermissions" property="InitOnly" value="true" /> <%-- reset task --%>
					<ffi:setProperty name="EditWireTemplatePermissions" property="TemplateIndex" value="<%= templateIndex %>"/>
					<ffi:setProperty name="SetWireTransfer" property="ID" value="<%= thisTemplate %>"/>
					<ffi:process name="SetWireTransfer"/>
					<ffi:setProperty name="EditWireTemplatePermissions" property="WireTemplateId" value="${WireTemplate.TemplateID}"/>
					<ffi:process name="EditWireTemplatePermissions"/>
					<ffi:cinclude value1="${EditWireTemplatePermissions.NumGrantedEntitlements}" value2="0" operator="notEquals">
						<ffi:setProperty name="ShowContinueButton" value="true" />
					</ffi:cinclude>
					<%
						String showContinueButton = (String) session.getAttribute( "ShowContinueButton" );
						if( showContinueButton.equalsIgnoreCase( "true" ) ) break;
					}
				}   %>
			<ffi:cinclude value1="${ShowContinueButton}" value2="true" operator="equals">
				<% action = "editWireTemplatePermissions-autoentitle" ;%>
			</ffi:cinclude>
	</ffi:cinclude>
</s:if>
<s:if test="%{#session.Section == 'UserProfile'}">
	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile">
		<% action = "editWireTemplatePermissions-execute"; %>
	</ffi:cinclude>
</s:if>
<% 
	request.setAttribute("action", action);
	session.setAttribute("action_execute", "editWireTemplatePermissions-execute");
%>
<%-- set action end--%>

		<div align="center">
			<div align="center" class="marginBottom10"><ffi:getProperty name="Context"/></div>
<s:form id="WireFormNameVerify" name="WireFormNameVerify" namespace="/pages/user" action="%{#request.action}" validate="false" theme="simple" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:if test="%{#session.Section == 'UserProfile'}">
	<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile">
		<input type="hidden" name="doAutoEntitle" value='true' />
	</ffi:cinclude>
</s:if>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
                                            <table width=100%" border="0" cellspacing="0" cellpadding="0" class="permissionTblWithBorder adminBackground tableData">
                                                <tr class="header">
                                                    <td valign="middle" align="center" nowrap class="sectionsubhead">
                                                        <s:text name="jsp.user_177"/>
                                                    </td>
                                                    <td valign="middle" nowrap class="sectionsubhead">
                                                        <s:text name="jsp.user_345"/>
                                                    </td>
                                                    <td valign="middle" align="left" nowrap class="sectionsubhead">
						    <ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
						    <ffi:process name="GetLimitBaseCurrency" />
                                                        <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                                    </td>
                                                    <td valign="middle" align="center" nowrap class="sectionsubhead">
                                                    </td>
                                                    <td valign="middle" align="center" nowrap class="sectionsubhead">
																											<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                                        <s:text name="jsp.user_153"/>
																											</ffi:cinclude>
																											<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																												&nbsp;
																											</ffi:cinclude>
                                                    </td>
                                                    <td valign="middle" align="left" nowrap class="sectionsubhead">
                                                        <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                                    </td>
                                                    <td valign="middle" align="center" nowrap class="sectionsubhead">
                                                    </td>
                                                    <td valign="middle" align="center" nowrap class="sectionsubhead">
																											<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                                        <s:text name="jsp.user_153"/>
																											</ffi:cinclude>
																											<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
																												&nbsp;
																											</ffi:cinclude>
                                                    </td>
                                                </tr>
<%
ArrayList wireTemplatesList = (ArrayList) session.getAttribute("WireTemplatesList" );
if (wireTemplatesList != null) {
	for (int i=0; i < wireTemplatesList.size(); i++) {
		String thisTemplate = (String) wireTemplatesList.get(i);
		String allowedTemplate = "WireTemplateNumber" + i;
		String thisTemplateId = (String) session.getAttribute(allowedTemplate);
		if (thisTemplateId == null) {
			allowedTemplate = "";
		}
		session.setAttribute( "TemplateIndex", new Integer(i) );
		session.setAttribute( "limitIndex", new Integer( 0 ) );
		session.setAttribute( "LimitsList", session.getAttribute("NonAccountEntitlementsMerged") );
%>
	<ffi:setProperty name="SetWireTransfer" property="ID" value="<%= thisTemplate %>"/>
	<ffi:process name="SetWireTransfer"/>

												<tr>
													<td class="" align="center" valign="middle">
														<ffi:cinclude value1="<%= allowedTemplate %>" value2="" operator="equals">
									    	                <input type="checkbox" disabled border="0">
									    	            </ffi:cinclude>    
														<ffi:cinclude value1="<%= allowedTemplate %>" value2="" operator="notEquals">
									    	                <input type="checkbox" disabled checked border="0">
									    	            </ffi:cinclude>    
									    	        </td>
									    	         <td class="" colspan="7" align="left">
														<span class="'<ffi:getPendingStyle fieldname="" defaultcss=" sectionsubhead " dacss=" sectionsubheadDA "  
													sessionCategoryName="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>" />'">
											                <ffi:getProperty name="WireTemplate" property="WireName"/>
											            </span>
													</td>
												</tr>
<ffi:setProperty name="DisplayedLimit" value="FALSE"/>
<ffi:setProperty name="CheckEntitlementObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATE %>"/>
<ffi:setProperty name="CheckEntitlementObjectId" value="${WireTemplate.TemplateID}"/> 

<ffi:flush/>
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<s:include value="%{#session.PagesPath}user/inc/corpadminuserlimit_merged-verify.jsp"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<s:include value="%{#session.PagesPath}user/inc/corpadmingrouplimit_merged-verify.jsp"/>
</s:if>

<ffi:cinclude value1="${DisplayedLimit}" value2="FALSE" operator="equals">
                                                <tr>
                                                    <td colspan="8" valign="middle" align="center">
                                                        <span class="columndata"><s:text name="jsp.user_226"/></span>
                                                    </td>
                                                </tr>
</ffi:cinclude>
<%
	}
}
%>
											</table>
										<div class="btn-row" style="margin-bottom:25px !important;">
											<s:url id="inputUrl" value="%{#session.PagesPath}user/wiretemplatesaccess.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
												<s:param name="FromBack" value="%{'TRUE'}"></s:param>
												<s:param name="UseLastRequest" value="%{'TRUE'}"></s:param>
												<s:param name="PermissionsWizard" value="%{#session.PermissionsWizard}"></s:param>
											</s:url>
														<sj:a
															href="%{inputUrl}"
															targets="wiretemplatesaccessDiv"
															button="true"
															onCompleteTopics="backButtonTopic"
															><s:text name="jsp.default_57"/></sj:a>
														<sj:a
															button="true"
															onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"
															><s:text name="jsp.default_102"/></sj:a>
												<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
														<sj:a
															formIds="WireFormNameVerify"
															targets="resultmessage"
															button="true" 
															validate="false"
															onBeforeTopics="beforeSubmit" 
															onErrorTopics="errorSubmit"
															onSuccessTopics="completedWireTemplatesEdit,reloadCompanyApprovalGrid"
															><s:text name="jsp.default_366"/></sj:a>
												</s:if>
												<s:if test="%{#session.Section == 'UserProfile'}">
													<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile" operator="equals">
														<sj:a
															formIds="WireFormNameVerify"
															targets="resultmessage"
															button="true" 
															validate="false"
															onBeforeTopics="beforeSubmit" 
															onErrorTopics="errorSubmit"
															onSuccessTopics="completedWireTemplatesEdit,reloadCompanyApprovalGrid"
															><s:text name="jsp.default_366"/></sj:a>
													</ffi:cinclude>
													<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="ChannelProfile" operator="notEquals">
														<ffi:cinclude value1="${GetCumulativeSettings.EnableWireTemplates}" value2="true" operator="equals">

														<ffi:setProperty name="EditWireTemplatePermissions" property="InitOnly" value="false" />

														<ffi:cinclude value1="${ShowContinueButton}" value2="true" operator="equals">
															<ffi:setProperty name="perm_target" value="Wire_Templates"/>
															<ffi:setProperty name="completedTopics" value="completedWireTemplatesEdit"/>
															
																<sj:a
																	formIds="WireFormNameVerify"
																	targets="Wire_Templates"
																	button="true" 
																	validate="false"
																	onBeforeTopics="beforeVerify" 
																	><s:text name="jsp.default_111"/></sj:a>
															<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
																<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg23')}" scope="request" />
																<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
																<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg24')}" scope="request" />
																<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
																<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg25')}" scope="request" />
																<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:setProperty name="autoEntitleBackURL" value="${SecurePath}user/wiretemplatesaccess-verify.jsp"/>
															<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
																<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
																<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
															</ffi:cinclude>
															<ffi:setProperty name="autoEntitleFormAction" value="${SecurePath}user/wiretemplatesaccess-confirm.jsp"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${ShowContinueButton}" value2="true" operator="notEquals">
															<sj:a
																formIds="WireFormNameVerify"
																targets="resultmessage"
																button="true" 
																validate="false"
																onBeforeTopics="beforeSubmit" 
																onErrorTopics="errorSubmit"
																onSuccessTopics="completedWireTemplatesEdit,reloadCompanyApprovalGrid"
																><s:text name="jsp.default_366"/></sj:a>
														</ffi:cinclude>

														<ffi:removeProperty name="ShowContinueButton" />

													</ffi:cinclude>

													<ffi:cinclude value1="${GetCumulativeSettings.EnableWireTemplates}" value2="false" operator="equals">
														<sj:a
															formIds="WireFormNameVerify"
															targets="resultmessage"
															button="true" 
															validate="false"
															onBeforeTopics="beforeSubmit" 
															onErrorTopics="errorSubmit"
															onSuccessTopics="completedWireTemplatesEdit,reloadCompanyApprovalGrid"
															><s:text name="jsp.default_366"/></sj:a>
													</ffi:cinclude>
													</ffi:cinclude>
												</s:if>
												<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles' && #session.Section != 'UserProfile'}">

													<ffi:cinclude value1="${GetCumulativeSettings.EnableWireTemplates}" value2="true" operator="equals">

														<ffi:setProperty name="EditWireTemplatePermissions" property="InitOnly" value="false" />

														<ffi:cinclude value1="${ShowContinueButton}" value2="true" operator="equals">
															<ffi:setProperty name="perm_target" value="Wire_Templates"/>
															<ffi:setProperty name="completedTopics" value="completedWireTemplatesEdit"/>
															
																<sj:a
																	formIds="WireFormNameVerify"
																	targets="Wire_Templates"
																	button="true" 
																	validate="false"
																	onBeforeTopics="beforeVerify" 
																	><s:text name="jsp.default_111"/></sj:a>
															<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
																<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg23')}" scope="request" />
																<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="Divisions" operator="equals">
																<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg24')}" scope="request" />
																<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="Groups" operator="equals">
																<s:set var="tmpI18nStr" value="%{getText('jsp.user.autoEntitleConfirmMsg25')}" scope="request" />
																<ffi:setProperty name="autoEntitleConfirmMsg" value="${tmpI18nStr}"/>
															</ffi:cinclude>
															<ffi:setProperty name="autoEntitleBackURL" value="${SecurePath}user/wiretemplatesaccess-verify.jsp"/>
															<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
																<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/corpadmininfo.jsp"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
																<ffi:setProperty name="autoEntitleCancel" value="${SecurePath}user/permissions.jsp"/>
															</ffi:cinclude>
															<ffi:setProperty name="autoEntitleFormAction" value="${SecurePath}user/wiretemplatesaccess-confirm.jsp"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${ShowContinueButton}" value2="true" operator="notEquals">
															<sj:a
																formIds="WireFormNameVerify"
																targets="resultmessage"
																button="true" 
																validate="false"
																onBeforeTopics="beforeSubmit" 
																onErrorTopics="errorSubmit"
																onSuccessTopics="completedWireTemplatesEdit,reloadCompanyApprovalGrid"
																><s:text name="jsp.default_366"/></sj:a>
														</ffi:cinclude>

														<ffi:removeProperty name="ShowContinueButton" />

													</ffi:cinclude>

													<ffi:cinclude value1="${GetCumulativeSettings.EnableWireTemplates}" value2="false" operator="equals">
														<sj:a
															formIds="WireFormNameVerify"
															targets="resultmessage"
															button="true" 
															validate="false"
															onBeforeTopics="beforeSubmit" 
															onErrorTopics="errorSubmit"
															onSuccessTopics="completedWireTemplatesEdit,reloadCompanyApprovalGrid"
															><s:text name="jsp.default_366"/></sj:a>
													</ffi:cinclude>

												</s:if>
										</div></div></div>
							</s:form>
		</div>
<ffi:removeProperty name="PageHeading2"/>
<ffi:removeProperty name="CheckEntitlementObjectType"/>
<ffi:removeProperty name="CheckEntitlementObjectId"/>
<ffi:removeProperty name="TemplateIndex"/>
<ffi:removeProperty name="CheckOperationEntitlement"/>
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>
<ffi:removeProperty name="SavePermissionsWizard"/>
</div>



