<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<ffi:help id="user_clone-account-permissions-confirm" className="moduleHelpClass"/>
<div align="center">
	<TABLE class="adminBackground" width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD>
				<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
					<TR>
						<TD class="columndata" align="center"><s:text name="jsp.user_307"/></TD>
					</TR>

					<TR>
						<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="5"></TD>
					</TR>
					<TR>
						<TD align="center">
							<sj:a button="true"  onClickTopics="completeCloneAccountPermLoad"><s:text name="jsp.default_175"/></sj:a>
						</TD>
					</TR>
				</TABLE>
			</TD>
		</TR>
	</TABLE>
	<p></p>
</div>

