<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_corpadminuseredit-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_85')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<%-- Set the passEmpty value to "" only if both fields are empty.  This makes sure that if the task fails the
         process, when we go back to the main screen, the password fields will be blank.  Otherwise, it will
		 contain the NewBusinessEmployee's current password, but that's not the state that the user left it in.
--%>
<ffi:setProperty name="success" value="false"/>
<ffi:cinclude value1="${NewBusinessEmployee.Password}" value2="" operator="equals">
	<ffi:cinclude value1="${NewBusinessEmployee.ConfirmPassword}" value2="" operator="equals">
	<ffi:setProperty name="passEmpty" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>


<ffi:removeProperty name="passEmpty"/>
<ffi:removeProperty name="success"/>
<ffi:removeProperty name="NewBusinessEmployee.Password"/>
<ffi:removeProperty name="NewBusinessEmployee.ConfirmPassword"/>
<ffi:removeProperty name="NewPassword2"/>
<ffi:removeProperty name="ConfirmPassword2"/>

		<div align="center" class="marginTop20">
				<div class="columndata">
					<ffi:cinclude value1="${sendForApproval}" value2="true" operator="notEquals">
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="notEquals">
						<%-- <ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseredit-confirm.jsp-1" parm0="${BusinessEmployee.MasterBusinessEmployee.UserName}"/> --%>
						<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseredit-confirm.msg1.jsp-1"/>
						<strong> <ffi:getProperty name="BusinessEmployee.MasterBusinessEmployee" property="UserName"/> </strong>
						<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseredit-confirm.msg1.jsp-2"/>
					</ffi:cinclude>
					</ffi:cinclude>
						
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">
							<ffi:cinclude value1="${BusinessEmployee.accountStatus}" value2="1" operator="equals">
								<%-- <ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseredit-confirm.jsp-3" parm0="${BusinessEmployee.MasterBusinessEmployee.UserName}"/> --%>
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseredit-confirm.msg3.jsp-1"/>
								<strong> <ffi:getProperty name="BusinessEmployee.MasterBusinessEmployee" property="UserName"/> </strong>
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseredit-confirm.msg3.jsp-2"/>
						    </ffi:cinclude>
					
							<ffi:cinclude value1="${BusinessEmployee.accountStatus}" value2="128" operator="equals">
								<%-- <ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseredit-confirm.jsp-4" parm0="${BusinessEmployee.UserName}"/> --%>
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseredit-confirm.jsp.msg4-1"/>
								<strong> <ffi:getProperty name="BusinessEmployee" property="UserName"/> </strong>
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseredit-confirm.jsp.msg4-2"/>
							</ffi:cinclude>
						</ffi:cinclude>
				</div>
	    </div>
		<div class="btn-row">
								<sj:a button="true" onClickTopics="cancelUserForm,refreshUserGrid"><s:text name="jsp.default_175"/></sj:a>
		</div>
<ffi:removeProperty name="sendForApproval"/>
<ffi:removeProperty name="TempBusEmployee"/>
<ffi:removeProperty name="NewBusinessEmployee"/>
<ffi:removeProperty name="BusinessEmployees"/>
<ffi:removeProperty name="GroupAdminEmployees"/>
<ffi:removeProperty name="DAItem"/>
<ffi:removeProperty name="fromPendingTable"/>
<ffi:removeProperty name="TempBusinessEmployee"/>

<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadminusers-pre.jsp"/>
