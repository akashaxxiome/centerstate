<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<s:set var="tmpI18nStr" value="%{getText('jsp.user_373')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:help id="user_corpadmindivdelete-verify" className="moduleHelpClass"/>
<%
	String groupId = request.getParameter("DeleteGroup_GroupId");
	String divName = request.getParameter("DeleteDivision_DivisionName");
%>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="<%=groupId%>"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='<%=groupId%>'/>
<ffi:setProperty name="DivisionToDeleteName" value='<%=divName%>'/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:object id="CanDeleteEntitlementGroup" name="com.ffusion.tasks.admin.CanDeleteEntitlementGroup" scope="session"/>
<ffi:process name="CanDeleteEntitlementGroup"/>

<ffi:removeProperty name="CanDeleteEntitlementGroup" />

<div id="wholeworld">
	<div align="center">

		<table class="adminBackground" width="480" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<br>
						<tr>
							<td class="columndata" align="center">
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmindivdelete-verify.jsp-1" parm0="${Entitlement_EntitlementGroup.groupName}"/></td>
						</tr>
						<tr>
							<td height="60">&nbsp;</td>
						</tr>
						<tr>
							<td>
								<div align="center" class="ui-widget-header customDialogFooter">
									<br>
									<sj:a id="cancelDeleteDivLink" button="true" onClickTopics="closeDialog"
										  title="%{getText('jsp.default_83')}" 
										  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_82" /></sj:a>
									<s:url id="deleteDivUrl" value="/pages/jsp/user/corpadmindivdelete-confirm.jsp">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									</s:url>
									<sj:a id="deleteDivLink" href="%{deleteDivUrl}"
										  targets="wholeworld"
										  button="true" 
										  title="%{getText('jsp.user_394')}"
										  onCompleteTopics="completeDeleteGroupVerify" onErrorTopics="errorDeleteGroup"
										  effectDuration="1500"
										  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_162" /></sj:a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<p></p>
	</div>
</div>
