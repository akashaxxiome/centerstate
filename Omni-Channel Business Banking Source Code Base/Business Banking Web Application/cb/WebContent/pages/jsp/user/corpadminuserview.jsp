<%--
This is a common view admin user information page.

Pages that request this page
----------------------------
Pages this page requests
------------------------
DONE button requests corpadminusers.jsp

Pages included in this page
---------------------------
--%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%-- Need to change help file id name after adding help file for admin user view --%>
<ffi:help id="payments_wirepayeeview" className="moduleHelpClass"/>

<%
	session.setAttribute("itemId", request.getParameter("itemId"));
%>
<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>

<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminusers.jsp"/>

<%-- set BusinessEmployee to be the current logged in user --%>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>
<ffi:removeProperty name="GetBusinessEmployee" />

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployees"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>

<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployees" />
<ffi:process name="SetBusinessEmployee"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">

	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="BusinessEmployee"/>
	<ffi:process name="PopulateObjectWithDAValues"/>

	<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
	<ffi:setProperty name="GetDAItem" property="Validate" value="itemId,ItemType" />
	<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_USER%>"/>
	<ffi:process name="GetDAItem"/>
	<ffi:cinclude value1="${DAItem.UserAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED%>">
		<ffi:setProperty name="isUserDeleted" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>

<%-- for localizing state name --%>
<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${BusinessEmployee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${BusinessEmployee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>

<div align="center">
	<%-- <table class="adminBackground" width="676" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="2">
				<div align="center">
					<span class="sectionsubhead"><br><ffi:getProperty name="BusinessName"/> <ffi:cinclude value1="DivisionName" value2="" operator="equals"> / <ffi:getProperty name="DivisionName"/></ffi:cinclude>&nbsp;<br>
						<br>
					</span></div>
			</td>
		</tr>
	</table> --%>
	<div class="blockWrapper">
		<div class="blockHead"><s:text name="user.login.summary" /></div>
		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock">
					<span class='<ffi:getPendingStyle fieldname="userName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/> sectionLabel' valign="baseline"><s:text name="jsp.user_Username" /></span>
					<span class="sectionsubhead valueCls">
						<ffi:getProperty name="BusinessEmployee" property="UserName"/>
						<ffi:cinclude value1="${oldDAObject.UserName}" value2="" operator="notEquals">
						<span class="sectionhead_greyDA">
							<br>
							<ffi:getProperty name="oldDAObject" property="UserName"/>
						</span>
						</ffi:cinclude>
					</span>
				</div>
				<div class="inlineBlock">
					<ffi:cinclude value1="${isUserDeleted}" value2="true">
					<span class="sectionsubhead sectionLabel" valign="baseline"><s:text name="jsp.user_User_Status" /></span>
					<span  class="sectionsubhead valueCls" width="200" align="left" >
						<s:text name="jsp.user_Deleted"/>
						<span class="sectionhead_greyDA">
						<br>
							<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%=String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE)%>"><s:text name="jsp.user_Active"/></ffi:cinclude>
							<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%=String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE)%>"><s:text name="jsp.user_Inctive"/></ffi:cinclude>
							<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%=String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE)%>"><s:text name="jsp.user_Temp_Inactive"/></ffi:cinclude>
							<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%=String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL)%>"><s:text name="jsp.user_Pending_Approval"/></ffi:cinclude>
						</span>
					</span>
				</ffi:cinclude>
				<ffi:cinclude value1="${isUserDeleted}" value2="true" operator="notEquals">
					<span class='<ffi:getPendingStyle fieldname="accountStatus" defaultcss="sectionsubhead"  dacss="sectionheadDA"/> sectionLabel' valign="baseline"><s:text name="jsp.user_User_Status" /></span>
					<span  class="sectionsubhead valueCls" width="200" align="left" >
						<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="" operator="notEquals">
							<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.AccountStatus}" />
							<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE )%>"/>
							<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Active" /></ffi:cinclude>
							<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
							<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Inactive" /></ffi:cinclude>
							<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE )%>"/>
							<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Temp_Inactive" /></ffi:cinclude>
							<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL )%>"/>
							<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Pending_Approval" /></ffi:cinclude>
						</ffi:cinclude>

						<ffi:cinclude value1="${oldDAObject.AccountStatus}" value2="" operator="notEquals">
							<span class="sectionhead_greyDA">
							<br>
								<ffi:setProperty name="Compare" property="Value1" value="${oldDAObject.AccountStatus}" />
								<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE )%>"/>
								<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Active" /></ffi:cinclude>
								<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
								<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Inactive" /></ffi:cinclude>
								<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE )%>"/>
								<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Temp_Inactive" /></ffi:cinclude>
								<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL )%>"/>
								<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Pending_Approval" /></ffi:cinclude>
							</span>
						</ffi:cinclude>
					</span>
				</ffi:cinclude>
				
				</div>
			</div>
		</div>
	</div>
	
	<div class="blockWrapper">
		<div class="blockHead"><s:text name="user.details" /></div>
		<div class="blockContent blockLabel130">
			<div class="blockRow">
				<div class="inlineBlock">
					<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
						<span class='<ffi:getPendingStyle fieldname="firstName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_First_Name" /></span>
						<span class="sectionsubhead valueCls" width="180" >
							<ffi:getProperty name="BusinessEmployee" property="FirstName"/>
							<span class="sectionhead_greyDA">
							<br>
								<ffi:getProperty name="oldDAObject" property="FirstName"/>
							</span>
						</span>
					</ffi:cinclude>
				</div>
				<div class="inlineBlock">
				<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
				<span class='<ffi:getPendingStyle fieldname="lastName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Last_Name" /></span>
				</ffi:cinclude>
				<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
					<span class='<ffi:getPendingStyle fieldname="lastName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Name" /></span>
				</ffi:cinclude>
				<span  class="sectionsubhead valueCls" width="180" valign="baseline">
					<ffi:getProperty name="BusinessEmployee" property="LastName"/>
					<span class="sectionhead_greyDA">
						<br>
						<ffi:getProperty name="oldDAObject" property="LastName"/>
					</span>
				</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock">
					<span class='<ffi:getPendingStyle fieldname="entitlementGroupId" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Group" />:</span>
					<span class="sectionsubhead valueCls" width="200" valign="baseline">
						<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
											<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
											<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value="NewEntitlementGroup" />
						<ffi:process name="GetEntitlementGroup"/>												
						<ffi:getProperty name="NewEntitlementGroup" property="GroupName"/>
						<span class="sectionhead_greyDA">
							<br>
                                     <ffi:cinclude value1="${oldDAObject.EntitlementGroupId}" value2="" operator="notEquals">
                                         <ffi:cinclude value1="${oldDAObject.EntitlementGroupId}" value2="0" operator="notEquals">
                                             <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${oldDAObject.EntitlementGroupId}" />
                                             <ffi:process name="GetEntitlementGroup"/>
                                             <ffi:getProperty name="NewEntitlementGroup" property="GroupName"/>
                                         </ffi:cinclude>
                                     </ffi:cinclude>
						</span>
                        <ffi:removeProperty name="NewEntitlementGroup"/>
					</span>	
				</div>
				<div class="inlineBlock">
					
					<%-- Retrieve the list of admins on this group --%>
					<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
					<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}"/>
					<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="GroupAdmins"/>
					<ffi:process name="GetAdminsForGroup"/>
					<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="request"/>
                       <ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="BusinessEmployeesSessionName" value="GroupAdmins" />
                       <ffi:process name="GetBusinessEmployeesByEntGroups"/>

                       <ffi:setProperty name="GroupAdmins" property="Filter" value="ID!${BusinessEmployee.Id}"/>
					<span class="sectionsubhead" align="right">
                           <ffi:cinclude value1="${GroupAdmins.Size}" value2="0" operator="notEquals">
                               <div><span class='<ffi:getPendingStyle fieldname="primaryAdmin" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>'><s:text name="jsp.user_Primary_Admin" /></span></div>
                           </ffi:cinclude>
                    </span>
					<span class="sectionsubhead valueCls">
                        <ffi:cinclude value1="${GroupAdmins.Size}" value2="0" operator="notEquals">
                            <ffi:list collection="GroupAdmins" items="employee">
                                <ffi:cinclude value1="${employee.Id}" value2="${BusinessEmployee.PrimaryAdmin}">
                                    <ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
                                </ffi:cinclude>
                            </ffi:list>
                        </ffi:cinclude>
                        <span class="sectionhead_greyDA">
                            <br>
						<ffi:list collection="GroupAdmins" items="employee">
							<ffi:cinclude value1="${oldDAObject.PrimaryAdmin}" value2="${employee.Id}">
							 	<ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
							</ffi:cinclude>
						</ffi:list>
						</span>
                    </span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock">
					<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
					<span class="sectionsubhead" align="right" valign="baseline"><s:text name="jsp.user_Lockout_Status" /></span>
					<span class="sectionsubhead valueCls" width="200" valign="baseline">
						<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="equals">
							<s:text name="jsp.user_Expired" />
						</ffi:cinclude>
						<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="equals">
							<s:text name="jsp.user_Locked" />
						</ffi:cinclude>
						<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="equals">
							<s:text name="jsp.user_Locked" />
						</ffi:cinclude>
						<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="notEquals">
							<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="notEquals">
								<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="notEquals">
								<s:text name="jsp.user_Unlocked" />
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</span>
				</ffi:cinclude>
                            
				<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
					<span class="sectionsubhead" align="right" valign="baseline"></td>
					<span class="sectionsubhead valueCls" width="200" valign="baseline"></span>
				</ffi:cinclude>			
				</div>
				
				<div class="inlineBlock">
					<span class='<ffi:getPendingStyle fieldname="email" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Email" /></span>
					<span class="sectionsubhead valueCls" width="180" valign="baseline">
						<ffi:getProperty name="BusinessEmployee" property="Email"/>									
						<ffi:cinclude value1="${oldDAObject.Email}" value2="" operator="notEquals">
						<span class="sectionhead_greyDA">
							<br>
							<ffi:getProperty name="oldDAObject" property="Email"/>
						</span>
						</ffi:cinclude>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock">
					<span class='<ffi:getPendingStyle fieldname="phone" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Phone" /></span>
					<span class="sectionsubhead valueCls" width="180" valign="baseline">
						<ffi:getProperty name="BusinessEmployee" property="Phone"/>
						<ffi:cinclude value1="${oldDAObject.Phone}" value2="" operator="notEquals">
							<span class="sectionhead_greyDA">
								<br>
								<ffi:getProperty name="oldDAObject" property="Phone"/>
							</span>
						</ffi:cinclude>
					</span>
				</div>
				<div class="inlineBlock">
					<span class='<ffi:getPendingStyle fieldname="dataPhone" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_DataPhone" /></span>
					<span class="sectionsubhead valueCls" width="180" valign="baseline">
						<ffi:getProperty name="BusinessEmployee" property="DataPhone"/>
						<span class="sectionhead_greyDA">
							<br>
							<ffi:getProperty name="oldDAObject" property="DataPhone"/>
						</span>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock">
					<span class='<ffi:getPendingStyle fieldname="faxPhone" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Fax" /></span>
					<span class="sectionsubhead valueCls" width="180" valign="baseline">
						<ffi:getProperty name="BusinessEmployee" property="FaxPhone"/>
						<span class="sectionhead_greyDA">
							<br>
							<ffi:getProperty name="oldDAObject" property="FaxPhone"/>
						</span>
					</span>
				</div>
				<ffi:object id="GetLanguageList" name="com.ffusion.tasks.util.GetLanguageList" scope="session" />
				<ffi:process name="GetLanguageList" />
				<div class="inlineBlock">
					<ffi:cinclude value1="${GetLanguageList.LanguagesList.size}" value2="1" operator="notEquals">
					<ffi:object id="LanguageResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
						<ffi:setProperty name="LanguageResource" property="ResourceFilename" value="com.ffusion.utilresources.languages" />
					<ffi:process name="LanguageResource" />
					<ffi:setProperty name="LanguageResource" property="ResourceID" value="${BusinessEmployee.PreferredLanguage}"/>
					
					<span class='<ffi:getPendingStyle fieldname="preferredLanguage" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_PreferredLanguage" /></span>
					<span class="sectionsubhead valueCls" width="180" valign="baseline">
						<ffi:getProperty name="LanguageResource" property="Resource"/>
						<span class="sectionhead_greyDA">
							<br>
							<ffi:setProperty name="LanguageResource" property="ResourceID" value="${oldDAObject.PreferredLanguage}" />
							<ffi:getProperty name='LanguageResource' property='Resource'/>
						</span>
					</span>									
				</ffi:cinclude>
				</div>
				<ffi:removeProperty name="GetLanguageList" />
				<ffi:removeProperty name="LanguageResource" />
			</div>
			<div class="blockRow">
				<div class="inlineBlock">
					<span class='<ffi:getPendingStyle fieldname="password" defaultcss="sectionhead"  dacss="sectionheadDA"/>' align="right" valign="baseline">Password</span>
					<span class="sectionsubhead valueCls" width="200" valign="baseline">
						******												
					</span>	
				</div>
			</div>	
			<div class="blockRow">
				<div class="inlineBlock" style="width:100%;">
					<ffi:list collection="CATEGORY_BEAN.DaItems" items="daItemRow">
					<ffi:cinclude value1="${daItemRow.FieldName}" value2="street" operator="equals">
						<ffi:setProperty name="highlightAddress" value="y" />
					</ffi:cinclude>
					<ffi:cinclude value1="${daItemRow.FieldName}" value2="street2" operator="equals">
						<ffi:setProperty name="highlightAddress" value="y" />
					</ffi:cinclude>
					<ffi:cinclude value1="${daItemRow.FieldName}" value2="city" operator="equals">
						<ffi:setProperty name="highlightAddress" value="y" />
					</ffi:cinclude>
					<ffi:cinclude value1="${daItemRow.FieldName}" value2="state" operator="equals">
						<ffi:setProperty name="highlightAddress" value="y" />
					</ffi:cinclude>
					<ffi:cinclude value1="${daItemRow.FieldName}" value2="zipCode" operator="equals">
						<ffi:setProperty name="highlightAddress" value="y" />
					</ffi:cinclude>
					<ffi:cinclude value1="${daItemRow.FieldName}" value2="country" operator="equals">
						<ffi:setProperty name="highlightAddress" value="y" />
					</ffi:cinclude>
				</ffi:list>
				<ffi:cinclude value1="${highlightAddress}" value2="y" operator="equals">
					<span class="sectionheadDA" align="right" valign="baseline" ><s:text name="jsp.user_Address" /></span>
				</ffi:cinclude>
				<ffi:cinclude value1="${highlightAddress}" value2="y" operator="notEquals">
					<span class="sectionhead" align="right" valign="baseline" ><s:text name="jsp.user_Address" /></span>
				</ffi:cinclude>
				<span class="sectionsubhead valueCls"  width="95%" valign="baseline">
				
					<ffi:getProperty name="BusinessEmployee" property="Street"/>&nbsp;
					<ffi:cinclude value1="${BusinessEmployee.Street2}" value2="" operator="notEquals">
						<ffi:getProperty name="BusinessEmployee" property="Street2"/>&nbsp;
					</ffi:cinclude>
					<ffi:cinclude value1="${BusinessEmployee.City}" value2="" operator="notEquals">
						<ffi:getProperty name="BusinessEmployee" property="City"/>&nbsp;
					</ffi:cinclude>
					<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
						<ffi:getProperty name="BusinessEmployee" property="State"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
						<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
					</ffi:cinclude>

					<ffi:cinclude value1="${BusinessEmployee.State}" value2="" operator="notEquals">
					  <ffi:cinclude value1="${BusinessEmployee.ZipCode}" value2="" operator="notEquals">
						,
					  </ffi:cinclude>
					</ffi:cinclude>

					<ffi:getProperty name="BusinessEmployee" property="ZipCode"/>
					<ffi:cinclude value1="${BusinessEmployee.State}${BusinessEmployee.ZipCode}" value2="" operator="notEquals">
						&nbsp;
					</ffi:cinclude>


					<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
					<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
					<ffi:process name="CountryResource" />
									  						
 						<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${BusinessEmployee.Country}" />
					<ffi:getProperty name='CountryResource' property='Resource'/>
					<span class="sectionhead_greyDA">
						<ffi:cinclude value1="${oldDAObject.Street}" value2="" operator="notEquals">
						<br><ffi:getProperty name="oldDAObject" property="Street"/>,
						</ffi:cinclude>
						<ffi:cinclude value1="${oldDAObject.Street2}" value2="" operator="notEquals">
							&nbsp;<ffi:getProperty name="oldDAObject" property="Street2"/>,
						</ffi:cinclude>
						<ffi:cinclude value1="${oldDAObject.City}" value2="" operator="notEquals">
							<br><ffi:getProperty name="oldDAObject" property="City"/>,
						</ffi:cinclude>
  						<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${oldDAObject.Country}" />
  						<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${oldDAObject.State}" />
						<ffi:process name="GetStateProvinceDefnForState"/>
						<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
							&nbsp;<ffi:getProperty name="oldDAObject" property="State"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
							&nbsp;<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${oldDAObject.State}" value2="" operator="notEquals">
						  <ffi:cinclude value1="${oldDAObject.ZipCode}" value2="" operator="notEquals">
						  ,
						  </ffi:cinclude>
						</ffi:cinclude>
						<ffi:getProperty name="oldDAObject" property="ZipCode"/>
						<ffi:cinclude value1="${oldDAObject.State}${oldDAObject.ZipCode}" value2="" operator="notEquals">
						  &nbsp;
						</ffi:cinclude>
						<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${oldDAObject.Country}" />
						<ffi:getProperty name='CountryResource' property='Resource'/>
					</span>
				</span>
				</div>
			</div>
		</div>
	</div>		
	
	<%-- <table class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>&nbsp;</td>
			<td align="left" class="adminBackground">
					<div align="left">
						<table width="723" border="0" cellspacing="5" cellpadding="3" class="adminBackground">
							<tr>
								<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
									<td class='<ffi:getPendingStyle fieldname="firstName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_First_Name" /></td>
									<td class="columndata" width="180" >
										<ffi:getProperty name="BusinessEmployee" property="FirstName"/>
										<span class="sectionhead_greyDA">
										<br>
											<ffi:getProperty name="oldDAObject" property="FirstName"/>
										</span>
									</td>
								</ffi:cinclude>
								<td class='<ffi:getPendingStyle fieldname="faxPhone" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Fax" /></td>
								<td class="columndata" width="180" valign="baseline">
									<ffi:getProperty name="BusinessEmployee" property="FaxPhone"/>
									<span class="sectionhead_greyDA">
										<br>
										<ffi:getProperty name="oldDAObject" property="FaxPhone"/>
									</span>
								</td>
							</tr>
							<tr>
								<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<td class='<ffi:getPendingStyle fieldname="lastName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Last_Name" /></td>
								</ffi:cinclude>
								<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
									<td class='<ffi:getPendingStyle fieldname="lastName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Name" /></td>
								</ffi:cinclude>
								<td class="columndata" width="180" valign="baseline">
									<ffi:getProperty name="BusinessEmployee" property="LastName"/>
									<span class="sectionhead_greyDA">
										<br>
										<ffi:getProperty name="oldDAObject" property="LastName"/>
									</span>
								</td>
								<td class='<ffi:getPendingStyle fieldname="dataPhone" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_DataPhone" /></td>
								<td class="columndata" width="180" valign="baseline">
									<ffi:getProperty name="BusinessEmployee" property="DataPhone"/>
									<span class="sectionhead_greyDA">
										<br>
										<ffi:getProperty name="oldDAObject" property="DataPhone"/>
									</span>
								</td>
							</tr>
							<tr>
								<ffi:list collection="CATEGORY_BEAN.DaItems" items="daItemRow">
									<ffi:cinclude value1="${daItemRow.FieldName}" value2="street" operator="equals">
										<ffi:setProperty name="highlightAddress" value="y" />
									</ffi:cinclude>
									<ffi:cinclude value1="${daItemRow.FieldName}" value2="street2" operator="equals">
										<ffi:setProperty name="highlightAddress" value="y" />
									</ffi:cinclude>
									<ffi:cinclude value1="${daItemRow.FieldName}" value2="city" operator="equals">
										<ffi:setProperty name="highlightAddress" value="y" />
									</ffi:cinclude>
									<ffi:cinclude value1="${daItemRow.FieldName}" value2="state" operator="equals">
										<ffi:setProperty name="highlightAddress" value="y" />
									</ffi:cinclude>
									<ffi:cinclude value1="${daItemRow.FieldName}" value2="zipCode" operator="equals">
										<ffi:setProperty name="highlightAddress" value="y" />
									</ffi:cinclude>
									<ffi:cinclude value1="${daItemRow.FieldName}" value2="country" operator="equals">
										<ffi:setProperty name="highlightAddress" value="y" />
									</ffi:cinclude>
								</ffi:list>
								<ffi:cinclude value1="${highlightAddress}" value2="y" operator="equals">
									<td class="sectionheadDA" align="right" valign="baseline" ><s:text name="jsp.user_Address" /></td>
								</ffi:cinclude>
								<ffi:cinclude value1="${highlightAddress}" value2="y" operator="notEquals">
									<td class="sectionhead" align="right" valign="baseline" ><s:text name="jsp.user_Address" /></td>
								</ffi:cinclude>
								<td class="columndata" width="180" valign="baseline">
								
									<ffi:getProperty name="BusinessEmployee" property="Street"/><br>
									<ffi:cinclude value1="${BusinessEmployee.Street2}" value2="" operator="notEquals">
										<ffi:getProperty name="BusinessEmployee" property="Street2"/><br>
									</ffi:cinclude>
									<ffi:cinclude value1="${BusinessEmployee.City}" value2="" operator="notEquals">
										<ffi:getProperty name="BusinessEmployee" property="City"/><br>
									</ffi:cinclude>
									<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
										<ffi:getProperty name="BusinessEmployee" property="State"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
										<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
									</ffi:cinclude>

									<ffi:cinclude value1="${BusinessEmployee.State}" value2="" operator="notEquals">
									  <ffi:cinclude value1="${BusinessEmployee.ZipCode}" value2="" operator="notEquals">
										,
									  </ffi:cinclude>
									</ffi:cinclude>

									<ffi:getProperty name="BusinessEmployee" property="ZipCode"/>
									<ffi:cinclude value1="${BusinessEmployee.State}${BusinessEmployee.ZipCode}" value2="" operator="notEquals">
										<br>
									</ffi:cinclude>


									<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
									<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
									<ffi:process name="CountryResource" />
													  						
			  						<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${BusinessEmployee.Country}" />
									<ffi:getProperty name='CountryResource' property='Resource'/>
									<span class="sectionhead_greyDA">
										<ffi:cinclude value1="${oldDAObject.Street}" value2="" operator="notEquals">
										<br><ffi:getProperty name="oldDAObject" property="Street"/>,
										</ffi:cinclude>
										<ffi:cinclude value1="${oldDAObject.Street2}" value2="" operator="notEquals">
											<br><ffi:getProperty name="oldDAObject" property="Street2"/>,
										</ffi:cinclude>
										<ffi:cinclude value1="${oldDAObject.City}" value2="" operator="notEquals">
											<br><ffi:getProperty name="oldDAObject" property="City"/>,
										</ffi:cinclude>
				  						<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${oldDAObject.Country}" />
				  						<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${oldDAObject.State}" />
										<ffi:process name="GetStateProvinceDefnForState"/>
										<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
											<br><ffi:getProperty name="oldDAObject" property="State"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
											<br><ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${oldDAObject.State}" value2="" operator="notEquals">
										  <ffi:cinclude value1="${oldDAObject.ZipCode}" value2="" operator="notEquals">
										  ,
										  </ffi:cinclude>
										</ffi:cinclude>
										<ffi:getProperty name="oldDAObject" property="ZipCode"/>
										<ffi:cinclude value1="${oldDAObject.State}${oldDAObject.ZipCode}" value2="" operator="notEquals">
										  <br>
										</ffi:cinclude>
										<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${oldDAObject.Country}" />
										<ffi:getProperty name='CountryResource' property='Resource'/>
									</span>
								</td>
								<td class='<ffi:getPendingStyle fieldname="email" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Email" /></td>
								<td class="columndata" width="180" valign="baseline">
									<ffi:getProperty name="BusinessEmployee" property="Email"/>									
									<ffi:cinclude value1="${oldDAObject.Email}" value2="" operator="notEquals">
									<span class="sectionhead_greyDA">
										<br>
										<ffi:getProperty name="oldDAObject" property="Email"/>
									</span>
									</ffi:cinclude>
								</td>
							</tr>
								<ffi:object id="GetLanguageList" name="com.ffusion.tasks.util.GetLanguageList" scope="session" />
								<ffi:process name="GetLanguageList" />
							<tr>
								<ffi:cinclude value1="${GetLanguageList.LanguagesList.size}" value2="1" operator="notEquals">
									<ffi:object id="LanguageResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
										<ffi:setProperty name="LanguageResource" property="ResourceFilename" value="com.ffusion.utilresources.languages" />
									<ffi:process name="LanguageResource" />
									<ffi:setProperty name="LanguageResource" property="ResourceID" value="${BusinessEmployee.PreferredLanguage}"/>
									
										<td class='<ffi:getPendingStyle fieldname="preferredLanguage" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_PreferredLanguage" /></td>
										<td class="columndata" width="180" valign="baseline">
											<ffi:getProperty name="LanguageResource" property="Resource"/>
											<span class="sectionhead_greyDA">
												<br>
												<ffi:setProperty name="LanguageResource" property="ResourceID" value="${oldDAObject.PreferredLanguage}" />
												<ffi:getProperty name='LanguageResource' property='Resource'/>
											</span>
										</td>									
								</ffi:cinclude>
								<td class='<ffi:getPendingStyle fieldname="userName" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Username" /></td>
								<td class="columndata" width="200" valign="baseline">
									<ffi:getProperty name="BusinessEmployee" property="UserName"/>
									<ffi:cinclude value1="${oldDAObject.UserName}" value2="" operator="notEquals">
									<span class="sectionhead_greyDA">
										<br>
										<ffi:getProperty name="oldDAObject" property="UserName"/>
									</span>
									</ffi:cinclude>
								</td>
							</tr>
								<ffi:removeProperty name="GetLanguageList" />
								<ffi:removeProperty name="LanguageResource" />
							<tr>
								<td class='<ffi:getPendingStyle fieldname="phone" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Phone" /></td>
								<td class="columndata" width="180" valign="baseline">
									<ffi:getProperty name="BusinessEmployee" property="Phone"/>
									<ffi:cinclude value1="${oldDAObject.Phone}" value2="" operator="notEquals">
										<span class="sectionhead_greyDA">
											<br>
											<ffi:getProperty name="oldDAObject" property="Phone"/>
										</span>
									</ffi:cinclude>
								</td>
								<td class='<ffi:getPendingStyle fieldname="password" defaultcss="sectionhead"  dacss="sectionheadDA"/>' align="right" valign="baseline">Password</td>
									<td class="columndata" width="200" valign="baseline">
										******												
								</td>	
								
							</tr>
							<tr>
								<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
									<td class='<ffi:getPendingStyle fieldname="faxPhone" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Fax" /></td>
									<td class="columndata" width="180" valign="baseline">
										<ffi:getProperty name="BusinessEmployee" property="FaxPhone"/>
										<ffi:cinclude value1="${oldDAObject.FaxPhone}" value2="" operator="notEquals">
										<span class="sectionhead_greyDA">
											<br>
											<ffi:getProperty name="oldDAObject" property="FaxPhone"/>
										</span>
										</ffi:cinclude>
									</td>
								</ffi:cinclude>

								<ffi:cinclude value1="${isUserDeleted}" value2="true">
									<td class="sectionheadDA" align="right" valign="baseline"><s:text name="jsp.user_User_Status" /></td>
									<td class="columndata" width="200" align="left" valign="baseline">
										<s:text name="jsp.user_Deleted"/>
										<span class="sectionhead_greyDA">
										<br>
											<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%=String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE)%>"><s:text name="jsp.user_Active"/></ffi:cinclude>
											<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%=String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE)%>"><s:text name="jsp.user_Inctive"/></ffi:cinclude>
											<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%=String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE)%>"><s:text name="jsp.user_Temp_Inactive"/></ffi:cinclude>
											<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%=String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL)%>"><s:text name="jsp.user_Pending_Approval"/></ffi:cinclude>
										</span>
									</td>
								</ffi:cinclude>
								<ffi:cinclude value1="${isUserDeleted}" value2="true" operator="notEquals">
									<td class='<ffi:getPendingStyle fieldname="accountStatus" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_User_Status" /></td>
									<td class="columndata" width="200" align="left" valign="baseline">
										<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="" operator="notEquals">
											<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.AccountStatus}" />
											<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE ) %>"/>
											<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Active" /></ffi:cinclude>
											<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
											<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Inactive" /></ffi:cinclude>
											<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE ) %>"/>
											<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Temp_Inactive" /></ffi:cinclude>
											<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL ) %>"/>
											<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Pending_Approval" /></ffi:cinclude>
										</ffi:cinclude>

										<ffi:cinclude value1="${oldDAObject.AccountStatus}" value2="" operator="notEquals">
											<span class="sectionhead_greyDA">
											<br>
												<ffi:setProperty name="Compare" property="Value1" value="${oldDAObject.AccountStatus}" />
												<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE ) %>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Active" /></ffi:cinclude>
												<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Inactive" /></ffi:cinclude>
												<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE ) %>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Temp_Inactive" /></ffi:cinclude>
												<ffi:setProperty name="Compare" property="value2" value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL ) %>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_Pending_Approval" /></ffi:cinclude>
											</span>
										</ffi:cinclude>
									</td>
								</ffi:cinclude>
							</tr>		
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td class='<ffi:getPendingStyle fieldname="entitlementGroupId" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>' align="right" valign="baseline"><s:text name="jsp.user_Group" /></td>
								<td class="columndata" width="200" valign="baseline">
									<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
   											<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
   											<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value="NewEntitlementGroup" />
									<ffi:process name="GetEntitlementGroup"/>												
									<ffi:getProperty name="NewEntitlementGroup" property="GroupName"/>
									<span class="sectionhead_greyDA">
										<br>
                                        <ffi:cinclude value1="${oldDAObject.EntitlementGroupId}" value2="" operator="notEquals">
                                            <ffi:cinclude value1="${oldDAObject.EntitlementGroupId}" value2="0" operator="notEquals">
                                                <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${oldDAObject.EntitlementGroupId}" />
                                                <ffi:process name="GetEntitlementGroup"/>
                                                <ffi:getProperty name="NewEntitlementGroup" property="GroupName"/>
                                            </ffi:cinclude>
                                        </ffi:cinclude>
									</span>
                                    <ffi:removeProperty name="NewEntitlementGroup"/>
								</td>							
							</tr>									
							<tr>
								Retrieve the list of admins on this group
								<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
								<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}"/>
								<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="GroupAdmins"/>
								<ffi:process name="GetAdminsForGroup"/>
								<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="request"/>
                                          <ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="BusinessEmployeesSessionName" value="GroupAdmins" />
                                      <ffi:process name="GetBusinessEmployeesByEntGroups"/>

                                      <ffi:setProperty name="GroupAdmins" property="Filter" value="ID!${BusinessEmployee.Id}"/>
								<td class="sectionsubhead" align="right">
                                          <ffi:cinclude value1="${GroupAdmins.Size}" value2="0" operator="notEquals">
                                              <div align="right"><span class='<ffi:getPendingStyle fieldname="primaryAdmin" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>'><s:text name="jsp.user_Primary_Admin" /></span></div>
                                          </ffi:cinclude>
                                      </td>
								<td class="columndata">
                                          <ffi:cinclude value1="${GroupAdmins.Size}" value2="0" operator="notEquals">
                                              <ffi:list collection="GroupAdmins" items="employee">
                                                  <ffi:cinclude value1="${employee.Id}" value2="${BusinessEmployee.PrimaryAdmin}">
                                                      <ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
                                                  </ffi:cinclude>
                                              </ffi:list>
                                          </ffi:cinclude>
                                          <span class="sectionhead_greyDA">
                                              <br>
										<ffi:list collection="GroupAdmins" items="employee">
											<ffi:cinclude value1="${oldDAObject.PrimaryAdmin}" value2="${employee.Id}">
											 	<ffi:getProperty name="employee" property="firstName"/> <ffi:getProperty name="employee" property="lastName"/>
											</ffi:cinclude>
										</ffi:list>
									</span>
                                      </td>
								<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
									<td class="sectionsubhead" align="right" valign="baseline"><s:text name="jsp.user_Lockout_Status" /></td>
									<td class="columndata" width="200" valign="baseline">
										<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="equals">
											<s:text name="jsp.user_Expired" />
										</ffi:cinclude>
										<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="equals">
											<s:text name="jsp.user_Locked" />
										</ffi:cinclude>
										<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="equals">
											<s:text name="jsp.user_Locked" />
										</ffi:cinclude>
										<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="notEquals">
											<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="notEquals">
												<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="notEquals">
												<s:text name="jsp.user_Unlocked" />
												</ffi:cinclude>
											</ffi:cinclude>
										</ffi:cinclude>
									</td>
								</ffi:cinclude>
                                
								<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
									<td class="sectionsubhead" align="right" valign="baseline"></td>
									<td class="columndata" width="200" valign="baseline">
									</td>
								</ffi:cinclude>										
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td class="columndata" colspan="6" nowrap align="center">
								<form method="post">   <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/><sj:a button="true" title="Close"  onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a></form>
								</td>
							</tr>							
						</table>
					</div>				
			</td>
			<td>&nbsp;</td>
		</tr>		
	</table> --%>
	<table>
		<tr>
			<td height="40" colspan="4">&nbsp;</td>
		</tr>
		<tr>
			<td class="ui-widget-header customDialogFooter" colspan="6" nowrap align="center">
			<form method="post">   <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/><sj:a button="true" title="Close"  onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a></form>
			</td>
		</tr>
	</table>
</div>
	
<ffi:removeProperty name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>
<ffi:removeProperty name="GetStateProvinceDefnForState"/>
<ffi:removeProperty name="GroupAdmins"/>
<ffi:removeProperty name="isUserDeleted"/>