<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:action namespace="/pages/common" name="QuickBankLookupAction_quickBankSearch" executeResult="true"/>

<%-- <sj:dialog id="searchDestinationBankDialogID"
	title="%{getText('jsp.billpay_93')}" modal="true"
	autoOpen="false" closeOnEscape="true" showEffect="fold"
	hideEffect="clip" width="600" height="480">
</sj:dialog>

 --%>