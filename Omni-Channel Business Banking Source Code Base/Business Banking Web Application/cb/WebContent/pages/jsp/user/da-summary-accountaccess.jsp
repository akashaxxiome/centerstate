<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.tasks.dualapproval.IDACategoryConstants" %>
<ffi:setL10NProperty name='PageHeading' value='Pending Approval Summary'/>

<%
if(request.getParameter("Section") != null)
	session.setAttribute("Section", request.getParameter("Section"));
if(request.getParameter("ParentMenu") != null)
	session.setAttribute("ParentMenu", request.getParameter("ParentMenu"));
if(request.getParameter("PermissionsWizard") != null)
	session.setAttribute("PermissionsWizard", request.getParameter("PermissionsWizard"));
if(request.getParameter("CurrentWizardPage") != null)
	session.setAttribute("CurrentWizardPage", request.getParameter("CurrentWizardPage"));
if(request.getParameter("SortKey") != null)
	session.setAttribute("SortKey", request.getParameter("SortKey"));
%>

<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<ffi:include page="${PathExt}inc/init/admin-init.jsp" />
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users'}">
	<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
	<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
	<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
	<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
	<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployeesDA"/>
	<ffi:process name="GetBusinessEmployees"/>
	<ffi:removeProperty name="GetBusinessEmployees"/>
	<ffi:removeProperty name="SearchBusinessEmployee"/>
	
	<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployeesDA" />
	
	<ffi:process name="SetBusinessEmployee"/>
	<ffi:setProperty name="EditGroup_GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
</s:if>
<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
	<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
	<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
	<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
	<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
	<ffi:setProperty name="GetBusinessEmployees" property="SearchEntitlementProfiles" value="TRUE" />
	<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployeesDA"/>
	
	<ffi:process name="GetBusinessEmployees"/>
	<ffi:removeProperty name="GetBusinessEmployees"/>
	<ffi:removeProperty name="SearchBusinessEmployee"/>
	
	<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployeesDA" />
	<ffi:process name="SetBusinessEmployee"/>
	<ffi:setProperty name="EditGroup_GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
</ffi:cinclude>

<%-- Get the entitlementGroup, which the permission or its member's permission is currently being edited --%>
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="GetEntitlementGroup"/>


<ffi:setProperty name="currentPage" value="<%=IDACategoryConstants.IS_ACCOUNT_ACCESS_CHANGED  %>" />

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="${itemType}"/>
<ffi:process name="GetDAItem"/>

<ffi:object id="SubmittedUser" name="com.ffusion.tasks.user.GetUserById"/>
<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="equals">
   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.createdBy}" />
</ffi:cinclude>
<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="notEquals">
   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.modifiedBy}" />
</ffi:cinclude>
<ffi:setProperty name="SubmittedUser" property="userSessionName" value="SubmittedUserName"/>
<ffi:process name="SubmittedUser"/>

<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetCategories" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetCategories" property="itemType" value="${itemType}"/>	
	<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetCategories" property="CategorySubType" value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS %>"/>
<ffi:process name="GetCategories"/>

<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
	<ffi:object id="GetAccountsForGroup" name="com.ffusion.tasks.admin.GetAccountsForGroup" scope="session"/>
	<ffi:setProperty name="GetAccountsForGroup" property="GroupID" value="${EditGroup_GroupId}"/>
	<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="AccountAccessList"/>
	<ffi:setProperty name="GetAccountsForGroup" property="CheckAccountGroupAccess" value="false"/>
	<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="true"/>
	<ffi:process name="GetAccountsForGroup"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
	<ffi:object id="GetAccountsForGroup" name="com.ffusion.tasks.admin.GetAccountsForGroup" scope="session"/>
	<ffi:setProperty name="GetAccountsForGroup" property="GroupID" value="${Entitlement_EntitlementGroup.ParentId}"/>
	<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="AccountAccessList"/>
	<ffi:setProperty name="GetAccountsForGroup" property="CheckAccountGroupAccess" value="false"/>
	<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="true"/>
	<ffi:process name="GetAccountsForGroup"/>
</s:if>
<ffi:removeProperty name="GetAccountsForGroup"/>



<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >
<input type="hidden" name="userAction" value="<ffi:getProperty name='userAction'/>" >

<ffi:cinclude value1="${tempSuccessUrl}" value2="">
	<ffi:setProperty name="tempSuccessUrl" value="${SuccessUrl}"/>
</ffi:cinclude>

<ffi:cinclude value1="${SuccessUrl}" value2="">
	<ffi:setProperty name='SuccessUrl' value="${tempSuccessUrl}"/>
</ffi:cinclude>

<div align="center">
	<span id="formError"></span>
	<s:if test="%{#session.Section == 'Users'}">
		<ffi:setProperty name="subMenuSelected" value="users"/>
	</s:if>
	<ffi:cinclude value1="${Section}" value2="Profiles">
		<ffi:setProperty name="subMenuSelected" value="users"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company">
		<ffi:setProperty name="subMenuSelected" value="company"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Division">
		<ffi:setProperty name="subMenuSelected" value="divisions"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Group">
		<ffi:setProperty name="subMenuSelected" value="groups"/>
	</ffi:cinclude>

	<ffi:setProperty name="isPendingIsland" value="TRUE"/>
	<s:include value="/pages/jsp/user/inc/nav_header.jsp" />

	<ffi:removeProperty name="isPendingIsland"/>
		<div class="blockWrapper">
	<div  class="blockHead tableData">
		<span><strong><ffi:cinclude value1="${rejectFlag}" value2="y"  operator="notEquals">
			<!--L10NStart-->Verify Pending Changes for Approval<!--L10NEnd-->
		</ffi:cinclude>
		<ffi:cinclude value1="${rejectFlag}" value2="y" >
			<!--L10NStart-->Verify Pending Changes for Rejection<!--L10NEnd-->
		</ffi:cinclude></strong></span>
	</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 33%">
				<span class="sectionsubhead"  >
				 	<s:if test="%{#session.Section == 'Users'}">
						<!--L10NStart-->User<!--L10NEnd-->:
                   </s:if>
                   <ffi:cinclude value1="${Section}" value2="Profiles">
						<!--L10NStart-->Profiles<!--L10NEnd-->:
                   </ffi:cinclude>
                   <ffi:cinclude value1="${Section}" value2="Company">
                       <!--L10NStart-->Company<!--L10NEnd-->:
                   </ffi:cinclude>
                   <ffi:cinclude value1="${Section}" value2="Division">
                       <!--L10NStart-->Division<!--L10NEnd-->:
                   </ffi:cinclude>
                   <ffi:cinclude value1="${Section}" value2="Group">
                       <!--L10NStart-->Group<!--L10NEnd-->:
                   </ffi:cinclude>
              </span>
              <span class="sectionsubhead adminBackground"    >
                   <s:if test="%{#session.Section == 'Users'}">
						<ffi:getProperty name="userName"/>
                   </s:if>
                   <ffi:cinclude value1="${Section}" value2="Profiles">
						<ffi:getProperty name="userName"/>
                   </ffi:cinclude>
                   <s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
                       <ffi:cinclude value1="${Section}" value2="Company">
                           <ffi:getProperty name="Business" property="BusinessName"/>
                       </ffi:cinclude>
                       <ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
                           <ffi:object name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" id="GetEntitlementGroup" scope="session"/>
                           <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${DAItem.itemId}"/>
                           <ffi:process name="GetEntitlementGroup"/>
                           <ffi:setProperty name="groupName" value="${Entitlement_EntitlementGroup.GroupName}"/>
                           <ffi:getProperty name="groupName"/>
                       </ffi:cinclude>
                   </s:if>
					</span>
			</div>
			<div class="inlineBlock" style="width: 33%">
				<span class="sectionsubhead"  >
					<!--L10NStart-->Submitted By:<!--L10NEnd-->
				</span>
				<span class="sectionsubhead adminBackground"   >
					<ffi:getProperty name="SubmittedUserName" property="userName"/>
				</span>
			</div>
			<div class="inlineBlock"  style="width: 33%">
				<span class="sectionsubhead"  >
					<!--L10NStart-->Submitted On:<!--L10NEnd-->
				</span>
				<span class="sectionsubhead adminBackground"   >
					<ffi:getProperty name="DAItem" property="formattedSubmittedDate"/>
				</span>
		</div>
	</div>
</div></div>
<div class="marginTop10"></div>
<div class="paneWrapper approvalPermHeight">
  	<div class="paneInnerWrapper">
							<table width="750" border="0" cellspacing="0" cellpadding="3" class="tableData">
								<%-- <tr>
									<td width="110">&nbsp;</td>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110" >
                                        <s:if test="%{#session.Section == 'Users'}">
    										<!--L10NStart-->User<!--L10NEnd-->
                                        </s:if>
                                        <ffi:cinclude value1="${Section}" value2="Profiles">
    										<!--L10NStart-->Profiles<!--L10NEnd-->
                                        </ffi:cinclude>
                                        <ffi:cinclude value1="${Section}" value2="Company">
                                            <!--L10NStart-->Company<!--L10NEnd-->
                                        </ffi:cinclude>
                                        <ffi:cinclude value1="${Section}" value2="Division">
                                            <!--L10NStart-->Division<!--L10NEnd-->
                                        </ffi:cinclude>
                                        <ffi:cinclude value1="${Section}" value2="Group">
                                            <!--L10NStart-->Group<!--L10NEnd-->
                                        </ffi:cinclude>
									</td>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="108" >
										<!--L10NStart-->Submitted By<!--L10NEnd-->
									</td>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" width="110" >
										<!--L10NStart-->Submitted On<!--L10NEnd-->
									</td>
									<td width="110">&nbsp;</td>
									<td width="110">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="1">&nbsp;</td>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" colspan="1"  >
                                        <s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
    										<ffi:getProperty name="userName"/>
                                        </s:if>
                                        <s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
                                            <ffi:cinclude value1="${Section}" value2="Company">
                                                <ffi:getProperty name="Business" property="BusinessName"/>
                                            </ffi:cinclude>
                                            <ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
                                                <ffi:object name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" id="GetEntitlementGroup" scope="session"/>
                                                <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${DAItem.itemId}"/>
                                                <ffi:process name="GetEntitlementGroup"/>
                                                <ffi:setProperty name="groupName" value="${Entitlement_EntitlementGroup.GroupName}"/>
                                                <ffi:getProperty name="groupName"/>
                                            </ffi:cinclude>
                                        </s:if>
									</td>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17"  colspan="1">
										<ffi:getProperty name="SubmittedUserName" property="userName"/>
									</td>
									<td class="sectionsubhead adminBackground" valign="baseline" align="left" height="17" colspan="3" >
										<ffi:getProperty name="DAItem" property="formattedSubmittedDate"/>
									</td>
								</tr> --%>
								<s:include value="/pages/jsp/user/inc/da-summary-header-permission.jsp"/>
								
								<ffi:object id="GetEntitlmentDisplayName" name="com.ffusion.tasks.dualapproval.GetEntitlmentDisplayName"/>
								<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
								<ffi:list collection="categories" items="category"> 
									<ffi:flush/>
									<ffi:cinclude value1="${category.categoryType}" value2="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>">
										<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="${category.categoryType}"/>	
										<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${category.categorySubType}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${category.ObjectId}" />
										<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="${category.ObjectType}" />
										<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="${itemType}"/>	
										<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
										<ffi:process name="GetDACategoryDetails" />
				
										<ffi:object id="HandleAccountAccessRowDisplayDA" name="com.ffusion.tasks.dualapproval.HandleAccountAccessRowDisplayDA"/>
										<ffi:setProperty name="HandleAccountAccessRowDisplayDA" property="AccountsName" value="AccountAccessList"/>
										<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
									    <ffi:setProperty name="HandleAccountAccessRowDisplayDA" property="GroupId" value="${EditGroup_GroupId}"/>
										</s:if>
										<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
										    <ffi:setProperty name="HandleAccountAccessRowDisplayDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
										    <ffi:setProperty name="HandleAccountAccessRowDisplayDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
										    <ffi:setProperty name="HandleAccountAccessRowDisplayDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
										    <ffi:setProperty name="HandleAccountAccessRowDisplayDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
										</s:if>
										<ffi:process name="HandleAccountAccessRowDisplayDA"/>
										<ffi:list collection="CATEGORY_BEAN.daItems" items="entitlement">
											<ffi:flush/>
											<tr>
											<td class="columndata"><ffi:flush/><ffi:getL10NString rsrcFile="cb" msgKey="da.field.${category.categorySubTypeFormatted}" /></td>
								 				<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
											 	<ffi:process name="GetEntitlmentDisplayName" />
											 	<td class="columndata"> 
												<ffi:flush/><ffi:getProperty name="entitlementDisplayName" /> 
											</td>
											<td class="columndata">
												<ffi:flush/><ffi:getProperty name="CATEGORY_BEAN" property="objectType" />				
											</td>
											<td class="columndata">
												<ffi:flush/><ffi:getProperty name="entitlement" property="formattedFieldName" />	
											</td>
											<td class="columndata">
												<ffi:cinclude value1="${entitlement.oldValue}" value2="true">
													<ffi:flush/><!--L10NStart-->Grant<!--L10NEnd-->				
												</ffi:cinclude>
												<ffi:cinclude value1="${entitlement.oldValue}" value2="false" >
													<ffi:flush/><!--L10NStart-->Revoke<!--L10NEnd-->						
												</ffi:cinclude>
											</td>
											<td class="columndata sectionheadDA">
												<ffi:cinclude value1="${entitlement.newValue}" value2="true">
													<ffi:flush/><!--L10NStart-->Grant<!--L10NEnd-->				
												</ffi:cinclude>
												<ffi:cinclude value1="${entitlement.newValue}" value2="false" >
													<ffi:flush/><!--L10NStart-->Revoke<!--L10NEnd-->						
												</ffi:cinclude>
											</td>
											<td class="columndata"><ffi:flush/>
												<ffi:getProperty name="entitlement"	property="userAction" />
											</td>
										</tr>
										<ffi:cinclude value1="${entitlement.error}" value2="" operator="notEquals">
											<ffi:setProperty name="DISABLE_APPROVE" value="true"/>
											<tr>
												<td class="columndataDANote" colspan="7">
													<ffi:flush/><ffi:getProperty name="entitlement" property="error"/> 
												</td>
											</tr>
										</ffi:cinclude>
									</ffi:list>
								</ffi:cinclude>
							</ffi:list>

							<ffi:list collection="categories" items="category">
							<ffi:cinclude value1="${category.categoryType}" value2="<%=IDualApprovalConstants.CATEGORY_LIMIT %>"> 
								<ffi:setProperty name="GetDACategoryDetails" property="categorySubType" value="${category.categorySubType}"/>
								<ffi:setProperty name="GetDACategoryDetails" property="ObjectId" value="${category.ObjectId}" />
								<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="${category.categoryType}"/>	
								<ffi:setProperty name="GetDACategoryDetails" property="ObjectType" value="${category.ObjectType}" />
								<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
								<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="${itemType}"/>	
								<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
								
								<ffi:process name="GetDACategoryDetails" />
								<s:include value="/pages/jsp/user/inc/da-verify-user-accountaccess-limits.jsp"/>
	
								<ffi:list collection="CATEGORY_BEAN.daItems" items="limit">
									<ffi:flush/>
									<tr>
									<td class="columndata"><ffi:flush/>
										<ffi:getL10NString rsrcFile="cb" msgKey="da.field.${category.categorySubTypeFormatted}" /></td>
		 								<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" 
		 												value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
		 								<ffi:setProperty name="displayOperationName" value="${limit.formattedOperationName}"/>
		 								<ffi:setProperty name="limit" property="operationName" 
		 												value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCESS %>"/>
										<ffi:process name="GetEntitlmentDisplayName"/>
									<td class="columndata"><ffi:flush/><ffi:getProperty name="entitlementDisplayName" /></td>
									<td class="columndata"><ffi:flush/><ffi:getProperty name="CATEGORY_BEAN" property="objectType" /></td>
									<td class="columndata"><ffi:flush/><ffi:getProperty name="displayOperationName" /></td>
									<td class="columndata">
										<ffi:cinclude value1="${limit.oldData}" value2="" operator="notEquals">
											<ffi:setProperty name="CurrencyObject" property="Amount" value="${limit.oldData}"/>
											<ffi:flush/><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
										</ffi:cinclude>
									</td>
									<td class="columndata sectionheadDA">
										<ffi:cinclude value1="${limit.newData}" value2="" operator="notEquals">
											<ffi:setProperty name="CurrencyObject" property="Amount" value="${limit.newData}"/>
											<ffi:flush/><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
										</ffi:cinclude>
									</td>
									<td class="columndata">
										<ffi:cinclude value1="${limit.allowNewApproval}" value2="N">
											<ffi:flush/><!--L10NStart-->Yes<!--L10NEnd-->
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.allowNewApproval}" value2="Y">
											<ffi:flush/><!--L10NStart-->No<!--L10NEnd-->
										</ffi:cinclude>
									</td>
									<td class="columndata sectionheadDA">
										<ffi:cinclude value1="${limit.allowNewApproval}" value2="Y">
											<ffi:flush/><!--L10NStart-->Yes<!--L10NEnd-->
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.allowNewApproval}" value2="N">
											<ffi:flush/><!--L10NStart-->No<!--L10NEnd-->
										</ffi:cinclude>
									</td>
									<td class="columndata">
										<ffi:cinclude value1="${limit.period}" value2="1">
											<ffi:flush/><!--L10NStart-->per transaction<!--L10NEnd-->				
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.period}" value2="2">
											<ffi:flush/><!--L10NStart-->per day<!--L10NEnd-->				
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.period}" value2="3">
											<ffi:flush/><!--L10NStart-->per week<!--L10NEnd-->				
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.period}" value2="4">
											<ffi:flush/><!--L10NStart-->per month<!--L10NEnd-->				
										</ffi:cinclude>
									</td>
								</tr>
								<ffi:cinclude value1="${limit.error}" value2="" operator="notEquals">
									<ffi:setProperty name="DISABLE_APPROVE" value="true"/>
									<tr>
										<td class="columndataDANote" colspan="8"><ffi:flush/><ffi:getProperty name="limit" property="error" /></td>
									</tr>
								</ffi:cinclude>
								<ffi:flush/>
							</ffi:list>
						</ffi:cinclude>
					</ffi:list>
				</table></div></div>
		<s:include value="/pages/jsp/user/inc/da-summary-footer-permission.jsp"/>
		<ffi:setProperty name="url" value="${SecurePath}user/da-summary-accountaccess.jsp?PermissionsWizard=TRUE"  URLEncrypt="true" />
		<ffi:setProperty name="BackURL" value="${url}"/>
	</div>	
<ffi:removeProperty name="AccountAccessList"/>
