<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants "%>
<%@ page import="com.ffusion.tasks.dualapproval.IDACategoryConstants" %>

<ffi:setL10NProperty name='PageHeading' value='Pending Approval Summary'/>

<%
if(request.getParameter("Section") != null)
	session.setAttribute("Section", request.getParameter("Section"));
if(request.getParameter("ParentMenu") != null)
	session.setAttribute("ParentMenu", request.getParameter("ParentMenu"));
if(request.getParameter("PermissionsWizard") != null)
	session.setAttribute("PermissionsWizard", request.getParameter("PermissionsWizard"));
if(request.getParameter("CurrentWizardPage") != null)
	session.setAttribute("CurrentWizardPage", request.getParameter("CurrentWizardPage"));
if(request.getParameter("SortKey") != null)
	session.setAttribute("SortKey", request.getParameter("SortKey"));
%>

<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<ffi:include page="${PathExt}inc/init/admin-init.jsp" />
</ffi:cinclude>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="Validate" value="itemId,ItemType" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE %>"/>
<ffi:process name="GetDAItem"/>
<ffi:removeProperty name="GetDAItem"/>

<ffi:object id="SubmittedUser" name="com.ffusion.tasks.user.GetUserById"/>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="equals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.createdBy}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="notEquals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.modifiedBy}" />
	</ffi:cinclude>
	<ffi:setProperty name="SubmittedUser" property="userSessionName" value="SubmittedUserName"/>
<ffi:process name="SubmittedUser"/>

<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
<ffi:setProperty name="GetBusinessEmployees" property="SearchEntitlementProfiles" value="TRUE" />
<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployeesDA"/>
<ffi:process name="GetBusinessEmployees"/>
<ffi:removeProperty name="GetBusinessEmployees"/>
<ffi:removeProperty name="SearchBusinessEmployee"/>

<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployeesDA" />
<ffi:process name="SetBusinessEmployee"/>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}" />
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ENTITLEMENT_PROFILE%>" />
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}" />
	<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>" />
<ffi:process name="GetDACategoryDetails" />

<ffi:object id="OrderDACategoryDetails" name="com.ffusion.tasks.dualapproval.OrderDACategoryDetails" />
	<ffi:setProperty name="OrderDACategoryDetails" property="Order" value="userName,entitlementGroupId" />
<ffi:process name="OrderDACategoryDetails" />

<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="CountryResource" />
	
<ffi:object id="LanguageResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
<ffi:setProperty name="LanguageResource" property="ResourceFilename" value="com.ffusion.utilresources.languages" />
<ffi:process name="LanguageResource" />	
	
<ffi:object name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" id="GetAdminsForGroup" scope="request"/>
	
<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="request"/>
<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="BusinessEmployeesSessionName" value="GroupAdmins" />

<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="request"/>

<script type="text/javascript">
function reject()
{
	document.frmDualApproval.action = "rejectchangesda.jsp";
	document.frmDualApproval.submit();
}

// Handle text area maxlength
$(document).ready(function(){
	ns.common.handleTextAreaMaxlength("#RejectReason");
});
</script>

<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >
<input type="hidden" name="userAction" value="<ffi:getProperty name='userAction'/>" >

<ffi:cinclude value1="${tempSuccessUrl}" value2="">
	<ffi:setProperty name="tempSuccessUrl" value="${SuccessUrl}"/>
</ffi:cinclude>

<ffi:cinclude value1="${SuccessUrl}" value2="">
	<ffi:setProperty name='SuccessUrl' value="${tempSuccessUrl}"/>
</ffi:cinclude>

<div align="center" class="approvalDialogHt">
	<span id="formError"></span>
	<ffi:setProperty name="subMenuSelected" value="users"/>
	<%-- include page header --%>
	<ffi:setProperty name="isPendingIsland" value="TRUE"/>
	<%-- <s:include value="/pages/jsp/user/inc/nav_header.jsp" /> --%>
	<ffi:removeProperty name="isPendingIsland"/>
	<table width="750" border="0" cellspacing="0" cellpadding="0" class="marginTop10">
		<tr>
			<td align="center" class="adminBackground">
				<table width="708" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td align="left" class="blockHead" width="750">
							<ffi:cinclude value1="${rejectFlag}" value2="y"  operator="notEquals">
								<s:text name="jsp.da_approvals_pending_user_verify_approvalchanges" />
							</ffi:cinclude>
							<ffi:cinclude value1="${rejectFlag}" value2="y" >
								<s:text name="jsp.da_approvals_pending_user_verify_rejectchanges" />
							</ffi:cinclude>
						</td>
					</tr>
					<tr>
						<td>	
							<div class="paneWrapper">
						  		<div class="paneInnerWrapper">
						  		<div class="header">
								<table width="750" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td  valign="baseline" align="left" height="17" width="250" >
											<s:text name="jsp.da_approvals_pending_user_User_column" />
										</td>
										<td  valign="baseline" align="left" height="17" width="250" >
											<s:text name="jsp.da_approvals_pending_user_SubmittedBy_column" />
										</td>
										<td  valign="baseline" align="left" height="17" width="250" >
											<s:text name="jsp.da_approvals_pending_user_SubmittedOn_column" />
										</td>
									</tr>
								</table>
								</div>
								<table width="750" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td  width="250"  class="sectionhead adminBackground" valign="baseline" align="left" height="17" colspan="1"  >
											<ffi:getProperty name="userName"/>
										</td>
										<td  width="250"  class="sectionhead adminBackground" valign="baseline" align="left" height="17"  colspan="1">
											<ffi:getProperty name="SubmittedUserName" property="userName"/>
										</td>
										<td  width="250"  class="sectionhead adminBackground" valign="baseline" align="left" height="17" colspan="3" >
											<ffi:getProperty name="DAItem" property="formattedSubmittedDate"/>
										</td>
									</tr>
								</table>
								</div>
							</div>
								<br>
								<table width="750" border="0" cellspacing="0" cellpadding="3">
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
										
										<tr>
											<td align="left" class="blockHead"><s:text name="user.profiles.summary" /></td>
										</tr>
									</table>
									<div class="paneWrapper">
								  		<div class="paneInnerWrapper">
								  		<div class="header">
										<table width="750" border="0" cellspacing="0" cellpadding="0">	
										<tr>
											<td align="left" height="17" width="250"><s:text name="jsp.da_approvals_pending_user_FieldName_column" /></td>
											<td align="left" height="17" width="250"><s:text name="jsp.da_approvals_pending_user_OldValue_column" /></td>
											<td align="left" height="17" width="250"><s:text name="jsp.da_approvals_pending_user_NewValue_column" /></td>
										</tr>
										</table>
									</div>
									<table width="750" border="0" cellspacing="0" cellpadding="3">
									</ffi:cinclude>
									<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
										<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId">
											<ffi:setProperty name="oldEntitlementGroupId" value="${details.oldValue}"/>
											<ffi:cinclude value1="${oldEntitlementGroupId}" value2="0">
												<ffi:setProperty name="oldEntitlementGroupId" value=""/>
											</ffi:cinclude>
											<ffi:setProperty name="newEntitlementGroupId" value="${details.newValue}"/>
										</ffi:cinclude>
									</ffi:list>
									<ffi:list collection="CATEGORY_BEAN.daItems" items="details">
										<ffi:cinclude value1="${details.fieldName}" value2="confirmPassword" operator="notEquals">
										<tr>
											<td align="left" width="250"  class="columndata"><ffi:getL10NString rsrcFile="cb"
												msgKey="da.field.${details.fieldName}" /></td>
											<td  width="250"  align="left" colspan="1" class="columndata">
												<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId">
													<ffi:cinclude value1="${oldEntitlementGroupId}" value2="" operator="notEquals">
														<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${oldEntitlementGroupId}"/>
														<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value="OldGroup"/>
														<ffi:process name="GetEntitlementGroup"/>
														<ffi:getProperty name="OldGroup" property="GroupName"/>
													</ffi:cinclude>
												</ffi:cinclude>
													<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId" operator="notEquals">
														<ffi:getProperty name="details" property="oldValue" />
													</ffi:cinclude>
											</td>
											<td  width="250"  align="left" colspan="4" class="columndata">
												<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId">
													<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${newEntitlementGroupId}"/>
													<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value="NewGroup"/>
													<ffi:process name="GetEntitlementGroup"/>
													<ffi:getProperty name="NewGroup" property="GroupName"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${details.fieldName}" value2="entitlementGroupId" operator="notEquals">
													<ffi:getProperty name="details" property="newValue" />
												</ffi:cinclude>
											</td>
										</tr>
										</ffi:cinclude>
									</ffi:list>
								</table>
								</div>
							</div>
							</td>
						</tr>
						<tr><td align="left" >&nbsp;</td></tr>
						<ffi:setProperty name="entitlementTypeOrder" property="Filter" value="EnableMenu=true" />
						<ffi:object id="DAWizardUtil" name="com.ffusion.tasks.dualapproval.DAWizardUtil" />
						<ffi:setProperty name="DAWizardUtil" property="currentPage" value="<%=IDACategoryConstants.IS_PROFILE_CHANGED  %>" />
						<ffi:process name="DAWizardUtil" />
						<tr><td align="left" colspan="7">&nbsp;</td></tr>
						<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>">
							<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
							
								<tr>
									<td align="left" width="100%" >
									<table width="100%" >
										<tr>
											<td width="15%" class="columndata" align="center">
												<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/da-user-verify.jsp-2" parm0="${userName}"/>
											</td>
										</tr>
									</table>
									</td>
								</tr>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${rejectFlag}" value2="y" >
						<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
							<tr>
								<td align="left" width="100%" >
							<ffi:object id="RejectPendingChange" name="com.ffusion.tasks.dualapproval.RejectPendingChange"  />
							<ffi:setProperty name="RejectPendingChange" property="validate" value="REJECTREASON" />
								<% 
									session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("RejectPendingChange")); 
								%>
								<s:form id="rejectDualApprovalFormID2" namespace="/pages/dualapproval" validate="false" action="dualApprovalAction-rejectDAChanges" method="post" name="RejectDualApprovalForm" theme="simple">
	                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	           		       	<input type="hidden" name="CommonDualApprovalTask.ItemId" value="<ffi:getProperty name='itemId'/>"/>
							<input type="hidden" name="CommonDualApprovalTask.ItemType" value="<ffi:getProperty name='itemType'/>"/>
							<input type="hidden" name="module" value="User"/>
							<input type="hidden" name="daAction" value="Rejected"/>
								<table width="100%" >
									<tr>
										<td align="left" width="15%" class="sectionhead adminBackground" ><s:text name="jsp.da_approvals_pending_user_reject_reason" /><span class="required">*</span></td> 
									</tr>
									<tr>	
										<td align="left" >
										<textarea align="left" id="RejectReason" name="REJECTREASON" rows="3" cols="80" maxlength="120"></textarea>
										<span align="left" id="RejectFailureError"></span>
										</td>
									</tr>
									<tr>
										<td align="left"><span class="required"><s:text name="jsp.da_approvals_pending_user_require_field" /></span></td>
									</tr>
								</table>
								</s:form>
								</td>
							</tr>
							</ffi:cinclude>
						</ffi:cinclude>
				
						<tr><td align="center" class="ui-widget-header customDialogFooter">
							<sj:a 
								id="cancelWizardID2"
								button="true" 
								onclick="cancelWizard();" title="%{getText('jsp.default_83')}"><s:text name="jsp.default_82" /></sj:a>
							<ffi:cinclude value1="${previouspage}" value2="" operator="notEquals">
									<sj:a 
										id="previousPageID2"
										button="true" 
										onclick="previousPage();"
									><s:text name="jsp.default_57" /></sj:a>
							</ffi:cinclude>
							<ffi:cinclude value1="${nextpage}" value2="" operator="notEquals">
									<sj:a 
										id="nextPageID2"
										button="true" 
										onclick="nextPage();"
									><s:text name="jsp.default_111" /></sj:a>
							</ffi:cinclude>
						
							<ffi:cinclude value1="${rejectFlag}" value2="y" operator="notEquals" >
								<ffi:cinclude value1="${nextpage}" value2="" operator="equals">
									<s:url id="approveURL" value="/pages/user/approveBusinessEntitlementProfileChanges.action" escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
										<s:param name="ItemId" value="%{#session.itemId}"></s:param>
										<s:param name="UserAction" value="%{#session.userAction}"></s:param>
										<s:param name="UserName" value="%{#session.userName}"></s:param> 
									</s:url>
										
									<sj:a 
										id="ApproveID2"
										button="true" 
										title="Approve Changes"
										targets="approvalWizardDialogID"
										href="%{approveURL}" 
										onCompleteTopics="addModifyUserVerifyCompleteTopics"
									><s:text name="jsp.da_company_approval_button_text" /></sj:a>
									
								</ffi:cinclude>
							</ffi:cinclude>
						
						<ffi:cinclude value1="${rejectFlag}" value2="y" >
							<ffi:cinclude value1="${nextpage}" value2="" operator="equals">								
									<sj:a 
										id="rejectID2"
										formIds="rejectDualApprovalFormID2"
										targets="resultmessage"
										button="true" 
										title="Reject Changes"
										validate="true"
										validateFunction="customValidation" 
										onClickTopics="removeValidationErrors();"
										onSuccessTopics="rejectDAChangesSuccessTopics"
										onCompleteTopics="rejectDAChangesCompleteTopics"
									><s:text name="jsp.da_company_reject_button_text" /></sj:a>
							</ffi:cinclude>
						</ffi:cinclude>						
					</td></tr>
				</table>
			</td>
		</tr>
		
	</table>
	<br>
	<ffi:flush/>
</div>

<ffi:setProperty name="url" value="${SecurePath}user/da-summary-profile.jsp?PermissionsWizard=TRUE"  URLEncrypt="true" />
<ffi:setProperty name="BackURL" value="${url}"/>

<script>
function nextPage(){
	$.ajax({
		url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${nextpage.MenuUrl}&ParentMenu=${nextpage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${nextpage.PageKey}&SortKey=${nextpage.NextSortKey}"/>',
		success: function(data)
		{
			$('#approvalWizardDialogID').html(data);
			ns.admin.checkResizeApprovalDialog();
		}
	});
}

function previousPage(){
	$.ajax({
		url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${previouspage.MenuUrl}&ParentMenu=${previouspage.ParentMenu}&PermissionsWizard=TRUE&CurrentWizardPage=${previouspage.PageKey}&SortKey=${previouspage.NextSortKey}"/>',
		success: function(data)
		{
			$('#approvalWizardDialogID').html(data);
			ns.admin.checkResizeApprovalDialog();
		}
	});
}

function cancelWizard(){
	$('#approvalWizardDialogID').dialog('close');
}

function approve(){
	$.ajax({
		url: '<ffi:urlEncrypt url="/cb/pages/jsp/user/${approveURL}"/>',
		success: function(data)
		{
			ns.admin.reloadPendingUsersAndUsersGrid();
			ns.common.closeDialog("#approvalWizardDialogID");
		}
	});

}
</script>

<ffi:removeProperty name="DAItem"/>
<ffi:removeProperty name="categories"/>
<ffi:removeProperty name="SubmittedUserName"/>
<ffi:removeProperty name="SetBusinessEmployee"/>
<ffi:removeProperty name="nextpage"/>