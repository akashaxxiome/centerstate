<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:help id="user_clone-user-permissions-clone" className="moduleHelpClass"/>
<ffi:setProperty name='PageHeading' value='Clone User Permissions'/>

<ffi:setProperty name="BackURL" value="${SecurePath}user/clone-user-permissions.jsp" URLEncrypt="true"/>

<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" scope="request"/>
<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${SourceBusinessEmployeeId}"/>
<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeeSessionName" value="SourceBusinessEmployee"/>
<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeesSessionName" value="BusinessEmployeesForClone"/>
<ffi:setProperty name="SetBusinessEmployee" property="TaskErrorURL" value=""/>
<ffi:process name="SetBusinessEmployee"/>

<ffi:cinclude value1="${SourceBusinessEmployee}" value2="" operator="equals">
	<ffi:setProperty name="SetBusinessEmployee" property="Id" value="${SourceBusinessEmployeeId}"/>
	<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeeSessionName" value="SourceBusinessEmployee"/>
	<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeesSessionName" value="EntitlementProfilesForClone"/>
	<ffi:setProperty name="SetBusinessEmployee" property="TaskErrorURL" value="<%=com.ffusion.tasks.BaseTask.TASK_ERROR%>"/>
	<ffi:process name="SetBusinessEmployee"/>

	<ffi:setProperty name="ClonePermissions" property="CloneGroupAdminEntitlement" value="false"/>

</ffi:cinclude>

<ffi:process name="ClonePermissions"/>

<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminusers.jsp" URLEncrypt="true"/>

<script>
	location.replace("<ffi:getProperty name="SecurePath"/>user/clone-user-permissions-confirm.jsp");
</script>

