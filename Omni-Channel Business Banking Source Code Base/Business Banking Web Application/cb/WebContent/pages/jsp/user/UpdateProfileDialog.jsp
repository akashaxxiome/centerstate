<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script>

	$.subscribe('quickafterSuccessfulSave', function(event, data) {
		if (event.originalEvent.status == "success") {
			ns.common.showStatus($("#quickUpdateProfileResultmessage").html()
					.trim());
			ns.common.closeDialog("updateProfileDialogId");
			setTimeout(function() {
				//window.location.reload();
				window.location.hash = ns.home.currentMenuId;
				window.location.reload();
			}, 1000)
		}
	});

</script>

<div style="width:100%; margin:0 auto; text-align:center;">
	 <s:action namespace="/pages/jsp/user" name="ModifyUserProfileAction_init" executeResult="true">
	 	<s:param name="isQuickSearch" value="true"/>
	 </s:action>
</div>