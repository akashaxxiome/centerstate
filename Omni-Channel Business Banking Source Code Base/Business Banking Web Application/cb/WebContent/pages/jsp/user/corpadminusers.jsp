<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadminprofiles" className="moduleHelpClass"/>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.USER_ADMIN %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
		<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
		<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
		<ffi:process name="ThrowException"/>
</ffi:cinclude>

<%-- include this so refresh of grid will populate properly --%>
<ffi:cinclude value1="${corpadminusers_init_touched}" value2="true" operator="notEquals" >
	<s:include value="inc/corpadminusers-pre.jsp"/>	
</ffi:cinclude>

<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right" valign="middle" class="columndata">
			<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
			<ffi:process name="CanAdministerAnyGroup"/>
            <ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
                <s:url id="addUserURL" value="/pages/user/addBusinessUser_init.action" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					<s:param name="Init" value="true"/>
				</s:url>
                <sj:a id="addUserLink"
                   href="%{addUserURL}"
                   targets="inputDiv"
                   onClickTopics="beforeLoad"
                   onCompleteTopics="completeUserLoad"
                   button="true"
                   buttonIcon="ui-icon-add"><s:text name="jsp.user_28"/></sj:a>
            </ffi:cinclude>
        </td>
	</tr>
</table> --%>

<ffi:setGridURL grid="GRID_adminUser" name="DeleteURL" url="/cb/pages/user/deleteBusinessUser_init.action?employeeID={0}" parm0="Id"/>
<ffi:setGridURL grid="GRID_adminUser" name="EditURL" url="/cb/pages/user/editBusinessUser_init.action?employeeID={0}" parm0="Id"/>
<ffi:setGridURL grid="GRID_adminUser" name="PermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Users&BusinessEmployeeId={0}&OneAdmin={1}" parm0="Id" parm1="OneAdmin"/>
<ffi:setGridURL grid="GRID_adminUser" name="ApprovalsURL" url="/cb/pages/jsp/user/corpadminapprovalgroupassign-selected.jsp?ApprovalGroupUserID={0}&Context={1} (User)" parm0="Id" parm1="FullName"/>

<ffi:setProperty name="adminUserGridTempURL" value="getBusinessEmployees.action?CollectionName=BusinessEmployees&GridURLs=GRID_adminUser" URLEncrypt="true"/>
<s:url namespace="/pages/user" id="remoteurl" action="%{#session.adminUserGridTempURL}" escapeAmp="false"/>
     <sjg:grid
       id="adminUserGrid"
       caption=""
	   sortable="true"  
       dataType="json"
       href="%{remoteurl}"
       pager="true"
       viewrecords="true"
       gridModel="gridModel"
	   rowList="%{#session.StdGridRowList}" 
	   rowNum="%{#session.StdGridRowNum}" 
       rownumbers="false"
       altRows="true"
       sortname="NAME"
       sortorder="asc"
       shrinkToFit="true"
       navigator="true"
       navigatorAdd="false"
       navigatorDelete="false"
       navigatorEdit="false"
       navigatorRefresh="false"
       navigatorSearch="false"
       onGridCompleteTopics="addGridControlsEvents"
     >
     <sjg:gridColumn name="fullName" width="300" index="NAME" title="%{getText('jsp.default_283')}" sortable="true" />
     <sjg:gridColumn name="userName" width="150" index="USERNAME" title="%{getText('jsp.default_455')}" sortable="true" />
     <sjg:gridColumn name="map.Group" width="150" index="Group" title="%{getText('jsp.default_225')}" sortable="true" />
     <sjg:gridColumn name="map.Admin" width="75" index="admin" title="%{getText('jsp.user_32')}" sortable="false"  search="false"/>
     <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatUserActionLinks" width="90" search="false" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
	 <sjg:gridColumn name="map.IsAnAdminOf" width="0" index="IsAnAdminOf" title="%{getText('jsp.user_184')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="map.OneAdmin" width="0" index="OneAdmin" title="%{getText('jsp.user_231')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="map.Self" width="0" index="Self" title="%{getText('jsp.user_294')}" hidden="true" sortable="false" hidedlg="true"/>

 </sjg:grid>

<script type="text/javascript">

	var fnCustomUsersRefresh = function (event) {
		ns.common.forceReloadCurrentSummary();
	};
	
	$("#adminUserGrid").data('fnCustomRefresh', fnCustomUsersRefresh);
	
	$("#adminUserGrid").data("supportSearch",true);

	//disable sortable rows
	if(ns.common.isInitialized($("#adminUserGrid tbody"),"ui-jqgrid")){
		$("#adminUserGrid tbody").sortable("destroy");	
	};
	


	$("#adminUserGrid").jqGrid('setColProp','ID',{title:false});
</script>
<%-- include this so refresh of grid will populate properly --%>
<ffi:cinclude value1="${corpadminusers_init_touched}" value2="true" operator="notEquals" >
  <s:include value="inc/corpadminusers-pre.jsp"/> 
</ffi:cinclude>

<ffi:removeProperty name="corpadminusers_init_touched"/>
