<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:setProperty name="CanAdministerAnyGroup" property="GroupType" value="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>"/>
<ffi:process name="CanAdministerAnyGroup"/>

<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>

<ffi:help id="user_corpadmingroupadd-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_54')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<ffi:setProperty name="AddBusinessGroup" property="InitAutoEntitle" value="True"/>
<ffi:process name="AddBusinessGroup"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="AddBusinessGroup" property="SuccessURL" value=""/>
</ffi:cinclude>
<ffi:setProperty name="AddBusinessGroup" property="ValidateOnly" value="true"/>	
<ffi:process name="AddBusinessGroup"/>

<DIV align="center">
	<table cellspacing="0" cellpadding="0" border="0" width="100%" class="marginBottom10 marginTop20">
	<tr>
		<td width="25%">
			<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<s:text name="admin.group.new.summary"/>
				</div>
				<div class="paneContentWrapper">
					<table cellspacing="0" cellpadding="3" border="0" width="100%">
						<tr>
							<td><s:text name="jsp.default_226"/>:&nbsp; <ffi:getProperty name="AddBusinessGroup" property="GroupName"/></td>
						</tr>
						<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
							<input type="hidden" name="EditBusinessGroup.ParentGroupId" value="<ffi:getProperty name="Entitlement_EntitlementGroup" property="ParentId"/>"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
						<tr>
							<td>
								<s:text name="jsp.default_174"/>:&nbsp;
								<ffi:cinclude value1="${AddBusinessGroup.ParentGroupId}" value2="" operator="equals"><s:text name="jsp.default_296"/></ffi:cinclude>
								<ffi:cinclude value1="${AddBusinessGroup.ParentGroupId}" value2="" operator="notEquals">
									<ffi:list collection="Entitlement_EntitlementGroups" items="EntGroupItem">
										<ffi:cinclude value1="${AddBusinessGroup.ParentGroupId}" value2="${EntGroupItem.GroupId}" operator="equals">
											<ffi:getProperty name="EntGroupItem" property="GroupName"/>
										</ffi:cinclude>
									</ffi:list>
								</ffi:cinclude>
							</td>
						</tr>
						<tr>
							<td>
							<table cellspacing="0" cellpadding="0" border="0" width="100%">
								<tr>
									<td valign="top" width="100"><s:text name="jsp.user_38"/>:&nbsp;</td>
									<td><ffi:getProperty name="AdministratorStr"/></td>
								</tr>
							</table>
						</tr>
						</ffi:cinclude>
					</table>
				</div>
				</div>
			</div>
			</td>
			<td width="75%">
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<s:url id="tmpRedirectURL" value="%{'addBusinessGroup-daExecute'}" escapeAmp="false" namespace="/pages/user">
						<s:param name="ItemId" value="%{#parameters.ItemId}"/>
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
						<s:param name="doAutoEntitle" value="%{'false'}"/>
					</s:url>
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
					<s:url id="tmpRedirectURL" value="%{'addBusinessGroup-execute'}" escapeAmp="false" namespace="/pages/user"/>
				</ffi:cinclude>
				<s:form id="groupAddAutoEntitleFormId" action="%{tmpRedirectURL}" theme="simple" method="post" name="autoEntConfirm">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<table cellspacing="0" cellpadding="3" border="0" width="70%"  id="adminTblWidth">
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
					<ffi:cinclude value1="${AddBusinessGroup.AutoEntitle}" value2="true" operator="equals">
					<tr>
						<td align="center">
							&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="columndata marginRight20 paddingLeft10"><s:text name="jsp.user_125"/></span>
							<input type="radio" name="autoEntitle" value="true" checked="checked"/><s:text name="jsp.default_467"/>&nbsp;&nbsp;
							<input type="radio" name="autoEntitle" value="false"/><s:text name="jsp.default_295"/>
						</td>
						<td>&nbsp;</td>
					</tr>
					</ffi:cinclude>
					</ffi:cinclude>
					<tr>
						<td align="center" class="sectionhead"><s:text name="jsp.user_379"/></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td align="center">
							<br><br><sj:a
									  button="true" 
									  onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
								<sj:a
									  button="true" 
									  summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelGroupForm"><s:text name="jsp.default_82"/></sj:a>
								<sj:a
									formIds="groupAddAutoEntitleFormId"
									targets="confirmDiv"
									button="true" 
									validate="false"
									onBeforeTopics="beforeSubmit" onCompleteTopics="completeSubmit"
									onErrorTopics="errorSubmit" onSuccessTopics="successSubmit"
									><s:text name="jsp.default_366"/></sj:a>
						</td>
						<td>&nbsp;</td>
					</tr>
				</table>
				</s:form>
			</td>
		</tr>	
	</table>
</DIV>
