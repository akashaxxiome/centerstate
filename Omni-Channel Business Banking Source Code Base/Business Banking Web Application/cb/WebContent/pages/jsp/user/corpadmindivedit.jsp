<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_corpadmindivedit" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_141')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<span id="PageHeading" style="display:none;"><s:text name="jsp.user_141"/></span>

<%-- Call/Initialize any tasks that are required for pulling data necessary for this page.	--%>
<ffi:object id="MultipleCategories" name="com.ffusion.util.FilteredList" scope="session" />

<%

	//if flag is sent in request then give priority over session.
	String UseLastRequest = request.getParameter("UseLastRequest");
	if(UseLastRequest != null && !"".equals(UseLastRequest)){
		session.setAttribute("UseLastRequest", UseLastRequest);
	}
	String CancelReload = request.getParameter("CancelReload");
	if(CancelReload!= null){
		session.setAttribute("CancelReload", CancelReload);
	}
	
	String AdminsSelected = request.getParameter("AdminsSelected");
	if(AdminsSelected!= null){
		session.setAttribute("AdminsSelected", AdminsSelected);
	}
	
	String AutoEntitleAdministrators = request.getParameter("AutoEntitleAdministrators");
	if(AutoEntitleAdministrators!= null){
		session.setAttribute("AutoEntitleAdministrators", AutoEntitleAdministrators);
	}
	
	String EditDivision_GroupId = request.getParameter("EditDivision_GroupId");
	if(EditDivision_GroupId!= null){
		session.setAttribute("EditDivision_GroupId", EditDivision_GroupId);
	}
	
	String EditGroup_GroupId = request.getParameter("EditGroup_GroupId");
	if(EditGroup_GroupId!= null){
		session.setAttribute("EditGroup_GroupId", EditGroup_GroupId);
	}
	
	String EditDivision_ACHCompID = request.getParameter("EditDivision_ACHCompID");
	if(EditDivision_ACHCompID!= null){
		session.setAttribute("EditDivision_ACHCompID", EditDivision_ACHCompID);
	}
	
	String EntGroupItemACHCompanyName = request.getParameter("EntGroupItemACHCompanyName");
	if(EntGroupItemACHCompanyName!= null){
		session.setAttribute("EntGroupItemACHCompanyName", EntGroupItemACHCompanyName);
	}
		
	String Section = request.getParameter("Section");
	if(Section!= null){
		session.setAttribute("Section", Section);
	}
	
	String DisplayLocation = request.getParameter("DisplayLocation");
	if(DisplayLocation!= null){
		session.setAttribute("DisplayLocation", DisplayLocation);
	}
%>
<%
	String groupId = request.getParameter("EditDivision_GroupId");
%>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${EditDivision_GroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:cinclude value1="${CancelReload}" value2="TRUE" operator="equals">
<%
// The user has pressed cancel, so go back to the previous version of the collections
session.setAttribute( "NonAdminEmployees", session.getAttribute("NonAdminEmployeesCopy"));
session.setAttribute( "AdminEmployees", session.getAttribute("AdminEmployeesCopy"));
session.setAttribute( "NonAdminGroups", session.getAttribute("NonAdminGroupsCopy"));
session.setAttribute( "AdminGroups", session.getAttribute("AdminGroupsCopy"));
session.setAttribute( "adminMembers", session.getAttribute("adminMembersCopy"));
session.setAttribute( "userMembers", session.getAttribute("userMembersCopy"));
session.setAttribute( "groupIds", session.getAttribute("groupIdsCopy"));
session.setAttribute( "tempAdminEmps", session.getAttribute("tempAdminEmpsCopy") );
session.setAttribute( "GroupDivAdminEdited", session.getAttribute("GroupDivAdminEditedCopy"));
%>
</ffi:cinclude>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="CancelReload"/>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
	
<%-- Initialize the object QTS#628252. --%>
	<ffi:cinclude value1="${GetEntitlementGroup}" value2="" operator="equals">
		<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
	</ffi:cinclude>
	
    <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>'/>
    <ffi:process name="GetEntitlementGroup"/>
	<ffi:object id="EditBusinessGroup" name="com.ffusion.tasks.admin.EditBusinessGroup" scope="session"/>
	<ffi:setProperty name="EditBusinessGroup" property="GroupName" value="${Entitlement_EntitlementGroup.GroupName}"/>
	<ffi:object id="GetSupervisorFor" name="com.ffusion.tasks.admin.GetSupervisorFor" scope="request" />
	<ffi:setProperty name="GetSupervisorFor" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
	<ffi:process name="GetSupervisorFor"/>
	<ffi:cinclude value1="${GetSupervisorFor.SupervisorFound}" value2="true" operator="equals">
		<ffi:setProperty name="EditBusinessGroup" property="SupervisorId" value="${Supervisor.Id}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${GetSupervisorFor.SupervisorFound}" value2="true" operator="notEquals">
		<ffi:setProperty name="EditBusinessGroup" property="SupervisorId" value=""/>
	</ffi:cinclude>

	<%-- Set this flag for determining if to load the Entitlement groups for SetAdministrator task --%>
	<ffi:setProperty name="editDivisionTouched" value="true"/>

    <ffi:setProperty name="EditGroup_DivisionName" value="${Entitlement_EntitlementGroup.GroupName}"/>
	<ffi:removeProperty name="GroupDivAdminEdited"/>
</ffi:cinclude>
<%-- save the group name - if we are coming back from admin edit, we need the possibly modified group name, not the one from the DA object --%>
<ffi:setProperty name="saveGroupName" value="${EditBusinessGroup.groupName}"/>
<!-- Dual Approval for profile starts-->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE%>"/>
	<ffi:process name="GetDACategoryDetails"/>
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="EditBusinessGroup"/>
	<ffi:process name="PopulateObjectWithDAValues"/>
	<ffi:removeProperty name="GetDACategoryDetails"/>
	<ffi:removeProperty name="PopulateObjectWithDAValues"/>
</ffi:cinclude>
<!-- Dual Approval ends -->
<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="equals">
	<ffi:setProperty name="EditBusinessGroup" property="groupName" value="${saveGroupName}"/>
</ffi:cinclude>
<ffi:removeProperty name="saveGroupName"/>
<ffi:removeProperty name="UseLastRequest"/>

<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
<ffi:process name="GetBusinessEmployee"/>

<ffi:object id="GetBusinessByEmployee" name="com.ffusion.tasks.business.GetBusinessByEmployee" scope="request"/>
<ffi:setProperty name="GetBusinessByEmployee" property="BusinessEmployeeId" value="${BusinessEmployee.Id}"/>
<ffi:process name="GetBusinessByEmployee"/>

<ffi:object id="IsValidGroupDescendant" name="com.ffusion.efs.tasks.entitlements.IsValidGroupDescendant" scope="request"/>
<ffi:setProperty name="IsValidGroupDescendant" property="AncestorEntGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="IsValidGroupDescendant" property="AncestorEntGroupType" value="Business"/>
<ffi:setProperty name="IsValidGroupDescendant" property="DescendantEntGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:setProperty name="IsValidGroupDescendant" property="DescendantEntGroupType" value="Division"/>
<ffi:process name="IsValidGroupDescendant"/>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="request" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>

<ffi:object id="GetBusinessAccounts" name="com.ffusion.tasks.accounts.GetBusinessAccounts" scope="session"/>
<ffi:setProperty name="GetBusinessAccounts" property="AccountsName" value="Accounts"/>
<ffi:process name="GetBusinessAccounts"/>
<ffi:removeProperty name="GetBusinessAccounts"/>

<ffi:object id="GetPossibleSupervisors" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroupId" scope="session"/>
<ffi:setProperty name="GetPossibleSupervisors" property="EntitlementGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:setProperty name="GetPossibleSupervisors" property="BusinessEmployeesSessionName" value="PossibleSupervisors"/>
<ffi:process name="GetPossibleSupervisors"/>
<ffi:removeProperty name="GetPossibleSupervisors" />
<ffi:setProperty name="CanDeleteEntitlementGroup" property="SuccessURL" value="${SecurePath}user/corpadmindivdelete-verify.jsp"/>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
	<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount" scope="session" />
	<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%=com.ffusion.tasks.GetDisplayCount.CASHCON %>"/>
	<ffi:process name="GetDisplayCount"/>
	<ffi:removeProperty name="GetDisplayCount"/>

	<ffi:object id="GetLocations" name="com.ffusion.tasks.cashcon.GetLocations"/>
	<ffi:setProperty name ="GetLocations" property="DivisionID" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="GetLocations" property="LocationName" value="${LocationSearch_Name}"/>
	<ffi:setProperty name="GetLocations" property="LocationID" value="${LocationSearch_ID}"/>
	<ffi:setProperty name="GetLocations" property="MaxResults" value="${DisplayCount}"/>
	<ffi:process name="GetLocations"/>

	<ffi:cinclude value1="${SortedBy}" value2="" operator="notEquals">
    	<ffi:setProperty name="Locations" property="SortedBy" value="${SortedBy}"/>
	</ffi:cinclude>

	<ffi:setProperty name="EditDivision_ShowLocSearch" value="TRUE"/> 
	<ffi:cinclude value1="${GetLocations.LocationName},${GetLocations.LocationID}" value2="," operator="equals">
		<ffi:cinclude value1="${DisplayCount}" value2="${Locations.size}" operator="notEquals">
			<ffi:setProperty name="EditDivision_ShowLocSearch" value="FALSE"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<%--get all administrators from GetAdminsForGroup, which stores info in session under EntitlementAdmins--%>
<ffi:object id="GetAdministratorsForGroups" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" scope="session"/>
    <ffi:setProperty name="GetAdministratorsForGroups" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}" />
<ffi:process name="GetAdministratorsForGroups"/>
<ffi:removeProperty name="GetAdministratorsForGroups"/>


<% session.setAttribute("FFIEditBusinessGroup", session.getAttribute("EditBusinessGroup")); %>


		<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" align="center">
						<ul id="formerrors"></ul>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<s:form id="editDivFormId" namespace="/pages/user" action="editBusinessDivision-verify" theme="simple" name="FormName" method="post">
								<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<input type="hidden" name="EditBusinessGroup.ParentGroupId" value="<ffi:getProperty name="Entitlement_EntitlementGroup" property="ParentId"/>">
								<input type="hidden" name="SecurityPath" value="<ffi:getProperty name="SecurePath"/>"><%-- "SecurePath" is in blacklist, it can not be a parameter here--%>
								<input type="hidden" name="EditBusinessGroup.SessionGroupName" value="Entitlement_EntitlementGroup">
								<input type="hidden" name="EditBusinessGroup.CheckboxesAvailable" value="false">
							
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td width="40">&nbsp;</td>
									<td width="100"><s:text name="jsp.user_121"/><span class="required">*</span></td>
									<td align="left">
										<input class="ui-widget-content ui-corner-all" type="text" name="GroupName" style="width:120px;"  value="<ffi:getProperty name="EditBusinessGroup" property="GroupName"/>" size="24" border="0" maxlength="150">
										<input class="ui-widget-content ui-corner-all" type="text" name="nonDisplayGroupName" size="24" maxlength="255" border="0" value="" style="display:none">
										<span class="sectionhead_greyDA">
											<ffi:getProperty name="oldDAObject" property="GroupName"/>
										</span>
										 (<ffi:getProperty name="Business" property="BusinessName"/>) &nbsp;&nbsp; <span id="GroupNameError"></span>
									</td>
								</tr>
								<!-- Dual Approval for administrator starts -->
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">

									<ffi:removeProperty name="CATEGORY_BEAN"/>
									<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="TempBusinessEmployee"/>
									<ffi:setProperty name="TempBusinessEmployee" property="BusinessId" value="${SecureUser.BusinessID}"/>
									<ffi:setProperty name="TempBusinessEmployee" property="BankId" value="${SecureUser.BankID}"/>

									<%-- based on the business employee object, get the business employees for the business --%>
									<ffi:removeProperty name="GetBusinessEmployees"/>
									<ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
									<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="TempBusinessEmployee"/>
									<ffi:process name="GetBusinessEmployees"/>
									<ffi:removeProperty name="GetBusinessEmployees"/>
									<ffi:removeProperty name="TempBusinessEmployee"/>

									<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
										<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
										<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
										<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_ADMINISTRATOR%>"/>
									<ffi:process name="GetDACategoryDetails"/>

									<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
										<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="EntitlementAdmins"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="AdminEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="AdminGroups"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="NonAdminEmployees"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="NonAdminGroups"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
										<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
										<ffi:process name="GenerateUserAndAdminLists"/>
										<ffi:removeProperty name="adminIds"/>
										<ffi:removeProperty name="userIds"/>
										<ffi:removeProperty name="GenerateUserAndAdminLists"/>

										<ffi:object id="GenerateListsFromDAAdministrators"  name="com.ffusion.tasks.dualapproval.GenerateListsFromDAAdministrators" scope="session"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminEmployees" value="AdminEmployees"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="adminGroups" value="AdminGroups"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminGroups" value="NonAdminGroups"/>
										<ffi:setProperty name="GenerateListsFromDAAdministrators" property="nonAdminEmployees" value="NonAdminEmployees"/>
										<ffi:process name="GenerateListsFromDAAdministrators"/>
									</ffi:cinclude>

									<ffi:object id="GenerateUserAdminList" name="com.ffusion.tasks.dualapproval.GenerateUserAdminList"/>
									<ffi:setProperty name="GenerateUserAdminList" property="adminEmployees" value="AdminEmployees"/>
									<ffi:setProperty name="GenerateUserAdminList" property="adminGroups" value="AdminGroups"/>
									<ffi:setProperty name="GenerateUserAdminList" property="sessionName" value="NewAdminBusinessEmployees"/>
									<ffi:process name="GenerateUserAdminList"/>
									<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
										<ffi:setProperty name="FilterNotEntitledEmployees" property="BusinessEmployeesSessionName" value="NewAdminBusinessEmployees" />
										<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.DIVISION_MANAGEMENT%>" />
									<ffi:process name="FilterNotEntitledEmployees"/>

									<ffi:object id="GenerateCommaSeperatedList" name="com.ffusion.tasks.dualapproval.GenerateCommaSeperatedList"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="NewAdminBusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="NewAdminUserStringList"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:process name="GenerateCommaSeperatedList"/>

									<ffi:object id="GenerateUserAndAdminLists" name="com.ffusion.tasks.user.GenerateUserAndAdminLists"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="EntAdminsName" value="EntitlementAdmins"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="EmployeesName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserAdminName" value="OldAdminEmployees"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupAdminName" value="OldAdminGroups"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserName" value="tempUserName"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="GroupName" value="tempGroupName"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="AdminIdsName" value="adminIds"/>
									<ffi:setProperty name="GenerateUserAndAdminLists" property="UserIdsName" value="userIds"/>
									<ffi:process name="GenerateUserAndAdminLists"/>
									<ffi:removeProperty name="adminIds"/>
									<ffi:removeProperty name="tempUserName"/>
									<ffi:removeProperty name="tempGroupName"/>
									<ffi:removeProperty name="userIds"/>
									<ffi:removeProperty name="GenerateUserAndAdminLists"/>

									<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
									<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
									  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
									<ffi:process name="GetBusinessEmployeesByEntGroups"/>
									<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
									<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
										  <ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.DIVISION_MANAGEMENT%>" />
									<ffi:process name="FilterNotEntitledEmployees"/>

									<ffi:setProperty name="GenerateCommaSeperatedList" property="collectionName" value="BusinessEmployees"/>
									<ffi:setProperty name="GenerateCommaSeperatedList" property="sessionKeyName" value="OldAdminStringList"/>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="name"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
										<ffi:setProperty name="GenerateCommaSeperatedList" property="property" value="lastName"/>
									</ffi:cinclude>
									<ffi:process name="GenerateCommaSeperatedList"/>

									<ffi:removeProperty name="GenerateCommaSeperatedList"/>
									<ffi:removeProperty name="GetDACategoryDetails"/>
									<ffi:removeProperty name="GenerateListsFromDAAdministrators"/>
									<ffi:removeProperty name="GenerateCommaSeperatedAdminList"/>
									<ffi:removeProperty name="NewAdminStringList"/>
									<ffi:removeProperty name="BusinessEmployees"/>
									<ffi:removeProperty name="NewAdminBusinessEmployees"/>
								</ffi:cinclude>
								<!-- Dual Approval for administrator ends -->

								<%--get the employees from those groups, they'll be stored under BusinessEmployees --%>
								<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntAdmins" scope="session"/>
								  	<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementAdminsSessionName" value="EntitlementAdmins" />
								<ffi:process name="GetBusinessEmployeesByEntGroups"/>
								<ffi:removeProperty name="GetBusinessEmployeesByEntGroups" />
								<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
									  <ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.DIVISION_MANAGEMENT%>" />
								<ffi:process name="FilterNotEntitledEmployees"/>
								<tr>
									<td width="40">&nbsp;</td>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
										<td class="adminBackground sectionsubhead" width="120">
												<s:text name="jsp.user_38"/>:&nbsp;
										</td>
									</ffi:cinclude>
									<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
										<td class="adminBackground sectionheadDA" align="right" valign="baseline" width="120">
												<s:text name="jsp.user_38"/>:&nbsp;
										</td>
									</ffi:cinclude>
									<td valign="middle" align="left">
										<div style="display:inline-block;"  class="alignItemsDivision">
											<%--display name of each employee in BusinessEmployees--%>
				 							<%
			 							       		boolean isFirst= true;
		 										String temp = "";
			 							       	%>
		 									<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
												 <ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
													<ffi:setProperty name="FilterNotEntitledEmployees" property="BusinessEmployeesSessionName" value="tempAdminEmps" />
													<ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.DIVISION_MANAGEMENT%>" />
												 <ffi:process name="FilterNotEntitledEmployees"/>
			 									<ffi:setProperty name="tempAdminEmps" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
												<ffi:list collection="tempAdminEmps" items="emp">
					 							<%--display separator if this is not the first one--%>
												<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
													<ffi:setProperty name="empName" value="${emp.Name}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
													<ffi:setProperty name="empName" value="${emp.LastName}"/>
												</ffi:cinclude>
		 										<%
		 										if( isFirst ) {
			 									isFirst = false;
				 								temp = temp + session.getAttribute( "empName" );
					 							} else {
		 										temp = temp + ", " + session.getAttribute( "empName" );
		 										}
			 									%>
		 										</ffi:list>
												<ffi:setProperty name="modifiedAdmins" value="TRUE"/>
			 								</ffi:cinclude>
											<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
												<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
												<ffi:list collection="BusinessEmployees" items="emp">
			 									<%--display separator if this is not the first one--%>
												<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
													<ffi:setProperty name="empName" value="${emp.Name}"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
													<ffi:setProperty name="empName" value="${emp.LastName}"/>
												</ffi:cinclude>
		 										<%
		 										if( isFirst ) {
		 											isFirst = false;
			 										temp = temp + session.getAttribute( "empName" );
		 										} else {
		 											temp = temp + ", " + session.getAttribute( "empName" );
			 									}
				 								%>
		 										</ffi:list>
												<ffi:setProperty name="modifiedAdmins" value="FALSE"/>
											</ffi:cinclude>
															
											<%-- <ffi:removeProperty name="AdminsSelected"/> --%>
											<%
			 								if( temp.length() > 120 ) {
		 										temp = temp.substring( 0, 120 );
		 										int index = temp.lastIndexOf( ',' );
												temp = temp.substring( 0, index ) + "...";
		 									}
		 									session.setAttribute( "AdministratorStr", temp );%>
		
			 								<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="equals">
			 									<ffi:getProperty name="AdministratorStr"/>
			 									<ffi:cinclude value1="${AdministratorStr}" value2="" operator="equals">
													<s:text name="jsp.default_296"/>
												</ffi:cinclude>
			 								</ffi:cinclude>
			 								<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
			 									<ffi:getProperty name="NewAdminUserStringList"/>
			 								</ffi:cinclude>
											<ffi:removeProperty name="empName"/>
											<ffi:cinclude value1="${CATEGORY_BEAN}" value2="" operator="notEquals">
												<span class="sectionhead_greyDA">
														<br>
														<ffi:getProperty name="OldAdminStringList"/>
												</span>
											</ffi:cinclude>
										</div>
									   &nbsp;&nbsp;
									   
									   <s:hidden id="needVerify_HiddenFieldId" name="needVerify" value="true"/>
										<%-- Kaijies: add administrator icon button --%>		
										<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
											<sj:a
												id="editDivAdminButtonId"
												button="true"
												title="Edit"
												targets="editAdminerDialogId"
												formIds="editDivFormId"
												formid="editDivFormId"
												field1="needVerify_HiddenFieldId"
												value1="false"
												postaction="/cb/pages/user/editBusinessDivision-editDivModifyAdminerBeforeChange.action"
												onClickTopics="postFormTopic"
												onCompleteTopics="openDivisionAdminerDialog">
												<s:text name="jsp.default_178" />
											</sj:a>
										</ffi:cinclude>
										<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
											<sj:a
												id="editDivAdminButtonId"
												button="true"
												title="Edit"
												targets="editAdminerDialogId"
												formIds="editDivFormId"
												formid="editDivFormId"
												field1="needVerify_HiddenFieldId"
												value1="false"												
												postaction="/cb/pages/user/editBusinessDivision-editDivModifyAdminerAfterChange.action"
												onClickTopics="postFormTopic"
												onCompleteTopics="openDivisionAdminerDialog">
												<s:text name="jsp.default_178" />
											</sj:a>
										</ffi:cinclude>
										
										<ffi:removeProperty name="AdminsSelected"/>
										
										</td>
								</tr>
								<tr>
									<td colspan="3">
										<div align="center">
												<span class="required">* <s:text name="jsp.default_240"/></span>
												<br><br>
												
												<s:url id="resetEditDivButtonUrl" value="/pages/jsp/user/corpadmindivedit.jsp?UseLastRequest=FALSE">
													<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
												</s:url>
												<sj:a id="resetEditDivBtn"
		                                           href="%{resetEditDivButtonUrl}"
												   targets="inputDiv"
												   button="true"><s:text name="jsp.default_358"/></sj:a>
												
												<sj:a id="cancelEditDivBtn"
												   button="true"
												   summaryDivId="summary" buttonType="cancel"
												   onClickTopics="cancelDivisionForm"><s:text name="jsp.default_82"/></sj:a>
												<sj:a
													formIds="editDivFormId"
													targets="verifyDiv"
													button="true"
													validate="true"
													validateFunction="customValidation"
													onBeforeTopics="beforeVerify2"
													onCompleteTopics="completeVerify2"
													onErrorTopics="errorVerify"
													onSuccessTopics="successVerify"
													formid="editDivFormId"
													field1="needVerify_HiddenFieldId"
													value1="true"												
													postaction="/cb/pages/user/editBusinessDivision-verify.action"
													onClickTopics="postFormTopic"
													><s:text name="jsp.default_366"/></sj:a>
													
											</div>
									</td>
								</tr>
							</table>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
		
		
		<ffi:cinclude value1="${DisplayLocation}" value2="true" operator="equals">
			<div id="locationEditTableID" align="center" style="width:750px; margin:10px auto;">
			<div class="blockHead" align="left"><s:text name="user.location.summary" /></div>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT%>">				
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">		
					<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
						<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
						<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION%>"/>
						<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
						<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION%>"/>
						<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="<%=IDualApprovalConstants.TRUE%>"/>
					<ffi:process name="GetDACategoryDetails"/>
			
					<ffi:object id="GetLocationsDA" name="com.ffusion.tasks.dualapproval.GetLocationsDA"/>
					<ffi:setProperty name="GetLocationsDA" property="LocationsSessionName" value="Locations"/>
					<ffi:setProperty name="GetLocationsDA" property="DALocationsSessionName" value="DALocations"/>
					<ffi:process name="GetLocationsDA"/>
			
					<ffi:cinclude value1="${SortedBy}" value2="" operator="notEquals">
			   			<ffi:setProperty name="DALocations" property="SortedBy" value="${SortedBy}"/>
					</ffi:cinclude>
				</ffi:cinclude>
				
				
				<ffi:cinclude value1="${EditDivision_ShowLocSearch}" value2="TRUE" operator="equals">
		           
		            <table width="50%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" class="adminBackground" width="708">
	                            <table width="50%" border="0" cellspacing="3" cellpadding="0">
			                        <form action="<ffi:getProperty name="SecurePath" />user/corpadmindivedit.jsp" method="post" name="frmFilter">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		                                <tr>
		                                    <td class="tbrd_b" colspan="4" align="left" nowrap><span class="sectionhead"><!--L10NStart-->Location Search<!--L10NEnd--></span></td>
		                                </tr>
		                                <tr>
		                                    <td align="left" nowrap>&nbsp;</td>
		                                    <td align="right" class="sectionhead"><!--L10NStart-->Location Name<!--L10NEnd--></td>
		                                    <td colspan="2" align="left"><input class="txtbox" type="text" name="LocationSearch_Name" size="25" maxlength="16" border="0" value="<ffi:getProperty name="LocationSearch_Name" />"></td>
		                                </tr>
		                                <tr>
		                                    <td align="left" nowrap>&nbsp;</td>
		                                    <td align="right" class="sectionhead"><s:text name="jsp.user_189" /></td>
		                                    <td colspan="2" align="left"><input class="txtbox" type="text" name="LocationSearch_ID" size="25" maxlength="15" border="0" value="<ffi:getProperty name="LocationSearch_ID" />"></td>
		                                </tr>
		                                <tr>
		                                    <td colspan="4" align="center" nowrap><input class="submitbutton" type="submit" value="SEARCH" ></td>
		                                </tr>

			                        </form>
	                            </table>		                    
	                        </td>
		                </tr>
		            </table>
		            <br>
				</ffi:cinclude>
				<ffi:cinclude value1="${DisplayCount}" value2="${Locations.Size}" operator="equals">
					<table width="750" border="0" cellspacing="0" cellpadding="0">
					     <tr>
					          <td align="center" ><span class="sectionsubhead"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmindivedit.jsp-1" parm0="${DisplayCount}" parm1="${DisplayCount}"/></span></td>
					     </tr>
					</table>
					<br>
				</ffi:cinclude>
				<ffi:cinclude value1="${MultipleCategories.Size}" value2="0" operator="equals">
				<div class="paneWrapper">
				<div class="paneInnerWrapper">
					<div class="header">
					<table width="750" border="0" cellspacing="0" cellpadding="0">
						<tr>
		                    <ffi:cinclude value1="${Locations.SortedBy}" value2="LOCATION_NAME," operator="notEquals">
		                        <ffi:cinclude value1="${Locations.SortedBy}" value2="LOCATION_NAME,REVERSE" operator="notEquals">
		                            <td class="sectionhead" width="150"> <s:text name="jsp.user_190" />&nbsp;</td>
		                        </ffi:cinclude>
		                        <ffi:cinclude value1="${Locations.SortedBy}" value2="LOCATION_NAME,REVERSE" operator="equals">
		                            <td class="sectionhead" width="150"> <s:text name="jsp.user_190" />&nbsp;</td>
		                        </ffi:cinclude>
		                    </ffi:cinclude>
		                    <ffi:cinclude value1="${Locations.SortedBy}" value2="LOCATION_NAME," operator="equals">
		                        <td class="sectionhead" width="150"> <s:text name="jsp.user_190" />&nbsp;</td>
		                    </ffi:cinclude>
		
		                    <ffi:cinclude value1="${Locations.SortedBy}" value2="LOCATION_ID," operator="notEquals">
		                        <ffi:cinclude value1="${Locations.SortedBy}" value2="LOCATION_ID,REVERSE" operator="notEquals">
		                            <td class="sectionhead" width="150"><s:text name="jsp.user_189" />&nbsp;</td>
		                        </ffi:cinclude>
		                        <ffi:cinclude value1="${Locations.SortedBy}" value2="LOCATION_ID,REVERSE" operator="equals">
		                            <td class="sectionhead" width="150"><s:text name="jsp.user_189" />&nbsp;</td>
		                        </ffi:cinclude>
		                    </ffi:cinclude>
		                    <ffi:cinclude value1="${Locations.SortedBy}" value2="LOCATION_ID," operator="equals">
		                        <td class="sectionhead" width="150"><s:text name="jsp.user_189" />&nbsp;</td>
		                    </ffi:cinclude>
		                   
		                    <td class="sectionhead" width="150"><s:text name="jsp.home.column.label.action" /></td>
							<td width="150">&nbsp;</td>
							<td width="50">&nbsp;</td>
						</tr>
					</table>
					</div>
					<div class="paneContentWrapper">
					<table>
					<%	int color = 0;	%>
	                <ffi:setProperty name="EmptyLocations" value="true"/>
					<ffi:list collection="Locations" items="Location">
                        <ffi:setProperty name="EmptyLocations" value="false"/>
                        <tr <%= color % 2 == 0 ? "class=\"ltrow3\"" : "class=\"dkrow\"" %> >
                            <td class="columndata" width="150" nowrap><ffi:getProperty name="Location" property="LocationName"/></td>
                            <td class="columndata" width="150" nowrap><ffi:getProperty name="Location" property="LocationID"/></td>
                            <td class="columndata" width="150" align="right">
                            	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
										<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>
										<s:url id="locationEditURL" value="%{#session.PagesPath}user/corpadminlocedit.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
												<s:param name="editlocationreload" value="%{'true'}"/>
												<s:param name="fromDivEdit" value="%{'true'}"/>
												<s:param name="EditLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
										</s:url>
										<sj:a
											href="%{locationEditURL}" 
											targets="viewLocationDialogID" 
											button="true" 
											title="Edit"
											onClickTopics=""
											onCompleteTopics="openViewLocationDialog"
										>
												<s:text name="jsp.default_178" />
										</sj:a>	
                                </ffi:cinclude>
                                <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
                                	<ffi:setProperty name="childSequence" value="<%= String.valueOf(color)%>"/>
										<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>
										<s:url id="locationEditURL" value="%{#session.PagesPath}user/corpadminlocedit.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
												<s:param name="editlocationreload" value="%{'true'}"/>
												<s:param name="fromDivEdit" value="%{'true'}"/>
												<s:param name="childSequence" value="%{#session.childSequence}"/>
												<s:param name="EditLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
										</s:url>
										<sj:a
											href="%{locationEditURL}" 
											targets="viewLocationDialogID" 
											button="true" 
											title="Edit"
											onClickTopics=""
											onCompleteTopics="openViewLocationDialog"
										>
												<s:text name="jsp.default_178" />
										</sj:a>	
                                </ffi:cinclude>
                            </td>
                            <td class="columndata"  width="150">
	                            <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
										<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>
										<ffi:setProperty name="DeleteLocation_LocationName"  value="${Location.LocationName}"/>
										<s:url id="locationDeleteURL" value="%{#session.PagesPath}user/corpadminlocdelete-verify.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
												<s:param name="DeleteLocation_LocationName" value="%{#session.DeleteLocation_LocationName}"/>
												<s:param name="childSequence" value="%{#session.childSequence}"/>
												<s:param name="DeleteLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
										</s:url>
										<sj:a
											href="%{locationDeleteURL}" 
											targets="deleteLocationDialogID" 
											button="true" 
											title="Delete"
											onClickTopics=""
											onCompleteTopics="openDeleteLocationDialog"
										>
												<s:text name="jsp.default_162" />
										</sj:a>	
								</ffi:cinclude>
                                <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
                                	<ffi:setProperty name="childSequence" value="<%= String.valueOf(color)%>"/>
										<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>
										<ffi:setProperty name="DeleteLocation_LocationName"  value="${Location.LocationName}"/>
										<s:url id="locationDeleteURL" value="%{#session.PagesPath}user/corpadminlocdelete-verify.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
												<s:param name="DeleteLocation_LocationName" value="%{#session.DeleteLocation_LocationName}"/>
												<s:param name="childSequence" value="%{#session.childSequence}"/>
												<s:param name="DeleteLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
										</s:url>
										<sj:a
											href="%{locationDeleteURL}" 
											targets="deleteLocationDialogID" 
											button="true" 
											title="Delete"
											onClickTopics=""
											onCompleteTopics="openDeleteLocationDialog"
										>
												<s:text name="jsp.default_162" />
										</sj:a>	
                                </ffi:cinclude>
						    </td>
						    <td width="50">&nbsp;</td>
                         </tr>
                        <%	color++;	%>
					</ffi:list>
	                <ffi:cinclude value1="${EmptyLocations}" value2="true" operator="equals">
	                    <tr class="ltrow3">
	                        <td colspan="5" class="columndata">
	                            <!--L10NStart-->No Locations Available<!--L10NEnd-->
	                        </td>
	                    </tr>
                        <%	color++;	%>
	                </ffi:cinclude>
	
					<tr>
						<td colspan="5" align="center">
					<ffi:setProperty name="PageScope_Color" value="<%= String.valueOf(--color)%>" />
					<form action="<ffi:urlEncrypt url="${SecurePath}user/corpadminlocadd.jsp?addlocation-reload=true&childSequence=${PageScope_Color}" />" name="AddLocation" method="post" >
					<table>
						<tr>
					<ffi:removeProperty name="PageScope_Color" />

		                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<td <%= color % 2 == 0 ? "class=\"ltrow3\"" : "class=\"dkrow\"" %> colspan="4" align="center" >
								<s:url id="addLocationURL" value="/pages/jsp/user/corpadminlocadd.jsp" escapeAmp="false">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									<s:param name="addlocation-reload" value="true"/>
									<s:param name="childSequence" value="%{#session.PageScope_LocColor}"/>
								</s:url>
								<sj:a id="addLocationLinkInEditDiv"
									  href="%{addLocationURL}"
									  targets="inputDiv"
									  onClickTopics="beforeLoad"
									  onCompleteTopics="completeDivisionLoad"
									  button="true"><s:text name="jsp.user_20"/></sj:a>
							</td>
							</tr>
						</table>
						</form>
						</td>
					</tr>
				</table>
				</div>
				</div>	
				</div>
			</ffi:cinclude>
			<ffi:cinclude value1="${MultipleCategories.Size}" value2="0" operator="notEquals">
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
						
				<table width="750" border="0" cellspacing="0" cellpadding="0">
					<tr>
	                    <ffi:cinclude value1="${DALocations.SortedBy}" value2="LOCATION_NAME," operator="notEquals">
	                        <ffi:cinclude value1="${DALocations.SortedBy}" value2="LOCATION_NAME,REVERSE" operator="notEquals">
	                            <td class="sectionhead" width="150"> <s:text name="jsp.user_190" />&nbsp;</td>
	                        </ffi:cinclude>
	                        <ffi:cinclude value1="${DALocations.SortedBy}" value2="LOCATION_NAME,REVERSE" operator="equals">
	                            <td class="sectionhead" width="150"> <s:text name="jsp.user_190" />&nbsp;</td>
	                        </ffi:cinclude>
	                    </ffi:cinclude>
	                    <ffi:cinclude value1="${DALocations.SortedBy}" value2="LOCATION_NAME," operator="equals">
	                        <td class="sectionhead" width="150"> <s:text name="jsp.user_190" />&nbsp;</td>
	                    </ffi:cinclude>
	
	                    <ffi:cinclude value1="${DALocations.SortedBy}" value2="LOCATION_ID," operator="notEquals">
	                        <ffi:cinclude value1="${DALocations.SortedBy}" value2="LOCATION_ID,REVERSE" operator="notEquals">
	                            <td class="sectionhead" width="150"><s:text name="jsp.user_189" />&nbsp;</td>
	                        </ffi:cinclude>
	                        <ffi:cinclude value1="${DALocations.SortedBy}" value2="LOCATION_ID,REVERSE" operator="equals">
	                            <td class="sectionhead" width="150"><s:text name="jsp.user_189" />&nbsp;</td>
	                        </ffi:cinclude>
	                    </ffi:cinclude>
	                    <ffi:cinclude value1="${DALocations.SortedBy}" value2="LOCATION_ID," operator="equals">
	                        <td class="sectionhead" width="150"><s:text name="jsp.user_189" />&nbsp;</td>
	                    </ffi:cinclude>
	
	                    <td class="sectionhead" width="150"><s:text name="jsp.home.column.label.action" /></td>
	                    <td width="150">&nbsp;</td>
	                    <td width="50">&nbsp;</td>
					</tr>
				</table>
				</div>
				<div class="paneContentWrapper">
				<table width="750" border="0" cellspacing="0" cellpadding="0">
					<%	int locColor = 0;	%>
	                <ffi:setProperty name="EmptyLocations" value="true"/>
					<ffi:list collection="DALocations" items="Location">
                        <ffi:setProperty name="EmptyLocations" value="false"/>
                        <tr <%= locColor % 2 == 0 ? "class=\"ltrow3\"" : "class=\"dkrow\"" %> >
                        	<ffi:cinclude value1="${Location.UserAction}" value2="" operator="notEquals">
	                            <td class="columndataDA" nowrap width="150"><ffi:getProperty name="Location" property="LocationName"/></td>
	                            <td class="columndataDA" nowrap width="150"><ffi:getProperty name="Location" property="LocationID"/></td>
	                            <td class="columndataDA" nowrap width="150"><ffi:getProperty name="Location" property="UserAction"/></td>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${Location.UserAction}" value2="" operator="equals">
	                            <td class="columndata" nowrap width="150"><ffi:getProperty name="Location" property="LocationName"/></td>
	                            <td class="columndata" nowrap width="150"><ffi:getProperty name="Location" property="LocationID"/></td>
	                            <td class="columndata" nowrap width="150"><ffi:getProperty name="Location" property="UserAction"/></td>
                            </ffi:cinclude>
                            <td class="columndata" align="right" width="150">
                            	<ffi:cinclude value1="${Location.ChildSequence}" value2="" operator="equals">
                           			<ffi:setProperty name="childSequence" value="<%= String.valueOf(locColor)%>"/>
                           		</ffi:cinclude>
                           		<ffi:cinclude value1="${Location.ChildSequence}" value2="" operator="notEquals">
                           			<ffi:setProperty name="childSequence" value="${Location.ChildSequence}"/>
                           		</ffi:cinclude>
                            	<ffi:cinclude value1="${Location.UserAction}" value2="<%= IDualApprovalConstants.USER_ACTION_DELETED%>" operator="notEquals">
	                            	<ffi:cinclude value1="${Location.UserAction}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED%>">
										<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>
										<s:url id="locationEditURL" value="%{#session.PagesPath}user/corpadminlocedit.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
												<s:param name="editlocationreload" value="%{'true'}"/>
												<s:param name="fromDivEdit" value="%{'true'}"/>
												<s:param name="childSequence" value="%{#session.childSequence}"/>
												<s:param name="EditLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
										</s:url>
										<sj:a
											href="%{locationEditURL}" 
											targets="viewLocationDialogID" 
											button="true" 
											title="Edit"
											onClickTopics=""
											onCompleteTopics="openViewLocationDialog"
										>
												<s:text name="jsp.default_178" />
										</sj:a>	
	                                </ffi:cinclude>
	                                <ffi:cinclude value1="${Location.UserAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED%>">
										<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>
										<s:url id="locationEditURL" value="%{#session.PagesPath}user/da-location-edit.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
												<s:param name="editlocationreload" value="%{'true'}"/>
												<s:param name="childSequence" value="%{#session.childSequence}"/>
												<s:param name="EditLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
										</s:url>
										<sj:a
											href="%{locationEditURL}" 
											targets="viewLocationDialogID" 
											button="true" 
											title="Edit"
											onClickTopics=""
											onCompleteTopics="openViewLocationDialog"
										>
												<s:text name="jsp.default_178" />
										</sj:a>	
	                                </ffi:cinclude>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${Location.UserAction}" value2="<%= IDualApprovalConstants.USER_ACTION_DELETED%>">
										<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>	
										<s:url id="locationViewURL" value="%{#session.PagesPath}user/corpadminlocview.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
												<s:param name="editlocationreload" value="%{'true'}"/>
												<s:param name="EditLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
										</s:url>
										<sj:a
											href="%{locationViewURL}" 
											targets="viewLocationDialogID" 
											button="true" 
											title="View"
											onClickTopics=""
											onCompleteTopics="openViewLocationDialog"
										>
												<s:text name="jsp.default_6" />
										</sj:a>	
                                </ffi:cinclude>
                                <ffi:cinclude value1="${Location.UserAction}" value2="">
										<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>
										<s:url id="locationEditURL" value="%{#session.PagesPath}user/corpadminlocedit.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
												<s:param name="fromDivEdit" value="%{'true'}"/>
												<s:param name="editlocationreload" value="%{'true'}"/>
												<s:param name="EditLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
										</s:url>
										<sj:a
											href="%{locationEditURL}" 
											targets="viewLocationDialogID" 
											button="true" 
											title="Edit"
											onClickTopics=""
											onCompleteTopics="openViewLocationDialog"
										>
												<s:text name="jsp.default_178" />
										</sj:a>	
	                             </ffi:cinclude>
                            </td>
                            <td class="columndata" align="right" width="50">
                            	<ffi:cinclude value1="${Location.UserAction}" value2="<%= IDualApprovalConstants.USER_ACTION_DELETED%>" operator="notEquals">
	                               	<ffi:cinclude value1="${Location.UserAction}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED%>" operator="notEquals">
	                               		<ffi:cinclude value1="${Location.ChildSequence}" value2="" operator="equals">
	                               			<ffi:setProperty name="childSequence" value="<%= String.valueOf(locColor)%>"/>
	                               		</ffi:cinclude>
	                               		<ffi:cinclude value1="${Location.ChildSequence}" value2="" operator="notEquals">
	                               			<ffi:setProperty name="childSequence" value="${Location.ChildSequence}"/>
	                               		</ffi:cinclude>
										<ffi:setProperty name="LocationBPWID"  value="${Location.LocationBPWID}"/>
										<ffi:setProperty name="DeleteLocation_LocationName"  value="${Location.LocationName}"/>
										<s:url id="locationDeleteURL" value="%{#session.PagesPath}user/corpadminlocdelete-verify.jsp" escapeAmp="false">
												<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
												<s:param name="DeleteLocation_LocationName" value="%{#session.DeleteLocation_LocationName}"/>
												<s:param name="childSequence" value="%{#session.childSequence}"/>
												<s:param name="DeleteLocation_LocationBPWID" value="%{#session.LocationBPWID}"/>
										</s:url>
										<sj:a
											href="%{locationDeleteURL}" 
											targets="deleteLocationDialogID" 
											button="true" 
											title="Delete"
											onClickTopics=""
											onCompleteTopics="openDeleteLocationDialog"
										>
												<s:text name="jsp.default_162" />
										</sj:a>	
									 </ffi:cinclude>
								</ffi:cinclude>
						    </td>
						   
                         </tr>
                        <%	locColor++;	%>
					</ffi:list>
	                <ffi:cinclude value1="${EmptyLocations}" value2="true" operator="equals">
	                    <tr class="ltrow3">
	                        <td colspan="5" class="columndata">
	                            <!--L10NStart-->No Locations Available<!--L10NEnd-->
	                        </td>
	                    </tr>
                        <%	locColor++;	%>
	                </ffi:cinclude>
	
				<ffi:setProperty name="PageScope_LocColor" value="<%= String.valueOf(locColor)%>" />
				<tr>
				<td colspan="5" align="center">
				<form action="<ffi:urlEncrypt url="${SecurePath}user/corpadminlocadd.jsp?addlocation-reload=true&childSequence=${PageScope_LocColor}" />" name="AddLocation" method="post" >
				<table>
				<tr>
				<ffi:removeProperty name="PageScope_LocColor" />
                    		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<td <%= locColor % 2 == 0 ? "class=\"ltrow3\"" : "class=\"dkrow\"" %> >
								<s:url id="addLocationURL" value="/pages/jsp/user/corpadminlocadd.jsp" escapeAmp="false">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									<s:param name="addlocation-reload" value="true"/>
									<s:param name="childSequence" value="%{#session.PageScope_LocColor}"/>
								</s:url>
								<sj:a id="addLocationLinkInEditDiv"
									  href="%{addLocationURL}"
									  targets="inputDiv"
									  onClickTopics="beforeLoad"
									  onCompleteTopics="completeDivisionLoad"
									  button="true"><s:text name="jsp.user_20"/></sj:a>
							</td>
							</tr>
							</table>
						</form>
						</td>
					</tr>
				</table>
				</div>
				</div>
			</div>
			</ffi:cinclude>
			</ffi:cinclude>
		</div>
	</ffi:cinclude>
		
		
<script>
	$(document).ready(function(){
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#editDivFormId").setFocus();
		}
		
		/* $('#locationEditTableID').pane({
			title: 'Location'
		}); */
	});
</script>		
		
<ffi:setProperty name="onCancelGoto" value="corpadmindivedit.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmindivedit.jsp"/>
<ffi:removeProperty name="LastRequest"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadmindivedit.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
<ffi:removeProperty name="BusinessEmployees"/>
<ffi:removeProperty name="oldDAObject"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
<ffi:removeProperty name="NewAdminUserStringList"/>
<ffi:removeProperty name="OldAdminStringList"/>
<ffi:removeProperty name="GetDACategoryDetails"/>
<ffi:removeProperty name="MultipleCategories"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
