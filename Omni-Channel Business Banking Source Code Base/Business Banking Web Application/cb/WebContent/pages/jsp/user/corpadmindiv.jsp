<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_corpadmindiv" className="moduleHelpClass"/>
<ffi:removeProperty name="DisplayLocation"/>

<ffi:setGridURL grid="GRID_adminDivision" name="DeleteURL" url="/cb/pages/jsp/user/corpadmindivdelete-verify.jsp?DeleteGroup_GroupId={0}&DeleteDivision_DivisionName={1}" parm0="GroupId" parm1="Division"/>
<ffi:setGridURL grid="GRID_adminDivision" name="EditURL" url="/cb/pages/jsp/user/corpadmindivedit.jsp?EditDivision_GroupId={0}&DisplayLocation=false&UseLastRequest=FALSE" parm0="GroupId"/>
<ffi:setGridURL grid="GRID_adminDivision" name="PermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Divisions&EditGroup_GroupId={0}" parm0="GroupId"/>
<ffi:setGridURL grid="GRID_adminDivision" name="LocationsURL" url="/cb/pages/jsp/user/corpadminlocations-selected.jsp?EditDivision_GroupId={0}" parm0="GroupId"/>
<ffi:setGridURL grid="GRID_adminDivision" name="AdminEditURL" url="/cb/pages/jsp/user/corpadminadminedit-selected.jsp?EditDivision_GroupId={0}&AdminEditType=Division" parm0="GroupId"/>

 <ffi:setProperty name="adminDivisionGridTempURL" value="getBusinessGroups.action?CollectionName=Descendants&GridURLs=GRID_adminDivision&dualApprovalMode=2" URLEncrypt="true"/>
 <s:url namespace="/pages/user" id="remoteurl" action="%{#session.adminDivisionGridTempURL}" escapeAmp="false"/>
     <sjg:grid
       id="adminDivisionGrid"
       caption=""
	   sortable="true"  
       dataType="json"
       href="%{remoteurl}"
       pager="true"
       viewrecords="true"
       gridModel="gridModel"
	   rowList="%{#session.StdGridRowList}" 
	   rowNum="%{#session.StdGridRowNum}" 
       rownumbers="false"
       scroll="false"
       altRows="true"
       sortname="NAME"
       sortorder="asc"
       shrinkToFit="true"
	   subGridUrl="corpadminlocations.jsp"
	   navigator="true"
	   navigatorSearch="false"
	   navigatorAdd="false"
	   navigatorDelete="false"
	   navigatorEdit="false"
	   navigatorRefresh="false"
	   onGridCompleteTopics="addGridControlsEvents,selectDefaultDivision"	   
     >
     <sjg:gridColumn name="groupName" width="300" index="NAME" title="%{getText('jsp.default_283')}" sortable="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="groupId" width="300" index="groupId" title="%{getText('jsp.default_283')}" sortable="true" cssClass="datagrid_textColumn" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.Administrator" width="250" index="administrator" title="%{getText('jsp.user_38')}" sortable="false" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatDivisionActionLinks" search="false" width="100"  hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
	 <sjg:gridColumn name="map.IsAnAdminOf" index="IsAnAdminOf" title="%{getText('jsp.user_184')}" sortable="false" hidden="true" hidedlg="true" width="0" cssClass="datagrid_actionIcons"/>
	 <sjg:gridColumn name="map.LocationsAdmin" index="LocationsAdmin" title="%{getText('jsp.user_191')}" sortable="false" hidden="true" hidedlg="true" width="0" cssClass="datagrid_actionIcons"/>
 </sjg:grid>

<script type="text/javascript">

	var fnCustomDivisionRefresh = function (event) {
		ns.common.forceReloadCurrentSummary();
	};

	$("#adminDivisionGrid").data('fnCustomRefresh', fnCustomDivisionRefresh);
	
	$("#adminDivisionGrid").data("supportSearch",true);

	//disable sortable rows
	if(ns.common.isInitialized($("#adminDivisionGrid tbody"), "ui-sortable")){
		$("#adminDivisionGrid tbody").sortable("destroy");
	}

	$("#adminDivisionGrid").jqGrid('setColProp','ID',{title:false});
</script>

