<%@page import="com.ffusion.beans.entitlementprofile.BusinessUserProfile"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="backProfileLink"
                 value="/pages/jsp/businessuserprofile/modifyBusinessUserProfile_init.action"
                 URLEncrypt="false"/>

<s:url id="showReturnProfileUrl" value="%{#session.backProfileLink}" escapeAmp="false">  
	<s:param name="OneAdmin" value="%{#parameters.OneAdmin}" />
	<s:param name="employeeID" value="BusinessUserProfile.ProfileEntGroupId"/>
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
</s:url>
<script type="text/javascript"
	src="<s:url value='/web'/>/js/user/profiles.js"></script>
<ffi:help id="user_corpadminprofileclone" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_142')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<span id="PageHeading" style="display:none;"><s:text name="jsp.user_142"/></span>

<script language="javascript">
ns.users.entitlementProfile={};
jQuery( document ).ready(function( $ ) {
	var parentProfileId = "<s:property value='parentProfileId' />";
	$("#details").show();
	var groupSelectWiz = new selectWizard();
	groupSelectWiz.init("selectProfileGroupID", {width:250});
	groupSelectWiz.disable();
});

</script>

<%-- Retrieve the list of admins on this group --%>
<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="Admins"/>
<ffi:process name="GetAdminsForGroup"/>

<ffi:object id="GroupAccounts" name="com.ffusion.beans.accounts.Accounts" scope="session"/>

<%-- Get the entitlementGroup for this new user back to session --%>
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EntitlementGroupIdStr}"/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:setProperty name="checkedfalse" value=""/>
<ffi:setProperty name="checkedtrue" value="checked"/>
<%-- This BackURL is also used when state list is dynamically updated --%>
<ffi:setProperty name="BackURL" value="${SecurePath}user/businessuserprofileedit.jsp"/>

<div align="center">
	<span id="formerrors"></span>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="adminBackground">
				<s:form id="cloneBusinessProfileFormId" namespace="/pages/jsp/businessuserprofile" action="cloneBusinessUserProfile_verify" theme="simple" name="CloneBusinessUserForm" method="post" >
				<table width="100%" border="0" cellspacing="0" cellpadding="3" align="left">
                  	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                  	<tr>
					<td width="250px" align="left" colspan="1">
						<span class="sectionsubhead">Source Profile:</span>
					</td>
					<td> 
						<ffi:getProperty name="CloneBusinessUserProfile" property="ProfileName"/>
					</td>
					<td align="left">&nbsp</td>
				</tr>
				<tr>
					<td width="250px" align="left">
						<span class="sectionsubhead">New <s:text name="jsp.user_267"/>:</span><span class="required">*</span>
					</td>
					<td> 
						<input tabindex="14" class="ui-widget-content ui-corner-all" type="text" name="CloneBusinessUserProfile.ProfileName" value="<ffi:cinclude value1="${TempBusEmployee.Id}" value2="" operator="equals"><ffi:getProperty name="CloneBusinessUserProfile" property="ProfileName"/></ffi:cinclude><ffi:cinclude value1="${TempBusEmployee.Id}" value2="" operator="notEquals"><ffi:getProperty name="TempBusEmployee" property="ProfileName"/></ffi:cinclude>" size="35" maxlength="200" border="0">
						<span id="CloneBusinessUserProfile.profileNameError"></span>
					</td>
					<td align="left">&nbsp</td>
				</tr>
				<tr>
					<td align="left">New <s:text name="jsp.default_171"/> </td>
					<td align="left"><textarea cols="40" name="CloneBusinessUserProfile.description" class="ui-widget-content"><s:property value="CloneBusinessUserProfile.description" /></textarea></td>
					<td align="left">&nbsp</td>
				</tr>
				<tr>
					<td><s:text name="jsp.user.label.Supported.Channels"/>: </td>
					<td align="left">&nbsp</td>
					<td align="left">&nbsp</td>
				</tr>
				<s:iterator value="selectedProfileChannels" var="entList">
					<s:set name="channelSelected" value="%{'false'}" />
					<tr>
						<td valign="bottom" align="right">
						  &nbsp;
						</td>
						<td valign="bottom" align="left" class="sectionsubhead">
						  	<input type="checkbox" name="channels" value="<s:property  value='channelId'/>"/>
						  	<span class="sectionsubhead"><b><s:property  value='#entList.channelId'/></b></span>
						  	<span class="channelId-padding">&#40; <s:property  value='#entList.description'/> &#41; </span>
						 </td>
					</tr>
				</s:iterator>
				<tr>
				  <td>&nbsp</td>
				  <td><span id="CloneBusinessUserProfile.channelListError"></span></td>
				</tr>
				<tr>
					<td valign="bottom" align="left">
					  Profile Status:
					</td>
					<td valign="bottom" align="left" class="sectionsubhead">
					  	<input type="radio" name="CloneBusinessUserProfile.status" value="ACTIVE"> Active
					  	<input type="radio" name="CloneBusinessUserProfile.status" value="INACTIVE" checked> InActive
					 </td>
				</tr>
				<s:if test="isProfileInUse()">
              			<tr>
              			  <td colspan="3" align="center">
              			  	<span class="required"><s:text name="jsp.user.error.user.assigned.profile" /></span>
              			  </td>
              			</tr>
             			</s:if><tr>
							<td colspan="3" valign="top">
								<div align="center">
									<span class="required">* <s:text name="jsp.default_240"/></span>
									<br>
									<br>
									<s:url id="resetCloneProfileButtonUrl" value="/pages/jsp/businessuserprofile/cloneBusinessUserProfile_init.action" escapeAmp="false">  
											<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
											<s:param name="EmployeeID" value="%{#request.CloneBusinessUserProfile.ProfileEntGroupId}"></s:param>		
										</s:url>	
										<sj:a id="resetCloneProfileBtn"
										   href="%{resetCloneProfileButtonUrl}"
										   targets="inputDiv"
										   button="true" ><s:text name="jsp.default_358"/></sj:a>
									<sj:a
										  button="true"
										  summaryDivId="profileSummary" 
										  buttonType="cancel"
										  onClickTopics="showSummary,cancelProfileForm"><s:text name="jsp.default_82"/></sj:a>
									<sj:a
											id="cloneProfileBtnId"
											formIds="cloneBusinessProfileFormId"
											targets="verifyDiv"
											button="true"
											validate="true"
											validateFunction="customValidation"
											onBeforeTopics="beforeVerify"
											onCompleteTopics="completeVerify"
											onErrorTopics="errorVerify"
											onSuccessTopics="successVerify"
											><s:text name="jsp.default_366"/></sj:a>

								</div>
							</td>
						</tr>
				</table>
				</s:form>
			</td>
		</tr>
	</table>
</div>
<%-- set Entitlement_CanAdminister back to indicate logged in users' administer right--%>
<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:removeProperty name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="EmployeeCanAdministerAnyGroup"/>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>
<ffi:removeProperty name="TempBusEmployee"/>
<ffi:removeProperty name="Country_List"/>
<ffi:removeProperty name="GetStatesForCountry"/>