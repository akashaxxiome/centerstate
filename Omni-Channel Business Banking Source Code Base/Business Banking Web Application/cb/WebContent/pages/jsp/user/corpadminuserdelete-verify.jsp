<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminuserdelete-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_371')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<%
    String userID = request.getParameter("SetBusinessEmployee.Id");
%>

<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
<ffi:setProperty name="SetBusinessEmployee" property="Id" value="<%=userID%>"/>
<ffi:setProperty name="SetBusinessEmployee" property="BusinessEmployeesSessionName" value="BusinessEmployees"/>
<ffi:process name="SetBusinessEmployee"/>
<ffi:removeProperty name="SetBusinessEmployee"/>

<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EntitlementGroupIdStr}"/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:object id="RemoveBusinessUser" name="com.ffusion.tasks.admin.RemoveBusinessUser" scope="session"/>
<% session.setAttribute("FFIRemoveBusinessUser", session.getAttribute("RemoveBusinessUser")); %>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<div id="wholeworld">
		<div align="center">
					<s:form namespace="/pages/user" action="deleteBusinessUser_verify" theme="simple" name="RemoveUserForm" id="RemoveUserFormID" method="post">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<div align="center" class="marginTop20">
						<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuserdelete-verify.jsp-1" parm0="${BusinessEmployee.FullName}"/>
					</div>
					<div align="center" class="ui-widget-header customDialogFooter">
						<sj:a id="cancelDeleteUserLink" button="true" onClickTopics="closeDialog"
							  title="%{getText('jsp.default_83')}" 
							  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_82" /></sj:a>
						<sj:a id="deleteUserLink"
							  formIds="RemoveUserFormID"
							  targets="wholeworld"
							  button="true"
							  title="%{getText('jsp.user_400')}"
							  effectDuration="1500"
							  focusablecontainer="wholeworld"
							  onCompleteTopics="deleteUserVerifyCompleteTopics,tabifyDialog,setFocusTopic"
							  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_162" /></sj:a>
					</div>
					</s:form>
		</div>
</div>

