<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<ffi:setL10NProperty name='PageHeading' value='Pending Approval Summary'/>

<%	
	if( request.getParameter("rejectFlag") != null ) { 
		String rejectFlag = request.getParameter("rejectFlag");
		session.setAttribute("rejectFlag", rejectFlag); 
	}
%>

<%--  
<ffi:include page="${PathExt}user/inc/da-session-cleanup.jsp"/>
--%>

<%-- 
<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<ffi:include page="${PathExt}inc/init/admin-init.jsp" />
</ffi:cinclude>
--%>

<s:include value="inc/da-summary-common.jsp" />

<ffi:setProperty name='PageText' value=''/>

<ffi:object id="GetCategories" name="com.ffusion.tasks.dualapproval.GetCategories" />
	<ffi:setProperty name="GetCategories" property="itemId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetCategories" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_BUSINESS %>"/>
	<ffi:setProperty name="GetCategories" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetCategories"/>

<ffi:removeProperty name="DISABLE_APPROVE"/>

<%-- BAI Export Settings --%>
<ffi:object name="com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA" id="AddBAIExportSettingsToDA" scope="session" />
<ffi:setProperty name="AddBAIExportSettingsToDA" property="ReadFromDA" value="true" />
<ffi:setProperty name="AddBAIExportSettingsToDA" property="TmpBankIdDisplayText" value="${TempBankIdentifierDisplayText}" /> 
<ffi:process name="AddBAIExportSettingsToDA" />
<%-- END BAI Export Settings --%>

<%-- Dual Approval for Transaction Groups --%>
<ffi:object id="GetCompanyTransGroupsDA" name="com.ffusion.tasks.dualapproval.GetCompanyTransGroupsDA" />
<ffi:setProperty name="GetCompanyTransGroupsDA" property="ReadFromDA" value="true" />								
<ffi:process name="GetCompanyTransGroupsDA" />
<%-- END Dual Approval for Transaction Groups --%>


<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >

<ffi:setProperty name="SuccessUrl" value="${SecurePath}user/corpadmininfo.jsp" />

<div align="center">
	<ffi:setProperty name="subMenuSelected" value="company"/>
	<ffi:object name="com.ffusion.tasks.dualapproval.ProcessDACategories" id="ProcessDACategories" />
	<ffi:process name="ProcessDACategories" />
	<s:include value="inc/da-processwizardmenu.jsp" />
</div>
<ffi:setProperty name="BackURL" value="${SecurePath}user/da-business-verify.jsp"/>
<ffi:removeProperty name="categories"/>
