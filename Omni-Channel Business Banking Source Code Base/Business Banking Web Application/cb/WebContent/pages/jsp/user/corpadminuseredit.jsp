<%@page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees"%>
<%@ page import="com.ffusion.beans.user.BusinessEmployee"%>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember"%>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroup"%>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroups"%>
<%@ page import="com.ffusion.beans.user.User"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<style type="text/css">
/* #EditBUsinessUserFormID input[type="text"], #EditBUsinessUserFormID input[type="password"]{width: 200px;} */
#EditBUsinessUserFormID .selectBoxHolder input{width: 216px;}
#EditBUsinessUserFormID .errorLabel{display: block;}
</style>
<ffi:setProperty name="channelListLink"
                 value="/pages/user/editBusinessUser_getChannelsForProfile.action"
                 URLEncrypt="false"/>
<s:url id="getChannelsForProfileUrl" value="%{#session.channelListLink}" escapeAmp="false"> 
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>				
</s:url>

<script type="text/javascript"
	src="<s:url value='/web'/>/js/user/profiles.js"></script>
<ffi:help id="user_corpadminuseredit" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_146')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<span id="PageHeading" style="display:none;"><s:text name="jsp.user_146"/></span>
<% 
	com.ffusion.beans.user.BusinessEmployee emp = (com.ffusion.beans.user.BusinessEmployee)request.getAttribute("businessEmployee");
%>
<%
String userName1=null;
String userName2=null;
%>
<script language="javascript">
	var isSelfLogin = "false";
</script>
<ffi:getProperty name="BusinessEmployee" property="UserName" assignTo="userName1" />
<ffi:getProperty name="<%= com.ffusion.efs.tasks.SessionNames.SECURE_USER %>" property="UserName" assignTo="userName2" />
<ffi:cinclude value1="<%= userName1 %>" value2="<%= userName2 %>" operator="equals">
<script language="javascript">
	isSelfLogin = "SELF";
</script>
</ffi:cinclude>
<ffi:setProperty name="userId" value="<%=emp.getId()%>"/>
<script language="javascript">
var channelUrl = "<ffi:getProperty name='getChannelsForProfileUrl'/>";


jQuery( document ).ready(function( $ ) {
	var entitlementGroupId = $("#selectGroupID").val();
	
	$("#crossProfilesId").show();
	//Call the method to get the channelprofiles for the user.
	getChannelProfilesforEntId(entitlementGroupId);
});

//TODO: Move this in the jquery onReady method.//TODO: Move this in the jquery onReady method.
$(function(){
	$("#selectCountryID").combobox({width: 240});
	$("#selectUserStateID").combobox({width: 240,searchFromFirst:true});
	$("#selectLanguageID").selectmenu({width: 240});
	var type = $("#selectGroupID").attr("type");
	if(type == undefined) {
		$("#selectGroupID").selectmenu({width: 240});
	}
	$("#selectStatusID").selectmenu({width: 240});
	$("#selectAdminID").selectmenu({escapeHtml:true, width: 240});
	ns.users.entitlementProfile={};
	$("select[data-sap-banking-channelType]").selectmenu({width: 240});
});

function showState(obj) {
	var urlString = '/cb/pages/jsp/user/inc/country-state-useredit.jsp';
	$.ajax({
		   type: 'POST',
		   url: urlString,
		   data: "countryCode=" + obj.value + "&CSRF_TOKEN=" + '<ffi:getProperty name='CSRF_TOKEN'/>',
		   success: function(data){
		    if(data.indexOf("option") != -1) {
		    	$("#stateNameId").html('<span class="<ffi:getPendingStyle fieldname="state" defaultcss="sectionhead" dacss="sectionheadDA"/>"><s:text name="jsp.default_386"/>:</span>');
		    } else {
		    	$("#stateNameId").html('<span></span>');
		    }
		   	$("#stateSelectId").html(data);
	   }
	});
}
// submit the form to update the entitlement group and other items
function updateEntGroup(obj) {
	// Get the channel profiles for the entitlement group ID 
	var entitlementGroupId = $("#selectGroupID").val();
	$.ajax({
		type: "POST",
		data: $("#EditBUsinessUserFormID").serialize(),
		url: "/cb/pages/user/editBusinessUser_updateEntGroupForEditUser.action?entitlementGroupId="+entitlementGroupId,
		success: function(data){
			$("#adminProfileList").html(data);
			$("#selectAdminID").selectmenu({escapeHtml:true, width: 240});
			getChannelProfilesforEntId(entitlementGroupId, true);
			$("#crossChannelChildId").empty();
	   }
	});
}

function getChannelProfilesforEntId(entitlementGroupId, isGroupChange) {
	var usingEntitlementProfile = '<ffi:getProperty name="Business" property="usingEntProfiles"/>';
	var isMigration = '<s:property value="migration" />';
	
	if(usingEntitlementProfile && usingEntitlementProfile == 'true' || isMigration == 'true') {
		var editUserId = '<ffi:getProperty name="userId" />';
		$.ajax({
				type: "POST",
				data: $("#AddUserFormID").serialize(),
				url: "/cb/pages/user/addBusinessUser_getUserProfilesList.action?entitlementGroupId="+entitlementGroupId + "&userId=" + editUserId
					+ "&CSRF_TOKEN=" + ns.home.getSessionTokenVarForGlobalMessage,
				success: function(response){
					$("#sharedProfilesId").html(response);
					crossSel = new selectWizard();
					crossSel.init("selectEntitlementGroup_X", {width:200});
					crossSel.addChangeEvent(crossProfileChange);
					var selectedProfile = $("#selectEntitlementGroup_X").val();
					var option = $('option:selected', $("#selectEntitlementGroup_X")).attr('scope');
					if(option == 1 || isSelfLogin == "SELF") {
						crossSel.disable();
					}
					if(selectedProfile != undefined && selectedProfile != "-1") {
						populateChannelsForProfile(selectedProfile);
					}
			   }
		});
	}
}

</script>

<s:include value="/pages/jsp/user/password-indicator-js.jsp" />
<s:include value="/pages/jsp/user/corpadmin_js.jsp" />


<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>
 <ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="notEquals">
 	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
 </ffi:cinclude>

<div id="resetDiv"  style="display:none;"></div>
 
<!-- fromPendingTable hidden variable represents if request is from user or pending approval island. -->
<input type="hidden" name="fromPendingTable" value="<ffi:getProperty name='fromPendingTable'/>" >
<div align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td colspan="5" align="center">
				<ul id="formerrors"></ul>
			</td>
		</tr>
		<%-- <tr>
			<td colspan="2">
				<div align="center">
					<span class="sectionsubhead"><br><ffi:getProperty name="BusinessName"/> <ffi:cinclude value1="DivisionName" value2="" operator="equals"> / <ffi:getProperty name="DivisionName"/></ffi:cinclude>&nbsp;<br>
		<br>
	</span></div>
		</td>
	</tr> --%>
</table>
<s:form namespace="/pages/user" action="editBusinessUser_verify" theme="simple" name="EditBusinessUserForm" id="EditBUsinessUserFormID" method="post" >
<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<tr>
		<td colspan="4"><h3 class="transactionHeading"><s:text name="admin.login.information" /></h3></td>
	</tr>
	<tr>
		
		<td width="25%" class="adminBackground" valign="baseline">
			<div align="left">
			<span class='<ffi:daPendingStyle
			newFieldValue="${BusinessEmployee.UserName}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.UserName}" daUserAction="${DABusinessEmployee.Action}" 
			dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.default_455"/>
			<ffi:cinclude value1='<%= userName1 %>' value2='<%= userName2 %>' operator='notEquals' ></span><span class="required">*</span></ffi:cinclude></div>
		</td>
		<td class="adminBackground" width="25%" >
			<span <s:if test="%{isPasswordModified}">class='sectionheadDA'</s:if><s:else>class='sectionsubhead'</s:else>>
			<s:text name="jsp.user_233"/></span><span class="required">*</span>
		</td>
		<td class="adminBackground" width="25%">
			<span <s:if test="%{isPasswordModified}">class='sectionheadDA'</s:if><s:else>class='sectionsubhead'</s:else>>
			<s:text name="jsp.user_92"/>
			</span><span class="required">*</span>
		</td>
		<td width="25%" class="adminBackground" valign="baseline">
			<span class='<ffi:daPendingStyle
					newFieldValue="${BusinessEmployee.EntitlementGroupId}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.EntitlementGroupId}" daUserAction="${DABusinessEmployee.Action}" 
					dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.default_225"/>
			</span>
			<ffi:cinclude value1="<%= userName1 %>" value2="<%= userName2 %>" operator="notEquals"><span class="required">*</span></ffi:cinclude>
		</td>
	</tr>
	<tr class="adminUserTr">
		<td class="adminBackground columndata">
			<%
			if( userName1!=null && userName1.equalsIgnoreCase( userName2 ) ) {
			%>
				<ffi:getProperty name="BusinessEmployee" property="UserName"/>
			<%
			}else{
			%>
				<input tabindex="14" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.UserName" value="<ffi:getProperty name="BusinessEmployee" property="UserName"/>" size="24" maxlength="20" border="0">
			<%
			}
			%>
			<span id="BusinessEmployee.userNameError"></span>
			<span class="sectionhead_greyDA">
				<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${BusinessEmployee.UserName}" value2="${BusinessEmployee.MasterBusinessEmployee.UserName}" operator="notEquals">
					<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.UserName"/>
				</ffi:cinclude>
				</ffi:cinclude>	
			</span>
		</td>
		<td class="adminBackground" width="200" valign="top" align="left">
			<div style="width:242px;" id="userPasswordDvc">
			<input tabindex="15" class="ui-widget-content ui-corner-all inputPassword" width:237px;" id="editPswd"  type="password" name="BusinessEmployee.Password" size="24" maxlength="50" border="0" value="">
			</div>
			<span id="BusinessEmployee.passwordError"></span>
		</td>
		<td class="adminBackground" width="200" valign="top" align="left">
			<input tabindex="16" class="ui-widget-content ui-corner-all" type="password" name="BusinessEmployee.ConfirmPassword" size="24" maxlength="50" border="0" value="<ffi:getProperty name="BusinessEmployee" property="ConfirmPassword"/>">
			<span id="BusinessEmployee.confirmPasswordError"></span>
		</td>
		<td class="adminBackground columndata"   valign="baseline" style="text-align:left">
			<%-- Allow changing group only if the user is not changing its own group --%>
			<ffi:cinclude value1="<%= userName1 %>" value2="<%= userName2 %>" operator="equals">
			
			<ffi:list collection="GroupSummaries" items="group" >
				<ffi:list collection="group" items="spaces, groupName, groupId, numUsers" >
					<ffi:cinclude value1="${groupId}" value2="0" operator="notEquals" >
					<s:if test="%{#session.Business.usingEntProfiles == true}">
					  <ffi:cinclude value1="${groupId}" value2="${BusinessEmployee.map.PARENT_ENT_GROUP_ID}" operator="equals" >
					  	 <ffi:getProperty name="groupName" />
					  	 <input type="hidden" name="entGroup" id="selectGroupID" value="<s:property value='BusinessEmployee.map.PARENT_ENT_GROUP_ID'/>" />
					  </ffi:cinclude>
					  </s:if>
					  <s:else>
					  	 <ffi:cinclude value1="${groupId}" value2="${BusinessEmployee.EntitlementGroupId}" operator="equals" >
					  	 <ffi:getProperty name="groupName" />
					  	 <input type="hidden" name="entGroup" id="selectGroupID" value="<s:property value='BusinessEmployee.EntitlementGroupId'/>" />
					  </ffi:cinclude>
					  </s:else>
					  </ffi:cinclude>
				</ffi:list>
			</ffi:list>
			</ffi:cinclude>
	
			<ffi:cinclude value1="<%= userName1 %>" value2="<%= userName2 %>" operator="notEquals">
				<ffi:cinclude value1="${GroupSummaries.Size}" value2="0" operator="notEquals">
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<%-- Restore the value selected by the user for dual approval.--%>
				<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${BusinessEmployee.EntitlementGroupId}" value2="${BusinessEmployee.MasterBusinessEmployee.EntitlementGroupId}" operator="notEquals">
					<ffi:cinclude value1="${BusinessEmployee.MasterBusinessEmployee.EntitlementGroupId}" value2="0" operator="notEquals">
						<ffi:setProperty name="BusinessEmployee" property="EntitlementGroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
					</ffi:cinclude>
				</ffi:cinclude>
				</ffi:cinclude>
				</ffi:cinclude>
					<s:if test="%{#session.Business.usingEntProfiles == true}">
						<select id="selectGroupID" class="txtbox" tabindex="20" name="Xa" onchange="updateEntGroup(this);">
						<option value="-1"><s:text name="jsp.default_375"/></option>
							<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.map.PARENT_ENT_GROUP_ID}" />
					</s:if>
					<s:elseif test="%{migration == true}">
					  <select id="selectGroupID" class="txtbox" tabindex="20" name="Xa" onchange="updateEntGroup(this);">
						<option value="-1"><s:text name="jsp.default_375"/></option>
							<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.EntitlementGroupId}" />
					</s:elseif>
					<s:else>
						<select id="selectGroupID" class="txtbox" tabindex="20" name="BusinessEmployee.EntitlementGroupId" onchange="updateEntGroup(this);">
							<option value="-1"><s:text name="jsp.default_375"/></option>
							<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.EntitlementGroupId}" />
					</s:else>
					<ffi:list collection="GroupSummaries" items="group" >
						<ffi:list collection="group" items="spaces, groupName, groupId, numUsers" >
							<ffi:cinclude value1="${groupId}" value2="0" operator="notEquals" >
								<ffi:setProperty name="Compare" property="value2" value="${groupId}"/>
								<option value="<ffi:getProperty name="groupId" />" <ffi:getProperty name="selected${Compare.Equals}"/> >
									<ffi:getProperty name="spaces" encode="false"/><ffi:getProperty name="groupName" />
								</option>
							</ffi:cinclude>
						</ffi:list>
					</ffi:list>
					</select>
					<span id="BusinessEmployee.entitlementGroupIdError"></span>
					<span class="sectionhead_greyDA">
					<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
					<ffi:cinclude value1="${BusinessEmployee.FaxPhone}" value2="${BusinessEmployee.MasterBusinessEmployee.FaxPhone}" operator="notEquals">
						<ffi:list collection="GroupSummaries" items="group">
							<ffi:list collection="group" items="spaces, groupName, groupId, numUsers" >
								<ffi:cinclude value1="${groupId}" value2="0" operator="notEquals" >
									<ffi:cinclude value1="${BusinessEmployee.MasterBusinessEmployee.EntitlementGroupId}" value2="${groupId}">
									 	<ffi:getProperty name="groupName" />
									</ffi:cinclude>
								</ffi:cinclude>
							 </ffi:list>
						</ffi:list>
					</ffi:cinclude>
					</ffi:cinclude>
					</span>
				</ffi:cinclude>
			</ffi:cinclude>
		</td>
	</tr>
	<tr>
		<td class="4">&nbsp;</td>
	</tr>
	<tr>
		<td class="adminBackground">
			<span class='<ffi:daPendingStyle
				newFieldValue="${BusinessEmployee.AccountStatus}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.AccountStatus}" daUserAction="${DABusinessEmployee.Action}" 
				dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
				<s:text name="jsp.user_364"/>
				</span>										
		</td>		
		<td class="adminBackground">
            <div align="left"><span class='<ffi:getPendingStyle fieldname="primaryAdmin" defaultcss="sectionhead"  dacss="sectionheadDA"/>'><s:text name="jsp.user_264"/></span></div>
        </td>
			
		<td class="adminBackground">
			<div align="left"><span class='<ffi:daPendingStyle
				newFieldValue="${BusinessEmployee.PasswordReminder}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.PasswordReminder}" daUserAction="${DABusinessEmployee.Action}" 
				dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.user_279"/></span></div>
		</td>
		<td class="adminBackground sectionsubhead" nowrap><s:text name="jsp.user_193" /></td>
	</tr>
	<tr class="adminUserTr">
		<s:if test="%{#request.OnlyActiveAdministrator}"> 
			<td class="adminBackground columndata"  align="left">Active</td>
		</s:if>
		<s:else>
		<td class="adminBackground"   align="left" valign="top">
			<select id="selectStatusID" tabindex="19" class="txtbox" name="BusinessEmployee.AccountStatus">
				<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.AccountStatus}" />
				<ffi:setProperty name="accountStatus" value="${BusinessEmployee.AccountStatus}"/>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE )%>"/>
					<option value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE ) %>" <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.default_28"/></option>
					<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL ) %>">
						<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL )%>"/>
						<option  value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL ) %>"  <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.user_235"/></option>
					</ffi:cinclude>
					<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL ) %>" operator="notEquals">
						<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
						<option  value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE ) %>"  <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.default_238"/></option>
						<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE )%>"/>
						<option  value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE ) %>" <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.user_306"/></option>
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
					<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE )%>"/>
					<option value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE ) %>" <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.default_28"/></option>
					<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
					<option  value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE ) %>"  <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.default_238"/></option>
					<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE )%>"/>
					<option  value="<%= String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE ) %>" <ffi:getProperty name="selected${Compare.Equals}"/> ><s:text name="jsp.user_306"/></option>
				</ffi:cinclude>
			</select>
			<br>
			<span class="sectionhead_greyDA">
			
			<ffi:cinclude value1="${BusinessEmployee.AccountStatus}" value2="${BusinessEmployee.MasterBusinessEmployee.AccountStatus}" operator="notEquals">
				<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.MasterBusinessEmployee.AccountStatus}" />
				<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE )%>"/>
				<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.default_28"/></ffi:cinclude>
				<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
				<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.default_238"/></ffi:cinclude>
				<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE )%>"/>
				<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_306"/></ffi:cinclude>
				<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL )%>"/>
				<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_235"/></ffi:cinclude>
			</ffi:cinclude>
			
			</span>
			<span id="BusinessEmployee.accountStatusError"></span>
		</td>
		</s:else>
		<td id="adminProfileList" class="adminBackground columndata">
			<s:include value="/pages/jsp/user/corpadminuser-select-profile-admin.jsp" />
	    </td>
	  <td class="adminBackground columndata" valign="middle">									
			<s:url id="resetURL" value="/pages/user/editBusinessUser_resetPasswordQuestionAndAnswer.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="resetLink"
			   href="%{resetURL}"
			   targets="resetDiv"
			   onSuccessTopics="showSuccessMessageTopic"
			   button="false" cssClass="anchorText"><s:text name="jsp.default_358"/></sj:a>
		</td>
		<td class="adminBackground columndata"   valign="middle" align="left" colspan="4">
		<input type="hidden" name="passwordStatus" id="passwordStatus"/>
			<div id="unlockAccountId">				
				<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="equals">
					<s:text name="jsp.user_155"/>&nbsp;&nbsp;(<input class="anchorText borderNoneBtnCls" type="button" value="<s:text name="jsp.user_350"/>" onclick="ns.admin.unlockAccount('<ffi:urlEncrypt url="/cb/pages/user/unlockUserAction.action?profileId=${BusinessEmployee.Id}"/>');">)
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="equals">
					<s:text name="jsp.user_192"/>&nbsp;&nbsp;(<input class="anchorText borderNoneBtnCls" type="button" value="<s:text name="jsp.user_350"/>" onclick="ns.admin.unlockAccount('<ffi:urlEncrypt url="/cb/pages/user/unlockUserAction.action?profileId=${BusinessEmployee.Id}"/>');">)
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="equals">
					<s:text name="jsp.user_192"/>&nbsp;&nbsp;(<input class="anchorText borderNoneBtnCls" type="button" value="<s:text name="jsp.user_350"/>" onclick="ns.admin.unlockAccount('<ffi:urlEncrypt url="/cb/pages/user/unlockUserAction.action?profileId=${BusinessEmployee.Id}"/>');">)
				</ffi:cinclude>
				<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_EXPIRED) %>" operator="notEquals">
					<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED) %>" operator="notEquals">
						<ffi:cinclude value1="${BusinessEmployee.PASSWORD_STATUS}" value2="<%= String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.PASSWORD_STATUS_LOCKED_BEFORE_CHANGE) %>" operator="notEquals">
						<s:text name="jsp.user_351"/>						
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4" class="adminBackground sectionsubhead" nowrap><s:text name="jsp.user_53"/></td>
	</tr>
	<tr>
		<td colspan="4" class="columndata adminBackground textWrapInTd" valign="baseline" align="left">
			<ffi:getProperty name="adminStrLine"/>
	  	</td>	
	</tr>
	<tr>
	<td colspan="4">
		<span class="required">
			*<ffi:setProperty name="useBCsettings" value="false"/>
			<ffi:setProperty name="changeOwnPassword" value="false"/>
			<s:include value="/pages/jsp-ns/common/password-strength-text.jsp"/>
			<ffi:removeProperty name="changeOwnPassword"/>
			<ffi:removeProperty name="useBCsettings"/>
		</span>
	</td>
	</tr>
	<s:if test="%{#session.Business.usingEntProfiles == true || migration == true}">	
	<tr>
	<td colspan="4"><h3 class="transactionHeading"><s:text name="admin.ent.profiles" /></h3></td>
	</tr>
	<tr>
		<td colspan="4" class="adminBackground" valign="baseline">
			<div align="left"><span class="sectionsubhead"><s:text name="admin.profile.assignment"/></span></div>
		</td>	
	</tr>
	<% String channelProp = ""; %>
	<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
		<s:if test="%{ #request.OldProfileToChannelAssignment[#channel] != null }">
			<s:set var="cssToApply" value="%{'sectionheadDA'}" />
		</s:if>
		<s:else>
			<s:set var="cssToApply" value="%{'sectionsubhead'}" />
		</s:else>
	</ffi:cinclude>
				
	<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>" operator="notEquals">
		<s:set var="cssToApply" value="%{'sectionsubhead'}" />
	</ffi:cinclude>
	<tr id="crossProfilesId" style="display: none;">
		<td id="sharedProfilesId" class="adminBackground" valign="top" align="left" colspan="3">
			
		</td>
	</tr>
	
	</s:if>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="4"><h3 class="transactionHeading"><s:text name="admin.personal.information" /></h3></td>
	</tr>
	<tr>
		<td class="adminBackground">
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
				<div align="left">
				<span class='<ffi:daPendingStyle 
				newFieldValue="${BusinessEmployee.FirstName}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.FirstName}" daUserAction="${DABusinessEmployee.Action}"
				dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
				<s:text name="jsp.user_161"/>
				</span>
				<span class="required">*</span>
				</div>
		</ffi:cinclude>
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">&nbsp;</ffi:cinclude>
		</td>
		
	 	<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
			<td align="left" class="sectionsubhead adminBackground">
			<span class='<ffi:daPendingStyle 
			newFieldValue="${BusinessEmployee.LastName}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.LastName}" daUserAction="${DABusinessEmployee.Action}"
			dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
				<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
						<s:text name="jsp.default_283"/><span class="required">*</span>
			    </ffi:cinclude>
				<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
						<s:text name="jsp.user_185"/><span class="required">*</span>
				</ffi:cinclude>
			</span>
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
				<td class="adminBackground">
					<div align="left"><span class='<ffi:daPendingStyle
					newFieldValue="${BusinessEmployee.LastName}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.LastName}" daUserAction="${DABusinessEmployee.Action}" 
					dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.default_283"/></span><span class="required">*</span></div>
				</td>
		</ffi:cinclude>
		<td class="adminBackground">
			<span class='<ffi:daPendingStyle
				newFieldValue="${BusinessEmployee.Phone}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.Phone}" daUserAction="${DABusinessEmployee.Action}" 
				dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.user_250"/>
			</span>
		</td>
		<td class="adminBackground">
			<span class='<ffi:daPendingStyle
			newFieldValue="${BusinessEmployee.Email}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.Email}" daUserAction="${DABusinessEmployee.Action}" 
			dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.user_131"/>
			</span>
		</td>
	</tr>
	<tr>
		<td class="adminBackground">
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">&nbsp;</ffi:cinclude>
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
		<input tabindex="1" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.FirstName" value="<ffi:getProperty name="BusinessEmployee" property="FirstName"/>" size="24" maxlength="35" border="0">
		<span id="BusinessEmployee.firstNameError"></span>
		<span class="sectionhead_greyDA">
			<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${BusinessEmployee.FirstName}" value2="${BusinessEmployee.MasterBusinessEmployee.FirstName}" operator="notEquals">
				<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.FirstName"/>
			</ffi:cinclude>
			</ffi:cinclude>
		</span>
		</ffi:cinclude>
		</td>
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
			<td class="adminBackground"   valign="top" align="left">
				<input tabindex="2" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.LastName" value="<ffi:getProperty name="BusinessEmployee" property="LastName"/>" size="24" maxlength="35" border="0">
				<span id="BusinessEmployee.lastNameError"></span>
				<span class="sectionhead_greyDA">
						<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
						<ffi:cinclude value1="${BusinessEmployee.LastName}" value2="${BusinessEmployee.MasterBusinessEmployee.LastName}" operator="notEquals">
							<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.LastName"/>
						</ffi:cinclude>
						</ffi:cinclude>											
				</span>
			</td>
		</ffi:cinclude>
		<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
				<td class="adminBackground"   valign="top" align="left">
					<input tabindex="2" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.LastName" value="<ffi:getProperty name="BusinessEmployee" property="LastName"/>" size="24" maxlength="35" border="0">
					<span class="sectionhead_greyDA">
						<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
						<ffi:cinclude value1="${BusinessEmployee.LastName}" value2="${BusinessEmployee.MasterBusinessEmployee.LastName}" operator="notEquals">								
							<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.LastName"/>
						</ffi:cinclude>
						</ffi:cinclude>	
					</span>
				</td>
		</ffi:cinclude>
		<td class="adminBackground">
			<input tabindex="10" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.Phone" value="<ffi:getProperty name="BusinessEmployee" property="Phone"/>" size="24" maxlength="14" border="0">
			<span id="BusinessEmployee.phoneError"></span>
			<span class="sectionhead_greyDA">
					<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
					<ffi:cinclude value1="${BusinessEmployee.Phone}" value2="${BusinessEmployee.MasterBusinessEmployee.Phone}" operator="notEquals">
						<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.Phone"/>
					</ffi:cinclude>
					</ffi:cinclude>
			</span>
		</td>
		<td class="adminBackground">
			<input tabindex="13" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.email" value="<ffi:getProperty name="BusinessEmployee" property="Email"/>" size="24" maxlength="40" border="0">
			<span id="BusinessEmployee.email"></span>
			<span class="sectionhead_greyDA">
				<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${BusinessEmployee.Email}" value2="${BusinessEmployee.MasterBusinessEmployee.Email}" operator="notEquals">
					<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.Email"/>
				</ffi:cinclude>
				</ffi:cinclude>	
			</span>
			<span id="BusinessEmployee.emailError"></span>
		</td>
	</tr>	
	<tr>
		<td class="4">&nbsp;</td>
	</tr>
	<tr>
		<td class="adminBackground">
			<span class='<ffi:daPendingStyle
			newFieldValue="${BusinessEmployee.DataPhone}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.DataPhone}" daUserAction="${DABusinessEmployee.Action}" 
			dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.user_103"/></span>
		</td>
		<td class="adminBackground">
			<span class='<ffi:daPendingStyle 
			newFieldValue="${BusinessEmployee.FaxPhone}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.FaxPhone}" daUserAction="${DABusinessEmployee.Action}"
			dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.user_156"/>
			</span>
		</td>
		<td class="adminBackground">
			<span class='<ffi:daPendingStyle
			newFieldValue="${BusinessEmployee.Street}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.Street}" daUserAction="${DABusinessEmployee.Action}" 
			dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.default_35"/>
			</span>
		</td>
		<td class="adminBackground">
			<span class='<ffi:daPendingStyle
			newFieldValue="${BusinessEmployee.Street2}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.Street2}" daUserAction="${DABusinessEmployee.Action}" 
			dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.default_37"/>
			</span>
		</td>
	</tr>
	<tr>
		<td class="adminBackground">
			<input tabindex="12" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.DataPhone" value="<ffi:getProperty name="BusinessEmployee" property="DataPhone"/>" size="24" maxlength="14" border="0">
			<span id="BusinessEmployee.dataPhoneError"></span>
			<span class="sectionhead_greyDA">
				<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${BusinessEmployee.DataPhone}" value2="${BusinessEmployee.MasterBusinessEmployee.DataPhone}" operator="notEquals">
					<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.DataPhone"/>
				</ffi:cinclude>
				</ffi:cinclude>											
			</span>
		</td>
		<td class="adminBackground">
			<input tabindex="11" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.FaxPhone" value="<ffi:getProperty name="BusinessEmployee" property="FaxPhone"/>" size="24" maxlength="14" border="0">
			<span id="BusinessEmployee.faxPhoneError"></span>
			<span class="sectionhead_greyDA">
				<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${BusinessEmployee.FaxPhone}" value2="${BusinessEmployee.MasterBusinessEmployee.FaxPhone}" operator="notEquals">										
					<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.FaxPhone"/>
				</ffi:cinclude>
				</ffi:cinclude>
			</span>
		</td>
		<td class="adminBackground">
			<input tabindex="3" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.Street" value="<ffi:getProperty name="BusinessEmployee" property="Street"/>" size="24" maxlength="40" border="0">
			<span class="sectionhead_greyDA">
				<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${BusinessEmployee.Street}" value2="${BusinessEmployee.MasterBusinessEmployee.Street}" operator="notEquals">
					<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.Street"/>
				</ffi:cinclude>
				</ffi:cinclude>											
			</span>
		</td>
		<td class="adminBackground">
			<input tabindex="4" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.Street2" value="<ffi:getProperty name="BusinessEmployee" property="Street2"/>" size="24" maxlength="40" border="0">
			<span class="sectionhead_greyDA">
				<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${BusinessEmployee.Street2}" value2="${BusinessEmployee.MasterBusinessEmployee.Street2}" operator="notEquals">
					<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.Street2"/>
				</ffi:cinclude>
				</ffi:cinclude>	
			</span>
		</td>
	</tr>
	<tr>
		<td class="4">&nbsp;</td>
	</tr>
	<tr>
		<td class="adminBackground">
			<span class='<ffi:daPendingStyle
			newFieldValue="${BusinessEmployee.City}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.City}" daUserAction="${DABusinessEmployee.Action}" 
			dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.default_101"/>
			</span>
		</td>
		<ffi:cinclude value1="${StatesExists}" value2="true" operator="equals">
			<td class="adminBackground">
				<div align="left" id="stateNameId">
				<span class='<ffi:daPendingStyle
				newFieldValue="${BusinessEmployee.State}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.State}" daUserAction="${DABusinessEmployee.Action}" 
				dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
				<s:text name="jsp.default_386"/>
				</span>
				</div>
			</td>
		</ffi:cinclude>
          <ffi:cinclude value1="${StatesExists}" value2="true" operator="notEquals">
           <ffi:setProperty name="BusinessEmployee" property="State" value=""/>
           <td class="adminBackground">&nbsp;</td>
		</ffi:cinclude>
		<td class="adminBackground"> 
			<div align="left">
			<span class='<ffi:daPendingStyle
				newFieldValue="${BusinessEmployee.Country}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.Country}" daUserAction="${DABusinessEmployee.Action}" 
				dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.default_115"/>
			</span><span class="required">*</span>
			</div>
		</td>
		<td class="adminBackground">
			<span class='<ffi:daPendingStyle
				newFieldValue="${BusinessEmployee.ZipCode}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.ZipCode}" daUserAction="${DABusinessEmployee.Action}" 
				dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.default_473"/></span>
		</td>
	</tr>
	<tr class="adminUserTr2">
		<td class="adminBackground"   valign="top" align="left">
			<input tabindex="5" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.City" value="<ffi:getProperty name="BusinessEmployee" property="City"/>" size="24" maxlength="20" border="0">
			<span class="sectionhead_greyDA">
				<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${BusinessEmployee.City}" value2="${BusinessEmployee.MasterBusinessEmployee.City}" operator="notEquals">
					<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.City"/>
				</ffi:cinclude>
				</ffi:cinclude>
			</span>
		</td>
		<ffi:cinclude value1="${StatesExists}" value2="true" operator="equals">
	    <td class="adminBackground">
		<div id="stateSelectId">										
			<select id="selectUserStateID" class="txtbox" tabindex="6" name="BusinessEmployee.State" size="1">
				<option<ffi:cinclude value1="${BusinessEmployee.State}" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.default_376"/></option>												
				<ffi:list collection="StateList" items="item">
					<option <ffi:cinclude value1="${BusinessEmployee.State}" value2="${item.StateKey}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="StateKey"/>"><ffi:getProperty name='item' property='Name'/></option>
				</ffi:list>
			</select>
		</div>
			<span class="sectionhead_greyDA">
			<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${BusinessEmployee.State}" value2="${BusinessEmployee.MasterBusinessEmployee.State}" operator="notEquals">
				<ffi:list collection="StateList" items="item">
					<ffi:cinclude value1="${BusinessEmployee.MasterBusinessEmployee.State}" value2="${item.StateKey}">
					 	<ffi:getProperty name='item' property='Name'/>
					 </ffi:cinclude>
				</ffi:list>
			</ffi:cinclude>
			</ffi:cinclude>
			</span>
	    </td>
		</ffi:cinclude>
        <ffi:cinclude value1="${StatesExists}" value2="true" operator="notEquals">
         <ffi:setProperty name="BusinessEmployee" property="State" value=""/>
	    <td class="adminBackground">&nbsp;</td>
		</ffi:cinclude>
		<td class="adminBackground">										
			<select id="selectCountryID" class="txtbox" tabindex="8" name="BusinessEmployee.Country" size="1" onchange="showState(this);">
				<option<ffi:cinclude value1="${BusinessEmployee.Country}" value2=""> selected</ffi:cinclude> value=""><s:text name="jsp.user_283"/></option>
				<ffi:list collection="CountryList" items="item">
				<option <ffi:cinclude value1="${BusinessEmployee.Country}" value2="${item.CountryCode}">selected</ffi:cinclude> value="<ffi:getProperty name="item" property="CountryCode"/>"><ffi:getProperty name='item' property='Name'/></option>
				</ffi:list>
			</select>
			
				<span class="sectionhead_greyDA">
				<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${BusinessEmployee.Country}" value2="${BusinessEmployee.MasterBusinessEmployee.Country}" operator="notEquals">
						<ffi:list collection="CountryList" items="item">
							<ffi:cinclude value1="${BusinessEmployee.MasterBusinessEmployee.Country}" value2="${item.CountryCode}">
							 	<ffi:getProperty name='item' property='Name'/>
							 </ffi:cinclude>
						</ffi:list>
				</ffi:cinclude>
				</ffi:cinclude>
				</span>
			
			<ffi:removeProperty name="tmp_url"/>
		</td>
		<td class="adminBackground">
			<input tabindex="7" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.ZipCode" value="<ffi:getProperty name="BusinessEmployee" property="ZipCode"/>" size="24" maxlength="11" border="0">
			<span id="BusinessEmployee.zipCodeError"></span>
			<span class="sectionhead_greyDA">
					<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
					<ffi:cinclude value1="${BusinessEmployee.ZipCode}" value2="${BusinessEmployee.MasterBusinessEmployee.ZipCode}" operator="notEquals">
						<ffi:getProperty name="BusinessEmployee" property="MasterBusinessEmployee.ZipCode"/>
					</ffi:cinclude>
					</ffi:cinclude>
			</span>
		</td>
	</tr>
	<tr>
		<td class="4">&nbsp;</td>
	</tr>
	
	<ffi:cinclude value1="${LanguagesList.size}" value2="1" operator="notEquals">
	<tr>
		<td class="adminBackground" nowrap="nowrap">
			<span class='<ffi:daPendingStyle
				newFieldValue="${BusinessEmployee.PreferredLanguage}" oldFieldValue="${BusinessEmployee.MasterBusinessEmployee.PreferredLanguage}" daUserAction="${DABusinessEmployee.Action}" 
				dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
			<s:text name="jsp.user_261"/>
			</span>
		</td>
		<td colspan="3">&nbsp;</td>
	</tr>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${LanguagesList.size}" value2="1" operator="notEquals">
	<tr class="adminUserTr2">
		<td class="adminBackground"   valign="top" align="left">
		<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.PreferredLanguage}" />
		<select id="selectLanguageID" class="txtbox" tabindex="9" name="BusinessEmployee.PreferredLanguage">
			<ffi:list collection="LanguagesList" items="language">
				<ffi:setProperty name="Compare" property="Value2" value="${language.Language}" />
			<option value="<ffi:getProperty name="language" property="Language" />" <ffi:cinclude value1="${Compare.Equals}" value2="true" operator="equals">selected</ffi:cinclude> >
				<ffi:getProperty name="language" property="DisplayName" />
			</option>
			</ffi:list>
		</select>
		<span class="sectionhead_greyDA">
			<ffi:cinclude value1="${DABusinessEmployee.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${BusinessEmployee.PreferredLanguage}" value2="${BusinessEmployee.MasterBusinessEmployee.PreferredLanguage}" operator="notEquals">
			<ffi:list collection="LanguagesList" items="language">
				<ffi:cinclude value1="${BusinessEmployee.MasterBusinessEmployee.PreferredLanguage}" value2="${language.Language}">
				 	<ffi:getProperty name='language' property='DisplayName'/>
				 </ffi:cinclude>
			</ffi:list>
			</ffi:cinclude>
			</ffi:cinclude>
		</span>
		</td>
		<td colspan="3">&nbsp;</td>
	</tr>
	</ffi:cinclude>
	<tr>
		<td colspan="4" align="center">
			<div align="center">
				<br>
				<span class="required">* <s:text name="jsp.default_240"/></span>
				<br>
				<br>
				<s:url id="resetEditUserButtonUrl" value="/pages/user/editBusinessUser_init.action" escapeAmp="false">  
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
					<s:param name="EmployeeID" value="%{#request.BusinessEmployee.Id}"></s:param>
				</s:url>	
				<sj:a id="resetEditUserBtn"
				   href="%{resetEditUserButtonUrl}"
				   targets="inputDiv"
				   button="true" ><s:text name="jsp.default_358"/></sj:a>
				<sj:a
					  button="true"
					  summaryDivId="summary" 
					  buttonType="cancel"
					  onClickTopics="showSummary,cancelUserForm"><s:text name="jsp.default_82"/></sj:a>
				<sj:a
						formIds="EditBUsinessUserFormID"
						targets="verifyDiv"
						button="true"
						validate="true"
						validateFunction="customValidation"
						onBeforeTopics="beforeVerify"
						onCompleteTopics="completeVerify"
						onErrorTopics="errorVerify"
						onSuccessTopics="successVerify"
						><s:text name="jsp.default_366"/></sj:a>
					
			</div>
		</td>
	</tr>
</table>
</s:form>

</div>

<!-- Restore Business Employee in session to the original values -->
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="BusinessEmployee"/>
		<ffi:setProperty name="PopulateObjectWithDAValues" property="restoredObject" value="y"/>
	<ffi:process name="PopulateObjectWithDAValues"/>
</ffi:cinclude>

<%-- set Entitlement_CanAdminister back to indicate logged in users' administer right--%>
<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>

<script type="text/javascript">
$("#editPswd").strength({
});
</script>

<ffi:removeProperty name="CanAdminister"/>

<ffi:removeProperty name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>
<ffi:removeProperty name="GetStatesForCountry"/>
<ffi:removeProperty name="fromPendingTable" />
<ffi:removeProperty name="oldDAObject" />
<ffi:removeProperty name="Init"/>
<ffi:removeProperty name="EditUserChannel"/>
