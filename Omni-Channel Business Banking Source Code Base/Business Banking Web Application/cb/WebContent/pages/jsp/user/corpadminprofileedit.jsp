<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="backProfileLink"
                 value="/pages/user/modifyBusinessEntitlementProfile_init.action"
                 URLEncrypt="false"/>
<s:url id="showReturnProfileUrl" value="%{#session.backProfileLink}" escapeAmp="false">  
	<s:param name="OneAdmin" value="%{#parameters.OneAdmin}" />
	<s:param name="employeeID" value="BusinessEmployee.id"/>
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
</s:url>
<script type="text/javascript"
	src="<s:url value='/web'/>/js/user/profiles.js"></script>
<ffi:help id="user_corpadminprofileedit" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_142')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<span id="PageHeading" style="display:none;"><s:text name="jsp.user_142"/></span>

<script language="javascript">
ns.users.entitlementProfile={};
jQuery( document ).ready(function( $ ) {
	var parentProfileId = "<s:property value='parentProfileId' />";
	$("#details").show();
	var groupSelectWiz = new selectWizard();
	groupSelectWiz.init("selectProfileGroupID", {width:250});
});
$.subscribe('callPermissionLink', function(event, data)
{	
	var url = "<s:property value='showReturnProfileUrl' />";
	ns.admin.BackToProfileEditUrl = url.replace(/&amp;/g, "&");
	ns.admin.currentVisibleDashboard = "EDIT_PROFILE";
	$('#profilePermissionsLink').click();
	$("#permissionsDiv").show();
	$("#details").hide();
});

</script>

<%-- Retrieve the list of admins on this group --%>
<ffi:object id="GetAdminsForGroup" name="com.ffusion.efs.tasks.entitlements.GetAdminsForGroup" />
<ffi:setProperty name="GetAdminsForGroup" property="GroupId" value="${Business.EntitlementGroupId}"/>
<ffi:setProperty name="GetAdminsForGroup" property="SessionName" value="Admins"/>
<ffi:process name="GetAdminsForGroup"/>

<ffi:object id="GroupAccounts" name="com.ffusion.beans.accounts.Accounts" scope="session"/>

<%-- Get the entitlementGroup for this new user back to session --%>
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EntitlementGroupIdStr}"/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:setProperty name="checkedfalse" value=""/>
<ffi:setProperty name="checkedtrue" value="checked"/>
<%-- This BackURL is also used when state list is dynamically updated --%>
<ffi:setProperty name="BackURL" value="${SecurePath}user/corpadminprofileedit.jsp"/>
<% String customerType = ""; %>												
<ffi:getProperty name="BusinessEmployee" property="CustomerType" assignTo="customerType"/>
		<div align="center">
			<span id="formerrors"></span>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="adminBackground">
						<s:form id="editBusinessProfileFormId" namespace="/pages/user" action="modifyBusinessEntitlementProfile_verify" theme="simple" name="EditBusinessUserForm" method="post" >
						<table width="100%" border="0" cellspacing="0" cellpadding="3" align="left">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<tr>
							<td width="250px" align="left">
								<span class="sectionsubhead"><s:text name="jsp.user_267"/>:</span><span class="required">*</span>
							</td>
							<td> 
								<input tabindex="14" class="ui-widget-content ui-corner-all" type="text" name="BusinessEmployee.UserName" value="<ffi:cinclude value1="${TempBusEmployee.Id}" value2="" operator="equals"><ffi:getProperty name="BusinessEmployee" property="UserName"/></ffi:cinclude><ffi:cinclude value1="${TempBusEmployee.Id}" value2="" operator="notEquals"><ffi:getProperty name="TempBusEmployee" property="UserName"/></ffi:cinclude>" size="35" maxlength="200" border="0">
								<span id="BusinessEmployee.userNameError"></span>
							</td>
							<td align="left">&nbsp</td>
						</tr>							

						<tr>
							<td align="left"><span class="sectionsubhead"><s:text name="jsp.default_225"/>:</span><span class="required">*</span></td>
							<td>
								<ffi:cinclude value1="${GroupSummaries.Size}" value2="0" operator="notEquals">
									<ffi:setProperty name="tmp_url" value="${SecurePath}user/corpadminprofileedit.jsp?EntitlementGroupIdStr=${EntitlementGroupIdStr}&SetBusinessEmployee.Id=${SetBusinessEmployee.Id}&UpdateEntGroupId=TRUE" URLEncrypt="true"/>
									<select fr="ff" id="selectProfileGroupID" class="txtbox" tabindex="20" name="BusinessEmployee.EntitlementGroupId">
									
									<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.EntitlementGroupId}" />											

									<ffi:list collection="GroupSummaries" items="group" >
										<ffi:list collection="group" items="spaces, groupName, groupId, numUsers" >
											<ffi:cinclude value1="${groupId}" value2="0" operator="notEquals" >
												<ffi:setProperty name="Compare" property="value2" value="${groupId}"/>
												<option value="<ffi:getProperty name="groupId" />" <ffi:getProperty name="selected${Compare.Equals}"/> >
													<ffi:getProperty name="spaces" encode="false"/><ffi:getProperty name="groupName" />
												</option>
											</ffi:cinclude>
										</ffi:list>
									</ffi:list>
									</select>
									<span id="BusinessEmployee.EntitlementGroupIdError"></span>
									<ffi:removeProperty name="tmp_url"/>
								</ffi:cinclude>
							</td>
							<td align="left">&nbsp</td>
						</tr>
						
						<s:if test="isProfileInUse()">
	               			<tr>
	               			  <td colspan="3" align="center">
	               			  	<span class="required"><s:text name="jsp.user.error.user.assigned.profile" /></span>
	               			  </td>
	               			</tr>
               			</s:if><tr>
									<td colspan="3" valign="top">
										<div align="center">
											<span class="required">* <s:text name="jsp.default_240"/></span>
											<br>
											<br>
											<s:url id="resetEditProfileButtonUrl" value="/pages/user/modifyBusinessEntitlementProfile_init.action" escapeAmp="false">  
													<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
													<s:param name="EmployeeID" value="%{#request.BusinessEmployee.Id}"></s:param>		
												</s:url>	
												<sj:a id="resetEditProfileBtn"
												   href="%{resetEditProfileButtonUrl}"
												   targets="inputDiv"
												   button="true" ><s:text name="jsp.default_358"/></sj:a>
											<sj:a
												  button="true"
												  summaryDivId="profileSummary" 
												  buttonType="cancel"
												  onClickTopics="showSummary,cancelProfileForm"><s:text name="jsp.default_82"/></sj:a>
											<sj:a
													id="editProfileBtnId"
													formIds="editBusinessProfileFormId"
													targets="verifyDiv"
													button="true"
													validate="true"
													validateFunction="customValidation"
													onBeforeTopics="beforeVerify"
													onCompleteTopics="completeVerify"
													onErrorTopics="errorVerify"
													onSuccessTopics="successVerify"
													><s:text name="jsp.default_366"/></sj:a>

										</div>
									</td>
								</tr>
						</table>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
<%-- set Entitlement_CanAdminister back to indicate logged in users' administer right--%>
<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:removeProperty name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="EmployeeCanAdministerAnyGroup"/>
<ffi:removeProperty name="Entitlement_CanAdministerAnyGroup"/>
<ffi:removeProperty name="TempBusEmployee"/>
<ffi:removeProperty name="Country_List"/>
<ffi:removeProperty name="GetStatesForCountry"/>