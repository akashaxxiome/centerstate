<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadmingroupdelete-verify" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_374')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<%
	String groupId = request.getParameter("DeleteGroup_GroupId");
%>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="<%=groupId%>"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value='<%=groupId%>'/>
<ffi:process name="GetEntitlementGroup"/>
<ffi:setProperty name="GroupToDeleteName" value='${Entitlement_EntitlementGroup.groupName}'/>

<ffi:object id="CanDeleteEntitlementGroup" name="com.ffusion.tasks.admin.CanDeleteEntitlementGroup" scope="session"/>
<ffi:process name="CanDeleteEntitlementGroup"/>

<div id="groupwholeworld">
	<div align="center">
		<TABLE class="adminBackground" width="480" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD class="columndata" align="center" id="deleteGroupConfirmCell">
								<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmingroupdelete-verify.jsp-1" parm0="${GroupToDeleteName}"/></TD>
							<td></td>
						</TR>
						<TR>
							<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15">
							</TD>
							<td></td>
						</TR>
						<TR>
							<TD>
								<div align="center" class="ui-widget-header customDialogFooter">
									<sj:a button="true" onClickTopics="closeDialog"
										  title="%{getText('jsp.default_83')}" 
										  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_82" /></sj:a>
									<s:url id="deleteGroupUrl" value="/pages/jsp/user/corpadmingroupdelete-confirm.jsp">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									</s:url>
									<sj:a href="%{deleteGroupUrl}"
										  targets="deleteGroupDialogID"
										  button="true"
										  title="%{getText('jsp.user_395')}"										 
										  effectDuration="1500"
										  cssClass="buttonlink ui-state-default ui-corner-all"><s:text name="jsp.default_162" /></sj:a>
								</div>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
		<p></p>
	</div>
</div>
