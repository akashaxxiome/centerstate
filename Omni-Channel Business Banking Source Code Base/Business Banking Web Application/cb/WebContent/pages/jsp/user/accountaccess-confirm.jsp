<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="user_accountaccess-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_76')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<%-- set the state of auto entitlement as necessary --%>
<ffi:cinclude value1="${doAutoEntitle}" value2="" operator="equals">
        <%-- the value of the ${GetCumulativeSettings.EnablePermissions} should be false --%>
	<ffi:setProperty name="EditAccountAccessPermissions" property="AutoEntitle" value="${GetCumulativeSettings.EnableAccounts}"/>
</ffi:cinclude>
<ffi:cinclude value1="${doAutoEntitle}" value2="true" operator="equals">
        <ffi:setProperty name="EditAccountAccessPermissions" property="AutoEntitle" value="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${doAutoEntitle}" value2="false" operator="equals">
        <ffi:setProperty name="EditAccountAccessPermissions" property="AutoEntitle" value="false"/>
</ffi:cinclude>
<ffi:removeProperty name="doAutoEntitle"/>
<ffi:removeProperty name="GetCumulativeSettings"/>

<%-- DA mode processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">
	<%-- Process tree if flag is set --%>
	<ffi:cinclude value1="${PROCESS_TREE_FLAG}" value2="TRUE" operator="equals">
		<ffi:setProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>"
							value="<%=IDualApprovalConstants.CATEGORY_SUB_ACCOUNT_ACCESS%>"/>
		<ffi:process name="ProcessEntitlementTree" />
		<ffi:setProperty name="EditAccountAccessPermissions" property="AutoEntitle" value="true"/>
		<ffi:process name="EditAccountAccessPermissions"/>
		<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SUB_SESSION_NAME%>" />
		<ffi:removeProperty name="ProcessEntitlementTree" />
	</ffi:cinclude>
	<%-- Entitlement tree is not displayed. --%>
	<ffi:cinclude value1="${PROCESS_TREE_FLAG}" value2="TRUE" operator="notEquals">
		<ffi:setProperty name="EditAccountAccessPermissions" property="AutoEntitle" value="false"/>
		<ffi:process name="EditAccountAccessPermissions"/>
	</ffi:cinclude>
	<ffi:removeProperty name="DA_GRANT_ENTITLEMENTS" />
	<ffi:removeProperty name="DA_REVOKE_ENTITLEMENTS" />
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:removeProperty name="ENTITLEMENT_TREE_GROUPS" />
	<ffi:removeProperty name="ENTITLEMENT_TREE_MEMBERS_MAP" />
	<ffi:removeProperty name="RootGroupId" />
</ffi:cinclude>

<%-- now that the account's may have been activated or deactivated, reload account lists --%>
<ffi:object id="GetBusinessAccounts" name="com.ffusion.tasks.accounts.GetBusinessAccounts" scope="session"/>
<ffi:setProperty name="GetBusinessAccounts" property="AccountsName" value="BankingAccounts"/>
<ffi:setProperty name="GetBusinessAccounts" property="CheckEntitlements" value="true"/>
<ffi:process name="GetBusinessAccounts"/>
<ffi:removeProperty name="GetBusinessAccounts"/>

<ffi:object id="Accounts" name="com.ffusion.beans.accounts.Accounts" scope="session"/>
<ffi:setProperty name="Accounts" property="XML" value="${BankingAccounts.XML}"/>

<ffi:object name="com.ffusion.tasks.accounts.GetAccounts" id="GetAccounts"/>
   <ffi:setProperty name="GetAccounts" property="bankingService" value="true"/>
    <ffi:setProperty name="GetAccounts" property="AccountsName" value="Accounts"/>
    <ffi:setProperty name="GetAccounts" property="CheckEntitlements" value="true" />
<ffi:process name="GetAccounts"/>

<ffi:object name="com.ffusion.tasks.accounts.MergeBalances" id="MergeBalances" />
    <ffi:setProperty name="MergeBalances" property="ProfileAccountsName" value="BankingAccounts"/>
    <ffi:setProperty name="MergeBalances" property="BackendAccountsName" value="Accounts"/>
<ffi:process name="MergeBalances"/>

<%-- derive the most-recent summary lists from BankingAccounts --%>
<ffi:object id="GetMostRecentSummaries" name="com.ffusion.tasks.banking.GetMostRecentSummaries" scope="session"/>
    <ffi:setProperty name="GetMostRecentSummaries" property="AccountsName" value="BankingAccounts"/>

<ffi:object id="GetMostRecentSummary" name="com.ffusion.tasks.banking.GetMostRecentSummary" scope="session"/>

<%-- This is a patch to fix the problem with account register stuffs --%>
<ffi:object name="com.ffusion.tasks.register.SetRegisterAccountsData" id="SetRegisterAccountsData"/>
<ffi:process name="SetRegisterAccountsData"/>
<ffi:removeProperty name="SetRegisterAccountsData"/>
<%-- patch end --%>
<div align="center"><s:text name="jsp.user_56"/></div>
<ffi:removeProperty name="LastRequest"/>
<ffi:removeProperty name="ApprovalAdminByBusiness"/>
<ffi:removeProperty name="ParentsAccounts"/>
<ffi:removeProperty name="MyAccounts"/>
<ffi:removeProperty name="FilteredAccounts"/>
<ffi:removeProperty name="EditAccountAccessPermissions"/>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT %>"/>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_SESSION_LIMIT %>"/>

<%-- remove action from session --%>
<ffi:removeProperty name="action"/>

<ffi:setProperty name="confirmation_done" value="true"/>
<ffi:setProperty name="BackURL" value="${SecurePath}user/accountaccess.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>

<s:include value="/pages/jsp/inc/checkbox-limit-cleanup.jsp"/>
