<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.tasks.dualapproval.IDACategoryConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:setL10NProperty name='PageHeading' value='Pending Approval Summary'/>

<%
if(request.getParameter("Section") != null)
	session.setAttribute("Section", request.getParameter("Section"));
if(request.getParameter("ParentMenu") != null)
	session.setAttribute("ParentMenu", request.getParameter("ParentMenu"));
if(request.getParameter("PermissionsWizard") != null)
	session.setAttribute("PermissionsWizard", request.getParameter("PermissionsWizard"));
if(request.getParameter("CurrentWizardPage") != null)
	session.setAttribute("CurrentWizardPage", request.getParameter("CurrentWizardPage"));
if(request.getParameter("SortKey") != null)
	session.setAttribute("SortKey", request.getParameter("SortKey"));
%>

<ffi:cinclude value1="${admin_init_touched}" value2="true" operator="notEquals">
	<s:include value="/pages/jsp/inc/init/admin-init.jsp" />
</ffi:cinclude>

<s:if test="%{#session.Section == 'Users'}">
	<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
	<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
	<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
	<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
	<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployeesDA"/>
	<ffi:process name="GetBusinessEmployees"/>
	<ffi:removeProperty name="GetBusinessEmployees"/>
	<ffi:removeProperty name="SearchBusinessEmployee"/>
	
	<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployeesDA" />
	<ffi:process name="SetBusinessEmployee"/>
	<ffi:setProperty name="EditGroup_GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
</s:if>
<ffi:cinclude value1="${Section}" value2="Profiles" operator="equals">
	<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
	<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
	<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
	<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
	<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
	<ffi:setProperty name="GetBusinessEmployees" property="SearchEntitlementProfiles" value="TRUE"/>
	<ffi:setProperty name="GetBusinessEmployees" property="businessEmployeesSessionName" value="BusinessEmployeesDA"/>
	<ffi:process name="GetBusinessEmployees"/>
	<ffi:removeProperty name="GetBusinessEmployees"/>
	<ffi:removeProperty name="SearchBusinessEmployee"/>
	
	<ffi:object id="SetBusinessEmployee" name="com.ffusion.tasks.user.SetBusinessEmployee" />
	<ffi:setProperty name="SetBusinessEmployee" property="id" value="${itemId}" />
	<ffi:setProperty name="SetBusinessEmployee" property="businessEmployeesSessionName" value="BusinessEmployeesDA" />
	<ffi:process name="SetBusinessEmployee"/>
	<ffi:setProperty name="EditGroup_GroupId" value="${BusinessEmployee.EntitlementGroupId}" />
</ffi:cinclude>

<%-- Get the entitlementGroup, which the permission or its member's permission is currently being edited --%>
<ffi:object id="GetEntitlementGroupId" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${EditGroup_GroupId}"/>
<ffi:process name="GetEntitlementGroup"/>

<ffi:setProperty name="currentPage" value="<%=IDACategoryConstants.IS_PER_ACCOUNT_CHANGED %>" />

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
	<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${itemId}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="${itemType}" />
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:setProperty name="GetDACategoryDetails" property="objectType" value="<%=EntitlementsDefines.ACCOUNT%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="true"/>
	<ffi:setProperty name="GetDACategoryDetails" property="multipleCategoriesSessionName" value="categories"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:object id="FilterDACategories" name="com.ffusion.tasks.dualapproval.FilterDACategories" />
<ffi:setProperty name="FilterDACategories" property="CategoryLike" value="<%=IDualApprovalConstants.CATEGORY_SUB_PER_ACCOUNT %>"/>
<ffi:process name="FilterDACategories" />
<ffi:removeProperty name="FilterDACategories"/>

<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
<ffi:setProperty name="GetDAItem" property="Validate" value="itemId,ItemType" />
<ffi:setProperty name="GetDAItem" property="itemId" value="${itemId}"/>
<ffi:setProperty name="GetDAItem" property="itemType" value="${itemType}" />
<ffi:process name="GetDAItem"/>

<ffi:object id="SubmittedUser" name="com.ffusion.tasks.user.GetUserById"/>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="equals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.createdBy}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${DAItem.modifiedBy}" value2="" operator="notEquals">
	   <ffi:setProperty name="SubmittedUser" property="profileId" value="${DAItem.modifiedBy}" />
	</ffi:cinclude>
	<ffi:setProperty name="SubmittedUser" property="userSessionName" value="SubmittedUserName"/>
<ffi:process name="SubmittedUser"/>

<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
<ffi:object id="GetEntitlmentDisplayName" name="com.ffusion.tasks.dualapproval.GetEntitlmentDisplayName"/>
<ffi:object id="ValidatePerAccountDA" name="com.ffusion.tasks.dualapproval.ValidatePerAccountDA" scope="request"/>
<ffi:object id="HandleAccountRowDisplayDA" name="com.ffusion.tasks.dualapproval.HandleAccountRowDisplayDA" scope="request"/>

<script type="text/javascript">
function reject()
{
	document.frmDualApproval.action = "rejectchangesda.jsp";
	document.frmDualApproval.submit();
}
</script>

<input type="hidden" name="itemId" value="<ffi:getProperty name='itemId'/>" >
<input type="hidden" name="itemType" value="<ffi:getProperty name='itemType'/>" >
<input type="hidden" name="category" value="<ffi:getProperty name='category'/>" >
<input type="hidden" name="successUrl" value="<ffi:getProperty name='successUrl'/>" >
<input type="hidden" name="userAction" value="<ffi:getProperty name='userAction'/>" >

<ffi:cinclude value1="${tempSuccessUrl}" value2="">
	<ffi:setProperty name="tempSuccessUrl" value="${SuccessUrl}"/>
</ffi:cinclude>

<ffi:cinclude value1="${SuccessUrl}" value2="">
	<ffi:setProperty name='SuccessUrl' value="${tempSuccessUrl}"/>
</ffi:cinclude>

<div align="center">
	<span id="formError"></span>
	<s:if test="%{#session.Section == 'Users'}">
		<ffi:setProperty name="subMenuSelected" value="users"/>
	</s:if>
	<ffi:cinclude value1="${Section}" value2="Profiles">
		<ffi:setProperty name="subMenuSelected" value="users"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Company">
		<ffi:setProperty name="subMenuSelected" value="company"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Division">
		<ffi:setProperty name="subMenuSelected" value="divisions"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${Section}" value2="Group">
		<ffi:setProperty name="subMenuSelected" value="groups"/>
	</ffi:cinclude>
<%-- include page header --%>
	<ffi:setProperty name="isPendingIsland" value="TRUE"/>
	<s:include value="/pages/jsp/user/inc/nav_header.jsp" />
	<ffi:removeProperty name="isPendingIsland"/>
	<div class="blockWrapper">
	<div  class="blockHead tableData"><span><strong>
		<ffi:cinclude value1="${rejectFlag}" value2="y"  operator="notEquals">
						<!--L10NStart-->Verify Pending Changes for Approval<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:cinclude value1="${rejectFlag}" value2="y" >
						<!--L10NStart-->Verify Pending Changes for Rejection<!--L10NEnd-->
					</ffi:cinclude></strong></span>
	</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 33%">
				<span class="sectionsubhead adminBackground">
                             <s:if test="%{#session.Section == 'Users'}">
                                 <!--L10NStart-->User<!--L10NEnd-->:
                             </s:if>
                             <ffi:cinclude value1="${Section}" value2="Profiles">
                                 <!--L10NStart-->Profile<!--L10NEnd-->:
                             </ffi:cinclude>
                             <ffi:cinclude value1="${Section}" value2="Company">
                                 <!--L10NStart-->Company<!--L10NEnd-->:
                             </ffi:cinclude>
                             <ffi:cinclude value1="${Section}" value2="Division">
                                 <!--L10NStart-->Division<!--L10NEnd-->:
                             </ffi:cinclude>
                             <ffi:cinclude value1="${Section}" value2="Group">
                                 <!--L10NStart-->Group<!--L10NEnd-->:
                             </ffi:cinclude>
				</span>
				<span class="sectionsubhead adminBackground" >
                             <s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
                                 <ffi:getProperty name="userName"/>
                             </s:if>
                             <s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
                                 <ffi:cinclude value1="${Section}" value2="Company">
                                     <ffi:getProperty name="Business" property="BusinessName"/>
                                 </ffi:cinclude>
                                 <ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
                                     <ffi:object name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" id="GetEntitlementGroup" scope="session"/>
                                     <ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="${DAItem.itemId}"/>
                                     <ffi:process name="GetEntitlementGroup"/>
                                     <ffi:setProperty name="groupName" value="${Entitlement_EntitlementGroup.GroupName}"/>
                                     <ffi:getProperty name="groupName"/>
                                 </ffi:cinclude>
                             </s:if>
				</span>
			</div>
			<div class="inlineBlock" style="width: 33%">
				<span class="sectionsubhead adminBackground" width="108" >
					<!--L10NStart-->Submitted By<!--L10NEnd-->:
				</span>
				<span class="sectionsubhead adminBackground"  colspan="1">
					<ffi:getProperty name="SubmittedUserName" property="userName"/>
				</span>
			</div>
			<div class="inlineBlock" style="width: 33%">
				<span class="sectionsubhead adminBackground">
					<!--L10NStart-->Submitted On<!--L10NEnd-->:
				</span>
			<span class="sectionsubhead adminBackground">
				<ffi:getProperty name="DAItem" property="formattedSubmittedDate"/>
			</span>
			</div>
		</div>
	</div>
</div>
<div class="marginTop10"></div>					
	<div class="paneWrapper approvalPermHeight">
  		<div class="paneInnerWrapper">
					<table width="750" border="0" cellspacing="0" cellpadding="3">
						</tr>
						<ffi:cinclude value1="${userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED  %>" operator="notEquals">
							<s:include value="/pages/jsp/user/inc/da-summary-header-permission.jsp"/>
							<ffi:object id="GetTypesForEditingLimits" name="com.ffusion.tasks.admin.GetTypesForEditingLimits" scope="session"/>
							<ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithLimitsName" value="NonAccountEntitlementsWithLimitsBeforeFilter"/>
						    <ffi:setProperty name="GetTypesForEditingLimits" property="NonAccountWithoutLimitsName" value="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
							<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithLimitsName" value="AccountEntitlementsWithLimitsBeforeFilter"/>
					    	<ffi:setProperty name="GetTypesForEditingLimits" property="AccountWithoutLimitsName" value="AccountEntitlementsWithoutLimitsBeforeFilter"/>
							<ffi:setProperty name="GetTypesForEditingLimits" property="AccountMerged" value="AccountEntitlementsMergedBeforeFilter"/>
							<ffi:process name="GetTypesForEditingLimits"/>
							<ffi:removeProperty name="GetTypesForEditingLimits" />

							<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness" scope="request"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsMergedBeforeFilter"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsMerged"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroup.ParentId}"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>

							<ffi:list collection="categories" items="category">
							<ffi:cinclude value1="${category.categoryType}" value2="<%=IDualApprovalConstants.CATEGORY_ENTITLEMENT %>">
								<% session.setAttribute("CATEGORY_BEAN", request.getAttribute("category")); %>

								<s:if test="%{#attr.category.objectId != #attr.FilterEntitlementsForBusiness.ObjectId}">
									<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectId" value="${category.objectId}"/>
									<ffi:process name="FilterEntitlementsForBusiness"/>
								</s:if>

								<ffi:setProperty name="HandleAccountRowDisplayDA" property="EntitlementTypePropertyLists" value="AccountEntitlementsMerged"/>
								<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
									<ffi:setProperty name="HandleAccountRowDisplayDA" property="GroupId" value="${BusinessEmployee.EntitlementGroupId}"/>
									<ffi:setProperty name="HandleAccountRowDisplayDA" property="MemberId" value="${BusinessEmployee.EntitlementGroupMember.Id}"/>
									<ffi:setProperty name="HandleAccountRowDisplayDA" property="MemberSubType" value="${BusinessEmployee.EntitlementGroupMember.MemberSubType}"/>
									<ffi:setProperty name="HandleAccountRowDisplayDA" property="MemberType" value="${BusinessEmployee.EntitlementGroupMember.MemberType}"/>
								</s:if>
								<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
									<ffi:setProperty name="HandleAccountRowDisplayDA" property="GroupId" value="${EditGroup_GroupId}"/>
								</s:if>
								<ffi:setProperty name="HandleAccountRowDisplayDA" property="ObjectType" value="${category.objectType}"/>
								<ffi:setProperty name="HandleAccountRowDisplayDA" property="ObjectId" value="${category.objectId}"/>
								<ffi:setProperty name="HandleAccountRowDisplayDA" property="CategoryBeanSessionName" value="CATEGORY_BEAN"/>
								<ffi:process name="HandleAccountRowDisplayDA"/>
								<ffi:removeProperty name="LimitsList"/>
								
								<%-- Entitlement check within Dual approval tables --%>
								<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
									<ffi:setProperty name="ValidatePerAccountDA" property="itemId" value="${itemId}"/>
									<ffi:setProperty name="ValidatePerAccountDA" property="itemType" value="${itemType}"/>
									<ffi:setProperty name="ValidatePerAccountDA" property="EntitlementTypePropertyLists" value="AccountEntitlementsMerged"/>
									<ffi:process name="ValidatePerAccountDA"/>
								</s:if>
								
								<ffi:list collection="CATEGORY_BEAN.daItems" items="entitlement">
								<ffi:flush/>
								<tr>
									<td class="columndata"><ffi:flush/><ffi:getL10NString rsrcFile="cb" msgKey="da.field.${category.categorySubTypeFormatted}" /></td>
							 		<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="${entitlement.fieldName}"/>
				 				 	<ffi:process name="GetEntitlmentDisplayName" />
				 				 	<td class="columndata"><ffi:getProperty name="entitlementDisplayName" /></td>
									<td class="columndata"><ffi:getProperty name="CATEGORY_BEAN" property="objectType" /></td>
									<td class="columndata"><ffi:getProperty name="CATEGORY_BEAN" property="formattedObjectId" /></td>
									<td class="columndata">
										<ffi:cinclude value1="${entitlement.oldValue}" value2="true">
											<ffi:flush/><!--L10NStart-->Grant<!--L10NEnd-->				
										</ffi:cinclude>
										<ffi:cinclude value1="${entitlement.oldValue}" value2="false" >
											<ffi:flush/><!--L10NStart-->Revoke<!--L10NEnd-->						
										</ffi:cinclude>
									</td>
									<td class="columndata sectionheadDA">
										<ffi:cinclude value1="${entitlement.newValue}" value2="true">
											<ffi:flush/><!--L10NStart-->Grant<!--L10NEnd-->				
										</ffi:cinclude>
										<ffi:cinclude value1="${entitlement.newValue}" value2="false" >
											<ffi:flush/><!--L10NStart-->Revoke<!--L10NEnd-->						
										</ffi:cinclude>
									</td>
									<td class="columndata"><ffi:flush/><ffi:getProperty name="entitlement"	property="userAction" /></td>
						
								</tr>
								<ffi:cinclude value1="${entitlement.error}" value2="" operator="notEquals">
									<ffi:setProperty name="DISABLE_APPROVE" value="true"/>
									<tr>
										<td class="columndataDANote" colspan="7">
											<ffi:flush/><ffi:getProperty name="entitlement" property="error"/> 
										</td>
									</tr>
								</ffi:cinclude>
								</ffi:list>
							</ffi:cinclude>
							</ffi:list>
							
							<ffi:list collection="categories" items="category"> 
							<ffi:cinclude value1="${category.categoryType}" value2="<%=IDualApprovalConstants.CATEGORY_LIMIT %>">
								<% session.setAttribute("CATEGORY_BEAN", request.getAttribute("category")); %>
								
								<ffi:object id="FilterEntitlementsForBusiness" name="com.ffusion.tasks.admin.FilterEntitlementsForBusiness" scope="request"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsMergedBeforeFilter"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsMerged"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroup.ParentId}"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="ObjectId" value="${category.objectId}"/>
								<ffi:process name="FilterEntitlementsForBusiness"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsBeforeFilter" value="AccountEntitlementsWithLimitsBeforeFilter"/>
								<ffi:setProperty name="FilterEntitlementsForBusiness" property="EntitlementTypePropertyListsAfterFilter" value="AccountEntitlementsWithLimits"/>
								<ffi:process name="FilterEntitlementsForBusiness"/>

								<s:include value="/pages/jsp/user/inc/da-verify-user-peraccount-limits.jsp"/>
								
								<ffi:list collection="CATEGORY_BEAN.daItems" items="limit">
								<ffi:flush/>
								<tr>
									<td class="columndata"><ffi:flush/><ffi:getL10NString rsrcFile="cb" msgKey="da.field.${category.categorySubTypeFormatted}" /></td>
							 		<ffi:setProperty name="GetEntitlmentDisplayName" property="operationName" value="${limit.operationName}"/>
				 				 	<ffi:process name="GetEntitlmentDisplayName" />
				 				 	<td class="columndata"><ffi:getProperty name="entitlementDisplayName" /></td>
									<td class="columndata"><ffi:getProperty name="CATEGORY_BEAN" property="objectType" /></td>
									<td class="columndata"><ffi:getProperty name="CATEGORY_BEAN" property="formattedObjectId" /></td>
									<td class="columndata">
										<ffi:cinclude value1="${limit.oldData}" value2="" operator="notEquals">
											<ffi:setProperty name="CurrencyObject" property="Amount" value="${limit.oldData}"/>
											<ffi:flush/><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
										</ffi:cinclude></td>
									</td>
									<td class="columndata sectionheadDA">
										<ffi:cinclude value1="${limit.newData}" value2="" operator="notEquals">
											<ffi:setProperty name="CurrencyObject" property="Amount" value="${limit.newData}"/>
											<ffi:flush/><ffi:getProperty name="CurrencyObject" property="CurrencyStringNoSymbol"/> 
										</ffi:cinclude>
									</td>
									<td class="columndata">
										<ffi:cinclude value1="${limit.allowNewApproval}" value2="Y">
											<ffi:flush/><!--L10NStart-->No<!--L10NEnd-->
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.allowNewApproval}" value2="N">
											<ffi:flush/><!--L10NStart-->Yes<!--L10NEnd-->
										</ffi:cinclude>
									</td>
									<td class="columndata sectionheadDA">
									<s:include value="/pages/jsp/user/inc/da-verify-user-checkrequireapproval.jsp"/>
										<ffi:cinclude value1="${IsRequireApprovalChecked}" value2="" operator="notEquals">
											<ffi:cinclude value1="${limit.allowNewApproval}" value2="Y" operator="equals">
												<ffi:setProperty name="limit" property="error" value="${RequiresApprovalErrMsg}" />
											</ffi:cinclude>
										</ffi:cinclude>
										<ffi:removeProperty name="IsRequireApprovalChecked" />
										<ffi:removeProperty name="RequiresApprovalErrMsg" />
										<ffi:cinclude value1="${limit.allowNewApproval}" value2="Y">
											<ffi:flush/><!--L10NStart-->Yes<!--L10NEnd-->
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.allowNewApproval}" value2="N">
											<ffi:flush/><!--L10NStart-->No<!--L10NEnd-->
										</ffi:cinclude></td>
									<td class="columndata">
										<ffi:cinclude value1="${limit.period}" value2="1">
											<ffi:flush/><!--L10NStart-->per transaction<!--L10NEnd-->				
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.period}" value2="2">
											<ffi:flush/><!--L10NStart-->per day<!--L10NEnd-->				
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.period}" value2="3">
											<ffi:flush/><!--L10NStart-->per week<!--L10NEnd-->				
										</ffi:cinclude>
										<ffi:cinclude value1="${limit.period}" value2="4">
											<ffi:flush/><!--L10NStart-->per month<!--L10NEnd-->				
										</ffi:cinclude>
									</td>
								</tr>
								<ffi:cinclude value1="${entitlement.error}" value2="" operator="notEquals">
									<ffi:setProperty name="DISABLE_APPROVE" value="true"/>
									<tr>
										<td class="columndataDANote" colspan="7">
											<ffi:flush/><ffi:getProperty name="entitlement" property="error"/> 
										</td>
									</tr>
								</ffi:cinclude>
								</ffi:list>
							</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
					</table></div></div>
			<s:include value="/pages/jsp/user/inc/da-summary-footer-permission.jsp"/>
			<ffi:setProperty name="url" value="/pages/jsp/user/da-summary-peraccount.jsp?PermissionsWizard=TRUE"  URLEncrypt="true" />
			<ffi:setProperty name="BackURL" value="${url}"/>			
	</div>		
<ffi:removeProperty name="AccountGroupAccessList"/>
<ffi:removeProperty name="HandleAccountGroupAccessRowDisplayDA"/>
<ffi:removeProperty name="CATEGORY_BEAN"/>
