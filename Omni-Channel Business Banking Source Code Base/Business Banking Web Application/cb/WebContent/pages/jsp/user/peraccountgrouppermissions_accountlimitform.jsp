<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_peraccountgrouppermissions_accountlimitform" className="moduleHelpClass"/>
<% 
	if(request.getParameter("runSearch") != null)
		request.setAttribute("runSearch",request.getParameter("runSearch")); 
	request.setAttribute("nextPermission", request.getParameter("nextPermission"));
%>

<ffi:cinclude value1="${runSearch}" value2="false" operator="equals">

	<%
	session.setAttribute("EditGroup_AccountGroupID",request.getParameter("EditGroup_AccountGroupID"));
	session.setAttribute("SetBusinessEmployee.Id",request.getParameter("SetBusinessEmployee.Id"));
	session.setAttribute("EditGroup_GroupName",request.getParameter("EditGroup_GroupName"));
	session.setAttribute("EditGroup_GroupId",request.getParameter("EditGroup_GroupId"));
	session.setAttribute("EditGroup_Supervisor",request.getParameter("EditGroup_Supervisor"));
	session.setAttribute("EditGroup_DivisionName",request.getParameter("EditGroup_DivisionName"));
	session.setAttribute("EditGroup_GroupId",request.getParameter("EditGroup_GroupId"));
	session.setAttribute("PermissionsWizard",request.getParameter("PermissionsWizard"));
	
	session.setAttribute("perm_target",request.getParameter("perm_target"));
	%>
	<s:include value="inc/peraccountgrouppermissions-pre.jsp" />
</ffi:cinclude>

<% session.setAttribute("FFIEditAccountGroupPermissions",session.getAttribute("EditAccountGroupPermissions")); %>

<!-- The Account limit Table -->
<ffi:cinclude value1="${MyAccountGroups.size}" value2="0" operator="notEquals">

<div align="left">
   <s:form id="permLimitFrm" name="permLimitFrm" namespace="/pages/user" action="editAccountGroupPermissions-verify"  validate="false" theme="simple" method="post">
					
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
        <input type="hidden" name="EditGroup_AccountGroupID" value='<ffi:getProperty name="EditGroup_AccountGroupID"/>'>
        <input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">
		<div align="center" class="marginBottom10 marginTop10"><ffi:getProperty name="AccountGrp" property="Name"/> - <ffi:getProperty name="AccountGrp" property="AcctGroupId"/></div>
		<div class="paneWrapper">
  			<div class="paneInnerWrapper">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="adminBackground">
                             <tr class="header">
                              <ffi:cinclude value1="${AdminCheckBoxType}" value2="hidden" operator="notEquals">
                           		<td valign="middle" align="center" nowrap class="sectionsubhead">
                               	 	<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
							<input type="checkbox" value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'admin');"> <s:text name="jsp.user_32"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
							<input type="checkbox" disabled value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'admin');"> <s:text name="jsp.user_32"/>
						</ffi:cinclude>
					</td>
                        		</ffi:cinclude>
                           		 <td valign="middle" align="center" nowrap class="sectionsubhead">
                                        
                               		 <ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
							<input type="checkbox" value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'entitlement');"> <s:text name="jsp.user_177"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
								<input type="checkbox" disabled value="ACTIVATE ALL" onclick="setCheckboxes(this.form,this.checked,'entitlement');"> <s:text name="jsp.user_177"/>
						</ffi:cinclude>
                            	</td>
                                <td valign="middle" align="left" class="sectionsubhead">
                                                 <s:text name="jsp.user_346"/>
                                </td>
                                <td colspan="2" valign="middle" align="center" class="sectionsubhead">
						<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
						<ffi:process name="GetLimitBaseCurrency" />
                                                 <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                </td>
                                <td valign="middle" align="center" class="sectionsubhead">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                                 <s:text name="jsp.user_153"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
							&nbsp;
						</ffi:cinclude>
                                </td>
                                <td colspan="2" valign="middle" align="center" class="sectionsubhead">
                                                 <s:text name="jsp.default_261"/> (<s:text name="jsp.user_173"/> <ffi:getProperty name="<%= com.ffusion.tasks.util.GetLimitBaseCurrency.BASE_CURRENCY %>"/>)
                                </td>
                                <td valign="middle" align="center" class="sectionsubhead">
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="equals">
                                                 <s:text name="jsp.user_153"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${ApprovalAdminByBusiness.Entitled}" value2="TRUE" operator="notEquals">
							&nbsp;
						</ffi:cinclude>
                                </td>
                             </tr>

<%-- Flag so that the included pages that actually display the limits and entitlements
	 will include the javascript function that makes parent/child work --%>
<ffi:setProperty name="ParentChild" value="true"/>

<%-- Flag to determine if the admin column should be drawn --%>
<ffi:setProperty name="HasAdmin" value="true"/>

<%-- Flag to indicate that the per account page is being displayed (and that cross account entitlements should be checked --%>
<ffi:setProperty name="PerAccount" value="true"/>
<%
session.setAttribute( "limitIndex", new Integer( 0 ) );
session.setAttribute( "LimitsList", session.getAttribute("AccountEntitlementsMerged") );
%>
<ffi:setProperty name="DisplayedLimit" value="FALSE"/>
<ffi:setProperty name="CheckEntitlementObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT_GROUP %>"/>
<ffi:setProperty name="CheckEntitlementObjectId" value="${AccountGrp.Id}"/>
<ffi:flush/>

<ffi:cinclude value1="${MyAccountGroups.size}" value2="0" operator="notEquals">
<s:if test="%{#session.Section == 'Users' || #session.Section == 'Profiles'}">
<s:include value="inc/corpadminuserlimit_merged.jsp"/>
</s:if>
<s:if test="%{#session.Section != 'Users' && #session.Section != 'Profiles'}">
<s:include value="inc/corpadmingrouplimit_merged.jsp"/>
</s:if>
</ffi:cinclude>

<ffi:cinclude value1="${DisplayedLimit}" value2="FALSE" operator="equals">
                                                <tr>
                                                    <td colspan="<ffi:getProperty name="NumTotalColumns"/>" valign="middle" align="center" style="padding-top:8px;">
                                                        <span class="columndata"><s:text name="jsp.user_213"/></span>
                                                    </td>
                                                </tr>
</ffi:cinclude>
								</table>
						<div class="btn-row" style="margin-bottom:25px !important;">
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="notEquals">
								<ffi:cinclude value1="${FromBack}" value2="TRUE" operator="notEquals">
										<sj:a  id="perAccountGrpResetButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon' 
											title='<s:text name="jsp.default_358.1" />' 
											href='#'
											onClick='ns.admin.resetFormForPerAcctGrp(); removeValidationErrors();'
											button="true">
											<s:text name="jsp.default_358"/>
										</sj:a>
								</ffi:cinclude>
								
								<ffi:cinclude value1="${FromBack}" value2="TRUE" operator="equals">
										<a  class='ui-button ui-widget ui-state-default ui-corner-all' 
											title='<s:text name="jsp.default_358.1" />' 
											href='#'
											onClick='ns.admin.reset( "<ffi:urlEncrypt url='/cb/pages/jsp/user/peraccountgrouppermissions.jsp?ComponentIndex=${ComponentIndex}&perm_target=${perm_target}&completedTopics=${completedTopics}&nextPermission=${nextPermission}'/>",'#<ffi:getProperty name="perm_target"/>');'>
											<span class="ui-button-text"><s:text name="jsp.default_358"/></span>
										</a>
								</ffi:cinclude>
								<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
									<sj:a
										button="true"
										onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
								</ffi:cinclude>
								<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
									<sj:a
										button="true"
										onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
								</ffi:cinclude>
								<%-- <ffi:cinclude value1="${CurrentWizardPage}" value2="" operator="equals"> --%>
								  	<sj:a
										formIds="permLimitFrm"
										targets="%{#session.perm_target}"
										button="true"
										validate="true"
										validateFunction="customValidation"
										onBeforeTopics="beforeVerify"
										><s:text name="jsp.default_366"/></sj:a>
								 <%-- </ffi:cinclude> --%>
                                <% boolean includeNEXTPage = false;
                                // if NOT ENTITLED to LOCATION or WIRE TEMPLATES, don't show the
                                // NEXT PERMISSION button
                                %>
                                <ffi:cinclude value1="${ComponentIndex}" value2="${pgcount}" operator="notEquals">
                                    <% includeNEXTPage = true; %>
                                </ffi:cinclude>
                                <s:include value="%{#session.PagesPath}user/inc/checkentitledlocations.jsp" />
                                <ffi:cinclude value1="${CheckEntitlementByGroupCB.Entitled}" value2="TRUE" operator="equals">
                                    <ffi:cinclude value1="${displayPerLocation}" value2="TRUE" operator="equals">
                                        <% includeNEXTPage = true; %>
                                    </ffi:cinclude>
                                </ffi:cinclude>
                                <ffi:removeProperty name="CheckEntitlementByGroupCB"/>

                                <ffi:cinclude value1="${ComponentIndex}" value2="${pgcount}" operator="equals">
                                    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>">
                                        <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE %>">
                                            <% includeNEXTPage = true; %>
                                        </ffi:cinclude>
                                    </ffi:cinclude>
                                </ffi:cinclude>

                                <% if (includeNEXTPage) { %>
									<sj:a
										id="%{#attr.nextPermission}"
										button="true" 
										onclick="ns.admin.nextPermissionForPerAcctGrp();"
										><s:text name="jsp.user_203"/></sj:a>							 
                                    <% } %>
							    	</ffi:cinclude>
							<%--
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
								<ffi:cinclude value1="${Section}" value2="Company" operator="notEquals">
									<input class="submitbutton" type="button" name="submitButtonName" value="BACK" border="0" onclick="location.replace('<ffi:getProperty name="SecurePath"/>user/permissions.jsp')">
								</ffi:cinclude>
								<ffi:cinclude value1="${Section}" value2="Company" operator="equals">
									<input class="submitbutton" type="button" name="submitButtonName" value="BACK" border="0" onclick="location.replace('<ffi:getProperty name="SecurePath"/>user/corpadmininfo.jsp')">
								</ffi:cinclude>
							</ffi:cinclude>		
							--%>
							<ffi:cinclude value1="${ViewUserPermissions}" value2="TRUE" operator="equals">
								<sj:a
									button="true"
									onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
							</ffi:cinclude>
						</div></div></div>
	</s:form>
</div>
</ffi:cinclude>
	<!-- End of The Account limit table -->
