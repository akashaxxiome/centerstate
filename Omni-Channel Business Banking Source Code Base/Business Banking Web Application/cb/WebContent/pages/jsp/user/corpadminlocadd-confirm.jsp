<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@page import="com.ffusion.util.logging.TrackingIDGenerator"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>


<ffi:help id="user_corpadminlocadd-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_78')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<%
	String locationBpwdId = request.getParameter("locationBpwdId");
	if(locationBpwdId!= null){
		session.setAttribute("locationBpwdId", locationBpwdId);
	}
%>

<%-- DA mode processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">

	<% String trackingId = null; %>
	<ffi:cinclude value1="${locationBpwdId}" value2="">
		<% trackingId = TrackingIDGenerator.GetNextID(); %>
	</ffi:cinclude>
	<ffi:cinclude value1="${locationBpwdId}" value2="" operator="notEquals">
		<% trackingId = (String) session.getAttribute("locationBpwdId"); %>
	</ffi:cinclude>
	<ffi:removeProperty name="locationBpwdId"/>

	<ffi:setProperty name="AddLocation" property="locationBPWID" value="<%= trackingId %>"/>
	<ffi:setProperty name="AddLocation" property="ProcessFlag" value="false" />
	<ffi:process name="AddLocation"/> 

	<ffi:object id="AddLocationToDA" name="com.ffusion.tasks.dualapproval.AddLocationToDA" scope="session"/>
	<ffi:setProperty name="AddLocationToDA" property="CategoryType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION %>"/>
	<ffi:setProperty name="AddLocationToDA" property="UserAction" value="<%= String.valueOf( com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.USER_ACTION_ADDED ) %>"/>
	<ffi:setProperty name="AddLocationToDA" property="ItemId" value="${EditDivision_GroupId}"/>
	<ffi:setProperty name="AddLocationToDA" property="newLocationSessionName" value="AddLocation"/>
	<ffi:setProperty name="AddLocationToDA" property="childSequence" value="${childSequence}"/>
	<ffi:setProperty name="AddLocationToDA" property="objectId" value="<%= trackingId %>"/>
	<ffi:setProperty name="AddLocationToDA" property="objectType" value="<%=IDualApprovalConstants.CATEGORY_LOCATION %>"/>

	<%-- sendForApproval property will decide message to be shown on this page --%>

	<ffi:process name="AddLocationToDA"/>

	<ffi:removeProperty name="AddLocationToDA"/>
	<ffi:removeProperty name="childSequence"/>
</ffi:cinclude>

		<div align="center">
		<TABLE class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD class="columndata" align="center">
							
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">

									<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminlocadd-confirm.jsp-1" parm0="${AddLocation.LocationName}"/>
							
								</ffi:cinclude>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									<input value="true" id="isDA" type="hidden"/>
									<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminlocadd-confirm.jsp-2" parm0="${AddLocation.LocationName}"/>
								</ffi:cinclude>
							
							</TD>
						</TR>
						<TR>
							<TD><IMG src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0" width="1" height="15"></TD>
						</TR>
						<TR>
							<TD align="center">
								<sj:a button="true" summaryDivId="summary" buttonType="done" onClickTopics="closeDialog,cancelLocationForm"><s:text name="jsp.default_175"/></sj:a>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
			<p></p>
		</div>
<ffi:removeProperty name="AddLocation"/>
<ffi:removeProperty name="verifyEditAddLocation"/>
<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadminlocations-pre.jsp"/>
