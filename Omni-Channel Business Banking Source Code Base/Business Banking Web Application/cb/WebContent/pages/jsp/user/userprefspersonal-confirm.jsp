<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>


<%

	session.setAttribute("Street", request.getParameter("Street"));
	session.setAttribute("Street2", request.getParameter("Street2"));
	session.setAttribute("State_ResourceFilename", request.getParameter("State_ResourceFilename"));
	session.setAttribute("State_ResourceID", request.getParameter("State_ResourceID"));
	session.setAttribute("Country_ResourceFilename", request.getParameter("Country_ResourceFilename"));
	session.setAttribute("Country_ResourceID", request.getParameter("Country_ResourceID"));
	session.setAttribute("City", request.getParameter("City"));
	session.setAttribute("State", request.getParameter("State"));
	session.setAttribute("ZipCode", request.getParameter("ZipCode"));
	session.setAttribute("Country", request.getParameter("Country"));
	session.setAttribute("PreferredLanguage", request.getParameter("PreferredLanguage"));
	session.setAttribute("Phone", request.getParameter("Phone"));
	session.setAttribute("FaxPhone", request.getParameter("FaxPhone"));
	session.setAttribute("DataPhone", request.getParameter("DataPhone"));
	session.setAttribute("FirstName", request.getParameter("FirstName"));
	session.setAttribute("LastName", request.getParameter("LastName"));
	session.setAttribute("CurrentPassword", request.getParameter("CurrentPassword"));
	session.setAttribute("ConfirmPassword", request.getParameter("ConfirmPassword"));
	session.setAttribute("PasswordClue", request.getParameter("PasswordClue"));
	session.setAttribute("NewPasswordReminder", request.getParameter("NewPasswordReminder"));
	session.setAttribute("ConfirmPasswordReminder2", request.getParameter("ConfirmPasswordReminder2"));
	session.setAttribute("PasswordClue2", request.getParameter("PasswordClue2"));
	session.setAttribute("ConfirmPasswordReminder", request.getParameter("ConfirmPasswordReminder"));
	session.setAttribute("NewPassword", request.getParameter("NewPassword"));
	session.setAttribute("NewPasswordReminder2", request.getParameter("NewPasswordReminder2"));
	session.setAttribute("Email", request.getParameter("Email"));


%>
<ffi:removeProperty name="ModifyUser"/>

<ffi:removeProperty name="State_ResourceFilename"/>
<ffi:removeProperty name="State_ResourceID"/>
<ffi:removeProperty name="Country_ResourceFilename"/>
<ffi:removeProperty name="Country_ResourceID"/>

<ffi:setProperty name="BusinessEmployee" property="Street" value="${Street}"/>
<ffi:setProperty name="BusinessEmployee" property="Street2" value="${Street2}"/>
<ffi:setProperty name="BusinessEmployee" property="City" value="${City}"/>
<ffi:setProperty name="BusinessEmployee" property="State" value="${State}"/>
<ffi:setProperty name="BusinessEmployee" property="ZipCode" value="${ZipCode}"/>
<ffi:setProperty name="BusinessEmployee" property="Country" value="${Country}"/>
<ffi:setProperty name="BusinessEmployee" property="PreferredLanguage" value="${PreferredLanguage}" />
<ffi:setProperty name="BusinessEmployee" property="Phone" value="${Phone}"/>
<ffi:setProperty name="BusinessEmployee" property="FaxPhone" value="${FaxPhone}"/>
<ffi:setProperty name="BusinessEmployee" property="DataPhone" value="${DataPhone}"/>
<ffi:setProperty name="BusinessEmployee" property="Email" value="${Email}"/>
<ffi:setProperty name="BusinessEmployee" property="PasswordClue" value="${PasswordClue}"/>
<ffi:setProperty name="BusinessEmployee" property="PasswordReminder" value="${PasswordReminder}"/>
<ffi:setProperty name="BusinessEmployee" property="PasswordClue2" value="${PasswordClue2}"/>
<ffi:setProperty name="BusinessEmployee" property="PasswordReminder2" value="${PasswordReminder2}"/>
<ffi:setProperty name="BusinessEmployee" property="UserName" value="${ModifyUser.UserName}"/>
<ffi:setProperty name="BusinessEmployee" property="CurrentPassword" value="${CurrentPassword}"/>
<ffi:setProperty name="BusinessEmployee" property="NewPassword" value="${NewPassword}"/>
<ffi:setProperty name="BusinessEmployee" property="ConfirmPassword" value="${ConfirmPassword}"/>

<div id="userpreferenceConfirmTextID" align="center">
	<table style="width:100%; height:100%;" cellspacing="0" cellpadding="0" class="tableData">
		<tr>
			<td class="columndata" style="text-align: center;"><s:text name="jsp.user_389"/></td>
		</tr>	
	</table>
</div>

<ffi:removeProperty name="ModifyUser"/>
<ffi:removeProperty name="Street" />
<ffi:removeProperty name="Street2" />
<ffi:removeProperty name="City" />
<ffi:removeProperty name="State" />
<ffi:removeProperty name="ZipCode" />
<ffi:removeProperty name="Country" />
<ffi:removeProperty name="PreferredLanguage" />
<ffi:removeProperty name="Phone" />
<ffi:removeProperty name="FaxPhone" />
<ffi:removeProperty name="DataPhone" />
<ffi:removeProperty name="Email" />
<ffi:removeProperty name="FirstName" />
<ffi:removeProperty name="LastName" />
<ffi:removeProperty name="CurrentPassword" />
<ffi:removeProperty name="NewPassword" />
<ffi:removeProperty name="PasswordClue" />
<ffi:removeProperty name="PasswordClue2" />
<ffi:removeProperty name="NewPasswordReminder" />
<ffi:removeProperty name="NewPasswordReminder2" />
<ffi:removeProperty name="pageRefresh"/>
<ffi:removeProperty name="ConfirmPasswordReminder"/>
<ffi:removeProperty name="ConfirmPasswordReminder2"/>
<ffi:removeProperty name="tmp_checkReminder1"/>
<ffi:removeProperty name="tmp_checkReminder2"/>
