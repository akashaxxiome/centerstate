<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_peraccountgrouppermissions_groupform" className="moduleHelpClass"/>
<div align="center">
	<ffi:cinclude value1="${MyAccountGroups.size}" value2="0" operator="equals">
		<div align="center">
			<div class="sectionsubhead"><s:text name="jsp.user_325"/></div>
			<div class="marginTop10">    <sj:a
					    	href="#"
							button="true"
							onClickTopics="cancelPermForm,hideCloneUserButtonAndCloneAccountButton"><s:text name="jsp.default_102"/></sj:a>
			<%-- no groups, but still may need next permission button. If NOT ENTITLED to LOCATION or WIRE TEMPLATES, don't show the NEXT PERMISSION button --%>
			<% boolean includeNEXTPage = false; %>
			<ffi:cinclude value1="${ComponentIndex}" value2="${pgcount}" operator="notEquals">
				<% includeNEXTPage = true; %>
			</ffi:cinclude>
			<s:include value="%{#session.PagesPath}user/inc/checkentitledlocations.jsp" />
			<ffi:cinclude value1="${CheckEntitlementByGroupCB.Entitled}" value2="TRUE" operator="equals">
				<ffi:cinclude value1="${displayPerLocation}" value2="TRUE" operator="equals">
					<% includeNEXTPage = true; %>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:removeProperty name="CheckEntitlementByGroupCB"/>
			<ffi:cinclude value1="${ComponentIndex}" value2="${pgcount}" operator="equals">
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>">
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE %>">
						<% includeNEXTPage = true; %>
					</ffi:cinclude>
				</ffi:cinclude>
			</ffi:cinclude>

			<% if (includeNEXTPage) { %>
				<sj:a
					href="#"
					id="%{#attr.nextPermission}"
					button="true"
					onclick="ns.admin.nextPermissionForPerAcctGrp();"
					><s:text name="jsp.user_203"/></sj:a>
				<% } %>
		</div>	</div>
	</ffi:cinclude>
	<ffi:cinclude value1="${MyAccountGroups.size}" value2="0" operator="notEquals">
		<s:form id="perAcctGrpViewFrm" name="perAcctGrpViewFrm" namespace="/pages/user" action="peraccountgrouppermissions_accountlimitform" validate="false" theme="simple" method="post">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<input type="hidden" name="runSearch" value="false">
				<input type="hidden" name="Section" value="<ffi:getProperty name="Section"/>">
				<input type="hidden" name="EditGroup_GroupId" value="<ffi:getProperty name="EditGroup_GroupId"/>">
				<input type="hidden" name="SetBusinessEmployee.Id" value="<ffi:getProperty name="BusinessEmployee" property="Id"/>">
				<input type="hidden" name="EditGroup_GroupName" value="<ffi:getProperty name="EditGroup_GroupName"/>">
				<input type="hidden" name="EditGroup_Supervisor" value="<ffi:getProperty name="EditGroup_Supervisor"/>">
				<input type="hidden" name="EditGroup_DivisionName" value="<ffi:getProperty name="EditGroup_DivisionName"/>">
				<input type="hidden" name="EditGroup_Supervisor" value="<ffi:getProperty name="EditGroup_Supervisor"/>">
				<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard"/>">
				
				<input type="hidden" name="perm_target" value="<ffi:getProperty name="perm_target"/>">
			<table border="0">
				<tr>
					<td class="columndata" height="20" align="left"><s:text name="jsp.user_286"/></td>
				</tr>
				<tr>
					<td align="center" class="sectionsubhead">&nbsp;&nbsp;&nbsp;
						<select id="EditGroup_AccountGroupID" name="EditGroup_AccountGroupID" class="txtbox" size="<ffi:getProperty name="accountGroupDisplaySize"/>" onchange="loadAccountDetails()">
							<ffi:list collection="MyAccountGroups" items="AccountGroupItem">
								<option value="<ffi:getProperty name="AccountGroupItem" property="Id"/>"
									<ffi:list collection="daObjectIds" items="daGroupId">
										<ffi:cinclude value1="${daGroupId}" value2="${AccountGroupItem.Id}" operator="equals"> class="columndataDA"</ffi:cinclude>
									</ffi:list>
									>
									<ffi:getProperty name="AccountGroupItem" property="Name"/> -
									<ffi:getProperty name="AccountGroupItem" property="AcctGroupId"/>
								</option>
							</ffi:list>
						</select>
					</td>
				</tr>
				<tr>
					<td class="sectionsubhead" colspan="2" align="center">
						<sj:a
						id="viewAccountDetails"
							href="#"
							formIds="perAcctGrpViewFrm"
							targets="acctLimitDiv"
							button="true"
							cssStyle="display:none"
							validate="false"
							><s:text name="jsp.default_459"/></sj:a>
					</td>
				</tr>
			</table>
		</s:form>
	</ffi:cinclude>
</div>
<script language="javascript">
$(function(){
	$("#EditGroup_AccountGroupID").selectmenu({width: 150});
});

function loadAccountDetails(){
	$('#viewAccountDetails').trigger('click')
}
</script>