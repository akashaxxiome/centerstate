<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setL10NProperty name='PageHeading' value='Verify Submit Division for Approval'/>

<% session.setAttribute("itemId",request.getParameter("itemId"));%>
<% session.setAttribute("itemType",request.getParameter("itemType"));%>
<% session.setAttribute("GroupName",request.getParameter("GroupName"));%>

<ffi:object name="com.ffusion.tasks.dualapproval.SubmitForApproval" id="SubmitForApprovalTask" />
<ffi:setProperty name="SubmitForApprovalTask" property="itemType" value="${itemType}" />
<ffi:setProperty name="SubmitForApprovalTask" property="itemId" value="${itemId}" />

<% session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("SubmitForApprovalTask") ); %>

		<div align="center">
		<table class="adminBackground" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="columndata" align="center">
								<br>
								<s:text name="da.verify.submit.division">
									<s:param>
										<ffi:getProperty name="GroupName"/>
									</s:param>
								</s:text>
							</td>
							<td></td>
						</tr>
						<tr>
							<td align="center">
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>

								<s:url id="submitDivisionForApprovalUrl" escapeAmp="false" value="/pages/dualapproval/dualApprovalAction-submitForApproval.action?itemId=${itemId}&itemType=${itemType}&module=Division&daAction=Pending Approval">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>  
								</s:url>

								<sj:a id="submitDivisionForApprovalLink"
									href="%{submitDivisionForApprovalUrl}"
									targets="resultmessage"
									button="true"
									title="%{getText('jsp.default_tile_confirm')}"
									onClickTopics=""
									onSuccessTopics="DADivisionSubmitForApprovalCompleteTopics"
									onCompleteTopics=""
									onErrorTopics="">
									<s:text name="jsp.default_107" /> 
								</sj:a>

							</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
			<p></p>
		</div>

<ffi:removeProperty name="GroupName"/>