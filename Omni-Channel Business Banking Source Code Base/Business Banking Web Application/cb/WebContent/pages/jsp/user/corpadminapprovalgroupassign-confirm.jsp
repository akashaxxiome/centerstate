<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="user_corpadminapprovalgroupassign-confirm" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_79')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

<div align="center" class="marginTop20">
	<div class="columndata marginBottom10" align="center"><s:text name="jsp.user_45"/></div>
	<div class="btn-row"><sj:a button="true" onClickTopics="cancelUserForm"><s:text name="jsp.default_175"/></sj:a></div>
</div>
<ffi:removeProperty name="AddApprovalGroup"/>
