<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:help id="user_autoentitle-confirm" className="moduleHelpClass"/>
<ffi:setProperty name='PageHeading' value='Auto-Entitle Confirmation'/>
<ffi:setProperty name="SavePermissionsWizard" value="${PermissionsWizard}"/>	
<div align="center" style="position:relative; overflow:hidden">
<div class="marginTop10"><ffi:getProperty name="Context"/></div>
<div class="marginTop10"></div>

						<s:form namespace="/pages/user" action="%{#session.action_execute}" validate="false" theme="simple" method="post" name="autoEntConfirm" id="autoEntConfirm">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			    			<input type="hidden" name="PermissionsWizard" value="<ffi:getProperty name="SavePermissionsWizard" />" >
						<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">								
							<tr>
								<td colspan="2" style="text-align:center" nowrap><span class="columndata"><ffi:getProperty name="autoEntitleConfirmMsg"/></span></td>
							</tr>
							<tr>
								<td style="text-align:right"><input type="radio" name="doAutoEntitle" value="true" checked="checked"/></td>
								<td class="columndata" style="text-align:left"><s:text name="jsp.default_467"/></td>
							</tr>
							<tr>
								<td style="text-align:right"><input type="radio" name="doAutoEntitle" value="false"/></td>
								<td class="columndata" style="text-align:left"><s:text name="jsp.default_295"/></td>
							</tr>
						 </table>
						 <div class="btn-row">
						 	<s:url id="inputUrl" value="%{#session.autoEntitleBackURL}" escapeAmp="false">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								</s:url>
								<sj:a
									href="%{inputUrl}"
									targets="%{#session.perm_target}"
									button="true"
									><s:text name="jsp.default_57"/></sj:a>
								<sj:a
									button="true"
									onClickTopics="cancelPermForm"
									><s:text name="jsp.default_102"/></sj:a>
								<%-- <sj:submit
									id="autoEntSubmit"
									formIds="autoEntConfirm"
									targets="resultmessage"
									button="true"
									buttonIcon="ui-icon-check"
									validate="false"
									onBeforeTopics="disableSubmit"
									onErrorTopics="errorSubmit"
									onCompleteTopics="%{#session.completedTopics}"
									value="%{getText('jsp.default_366')}"
									/> --%>
								<sj:a
									id="autoEntSubmit"
									formIds="autoEntConfirm"
									targets="resultmessage"
									button="true"
									validate="false"
									onBeforeTopics="disableSubmit"
									onErrorTopics="errorSubmit"
									onCompleteTopics="%{#session.completedTopics}"
									><s:text name="jsp.default_366" /></sj:a>
						 </div>
			   		 </s:form>
</div>
<%-- Disable the submit button so the form can't be submitted twice. Processing may be slow and the spinner doesn't always block user input after a submit. --%>
<script>
	$.subscribe('disableSubmit', function(event, data)
	{
		$('#autoEntSubmit').attr('disabled', 'disabled');
	});
</script>

<ffi:removeProperty name="SavePermissionsWizard" />

<%-- remove action and topic name unnecessary --%>
<ffi:removeProperty name="action" />
<ffi:removeProperty name="action_execute" />
<ffi:removeProperty name="completedTopics" />
<ffi:removeProperty name="perm_target" />
