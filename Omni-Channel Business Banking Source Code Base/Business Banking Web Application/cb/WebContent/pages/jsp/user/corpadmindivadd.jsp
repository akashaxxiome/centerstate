<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroupMember" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.web.util.entitlements.EntitlementsUtil"%>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>
<ffi:removeProperty name="Entitlement_CanAdminister"/>

<ffi:help id="user_corpadmindivadd" className="moduleHelpClass"/>
<span id="PageHeading" style="display:none;"><s:text name="jsp.user_16"/></span>
<ffi:setProperty name='PageText' value=''/>
<%
	String CancelReload = request.getParameter("CancelReload");
	if(CancelReload!= null){
		session.setAttribute("CancelReload", CancelReload);
	}
	String AdminsSelected = request.getParameter("AdminsSelected");
	if(AdminsSelected!= null){
		session.setAttribute("AdminsSelected", AdminsSelected);
	}
	String AutoEntitleAdministrators = request.getParameter("AutoEntitleAdministrators");
	if(AutoEntitleAdministrators!= null){
		session.setAttribute("AutoEntitleAdministrators", AutoEntitleAdministrators);
	}
	String UseLastRequest = request.getParameter("UseLastRequest");
	if(UseLastRequest!= null){
		session.setAttribute("UseLastRequest", UseLastRequest);
	}
%>

<ffi:cinclude value1="${CancelReload}" value2="TRUE" operator="equals">
<%
// The user has pressed cancel, so go back to the previous version of the collections
session.setAttribute( "NonAdminEmployees", session.getAttribute("NonAdminEmployeesCopy"));
session.setAttribute( "AdminEmployees", session.getAttribute("AdminEmployeesCopy"));
session.setAttribute( "NonAdminGroups", session.getAttribute("NonAdminGroupsCopy"));
session.setAttribute( "AdminGroups", session.getAttribute("AdminGroupsCopy"));
session.setAttribute( "adminMembers", session.getAttribute("adminMembersCopy"));
session.setAttribute( "userMembers", session.getAttribute("userMembersCopy"));
session.setAttribute( "groupIds", session.getAttribute("groupIdsCopy"));
session.setAttribute( "tempAdminEmps", session.getAttribute("tempAdminEmpsCopy") );
session.setAttribute( "GroupDivAdminEdited", session.getAttribute("GroupDivAdminEditedCopy"));
%>
</ffi:cinclude>

<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>

<ffi:removeProperty name="CancelReload"/>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
		<ffi:object id="AddBusinessGroup" name="com.ffusion.tasks.admin.AddBusinessGroup" scope="session"/>
		<% session.setAttribute("FFIAddBusinessGroup", session.getAttribute("AddBusinessGroup")); %>
	</ffi:cinclude>

	<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
		<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
	</ffi:cinclude>
	<ffi:removeProperty name="UseLastRequest"/>

	<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
		<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
	</ffi:cinclude>
</ffi:cinclude>

		<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2" align="center">
						<ul id="formerrors"></ul>
					</td>
				</tr>
				<tr>
					<td colspan="2">
					<!--BO Migration:: Added condition to Support DA business for Add division -->
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
						<ffi:setProperty name="actionName" value="addBusinessDivision-verify" />
					</ffi:cinclude>					
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">					
						<ffi:setProperty name="actionName" value="AddDivision_verify" />
					</ffi:cinclude>
										
					<s:form id="addDivFormId" namespace="/pages/user" action="%{#session.actionName}" theme="simple" name="divisionAddFormName" method="post">
						<!--BO Migration:: Added condition to Support DA business for Add division -->
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
							<input type="hidden" name="AddBusinessGroup.ParentGroupId" value="<ffi:getProperty name="Business" property="EntitlementGroupId"/>">
							<input type="hidden" name="UseLastRequest" value="TRUE">
							<input type="hidden" name="divAdd" value="true">
							<input type="hidden" name="AddBusinessGroup.GroupType" value="Division">
							<input type="hidden" name="AddBusinessGroup.CheckboxesAvailable" value="false">
						</ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<input type="hidden" name="parentId" value="<ffi:getProperty name="Business" property="EntitlementGroupId"/>">
							<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
								<input type="hidden" name="businessEmployees" value="<ffi:getProperty name="tempAdminEmps"/>" />
							</ffi:cinclude>	
						</ffi:cinclude>	
							<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							
							<table width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td width="40">&nbsp;</td>
									<td width="100"><s:text name="jsp.user_121"/><span class="required">*</span></td>
									<td align="left">
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
										<input class="ui-widget-content ui-corner-all" type="text" name="GroupName" style="width:120px;" size="24" maxlength="150" border="0" value="<ffi:getProperty name="AddBusinessGroup" property="GroupName"/>">
										<input class="ui-widget-content ui-corner-all" type="text" name="nonDisplayGroupName" size="24" maxlength="255" border="0" value="" style="display:none">
										(<ffi:getProperty name="Business" property="BusinessName"/>) &nbsp;&nbsp; <span id="NameError"></span>
									</ffi:cinclude>	
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
										<s:textfield cssClass="ui-widget-content ui-corner-all" type="text" name="name" size="24" maxlength="150" border="0" value="%{#session.division.name}" />																				 
										(<ffi:getProperty name="Business" property="BusinessName"/>) &nbsp;&nbsp; <span id="NameError"></span>
									</ffi:cinclude>	
									</td>
								</tr>
								<tr>
									<ffi:setProperty name="AddBusinessGroup" property="AdminListName" value="adminMembers"/>
									
									<td width="40">&nbsp;</td>
									<td><s:text name="jsp.user_38"/></td>
									<td valign="middle" align="left">
										<div style="display:inline-block;"  class="alignItemsDivision">
											<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
											<ffi:object id="FilterNotEntitledEmployees" name="com.ffusion.tasks.user.FilterNotEntitledEmployees" scope="request"/>
											   <ffi:setProperty name="FilterNotEntitledEmployees" property="BusinessEmployeesSessionName" value="tempAdminEmps" />
											   <ffi:setProperty name="FilterNotEntitledEmployees" property="EntToCheck" value="<%= EntitlementsDefines.DIVISION_MANAGEMENT%>" />
											<ffi:process name="FilterNotEntitledEmployees"/>
	 										<ffi:setProperty name="tempAdminEmps" property="SortedBy" value="<%= com.ffusion.util.beans.XMLStrings.NAME %>"/>
											<%--display name of each employee in BusinessEmployees--%>
		 									<%
 										       		boolean isFirst= true;
 												String temp = "";
	 									       	%>
 											<ffi:list collection="tempAdminEmps" items="emp">
			 								<%--display separator if this is not the first one--%>
											<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
	 											<ffi:setProperty name="empName" value="${emp.Name}"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
												<ffi:setProperty name="empName" value="${emp.LastName}"/>
											</ffi:cinclude>
 											<%
 											if( isFirst ) {
 												isFirst = false;
	 											temp = temp + session.getAttribute( "empName" );
		 									} else {
 												temp = temp + ", " + session.getAttribute( "empName" );
 											}
	 										%>
 											</ffi:list>
		 									<%
	 										if( temp.length() > 120 ) {
 												temp = temp.substring( 0, 120 );
 												int index = temp.lastIndexOf( ',' );
												temp = temp.substring( 0, index ) + "...";
		 									}
 											session.setAttribute( "AdministratorStr", temp );%>

	 										<ffi:getProperty name="AdministratorStr"/>
											<ffi:cinclude value1="AdministratorStr" value2="" operator="equals">
												<s:text name="jsp.default_296"/>
											</ffi:cinclude>
											<%-- set modifiedAdmins to "TRUE", so when the edit icon is clicked the corpadminadminedit.jsp --%>
											<%-- will look for the appropriate object in session for displaying --%>
											<ffi:setProperty name="modifiedAdmins" value="TRUE"/>
											<ffi:removeProperty name="empName"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
											<%-- By default, the administrator for the new group is the currently logged in user --%>
											
											<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
												<ffi:object id="GetBusinessEmployee" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
												<ffi:setProperty name="GetBusinessEmployee" property="ProfileId" value="${SecureUser.ProfileID}"/>
												<ffi:process name="GetBusinessEmployee"/>
												<ffi:removeProperty name="GetBusinessEmployee"/>
												<ffi:getProperty name="BusinessEmployee" property="Name"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
												<s:property value="%{#session.division.businessEmployeeNames}"/>
											</ffi:cinclude>
											<%
											   java.util.ArrayList adminMembers = new java.util.ArrayList();
											   BusinessEmployee tempEmp = (BusinessEmployee)session.getAttribute( com.ffusion.tasks.Task.BUSINESS_EMPLOYEE );
											   EntitlementGroupMember member = EntitlementsUtil.getEntitlementGroupMemberForBusinessEmployee(tempEmp);
									         	   adminMembers.add( member );
											   session.setAttribute( "adminMembers", adminMembers );
											   session.setAttribute( "AdministratorStr", tempEmp.getUserName() );
											%>
											<ffi:setProperty name="modifiedAdmins" value="FALSE"/>
										</ffi:cinclude>
										</div>
									   &nbsp;&nbsp;
									   
									   <s:hidden id="needVerify_HiddenFieldId" name="needVerify" value="true"/>
										<%-- Kaijies: add administrator icon button --%>
										<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="notEquals">
											<sj:a
												id="addDivAdminButtonId"
												button="true"
												targets="editAdminerDialogId"
												formIds="addDivFormId"	
												formid="addDivFormId"
												field1="needVerify_HiddenFieldId"
												value1="false"
												postaction="/cb/pages/user/addBusinessDivision-addDivModifyAdminerBeforeChange.action"																								
												buttontype="editadmin"
												onClickTopics="postFormTopic"												
												onCompleteTopics="openDivisionAdminerDialog"
											>
												<s:text name="jsp.default_178" />
											</sj:a>
										</ffi:cinclude>
										<ffi:cinclude value1="${AdminsSelected}" value2="TRUE" operator="equals">
											<sj:a
												id="addDivAdminButtonId"
												button="true"
												targets="editAdminerDialogId"
												formIds="addDivFormId"
												formid="addDivFormId"
												field1="needVerify_HiddenFieldId"
												value1="false"
												postaction="/cb/pages/user/addBusinessDivision-addDivModifyAdminerAfterChange.action"																								
												buttontype="editadmin"
												onClickTopics="postFormTopic"												
												onCompleteTopics="openDivisionAdminerDialog"		 			
											>
												<s:text name="jsp.default_178" />
											</sj:a>
										</ffi:cinclude>
										
										<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
											<ffi:removeProperty name="AdminsSelected"/>
										</ffi:cinclude>
										
										</td>
								</tr>
								<tr>
									<td colspan="3">
										<div align="center">
												<span class="required">* <s:text name="jsp.default_240"/></span>
												<br><br>
												
												<s:url id="resetAddDivButtonUrl" value="/pages/jsp/user/corpadmindivadd.jsp?UseLastRequest=FALSE">  
													<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
												</s:url>	
												<sj:a id="resetAddDivBtn"
		                                           href="%{resetAddDivButtonUrl}"
												   targets="inputDiv"
												   button="true"><s:text name="jsp.default_358"/></sj:a>


												<sj:a
												   id="divAddCancelButtonId"
												   button="true"
												    summaryDivId="summary" buttonType="cancel"
												   onClickTopics="cancelDivisionForm"><s:text name="jsp.default_82"/></sj:a>
												
												<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
													<sj:a
														id="divAddButtionId"
														formIds="addDivFormId"
														targets="verifyDiv"
														button="true"
														validate="true"
														validateFunction="customValidation"
														onBeforeTopics="beforeVerify2"
														onCompleteTopics="completeVerify2"
														onErrorTopics="errorVerify"
														onSuccessTopics="successVerify"
														formid="addDivFormId"
														field1="needVerify_HiddenFieldId"
														value1="true"
														postaction="/cb/pages/user/addBusinessDivision-verify.action"																								
														onClickTopics="postFormTopic"												
														><s:text name="jsp.default_29"/></sj:a>
												</ffi:cinclude>

												<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
												<sj:a
													id="divAddButtionId"
													formIds="addDivFormId"
													targets="verifyDiv"
													button="true"
													validate="true"
													validateFunction="customValidation"
													onBeforeTopics="beforeVerify2"
													onCompleteTopics="completeVerify2"
													onErrorTopics="errorVerify"
													onSuccessTopics="successVerify"
													formid="addDivFormId"
													field1="needVerify_HiddenFieldId"
													value1="true"
													postaction="/cb/pages/user/AddDivision_verify.action"																								
													onClickTopics="postFormTopic"												
													><s:text name="jsp.default_29"/></sj:a>
												</ffi:cinclude>													
											</div>
									</td>
								</tr>
							</table>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
		<script>
			$(document).ready(function(){
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#addDivFormId").setFocus();
				}
			});
		</script>
<ffi:removeProperty name="LastRequest"/>


<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="equals">
	<ffi:setProperty name="BackURL" value="/cb/pages/user/corpadmindivadd.jsp?UseLastRequest=TRUE&AdminsSelected=TRUE" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${modifiedAdmins}" value2="TRUE" operator="notEquals">
	<ffi:setProperty name="BackURL" value="/cb/pages/user/corpadmindivadd.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
</ffi:cinclude>

<ffi:setProperty name="onCancelGoto" value="corpadmindivadd.jsp"/>
<ffi:setProperty name="onDoneGoto" value="corpadmindivadd.jsp"/>

