<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>

<ffi:help id="user_corpadminprofiles" className="moduleHelpClass"/>
<s:include value="inc/corpadminprofiles-pre.jsp"/>

<ffi:removeProperty name="AddButton"/>
<ffi:setGridURL grid="GRID_UserProfile" name="DeleteURL" url="/cb/pages/user/deleteBusinessEntitlementProfile_init.action?employeeID={0}&selectedEntGroupId={1}" parm0="Id"  parm1="EntitlementGroupId"/>
<ffi:setGridURL grid="GRID_UserProfile" name="EditURL" url="/cb/pages/user/modifyBusinessEntitlementProfile_init.action?employeeID={0}&OneAdmin={1}&UsingEntProfiles={2}" parm0="Id" parm1="OneAdmin" parm2="UsingEntProfiles"/>
<ffi:setGridURL grid="GRID_UserProfile" name="PermissionsURL" url="/cb/pages/jsp/user/corpadminpermissions-selected.jsp?Section=Profiles&BusinessEmployeeId={0}&OneAdmin={1}&UsingEntProfiles={2}" parm0="Id" parm1="OneAdmin" parm2="UsingEntProfiles"/>

<ffi:setProperty name="adminProfileGridTempURL" value="getBusinessEntitlementProfiles?CollectionName=entitlementProfiles&GridURLs=GRID_UserProfile" URLEncrypt="true"/>
<ffi:setProperty name="adminProfileSubGridTempURL" value="getBusinessEntitlementProfilesChild?CollectionName=entitlementProfiles" URLEncrypt="false"/>
<s:url namespace="/pages/user" id="remoteurlProfile" action="%{#session.adminProfileGridTempURL}" escapeAmp="false"/>
     <sjg:grid
       id="adminProfileGrid"
       caption=""
	   sortable="true"  
       dataType="json"
       href="%{remoteurlProfile}"
       pager="true"
       viewrecords="true"
       gridModel="gridModel"
	   rowList="%{#session.StdGridRowList}" 
	   rowNum="%{#session.StdGridRowNum}" 
       rownumbers="false"
       altRows="true"
       sortname="USERNAME"
       sortorder="asc"
       shrinkToFit="true"
       navigator="true"
       groupCollapse="true"
       navigatorAdd="false"
       navigatorDelete="false"
       navigatorEdit="false"
       navigatorRefresh="false"
       navigatorSearch="false"
       onGridCompleteTopics="addGridControlsEvents,pendingWiresGridCompleteEvents"
     >
     <sjg:gridColumn name="userName" width="80" index="USERNAME" title="%{getText('jsp.user_267')}" sortable="true" />
     <sjg:gridColumn name="map.Group" width="60" index="Group" title="%{getText('jsp.default_225')}" sortable="true" />
     <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatProfileActionLinks" search="false" width="150" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
     <sjg:gridColumn name="map.IsAnAdminOf" width="0" index="IsAnAdminOf" title="%{getText('jsp.user_184')}" hidden="true" sortable="false" hidedlg="true"/>
	 <sjg:gridColumn name="map.OneAdmin" width="0" index="OneAdmin" title="%{getText('jsp.user_231')}" hidden="true" sortable="false" hidedlg="true"/>
 </sjg:grid>
<script type="text/javascript">
	var fnCustomProfilesRefresh = function (event) {
		ns.common.forceReloadCurrentSummary();
	};
	
	$("#adminProfileGrid").data('fnCustomRefresh', fnCustomProfilesRefresh);
	
	$("#adminProfileGrid").data("supportSearch",true);
</script>
