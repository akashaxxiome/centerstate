<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<ffi:help id="user_corpadminuseradd-autoentitle" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_54')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>

		<%-- include javascripts for top navigation menu bar --%>
				<%-- <TR>
					<TD class="sectionhead" align="center" valign="top">
						<DIV align="center">
							<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadminuseradd-autoentitle.jsp-1" parm0="${BusinessEmployee.FullName}"/><br>
							<hr width="90%" noshade size="1">
						</DIV>
					</TD>
				</TR> --%>
				<div class="leftPaneWrapper" id="userConfirmLeftPane">
					<div class="leftPaneInnerWrapper">					
								<div class="header">
									<s:text name="user.login.summary"/>
								</div>
								<div style="padding: 15px 10px;" class="summaryBlock">
									<div style="width:100%" class="floatleft">
											<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_455"/>:</span>
											<span id="" class="inlineSection floatleft labelValue"><ffi:getProperty name="BusinessEmployee" property="UserName"/></span>
									</div>
									<div style="width:100%" class="marginTop10 clearBoth floatleft">
											<span id="" class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.user_364"/>:</span>
											<span id="" class="inlineSection floatleft labelValue">
												<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
														<ffi:setProperty name="BusinessEmployee" property="AccountStatus" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_PENDING_APPROVAL )%>"/>
														<s:text name="jsp.user_235"/>
												</ffi:cinclude>
											
											<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
												<ffi:setProperty name="Compare" property="Value1" value="${BusinessEmployee.AccountStatus}" />
												<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_ACTIVE )%>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.default_28"/></ffi:cinclude>
												<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_INACTIVE )%>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.default_238"/></ffi:cinclude>
												<ffi:setProperty name="Compare" property="value2" value="<%=String.valueOf( com.ffusion.efs.adapters.profile.constants.ProfileDefines.STATUS_TEMP_INACTIVE )%>"/>
												<ffi:cinclude value1="true" value2="${Compare.Equals}"><s:text name="jsp.user_306"/></ffi:cinclude>
											</ffi:cinclude>
											</span>
								   </div>
								</div>
							</div>
				</div>
				<div class="confirmPageDetails">
						
						<s:form namespace="/pages/user" action="addBusinessUser_execute" theme="simple" method="post" name="autoEntConfirm" id="AutoEntConfirmId">
							<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<s:include value="corpadminuseradd-verify.jsp"/>

								<s:if test="%{#request.DoAutoEntitle}"> 
									<div class="summeryBlockMessage">
											<span class="columndata"><s:text name="jsp.user_127"/></span>&nbsp;
											<input type="radio" name="BusinessEmployee.AutoEntitle" value="true" checked="checked"/>&nbsp;<s:text name="jsp.default_467"/>&nbsp;&nbsp;
											<input type="radio" name="BusinessEmployee.AutoEntitle" value="false"/>&nbsp;<s:text name="jsp.default_295"/>
									</div>
								</s:if>
								<div class="btn-row">
										<sj:a id="backBtn2"
											  button="true"
											  onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
										<sj:a id="cancelBtn2"
											  button="true"
											  summaryDivId="summary" 
					  						  buttonType="cancel"
					  						  onClickTopics="showSummary,cancelUserForm"><s:text name="jsp.default_82"/></sj:a>
										<sj:a
												formIds="AutoEntConfirmId"
												id="addSubmit"
												targets="confirmDiv"
												button="true"
												validate="false"
												onBeforeTopics="beforeSubmit"
												onErrorTopics="errorSubmit"
												onSuccessTopics="completeAddUserAutoEntileSubmit"><s:text name="jsp.default_366"/></sj:a>


									</div>
							</TABLE>
						</s:form>
		</div>


