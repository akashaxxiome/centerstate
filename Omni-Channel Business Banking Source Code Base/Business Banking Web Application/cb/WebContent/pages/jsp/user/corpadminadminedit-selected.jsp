<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	String groupId = request.getParameter("EditDivision_GroupId");
	session.setAttribute("AdminEditType", request.getParameter("AdminEditType"));
%>

<ffi:cinclude value1="${GetEntitlementGroup}" value2="" operator="equals">
	<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" />
</ffi:cinclude>

<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="<%=groupId%>"/>
<ffi:setProperty name="GetEntitlementGroup" property="SessionName" value='<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>'/>
<ffi:process name="GetEntitlementGroup"/>
<ffi:object id="EditBusinessGroup" name="com.ffusion.tasks.admin.EditBusinessGroup" scope="session"/>
<ffi:setProperty name="EditBusinessGroup" property="GroupName" value="${Entitlement_EntitlementGroup.GroupName}"/>
<ffi:object id="GetSupervisorFor" name="com.ffusion.tasks.admin.GetSupervisorFor" scope="request" />
<ffi:setProperty name="GetSupervisorFor" property="GroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
<ffi:process name="GetSupervisorFor"/>
<ffi:cinclude value1="${GetSupervisorFor.SupervisorFound}" value2="true" operator="equals">
	<ffi:setProperty name="EditBusinessGroup" property="SupervisorId" value="${Supervisor.Id}"/>
</ffi:cinclude>
<ffi:cinclude value1="${GetSupervisorFor.SupervisorFound}" value2="true" operator="notEquals">
	<ffi:setProperty name="EditBusinessGroup" property="SupervisorId" value=""/>
</ffi:cinclude>

<%-- Set this flag for determining if to load the Entitlement groups for SetADministrator task --%>
<ffi:setProperty name="editDivisionTouched" value="true"/>

<ffi:setProperty name="EditGroup_DivisionName" value="${Entitlement_EntitlementGroup.GroupName}"/>
