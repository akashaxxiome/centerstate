<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<s:set var="tmpI18nStr" value="%{getText('jsp.user_83')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="user_corpadmindivedit-confirm" className="moduleHelpClass"/>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${EditDivision_GroupId}"/>
<ffi:process name="CanAdminister"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="notEquals">
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.ERROR_NOT_ENTITLED)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.SERVICE_ERROR%>"/>
	<ffi:process name="ThrowException"/>
</ffi:cinclude>

<ffi:removeProperty name="Entitlement_CanAdminister"/>

<%-- We would only have the usersMembers, adminMembers and groupIds in session if the admins has been edited --%>

	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
		
	 	<ffi:cinclude value1="${GroupDivAdminEdited}" value2="" operator="notEquals">
			<ffi:object id="SetAdministrators" name="com.ffusion.tasks.user.SetAdministrators"/>
			<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembers"/>
			<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembers"/>
			<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
			<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			<ffi:setProperty name="SetAdministrators" property="EntitlementGroupType" value="${Entitlement_EntitlementGroup.EntGroupType}"/>
			<ffi:setProperty name="SetAdministrators" property="EntitlementGroupName" value="${Entitlement_EntitlementGroup.GroupName}"/>
			<ffi:setProperty name="SetAdministrators" property="HistoryId" value="${Business.Id}"/>
			<ffi:setProperty name="SetAdministrators" property="Commit" value="true"/>
			<ffi:process name="SetAdministrators"/>
		</ffi:cinclude>

	 </ffi:cinclude>

	 <%-- Add administrator data in DA tables if Dual approval is enabled. --%>
	 <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">

	 	<ffi:setProperty name="EditBusinessGroup" property="ValidateOnly" value="true"/>
		<ffi:process name="EditBusinessGroup"/>

		<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
		<ffi:setProperty name="GetDAItem" property="itemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetDAItem" />

		<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
			<ffi:setProperty name="fromPendingTable" value="true"/>
		</ffi:cinclude>

		<ffi:object id="AddBusinessGroupToDA" name="com.ffusion.tasks.dualapproval.AddBusinessGroupToDA" scope="session"/>
		<ffi:setProperty name="AddBusinessGroupToDA" property="UserAction" value="<%= String.valueOf(IDualApprovalConstants.USER_ACTION_MODIFIED) %>"/>
		<ffi:setProperty name="AddBusinessGroupToDA" property="NewEntitlementGroupSessionName" value="NewEntitlementGroupDA"/>
		<ffi:setProperty name="AddBusinessGroupToDA" property="OldEntitlementGroupSessionName" value="Entitlement_EntitlementGroup"/>
		<ffi:setProperty name="AddBusinessGroupToDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
		<ffi:setProperty name="AddBusinessGroupToDA" property="ItemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		<ffi:setProperty name="AddBusinessGroupToDA" property="CategoryType" value="<%=IDualApprovalConstants.CATEGORY_PROFILE %>"/>
		<ffi:process name="AddBusinessGroupToDA"/>

	 	<ffi:cinclude value1="${GroupDivAdminEdited}" value2="" operator="notEquals">
			<ffi:object id="SetAdministrators" name="com.ffusion.tasks.user.SetAdministrators"/>
			<ffi:setProperty name="SetAdministrators" property="UserListName" value="userMembers"/>
			<ffi:setProperty name="SetAdministrators" property="AdminListName" value="adminMembers"/>
			<ffi:setProperty name="SetAdministrators" property="GroupListName" value="groupIds"/>
			<ffi:setProperty name="SetAdministrators" property="Commit" value="false"/>
			<ffi:setProperty name="SetAdministrators" property="EntitlementGroupId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			<ffi:setProperty name="SetAdministrators" property="EntitlementGroupType" value="${Entitlement_EntitlementGroup.EntGroupType}"/>
			<ffi:setProperty name="SetAdministrators" property="EntitlementGroupName" value="${Entitlement_EntitlementGroup.GroupName}"/>
			<ffi:setProperty name="SetAdministrators" property="HistoryId" value="${Business.Id}"/>
			<ffi:process name="SetAdministrators"/>

			<ffi:object id="SetAdministratorsInDA" name="com.ffusion.tasks.dualapproval.SetAdministratorsInDA"/>
			<ffi:setProperty name="SetAdministratorsInDA" property="AddedMemberIds" value="addedMemberIds"/>
			<ffi:setProperty name="SetAdministratorsInDA" property="RemovedMemberIds" value="removedMemberIds"/>
			<ffi:setProperty name="SetAdministratorsInDA" property="ItemId" value="${Entitlement_EntitlementGroup.GroupId}"/>
			<ffi:setProperty name="SetAdministratorsInDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_DIVISION %>"/>
			<ffi:setProperty name="SetAdministratorsInDA" property="UserAction" value="<%= String.valueOf(IDualApprovalConstants.USER_ACTION_MODIFIED) %>"/>
			<ffi:process name="SetAdministratorsInDA"/>
			<ffi:setProperty name="sendForApproval" value="true"/>
		</ffi:cinclude>
		<ffi:removeProperty name="DAItem"/>
		<ffi:removeProperty name="GetDAItem"/>
		<ffi:removeProperty name="ItemId"/>
		<ffi:removeProperty name="ItemType"/>
		<ffi:removeProperty name="SetAdministratorsInDA"/>
	</ffi:cinclude>

	<ffi:removeProperty name="adminMembers"/>
	<ffi:removeProperty name="adminIds"/>
	<ffi:removeProperty name="userIds"/>
	<ffi:removeProperty name="groupIds"/>
	<ffi:removeProperty name="userMembers"/>
	<ffi:removeProperty name="SetAdministrators"/>
	<ffi:removeProperty name="addedMemberIds"/>
	<ffi:removeProperty name="removedMemberIds"/>
	<%-- it is important to remove GroupDivAdminEdited object from session --%>
	<ffi:removeProperty name="GroupDivAdminEdited"/>

		<div align="center">

		<TABLE class="adminBackground" width="750" border="0" cellspacing="0" cellpadding="0">
			<TR>
				<TD>
					<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
						<TR>
							<TD class="columndata textWrapInTd" align="center" style="width:120px;">
								<ffi:cinclude value1="${sendForApproval}" value2="true" operator="notEquals">
									<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmindivedit-confirm.jsp-1" parm0="${EditBusinessGroup.GroupName}"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${sendForApproval}" value2="true">
									<ffi:cinclude value1="${fromPendingTable}" value2="true">
										<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmindivedit-confirm.jsp-2" parm0="${Entitlement_EntitlementGroup.GroupName}"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${fromPendingTable}" value2="true" operator="notEquals">
										<ffi:getL10NString rsrcFile="cb" msgKey="jsp/user/corpadmindivedit-confirm.jsp-2" parm0="${Entitlement_EntitlementGroup.GroupName}"/>
									</ffi:cinclude>
								</ffi:cinclude>
							</TD>
						</TR>
						<TR>
							<TD align="center">
								<div class="ffivisible marginTop10">
									<sj:a button="true"  onClickTopics="closeDialog,cancelDivisionForm"><s:text name="jsp.default_175"/></sj:a>
								</div>
							</TD>
						</TR>
					</TABLE>
				</TD>
			</TR>
		</TABLE>
		</div>
<ffi:removeProperty name="PossibleSupervisors"/>
<ffi:removeProperty name="sendForApproval"/>
<ffi:removeProperty name="fromPendingTable"/>
<ffi:removeProperty name="DALocations"/>
<ffi:removeProperty name="divisionEditDualApprovalMode"/>
<%-- include this so refresh of grid will populate properly --%>
<s:include value="inc/corpadmindiv-pre.jsp"/>
