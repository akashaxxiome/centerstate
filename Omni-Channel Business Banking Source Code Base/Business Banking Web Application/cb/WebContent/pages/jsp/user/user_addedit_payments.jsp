<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:setProperty name="PageTitle" value="Manage Users"/>
<ffi:setProperty name="topMenu" value="home"/>
<ffi:setProperty name="subMenu" value="preferences"/>
<ffi:setProperty name="sub2Menu" value="manageusers"/>

<ffi:setProperty name="SkipPage" value="FALSE"/>

<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'>
    <ffi:setProperty name="SkipPage" value="TRUE"/>
</ffi:cinclude>

<ffi:cinclude value1="${SkipPage}" value2="TRUE" operator="equals">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><ffi:getProperty name="PageTitle"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="<ffi:getProperty name="ServletPath"/>FFretail.css" type="text/css">
</head>
<ffi:setProperty name="TempURL" value="${SecurePath}user/user_addedit_payments_save.jsp?Target=${SecurePath}user/user_addedit_confirm.jsp" URLEncrypt="true"/>
<ffi:cinclude value1="${BackButton}" value2="" operator="notEquals">
    <ffi:setProperty name="TempURL" value="${SecurePath}user/user_addedit_payments_save.jsp?Target=${SecurePath}user/user_addedit_exttransfers.jsp" URLEncrypt="true"/>
</ffi:cinclude>
<body onload="document.location.href='<ffi:getProperty name="TempURL"/>';">
</body>
</html>
</ffi:cinclude>
<ffi:cinclude value1="${SkipPage}" value2="FALSE" operator="equals">
<ffi:cinclude value1="${AddEditMode}" value2="Edit" operator="equals">
    <ffi:cinclude value1="${IsBillPaymentPermissionsInitialized}" value2="" operator="equals">
        <ffi:setProperty name="IncludeFromSection" value="TRUE"/>
        <ffi:setProperty name="IncludeToSection" value="TRUE"/>
        <ffi:object name="com.ffusion.beans.util.StringList" id="AccountsCollectionNames" scope="session"/>
        <ffi:setProperty name="AccountsCollectionNames" property="Add" value="SecondaryUserEntitledCoreAccounts"/>
        <ffi:setProperty name="FromOperationName" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'/>

        <ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsFromFilters" scope="session"/>
        <ffi:setProperty name="AccountsFromFilters" property="Key" value="SecondaryUserEntitledCoreAccounts"/>
        <ffi:setProperty name="AccountsFromFilters" property="Value" value='<%= com.ffusion.beans.accounts.AccountFilters.BILL_PAY_FILTER %>'/>

        <ffi:setProperty name="FeatureAccessAndLimitsTaskName" value="FFIBillPaymentAccessAndLimits"/>
		<s:include value="/pages/jsp/user/inc/user_addedit_permissions_init.jsp" />
        
    </ffi:cinclude>

    <ffi:setProperty name="IsBillPaymentPermissionsInitialized" value="true"/>
</ffi:cinclude>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><ffi:getProperty name="PageTitle"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="<ffi:getProperty name="ServletPath"/>FFretail.css" type="text/css">

</head>
<body>
<div class="marginTop10" style="position:relative; overflow:hidden">
<ffi:help id="user_user_addedit_payments" className="moduleHelpClass"/>
<p><span class="txt_normal_bold"><ffi:getProperty name="SecondaryUser" property="FullNameWithLoginId"/></span></p>
<form name="UserPaymentsForm" id="userPaymentsForm"  action="/cb/pages/jsp/user/addSecondaryUserPayments.action"  method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header"><s:text name="secondaryUser.paymentsInformation"/></div>
   		<div class="paneContentWrapper">
           <div class="instructions">
              <s:text name="secondaryUser.paymentsTab.instructions"/>
           </div>
           <div class="instructions">
               <input name="EnableBillPayments" togglepermissions="true" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value="TRUE"  <ffi:cinclude value1="${EnableBillPayments}" value2="TRUE" operator="equals">checked</ffi:cinclude>> <span style="padding-right: 20px;" class="txt_normal_bold">Yes</span> <input name="EnableBillPayments" togglepermissions="true" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value="FALSE"  <ffi:cinclude value1="${EnableBillPayments}" value2="FALSE" operator="equals">checked</ffi:cinclude>> <span class="txt_normal_bold">No</span>
           </div>
           <div containerType="permissionsBlock" <ffi:cinclude value1="${EnableBillPayments}" value2="TRUE" operator="notEquals">style="display: none;"</ffi:cinclude>>
              <div class="instructions">
                 <s:text name="secondaryUser.paymentsTab.instructions2"/>
              </div>
            </div>
         </div>
     </div>
</div>
    <div containerType="permissionsBlock" <ffi:cinclude value1="${EnableBillPayments}" value2="TRUE" operator="notEquals">style="display: none;"</ffi:cinclude>>
<ffi:setProperty name="PermissionsTitle" value="Bill Payment Permissions"/>
<ffi:setProperty name="EnableOperationDisplayValue" value="Bill Payment"/>
<ffi:setProperty name="NoAccountsMessage" value="(No accounts authorized for bill payment are available.)"/>
<ffi:setProperty name="IncludeFromSection" value="TRUE"/>
<ffi:setProperty name="IncludeToSection" value="FALSE"/>
<ffi:object name="com.ffusion.beans.util.StringList" id="AccountsCollectionNames" scope="session"/>
<ffi:setProperty name="AccountsCollectionNames" property="Add" value="SecondaryUserEntitledCoreAccounts"/>
<ffi:setProperty name="FromOperationName" value='<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.PAYMENTS %>'/>

<ffi:object name="com.ffusion.beans.util.StringTable" id="AccountsFromFilters" scope="session"/>
<ffi:setProperty name="AccountsFromFilters" property="Key" value="SecondaryUserEntitledCoreAccounts"/>
<ffi:setProperty name="AccountsFromFilters" property="Value" value='<%= com.ffusion.beans.accounts.AccountFilters.BILL_PAY_FILTER %>'/>

<ffi:setProperty name="FeatureAccessAndLimitsTaskName" value="FFIBillPaymentAccessAndLimits"/>
<s:include value="/pages/jsp/user/inc/user_addedit_permissions.jsp" />
        <br>
        <ffi:cinclude ifEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.PAYEES %>'>
			<table border="0" cellpadding="0" cellspacing="0" width="80%" class="tableData tdTopBottomPadding5">
				<tr>
					<td>
						<div style="padding-left: 10px; padding-right: 10px;" class="instructions">
							<br>
							Would you like to grant your secondary user permission to manage payees? This option is not recommended as it allows the secondary user to add, edit, and delete payees from your profile, granting him or her your permission to make payments to payees you may not know. Payees are immediately available to all users you allow to perform bill payment transactions.
						</div>
						<br>
						<div style="padding-left: 60px;">
							<input name="EnableManagePayees" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value="TRUE" <ffi:cinclude value1="${EnableManagePayees}" value2="TRUE" operator="equals">checked</ffi:cinclude>> <span style="padding-right: 20px;" class="txt_normal_bold">Yes</span> <input name="EnableManagePayees" type="radio" style="margin: 0; padding: 0px; vertical-align: middle;" value="FALSE" <ffi:cinclude value1="${EnableManagePayees}" value2="TRUE" operator="notEquals">checked</ffi:cinclude>> <span class="txt_normal_bold">No</span>
						</div>
					</td>
				</tr>
			</table>
		</ffi:cinclude>
		<ffi:cinclude ifNotEntitled='<%= com.ffusion.csil.core.common.EntitlementsDefines.PAYEES %>'>
			<ffi:setProperty name="EnableManagePayees" value="FALSE"/>
		</ffi:cinclude>
    </div>
    <div class="btn-row">	
    	<sj:a id="secUserPaymentsCancel" 
    	button="true" 
		summaryDivId="summary" 
		buttonType="cancel" 
		onClickTopics="showSummary,secondaryUsers.cancel">
    	<s:text name="jsp.default_82"/></sj:a>        
		<ffi:cinclude value1="${EnableInternalTransfers}" value2="FALSE" operator="equals">		
			<sj:a id="secUserPaymentsBack" button="true" buttontype="back" backstep="2" ><s:text name="jsp.default_57"/></sj:a>
		</ffi:cinclude>
		<ffi:cinclude value1="${EnableInternalTransfers}" value2="TRUE" operator="equals">
			<sj:a id="secUserPaymentsBack" button="true" buttontype="back" backstep="3" ><s:text name="jsp.default_57"/></sj:a>
		</ffi:cinclude>			
		<sj:a id="secUserPaymentsNext" button="true"  formIds="userPaymentsForm"  targets="targetDiv"	 validate="true" validateFunction="customValidation" onBeforeTopics="secondaryUsers.clearErrors" onSuccessTopics="secondaryUsers.addPaymentsSuccess"><s:text name="jsp.default_291"/></sj:a>		
    </div>
</form>

</div>
</body>
</html>

<ffi:removeProperty name="TempURL"/>
<ffi:removeProperty name="PermissionsTitle"/>
<ffi:removeProperty name="EnableOperationDisplayValue"/>
<ffi:removeProperty name="IncludeFromSection"/>
<ffi:removeProperty name="IncludeToSection"/>
<ffi:removeProperty name="AccountsCollectionNames"/>
<ffi:removeProperty name="FromOperationName"/>
<ffi:removeProperty name="AccountsFromFilters"/>
<ffi:removeProperty name="FeatureAccessAndLimitsTaskName"/>
<ffi:removeProperty name="NoAccountsMessage"/>
<ffi:removeProperty name="CancelButton"/>
<ffi:removeProperty name="BackButton"/>
<ffi:removeProperty name="NextButton"/>

<ffi:setProperty name="BackURL" value="${SecurePath}user/user_addedit_payments.jsp" URLEncrypt="true"/>
</ffi:cinclude>
