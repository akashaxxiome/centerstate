<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Get the list of credentials to authenticate - don't want to create the getCredentialsRequest task if the 
GetCredentials task already exists in session--%>
<ffi:object id="GetCredentials" name="com.ffusion.tasks.authentication.GetCredentialRequests"/>
<ffi:setProperty name="GetCredentials" property="UserType" value="<%=com.ffusion.beans.authentication.AuthConsts.CONSUMER_USER_TYPE%>"/>
<ffi:setProperty name="GetCredentials" property="Operation" value="${Credential_Op_Type}"/>
<ffi:setProperty name="GetCredentials" property="LogoutURL" value="${PathExt}invalidate-session.jsp"/>

<ffi:process name="GetCredentials"/>
<ffi:setProperty name="Temp_Credential_Op_Type" value="${Credential_Op_Type}"/>
<ffi:removeProperty name="Credential_Op_Type"/>
<ffi:removeProperty name="GetCredentials"/>

<%-- the session object credentials is created by the call to the GetCredentialRequests task --%>
<ffi:cinclude value1="${Credentials.Size}" value2="0" operator="equals">
	<ffi:setProperty name="verified" value="${UserSecurityKey.ImmutableString}" />
	<ffi:setProperty name="SecurePath" value="/cb/pages/jsp/user/"/>
	<s:include value="/cb/pages/jsp/user/userprefspersonal.jsp" />
	<SCRIPT LANGUAGE="JavaScript">
	</SCRIPT>
</ffi:cinclude>
<ffi:cinclude value1="${Credentials.Size}" value2="0" operator="notEquals">
<s:include value="/cb/pages/jsp/user/security-check.jsp" />
	<SCRIPT LANGUAGE="JavaScript">
		//function setLocation() {
		//	location.replace("<ffi:urlEncrypt url="${SecurePath}security-check.jsp"/>");
		//}
		//window.onload=setLocation;
	</SCRIPT>
</ffi:cinclude>
