<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
var rowElement ='';
ns.cash.reject_reasons;
$.ajax({    
	  url: $('#getReversePositivePayRejectReasonsActionURLValue').val(),    
	  data: $("#rppayExceptionFormID").serialize(),
	  async: false,
	  success: function(data) {
		  ns.cash.reject_reasons = data; 
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
	  }   
	});  
</script>

<ffi:help id="cash_cashrppay" />
<link rel='stylesheet' type='text/css' href='/cb/web/css/home-page.css' />

<s:url id="updateReversePositivePayDecisionsActionURL" value="/pages/jsp/reversepositivepay/updateReversePositivePayDecisionsAction.action" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
</s:url>

<s:url id="getReversePositivePayRejectReasonsActionURL" value="/pages/jsp/reversepositivepay/getReversePositivePayRejectReasonsAction.action" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
</s:url>

<ffi:setProperty name="reloadRPPayIssues" value="true" />

<s:form name="rppayExceptionForm" id="rppayExceptionFormID" action="/pages/jsp/reversepositivepay/getReversePositivePayConfirmIssuesAction_init.action" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	
	<input type="hidden" name="sortColumnName" value="ACCOUNTID,CHECKNUMBER"/>
	<input type="hidden" name="isAscending" value="true"/>
	<input type="hidden" name="accountID" value="All"/>
	<input type="hidden" name="bankID" value="All"/>
	<input type="hidden" name="payRadioAll" id="payRadioAll"  value="off"/>
	<input type="hidden" name="returnRadioAll" id="returnRadioAll" value="off"/>
	<input type="hidden" name="holdRadioAll" id="holdRadioAll" value="off"/>
	
	<s:hidden id="updateReversePositivePayDecisionsActionURLValue" name="updateReversePositivePayDecisionsActionURLValue" value="%{#updateReversePositivePayDecisionsActionURL}" />
	
	<s:hidden id="getReversePositivePayRejectReasonsActionURLValue" name="getReversePositivePayRejectReasonsActionURLValue" value="%{#getReversePositivePayRejectReasonsActionURL}" />
	
	<ffi:setGridURL grid="GRID_rppayExceptionGrid" name="ViewURL" url="/cb/pages/jsp/reversepositivepay/SearchImageAction.action?module=REVERSEPOSITIVEPAY&pPayaccountID={0}&pPaybankID={1}&pPaycheckNumber={2}" parm0="CheckRecord.AccountID" parm1="CheckRecord.BankID" parm2="CheckRecord.CheckNumber" />
	
	<ffi:setProperty name="rppayExceptionGridTempURL" value="/pages/jsp/reversepositivepay/getReversePositivePayIssuesAction.action?collectionName=PPayIssues&status=New&GridURLs=GRID_rppayExceptionGrid" URLEncrypt="true"/>
	<s:url id="rppayExceptionGridURL" value="%{#session.rppayExceptionGridTempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="rppayExceptionGridID"  
		sortable="true"  
		dataType="json"  
		href="%{rppayExceptionGridURL}"  
		sortname="account"
		sortorder="asc"
		pager="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
 		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onSortColTopics=""
		onGridCompleteTopics="addGridControlsEvents,RPPayExceptionEvent,rppayExceptionGridCompleteTopicForRadioRendering"
		> 
		
		<sjg:gridColumn name="issueDate" index="issueDate" title="%{getText('jsp.default_137')}" formatter="ns.cash.dateFormatter" sortable="true" width="80"/>
	    <sjg:gridColumn name="account" index="account" title="%{getText('jsp.default_15')}" sortable="true" width="348"/>
	    <sjg:gridColumn name="checkRecord.checkNumber" index="checkNumber" title="%{getText('jsp.cash_28')}" formatter="ns.cash.searchImageFormatter" sortable="true" width="80" cssClass="datagrid_actionColumn ppayDetailsOnHoverCls"/>
	    <sjg:gridColumn name="checkRecord.amount.currencyStringNoSymbol" index="amount" title="%{getText('jsp.default_43')}" sortable="true" width="90"/>
	    <%-- <sjg:gridColumn name="rejectReason" index="rejectReason" title="%{getText('jsp.default_350')}" sortable="true" width="110"/>
		--%>
		
		<sjg:gridColumn name="map.pay" index="pay" title="%{getText('jsp.default_312')}" formatter="ns.cash.RadioButtonPayFormatter" sortable="false" width="50" cssClass = "PPAYDecisionId"/> 
	    <sjg:gridColumn name="map.hold" index="hold" title="%{getText('jsp.default_596')}" formatter="ns.cash.RadioButtonHoldFormatter" sortable="false" width="70"/>
	    <sjg:gridColumn name="map.return" index="return" title="%{getText('jsp.default_360')}" formatter="ns.cash.RadioButtonReturnFormatter" sortable="false" width="70"/>
        <sjg:gridColumn name="rejectReason" index="rejectReason" title="%{getText('jsp.default_350')}" editable="true" formatter="ns.cash.OptionFormatter" sortable="false" width="120" cssClass = "RPPAYRejectReasonId"/>

		<sjg:gridColumn name="map.routingNumber" index="rountingNumber" title="%{getText('jsp.cash_95')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.cash_9')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.nickName" index="nickName" title="%{getText('jsp.default_293')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.currencyType" index="currencyType" title="%{getText('jsp.cash_44')}" sortable="true" hidden="true" hidedlg="true"/>

        <sjg:gridColumn name="map.defaultdecision" index="defaultdecision" title="%{getText('jsp.cash_44')}" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.currentDecision" index="currentDecision" title="%{getText('jsp.cash_44')}" hidden="true" hidedlg="true"/>
	
		<sjg:gridColumn name="checkRecord.bankID" index="bankIDColumn" title="%{getText('jsp.cash_19')}"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="checkRecord.accountID" index="accountIDColumn" title="%{getText('jsp.cash_11')}" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass = "PPAYImageUrlId"/>
	</sjg:grid>
	
	<%-- <ffi:cinclude value1="${AnyPendingPPayIssues}" value2="true" operator="equals"> --%>
		<div class="submitButtonsDiv">	
			<sj:a
				id="rppayExceptionGridSubmitDummy"
                button="true"
				onClickTopics="rppayExceptionGridDummySubmit"
				><s:text name="jsp.default_395" /></sj:a>
		</div>
		<sj:submit
				id="rppayExceptionGridSubmit"
				cssStyle="display:none;"
				formIds="rppayExceptionFormID"
				targets="secondDiv"
                button="true"
                onSuccessTopics="exceptionGridSubmitSuccessTopic"
                onErrorTopics="exceptionGridSubmitErrorTopic"
				onCompleteTopics="exceptionGridSubmitCompleteTopic"
                value="%{getText('jsp.default_395')}" 
		/>		
	<%-- </ffi:cinclude> --%>			                        
</s:form>

<div class="ppayHoverDetailsMaster">
	<div class="ppayDetailsWinOverlay"></div>
	<div class="ppayHoverDetailsHolder">
		<span class="winTitle">Check Image</span>

		<hr />
		<div class="actionItemHolder">
			<input id="payImgHlder" type="radio" name="ppayAction" value="pay" >Pay
			<input id="returnImgHlder" type="radio" name="ppayAction" value="return">Return
			<input id="holdImgHlder" type="radio" name="ppayAction" value="hold">Hold
		</div>
		<div id ="imgRGridHolder" class="chkImgHolder">
			<span class="imagePlaceholderText">Loading check image, please wait...</span>
		</div>

		<div class="imageControlsHolder">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<tr>
					<td colspan="2" align="center">
						<s:form action="" method="post" name="checkImageForm" id="checkImageID" theme="simple">
							<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>



	<script type="text/javascript">
		var currPpayExceptionGridPage = 1;
		var imgData;
		$.subscribe('rppayExceptionGridCompleteTopicForRadioRendering', function(event,data) {
			$("#jqgh_rppayExceptionGridID_account").addClass("gridNicknameField");
 
			var payRadio = '<td align="left"><input type="radio" id="payRadioAllDummy" name="RadioAllDummy" style="vertical-align: middle;"> <s:text name="jsp.default_312" /></td>';
			var returnRadio = '<td align="left"><input type="radio" id="returnRadioAllDummy" name="RadioAllDummy" style="vertical-align: middle;"> <s:text name="jsp.default_360" /></td>';
			var holdRadio = '<td align="left"><input type="radio" id="holdRadioAllDummy" name="RadioAllDummy" style="vertical-align: middle;"> <s:text name="jsp.default_596" /></td>';

			$("div[id='jqgh_rppayExceptionGridID_map.pay']").html(payRadio);
			$("div[id='jqgh_rppayExceptionGridID_map.return']").html(returnRadio);
			$("div[id='jqgh_rppayExceptionGridID_map.hold']").html(holdRadio);
			
			 var rowCount = $('#rppayExceptionGridID tr').length - 1;
			 
			 var toAppend = '';
	            
			 $.each(ns.cash.reject_reasons, function(index, row) {
				 $.each(row, function(index, RejectReasons) {
					 for(var i=0;i<RejectReasons.length;i++){
					 	toAppend += '<option value="'+RejectReasons[i]['value']+'">'+RejectReasons[i]['value']+'</option>';
					 }
				});
	        });
			
			for(var i=1;i<=rowCount;i++){
				 $('#rejectReasonsId'+i).append(toAppend);
				
				 if ($('#returnId'+i).prop('checked')) {
					 $('#rejectReasonsId'+i).removeAttr('disabled');
				 } else {
					$('#rejectReasonsId'+i).attr('disabled', 'disabled');
				 }
			}
			 
			$("#payRadioAllDummy").click(function(event){
				$("#payRadioAll").val("on");
				$("#returnRadioAll").val("off");
				$("#holdRadioAll").val("off");
				$("input[class='payclass']").prop('checked', true);
				
				 for(var i=1;i<=rowCount;i++){
					 $('#rejectReasonsId'+i).attr('disabled', 'disabled');
				 }
				
			});

			$("#returnRadioAllDummy").click(function(event){
				$("#payRadioAll").val("off");
				$("#returnRadioAll").val("on");
				$("#holdRadioAll").val("off");
				$("input[class='returnclass']").prop('checked', true);
				
				 for(var i=1;i<=rowCount;i++){
					 $('#rejectReasonsId'+i).removeAttr('disabled');
				 }
			});
			
			$("#holdRadioAllDummy").click(function(event){
				$("#payRadioAll").val("off");
				$("#returnRadioAll").val("off");
				$("#holdRadioAll").val("on");
				$("input[class='holdclass']").prop('checked', true);
				
				 for(var i=1;i<=rowCount;i++){
					 $('#rejectReasonsId'+i).attr('disabled', 'disabled');
				 }
			});
			
			$(".ppayDetailsOnHoverCls a").hover(
					function(event) {
						// rowElement = event.fromElement.parentElement;
						rowElement = event.relatedTarget.parentElement;
						var urlString = $(rowElement.getElementsByClassName('PPAYImageUrlId')).attr('title');
						
						$.ajax({
							url: urlString,
							success: function(data){
								$('#imgRGridHolder').html(data);
								$('#payImgHlder').attr('checked', false);
								$('#returnImgHlder').attr('checked', false);
								$('#holdImgHlder').attr('checked', false);
								var decisionName  = $(rowElement.getElementsByClassName('PPAYDecisionId')).find('input').attr('name');
								// Checke if selected row has any decision.
								if( $('input:radio[name="'+decisionName+'"]').filter('[value="Pay"]').is(':checked')) {
									$('#payImgHlder').attr('checked', true);
								} else if($('input:radio[name="'+decisionName+'"]').filter('[value="Return"]').is(':checked')){
									$('#returnImgHlder').attr('checked', true);
								} else if($('input:radio[name="'+decisionName+'"]').filter('[value="Hold"]').is(':checked')) {
									$('#holdImgHlder').attr('checked', true);
								}
							}
						});
						$(".ppayHoverDetailsMaster").show();
						
						if(($(this).offset().left) <= ($(window).width()/2)) {
							$(".ppayHoverDetailsHolder").offset({left: ($(this).offset().left) });
						} else {
							$(".ppayHoverDetailsHolder").offset({left: ($(window).width() / 2) });
						}

					}, function() {
						// space for code to run on mouseout...
					}
				);
				
				 $("#payImgHlder").click(function(event){
					 var decisionName  = $(rowElement.getElementsByClassName('PPAYDecisionId')).find('input').attr('name');
					 $('input:radio[name="'+decisionName+'"]').filter('[value="Pay"]').attr('checked', true);
					 
					 var rejectReasonId  = $(rowElement.getElementsByClassName('RPPAYRejectReasonId')).find('select').attr('id');
					 $('#'+rejectReasonId).attr('disabled', 'disabled');
				 });
				 
				 $("#returnImgHlder").click(function(event){
					 var decisionName  = $(rowElement.getElementsByClassName('PPAYDecisionId')).find('input').attr('name');
					 $('input:radio[name="'+decisionName+'"]').filter('[value="Return"]').attr('checked', true);
					
					 var rejectReasonId  = $(rowElement.getElementsByClassName('RPPAYRejectReasonId')).find('select').attr('id');
					 $('#'+rejectReasonId).removeAttr('disabled', 'disabled');
				 });
				 
				 $("#holdImgHlder").click(function(event){
					 var decisionName  = $(rowElement.getElementsByClassName('PPAYDecisionId')).find('input').attr('name');
					 $('input:radio[name="'+decisionName+'"]').filter('[value="Hold"]').attr('checked', true);
					 
					 var rejectReasonId  = $(rowElement.getElementsByClassName('RPPAYRejectReasonId')).find('select').attr('id');
					 $('#'+rejectReasonId).attr('disabled', 'disabled');
				 });
				

				$(".ppayHoverDetailsHolder").hover(function(event){
					event.stopPropagation();
				});

				$(".ppayDetailsWinOverlay").hover(function(event) {
					$(".ppayHoverDetailsMaster").hide();
				})
				.mouseleave(function() {
					// space for code to run on mouseleave event
				});
			
		});

	    $(document).ready(function () {
			
			var fnCustomPaging = function(nextPage){
				var currPpayExceptionGridPage = $("#rppayExceptionGridID").jqGrid('getGridParam', 'page');
				var rowNumberSelected = $('#gbox_rppayExceptionGridID .ui-pg-selbox :selected').val();
				
				//When user clicks on First and Last links, the page returned is not the actual pagem but the string is returned
				//Convert that string to the actual first or last pagenumber
				if(nextPage == 'first_rppayExceptionGridID_pager'){
					nextPage = 1;
				}else if(nextPage == 'last_rppayExceptionGridID_pager'){
					//Get the last page number of the grid
					nextPage = $("#rppayExceptionGridID").jqGrid('getGridParam', 'lastpage');	
				}
				
				$.ajax({    
				  url: $('#updateReversePositivePayDecisionsActionURLValue').val(),    
				  data: $("#rppayExceptionFormID").serialize(),
				  success: function(data) {
					  $("#rppayExceptionGridID").jqGrid('setGridParam', {rowNum:rowNumberSelected});
					  $('#rppayExceptionGridID').trigger("reloadGrid",[{page:nextPage}]);
					  $.publish('RPPayExceptionPagingEvent');
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
					 //set the grid page value back to original
					 $("#rppayExceptionGridID").jqGrid('setGridParam', {rowNum:rowNumberSelected});
					 $("#rppayExceptionGridID").jqGrid('setGridParam', {page:currPpayExceptionGridPage});
				  }   
				});  
				return 'stop';
			};
			
			$("#rppayExceptionGridID").data('onCustomPaging', fnCustomPaging);
			
			var fnCustomSortCol = function (index, columnIndex, sortOrder) {
				$.publish("PPayExceptionOnSortTopic");
			};
			
			$("#rppayExceptionGridID").data('onCustomSortCol', fnCustomSortCol);
			
			
			//ppay exception grid complete event.
			$.subscribe('RPPayExceptionPagingEvent', function(event,data) {
				$("#payRadioAll").val("off");
				$("#returnRadioAll").val("off");
				$("#holdRadioAll").val("off");
			});
			
			
			$.subscribe('rppayExceptionGridDummySubmit', function(event,data) {
				$.ajax({    
					  url: $('#updateReversePositivePayDecisionsActionURLValue').val(),    
					  data: $("#rppayExceptionFormID").serialize(),
					  success: function(data) {  
						 //Trigger form submt action now
						 $("#rppayExceptionGridSubmit").click();
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
						 //console.log('Error occurred while submitting !');
					  }      
					});
			});
			
			$.subscribe('PPayExceptionOnSortTopic', function(event,data) {
				$.ajax({    
					  url: $('#updateReversePositivePayDecisionsActionURLValue').val(),    
					  data: $("#rppayExceptionFormID").serialize(),
					  success: function(data) {
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
					  }   
					});  
			});
	    });
	    
	   

    </script>