<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script >
	helpGridId = "rppay_workflowID";
</script>

<ffi:help id="cash_cashrppconfirm" />

<s:form name="rppayExceptionConfirmForm" id="rppayExceptionConfirmFormID" action="/pages/jsp/reversepositivepay/submitReversePositivePayDecisionsAction.action" theme="simple">
    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
    <%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center" class="ltrow2_color sectionsubhead">
				<s:text name="jsp.cash_82"/><br>
			</td>
		</tr>
    </table> --%>

	<ffi:setProperty name="tempURL" value="/pages/jsp/reversepositivepay/getReversePositivePayConfirmIssuesAction.action" URLEncrypt="true"/>
    <s:url id="rppayExceptionConfirmGridURL" value="%{#session.tempURL}"/>
	<sjg:grid  
		id="rppayExceptionConfirmGridID"  
		sortable="true"  
		dataType="json"  
		href="%{rppayExceptionConfirmGridURL}"  
		pager="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"		
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="RPPayExceptionConfirmEvent"
		> 
		
		<sjg:gridColumn name="issueDate" index="issueDate" title="%{getText('jsp.default_137')}" formatter="ns.cash.dateFormatter" sortable="false" width="80"/>
	    <sjg:gridColumn name="account" index="account" title="%{getText('jsp.default_15')}" sortable="false" width="348"/>
	    <sjg:gridColumn name="checkRecord.checkNumber" index="checkNumber" title="%{getText('jsp.cash_28')}" sortable="false" width="80"/>
	    <sjg:gridColumn name="checkRecord.amount.currencyStringNoSymbol" index="amount" title="%{getText('jsp.default_43')}" sortable="false" width="90"/>
	    
		<sjg:gridColumn name="map.pay" index="pay" title="%{getText('jsp.default_312')}" formatter="ns.cash.RadioButtonPayFormatterConfirm" sortable="false" width="50"/> 
	    <sjg:gridColumn name="map.return" index="return" title="%{getText('jsp.default_360')}" formatter="ns.cash.RadioButtonReturnFormatterConfirm" sortable="false" width="50"/> 
		<sjg:gridColumn name="rejectReason" index="rejectReason" title="%{getText('jsp.default_350')}" sortable="false" width="110"/>
		<sjg:gridColumn name="decision" index="decision" title="%{getText('jsp.default_160')}" sortable="false" width="50" hidden="true" hidedlg="true"/> 
		<sjg:gridColumn name="map.routingNumber" index="routingNumber" title="%{getText('jsp.cash_95')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.cash_9')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.nickName" index="nickName" title="%{getText('jsp.default_293')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.currencyType" index="currencyType" title="%{getText('jsp.cash_44')}" sortable="true" hidden="true" hidedlg="true"/>
	</sjg:grid>
	<br>
	<div class="submitButtonsDiv">	
		<s:url id="rppayCancelUrl" value="/pages/jsp/reversepositivepay/initReversePositivePayAction.action" escapeAmp="false">
			    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="reversepositivepayReload" value="true"/>
		</s:url>
		<sj:a 
			id="cancelrppayExceptionConfirmGrid"
			href="%{rppayCancelUrl}" 
			targets="notes" 
			indicator="indicator" 
			button="true"
			><s:text name="jsp.default_82"/></sj:a>
		
		<sj:a id="backFormButton" button="true"
						onClickTopics="backToInput">
						<s:text name="jsp.default_57" />
					</sj:a>
			
		<sj:a 
			id="rppayExceptionConfirmGridSubmit"
			formIds="rppayExceptionConfirmFormID"
			targets="thirdDiv" 
			button="true"
			onSuccessTopics="exceptionGridSubmitVerifySuccessTopic"
			onErrorTopics="exceptionGridSubmitVerifyErrorTopic" 
			onCompleteTopics="exceptionGridSubmitVerifyCompleteTopic"
			><s:text name="jsp.default_175"/></sj:a>
	</div>					                        
</s:form>

<script>
		ns.common.addGridControls("#rppayExceptionConfirmGridID", false);
		ns.common.updatePortletTitle("rppay_workflowID",js_rppay_verify_portlet_title,false);
</script>

