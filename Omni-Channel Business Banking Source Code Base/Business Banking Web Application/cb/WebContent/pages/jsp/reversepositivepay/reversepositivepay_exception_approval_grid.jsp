<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
var rowElement ='';
ns.cash.reject_reasons;
$.ajax({    
	  url: $('#getReversePositivePayRejectReasonsActionURLValue').val(),    
	  data: $("#rppayExceptionFormID").serialize(),
	  async: false,
	  success: function(data) {
		  ns.cash.reject_reasons = data; 
	  },
	  error: function(jqXHR, textStatus, errorThrown) {
	  }   
	});  
</script>

<ffi:help id="cash_cashrppayapproval" />
<link rel='stylesheet' type='text/css' href='/cb/web/css/home-page.css' />

<s:url id="updateReversePositivePayDecisionsActionURL" value="/pages/jsp/reversepositivepay/updateReversePositivePayDecisionsAction.action" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
</s:url>

<s:url id="getReversePositivePayRejectReasonsActionURL" value="/pages/jsp/reversepositivepay/getReversePositivePayRejectReasonsAction.action" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
</s:url>

<ffi:setProperty name="reloadRPPayIssues" value="true" />

	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	
	<input type="hidden" name="sortColumnName" value="ACCOUNTID,CHECKNUMBER"/>
	<input type="hidden" name="isAscending" value="true"/>
	<input type="hidden" name="accountID" value="All"/>
	<input type="hidden" name="bankID" value="All"/>
	<input type="hidden" name="payRadioAll" id="payRadioAll"  value="off"/>
	<input type="hidden" name="returnRadioAll" id="returnRadioAll" value="off"/>
	<input type="hidden" name="holdRadioAll" id="holdRadioAll" value="off"/>
	
	<s:hidden id="updateReversePositivePayDecisionsActionURLValue" name="updateReversePositivePayDecisionsActionURLValue" value="%{#updateReversePositivePayDecisionsActionURL}" />
	
	<s:hidden id="getReversePositivePayRejectReasonsActionURLValue" name="getReversePositivePayRejectReasonsActionURLValue" value="%{#getReversePositivePayRejectReasonsActionURL}" />
	
	<ffi:setGridURL grid="GRID_rppayExceptionGrid" name="ViewURL" url="/cb/pages/jsp/reversepositivepay/SearchImageAction.action?module=REVERSEPOSITIVEPAY&pPayaccountID={0}&pPaybankID={1}&pPaycheckNumber={2}" parm0="CheckRecord.AccountID" parm1="CheckRecord.BankID" parm2="CheckRecord.CheckNumber" />
	
	<ffi:setProperty name="rppayExceptionGridTempURL" value="/pages/jsp/reversepositivepay/getReversePositivePayIssuesAction.action?collectionName=PPayIssues&status=pendingApproval&GridURLs=GRID_rppayExceptionGrid" URLEncrypt="true"/>
	<s:url id="rppayExceptionGridURL" value="%{#session.rppayExceptionGridTempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="rppayApprovalExceptionGridID"  
		sortable="true"  
		dataType="json"  
		href="%{rppayExceptionGridURL}"  
		sortname="account"
		sortorder="asc"
		pager="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
 		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onSortColTopics=""
		onGridCompleteTopics="addGridControlsEvents,RPPayApprovExceptionEvent,rppayExceptionGridCompleteTopicForRadioRendering,pendingApprovalGridCompleteEvents"
		> 
		
		<sjg:gridColumn name="issueDate" index="issueDate" title="%{getText('jsp.default_137')}" formatter="ns.cash.dateFormatter" sortable="true" width="80"/>
	    <sjg:gridColumn name="account" index="account" title="%{getText('jsp.default_15')}" sortable="true" width="348"/>
	    <sjg:gridColumn name="checkRecord.checkNumber" index="checkNumber" title="%{getText('jsp.cash_28')}"  sortable="true" width="80" />
	    <sjg:gridColumn name="checkRecord.amount.currencyStringNoSymbol" index="amount" title="%{getText('jsp.default_43')}" sortable="true" width="90"/>
	    
		<sjg:gridColumn name="map.routingNumber" index="rountingNumber" title="%{getText('jsp.cash_95')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.cash_9')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.nickName" index="nickName" title="%{getText('jsp.default_293')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.currencyType" index="currencyType" title="%{getText('jsp.cash_44')}" sortable="true" hidden="true" hidedlg="true"/>

        <sjg:gridColumn name="map.defaultdecision" index="defaultdecision" title="%{getText('jsp.cash_44')}" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.currentDecision" index="currentDecision" title="%{getText('jsp.cash_44')}" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="resultOfApproval" index="resultOfApproval" title="Submitted For" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverName" index="approverName" title="ApproverName" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverIsGroup" index="approverIsGroup" title="ApproverIsGroup" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="userName" index="userName" title="UserName" hidden="true" hidedlg="true"/>
	
		<sjg:gridColumn name="checkRecord.bankID" index="bankIDColumn" title="%{getText('jsp.cash_19')}"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="checkRecord.accountID" index="accountIDColumn" title="%{getText('jsp.cash_11')}" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass = "PPAYImageUrlId"/>
	</sjg:grid>
	
<div class="ppayHoverDetailsMaster">
	<div class="ppayDetailsWinOverlay"></div>
	<div class="ppayHoverDetailsHolder">
		<span class="winTitle">Check Image</span>

		<hr />

		
	</div>
</div>



	<script type="text/javascript">
		var currPpayExceptionGridPage = 1;
		var imgData;
		

	    $(document).ready(function () {
			
			var fnCustomPaging = function(nextPage){
				var currPpayExceptionGridPage = $("#rppayExceptionGridID").jqGrid('getGridParam', 'page');
				var rowNumberSelected = $('#gbox_rppayExceptionGridID .ui-pg-selbox :selected').val();
				
				//When user clicks on First and Last links, the page returned is not the actual pagem but the string is returned
				//Convert that string to the actual first or last pagenumber
				if(nextPage == 'first_rppayExceptionGridID_pager'){
					nextPage = 1;
				}else if(nextPage == 'last_rppayExceptionGridID_pager'){
					//Get the last page number of the grid
					nextPage = $("#rppayExceptionGridID").jqGrid('getGridParam', 'lastpage');	
				}
				
				$.ajax({    
				  url: $('#updateReversePositivePayDecisionsActionURLValue').val(),    
				  data: $("#rppayExceptionFormID").serialize(),
				  success: function(data) {
					  $("#rppayExceptionGridID").jqGrid('setGridParam', {rowNum:rowNumberSelected});
					  $('#rppayExceptionGridID').trigger("reloadGrid",[{page:nextPage}]);
					  $.publish('RPPayExceptionPagingEvent');
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
					 //set the grid page value back to original
					 $("#rppayExceptionGridID").jqGrid('setGridParam', {rowNum:rowNumberSelected});
					 $("#rppayExceptionGridID").jqGrid('setGridParam', {page:currPpayExceptionGridPage});
				  }   
				});  
				return 'stop';
			};
			
			$("#rppayExceptionGridID").data('onCustomPaging', fnCustomPaging);
			
			var fnCustomSortCol = function (index, columnIndex, sortOrder) {
				$.publish("PPayExceptionOnSortTopic");
			};
			
			$("#rppayExceptionGridID").data('onCustomSortCol', fnCustomSortCol);
			
			
			//ppay exception grid complete event.
			$.subscribe('RPPayExceptionPagingEvent', function(event,data) {
				$("#payRadioAll").val("off");
				$("#returnRadioAll").val("off");
				$("#holdRadioAll").val("off");
			});
			
			
			$.subscribe('rppayExceptionGridDummySubmit', function(event,data) {
				$.ajax({    
					  url: $('#updateReversePositivePayDecisionsActionURLValue').val(),    
					  data: $("#rppayExceptionFormID").serialize(),
					  success: function(data) {  
						 //Trigger form submt action now
						 $("#rppayExceptionGridSubmit").click();
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
						 //console.log('Error occurred while submitting !');
					  }      
					});
			});
			
			$.subscribe('PPayExceptionOnSortTopic', function(event,data) {
				$.ajax({    
					  url: $('#updateReversePositivePayDecisionsActionURLValue').val(),    
					  data: $("#rppayExceptionFormID").serialize(),
					  success: function(data) {
					  },
					  error: function(jqXHR, textStatus, errorThrown) {
					  }   
					});  
			});
	    });
	    
	   

    </script>