<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.Locale"%>
<%@page import="com.ffusion.util.UserLocaleConsts"%>
<%@page import="com.ffusion.beans.user.UserLocale"%>
<%@page import="com.ffusion.beans.authentication.AuthConsts"%>
<SCRIPT LANGUAGE="JavaScript">

$(function(){

	$.subscribe('sendCredentialFormSuccess', function(event,data) {
		var submitButtonId = ns.common.activeSubmitButton;
		if(submitButtonId){
			$("#" + submitButtonId ).click();
			$('#validateCredentialResult').html(event.originalEvent.request.responseText);
			$('#authenticationInfoDialogID').dialog('close');
		}
	});

	$.subscribe('beforeSendCredentialForm', function(event,data) {
	});

	$.subscribe('errorSendCredentialForm', function(event,data) {
        $("#frmCredentialList :input:visible").val('');
		$('#validateCredentialResult').html(event.originalEvent.request.responseText);

		var resStatus = event.originalEvent.request.status;
		if(resStatus === 600) //600 indicates session should be invalidated
			location.href="<ffi:getProperty name="FullPagesPath"/>invalidate-session.jsp?TimedOut=true";

	});


});

function setFocus()
{
	frm = document.frmCredentialList;

	for( i=0; i < frm.length; i++ ) {
		curr_type=frm.elements[i].type;
		if (curr_type.indexOf("password") >= 0) {
			frm.elements[i].focus();
			break;
		}
	}
}
</SCRIPT>

<%-------------------- Get the appropriate resources files containing the PasswordQuestions -----------------%>
<ffi:object name="com.ffusion.tasks.util.Resource" id="UserResourcePassQue" scope="session"/>
    <ffi:setProperty name="UserResourcePassQue" property="ResourceFilename" value="com.ffusion.beansresources.user.resources" />
<ffi:process name="UserResourcePassQue"/>

<ffi:object name="com.ffusion.tasks.util.ResourceList" id="UserResourcePassQueList" scope="session"/>
    <ffi:setProperty name="UserResourcePassQueList" property="ResourceFilename" value="com.ffusion.beansresources.user.resources" />
<ffi:process name="UserResourcePassQueList" />

<%
	//This code is needed to switch locale to get english value for chinese text of password questions
	Locale englishLocale = new Locale("en", "US");
	com.ffusion.tasks.util.Resource userResourceTemp = (com.ffusion.tasks.util.Resource)session.getAttribute("UserResourcePassQue");
	UserLocale userLocale = (UserLocale)session.getAttribute(UserLocaleConsts.USERLOCALE);
	Locale originalLocale = userLocale.getLocale();
%>




<%-- Create a Resource tasks to access the authentication resource bundle for names of credentials --%>
<%-- Currently supporting scratch card, token and challenge questions --%>
<ffi:object name="com.ffusion.tasks.util.Resource" id="UserResource" scope="session"/>
    <ffi:setProperty name="UserResource" property="ResourceFilename" value="com.ffusion.beansresources.authentication.resources" />
<ffi:process name="UserResource"/>
<% String type_token = "" + com.ffusion.beans.authentication.AuthConsts.AUTH_TYPE_TOKEN;
   String type_scratch = "" + com.ffusion.beans.authentication.AuthConsts.AUTH_TYPE_SCRATCH;
   String type_challenge = "" + com.ffusion.beans.authentication.AuthConsts.AUTH_TYPE_CHALLENGE;
   String credential_scratch= "CREDENTIAL_FIELD_NAME_" + type_scratch;
   String credential_token = "CREDENTIAL_FIELD_NAME_" + type_token;

%>

<%--
<body onload="setFocus();">
--%>
<div align="center">


<s:form id="frmCredentialList" namespace="/pages/common" validate="false" action="multiFactorAuthenticate" method="post" name="frmCredentialList" theme="simple" onsubmit="$('#continueButtonID').click();return false">
<%--
<form method="post" name="frmCredentialList" action="<ffi:urlEncrypt url="${SecurePath}payments/security-check-confirm.jsp"/>">
--%>
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<table width="100%" border="0">
	<tr height="10">
		<td align="center" class="instructions">
		<ffi:cinclude value1="${securityMessageKey}" value2="" operator="notEquals">
			<ffi:getL10NString rsrcFile="cb" msgKey="${securityMessageKey}"/><br>&nbsp;
		</ffi:cinclude>
		<br/><div class="errorLabel" id="validateCredentialResult"></div>
		</td>
	</tr>
</table>


<%-- List the Credentials for validation --%>
<% int curNum = 0; %>
<table width="100%" border="0">
<s:if test="#session.SecureUser.multifactorInfo.size > 0">
	<s:iterator value="#session.SecureUser.multifactorInfo" var="next" status="status">
		<s:if test="#next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_QUESTION || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_TOKEN || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_SCRATCH_CARD || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_OTP">
			<s:set var="tempType" value="#next.type" scope="session"/>
			<tr>
				<td style="word-wrap:break-word;" class="sectionhead">
					<label for="SecureUser.MultifactorInfo[<s:property value='#status.index'/>].response"><s:text name="%{@com.ffusion.beans.authentication.AuthConsts@CREDENTIAL_FIELD_NAME_PREFIX}%{#next.type}"/>:</label><span class="requiredField">*</span>
					<s:property value="#next.challenge" escape="false"/>
				</td>
				<td class="sectionhead">
				<input class="credentialUserInput ui-widget-content ui-corner-all" id="SecureUser.MultifactorInfo[<s:property value="#status.index"/>].response" name="SecureUser.MultifactorInfo[<s:property value="#status.index"/>].response" size="20" maxlength="50" required="true" tabindex="2" type="password" />
			</tr>
		</s:if>
	</s:iterator>
</s:if>
<s:else>
		<tr>
			<td> <s:text name="jsp.default.unknown.mfa.error"></s:text> </td>
		</tr>
</s:else>
<tr>
	<td><span id="SecureUser.multifactorInfoError"></span></td>
</tr>
</table>
<table width="100%">
    <tr>
        <td width="100%" class="sectionhead" align="center">
            <span class="required">* <s:text name="jsp.default_240"/></span>
        </td>
    </tr>
</table>

<%-- --------------------------------------------------------- --%>
<table width="100%">
	<tr>
		<td class="ltrow2_color" width="100%">&nbsp;</td>
	</tr>
</table>
<center>
<%--
<input type="button" name="BACK" value="BACK" class="submitbutton" onClick="window.location='<ffi:urlEncrypt url="${BackURL}"/>';">
<input type="Submit" name="Continue" value="CONTINUE" class="submitbutton">
--%>
								<sj:a   button="true"
										onClickTopics="closeDialog"
						        ><s:text name="jsp.default_83"/></sj:a>
						        <s:if test="#session.SecureUser.multifactorInfo.size > 0">
						            <sj:a
										enableglobaltrack="false"
										id="continueButtonID"
										formIds="frmCredentialList"
		                                targets="validateCredentialResult"
		                                button="true"
		                                onBeforeTopics="beforeSendCredentialForm"
		                                onSuccessTopics="sendCredentialFormSuccess"
		                                onErrorTopics="errorSendCredentialForm"
		                                onCompleteTopics="sendCredentialFormComplete"
			                        ><s:text name="jsp.default_112"/></sj:a>
			                     </s:if>
</center>
<table width="100%">
	<tr height="5">
		<td class="ltrow2_color" width="100%">&nbsp;</td>
	</tr>
</table>
</s:form>

</div>

<ffi:removeProperty name="UserResource"/>
