<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.tasks.admin.AdminTask"%>

<s:set var="tmpI18nStr" value="%{getText('jsp.default_188')}" scope="request" /><ffi:setProperty name="PageTitle" value="${tmpI18nStr}"/>

<% String taskErrorCode; %>
<ffi:getProperty name="CurrentTask" property="Error"  assignTo="taskErrorCode"/>
 <%
boolean useAdditionalErrorMessage = com.ffusion.util.CommBankIdentifier.isValidAdditionalTaskCode(taskErrorCode);
 %>



<div align="center">
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_189')}" scope="request" /><ffi:setProperty name="navTitle" value="${tmpI18nStr}"/>
    <ffi:setProperty name="showDate" value="true"/>
    <ffi:removeProperty name="navTitle"/>
    <ffi:removeProperty name="showDate"/>
</div>
<br><br>

<%--
<div align="center">
<ffi:include page="${PathExt}inc/nav_header.jsp" />
--%>
<%-- ================ MAIN CONTENT START ================ --%>
<ffi:setProperty name="${touchedvar}" value="false"/>

<%-- ERROR MESSAGE BEGIN --%>
<center>
                    <table cellpadding="1" cellspacing="2" border="0" class="mainfont">
                        <tr>
                            <td align="center" colspan="2"> 
                              <% if ( useAdditionalErrorMessage ) { %>
                                    <ffi:setProperty name="ErrorsResource" property="ResourceID" value="ErrorA${CurrentTask.Error}_descr" />
                              <% } else { %>                                          
                                    <ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${CurrentTask.Error}_descr" />
                              <% } %>
                             <ffi:cinclude value1="${ErrorsResource.Resource}" value2="" operator="equals" >
								<s:text name="jsp.default_114" />
</ffi:cinclude>
<ffi:cinclude value1="${ErrorsResource.Resource}" value2="" operator="notEquals" >
                                <ffi:getProperty name="ErrorsResource" property="Resource"/>
</ffi:cinclude>
                              <br>
                              <% String errorCode = ""; %>
                              <ffi:getProperty name="CurrentTask" property="Error" assignTo="errorCode"/>
                              <%
                                 String errorA = Integer.toString(AdminTask.TASK_ERROR_DEL_USER_IS_A_PRIMARY_ADMIN);
                                 String errorB = Integer.toString(AdminTask.TASK_ERROR_REMOVE_ADMIN_USER_IS_A_PRIMARY_ADMIN);
                                 String errorC = Integer.toString(AdminTask.TASK_ERROR_INACTIVE_USER_IS_A_PRIMARY_ADMIN);

                                 if (errorCode.equals(errorA) || errorCode.equals(errorB) || errorCode.equals(errorC))
                                 {
                              %>
                                     <ffi:list collection="PrimaryAdminAssignments" items="Assignment">
                                         <ffi:getProperty name="Assignment" property="FirstName"/> <ffi:getProperty name="Assignment" property="LastName"/><br>
                                     </ffi:list>
                              <% } %>
                            </td>
                        </tr>

<%-- TE_WHY will have a specific error, text created in the task with details --%>
<% if (session.getAttribute("TE_WHY") != null) { %>
                        <tr>
                            <td align="right" class="homesub">&nbsp;</td>
                            <td class="errorBackground">
                                <ffi:getProperty name="TE_WHY"/>
                            </td>
                        </tr>
<% session.removeAttribute("TE_WHY");
} %>
                        <tr>
                            <td align="center" colspan="2">&nbsp;<br><b><s:text name="jsp.default_213"/></b></td>
                        </tr>
                        <tr>
                            <td align="right" class="homesub"><b><s:text name="jsp.default_404"/></b>&nbsp;</td>
                            <td class="errorBackground">
                                <ffi:getProperty name="CurrentTask"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="homesub"><b><s:text name="jsp.default_403"/></b>&nbsp;</td>
                            <td class="errorBackground">
                                <ffi:getProperty name="CurrentTask" property="Error"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" class="homesub"><b><s:text name="jsp.default_191"/></b>&nbsp;</td>
                            <td class="errorBackground">
                                <ffi:getProperty name="ErrorsResource" property="ResourceFilename"/>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <form method="post">			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/><input type="button" class="submitbutton" value="<s:text name="jsp.default_57"/>" onclick="document.location='<ffi:getProperty name="BackURL"/>';"></form>
</center>
