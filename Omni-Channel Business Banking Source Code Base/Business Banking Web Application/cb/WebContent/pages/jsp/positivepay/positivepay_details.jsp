<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
		
		<span id="normalInputTab" style="display:none;"><s:text name="jsp.default_390"/></span>
    	<span id="normalVerifyTab" style="display:none;"><s:text name="jsp.default_391"/></span>
    	<span id="normalSummaryTab" style="display:none;"><s:text name="jsp.cash_84"/></span>
		<span id="uploadInputTab" style="display:none;"><s:text name="jsp.cash_fileUploadStep1"/></span>
    	<span id="uploadVerifyTab" style="display:none;"><s:text name="jsp.cash_fileUploadStep2"/></span>

        <div id="ppay_workflowID" class="portlet wizardSupportCls">
	        <div class="portlet-header">
	        	<span class="portlet-title hidden" title="Positive Pay Exception Summary"><s:text name="jsp.cash_84" /></span>
				<div class="searchHeaderCls hidden">
					<span class="searchPanelToggleAreaCls" onclick="$('#allTypesOfAccount').slideToggle();">
						<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
					</span>
					<div class="summaryGridTitleHolder" style="margin-left:20px;">
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_DECISION_ENTITLEMENT %>" >
						<span id="selectedGridTab" class="selectedTabLbl" style="padding-left:0px;">
							<s:text name="jsp.default_606"/>
						</span>
						<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
						
						<div class="gridTabDropdownHolder">
							<span class="gridTabDropdownItem" id='approvalPPay' onclick="showPPayGridSummary('approvalPPay')" >
								<s:text name="jsp.default_606"/>
							</span>
							<span class="gridTabDropdownItem" id='pendingPPay' onclick="showPPayGridSummary('pendingPPay')">
								<s:text name="jsp.cash_84"/>
							</span>							
						</div>
						</ffi:cinclude>
					</div>
				</div>
			</div>
			<div class="portlet-content">
				<div id="gridContainer" class="summaryGridHolderDivCls">
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_DECISION_ENTITLEMENT %>" >
					<s:include value="/pages/jsp/positivepay/inc/positivepaySearchCriteria.jsp"/>
					<div id="approvalPPaySummaryGrid" gridId="ppayApprovalExceptionGridID" class="gridSummaryContainerCls" >
							 <s:action namespace="/pages/jsp/positivepay" name="initPostivePaySummaryAction" executeResult="true">
								<s:param name="collectionName">PendingApprovalPPayIssues</s:param>
							</s:action> 
					</div>
					
					<div id="pendingPPaySummaryGrid" gridId="ppayExceptionGridID" class="gridSummaryContainerCls hidden" >
						
					        <sj:tabbedpanel id="PPayWizardTabs" cssClass="marginTop20" >
					    	
					        <sj:tab id="inputTab" target="firstDiv" label="%{getText('jsp.default_390')}"/>
					        <sj:tab id="verifyTab" target="secondDiv" label="%{getText('jsp.default_392')}"/>
					        <sj:tab id="confirmTab" target="thirdDiv" label="%{getText('jsp.default_393')}"/>
					       <%--  <div id="firstDiv" class="borderNone"> --%>
					        <div id="firstDiv" class="borderNone" >
								<%-- <s:text name="jsp.cash_84"/> --%>
								 <s:action namespace="/pages/jsp/positivepay" name="initPostivePaySummaryAction" executeResult="true">
									<s:param name="collectionName">PendingPPayIssues</s:param>
								</s:action> 
					        </div>
					        <div id="secondDiv" class="borderNone">
					            <s:text name="jsp.cash_120"/>
					        </div>
					         <div id="thirdDiv" class="borderNone">
					            <s:text name="jsp.cash_120"/>
					        </div>
					    </sj:tabbedpanel>
				    </div>
				    </ffi:cinclude>
				    <ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_DECISION_ENTITLEMENT %>" >
				    	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_ENTITLEMENT_ISSUE_MAINTENANCE  %>" >
				    		<sj:tabbedpanel id="PPayWizardTabs" cssClass="marginTop20" >
					        <sj:tab id="inputTab" target="firstDiv" label="%{getText('jsp.default_390')}"/>
					        <sj:tab id="verifyTab" target="secondDiv" label="%{getText('jsp.default_392')}"/>
					        <sj:tab id="confirmTab" target="thirdDiv" label="%{getText('jsp.default_393')}"/>
					       <%--  <div id="firstDiv" class="borderNone"> --%>
					        <div id="firstDiv" class="borderNone" >
								<%-- <s:text name="jsp.cash_84"/> --%>
									<s:action namespace="/pages/jsp/positivepay" name="buildPositivePayManualFileAction_init" executeResult="true"/>			
								
					        </div>
					        <div id="secondDiv" class="borderNone">
					            <s:text name="jsp.cash_120"/>
					        </div>
					         <div id="thirdDiv" class="borderNone">
					            <s:text name="jsp.cash_120"/>
					        </div>
					    </sj:tabbedpanel>
				    		
				    	</ffi:cinclude>
				    </ffi:cinclude>
				 </div>	
			    
			  <%--   <div id="gridContainer" class="summaryGridHolderDivCls">
						<div id="approvalPPaySummaryGrid" gridId="pendingApprovalPPayGridID" class="gridSummaryContainerCls" >
							 <s:action namespace="/pages/jsp/positivepay" name="initPostivePaySummaryAction" executeResult="true">
								<s:param name="collectionName">PendingPPayIssues</s:param>
							</s:action> 
						</div>
						<div id="pendingSummaryGrid" gridId="pendingPPayGridID" class="gridSummaryContainerCls hidden" >
							<s:action namespace="/pages/jsp/positivepay" name="initPostivePaySummaryAction" executeResult="true">
								<s:param name="collectionName">PendingApprovalPPayIssues</s:param>
							</s:action> 
						</div>						
				</div>  --%>
			</div>
			<div id="positivePayDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
		       <ul class="portletHeaderMenu" style="background:#fff">
		       		  <%-- <li><a href='#' onclick="ns.common.bookmarkThis('detailsPortlet')"><span class="sapUiIconCls icon-create-session" style="float:left;"></span>Bookmark</a></li> --%>
		              <li><a href='#' onclick="ns.common.showDetailsHelp('ppay_workflowID')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
		       </ul>
			</div>
		</div>
		<input type="hidden" id="actionType" value="<s:property value='#parameters.summaryToLoad' />"/>
    <script>	
 	    //Initialize portlet with settings icon		
		ns.common.initializePortlet("ppay_workflowID");
 	    var actionType = $('#actionType').val();
 	    if(actionType == 'positivePay'){
 	    	ns.common.updatePortletTitle("ppay_workflowID",js_ppay_build_portlet_title,false);
 	    }else if(actionType == 'positivePayUpload'){
 	    	ns.common.updatePortletTitle("ppay_workflowID",js_fileupload_portlet_title,false);
 	    }else{
 	    	ns.common.updatePortletTitle("ppay_workflowID",jsp.cash_84,true);
 	    }
	</script>