<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script >
	var rowElement ='';
</script>

<ffi:help id="cash_cashppayforaccount" />
<link rel='stylesheet' type='text/css' href='/cb/web/css/home-page.css' />

<s:url id="updatePositivePayDecisionsActionURL" value="/pages/jsp/positivepay/updatePositivePayDecisionsAction.action" escapeAmp="false">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
</s:url>

<ffi:setProperty name="reloadPPayIssues" value="true" />

<s:form name="ppayExceptionForAccountForm" id="ppayExceptionForAccountFormID" action="/pages/jsp/positivepay/getPositivePayConfirmIssuesAction_init.action" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	
	<input type="hidden" name="sortColumnName" value="CHECKNUMBER"/>
	<input type="hidden" name="isAscending" value="true"/>
	<input type="hidden" name="accountID" value="<s:property value='#request.accountID'/>"/>
	<input type="hidden" name="bankID" value="<s:property value='#request.bankID'/>"/>
	<input type="hidden" name="payRadioAll" id="payRadioAll"  value="off"/>
	<input type="hidden" name="returnRadioAll" id="returnRadioAll" value="off"/>
	<input type="hidden" name="holdRadioAll" id="holdRadioAll" value="off"/>
	

	<s:hidden id="updatePositivePayDecisionsActionURLValue" name="updatePositivePayDecisionsActionURLValue" value="%{#updatePositivePayDecisionsActionURL}" />
	
	<ffi:setGridURL grid="GRID_ppayExceptionGridForAccount" name="ViewURL" url="/cb/pages/jsp/positivepay/SearchImageAction.action?module=POSITIVEPAY&pPayaccountID={0}&pPaybankID={1}&pPaycheckNumber={2}" parm0="CheckRecord.AccountID" parm1="CheckRecord.BankID" parm2="CheckRecord.CheckNumber" />
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/positivepay/getPositivePayIssuesForAccountAction.action?GridURLs=GRID_ppayExceptionGridForAccount&status=New&accountID=${accountID}&bankID=${bankID}" URLEncrypt="true"/>
    <s:url id="ppayExceptionForAccountGridURL" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="ppayExceptionForAccountGridID"
		sortable="true"  
		dataType="json"  
		href="%{ppayExceptionForAccountGridURL}"
		sortname="checkNumber"
		sortorder="asc"  
		pager="true"  
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onSortColTopics=""
		onGridCompleteTopics="addGridControlsEvents,PPayExceptionForAccountEvent,ppayAccountExceptionGridCompleteTopicForRadioRendering"
		> 
		
		<sjg:gridColumn name="issueDate" index="issueDate" title="%{getText('jsp.default_137')}" formatter="ns.cash.dateFormatter" sortable="true" width="80"/>
	    <sjg:gridColumn name="account" index="account" title="%{getText('jsp.default_15')}" sortable="true" width="348"/>
	    <sjg:gridColumn name="checkRecord.checkNumber" index="checkNumber" title="%{getText('jsp.cash_28')}" formatter="ns.cash.searchImageFormatter" sortable="true" width="80"  cssClass="datagrid_actionColumn ppayDetailsOnHoverCls"/>
	    <sjg:gridColumn name="checkRecord.amount.currencyStringNoSymbol" index="amount" title="%{getText('jsp.default_43')}" sortable="true" width="90"/>
	    <sjg:gridColumn name="rejectReason" index="rejectReason" title="%{getText('jsp.default_350')}" sortable="true" width="110"/>
		<sjg:gridColumn name="map.pay" index="pay" title="%{getText('jsp.default_312')}" formatter="ns.cash.RadioButtonPayFormatter" sortable="false" width="50"  cssClass = "PPAYDecisionId"/> 
	    <sjg:gridColumn name="map.return" index="return" title="%{getText('jsp.default_360')}" formatter="ns.cash.RadioButtonReturnFormatter" sortable="false" width="70"/> 
		<sjg:gridColumn name="map.hold" index="hold" title="%{getText('jsp.default_596')}" formatter="ns.cash.RadioButtonHoldFormatter" sortable="false" width="70"/>
		<sjg:gridColumn name="map.routingNumber" index="rountingNumber" title="%{getText('jsp.cash_95')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.cash_9')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.nickName" index="nickName" title="%{getText('jsp.default_293')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.currencyType" index="currencyType" title="%{getText('jsp.cash_44')}" sortable="true" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="map.defaultdecision" index="defaultdecision" title="%{getText('jsp.cash_44')}" hidden="true" hidedlg="true"/>
 		<sjg:gridColumn name="map.currentDecision" index="currentDecision" title="%{getText('jsp.cash_44')}" hidden="true" hidedlg="true"/>
	
		<sjg:gridColumn name="checkRecord.bankID" index="bankIDColumn" title="%{getText('jsp.cash_19')}" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="checkRecord.accountID" index="accountIDColumn" title="%{getText('jsp.cash_11')}" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass = "PPAYImageUrlId"/>
		
		
	</sjg:grid>
	<br>
	<%-- <ffi:cinclude value1="${AnyPendingPPayIssues}" value2="true" operator="equals"> --%>
		<div class="submitButtonsDiv">
			<sj:a
				id="ppayExceptionGridSubmitDummy"
                button="true"
				onClickTopics="ppayExceptionGridDummySubmit"
				><s:text name="jsp.default_395" /></sj:a>
		</div>
		<sj:submit
				id="ppayExceptionGridSubmit"
				cssStyle="display:none;"				
				formIds="ppayExceptionForAccountFormID"
				targets="secondDiv" 
				button="true" 
				onSuccessTopics="exceptionGridSubmitSuccessTopic"
                onErrorTopics="exceptionGridSubmitErrorTopic"
				onCompleteTopics="exceptionGridSubmitCompleteTopic"
                value="%{getText('jsp.default_395')}" 
		/>	
	<%-- </ffi:cinclude>    --%>                  
</s:form>


<div class="ppayHoverDetailsMaster">
	<div class="ppayDetailsWinOverlay"></div>
	<div class="ppayHoverDetailsHolder">
		<span class="winTitle">Check Image</span>
<!-- 		<div class="checkDetailsSection">
			<table border="0" width="100%" class="checkDetailsTable">
				<tr>
					<td class="fieldLbl" width="25%">Check Number:</td>
					<td class="fieldVal" width="25%">1016</td>
					<td class="fieldLbl" width="25%">Posting Date:</td>
					<td class="fieldVal" width="25%">04/30/1998</td>
				</tr>
				<tr>
					<td class="fieldLbl" width="25%">Account:</td>
					<td class="fieldVal" width="25%">50000-Savings</td>
					<td class="fieldLbl" width="25%">Debit Amount:</td>
					<td class="fieldVal" width="25%">$400,000.00</td>
				</tr>
			</table>
		</div> -->
		<hr />
		<div class="actionItemHolder">
			<input id="payImgHlder" type="radio" name="ppayAction" value="pay" >Pay
			<input id="returnImgHlder" type="radio" name="ppayAction" value="return">Return
			<input id="holdImgHlder" type="radio" name="ppayAction" value="hold">Hold
		</div>
		<div id ="imgGridHolder" class="chkImgHolder">
			<span class="imagePlaceholderText">Loading check image, please wait...</span>
		</div>
		

		<div class="imageControlsHolder">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<tr>
					<td colspan="2" align="center">
						<s:form action="" method="post" name="checkImageForm" id="checkImageID" theme="simple">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							
						</s:form>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>

	<script type="text/javascript">
		
	var currPpayExceptionForAccountGridPage = 1;

    $(document).ready(function () {
    	
    	$.subscribe('ppayAccountExceptionGridCompleteTopicForRadioRendering', function(event,data) {
        	$("#jqgh_ppayExceptionForAccountGridID_account").addClass("gridNicknameField");
        	
    		var payRadio = '<td align="left"><input type="radio" id="payRadioAllDummy" name="RadioAllDummy" style="vertical-align: middle;"> <s:text name="jsp.default_312" /></td>';
    		var returnRadio = '<td align="left"><input type="radio" id="returnRadioAllDummy" name="RadioAllDummy" style="vertical-align: middle;"> <s:text name="jsp.default_360" /></td>';
    		var holdRadio = '<td align="left"><input type="radio" id="holdRadioAllDummy" name="RadioAllDummy" style="vertical-align: middle;"> <s:text name="jsp.default_596" /></td>';
    		 
    		$("div[id='jqgh_ppayExceptionForAccountGridID_map.pay']").html(payRadio);
    		$("div[id='jqgh_ppayExceptionForAccountGridID_map.return']").html(returnRadio);
    		$("div[id='jqgh_ppayExceptionForAccountGridID_map.hold']").html(holdRadio);
    		
    		$("#payRadioAllDummy").click(function(event){
    			$("#payRadioAll").val("on");
				$("#returnRadioAll").val("off");
				$("#holdRadioAll").val("off");
				$("input[class='payclass']").prop('checked', true);
    		});

    		$("#returnRadioAllDummy").click(function(event){
    			$("#payRadioAll").val("off");
				$("#returnRadioAll").val("on");
				$("#holdRadioAll").val("off");
				$("input[class='returnclass']").prop('checked', true);
    		});
    		
    		$("#holdRadioAllDummy").click(function(event){
    			$("#payRadioAll").val("off");
				$("#returnRadioAll").val("off");
				$("#holdRadioAll").val("on");
				$("input[class='holdclass']").prop('checked', true);
    		});
    		
    		$(".ppayDetailsOnHoverCls a").hover(
					function(event) {						
						rowElement = event.fromElement.parentElement;
						var urlString = $(rowElement.getElementsByClassName('PPAYImageUrlId')).attr('title');
						$.ajax({
							url: urlString,
							success: function(data){
								$('#imgGridHolder').html(data);
								$('#payImgHlder').attr('checked', false);
								$('#returnImgHlder').attr('checked', false);
								$('#holdImgHlder').attr('checked', false);
								var decisionName  = $(rowElement.getElementsByClassName('PPAYDecisionId')).find('input').attr('name');
								// Checke if selected row has any decision.
								if( $('input:radio[name="'+decisionName+'"]').filter('[value="Pay"]').is(':checked')) {
									$('#payImgHlder').attr('checked', true);
								} else if($('input:radio[name="'+decisionName+'"]').filter('[value="Return"]').is(':checked')){
									$('#returnImgHlder').attr('checked', true);
								} else if($('input:radio[name="'+decisionName+'"]').filter('[value="Hold"]').is(':checked')) {
									$('#holdImgHlder').attr('checked', true);
								}
							}
						});
						$(".ppayHoverDetailsMaster").show();
						
						if(($(this).offset().left) <= ($(window).width()/2)) {
							$(".ppayHoverDetailsHolder").offset({left: ($(this).offset().left) });
						} else {
							$(".ppayHoverDetailsHolder").offset({left: ($(window).width() / 2) });
						}

					}, function() {
						// space for code to run on mouseout...
					}
				);
				
				 $("#payImgHlder").click(function(event){
					 var decisionName  = $(rowElement.getElementsByClassName('PPAYDecisionId')).find('input').attr('name');
					 $('input:radio[name="'+decisionName+'"]').filter('[value="Pay"]').attr('checked', true);
				 });
				 
				 $("#returnImgHlder").click(function(event){
					 var decisionName  = $(rowElement.getElementsByClassName('PPAYDecisionId')).find('input').attr('name');
					 $('input:radio[name="'+decisionName+'"]').filter('[value="Return"]').attr('checked', true);
				 });
				 
				 $("#holdImgHlder").click(function(event){
					 var decisionName  = $(rowElement.getElementsByClassName('PPAYDecisionId')).find('input').attr('name');
					 $('input:radio[name="'+decisionName+'"]').filter('[value="Hold"]').attr('checked', true);
				 });
				

				$(".ppayHoverDetailsHolder").hover(function(event){
					event.stopPropagation();
				});

				$(".ppayDetailsWinOverlay").hover(function(event) {
					$(".ppayHoverDetailsMaster").hide();
				})
				.mouseleave(function() {
					// space for code to run on mouseleave event
				});
		});


		
		var fnCustomPaging = function(nextPage){
			
			currPpayExceptionForAccountGridPage = $("#ppayExceptionForAccountGridID").jqGrid('getGridParam', 'page');
			var rowNumberSelected = $('#gbox_ppayExceptionForAccountGridID .ui-pg-selbox :selected').val();
			
			//When user clicks on First and Last links, the page returned is not the actual pagem but the string is returned
			//Convert that string to the actual first or last pagenumber
			if(nextPage == 'first_ppayExceptionForAccountGridID_pager'){
				nextPage = 1;
			}else if(nextPage == 'last_ppayExceptionForAccountGridID_pager'){
				//Get the last page number of the grid
				nextPage = $("#ppayExceptionForAccountGridID").jqGrid('getGridParam', 'lastpage');	
			}
			
			$.ajax({    
			  url: $('#updatePositivePayDecisionsActionURLValue').val(),    
			  data: $("#ppayExceptionForAccountFormID").serialize(),
			  success: function(data) {
				  $("#ppayExceptionForAccountGridID").jqGrid('setGridParam', {rowNum:rowNumberSelected});
				  $('#ppayExceptionForAccountGridID').trigger("reloadGrid",[{page:nextPage}]);
				  $.publish('PPayExceptionPagingEvent');
			  },
			  error: function(jqXHR, textStatus, errorThrown) {
				 //set the grid page value back to original
				  $("#ppayExceptionForAccountGridID").jqGrid('setGridParam', {rowNum:rowNumberSelected});
				  $("#ppayExceptionForAccountGridID").jqGrid('setGridParam', {page:currPpayExceptionForAccountGridPage});
			  }   
			});  
			return 'stop';
		}
		
		$("#ppayExceptionForAccountGridID").data('onCustomPaging', fnCustomPaging);
		
		var fnCustomSortCol = function (index, columnIndex, sortOrder) {
			$.publish("PPayExceptionForAccountOnSortTopic");
		};
		
		$("#ppayExceptionForAccountGridID").data('onCustomSortCol', fnCustomSortCol);
		
		//ppay exception grid complete event.
		$.subscribe('PPayExceptionPagingEvent', function(event,data) {
			$("#payRadioAll").val("off");
			$("#returnRadioAll").val("off");
			$("#holdRadioAll").val("off");
		});
		
		
		$.subscribe('ppayExceptionGridDummySubmit', function(event,data) {
			$.ajax({    
				  url: $('#updatePositivePayDecisionsActionURLValue').val(),    
				  data: $("#ppayExceptionForAccountFormID").serialize(),
				  success: function(data) {  
					 //Trigger form submt action now
					 $("#ppayExceptionGridSubmit").click();
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
					 //console.log('Error occurred while submitting !');
				  }      
				});
		});
		
		$.subscribe('PPayExceptionForAccountOnSortTopic', function(event,data) {
			$.ajax({    
				  url: $('#updatePositivePayDecisionsActionURLValue').val(),    
				  data: $("#ppayExceptionForAccountFormID").serialize(),
				  success: function(data) {
				  },
				  error: function(jqXHR, textStatus, errorThrown) {
				  }   
				});  
		});
    });

    </script>