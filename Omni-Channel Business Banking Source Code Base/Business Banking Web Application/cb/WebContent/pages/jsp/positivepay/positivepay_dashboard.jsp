<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

   	<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-positivePay"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_163" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/cashflowSubmenuDropdown.jsp" />
    	</div>
    	
    	<!-- dashboard items listing for PositivePay -->
        <div id="dbPositivePaySummary" class="dashboardSummaryHolderDiv">
        <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_DECISION_ENTITLEMENT %>" >
       		<sj:a
				id="ppaySummaryBtn"
				indicator="indicator"
				cssClass="summaryLabelCls"
				button="true"
				onclick="$('li[menuId=cashMgmt_ppay]').find('a').eq(0).click()"
				title="%{getText('jsp.default_400')}"
			><s:text name="jsp.default_400"/></sj:a>
        </ffi:cinclude>
			
			<s:url id="ppaybuildUrl" value="/pages/jsp/positivepay/buildPositivePayManualFileAction_init.action">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
			</s:url>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_ENTITLEMENT_ISSUE_MAINTENANCE %>" >
			<sj:a 
				id="ppaybuildLink"
				href="%{ppaybuildUrl}" 
				targets="firstDiv" 
				indicator="indicator" 
				button="true"
				onBeforeTopics="enableManualFileDiv"
				onSuccessTopics="buildManualFileSuccess"
			><s:text name="jsp.cash_21"/></sj:a>
			</ffi:cinclude>
			<%-- 
			<sj:a id="PPayReportingButton" 
				  button="true"					
				  onclick="ns.shortcut.goToFavorites('reporting_cashmgt');"
				  ><s:text name="jsp.default_355"/></sj:a>
			
			 --%>
		</div>
		
		
		<!-- dashboard items listing for File Upload -->
        <div id="dbPPayFileUploadSummary" class="dashboardSummaryHolderDiv">
        	<% boolean fileUploadEntitled = false; %>
			
			
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_ENTITLEMENT_FILE_UPLOAD %>" >
					<% fileUploadEntitled = true; %>
				</ffi:cinclude>
			</ffi:cinclude>
			
			<% if( fileUploadEntitled ) { %>
			    <s:url id="ppayuploadUrl" value="/pages/jsp/cash/cashppupload.jsp">
				    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
				</s:url>
				<sj:a 
					id="ppayuploadLink"
					href="%{ppayuploadUrl}" 
					targets="firstDiv" 
					indicator="indicator" 
					button="true"
					cssClass="summaryLabelCls"
					onClickTopics="onClickUploadCheckingRecord"
					onSuccessTopics="uploadCheckingRecordSuccess"
				><s:text name="jsp.default_211"/></sj:a>
			<% } %>
			
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
				 	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_ENTITLEMENT_FILE_UPLOAD %>" >
					
					<s:url id="ppaymappingUrl" value="/pages/jsp/fileuploadcustom.jsp" escapeAmp="false">
						<s:param name="Section" value="%{'PPAY'}"/>
					    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					</s:url>
					<sj:a 
						id="ppaymappingLink"
						href="%{ppaymappingUrl}" 
						targets="mappingDiv" 
						indicator="indicator" 
						button="true" 
						onSuccessTopics="mappingSuccess"
					><s:text name="jsp.cash_1"/></sj:a>				
					
					<s:url id="addCustomMappingUrl" value="/pages/jsp/custommapping.jsp" escapeAmp="false">
						<s:param name="SetMappingDefinitionID" value="0"/>
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
					</s:url>
					<sj:a
						id="addCustomMappingsLink"
						href="%{addCustomMappingUrl}"
						targets="inputCustomMappingDiv"
						title="%{getText('jsp.default_135')}"
						indicator="indicator"
						button="true"
						onClickTopics="beforeCustomMappingsOnClickTopics" 
						onSuccessTopics="addmappingSuccess,addMappingSuccessCash"
						onCompleteTopics="addCustomMappingsOnCompleteTopics" 
						onErrorTopics="errorCustomMappingsOnErrorTopics"
					>
						<s:text name="jsp.default_34" />
					</sj:a> 
					</ffi:cinclude>
				</ffi:cinclude>
			</ffi:cinclude>
		</div>	
		
		
		<% if( fileUploadEntitled ) { %>
			<!-- More list option listing -->
			<div class="dashboardSubmenuItemCls">
				<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
	    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
				<!-- UL for rendering tabs -->
				<ul class="dashboardSubmenuItemList">
					<li class="dashboardSubmenuItem"><a id="showdbPositivePaySummary" onclick="ns.common.refreshDashboard('showdbPositivePaySummary',true)"><s:text name="jsp.home_163"/></a></li>
					<li class="dashboardSubmenuItem"><a id="showdbPPayFileUploadSummary" onclick="ns.common.refreshDashboard('showdbPPayFileUploadSummary',true)"><s:text name="jsp.default_211"/></a></li>
				</ul>
			</div>
		<% } %>
		
		
		
		
	</div>
	
	<sj:dialog id="InquiryTransferDialogID" cssClass="cashMgmtDialog" title="%{getText('jsp.default_249')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
	</sj:dialog>
	
	
<script>
	$("#ppaySummaryBtn").find("span").addClass("dashboardSelectedItem");
</script>