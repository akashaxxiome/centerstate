<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript">
    $(document).ready(function () {
    	$("#sentMessagesGridId").data("supportSearch",true);
    	
		var checkbox = '<input style="margin-top:0px" type="checkbox" name="selectionCtl2" id="selectionCtl2"><s:text name="jsp.default_163"/>';
		$idheader = $('#jqgh_sentMessagesGridId_ID');
		$idheader.css({'text-align': 'left','padding-left':'15px'});
		$idheader.html(checkbox);
		$('#sentMessagesGridId_ID').unbind('click');
		$('#jqgh_sentMessagesGridId_ID #selectionCtl2').click(function(event){
			if($(this).attr('checked'))
				$("table#sentMessagesGridId").find("input[name='selectedMsgIDs'][type='checkbox']").attr('checked', true);
			else
				$("table#sentMessagesGridId").find("input[name='selectedMsgIDs'][type='checkbox']").attr('checked', false); 
		});

		if($("#sentMessagesGridId tbody").data("ui-jqgrid") != undefined){
    		$("#sentMessagesGridId tbody").sortable("destroy");
		}
		
		$.subscribe('deleteAllCheckboxes', function(event,data) {
			$('#jqgh_sentMessagesGridId_Delete').removeClass( "ui-jqgrid-sortable" ).addClass( "sapUiIconCls icon-delete" );
			$('#jqgh_sentMessagesGridId_Delete').css({"cursor" : "pointer"});
			$("#sentMessagesGridId tbody > tr > td:first-child input").css({ "margin-left" : "14px" });
			//$('#jqgh_inboxMessagesGridId_cb').css({"padding-left":"0", "float":"left"});
		});
    });
</script>
<div id="Sent_Messages">
	<ffi:help id="messages_msg_sent"/>
	<ffi:setProperty name="MessageBox" value="Sent"/>

	<ffi:setGridURL grid="GRID_msgsent" name="ViewURL" url="/cb/pages/jsp/message/MessageViewAction_sent.action?MessageID={0}" parm0="ID"/>

    <form id="frmDelMsgFromSentbox" action="/pages/jsp/message/messagesdelete.jsp" method="post" name="frmDelMsgFromSentbox">  
    
    	<input type="hidden" name="selectedMsgIDs" id="selectedSentMsgIDs" />
    	<input type="hidden" name="messageIDs" id="allSelectedInboxMsgIDs" />
		<input type="hidden" id="multicheck" value="false"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/message/GetMessagesAction.action?msgFilter=SentFilter&GridURLs=GRID_msgsent&sentItems=true&messageBody=false" URLEncrypt="true"/>
    <s:url id="sentMessagesUrl" value="%{#session.tempURL}" escapeAmp="false"/>
    
    <s:if test="%{#parameters.visibleGrid[0] == 'sentbox'}">
		<ffi:setProperty name="gridDataType" value="json"/>
	</s:if>
	<s:else>
		<ffi:setProperty name="gridDataType" value="local"/>
	</s:else>
    
	<sjg:grid  
		id="sentMessagesGridId"  
		dataType="%{#session.gridDataType}"  
		sortable="true"  
		href="%{sentMessagesUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		sortname="receiveddate"
		sortorder="desc"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		multiselect="true"
        multiselectWidth="30"
		reloadTopics="reloadSentMessages"
		onPagingTopics="sentMessagesGridPagingEvents"
		onCompleteTopics="sentMessagesLoadCompleteEvents"
		onGridCompleteTopics="addGridControlsEvents,sentMessagesGridCompleteEvents,deleteAllCheckboxes"
		> 
		
		<sjg:gridColumn 
			title="" 
			name="Delete"
			sortable="false" 
			width="35" 
			cssClass="datagrid_textColumn"
			hidedlg="true"/>
			
		<sjg:gridColumn 
			name="messageIds" 
			title="messageIds" 
			sortable="false" 
			search="false" 
			width="15" 
			cssClass="datagrid_textColumn"
			formatter="ns.message.deleteAllIds"
			hidedlg="true"
			hidden="true"/>
			
		<sjg:gridColumn 
			name="toName" 
			index="to" 
			title="%{getText('jsp.message_24')}" 
			sortable="true" 
			formatter="ns.message.formatToName" 
			width="270"/>
			
		<sjg:gridColumn 
			name="subject" 
			index="subject" 
			title="%{getText('jsp.default_394')}" 
			sortable="true" 
			formatter="ns.message.formatSubjectLink2" 
			width="360"/>
			
		<sjg:gridColumn 
			name="caseNumDesc" 
			index="casenum" 
			title="%{getText('jsp.message_7')}" 
			hidedlg="false"  
			sortable="true" 
			search="true" 
			hidden="false" 
			width="150"/>
		
		<sjg:gridColumn 
			name="createDate" 
			index="receiveddate" 
			title="%{getText('jsp.message_19')}" 
			hidedlg="false" 
			sortable="true" 
			hidden="false" 
			width="150"/>

		<sjg:gridColumn 
			name="ID" 
			index="id" 
			title="%{getText('jsp.default_163')}" 
			hidedlg="true"
			search="false" 
			sortable="false" 
			hidden="true" 
			cssClass="__gridActionColumn"
			formatter="ns.message.formatSentDeleteLinks" 
			width="150"/>

		<sjg:gridColumn name="ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		
	</sjg:grid>
	
	</form>
</div>
<ffi:removeProperty name="gridDataType"/>    