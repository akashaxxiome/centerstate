<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>


<ffi:help id="messages_messagereply" />






<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="M/d/yyyy" />
<s:form id="sendReplyForm" namespace="/pages/jsp/message" validate="false" action="SendReplyAction" method="post" name="sendReplyForm" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden name="messageID" value="%{message.ID}"></s:hidden>				
<s:hidden name="replyMessage.ID" value="%{message.ID}"></s:hidden>
<s:hidden name="replyMessage.subject" value="%{message.subject}"></s:hidden>
<s:hidden name="replyMessage.to" value="%{message.from}"></s:hidden>
<s:hidden name="replyMessage.from" value="%{#session.User.id}"></s:hidden>
<s:hidden name="replyMessage.toType" value="%{message.fromType}"></s:hidden>
<s:hidden name="replyMessage.fromType" value="%{message.toType}"></s:hidden>
<div class="leftPaneWrapper">
		<div class="leftPaneInnerWrapper">
			<div class="header">Message Summary</div>
			<div class="leftPaneInnerBox summaryBlock">
				<div style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.message_9"/></span>
					<span class="inlineSection floatleft labelValue" id="fromInViewMsg"><ffi:getProperty name="Message" property="FromName"/></span>
				</div>
				<div class="marginTop10 clearBoth floatleft" style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.message_16"/></span>
					<span class="inlineSection floatleft labelValue" id="ReceivedInViewMsg"><ffi:getProperty name="Message" property="DateValue"/></span>
				</div>
			</div>
		</div>
</div>
<div class="rightPaneWrapper w71">
	<div class="paneWrapper">
		  	<div class="paneInnerWrapper">
		   		<div class="header"><s:text name="jsp.default_516"/> <ffi:getProperty name="Message" property="Subject"/></div>
		   		<div class="paneContentWrapper" >
			   		<div>
			   		<s:text name="jsp.message_10"/><br/><br/>
			   		<textarea class="txtbox ui-widget-content ui-corner-all" id="sendmessagereply_memo" name="replyMessage.memo" rows="9" cols="120" wrap="physical">


						&gt;-------------
						<s:text name="jsp.message_9"/> <ffi:getProperty name="Message" property="FromName"/>
						<s:text name="jsp.message_21"/> <ffi:getProperty name="Message" property="DateValue"/>
						
						<ffi:getProperty name="Message" property="Memo"/></textarea></div>
						<div class="marginTop10"><span id="memoError"></span></div>
		  
<div class="btn-row">

<sj:a   
							id="cencelMessageReplyButton"
								href="#"
							button="true" 
							onClickTopics="cancelReply"
			        ><s:text name="jsp.default_82"/></sj:a>

		            <sj:a
						id="sendingReplySubmit"
						formIds="sendReplyForm"
                        targets="resultmessage" 
                        button="true" 
                        validate="true" 
                        validateFunction="customValidation"
                        onBeforeTopics="beforeSendingReply"
                        onCompleteTopics="sendingReplyComplete"
					    onErrorTopics="errorSendingReply"
                        onSuccessTopics="successSendingReply"
                       ><s:text name="jsp.default_378"/></sj:a>
</div>
</div>
</div>
</div>  
</div>    
		<%--	<form action="(EmptyReference!)" method="post" name="FormName"> --%>
		<%-- <table width="100%" border="0" cellspacing="0" cellpadding="3" class="greenClayBackground">
			<tr>
				<td align="right" valign="baseline" nowrap class="greenClayBackground" width="10%" height="17"><span class="sectionsubhead"><s:text name="jsp.message_25"/> </span></td>
				<td width="20%" valign="baseline"><span id="toInReplyMsg" class="columndata"><ffi:getProperty name="Message" property="FromName"/></span></td>
				<td width="30%"></td>
				<td width="40%"></td>
			</tr>
			<tr>
				<td align="right" valign="baseline" nowrap class="greenClayBackground" height="17"><span class="sectionsubhead"><s:text name="jsp.default_516"/> </span></td>
				<td  valign="baseline"><span id="subjectInReplyMsg" class="columndata"><s:text name="jsp.message_14"/><ffi:getProperty name="Message" property="Subject"/></span></td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3" class="greenClayBackground" >
				<textarea class="txtbox ui-widget-content ui-corner-all" id="sendmessagereply_memo" name="replyMessage.memo" rows="9" cols="120" wrap="physical">


&gt;-------------
<s:text name="jsp.message_9"/> <ffi:getProperty name="Message" property="FromName"/>
<s:text name="jsp.message_21"/> <ffi:getProperty name="Message" property="DateValue"/>

<ffi:getProperty name="Message" property="Memo"/></textarea>
					<br/><span id="memoError"></span>
				</td>
			</tr>
			<tr>
	
				<td colspan="4" align="center" nowrap class="greenClayBackground" >

					<sj:a   
							id="cencelMessageReplyButton"
								href="#"
							button="true" 
							onClickTopics="cancelReply"
			        ><s:text name="jsp.default_82"/></sj:a>

		            <sj:a
						id="sendingReplySubmit"
						formIds="sendReplyForm"
                        targets="resultmessage" 
                        button="true" 
                        validate="true" 
                        validateFunction="customValidation"
                        onBeforeTopics="beforeSendingReply"
                        onCompleteTopics="sendingReplyComplete"
					    onErrorTopics="errorSendingReply"
                        onSuccessTopics="successSendingReply"
                       ><s:text name="jsp.default_378"/></sj:a>

				</td>
			</tr>
		</table> --%>

	</s:form>

	<!-- <ffi:setProperty name="SendReply" property="Subject" value="Re:${Message.Subject}"/> -->



	<script>
		
	$(document).ready(function() {
		ns.common.renderRichTextEditor("sendmessagereply_memo");
	});
	
	</script>