<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="com.ffusion.beans.user.UserLocale"%>

<ffi:help id="home_leftbar_messagesmallPanel" />
<ffi:flush />

<div id="msgsMasterWrapper">
	<div id="msgsTopActionbar" class="msgsTopActionBarCls">
		<div class="floatleft topActionItemsCls">
			<span id="unread" onclick="ns.home.loadUnReadMessages(5)" class="topActionItemSelected"><s:text name="message.unread" /></span>
			<span id="read" onclick="ns.home.loadAllMessages(5)"><s:text name="message.read" /></span>
		</div>
		<span class="newMsgComposeActionCls marginRight5" onclick="composeNewMsg()">+ <s:text name="jsp.message_27" /></span>
	</div>
	<div class="msgItemHolderDiv">
		<!-- if there's no message -->
		<s:if test="%{Messages.size==0}">
			<span class="noMsgSpan"><s:text name="jsp.message_22" /></td>
		</s:if>
		<!-- if there're message(s) -->
		<ffi:cinclude value1="0" value2="${Messages.Size}" operator="notEquals">
			<ffi:setProperty name="Math" property="Value1" value="3" />
			<ffi:setProperty name="Math" property="Value2" value="1" />
			<s:set var="count" value="0" />
			<ffi:list collection="Messages" items="Message1">
			<s:if test="%{#count<messageSearchCriteria.pageInfo.pageSize}">
				<ffi:setProperty name="Message1" property="DateFormat" value="${UserLocale.DateTimeFormat}" />
				<div class='msgDataHolder' id='<ffi:getProperty name="Message1" property="ID"/>'>
					<a href="javascript:void(0)" onclick="ns.home.quickViewMessage('<ffi:urlEncrypt url="/cb/pages/jsp/message/MessageViewAction_viewNotificationMessage.action?MessageID=${Message1.ID}"/>' ,'<ffi:getProperty name="Message1" property="ID"/>' ,true)">
						<div class="msgSenderDateDetails">
							<span class="msgSenderName">
									<%String fromUserName = ""; %>
										<ffi:getProperty name="Message1" property="fromName" assignTo="fromUserName" />
										<%
										if(fromUserName !=null && !fromUserName.equals("") && fromUserName.indexOf("(") >=0 ){
											fromUserName = fromUserName.substring(0, fromUserName.indexOf("(")); 
										}
										%>
										<%=fromUserName%>
							</span>
							<span id="dateField" class="msgDate"><ffi:getProperty name="Message1" property="DateValue" />
							</span>
						</div>
						<div class="msgDetails">
							<span class="messageSubject">
								<ffi:getProperty name="Message1" property="subject" />
							</span>
							<textarea class="memoTextAreaClass" style="display: none;">
									<ffi:getProperty name="Message1" property="memo" />
							</textarea>   
							<span class="messageMemo">
								<ffi:getProperty name="Message1" property="memo" />
							</span>
						</div>
					</a>
					<div class="msgActionItemHolder">
						<ffi:cinclude value1="${Message1.TypeValue}" value2="<%= Integer.toString(com.ffusion.efs.adapters.profile.constants.ProfileDefines.MESSAGE_TYPE_GLOBAL_MESSAGE)%>" operator="notEquals" >
							<span class="ffiUiIcoMedium ffiUiIco-icon-reply" title="<s:text name='jsp.message_17' />" onclick="ns.home.quickReplyMessage('<ffi:urlEncrypt url="/cb/pages/jsp/message/SendReplyAction_initNotificationReply.action?MessageID=${Message1.ID}"/>' ,'<ffi:getProperty name="Message1" property="ID"/>' ,true)"></span>
						</ffi:cinclude>
						<span class="ffiUiIcoMedium ffiUiIco-icon-medium-delete" title="<s:text name='message.delete' />" onclick="ns.home.quickDeleteMessage('<ffi:urlEncrypt url="/cb/pages/jsp/message/DeleteMessagesAction_confirm.action?selectedMsgIDs=${Message1.ID}&quickDelete=true"/>' ,'<ffi:getProperty name="Message1" property="ID"/>' ,true)"></span>
					</div>
				</div>
				<ffi:setProperty name="Math" property="Value2" value="${Math.Subtract}" />
				</s:if>
				<s:set var="count" value="%{#count+1}" />
			</ffi:list>
			<input type="hidden" id="pageSize" value="<s:property value='%{messageSearchCriteria.pageInfo.pageSize}'/>"/>
			<input type="hidden" id="messagesCount" value="<s:property value="%{messageSearchCriteria.pageInfo.totalRecords}"/>"/>
			<!-- Show 'More' label for navigation to Messages module -->
			<s:if test="%{messageSearchCriteria.pageInfo.totalRecords>5 && messageSearchCriteria.pageInfo.pageSize<messageSearchCriteria.pageInfo.totalRecords}">
				<tr class="boldIt" style="text-align: center!important;cursor: pointer;">
					<span class="msgMoreBtnSpan" onclick="updateCount()"><s:text name="message.more" /><span class="msgMoreLinkDownArrow ffiUiIcoSmall ffiUiIco-icon-downArrow marginleft5"></span></span>
				</tr>
			</s:if>
		</ffi:cinclude>
	</div>
</div>

<div id="quickMessagingResponse" style="display: block; height: 0 !important;"></div>

<script type="text/javascript">

function updateCount(){
	var messagesCount = parseInt($('#messagesCount').val());
	var pageSize = parseInt($('#pageSize').val());
	if(messagesCount>pageSize){
		pageSize+=5;
		$('#pageSize').val(pageSize);
		if('<s:property value="%{messageSearchCriteria.unReadOnly}"/>'=='true'){
			ns.home.loadUnReadMessages($('#pageSize').val());
		}else{
			ns.home.loadAllMessages($('#pageSize').val());
		}
	}
}
	$(document).ready(function() {
		$("#messageSmallPanelPortlet").find("tr.quickMessageStyle")
			.hover(function() {
				$(this).find(".messageActionsWrapper").show();
			}, function() {
				$(this).find(".messageActionsWrapper").hide();
		});
		
		$(".memoTextAreaClass").each(function(){
			$(this).parent().find(".messageMemo").html($(this).text());
		});
	});
	function composeNewMsg() {
		ns.home.quickSendMessageURL = '<ffi:urlEncrypt url="/cb/pages/jsp/message/SendMessageAction_init.action?initResponse=quickNewMessage"/>';
		ns.home.quickSendMessage(ns.home.quickSendMessageURL);
	}
</script>
