<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

	<div align="left">

      <s:form id="frmSendQuickMessage" namespace="/pages/jsp/message" validate="false" action="SendMessageAction" method="post" name="frmSendQuickMessage" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="greenClayBackground tableData">
				<tr>
					<td align="right" nowrap class="greenClayBackground" ><span class="sectionsubhead"><s:text name="jsp.message_9"/></span></td>
					<td colspan="3"><ffi:getProperty name="User" property="FullName"/></td>
					<s:hidden name="messageModel.from" value="%{#session.User.id}"></s:hidden>
				</tr>

				<tr>
					<td align="right" nowrap class="greenClayBackground" ><span class="sectionsubhead"><s:text name="message.messageForm.date"/></span></td>
					<td colspan="3"><ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateFormat}"/>
			      		<ffi:getProperty name="GetCurrentDate" property="Date"/></td>
				</tr>
			
				<tr>
					<td align="right" nowrap class="greenClayBackground" width="10%"><span class="sectionsubhead"><s:text name="jsp.default_430"/></span></td>
					<td width="40%">
						<%-- <ffi:setProperty name="MessageQueues" property="SortedBy" value="QueueName"/>
						<ffi:setProperty name="MessageQueues" property="Filter" value="QueueType=0" /> --%>
						<select id="new_quick_message_topics" class="txtbox"  name="messageModel.to" size="1">
							<ffi:list collection="MessageQueues" items="Queue1">
								<option value="<ffi:getProperty name="Queue1" property="QueueID"/>" 
									<ffi:cinclude value1="${MessageModel.To}" value2="${Queue1.QueueID}" operator="equals">selected</ffi:cinclude>>
									<ffi:getProperty name="Queue1" property="QueueName"/>
								</option>
							</ffi:list>
						</select><span id="toError"></span>
						<script>
							$('#new_quick_message_topics').selectmenu({width:300});
						</script>
					</td>
					<td width="40%"></td>
					<td width="10%"></td>
				</tr>
				<tr>
					<td align="right" nowrap class="greenClayBackground" ><span class="sectionsubhead"><s:text name="jsp.default_516"/></span><span class="required">*</span></td>
					<td colspan="3"><input class="txtbox ui-widget-content ui-corner-all" type="text" id="sendquickmessage_subject" name="messageModel.subject" value="<ffi:getProperty name="SendMessage" property="Subject"/>" size="40" maxlength="100">
						<br/><span id="subjectError"></span>
					</td>
				</tr>
				<tr>
					<td align="right" nowrap class="greenClayBackground" style="vertical-align:top;"><span class="sectionsubhead"><s:text name="jsp.message_10"/></span><span class="required">*</span></td>
					<td colspan="3" class="greenClayBackground">
						<textarea class="txtbox ui-widget-content ui-corner-all" id="sendQuickMessageMemo" name="messageModel.memo" rows="9" cols="60" wrap="physical"><ffi:getProperty name="SendMessage" property="Memo"/></textarea>
						<%-- <sj:a   button="true" id="clearQuickMessageBoxButton"	onClickTopics="messages.clearQuickMessageBoxArea"><s:text name="jsp.default.label.clear"/></sj:a> --%>
						<br/><span id="memoError"></span>
					</td>
				</tr>
				<tr>
					<td colspan="3" class="greenClayBackground"><br></td>
					<td class="greenClayBackground"></td>
				</tr>
				<tr>	
					<td align="center" colspan="4">
					<span class="required">* <s:text name="jsp.default_240"/></span>
					</td>
				</tr>
				<tr>
					<td colspan="4"><br></td>
				</tr>
				<tr>
					<td colspan="4" align="center" nowrap class="ui-widget-header customDialogFooter">
							
							<sj:a button="true" 
								id="clearQuickMessageBoxButton"
								onClick ="CKEDITOR.instances.sendQuickMessageMemo.setData('')"	
								onClickTopics="messages.clearQuickMessageBoxArea">
									<s:text name="jsp.default.label.clear"/>
							</sj:a>
							
							<sj:a   button="true" 
									onClickTopics="closeDialog"
					        ><s:text name="jsp.default_82"/></sj:a>
				            <sj:a
								id="sendQuickMessageSubmit"
								formIds="frmSendQuickMessage"
	                            targets="quickMessagingResponse" 
	                            button="true" 
	                            validate="true" 
	                            validateFunction="customValidation"
	                            onBeforeTopics="beforeSendingQuickMessage"
	                            onCompleteTopics="sendingQuickMessageComplete"
								onErrorTopics="errorSendingQuickMessage"
	                            onSuccessTopics="successSendingQuickMessage"
	                        ><s:text name="jsp.default_378"/></sj:a>
					</td>
				</tr>
			</table>
		</s:form>
		<br>
	</div>

	<script>
		$(document).ready(function(){
			if(accessibility){
				$('#sendquickmessage_subject').focus();
			}
		});
		
		$.subscribe('newMessageDialogOpenTopic', function(event,data) {
			setTimeout(function(){ns.common.renderRichTextEditor('sendQuickMessageMemo','500');}, 500);
		});	
		
		$.subscribe('newMessageDialogCloseTopic', function(event,data) {
			CKEDITOR.instances['sendQuickMessageMemo'].updateElement();
			CKEDITOR.instances['sendQuickMessageMemo'].setData('');
		});
		
		
		
	</script>