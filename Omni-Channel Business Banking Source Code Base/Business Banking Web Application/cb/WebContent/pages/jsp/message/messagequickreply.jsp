<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="M/d/yyyy" />
	<div align="center">
	<span id="formError" align="center"></span>
    <s:form id="sendQuickReplyForm" namespace="/pages/jsp/message" validate="false" action="SendReplyAction" method="post" name="sendQuickReplyForm" theme="simple">
   		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
   		<s:hidden name="messageID" value="%{message.ID}"></s:hidden>
   		<s:hidden name="replyMessage.ID" value="%{message.ID}"></s:hidden>
   		<s:hidden name="replyMessage.subject" value="%{message.subject}"></s:hidden>
   		<s:hidden name="replyMessage.to" value="%{message.from}"></s:hidden>
   		<s:hidden name="replyMessage.from" value="%{#session.User.id}"></s:hidden>	
   		<s:hidden name="replyMessage.toType" value="%{message.fromType}"></s:hidden>
   		<s:hidden name="replyMessage.fromType" value="%{message.toType}"></s:hidden>
		<%--	<form action="(EmptyReference!)" method="post" name="FormName"> --%>
		<table width="100%" border="0" cellspacing="0" cellpadding="3" class="greenClayBackground tableData">
			<tr>
				<td align="right" valign="baseline" nowrap class="greenClayBackground" width="10%" height="17"><span class="sectionsubhead"><s:text name="jsp.message_25"/> </span></td>
				<td width="40%" valign="baseline"><span id="toInReplyMsg" class="columndata""><ffi:getProperty name="Message" property="FromName"/></span></td>
				<td width="25%"></td>
				<td width="25%"></td>
			</tr>
			<tr>
				<td align="right" valign="baseline" nowrap class="greenClayBackground"  height="17"><span class="sectionsubhead"><s:text name="jsp.default_516"/> </span></td>
				<td valign="baseline"><span id="subjectInReplyMsg" class="columndata"><s:text name="jsp.message_13"/> <ffi:getProperty name="Message" property="Subject"/></span></td>
			</tr>
			<tr>
				<td></td>
				<td colspan="3" class="greenClayBackground" >
				<textarea class="txtbox ui-widget-content ui-corner-all" id="sendquickmessagereply_memo" name="replyMessage.memo" rows="9" cols="70" wrap="physical" style="width:90%;">


&gt;-------------
<s:text name="jsp.message_9"/> <ffi:getProperty name="Message" property="FromName"/>
<s:text name="jsp.message_21"/> <ffi:getProperty name="Message" property="DateValue"/>
	
<ffi:getProperty name="Message" property="Memo"/></textarea>
				<br/><span id="memoError"></span>
			</td>
			</tr>
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="4" align="center" nowrap class="ui-widget-header customDialogFooter">

					<sj:a   button="true" 
							onClickTopics="cancelQuickReplyMsg"
			        ><s:text name="jsp.default_82"/></sj:a>

		            <sj:a
						id="sendingQuickReplySubmit"
						formIds="sendQuickReplyForm"
                        targets="quickMessagingResponse" 
                        button="true" 
                        validate="true" 
                        validateFunction="customValidation"
                        onBeforeTopics="beforeSendingQuickReply"
                        onCompleteTopics="sendingQuickReplyComplete"
					    onErrorTopics="errorSendingQuickReply"
                        onSuccessTopics="successSendingQuickReply"
                       ><s:text name="jsp.default_378"/></sj:a>

				</td>
			</tr>
		</table>

	</s:form>
	<br>

	<%-- <ffi:setProperty name="SendReply" property="Subject" value="Re:${Message.Subject}"/> --%>


	</div>
	<script>
		$(document).ready(function() {
			setTimeout(function(){ns.common.renderRichTextEditor('sendquickmessagereply_memo','500');}, 500);
		});
	</script>
	