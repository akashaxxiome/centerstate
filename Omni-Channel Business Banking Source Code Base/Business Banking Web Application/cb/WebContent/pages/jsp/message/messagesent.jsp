<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>

<%--
	Check if the messages have been given a sort order.
	If not (i.e., if ${Messages.SortedBy} is "DateCreated,REVERSE"), sort by received date.
--%>
<ffi:cinclude value1="${Messages.SortedBy}" value2="DateCreated,REVERSE" operator="equals" >
	<ffi:setProperty name="Messages" property="SortedBy" value="DateValue,REVERSE"/>
</ffi:cinclude>

<ffi:cinclude value1="${refresh}" value2="true">
	<ffi:setProperty name="GetMessages" property="Refresh" value="true"/>
	<ffi:process name="GetMessages"/>
	<ffi:setProperty name="GetMessages" property="Refresh" value="false"/>
	<ffi:setProperty name="refresh" value=""/>
 </ffi:cinclude>

		<div align="center">

<%-- include page header, PageHeading and PageText should be set --%>
<ffi:include page="${PathExt}inc/nav_header.jsp" />

			<ffi:flush/>
            <form name="frmMessages" method="post" action="<ffi:getProperty name='SecurePath'/>messagesdeletesent.jsp">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
            <table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="5" width="750" background="/cb/web/multilang/grafx/sechdr_blank.gif" class="sectiontitle">&nbsp;&nbsp; &gt; <s:text name="jsp.message_20"/></td>
				</tr>
				<ffi:setProperty name="reverse${Messages.SortedBy}" value=",REVERSE"/>
				<tr>
        <ffi:setProperty name="reverse${Messages.SortedBy}" value=",REVERSE"/>


					<td class="tbrd_ltb" valign="middle" width="120">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><span class="sectionsubhead"><s:text name="jsp.message_24"/>&nbsp;</span></td>
								<td><ffi:link url="${SecurePath}messagesent.jsp?Messages.SortedBy=To_Name${reverseTo_Name}">
								    <ffi:cinclude value1="${Messages.SortedBy}" value2="To_Name" operator="notEquals" >
									    <ffi:cinclude value1="${Messages.SortedBy}" value2="To_Name,REVERSE" operator="notEquals" >
										    <img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
									    </ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude value1="${Messages.SortedBy}" value2="To_Name" operator="equals" >
										<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								   </ffi:cinclude>
								   <ffi:cinclude value1="${Messages.SortedBy}" value2="To_Name,REVERSE" operator="equals" >
									   <img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								   </ffi:cinclude>
							   </ffi:link></td>
							</tr>
						</table>
					</td>
					<td class="tbrd_tb" width="300">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td nowrap><span class="sectionsubhead"><s:text name="jsp.default_394"/></span></td>
								<td><ffi:link url="${SecurePath}messagesent.jsp?Messages.SortedBy=Subject${reverseSubject}">
								    <ffi:cinclude value1="${Messages.SortedBy}" value2="Subject" operator="notEquals" >
									    <ffi:cinclude value1="${Messages.SortedBy}" value2="Subject,REVERSE" operator="notEquals" >
										    <img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
									    </ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude value1="${Messages.SortedBy}" value2="Subject" operator="equals" >
										<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								   </ffi:cinclude>
								   <ffi:cinclude value1="${Messages.SortedBy}" value2="Subject,REVERSE" operator="equals" >
									   <img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								   </ffi:cinclude>
								</ffi:link></td>
							</tr>
						</table>
					</td>

					<td class="tbrd_tb" width="120">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><span class="sectionsubhead"><s:text name="jsp.message_6"/></span></td>
								<td><ffi:link url="${SecurePath}messagesent.jsp?Messages.SortedBy=Case_Num_Desc${reverseCase_Num_Desc}>">
									    <ffi:cinclude value1="${Messages.SortedBy}" value2="Case_Num_Desc" operator="notEquals" >
									    <ffi:cinclude value1="${Messages.SortedBy}" value2="Case_Num_Desc,REVERSE" operator="notEquals" >
										<img src="/cb/web/multilang/grafx/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
									    </ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude value1="${Messages.SortedBy}" value2="Case_Num_Desc" operator="equals" >
										<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								   </ffi:cinclude>
								   <ffi:cinclude value1="${Messages.SortedBy}" value2="Case_Num_Desc,REVERSE" operator="equals" >
									   <img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								   </ffi:cinclude>
								</ffi:link></td>
							</tr>
						</table>
					</td>


					<td class="tbrd_tb" width="120">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><span class="sectionsubhead"><s:text name="jsp.message_18"/></span></td>
								<td><ffi:link url="${SecurePath}messagesent.jsp?Messages.SortedBy=DateValue${reverseDateValue}">
									    <ffi:cinclude value1="${Messages.SortedBy}" value2="DateValue" operator="notEquals" >
									    <ffi:cinclude value1="${Messages.SortedBy}" value2="DateValue,REVERSE" operator="notEquals" >
										    <img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
									    </ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude value1="${Messages.SortedBy}" value2="DateValue" operator="equals" >
										<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								   </ffi:cinclude>
								   <ffi:cinclude value1="${Messages.SortedBy}" value2="DateValue,REVERSE" operator="equals" >
									   <img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="<s:text name="jsp.default_384"/>" width="24" height="8" border="0">
								   </ffi:cinclude>
								</ffi:link></td>
                <ffi:setProperty name="reverse${Messages.SortedBy}" value=""/>
							</tr>
						</table>
					</td>
					<td class="tbrd_trb" style="padding-right:20px" align="right" width="80">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><span class="sectionsubhead"><s:text name="jsp.default_163"/></span></td>
                                <td><input type="checkbox" onclick="selectCheckboxes(this)"></td>
                            </tr>
						</table>
					</td>
				</tr>
      <ffi:setProperty name="Messages" property="Filter" value="FROM_ID=${User.Id}"/>
      <ffi:cinclude value1="0" value2="${Messages.Size}" operator="equals">
	<tr>
		<td class="columndata ltrow" height="20" colspan="5" align="center"><s:text name="jsp.message_23"/></td>
	</tr>
</ffi:cinclude>
<ffi:cinclude value1="0" value2="${Messages.Size}" operator="notEquals">
<ffi:setProperty name="Math" property="Value1" value="3" />
<ffi:setProperty name="Math" property="Value2" value="1" />

<script type="text/javascript">
    var msgSize = <ffi:getProperty name="Messages" property="Size"/>;

    function selectCheckboxes(chkbox)
    {
        var counter = 1;

        for (i=0; i < msgSize; i++)
        {
            var msgChkbox = eval("document.frmMessages.chkbox" + counter++);
            msgChkbox.checked = chkbox.checked;
        }
    }

    function setMessageIDs()
    {
        var counter = 1;
        var selectedIDs = "";
        document.frmMessages.messageIDs.value = "";

        for (i=0; i < msgSize; i++)
        {
            var msgChkbox = eval("document.frmMessages.chkbox" + counter++);
            if (msgChkbox.checked)
            {
                if (selectedIDs.length > 0) selectedIDs = selectedIDs + ",";
                selectedIDs = selectedIDs + msgChkbox.value;
            }
        }

        if (selectedIDs.length > 0)
        {
            document.frmMessages.messageIDs.value = selectedIDs;
            return true;
        }

        return false;
    }
</script>

<% boolean toggle = false; int counter = 1; %>
<ffi:list collection="Messages" items="Message1">
<% toggle = !toggle; %>
<ffi:setProperty name="band" value='<%= toggle ? "ltrow" : "dkrow" %>'/>
<ffi:setProperty name="Compare" property="Value1" value="" />
<ffi:setProperty name="Compare" property="Value2" value="${Message1.ReadDate}" />
<ffi:setProperty name="Message1" property="DateFormat" value="${UserLocale.DateTimeFormat}" />
		<tr class="<ffi:getProperty name="band" encode="false"/>">

					<td width="120"><ffi:getProperty name="Message1" property="ToName"/></td>
					<td width="320"><ffi:link url="${SecurePath}messageviewsent.jsp?MessageID=${Message1.ID}"><ffi:getProperty name="Message1" property="Subject"/></ffi:link></td>
					<td width="110"><ffi:getProperty name="Message1" property="CaseNumDesc"/></td>
					<td width="120"><ffi:getProperty name="Message1" property="DateValue"/></td>
                    <td align="right" style="padding-right:21px">
                        <input type="checkbox" name="chkbox<%= counter++ %>" value="<ffi:getProperty name='Message1' property='ID'/>">
                    </td>
        </tr>

<ffi:setProperty name="Math" property="Value2" value="${Math.Subtract}" />
</ffi:list>
</ffi:cinclude>

				<tr>
					<td class="tbrd_full" colspan="5"><span class="columndata"><br></span></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/cb/web/multilang/grafx/sechdr_btm.gif" alt="" width="750" height="11" border="0"></td>
				</tr>
				<tr>
					<td colspan="5"><img src="/cb/web/multilang/grafx/spacer.gif" height="11" width="1" border="0"></td>
				</tr>
				<tr>
					<td colspan="5" valign="middle" align="center">
                        <a href="newmessage.jsp"><img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/button_sendnew.gif" alt="" width="105" height="14" border="0"></a>
                        <a href="messages.jsp"><img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/button_inbox.gif" alt="" width="43" height="14" border="0" hspace="6"></a>
                        <ffi:cinclude value1="${Messages.Size}" value2="0" operator="notEquals">
                        <input type="image" src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/button_deletemessages.gif" alt="Delete Selected Messages" onclick="return setMessageIDs();"/>
                        </ffi:cinclude>
                    </td>
				</tr>
			</table>
			<br>
            <input type="hidden" name="messageIDs"/>
            </form>
        </div>

<ffi:setProperty name="BackURL" value="${SecurePath}messagesent.jsp"/>
<ffi:setProperty name="MessageListURL" value="${SecurePath}messagesent.jsp"/>
