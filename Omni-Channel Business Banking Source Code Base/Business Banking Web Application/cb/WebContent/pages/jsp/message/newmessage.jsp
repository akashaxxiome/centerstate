<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<script language="JavaScript" type="text/javascript">
<!--

// This function is used to check event of enter key.
/*
$("#frmSendMessage").keypress(function(e) {  
	if (e.which == 13) {  
		$("#sendNewMessageSubmit").click();
		return false;
	}   
});	
*/
	$(document).ready(function() {
		ns.common.renderRichTextEditor('sendMemo');
	});
	
//--></script>


<ffi:help id="messages_newmessage"/>
	<div align="left">
      <s:form id="frmSendMessage" namespace="/pages/jsp/message" validate="false" action="SendMessageAction" method="post" name="frmSendMessage" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<table width="100%" border="0" cellspacing="3" cellpadding="3" class="greenClayBackground tableData">
				<tr class="hidden">
					<td align="right" nowrap class="greenClayBackground" ><span class="sectionsubhead"><s:text name="jsp.message_9"/></span></td>
					<td colspan="3"><ffi:getProperty name="User" property="FullName"/></td>
					<s:hidden name="messageModel.from" value="%{#session.User.id}"></s:hidden>					
				</tr>

				<%-- <tr>
					<td align="right" nowrap class="greenClayBackground" ><span class="sectionsubhead"><s:text name="message.messageForm.date"/></span></td>
					<td colspan="3"><s:property value="currentDate"/></td>
				</tr>	 --%>			
				<tr>
					<td align="right" nowrap class="greenClayBackground" width="10%"><span class="sectionsubhead"><s:text name="jsp.default_430"/></span></td>
					<td width="800px">						
						<select id="new_message_topics" class="ui-widget-content ui-corner-all"  name="messageModel.to" size="1">
							<ffi:list collection="MessageQueues" items="Queue1">
								<option value="<ffi:getProperty name="Queue1" property="QueueID"/>" <ffi:cinclude value1="${MessageModel.To}" value2="${Queue1.QueueID}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="Queue1" property="QueueName"/></option>
							</ffi:list>
						</select><span id="toError"></span>
						<script>
							$('#new_message_topics').selectmenu({width:300});
						</script>
						<span class="sectionsubhead floatRight">
							<s:text name="message.messageForm.date"/>
							<s:property value="currentDate"/>
						</span>
					</td>
					<td></td>
				</tr>
				<tr>
					<td align="right" nowrap class="greenClayBackground"><span class="sectionsubhead"><s:text name="jsp.default_516"/></span><span class="required">*</span></td>
					<td colspan="2">
					<s:textfield cssClass="txtbox ui-widget-content ui-corner-all" id="subject" name="messageModel.subject" size="86" maxlength="100"></s:textfield>					
					</td>
				</tr>
				<tr>
					<td></td>
					<td><span id="subjectError"></span></td>
				</tr>
				<tr>
					<td align="right" nowrap class="greenClayBackground" style="vertical-align:top;"><span class="sectionsubhead"><s:text name="jsp.message_10"/></span><span class="required">*</span></td>
					<td colspan="2" class="greenClayBackground ui-widget-content ui-corner-all"  style="border-width: 0;"><s:textarea cssClass="txtbox ui-widget-content ui-corner-all" id="sendMemo" name="messageModel.memo" rows="9" cols="120" wrap="physical"></s:textarea>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><span id="memoError"></span></td>
				</tr>
				<tr>
					<td></td>
					<td align="center" colspan="2">
						<span class="required">* <s:text name="jsp.default_240"/></span>
					</td>
				</tr>				
				<tr>
					<td></td>
					<td align="center" nowrap class="greenClayBackground" colspan="2">
							<sj:a button="true" id="clearMessageBoxButton" onClickTopics="messages.clearMessageBoxArea"><s:text name="jsp.default.label.clear"/></sj:a>
							<sj:a   id="cancelNewMessageSendButton"
									button="true"
									summaryDivId="summary" 
									buttonType="cancel"
									onClickTopics="showSummary,cancelForm"
					        ><s:text name="jsp.default_82"/></sj:a>
				            <sj:a
								id="sendNewMessageSubmit"
								formIds="frmSendMessage"
	                            targets="resultmessage"
	                            button="true"
	                            validate="true"
	                            validateFunction="customValidation"	                           
	                            onBeforeTopics="beforeSendingNewMessage"
	                            onCompleteTopics="sendingNewMessageComplete"
								onErrorTopics="errorSendingNewMessage"
	                            onSuccessTopics="successSendingNewMessage"
	                        ><s:text name="jsp.default_378"/></sj:a>
					</td>
				</tr>
			</table>
		</s:form>
		<br>
	</div>
