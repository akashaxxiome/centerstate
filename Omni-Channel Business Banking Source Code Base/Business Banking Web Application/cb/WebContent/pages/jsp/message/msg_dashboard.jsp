<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-message"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_121" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/homeSubmenuDropdown.jsp" />
    	</div>
    	 <!-- dashboard items listing for messages -->
        <div id="dbMessagesSummary" class="dashboardSummaryHolderDiv">
        
        	<s:url id="goBackSummaryUrl" value="/pages/jsp/message/msg_summary.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
        
        	<sj:a id="goBackSummaryLink" href="%{goBackSummaryUrl}"
				button="true" cssClass="summaryLabelCls" targets="summary"
				title="%{getText('jsp.billpay_go_back_summary')}"
				onClickTopics="openMessageSummary">
				  	<s:text name="jsp.billpay_summary" />
			</sj:a>
			
        	<s:url id="newMessageUrl" value="/pages/jsp/message/SendMessageAction_init.action">
		    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
			</s:url>
		    <sj:a id="newMessageLink" href="%{newMessageUrl}" targets="newMessageFormDiv" button="true" 
		    	title="%{getText('jsp.message_27.1')}"
		    	onClickTopics="beforeLoadMessageForm"
		    	onSuccessTopics="successLoadMessageForm" 
		    	onErrorTopics="errorLoadMessageForm"
		    	onCompleteTopics="loadMessageFormComplete"> 
			  	<s:text name="jsp.message_27" />
		    </sj:a>
        </div>
</div>
   <%--  <div id="appdashboard-left" class="formLayout">--%>
	
	    

        <s:url id="deleteMessageUrl" value="/pages/jsp/message/DeleteMessagesAction_confirm.action">
        <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
		</s:url>
	    <sj:a id="deleteInboxMessageLink" href="%{deleteMessageUrl}" targets="DeleteMessagesConfirmDialogID" button="true" buttonIcon="ui-icon-trash" 
	    	title="%{getText('jsp.message_29')}"
	    	formIds="frmDelMsgFromInbox"
	    	onClickTopics="beforeDeletingMessagesConfirm" 
	    	onSuccessTopics="successDeletingMessagesConfirm" 
	    	onErrorTopics="errorDeletingMessagesConfirm"
	    	onCompleteTopics="DeletingMessagesConfirmComplete"
	    	cssClass="hidden"
	    	cssStyle="display:none"> 
		  	&nbsp;<s:text name="jsp.default_162" />
	    </sj:a>
	    <sj:a id="deleteSentMessageLink" href="%{deleteMessageUrl}" targets="DeleteMessagesConfirmDialogID" button="true" buttonIcon="ui-icon-trash" 
	    	title="%{getText('jsp.message_28')}"
	    	formIds="frmDelMsgFromSentbox"
	    	onClickTopics="beforeDeletingSentMessagesConfirm" 
	    	onSuccessTopics="successDeletingSentMessagesConfirm" 
	    	onErrorTopics="errorDeletingSentMessagesConfirm"
	    	onCompleteTopics="DeletingSentMessagesConfirmComplete"
	    	cssClass="hidden"
	    	cssStyle="display:none"
	    	> 
		  	<s:text name="jsp.default_162" />
		  	
	    </sj:a>
<!-- 
    </div>  -->
	<%-- <span class="ui-helper-clearfix">&nbsp;</span>	 --%>
	
<ffi:object id="DeleteMessages" name="com.ffusion.tasks.messages.DeleteMessages" scope="session"/>
<% session.setAttribute("FFIDeleteMessages", session.getAttribute("DeleteMessages")); %>

	<sj:dialog id="DeleteMessagesConfirmDialogID" cssClass="home_messageDialog deleteMsgDialogCls" title="%{getText('jsp.message_8')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
	</sj:dialog>
<script>
$("#goBackSummaryLink").find("span").addClass("dashboardSelectedItem");
</script>