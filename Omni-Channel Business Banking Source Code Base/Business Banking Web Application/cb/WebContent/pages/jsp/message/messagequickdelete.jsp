<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<%-- <ffi:setProperty name="DeleteMessages" property="MessageIDs" value="${messageIDs}"/>
<ffi:setProperty name="DeleteMessages" property="Process" value="false"/>
<ffi:process name="DeleteMessages"/>

<ffi:setProperty name="DeleteMessages" property="MarkAsRead" value="true"/>
<ffi:setProperty name="DeleteMessages" property="Process" value="true"/> --%>


	<div align="center">
	<span id="formError"></span>
    <s:form id="quickDeleteMessageForm" namespace="/pages/jsp/message" validate="false" action="DeleteMessagesAction" method="post" name="quickDeleteMessageForm" theme="simple">
	   		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>	   		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>

                    <td colspan="4" align="center" width="750">
                        <span class="sectionhead"><font color="red">
                        	<s:if test="%{messages.size == 1}" >
                        		<s:text name="jsp.message_2"/>
                        	</s:if>      
                            <s:if test="%{messages.size != 1}" >
                            	<s:text name="jsp.message_1"/>
                            </s:if>
                        </font></span>
                    </td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
			</table>
			<div class="paneWrapper">
		  		<div class="paneInnerWrapper">
		  			<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">	
						<tr>
							<td width="20%">
		                        <span class="sectionsubhead"><s:text name="jsp.default_216"/></span>
							</td>
							<td width="55%">
								<span class="sectionsubhead"><s:text name="jsp.default_394"/></span>
							</td>
		                    <td width="10%">
								<span class="sectionsubhead"><s:text name="jsp.message_6"/></span>
							</td>
		                    <td width="15%">
								<span class="sectionsubhead"><s:text name="jsp.default_339"/></span>
							</td>
						</tr>
					</table>
					</div>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">	
		                <% boolean toggle = false; %>
		                <ffi:list collection="Messages" items="Message">
		                	<s:hidden name="selectedMsgIDs" value="%{#attr.Message.ID}"></s:hidden>             	
		                <% toggle = !toggle; %>
		                <ffi:setProperty name="band" value='<%= toggle ? "ltrow" : "dkrow" %>'/>
		                <ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateTimeFormat}" />
		
		                <tr class="<ffi:getProperty name="band" encode="false"/>">
							<td class="columndata" width="20%">&nbsp;&nbsp;<ffi:getProperty name="Message" property="FromName"/></td>
							<td class="columndata" width="55%"><ffi:getProperty name="Message" property="Subject"/></td>
		                    <td class="columndata" width="10%"><ffi:getProperty name="Message" property="CaseNum"/></td>
		                    <td class="columndata" width="15%"><ffi:getProperty name="Message" property="DateValue"/></td>
						</tr>
		                </ffi:list>
					</table>
					</div>
				</div>	
					<table>
						<tr>
							<td colspan="4" height="30">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="4" valign="middle" align="center" class="ui-widget-header customDialogFooter">
								<%-- 
								<input class="submitbutton" type="button" value="CANCEL" onClick="location.replace('<ffi:getProperty name="SecurePath"/>messages.jsp')">&nbsp;
								<input class="submitbutton" type="button" value="DELETE" onClick="location.replace('<ffi:getProperty name="SecureServletPath"/>DeleteMessages')">
								--%>
		
									<sj:a   button="true" 
											onClickTopics="cancelQuickDeleteMsg"
							        ><s:text name="jsp.default_82"/></sj:a>
		
						            <sj:a
										id="quickDeleteMessageSubmit"
										formIds="quickDeleteMessageForm"
			                            targets="quickMessagingResponse" 
			                            button="true" 
			                            onBeforeTopics="beforeQuickDeletingMessage"
			                            onCompleteTopics="quickDeletingMessageComplete"
										onErrorTopics="errorQuickDeletingMessage"
			                            onSuccessTopics="successQuickDeletingMessage"
			                        ><s:text name="jsp.default_162"/></sj:a>
		
							</td>
						</tr>
				</table>		
		</s:form>
		<br>
	</div>
