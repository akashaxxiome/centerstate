<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>


<ffi:help id="messages_messageview" />
<ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateTimeFormat}" />
<div class="leftPaneWrapper">
		<div class="leftPaneInnerWrapper">
			<div class="header">Message Summary</div>
			<div class="leftPaneInnerBox summaryBlock">
				<div style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.message_9"/></span>
					<span class="inlineSection floatleft labelValue" id="fromInViewMsg"><ffi:getProperty name="Message" property="FromName"/></span>
				</div>
				<div class="marginTop10 clearBoth floatleft" style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.message_16"/></span>
					<span class="inlineSection floatleft labelValue" id="ReceivedInViewMsg"><ffi:getProperty name="Message" property="DateValue"/></span>
				</div>
			</div>
		</div>
</div>
<div class="rightPaneWrapper w71">
	<div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header" id="subjectInViewMsg"><s:text name="jsp.default_516"/> <ffi:getProperty name="Message" property="Subject"/></div>
   		<div class="paneContentWrapper">
   			<ffi:list collection="Message.MemoLines" items="line">                                                
				<textarea class="testClass1" style="display: none;">
					<ffi:getProperty name="line" encodeLeadingSpaces="true"/>
				</textarea>
			</ffi:list>
			<span id="dataSpan1"></span>  
			
			<div class="btn-row">
<ffi:cinclude value1="${Message.TypeValue}" value2="<%= Integer.toString(com.ffusion.efs.adapters.profile.constants.ProfileDefines.MESSAGE_TYPE_GLOBAL_MESSAGE)%>" operator="notEquals" >

							<sj:a   id="doneMessageViewButton"
									href="#"
									button="true" 
									onClickTopics="cancelForm"
					        ><s:text name="jsp.default_175"/></sj:a>

						    <s:url id="replyMessageUrl" value="/pages/jsp/message/SendReplyAction_init.action" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							    <s:param name="MessageID" value="%{#attr.Message.ID}"></s:param>		
							</s:url>
				            <sj:a
								id="loadReplyFormSubmit"
								href="%{replyMessageUrl}"
	                            targets="messageReplyWrapper" 
	                            button="true" 
	                            onBeforeTopics="beforeLoadingReplyForm"
	                            onCompleteTopics="loadingReplyFormComplete"
								onErrorTopics="errorLoadingReplyForm"
	                            onSuccessTopics="successLoadingReplyForm"
	                        ><s:text name="jsp.message_17"/></sj:a>

						    <s:url id="deleteMessageUrl" value="/pages/jsp/message/DeleteMessagesAction_confirm.action" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
							    <s:param name="selectedMsgIDs" value="%{#attr.Message.ID}"></s:param>		
							</s:url>
				            <sj:a
								id="deleteMessageConfirmSubmit"
								href="%{deleteMessageUrl}"
	                            targets="DeleteMessagesConfirmDialogID" 
	                            button="true" 
	                            onBeforeTopics="beforeDeletingMessageConfirm"
	                            onCompleteTopics="deletingMessageConfirmComplete"
								onErrorTopics="errorDeletingMessageConfirm"
	                            onSuccessTopics="successDeletingMessageConfirm"
	                        ><s:text name="jsp.default_162"/></sj:a>


</ffi:cinclude>
<ffi:cinclude value1="${Message.TypeValue}" value2="<%= Integer.toString(com.ffusion.efs.adapters.profile.constants.ProfileDefines.MESSAGE_TYPE_GLOBAL_MESSAGE)%>" operator="equals" >
							<sj:a   button="true" 
									onClickTopics="cancelForm"
					        ><s:text name="jsp.default_175"/></sj:a>
						    <s:url id="deleteMessageUrl" value="/pages/jsp/message/DeleteMessagesAction_confirm.action" escapeAmp="false">
							    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
							    <s:param name="selectedMsgIDs" value="%{#attr.Message.ID}"></s:param>		
							</s:url>
				            <sj:a
								id="deleteMessageConfirmSubmit"
								href="%{deleteMessageUrl}"
	                            targets="DeleteMessagesConfirmDialogID" 
	                            button="true" 
	                            onBeforeTopics="beforeDeletingMessageConfirm"
	                            onCompleteTopics="deletingMessageConfirmComplete"
								onErrorTopics="errorDeletingMessageConfirm"
	                            onSuccessTopics="successDeletingMessageConfirm"
	                        ><s:text name="jsp.default_162"/></sj:a>
</ffi:cinclude>
</div>	
   		</div>
 </div>
</div>  
</div>
				
<script>
	$(document).ready(function() {
		var gmMsgStr = "";
		$(".testClass1").each(function(){
			gmMsgStr += $(this).text()+"<br>";
		});
		$("#dataSpan1").html(gmMsgStr);
	})
</script>	