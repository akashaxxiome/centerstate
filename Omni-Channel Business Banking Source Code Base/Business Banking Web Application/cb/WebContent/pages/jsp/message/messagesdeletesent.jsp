<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object id="DeleteMessages" name="com.ffusion.tasks.messages.DeleteMessages" scope="session"/>
    <ffi:setProperty name="DeleteMessages" property="MessageIDs" value="${messageIDs}"/>
<ffi:process name="DeleteMessages"/>

<ffi:setProperty name="DeleteMessages" property="MarkAsRead" value="true"/>
<ffi:setProperty name="DeleteMessages" property="SuccessURL" value="${SecurePath}messagesent.jsp"/>
<ffi:setProperty name="DeleteMessages" property="Process" value="true"/>

		<div align="center">

			<table width="656" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="4" width="750" background="/cb/web/multilang/grafx/sechdr_blank.gif" class="sectiontitle">&nbsp;&nbsp; &gt; <s:text name="jsp.message_11"/></td>
				</tr>
				<tr>
					<td class="tbrd_full" colspan="4" align="center" width="750">
                        <span class="sectionhead"><font color="red">
                            <ffi:cinclude value1="${DeleteMessages.Messages.Size}" value2="1">
                                <s:text name="jsp.message_2"/>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${DeleteMessages.Messages.Size}" value2="1" operator="notEquals">
                                <s:text name="jsp.message_1"/>
                            </ffi:cinclude>
                        </font></span>
                    </td>
				</tr>
				<tr>
					<td class="tbrd_lb" valign="middle" width="150">
                        <img src="/cb/web/multilang/grafx/spacer.gif" height="11" width="1" border="0">
                        <span class="sectionsubhead"><s:text name="jsp.message_24"/></span>
					</td>
					<td class="tbrd_b" width="320">
						<span class="sectionsubhead"><s:text name="jsp.default_394"/></span>
					</td>
                    <td class="tbrd_b" width="110">
						<span class="sectionsubhead"><s:text name="jsp.message_6"/></span>
					</td>
                    <td class="tbrd_br">
						<span class="sectionsubhead"><s:text name="jsp.message_18"/></span>
					</td>
				</tr>

                <% boolean toggle = false; %>
                <ffi:list collection="DeleteMessages.Messages" items="Message">
                <% toggle = !toggle; %>
                <ffi:setProperty name="band" value='<%= toggle ? "ltrow" : "dkrow" %>'/>

                <tr class="<ffi:getProperty name="band" encode="false"/>">
					<td class="columndata" width="150">&nbsp;&nbsp;<ffi:getProperty name="Message" property="To"/></td>
					<td class="columndata" width="320"><ffi:getProperty name="Message" property="Subject"/></td>
                    <td class="columndata" width="110"><ffi:getProperty name="Message" property="CaseNum"/></td>
                    <td class="columndata"><ffi:getProperty name="Message" property="DateValue"/></td>
				</tr>
                </ffi:list>

                <tr>
					<td class="tbrd_full" colspan="4"><span class="columndata"><br></span></td>
				</tr>
				<tr>
					<td colspan="4" width="656"><img src="/cb/web/multilang/grafx/sechdr_btm.gif" alt="" width="750" height="11" border="0"></td>
				</tr>
				<tr>
					<td colspan="4"><img src="/cb/web/multilang/grafx/spacer.gif" height="11" width="1" border="0"></td>
				</tr>
				<tr>
					<td colspan="4" valign="middle" align="center">
						<input class="submitbutton" type="button" value="<s:text name="jsp.default_82"/>" onClick="location.replace('<ffi:getProperty name="SecurePath"/>messagesent.jsp')">&nbsp;
						<input class="submitbutton" type="button" value="<s:text name="jsp.default_162"/>" onClick="location.replace('<ffi:getProperty name="SecureServletPath"/>DeleteMessages')">
					</td>
				</tr>
			</table>
			<br>
		</div>
