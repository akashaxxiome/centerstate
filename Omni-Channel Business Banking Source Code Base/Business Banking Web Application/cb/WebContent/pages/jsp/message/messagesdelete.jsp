<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>




<!--<ffi:setProperty name="DeleteMessages" property="MarkAsRead" value="true"/>
<ffi:setProperty name="DeleteMessages" property="Process" value="true"/> -->

<ffi:help id="messages_messagesdelete" />
<s:form id="deleteMessagesForm" namespace="/pages/jsp/message" validate="false" action="DeleteMessagesAction" method="post" name="deleteMessagesForm" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

<div class="approvalDialogHt">
 <% boolean toggle = false; %>
<ffi:list collection="Messages" items="Message">

<div>
	<s:hidden name="selectedMsgIDs" value="%{#attr.Message.ID}"></s:hidden> 
<% toggle = !toggle; %>
<ffi:setProperty name="band" value='<%= toggle ? "ltrow" : "dkrow" %>'/>
<ffi:setProperty name="Message" property="DateFormat" value="${UserLocale.DateTimeFormat}" />
<span class="labelCls">
<ffi:cinclude value1="${MessageBox}" value2="Inbox" operator="equals">
	<s:text name="jsp.default_216"/>:
</ffi:cinclude>
<ffi:cinclude value1="${MessageBox}" value2="Inbox" operator="notEquals">
	<s:text name="jsp.message_24"/>:
</ffi:cinclude>
</span>
<span class="valueCls">
<ffi:cinclude value1="${MessageBox}" value2="Inbox" operator="equals">
	<ffi:getProperty name="Message" property="FromName"/>
</ffi:cinclude>
<ffi:cinclude value1="${MessageBox}" value2="Inbox" operator="notEquals">
	<ffi:getProperty name="Message" property="ToName"/>
</ffi:cinclude>
</span>
<span class="floatRight">
	<ffi:cinclude value1="${Message.From}" value2="${SecureUser.ProfileID}" operator="equals">
		<span class="labelCls"><s:text name="jsp.default_339.1"/>:</span>
	</ffi:cinclude>
	<ffi:cinclude value1="${Message.From}" value2="${SecureUser.ProfileID}" operator="notEquals">
		<span class="labelCls"><s:text name="jsp.default_339"/>:</span>
	</ffi:cinclude>
	<span class="valueCls"><ffi:getProperty name="Message" property="DateValue"/></span>
</span>
</div>
<div class="marginTop5">
	<span class="labelCls"><s:text name="jsp.default_394"/>:</span>
	<span class="valueCls"><ffi:getProperty name="Message" property="Subject" encode="true"/></span>
</div>
<div class="marginTop5 marginBottom10">
	<span class="labelCls"><s:text name="jsp.message_6"/>:</span>
	<span class="valueCls"><ffi:getProperty name="Message" property="CaseNum"/></span>
</div>

</ffi:list>
</div>
<div class="marginTop20">&nbsp;</div>
<div  class="ui-widget-header customDialogFooter">
<sj:a   button="true" onClickTopics="closeMessageDialog"><s:text name="jsp.default_82"/></sj:a>
<sj:a
	id="deleteMessageSubmit"
	formIds="deleteMessagesForm"
	targets="resultmessage" 
	button="true" 
	onBeforeTopics="beforeDeletingMessage"
	onCompleteTopics="deletingMessageComplete"
	onErrorTopics="errorDeletingMessage,closeMessageDialog"
	onSuccessTopics="successDeletingMessage"
><s:text name="jsp.default_162"/></sj:a>
</div>
</s:form>

