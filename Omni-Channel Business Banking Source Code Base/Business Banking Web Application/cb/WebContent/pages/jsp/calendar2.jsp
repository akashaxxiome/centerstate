<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.*" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ page import="com.ffusion.util.beans.DateTime" %>
<%@ page import="com.ffusion.tasks.smartcalendar.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.GregorianCalendar" %>
<%
	session.setAttribute("calUserDate", request.getParameter("calUserDate"));
	session.setAttribute("calTarget", request.getParameter("calTarget"));
	session.setAttribute("calForm", request.getParameter("calForm"));
	session.setAttribute("calDirection", request.getParameter("calDirection"));
	session.setAttribute("calDisplay", request.getParameter("calDisplay"));
	session.setAttribute("calOneDirectionOnly", request.getParameter("calOneDirectionOnly"));
	session.setAttribute("calTransactionType", request.getParameter("calTransactionType"));
	session.setAttribute("calBankRoutingNum", request.getParameter("calBankRoutingNum"));
	session.setAttribute("calClearTarget", request.getParameter("calClearTarget"));
	session.setAttribute("calClearTargetPart2", request.getParameter("calClearTargetPart2"));
	session.setAttribute("calStartYear", request.getParameter("calStartYear"));
	session.setAttribute("calStartMonth", request.getParameter("calStartMonth"));
	session.setAttribute("SECCode", request.getParameter("SECCode"));
	session.setAttribute("fromACHorTax", request.getParameter("fromACHorTax"));
    session.setAttribute("isAnnual", request.getParameter("isAnnual"));
    session.setAttribute("isQuarterly", request.getParameter("isQuarterly"));
	session.setAttribute("ACHCompanyID", request.getParameter("ACHCompanyID"));
	session.setAttribute("calMinDate", request.getParameter("calMinDate"));
	session.setAttribute("calMaxDate", request.getParameter("calMaxDate"));
%>

<%----------------------------- INSTRUCTIONS ON HOW TO USE -----------------------------
	This calendar has 3 display styles that are passed (usually) in the URL, as "calDisplay":
		-"default"
		-"show"
		-"select"
	You may specify the following directions using "calDirection":
		-"forward"
		-"back"
	You may also specify that the calendar is limited to one direction using "calOneDirectionOnly=true"
	Doing this restricts the user from browsing 1 or more months before the current date if the direction is forward.
	And it stops the user from browsing 1 or months after the current date if the direction is back.
	
	See example calling pages for other variables and their uses.
	
	The box that appears around the first available business day is only displayed when the calendar direction is
	forward, and the display isn't default. This is the only time when it would make sense to have the first-available-business-day
	box appear. It is also suggested that you limit the calendar to only on direction in this scenario.
	
	If you choose a non default display, then supposed "valid banking days" are taken from the calendar associated
	with the bank associated with the SecureUser in session as "SecureUser". 
	If you wish to explicitly specify a different bank, and thus calendar (maybe),
	to be used then you may pass in the bank's routing number using "calBankRoutingNum"
-------------------------------------------------------------------------------------------%>

<%-- NOTE: This JSP recieves a starting date and month in order to set the display.
The month must be passed in using base 1 (i.e. Jan = 1) The date defaults to the current date --%>

<%!
	// Creates an HTML representation of a Gregorian Calendar month
	//	year is the year to display
	//	month is the month to display with base 0 (meaning Jan = 0, Feb = 1, etc.)
	//	display is the display type of the calendar (either "default", "show", or "select")
	//	forward is true if the calendar direction is forward
	// This method also REQUIRES that a String representation of a boolean value called "firstBusinessDayDisplayed" be in the session
	// 	this value should be set to "false" with each page reload
	//	also, 2 session properties called "firstBusDayMonth" and "firstBusDayYear" must have initial values of "-1"
	String makeCal( int year, int month, String display, String transactionType, boolean forward, javax.servlet.http.HttpSession session, javax.servlet.http.HttpServletRequest request ) {
	
		//
		// Begin: Setup useful calendar information
		//

		// Determine format of full date display based on user's locale
		//	the full date display is used for inserting dates in editable text fields such as the one
		//	at the bottom of this JSP
		// Also remember the user's locale
		UserLocale userLocale = (UserLocale)session.getAttribute("UserLocale");
		Locale locale = userLocale.getLocale();
		SimpleDateFormat sdf = new SimpleDateFormat( userLocale.getDateFormat(), locale );

		// This is used to walk through the days of the current month
		DateTime cal = new DateTime( locale );
		// grab date information first
		int todaysDate = cal.get( Calendar.DATE );
		
		//Fixing for CR #558765
		//set the DAY OF MONTH to 1 before setting MONTH in case the original day is larger than the max days of new month
		//author: Bill Wang
		cal.set( Calendar.DAY_OF_MONTH, 1 );
		//end of fixing
		
		// also, decide if today belongs to the month that is going to be displayed
		//	and decide if today occurs in a month previous to the one being displayed
		boolean isTodayInDisplayMonth = ( ( year == cal.get(Calendar.YEAR) ) && ( month == cal.get(Calendar.MONTH) ) );
		boolean isTodayBeforeDisplayMonth = ( ( year > cal.get(Calendar.YEAR) ) || ( (year == cal.get(Calendar.YEAR)) && (month > cal.get(Calendar.MONTH)) ) );

		// start at the start month
		cal.set( Calendar.YEAR, year );
		cal.set( Calendar.MONTH, month );
		
		// find the number of days in this month
		int maxDaysInMonth = cal.getActualMaximum( Calendar.DATE );		
		
		// retrieve the month name
		cal.setFormat( "MMMMM" );
		String monthName = cal.toString();

		// retrieve the names of the days of the week
		 cal.setFormat( "E" );
		 // move to a week that will have all days of the week
		 cal.set( Calendar.WEEK_OF_MONTH, 2 );

		 int maxDayOfWeek = cal.getActualMaximum( Calendar.DAY_OF_WEEK );
		 cal.set( Calendar.DAY_OF_WEEK, cal.getActualMinimum( Calendar.DAY_OF_WEEK ) );
		 String[] daysOfWeekNames = new String[7];
		 
		 // loop through all the days in a week (storing names)
	 	 for ( int counter = 0; counter != 7; counter++ ) {
	 	 if( locale.getLanguage().equalsIgnoreCase("ZH") ) { // If language is chinese, only pick up the 3rd character in date of week.
	 	 	daysOfWeekNames[counter] = cal.toString().substring( 2, 3 );
	 	 } else {
			daysOfWeekNames[counter] = cal.toString().substring( 0, 2 );
			}
			
			// move forward a day only if there are days left in the week
			if ( counter != 6 ) {
				cal.add( Calendar.DAY_OF_WEEK, 1 );
			}
	 	 }
		
		// grab the BankingDaysRange from the request (from the struts action GetBankingDaysInRange)
		boolean [] bDaysInRange = null;
		if ( !display.equals("default") ) {
			bDaysInRange = (boolean[])request.getAttribute("BankingDaysRange");
		}

		// start at the first day of the month
		cal.set( Calendar.DATE , 1 );

		//
		// End: Setup useful calendar information
		//		


		// Our result (the xml calendar infos)
		StringBuffer result = new StringBuffer();

		result.append("<ul name='daysInfo'>");
		////////////////////////////////////////////////////
		// Start the XML for the actual days of the month
		////////////////////////////////////////////////////

		// this is a flag for determining if we have built HTML for the first available business day
		//	(or the day doesn't ocurr in this month)
		// false means we still need to build the HTML
		boolean firstBusinessDayDisplayed = Boolean.valueOf( (String)session.getAttribute( "firstBusinessDayDisplayed" ) ).booleanValue();
        boolean notBeforeToday = true;
        if (transactionType != null && "12".equals(transactionType))
            notBeforeToday = false;

		// assume the first business day has already been displayed if it's not in this month
		//	Note: we only need to worry about the first business day when the calendar direction is forward
		if ( !firstBusinessDayDisplayed && forward) {
			int firstBusDayMonth = Integer.parseInt( (String)session.getAttribute("firstBusDayMonth") );
			int firstBusDayYear = Integer.parseInt( (String)session.getAttribute("firstBusDayYear") );

			// if first bus day is not in this month
			//	(firstBusDay will be -1 if the first business day hasn't been found yet)
			if ( ( firstBusDayYear != -1 ) && ( ( firstBusDayYear != year ) || ( firstBusDayMonth != month ) ) ) {
				firstBusinessDayDisplayed = true;
			}
		}

		// loop through each day in the month - producing HTML
		for( int counter = 1; counter <= maxDaysInMonth; counter++ ){
			// move date to today
			cal.set( Calendar.DATE, counter );

			
			// true if today is a valid business day
			boolean validBusinessDay = false;

			if (bDaysInRange != null && bDaysInRange.length >= counter) {
				validBusinessDay = bDaysInRange[counter-1];
			}

			// any day previous to the current date is considered an invalid business day for the purposes of this calendar
			if ( notBeforeToday && !( (isTodayInDisplayMonth && (todaysDate <= cal.get(Calendar.DATE))) || isTodayBeforeDisplayMonth ) ) {
				validBusinessDay = false;
			}
			
			// but a box around the first available business day (if of course this is that day)
			if ( !firstBusinessDayDisplayed && validBusinessDay && forward) {

				//style += " isFirstBusDay='true'";

				// remember that we have now displayed the first available business day
				firstBusinessDayDisplayed = true;
				session.setAttribute( "firstBusinessDayDisplayed", "true" );
				// remember which month/year the first business day was in
				session.setAttribute( "firstBusDayMonth", cal.get( Calendar.MONTH ) + "" );
				session.setAttribute( "firstBusDayYear", cal.get( Calendar.YEAR ) + "" );
			}
			// build cell XML for current date (current date refers to the date we are building html for, not today's date)
			result.append("<li date='").append(counter).append("'>").append(validBusinessDay).append("</li>");
		}
		
		// close off HTML calendar
		result.append("</ul>");
		
		return result.toString();
	}


	String makeSimpleCal(int year,int  month,javax.servlet.http.HttpSession session){
		UserLocale userLocale = (UserLocale)session.getAttribute("UserLocale");
		Locale locale = userLocale.getLocale();
		SimpleDateFormat sdf = new SimpleDateFormat( userLocale.getDateFormat(), locale );

		
		DateTime cal = new DateTime( locale );		
		int todaysDate = cal.get( Calendar.DATE );		
		cal.set( Calendar.DAY_OF_MONTH, 1 );
		cal.set( Calendar.YEAR, year );
		cal.set( Calendar.MONTH, month );
		
		int maxDaysInMonth = cal.getActualMaximum( Calendar.DATE );		
		
		StringBuffer result = new StringBuffer();
		result.append("<ul name='daysInfo'>");
		for(int i=1;i<=maxDaysInMonth;i++){
			result.append("<li date='").append(i).append("'>").append("true").append("</li>");
		}
		// close off HTML calendar
		result.append("</ul>");
		//session.setAttribute( "removeLink", "both" );
		return result.toString();
	}
	
	// creates the HTML that will go in the td tag of a single day's cell
	//	value is the date to put in the cell
	private String getCellHTML( String date, boolean bottomTopPadding ) {
		if ( bottomTopPadding ) {
			return "<font style='font-size:1px'>&nbsp;<br/></font>" + date + "<font style='font-size:1px'><br/>&nbsp;</font>";
		} else {
			return date;
		}
	}
	
	// Returns true if we should process the GetBankingDaysInRange action
	//	which is done only if the display is "show" or "select"
	// If we do need to process, the necessary start and end Date objects are placed in the session
	//	year is the year to display
	//	month is the month to display with base 0 (meaning Jan = 0, Feb = 1, etc.)
	private boolean decideToProcessBankingDaysInRange( int year, int month, String display, javax.servlet.http.HttpSession session ) {
	
		if ( !display.equals("default") ) {
			// get locale
			Locale locale = ((UserLocale)session.getAttribute("UserLocale")).getLocale();

			// set start date
			Date startDate = new Date();
			startDate.setYear( year - 1900 );
			startDate.setMonth( month );
			startDate.setDate( 1 );
			session.setAttribute( ISCTask.SC_START_DATE, startDate );
			
			// make a calendar representing the given month
			GregorianCalendar cal = new GregorianCalendar( locale );
			
			//Fixing for CR #558765
			//set the DAY OF MONTH to 1 before setting MONTH in case the original day is larger than the max days of new month
			//author: Bill Wang
			cal.set( Calendar.DAY_OF_MONTH, 1 );
			//end of fixing
			
			cal.set( Calendar.YEAR, year );
			cal.set( Calendar.MONTH, month );
			
			// set end date
			Date endDate = new Date();
			endDate.setYear( year - 1900 );
			endDate.setMonth( month );
			endDate.setDate( cal.getActualMaximum( Calendar.DATE ) );
			session.setAttribute( ISCTask.SC_END_DATE, endDate );
			
			// set bank identifier
			com.ffusion.util.beans.BankIdentifier fi = new com.ffusion.util.beans.BankIdentifier( locale );
			fi.setBankDirectoryId( (String)session.getAttribute("SCBankRoutingNum") );
			session.setAttribute( com.ffusion.tasks.smartcalendar.ISCTask.SC_BANKIDENTIFIER, fi );
			
			return true;
		} else {
			return false;
		}
	}
%>
	
	<%-- Make copies of all passed in parameters --%>

	<% String strYear = "";		//optional (defaults to current year) %>
	 <ffi:setProperty name="tempVar" value="${calStartYear}"/>
	 <ffi:getProperty name="tempVar" assignTo="strYear"/>
	 <ffi:removeProperty name="calStartYear"/>

	<% String strMonth = "";	//optional (defaults to current month) %>
	 <ffi:setProperty name="tempVar" value="${calStartMonth}"/>
	 <ffi:getProperty name="tempVar" assignTo="strMonth"/>
	 <ffi:removeProperty name="calStartMonth"/>

	<% String strUserDate= "";	// cannot be overwritten -- passed in by calendar.jsp %>
	 <ffi:setProperty name="tempVar" value="${calUserDate}"/>
	 <ffi:getProperty name="tempVar" assignTo="strUserDate"/>

	 <%-- Determine the value of the user-editable date text field at the bottom of this page --%>
	 <ffi:cinclude value1="${DateTextField}" value2="" operator="equals">
		 <ffi:setProperty name="DateTextField" value="${calUserDate}"/>
	 </ffi:cinclude>
 	 <ffi:cinclude value1="${DateTextField}" value2="" operator="equals">
 	 	 <ffi:setProperty name="DateTextField" value="${UserLocale.DateFormat}"/>
 	 </ffi:cinclude>
	 <ffi:removeProperty name="calUserDate"/>
	
	<% String strTarget = null;		//required %>
 	 <ffi:setProperty name="tempVar" value="${calTarget}"/>
  	 <ffi:getProperty name="tempVar" assignTo="strTarget"/>
		<% strTarget = com.ffusion.util.HTMLUtil.encode(strTarget); %>
	
	<% String strForm = null;		//required %>
  	 <ffi:setProperty name="tempVar" value="${calForm}"/>
  	 <ffi:getProperty name="tempVar" assignTo="strForm"/>
	   <% strForm = com.ffusion.util.HTMLUtil.encode(strForm); %>
	
	 <ffi:cinclude value1="${calForm}" value2="frmPayBill">
 	 	 <%  strTarget = strTarget.replaceAll("&amp;", "&"); %>
 	 </ffi:cinclude>

	 <ffi:cinclude value1="${calForm}" value2="MultipleTransferNew">
 	 	 <%  strTarget = strTarget.replaceAll("&amp;", "&"); %>
 	 </ffi:cinclude>
 	 
	 <ffi:cinclude value1="${calForm}" value2="AddBatchForm">
 	 	 <%  strTarget = strTarget.replaceAll("&amp;", "&"); %>
 	 </ffi:cinclude> 	 
 	 
	<% String strDirection = null;	//optional (defaults to forward) %>
  	 <ffi:setProperty name="tempVar" value="${calDirection}"/>
  	 <ffi:removeProperty name="calDirection"/>
  	 <ffi:getProperty name="tempVar" assignTo="strDirection"/>
	<% strDirection = com.ffusion.util.HTMLUtil.encode(strDirection); %>
	
	<% String strDisplay = null;	//optional (defaults to "default") %>
	 <ffi:setProperty name="tempVar" value="default"/>
	 <ffi:cinclude value1="${calDisplay}" value2="" operator="notEquals">
	  	 <ffi:setProperty name="tempVar" value="${calDisplay}"/>
	 </ffi:cinclude>
  	 <ffi:removeProperty name="calDisplay"/>
  	 <ffi:getProperty name="tempVar" assignTo="strDisplay"/>
	<% strDisplay = com.ffusion.util.HTMLUtil.encode(strDisplay); %>

	<% String oneDirectionOnly = null;	//optional (defaults to "false") %>
	 <ffi:setProperty name="tempVar" value="false"/>
	 <ffi:cinclude value1="${calOneDirectionOnly}" value2="" operator="notEquals">
	  	 <ffi:setProperty name="tempVar" value="${calOneDirectionOnly}"/>
	 </ffi:cinclude>
  	 <ffi:removeProperty name="calOneDirectionOnly"/>
  	 <ffi:getProperty name="tempVar" assignTo="oneDirectionOnly"/>
	<% oneDirectionOnly = com.ffusion.util.HTMLUtil.encode(oneDirectionOnly); %>
	
	<% String strMinDate = null; %>
  	 <ffi:setProperty name="tempVar" value="${calMinDate}"/>
  	 <ffi:removeProperty name="calMinDate"/>
  	 <ffi:getProperty name="tempVar" assignTo="strMinDate"/>
	<% strMinDate = com.ffusion.util.HTMLUtil.encode(strMinDate); %>
	
	<% String strMaxDate = null; %>
  	 <ffi:setProperty name="tempVar" value="${calMaxDate}"/>
  	 <ffi:removeProperty name="calMaxDate"/>
  	 <ffi:getProperty name="tempVar" assignTo="strMaxDate"/>
	<% strMaxDate = com.ffusion.util.HTMLUtil.encode(strMaxDate); %>
	
	<% String strTransactionType = null;	//optional (task will default it to unknown type) %>
  	 <ffi:setProperty name="tempVar" value="${calTransactionType}"/>
  	 <ffi:removeProperty name="calTransactionType"/>
  	 <ffi:getProperty name="tempVar" assignTo="strTransactionType"/>
	<% strTransactionType = com.ffusion.util.HTMLUtil.encode(strTransactionType); %>

  	<% String fromACHorTax = null; //optional (required for calls from ACH Payments page only) %>
     <ffi:setProperty name="tempVar" value="${fromACHorTax}"/>
	 <ffi:removeProperty name="fromACHorTax"/>
     <ffi:getProperty name="tempVar" assignTo="fromACHorTax"/>

<% boolean isAnnual = false; //optional TAX PAYMENTS dates are Annual
String isAnnualString = "";%>
<ffi:setProperty name="tempVar" value="${isAnnual}"/>
   <ffi:removeProperty name="isAnnual"/>
<ffi:getProperty name="tempVar" assignTo="isAnnualString"/>
<% isAnnual = Boolean.valueOf(isAnnualString).booleanValue(); %>

<% boolean isQuarterly = false; //optional TAX PAYMENTS dates are Quarterly
String isQuarterlyString = "";%>
<ffi:setProperty name="tempVar" value="${isQuarterly}"/>
   <ffi:removeProperty name="isQuarterly"/>
<ffi:getProperty name="tempVar" assignTo="isQuarterlyString"/>
<% isQuarterly = Boolean.valueOf(isQuarterlyString).booleanValue(); %>

  	<% String ACHCompanyID = null; //optional (required for calls from ACH Payments page only) %>
     <ffi:setProperty name="tempVar" value="${ACHCompanyID}"/>
	 <ffi:removeProperty name="ACHCompanyID"/>
     <ffi:getProperty name="tempVar" assignTo="ACHCompanyID"/>

  	<% String SECCode = null; //optional (required for calls from ACH Payments page only) %>
     <ffi:setProperty name="tempVar" value="${SECCode}"/>
	 <ffi:removeProperty name="SECCode"/>
     <ffi:getProperty name="tempVar" assignTo="SECCode"/>
     
     <% String wireDest = null; //optional (required for calls from Wire Transactions page only) %>
     <ffi:setProperty name="tempVar" value="${wireDest}"/>
     	 <ffi:removeProperty name="wireDest"/>
     <ffi:getProperty name="tempVar" assignTo="wireDest"/>

	<% String SCBankRoutingNum = null; // optional (only if you want to use a different bank other than the one associated with the secure user ) %>
	 <ffi:setProperty name="SCBankRoutingNum" value="${calBankRoutingNum}"/>
	 <ffi:cinclude value1="${calBankRoutingNum}" value2="" operator="equals">
		<%-- Load the affiliate bank associated with the current user and then put its routing number in session --%>
		<ffi:object name="com.ffusion.tasks.affiliatebank.GetAffiliateBankByID" id="GetAffiliateBankByID"/>
		<ffi:setProperty name="GetAffiliateBankByID" property="AffiliateBankID" value="${SecureUser.AffiliateID}"/>
		<ffi:process name="GetAffiliateBankByID"/>
		<ffi:removeProperty name="GetAffiliateBankByID"/>

		<ffi:setProperty name="SCBankRoutingNum" value="${AffiliateBank.AffiliateRoutingNum}"/>
		<ffi:removeProperty name="AffiliateBank"/>
	 </ffi:cinclude>
	 <ffi:getProperty name="SCBankRoutingNum" assignTo="SCBankRoutingNum"/>
	<% SCBankRoutingNum = com.ffusion.util.HTMLUtil.encode(SCBankRoutingNum); %>
	 <ffi:removeProperty name="calBankRoutingNum"/>

	<ffi:removeProperty name="tempVar"/>

	<%-- Assume we have not yet marked the first available business day with a box yet --%>
	<ffi:setProperty name="firstBusinessDayDisplayed" value="false"/>
	<%-- Reset the time the first business day occurred only if told to do so.
			resetFirstBusDay is passed in the URL and is only false when the user clicks the forward/previous
			month links. We assume null or "" to mean true.
	--%>
	<ffi:cinclude value1="false" value2="${resetFirstBusDay}" operator="notEquals">
		<ffi:setProperty name="firstBusDayMonth" value="-1"/>
		<ffi:setProperty name="firstBusDayYear" value="-1"/>
	</ffi:cinclude>

	<ffi:removeProperty name="resetFirstBusDay"/>

<%
	// user's locale
	Locale locale = ((UserLocale)session.getAttribute("UserLocale")).getLocale();
	
	// used for date calculations (defualt value is today's date)
	GregorianCalendar cal = new GregorianCalendar( locale );
	
	//Fixing for CR #558765
	//set the DAY OF MONTH to 1 before setting MONTH in case the original day is larger than the max days of new month
	//author: Bill Wang
	cal.set( Calendar.DAY_OF_MONTH, 1 );
	//end of fixing
	
	// store today's date
	GregorianCalendar todayCal = new GregorianCalendar( locale );

	// See if the user had entered a date in the opening page's associated date text field
	//	If this is the case, then we want to default to that date
	if ( !"".equals( strUserDate ) ) {
		com.ffusion.util.beans.DateTime userDateTime = new com.ffusion.util.beans.DateTime( locale );
		userDateTime.setFormat( ((UserLocale)session.getAttribute("UserLocale")).getDateFormat() );
		userDateTime.setDate( strUserDate );
		cal.set( Calendar.YEAR, userDateTime.get( Calendar.YEAR ) );
		cal.set( Calendar.MONTH, userDateTime.get( Calendar.MONTH ) );
	}

	// if a month and year were passed in, use them
	if ( strYear != null && strYear.length() != 0 && strMonth != null && strMonth.length() != 0){
		cal.set( Calendar.YEAR, Integer.parseInt( strYear ) );
		cal.set( Calendar.MONTH, Integer.parseInt( strMonth )-1 );
	}
	
	/*
	if ( strDisplay.equals( "server" ) ) {
		com.ffusion.util.beans.DateTime userDateTime = new com.ffusion.util.beans.DateTime( locale );
		userDateTime.setFormat( ((UserLocale)session.getAttribute("UserLocale")).getDateFormat() );
		userDateTime.setDate( strUserDate );
		cal.set( Calendar.YEAR, userDateTime.get( Calendar.YEAR ) );
		cal.set( Calendar.MONTH, userDateTime.get( Calendar.MONTH ) );
	}*/

	
	// Now do some error checking on the date (which is either entered by the user or the calling page)
	//	If the date is beyond the selectable range then we default to today's date.
	boolean dateErrorFlag = false;
	
	// if direction is back and one direction only
	if ( strDirection != null && strDirection.length() != 0 && strDirection.equals("back") && oneDirectionOnly.equals("true") ) {
		//
		// flag an error if the month is before today's month
		//
		
		// if future year	
		if ( cal.get( Calendar.YEAR ) > todayCal.get( Calendar.YEAR ) ) {
			dateErrorFlag = true;
		}
		// if this year but previous month
		else if ( ( cal.get( Calendar.YEAR ) == todayCal.get( Calendar.YEAR ) ) && ( cal.get( Calendar.MONTH ) > todayCal.get( Calendar.MONTH ) ) ) {
			dateErrorFlag = true;
		}
	}
	// if direction is forward and one direction only
	else if ( oneDirectionOnly.equals("true") ) {
		//
		// flag an error if the month is before today's month
		//
		
		// if previous year	
		if ( cal.get( Calendar.YEAR ) < todayCal.get( Calendar.YEAR ) ) {
			dateErrorFlag = true;
		}
		// if this year but previous month
		else if ( ( cal.get( Calendar.YEAR ) == todayCal.get( Calendar.YEAR ) ) && ( cal.get( Calendar.MONTH ) < todayCal.get( Calendar.MONTH ) ) ) {
			dateErrorFlag = true;
		}
	}
	
	// default to todays date if there was a problem
	if ( dateErrorFlag ) {
		cal = new GregorianCalendar( locale );
		//Fixing for CR #558765
		//set the DAY OF MONTH to 1 before setting MONTH in case the original day is larger than the max days of new month
		//author: Bill Wang
		cal.set( Calendar.DAY_OF_MONTH, 1 );
		//end of fixing
	}
	
	// storage for the 2 months this jsp displays
	int year_1, year_2, month_1, month_2;
	
	// set the beginning date/month
	year_1 = cal.get( Calendar.YEAR );
	month_1 = cal.get( Calendar.MONTH );

	//
	// assume forward direction unless direction set to back
	//
	// if direction is back
	if ( strDirection != null && strDirection.length() != 0 && strDirection.equals("back") ) {
		// the first month/year is now the second
		year_2 = year_1;
		month_2 = month_1;
		
		// calculate the previous month/year
		cal.add( Calendar.MONTH, -1);
		year_1 = cal.get( Calendar.YEAR );
		month_1 = cal.get( Calendar.MONTH );
	}
	// if direction is forward
	else {
		cal.add( Calendar.MONTH, 1 );
		year_2 = cal.get( Calendar.YEAR );
		month_2 = cal.get( Calendar.MONTH );
	}
	
	/////////////////////////////////////////////////////////////////////
	// decide if we should display the next/previous month links
	//	the next month link is removed if the direction is back and the current day is already being displayed
	// 	and the previous month link is removed if the direction is forward and the current day is being displayed
	//  however, the links will NOT be removed if the display is set to default
	//
	// also calculate the beginning year and month should the user wish to browse forward or backwards one month
	/////////////////////////////////////////////////////////////////////
	
	// temp calendar for calculating previous and next months
	GregorianCalendar tempCal = new GregorianCalendar( locale );
	
	//Fixing for CR #558765
	//set the DAY OF MONTH to 1 before setting MONTH in case the original day is larger than the max days of new month
	//author: Bill Wang
	tempCal.set( Calendar.DAY_OF_MONTH, 1 );
	//end of fixing
	
	// remembers which link to remove
	String removeLink = "";
	// storage of which months to display should the user click the forward or previous month links
	int year_p, year_n, month_p, month_n;
	
	// true if the direction is forward (for later use when calling makeCal)
	boolean forward = false;
	
	// if direction is back
	if ( strDirection != null && strDirection.length() != 0 && strDirection.equals("back") ) {
		// if today is currently being displayed in the second month and we want one direction only
		if ( year_2 == todayCal.get( Calendar.YEAR ) && month_2 == todayCal.get( Calendar.MONTH ) && oneDirectionOnly.equals("true") ) {
			// remember to remove next month link
			removeLink = "next";
		}

		//
		// calculate the previous/next months to display
		//	we need to translate the month value to base 1 (for a page reload, since this JSP accepts dates in base 1 from other calling pages)
		//
		year_p = year_1;
		month_p = month_1 + 1;
		
		tempCal.set( Calendar.YEAR, year_2 );
		tempCal.set( Calendar.MONTH, month_2 );
		tempCal.add( Calendar.MONTH, 1 );

		year_n = tempCal.get( Calendar.YEAR );
		// we need to translate the month value to base 1
		month_n = tempCal.get( Calendar.MONTH ) + 1;
	}
	// if direction is forward
	else {
		forward = true;
		
		// if today is currently being displayed in the first month and we want one direction only
		if ( year_1 == todayCal.get( Calendar.YEAR ) && month_1 == todayCal.get( Calendar.MONTH ) && oneDirectionOnly.equals("true") ) {
			// remember to remove previous month link
			removeLink = "previous";
		}

		//		
		// calculate the previous/next months to display
		//	we need to translate the month value to base 1
		//
		year_n = year_2;
		month_n = month_2 + 1;
		
		tempCal.set( Calendar.YEAR, year_1 );
		tempCal.set( Calendar.MONTH, month_1 );
		tempCal.add( Calendar.MONTH, -1 );

		year_p = tempCal.get( Calendar.YEAR );
		//	we need to translate the month value to base 1
		month_p = tempCal.get( Calendar.MONTH ) + 1;
	}
	session.setAttribute( "removeLink", removeLink );
	
	// set the partial path and parameters for the next/previous month links
    String link_prev = "calendar2.jsp?calStartYear="+year_p+"&calStartMonth="+month_p+"&calForm="+strForm+"&calTarget="+strTarget+"&calDisplay="+strDisplay+"&calTransactionType="+strTransactionType+"&calDirection="+strDirection+"&resetFirstBusDay=false"+"&calOneDirectionOnly="+oneDirectionOnly+"&fromACHorTax="+fromACHorTax+"&ACHCompanyID="+ACHCompanyID+"&SECCode="+SECCode+"&isAnnual="+isAnnual+"&isQuarterly="+isQuarterly+"&wireDest="+wireDest+"&calBankRoutingNum="+SCBankRoutingNum+"&DateTextField="+session.getAttribute("DateTextField");
    String link_next = "calendar2.jsp?calStartYear="+year_n+"&calStartMonth="+month_n+"&calForm="+strForm+"&calTarget="+strTarget+"&calDisplay="+strDisplay+"&calTransactionType="+strTransactionType+"&calDirection="+strDirection+"&resetFirstBusDay=false"+"&calOneDirectionOnly="+oneDirectionOnly+"&fromACHorTax="+fromACHorTax+"&ACHCompanyID="+ACHCompanyID+"&SECCode="+SECCode+"&isAnnual="+isAnnual+"&isQuarterly="+isQuarterly+"&wireDest="+wireDest+"&calBankRoutingNum="+SCBankRoutingNum+"&DateTextField="+session.getAttribute("DateTextField");

	
	// BEGIN FOR 80 ADDED
	if ( strDirection != null && strDirection.length() != 0 && strDirection.equals("back") ) {
		// the first month/year is now the second
		year_1 = year_2;
		month_1 = month_2;
	}
%>
<div>
<ul name="calInfo">
<li name="display"><%= strDisplay %></li>
<li name="year"><%= year_1 %></li>
<li name="month"><%= month_1 + 1 %></li>
<li name="today"><%= (year_1 == todayCal.get( Calendar.YEAR ) && month_1 == todayCal.get( Calendar.MONTH ) ) ? todayCal.get(Calendar.DATE) : 1 %></li>
<li name="curDate"></li>
<li name="removeLink"><ffi:getProperty name="removeLink"/></li>
<li name="minDate"><%= strMinDate %></li>
<li name="maxDate"><%= strMaxDate %></li>
<% if(strDisplay.equals("server")){%>
	<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
	<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateFormat}"/>
	<ffi:process name="GetCurrentDate"/>
	<li name="serverDate"><ffi:getProperty name="GetCurrentDate" property="Date" /></li>		
<% } %>
</ul>
<%
if(strDisplay.equals("server")){
	out.println( makeSimpleCal(year_1, month_1,session));
} else if(!strDisplay.equals("default")) {
	if (decideToProcessBankingDaysInRange(year_1, month_1, strDisplay, session)) {	%>
		<s:action namespace="/pages/common" name="getBankingDaysInRange">
			<s:param name="TransactionType" value="%{#attr.strTransactionType}"/>
			<s:param name="ACHCompanyId" value="%{#attr.ACHCompanyID}"/>
			<s:param name="SECCode" value="%{#attr.SECCode}"/>
			<s:param name="WireDest" value="%{#attr.wireDest}"/>
			<s:if test="#attr.isQuarterlyString">
				<s:param name="IsQuarterly" value="true"/>
			</s:if>
			<s:if test="#attr.isAnnual">
				<s:param name="isAnnualString" value="true"/>
			</s:if>
		</s:action>
	<%
	}
	out.println(makeCal(year_1, month_1, strDisplay, strTransactionType, forward, session, request));
}
%></div>
<ffi:removeProperty name="SCBankRoutingNum"/>
<ffi:removeProperty name="DateTextField"/>