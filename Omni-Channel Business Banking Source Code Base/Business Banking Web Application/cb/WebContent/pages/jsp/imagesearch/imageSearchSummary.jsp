<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="imageSummaryContent">  
		<div id="imageSearchHolder" class="floatLeft marginTop10">
			<div class="portlet-header">
				 <div class="searchHeaderCls">
					<span class="searchPanelToggleAreaCls" onclick="$('.quickSearchAreaCls').slideToggle();">
						<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
					</span>
					<div class="summaryGridTitleHolder">
						<span id="selectedGridTab" class="selectedTabLbl marginRight10">
							<s:text name="jsp.imageSearch.accountImagesPortlet.header"/>
						</span>
					</div>
				</div> 
			 </div>
			 <div class="portlet-content marginTop20">
		     		<%-- <s:include value="/pages/jsp/imagesearch/image_search_form.jsp" /> --%>
		     		<div id="querybord">
		     			<s:include value="/pages/jsp/imagesearch/image_search_form.jsp" />
					</div> 
					<div id="imageArchiveGrid" class="marginTop10" >
						<s:include value="/pages/jsp/imagesearch/images_grid.jsp" />
					</div>
					<div name="carouselContainer"></div>
			</div> 
			<div id="imageSearch-menu" class="portletHeaderMenuContainer" style="display:none;">
				<ul class="portletHeaderMenu">
					<li><a href='#' onclick="ns.common.showDetailsHelp('imageSearchHolder')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
				</ul>
			</div> 
		</div>
		
		<div id="offsetSummary" class="marginTop10 hidden">
				<s:include value="/pages/jsp/imagesearch/images_offset_grid.jsp" />
		</div>
	</div>
	
<script type="text/javascript">
	// moved here from imagesearch/index.jsp
	ns.common.initializePortlet('imageSearchHolder');
	
	$(document).ready(function(){
	   //style for select boxes
    	$("#SIAccountID").selectmenu({width: 300});
    	$("#SITransType").selectmenu({width: 150});
    	
    	
    	//Default dates
		var fromDate = $("#StartDateSessionValue").val();
		var toDate = $("#EndDateSessionValue").val();
		$("#SIPostingDateFrom").val(fromDate);
		$("#SIPostingDateTo").val(toDate);	
	});
</script>	
	
	