<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>


<%-- Gets default date range values --%>
<ffi:object id="GetDatesFromDateRange" name="com.ffusion.tasks.util.GetDatesFromDateRange" scope="session"/>
<ffi:setProperty name="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.LAST_60_DAYS %>"/>
<ffi:setProperty name="GetDatesFromDateRange" property="DateRangeValue" value="${DateRangeValue}"/>
<ffi:process name="GetDatesFromDateRange"/>

<%-- Retrieve the latest account summary values from banksim and update the accounts in session with those latest values --%>
<ffi:object id="GetAccountSummaries" name="com.ffusion.tasks.banking.GetSummariesForAccountDate" scope="session"/>
<ffi:setProperty name="GetAccountSummaries" property="UpdateAccountBalances" value="true"/>
<ffi:setProperty name="GetAccountSummaries" property="SummariesName" value="LatestSummaries"/>
<ffi:process name="GetAccountSummaries"/>
<ffi:removeProperty name="GetAccountSummaries"/>
<ffi:removeProperty name="LatestSummaries"/>

<%-- For remembering values entered that are invalid or out of range and refresh accordingly --%>


<ffi:cinclude value1="${BackURL}" value2="${SecurePath}imagesearch.jsp" operator="notEquals">
	<ffi:setProperty name="tempURL" value="${BackURL}"/>
</ffi:cinclude>
<ffi:setProperty name="canSetBackURL" value="TRUE"/>

<%-- Task for deleting already displayed images from IRB --%>
<ffi:object name="com.ffusion.tasks.checkimaging.DeleteImageResults" id="DeleteImageResults" />
<ffi:setProperty name="DeleteImageResults" property="DeleteOnlyAvailableImageFlag" value="true" />
<ffi:setProperty name="DeleteImageResults" property="Collection" value="CheckedImages" />
<ffi:process name="DeleteImageResults"/>
<ffi:cinclude value1="${DeleteImageResults.Error}" value2="0" operator="notEquals">
	<ffi:setProperty name="canSetBackURL" value="FALSE"/>
</ffi:cinclude>

<%-- Task for building image request --%>
<ffi:object name="com.ffusion.beans.checkimaging.ImageRequest" id="FFIImageRequest" scope="session"/>

<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="FFISearchImage" scope="session"/>
<ffi:setProperty name="FFISearchImage" property="Init" value="true"/>
<ffi:process name="FFISearchImage"/>

<ffi:cinclude value1="${SearchImage.Error}" value2="0" operator="notEquals">
	<ffi:setProperty name="canSetBackURL" value="FALSE"/>
</ffi:cinclude>
<ffi:setProperty name="FFISearchImage" property="SuccessURL" value="DO_NOT_FORWARD" />
<ffi:setProperty name="FFISearchImage" property="Locale" value="${UserLocale.Locale}" />
<ffi:setProperty name="FFISearchImage" property="DateFormat" value="${UserLocale.DateFormat}" />
<ffi:setProperty name="FFISearchImage" property="MinimumFields" value="0" />
<ffi:setProperty name="FFISearchImage" property="Validate" value="MINIMUMFIELDS,POSTINGDATETO,POSTINGDATEFROM,ACCOUNTID" />
<ffi:setProperty name="FFISearchImage" property="VerifyFormat" value="ACCOUNTID,POSTINGDATEFROM,POSTINGDATETO,AMOUNTFROM,AMOUNTTO,CHECKNUMBERFROM,CHECKNUMBERTO" />

<ffi:object name="com.ffusion.tasks.checkimaging.GetImageIndex" id="FFIGetImageIndex" scope="session"/>

<%-- Task for extracting selected images from collection to display --%>
<ffi:object name="com.ffusion.tasks.checkimaging.GetCheckedImages" id="FFIGetCheckedImages" scope="session"/>


<%-- Task for getting images from the collection of checked images:mahesh --%>
<ffi:object name="com.ffusion.tasks.checkimaging.GetImages" id="FFIGetImages" scope="session"/>



<%-- Task for logging image fee details --%>
<ffi:object name="com.ffusion.tasks.checkimaging.LogFee" id="LogFee" scope="session"/>

<ffi:cinclude value1="${canSetBackURL}" value2="TRUE" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}imagesearch.jsp"/>
</ffi:cinclude>
<ffi:removeProperty name="canSetBackURL"/>

<%-- Flag variables --%>
<ffi:setProperty name="get-image-flag" value="true"/>
<ffi:setProperty name="previously-requested-images-sorting-flag" value="false" />
<ffi:setProperty name="image-search-results-sorting-flag" value="false" />

<style>
 .imgSrcErrorMsg{display:block !important;}
</style>

<div class="searchDashboardRenderCls quickSearchAreaCls" id="imageSeachBox" style="float:none !important;margin-top:0 !important;">   		
<div id="quicksearchcriteriaDiv" >	
<s:form id="formImageSearch" action="ImageSearchAction" namespace="/pages/jsp/account" method="post" name="formImageSearch" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<s:hidden id="StartDateSessionValue" name="StartDateSessionValue" value="%{#session.GetDatesFromDateRange.StartDate}"/>
	<s:hidden id="EndDateSessionValue" name="EndDateSessionValue" value="%{#session.GetDatesFromDateRange.EndDate}"/>					
	
	<div class="acntDashboard_masterItemHolder paddingLeft10">
		<div class="acntDashboard_itemHolder marginTop10">
			<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default_21"/>
			<span class="required">*</span>
			</span>
			
			<!-- POPULATING ACCOUNT NUMBERS IN LIST BOX -->
	         <select class="form_elements_nowidth" name="accountID" id="SIAccountID" onChange="ns.common.adjustSelectMenuWidth('SIAccountID')">
					<option value="" ><s:text name="jsp.default_375"/></option>
	            <ffi:setProperty name="OldFilter" value="${BankingAccounts.Filter}" />
				<!-- Only show Core accounts in the list box. -->
				<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND" />
				<ffi:list collection="BankingAccounts" items="Account1">
				   <ffi:setProperty name="Account1" property="locale" value="${UserLocale.Locale}" />	
	               <option value="<ffi:getProperty name="Account1" property="ID"/>" ><ffi:getProperty name="Account1" property="ConsumerMenuDisplayText"/></option>
	
	            </ffi:list>
	            <ffi:setProperty name="BankingAccounts" property="Filter" value="${OldFilter}" />
				<ffi:removeProperty name="OldFilter" />
	
	         </select>
	         <span id="accountIDError" class="errorLabel imgSrcErrorMsg"></span>
		</div>
		<div class="acntDashboard_itemHolder marginTop10"><!-- Check -->
			<span class="sectionsubhead dashboardLabelMargin">
				<s:text name="jsp.default_439"/>
			</span>
			<select class="form_elements_nowidth" name="transType" size="1" id="SITransType">
	            <option value=""><s:text name="jsp.account.transactionTypeAll"/></option>
	            <option value="C"><s:text name="jsp.imageSearch.quickSearch.transType.deposit"/></option>
	            <option value="D"><s:text name="jsp.imageSearch.quickSearch.transType.withdrawal"/></option>
	         </select>
	         <span class="errorLabel imgSrcErrorMsg"></span>
		</div>
		<div class="acntDashboard_itemHolder marginTop10 "><!-- from date -->
			<span class="sectionsubhead dashboardLabelMargin">
			<s:text name="jsp.default_528"/>&nbsp;(<s:text name="jsp.default_216"/>)<span class="required">*</span></span>
			<sj:datepicker title="Choose to date" value="" id="SIPostingDateFrom"  name="postingDateFrom" label="To Date" size="10" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
      		<%-- <br> --%>
      		<span id="postingDateFromError" class="errorLabel imgSrcErrorMsg"></span>
		</div>
		
		<div class="acntDashboard_itemHolder marginRight15 marginTop10"><!-- to date -->
			<span class="sectionsubhead dashboardLabelMargin">
			<s:text name="jsp.default_528"/>&nbsp;(<s:text name="jsp.default_text_to"/>)<span class="required">*</span></span>
			<sj:datepicker title="Choose to date" value="" id="SIPostingDateTo" name="postingDateTo" label="To Date" size="10" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
			<%-- <br> --%>
			<span id="postingDateToError" class="errorLabel imgSrcErrorMsg"></span>
		</div>
		
		<div class="acntDashboard_itemHolder marginRight15 marginTop10"><!-- Amount from -->
			<span class="sectionsubhead dashboardLabelMargin" style="display:block;">
				<s:text name="jsp.default_45"/>
			</span>
			<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_from' />"
			name="amountFrom" id="amountFrom" size="10" maxlength="10" />
	      	<span id="amountFromError" class="errorLabel"></span>
	   		
	   		<!-- Amount to -->  	
	      	<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_423.1' />"
			name="amountTo" id="amountTo" size="10" maxlength="10" />
	      	<span id="amountToError" class="errorLabel imgSrcErrorMsg"></span>
		</div>
		
		<div class="acntDashboard_itemHolder marginTop10"><!-- Reference -->
			<span class="sectionsubhead dashboardLabelMargin">
				<s:text name="jsp.imageSearch.quickSearch.checkNoLabel"/>
			</span>
			<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_from' />"
			name="checkNumberFrom" id="checkNumberFromError" size="10" maxlength="10" />
	      	<span id="checkNumberFromError" class="errorLabel"></span>
	     	
	     	<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_423.1' />"
			name="checkNumberTo" id="checkNumberTo" size="10" maxlength="10" />
	      	<span id="checkNumberToError" class="errorLabel imgSrcErrorMsg"></span>
		</div>
		
		<div class="acntDashboard_itemHolder marginTop10 ">
            <span class="sectionsubhead dashboardLabelMargin">&nbsp;</span>
            <sj:a id="searchImagesButton" formIds="formImageSearch"	targets="targetDiv"	button="true" validate="true" validateFunction="customValidation" onBeforeTopics="beforeImageSearch" onCompleteTopics="reloadArchiveImagesGrid">
				<s:text name="jsp.default_6"/>
			</sj:a>
			<span class="errorLabel imgSrcErrorMsg"></span>
         </div> 
        
	</div>
	
</s:form>	
 </div>
</div>