<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<script type="text/javascript" src="<s:url value='/web/js/imagesearch/imagesearch%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/imagesearch/pendingimages%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/jcarousellite_1.0.1%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/common/checkimage%{#session.minVersion}.js'/>"></script>

<%-- Remove objects from session --%>
<ffi:removeProperty name="IMAGE_RESULTS"/>

<ffi:object name="com.ffusion.tasks.checkimaging.DeleteImageRequests" id="FFIDeleteImageRequests" scope="session"/>
<%-- Task for retrieving all previously requested images i.e, pending images --%>
<ffi:object name="com.ffusion.tasks.checkimaging.GetPendingImages" id="FFIGetPendingImages" scope="session"/>
<ffi:process name="FFIGetPendingImages" />

<div id="desktop" align="left">
	<!-- result -->
	<%-- <div id="operationresult">
		<div id="resultmessage"><s:text name="jsp.default_498"/></div>
	</div>	

	<div id="appdashboard">
		<s:include value="dashboard.jsp" />
		</br>
	</div>
	
	<div id="summary">
		<div id="offsetSummary">
			<s:include value="/pages/jsp/imagesearch/images_offset_grid.jsp" />
		</div>
		<br>
		<s:include value="/pages/jsp/imagesearch/images_grid.jsp" />
		<div name="carouselContainer"></div>
	</div>

	<div id="pendingImagesSummary" class="summary">
	</div> --%>
	
	<div id="deleteDialogContainer"></div>
	
	<div id="appdashboard" class="dashboardUiCls"> 
		<s:include value="/pages/jsp/imagesearch/dashboard.jsp" />
	</div>	
	<div id="imageSearchSummary"> 
		<ffi:cinclude value1="${summaryToLoad}" value2="RequestedImageSearch"	operator="notEquals">
			<s:include value="/pages/jsp/imagesearch/imageSearchSummary.jsp" />
		</ffi:cinclude>
	</div>	
	
	<div id="pendingImagesSummary" class="summary marginTop10">
		<ffi:cinclude value1="${summaryToLoad}" value2="RequestedImageSearch"	operator="equals">
			<s:include value="/pages/jsp/imagesearch/pending_images_grid.jsp" />
		</ffi:cinclude>
	</div>
		
</div>

<script type="text/javascript">
	/* $('#appdashboard').pane({
		title: js_dashboard,
		minmax: true,
		close: false
	}); */
	/*
	moved to imagesearch/imageSearchSummary.jsp
	ns.common.initializePortlet('imageSearchHolder');
	*/
	
	$(document).ready(function(){
		ns.imagesearch.showImageSearchDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	});
</script>