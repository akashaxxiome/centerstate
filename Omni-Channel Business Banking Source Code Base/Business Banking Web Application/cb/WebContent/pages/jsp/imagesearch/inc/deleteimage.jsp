<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<sj:dialog id="deleteImageDialog" title="%{getText('jsp.imageSearch.imageDelete.dialotTitle')}" autoOpen="false" cssClass="staticContentDialog">
	    <div>
	    	<span class="langSelectionSeperator" style="display:block; margin-top:10px;"><s:text name="jsp.imageSearch.imageDeleteText"/></span>
			<div align="center" class="ui-widget-header customDialogFooter">
		    	<sj:a 	id="cancelDeleteImageBtn"	button="true">&nbsp;<s:text name='jsp.default_82'/></sj:a>	    
				<sj:a 	id="deleteImageBtn"	button="true">&nbsp;<s:text name='jsp.default_162'/></sj:a>	    	    
		    </div>
	    </div>		                        	    
</sj:dialog>
