<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<%-- param is always prop name like getImageHandle so specify param as ImageHandle and not imageHandle --%>
	<ffi:setGridURL grid="GRID_accountImages" name="ViewURL" url="/cb/pages/jsp/account/GetImagesAction_viewImage.action?module=DEPOSIT&imageID={0}" parm0="ImageHandle"/>
	<ffi:setGridURL grid="GRID_accountImages" name="ViewOffsetURL" url="/cb/pages/jsp/account/GetOffsetImagesAction.action?module=OFFSET&imageID={0}" parm0="ImageHandle"/>
																		
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/account/GetArchivedImagesAction.action?GridURLs=GRID_accountImages" URLEncrypt="true"/>
	
	<s:url id="accountImagesSummaryURL" value="%{#session.tempURL}" escapeAmp="false"/>
	
<style>
 /* the checkbox issue - overriding the style from 10px to 0 */
 .ui-jqgrid .ui-jqgrid-htable th.ui-th-column:first-child div{padding-left:0;} 
</style>	
<div id="accountImagesPortlet" >
	<ffi:help id="account_imageSearch_archievedImages" />
	     
	<sjg:grid  
		id="accountImagesSummaryID"  
		sortable="true" 
		dataType="json"  
		href="%{accountImagesSummaryURL}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		navigator="true"
	    navigatorAdd="false"
	    navigatorDelete="false"
	    navigatorEdit="false"
	    navigatorRefresh="false"
	    navigatorSearch="false"
	    navigatorView="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"		
		multiselect = "true"
		multiboxonly = "true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,imagesGridOnComplete"
		onSelectRowTopics="onSelectRow"
		onSelectAllTopics="onSelectAllRows"
		> 
              
		
		<%-- name is grid model field name which it is mapped to and index is sorting index on grid which filtered list will sort on --%>		
		<sjg:gridColumn name="imageHandle" index="imageHandle" title="%{getText('jsp.imageSearch.gridColumnHeader.imageHandle')}" sortable="false" width="100" hidden="true" hidedlg="true"/> 
		<sjg:gridColumn name="postingDate" index="posting_date" title="%{getText('jsp.imageSearch.gridColumnHeader.postingDate')}" sortable="true" width="65"/>
		<sjg:gridColumn name="drcr" index="dr_cr" title="%{getText('jsp.imageSearch.gridColumnHeader.DR_CR')}" sortable="true" width="65" formatter="ns.imagesearch.debitCreditFormatter"/>		
		<sjg:gridColumn name="amount" index="amount" title="%{getText('jsp.default_43')}" sortable="true" width="80"/>
		<sjg:gridColumn name="maskedAccountID" index="account_id" title="%{getText('jsp.default_15')}" sortable="true" width="80"/>
		<sjg:gridColumn name="checkNumber" index="check_number" title="%{getText('jsp.default_92')}" sortable="true" width="80" formatter="ns.imagesearch.checkNumberFormatter"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_27')}" search="false" sortable="false"  hidden="true" hidedlg="true" cssClass="__gridActionColumn" formatter="ns.imagesearch.viewImageLinkFormatter" width="30"/>				
	</sjg:grid>
</div>
	<script>
		
	</script>