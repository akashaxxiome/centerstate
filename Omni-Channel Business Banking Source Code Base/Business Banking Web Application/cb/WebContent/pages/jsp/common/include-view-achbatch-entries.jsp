<%@ page import="com.ffusion.beans.ach.ACHPayee,
				 com.ffusion.beans.SecureUser,
				 com.ffusion.beans.ach.ACHEntry"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants,java.util.*,com.ffusion.beans.ach.*" %>
<%-- ----------------------------------------
	this file is used to display the entries portion for the following files:
		achbatchdelete.jsp
		achbatchview.jsp
		achbatch-reverse.jsp
	if you make changes, test with all of the files
	it requires being inside of a form named ViewForm
	It also requires "ACHBatch" in the session
--------------------------------------------- --%>

<%
	boolean showDiscretionaryData = true;
	boolean showParticipantID = false; //check to see for what SEC Codes, Participant ID should be shown
	boolean checkSerNum = false;
	String classCode = "";
	String sortAction = "";
	boolean isView = false;
	boolean isDelete = false;
	boolean isReverse = false;
	boolean isApproval = false;
	boolean isTemplate = false;
    boolean showExport = false;
	int spanCols = 10;
	String returnPage = "achpayments.jsp";
	if (session.getAttribute("ReverseACHBatch") != null) {
		isReverse = true;
		sortAction = "achbatch-reverse.jsp";
		spanCols = 11;
    } else if (session.getAttribute("ViewMultipleBatchTemplate") != null) {
        isTemplate = true;
        showExport = false;         // don't show export because DATE is wrong
        isDelete = true;
		sortAction = "achbatchdelete.jsp";
		returnPage = "achmultitemplateaddedit.jsp";
    } else if (session.getAttribute("DeleteACHBatch") != null) {
		isDelete = true;
		sortAction = "achbatchdelete.jsp";
	} else if (session.getAttribute("ViewTemplateBatch") != null) {
		isTemplate = true;
		isView = true;
		sortAction = "achbatchview.jsp";
		returnPage = "achtemplates.jsp";
	} else if (session.getAttribute("ach_details_post_jsp") != null) {
		isView = true;
        showExport = true;
		returnPage = ( String )session.getAttribute( "ach_details_post_jsp" );
		sortAction = "achbatchview.jsp";
	} else {
		isView = true;
        showExport = true;
		sortAction = "achbatchview.jsp";
	}
	
	String viewPage = ( String )session.getAttribute( "FullPagesPath" ) + sortAction;
	
	if( session.getAttribute( "ApprovalsViewTransactionDetails" ) != null ) {
		isApproval = true;
		sortAction = ( String )session.getAttribute( "FullPagesPath" ) + 
			     ( String )session.getAttribute( "approval_view_ach_jsp" );
		
		viewPage = sortAction;
        returnPage = ( String )session.getAttribute( "ach_details_post_jsp" );
        }
%>
<ffi:cinclude value1="${ACHBatch.TemplateScope}" value2="" operator="notEquals">
    <ffi:cinclude value1="${ACHBatch.TemplateID}" value2="" operator="equals">
        <% isTemplate = true; %>
    </ffi:cinclude>
</ffi:cinclude>
<ffi:getProperty name="ACHBatch" property="StandardEntryClassCode" assignTo="classCode" />
<%
	if (classCode.equalsIgnoreCase("WEB") ||
        classCode.equalsIgnoreCase("TEL") ||
		classCode.equalsIgnoreCase("IAT"))
		showDiscretionaryData = false;
	if (classCode.equalsIgnoreCase("ARC") ||
		classCode.equalsIgnoreCase("BOC") ||
		classCode.equalsIgnoreCase("POP") ||
		classCode.equalsIgnoreCase("XCK") ||
		classCode.equalsIgnoreCase("RCK"))
		checkSerNum = true;
	if ((classCode.equals("CIE")) ||
		(classCode.equals("CCD")) ||
		(classCode.equals("CTX")) ||
		(classCode.equals("WEB")) ||
		(classCode.equals("PPD")) ||
		(classCode.equals("TEL"))) {
			showParticipantID = true;
		}
%>

<% if ((isView || isReverse) && !isApproval && !isTemplate) { 		// only display transaction history for View
%>	
		<ffi:object name="com.ffusion.tasks.ach.GetAuditHistory" id="GetAuditHistory" scope="session"/>
		<ffi:setProperty name='GetAuditHistory' property='BeanSessionName' value='ACHBatch'/>
		<ffi:setProperty name="GetAuditHistory" property="AuditLogSessionName" value="TransactionHistory" />
	<ffi:process name="GetAuditHistory"/>
<% }
    if (isView && showExport) { %>
	<ffi:object name="com.ffusion.tasks.ach.ExportACHBatch" id="ExportACHBatch" scope="session"/>
		<ffi:setProperty name='ExportACHBatch' property='BatchName' value='ACHBatch'/>
<%
	} 
%>

<%
if( request.getParameter("ACHEntries.ToggleSortedBy") != null && session.getAttribute("ACHEntries") != null)
{
	((ACHEntries)session.getAttribute("ACHEntries")).setToggleSortedBy(request.getParameter("ACHEntries.ToggleSortedBy"));
}
%>
<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="NoSort" value='<img src="${sort_off_gif}" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="${sort_asc_gif}" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="${sort_desc_gif}" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="ACHEntries"/>
<ffi:process name="SortImage"/>

		<script language="JavaScript">
<!--
function doSort(SortURLParams)
{
	$.ajax({
		url:    SortURLParams ,
		type: "post",
		data:	$("#ViewACHForm").serialize() + "&CSRF_TOKEN=<ffi:getProperty name="CSRF_TOKEN"/>",
		success: function(data){ 
				$("#ACHViewLayer").parent().html(data);
		}
	}); 
}
function doSearch()
{
	$.ajax({
		url:    '<%= viewPage %>' , 
		type: "post",
		data:	$("#ViewACHForm").serialize() + "&DoSearch=true&SearchString=" + $('#SearchString').val() + "&CSRF_TOKEN=<ffi:getProperty name="CSRF_TOKEN"/>",
		success: function(data){ 
				$("#ACHViewLayer").parent().html(data);
		}
	}); 
    return false;
}
function gotoPage(pageNumber)
{
	$.ajax({
		url:    "<%= viewPage %>" , 
		type:	"post",
		data:	$("#ViewACHForm").serialize() + "&GoToPage=true&pageNumber=" + pageNumber + "&CSRF_TOKEN=<ffi:getProperty name="CSRF_TOKEN"/>",
		success: function(data){ 
				$("#ACHViewLayer").parent().html(data);
		}
	}); 
    return false;
}

// -->--</script>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="tableAlerternateRowColor marginTop20">
<%-- adjust how many need to exist before the Search happens in csil.xml, ACHEntryDisplaySize --%>
<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount"/>
<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%= com.ffusion.tasks.GetDisplayCount.ACHENTRY %>"/>
<ffi:setProperty name="GetDisplayCount" property="DisplayCountName" value="achentryDisplaySize"/>
<ffi:process name="GetDisplayCount"/>
<ffi:removeProperty name="GetDisplayCount"/>
<%
    if (((com.ffusion.beans.ach.ACHEntries)session.getAttribute("ACHEntries")).nonFilteredSize() > Integer.parseInt((String)session.getAttribute("achentryDisplaySize")))
    {
        %>
            <ffi:setProperty name="ACHEntries" property="FilteredPageSize" value="${achentryDisplaySize}"/>
            <%-- repeat the last search OR do ALL search with paging --%>
            <ffi:setProperty name="ACHEntries" property="FilterPaging" value="paged"/>
            <ffi:cinclude value1="${ACHEntries.IsPageFiltered}" value2="true" operator="notEquals" >
                <ffi:setProperty name="ACHEntries" property="Filter" value="${ACHEntries.Filter}"/>
            </ffi:cinclude>
        <%
    }
	    if (request.getParameter("NextPage") != null )	// if they changed the SEC code, need to re-test default effective date
    	{
            String currentPage = ""+(Integer.valueOf(request.getParameter("NextPage")) + 1);
            %>
            <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="<%=currentPage%>"/>
            <ffi:setProperty name="ACHEntries" property="Filter" value="${ACHEntries.Filter}"/>
            <%
    	}

	    if (request.getParameter("PreviousPage") != null )	// if they changed the SEC code, need to re-test default effective date
    	{
            String currentPage = ""+(Integer.valueOf(request.getParameter("PreviousPage")) - 1);
            %>
            <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="<%=currentPage%>"/>
            <ffi:setProperty name="ACHEntries" property="Filter" value="${ACHEntries.Filter}"/>
            <%
    	}

	    if (request.getParameter("GoToPage") != null )	// if they changed the SEC code, need to re-test default effective date
    	{
            String currentPage = request.getParameter("pageNumber");
            %>
            <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="<%=currentPage%>"/>
            <ffi:setProperty name="ACHEntries" property="Filter" value="${ACHEntries.Filter}"/>
            <%
    	}
	    if (request.getParameter("DoSearch") != null )	// if they changed the SEC code, need to re-test default effective date
    	{
            String SearchString = request.getParameter("SearchString");
            if (SearchString == null)
                SearchString = "";
            %>
            <ffi:setProperty name="SearchString" value="<%=SearchString%>"/> 
            <ffi:cinclude value1="${SearchString}" value2="" operator="notEquals">
                <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="0"/>
                <ffi:setProperty name="ACHEntries" property="Filter" value="PAYEE_NAME**${SearchString},PAYEE_NICKNAME**${SearchString},PAYEE_ACCOUNTNUMBER**${SearchString},PAYEE_ROUTINGNUM**${SearchString},PAYEE_USERACCOUNTNUMBER**${SearchString},AMOUNT**${SearchString}"/>
            </ffi:cinclude>
            <ffi:cinclude value1="${SearchString}" value2="" operator="equals">
                <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="0"/>
                <ffi:setProperty name="ACHEntries" property="Filter" value="All"/>
            </ffi:cinclude>
            <%
    	}
%>
<ffi:cinclude value1="${ACHEntries.IsPaged}" value2="true">
									<tr>

		<td colspan="8">
		<table width="100%" border="0" cellspacing="0" cellpadding="1" class="tdWithPadding">
		<tr>
	    <td class="sectionhead" width="6%">
            &nbsp;
	    </td>

		<td class="sectionsubhead" valign="center" width="35%">
            <input class="txtbox" type="text" id="SearchString" name="SearchString" value="<ffi:getProperty name="SearchString" />" size="25">
            &nbsp;
            <input class="submitbutton" type="button" value="<s:text name="jsp.default_373"/>" onClick="doSearch();return false;">
            <br>
            <s:text name="jsp.default_515"/><ffi:getProperty name="ACHEntries" property="FilteredSize"/>
        </td>
		<td class="sectionsubhead" width="59%">
        <ffi:cinclude value1="${ACHEntries.MaximumPageNumber}" value2="0" operator="notEquals">
            <input class="submitbutton" type="button" <ffi:cinclude value1="false" value2="${ACHEntries.HasPrevPage}">disabled</ffi:cinclude> value="PREV PAGE" onClick="return gotoPage(<ffi:getProperty name="ACHEntries" property="CurrentPageNumber"/>-1);">
            &nbsp;
            <input class="submitbutton" type="button" <ffi:cinclude value1="false" value2="${ACHEntries.HasNextPage}">disabled</ffi:cinclude> value="NEXT PAGE" onClick="return gotoPage(<ffi:getProperty name="ACHEntries" property="CurrentPageNumber"/>+1)">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <s:text name="jsp.common_112"/> <ffi:getProperty name="ACHEntries" property="CurrentPageNumber"/> <s:text name="jsp.common_106"/> <ffi:getProperty name="ACHEntries" property="MaximumPageNumber"/>
            &nbsp;&nbsp;
            <s:text name="jsp.default_490"/>
            <select class="txtbox" name="pageNumber" onchange="gotoPage(this.value)">
<ffi:object id="Paging" name="com.ffusion.beans.util.Paging" scope="session"/>
<ffi:setProperty name="Paging" property="Pages" value="${ACHEntries.MaximumPageNumber}" />
<ffi:list collection="Paging" items="PageNum">
            <option value='<ffi:getProperty name="PageNum"/>' <ffi:cinclude value1="${ACHEntries.CurrentPageNumber}" value2="${PageNum}" >selected</ffi:cinclude> > <ffi:getProperty name="PageNum"/></option>
</ffi:list>
            </select>
        </ffi:cinclude>
        <ffi:cinclude value1="${ACHEntries.MaximumPageNumber}" value2="0" operator="equals">
            <s:text name="jsp.default_478"/>
        </ffi:cinclude>

		</td>
        </tr>
		</table>
		</td>
		</tr>
</ffi:cinclude>



    <tr class="resetTableRowBGColor">
    <% if (isReverse) { %>
	    <td class="columndata">
		    <span class="sectionsubhead"><s:text name="jsp.default_505"/></span>
	    </td>
    <% } %>
    <td id="viewAchBatchParticipantLabel" class="columndata">
    <ffi:setProperty name="sortAction" value="<%=sortAction%>"/>
    <%
	if (!classCode.equalsIgnoreCase("XCK")) {
    %>
	    <span class=""><s:text name="jsp.default_502"/></span>
	    <ffi:setProperty name="SortImage" property="Compare" value="PAYEENAME,ID"/>
        <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=PAYEENAME,ID" URLEncrypt="true"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
    <% } else { %>
		<span class=""><s:text name="jsp.default_492"/></span>
		<ffi:setProperty name="SortImage" property="Compare" value="ITEMRESEARCHNUMBER,ID"/>
        <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=ITEMRESEARCHNUMBER,ID" URLEncrypt="true"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	<% } %>
	</td>
		<% if (showParticipantID) { %>
			<td><s:text name="jsp.default_503"/>
			<ffi:setProperty name="SortImage" property="Compare" value="USERACCOUNTNUMBER,ID"/>
            <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=USERACCOUNTNUMBER,ID" URLEncrypt="true"/>
            <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
		<% } %>
		<td><s:text name="jsp.default_476"/>
		<ffi:setProperty name="SortImage" property="Compare" value="ROUTINGNUM,ID"/>
        <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=ROUTINGNUM,ID" URLEncrypt="true"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
		<td id="viewAchBatchAccountLabel"><s:text name="jsp.default_16"/>
		<ffi:setProperty name="SortImage" property="Compare" value="ACCOUNTNUMBER,ID"/>
        <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=ACCOUNTNUMBER,ID" URLEncrypt="true"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
		<td id="viewAchAccountTypeLabel"><s:text name="jsp.default_20"/>
		<ffi:setProperty name="SortImage" property="Compare" value="ACCOUNTTYPE,ID"/>
        <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=ACCOUNTTYPE,ID" URLEncrypt="true"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	<td id="viewAchBatchCheckSerialNumberLabel" class="columndata">
	<% if (checkSerNum) { %>
		<span class=""><s:text name="jsp.default_479"/></span>
		<ffi:setProperty name="SortImage" property="Compare" value="CheckSerialNumber,ID"/>
        <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=CheckSerialNumber,ID" URLEncrypt="true"/>
	    <a onclick="k('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	<% } %>
	&nbsp;</td>
	<td id="viewAchBatchDDLabel" class="columndata">
	<% if (showDiscretionaryData) { %>
		<span class=""><s:text name="jsp.default_483"/></span>
		<ffi:setProperty name="SortImage" property="Compare" value="DISCRETIONARYDATA,ID"/>
        <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=DISCRETIONARYDATA,ID" URLEncrypt="true"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	<% } %>
	&nbsp;</td>
	<td id="viewAchAmountLabel" class="columndata">
		<span class=""><s:text name="jsp.default_43"/></span>
		<ffi:setProperty name="SortImage" property="Compare" value="AMOUNT,ID"/>
        <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=AMOUNT,ID" URLEncrypt="true"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	</td>
	<td id="viewAchBatchTypeLabel" class="columndata">
		<span class=""><s:text name="jsp.default_444"/></span>
		<ffi:setProperty name="SortImage" property="Compare" value="TYPE,ID"/>
        <ffi:setProperty name="SortURL" value="${sortAction}?ACHEntries.ToggleSortedBy=TYPE,ID" URLEncrypt="true"/>
	    <a onclick="doSort('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	</td> 
</tr>
   <% int idCount=0; %>

    <ffi:list collection="ACHEntries" items="ACHEntry">
	<tr>
	
	<% boolean canReverse = true;
	boolean isSecure = false;       // should secure information be shown?
	String daStatus = "";
	String styleColor = "";

	// For Dual Approval - to get payee id to fetch DAItem status
	ACHEntry entry = (ACHEntry)request.getAttribute("ACHEntry");
	ACHPayee payee = entry.getAchPayee();
		
	if (payee != null && payee.getDisplayAsSecurePayeeValue() == true)
		isSecure = true;
	%>
	
	<% if (payee.getID() != null) { %>
		<%-- Start: Dual approval processing --%>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
			<ffi:setProperty name="GetDAItem" property="itemId" value="<%=payee.getID()%>"/>
			<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>"/>
			<ffi:process name="GetDAItem"/>
			<ffi:removeProperty name="GetDAItem"/>

			<ffi:getProperty name="DAItem" property="Status" assignTo="daStatus"/>
		</ffi:cinclude>	
		<%-- End: Dual approval processing--%>
	<% } %>

	<% if (isReverse) { %>
		<ffi:cinclude value1="${ACHEntry.canReverse}" value2="false">
			<% canReverse = false; %>
		</ffi:cinclude>
		<td class="columndata">
			<div class="columndata" align="left">
                <input id="hold<ffi:getProperty name='ACHEntry' property='ID'/>" type="checkbox" <%=canReverse?"":"disabled"%> name="checkbox" onclick="changeHold(this,'<ffi:getProperty name='ACHEntry' property='ID'/>');" <ffi:cinclude value1="${ACHEntry.active}" value2="true">checked</ffi:cinclude>>
                <input id="active<ffi:getProperty name='ACHEntry' property='ID'/>" type="hidden" <%=canReverse?"":"disabled"%> name="ReverseACHBatch.CurrentEntry=<ffi:getProperty name='ACHEntry' property='ID'/>&ReverseACHBatch.EntryActive" value="<ffi:getProperty name='ACHEntry' property='Active'/>" >
			</div>
		</td>
	<% } %>

	<%-- Start: Dual approval processing--%>
	<ffi:cinclude value1="<%=daStatus%>" value2="">
		<% styleColor = ""; %>
	</ffi:cinclude>
	<ffi:cinclude value1="<%=payee.getScope()%>" value2="ACHBatch">
		<% styleColor = "red"; %>
	</ffi:cinclude>
	<ffi:cinclude value1="<%=payee.getScope()%>" value2="ACHTemplate">
		<% styleColor = "red"; %>
	</ffi:cinclude>
	<ffi:cinclude value1="${BatchIsland}" value2="Completed">
		<% styleColor = ""; %>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
		<% styleColor = ""; %>
	</ffi:cinclude>
	<%-- End: Dual approval processing--%>
		
		<td id="viewAchBatchPayeeNameValue<%=idCount%>" style="color:<%=styleColor%>" class="columndata">
		<%
		if (!classCode.equalsIgnoreCase("XCK")) {
		%>
			<ffi:cinclude value1="${ACHEntry.AchPayee.NickName}" value2="" operator="notEquals" >
<% if (isSecure) { // QTS 428174
%>
						<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
<% } else { %>
				<ffi:getProperty name="ACHEntry" property="AchPayee.NickName"/>
<% } %>
			</ffi:cinclude>
			<ffi:cinclude value1="${ACHEntry.AchPayee.NickName}" value2="" operator="equals" >
<% if (isSecure) { %>
				<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
<% } else { %>
				<ffi:getProperty name="ACHEntry" property="AchPayee.Name"/>
<% } %>
			</ffi:cinclude>
	<% } else { %>
			<ffi:getProperty name="ACHEntry" property="ItemResearchNumber"/>
	<% } %>
		</td>
		<% if (showParticipantID) { %>
			<td id="viewAchBatchParticipantIdValue<%=idCount%>" class="columndata"><ffi:getProperty name="ACHEntry" property="AchPayee.UserAccountNumber"/></td>
		<% } %>

			<td id="viewAchBatchAbaValue<%=idCount%>" class="columndata">
<% if (isSecure) { %>
				<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
<% } else { %>
				<ffi:getProperty name="ACHEntry" property="AchPayee.RoutingNumber"/>
<% } %>
			</td>
			<td id="viewAchBatchAccountNumberValue<%=idCount%>"  class="columndata">
<% if (isSecure) { %>
				<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
<% } else { %>
				<ffi:getProperty name="ACHEntry" property="AchPayee.AccountNumber"/>
<% } %>
			</td>
			<td id="viewAchBatchAccountTypeValue<%=idCount%>" class="columndata">
<% if (isSecure) { %>
				<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
<% } else { %>
				<ffi:getProperty name="ACHEntry" property="AchPayee.AccountType"/>
<% } %>
			</td>
		<td id="viewAchBatchCheckSerialNumberValue<%=idCount%>" class="columndata">
		<% if (checkSerNum) { %>
			<ffi:getProperty name="ACHEntry" property="CheckSerialNumber"/>
		<% } %>
		&nbsp;</td>
		<td id="viewAchBatchDiscreationaryDataValue<%=idCount%>" class="columndata">
		<% if (showDiscretionaryData) { %>
			<ffi:getProperty name="ACHEntry" property="DiscretionaryData"/>
		<% } %>
		&nbsp;</td>
		<td id="viewAchBatchAmountValue<%=idCount%>" class="columndata">
			<ffi:getProperty name="ACHEntry" property="AmountValue.CurrencyStringNoSymbol"/>
		</td>
		<td id="viewAchBatchTypeValue<%=idCount++%>" class="columndata">
			<span class="columndata" align="center">
			<ffi:cinclude value1="${ACHEntry.AmountIsDebit}" value2="true"><s:text name="jsp.default_146"/></ffi:cinclude>
			<ffi:cinclude value1="${ACHEntry.AmountIsDebit}" value2="false"><s:text name="jsp.default_120"/></ffi:cinclude>
			</span>
		</td>
	</tr>
	
	<%-- Start: Dual approval processing--%>
	<ffi:cinclude value1="<%=daStatus%>" value2="<%=IDualApprovalConstants.STATUS_TYPE_PENDING_APPROVAL%>">
	<tr>
		<td class="sectionheadDA" colspan="5">
			<ffi:getL10NString rsrcFile="cb" msgKey="da.errormessage.ach.ACHParticipantIsPendingApproval"/>
		</td>
	</tr>
	<ffi:removeProperty name="DAItem"/>			
	</ffi:cinclude>	
	<%-- End: Dual approval processing--%>
	
	<ffi:cinclude value1="${ACHBatch.BatchType}" value2="Entry Balanced" operator="equals">
	    <tr>
		    <td class="columndata" colspan="<%= spanCols %>" align="center">
			    <span class="sectionsubhead"><s:text name="jsp.common_107"/> </span>
			    <ffi:getProperty name="ACHEntry" property="OffsetAccountNumber" /> - <ffi:getProperty name="ACHEntry" property="OffsetAccountType" />
		    </td>
	    </tr>
	</ffi:cinclude>


	<%-- Need to check if the ACH Company is Addenda Entitled --%>
<% if (isTemplate) { %>
        <ffi:setProperty name="ACHCOMPANY" property="CurrentClassCodeTemplate" value="${ACHBatch.StandardEntryClassCode}" />
<% } else { %>
        <ffi:setProperty name="ACHCOMPANY" property="CurrentClassCode" value="${ACHBatch.StandardEntryClassCode}" />
<% } %>
	<ffi:cinclude value1="${ACHCOMPANY.ClassCodeAddendaEntitled}" value2="TRUE" operator="equals" >
		<ffi:list collection="ACHEntry.Addendas" items="ACHAddenda">
			<tr>
				<td colspan="<%= spanCols %>" class="columndata" align="center">
					<input type="text" name="textfield5" value="<ffi:getProperty name="ACHAddenda" property="PmtRelatedInfo"/>" disabled style="width:98%;">
				</td>
			</tr>
		</ffi:list>
	</ffi:cinclude>
	

<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
	<tr class="lightBackground">
		<td class="sectionsubhead" align="right"><s:text name="jsp.default_510"/></td>
		<% String taxformid; %>
		<ffi:getProperty name="ACHEntry" property="TaxFormID" assignTo="taxformid" />
		<% if (taxformid == null) { %>
			<ffi:getProperty name="ACHBatch" property="TaxFormID" assignTo="taxformid" />
		<% } %>
		<ffi:object name="com.ffusion.tasks.ach.GetTaxForm" id="TaxForm" scope="session"/>
		<ffi:setProperty name="TaxForm" property="ID" value="<%=taxformid%>" />
		<ffi:process name="TaxForm"/>
		<td class="columndata" colspan="<%= spanCols - 1 %>">
			<ffi:cinclude value1="${TaxForm.type}" value2="STATE"><ffi:getProperty name="TaxForm" property="state" /> State</ffi:cinclude>
			<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /> - <ffi:getProperty name="TaxForm" property="TaxFormDescription" />
		</td>
	</tr>
</ffi:cinclude>


	<%
		if (!canReverse) { %>
	    <tr>
		    <td class="sectionsubhead" colspan="<%= spanCols %>" align="center">
			    <s:text name="jsp.default_511"/>
		    </td>
	    </tr>
	<%	}
	%>
    </ffi:list>

    <ffi:setProperty name="ACHEntries" property="FilterPaging" value="none"/>
</table>
<div class="marginTop20">
    <% String tTrackingId = ""; %>
	<ffi:getProperty name="ACHBatch" property="TrackingID"  assignTo="tTrackingId" />
       <% if ((isView || isReverse) && !isApproval && !isTemplate) { 		// only display transaction history for View %>
		    <%-- include batch Transaction History --%>
            <s:action namespace="/pages/jsp/approvals" name="GetTransactionHistoryAction_init" executeResult="true">
                <s:param name="TrackingID" value="%{#session.ACHBatch.TrackingID}"/>
            </s:action>


        <% } else if ( tTrackingId != null && !"".equals( tTrackingId )  )  {   %>
        
            <s:action namespace="/pages/jsp/approvals" name="GetTransactionHistoryAction_init" executeResult="true">
                <s:param name="TrackingID" value="%{#session.ACHBatch.TrackingID}"/>
            </s:action>
        <% } %>	    
</div>
<div align="center">
    <% if (isView) { %>
			<% if (showExport) { 		// only display EXPORT button for pure view
			%>
			    &nbsp;&nbsp;&nbsp;
<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
			    <input class="submitbutton" type="button" value="<s:text name="jsp.default_195"/>" onclick="document.forms['ViewForm'].action='<ffi:getProperty name="SecureServletPath"/>ExportACHBatch'; document.forms['ViewForm'].submit();">
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
			    <input class="submitbutton" type="button" value="<s:text name="jsp.default_199"/>" onclick="document.forms['ViewForm'].action='<ffi:getProperty name="SecureServletPath"/>ExportACHBatch'; document.forms['ViewForm'].submit();">
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" >
			    <input class="submitbutton" type="button" value="<s:text name="jsp.default_197"/>" onclick="document.forms['ViewForm'].action='<ffi:getProperty name="SecureServletPath"/>ExportACHBatch'; document.forms['ViewForm'].submit();">
</ffi:cinclude>
			<% } %>
    <% } 
    if (isReverse || isDelete) { %>
			    <input class="submitbutton" type="button" value="<s:text name="jsp.default_82"/>" onclick='document.location="<%= returnPage %>";return false;'>
			    &nbsp;&nbsp;&nbsp;
    <% } %>
    <% if (isReverse) { %>
<ffi:cinclude value1="${ReverseACHBatch.DateWithinRangeForReverse}" value2="true">
			    <input class="submitbutton" type="submit" value="<s:text name="jsp.default_378"/>">
</ffi:cinclude>
    <% } %>
    <% if (isDelete) { %>
<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
			    <input class="submitbutton" type="submit" value="<s:text name="jsp.default_162"/>">
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
			    <input class="submitbutton" type="submit" value="<s:text name="jsp.default_162"/>">
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" >
			    <input class="submitbutton" type="submit" value="<s:text name="jsp.default_162"/>">
</ffi:cinclude>
    <% } %>
</div>

<ffi:removeProperty name="SortURL,SortAction"/>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});
	
</script>