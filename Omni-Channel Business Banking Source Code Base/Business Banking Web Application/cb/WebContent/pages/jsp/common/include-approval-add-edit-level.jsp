<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines" %>
<%@ page import="com.ffusion.beans.bankemployee.BankEmployees" %>
<%@ page import="com.ffusion.csil.beans.entitlements.EntitlementGroups" %>
<%@ page import="com.ffusion.beans.approvals.ApprovalsGroups" %>

<script src="<s:url value='/web'/>/js/approvals/approvals-chain.js" type="text/javascript"></script>

<s:include value="%{#session.PagesPath}common/checkAmount_js.jsp" />

<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;

%>
<ffi:setProperty name="ApprovalsLevelCurrencyCode" value="" />

	<ffi:object id="GetCurrencyCode" name="com.ffusion.tasks.GetCurrencyCode" scope="session" />
	<ffi:setProperty name="GetCurrencyCode" property="BusinessID" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetCurrencyCode" />
	<ffi:setProperty name="ApprovalsLevelCurrencyCode" value="${GetCurrencyCode.CurrencyCode}"/>
	<ffi:removeProperty name="GetCurrencyCode" />

<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
<ffi:process name="GetLimitBaseCurrency" />
<ffi:cinclude value1="${ApprovalsLevelCurrencyCode}" value2="" operator="equals">
    <ffi:setProperty name="ApprovalsLevelCurrencyCode" value="${BaseCurrency}" />
</ffi:cinclude>





<%
// determines the destination i.e which page should be refreshed when
// value of the combo box containg the operation types is changed
String _destination = ( String )session.getAttribute( "FullPagesPath" ) +
	( String )session.getAttribute( "approval_display_workflow_jsp" );
com.ffusion.beans.approvals.ApprovalsGroups apprGroups = null;

	// hold onto this field so that we can create the level object
	String _levelID = "";
	String _strMinAmount = "";
	String _strMaxAmount = "";
	String _formSubmit = ( String )session.getAttribute( "FullPagesPath" ) +
		( String )session.getAttribute( "approval_add_workflow_jsp" );

	String _editPage = request.getParameter( "Edit" );
	String _opType = request.getParameter("OpType");
	String _workFlowId = request.getParameter( "WorkflowID" );
	String _workFlowName = request.getParameter("WorkflowName");
	if( "All other activities".equals(_opType  ))
		_opType = "";

	String _errorPage = ( String )session.getAttribute( "errorPage" );

	String _backURL=(String)session.getAttribute("FullPagesPath") +
		( String )session.getAttribute("approval_display_add_edit_workflow_jsp") +
		"?Edit=" + _editPage + "&OpType=";
	if( _opType != null && _opType.length() > 0 ) {
		_backURL = _backURL + _opType;
	}


	String _accountID = request.getParameter("AccountID");
	if("All Accounts".equals(_accountID))
		_accountID = "";
	if( _accountID != null && _accountID.length() > 0 ) {
		_backURL = _backURL + "&AccountID="+_accountID;
	}

	if ( "true".equals(_editPage) ) {
	    _formSubmit = ( String )session.getAttribute( "FullPagesPath") +
	    	( String )session.getAttribute("approval_edit_workflow_jsp");
	    _strMinAmount = ( String )request.getParameter( "MinAmt" );
	    _strMaxAmount = ( String )request.getParameter( "MaxAmt" );
	    _levelID = com.ffusion.util.HTMLUtil.encode(( String )request.getParameter( "LevelID" ));
	    _backURL+= "&MinAmt=" + _strMinAmount + "&MaxAmt=" + _strMaxAmount + "&LevelID=" + _levelID;
	}

	if ( _errorPage == null ) {
	    _errorPage = "";
	}

	//used to populate values on the page that were passed back from the stall condition error page
	if ( _errorPage.equalsIgnoreCase( "true" ) ) {
	    _strMinAmount = ( String )request.getParameter( "MinAmt" );
	    _strMaxAmount = ( String )request.getParameter( "MaxAmt" );
	    if ( _strMinAmount == null ) {
	        _strMinAmount = "";
	    }
	    if ( _strMaxAmount == null ) {
	        _strMaxAmount = "";
	    }
	}


%>

<script>

	var isCB = true;

</script>

<%-- clean up the session --%>
<ffi:removeProperty name="approvalsLevelsOperationType" />
<ffi:removeProperty name="approvalsLevelsMinAmount" />
<ffi:removeProperty name="approvalsLevelsMaxAmount" />
<ffi:removeProperty name="approvalsLevelsID" />
<ffi:removeProperty name="APPROVALS_ENTITLEMENTS_COVERAGE"/>


	<ffi:object id="GetApprovalGroups" name="com.ffusion.tasks.approvals.GetApprovalsEntitledApprovalsGroups" scope="session"/>
	<ffi:setProperty name="GetApprovalGroups" property="ActivityType" value="<%= _opType %>"/>
	<ffi:process name="GetApprovalGroups" />
	<% apprGroups = ( com.ffusion.beans.approvals.ApprovalsGroups )session.getAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_GROUPS ); %>


<FORM id="ApprovalsLevelForm" name="ApprovalsLevelForm" action="<%= _formSubmit %>" method="post" >
    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<!-- <table width="100%" border="0"> -->
<div class="marginBottom20" id="accordianHolderDiv">
    <% if( _opType != null && _opType.length() > 0 ) { %>
	<input type="hidden" name="approvalsLevelsOperationType" value="<%=com.ffusion.util.HTMLUtil.encode(_opType)%>">
    <% } %>

	 <% if( _accountID != null && _accountID.length() > 0 ) { %>
		<input type="hidden" name="approvalsLevelsAccountID" value="<%=com.ffusion.util.HTMLUtil.encode(_accountID)%>">
    <% } %>

	 <% if( _workFlowName != null && _workFlowName.length() > 0 ) { %>
		<input type="hidden" name="approvalsLevelsWorkflowName" value="<%=com.ffusion.util.HTMLUtil.encode(_workFlowName)%>">
    <% } %>


	 <% if( _workFlowId != null && _workFlowId.length() > 0 ) { %>
		<input type="hidden" name="approvalsLevelsWorkflowID" value="<%=com.ffusion.util.HTMLUtil.encode(_workFlowId)%>">
    <% } %>

    <%-- <tr>
	<td nowrap align="left" class="sectionhead"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="11" border="0"> --%>
	    <%-- <ffi:cinclude value1="<%=_editPage%>" value2="true" operator="equals">
		<s:text name="jsp.common_73"/>
	    </ffi:cinclude> --%>

	    <div id="manageApprovalsDiv" class="paddingLeft10" style="width:98%;" align="center">
	    <div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<%-- <s:text name="jsp.approvals_11" /> --%>
					<ffi:cinclude value1="<%=_editPage%>" value2="true" operator="notEquals">
						<s:text name="jsp.approvals_11"/> <!-- Manage Approval Workflow  -->
				    </ffi:cinclude>

				    <ffi:cinclude value1="<%=_editPage%>" value2="true" operator="equals">
						<s:text name="jsp.common_73"/> <!-- Edit Approval Workflow  --><ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="equals">
						:<%=com.ffusion.util.HTMLUtil.encode(_workFlowName)%>
					 </ffi:cinclude>
				    </ffi:cinclude>
				</div>
				<div class="paneContentWrapper paneContentWrapperForApprovals"  id="approval_workflow_note_pane">
					<ffi:cinclude value1="<%=_editPage%>" value2="true" operator="notEquals">

					<ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="notEquals">
						<s:text name="jsp.common_101"/>
					 </ffi:cinclude>
					 <ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="equals">
					 <!--WORKFLOW NAME -->
					 <input class="txtbox ui-widget-content ui-corner-all"
					 type="text"
					 name="approvalsLevelsWorkflowName"
					 <%if(_workFlowName!=null && _workFlowName.length()>0){%>
					  value="<%=com.ffusion.util.HTMLUtil.encode(_workFlowName)%>"
					 disabled<%}else{%>
					 value=""
					 <%}%>
					 size="24"/>



					 </ffi:cinclude>
				    </ffi:cinclude>

					<img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
				    <s:text name="jsp.common_10"/>
				    <% if( _opType != null && _opType.length() > 0 ) { %>
					<% if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ACH_PAYMENTS ) ) { %>
						<s:text name="jsp.default_25"/>

					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ARC_ACH_Payment ) ) { %>
						<s:text name="jsp.common_31"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_BOC_ACH_Payment ) ) { %>
						<s:text name="jsp.common_35"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CCD_ACH_Payment ) ) { %>
						<s:text name="jsp.common_42"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CCD_Addenda_ACH_Payment ) ) { %>
						<s:text name="jsp.common_40"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CIE_ACH_Payment ) ) { %>
						<s:text name="jsp.common_46"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CIE_Addenda_ACH_Payment ) ) { %>
						<s:text name="jsp.common_44"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CTX_Addenda_ACH_Payment ) ) { %>
						<s:text name="jsp.common_60"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_IAT_ACH_Payment ) ) { %>
						<s:text name="jsp.common_87"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_IAT_Addenda_ACH_Payment ) ) { %>
						<s:text name="jsp.common_85"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_POP_ACH_Payment ) ) { %>
						<s:text name="jsp.common_128"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PPD_ACH_Payment ) ) { %>
						<s:text name="jsp.common_132"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PPD_Addenda_ACH_Payment ) ) { %>
						<s:text name="jsp.common_130"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_RCK_ACH_Payment ) ) { %>
						<s:text name="jsp.common_136"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TEL_ACH_Payment ) ) { %>
						<s:text name="jsp.common_154"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WEB_ACH_Payment ) ) { %>
						<s:text name="jsp.common_173"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WEB_Addenda_ACH_Payment ) ) { %>
						<s:text name="jsp.common_171"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_XCK_ACH_Payment ) ) { %>
						<s:text name="jsp.common_195"/>

					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_BILL_PAYMENTS ) ){ %>
						<s:text name="jsp.common_33"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DEPOSITS ) ){ %>
						<s:text name="jsp.common_37"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DISBURSEMENTS ) ){ %>
						<s:text name="jsp.common_38"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CHILD_SUPPORT_PAYMENTS ) ){ %>
						<s:text name="jsp.default_98"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TAX_PAYMENTS ) ){ %>
						<s:text name="jsp.default_407"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TRANSFERS ) ){ %>
						<s:text name="jsp.default_443"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TRANSFERS_TEMPLATE ) ){ %>
						<s:text name="jsp.common_162"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_CREATE ) ){ %>
						<s:text name="jsp.common_179"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_BOOK ) ){ %>
						<s:text name="jsp.common_176"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_BOOK_FREEFORM ) ){ %>
						<s:text name="jsp.common_177"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_BOOK_TEMPLATED ) ){ %>
						<s:text name="jsp.common_178"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DOMESTIC ) ){ %>
						<s:text name="jsp.common_180"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DOMESTIC_FREEFORM ) ){ %>
						<s:text name="jsp.common_181"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DOMESTIC_TEMPLATED ) ){ %>
						<s:text name="jsp.common_182"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DRAWDOWN ) ){ %>
						<s:text name="jsp.common_183"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DRAWDOWN_FREEFORM ) ){ %>
						<s:text name="jsp.common_184"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DRAWDOWN_TEMPLATED ) ){ %>
						<s:text name="jsp.common_185"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_FED ) ){ %>
						<s:text name="jsp.common_186"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_FED_FREEFORM ) ){ %>
						<s:text name="jsp.common_187"/>
					<% } else if( _opType.equals(com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_FED_TEMPLATED ) ){ %>
						<s:text name="jsp.common_188"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_HOST ) ){ %>
						<s:text name="jsp.common_189"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_INTERNATIONAL ) ){ %>
						<s:text name="jsp.common_190"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_INTERNATIONAL_FREEFORM ) ){ %>
						<s:text name="jsp.common_191"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_INTERNATIONAL_TEMPLATED ) ){ %>
						<s:text name="jsp.common_192"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRES_TEMPL_CREATE ) ){ %>
						<s:text name="jsp.common_193"/>
					<% } else if( _opType.equals( "All other activities" ) ){ %>
						<s:text name="jsp.common_16"/>
					<% } else if( _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ACH_TEMPLATE ) ){ %>
						<s:text name="jsp.default_26"/>
					<% } %>
			    <% } else { %>
					<s:text name="jsp.common_16"/>
			    <% } %>
				</div>
			</div>
		</div>
		<div>&nbsp;</div>
		 <div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<s:text name="jsp.common_161"/>
				</div>
				<div class="paneContentWrapper paneContentWrapperForApprovals"  id="approval_workflow_note_pane">
					<div>
						 <span> <s:text name="jsp.approvals.workflow.transaction.value.msg" /> </span>
					</div>
					<div>
						<div>&nbsp;</div>
						<span class="sectionsubhead dashboardLabelMargin marginLeft10"><s:text name="jsp.common_98"/> (<ffi:getProperty name="ApprovalsLevelCurrencyCode" />)</span>

						<input class="txtbox ui-widget-content ui-corner-all" type="text" name="approvalsLevelsMinAmount" value="<%=com.ffusion.util.HTMLUtil.encode(_strMinAmount)%>" size="24">
						(<s:text name="jsp.common_90"/>)
					</div>
					<div>
						<span class="sectionsubhead dashboardLabelMargin marginLeft10"><s:text name="jsp.common_97"/> (<ffi:getProperty name="ApprovalsLevelCurrencyCode" />)</span>
						<input class="txtbox ui-widget-content ui-corner-all" type="text" name="approvalsLevelsMaxAmount" value="<%=com.ffusion.util.HTMLUtil.encode(_strMaxAmount)%>" size="24">
				        (<s:text name="jsp.common_89"/>)
					</div>
					<div>
						<input class="txtbox" type="hidden" name="approvalsLevelsID" value="<%=_levelID%>" size="0"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" border="0" height="5" width="1">
					</div>
				</div>
			</div>
		</div>
		</div>
</div>
	<%-- </td>
    </tr>
    <tr>
	<td><img src='<ffi:getProperty name="spacer_gif"/>' height="10" width="1" border="0" alt=""></td>
    </tr>
</table> --%>

	 <div class="paneWrapper customAccordianBgClass">
	   	<div class="paneInnerWrapper">
			<div class="header">
				<s:text name="jsp.common_22"/>
			</div>
			<div class="paneContentWrapper paneContentWrapperForApprovals"  id="approval_workflow_note_pane">
				<table width="100%" border="0" id="selectPool">
				    <tr>
					<td colspan="3"><img src='<ffi:getProperty name="spacer_gif"/>' height="10" width="1" border="0" alt=""></td>
				    </tr>

				    <tr>
					<td width="45%">
						<div id="accordion1" >
						    <!-- user list -->
							<SELECT name="userList" id="userList" class="txtbox" multiple size="5">
							  <% if( _accountID != null && _accountID.length() > 0 ) { %>

							  		<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
									<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="ParentsAccounts"/>
									<ffi:setProperty name="SearchAcctsByNameNumType" property="Id" value="<%=_accountID%>"/>
									<ffi:process name="SearchAcctsByNameNumType"/>
									<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
									<ffi:setProperty name="SetAccount" property="AccountsName" value="FilteredAccounts"/>
									<ffi:setProperty name="SetAccount" property="ID" value="<%=_accountID%>"/>
									<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount"/>
									<ffi:process name="SetAccount"/>

							 		 <%-- get the user list and display it --%>
									<ffi:object id="GetAccountEntitledBusinessEmployees" name="com.ffusion.tasks.approvals.GetAccountEntitledBusinessEmployees" scope="session"/>
									<ffi:setProperty name="GetAccountEntitledBusinessEmployees" property="ActivityType" value="<%= _opType %>"/>
									<ffi:process name="GetAccountEntitledBusinessEmployees"/>

									<ffi:list collection="<%= com.ffusion.tasks.approvals.IApprovalsTask.ACCOUNT_APPROVALS_ENTITLED_BUSINESS_EMPLOYEES %>" items="ApprovalBusinessEmployee">
								    <%-- add the number and one space --%>
								    <%-- we can assume that business employees are SecureUser.TYPE_CUSTOMER --%>
								    <ffi:setProperty name="_userID" value="${ApprovalBusinessEmployee.Id}"/>
								    <option value="user-<%= SecureUser.TYPE_CUSTOMER %>-<ffi:getProperty name="_userID"/>-<ffi:getProperty name="ApprovalBusinessEmployee" property="FullName"/> (user)"><ffi:getProperty name="ApprovalBusinessEmployee" property="SortableFullNameWithLoginId"/></option>
								    <ffi:removeProperty name="ApprovalBusinessEmployee"/>
									</ffi:list>

									<% BusinessEmployees emps = (BusinessEmployees)session.getAttribute(com.ffusion.tasks.approvals.IApprovalsTask.ACCOUNT_APPROVALS_ENTITLED_BUSINESS_EMPLOYEES);
										if(emps == null || emps.size() == 0){
									%>
										 <optgroup label="No eligible users"></optgroup>
									<%} %>

								   <ffi:removeProperty name="<%= com.ffusion.tasks.approvals.IApprovalsTask.ACCOUNT_APPROVALS_ENTITLED_BUSINESS_EMPLOYEES %>" />

							    <%}else{%>
								    <%-- get the user list and display it --%>
									<ffi:object id="GetApprovalsEntitledBusinessEmployees" name="com.ffusion.tasks.approvals.GetApprovalsEntitledBusinessEmployees" scope="session"/>
									<ffi:setProperty name="GetApprovalsEntitledBusinessEmployees" property="ActivityType" value="<%= _opType %>"/>
									<ffi:process name="GetApprovalsEntitledBusinessEmployees"/>

									<ffi:list collection="<%= com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ENTITLED_BUSINESS_EMPLOYEES %>" items="ApprovalBusinessEmployee">

									    <%-- add the number and one space --%>
									    <%-- we can assume that business employees are SecureUser.TYPE_CUSTOMER --%>
									    <ffi:setProperty name="_userID" value="${ApprovalBusinessEmployee.Id}"/>
									    <option value="user-<%= SecureUser.TYPE_CUSTOMER %>-<ffi:getProperty name="_userID"/>-<ffi:getProperty name="ApprovalBusinessEmployee" property="FullName"/> (user)"><ffi:getProperty name="ApprovalBusinessEmployee" property="SortableFullNameWithLoginId"/></option>
									    <ffi:removeProperty name="ApprovalBusinessEmployee"/>
									</ffi:list>

									<% BusinessEmployees emps = (BusinessEmployees)session.getAttribute(com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ENTITLED_BUSINESS_EMPLOYEES);
										if(emps == null || emps.size() == 0){
									%>
										 <optgroup label="No eligible users"></optgroup>
									<%} %>

									<ffi:removeProperty name="<%= com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ENTITLED_BUSINESS_EMPLOYEES %>" />

							     <%}%>
							</SELECT>
						</div>

						<div id="accordion2" >
							<SELECT name="groupList" id="groupList" class="txtbox" multiple size="10">

								<%-- get the group list and display it --%>
								<ffi:object id="GetApprovalsEntitledBusinessGroups" name="com.ffusion.tasks.approvals.GetApprovalsEntitledBusinessGroups" scope="session"/>
								<ffi:setProperty name="GetApprovalsEntitledBusinessGroups" property="ActivityType" value="<%= _opType %>"/>
								<ffi:process name="GetApprovalsEntitledBusinessGroups"/>
								<ffi:removeProperty name="GetApprovalsEntitledBusinessGroups"/>

								<ffi:list collection="<%= com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ENTITLED_BUSINESS_GROUPS %>" items="ApprovalEntitlementGroup">
								    <ffi:setProperty name="_groupName" value="${ApprovalEntitlementGroup.GroupName}" />
								    <ffi:setProperty name="_groupID" value="${ApprovalEntitlementGroup.GroupId}" />
								    <ffi:cinclude value1="${ApprovalEntitlementGroup.GROUP_CASCADING_MODE}" value2="Group_cascading">
								    	<option value="group-<%=com.ffusion.approvals.constants.IApprovalsConsts.CASCADING_GROUP %>-<ffi:getProperty name="_groupID" />-<ffi:getProperty name="_groupName" /> (group)"><ffi:getProperty name="_groupName" /> (<s:text name="jsp.common_36"/>)</option>
								    </ffi:cinclude>
								    <ffi:cinclude value1="${ApprovalEntitlementGroup.GROUP_CASCADING_MODE}" value2="Group_cascading" operator="notEquals">
								    	<option value="group-0-<ffi:getProperty name="_groupID" />-<ffi:getProperty name="_groupName" /> (group)"><ffi:getProperty name="_groupName" /></option>
								    </ffi:cinclude>
								</ffi:list>

									<% EntitlementGroups grps = (EntitlementGroups)session.getAttribute(com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ENTITLED_BUSINESS_GROUPS);
										if(grps == null || grps.size() == 0){
									%>
										 <optgroup label="No eligible groups"></optgroup>
									<%} %>


							</SELECT>
						</div>


						<div id="accordion3" >
							<SELECT name="apprGroupList" id="apprGroupList" class="txtbox" multiple size="10">

								<ffi:object id="GetApprovalGroups" name="com.ffusion.tasks.approvals.GetApprovalsEntitledApprovalsGroups" scope="session"/>
								<ffi:setProperty name="GetApprovalGroups" property="ActivityType" value="<%= _opType %>"/>
								<ffi:process name="GetApprovalGroups"/>

								<ffi:list collection="<%= com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_GROUPS %>" items="ApprovalGroup">
									<ffi:object id="GetApprovalGroupMembers" name="com.ffusion.tasks.approvals.GetApprovalGroupMembers" scope="request"/>
										<ffi:setProperty name="GetApprovalGroupMembers" property="ApprovalGroupID" value="${ApprovalGroup.ApprovalsGroupID}"/>
									<ffi:process name="GetApprovalGroupMembers" />
									<ffi:cinclude value1="${ApprovalsGroupMembers}" value2="" operator="notEquals">
										<ffi:setProperty name="_apprGroupName" value="${ApprovalGroup.GroupName}"/>
										<ffi:setProperty name="_approvalGroupID" value="${ApprovalGroup.ApprovalsGroupID}"/>
											<option value="apprGroup-0-<ffi:getProperty name="_approvalGroupID"/>-<ffi:getProperty name="_apprGroupName"/> (approval group)"><ffi:getProperty name="_apprGroupName"/></option>
									</ffi:cinclude>
								</ffi:list>

									<% ApprovalsGroups grps1 = (ApprovalsGroups)session.getAttribute(com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_GROUPS);
										if(grps1 == null || grps1.size() == 0){
									%>
										 <optgroup label="No eligible approval groups"></optgroup>
									<%} %>

							</SELECT>
						</div>

					</td>


					<td align="center">
					    <!-- central buttons -->
					    <table border=0>

						<tr>
						    <td valign="top"><!-- button -->
				    		<%-- <sj:a button="true"	cssStyle="width:90px" onClick="ns.approval.removeAllFromChainList(isCB)"><s:text name="jsp.common_200"/></sj:a> --%>
						    </td>
						</tr>
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ADMINISTRATION %>">

				<% if ((! _isBC) && (_opType != null)
					    && (_opType.length() > 0)
					    && (! _opType.equals("All other activities"))
					    && (! _opType.equals(com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_BILL_PAYMENTS))
					    && (! _opType.equals(com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TRANSFERS))
					    && (! _opType.equals(com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TRANSFERS_TEMPLATE))
					    && (! _opType.equals(com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DEPOSITS))
					    && (! _opType.equals(com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DISBURSEMENTS))
					    && ((com.ffusion.beans.SecureUser)session.getAttribute("SecureUser")).getUsingEntProfiles() == false) { %>
						<tr>
						    <td>&nbsp;</td>
						</tr>
						<tr>
						    <td valign="top">
				    		<sj:a button="true"	cssStyle="width:90px" onClick="ns.approval.viewPermissions(document.ApprovalsLevelForm)"><s:text name="jsp.default_463"/></sj:a>
						    </td>
						</tr>

				<% } %>
				<script type="text/javascript">

				</script>

				</ffi:cinclude>
					    </table>
					</td>

					<td width="45%" rowspan="1" valign="top">
					    <!-- approval chain -->

					    <div id="accordion4" >
							<SELECT name="chainItemList" id="chainItemList" class="txtbox" multiple size="32" style="width:255px; height: 300px;" >
							    <ffi:cinclude value1="<%=_editPage %>" value2="true" operator="equals">
									<%-- get the chain item list and display them --%>
									<%-- put the approval level object into the session --%>

									<%-- call the task to put the level into the session --%>
									<ffi:object id="GetApprovalsLevel" name="com.ffusion.tasks.approvals.GetApprovalsLevel" scope="session"/>
									<ffi:setProperty name="GetApprovalsLevel" property="LevelID" value="<%= _levelID %>" />
									<ffi:setProperty name="GetApprovalsLevel" property="LevelType" value="<%= com.ffusion.approvals.constants.IApprovalsConsts.LEVEL_TYPE_BANK_DEFINED %>" />
									<ffi:process name="GetApprovalsLevel"/>

									<%-- get the work flow chain items --%>
									<ffi:object id="GetWorkflowChainItems" name="com.ffusion.tasks.approvals.GetWorkflowChainItems" scope="page"/>
									<ffi:process name="GetWorkflowChainItems"/>
							    </ffi:cinclude>
							    <% if ( _errorPage.equalsIgnoreCase( "true" ) || _editPage.equalsIgnoreCase( "true" ) ) { %>
								<ffi:list collection="ApprovalsChainItems" items="chainItem">
								    <ffi:setProperty name="groupOrUserID" value="${chainItem.GroupOrUserID}"/>
								    <ffi:setProperty name="userType" value="${chainItem.UserType}"/>

								    <ffi:cinclude value1="${chainItem.UsingUser}" value2="true" operator="equals">

									    <ffi:object id="ApprovalsChainItemUserName" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
									    <ffi:setProperty name="ApprovalsChainItemUserName" property="ProfileId" value="${chainItem.GroupOrUserID}"/>
									    <ffi:process name="ApprovalsChainItemUserName"/>

									    <ffi:setProperty name="ApprovalsChainItemUserFullName" value="${BusinessEmployee.FullName}"/>
									    <ffi:setProperty name="ApprovalsChainItemUserSortableFullNameWithLoginId" value="${BusinessEmployee.SortableFullNameWithLoginId}"/>
									    <%-- add the number and one space --%>
									    <option  mother="userList"  value="user-<ffi:getProperty name="userType"/>-<ffi:getProperty name="groupOrUserID"/>-<ffi:getProperty name="ApprovalsChainItemUserFullName"/> (user)"><ffi:getProperty name="ApprovalsChainItemUserSortableFullNameWithLoginId"/> (<s:text name="jsp.common_164"/>)</option>
									    <ffi:removeProperty name="BusinessEmployee"/>

								    </ffi:cinclude>

								    <% com.ffusion.beans.approvals.ApprovalsGroup apprGroup = null;
								       String apprGroupID;
								    %>
								    	<ffi:cinclude value1="${chainItem.IsApprovalsGroup}" value2="true" operator="equals">
								    	<ffi:setProperty name="ApprovalGroupID" value="${chainItem.GroupOrUserID}"/>
								    	<ffi:getProperty name="ApprovalGroupID" assignTo="apprGroupID"/>
								    	<% if ( apprGroups != null ) {
								    		apprGroup = apprGroups.getByID( Integer.parseInt( apprGroupID ) );
								    	   }
								    	%>
				                        <% if ( apprGroup != null && apprGroup.getGroupName() != null ) { %>
				                            <ffi:setProperty name="ApprovalsGroupName" value="<%=apprGroup.getGroupName()%>" />
				                            <option mother="apprGroupList"  value="apprGroup-0-<ffi:getProperty name="ApprovalGroupID"/>-<ffi:getProperty name="ApprovalsGroupName"/> (approval group)"><ffi:getProperty name="ApprovalsGroupName"/> (<s:text name="jsp.common_23"/>)</option>
				                            <% } %>
								    	</ffi:cinclude>

								    <ffi:cinclude value1="${chainItem.UsingUser}" value2="false" operator="equals">
								    <ffi:cinclude value1="${chainItem.IsApprovalsGroup}" value2="false" operator="equals">
									<ffi:object id="GetEntitlementGroupPropertyValues" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroupPropertyValues" scope="session"/>
									<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="PropertyName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
									<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="EntGroupId" value="${chainItem.GroupOrUserID}"/>
									<ffi:object id="ApprovalsChainItemGroupName" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
									<ffi:setProperty name="ApprovalsChainItemGroupName" property="GroupId" value="${chainItem.GroupOrUserID}"/>
									<ffi:process name="ApprovalsChainItemGroupName"/>
									<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="DefaultPropertyValue" value="${Entitlement_EntitlementGroup.GroupName}"/>
									<ffi:process name="GetEntitlementGroupPropertyValues"/>
									<%-- add the number and one space --%>
									<ffi:cinclude value1="${chainItem.UserType}" value2="0">
										<option mother="groupList" value="group-<ffi:getProperty name="userType"/>-<ffi:getProperty name="groupOrUserID"/>-<ffi:getProperty name="GetEntitlementGroupPropertyValues" property="PropertyValue"/> (group)"><ffi:getProperty name="GetEntitlementGroupPropertyValues" property="PropertyValue"/> (<s:text name="jsp.common_82"/>)</option>
									</ffi:cinclude>
									<ffi:cinclude value1="${chainItem.UserType}" value2="<%=String.valueOf(com.ffusion.approvals.constants.IApprovalsConsts.CASCADING_GROUP) %>">
										<option mother="groupList" value="group-<ffi:getProperty name="userType"/>-<ffi:getProperty name="groupOrUserID"/>-<ffi:getProperty name="GetEntitlementGroupPropertyValues" property="PropertyValue"/> (cascading group)"><ffi:getProperty name="GetEntitlementGroupPropertyValues" property="PropertyValue"/> (<s:text name="jsp.common_36"/>)</option>
									</ffi:cinclude>
									<ffi:removeProperty name="Entitlement_EntitlementGroup"/>
								    </ffi:cinclude>
								    </ffi:cinclude>
								</ffi:list>
								<ffi:removeProperty name="ApprovalsChainItems"/>
							    <% } %>
							</SELECT>
						<%--
					    </sj:accordionItem>
					    </sj:accordion>
						--%>
						</div>
					    <span class="requiredField"><s:text name="jsp.common_71"/><br/><br/></span>
					</td>
				    </tr>
				    <ffi:cinclude value1="<%=_editPage %>" value2="true" operator="equals">
				    <tr>
				    	<td class="sectionsubhead" colspan="6" style="color:red" align="center" width="600">
				    	<s:text name="jsp.common_169"/><br><br>
				    	</td>
				    </tr>
				    </ffi:cinclude>
				    <tr>
				    	<td colspan="3">&nbsp;</td>
				    </tr>
				    <tr>
					<td colspan="3" align="center">
						<sj:a
				                button="true"
								onClickTopics="cancelForm"
				        ><s:text name="jsp.default_82"/></sj:a>
				        <sj:a
							id="submitWorkflowLink"
							formIds="ApprovalsLevelForm"
				            targets="resultmessage"
				            button="true"
				            onBeforeTopics="beforeSubmitWorkflowForm"
				            onCompleteTopics="submitWorkflowFormComplete"
							onErrorTopics="errorSubmitWorkflowForm"
				            onSuccessTopics="successSubmitWorkflowForm"
				            onClickTopics="validateSaveTopic"
				            ><s:text name="jsp.default_366"/></sj:a>
					</td>
				    </tr>
				</table>
			</div>
		</div>
	</div>



	<div class="">&nbsp;</div>
<script>ns.approval.removeItemsFromCurrentList( document.ApprovalsLevelForm );</script>
</FORM>
<script type="text/javascript">

$.subscribe('validateSaveTopic', function(event,data){
	return ns.approval.validateSave( document.ApprovalsLevelForm, document.ApprovalsLevelForm.chainItemList );
});


$.subscribe('enableCascadingClick', function(event,data){

	var item, fromList, toList;
	fromList = $("#groupList").data('ownner');
	toList = $("#chainItemList").data('ownner');

	var optionValue = $("#queryCascadingEnableID .optionValue").eq(0).html();
	var iStr = $("#queryCascadingEnableID .insertCount").eq(0).html();
	var i = parseInt(iStr);

	optionValue = optionValue.replace("-0-","-8-");
	optionValue = optionValue.replace(/\(.*\)/, "(group)");

	item = toList.theList.find('li.ui-element').eq(i);
	item.data('optionLink').val(optionValue);

	var itemText = item.find('font').html();
	item.find('font').html(itemText.substr(0, itemText.length - 7) + "(<s:text name="jsp.common_36"/>)" );

	ns.common.closeDialog('#queryCascadingEnableID');
});

$.subscribe('disableCascadingClick', function(event,data){
	ns.common.closeDialog('#queryCascadingEnableID');
});

$.subscribe('enableMultiCascadingClick', function(event,data){
	var item, fromList, toList, optionValue;

	toList = $("#chainItemList").data('ownner');

	$('#multiQueryCascadingEnableID').find('table.queryList').find('select').each(function(){
		if($(this).val() != '-1')
		{
			var i = parseInt( $(this).val() );
			item = toList.theList.find('li.ui-element').eq(i);
			optionValue = item.data('optionLink').val();

			optionValue = optionValue.replace("-0-","-8-");
			optionValue = optionValue.replace(/\(.*\)/, "(group)");

			item.data('optionLink').val(optionValue);

			var itemText = item.find('font').html();
			item.find('font').html(itemText.substr(0, itemText.length - 7) + "(<s:text name="jsp.common_36"/>)" );
		}
	});
	ns.common.closeDialog('#multiQueryCascadingEnableID');
});


ns.approval.exchangeMirrorRealItem = function( item, listId )
{
	var fakeItem = $(listId + "_sms_list").find(".dragging-mirror").eq(0);

	if( fakeItem == null ) return null;

	fakeItem.removeClass("dragging-mirror").addClass("ui-element").show();
	fakeItem.data('optionLink', item.data('optionLink') );
	item.data('optionLink', item.data('optionLink').clone() );

	$(listId).data('ownner')._applyItemState(fakeItem);

	return fakeItem;
};

ns.approval.openCascadingQueryDialog = function(optionValue, optionText, insertCount)
{
	$("#queryCascadingEnableID .optionValue").html(optionValue);
	$("#queryCascadingEnableID .groupName").html(optionText);

	$("#queryCascadingEnableID .insertCount").html(insertCount);

	$("#queryCascadingEnableID").dialog('open');
};

ns.approval.updateChainNo = function(e, ui, updateList)
{
	$("#chainItemList_sms_list").find('font').map(function(i){

		var tempText 	= $(this).html();

		var numberIndex = tempText.indexOf( "." );
		var number 		= new String( i + 1 ).concat( ". " );
		var tempText	= number.concat( tempText.substr( numberIndex + 1, tempText.length ) );
		$(this).html(tempText);
	});
};

ns.approval.afterApplyLeftItem = function(item, wgt){
	item.unbind('click');
	var $add = item.find('a.action').eq(0);
	item.bind('click', function(){
		var $check = item.find('span.ui-icon-check');
		if( $check.size() > 0 )
		{
			$check.parent().remove();
		}
		else
		{
			$('<a class="ui-corner-all" style="" title="select to view" href="#">'
					+ '<span class="ui-icon ui-icon-check custom-ui-icon-check"></span></a>').insertBefore($add);
		}
	});
};
ns.approval.afterAdd = function(item, fromList, toList)
{
	item.find('font').html( toList.theList.children().size() + ". " + item.find('font').html() );
};

ns.approval.afterChainCreate = function(chainList) {

	var options = chainList.element.children('option');
	var items = chainList.theList.children('li.ui-element');

	for( var i = 0 ; i < items.size() ; i++ ) {
		var item = items.eq(i);
		var option = item.data('optionLink');
		var oText = option.text();

		if( oText.lastIndexOf("(user)") == oText.length - 6 )
			oText = oText.substr(0, oText.length - 6);
		else if ( oText.lastIndexOf("(group)") == oText.length - 7 )
			oText = oText.substr(0, oText.length - 7);
		else if ( oText.lastIndexOf("(approval group)") == oText.length - 16 )
			oText = oText.substr(0, oText.length - 16);
		else if ( oText.lastIndexOf("(cascading group)") == oText.length - 17 )
			oText = oText.substr(0, oText.length - 17);

		option.text( oText );
		var j = i + 1;

		item.find('font').html( j + ". " + item.find('font').html() );
	}
};

ns.approval.afterChainRemove = function( item, homeList, guestList ) {
	if( item == null || homeList == null || guestList == null ||  item.data('optionLink') == null )
		return;

	if(item.data('optionLink').attr('mother') == "userList") {
		ns.approval.updateChainNo();
	}
};

ns.approval.beforeApplyRightItem = function(item, chainList)
{
	var listName = item.data('optionLink').val().substr( 0, 6 );
	var homeList = "";
	if( listName.indexOf( "user" ) != -1 ) {
		homeList = "userList";
	} else if( listName.indexOf( "group" ) != -1 )
    	homeList = "groupList";
	else
    	homeList = "apprGroupList";

	item.data('optionLink').attr('mother', homeList);

	return true;
};

ns.approval.beforeApprGroupAdd = function( item, fromList, toList ){
	if( item == null || fromList == null || toList == null ||  item.data('optionLink') == null )
		return false;

	var _item = item.clone();
	_item.data('optionLink', item.data('optionLink').clone() );

	_item.find('font').html(_item.find('font').html() + " (<s:text name="jsp.common_23"/>)" );

	fromList.addItem( _item, toList );

	return false;
};



ns.approval.beforeChainReceive = function( item, sender, rcvList) {

	if( item == null || sender == null || rcvList == null)
		return false;


	var $check = item.find('span.ui-icon-check');

	if( $check.size() == 0 ){	// single dragging
		if( item.data('optionLink').attr('mother') == "userList" )
		{
			item.find('font').html(item.find('font').html() + " (<s:text name="jsp.common_164"/>)" );
			return true;
		}
		else if ( item.data('optionLink').attr('mother') == "apprGroupList" ) {
			ns.approval.exchangeMirrorRealItem( item, "#apprGroupList" );
			$("#apprGroupList").data('ownner').updateSelect();

			item.find('font').html(item.find('font').html() + " (<s:text name="jsp.common_23"/>)" );
			return true;
		} else {
			var option = item.data('optionLink');

			var optionValue = option.val();

			var fakeItem = ns.approval.exchangeMirrorRealItem( item, "#groupList" );
			$("#groupList").data('ownner').updateSelect();
			var tempText = "";

			if(isCB){
				var optionText = option.text();
				var firstIndex = optionValue.indexOf( '-' );
				var secondIndex = optionValue.indexOf( '-',  firstIndex + 1 );
				var userType = new Number(optionValue.substring( firstIndex + 1, secondIndex ));
				//com.ffusion.approvals.constants.IApprovalsConsts.CASCADING_GROUP = 8
				if (userType == 8){
					optionValue = optionValue.replace(/\(.*\)/, "(group)");
				} else {
					// confirm box check
					var i =0 ;
					rcvList.theList.children('li.ui-element').map(function(j){
						if($(this).attr('container') == "groupList_sms" )
							i = j;
					});

					ns.approval.openCascadingQueryDialog(optionValue, optionText, i);

					tempText += " (<s:text name="jsp.common_82"/>)";
				}

			} else {
				tempText += " (<s:text name="jsp.common_82"/>)";
			}

			item.data('optionLink').val(optionValue);

			item.find('font').html(" . " + item.find('font').html() + tempText );

		    return true;
		}
	} // end $check.size() == 0
	else {	// multiple dragging
		var isHomeList = false;
		var beforeArray = new Array();
		var afterArray = new Array();
		var curArray = beforeArray;
		var tmpItem = null;

		// find items in userList
		if( item.data('optionLink').attr('mother') == "userList" )
		{
			item.find('font').html(item.find('font').html() + " (<s:text name="jsp.common_164"/>)" );
			isHomeList = true;
		}
		else
			isHomeList = false;

		$("#userList_sms_list").children().each(function(){
			if(!isHomeList)
			{
				if($(this).hasClass('ui-element') && $(this).find('span.ui-icon-check').size() > 0  ){
					$(this).find('font').html($(this).find('font').html() + " (<s:text name="jsp.common_164"/>)" );
					curArray.push($(this));
				}
			} else {
				if($(this).hasClass('ui-element') && $(this).find('span.ui-icon-check').size() > 0  ){
					$(this).find('font').html($(this).find('font').html() + " (<s:text name="jsp.common_164"/>)" );
					curArray.push($(this));
				} else if($(this).hasClass('dragging-mirror') && $(this).find('span.ui-icon-check').size() > 0  ) {
					curArray = afterArray;
				}
			}
		});

		// find items in groupList
		if( item.data('optionLink').attr('mother') == "groupList" )
		{
			var option = item.data('optionLink');
			var optionValue = option.val();

			tmpItem = ns.approval.exchangeMirrorRealItem( item, "#groupList" );
			item.clone().removeClass("ui-element").hide().addClass('dragging-mirror').insertBefore( tmpItem );
			$("#groupList").data('ownner').updateSelect();

			var tempText = "";
			if(isCB){
				var optionText = option.text();
				var firstIndex = optionValue.indexOf( '-' );
				var secondIndex = optionValue.indexOf( '-',  firstIndex + 1 );
				var userType = new Number(optionValue.substring( firstIndex + 1, secondIndex ));
				//com.ffusion.approvals.constants.IApprovalsConsts.CASCADING_GROUP = 8
				if (userType == 8){
					optionValue = optionValue.replace(/\(.*\)/, "(group)");
				} else {
					// confirm box check
					item.attr("casQuery", optionText);
					tempText += " (<s:text name="jsp.common_82"/>)";
				}

			} else {
				tempText += " (<s:text name="jsp.common_82"/>)";
			}

			item.data('optionLink').val(optionValue);

			item.find('font').html(" . " + item.find('font').html() + tempText );
			isHomeList = true;
		}
		else
			isHomeList = false;

		$("#groupList_sms_list").children().each(function(){
			if(!isHomeList)
			{
				if($(this).hasClass('ui-element') && $(this).find('span.ui-icon-check').size() > 0 ){
					tmpItem = $(this).clone();
					tmpItem.data('optionLink', $(this).data('optionLink').clone() );

					var option = tmpItem.data('optionLink');
					var optionValue = option.val();

					var tempText = "";
					if(isCB){
						var optionText = option.text();
						var firstIndex = optionValue.indexOf( '-' );
						var secondIndex = optionValue.indexOf( '-',  firstIndex + 1 );
						var userType = new Number(optionValue.substring( firstIndex + 1, secondIndex ));
						//com.ffusion.approvals.constants.IApprovalsConsts.CASCADING_GROUP = 8
						if (userType == 8){
							optionValue = optionValue.replace(/\(.*\)/, "(group)");
						} else {
							// confirm box check
							tmpItem.attr("casQuery", optionText);
							tempText += " (<s:text name="jsp.common_82"/>)";
						}

					} else {
						tempText += " (<s:text name="jsp.common_82"/>)";
					}

					tmpItem.data('optionLink').val(optionValue);

					tmpItem.find('font').html(" . " + tmpItem.find('font').html() + tempText );

					curArray.push(tmpItem);
				}
			} else {
				if($(this).hasClass('ui-element') && $(this).find('span.ui-icon-check').size() > 0  ){
					tmpItem = $(this).clone();
					tmpItem.data('optionLink', $(this).data('optionLink').clone() );

					var option = tmpItem.data('optionLink');
					var optionValue = option.val();

					var tempText = "";
					if(isCB){
						var optionText = option.text();
						var firstIndex = optionValue.indexOf( '-' );
						var secondIndex = optionValue.indexOf( '-',  firstIndex + 1 );
						var userType = new Number(optionValue.substring( firstIndex + 1, secondIndex ));
						//com.ffusion.approvals.constants.IApprovalsConsts.CASCADING_GROUP = 8
						if (userType == 8){
							optionValue = optionValue.replace(/\(.*\)/, "(group)");
						} else {
							// confirm box check
							tmpItem.attr("casQuery", optionText);
							tempText += " (<s:text name="jsp.common_82"/>)";
						}

					} else {
						tempText += " (<s:text name="jsp.common_82"/>)";
					}

					tmpItem.data('optionLink').val(optionValue);

					tmpItem.find('font').html(" . " + tmpItem.find('font').html() + tempText );

					curArray.push(tmpItem);
				} else if($(this).hasClass('dragging-mirror') && $(this).find('span.ui-icon-check').size() > 0  ) {
					curArray = afterArray;
				}
			}
		});

		// find items in apprGroupList
		if( item.data('optionLink').attr('mother') == "apprGroupList" )
		{
			tmpItem = ns.approval.exchangeMirrorRealItem( item, "#apprGroupList" );
			item.clone().removeClass("ui-element").hide().addClass('dragging-mirror').insertBefore( tmpItem );
			$("#apprGroupList").data('ownner').updateSelect();

			item.find('font').html(item.find('font').html() + " (<s:text name="jsp.common_23"/>)" );
			isHomeList = true;
		}
		else
			isHomeList = false;

		$("#apprGroupList_sms_list").children().each(function(){
			if(!isHomeList)
			{
				if($(this).hasClass('ui-element') && $(this).find('span.ui-icon-check').size() > 0  ){
					tmpItem = $(this).clone();
					tmpItem.find('font').html(tmpItem.find('font').html() + " (<s:text name="jsp.common_23"/>)" );
					tmpItem.data('optionLink', $(this).data('optionLink').clone() );
					curArray.push(tmpItem);
				}
			} else {
				if($(this).hasClass('ui-element') && $(this).find('span.ui-icon-check').size() > 0  ){
					tmpItem = $(this).clone();
					tmpItem.find('font').html(tmpItem.find('font').html() + " (<s:text name="jsp.common_23"/>)" );
					tmpItem.data('optionLink', $(this).data('optionLink').clone() );
					curArray.push(tmpItem);
				} else if( $(this).hasClass('dragging-mirror') && $(this).find('span.ui-icon-check').size() > 0  ) {
					curArray = afterArray;
				}
			}
		});

		// transfer items which shoud be inserted before the dragging item
		for( var i = 0 ; i < beforeArray.length ; i ++ )
		{
			var insertItem = $( beforeArray[i] );
			insertItem.insertBefore( item );
			rcvList.applyItemState( insertItem );
		}


		// transfer items which shoud be inserted after the dragging item\
		for( var i = afterArray.length - 1 ; i >= 0 ; i -- )
		{
			var insertItem = $( afterArray[i] );
			insertItem.insertAfter(item);
			rcvList.applyItemState( insertItem );
		}

		// update userList's number of items
		$("#userList").data('ownner').updateSelect();

		// check if show multiCascadingQuery dialog
		var $needChkItems = $("#chainItemList_sms_list").children('li[casQuery]');
		if($needChkItems.size() > 0)
		{
			$('#multiQueryCascadingEnableID').find('table.queryList').html("");

			var j = -1;
			$("#chainItemList_sms_list").children('li.ui-element').each(function(){
				j++;

			 	if($(this).attr('casQuery') != null)
			 	{
			 		$('#multiQueryCascadingEnableID').find('table.queryList')
			 			.append('<tr><td><s:text name="jsp.common_74"/> <b class="groupName">'
			 					+ $(this).attr('casQuery') + '</b></td><td>'
			 					+ '<select class="txtbox"><option value="' + j + '"><s:text name="jsp.default_467" /></option> <option value="-1" selected><s:text name="jsp.default_295" /></option></select></td></tr>');
			 		$(this).removeAttr('casQuery');
				}
			});

			$('#multiQueryCascadingEnableID').find('select').selectmenu({ width: 70 });

			$('#multiQueryCascadingEnableID').dialog('open');
		}

		// remove all check icon

		$('#groupList_sms_list').find('span.ui-icon-check').each(function(){
			$(this).parent().remove();
		});

		$('#apprGroupList_sms_list').find('span.ui-icon-check').each(function(){
			$(this).parent().remove();
		});
		return true;
	}

};

ns.approval.beforeChainRemove = function( item, homeList, guestList ) {
	if( item == null || homeList == null || guestList == null ||  item.data('optionLink') == null )
		return false;
	var option = item.data('optionLink');

	var homeId = option.attr('mother');

	if(homeId == "userList"){
		var itemTxt = $.trim( item.find('font').html() );
		item.find('font').html( $.trim( itemTxt.substr(3, itemTxt.length - 9) ) );
		return true;
	} else {
		option.remove();
		item.remove();
		guestList.updateCount();
		ns.approval.updateChainNo();
		return false;
	}
};

ns.approval.beforeGroupAdd = function( item, fromList, toList ){
	if( item == null || fromList == null || toList == null ||  item.data('optionLink') == null )
		return false;

	var option = item.data('optionLink');

	var optionValue = option.val();
	var tempText = "";
	if(isCB){
		option = $(option);
		var optionText = option.text();
		var firstIndex = optionValue.indexOf( '-' );
		var secondIndex = optionValue.indexOf( '-',  firstIndex + 1 );
		var userType = new Number(optionValue.substring( firstIndex + 1, secondIndex ));
		//com.ffusion.approvals.constants.IApprovalsConsts.CASCADING_GROUP = 8
		if (userType == 8){
			optionValue = optionValue.replace(/\(.*\)/, "(group)");
		} else {
			var i = toList.theList.children('li.ui-element').size()

			ns.approval.openCascadingQueryDialog(optionValue, optionText, i);
			tempText += " (<s:text name="jsp.common_82"/>)";
		}

	} else {
		tempText += " (<s:text name="jsp.common_82"/>)";
	}

	var _item = item.clone();
	_item.data('optionLink', item.data('optionLink').clone() );

	_item.data('optionLink').val(optionValue);

	_item.find('font').html(_item.find('font').html() + tempText );

	fromList.addItem(_item, toList);

    return false;
};

ns.approval.beforeUserAdd = function( item, fromList, toList ){
	if( item == null || fromList == null || toList == null )
		return false;

	var tempHtml = item.find('font').html() + " (<s:text name="jsp.common_164"/>)";

	item.find('font').html(tempHtml);
	return true;
};

ns.approval.removeAllFromChainList = function(isCB) {

	var homeId, option, item;
	var $userList = $("#userList").data('ownner'),
		$chainList = $("#chainItemList").data('ownner');


	$("#chainItemList_sms_list").find('li.ui-element').each(
		function(){
			item = $(this);
			option = item.data('optionLink');

			homeId = option.attr('mother');
			if(homeId == "userList"){
				var itemTxt = $.trim( item.find('font').eq(0).html() );

				item.find('font').eq(0).html( $.trim( itemTxt.substr(3, itemTxt.length - 9) ) );
				$chainList.removeItem(item, $userList);
			} else {
				item.data('optionLink').remove();
				item.remove();
			}
	});

	$chainList.updateSelect();
};




(function($){

	// init supermultiselect i18n texts
	$.extend($.ui.supermultiselect, {
		locale: {
			addAll: js_multiSelect_addAll,
			removeAll: js_multiSelect_removeAll,
			itemsCount: js_multiSelect_itemsRemain
		}
	});

	$('div.ui-accordion-content').attr('style','');
	$("#chainItemList").supermultiselect({
				width: "100%"
			, 	height: "500px"
			, 	containedPool: "#notes"
			, 	sortable: true
			, 	addRemoveAllButton : true
			,	beforeRemove: ns.approval.beforeChainRemove
			,	afterRemove:  function(item, homeList, guestList ){
					ns.approval.afterChainRemove(item, homeList, guestList );
					var $superSelectMenu = $("#chainItemList_sms");
					ns.common.superSelectFocusHandler($superSelectMenu);
				}
			,	afterUpdate:  ns.approval.updateChainNo
			,	beforeReceive: ns.approval.beforeChainReceive
			,	beforeApplyItem: ns.approval.beforeApplyRightItem
			,	afterApplyItem: ns.approval.afterApplyLeftItem
			,	afterCreate:	ns.approval.afterChainCreate
	});

	$("#chainItemListRemoveAllLink").click(function() {
		ns.approval.removeAllFromChainList(isCB);
	});

	$("#userList").supermultiselect({
				width: "100%"
			, 	height: "180px"
			,   toIds: [ "chainItemList" ]
			, 	containedPool: "#notes"
			, 	beforeAdd: ns.approval.beforeUserAdd
			,	afterAdd:  function(item, fromList, toList){
					ns.approval.afterAdd(item, fromList, toList);
					var $superSelectMenu = $("#userList_sms");
					ns.common.superSelectFocusHandler($superSelectMenu);
				}
			,	afterApplyItem: ns.approval.afterApplyLeftItem
			,	sortableOptions: {
						start: function(e, ui) {
						$("#userList_sms_list").find('.dragging-mirror').each(function(){
							$(this).remove();
						});
						var fakeItem = ui.item.clone().insertAfter(ui.item);
						fakeItem.addClass("dragging-mirror").removeClass('ui-element');
					}
				,	helper: function(event, ui) {
						var _helper = $('<div class="ui-supermultiselect ui-helper-clearfix ui-widget">'
								+ '<div class="selected"><ul class="selected"></ul></div></div>');
						var _item = ui.clone();
						_helper.find('ul').append(_item).width($(this).width());
						_helper.width($(this).width());

						var $check = ui.find('span.ui-icon-check');

						if( $check.size() > 0 )
						{
							var groupItem0 = _helper.clone();
							var groupItem1 = _helper.clone();
							var groupItem2 = _helper.clone();

							_helper = $("<div></div>");

							groupItem0.attr('style', 'position: absolute; top: 0px; left: 0px;');
							groupItem1.attr('style', 'position: absolute; top: 3px; left: 3px;');
							groupItem2.attr('style', 'position: absolute; top: 6px; left: 6px;');

							_helper.append(groupItem2).append(groupItem1).append(groupItem0);
						}

						return _helper;
					}
			}
	});
	$("#groupList").supermultiselect({
				width: "100%"
			, 	height: "180px"
			, 	toIds: [ "chainItemList" ]
			, 	containedPool: "#notes"
			,	beforeAdd: ns.approval.beforeGroupAdd
			,	afterAdd:  function(item, fromList, toList){
					ns.approval.afterAdd(item, fromList, toList);
					var $superSelectMenu = $("#groupList_sms");
					ns.common.superSelectFocusHandler($superSelectMenu);
				}
			,	afterApplyItem: ns.approval.afterApplyLeftItem
			,	sortableOptions: {
						start: function(e, ui) {
							$("#groupList_sms_list").find('.dragging-mirror').each(function(){
								$(this).remove();
							});
							var fakeItem = ui.item.clone().insertAfter(ui.item);
							fakeItem.addClass("dragging-mirror").removeClass('ui-element');
						}
					,	helper: function(event, ui) {
							var _helper = $('<div class="ui-supermultiselect ui-helper-clearfix ui-widget">'
									+ '<div class="selected"><ul class="selected"></ul></div></div>');
							var _item = ui.clone();
							_helper.find('ul').append(_item).width($(this).width());
							_helper.width($(this).width());

							var $check = ui.find('span.ui-icon-check');

							if( $check.size() > 0 )
							{
								var groupItem0 = _helper.clone();
								var groupItem1 = _helper.clone();
								var groupItem2 = _helper.clone();

								_helper = $("<div></div>");

								groupItem0.attr('style', 'position: absolute; top: 0px; left: 0px;');
								groupItem1.attr('style', 'position: absolute; top: 3px; left: 3px;');
								groupItem2.attr('style', 'position: absolute; top: 6px; left: 6px;');

								_helper.append(groupItem2).append(groupItem1).append(groupItem0);
							}

							return _helper;
						}
				}
	});
	$("#apprGroupList").supermultiselect({
				width: "100%"
			, 	height: "180px"
			, 	toIds: [ "chainItemList" ]
			, 	containedPool: "#notes"
			,	beforeAdd: ns.approval.beforeApprGroupAdd
			,	afterAdd:  function(item, fromList, toList){
					ns.approval.afterAdd(item, fromList, toList);
					var $superSelectMenu = $("#apprGroupList_sms");
					ns.common.superSelectFocusHandler($superSelectMenu);
				}
			,	afterApplyItem: ns.approval.afterApplyLeftItem
			,	sortableOptions: {
						start: function(e, ui) {
							$("#apprGroupList_sms_list").find('.dragging-mirror').each(function(){
								$(this).remove();
							});
							var fakeItem = ui.item.clone().insertAfter(ui.item);
							fakeItem.addClass("dragging-mirror").removeClass('ui-element');
						}
					,	helper: function(event, ui) {
						var _helper = $('<div class="ui-supermultiselect ui-helper-clearfix ui-widget">'
								+ '<div class="selected"><ul class="selected"></ul></div></div>');
						var _item = ui.clone();
						_helper.find('ul').append(_item).width($(this).width());
						_helper.width($(this).width());

						var $check = ui.find('span.ui-icon-check');

						if( $check.size() > 0 )
						{
							var groupItem0 = _helper.clone();
							var groupItem1 = _helper.clone();
							var groupItem2 = _helper.clone();

							_helper = $("<div></div>");

							groupItem0.attr('style', 'position: absolute; top: 0px; left: 0px;');
							groupItem1.attr('style', 'position: absolute; top: 3px; left: 3px;');
							groupItem2.attr('style', 'position: absolute; top: 6px; left: 6px;');

							_helper.append(groupItem2).append(groupItem1).append(groupItem0);
						}

						return _helper;
					}
				}
	});
})(jQuery);

</script>

<!-- Dialog: queryCascadingEnableID -->
<sj:dialog id="queryCascadingEnableID" cssClass="includeApprovalDialog staticContentDialog" title="%{getText('jsp.common_91')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="400">
<div align="left"><s:text name="jsp.common_74"/> <b class="groupName"></b>? <br/>
<s:text name="jsp.common_175"/><br/><br/>
</div>
<div style="display: none">
<span class="optionValue"></span>
<span class="insertCount"></span>
</div>
<div align="center">
	<sj:a id="enableCascadingLink1" button="true"  onClickTopics="enableCascadingClick" title="%{getText('jsp.default_467')}"><s:text name="jsp.default_467"/></sj:a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<sj:a id="disableCascadingLink1" button="true" onClickTopics="disableCascadingClick" title="%{getText('jsp.default_295')}"><s:text name="jsp.default_295"/></sj:a></div>
</sj:dialog>

<!-- Dialog: multiQueryCascadingEnableID -->
<sj:dialog id="multiQueryCascadingEnableID" cssClass="includeApprovalDialog staticContentDialog" title="%{getText('jsp.common_91')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="400">
<div align="left"><s:text name="jsp.common_175"/><br/><br/>
</div>
<div>
	<table class="queryList">
	</table>
</div>
<br/><br/>
<div align="center">
	<sj:a id="enableCascadingLink2" button="true"  onClickTopics="enableMultiCascadingClick" title="%{getText('jsp.default_303')}">&nbsp;<s:text name="jsp.default_303"/>&nbsp;</sj:a>
</div>
</sj:dialog>

<sj:dialog id="noneSelectedAlertID" cssClass="includeApprovalDialog staticContentDialog" title="%{getText('jsp.common_15')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="400">
<br/><div align="center"><s:text name="jsp.common_196"/></div>
<br/><br/>
<div align="center">
	<sj:a id="disableCascadingLink1" button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_303')}"><s:text name="jsp.default_303"/></sj:a>
</div>
</sj:dialog>

<sj:dialog id="AlertID" cssClass="includeApprovalDialog staticContentDialog" title="%{getText('jsp.common_15')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="400">
<br/><div align="center" class="alert-text"></div>
<br/><br/>
<div align="center">
	<sj:a id="cancelAlertLink1" button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_303')}"><s:text name="jsp.default_303"/></sj:a>
</div>
</sj:dialog>
<ffi:setProperty name="BackURL" value="<%= _backURL %>"/>
<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="ApprovalsLevelCurrencyCode" />
<ffi:removeProperty name="BaseCurrency"/>


<script>
	$('#accordion1').pane({
		title: js_user_pane_title,
		width: '100%',
		showHeader: true
	});
	$('#accordion2').pane({
		title: js_group_pane_title,
		width: '100%',
		showHeader: true
	});
	$('#accordion3').pane({
		title: js_approval_group_pane_title,
		width: '100%',
		showHeader: true
	});
	$('#accordion4').pane({
		title: js_approval_chain_pane_title,
		width: '100%',
		showHeader: true
	});
</script>

<style>
#accordion1, #accordion2{
	margin-bottom: 1em;
}
</style>