<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib uri="/struts-jquery-tags" prefix="sj"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;
if( session.getAttribute( "APPROVALS_BC" ) != null ) {
	String isBCStr = ( String )session.getAttribute( "APPROVALS_BC" );
	if( isBCStr.equalsIgnoreCase( "true" ) ) {
		_isBC = true;
	}
}
%>
<%  
boolean isConcentration = false;
boolean isDisbursement = false;
String ft_type = String.valueOf( com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_CASH_CONCENTRATION );      // default to concentration
%>
<ffi:cinclude value1="${AddEditCashCon.Type}" value2="<%= ft_type %>" operator="equals" >
	<% isConcentration = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditCashCon.Type}" value2="<%= ft_type %>" operator="notEquals" >
	<% isDisbursement = true; %>
</ffi:cinclude>

<ffi:setProperty name="viewCashConDoneURL" value="${FullPagesPath}payments/cashcon.jsp"/>
<% if( session.getAttribute( "cashcon_details_post_jsp" ) != null ) { %>
    <ffi:setProperty name="viewCashConDoneURL" value="${cashcon_details_post_jsp}"/>
<% } %>
<% if( session.getAttribute( "CashConView" ) != null ) { %>
	<ffi:object name="com.ffusion.tasks.cashcon.GetAuditHistory" id="GetAuditHistory" scope="session"/>
		<ffi:setProperty name='GetAuditHistory' property='BeanSessionName' value='AddEditCashCon'/>
		<ffi:setProperty name="GetAuditHistory" property="AuditLogSessionName" value="TransactionHistory" />
	<ffi:process name="GetAuditHistory"/>
<% } %>
 <form name="" method="post" action='<ffi:urlEncrypt url="${viewCashConDoneURL}"/>' >
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="marginTop10"></div>
<div class="confirmationDetails">
	<span id="" class="sectionLabel"><s:text name="jsp.default_435"/></span>
	<span class="columndata"><ffi:getProperty name="AddEditCashCon" property="TrackingID" /></span>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
<div class="blockContent">
	<div class="blockRow">
		<span class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
		<span><ffi:getProperty name="AddEditCashCon" property="StatusName" /></span>
	</div>
</div>
<div class="marginTop10"></div>
<div class="blockWrapper">
	<div class="blockContent label150">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.common_47"/></span>
				<span class="columndata"  >
					<ffi:getProperty name="CashConCompany" property="CompanyName" />&nbsp;-&nbsp;
					<ffi:getProperty name="AddEditCashCon" property="DivisionName" />&nbsp;-&nbsp;
					<ffi:getProperty name="AddEditCashCon" property="LocationName" />
				</span>
			</div>
			<div class="inlineBlock">
				<ffi:setProperty name="AddEditCashCon" property="DateFormat" value="MM/dd/yyyy" />
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.common_75"/>:</span>
				<span class="columndata"><ffi:getProperty name="AddEditCashCon" property="SubmitDate" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				
						<span class="sectionsubhead sectionLabel" >
				<% if( session.getAttribute( "CashConView" ) == null ) { %>
							<s:text name="jsp.common_76"/>:
				<% } %>
						</span>
				<span class="columndata" >
					<% if( session.getAttribute( "CashConView" ) == null ) { %>
					    <ffi:object name="com.ffusion.tasks.user.GetUserById" id="GetUserById"/>
					    <ffi:setProperty name="GetUserById" property="ProfileId" value="${AddEditCashCon.SubmittedBy}"/>
					    <ffi:setProperty name="GetUserById" property="UserSessionName" value="CashConUser"/>
					    <ffi:process name="GetUserById"/>
					    <ffi:getProperty name="CashConUser" property="Name"/>
					    <ffi:removeProperty name="GetUserById"/>
					    <ffi:removeProperty name="CashConUser"/>
					<% } %>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_174"/>:</span>
				<span class="columndata">
					<ffi:getProperty name="AddEditCashCon" property="DivisionName"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_266"/>:</span>
				<span class="columndata">
					<ffi:getProperty name="AddEditCashCon" property="LocationName"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_86"/>:</span>
       			<span class="columndata"><ffi:getProperty name="CashConCompany" property="CompanyName" /> / <ffi:getProperty name="CashConCompany" property="CompanyID" /></span>
			</div>
		</div>
		<div class="blockRow">
			<%-- QTS 403846: Don't show NextCutOffTime if completed --%>
			<ffi:cinclude value1="${AddEditCashCon.Status}" value2="6" operator="notEquals" >
				<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_603"/>:</span>
					<span class="columndata">
					    <% if (isConcentration) { %>
						    <ffi:getProperty name="CashConCompany" property="ConcNextCutOffTime" />
					    <% } %>
					    <% if (isDisbursement) { %>
						    <ffi:getProperty name="CashConCompany" property="DisbNextCutOffTime" />
					    <% } %>
					</span>
				</div>
			</ffi:cinclude>
			<div class="inlineBlock">
				<%-- get the currency code for BC --%>
				<% if( _isBC ) { %>
				    <ffi:object id="GetCurrencyCode" name="com.ffusion.tasks.GetCurrencyCode" scope="session"/>
				    <ffi:setProperty name="GetCurrencyCode" property="BusinessID" value="${Business.Id}"/>
				    <ffi:process name="GetCurrencyCode" />
				<% } %>
				<span class="sectionsubhead sectionLabel"  >
					<% if (isConcentration) { %>
					<s:text name="jsp.default_484"/> (<% if( _isBC ) { %><ffi:getProperty name="GetCurrencyCode" property="CurrencyCode" /><% } else { %><ffi:getProperty name="SecureUser" property="BaseCurrency"/><% } %>)
					<% } %>
					<% if (isDisbursement) { %>
					<s:text name="jsp.default_485"/> (<% if( _isBC ) { %><ffi:getProperty name="GetCurrencyCode" property="CurrencyCode" /><% } else { %><ffi:getProperty name="SecureUser" property="BaseCurrency"/><% } %>)
					<% } %>
					:</span>
					<span class="columndata">
						<ffi:getProperty name="AddEditCashCon" property="AmountValue.CurrencyStringNoSymbol"/>
				</span>
			</div>
		</div>
		<ffi:cinclude value1="${AddEditCashCon.Status}" value2="6" operator="notEquals" >
		<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
		
				<% if (isConcentration) { %>					
					<ffi:cinclude value1="true" value2="${CashConCompany.ConcSameDayCutOffDefined}" operator="equals" >
						<div class="blockRow">
							<div class="inlineBlock" style="width: 50%">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_604"/>:</span>
								<span class="columndata>
									<ffi:getProperty name="CashConCompany" property="ConcNextSameDayCutOffTime" />
								</span>
							</div>
							<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel">
									<s:text name="jsp.default_602"/>:
								</span>
								<span class="columndata ltrow2_color">
									<ffi:cinclude value1="${AddEditCashCon.SameDayCashCon}" value2="true" operator="equals"><s:text name="jsp.cashcon_47"/></ffi:cinclude>
									<ffi:cinclude value1="${AddEditCashCon.SameDayCashCon}" value2="false" operator="equals"><s:text name="jsp.cashcon_48"/></ffi:cinclude>
									 
							</span>
							</div>
						</div>
					</ffi:cinclude>
				   
				<% } %>
				<% if (isDisbursement) { %>
						<ffi:cinclude value1="true" value2="${CashConCompany.DisbSameDayCutOffDefined}" operator="equals" >
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_604"/>:</span>
									<span class="columndata ltrow2_color">
											<ffi:getProperty name="CashConCompany" property="DisbNextSameDayCutOffTime" />
									</span>
								</div>
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_602"/>:
									</span>
									<span class="columndata">
										<ffi:cinclude value1="${AddEditCashCon.SameDayCashCon}" value2="true" operator="equals"><s:text name="jsp.cashcon_47"/></ffi:cinclude>
										<ffi:cinclude value1="${AddEditCashCon.SameDayCashCon}" value2="false" operator="equals"><s:text name="jsp.cashcon_48"/></ffi:cinclude>
									</span>
								</div>
							</div>
						</ffi:cinclude>	
					
				<% } %>
		</ffi:cinclude>
	</ffi:cinclude>
		
			<ffi:cinclude value1="${AddEditCashCon.Status}" value2="6" >
				<div class="blockRow">
					<span class="sectionsubhead sectionLabel"   ><s:text name="jsp.common_133"/></span>
					<ffi:setProperty name="AddEditCashCon" property="DateFormat" value="MM/dd/yyyy HH:mm" />
					<span class="columndata"><ffi:getProperty name="AddEditCashCon" property="ProcessedOnDate" /></span>
				</div>
			</ffi:cinclude>
	</div>
</div>
<% if( session.getAttribute( "CashConView" ) != null ) { %>
<%-- include batch Transaction History --%>
<ffi:include page="${PagesPath}common/include-view-transaction-history.jsp" />
			<% } %>
</form>
