<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page import="com.ffusion.beans.common.Currency" %>
<%@ page import="com.ffusion.beans.*" %>
<%@ page import="com.ffusion.beans.approvals.*" %>
<%@ page import="com.ffusion.beans.tw.*" %>
<%@ page import="com.ffusion.beans.banking.*" %>
<%@ page import="com.ffusion.beans.billpay.*" %>
<%@ page import="com.ffusion.beans.ach.*" %>
<%@ page import="com.ffusion.beans.wiretransfers.*" %>
<%@ page import="com.ffusion.util.HTMLUtil" %>
<div id="include-approvepayments-verify">

<%
try{
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;
if( session.getAttribute( "APPROVALS_BC" ) != null ) {
	String isBCStr = ( String )session.getAttribute( "APPROVALS_BC" );
	if( isBCStr.equalsIgnoreCase( "true" ) ) {
		_isBC = true;
	}
}
boolean _isBCConsumer = false;
if( session.getAttribute( "APPROVALS_BC_CONSUMER" ) != null ) {
	String isBCConsumerStr = ( String )session.getAttribute( "APPROVALS_BC_CONSUMER" );
	if( isBCConsumerStr.equalsIgnoreCase( "true" ) ) {
		_isBCConsumer = true;
	}
}

String[] approvalDecisions 	= com.ffusion.util.HTMLUtil.encode(request.getParameterValues( "approvalActions" ));
String[] isSelected 		= com.ffusion.util.HTMLUtil.encode(request.getParameterValues( "isSelected" ));
String[] itemIndex 		= com.ffusion.util.HTMLUtil.encode(request.getParameterValues( "itemIndex" ));
String[] reason 		= com.ffusion.util.HTMLUtil.encode(request.getParameterValues( "reason" ));
String[] notifyPrimaryContactStr = null;
if( _isBC && !_isBCConsumer ) {
	notifyPrimaryContactStr = request.getParameterValues( "notifyPrimaryContact" );
}
session.setAttribute( "Approvals_approvalDecisions", approvalDecisions );
session.setAttribute( "Approvals_isSelected", isSelected );
session.setAttribute( "Approvals_itemIndex", itemIndex );
session.setAttribute( "Approvals_reason", reason );
session.removeAttribute( "ApprovalsItemErrors" );
String[] reasonReorder = new String[ reason.length ];
String[] approvalDecisionsReorder = new String[ approvalDecisions.length ];
String[] notifyPrimaryContactReorder = null;
if( _isBC && !_isBCConsumer ) {
	notifyPrimaryContactReorder = new String[ notifyPrimaryContactStr.length ];
}
for( int i = 0; i < approvalDecisions.length; i++ ) {
	reasonReorder[ Integer.parseInt( itemIndex[i] ) ] = reason[ i ];
	approvalDecisionsReorder[ Integer.parseInt( itemIndex[i] ) ] = approvalDecisions[ i ];
	if( _isBC && !_isBCConsumer ) {
		notifyPrimaryContactReorder[ Integer.parseInt( itemIndex[i] ) ] = notifyPrimaryContactStr[ i ];
	}
}
approvalDecisions = approvalDecisionsReorder;
reason = reasonReorder;

boolean[] notifyPrimaryContact = null;
if( _isBC && !_isBCConsumer ) {
        notifyPrimaryContactStr = notifyPrimaryContactReorder;
	notifyPrimaryContact = new boolean[ notifyPrimaryContactReorder.length ];
	for( int i = 0; i < notifyPrimaryContactReorder.length; i++ ) {
		notifyPrimaryContact[i] = Boolean.valueOf( notifyPrimaryContactReorder[i] ).booleanValue();
	}
	session.setAttribute( "Approvals_notifyPrimaryContact", notifyPrimaryContact );
}

String[] itemIDs 		= request.getParameterValues( "itemID" );
java.util.HashMap itemIDSet = new java.util.HashMap();
for( int i = 0; i < itemIDs.length; i++ ) {
	itemIDSet.put( itemIDs[i], new Integer( i ) );
}
session.setAttribute( "Approvals_itemIDSet", itemIDSet );
session.removeAttribute( "itemID" );
%>

	<%-- get the accounts for the business in BC --%>
	<% if( _isBC && !_isBCConsumer ) { %>
	    <ffi:object id="GetBusinessAccounts" name="com.ffusion.tasks.accounts.GetAccountsById" scope="page"/>
	    <ffi:setProperty name="GetBusinessAccounts" property="AccountsName" value="BankingAccounts"/>
	    <ffi:setProperty name="GetBusinessAccounts" property="DirectoryId" value="${Business.Id}"/>
	    <ffi:process name="GetBusinessAccounts"/>
	    <ffi:removeProperty name="GetBusinessAccounts"/>
	<% } %>
	<%
	ApprovalsItems items = (ApprovalsItems) session.getAttribute(com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ITEMS);
	if (items != null) {
		String _strStartIndex = request.getParameter("startIndex");
		String _strRows = request.getParameter("submitRows");

		int startIndex = Integer.parseInt(_strStartIndex);
		int rows = Integer.parseInt(_strRows);
		session.setAttribute("startIndex", _strStartIndex);
		session.setAttribute("submitRows", _strRows);

		for (int count = startIndex - 1; count < startIndex - 1 + rows; count++) {
			ApprovalsItem approvalsItem = (ApprovalsItem) items.get(count);
			TWTransaction twTransaction = (TWTransaction)approvalsItem.getItem();
			FundsTransaction trans = (FundsTransaction)twTransaction.getTransaction();

			approvalsItem.set("ItemIndex", String.valueOf(count + 1));
			approvalsItem.set("Decision", approvalDecisions[count - startIndex + 1]);
			approvalsItem.set("ResultOfApproval", trans.getResultOfApproval());

			if (approvalDecisions[count - startIndex + 1].equalsIgnoreCase((String) session.getAttribute("APPROVALS_DECISION_REJECTED")) ||
					approvalDecisions[count - startIndex + 1].equalsIgnoreCase((String) session.getAttribute("APPROVALS_DECISION_APPROVED")) ||
					approvalDecisions[count - startIndex + 1].equalsIgnoreCase((String) session.getAttribute("APPROVALS_DECISION_RELEASE"))) {
				approvalsItem.set("SanitizedReason", HTMLUtil.encode(reason[count - startIndex + 1]));
			} else {
				approvalsItem.set("SanitizedReason", "");
			}

		}
	}
	%>
	<ffi:setProperty name="Accounts" property="Filter" value="All"/>
    

<form action='<ffi:urlEncrypt url="${FullPagesPath}${approval_confirm_jsp}" />' method="post" name="approvePaymentsVerifyForm"
id="approvePaymentsVerifyForm">
   <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
     
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		
		<tr><td>&nbsp;</td></tr>
	</tbody>
</table>
<s:url id="getApprovePaymentsVerifyUrl"
	value="/pages/jsp/approvals/includeApprovePaymentsGrid_verify.action?" />
<sjg:grid
	id="approvePaymentsVerifyGrid" caption="" dataType="json"
	sortable="true"  
	href="%{getApprovePaymentsVerifyUrl}" pager="false" gridModel="gridModel"
	rowList="%{#session.StdGridRowList}" 
	rowNum="%{#session.StdGridRowNum}" 
	rownumbers="false" shrinkToFit="true"
	scroll="false" scrollrows="true" 
	onGridCompleteTopics="approvePaymentsVerifyGridComplete">
	<sjg:gridColumn name="map.ItemIndex" index="itemIndex" title="%{getText('jsp.common_1')}" sortable="false" width="10"
					cssClass="ui-state-default jqgrid-rownum" formatter="ns.approval.formatItemIndex" />
	<sjg:gridColumn name="item.submissionDateTimeDisplay" index="date" title="%{getText('jsp.default_137')}" sortable="false" width="90" />
	<sjg:gridColumn name="item.displayTypeAsString" index="type" title="%{getText('jsp.common_9')}" sortable="false" width="90" />
	<sjg:gridColumn name="Accounts" index="Accounts" title="Accounts" sortable="true" width="150" formatter="ns.approval.formatAccountDisplay" />
	<sjg:gridColumn name="map.FromAct" index="fromAct" title="%{getText('jsp.default_217')}"	sortable="false" width="180" formatter="ns.approval.formatAccount"  hidden="true"/>
	<sjg:gridColumn name="map.ToAct" index="toAct" title="%{getText('jsp.default_424')}" sortable="false" width="180" formatter="ns.approval.formatAccount"  hidden="true"/>
	<sjg:gridColumn name="submittingUserName" index="userName" title="%{getText('jsp.default_398')}"	sortable="false" width="100"  formatter="ns.approval.submittingUserName"/>
	<sjg:gridColumn name="map.Decision" index="decision" title="%{getText('jsp.default_160')}" hidden="true" sortable="false" />
	<sjg:gridColumn name="item.transaction.resultOfApproval" index="resultOfApproval" title="%{getText('jsp.default_359')}" hidden="true" sortable="false" />
	<sjg:gridColumn name="map.SanitizedReason" index="sanitizedReason" title="%{getText('jsp.common_144')}" hidden="true" sortable="false" />
	
</sjg:grid>
	<script>
		ns.common.disableSortableRows("#approvePaymentsVerifyGrid");
	</script>
</form>
<div class="marginTop10 marginBottom10" align="center">
	<sj:a id="cancelApprovePaymentsVerifyFormLink"
		button="true" title="%{getText('jsp.default_83')}" onClickTopics="CancelApprovePaymentsVerify"
		><s:text name="jsp.default_82"/></sj:a>
	<sj:a id="submitApprovePaymentsVerifyFormLink"
		formIds="approvePaymentsVerifyForm" 
		targets="approvalGroupSummary" button="true"
		title="%{getText('jsp.default_395.1')}" validate="false" 
		validateFunction="customValidation"
		onBeforeTopics="submitApprovePaymentsVerifyFormBefore"
		onCompleteTopics="submitApprovePaymentsVerifyFormComplete"
		onSuccessTopics="submitApprovePaymentsVerifyFormSuccess"
		effectDuration="1500"><s:text name="jsp.default_395"/></sj:a></div>
		
</div>

<%
}catch(Exception e){
%>
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.SERVICE_ERROR)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.TASK_ERROR%>"/>
	<ffi:process name="ThrowException"/>
<%
}
 %>