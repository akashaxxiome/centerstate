<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../jsp/common/wire_labels.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%
String wireDest = "";
String wireDom = WireDefines.WIRE_DOMESTIC;
String wireInt = WireDefines.WIRE_INTERNATIONAL;
String wireTemp = WireDefines.WIRE_TYPE_TEMPLATE;
String wireRecTemp = WireDefines.WIRE_TYPE_RECTEMPLATE;
String curType = "";
String curSource = "";
String restricted = "false";
String accountDisplayText = "";
boolean showTemplateFields = false;
%>
<ffi:getProperty name="WireTransfer" property="WireDestination" assignTo="wireDest"/>
<ffi:getProperty name="WireTransfer" property="WireType" assignTo="curType"/>
<ffi:getProperty name="WireTransfer" property="WireSource" assignTo="curSource"/>
<ffi:setProperty name="WireTransfer" property="DateFormat" value="${UserLocale.DateFormat}"/>
<ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText" assignTo="accountDisplayText"/>
<%
if(accountDisplayText == null && accountDisplayText.equalsIgnoreCase("Restricted")) {
	restricted = "true";
}
%>

<%-- ---- CONFIRMATION NUMBERS ---- --%>
<div class="confirmationDetails" style="width:50%; float:left">
	<span><s:text name="jsp.default_435"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="TrackingID"/></span>
</div>
<div class="confirmationDetails" style="width:49%; float:left">
	<span><s:text name="jsp.default_46"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="ReferenceNumber"/></span>
</div>
<div class="marginBottom10 clearBoth"></div>
<div class="confirmationDetails" style="width:50%; float:left">
	<span><s:text name="jsp.default_229"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="ConfirmationNum"/></span>
</div>
<div class="confirmationDetails" style="width:49%; float:left">
	<span><s:text name="jsp.common_79"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="FedConfirmationNum"/></span>
</div>
<div class="marginBottom10 clearBoth"></div>
<% if (curType.equals(wireTemp) || curType.equals(wireRecTemp) || curSource.equals("TEMPLATE")) { %>
	<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_413"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" id="viewWireTemplateNameLabel"><s:text name="jsp.default_416"/>: </span>
				<span class="columndata" id="viewWireTemplateName"><ffi:getProperty name="WireTransfer" property="WireName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" id="viewWireTemplateIDLabel"><s:text name="jsp.default_412"/>: </span>
				<span class="columndata" id="viewWireTemplateID"><ffi:getProperty name="WireTransfer" property="TemplateID"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" id="viewWireTemplateNickNameLabel"><s:text name="jsp.default_417"/>: </span>
				<span class="columndata" id="viewWireTemplateNickName"><ffi:getProperty name="WireTransfer" property="NickName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" id="viewWireTemplateLimitLabel"><s:text name="jsp.default_415"/>: </span>
				<span class="columndata" id="viewWireTemplateLimit"><ffi:getProperty name="WireTransfer" property="DisplayWireLimit"/></span>
			</div>
		</div>
	</div>
	</div>
<% } %>
<div  class="blockHead toggleClick">Beneficiary details <span class="sapUiIconCls icon-positive"></span></div>
<div class="toggleBlock hidden">
	<div class="blockWrapper">
	<div  class="blockHead" id="viewWireBeneficiaryInfoLabel"><s:text name="jsp.default_70"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireBeneficiaryAccountNumberLabel" class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_NUMBER %>: </span>
			    <span id="viewWireBeneficiaryAccountNumberValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.AccountNum"/></span>
			</div>
			<div class="inlineBlock">
				 <span id="viewWireAccountTypeLabel" class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_TYPE %>: </span>
			    <span id="viewWireAccountTypeValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.AccountTypeDisplayName"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				 <span id="viewWireBeneficiaryNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_71"/>: </span>
			    <span id="viewWireBeneficiaryNameValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Name"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewWireBeneficiaryNickNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_294"/>: </span>
			    <span id="viewWireBeneficiaryNickNameValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.NickName"/></span>
			</div>
		</div>
		
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireAddress1Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>: </span>
			    <span id="viewWireAddress1Value" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewWireAddress2Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>: </span>
			    <span id="viewWireAddress2Value" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street2"/></span>
			</div>
		</div>
		
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				 <span id="viewWireCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>: </span>
				    <span id="viewWireCityValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.City"/></span>
			</div>
			<ffi:cinclude value1="${IfStatesExists}" value2="true">
				<div class="inlineBlock">
				    <span id="viewWireStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_387" />: </span>
				    <span id="viewWireStateValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.StateDisplayName"/></span>
				</div>
			</ffi:cinclude>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				 <span id="viewWireZipCodeLabel"  class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>: </span>
			     <span id="viewWireZipCodeValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.ZipCode"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewWireCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>: </span>
			    <span id="viewWireCountryValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.CountryDisplayName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_110"/>: </span>
			    <span id="viewWireContactValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Contact"/></span>
			</div>
			<div class="inlineBlock">
				<span id="veiwWireScopeLabel"  class="sectionsubhead sectionLabel"><s:text name="jsp.default_72"/>: </span>
			    <span id="veiwWireScopeValue" class="columndata"><ffi:setProperty name="WirePayeeScopes" property="Key" value="${WireTransfer.WirePayee.PayeeScope}"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></span>
			</div>
		</div>
	</div>
</div>

<div class="blockWrapper">
<div  class="blockHead" id="viewWireBeneficiaryInfoLabel"><s:text name="jsp.default_69"/></div>
<div class="blockContent">
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
		    <span id="viewWireBankNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_63"/>: </span>
		    <span id="viewWireBankNameValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.BankName"/></span>
		</div>
		<div class="inlineBlock">
		    <span id="viewWireBankAddress1Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>: </span>
		    <span id="viewWireBankAddress1Value" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street"/></span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
		    <span id="viewWireBankAddress2Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>: </span>
		    <span id="viewWireBankAddress2Value" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street2"/></span>
		</div>
		<div class="inlineBlock">
		    <span id="viewWireBankCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>: </span>
		    <span id="viewWireBankCityValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.City"/></span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
		    <span id="viewWireBankStateLabel"  class="sectionsubhead sectionLabel"><s:text name="jsp.default_387"/>: </span>
		    <span id="viewWireBankStateValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.State"/></span>
		</div>
		<div class="inlineBlock">
		    <span id="viewWireBankZipCodeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>: </span>
		    <span id="viewWireBankZipCodeValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.ZipCode"/></span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
		    <span id="viewWireBankCountryCodeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>: </span>
		    <span id="viewWireBankCountryCodeValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.CountryDisplayName"/></span>
		</div>
		<div class="inlineBlock">
		    <span id="viewWireFEDLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_201"/>: </span>
		    <span id="viewWireFEDValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingFedWire"/></span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
		    <span id="viewWireSWIFTLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_401"/>: </span>
		    <span id="viewWireSWIFTLValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingSwift"/></span>
		</div>
		<div class="inlineBlock">
		    <span id="viewWireRoutingChipsLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_99"/>: </span>
		    <span id="viewWireRoutingChipsValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingChips"/></span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
		    <span id="viewWireRoutingOtherLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_289"/>: </span>
		    <span id="viewWireRoutingOtherValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingOther"/></span>
		</div>
		<div class="inlineBlock">
		    <span id="viewWireIBANLabel" class="sectionsubhead sectionLabel"><%= LABEL_IBAN %>: </span>
		    <span id="viewWireIBANLValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.IBAN"/></span>
		</div>
	</div>
	 <ffi:cinclude value1="${WireTransfer.WirePayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
		<div class="blockRow">
		    <span id="viewCorresPondAcountNumberLabe" class="sectionsubhead sectionLabel"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
		    <span id="viewCorresPondAcountNumberValue" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.CorrespondentBankAccountNumber"/></span>
		</div>
	</ffi:cinclude>
</div>
</div>
<ffi:cinclude value1="${WireTransfer.WirePayee.IntermediaryBanks}" value2="" operator="notEquals" >
    <ffi:cinclude value1="${WireTransfer.WirePayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
    	<div class="blockWrapper">
    	<% int counter = 0; %>
	     <ffi:list collection="WireTransfer.WirePayee.IntermediaryBanks" items="Bank1">				
			<ffi:cinclude value1="${Bank1.Action}" value2="del" operator="notEquals" >
    		<div  class="blockHead"><!--L10NStart-->Intermediary Banks<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_251"/>: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="BankName"/></span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_201"/>: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_401"/>: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/></span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_99"/>: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_289"/>: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/></span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/></span>
					</div>
				</div>
				<div class="blockRow">
						<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
		    			<span class="columndata">
		    				<% if(counter == 0) { %>
							<% } else { %>
								<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
							<% } counter++; %>
		    			</span>
				</div>
			</div>
		</ffi:cinclude>
    	</ffi:list>	
</div>
   </ffi:cinclude>
</ffi:cinclude>
</div>		

<div class="blockWrapper">
	<div  class="blockHead" id="viewWireDebitInfoLabel"><!--L10NStart--><s:text name="jsp.default_157"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireDebitAccountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_147"/>: </span>
			    <span id="viewWireDebitAccountValue" class="columndata">
					<ffi:cinclude value1="${WireTransfer.accountNickname}" value2="" operator="notEquals">
						<ffi:getProperty name="WireTransfer" property="accountNickname"/>
					</ffi:cinclude>

					<ffi:cinclude value1="${WireTransfer.accountNickname}" value2="" operator="equals">
						<ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/>
					</ffi:cinclude>
				</span>
			</div>
			<ffi:setProperty name="temp_amount" value="Amount"/>
			<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_TEMPLATE %>" operator="equals">
				    <ffi:setProperty name="temp_amount" value="Template Limit"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="equals">
				    <ffi:setProperty name="temp_amount" value="Template Limit"/>
			</ffi:cinclude>
		</div>
		<% if (wireDest.equals(wireDom)) { %>
		    <div class="blockRow">
				<span id="viewWireAmountLabel" class="sectionsubhead sectionLabel"><ffi:getProperty name="temp_amount"/><ffi:removeProperty name="temp_amount"/>: </span>
				<span id="viewWireAmountValue" class="columndata"><ffi:getProperty name="WireTransfer" property="DisplayAmount"/></span>
		    </div>
	    <%  } else { %>
	    <div class="blockRow">
	    	<div class="inlineBlock" style="width: 50%">
				<span  id="viewWireAmountOriginalLabel" class="sectionsubhead sectionLabel" nowrap><s:text name="jsp.default_499"/> <ffi:getProperty name="temp_amount"/>: </span>
				<span id="viewWireAmountOriginalValue" class="columndata"><ffi:getProperty name="WireTransfer" property="DisplayOrigAmount"/> (<ffi:getProperty name="WireTransfer" property="OrigCurrency"/>)</span>
			</div>
			<div class="inlineBlock">
				<span  id="ViewWireINUSDLabel" class="sectionsubhead sectionLabel" nowrap><ffi:getProperty name="temp_amount"/> <s:text name="jsp.default_491"/>: </span>
				<span id="ViewWireINUSDValue" class="columndata"><ffi:getProperty name="WireTransfer" property="DisplayAmount"/></span>
	    	</div>
	    </div>
		<%  } %>
		<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_TEMPLATE %>" operator="notEquals">
		    <ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="notEquals">
			<div class="blockRow">
			    <span id="viewWireValueDateLabel" class="sectionsubhead sectionLabel"><%= wireDest.equals(wireDom) ? "Value Date" : "Processing Date" %>: </span>
			    <span id="viewWireValueDateValue" class="columndata"><ffi:getProperty name="WireTransfer" property="DueDate"/></span>
			</div>
			<% if (wireDest.equals(wireInt)) { %>
			<div class="blockRow">
			    <span id="viewWireSettlementDateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_380"/>: </span>
			    <span id="viewWireSettlementDateValue" class="columndata"><ffi:getProperty name="WireTransfer" property="SettlementDate"/></span>
			</div>
			<%  } %>
			<div class="blockRow">
			    <span id="viewWireDateOFPostLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_141"/>: </span>
			    <span id="viewWireDateOfPostValue" class="columndata"><ffi:getProperty name="WireTransfer" property="DateToPost"/></span>
			</div>
		    </ffi:cinclude>
		</ffi:cinclude>
		
		<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="equals">
			<s:set var="isRecTemplate">true</s:set>
		</ffi:cinclude>
		<ffi:cinclude value1="${WireTransfer.Type}" value2="<%= String.valueOf(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) %>" operator="equals">
		
		<s:if test="%{#attr.viewRecurringModel || #isRecTemplate=='true'}">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
			    <span id="viewWireFreqLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_215"/>: </span>
			    <span id="viewWireFreqValue" class="columndata"><ffi:getProperty name="WireTransfer" property="Frequency"/></span>
		    </div>
		    <div class="inlineBlock">
			    <span id="viewWiresectionsubhead sectionLabelLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/>: </span>
			    <span id="viewWireUnlimitedHeadLabel" class="columndata">
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="equals">
					<s:text name="jsp.default_446"/>
			    </ffi:cinclude>
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="notEquals">
					<ffi:getProperty name="WireTransfer" property="NumberTransfers"/>
			    </ffi:cinclude>
			    </span>
		    </div>
		</div>
		<s:if test="%{#isRecTemplate!='true'}">
		<div class="blockRow">
		    <div class="inlineBlock" style="width: 50%">
			    <span id="viewWireEndDateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default.end.date"/> : </span>
			    <span id="viewWireEndDateValue" class="columndata">
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="equals">
			    		<s:text name="jsp.default.not.applicable"/>
			    </ffi:cinclude>
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="notEquals">
			    		<ffi:getProperty name="WireTransfer" property="EndDate"/>
			    </ffi:cinclude>
			    
			    </span>
		    </div>
			
		    <div class="inlineBlock">
			    <span id="viewWireFuturePaymentsLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default.future.payments"/>: </span>
			    <span id="viewWireFuturePaymentsValue" class="columndata">			    
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="equals">
					<s:text name="jsp.default.not.applicable"/>
			    </ffi:cinclude>
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="notEquals">
					<ffi:getProperty name="WireTransfer" property="NumberOfPendingTransfers"/>
			    </ffi:cinclude>			    
			    </span>
		    </div>
			
		</div>
		</s:if>
		</s:if>
		</ffi:cinclude>
				
<% if (wireDest.equals(wireDom)) { %>
<% } else { %>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="viewDebitCurrencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_155"/>: </span>
			<span id="viewDebitCurrencyValue" class="columndata"><ffi:getProperty name="WireTransfer" property="OrigCurrency"/></span>
		</div>
		<div class="inlineBlock">
			<span id="viewDebitExchangeRateLabel" class="sectionsubhead sectionLabel" nowrap><s:text name="jsp.default_156"/>: </span>
			<span id="viewDebitExchangeRateValue" class="columndata"><ffi:getProperty name="WireTransfer" property="ExchangeRate"/></span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span id="viewMathRuleLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_278"/>: </span>
			<span id="viewMathRuleValue" class="columndata"><ffi:getProperty name="WireTransfer" property="MathRule"/>: </span>
		</div>
		<div class="inlineBlock">
			<span id="ViewContractLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_113"/>: </span>
			<span id="ViewContractValue" class="columndata"><ffi:getProperty name="WireTransfer" property="ContractNumber"/></span>
		</div>
	</div>
	</div>
</div>	
<div class="blockWrapper">	
	<div  class="blockHead"><s:text name="jsp.default_504"/></div>
	<div class="blockContent">
		<div class="blockRow">
			 <span id="viewPayementCurrencyLabel" width="115" class="sectionsubhead sectionLabel"><s:text name="jsp.default_319"/>: </span>
	    	 <span id="viewPayementCurrencyValue" width="228" class="columndata"><ffi:getProperty name="WireTransfer" property="PayeeCurrencyType"/></span>
		</div>
	</div>
		<% } %>
</div>		   
<div class="blockWrapper">
	<div  class="blockHead"><%= wireDest.equals(wireDom) ? "Reference for Beneficiary" : "Sender's Reference" %>: <ffi:getProperty name="WireTransfer" property="Comment"/></div>
	<div  class="blockHead"><s:text name="jsp.default_307"/></div>
	<div class="blockContent">
		 <% String info = ""; %>
		<div class="blockRow">
			<ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo1"/>
		    <ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo2}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo2"/></ffi:cinclude>
		    <ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo3}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo3"/></ffi:cinclude>
	    	<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo4}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo4"/></ffi:cinclude>
		</div>
	</div>
	<div  class="blockHead"><s:text name="jsp.default_64"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<ffi:cinclude value1="${WireTransfer.BankToBankInfo2}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo2"/></ffi:cinclude>
		    <ffi:cinclude value1="${WireTransfer.BankToBankInfo3}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo3"/></ffi:cinclude>
		    <ffi:cinclude value1="${WireTransfer.BankToBankInfo4}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo4"/></ffi:cinclude>
		    <ffi:cinclude value1="${WireTransfer.BankToBankInfo5}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo5"/></ffi:cinclude>
		    <ffi:cinclude value1="${WireTransfer.BankToBankInfo6}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo6"/></ffi:cinclude>
		</div>
	</div>
</div>	
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_78"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_283"/>: </span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>: </span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress1"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_21" />: </span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>: </span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress3"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_19"/>: </span>
			<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAccount"/></span>
		</div>
	</div>
</div>
<table width="750" border="0" cellspacing="0" cellpadding="0">
    <tr>
	<td class='<ffi:getProperty name="wire_details_background_color"/>'>
	    <div align="center">
		<br>
		<ffi:cinclude value1="<%=restricted%>" value2="true" operator="notEquals">
			<s:if test="%{isVirtualInstance !='true'}">
				<s:include value="%{#session.PagesPath}/common/include-view-wire-history.jsp"/>
			</s:if>
		</ffi:cinclude>
		<br>
		<% if( session.getAttribute( "wire_details_post_jsp" ) == null ) { %>
		    <ffi:setProperty name="wireViewDoneURL" value="payments/wiretransfers.jsp"/>
		    <ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= wireTemp %>" operator="equals">
			<ffi:setProperty name="wireViewDoneURL" value="payments/wiretemplates.jsp"/>
		    </ffi:cinclude>
		    <ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= wireRecTemp %>" operator="equals">
			<ffi:setProperty name="wireViewDoneURL" value="payments/wiretemplates.jsp"/>
		    </ffi:cinclude>
		    <ffi:cinclude value1="${collectionName}" value2="WiresRelease" operator="equals">
			<ffi:setProperty name="wireViewDoneURL" value="payments/wiresrelease.jsp?DontInitialize=true" URLEncrypt="true" />
		    </ffi:cinclude>
		    <ffi:cinclude value1="${collectionName}" value2="WireReportRecords" operator="equals">
			<ffi:setProperty name="wireViewDoneURL" value="payments/wiretransferreport.jsp?haveReport=false&save=true&mod=false&DontInitializeReport=true" URLEncrypt="true" />
		    </ffi:cinclude>
		    <ffi:cinclude value1="${collectionName}" value2="WireBatch.Wires" operator="equals">
			<ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_DOMESTIC %>" operator="equals">
			    <ffi:setProperty name="wireViewDoneURL" value="payments/wirebatchview.jsp?DontInitialize=true" URLEncrypt="true" />
			</ffi:cinclude>
			<ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
			    <ffi:setProperty name="wireViewDoneURL" value="payments/wirebatchintview.jsp?DontInitialize=true" URLEncrypt="true" />
			</ffi:cinclude>
		    </ffi:cinclude>
		    <ffi:cinclude value1="${collectionName}" value2="WireTransfersByBatchId" operator="equals">
			<ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_DOMESTIC %>" operator="equals">
			    <ffi:setProperty name="wireViewDoneURL" value="payments/wirebatchview.jsp?DontInitialize=true" URLEncrypt="true" />
			</ffi:cinclude>
			<ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
			    <ffi:setProperty name="wireViewDoneURL" value="payments/wirebatchintview.jsp?DontInitialize=true" URLEncrypt="true" />
			</ffi:cinclude>
		    </ffi:cinclude>
		    <%
			StringBuffer tempURLBuf = new StringBuffer( ( String )session.getAttribute( "SecurePath" ) );
			tempURLBuf.append( ( String )session.getAttribute( "wireViewDoneURL" ) );
			session.setAttribute( "wireViewDoneURL", tempURLBuf.toString() );
		    %>
		<% } else { %>
		    <ffi:setProperty name="wireViewDoneURL" value="${wire_details_post_jsp}"/>
		<% } %>
		<% if( session.getAttribute( "WireBatch" ) != null &&
			    session.getAttribute( "ApprovalsViewTransactionDetails") != null ) { %>
			<ffi:setProperty name="wireViewDoneURL" value="${SecurePath}${approval_view_wirebatch_jsp}"/>
		<% } %>
		<!--
		<input type="Button" value="<s:text name="jsp.default_175"/>" class="submitbutton" onClick="javascript:document.location='<ffi:getProperty name="wireViewDoneURL"/>'">
		<div align="center">
			<sj:a button="true" title="DONE" onClickTopics="closeDialog">DONE</sj:a>
		</div>
		-->
		<ffi:removeProperty name="wireViewDoneURL"/>
		<ffi:removeProperty name="collectionName"/>
	    </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-positive").addClass("icon-negative")
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-positive").removeClass("icon-negative")
			}
		});
	});

	function viewModel() {
	var urlString = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/viewWireTransferAction.action?ViewRecurringModel=true&transType=RECURRING&recurringId=${WireTransfer.recurringID}&IsVirtualInstance=${WireTransfer.IsVirtualInstance}'/>";

	$.ajax({
		url: urlString,
		success: function(data){
			ns.common.closeDialog();
			$('#viewRecurringWireModelDetailsDialogID').html(data).dialog('open');
		}
	});
}
</script>