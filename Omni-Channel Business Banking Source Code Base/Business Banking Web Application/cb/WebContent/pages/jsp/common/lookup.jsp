<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%
   String source = request.getParameter("source");
   String term = request.getParameter("term");
   String collectionKey = request.getParameter("collectionKey");
   String excludeItem = request.getParameter("excludeItem");
   String element = request.getParameter("element");
   String gridId = request.getParameter("element") + "LookupGrid";
   String cancelButtonId = element + "LookupCancelButton";
   String searchButtonId = element + "LookupSearchBox";
%>
<ffi:setProperty name="source" value="<%= source %>"/>
<ffi:setProperty name="collectionKey" value="<%= collectionKey %>"/>
<ffi:setProperty name="excludeItem" value="<%= excludeItem %>"/>
<ffi:setProperty name="term" value="<%= term %>"/>
<ffi:setProperty name="gridId" value="<%= gridId %>"/>
<ffi:setProperty name="cancelButtonId" value="<%= cancelButtonId %>"/>
<ffi:setProperty name="element" value="<%= element %>"/>
<div id="lookupGridPortletContainer">
	<div id="searchContainer" align="center" style="padding: 5px 5px 5px 5px;">
		<input type="text" class="txtbox ui-widget-content ui-corner-all" size="50" id="<%=com.ffusion.util.HTMLUtil.encode(searchButtonId) %>" element="<%= com.ffusion.util.HTMLUtil.encode(element) %>" excludeitem="<%= com.ffusion.util.HTMLUtil.encode(excludeItem)%>" collectionkey="<%= com.ffusion.util.HTMLUtil.encode(collectionKey)%>" inputtype="lookupboxSearchField" />
	</div>
<ffi:setProperty name="tempURL" value="${source}?collectionKey=${collectionKey}&term=${term}&excludeItem=${excludeItem}" URLEncrypt="false"/>
<s:url id="lookupGridDataURL" value="%{#session.tempURL}" escapeAmp="false"/>
<sjg:grid
     id="%{#session.gridId}"
	 sortable="true" 
     dataType="json" 
     href="%{#session.tempURL}"
     pager="true"
     gridModel="gridModel"
	 rowList="%{#session.StdGridRowList}" 
	 rowNum="%{#session.StdGridRowNum}" 
	 rownumbers="false"
     navigator="true"
	 navigatorAdd="false"
	 navigatorDelete="false"
	 navigatorEdit="false"
	 navigatorRefresh="false"
	 navigatorSearch="false"
	 navigatorView="false"
	 shrinkToFit="true"
	 footerrow="true"
	 scroll="false"
	 scrollrows="true"
     onGridCompleteTopics="topics.ux.lookupbox.gridOnComplete"
	 onSelectRowTopics="topics.ux.lookupbox.onSelectRow"
     >
	<sjg:gridColumn name="ID" index="ID" title="Id" sortable="false" hidden="true" hidedlg="true"/>   
    <sjg:gridColumn name="displayTextNickNameCurrency" index="displayTextNickNameCurrency" title="Account" sortable="false" />    
	<sjg:gridColumn name="currencyCode" index="currencyCode" title="Currency" sortable="false" hidden="true" hidedlg="true"/>   	 
</sjg:grid>
	<div  align="center" style="padding: 5px 5px 5px 5px;">
		<sj:a id="%{#session.cancelButtonId}" buttontype="lookupboxButton" element="<%= element%>" button="true" onClickTopics="closeDialog">Cancel</sj:a>
	</div>
</div>