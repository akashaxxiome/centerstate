<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.*" %>
<%@ page import="com.ffusion.beans.accounts.*" %>
<%@ page import="com.ffusion.beans.approvals.*" %>
<%@ page import="com.ffusion.beans.tw.*" %>
<%@ page import="com.ffusion.beans.banking.*" %>
<%@ page import="com.ffusion.beans.billpay.*" %>
<%@ page import="com.ffusion.beans.wiretransfers.*" %>
<%@ page import="com.ffusion.approvals.constants.IApprovalsConsts" %>
<%@ page import="java.util.*" %>
<%@ page import="com.ffusion.beans.cashcon.*" %>

<div id="include-approvepayments">

<%-- clean up the session --%>
<ffi:removeProperty name="IncludeViewPayment"/>
<ffi:removeProperty name="IncludeViewTransfer"/>
<ffi:removeProperty name="WireTransfer"/>
<ffi:removeProperty name="WireBatch"/>
<ffi:removeProperty name="Wires"/>
<ffi:removeProperty name="collectionToShow"/>
<ffi:removeProperty name="ACHBatch"/>
<ffi:removeProperty name="TaxPayment"/>
<ffi:removeProperty name="TaxForm"/>
<ffi:removeProperty name="TaxFields"/>
<ffi:removeProperty name="CashCon"/>
<ffi:removeProperty name="payment_details_post_jsp"/>
<ffi:removeProperty name="transfer_details_post_jsp"/>
<ffi:removeProperty name="wire_details_post_jsp"/>
<ffi:removeProperty name="ach_details_post_jsp"/>
<ffi:removeProperty name="wirebatch_details_post_jsp"/>
<ffi:removeProperty name="tax_details_post_jsp"/>
<ffi:removeProperty name="child_support_details_post_jsp"/>
<ffi:removeProperty name="cashcon_details_post_jsp"/>
<ffi:removeProperty name="ApprovalsViewTransactionDetails"/>

<s:set var="showNote" value="%{'false'}"/>

<ffi:cinclude value1="${SameDayCashConEnabled}" value2="" >
	<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
	<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayCashConEnabled"/>
	<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.cashcon.sameday.support"/>
	<ffi:process name="GetBPWProperty"/>
</ffi:cinclude>

<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true">
	<s:set var="showNote" value="%{'true'}"/>
</ffi:cinclude>

<ffi:cinclude value1="${SameDayExtTransferEnabled}" value2="true">
<s:set var="showNote" value="%{'true'}"/>
</ffi:cinclude>

<style type="text/css">
.approvals-left-cell
{
	border-left-color: inherit;
	border-left-style: solid;
	border-left-width: 1px;
	border-right: none;
}

.disable-input
{
	background: #dddddd;
}
</style>
<ffi:cinclude value1="${Business.PerAccountApprovalMode}" value2="1" operator="equals">
<script>
var perAccount=true;
</script>
</ffi:cinclude>
<script src="<s:url value='/web'/>/js/common/include-approvepayments_grid.js" type="text/javascript"></script>
<script language="JavaScript">
<!--
$(function(){
	// Update the ApprovalSmallPanel in leftbar of the Page
	$.publish('refreshApprovalSmallPanel');

	$("#selectOption").selectmenu({width: 160});
});
ns.approval.validateSubmission = function( thisForm, isBC )
{
	if (thisForm.approvalActions == null) {
		// no rows
		window.alert(js_error_noDataToSubmit);
		return false;
	}
	var valid = false;

	// if we have multiple rows in the table, then approvalActions will be an array of select objects
	// if we have one row in the table, then approvalActions will be one select object
	// see if we have one object or an array of objects
	if (typeof( thisForm.approvalActions[ 0 ].length ) == "undefined") {
		// one row
		valid = true;
		thisForm.isSelected.value = true;
	} else {
		for (var i = 0; i < thisForm.approvalActions.length; i++) {
			valid = true;
			thisForm.isSelected[ i ].value = true;
		}
	}

	if (isBC == "true") {
		if (typeof( thisForm.notifyPrimaryContactCheckbox.length ) == "undefined") {
			// one row
			if (thisForm.notifyPrimaryContactCheckbox.checked) {
				thisForm.notifyPrimaryContact.value = "true";
			} else {
				thisForm.notifyPrimaryContact.value = "false";
			}
		} else {
			for (var i = 0; i < thisForm.notifyPrimaryContactCheckbox.length; i++) {
				idOfRejectDisplay = "displayReason" + i
				displayObj = document.getElementById(idOfRejectDisplay);
				if (thisForm.notifyPrimaryContactCheckbox[i].checked && displayObj.style.visibility == 'visible') {
					thisForm.notifyPrimaryContact[i].value = "true";
				} else {
					thisForm.notifyPrimaryContact[i].value = "false";
				}
			}
		}
	}

	if (valid == false) {
		window.alert(js_error_chooseAction);
		return false;
	} else {
		return true;
	}
};

ns.approval.decision_approved  = "<%= com.ffusion.approvals.constants.IApprovalsConsts.DECISION_APPROVED %>";
ns.approval.decision_release   = "<%= com.ffusion.approvals.constants.IApprovalsConsts.DECISION_RELEASE %>";
ns.approval.decision_rejected  = "<%= com.ffusion.approvals.constants.IApprovalsConsts.DECISION_REJECTED %>";
ns.approval.decision_hold 	   = "<%= com.ffusion.approvals.constants.IApprovalsConsts.DECISION_HOLD %>";

ns.approval.checkIsHold = function(actionStr)
{
	var approvalStr = ns.approval.decision_approved;
	var releaseStr  = ns.approval.decision_release;
	var rejectStr   = ns.approval.decision_rejected;

	return actionStr != approvalStr && actionStr != releaseStr && actionStr != rejectStr ;
};

// --></script>



	<ffi:object id="GetPendingApprovalsCount" name="com.ffusion.tasks.approvals.GetPendingApprovalsCount" scope="session"/>
	<ffi:process name="GetPendingApprovalsCount" />
	<ffi:cinclude value1="-1" value2="${PendingApprovalsCount}" operator="equals">
		<p class="sectionsubhead"><s:text name="jsp.common_163"/></p>
	</ffi:cinclude>
	<ffi:cinclude value1="0" value2="${PendingApprovalsCount}" operator="notEquals">

			<ffi:object id="GetPendingApprovals" name="com.ffusion.tasks.approvals.GetPendingApprovals" scope="session"/>
			<ffi:setProperty name="GetPendingApprovals" property="DateFormat" value="M/dd/yyyy"/>
			<ffi:process name="GetPendingApprovals" />


	</ffi:cinclude>


<ffi:cinclude value1="0" value2="${PendingApprovalsCount}" operator="equals">
	<div class="noPortletMsgCls"  align="center" ><s:text name="jsp.approval_no_pending_transactions" /></div>
</ffi:cinclude>
<ffi:cinclude value1="0" value2="${PendingApprovalsCount}" operator="notEquals">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" width="100%">
				<table border="0" cellspacing="3" cellpadding="10">
					 <tr>
						<td class="sectionsubhead" align="left" >&nbsp;&nbsp;&nbsp; <s:text name="jsp.common_27"/> &nbsp;
							<select id="selectOption" class="txtbox" name="ApproveOptions">
								<option value="Hold"><s:text name="jsp.default_228"/></option>
								<option value="Approved or Release"><s:text name="jsp.common_29"/></option>
								<option value="Rejected"><s:text name="jsp.common_139"/></option>
							</select>
						</td>
						<td class="sectionsubhead" nowrap>
							<sj:a id="applyButtonID" button="true" onClickTopics="onApplyButtonClick" title="%{getText('jsp.common_20')}"><s:text name="jsp.common_20"/></sj:a>
							<script type="text/javascript">
								$.subscribe('onApplyButtonClick', function(event,data) {
									ns.approval.applyToAllComboBoxes('selectOption');
								});
							</script>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</ffi:cinclude>

 <script type="text/javascript">


$.subscribe('onSubmitCheckTopics', function(event,data) {
	var isBC = false;
	return ns.approval.validateSubmission( document.approvePaymentsForm, isBC );
});
</script>

<ffi:cinclude value1="0" value2="${PendingApprovalsCount}" operator="notEquals">

	<s:form
		action='includeApprovePaymentsGrid_verifyGridData.action' namespace="/pages/jsp/approvals"
		method="post" name="approvePaymentsForm" id="approvePaymentsForm">
		<input type="hidden"
		name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />

	<ffi:setGridURL grid="GRID_approvePayments" name="ViewURL" url="${FullPagesPath}approvals/approvepayments-viewcommon.jsp?CollectionName=ApprovalsItems&Type={0}&ItemID={1}&SubmittingUserName={2}" parm0="Item.TypeAsString" parm1="ItemID" parm2="SubmittingUserName"/>
	<ffi:setProperty name="approvePaymentsTempURL" value="/pages/jsp/approvals/includeApprovePaymentsGrid.action?GridURLs=GRID_approvePayments" URLEncrypt="true"/>
	<s:url id="getApprovePaymentsUrl" value="%{#session.approvePaymentsTempURL}" escapeAmp="false"/>

	<%--Skip call to load grid data when this grid is not visible --%>
	<s:if test="%{#parameters.visibleGrid[0] == 'pendingTransactions'}">
		<ffi:setProperty name="gridDataType" value="json"/>
	</s:if>
	<s:elseif test="%{#parameters.visibleGrid[0] == 'pendingPayees'}">
		<ffi:setProperty name="gridDataType" value="local"/>
	</s:elseif>
	<s:else>
		<ffi:setProperty name="gridDataType" value="json"/>
	</s:else>

	<sjg:grid
		id="approvePaymentsGrid" caption=""
		dataType="%{#session.gridDataType}"
		sortable="true" sortname="SUBMISSIONDATE"
		href="%{getApprovePaymentsUrl}"
		pager="true"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}"
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="true"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="approvePaymentsGridCompleteEvents">
		<sjg:gridColumn name="item.submissionDateTimeDisplay" index="SUBMISSIONDATE" title="%{getText('jsp.default_137')}" sortable="true" width="60" formatter="ns.approval.formatSubmittedDate"/>
		<sjg:gridColumn name="item.displayTypeAsString" index="Type" title="%{getText('jsp.common_9')}" sortable="true" width="90" />
		<ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="equals">
			<sjg:gridColumn name="workFlowName" index="Type" title="%{getText('jsp.workFlowName')}" sortable="true" width="90" />
		</ffi:cinclude>
		<sjg:gridColumn name="Accounts" index="Accounts" title="Accounts" sortable="true" width="180" formatter="ns.approval.formatAccountDisplay" />
		<sjg:gridColumn name="map.FromAct" index="FromAct" title="%{getText('jsp.default_217')}" sortable="true" width="180" formatter="ns.approval.formatAccount" hidden="true" />
		<sjg:gridColumn name="map.ToAct" index="ToAct" title="%{getText('jsp.default_424')}" sortable="true" width="180" formatter="ns.approval.formatAccount" hidden="true" />
		<sjg:gridColumn name="submittingUserName" index="SUBMITTINGUSERNAME" title="%{getText('jsp.default_398')}" sortable="true" width="100" formatter="ns.approval.submittingUserName"/>
		<sjg:gridColumn name="item.transaction.approvalAmount.currencyStringNoSymbol" index="AMOUNT" title="%{getText('jsp.default_43')}" sortable="true" width="90" formatter="ns.approval.amountUpdate"/>
		<sjg:gridColumn name="item.transaction.approvalAmount.currencyCode" index="CURRENCY_CODE" title="%{getText('jsp.default_125')}" sortable="true" width="70" hidden="true"/>
		<sjg:gridColumn name="map.ViewURL" index="viewURL" title="%{getText('jsp.default_27')}" sortable="false" width="180" formatter="ns.approval.formatApprovePaymentActionLinks" />
		<sjg:gridColumn name="item.transaction.resultOfApproval" index="resultOfApproval" title="%{getText('jsp.default_359')}" hidden="true" sortable="false" />
		<sjg:gridColumn name="item.typeAsString" index="TypeString" title="%{getText('jsp.common_9')}" hidden="true" sortable="false"/>
		<sjg:gridColumn name="itemID" index="itemID" title="%{getText('jsp.common_95')}" hidden="true" sortable="false" formatter="ns.approval.formatItemID" />
	</sjg:grid>

	<script>
		ns.common.disableSortableRows("#approvePaymentsGrid");
	</script>

	<div id="sameDayNote" align="left" style="display:none;">
		<s:text name="jsp.common.samedaynote"/>
	</div>

	<div class="marginTop10" align="center">
	<sj:a id="submitApprovePaymentsFormLink"
		formIds="approvePaymentsForm"
		targets="targetApprovePaymentsVerify"
		button="true"
		title="%{getText('jsp.default_395.1')}"
		validate="true"
		validateFunction="customValidation"
		onClickTopics="onSubmitCheckTopics"
		onBeforeTopics="clearValidationErrors"
		onCompleteTopics="submitApprovePaymentsFormComplete"
		onSuccessTopics="submitApprovePaymentsFormSuccessTopics"
		effectDuration="1500"><s:text name="jsp.default_395"/></sj:a>
		<br/><br/>
		</div>

	</s:form>
</ffi:cinclude>

<sj:dialog
		id="viewApprovePaymentDialogID"
		cssClass="includeApprovalDialog"
		autoOpen="false"
		modal="true"
		title="%{getText('jsp.common_167')}"
		height="550"
		width="800"
		resizable="false"
		showEffect="fold"
   		hideEffect="clip"
   		buttons="{
    		'%{getText(\"jsp.default_175\")}':function() { ns.common.closeDialog(); }
   		}"
	>
	</sj:dialog>
	<script>
		$('#viewApprovePaymentDialogID').addHelp(function(){
			var helpFile = $('#viewApprovePaymentDialogID').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		});


$.subscribe('clearValidationErrors', function(event,data) {
	removeValidationErrors();
});
	</script>
</div>
<ffi:removeProperty name="Approvals_approvalDecisions"/>
<ffi:removeProperty name="Approvals_reason"/>
<ffi:removeProperty name="Approvals_notifyPrimaryContact"/>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
<ffi:removeProperty name="gridDataType"/>