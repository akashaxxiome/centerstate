<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<%
	
	String paramZoom = request.getParameter("paramZoom");
	String paramRotationBy = request.getParameter("paramRotationBy");
	
%>

<ffi:setProperty name="paramZoom" value="<%= paramZoom %>" />
<ffi:setProperty name="paramRotationBy" value="<%= paramRotationBy %>" />

<%-- Flag to check whether to retrieve image or not --%>
<ffi:setProperty name="getImageFlag" value="true"/>

<ffi:cinclude value1="true" value2="${getImageFlag}" operator="equals" >
<%-- size:<ffi:getProperty name="AvailableImages" property="size"/> --%>
	<ffi:cinclude value1="1" value2='<ffi:getProperty name="AvailableImages" property="size"/>' operator="equals" >
		<ffi:object name="com.ffusion.tasks.checkimaging.GetImageIndex" id="GetImageIndex"/>	
			<ffi:setProperty name="GetImageIndex" property="Collection" value="ImageResults" />
		<ffi:process name="GetImageIndex"/>

		<ffi:object name="com.ffusion.tasks.checkimaging.GetCheckedImages" id="GetCheckedImages"/>

		<ffi:list collection="ImageResults" items="ImageResult1">	
			<ffi:setProperty name="GetCheckedImages" property="ID" value="${ImageResult1.ImageHandle}" />
		</ffi:list>	

		<ffi:process name="GetCheckedImages"/>

		<ffi:object name="com.ffusion.tasks.checkimaging.GetImages" id="GetImages"/>	
		<ffi:process name="GetImages"/>
		
	</ffi:cinclude>



	<ffi:object name="com.ffusion.tasks.checkimaging.DisplayImage" id="DisplayFrontImage" scope="session"/>
	<ffi:object name="com.ffusion.tasks.checkimaging.DisplayImage" id="DisplayBackImage" scope="session"/>
		<ffi:setProperty name="DisplayFrontImage" property="ImageSide" value="FRONT" />
		<ffi:setProperty name="DisplayBackImage" property="ImageSide" value="BACK" />

	<%-- Task for sending email with images --%>
	<ffi:object name="com.ffusion.tasks.checkimaging.EmailImages" id="EmailImages" scope="session"/>
	<%-- <ffi:setProperty name="EmailImages" property="SuccessURL" value="/cb/pages/account/emailImage.jsp" /> --%>

		<%
			session.setAttribute("FFIEmailImages", session.getAttribute("EmailImages"));
        %>
        
	<ffi:setProperty name="getImageFlag" value="false"/>

</ffi:cinclude>


<ffi:setProperty name="availableImageFlag" value="true"/>

<%-- Set defaults for rotation and zoom values if not already set --%>

<ffi:cinclude value1="${paramZoom}" value2="" operator="equals">
	<ffi:setProperty name="zoom" value="0"/>
</ffi:cinclude>
<ffi:cinclude value1="${paramZoom}" value2="" operator="notEquals">
	<ffi:setProperty name="zoom" value="${paramZoom}"/>
</ffi:cinclude>
<ffi:cinclude value1="${paramRotationBy}" value2="" operator="equals">

	<ffi:setProperty name="rotationBy" value="0"/>
	<%-- Initialize the rotation (in case we have left-over values from a previous popup) --%>
	<ffi:setProperty name="DisplayFrontImage" property="Rotation" value="0" />
	<ffi:setProperty name="DisplayBackImage" property="Rotation" value="0" />	
</ffi:cinclude>
<ffi:cinclude value1="${paramRotationBy}" value2="" operator="notEquals">
	<%
		String rotationBy = (String)session.getAttribute("rotationBy");
		Integer paramRotationByInt = Integer.valueOf(paramRotationBy);
		int finalRotationByInt = Integer.valueOf(rotationBy).intValue() + paramRotationByInt.intValue(); 
		String finalRotation = Integer.toString(finalRotationByInt);
	%>
	<ffi:setProperty name="rotationBy" value="<%= finalRotation %>"/>
</ffi:cinclude>
<ffi:removeProperty name="paramZoom"/>
<ffi:removeProperty name="paramRotationBy"/>

<%-- Set the rotation --%>
<ffi:setProperty name="DisplayFrontImage" property="RotateBy" value="${rotationBy}" />
<ffi:setProperty name="DisplayBackImage" property="RotateBy" value="${rotationBy}" />	
<%-- Set the image type --%>
<ffi:setProperty name="DisplayFrontImage" property="RotatedImageFormat" value="1" />
<ffi:setProperty name="DisplayBackImage" property="RotatedImageFormat" value="1" />	


<%!
	// Stores the check image size in the session (in accordance with the zoom amount)
	private void calculateImageSize( HttpSession session ) {
		int baseWidth = 420;
		int baseHeight = 195;
		int zoomWidth = 40;
		int zoomHeight = 15;
		int zoomAmount = Integer.parseInt( session.getAttribute("zoom").toString() );
		
		// normalize zoom amount (only a problem if the user enters the value in the url)
		if ( zoomAmount > 3 ) {
			zoomAmount = 3;
			session.setAttribute("zoom", zoomAmount + "");
		}
		else if ( zoomAmount < 0 ) {
			zoomAmount = 0;
			session.setAttribute("zoom", zoomAmount + "");		
		}
		
		session.setAttribute( "imageWidth", ( baseWidth + ( zoomWidth *  zoomAmount ) ) + "" );
		session.setAttribute( "imageHeight", ( baseHeight + ( zoomHeight *  zoomAmount ) ) + "" );
	}
	
	// Stores the layout of the checks, based on rotation, in the session
	//	the check images can either be organized horizontally or vertically
	private void calculateLayout( HttpSession session ) {
		int rotation = ((com.ffusion.tasks.checkimaging.DisplayImage)session.getAttribute("DisplayFrontImage")).getRotationValue();
		System.out.println("###########"+rotation);
		if ( rotation == 0 || rotation == 180 ) {
			System.out.println("vertical");
			session.setAttribute( "imageLayout", "vertical" );
		}
		else {
			System.out.println("horizontal");
			session.setAttribute( "imageLayout", "horizontal" );
		}
	}
%>
<%-- Calculate new image size (for zooming) --%>
<% calculateImageSize(session); %>
<%-- Calculate the image layout (based on desired rotation) --%>
<% calculateLayout(session); %>


<center>
<ffi:cinclude value1="${AvailableImages.Size}" value2="1" operator="notEquals">
	<div class="carouselClass">
							<ul>
								
</ffi:cinclude>

							
<ffi:list collection="AvailableImages" items="image">

    <ffi:cinclude value1="true" value2="${availableImageFlag}" operator="equals" >
	
	<ffi:cinclude value1="${AvailableImages.Size}" value2="1" operator="notEquals">
	<li>
	</ffi:cinclude>
	
	<div align="center">
	<ffi:help id="account_imageSearch_checkImage" />
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<br>
				<tr>
					<td colspan="2">
						<center>
							<div containerType="image">
							<ffi:cinclude value1="${image.Status}" value2="PENDING" operator="equals" >
								<s:text name="jsp.account_102"/>
							</ffi:cinclude>
		
							<ffi:cinclude value1="${image.Status}" value2="AVAILABLE" operator="equals" >
							
								<ffi:cinclude value1="true" value2="${getImageFlag}" operator="equals" >
									<%-- ADD FEE DETAILS IN AUDIT LOG --%>
									<ffi:object name="com.ffusion.tasks.checkimaging.LogFee" id="LogFee"/>	
										<ffi:setProperty name="LogFee" property="ChargeFlag" value="0" />
										<ffi:setProperty name="LogFee" property="ImageID" value="${image.ImageHandle}" />
										<ffi:setProperty name="LogFee" property="ChargeAccount" value="${image.ChargeAccount}" />
		
									<ffi:process name="LogFee"/>
								</ffi:cinclude>	
								
								<%-- Common image properties --%>
								<ffi:setProperty name="DisplayFrontImage" property="ViewerID" value="${image.ImageHandle}" />
								<ffi:setProperty name="DisplayBackImage" property="ViewerID" value="${image.ImageHandle}" />
								<ffi:setProperty name="accountNumber" value="${image.maskedAccountID}" />
								<ffi:setProperty name="datePosted" value="${image.postingDate}" />
								<ffi:setProperty name="chequeNumber" value="${image.checkNumber}" />
								<ffi:setProperty name="amount" value="${image.amount}" />
								<ffi:setProperty name="drcr" value="${image.drCr}" />
								
								
								<%-- Vertical Layout (if images are on top of each other) --%>
								<ffi:cinclude value1="${imageLayout}" value2="vertical" operator="equals">						
									<!-- FRONT IMAGE -->
									<img src="/cb/pages/jsp/account/DisplayFrontImageAction.action?time=<%= System.currentTimeMillis() %>" alt="<s:text name="jsp.default_461"/>" width="<ffi:getProperty name="imageWidth"/>" height="<ffi:getProperty name="imageHeight"/>" border="1">
									<br/>		
									<!-- BACK IMAGE -->
									<img src="/cb/pages/jsp/account/DisplayBackImageAction.action?time=<%= System.currentTimeMillis() %>" alt="<s:text name="jsp.default_461"/>" width="<ffi:getProperty name="imageWidth"/>" height="<ffi:getProperty name="imageHeight"/>" border="1">
									<br/>
								</ffi:cinclude>

								<%-- Horizontal Layout (if images are beside each other) --%>
								<ffi:cinclude value1="${imageLayout}" value2="horizontal" operator="equals">			
									<!-- FRONT IMAGE -->
									<img src="/cb/pages/jsp/account/DisplayFrontImageAction.action?time=<%= System.currentTimeMillis() %>" alt="<s:text name="jsp.default_461"/>" height="<ffi:getProperty name="imageWidth"/>" width="<ffi:getProperty name="imageHeight"/>" border="1">
									<!-- BACK IMAGE -->
									<img src="/cb/pages/jsp/account/DisplayBackImageAction.action?time=<%= System.currentTimeMillis() %>" alt="<s:text name="jsp.default_461"/>" height="<ffi:getProperty name="imageWidth"/>" width="<ffi:getProperty name="imageHeight"/>" border="1">
									<br/>
								</ffi:cinclude>

							</ffi:cinclude>		
							</div>
						</center>				
                	</td>
				</tr>
				
				
				<tr>
					<td class="sectionsubhead" align="right" width="50%"><s:text name="jsp.imageSearch.gridColumnHeader.postingDate"/>:</td>
					<td class="columndata" width="50%" align="left"><ffi:getProperty name="datePosted"/></td>
				</tr>

				<tr>
					<td class="sectionsubhead" align="right" width="50%"><s:text name="jsp.imageSearch.gridColumnHeader.DR_CR"/>:</td>
					<td class="columndata" width="50%" align="left"><ffi:getProperty name="drcr"/></td>
				</tr>

				<tr>
					<td class="sectionsubhead" align="right" width="50%"><s:text name="jsp.default_43"/>:</td>
					<td class="columndata" width="50%" align="left"><ffi:getProperty name="amount"/></td>
				</tr>
				
				<tr>
					<td class="sectionsubhead" align="right" width="50%"><s:text name="jsp.default_21"/></td>
					<td class="columndata" width="50%" align="left" ><ffi:getProperty name="accountNumber"/></td>
				</tr>
				
				<tr>
					<td class="sectionsubhead" align="right" width="50%"><s:text name="jsp.account_55"/></td>
					<td class="columndata" width="50%" align="left"><ffi:getProperty name="chequeNumber"/></td>
				</tr>

				
				<%-- ERROR DISPLAY --%>
				<tr>
					<td colspan="2" align="center">
						<ffi:cinclude value1="${DisplayFrontImage.DisplayImageError}" value2="0" operator="notEquals">
							<!-- Start: Error Info (FrontImage) -->
						   	<table cellpadding="1" cellspacing="2" border="0">
							  	<tr>
							      	<td align="right" class="homesub"><b><s:text name="jsp.account_3"/>:</b>&nbsp;</td>
							      	<td class="errorBackground">
							      		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${DisplayFrontImage.DisplayImageError}_alert" />
										<ffi:getProperty name="ErrorsResource" property="Resource"/>
							      	</td>
							   	</tr>
							   	<tr>
							      	<td align="right" class="homesub"><b><s:text name="jsp.account_4"/>:</b>&nbsp;</td>
							      	<td class="errorBackground">
							      		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${DisplayFrontImage.DisplayImageError}_descr" />
										<ffi:getProperty name="ErrorsResource" property="Resource"/>
							      	</td>
							   	</tr>								
							</table>
							<!-- End: Error Info (FrontImage) -->
						</ffi:cinclude>
		
						<ffi:cinclude value1="${DisplayBackImage.DisplayImageError}" value2="0" operator="notEquals">
							<!-- Start: Error Info (BackImage) -->
						   	<table cellpadding="1" cellspacing="2" border="0">
							  	<tr>
							      	<td align="right" class="homesub"><b><s:text name="jsp.account_1"/>:</b>&nbsp;</td>
							      	<td class="errorBackground">
							      		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${DisplayBackImage.DisplayImageError}_alert" />
										<ffi:getProperty name="ErrorsResource" property="Resource"/>
							      	</td>
							   	</tr>
							   	<tr>
							      	<td align="right" class="homesub"><b><s:text name="jsp.account_2"/>:</b>&nbsp;</td>
							      	<td class="errorBackground">
							      		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${DisplayBackImage.DisplayImageError}_descr" />
										<ffi:getProperty name="ErrorsResource" property="Resource"/>
							      	</td>
							   	</tr>								
							</table>
							<!-- End: Error Info (BackImage) -->
						</ffi:cinclude>
					</td>
				</tr>
				
			</table>
			<p></p>
			<p>
		</div>
		<ffi:cinclude value1="${AvailableImages.Size}" value2="1" operator="notEquals">
		</li>
		</ffi:cinclude>	
		
    	</ffi:cinclude>	
	
	
	<ffi:setProperty name="availableImageFlag" value="false"/>
	<ffi:cinclude value1="${AvailableImages.Size}" value2="1" operator="notEquals">
		<!-- set this flag to true if it is multi image view-->
		<ffi:setProperty name="availableImageFlag" value="true"/>
	</ffi:cinclude>	
	

</ffi:list>	


<ffi:cinclude value1="${AvailableImages.Size}" value2="1" operator="notEquals">				
		</ul>
		</div>
</ffi:cinclude>

			<center>
						<table>
				<tr>
					<td colspan="2" align="center">
						<s:form action="" method="post" name="checkImageForm" id="checkImageID" theme="simple">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" id="rotatePlus90" name="rotatePlus90" value="<ffi:urlEncrypt url='/cb/pages/jsp/common/checkimage.jsp?paramRotationBy=90&paramZoom=${zoom}' />"/>
						<input type="hidden" id="rotateMinus90" name="rotateMinus90"  value="<ffi:urlEncrypt url='/cb/pages/jsp/common/checkimage.jsp?paramRotationBy=-90&paramZoom=${zoom}' />"/>
						<input type="hidden" id ="zoom" name="zoom" value="<ffi:getProperty name='paramZoom'/>"/>
						
								<sj:a 
									id="printID1"
									button="true" 
									onClick="ns.checkimage.printImage()"
									><s:text name="jsp.default_331"/></sj:a>
								<ffi:setProperty name="emailImageURL" value="/cb/pages/jsp/account/GenericEmailImagesAction.action?Init=true&ImageId=${image.ImageHandle}" URLEncrypt="true" />
								<input type="hidden" name="email" value="<ffi:getProperty name='emailImageURL'/>"/>
								<sj:a 
									id="emailID1"
									button="true"
									onClick="ns.checkimage.emailImage()"
									><s:text name="jsp.default_183"/></sj:a>
								
							<ffi:cinclude value1="${zoom}" value2="0" operator="notEquals">
								
								<sj:a 
									id="zoomOutID1"
									button="true" 
									onClick="ns.checkimage.zoom('out')"
									><s:text name="jsp.default_475"/></sj:a>
								
							</ffi:cinclude>
							<ffi:cinclude value1="${zoom}" value2="3" operator="notEquals">
								
								<sj:a 
									id="zoomInID1"
									button="true" 
									onClick="ns.checkimage.zoom('in')"
									><s:text name="jsp.default_474"/></sj:a>
							
							</ffi:cinclude>
							<sj:a 
								id="rotateRightID1"
								button="true" 
								onClick="ns.checkimage.rotate('90')"
								><s:text name="jsp.default_363"/></sj:a>
							<sj:a 
								id="rotateLeftID1"
								button="true" 
								onClick="ns.checkimage.rotate('-90')"
								><s:text name="jsp.default_364"/></sj:a>
							<sj:a 
								id="closeCheckImageID1"
								button="true" 
								onClick="ns.checkimage.closeDialog()"
								><s:text name="jsp.default_102"/></sj:a>
		
		

						</s:form>
					</td>
				</tr>
			</table>
			</center>

<ffi:cinclude value1="${AvailableImages.Size}" value2="1" operator="notEquals">				
		<div>
			<center>
				<sj:a 	id="prevImage" class="prev"	button="true" ><s:text name="jsp.label.previous"/></sj:a>
				<sj:a 	id="nextImage"	class="next" button="true"><s:text name="jsp.label.next"/></sj:a>
			</center>
		</div>
	</div >	
</ffi:cinclude>

<script type="text/javascript">
<!--
	$('#checkImageDialogID').addHelp(function(){
		var helpFile = $('#checkImageDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
//-->
</script>