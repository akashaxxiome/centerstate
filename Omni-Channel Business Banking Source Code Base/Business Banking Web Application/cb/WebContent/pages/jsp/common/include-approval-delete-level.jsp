<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
	String _opType = request.getParameter( "OperationType" );
	if(_opType==null) _opType = "";
	String _levelID = request.getParameter( "LevelID" );
	String _workFlowID = request.getParameter( "WorkflowID" );
%>

<%
String _accountID = request.getParameter("AccountID");
if("All Accounts".equals(_accountID))
	_accountID = "";
%>
<form action='<ffi:getProperty name="FullPagesPath"/>common/include-approval-delete-level-process.jsp' id="DeleteApprovalsLevelForm" name="DeleteApprovalsLevelForm" method="post">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td class="sectionsubhead" colspan="6" style="color:red" align="center"><br/>
	<%if(_workFlowID!=null && _workFlowID.length()>0){%>
		<s:text name="jsp.common_209"/><br><br>
		<%}else{%>
		<s:text name="jsp.common_168"/><br><br>
		<%}%>

	</td>
	</tr>
	<tr>
	    <td colspan="4" align="center" class="sectionhead">
		<%if(_workFlowID!=null && _workFlowID.length()>0){%>
		<s:text name="jsp.common_210"/>
		<%}else{%>
		<s:text name="jsp.common_32"/>
		<%}%>


		</td>
	</tr>
	<tr>
	    <td colspan="4" height="60">&nbsp;</td>
	</tr>
	<tr>
	    <td colspan="4" valign="mddle" align="center">
			<%--
			<input class="submitbutton" type="submit" name="CancelButton" value="CANCEL">
			<input class="submitbutton" onclick="return delLevel( document.DeleteApprovalsLevelForm );" type="submit" name="DeleteButton" value="DELETE">
			--%>

           	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="LevelID" value="<%= com.ffusion.util.HTMLUtil.encode(_levelID) %>">
		    <input type="hidden" name="OperationType" value="<%= com.ffusion.util.HTMLUtil.encode(_opType) %>">
			<%if(_workFlowID!=null && _workFlowID.length()>0){%>
			<input type="hidden" name="WorkflowID" value="<%= com.ffusion.util.HTMLUtil.encode(_workFlowID) %>">
			<%}%>

			 <% if( _accountID != null && _accountID.length() > 0 ) { %>
				<input type="hidden" name="approvalsLevelsAccountID" value="<%=com.ffusion.util.HTMLUtil.encode(_accountID)%>">
   			 <% } %>
	    	<div align="center" class="ui-widget-header customDialogFooter">
			<sj:a   button="true"
					onClickTopics="closeDialog"
	        ><s:text name="jsp.default_83"/></sj:a>
           <sj:a
           		id="DeleteApprovalsLevelLink"
				formIds="DeleteApprovalsLevelForm"
                targets="resultmessage"
                button="true"
                onBeforeTopics="beforeDeleteApprovalWFLevel"
                onSuccessTopics="deleteApprovalWFLevelSuccess"
                onErrorTopics="errorDeleteApprovalWFLevel"
                onCompleteTopics="deleteApprovalWFLevelComplete"
	       ><s:text name="jsp.default_163"/></sj:a>
	    </div>
	    </td>
	</tr>
</table>

</form>

