<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<script type="text/javascript"><!--
        	
        $(document).ready(function(){
        	
        });
        
        	$.subscribe('quickSearchResult', function(event,data) {
				$('#BankFormID').hide();
			});
        
        	function closeQuickBankSearch(){
        		ns.common.closeDialog("bankLookupDialogId");
        	}
        	
        	function fixBlanks(){
        		if($('#bankStateListSelectId').val()=='-1'){
					$('#bankStateListSelectId').val(' ');
				}
        		
        		if($('#billpay_bankCountryListSelectId').val()=='-1'){
					$('#billpay_bankCountryListSelectId').val();
				}
        	}
        	
			function updateBankStates(obj) {
				$.ajax({   
					type: "POST",   
					url: "/cb/pages/common/QuickBankLookupAction_getStates.action",
					data: {countryName: obj.value, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
					success: function(data) {
						
						$('#bankStateListSelectId').remove();
						$('#bankStateListSelectId_hidden').clone().appendTo("#bankStateSelectedId");
						$('#bankStateListSelectId_hidden').attr('id',"bankStateListSelectId");
						if(data.length>0){
							$('#bankStateLabelId').show();
						   	for(var i=0;i<data.length;i++){
						   		$('#bankStateListSelectId').append('<option></option>');
								 $('#bankStateListSelectId').append('<option value='+data[i].stateKey+ '>'+data[i].name+ '</option>');
								 $('#bankStateListSelectId').addClass("txtbox");
									$("#bankStateListSelectId").combobox({'size': '60'});
						   	}
					  	}
					  	else{
					  		$('#bankStateListSelectId').hide();
					  		$('#bankStateLabelId').hide();
					  	}
						 
					}, 
				}); 
			}

        //--></script>

<div align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="0"
		class="ltrow2_color">
		<tr>
			<td class="columndata" align="center" class="ltrow2_color">
				<table border="0" cellspacing="0" cellpadding="3"
					class="ltrow2_color">
					<tr>
						<td></td>
					</tr>
					<tr>
						<td><s:form
								action="/pages/common/QuickBankLookupAction_quickBankSearchResult.action"
								method="post" name="BankForm" id="BankFormID">
								<input type="hidden" name="CSRF_TOKEN"
									value="<ffi:getProperty name='CSRF_TOKEN'/>" />

								<table border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td align="right" class="sectionhead">
											<!--L10NStart-->
											<s:text name="jsp.default_61" />
											<!--L10NEnd-->&nbsp;
										</td>
										<td><input class="ui-widget-content ui-corner-all txtbox"
											type="text" name="institutionName" size="39"></td>
									</tr>
									<tr>
										<td align="right" class="sectionhead"><s:text
												name="jsp.default_101" />&nbsp;</td>
										<td><input class="ui-widget-content ui-corner-all txtbox"
											type="text" name="city" size="39" maxlength="32" value=""></td>
									</tr>


									<tr>
										<td align="right" class="sectionhead">
											<div id="bankStateLabelId">
												<!--L10NStart-->
												<s:text name="jsp.default_386" />
												<!--L10NEnd-->
												&nbsp;
											</div>
										</td>
										<td>
											<div id="bankStateSelectedId">
												<select class="" id="bankStateListSelectId" name="stateCode">
													<option></option>
													<s:iterator value="stateList" var="allStates">
														<option value="<s:property value="#allStates.stateKey"/>">
															<s:property value="#allStates.Name" />
														</option>
													</s:iterator>
												</select> <select name="state" id="bankStateListSelectId_hidden"
													class="hidden">
												</select>
												<script>
												$("#bankStateListSelectId").combobox({'size': '37'});
											</script>
											</div>
										</td>
									</tr>
									<tr>
										<td align="right" class="sectionhead">
											<!--L10NStart-->
											<s:text name="jsp.default_115" />
											<!--L10NEnd-->&nbsp;
										</td>
										<td>
											<%--  <s:select id="billpay_bankCountryListSelectId" list="countryList" name="country" onChange="updateBankStates(this)"
												listKey="bankLookupCountry" listValue="Name" class="" theme="simple" on
												value="%{countryName}">
											</s:select> --%> <select class=""
											id="billpay_bankCountryListSelectId" name="country"
											onchange="updateBankStates(this)">
												<s:iterator value="countryList" var="allCountries">
													<option
														value="<s:property value="#allCountries.bankLookupCountry"/>"
														<s:if test="%{#allCountries.bankLookupCountry==countryName}">selected</s:if>>
														<s:property value="#allCountries.Name" />
													</option>
												</s:iterator>
										</select> <input type="hidden" id="defaultCountry"
											value="<s:property value="%{countryName}"/>"> <script>
												$("#billpay_bankCountryListSelectId").combobox({'size': '37'});
											</script>
										</td>
									</tr>
									<%--<tr>
										<td align="right" class="sectionhead">
											<!--L10NStart-->
											<s:text name="jsp.default_ACH_Routing_No" />
											<!--L10NEnd-->&nbsp;
										</td>
										<td><input class="ui-widget-content ui-corner-all txtbox"
											type="text" name="achRoutingNumberForSearch" size="39"></td>
									</tr>
									<tr>
										<td align="right" class="sectionhead">
											<!--L10NStart-->
											<s:text name="jsp.default_Wire_Routing_No" />
											<!--L10NEnd-->&nbsp;
										</td>
										<td><input class="ui-widget-content ui-corner-all txtbox"
											type="text" name="wireRoutingNumberForSearch" size="39"></td>
									</tr>
									<tr>
										<td align="right" class="sectionhead"><s:text
												name="jsp.default_99" />&nbsp;</td>
										<td><input class="ui-widget-content ui-corner-all txtbox"
											type="text" name="chipsUID" size="39"></td>
									</tr>
									<tr>
										<td align="right" class="sectionhead"><s:text
												name="jsp.default_289" />&nbsp;</td>
										<td><input class="ui-widget-content ui-corner-all txtbox"
											type="text" name="nationalID" size="39"></td>
									</tr> --%>
									<tr>
										<td align="center" colspan="2">
											<div class="ui-widget-header customDialogFooter">
												<sj:a id="searchBankButtonID" formIds="BankFormID"
													targets="searchDestinationBankDialogID" button="true"
													onclick="fixBlanks()" onSuccessTopics="quickSearchResult">
													<s:text name="jsp.default_373" />
													<!-- SEARCH -->
												</sj:a>

												<sj:a id="closeReSerachBankListID" button="true"
													onclick="closeQuickBankSearch()">
													<s:text name="jsp.default_102" />
													<!-- CLOSE -->
												</sj:a>
											</div>
										</td>
									</tr>
									<tr>
										<td align="center" colspan="2">
									</tr>
									<!-- END: Manual Entry button -->
								</table>
                                    &nbsp;
                                    </s:form>
							<table width="98%" align="center" border="0" cellspacing="0"
								cellpadding="0">
								<tr>
									<td colspan="2">
										<div id="searchDestinationBankDialogID"></div>
									</td>
								</tr>
							</table></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>