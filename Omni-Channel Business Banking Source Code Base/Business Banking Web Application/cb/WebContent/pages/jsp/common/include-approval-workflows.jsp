<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>



<%!
public boolean isGroupEntitledToActivityType( int entGroupID, String activityType, boolean isBC )
{
    boolean ret = false;
    try {
	ret = com.sap.banking.web.util.entitlements.EntitlementsUtil.checkEntitlement(
		entGroupID,
		new com.ffusion.csil.beans.entitlements.Entitlement( activityType, null, null ) );
    } catch( java.lang.Exception ex ) {}

    return ret;
}
%>

<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;


// the entitlement group id of the business
int _busEntGroupID = 0;
int _spEntGroupID = 0;
// determines the destination i.e which page should be refreshed when
// value of the combo box containg the operation types is changed
String _destination;


_destination = ( String )session.getAttribute( "FullPagesPath" ) +
	( String )session.getAttribute( "approval_display_workflow_jsp" );
_busEntGroupID = ((com.ffusion.beans.business.Business)session.getAttribute( "Business" )).getEntitlementGroupIdValue();
_spEntGroupID = ((com.ffusion.beans.business.Business)session.getAttribute( "Business" )).getServicesPackageIdValue();

//Operation Type Search Criteria
String _defaultOperationType = "All other activities";
com.ffusion.beans.approvals.ApprovalsGroups apprGroups = null;
String _opType = request.getParameter( "OpType" );
//String _opType = (String)session.getAttribute( "OpType" );
if( _opType == null ) {
        _opType = _defaultOperationType;
} else if( _opType.length() == 0 ) {
        _opType = _defaultOperationType;
}
String _setOperationType = "true";
if( _opType.equalsIgnoreCase( _defaultOperationType ) ) {
    _setOperationType = "false";
}

//Account Search Criteria
String _defaultAccount = "All Accounts";
String _accountID = request.getParameter( "AccountID" );
if( _accountID == null ) {
        _accountID = _defaultAccount;
} else if( _accountID.length() == 0 ) {
        _accountID = _defaultAccount;
}

String _setAccountID = "true";
if( _accountID.equalsIgnoreCase( _defaultAccount ) ) {
    _setAccountID = "false";
}

%>

<ffi:setProperty name="ApprovalsLevelCurrencyCode" value="" />

<ffi:object id="GetCurrencyCode" name="com.ffusion.tasks.GetCurrencyCode" scope="session" />
<ffi:setProperty name="GetCurrencyCode" property="BusinessID" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetCurrencyCode" />
<ffi:setProperty name="ApprovalsLevelCurrencyCode" value="${GetCurrencyCode.CurrencyCode}"/>
<ffi:removeProperty name="GetCurrencyCode" />

<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
<ffi:process name="GetLimitBaseCurrency" />
<ffi:cinclude value1="${ApprovalsLevelCurrencyCode}" value2="" operator="equals">
    <ffi:setProperty name="ApprovalsLevelCurrencyCode" value="${BaseCurrency}" />
</ffi:cinclude>

<%-- determine whether the page should be viewed in read-only mode --%>
<% boolean _disableEdit = false; %>

<ffi:cinclude ifNotEntitled="Approvals Workflow Admin">
	<% _disableEdit = true; %>
</ffi:cinclude>





<ffi:setProperty name="EditGroup_GroupId" value="${Business.EntitlementGroupId}"/>

<ffi:object id="GetAccountsForGroup" name="com.ffusion.tasks.admin.GetAccountsForGroup" scope="session"/>
<ffi:setProperty name="GetAccountsForGroup" property="GroupID" value="${EditGroup_GroupId}"/>
<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="ParentsAccounts"/>
<ffi:setProperty name="GetAccountsForGroup" property="CheckAccountGroupAccess" value="false"/>
<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="true"/>
<ffi:process name="GetAccountsForGroup"/>

<ffi:object id="SetAccountsCollectionDisplayText"  name="com.ffusion.tasks.util.SetAccountsCollectionDisplayText" />
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CollectionName" value="<%= SessionNames.PARENTS_ACCOUNTS %>"/>
<ffi:setProperty name="SetAccountsCollectionDisplayText" property="CorporateAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.ROUNTING_NUM_DISPLAY%>"/>
<ffi:process name="SetAccountsCollectionDisplayText"/>
<ffi:removeProperty name="SetAccountsCollectionDisplayText"/>



<%-- clean up session variables --%>
<ffi:removeProperty name="errorPage" />
<ffi:removeProperty name="MinAmt" />
<ffi:removeProperty name="MaxAmt" />
<ffi:removeProperty name="LevelID" />
<ffi:removeProperty name="GoToApprovalsAddLevelPage" />

<%-- <s:form id="DisplayApprovalsLevelsForm" name="DisplayApprovalsLevelsForm" action="/pages/jsp/approvals/corpadmincoapwfaddedit.jsp" method="post" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<input type="hidden" name="Edit" value="false"/> --%>


		<!-- <div id="activityTypeId" > -->

	    <s:form id="DisplayApprovalsLevelsForm" name="DisplayApprovalsLevelsForm" action="/pages/jsp/approvals/corpadmincoapwfaddedit.jsp" method="post" theme="simple">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<input type="hidden" name="Edit" value="false"/>

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			    <tr>
				    <td class="paddingLeft10" valign="top">
					<script>
					ns.approval.csrf_tokenStr = '<ffi:getProperty name="CSRF_TOKEN"/>';
					</script>
					<select size="12" class="ui-widget-content ui-corner-all" id="OpType" name="OpType"
					<ffi:cinclude value1="${Business.PerAccountApprovalMode}" value2="1" operator="equals"> onChange="ns.approval.getLevelsByOpTypeAndAccountId()" ></ffi:cinclude>
					<ffi:cinclude value1="${Business.PerAccountApprovalMode}" value2="1" operator="notEquals"> onChange="ns.approval.getLevelsByOpType('<%= _destination %>',this.options[this.selectedIndex].value)" </ffi:cinclude>
					>
					<option value="<%= _defaultOperationType %>" <%= _opType.equals( _defaultOperationType ) ? " selected" : "" %>>
					<s:text name="jsp.common_16"/>
					</option>

				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DEPOSITS %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DEPOSITS ) ? " selected" : "" %>>
					<s:text name="jsp.common_37"/>
				    </option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DISBURSEMENTS %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DISBURSEMENTS ) ? " selected" : "" %>>
					<s:text name="jsp.common_38"/>
				    </option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.ACH_BATCH, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ACH_PAYMENTS %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ACH_PAYMENTS ) ? " selected" : "" %>>
					<s:text name="jsp.default_25"/>
					</option>
				<% } %>



				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.ARC, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ARC_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ARC_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_30"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.BOC, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_BOC_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_BOC_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_34"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.CCD_ADDENDA, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CCD_Addenda_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CCD_Addenda_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_39"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.CCD, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CCD_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CCD_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_41"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.CIE_ADDENDA, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CIE_Addenda_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CIE_Addenda_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_43"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.CIE, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CIE_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CIE_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_45"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.CTX_ADDENDA, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CTX_Addenda_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CTX_Addenda_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_59"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.IAT_ADDENDA, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_IAT_Addenda_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_IAT_Addenda_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_84"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.IAT, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_IAT_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_IAT_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_86"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.POP, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_POP_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_POP_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_127"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.PPD_ADDENDA, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PPD_Addenda_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PPD_Addenda_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_129"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.PPD, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PPD_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PPD_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_131"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.RCK, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_RCK_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_RCK_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_135"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.TEL, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TEL_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TEL_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_153"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WEB_ADDENDA, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WEB_Addenda_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WEB_Addenda_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_170"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WEB, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WEB_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WEB_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_172"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.XCK, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_XCK_ACH_Payment %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_XCK_ACH_Payment ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_194"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _spEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.ACH_BATCH, _isBC ) ) { %>
		            <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.ENT_ACH_TEMPLATE, _isBC ) ) { %>
		                <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ACH_TEMPLATE %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ACH_TEMPLATE ) ? " selected" : "" %>>
		                <s:text name="jsp.default_26"/>
		                </option>
		            <% }
		        }%>



				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.PAYMENTS, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_BILL_PAYMENTS %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_BILL_PAYMENTS ) ? " selected" : "" %>>
					<s:text name="jsp.common_33"/>
				    </option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.CHILD_SUPPORT, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CHILD_SUPPORT_PAYMENTS %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CHILD_SUPPORT_PAYMENTS ) ? " selected" : "" %>>
					<s:text name="jsp.default_98"/>
				    </option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.TAX_PAYMENTS, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TAX_PAYMENTS %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TAX_PAYMENTS ) ? " selected" : "" %>>
					<s:text name="jsp.default_407"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TRANSFERS %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TRANSFERS ) ? " selected" : "" %>>
					<s:text name="jsp.default_443"/>
					</option>
				<% } %>
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.ENT_TRANSFERS_TEMPLATE, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TRANSFERS_TEMPLATE %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TRANSFERS_TEMPLATE ) ? " selected" : "" %>>
					<s:text name="jsp.common_162"/>
					</option>
				<% } %>

				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.PAY_DECISION_ENTITLEMENT, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PAY_DECISION %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PAY_DECISION ) ? " selected" : "" %>>
					<s:text name="jsp.common_206"/>
					</option>
				<% } %>
				
				
				



				
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT, _isBC ) ) { %>
					<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_REVERSE_POSITIVE_PAY %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_REVERSE_POSITIVE_PAY ) ? " selected" : "" %>>
					<s:text name="jsp.common_208"/>
					</option>
				<% } %>
				
				
				
				<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRES, _isBC ) ) { %>
				    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE, _isBC ) ) { %>
					    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRES_TEMPL_CREATE %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRES_TEMPL_CREATE ) ? " selected" : "" %>>
						<s:text name="jsp.common_193"/>
						</option>
				    <% } %>
				    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE, _isBC ) ) { %>
				           <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_CREATE %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_CREATE ) ? " selected" : "" %>>
						   <s:text name="jsp.common_179"/>
		       	           </option>
					<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_HOST, _isBC ) ) { %>
						<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_HOST %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_HOST ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_189"/>
						</option>
					<% } %>
					<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BOOK, _isBC ) ) { %>
						<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_BOOK %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_BOOK ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_176"/>
					    </option>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BOOK_FREEFORM, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_BOOK_FREEFORM %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_BOOK_FREEFORM ) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_177"/>
							</option>
					    <% } %>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BOOK_TEMPLATED, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_BOOK_TEMPLATED %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_BOOK_TEMPLATED) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_178"/>
							</option>
					    <% } %>
					<% } %>
					<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DOMESTIC, _isBC ) ) { %>
						<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DOMESTIC %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DOMESTIC ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_180"/>
					    </option>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DOMESTIC_FREEFORM, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DOMESTIC_FREEFORM %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DOMESTIC_FREEFORM ) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_181"/>
							</option>
					    <% } %>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DOMESTIC_TEMPLATED, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DOMESTIC_TEMPLATED %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DOMESTIC_TEMPLATED ) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_182"/>
							</option>
					    <% } %>
					<% } %>
					<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DRAWDOWN, _isBC ) ) { %>
						<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DRAWDOWN %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DRAWDOWN ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_183"/>
					    </option>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DRAWDOWN_FREEFORM, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DRAWDOWN_FREEFORM %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DRAWDOWN_FREEFORM ) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_184"/>
							</option>
					    <% } %>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DRAWDOWN_TEMPLATED, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DRAWDOWN_TEMPLATED %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_DRAWDOWN_TEMPLATED) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_185"/>
							</option>
					    <% } %>
					<% } %>
					<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_FED, _isBC ) ) { %>
						<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_FED %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_FED ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_186"/>
						</option>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_FED_FREEFORM, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_FED_FREEFORM %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_FED_FREEFORM ) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_187"/>
							</option>
					    <% } %>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_FED_TEMPLATED, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_FED_TEMPLATED %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_FED_TEMPLATED) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_188"/>
							</option>
					    <% } %>
					<% } %>
					<% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_INTERNATIONAL, _isBC ) ) { %>
						<option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_INTERNATIONAL %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_INTERNATIONAL ) ? " selected" : "" %>>
						&nbsp;&nbsp;<s:text name="jsp.common_190"/>
					    </option>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_INTERNATIONAL_FREEFORM, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_INTERNATIONAL_FREEFORM %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_INTERNATIONAL_FREEFORM ) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_191"/>
							</option>
					    <% } %>
					    <% if( isGroupEntitledToActivityType( _busEntGroupID, com.ffusion.csil.core.common.EntitlementsDefines.WIRE_INTERNATIONAL_TEMPLATED, _isBC ) ) { %>
						    <option value="<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_INTERNATIONAL_TEMPLATED %>" <%= _opType.equals( com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WIRE_INTERNATIONAL_TEMPLATED ) ? " selected" : "" %>>
							&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.common_192"/>
							</option>
					    <% } %>
					<% } %>
				    <% } %>
				<% } %>
					</select>
					<br/><br/><br/>
					<div id="accountIDDiv">
					<ffi:cinclude value1="${Business.PerAccountApprovalMode}" value2="1" operator="equals">
						<select id="accountID" class="txtbox" name="AccountIDSelect" size="1" style="width:300px">
							<option value="All Accounts">
								<s:text name="jsp.all.accounts"/>
							</option>
						</select>
					</ffi:cinclude>
					<input type="hidden" id="AccountIDHidden" name="AccountID" value="All Accounts">

					</div>
				    </td>
					<td align="left" valign="top">
						<div class="approvalPanContenteDiv permissionMenuWrapper">
							 <div class="paneWrapper">
							   	<div class="paneInnerWrapper">
									<div class="header">
										<s:text name="jsp.approvals.workflow.note" />
									</div>
									<div class="paneContentWrapper paneContentWrapperForApprovals"  id="approval_workflow_note_pane">
									<ffi:cinclude value1="${Business.PerAccountApprovalMode}" value2="1" operator="notEquals"> <s:text name="jsp.common_199"/> </ffi:cinclude>
									<ffi:cinclude value1="${Business.PerAccountApprovalMode}" value2="1" operator="equals"> <s:text name="jsp.common_207"/> </ffi:cinclude>
									</div>
								</div>
							</div>
							<div>&nbsp;</div>
							 <%
								String isApprovalsLevelsNull2 = "true";
								if( session.getAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_LEVELS ) != null ) {
									isApprovalsLevelsNull2 = "false";
								}
							%>

							 <ffi:cinclude value1="<%= isApprovalsLevelsNull2 %>" value2="false" operator="equals">
									<input type="hidden" name="WorkflowName" value="Default Workflow" />
							</ffi:cinclude>

							<s:url id="definedWorkflows" value="/pages/jsp/common/include-defined-workflows.jsp">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							</s:url>
							<div class="paneWrapper">
							   	<div class="paneInnerWrapper">
									<!-- <div class="paneContentWrapper"  id="approval_defined_workflows_pane"> -->
										<sj:div href="%{definedWorkflows}" id="WorkflowLevelsDiv" onCompleteTopics="onLoadDefinedWorkflowTopic" >
											<div class="ui-autocomplete-loading loading">&nbsp;</div>
										</sj:div>
								</div>
							</div>

							<% if( !_disableEdit ) { %>

									<table cellspacing="0" cellpading="0" border="0" width="100%">
									 <tr><td colspan="4" align="center">&nbsp;</td></tr>
											<tr>
												<td colspan="4" align="center">
												     <sj:a
														id="addWorkflowLink"
														formIds="DisplayApprovalsLevelsForm"
														targets="detailsDiv"
														button="true"
												    	title="%{getText('jsp.common_AddApprovalWorkflowLevel')}"
												    	onBeforeTopics="beforeLoadApprovalsForm"
														onCompleteTopics="loadApprovalsFormComplete"
														onErrorTopics="errorLoadApprovalsForm"
														>
													  	<!---->
													  	 <ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="equals">
													  		 Add Workflow
													  	 </ffi:cinclude>
													  	 <ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="notEquals">
													  		 <s:text name="jsp.common_198" />
													  	 </ffi:cinclude>

												    </sj:a>

										   		 </td>
												  </tr>

										</table>

							<% } %>
						</div>
					</td>
			    </tr>
			    <tr><td>&nbsp;</td></tr>
			</table>
		</s:form>
	<!-- </div>	 -->
<%-- </s:form> --%>




<ffi:removeProperty name="GoingBack"/>
<% String _goingBack = _destination + "?GoingBack=TRUE"; %>
<ffi:cinclude value1="<%= _setOperationType %>" value2="true" operator="equals">
    <% _goingBack = _goingBack + "&opType=" + _opType; %>
    <ffi:setProperty name="BackURL" value="<%= _goingBack %>" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="<%= _setOperationType %>" value2="true" operator="notEquals">
    <ffi:setProperty name="BackURL" value="<%= _goingBack %>" URLEncrypt="true"/>
</ffi:cinclude>

<ffi:removeProperty name="GetLimitBaseCurrency"/>
<ffi:removeProperty name="BaseCurrency"/>


<script type="text/javascript"><!--


$(function(){
	//ns.common.initializePortlet("activityTypeId");

	/* $('#approval_workflow_note_pane').pane({
		title: js_approval_note_pane_title,
		minmax: false
	});

	$('#approval_defined_workflows_pane').pane({
		title: js_approval_level_pane_title,
		minmax: true
	});*/


});

//-->
</script>