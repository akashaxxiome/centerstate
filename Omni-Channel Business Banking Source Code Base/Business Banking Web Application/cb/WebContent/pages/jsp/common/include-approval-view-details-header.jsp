<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

			<%-- Start: Dual approval processing --%>
			<s:if test="%{#request.Type == 'ACH Batch' || #request.Type == 'Recurring ACH Batch' || #request.Type == 'Tax Payment' ||
 				#request.Type == 'Recurring Tax Payment' || #request.Type == 'Child Support Payment' || #request.Type == 'Recurring Child Support Payment'}">
			<div align="center" class="columndata" style="color:red;">
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
                <ffi:cinclude value1="${ACHBatch.IsCompletedStatus}" value2="true" operator="notEquals">
                    <ffi:cinclude value1="${ACHBatch.CoEntryDesc}" value2="REVERSAL" operator="notEquals">
				<b><ffi:getL10NString rsrcFile="cb" msgKey="da.message.ach.NonManagedACHParticipantsInRed"/></b>
                    </ffi:cinclude>
                </ffi:cinclude>
			</ffi:cinclude>
			</div>
			</s:if>
			<%-- End: Dual approval processing --%>
						
			<div class="blockWrapper label140">
			<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
	   		 <div class="blockContent">
				    <div class="blockRow">
							<div class="inlineBlock" style="width: 50%">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_435" />:</span>
								<span>
									<ffi:getProperty name="ApprovalsItem" property="Item.TrackingID"/>
								</span>	
								<%-- Start: Dual approval processing
									<s:if test="%{#request.Type == 'ACH Batch' || #request.Type == 'Recurring ACH Batch' || #request.Type == 'Tax Payment' ||
						 				#request.Type == 'Recurring Tax Payment' || #request.Type == 'Child Support Payment' || #request.Type == 'Recurring Child Support Payment'}">
									<td align="right" class="columndata" style="color:red;">
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
						                <ffi:cinclude value1="${ACHBatch.IsCompletedStatus}" value2="true" operator="notEquals">
						                    <ffi:cinclude value1="${ACHBatch.CoEntryDesc}" value2="REVERSAL" operator="notEquals">
										<b><ffi:getL10NString rsrcFile="cb" msgKey="da.message.ach.NonManagedACHParticipantsInRed"/></b>
						                    </ffi:cinclude>
						                </ffi:cinclude>
									</ffi:cinclude>
									</td>
									End: Dual approval processing
										</s:if>	
									<s:else>
										<td align="right" class="columndata">&nbsp;</td>		
									</s:else>  --%>
											
							</div>
							
							
							<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.common_137"/>:</span>
								<span>
									<ffi:getProperty name="IncludeViewTransfer" property="TrackingID"/>
								</span>	
								<ffi:cinclude value1="${IncludeViewTransfer.Frequency}" value2="" operator="notEquals">
								     <span id="pendingApprovalTransctionLblId" class="sectionsubhead sectionLabel"><s:text name="jsp.default_215"/></span>
							        <span><ffi:getProperty name="IncludeViewTransfer" property="Frequency"/></span>	
		         				</ffi:cinclude>
							</div>
						</div>
						<div class="blockRow">
							<div class="inlineBlock" style="width: 50%">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_508" /> :</span>
								<span>
									<ffi:getProperty name="ApprovalsItem" property="Item.SubmissionDateTime" />
								</span>	
							</div>
							
							<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_398" /> :</span>
								<span>
									<ffi:getProperty name="ApprovalsItem" property="SubmittingUserName" />
								</span>	
							</div>
							
						</div>	
						<ffi:setProperty name="ResultOfApproval" value="${ApprovalsItem.Item.Transaction.ResultOfApproval}" />
						<ffi:cinclude value1="${ResultOfApproval}" value2="" operator="notEquals">
							<div class="blockRow">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_399" /> :</span></ffi:cinclude>
									<ffi:cinclude value1="${ResultOfApproval}" value2="" operator="notEquals">
									<span>
										<ffi:getProperty name="ResultOfApproval" />	    
									</span>	
							</div>		
						</ffi:cinclude>			
					</div>			
  			</div>
 <div class="marginTop10"></div>  
<ffi:removeProperty name="ApprovalsItem" />
<ffi:removeProperty name="itemID" />