<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../jsp/common/wire_labels.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<%
String wireDest = "";
String wireDom = com.ffusion.beans.wiretransfers.WireDefines.WIRE_DOMESTIC;
String wireInt = com.ffusion.beans.wiretransfers.WireDefines.WIRE_INTERNATIONAL;
String wireTemp = com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_TEMPLATE;
String wireRecTemp = com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_RECTEMPLATE;
String curType = "";
String curSource = "";
String restricted = "false";
String accountDisplayText = "";
boolean showTemplateFields = false;
%>

<ffi:getProperty name="WireTransfer" property="WireDestination" assignTo="wireDest"/>
<ffi:getProperty name="WireTransfer" property="WireType" assignTo="curType"/>
<ffi:getProperty name="WireTransfer" property="WireSource" assignTo="curSource"/>
<ffi:setProperty name="WireTransfer" property="DateFormat" value="${UserLocale.DateFormat}"/>
<%
if (curType == null) curType = "";
if (curType.equals(wireTemp) || curType.equals(wireRecTemp) || curSource.equals("TEMPLATE")) showTemplateFields = true;
%>
<ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText" assignTo="accountDisplayText"/>

<%
if(accountDisplayText == null && accountDisplayText.equalsIgnoreCase("Restricted")) {
	restricted = "true";
}
%>
<%-- ---- CONFIRMATION NUMBERS ---- --%>
<div class="confirmationDetails" style="width:50%; float:left; text-align:left">
	<span><s:text name="jsp.default_435"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="TrackingID"/></span>
</div>
<div class="confirmationDetails" style="width:49%; float:left; text-align:left">
	<span><s:text name="jsp.default_46"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="ReferenceNumber"/></span>
</div>
<div class="confirmationDetails" style="width:50%; float:left; text-align:left">
	<span><s:text name="jsp.default_229"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="ConfirmationNum"/></span>
</div>
<div class="confirmationDetails" style="width:49%; float:left; text-align:left">
	<span><s:text name="jsp.common_79"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="FedConfirmationNum"/></span>
</div>
<div class="marginTop20" style="clear:both"></div>
<% if (curType.equals(wireTemp) || curType.equals(wireRecTemp) || curSource.equals("TEMPLATE")) { %>
<%-- ------ TEMPLATE (READ-ONLY) NAME, NICKNAME, ID, AND WIRE LIMIT ------ --%>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_413"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				 <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_416"/></span>
                 <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_412"/></span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="TemplateID"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_417"/></span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="NickName"/>&nbsp;</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_415"/></span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="DisplayWireLimit"/></span>
			</div>
		</div>
	</div>
</div>
<% } %>        
<div  class="blockHead toggleClick">Beneficiary details <span class="sapUiIconCls icon-positive"></span></div>
<div class="toggleBlock hidden">
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_379"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_71"/>: </span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.PayeeName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>: </span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>: </span>
            	 <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>: </span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_386"/>: </span>
                 <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.StateDisplayName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>: </span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.ZipCode"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_110"/>: </span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.Contact"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_121"/>: </span>
                 <span class="columndata">
					<ffi:cinclude value1="${WireTransfer.accountNickname}" value2="" operator="notEquals">
							<ffi:getProperty name="WireTransfer" property="accountNickname"/>
					</ffi:cinclude>
			
					<ffi:cinclude value1="${WireTransfer.accountNickname}" value2="" operator="equals">
							<ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/>
					</ffi:cinclude>
		</span>
			</div>
		</div>		
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_69"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_63"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.DestinationBank.BankName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
                 <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.DestinationBank.Street"/>:</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.DestinationBank.Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.DestinationBank.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_386"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.DestinationBank.State"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.DestinationBank.ZipCode"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_201"/>:</span>
            <span class="columndata"><ffi:getProperty name="WireTransfer" property="WireCreditInfo.DestinationBank.RoutingFedWire"/></span>
		</div>
	</div>
</div>
</div>      
<%-- DEBIT ACCOUNT INFO --%>   
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart--><s:text name="jsp.default_153"/><!--L10NEnd--></div>
	<div  class="blockHead"><!--L10NStart--><s:text name="jsp.default_148"/><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_DRAWDOWN_ACCOUNT_NUMBER %>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.AccountNum"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_DRAWDOWN_ACCOUNT_TYPE %>:</span>
                <span class="columndata">
                    <ffi:getProperty name="WireTransfer" property="WirePayee.AccountTypeDisplayName"/>
                </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_DRAWDOWN_ACCOUNT_NAME %>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Name"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_294"/>:</span>
		    	<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.NickName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street2"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.City"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_386"/>:</span>
                 <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.StateDisplayName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.ZipCode"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_110"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Contact"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_150"/>:</span>
            <span class="columndata"><ffi:setProperty name="WirePayeeScopes" property="Key" value="${WireTransfer.WirePayee.PayeeScope}"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></span>
		</div>
	</div>
</div>    

<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_152"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_154"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.BankName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_386"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.State"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.ZipCode"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_151"/>:</span>
            <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingFedWire"/></span>
		</div>
	</div>
</div>
<%-- DOMESTIC --%>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_122"/></div>
	<div class="blockContent">
		<ffi:setProperty name="temp_amount" value="Amount"/>
		<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_TEMPLATE %>" operator="equals">
		    <ffi:setProperty name="temp_amount" value="Template Limit"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="equals">
		    <ffi:setProperty name="temp_amount" value="Template Limit"/>
		</ffi:cinclude>
		<div class="blockRow">
				<span class="sectionsubhead sectionLabel"><ffi:getProperty name="temp_amount"/><ffi:removeProperty name="temp_amount"/>: </span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="Amount"/></span>
		</div>
		<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_TEMPLATE %>" operator="notEquals">
                   <ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="notEquals">
					<div class="blockRow">
						<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_457"/>:</span>
                            <span class="columndata"><ffi:getProperty name="WireTransfer" property="DateToPost"/></span>
						</div>
						<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_141"/>:</span>
                            <span class="columndata"><ffi:getProperty name="WireTransfer" property="DateToPost"/></span>
						</div>
					</div>
			</ffi:cinclude>
         </ffi:cinclude>
		<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="equals">
			<s:set var="isRecTemplate">true</s:set>
		</ffi:cinclude>
        <ffi:cinclude value1="${WireTransfer.Type}" value2="<%= String.valueOf(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) %>" operator="equals">
		<s:if test="%{#attr.viewRecurringModel || #isRecTemplate='true'}">
            <div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
                    <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_215"/>:</span>
                    <span class="columndata">
                        <ffi:getProperty name="WireTransfer" property="Frequency"/></span>
                </div>
                <div class="inlineBlock">
                    <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/>:</span>
                    <span class="columndata">
                    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="equals">
                        <s:text name="jsp.default_446"/>
                    </ffi:cinclude>
                    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="notEquals">
                        <ffi:getProperty name="WireTransfer" property="NumberTransfers"/>
                    </ffi:cinclude>
                    </span>
                </div>
			</div>
				<s:if test="%{#isRecTemplate!='true'}">
               <div class="blockRow">
		    <div class="inlineBlock" style="width: 50%">
			    <span id="viewWireEndDateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default.end.date"/> : </span>
			    <span id="viewWireEndDateValue" class="columndata">
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="equals">
					<s:text name="jsp.default.not.applicable"/>
			    </ffi:cinclude>
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="notEquals">
					<ffi:getProperty name="WireTransfer" property="EndDate"/>
			    </ffi:cinclude>

			    </span>
		    </div>
			
		    <div class="inlineBlock">
			    <span id="viewWireFuturePaymentsLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default.future.payments"/>: </span>
			    <span id="viewWireFuturePaymentsValue" class="columndata">			    
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="equals">
					<s:text name="jsp.default.not.applicable"/>
			    </ffi:cinclude>
			    <ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="notEquals">
					<ffi:getProperty name="WireTransfer" property="NumberOfPendingTransfers"/>
			    </ffi:cinclude>			    
			    </span>
		    </div>
			
		</div>
		</s:if>
		
		</s:if>
                </ffi:cinclude>    
	
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_348"/>: <ffi:getProperty name="WireTransfer" property="Comment"/></div>
	<div  class="blockHead"><s:text name="jsp.default_307"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<% String info = ""; %>
            <ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo1"/>
            <ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo2}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo2"/></ffi:cinclude>
            <ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo3}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo3"/></ffi:cinclude>
            <ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo4}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo4"/></ffi:cinclude>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_64"/></div>
	<div class="blockRow">
		<ffi:getProperty name="WireTransfer" property="BankToBankInfo1"/>
       <ffi:cinclude value1="${WireTransfer.BankToBankInfo2}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo2"/></ffi:cinclude>
       <ffi:cinclude value1="${WireTransfer.BankToBankInfo3}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo3"/></ffi:cinclude>
       <ffi:cinclude value1="${WireTransfer.BankToBankInfo4}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo4"/></ffi:cinclude>
       <ffi:cinclude value1="${WireTransfer.BankToBankInfo5}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo5"/></ffi:cinclude>
       <ffi:cinclude value1="${WireTransfer.BankToBankInfo6}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo6"/></ffi:cinclude>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_78"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_283"/></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress1"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress3"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_19"/></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAccount"/></span>
		</div>
	</div>
</div>
            <table width="750" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class='<ffi:getProperty name="wire_details_background_color"/>'>
                        <div align="center">

<ffi:cinclude value1="<%=restricted%>" value2="true" operator="notEquals">
	<s:if test="%{isVirtualInstance !='true'}">
		<s:include value="%{#session.PagesPath}/common/include-view-wire-history.jsp"/>
	</s:if>
</ffi:cinclude>
                        <br>
			<% if( session.getAttribute( "wire_details_post_jsp" ) == null ) { %>
                            <ffi:setProperty name="wireViewDoneURL" value="payments/wiretransfers.jsp"/>
                            <ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= wireTemp %>" operator="equals">
                                <ffi:setProperty name="wireViewDoneURL" value="payments/wiretemplates.jsp"/>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= wireRecTemp %>" operator="equals">
                                <ffi:setProperty name="wireViewDoneURL" value="payments/wiretemplates.jsp"/>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${collectionName}" value2="WiresRelease" operator="equals">
                                <ffi:setProperty name="wireViewDoneURL" value="payments/wiresrelease.jsp?DontInitialize=true" URLEncrypt="true"/>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${collectionName}" value2="WireReportRecords" operator="equals">
                                <ffi:setProperty name="wireViewDoneURL" value="payments/wiretransferreport.jsp?haveReport=false&save=true&mod=false&DontInitializeReport=true" URLEncrypt="true"/>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${collectionName}" value2="WireBatch.Wires" operator="equals">
                                <ffi:setProperty name="wireViewDoneURL" value="payments/wirebatchview.jsp?DontInitialize=true" URLEncrypt="true"/>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${collectionName}" value2="WireTransfersByBatchId" operator="equals">
                                <ffi:setProperty name="wireViewDoneURL" value="payments/wirebatchview.jsp?DontInitialize=true" URLEncrypt="true"/>
                            </ffi:cinclude>
			    <%
				StringBuffer tempURLBuf = new StringBuffer( ( String )session.getAttribute( "SecurePath" ) );
				tempURLBuf.append( ( String )session.getAttribute( "wireViewDoneURL" ) );
				session.setAttribute( "wireViewDoneURL", tempURLBuf.toString() );
			    %>
			<% } else { %>
		    		<ffi:setProperty name="wireViewDoneURL" value="${wire_details_post_jsp}" />
			<% } %>
			<% if( session.getAttribute( "WireBatch" ) != null && 
				    session.getAttribute( "ApprovalsViewTransactionDetails") != null ) { %>
				<ffi:setProperty name="wireViewDoneURL" value="${SecurePath}${approval_view_wirebatch_jsp}"/>
			<% } %>
                            <%--<input type="Button" value="DONE" class="submitbutton" onClick="javascript:document.location='<ffi:getProperty name="wireViewDoneURL"/>'">--%>
                             <div align="center">
								<%-- <sj:a button="true" title="DONE" onClickTopics="closeDialog">DONE</sj:a> --%>
							</div>
							<ffi:removeProperty name="wireViewDoneURL"/>
                            <ffi:removeProperty name="collectionName"/>
                        </div>
            </td>
        </tr>
    </table>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-positive").addClass("icon-negative")
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-positive").removeClass("icon-negative")
			}
		});
	});

	function viewModel() {
	var urlString = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/viewWireTransferAction.action?ViewRecurringModel=true&transType=RECURRING&recurringId=${WireTransfer.recurringID}&IsVirtualInstance=${WireTransfer.IsVirtualInstance}'/>";

		$.ajax({
			url: urlString,
			success: function(data){
				ns.common.closeDialog();
				$('#viewRecurringWireModelDetailsDialogID').html(data).dialog('open');
			}
		});
	}
</script>