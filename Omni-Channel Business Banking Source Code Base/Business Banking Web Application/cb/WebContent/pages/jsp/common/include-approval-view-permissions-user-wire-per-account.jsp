<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.HashSet" %>

<%
int bandCtr = 0;
String backgroundColour = "columndata";
String darkRow = "";
String lightRow = "";
%>

<ffi:object id="BankIdentifierDisplayTextTask" name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText" scope="session"/>
<ffi:process name="BankIdentifierDisplayTextTask"/>
<ffi:setProperty name="TempBankIdentifierDisplayText"   value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask"/>

<ffi:getProperty name="approval_view_permissions_table_dark_row" assignTo="darkRow"/>
<ffi:getProperty name="approval_view_permissions_table_light_row" assignTo="lightRow"/>
    <%
    bandCtr = (bandCtr + 1) % 2;
    backgroundColour = (bandCtr == 0) ? darkRow : lightRow;
    %>
<%-- --- Display Per-Account Entitlements -------------------------------- --%>
    <ffi:setProperty name="DisplayedHeader" value="false"/>
    <ffi:setProperty name="Entitlement_Types" property="Filter" value="category=per account"/>

    <ffi:cinclude value1="${Entitlement_Types.Size}" value2="0" operator="notEquals">
	<ffi:object name="com.ffusion.tasks.admin.GetAccountsForUser" id="GetAccountsForUser" scope="session"/>
	<ffi:setProperty name="GetAccountsForUser" property="GroupID" value="${User.EntitlementGroupId}"/>
	<ffi:setProperty name="GetAccountsForUser" property="MemberID" value="${User.Id}"/>
	<ffi:setProperty name="GetAccountsForUser" property="UserType" value="${User.MemberSubType}"/>
	<ffi:setProperty name="GetAccountsForUser" property="GroupAccountsName" value="UserAccounts"/>
	<ffi:process name="GetAccountsForUser"/>
    </ffi:cinclude>

    <ffi:list collection="Entitlement_Types" items="EntType">
	<ffi:setProperty name="EntType"
		property="CurrentProperty"
		value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_HIDE %>"/>
	<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="false" operator="equals">
	    <ffi:setProperty name="EntType"
		    property="CurrentProperty"
		    value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
	    <ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="equals">
		<ffi:setProperty name="EntTypeDisplayValue" value="${EntType.Value}"/>
	    </ffi:cinclude>
	    <ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="false" operator="equals">
		<ffi:setProperty name="EntTypeDisplayValue" value="${EntType.OperationName}"/>
	    </ffi:cinclude>

	    <ffi:setProperty name="UserAccounts" property="SortedBy" value="ID"/>
	    <ffi:list collection="UserAccounts" items="UserAccount">
		<ffi:setProperty name="GetEntitlementObjectIDUtil"
			property="ObjectType"
			value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
		<ffi:setProperty name="GetEntitlementObjectIDUtil"
			property="CurrentPropertyName"
			value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>"/>
		<ffi:setProperty name="GetEntitlementObjectIDUtil"
			property="CurrentPropertyValue"
			value="${UserAccount.ID}"/>
		<ffi:setProperty name="GetEntitlementObjectIDUtil"
			property="CurrentPropertyName"
			value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_BANK_ID %>"/>
		<ffi:setProperty name="GetEntitlementObjectIDUtil"
			property="CurrentPropertyValue"
			value="${UserAccount.BankID}"/>
		<ffi:setProperty name="GetEntitlementObjectIDUtil"
			property="CurrentPropertyName"
			value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>"/>
		<ffi:setProperty name="GetEntitlementObjectIDUtil"
			property="CurrentPropertyValue"
			value="${UserAccount.RoutingNum}"/>
		<ffi:process name="GetEntitlementObjectIDUtil"/>

		<ffi:removeProperty name="CheckEntitlementByMember"/>
		<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMemberCB"
			id="CheckEntitlementByMember"
			scope="session"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="${EntType.OperationName}"/>
		<ffi:setProperty name="CheckEntitlementByMember"
			property="ObjectType"
			value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="ObjectId" value="${GetEntitlementObjectIDUtil.ObjectID}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="GroupId" value="${User.EntitlementGroupId}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="MemberId" value="${User.Id}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="MemberType" value="${User.MemberType}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="MemberSubType" value="${User.MemberSubType}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="AttributeName" value="IsEntitled"/>
		<ffi:process name="CheckEntitlementByMember"/>

		<ffi:setProperty name="DisplayedLimit" value="false"/>
		<ffi:setProperty name="IDDisplayValue" value=""/>

		<ffi:cinclude value1="${IsEntitled}" value2="TRUE" operator="equals">

		    <ffi:removeProperty name="GetCompressedLimits"/>
		    <ffi:object name="com.ffusion.efs.tasks.entitlements.GetCompressedLimits" id="GetCompressedLimits" scope="session"/>
		    <ffi:setProperty name="GetCompressedLimits" property="GroupId" value="${User.EntitlementGroupId}"/>
		    <ffi:setProperty name="GetCompressedLimits" property="OperationName" value="${EntType.OperationName}"/>
		    <ffi:setProperty name="GetCompressedLimits" property="MemberId" value="${User.Id}"/>
		    <ffi:setProperty name="GetCompressedLimits" property="MemberType" value="${User.MemberType}"/>
		    <ffi:setProperty name="GetCompressedLimits" property="MemberSubType" value="${User.MemberSubType}"/>
		    <ffi:setProperty name="GetCompressedLimits" property="ObjectType" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>"/>
		    <ffi:setProperty name="GetCompressedLimits" property="ObjectId" value="${GetEntitlementObjectIDUtil.ObjectID}"/>
		    <ffi:process name="GetCompressedLimits"/>

		    <ffi:setProperty name="RoutingNumDisplayValue" value="${UserAccount.RoutingNum}"/>
		    <ffi:setProperty name="AccountNumDisplayValue" value="${UserAccount.DisplayText}"/>
		    <ffi:setProperty name="Limit1Data" value=""/>
		    <ffi:setProperty name="Limit2Data" value=""/>
		    <ffi:setProperty name="Limit3Data" value=""/>
		    <ffi:setProperty name="Limit4Data" value=""/>

		    <ffi:setProperty name="Entitlement_Limits" property="SortedBy" value="Period"/>
		    <ffi:list collection="Entitlement_Limits" items="Limit">
			<ffi:cinclude value1="${Limit.Period}" value2="1" operator="equals">
			    <ffi:setProperty name="Limit1Data" value="${Limit.Data}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Limit.Period}" value2="2" operator="equals">
			    <ffi:setProperty name="Limit2Data" value="${Limit.Data}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Limit.Period}" value2="3" operator="equals">
			    <ffi:setProperty name="Limit3Data" value="${Limit.Data}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Limit.Period}" value2="4" operator="equals">
			    <ffi:setProperty name="Limit4Data" value="${Limit.Data}"/>
			</ffi:cinclude>
		    </ffi:list>
			<ffi:cinclude value1="${DisplayedHeader}" value2="false" operator="equals">
	    <tr>
		<td colspan="7" style="<ffi:getProperty name='LimitsColumnHeadingStyle'/>" class="adminBackground">
		    <span class="sectionhead"><s:text name="jsp.common_116"/></span>
		</td>
	    </tr>
	    <tr>
		<td class="adminBackground"></td>
		<td class="adminBackground"></td>
		<td class="adminBackground"></td>
		<td colspan="4" class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="240" border="0"></td>
	    </tr>
	    <tr>
		<td class="adminBackground"></td>
		<td class="adminBackground"></td>
		<td class="adminBackground"></td>
		<td class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="60" border="0"></td>
		<td class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="60" border="0"></td>
		<td class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="60" border="0"></td>
		<td class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="60" border="0"></td>
	    </tr>
	    <tr>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_119"/></span>
		</td>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead">
		      <ffi:getProperty name="TempBankIdentifierDisplayText"/>
		    </span>
		</td>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.default_19"/></span>
		</td>
		<td colspan="4" style="<ffi:getProperty name='LimitsColumnHeadingStyle'/>" class="tbrd_b adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_96"/></span>
		</td>
	    </tr>
	    <tr>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		</td>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		</td>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		</td>
		<td style="<ffi:getProperty name='PeriodHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.default_436"/></span>
		</td>
		<td style="<ffi:getProperty name='PeriodHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_63"/></span>
		</td>
		<td style="<ffi:getProperty name='PeriodHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_174"/></span>
		</td>
		<td style="<ffi:getProperty name='PeriodHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_99"/></span>
		</td>
	    </tr>
			    <ffi:setProperty name="DisplayedHeader" value="true"/>
			</ffi:cinclude>
	    <tr>
		<td style="<ffi:getProperty name='NonLimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="EntTypeDisplayValue"/></span>
		</td>
		<td style="<ffi:getProperty name='NonLimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="RoutingNumDisplayValue"/></span>
		</td>
		<td style="<ffi:getProperty name='NonLimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="AccountNumDisplayValue"/></span>
		</td>
		<td style="<ffi:getProperty name='LimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="Limit1Data"/></span>
		</td>
		<td style="<ffi:getProperty name='LimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="Limit2Data"/></span>
		</td>
		<td style="<ffi:getProperty name='LimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="Limit3Data"/></span>
		</td>
		<td style="<ffi:getProperty name='LimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="Limit4Data"/></span>
		</td>
	    </tr>

			<ffi:setProperty name="EntTypeDisplayValue" value=""/>
			<ffi:setProperty name="DisplayedLimit" value="true"/>
			<ffi:setProperty name="RoutingNumDisplayValue" value=""/>
			<ffi:setProperty name="AccountNumDisplayValue" value=""/>
		</ffi:cinclude>
	    </ffi:list>
	</ffi:cinclude>
    </ffi:list>

    <ffi:cinclude value1="${DisplayedHeader}" value2="true" operator="equals">
	<ffi:setProperty name="UserHasData" value="true"/>
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_dark_row'/>">&nbsp;</td>
	    </tr>
    </ffi:cinclude>