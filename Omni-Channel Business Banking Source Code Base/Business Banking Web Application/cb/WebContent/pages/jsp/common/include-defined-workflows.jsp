<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------

// the entitlement group id of the business
int _busEntGroupID;
// determines the destination i.e which page should be refreshed when
// value of the combo box containg the operation types is changed
String _destination;


_destination = ( String )session.getAttribute( "SecurePath" ) +
	( String )session.getAttribute( "approval_display_workflow_jsp" );
_busEntGroupID = ((com.ffusion.beans.business.Business)session.getAttribute( "Business" )).getEntitlementGroupIdValue();


String _defaultOperationType = "All other activities";
com.ffusion.beans.approvals.ApprovalsGroups apprGroups = null;
String _opType = request.getParameter( "OpType" );
//String _opType = (String)session.getAttribute( "OpType" );
if( _opType == null ) {
        _opType = _defaultOperationType;
} else if( _opType.length() == 0 ) {
        _opType = _defaultOperationType;
}

String _setOperationType = "true";
if( _opType.equalsIgnoreCase( _defaultOperationType ) ) {
    _setOperationType = "false";
}

//Account Search Criteria
String _defaultAccount = "All Accounts";
String _accountID = request.getParameter( "AccountID" );
if( _accountID == null ) {
        _accountID = _defaultAccount;
} else if( _accountID.length() == 0 ) {
        _accountID = _defaultAccount;
}

String _setAccountID = "true";
if( _accountID.equalsIgnoreCase( _defaultAccount ) ) {
    _setAccountID = "false";
}


%>


<ffi:setProperty name="ApprovalsLevelCurrencyCode" value="" />

<ffi:object id="GetCurrencyCode" name="com.ffusion.tasks.GetCurrencyCode" scope="session" />
<ffi:setProperty name="GetCurrencyCode" property="BusinessID" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetCurrencyCode" />
<ffi:setProperty name="ApprovalsLevelCurrencyCode" value="${GetCurrencyCode.CurrencyCode}"/>
<ffi:removeProperty name="GetCurrencyCode" />

<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
<ffi:process name="GetLimitBaseCurrency" />
<ffi:cinclude value1="${ApprovalsLevelCurrencyCode}" value2="" operator="equals">
    <ffi:setProperty name="ApprovalsLevelCurrencyCode" value="${BaseCurrency}" />
</ffi:cinclude>

<%-- determine whether the page should be viewed in read-only mode --%>
<% boolean _disableEdit = false; %>

<ffi:cinclude ifNotEntitled="Approvals Workflow Admin">
	<% _disableEdit = true; %>
</ffi:cinclude>



    <%-- get the workflow levels --%>
    <ffi:object id="GetWorkflowLevels" name="com.ffusion.tasks.approvals.GetWorkflowLevels" scope="session"/>
    <ffi:setProperty name="GetWorkflowLevels" property="LevelType" value="<%= com.ffusion.approvals.constants.IApprovalsConsts.LEVEL_TYPE_BANK_DEFINED %>"/>

        <ffi:object id="GetApprovalGroups" name="com.ffusion.tasks.approvals.GetApprovalGroups" scope="session"/>
	<ffi:process name="GetApprovalGroups" />
	<% apprGroups = ( com.ffusion.beans.approvals.ApprovalsGroups )session.getAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_GROUPS ); %>

    <ffi:cinclude value1="<%= _setOperationType %>" value2="true" operator="equals">
	<ffi:setProperty name="GetWorkflowLevels" property="OperationType" value="<%= _opType %>"/>
    </ffi:cinclude>

	<ffi:cinclude value1="<%=_setAccountID%>" value2="true" operator="equals">
	<%	if( session.getAttribute("ParentsAccounts") == null ){ //check if the accounts are present in the session.If not set the account %>
			<ffi:setProperty name="EditGroup_GroupId" value="${Business.EntitlementGroupId}"/>
			<ffi:object id="GetAccountsForGroup" name="com.ffusion.tasks.admin.GetAccountsForGroup" scope="session"/>
			<ffi:setProperty name="GetAccountsForGroup" property="GroupID" value="${EditGroup_GroupId}"/>
			<ffi:setProperty name="GetAccountsForGroup" property="GroupAccountsName" value="ParentsAccounts"/>
			<ffi:setProperty name="GetAccountsForGroup" property="CheckAccountGroupAccess" value="false"/>
			<ffi:setProperty name="GetAccountsForGroup" property="CheckAdminAccess" value="true"/>
			<ffi:process name="GetAccountsForGroup"/>
	<%	} %>

		<%
			String id = request.getParameter("AccountID");
		%>
		<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
		<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="ParentsAccounts"/>
		<ffi:setProperty name="SearchAcctsByNameNumType" property="Id" value="<%=id%>"/>
		<ffi:process name="SearchAcctsByNameNumType"/>
		<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
		<ffi:setProperty name="SetAccount" property="AccountsName" value="FilteredAccounts"/>
		<ffi:setProperty name="SetAccount" property="ID" value="<%=id%>"/>
		<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount"/>
		<ffi:process name="SetAccount"/>
		<ffi:setProperty name="GetWorkflowLevels" property="AccountID" value="<%=id%>"/>
	</ffi:cinclude>

    <ffi:process name="GetWorkflowLevels"/>

	 <ffi:object id="GetWorkflows" name="com.ffusion.tasks.approvals.GetWorkflows" scope="session"/>
	 <ffi:process name="GetWorkflows"/>

    <%
	String isApprovalsLevelsNull = "true";
	if( session.getAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_LEVELS ) != null ) {
	    isApprovalsLevelsNull = "false";
	}

    %>

	<ffi:setProperty name="WorkflowID" value="-1" />
	<ffi:setProperty name="WorkflowName" value="Default Workflow" />

    <ffi:cinclude value1="<%= isApprovalsLevelsNull %>" value2="false" operator="equals">
	<s:set var="workFlwCount" value="1"/>
	<ffi:list collection="ApprovalsWorkflows" items="ApprovalsWorkflow">



	<div class=""  id="approval_defined_workflows_pane">
	<ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="equals">
	<div class="header">
	<table cellspacing="0" cellpading="0" border="0" width="100%">
		<tbody><tr id="tableHeader"  style="display: table-row;">
			<td width="100%">Workflow: <ffi:getProperty name="ApprovalsWorkflow" property="WorkflowName"/></td>
			</tr>
		</tbody>
	</table>
	</div>
	</ffi:cinclude>
	<div class="header">
										<table cellspacing="0" cellpading="0" border="0" width="100%">
											<tr id="tableHeader">
												<td width="20%"><s:text name="jsp.approvals.workflow.levels" /></td>
												<td width="30%"><s:text name="jsp.common_161"/>(<ffi:getProperty name="ApprovalsLevelCurrencyCode" />)</td>
												<td width="30%"><s:text name="jsp.approvals.workflow.approval.chain" /></td>
												<td width="20%"><s:text name="jsp.approval_daAction_7" /></td>
											</tr>
											<tr id="noDataHeader">
												<td colspan="4"><s:text name="jsp.approvals.workflow.levels" /></td>
											</tr>
										</table>
	</div>
	<s:if test="%{#workFlwCount==1}">
		<ffi:setProperty name="WorkflowID" value="${ApprovalsWorkflow.WorkflowId}" />
		<ffi:setProperty name="WorkflowName" value="${ApprovalsWorkflow.WorkflowName}" />
	</s:if>
	<%
	int levelCount = 1;
	%>
	<ffi:list collection="ApprovalsWorkflow.ApprovalsLevels" items="ApprovalsLevel1">

	    <table width="100%" border="0" cellspacing="0" cellpadding="0" <%= levelCount % 2 == 0 ? "class=\"columndata jqgrow ltback\"" : "class=\"columndata jqgrow\""  %>>
			<tr>
				<td width="20%" class="sectionsubhead" valign="top">&nbsp;<s:text name="jsp.default_259"/> <%= levelCount %> </td>
				<td width="30%" valign="top">
					<ffi:getProperty name="ApprovalsLevel1" property="MinAmount.CurrencyStringNoSymbol"/> - <ffi:getProperty name="ApprovalsLevel1" property="MaxAmount.CurrencyStringNoSymbol" />
				</td>
				<td width="30%" valign="top">
					 <% levelCount++; %>

					    <ffi:object id="GetApprovalsLevel" name="com.ffusion.tasks.approvals.GetApprovalsLevel" scope="session"/>
					    <ffi:setProperty name="GetApprovalsLevel" property="LevelID" value="${ApprovalsLevel1.LevelID}"/>
					    <ffi:process name="GetApprovalsLevel"/>
					    <ffi:object id="GetWorkflowChainItems" name="com.ffusion.tasks.approvals.GetWorkflowChainItems" scope="session"/>
					    <ffi:process name="GetWorkflowChainItems"/>

					    <%
						String isChainItemsNull = "true";
						if( session.getAttribute( "ApprovalsChainItems" ) != null ) {
						    isChainItemsNull = "false";
						}
					    %>

					    <ffi:cinclude value1="<%= isChainItemsNull %>" value2="false" operator="equals">
					    <ol class="apprChainOlList">
						    <ffi:list collection="ApprovalsChainItems" items="ApprovalsChainItem1">
							<li class="submain">
							    <ffi:cinclude value1="${ApprovalsChainItem1.UsingUser}" value2="true" operator="equals">

								    <ffi:object id="ApprovalsChainItemUserName" name="com.ffusion.tasks.user.GetBusinessEmployee" scope="session"/>
								    <ffi:setProperty name="ApprovalsChainItemUserName" property="ProfileId" value="${ApprovalsChainItem1.GroupOrUserID}"/>
								    <ffi:process name="ApprovalsChainItemUserName"/>
								    <ffi:getProperty name="BusinessEmployee" property="SortableFullNameWithLoginId"/> (<s:text name="jsp.common_164"/>)
			                                            <ffi:removeProperty name="BusinessEmployee"/>

							    </ffi:cinclude>

							    <% com.ffusion.beans.approvals.ApprovalsGroup apprGroup = null;
							       String apprGroupID;
							    %>
							    	<ffi:cinclude value1="${ApprovalsChainItem1.IsApprovalsGroup}" value2="true" operator="equals">
							    	<ffi:setProperty name="ApprovalGroupID" value="${ApprovalsChainItem1.GroupOrUserID}"/>
							    	<ffi:getProperty name="ApprovalGroupID" assignTo="apprGroupID"/>
							    	<% if ( apprGroups != null ) {
							    		apprGroup = apprGroups.getByID( Integer.parseInt( apprGroupID ) );
							    	   }
							    	%>
							    	<ffi:setProperty name="ApprovalsGroupName" value="<%=apprGroup.getGroupName()%>" />
							    	<ffi:getProperty name="ApprovalsGroupName"/>&nbsp;(<s:text name="jsp.common_23"/>)
							    	</ffi:cinclude>

							    <ffi:cinclude value1="${ApprovalsChainItem1.UsingUser}" value2="true" operator="notEquals">
							    <ffi:cinclude value1="${ApprovalsChainItem1.IsApprovalsGroup}" value2="true" operator="notEquals">
								<ffi:object id="GetEntitlementGroupPropertyValues" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroupPropertyValues" scope="session"/>
								<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="PropertyName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
								<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="EntGroupId" value="${ApprovalsChainItem1.GroupOrUserID}"/>
								<ffi:object id="ApprovalsChainItemGroupName" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session"/>
								<ffi:setProperty name="ApprovalsChainItemGroupName" property="GroupId" value="${ApprovalsChainItem1.GroupOrUserID}"/>
								<ffi:process name="ApprovalsChainItemGroupName"/>
								<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="DefaultPropertyValue" value="${Entitlement_EntitlementGroup.GroupName}"/>
								<ffi:process name="GetEntitlementGroupPropertyValues"/>
								<ffi:getProperty name="GetEntitlementGroupPropertyValues" property="PropertyValue"/>
								<ffi:cinclude value1="${ApprovalsChainItem1.UserType}" value2="<%=String.valueOf(com.ffusion.approvals.constants.IApprovalsConsts.CASCADING_GROUP) %>">
									(<s:text name="jsp.common_36"/>)
								</ffi:cinclude>
								<ffi:cinclude value1="${ApprovalsChainItem1.UserType}" value2="0">
									(<s:text name="jsp.common_82"/>)
								</ffi:cinclude>
								<ffi:removeProperty name="Entitlement_EntitlementGroup"/>
							    </ffi:cinclude>
							    </ffi:cinclude>
							</li>
						    </ffi:list>
						</ol>
						<ffi:removeProperty name="ApprovalsChainItems"/>
					    </ffi:cinclude>
				</td>
				<td width="20%" valign="top" class="__gridActionColumn">
					<% if( !_disableEdit ) { %>
					<ffi:cinclude value1="<%=_setAccountID%>" value2="true" operator="equals">
						<%
			String id = request.getParameter("AccountID");
		%>
		<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
		<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="ParentsAccounts"/>
		<ffi:setProperty name="SearchAcctsByNameNumType" property="Id" value="<%=id%>"/>
		<ffi:process name="SearchAcctsByNameNumType"/>
		<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
		<ffi:setProperty name="SetAccount" property="AccountsName" value="FilteredAccounts"/>
		<ffi:setProperty name="SetAccount" property="ID" value="<%=_accountID%>"/>
		<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount"/>
		<ffi:process name="SetAccount"/>

					<% session.setAttribute("addEditAccountID",_accountID);%>
					<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' onClick=
		                   	"ns.approval.editLevel('<ffi:urlEncrypt url="${FullPagesPath}${approval_display_add_edit_workflow_jsp}?Edit=true&WorkflowID=${ApprovalsWorkflow.WorkflowId}&WorkflowName=${ApprovalsWorkflow.WorkflowName}&LevelID=${ApprovalsLevel1.LevelID}&OpType=${ApprovalsLevel1.OperationType}&MinAmt=${ApprovalsLevel1.MinAmount.CurrencyStringNoSymbol}&MaxAmt=${ApprovalsLevel1.MaxAmount.CurrencyStringNoSymbol}&AccountID=${addEditAccountID}"/>')">
		                   	<s:text name="jsp.default_178"/>
		                </a>
					</ffi:cinclude>
					<ffi:cinclude value1="<%=_setAccountID%>" value2="true" operator="notEquals">
					<% session.removeAttribute("addEditAccountID");%>
						<a class='ui-button ui-state-default' title='<s:text name="jsp.default_179" />' href='#' onClick=
		                   	"ns.approval.editLevel('<ffi:urlEncrypt url="${FullPagesPath}${approval_display_add_edit_workflow_jsp}?Edit=true&WorkflowID=${ApprovalsWorkflow.WorkflowId}&WorkflowName=${ApprovalsWorkflow.WorkflowName}&LevelID=${ApprovalsLevel1.LevelID}&OpType=${ApprovalsLevel1.OperationType}&MinAmt=${ApprovalsLevel1.MinAmount.CurrencyStringNoSymbol}&MaxAmt=${ApprovalsLevel1.MaxAmount.CurrencyStringNoSymbol}"/>')">
		                   	<s:text name="jsp.default_178"/>
		                </a>
					</ffi:cinclude>
					<% } %>

					<% if( !_disableEdit ) { %>

						<a class='ui-button ui-state-default' title='<s:text name="jsp.default_163" />' href='#' onClick=
		                   	"ns.approval.deleteLevel('<ffi:urlEncrypt url="${FullPagesPath}${approval_display_delete_workflow_jsp}?LevelID=${ApprovalsLevel1.LevelID}&OperationType=${ApprovalsLevel1.OperationType}&AccountID=${addEditAccountID}"/>')">
		                   	<s:text name="jsp.default_163"/>
		                </a>

					<% } %>
				</td>
			</tr>
		</table>
	</ffi:list>
	<ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="equals">
	<% if( !_disableEdit ) { %>

											<!-- <input class="submitbutton" onclick="gotoAdd()" type="button" name="addButton" value="ADD LEVEL"> -->
											<table cellspacing="0" cellpading="0" border="0" width="100%">
											<tr>
												<td colspan="4" align="right">


												 <a  title="Add Approval Workflow Level" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"  href='#' onClick=
												"ns.approval.addLevel('<ffi:urlEncrypt url="${FullPagesPath}${approval_display_add_edit_workflow_jsp}?Edit=false&OpType=${ApprovalsWorkflow.OperationType}&AccountID=${ApprovalsWorkflow.AccountID}&WorkflowID=${ApprovalsWorkflow.WorkflowId}&WorkflowName=${ApprovalsWorkflow.WorkflowName}"/>')"><span class="ui-button-text">
													  <s:text name="jsp.common_198" />
												    </span></a>

										   		 </td>

												 <td colspan="4" align="left">
												 <a  title="Add Approval Workflow Level" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"  href='#' onClick=
												"ns.approval.deleteWorkflow('<ffi:urlEncrypt url="${FullPagesPath}${approval_display_delete_workflow_jsp}?WorkflowID=${ApprovalsWorkflow.WorkflowId}&WorkflowName=${ApprovalsWorkflow.WorkflowName}&OperationType=${ApprovalsWorkflow.OperationType}&AccountID=${ApprovalsWorkflow.AccountID}"/>')"><span class="ui-button-text">
													  	Delete Workflow
												    </span></a>

										   		 </td>

										    </tr>
										    <tr><td colspan="4" align="center">&nbsp;</td></tr>
										</table>


	<% } %>
	</ffi:cinclude>
	</div>
	<s:set var="workFlwCount" value="%{#workFlwCount+1}"/>
	</ffi:list>
    </ffi:cinclude>
    <ffi:cinclude value1="<%= isApprovalsLevelsNull %>" value2="true" operator="equals">
    	<input type="hidden" id="approvalLevelDefined" name="approvalLevelDefined" value="false"/>
    	<s:text name="jsp.common_102"/>
    </ffi:cinclude>

<ffi:removeProperty name="ApprovalsLevelCurrencyCode" />

 <ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="equals"><!-- This is for Add Workflow button -->
 	<input type="hidden" name="WorkflowID" value="-1"/>
 </ffi:cinclude>
 <ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="notEquals"><!-- This is for Add Level button -->
	<input type="hidden" name="WorkflowID" value="<ffi:getProperty name='WorkflowID' />"/>
	<input type="hidden" name="WorkflowName" value="<ffi:getProperty name='WorkflowName' />" />
 </ffi:cinclude>
 <ffi:cinclude value1="${Business.MultipleApprovalWorkFlowSupport}" value2="1" operator="notEquals">
 <s:set var="workFlwCount" value="%{#workFlwCount-1}"/>
 <!-- This is if Multi Approval to None Approval and then you will have to delete one workflow for add level to work so disable Add Level button till workflow count is 1 -->
		<s:if test="%{#workFlwCount > 1}">
				<script>
					$('#addWorkflowLink').button({ disabled: true });
				</script>
		</s:if>
		<s:else>
				<script>
					$('#addWorkflowLink').button({ disabled: false });
				</script>
		</s:else>
 </ffi:cinclude>


