<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<script>
var excludeAccIdOperation=[
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DEPOSITS %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CASH_CON_DISBURSEMENTS %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ACH_PAYMENTS %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ARC_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_BOC_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CCD_Addenda_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CCD_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CIE_Addenda_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CIE_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CTX_Addenda_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_IAT_Addenda_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_IAT_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_POP_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PPD_Addenda_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_PPD_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_RCK_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TEL_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WEB_Addenda_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_WEB_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_XCK_ACH_Payment %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_ACH_TEMPLATE %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_CHILD_SUPPORT_PAYMENTS %>",
"<%= com.ffusion.approvals.constants.IApprovalsConsts.OPERATION_TYPE_TAX_PAYMENTS %>"
];
</script>