<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script type="text/javascript">
	function quickReSearchBankList(){
		$('#BankFormID').show();
		$('#searchDestinationBankDialogID').html('');
	}
	
	function closeQuickBankSearchResult(){
		ns.common.closeDialog("bankLookupDialogId");
	}
	
</script>

		<div align="center"  style="max-height:300px; overflow-x:hidden; overflow-y:auto;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color tableData tableAlerternateRowColor" id="searchResult">
				<tr>
					<td class="columndata" align="center" class="ltrow2_color">
						<form action="wirebanklist.jsp" method="post" name="BankForm" id="researchWireBankListFormID">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="isBankSearch" value="true"/>
						<input type="hidden" name="Inter" value="<ffi:getProperty name='Inter'/>"/>
						<input type="hidden" name="bankLookupRaturnPage" value="<ffi:getProperty name='bankLookupRaturnPage'/>"/>
						<input type="hidden" id="manageBeneficiary" name="manageBeneficiary" value="<ffi:getProperty name='manageBeneficiary'/>"/>
                    	<input type="hidden" name ="returnPage" id="returnPage" value="<ffi:getProperty name='returnPage'/>"/>
						<table width="100%" border="0" cellspacing="0" cellpadding="3" class="ltrow2_color">
							<%-- <tr>
								<td><span class="sectionhead"><!--L10NStart-->Bank Name<!--L10NEnd--></span></td>
								<td><span class="sectionhead"><!--L10NStart-->Address<!--L10NEnd--></span></td>
								<td><span class="sectionhead"><!--L10NStart-->ACH Routing No.<!--L10NEnd--></span></td>
								<td><span class="sectionhead"><!--L10NStart-->Wire Routing No.<!--L10NEnd--></span></td>
							</tr> --%>
							<script type="text/javascript">
								function showAddInstitutions(id,token){
									
										var urlString = "/cb/pages/common/QuickBankLookupAction_quickBankSelection.action";
										$.ajax({				  
											  url: urlString,
											  data :'financialInstitution.institutionId=' + id +"&CSRF_TOKEN=" + token,
											  type: "POST",
											  success: function(data) {
												  $('#searchResult').hide();
												 $('#searchedBankDetailsDialogID').html(data)
											  }
										});
								}
							</script>
							<s:iterator value="fis" var="fis">
								<tr id="billpay_bankListRowsID">
									<td height="40" valign="top">
										 <a class="link anchorText" href="javascript:showAddInstitutions
										 ('<s:property value="%{#fis.institutionId}"/>',
		 				  	             '<ffi:getProperty name='CSRF_TOKEN'/>')">
		 				  	             <ffi:getProperty name="Payee1" property="Name"/>
		 				  	             <s:property value="%{#fis.institutionName}"/>
		 				  	             </a>
									</td>
									<td>
										<s:property value="%{#fis.Street}"/>
											<s:if test="%{#fis.Street2!=''}">
												<s:property value="%{#fis.Street2}"/>
											</s:if>
										<s:property value="%{#fis.City}"/>
										<s:property value="%{#fis.State}"/>,
										<s:property value="%{#fis.StateCode}"/>
										<s:property value="%{#fis.Country}"/>
										<p><!--L10NStart--><span class="labelCls">ACH Routing No.</span><!--L10NEnd--><span class="valueCls"><s:property value="%{#fis.achRoutingNumber}"/></span></p>
										<p><!--L10NStart--><span class="labelCls">Wire Routing No.</span><!--L10NEnd--><span class="valueCls"><s:property value="%{#fis.wireRoutingNumber}"/></span></p>
									</td>
									<%-- <td>
										<s:property value="%{#fis.achRoutingNumber}"/>
									</td>
									<td>
										<s:property value="%{#fis.wireRoutingNumber}"/>
									</td> --%>
									</tr>
 				  	        </s:iterator>
							<%-- <% int wtbCount = 0;%>
							<ffi:list collection="WireTransferBanks" items="Bank1">
							<% wtbCount++; %>
							<tr id="bankListRowsID">
								<input id="bank1idID<%= wtbCount%>" type="hidden" value="<ffi:getProperty name='Bank1' property='ID'/>">
								<input id="interID<%= wtbCount%>" type="hidden" value="<ffi:getProperty name='Inter'/>">
								
								<td class="columndata"><a href="javascript:void(0)" onclick="bankListRowsIDClick('<ffi:urlEncrypt url='/cb/pages/jsp/wires/getWireTransferBanksAction_selectBank.action?bankId=${Bank1.ID}&Inter=${Inter}&manageBeneficiary=${manageBeneficiary}&bankLookupRaturnPage=${bankLookupRaturnPage}&returnPage=${returnPage}'/>')"><ffi:getProperty name="Bank1" property="BankName"/></a></td>
								<td class="columndata">
                                    					<ffi:cinclude value1="${Bank1.Street}" value2="" operator="notEquals">
                                        					<ffi:getProperty name="Bank1" property="Street"/><br>
                                    					</ffi:cinclude>
                                    					<ffi:cinclude value1="${Bank1.Street2}" value2="" operator="notEquals">
                                        					<ffi:getProperty name="Bank1" property="Street2"/><br>
                                    					</ffi:cinclude>
                                    					<ffi:getProperty name="Bank1" property="City"/>,&nbsp;
                                    					<ffi:cinclude value1="${Bank1.State}" value2="" operator="notEquals" ><ffi:getProperty name="Bank1" property="State" /></ffi:cinclude>
                                    					<ffi:cinclude value1="${Bank1.State}" value2="" operator="equals" ><ffi:getProperty name="Bank1" property="StateCode" /></ffi:cinclude>
                                    					<br>
                                    					<ffi:getProperty name="Bank1" property="Country"/>
								</td>
							</tr>
							</ffi:list> --%>
							<tr class="">
								<td colspan="2"  height="80" class="columndata" valign="top" align="center"><br>
								<!--L10NStart-->*If you don't find the bank, please refine your search and try again<!--L10NEnd--></td>
							</tr>
							<tr>
								<td colspan="4" align="center">
									<div class="ui-widget-header customDialogFooter">
										<sj:a 											
											id="researchBankListID"
											button="true" 
											onclick="quickReSearchBankList()"
								        	> <s:text name="jsp.default_374"/>
										</sj:a>
										<sj:a id="closeSerachBankListID" 
											button="true" 
											onclick="closeQuickBankSearchResult()"
										> <s:text name="jsp.default_102"/>
										</sj:a>
									</div>
                                </td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2">
					<div  id="searchedBankDetailsDialogID"></div>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</div>

