<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.HashSet" %>

<%
int bandCtr = 0;
String backgroundColour = "columndata";
String darkRow = "";
String lightRow = "";
%>

<ffi:getProperty name="approval_view_permissions_table_dark_row" assignTo="darkRow"/>
<ffi:getProperty name="approval_view_permissions_table_light_row" assignTo="lightRow"/>

<%-- --- Display Per-ACH Company Entitlements ---------------------------- --%>
    <ffi:removeProperty name="GetACHCompanies"/>
    <ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies" scope="session"/>
    <ffi:setProperty name="GetACHCompanies" property="CustID" value="${Business.Id}" />
    <ffi:setProperty name="GetACHCompanies" property="FIID" value="${Business.BankId}" />
    <ffi:setProperty name="GetACHCompanies" property="Reload" value="true"/>
    <ffi:setProperty name="GetACHCompanies" property="CompaniesInSessionName" value="BusinessACHCompanies"/>
	<ffi:setProperty name="GetACHCompanies" property="LoadCompanyEntitlements" value="false"/>
    <ffi:process name="GetACHCompanies"/>

    <ffi:setProperty name="DisplayedHeader" value="false"/>
    <ffi:setProperty name="Entitlement_Types" property="Filter" value="category=per ACH company"/>
    <ffi:list collection="Entitlement_Types" items="EntType">
	<ffi:setProperty name="EntType"
		property="CurrentProperty"
		value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_HIDE %>"/>
	<ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="false" operator="equals">

	    <ffi:setProperty name="SetModuleDisplayName" value="false"/>
	    <ffi:setProperty name="EntType"
		    property="CurrentProperty"
		    value="<%= com.ffusion.csil.core.common.EntitlementsDefines.MODULE_NAME %>"/>
	    <ffi:cinclude value1="${EntType.Value}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.MODULE_NAME_ACH %>" operator="equals">
		<ffi:setProperty name="ModuleDisplayValue" value="ACH - Overall"/>
		<ffi:setProperty name="SetModuleDisplayName" value="true"/>
	    </ffi:cinclude>
	    <ffi:cinclude value1="${EntType.Value}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.MODULE_NAME_ACH_BATCH %>" operator="equals">
		<ffi:setProperty name="ModuleDisplayValue" value="ACH Payments"/>
		<ffi:setProperty name="SetModuleDisplayName" value="true"/>
	    </ffi:cinclude>
	    <ffi:cinclude value1="${EntType.Value}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.MODULE_NAME_CHILD_SUPPORT %>" operator="equals">
		<ffi:setProperty name="ModuleDisplayValue" value="Child Support Payments"/>
		<ffi:setProperty name="SetModuleDisplayName" value="true"/>
	    </ffi:cinclude>
	    <ffi:cinclude value1="${EntType.Value}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.MODULE_NAME_TAX %>" operator="equals">
		<ffi:setProperty name="ModuleDisplayValue" value="Tax Payments"/>
		<ffi:setProperty name="SetModuleDisplayName" value="true"/>
	    </ffi:cinclude>
	    <ffi:cinclude value1="${SetModuleDisplayName}" value2="false" operator="equals">
		<ffi:setProperty name="ModuleDisplayValue" value="${EntType.OperationName}"/>
	    </ffi:cinclude>

	    <ffi:setProperty name="LastACHCompName" value=""/>
	    <ffi:setProperty name="BusinessACHCompanies" property="SortedBy" value="CONAME,COID"/>
	    <ffi:list collection="BusinessACHCompanies" items="ACHComp">
		<ffi:removeProperty name="CheckEntitlementByMember"/>
		<ffi:object name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByMemberCB"
			id="CheckEntitlementByMember"
			scope="session"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="OperationName" value="${EntType.OperationName}"/>
		<ffi:setProperty name="CheckEntitlementByMember"
			property="ObjectType"
			value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="ObjectId" value="${ACHComp.CompanyID}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="GroupId" value="${User.EntitlementGroupId}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="MemberId" value="${User.Id}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="MemberType" value="${User.MemberType}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="MemberSubType" value="${User.MemberSubType}"/>
		<ffi:setProperty name="CheckEntitlementByMember" property="AttributeName" value="IsEntitled"/>
		<ffi:process name="CheckEntitlementByMember"/>

		<ffi:cinclude value1="${IsEntitled}" value2="TRUE" operator="equals">
		    <ffi:cinclude value1="${LastACHCompName}" value2="${ACHComp.CompanyName}" operator="notEquals">
			<ffi:setProperty name="LastACHCompName" value="${ACHComp.CompanyName}"/>
		    </ffi:cinclude>

		    <ffi:setProperty name="Credit1Data" value=""/>
		    <ffi:setProperty name="Credit2Data" value=""/>
		    <ffi:setProperty name="Debit1Data" value=""/>
		    <ffi:setProperty name="Debit2Data" value=""/>

		    <ffi:setProperty name="EntType"
			    property="CurrentProperty"
			    value="<%= com.ffusion.tasks.approvals.MergeEntitlementsAndLimits.PROP_NAME_CREDIT_LIMIT_OP_NAME %>"/>
		    <ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="equals">
			<ffi:removeProperty name="GetCompressedLimits"/>
			<ffi:object name="com.ffusion.efs.tasks.entitlements.GetCompressedLimits" id="GetCompressedLimits" scope="session"/>
			<ffi:setProperty name="GetCompressedLimits"
				property="ObjectType"
				value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
			<ffi:setProperty name="GetCompressedLimits" property="ObjectId" value="${ACHComp.CompanyID}"/>
			<ffi:setProperty name="GetCompressedLimits" property="GroupId" value="${User.EntitlementGroupId}"/>
			<ffi:setProperty name="GetCompressedLimits" property="OperationName" value="${EntType.Value}"/>
			<ffi:setProperty name="GetCompressedLimits" property="MemberId" value="${User.Id}"/>
			<ffi:setProperty name="GetCompressedLimits" property="MemberType" value="${User.MemberType}"/>
			<ffi:setProperty name="GetCompressedLimits" property="MemberSubType" value="${User.MemberSubType}"/>
			<ffi:process name="GetCompressedLimits"/>

			<ffi:setProperty name="Entitlement_Limits" property="SortedBy" value="Period"/>
			<ffi:list collection="Entitlement_Limits" items="Limit">
			    <ffi:cinclude value1="${Limit.Period}" value2="1" operator="equals">
				<ffi:setProperty name="Credit1Data" value="${Limit.Data}"/>
			    </ffi:cinclude>
			    <ffi:cinclude value1="${Limit.Period}" value2="2" operator="equals">
				<ffi:setProperty name="Credit2Data" value="${Limit.Data}"/>
			    </ffi:cinclude>
			</ffi:list>
		    </ffi:cinclude>

		    <ffi:setProperty name="EntType"
			    property="CurrentProperty"
			    value="<%= com.ffusion.tasks.approvals.MergeEntitlementsAndLimits.PROP_NAME_DEBIT_LIMIT_OP_NAME %>"/>
		    <ffi:cinclude value1="${EntType.IsCurrentPropertySet}" value2="true" operator="equals">
			<ffi:removeProperty name="GetCompressedLimits"/>
			<ffi:object name="com.ffusion.efs.tasks.entitlements.GetCompressedLimits" id="GetCompressedLimits" scope="session"/>
			<ffi:setProperty name="GetCompressedLimits"
				property="ObjectType"
				value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>"/>
			<ffi:setProperty name="GetCompressedLimits" property="ObjectId" value="${ACHComp.CompanyID}"/>
			<ffi:setProperty name="GetCompressedLimits" property="GroupId" value="${User.EntitlementGroupId}"/>
			<ffi:setProperty name="GetCompressedLimits" property="OperationName" value="${EntType.Value}"/>
			<ffi:setProperty name="GetCompressedLimits" property="MemberId" value="${User.Id}"/>
			<ffi:setProperty name="GetCompressedLimits" property="MemberType" value="${User.MemberType}"/>
			<ffi:setProperty name="GetCompressedLimits" property="MemberSubType" value="${User.MemberSubType}"/>
			<ffi:process name="GetCompressedLimits"/>

			<ffi:setProperty name="Entitlement_Limits" property="SortedBy" value="Period"/>
			<ffi:list collection="Entitlement_Limits" items="Limit">
			    <ffi:cinclude value1="${Limit.Period}" value2="1" operator="equals">
				<ffi:setProperty name="Debit1Data" value="${Limit.Data}"/>
			    </ffi:cinclude>
			    <ffi:cinclude value1="${Limit.Period}" value2="2" operator="equals">
				<ffi:setProperty name="Debit2Data" value="${Limit.Data}"/>
			    </ffi:cinclude>
			</ffi:list>
		    </ffi:cinclude>

		    <ffi:cinclude value1="${DisplayedHeader}" value2="false" operator="equals">
	    <tr>
		<td colspan="7" style="<ffi:getProperty name='LimitsColumnHeadingStyle'/>" class="adminBackground">
		    <span class="sectionhead"><s:text name="jsp.common_117"/></span>
		</td>
	    </tr>
	    <tr>
		<td class="adminBackground"></td>
		<td class="adminBackground"></td>
		<td class="adminBackground"></td>
		<td colspan="4" class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="240" border="0"></td>
	    </tr>
	    <tr>
		<td class="adminBackground"></td>
		<td class="adminBackground"></td>
		<td class="adminBackground"></td>
		<td class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="60" border="0"></td>
		<td class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="60" border="0"></td>
		<td class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="60" border="0"></td>
		<td class="adminBackground"><img src="/cb/web/multilang/grafx/spacer.gif" height="0" width="60" border="0"></td>
	    </tr>
	    <tr>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_119"/></span>
		</td>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.default_106"/></span>
		</td>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.default_233"/></span>
		</td>
		<td colspan="4" style="<ffi:getProperty name='LimitsColumnHeadingStyle'/>" class="tbrd_b adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_96"/></span>
		</td>
	    </tr>
	    <tr>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		</td>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		</td>
		<td style="<ffi:getProperty name='ColumnHeadingStyle'/>" class="adminBackground">
		</td>
		<td style="<ffi:getProperty name='PeriodHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_113"/></span>
		</td>
		<td style="<ffi:getProperty name='PeriodHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_61"/></span>
		</td>
		<td style="<ffi:getProperty name='PeriodHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_114"/></span>
		</td>
		<td style="<ffi:getProperty name='PeriodHeadingStyle'/>" class="adminBackground">
		    <span class="sectionsubhead"><s:text name="jsp.common_62"/></span>
		</td>
	    </tr>
			<ffi:setProperty name="DisplayedHeader" value="true"/>
		    </ffi:cinclude>
		    <ffi:cinclude value1="${ModuleDisplayValue}" value2="" operator="notEquals">
		    </ffi:cinclude>
	    <tr>
		<td style="<ffi:getProperty name='NonLimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="ModuleDisplayValue"/></span>
		</td>
		<td style="<ffi:getProperty name='NonLimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="ACHComp" property="CompanyName"/></span>
		</td>
		<td style="<ffi:getProperty name='NonLimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="ACHComp" property="CompanyID"/></span>
		</td>
		<td style="<ffi:getProperty name='LimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="Credit1Data"/></span>
		</td>
		<td style="<ffi:getProperty name='LimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="Credit2Data"/></span>
		</td>
		<td style="<ffi:getProperty name='LimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="Debit1Data"/></span>
		</td>
		<td style="<ffi:getProperty name='LimitsDataStyle'/>" class="<%= backgroundColour %>">
		    <span class="columndata"><ffi:getProperty name="Debit2Data"/></span>
		</td>
	    </tr>
		    <ffi:setProperty name="ModuleDisplayValue" value=""/>
		</ffi:cinclude>
	    </ffi:list>
	</ffi:cinclude>
    </ffi:list>