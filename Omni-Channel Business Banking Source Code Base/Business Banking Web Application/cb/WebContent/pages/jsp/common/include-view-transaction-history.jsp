<%@ page import="com.ffusion.beans.user.User,
				 com.ffusion.beans.user.UserLocale,
				 com.ffusion.util.UserLocaleConsts,com.ffusion.efs.adapters.profile.exception.ProfileException"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- ---- TRANSACTION HISTORY ---- --%>

<ffi:cinclude value1="${isTemplate}" value2="true">
             <span id="templateHis" class="toggleClick"><strong><!--L10NStart-->Template History </strong><span class="sapUiIconCls icon-navigation-down-arrow"></span><!--L10NEnd--></span>
         </ffi:cinclude>
            	<ffi:cinclude value1="${isTemplate}" value2="true" operator="notEquals">
              <span id="transactionHis" class="toggleClick"><strong><!--L10NStart-->Transaction History </strong><span class="sapUiIconCls icon-navigation-down-arrow"></span><!--L10NEnd--></span>
        </ffi:cinclude>	

<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableAlerternateRowColor tdWithPadding transactionHistoryTable marginTop10">
            <tr class="resetTableRowBGColor">
                <td class="sectionsubhead" nowrap><s:text name="jsp.default_142"/></td>
                <td class="sectionsubhead" nowrap><s:text name="jsp.default_438"/></td>
                <td class="sectionsubhead" nowrap><s:text name="jsp.default_333"/></td>
                <td class="sectionsubhead" nowrap><s:text name="jsp.default_170"/></td>
            </tr>
           <%--  <%
            	String df = ((UserLocale)session.getAttribute(UserLocaleConsts.USERLOCALE)).getDateFormat();
            	String tf = df + " HH:mm:ss";
            %>
            <ffi:list collection="TransactionHistory" items="historyItem">
            <ffi:setProperty name="historyItem" property="DateFormat" value="<%=tf%>"/>
            <tr>
                <td class="columndata"><ffi:getProperty name="historyItem" property="TranDate"/></td>
                <td class="columndata"><ffi:getProperty name="historyItem" property="State"/></td>
                <td class="columndata"><ffi:getProperty name="historyItem" property="ProcessedBy"/></td>
                <td class="columndata"><div class="description"><ffi:getProperty name="historyItem" property="Message"/></div></td>
            </tr>
            </ffi:list> --%>
            <s:iterator value="TransactionHistory" var="historyItem">
            	<tr>
                <td class="columndata"><ffi:getProperty name="historyItem" property="TranDate"/></td>
                <td class="columndata"><ffi:getProperty name="historyItem" property="State"/></td>
                <td class="columndata"><ffi:getProperty name="historyItem" property="ProcessedBy"/></td>
                <td class="columndata"><div class="description"><ffi:getProperty name="historyItem" property="Message"/></div></td> 
            </tr>
            </s:iterator>
        </table>
<ffi:removeProperty name="stateDisplay"/>
<input id="isCalendar" value="<s:property value="%{isCalendar}"/>" type="hidden">
<script type="text/javascript">
	$(document).ready(function(){
		var isCalendar = $('#isCalendar').val();
	 if(isCalendar!="undefined" && isCalendar!="true"){
	 
	 <ffi:cinclude value1="${isTemplate}" value2="true">
	 $("#templateHis").next().slideDown();
		$("#templateHis").find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
		$("#templateHis").removeClass("expandArrow").addClass("collapseArrow");
	 </ffi:cinclude>
      <ffi:cinclude value1="${isTemplate}" value2="true" operator="notEquals">
		
		  $("#transactionHis").next().slideDown();
		$("#transactionHis").find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
		$("#transactionHis").removeClass("expandArrow").addClass("collapseArrow");
	  </ffi:cinclude>
		// $( ".transactionHistoryTable").hide();
	 }else if(isCalendar!="undefined" && isCalendar=="true"){
		 $('.toggleClick').remove();
	 }
	});
</script>