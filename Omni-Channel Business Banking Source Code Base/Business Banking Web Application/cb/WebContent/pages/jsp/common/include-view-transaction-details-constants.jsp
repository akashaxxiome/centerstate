<%@ page import="com.ffusion.beans.wiretransfers.WireDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- set the values for BC/CB --%>

<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
	<ffi:cinclude value1="${APPROVALS_CB_PAYMENTS}" value2="true" operator="equals">
    <%-- constants for gifs and cascade styles used on pages --%>	
    <ffi:setProperty name="payment_details_background_color" value="ltrow2_color"/>
    <ffi:setProperty name="transfer_details_background_color" value="ltrow2_color"/>
    <ffi:setProperty name="ach_details_background_color" value="lightBackground"/>
    <ffi:setProperty name="tax_details_background_color" value="lightBackground"/>
    <ffi:setProperty name="wire_details_background_color" value="ltrow2_color"/>
    <ffi:setProperty name="wirebatch_details_background_color" value="ltrow2_color"/>
    <ffi:setProperty name="cashcon_details_background_color" value="ltrow2_color"/>
  </ffi:cinclude>
  <ffi:cinclude value1="${APPROVALS_CB_PAYMENTS}" value2="true" operator="notEquals">
    <%-- constants for gifs and cascade styles used on pages --%>	
    <ffi:setProperty name="payment_details_background_color" value="ltrow4_color"/>
    <ffi:setProperty name="transfer_details_background_color" value="ltrow4_color"/>
    <ffi:setProperty name="ach_details_background_color" value="ltrow4_color"/>
    <ffi:setProperty name="tax_details_background_color" value="ltrow4_color"/>
    <ffi:setProperty name="wire_details_background_color" value="ltrow4_color"/>
    <ffi:setProperty name="wirebatch_details_background_color" value="ltrow4_color"/>
    <ffi:setProperty name="cashcon_details_background_color" value="ltrow4_color"/>
  </ffi:cinclude>
    <%-- constants for gifs used on pages --%>	
    <ffi:setProperty name="sort_asc_gif" value="/cb/web/multilang/grafx/common/i_sort_asc.gif"/>
    <ffi:setProperty name="sort_desc_gif" value="/cb/web/multilang/grafx/common/i_sort_desc.gif"/>
    <ffi:setProperty name="sort_off_gif" value="/cb/web/multilang/grafx/common/i_sort_off.gif"/>
    <ffi:setProperty name="view_details_gif" value="/cb/web/multilang/grafx/common/i_view.gif"/>
    <ffi:setProperty name="spacer_gif" value="/cb/web/multilang/grafx/common/spacer.gif"/>
    <ffi:setProperty name="calendar_gif" value="/cb/web/multilang/grafx/common/i_calendar.gif"/>
    <ffi:setProperty name="edit_gif" value="/cb/web/multilang/grafx/common/i_edit.gif"/>
    <ffi:setProperty name="delete_gif" value="/cb/web/multilang/grafx/common/i_trash.gif"/>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
    <ffi:setProperty name="payment_details_background_color" value="dkback"/>
    <ffi:setProperty name="transfer_details_background_color" value="dkback"/>
    <ffi:setProperty name="ach_details_background_color" value="dkback"/>
    <ffi:setProperty name="tax_details_background_color" value="dkback"/>
    <ffi:setProperty name="wire_details_background_color" value="dkback"/>
    <ffi:setProperty name="wirebatch_details_background_color" value="dkback"/>
    <ffi:setProperty name="cashcon_details_background_color" value="dkback"/>
    
    <%-- constants for gifs used on pages --%>	
    <ffi:setProperty name="sort_asc_gif" value="/bc/bc/multilang/grafx/common/i_sort_asc.gif"/>
    <ffi:setProperty name="sort_desc_gif" value="/bc/bc/multilang/grafx/common/i_sort_desc.gif"/>
    <ffi:setProperty name="sort_off_gif" value="/bc/bc/multilang/grafx/common/i_sort_off.gif"/>
    <ffi:setProperty name="view_details_gif" value="/bc/bc/multilang/grafx/common/i_view.gif"/>
    <ffi:setProperty name="spacer_gif" value="/bc/bc/multilang/grafx/common/spacer.gif"/>
    <ffi:setProperty name="calendar_gif" value="/bc/bc/multilang/grafx/common/i_calendar.gif"/>
    <ffi:setProperty name="edit_gif" value="/bc/bc/multilang/grafx/common/i_edit.gif"/>
    <ffi:setProperty name="delete_gif" value="/bc/bc/multilang/grafx/common/i_trash.gif"/>
</ffi:cinclude>

<ffi:cinclude value1="${WirePayeeScopes}" value2="" operator="equals">
	<ffi:object id="WirePayeeScopes" name="com.ffusion.beans.util.StringTable" scope="session" />
	<ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_BUSINESS %>"/>
	<ffi:setProperty name="WirePayeeScopes" property="Value" value="Business"/>
	<ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_USER %>"/>
	<ffi:setProperty name="WirePayeeScopes" property="Value" value="User"/>
	<ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_UNMANAGED %>"/>
	<ffi:setProperty name="WirePayeeScopes" property="Value" value="One-Off"/>
</ffi:cinclude>
