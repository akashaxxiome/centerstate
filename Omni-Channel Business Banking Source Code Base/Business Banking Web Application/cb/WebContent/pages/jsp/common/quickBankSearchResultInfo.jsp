<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<script>
	function quickReSearchBankList(){
		$('#quickSearchBankDetails').hide();
		$('#BankFormID').show();
	}
</script>
		<div align="center">
			<table width="98%" border="0" cellspacing="0" cellpadding="0" id="quickSearchBankDetails">
				<tr>
					<td class="columndata ltrow2_color" align="center">
							<tr>
								<td>
									<s:form id="billpaybanklistitemFormId" name="billpaybanklistitem.jsp" method="post">
										<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
										<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td colspan="3" align="center" >
												<span class="headerTitle">
													<s:property value="%{financialInstitution.InstitutionName}"/>
												</span>
												<input type="hidden" name="tmp_url" value="billbanklistitem"/>
											</td>
										</tr>
										 <tr>
									    	<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_31"/></p></td>
									    </tr>
									    <tr>
									    	<td colspan="3">
									    		<table width="100%" border="0" cellspacing="0" cellpadding="5">
										    		<tr>
												    	<td width="40%" class="labelCls"><s:text name="jsp.default_599"/></td>
												    	<td width="20%" class="labelCls"><s:text name="jsp.default_101"/></td>
												    	<td width="20%" class="labelCls"><s:text name="jsp.default_386"/></td>
												    	<td width="20%" class="labelCls"><s:text name="jsp.default_115"/></td>
												    </tr>
											    </table>	
									    	</td>
									    </tr>
									    <tr>
									    	<td colspan="3">
									    		<table width="100%" border="0" cellspacing="0" cellpadding="5">
										    		<tr>
											    		<td width="40%">
															<span class="columndata valueCls">
																<s:if test="%{financialInstitution.Street!=''}"><s:property value="%{financialInstitution.Street}"/>,</s:if>
															</span>
															<span class="columndata valueCls">
																<s:if test="%{financialInstitution.ZipCode!=''}"><s:property value="%{financialInstitution.ZipCode}"/></s:if>
															</span>
														</td>
												    	<td width="20%" valign="top">
															<span class="columndata valueCls"><s:property value="%{financialInstitution.City}"/>
																<s:if test="%{financialInstitution.StateCode!=''}">,&nbsp;
																	<s:property value="%{financialInstitution.StateCode}"/>
																</s:if>
															</span>
														</td>
												    	<td width="20%" valign="top">
															<span class="columndata valueCls">
																<s:property value="%{financialInstitution.City}"/>
																	<s:if test="%{financialInstitution.StateCode!=''}">,&nbsp;
																		<s:property value="%{financialInstitution.StateCode}"/>
																	</s:if>
															</span>
														</td>
												    	<td width="20%" valign="top">
															<span class="columndata">
																<s:property value="%{financialInstitution.Country}"/>
															</span>
														</td>
													</tr>
											    </table>	
									    	</td>	
									    </tr>
									    <tr>
									    	<td colspan="4" height="20">&nbsp;</td>
									    </tr>
									    <tr>
									    	<td colspan="4"><p class="transactionHeading"><s:text name="admin.banklookup.identification.codes"/></p></td>
									    </tr>
									    <tr>
									    	<td colspan="4">
										    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
											    	<tr>
												    	<td width="20%"><s:text name="jsp.default_201"/></td>
												    	<td width="20%"><s:text name="jsp.default_99"/></td>
												    	<td width="20%"><s:text name="jsp.default_289"/></td>
												    	<td width="20%"><s:text name="jsp.default_22"/></td>
												    	<td width="20%"><s:text name="jsp.default_401"/></td>
												    </tr>
											    </table>
										    </td>	
									    </tr>
									    <tr>
									    	<td colspan="4">
										    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
											    	<tr>
												    	<td width="20%" valign="top">
													    	<s:if test="%{financialInstitution.wireRoutingNumber!=null}">
															<span class="columndata">
																<s:property value="%{financialInstitution.wireRoutingNumber}"/>
															</span>
														</s:if>
												    	</td>
												    	<td width="20%" valign="top">
												    		<s:if test="%{financialInstitution.chipsUID!=null}">
																<span class="columndata">
																	<s:property value="%{financialInstitution.chipsUID}"/>
																</span>
															</s:if>	
												    	</td>
												    	<td width="20%" valign="top">
															<s:if test="%{financialInstitution.nationalID!=null}">
																<span class="columndata">
																	<s:property value="%{financialInstitution.nationalID}"/>
																</span>
															</s:if>
														</td>
														<td width="20%" valign="top">
															<s:if test="%{financialInstitution.achRoutingNumber!=null}">
																<span class="columndata">
																	<s:property value="%{financialInstitution.achRoutingNumber}"/>
																</span>
															</s:if>
														</td>
														<td width="20%" valign="top">
															<s:if test="%{financialInstitution.swiftBIC!=null}">
																<span class="columndata">
																	<s:property value="%{financialInstitution.swiftBIC}"/>
																</span>
															</s:if>
														</td>
												    </tr>
											    </table>
										    </td>	
									    </tr>
										<%-- <tr>
											<td>
											<table align="right" cellpadding="0" cellspacing="0" border="0">
												<s:if test="%{financialInstitution.Street!=''}">
													<tr>
													<td class="columndata" width="49%">
														<s:property value="%{financialInstitution.Street}"/>,
													</td>
													<td class="columndata" align="right" width="49%">&nbsp;</td>
												</tr>
												</s:if>
												<tr>
													<td class="columndata">
													<s:property value="%{financialInstitution.City}"/>
													<s:if test="%{financialInstitution.StateCode!=''}">,&nbsp;
														<s:property value="%{financialInstitution.StateCode}"/>
													</s:if>
													</td>
												</tr>
												<tr>
													<td class="columndata">
													<s:property value="%{financialInstitution.SwiftBIC}"/>
													<td>
												</tr>
												<s:if test="%{financialInstitution.ZipCode!=''}">
												<tr>
													<td class="columndata">
													<s:property value="%{financialInstitution.ZipCode}"/>
													</td>
													<td align="right" class="columndata">FED ABA: 
													<s:property value="%{financialInstitution.AchRoutingNumber}"/>
													</td>
												</tr>
												</s:if>
												<tr>
													<td class="columndata">
													<s:property value="%{financialInstitution.Country}"/>
													</td>
													<td align="right" class="columndata">&nbsp;</td>
												</tr>
											</table>
											</td>
										
										<td>
											<table align="right" cellpadding="0" cellspacing="0" border="0">
											
												<tr>
													<td class="columndata" align="right"><!--L10NStart-->ACH Routing No.:<!--L10NEnd--></td>
													<s:if test="%{financialInstitution.achRoutingNumber!=null}">
														<td class="columndata">
															<s:property value="%{financialInstitution.achRoutingNumber}"/>
														</td>
													</s:if>
												</tr>
												<tr>
													<td class="columndata" align="right"><!--L10NStart-->Wire Routing No.:<!--L10NEnd--></td>
													<s:if test="%{financialInstitution.wireRoutingNumber!=null}">
														<td class="columndata">
															<s:property value="%{financialInstitution.wireRoutingNumber}"/>
														</td>
													</s:if>
												</tr>
											
												<tr>
													<td class="columndata" align="right"> Chips: </td>
													<s:if test="%{financialInstitution.chipsUID!=null}">
														<td class="columndata">
															<ffi:getProperty name="financialInstitution" property="chipsUID"/>
															<s:property value="%{financialInstitution.chipsUID}"/>
														</td>
													</s:if>
												</tr>
											
											
												<tr>
													<td class="columndata" align="right"> National: </td>
													<s:if test="%{financialInstitution.nationalID!=null}">
														<td class="columndata">
															<ffi:getProperty name="financialInstitution" property="nationalID"/>
															<s:property value="%{financialInstitution.nationalID}"/>
														</td>
													</s:if>
												</tr>
											
											</table>
										</td>
										</tr> --%>
											<tr>
												<td colspan="3"><br></td>
											</tr>
											<tr>	
											<td colspan="3" align="center" class="ui-widget-header customDialogFooter">
													
											<sj:a
												id="billpay_researchBankListbuttonId"
												button="true"
												onclick="quickReSearchBankList()"
											><s:text name="jsp.default_374"/></sj:a>
																				
											<sj:a id="billpay_closeSearchBankbuttonId"
												button="true"
												onclick="closeQuickBankSearch()"
											><s:text name="jsp.default_102"/></sj:a>
											
											</td>
											</tr>
											
										</table>
				  					  &nbsp;
									</s:form>
								</td>
							</tr>
							
					</td>
				</tr>
			</table>
		</div>
