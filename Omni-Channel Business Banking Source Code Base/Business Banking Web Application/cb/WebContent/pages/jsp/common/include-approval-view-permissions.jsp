<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.HashSet" %>

<ffi:setProperty name="ColumnHeadingStyle" value="text-align: left; height: 16px; vertical-align: middle;"/>
<ffi:setProperty name="LimitsColumnHeadingStyle" value="text-align: center; height: 16px; vertical-align: middle;"/>
<ffi:setProperty name="LimitsDataStyle" value="text-align: right"/>
<ffi:setProperty name="NonLimitsDataStyle" value="text-align: left"/>
<ffi:setProperty name="PeriodHeadingStyle" value="text-align: right; height: 16px; vertical-align: middle;"/>
<ffi:setProperty name="ErrorMsgStyle" value="width: 750px; line-height: 11px; text-align: center;"/>
<ffi:setProperty name="PermissionsHeaderStyle" value="${approval_view_permissions_table_header_style}"/>
<ffi:setProperty name="PermissionsFooterStyle" value="${approval_view_permissions_table_footer_style}"/>

<% String opType = request.getParameter("OpType"); 
	session.setAttribute("OpType", opType);
%>

<%
if (com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BOOK.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BOOK_FREEFORM.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BOOK_TEMPLATED.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DOMESTIC.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DOMESTIC_FREEFORM.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DOMESTIC_TEMPLATED.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DRAWDOWN.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DRAWDOWN_FREEFORM.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_DRAWDOWN_TEMPLATED.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_FED.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_FED_FREEFORM.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_FED_TEMPLATED.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_HOST.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_INTERNATIONAL.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_INTERNATIONAL_FREEFORM.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_INTERNATIONAL_TEMPLATED.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE.equals(opType)
	|| com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE.equals(opType))
{
%>
    <ffi:setProperty name="UserIncludePage" value="${PagesPath}common/include-approval-view-permissions-user-wire.jsp"/>
    <ffi:setProperty name="Mode" value="Wires"/>
<%
} else {
%>
    <ffi:setProperty name="UserIncludePage" value="${PagesPath}common/include-approval-view-permissions-user-ach.jsp"/>
    <ffi:setProperty name="Mode" value="ACH"/>
<%
}
%>

<ffi:removeProperty name="GetEntitlementTypesWithProperties"/>
<ffi:object name="com.ffusion.efs.tasks.entitlements.GetEntitlementTypesWithProperties"
	id="GetEntitlementTypesWithProperties"
	scope="session"/>
<ffi:setProperty name="GetEntitlementTypesWithProperties"
	property="SearchCritName"
	value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_APPROVAL_ACTIVITY %>"/>
<ffi:setProperty name="GetEntitlementTypesWithProperties" property="SearchCritValue" value="${OpType}"/>
<ffi:process name="GetEntitlementTypesWithProperties"/>

<ffi:removeProperty name="GetLimitTypesWithProperties"/>
<ffi:object name="com.ffusion.efs.tasks.entitlements.GetLimitTypesWithProperties"
	id="GetLimitTypesWithProperties"
	scope="session"/>
<ffi:setProperty name="GetLimitTypesWithProperties"
	property="SearchCritName"
	value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_APPROVAL_ACTIVITY %>"/>
<ffi:setProperty name="GetLimitTypesWithProperties" property="SearchCritValue" value="${OpType}"/>
<ffi:process name="GetLimitTypesWithProperties"/>

<ffi:removeProperty name="MergeEntitlementsAndLimits"/>
<ffi:object name="com.ffusion.tasks.approvals.MergeEntitlementsAndLimits"
	id="MergeEntitlementsAndLimits"
	scope="session"/>
<ffi:setProperty name="MergeEntitlementsAndLimits" property="EntitlementsCollectionName" value="Entitlement_Types"/>
<ffi:setProperty name="MergeEntitlementsAndLimits" property="LimitsCollectionName" value="Limit_Types"/>
<ffi:cinclude value1="${Mode}" value2="ACH" operator="equals">
    <ffi:setProperty name="MergeEntitlementsAndLimits" property="MergeForACH" value="true"/>
</ffi:cinclude>
<ffi:process name="MergeEntitlementsAndLimits"/>

<ffi:removeProperty name="GetEntitlementObjectIDUtil"/>
<ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetEntitlementObjectIDUtil" scope="session"/>

<ffi:cinclude value1="${Mode}" value2="Wires" operator="equals">
    <ffi:setProperty name="Entitlement_Types"
	    property="SortedBy"
	    value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${Mode}" value2="ACH" operator="equals">
    <ffi:removeProperty name="SortACHApprovalsEntitlements"/>
    <ffi:object name="com.ffusion.tasks.approvals.SortACHApprovalsEntitlements"
	    id="SortACHApprovalsEntitlements"
	    scope="session"/>
    <ffi:setProperty name="SortACHApprovalsEntitlements"
	    property="EntitlementsCollectionName"
	    value="Entitlement_Types"/>
    <ffi:process name="SortACHApprovalsEntitlements"/>
</ffi:cinclude>

<ffi:setProperty name="DisplayedData" value="false"/>
	<div id="ProcessingMsg" align="center" style="display: block;">
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
	    <tr>
		<td style="text-align: left; vertical-align: middle; line-height: 16px;" class="pagehead">
		    <ffi:getProperty name="OpType"/> <s:text name="jsp.common_24"/>
		</td>
	    </tr>
	    <tr>
		<td class="<ffi:getProperty name='approval_view_permissions_table_dark_row'/>">&nbsp;</td>
	    </tr>
	    <tr>
		<td style="text-align: left; vertical-align: middle; line-height: 16px;" class="sectionhead">
		    <s:text name="jsp.common_134"/>
		</td>
	    </tr>
	</table>
	</div>
	<div id="ApprovalPermissionsTable" align="center" style="display: none;">
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
	    <tr>
		<td colspan="7" style="text-align: left; vertical-align: middle; line-height: 16px;" class="pagehead">
		    <ffi:getProperty name="OpType"/> <s:text name="jsp.common_24"/>
		</td>
	    </tr>
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_dark_row'/>">&nbsp;</td>
	    </tr>
<%
java.util.ArrayList usersCollection = (java.util.ArrayList) session.getAttribute("ApprovalsUsersCollection");
if (usersCollection.size() > 0) {
%>
	    <tr>
		<td colspan="7">
			<div align="center" class="ui-widget ui-widget-content ui-corner-all">
			<div class="ui-widget-header ui-corner-all">
				<span><s:text name="jsp.common_121"/></span>
			</div>
		    <ffi:setProperty name="UsersCollectionName" value="ApprovalsUsersCollection"/>
		    <table><s:include value="%{#session.UserIncludePage}"/></table>
		    </div>

		</td>
	    </tr>
<% } %>

<%-- --- Display Group Data ---------------------------------------------- --%>
<%-- ffi:list is not used here because the resulting output is "garbled". --%>
<%
java.util.ArrayList groupsCollection = (java.util.ArrayList) session.getAttribute("ApprovalsGroupsCollection");
for (int i = 0; i < groupsCollection.size(); i++) {
    session.setAttribute("CurrGroup", (com.ffusion.csil.beans.entitlements.EntitlementGroup) groupsCollection.get(i));
%>
    <ffi:removeProperty name="GetMembers"/>

    <ffi:cinclude value1="${CurrGroup.GROUP_CASCADING_MODE}" value2="Group_cascading">
        <ffi:object name="com.ffusion.efs.tasks.entitlements.GetDescendantMembers" id="GetMembers" scope="session"/>
        <ffi:setProperty name="GetMembers" property="EntitlementGroupId" value="${CurrGroup.GroupId}"/>
        <ffi:process name="GetMembers"/>
    </ffi:cinclude>

    <ffi:cinclude value1="${CurrGroup.GROUP_CASCADING_MODE}" value2="Group_cascading" operator="notEquals">
        <ffi:object name="com.ffusion.efs.tasks.entitlements.GetMembers" id="GetMembers" scope="session"/>
        <ffi:setProperty name="GetMembers" property="EntitlementGroupId" value="${CurrGroup.GroupId}"/>
        <ffi:process name="GetMembers"/>
    </ffi:cinclude>

    <ffi:cinclude value1="${DisplayedData}" value2="true" operator="equals">
	    <tr>
		<td colspan="7">
		    <span class="sectionhead">&nbsp;</span>
		</td>
	    </tr>
	    <tr>
		<td colspan="7">
		    <span class="sectionhead">&nbsp;</span>
		</td>
	    </tr>
    </ffi:cinclude>
	    <tr>
		<td colspan="7">

			<div align="center" class="ui-widget ui-widget-content ui-corner-all">
			<div class="ui-widget-header ui-corner-all">
				<div><s:text name="jsp.common_120"/> <ffi:getProperty name="CurrGroup" property="GroupName"/>
		            <ffi:cinclude value1="${CurrGroup.GROUP_CASCADING_MODE}" value2="Group_cascading">
		                &nbsp;<s:text name="jsp.common_5"/>
		            </ffi:cinclude>
            	</div>
			</div>

		    <ffi:setProperty name="DisplayedUserData" value="false"/>
    		<ffi:setProperty name="UsersCollectionName" value="Entitlement_Group_Members"/>
    		<table><s:include value="%{#session.UserIncludePage}"/></table>
			</div>
		</td>
	    </tr>


    <ffi:cinclude value1="${DisplayedUserData}" value2="false" operator="equals">
	    <tr>
		<td colspan="7" style="text-align: center; height: 20px;" class="tbrd_full">
		    <span class="sectionhead">&nbsp;</span>
		</td>
	    </tr>
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_light_row'/>">&nbsp;</td>
	    </tr>
	    <tr>
		<td colspan="7" style="<ffi:getProperty name='ErrorMsgStyle'/>" class="<ffi:getProperty name='approval_view_permissions_table_light_row'/>">
		    <span class="sectionsubhead"><ffi:getProperty name="CurrGroup" property="GroupName"/> <s:text name="jsp.common_69"/></span>
		</td>
	    </tr>
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_light_row'/>">&nbsp;</td>
	    </tr>
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_dark_row'/>">&nbsp;</td>
	    </tr>
	<ffi:setProperty name="DisplayedData" value="true"/>
    </ffi:cinclude>

<% } %>

<%-- --- Display Approval Group Data ------------------------------------- --%>
<%-- ffi:list is not used here because the resulting output is "garbled". --%>
<%
java.util.ArrayList appGroupsCollection = (java.util.ArrayList) session.getAttribute("ApprovalsApprovalsGroupsCollection");
for (int i = 0; i < appGroupsCollection.size(); i++) {
    session.setAttribute("CurrGroup", (com.ffusion.beans.approvals.ApprovalsGroup) appGroupsCollection.get(i));
%>
    <ffi:removeProperty name="GetApprovalGroupMembers"/>
    <ffi:object name="com.ffusion.tasks.approvals.GetApprovalGroupMembers" id="GetApprovalGroupMembers" scope="session"/>
    <ffi:setProperty name="GetApprovalGroupMembers" property="ApprovalsGroupSessionName" value="CurrGroup"/>
    <ffi:setProperty name="GetApprovalGroupMembers" property="ApprovalsGroupMembersSessionName" value="ApprovalsGroupMembers"/>
    <ffi:process name="GetApprovalGroupMembers"/>

    <ffi:cinclude value1="${ApprovalsGroupMembers}" value2="" operator="notEquals">
	<ffi:removeProperty name="ConvertApprovalsGroupMembersToEntitlementGroupMembers"/>
	<ffi:object name="com.ffusion.tasks.approvals.ConvertApprovalsGroupMembersToEntitlementGroupMembers"
		id="ConvertApprovalsGroupMembersToEntitlementGroupMembers"
		scope="session"/>
	<ffi:setProperty name="ConvertApprovalsGroupMembersToEntitlementGroupMembers"
		property="ApprovalsGroupMembersName"
		value="ApprovalsGroupMembers"/>
	<ffi:process name="ConvertApprovalsGroupMembersToEntitlementGroupMembers"/>

	<ffi:cinclude value1="${DisplayedData}" value2="true" operator="equals">
	    <tr>
		<td colspan="7">
		    <span class="sectionhead">&nbsp;</span>
		</td>
	    </tr>
	    <tr>
		<td colspan="7">
		    <span class="sectionhead">&nbsp;</span>
		</td>
	    </tr>
	</ffi:cinclude>
	    <tr>
		<td colspan="7">
			<div align="center" class="ui-widget ui-widget-content ui-corner-all">
			<div class="ui-widget-header ui-corner-all">
				<span><s:text name="jsp.common_120"/> <ffi:getProperty name="CurrGroup" property="GroupName"/></span>
			</div>
			<ffi:setProperty name="UsersCollectionName" value="Entitlement_Group_Members"/>
			<table><s:include value="%{#session.UserIncludePage}"/></table>
			</div>
		</td>
	    </tr>

    </ffi:cinclude>
    <ffi:cinclude value1="${ApprovalsGroupMembers}" value2="" operator="equals">
	    <tr>
		<td colspan="7">
		    <span class="sectionhead">&nbsp;</span>
		</td>
	    </tr>
	    <tr>
		<td colspan="7">
		    <span class="sectionhead">&nbsp;</span>
		</td>
	    </tr>
	    <tr>
		<td colspan="7">
			<div align="center" class="ui-widget ui-widget-content ui-corner-all">
			<div class="ui-widget-header ui-corner-all">
				<span><s:text name="jsp.common_120"/> <ffi:getProperty name="CurrGroup" property="GroupName"/></span>
			</div>
			</div>
		</td>
	    </tr>
	    <tr>
		<td colspan="7" style="text-align: center; height: 20px;" class="tbrd_full">
		    <span class="sectionhead">&nbsp;</span>
		</td>
	    </tr>
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_light_row'/>">&nbsp;</td>
	    </tr>
	    <tr>
		<td colspan="7" style="<ffi:getProperty name='ErrorMsgStyle'/>" class="<ffi:getProperty name='approval_view_permissions_table_light_row'/>">
		    <span class="sectionsubhead"><ffi:getProperty name="CurrGroup" property="GroupName"/> <s:text name="jsp.common_69"/></span>
		</td>
	    </tr>
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_light_row'/>">&nbsp;</td>
	    </tr>
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_dark_row'/>">&nbsp;</td>
	    </tr>
	<ffi:setProperty name="DisplayedData" value="true"/>
    </ffi:cinclude>

<% } %>
	</table>
	<br>
			<sj:a   button="true" 
					onClickTopics="closeDialog"
	        ><s:text name="jsp.default_83"/></sj:a>
	</div>
	<script type="text/javascript">
	<!--
	//====================================================================
	// Hide the "Processing ..." message and show the data.
	//====================================================================

	document.getElementById("ProcessingMsg").style.display="none";
	document.getElementById("ApprovalPermissionsTable").style.display="block";
	//-->
	</script>
    </body>
</html>

<ffi:removeProperty name="GetEntitlementTypesWithProperties"/>
<ffi:removeProperty name="GetLimitTypesWithProperties"/>
<ffi:removeProperty name="MergeEntitlementsAndLimits"/>
<ffi:removeProperty name="GetCompressedLimits"/>
<ffi:removeProperty name="GetAllWireTemplates"/>
<ffi:removeProperty name="CheckEntitlementByMember"/>
<ffi:removeProperty name="GetMembers"/>
<ffi:removeProperty name="GetBusinessEmployeesByEntGroupId"/>
<ffi:removeProperty name="ApprovalsUsersCollection"/>
<ffi:removeProperty name="ApprovalsGroupsCollection"/>
<ffi:removeProperty name="Entitlement_Group_Members"/>
<ffi:removeProperty name="ApprovalsApprovalsGroupsCollection"/>
<ffi:removeProperty name="ApprovalsGroupMembers"/>
<ffi:removeProperty name="Entitlement_Types"/>
<ffi:removeProperty name="ConvertApprovalsGroupMembersToEntitlementGroupMembers"/>
<ffi:removeProperty name="GetACHCompanies"/>
<%
    session.removeAttribute("ApprovalsGroupsCollection");
    session.removeAttribute("CurrGroup");
    session.removeAttribute("ApprovalsApprovalsGroupsCollection");
%>