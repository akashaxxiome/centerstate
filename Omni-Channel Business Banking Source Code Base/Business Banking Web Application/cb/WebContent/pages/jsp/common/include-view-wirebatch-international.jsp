<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<% 
String returnURL = null;
if( session.getAttribute( "wirebatch_details_post_jsp") == null ) {  
	returnURL = ( String )session.getAttribute( "SecurePath" ) + "payments/wiretransfers.jsp";
} else {
	returnURL = ( String )session.getAttribute( "wirebatch_details_post_jsp" );
}
%>
<ffi:setProperty name="WireBatch" property="DateFormat" value="${UserLocale.DateFormat}"/>
<div class="blockWrapper label120" role="form" aria-labelledby="templateInfoHeader">
	<div  class="blockHead"><!--L10NStart--><label for="templateInfoHeader">Template Info</label><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireBatchNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_66"/>:</span>
					<span id="viewWireBatchNameValue" class="columndata"><ffi:getProperty name="WireBatch" property="BatchName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewWireBatchApplicationTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_47"/>:</span>
					<span id="viewWireBatchApplicationTypeValue" class="columndata"><ffi:getProperty name="WireBatch" property="BatchType"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireBatchValueDateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_457"/>:</span>
					<span id="viewWireBatchValueDateValue" class="columndata"><ffi:getProperty name="WireBatch" property="DueDate"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewWireBatchSettelementDateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_380"/>:</span>
					<span id="viewWireBatchSettelementDateValue" class="columndata"><ffi:getProperty name="WireBatch" property="SettlementDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireBatchDebitCurrencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_155"/>:</span>
					<span id="viewWireBatchDebitCurrencyValue" class="columndata"><ffi:getProperty name="WireBatch" property="OrigCurrency"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewWireBatchPaymentCurrencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_319"/>:</span>
					<span id="viewWireBatchPaymentCurrencyValue" class="columndata"><ffi:getProperty name="WireBatch" property="PayeeCurrencyType"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireBatchExchangeRateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_192"/>:</span>
					<span id="viewWireBatchExchangeRateValue" class="columndata"><ffi:getProperty name="WireBatch" property="ExchangeRate"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewWireBatchMathRuleLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_278"/>:</span>
					<span id="viewWireBatchMathRuleValue" class="columndata"><ffi:getProperty name="WireBatch" property="MathRule"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span id="vireWireBatchContractNumberLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_113"/>:</span>
			<span id="vireWireBatchContractNumberValue" class="columndata"><ffi:getProperty name="WireBatch" property="ContractNumber"/></span>
		</div>
	</div>
</div>

<div class="blockWrapper marginTop20" role="form" aria-labelledby="templateInfoHeader">
	<div  class="blockHead"><h2 id="templateInfoHeader"><s:text name="jsp.default_239"/></h2></div>
</div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
	<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tr class="header">
	<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
		<td id="viewWireBatchHostIDLabel" align="center" class="sectionsubhead"><s:text name="jsp.default_230"/></td>
	</ffi:cinclude>
	<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
		<td id="viewWireBatchBeneficiaryLabel" align="center" class="sectionsubhead"><s:text name="jsp.default_68"/></td>
	</ffi:cinclude>
		<td id="viewWireBatchStatusLabel" align="left" class="sectionsubhead" align="left"><s:text name="jsp.default_388"/></td>
		<td id="viewWireBatchAmountLabel" class="sectionsubhead" align="left" colspan="2"><s:text name="jsp.default_43"/></td>
	</tr>
	 <% int idCount=0; %>
	<ffi:list collection="WireBatch.Wires" items="wire">
	<tr>
	<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
		<td id="viewWireBatchHostIDValue<%=idCount %>" align="center" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
	</ffi:cinclude>
	<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
		<td id="viewWireBatchBeneficiaryNameValue<%=idCount %>" align="center" class="columndata" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
			<ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
	</ffi:cinclude>
	
		<td id="viewWireBatchStatusValue<%=idCount %>" align="left" class="columndata" align="left" nowrap>
			<ffi:getProperty name="wire" property="StatusName"/>
		</td>
	
		<td id="viewWireBatchAmountValue<%=idCount++ %>" class="columndata" align="left" nowrap><ffi:getProperty name="wire" property="AmountForBPW"/>&nbsp;<s:text name="jsp.common_6"/></td>
		<td class="columndata" align="right" nowrap>
		<%-- <ffi:link url="wiretransferview.jsp?ID=${wire.ID}&trackingID=${wire.TrackingID}&collectionName=${collectionToShow}&wireInBatch=true"><img src='<ffi:getProperty name="view_details_gif"/>' alt="View Info" height="14" width="19" border="0" hspace="3"></ffi:link>--%>
		
		<a class='ui-button' title='View' href='#' onClick="ns.wire.viewWireTransferFromBatch('<ffi:urlEncrypt url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID=${wire.ID}&transType=SINGLE&trackingID=${wire.TrackingID}&collectionName=${collectionToShow}&wireInBatch=true"/>')">
        <span class='ui-icon ui-icon-info'></span></a>
		</td>
	</tr>
			<ffi:cinclude value1="${wire.Status}" value2="<%= String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_REJECTED) %>" operator="equals" >
				<tr>
					<td colspan=4>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
							<td width="25%" class="sectionsubhead" align="right" ><s:text name="jsp.default_349"/>:&nbsp;</td>
							<td align="left" width="32%" class="columndata">
							<ffi:getProperty name="wire" property="RejectReason"/>
							<ffi:cinclude value1="${wire.RejectReason}" value2="" operator="equals"><s:text name="jsp.common_103"/></ffi:cinclude>
							</td>
							<ffi:cinclude value1="${wire.ApproverName}" value2="" operator="notEquals" >
								<td width="18%" class="sectionsubhead" align="right"><s:text name="jsp.common_140"/>:&nbsp;</td>
								<td align="left" width="25%" class="columndata">
								<ffi:getProperty name="wire" property="ApproverName" />
								<ffi:cinclude value1="true" value2="${wire.ApproverIsGroup}" >(<s:text name="jsp.common_82"/>)</ffi:cinclude>
								</td>
							</ffi:cinclude>
							</tr>
						</table>
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude value1="${wire.Status}" value2="<%= String.valueOf(WireDefines.WIRE_STATUS_PENDING_APPROVAL) %>" operator="equals" >
				<ffi:cinclude value1="${wire.ApproverName}" value2="" operator="notEquals" >
					<tr>
						<td colspan=4>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
								<td width="33%" class="sectionsubhead" align="right"><s:text name="jsp.common_25"/>:&nbsp;</td>
								<td align="left" width="67%" class="columndata">
								<ffi:getProperty name="wire" property="ApproverName" />
								<ffi:cinclude value1="true" value2="${wire.ApproverIsGroup}" >(<s:text name="jsp.common_82"/>)</ffi:cinclude>
								</td>
								</tr>
							</table>
						</td>
					</tr>
				</ffi:cinclude>
                <ffi:cinclude value1="${wire.ApproverName}" value2="" operator="equals" >
                <tr>
                    <td align="left" class="sectionsubhead" nowrap colspan=4>
                        <div align="right">
                                <s:text name="jsp.common_149"/>&nbsp;
                        </div>
                    </td>
                </tr>
                </ffi:cinclude>
			</ffi:cinclude>
	</ffi:list>
	
	<tr>
		<td align="left" class="columndata">&nbsp;</td>
		<td id="viewWireBatchTotalAmountLabel" class="columndata" align="right" valign="bottom" nowrap><s:text name="jsp.default_431"/>:</td>
		<td id="viewWireBatchTotalAmountValue" class="tbrd_t sectionsubhead" width="1%" align="right" valign="bottom" nowrap><ffi:getProperty name="WireBatch" property="TotalDebit"/> <s:text name="jsp.common_6"/></td>
		<td align="left" class="columndata" nowrap>&nbsp;</td>
	</tr>
</table>
	</div>
</div>
<%-- <table width="98%" border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td align="left" class="tbrd_b sectionhead">&gt; <s:text name="jsp.default_65"/></td>
	</tr>
</table>
<br> --%>
<%-- <table width="715" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="357" valign="top">
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td id="viewWireBatchNameLabel" align="left" width="115" class="sectionsubhead"><s:text name="jsp.default_66"/></td>
					<td id="viewWireBatchNameValue" align="left" width="228" class="columndata"><ffi:getProperty name="WireBatch" property="BatchName"/></td>
				</tr>
				<tr>
					<td id="viewWireBatchApplicationTypeLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_47"/></td>
					<td id="viewWireBatchApplicationTypeValue" align="left" class="columndata"><ffi:getProperty name="WireBatch" property="BatchType"/></td>
				</tr>
				<tr>
					<td id="viewWireBatchValueDateLabel" lign="left" class="sectionsubhead"><s:text name="jsp.default_457"/></td>
					<td id="viewWireBatchValueDateValue" align="left" class="columndata"><ffi:getProperty name="WireBatch" property="DueDate"/></td>
				</tr>
				<tr>
					<td id="viewWireBatchSettelementDateLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_380"/></td>
					<td id="viewWireBatchSettelementDateValue" align="left" class="columndata"><ffi:getProperty name="WireBatch" property="SettlementDate"/></td>
				</tr>
			</table>
		</td>
		<td class="tbrd_l" width="10"><img src='<ffi:getProperty name="spacer_gif"/>' width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td id="viewWireBatchDebitCurrencyLabel" align="left" width="115" class="sectionsubhead"><s:text name="jsp.default_155"/></td>
					<td id="viewWireBatchDebitCurrencyValue" align="left" width="220" class="columndata"><ffi:getProperty name="WireBatch" property="OrigCurrency"/></td>
				</tr>
				<tr>
					<td id="viewWireBatchPaymentCurrencyLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_319"/></td>
					<td id="viewWireBatchPaymentCurrencyValue" align="left" class="columndata"><ffi:getProperty name="WireBatch" property="PayeeCurrencyType"/></td>
				</tr>
				<tr>
					<td id="viewWireBatchExchangeRateLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_192"/></td>
					<td id="viewWireBatchExchangeRateValue" align="left" class="columndata"><ffi:getProperty name="WireBatch" property="ExchangeRate"/></td>
				</tr>
				<tr>
					<td id="viewWireBatchMathRuleLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_278"/></td>
					<td id="viewWireBatchMathRuleValue" align="left" class="columndata"><ffi:getProperty name="WireBatch" property="MathRule"/></td>
				</tr>
				<tr>
					<td id="vireWireBatchContractNumberLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_113"/></td>
					<td id="vireWireBatchContractNumberValue" align="left" class="columndata"><ffi:getProperty name="WireBatch" property="ContractNumber"/></td>
				</tr>
			</table> 
		</td>
	</tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td id="viewWireBatchIncludeBatchesLabel" align="left" class="tbrd_b sectionhead">&gt; <s:text name="jsp.default_239"/></td>
	</tr>
</table>--%>
<%-- <table width="450" border="0" cellspacing="0" cellpadding="3">
	<tr>
	<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
		<td id="viewWireBatchHostIDLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_230"/></td>
	</ffi:cinclude>
	<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
		<td id="viewWireBatchBeneficiaryLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_68"/></td>
	</ffi:cinclude>
		<td id="viewWireBatchStatusLabel" align="left" class="sectionsubhead" align="left"><s:text name="jsp.default_388"/></td>
		<td id="viewWireBatchAmountLabel" class="sectionsubhead" align="right"><s:text name="jsp.default_43"/></td>
	</tr>
	 <% int idCount=0; %>
	<ffi:list collection="WireBatch.Wires" items="wire">
	<tr>
	<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
		<td id="viewWireBatchHostIDValue<%=idCount %>" align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
	</ffi:cinclude>
	<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
		<td id="viewWireBatchBeneficiaryNameValue<%=idCount %>" align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
			<ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
	</ffi:cinclude>
	
		<td id="viewWireBatchStatusValue<%=idCount %>" align="left" class="columndata" align="left" nowrap>
			<ffi:getProperty name="wire" property="StatusName"/>
		</td>
	
		<td id="viewWireBatchAmountValue<%=idCount++ %>" class="columndata" align="right" nowrap><ffi:getProperty name="wire" property="AmountForBPW"/>&nbsp;<s:text name="jsp.common_6"/></td>
		<td class="columndata" align="right" nowrap>
		<ffi:link url="wiretransferview.jsp?ID=${wire.ID}&trackingID=${wire.TrackingID}&collectionName=${collectionToShow}&wireInBatch=true"><img src='<ffi:getProperty name="view_details_gif"/>' alt="View Info" height="14" width="19" border="0" hspace="3"></ffi:link>
		
		<a class='ui-button' title='View' href='#' onClick="ns.wire.viewWireTransferFromBatch('<ffi:urlEncrypt url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID=${wire.ID}&transType=SINGLE&trackingID=${wire.TrackingID}&collectionName=${collectionToShow}&wireInBatch=true"/>')">
        <span class='ui-icon ui-icon-info'></span></a>
		</td>
	</tr>
			<ffi:cinclude value1="${wire.Status}" value2="<%= String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_REJECTED) %>" operator="equals" >
				<tr>
					<td colspan=4>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
							<td width="25%" class="sectionsubhead" align="right" ><s:text name="jsp.default_349"/>:&nbsp;</td>
							<td align="left" width="32%" class="columndata">
							<ffi:getProperty name="wire" property="RejectReason"/>
							<ffi:cinclude value1="${wire.RejectReason}" value2="" operator="equals"><s:text name="jsp.common_103"/></ffi:cinclude>
							</td>
							<ffi:cinclude value1="${wire.ApproverName}" value2="" operator="notEquals" >
								<td width="18%" class="sectionsubhead" align="right"><s:text name="jsp.common_140"/>:&nbsp;</td>
								<td align="left" width="25%" class="columndata">
								<ffi:getProperty name="wire" property="ApproverName" />
								<ffi:cinclude value1="true" value2="${wire.ApproverIsGroup}" >(<s:text name="jsp.common_82"/>)</ffi:cinclude>
								</td>
							</ffi:cinclude>
							</tr>
						</table>
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude value1="${wire.Status}" value2="<%= String.valueOf(WireDefines.WIRE_STATUS_PENDING_APPROVAL) %>" operator="equals" >
				<ffi:cinclude value1="${wire.ApproverName}" value2="" operator="notEquals" >
					<tr>
						<td colspan=4>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
								<td width="33%" class="sectionsubhead" align="right"><s:text name="jsp.common_25"/>:&nbsp;</td>
								<td align="left" width="67%" class="columndata">
								<ffi:getProperty name="wire" property="ApproverName" />
								<ffi:cinclude value1="true" value2="${wire.ApproverIsGroup}" >(<s:text name="jsp.common_82"/>)</ffi:cinclude>
								</td>
								</tr>
							</table>
						</td>
					</tr>
				</ffi:cinclude>
                <ffi:cinclude value1="${wire.ApproverName}" value2="" operator="equals" >
                <tr>
                    <td align="left" class="sectionsubhead" nowrap colspan=4>
                        <div align="right">
                                <s:text name="jsp.common_149"/>&nbsp;
                        </div>
                    </td>
                </tr>
                </ffi:cinclude>
			</ffi:cinclude>
	</ffi:list>
	
	<tr>
		<td align="left" class="columndata">&nbsp;</td>
		<td id="viewWireBatchTotalAmountLabel" class="columndata" align="right" valign="bottom" nowrap><s:text name="jsp.default_431"/>:</td>
		<td id="viewWireBatchTotalAmountValue" class="tbrd_t sectionsubhead" width="1%" align="right" valign="bottom" nowrap><ffi:getProperty name="WireBatch" property="TotalDebit"/> <s:text name="jsp.common_6"/></td>
		<td align="left" class="columndata" nowrap>&nbsp;</td>
	</tr>
</table> --%>
<form method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<%-- <input type="button" class="submitbutton" value="DONE" onclick='document.location="<%= returnURL %>"'>&nbsp;--%>
</form>