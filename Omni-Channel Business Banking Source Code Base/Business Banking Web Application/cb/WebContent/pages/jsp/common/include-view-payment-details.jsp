<%@page import="com.ffusion.beans.billpay.PaymentStatus"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/struts-jquery-tags" prefix="sj"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<% if( session.getAttribute( "PaymentView" ) != null ) { %>
	<ffi:object name="com.ffusion.tasks.billpay.GetAuditHistory" id="GetAuditHistory" scope="session"/>
		<ffi:setProperty name='GetAuditHistory' property='BeanSessionName' value='IncludeViewPayment'/>
		<ffi:setProperty name="GetAuditHistory" property="AuditLogSessionName" value="TransactionHistory" />
	<ffi:process name="GetAuditHistory"/>
<% } %>

<%
	com.ffusion.beans.billpay.Payment payment = (com.ffusion.beans.billpay.Payment) session.getAttribute("IncludeViewPayment");
	String accountID = payment != null ? payment.getAccount().getID() : "";
	String filter = "ID=" + accountID;
	String _strDisplayTxt = "";
	String _strNickName = "";
	String _strCurCode = "";
	String _fromAccount = "";
	String pmtCurrency = null;
	String acctCurrency = null;
	String accountDisplayText = "";
	boolean restricted = false;
%>

<ffi:getProperty name="IncludeViewPayment" property="Account.AccountDisplayText" assignTo="accountDisplayText" />

<%
	if(accountDisplayText != null) {
		_fromAccount = accountDisplayText; 
	}
	if(accountDisplayText == null || accountDisplayText.equalsIgnoreCase("Restricted")) {
		restricted = true;
	}
%>

<%-- for localizing state name --%>
<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${IncludeViewPayment.Payee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${IncludeViewPayment.Payee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>

<ffi:object id="GetDefaultCurrency" name="com.ffusion.tasks.billpay.GetDefaultCurrency" scope="session"/>
<ffi:process name="GetDefaultCurrency"/>
<ffi:cinclude value1="${IncludeViewPayment.Payee.PayeeRoute.CurrencyCode}" value2="" operator="equals">
<ffi:setProperty name="IncludeViewPayment" property="Payee.PayeeRoute.CurrencyCode" value="${DEFAULT_CURRENCY.CurrencyCode}"/>
</ffi:cinclude>

<ffi:setProperty name="IncludeViewPayment" property="AmountValue.CurrencyCode" value="${IncludeViewPayment.AmtCurrency}"/>

<div class="blockWrapper clearBoth label130">
	<div  class="blockHead  toggleClick expandArrow"><s:text name="jsp.billpay.payee_summary" />: <ffi:getProperty name="IncludeViewPayment" property="PayeeName"/> (<ffi:getProperty name="IncludeViewPayment" property="PayeeNickName"/> - <ffi:getProperty name="IncludeViewPayment" property="Payee.UserAccountNumber"/>) <span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
	
	<div class="toggleBlock hidden">
		<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_payeename"/>:</span>
						<span id="viewBillPay_payeeNameValue"><ffi:getProperty name="IncludeViewPayment" property="PayeeName"/></span>
					</div>
					<div class="inlineBlock">
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
						<span id="viewBillPay_payeeNameValue"><ffi:getProperty name="IncludeViewPayment" property="PayeeNickName"/></span>
					</div>
				</div>
				
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<%
									
						if ( pmtCurrency != null && acctCurrency != null) 
						
						{ %>
									
						<ffi:cinclude value1="${IncludeViewPayment.AmtCurrency}" value2="${_strAccountCurrencyCode}" operator="notEquals" >
					
						<ffi:object id="ConvertToTargetCurrency" name="com.ffusion.tasks.fx.ConvertToTargetCurrency" scope="session"/>
						<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmount" value="${IncludeViewPayment.AmountForBPW}" />
						<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmtCurrency" value="${IncludeViewPayment.AmtCurrency}" />
						<ffi:setProperty name="ConvertToTargetCurrency" property="TargetAmtCurrency" value="${_strAccountCurrencyCode}" />
						<ffi:process name="ConvertToTargetCurrency"/>
						<ffi:setProperty name="CONVERTED_CURRENCY_AMOUNT"  value="${ConvertToTargetCurrency.TargetAmount}"/>
					
					        &#8776; <ffi:getProperty name="CONVERTED_CURRENCY_AMOUNT"/>
					        <ffi:getProperty name="_strAccountCurrencyCode"/>
						</ffi:cinclude>
						
						<% } %>
						<ffi:removeProperty name="_strAccountCurrencyCode"/>
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_account"/>:</span>
						<span id="viewBillPay_payeeNameValue"><ffi:getProperty name="IncludeViewPayment" property="Payee.UserAccountNumber"/></span>
					</div>
					<div class="inlineBlock">
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_20"/>:</span>
						<span id="viewBillPay_payeeNameValue"><ffi:getProperty name="IncludeViewPayment" property="Payee.Street"/></span>
					</div>
				</div>
				
				 <div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_21"/>:</span>
						<span id="viewBillPay_payeeNameValue"><ffi:getProperty name="IncludeViewPayment" property="Payee.Street2"/></span>
					</div>
					<div class="inlineBlock">
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_address3"/>:</span>
						<span id="viewBillPay_payeeNameValue"><ffi:getProperty name="IncludeViewPayment" property="Payee.Street3"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_city"/>:</span>
						<span id="viewBillPay_payeeNameValue"><ffi:getProperty name="IncludeViewPayment" property="Payee.City"/></span>
					</div>
					<div class="inlineBlock">
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_state"/>:</span>
						<span id="viewBillPay_payeeNameValue">
						<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
						<ffi:getProperty name="IncludeViewPayment" property="Payee.State"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
							<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
						</ffi:cinclude>
						</span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_zippostalcode"/>:</span>
						<span id="viewBillPay_payeeNameValue">
							<ffi:getProperty name="IncludeViewPayment" property="Payee.ZipCode"/>
						</span>
					</div>
					<div class="inlineBlock">
						<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_country"/>:</span>
						<span id="viewBillPay_payeeNameValue">
								<ffi:cinclude value1="${CountryResource}" value2="">
										<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
										<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
										<ffi:process name="CountryResource" />
									</ffi:cinclude>
									<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${IncludeViewPayment.Payee.Country}" />
								<ffi:getProperty name='CountryResource' property='Resource'/>
						</span>
					</div>
				</div>	
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_83"/>:</span>
							<span id="viewBillPay_payeeNameValue">
								<ffi:getProperty name="IncludeViewPayment" property="Payee.Phone"/>	
							</span>
					</div>	
				</div>
		</div>
	</div>
	<div  class="blockHead"><s:text name="jsp.approval.payment.summary"/></div>
	<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock">
					<span id="viewBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_amount"/>:</span>
					<span id="viewBillPay_payeeNickNameValue" class="columndata">
						<ffi:getProperty name="IncludeViewPayment" property="AmountValue.CurrencyStringNoSymbol" />
		   			 	<ffi:getProperty name="IncludeViewPayment" property="AmtCurrency" />
		   			 </span>
				</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock">
					<span id="viewBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_account"/>:</span>
					<span id="viewBillPay_payeeNickNameValue" class="columndata"><%= _fromAccount %></span>
				</div>
				<div class="inlineBlock">
					<span id="viewBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_76"/>:</span>
					<span id="viewBillPay_payeeNickNameValue" class="columndata"><ffi:getProperty name="IncludeViewPayment" property="PayDate"/></span>
				</div>
			</div>
			
			
			<div class="blockRow">
					<div class="inlineBlock">
						<span id="viewBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_deliver_by"/>:</span>
						<span id="viewBillPay_payeeNickNameValue" class="columndata">
						<ffi:setProperty name="IncludeViewPayment" property="DateFormat" value="${UserLocale.DateFormat}" />
						<ffi:getProperty name="IncludeViewPayment" property="DeliverByDate"/>
						</span>
					</div>
					<div class="inlineBlock">
						<span id="viewBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_repeating"/>:</span>
						<span id="viewBillPay_payeeNickNameValue" class="columndata"><ffi:getProperty name="IncludeViewPayment" property="Frequency"/></span>
					</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock">
					<span id="viewBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_payments"/>:</span>
					<span id="viewBillPay_payeeNickNameValue" class="columndata">
						<ffi:cinclude value1="${IncludeViewPayment.NumberPayments}" value2="999" operator="equals">
						<s:text name="jsp.default_446" />
						</ffi:cinclude>
						<ffi:cinclude value1="${IncludeViewPayment.NumberPayments}" value2="999" operator="notEquals">
						<ffi:getProperty name="IncludeViewPayment" property="NumberPayments"/>
						</ffi:cinclude>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="viewBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_memo"/>:</span>
					<span id="viewBillPay_payeeNickNameValue" class="columndata">
						<ffi:getProperty name="IncludeViewPayment" property="Memo"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock">
					<span id="viewBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_submittedby"/>:</span>
					<span id="viewBillPay_payeeNickNameValue" class="columndata">
						<ffi:getProperty name="SubmittingUserName"/>
					</span>
				</div>
			</div>	
	</div>
</div>		
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			</ffi:cinclude>		
			<ffi:cinclude value1="${IncludeViewPayment.AmountValue.CurrencyCode}" value2="${IncludeViewPayment.Account.CurrencyCode}" operator="notEquals" >
				   
		    </ffi:cinclude>
			<div id="viewBillPay_payeeNameLabel" class="floatRight required" ><s:text name="jsp.default_241"/></div>
				
	   
<% if( session.getAttribute( "PaymentView" ) != null && restricted == false ) { %>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
		<tr>
			<td colspan="6">
				<%-- include batch Transaction History --%>
				<ffi:include page="${PagesPath}common/include-view-transaction-history.jsp" />
			</td>
		</tr>
	</ffi:cinclude>
<% } %>

<ffi:removeProperty name="viewPaymentDoneURL"/>
<ffi:removeProperty name="GetStateProvinceDefnForState"/>
<script type="text/javascript">
$(document).ready(function(){
	$( ".toggleClick" ).click(function(){ 
		if($(this).next().css('display') == 'none'){
			$(this).next().slideDown();
			$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
			$(this).removeClass("expandArrow").addClass("collapseArrow");
		}else{
			$(this).next().slideUp();
			$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
			$(this).addClass("expandArrow").removeClass("collapseArrow");
		}
	});
});

</script>