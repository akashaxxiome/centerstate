<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:include page="${PagesPath}common/include-approval-view-details-init.jsp"/>

<%-- set the header depnding on the type of cashcon --%>
	<ffi:setProperty name='PPayHeading' value='Positive pay Summary'/>


<div class="nameSubTitle"><ffi:getProperty name="PPayHeading"/></div>
	<ffi:removeProperty name="PPayHeading"/>
<ffi:include page="${PagesPath}common/include-approval-view-details-header.jsp"/>
  <ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
	</ffi:cinclude>
	<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
	</ffi:cinclude>
<ffi:include page="${PagesPath}common/include-view-ppay-details.jsp"/>
