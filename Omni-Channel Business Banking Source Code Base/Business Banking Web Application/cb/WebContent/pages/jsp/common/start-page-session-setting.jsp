<!-- 
	This JSP is written to put first page after login in the Session. This value will be used
	by handleHistory.js to check, whether user has reached first page or not.
 -->

<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%
	session.setAttribute("startPage", request.getParameter("startPage"));
%>