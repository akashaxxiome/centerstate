<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.bcreport.ReportLogRecord,com.ffusion.beans.banking.Transfer" %>
<%@ page import="com.ffusion.beans.accounts.*" %>
<%@ page import="com.ffusion.util.enums.UserAssignedAmount"%>

<ffi:object id="GetAuditTransferById" name="com.ffusion.tasks.banking.GetAuditTransferById" scope="session" />
    <ffi:setProperty name="GetAuditTransferById" property="TrackingID" value="${IncludeViewTransfer.TrackingID}"/>
    <ffi:setProperty name="GetAuditTransferById" property="ID" value="${IncludeViewTransfer.ExternalID}"/>
    <ffi:cinclude value1="${IncludeViewTransfer.ExternalID}" value2="" operator="equals">
    	<ffi:setProperty name="GetAuditTransferById" property="ID" value="${IncludeViewTransfer.ID}"/>
    </ffi:cinclude>

    <ffi:setProperty name="GetAuditTransferById" property="BeanSessionName" value="TransactionHistory"/>

    <ffi:cinclude value1="${IncludeViewTransfer.TransferType}" value2="TEMPLATE" operator="equals">
        <ffi:setProperty name="GetAuditTransferById" property="Template" value="true"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${IncludeViewTransfer.TransferType}" value2="RECTEMPLATE" operator="equals">
        <ffi:setProperty name="GetAuditTransferById" property="Template" value="true"/>
    </ffi:cinclude>
<ffi:process name="GetAuditTransferById"/>

<ffi:object id="GetTransferAccounts" name="com.ffusion.tasks.banking.GetTransferAccounts" scope="session" />
    <ffi:setProperty name="GetTransferAccounts" property="AccountsName" value="TransferAccounts" />
<ffi:process name="GetTransferAccounts"/>

<%
	String _fromAccountID = "";
	String _fromAccountDisplayText = "";
	String _toAccountID = "";
	String _toAccountDisplayText = "";
%>
<ffi:getProperty name="IncludeViewTransfer" property="FromAccount.ID" assignTo="_fromAccountID"/>
<ffi:getProperty name="IncludeViewTransfer" property="FromAccount.AccountDisplayText" assignTo="_fromAccountDisplayText"/>

<ffi:getProperty name="IncludeViewTransfer" property="ToAccount.ID" assignTo="_toAccountID"/>
<ffi:getProperty name="IncludeViewTransfer" property="ToAccount.AccountDisplayText" assignTo="_toAccountDisplayText"/>

<%
	String _strFromAccount = "";
	String _strToAccount = "";
	String restricted = "false";
%>

<%
	if(_fromAccountDisplayText != null) {
		_strFromAccount = _fromAccountDisplayText;
	}
	if(_toAccountDisplayText != null) {
		_strToAccount = _toAccountDisplayText;
	}
	
	if(_fromAccountDisplayText == null || _fromAccountDisplayText.equalsIgnoreCase("Restricted")) {
		restricted = "true";
	}
	
	if(_toAccountDisplayText == null || _toAccountDisplayText.equalsIgnoreCase("Restricted")) {
		restricted = "true";
	}
%>

<ffi:cinclude value1="${CollectionName}" value2="ApprovalsItems">
    <%-- If we are dealing with a transfer to be approved and the user-assigned amount flag is FROM or TO,
         then set the estimated amount flag to true because the amount has not yet been "locked-in" --%>
    <ffi:cinclude value1="${IncludeViewTransfer.UserAssignedAmountFlagName}" value2="from">
        <ffi:setProperty name="IncludeViewTransfer" property="EstimatedAmountFlag" value="true"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${IncludeViewTransfer.UserAssignedAmountFlagName}" value2="to">
        <ffi:setProperty name="IncludeViewTransfer" property="EstimatedAmountFlag" value="true"/>
    </ffi:cinclude>
</ffi:cinclude>

<ffi:setProperty name="viewTransferDoneURL" value="${SecurePath}payments/index.jsp?DoNotReload=true" URLEncrypt="true" />
<% if( session.getAttribute( "transfer_details_post_jsp" ) != null ) { %>
    <ffi:setProperty name="viewTransferDoneURL" value="${transfer_details_post_jsp}"/>
<% } %>
<ffi:setProperty name="BackURL" value="${viewTransferDoneURL}"/>



	
	 <div class="blockWrapper label140">
		<div  class="blockHead"><ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/include-view-accounttransfer-details.jsp" parm0="${ApprovalsItem.Type}" /><%-- </ffi:getL10NString> --%></div>
	
	 
	
       	<div class="blockContent">
		<ffi:cinclude value1="${IncludeViewTransfer.TemplateName}" value2="" operator="notEquals">
		 <div class="blockRow">
	      	<div class="inlineBlock" style="width: 50%">	
                <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_416"/>:</span>
		        <span><ffi:getProperty name="IncludeViewTransfer" property="TemplateName"/></span>			
			</div>
			<div class="inlineBlock">
               <span class="sectionsubhead sectionLabel"></span>
		        <span></span>			
			</div>
		</div>
		</ffi:cinclude>
		<div class="blockRow">
		     <div class="inlineBlock" style="width: 50%">
		               <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_217"/>:</span>
				        <span><%= _strFromAccount %></span>		
			</div>
			<div class="inlineBlock">
		               <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_424"/>:</span>
				        <span><%= _strToAccount %></span>		
			</div>
		</div>
			
		<ffi:cinclude value1="${IncludeViewTransfer.UserAssignedAmountFlagName}" value2="single" operator="equals">
		<div class="blockRow">
		     <div class="inlineBlock">
		    <ffi:cinclude value1="${IncludeViewTransfer.UserAssignedAmountFlagName}" value2="single">
		               <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
				        <span><ffi:getProperty name="IncludeViewTransfer" property="AmountValue.CurrencyStringNoSymbol"/>
                           <ffi:getProperty name="IncludeViewTransfer" property="AmountValue.CurrencyCode"/></span>	
                 </ffi:cinclude>
			</div>
			<div class="inlineBlock">
			     <span class="sectionsubhead"></span>
		        <span></span>	
            </div>
		</div>
		</ffi:cinclude>
		<ffi:cinclude value1="${IncludeViewTransfer.UserAssignedAmountFlagName}" value2="single" operator="notEquals">
			<div class="blockRow">
			     <div class="inlineBlock" style="width: 50%">
			     	 <ffi:setProperty name="DisplayEstimatedAmountKey" value="false"/>
	                	<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_489"/>:</span>
			        	<span>
			       	 		<ffi:cinclude value1="${IncludeViewTransfer.IsAmountEstimated}" value2="true">
	                     		&#8776;<ffi:setProperty name="DisplayEstimatedAmountKey" value="true"/>
	                 		</ffi:cinclude>
	                 		<ffi:getProperty name="IncludeViewTransfer" property="AmountValue.CurrencyStringNoSymbol"/>
	                 		<ffi:getProperty name="IncludeViewTransfer" property="AmountValue.CurrencyCode"/>
	                	</span>	
		         </div>
				 <div class="inlineBlock">
				     <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_512"/>:</span>
			        <span>
				       <ffi:cinclude value1="${IncludeViewTransfer.IsToAmountEstimated}" value2="true">
	                                &#8776;<ffi:setProperty name="DisplayEstimatedAmountKey" value="true"/>
	                                
	                   </ffi:cinclude>
	                   <ffi:getProperty name="IncludeViewTransfer" property="ToAmountValue.CurrencyStringNoSymbol"/>
	                   <ffi:getProperty name="IncludeViewTransfer" property="ToAmountValue.CurrencyCode"/>
	                </span>	
		             
				</div>
				
			</div>
		</ffi:cinclude>
		<div class="blockRow">
		     <div class="inlineBlock" style="width: 50%">
			     <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_137"/>:</span>
		        <span><ffi:getProperty name="IncludeViewTransfer" property="Date"/></span>	
	         </div>
			<div class="inlineBlock">
			     <span class="sectionsubhead"></span>
		         <span></span>	
	        </div>
		</div>
		<ffi:cinclude value1="${IncludeViewTransfer.Frequency}" value2="" operator="notEquals">
				<div class="blockRow" >
				     <div class="inlineBlock" style="width: 50%">
						
					     	<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_351"/>:</span>
					    
				        <span><ffi:getProperty name="IncludeViewTransfer" property="Frequency"/></span>	
		        	 </div>
				     <div class="inlineBlock">
						   <span class="sectionsubhead sectionLabel"> <s:text name="jsp.common_105"/>:
						       <ffi:cinclude value1="${IncludeViewTransfer.NumberTransfers}" value2="999" operator="equals">
						       		<s:text name="jsp.default_446"/>:
						       	</ffi:cinclude>
					       </span>
					       <span>	
			                <ffi:cinclude value1="${IncludeViewTransfer.NumberTransfers}" value2="999" operator="notEquals">
			                	<ffi:getProperty name="IncludeViewTransfer" property="NumberTransfers"/>
			                </ffi:cinclude>
						   	</span>
				  	</div>
	         	</div>
		</ffi:cinclude>
		
		<div class="blockRow">
		     <div class="inlineBlock" style="width: 50%">
			     <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
		        <span><ffi:getProperty name="IncludeViewTransfer" property="Memo"/></span>	
	         </div>
		</div>
		
		<ffi:cinclude value1="${IncludeViewTransfer.UserAssignedAmountFlagName}" value2="single" operator="notEquals">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 49%">&nbsp;</div>
			    <div class="inlineBlock" style="width: 49%; text-align:right;">
				    <span class="required">
				    	<ffi:cinclude value1="${DisplayEstimatedAmountKey}" value2="true"> 
			        		<s:text name="jsp.common_2"/>
			        	</ffi:cinclude>
			        </spv>
		         </div>
			</div>
		</ffi:cinclude>	
	</div>	
	</div>   
		<div class="blockWrapper">
	         <div  class="blockHead">
	         
				<ffi:cinclude value1="<%=restricted%>" value2="false" operator="equals">
	                <s:action namespace="/pages/jsp/approvals" name="GetTransactionHistoryAction_init" executeResult="true">
	                    <s:param name="TrackingID" value="%{#session.IncludeViewTransfer.TrackingID}"/>
	                </s:action>
				</ffi:cinclude>
	
			 </div>
<ffi:removeProperty name="viewTransferDoneURL"/>
<ffi:removeProperty name="TransferFromAccount"/>
<ffi:removeProperty name="TransferToAccount"/>

<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});
	
</script>
