<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
// for wire batches set the wire transfer bean in the session
if( session.getAttribute( "WireBatch" ) != null && session.getAttribute( "ApprovalsViewTransactionDetails") != null ) {
	session.removeAttribute( "WireTransfer" );
	com.ffusion.beans.wiretransfers.WireBatch batch = 
		( com.ffusion.beans.wiretransfers.WireBatch ) session.getAttribute( "WireBatch" );
	com.ffusion.beans.wiretransfers.WireTransfers transfers = batch.getWires();
	String trackingID = ( String )session.getAttribute( "trackingID" );
	java.util.Iterator iterator = transfers.iterator();
	while( iterator.hasNext() ) {
		com.ffusion.beans.wiretransfers.WireTransfer trans = 
		( com.ffusion.beans.wiretransfers.WireTransfer )iterator.next();
		if( trans.getTrackingID().equals( trackingID ) ) {
			session.setAttribute( "WireTransfer" , trans );
			break;
		}
	}
} 

// get the wire type, source and destination from the wire transfer bean
com.ffusion.beans.wiretransfers.WireTransfer wire = 
 ( com.ffusion.beans.wiretransfers.WireTransfer ) session.getAttribute( "WireTransfer" );
String wireType = wire.getWireType();
String wireSource = wire.getWireSource();
String wireDestination = wire.getWireDestination();
if( wireType == null ) {
	wireType = "";
}
if( wireSource == null ) {
	wireSource = "";
}
if( wireDestination == null ) {
	wireDestination = "";
}

// get the appropriate display message using the wireType and wireSource
String _displayMessage = "View Wire Transfer Details"; // default message
if( wireType.equals( com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_TEMPLATE ) ||
    wireType.equals( com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_RECTEMPLATE ) ||
    wireSource.equals( "TEMPLATE" ) ) {
	_displayMessage = "View WireBatch Details";
}
%>
<ffi:object id="GetAuditWireTransferById" name="com.ffusion.tasks.wiretransfers.GetAuditWireTransferById" scope="session" />
    <ffi:setProperty name="GetAuditWireTransferById" property="TrackingID" value="${WireTransfer.TrackingID}"/>
    <ffi:setProperty name="GetAuditWireTransferById" property="ID" value="${WireTransfer.ID}"/>
    <ffi:setProperty name="GetAuditWireTransferById" property="BeanSessionName" value="WireTransferHistory"/>

    <ffi:cinclude value1="${WireTransfer.WireType}" value2="TEMPLATE" operator="equals">
        <ffi:setProperty name="GetAuditWireTransferById" property="Template" value="true"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${WireTransfer.WireType}" value2="RECTEMPLATE" operator="equals">
        <ffi:setProperty name="GetAuditWireTransferById" property="Template" value="true"/>
    </ffi:cinclude>
<ffi:process name="GetAuditWireTransferById"/>
<ffi:cinclude value1="${WireAccountType}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.Resource" id="WireAccountType" scope="session"/>
    <ffi:setProperty name="WireAccountType" property="ResourceFilename" value="com.ffusion.beansresources.wiretransfers.resources"/>
    <ffi:process name="WireAccountType" />
</ffi:cinclude>


    <div class="nameSubTitle" align="center"><%= _displayMessage %></div>
   	<ffi:include page="${PagesPath}common/include-approval-view-details-header.jsp"/>
	<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
	</ffi:cinclude>
	<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
	</ffi:cinclude>
	    <div align="center">
		<% if( wireDestination.equals( com.ffusion.beans.wiretransfers.WireDefines.WIRE_DRAWDOWN ) ) { %>
			<ffi:include page="${PagesPath}common/include-view-wiretransfer-drawdown.jsp" />
		<% } else if( wireDestination.equals( com.ffusion.beans.wiretransfers.WireDefines.WIRE_BOOK ) ) { %>
			<ffi:include page="${PagesPath}common/include-view-wiretransfer-book.jsp" />
		<% } else if( wireDestination.equals( com.ffusion.beans.wiretransfers.WireDefines.WIRE_HOST ) ) { %>
			<ffi:include page="${PagesPath}common/include-view-wiretransfer-host.jsp" />
		<% } else if( wireDestination.equals( com.ffusion.beans.wiretransfers.WireDefines.WIRE_FED ) ) { %>
			<ffi:include page="${PagesPath}common/include-view-wiretransfer-fed.jsp" />
		<% } else { %>
			<ffi:include page="${PagesPath}common/include-view-wiretransfer.jsp" />
		<% } %>
	    </div>
