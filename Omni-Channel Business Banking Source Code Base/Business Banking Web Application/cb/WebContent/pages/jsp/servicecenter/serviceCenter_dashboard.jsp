<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

    
    <div class="dashboardUiCls">
	   	<div class="moduleSubmenuItemCls">
		    <span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-services"></span>
	   		<span class="moduleSubmenuLbl">
	   			<s:text name="SERVICES"/>
	   		</span>
	   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			<!-- dropdown menu include -->    		
	   		<s:include value="/pages/jsp/home/inc/serviceCenterSubmenuDropdown.jsp" />
	  	</div>
  	
  	
	<div id="dbOnlineCustomerSummary" class="dashboardSummaryHolderDiv">
	
	   	<s:url id="onlineCustSummaryUrl" value="/pages/jsp/servicecenter/serviceCenter_summary.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		
		<sj:a id="olCustLink" href="%{onlineCustSummaryUrl}" targets="summary" button="true" 
			title="%{getText('SERVICES')}" cssClass="summaryLabelCls" 
			onClickTopics="" onCompleteTopics="onCustServicesLoadComplete" onErrorTopics="errorLoad"><s:text name="SERVICES"/></sj:a>
		
	</div>
	
	<!-- dashboard items listing for requested images, hidden by default -->
	<div id="dbContactInfoSummary" class="dashboardSummaryHolderDiv">

		<s:url id="contInfoSummaryUrl" value="/pages/jsp/servicecenter/inc/contact_information.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		
		<sj:a id="contactInfoLink" href="%{contInfoSummaryUrl}" targets="summary" button="true" 
			title="%{getText('jsp.servicecenter_contact_information')}" cssClass="summaryLabelCls" 
			onClickTopics="contactInfoLoadFunction" onCompleteTopics="" onErrorTopics="errorLoad"><s:text name="jsp.servicecenter_contact_information"/></sj:a>
			
	</div>
	
	
	<!-- More list option listing -->
	<div class="dashboardSubmenuItemCls">
		<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
	
		<!-- UL for rendering tabs -->
		<ul class="dashboardSubmenuItemList">
			<li class="dashboardSubmenuItem"><a id="showdbOnlineCustomerSummary" onclick="ns.common.refreshDashboard('showdbOnlineCustomerSummary',true)"><s:text name="SERVICES"/></a></li>
			<li class="dashboardSubmenuItem"><a id="showdbContactInfoSummary" onclick="ns.common.refreshDashboard('showdbContactInfoSummary',true)"><s:text name="jsp.servicecenter_contact_information" /></a></li>
		</ul>
	</div>
 </div> 	
  	
 <script>
	 $("#olCustLink").find("span").addClass("dashboardSelectedItem");
 </script>
