<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<ffi:author page="servicecenter/done.jsp" />

<ffi:getL10NString rsrcFile="cb" msgKey="${messageKey}"
	parm0="${parameter0}" parm1="${parameter1}" parm2="${parameter2}" />

<ffi:removeProperty name="messageKey" />
<ffi:removeProperty name="SCPageTitle" />
<ffi:removeProperty name="parameter0" />
<ffi:removeProperty name="parameter1" />
<ffi:removeProperty name="parameter2" />
