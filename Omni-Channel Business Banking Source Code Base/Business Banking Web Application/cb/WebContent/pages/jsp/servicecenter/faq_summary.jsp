<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>



<div id="serviceSummaryTabs" class="portlet showTitle" style="float: left; width: 100%;">
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="FAQ"/></span>
	</div>
	<div class="portlet-content" id="serviceSummaryTabsContent">
		<s:include value="/pages/jsp/servicecenter/faq.jsp"/>
	</div>
	<div id="serviceSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('serviceSummaryTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<script type="text/javascript">	
	//Initialize portlet with settings icon
	ns.common.initializePortlet("serviceSummaryTabs");
</script>