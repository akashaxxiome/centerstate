<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<script type="text/javascript" src="<s:url value='/web/js/servicecenter/servicecenter%{#session.minVersion}.js'/>"></script>
<script src="<s:url value='/web'/>/js/reports/reports.js" type="text/javascript"></script>

<style media="print">
	#sessionReceiptReportTableID,
	#sessionReceiptFilterContentDiv{
		-moz-transform-origin: 0px 0px;
		-webkit-transform-origin: 0px 0px;
		-moz-transform: scale(0.90, 1);
	    -webkit-transform: scale(0.90, 1);
	    zoom : 0.67;
	}
	
</style>	
<s:include value="/pages/jsp/servicecenter/inc/session-receipt-init.jsp"/>
<iframe id="ifrmDownload" name="ifrmDownload" src="" style="visibility:hidden; width: 0px; height: 0px;">
</iframe>
<div id="desktop" align="center">
		<div id="operationresult">
			<div id="resultmessage"><s:text name="jsp.default_498"/></div>
		</div><!-- result -->
				<s:include value="/pages/jsp/servicecenter/inc/session-receipt-filter.jsp"/>
</div>

<script type="text/javascript">
<!--
$(document).ready(function() {
	$('#sessionReceiptFilterFormSubmit').trigger('click');
});
//-->
</script>