<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

	<div id="detailsPortlet" class="portlet ui-widget ui-widget-content ui-corner-all showTitle">
        <div class="portlet-header ui-widget-header ui-corner-all">
        	<span class="portlet-title"></span>
		</div>
        <div class="portlet-content">
	        <div id="inputDiv" style="border:1px">
	        </div>
		</div>
		<div id="detailsPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
    </div>


<script>   

	//Initialize portlet with settings icon
	ns.common.initializePortlet("detailsPortlet");
</script>
