<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%--
This page implements the paye layout tile for date/time input for any of CB/BC reports. This page will layout
widgets to ask the user for absolute or relative date. Based on the user's selection it will enable/disable
appropriate text fields/list box.

It displays the widget for the user to enter the time values if "DisplayTime" variable is set in the session
before including this page.

It displays the previous business day range if "ShowPreviousBusinessDay" variable is set in the session
before including this page.

By default, the date range cannot be longer than 31 days.  A page may override this restriction by putting
a value (the number of days) as "OverrideDateSearchDefault" in the session.  OverrideDateSearchDefault=-1
is treated as unlimited.  If present, OverrideDateSearchDefault is removed from the session.
This is done in rptdatescheck_js.jsp.

If the including page has added a variable "ShowTodayIntradayNew" to the session with a value of true, the
option to display today's new intraday transactions will also be displayed.
--%>

<%-- <ffi:setProperty name="_calTarget_Start" value="RSeC_StartDate"/>
<ffi:setProperty name="_calTarget_End" value="RSeC_EndDate"/>
<ffi:setProperty name="_urlEncrypt_Start" value='calendar.jsp?calDirection=back&calForm=FormName&calTarget=${_calTarget_Start}' URLEncrypt="true"/>
<ffi:setProperty name="_urlEncrypt_End" value='calendar.jsp?calDirection=back&calForm=FormName&calTarget=${_calTarget_End}' URLEncrypt="true"/>

<ffi:include page="${PagesPath}reports/inc/rptdatescheck_js.jsp" /> --%>
<script type="text/javascript">


$(document).ready(function(){
	/* init date range picker component*/
	var aUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?onlyOneDirection=true&calDirection=back&calForm=LogForm&calTarget=FindLogMessages.StartDate"/>';
	var aConfig = {
		url:aUrl,
		startDateBox:$("#startDateStr"),
		endDateBox:$("#endDateStr"),
	}
	$("#sessionActivityDateRangeBox").extdaterangepicker(aConfig);
});	

function setDateRange( form ) {
	var form1 = document.forms[form];
	var dateTypeSelector = form1["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"];
	var hiddenDateRangeField = form1["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"];
  	if ( dateTypeSelector[1].checked ) {
   		// we have selected a range using the drop down, put that range in the hidden field
   		hiddenDateRangeField.value = form1["DateRangeDropDown"].value;
  	} else if ( dateTypeSelector[2] != null && dateTypeSelector[2].checked ) {
   		hiddenDateRangeField.value = "<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_VALUE_TODAY_NEW %>";
       	}
}

function selectRadio( name, form ) {
	var form1 = document.forms[form];
	obj = form1["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"]
	if (name == 'relative') 	{
		obj[1].checked = true
	}
	else {
		obj[0].checked = true
	}
}

// --></script>
<style>
.datetime-datepicker-icon{
	visibility:hidden;
	position: relative;
	margin:0px;
	left:-85px;
	width:0px;
}
</style>
<script>
$( ".datetime-datepicker-icon" ).each(function(){
	var $dp = $(this);
	//var oTxt = $dp.val();
	//alert("t original text: " + oTxt);
	$dp.datepicker({
		constrainInput: false,
		showOn: "button",
		buttonImage: "<s:url value="/struts/js/"/>calendar.gif",
		buttonImageOnly: true,
		onSelect: function(dateText, inst) {
			$dp.prev('input').val(dateText);
			selectRadio('abs', $dp[0].form);
		}
	});
});


</script>
<% String dateFormat = "";
String timeFormat = "";
%>
<ffi:getProperty name="UserLocale" property="DateFormat" assignTo="dateFormat"/>
<ffi:getProperty name="UserLocale" property="TimeFormat" assignTo="timeFormat"/>

<%
	com.ffusion.beans.reporting.Report report = (com.ffusion.beans.reporting.Report) session.getAttribute( "ReportData" );
	String rangeType = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE );
	String startDate = new String();
	String startTime = new String();
	String endDate = new String();
	String endTime = new String();
	if( rangeType.equalsIgnoreCase( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE ) ) {
		startDate = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_DATE );
		if( startDate != null && startDate.length() != 0 && !startDate.equalsIgnoreCase( dateFormat ) ) {
			java.util.Date start;
			try{
			    //start = com.ffusion.util.DateFormatUtil.getFormatter( com.ffusion.util.DateConsts.REPORT_CRITERIA_INTERNAL_FORMAT_DATETIME_STR_WITH_SEC ).parse( startDate );
				start = com.ffusion.util.DateParser.parse( startDate, com.ffusion.util.DateConsts.REPORT_CRITERIA_INTERNAL_FORMAT_DATETIME_STR_WITH_SEC );
			} catch ( Exception ex ) {
			    // Try dateFormat format
			    //start = com.ffusion.util.DateFormatUtil.getFormatter( com.ffusion.util.DateConsts.REPORT_CRITERIA_INTERNAL_FORMAT_DATE_STR ).parse( startDate );
				start = com.ffusion.util.DateParser.parse( startDate, com.ffusion.util.DateConsts.REPORT_CRITERIA_INTERNAL_FORMAT_DATE_STR );
			}
			startDate = com.ffusion.util.DateFormatUtil.getFormatter(dateFormat).format( start );
			java.util.Calendar cal = java.util.Calendar.getInstance();
			cal.setTime( start );
			if( cal.get( java.util.Calendar.HOUR_OF_DAY ) == 0 && cal.get( java.util.Calendar.MINUTE ) == 0 ) {
				startTime = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_TIME );
			} else {
				startTime = com.ffusion.util.DateFormatUtil.getFormatter(timeFormat).format( start );
			}
		} else {
			startDate = dateFormat;
			startTime = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_TIME );
		}

		endDate = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_DATE );;
		if( endDate != null && endDate.length() != 0 && !endDate.equalsIgnoreCase( dateFormat ) ) {
			java.util.Date end;
			try {
			   //end = com.ffusion.util.DateFormatUtil.getFormatter( com.ffusion.util.DateConsts.REPORT_CRITERIA_INTERNAL_FORMAT_DATETIME_STR_WITH_SEC ).parse( endDate );
				end = com.ffusion.util.DateParser.parse( endDate, com.ffusion.util.DateConsts.REPORT_CRITERIA_INTERNAL_FORMAT_DATETIME_STR_WITH_SEC );
			} catch ( Exception ex ) {
			   // Try dateFormat format
			   //end = com.ffusion.util.DateFormatUtil.getFormatter( com.ffusion.util.DateConsts.REPORT_CRITERIA_INTERNAL_FORMAT_DATE_STR ).parse( endDate );
				end = com.ffusion.util.DateParser.parse( endDate, com.ffusion.util.DateConsts.REPORT_CRITERIA_INTERNAL_FORMAT_DATE_STR );
			}
			endDate = com.ffusion.util.DateFormatUtil.getFormatter(dateFormat).format( end );
			java.util.Calendar cal = java.util.Calendar.getInstance();
			cal.setTime( end );
			if( cal.get( java.util.Calendar.HOUR_OF_DAY ) == 0 && cal.get( java.util.Calendar.MINUTE ) == 0 ) {
				endTime = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_TIME );
			} else {
				endTime = com.ffusion.util.DateFormatUtil.getFormatter(timeFormat).format( end );
			}
		} else {
			endDate = dateFormat;
			endTime = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_TIME );
		}
	} else {
		//startDate = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_DATE );
		//startTime = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_TIME );
		//endDate = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_DATE );
		//endTime = (String) report.getReportCriteria().getSearchCriteria().get( com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_TIME );
	}

	if( startDate == null || startDate.length() == 0 ) {
		startDate = dateFormat;
	}

	if( endDate == null || endDate.length() == 0 ) {
		endDate = dateFormat;
	}

	if( startTime == null || startTime.length() == 0 ) {
		startTime = timeFormat;
	}

	if( endTime == null || endTime.length() == 0 ) {
		endTime = timeFormat;
	}

%>
<ffi:cinclude value1="${ShowTodayIntradayNew}" value2="" operator="equals">
	<ffi:setProperty name="ShowTodayIntradayNew" value="FALSE"/>
</ffi:cinclude>

<ffi:cinclude value1="${ShowTodayIntradayNew}" value2="TRUE" operator="notEquals">
	<%-- we are not displaying the third radio button option, but if it is selected, we need to retrieve the default
	     values from the default report criteria object --%>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>


	<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>" operator="equals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
		<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_VALUE_TODAY_NEW %>" operator="equals">
			<ffi:object id="GetDefaultReportCriteriaForDateTimePage" name="com.ffusion.tasks.reporting.GetDefaultReportCriteria"/>
			<ffi:setProperty name="GetDefaultReportCriteriaForDateTimePage" property="ReportCriteriaName" value="defaultReportCriteriaForDateTimePage"/>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>"/>
			<ffi:setProperty name="GetDefaultReportCriteriaForDateTimePage" property="ReportType" value="${ReportData.ReportCriteria.CurrentReportOptionValue}"/>
			<ffi:process name="GetDefaultReportCriteriaForDateTimePage"/>

			<%-- replace the current date range type with the default date range type --%>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
			<ffi:setProperty name="defaultReportCriteriaForDateTimePage" property="CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${defaultReportCriteriaForDateTimePage.CurrentSearchCriterionValue}"/>


			<%-- replace the current date range with the default date range --%>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
			<ffi:setProperty name="defaultReportCriteriaForDateTimePage" property="CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${defaultReportCriteriaForDateTimePage.CurrentSearchCriterionValue}"/>

			<ffi:removeProperty name="defaultReportCriteriaForDateTimePage"/>
			<ffi:removeProperty name="GetDefaultReportCriteriaForDateTimePage"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<span class="floatleft" style="display:inline-block; width:50px; line-height:70px"><s:text name="jsp.default_137"/>:</span>
	<span style="display:inline-block;"><% String tempValue; %>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
		<input type="radio" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>"
		<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>" operator="equals"> Checked </ffi:cinclude> >
		<input type="text" id="sessionActivityDateRangeBox" style="width:180px" />
		<%-- <input class="ui-widget-content ui-corner-all"
			onfocus="selectRadio('abs', this.form)"
			type="text"
			daterangetype="absolute"
			name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_DATE %>"
			value='<%= startDate %>' size="12" maxlength="10"><input class="datetime-datepicker-icon" /> --%>
		<input id="startDateStr" type="Hidden" name="RSeC_StartDate" value='<%= startDate %>'/>
		<input id="endDateStr" type="Hidden" name="RSeC_EndDate" value='<%= endDate %>'/>
		<input id="" type="Hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.DATE_FLAG_PREFIX %><%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_DATE %>" value="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.DATETIME_FLAG_ENABLE_STRING %>">
		<ffi:cinclude value1="${DisplayTime}" value2="TRUE" operator="equals">
			<input class="ui-widget-content ui-corner-all" onfocus="selectRadio('abs', this.form)" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_TIME %>" value='<%= startTime %>' size="5">
			<input type="Hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.TIME_FLAG_PREFIX %><%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_TIME %>" value="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.DATETIME_FLAG_ENABLE_STRING %>">
		</ffi:cinclude>
		<%-- <input class="ui-widget-content ui-corner-all"
			onfocus="selectRadio('abs', this.form)"
			daterangetype="absolute"
			type="text"
			name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_DATE %>"
			value='<%= endDate %>' size="12" maxlength="10"><input class="datetime-datepicker-icon" /> --%>
		<input id="" type="Hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.DATE_FLAG_PREFIX %><%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_DATE %>" value="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.DATETIME_FLAG_ENABLE_STRING %>">
		<ffi:cinclude value1="${DisplayTime}" value2="TRUE" operator="equals">
		</span>
		<input class="ui-widget-content ui-corner-all" onfocus="selectRadio('abs', this.form)" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_TIME %>" value='<%= endTime %>' size="5">
			<input type="Hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.TIME_FLAG_PREFIX %><%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_TIME %>" value="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.DATETIME_FLAG_ENABLE_STRING %>">
			<ffi:getProperty name="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
			<span class="submain"> &nbsp;&nbsp;<s:text name="jsp.reports_timein24"/></span>
		</ffi:cinclude>	<span id="absDateRangeError"></span><span style="display:block" class="marginTop10">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
		<input onchange="" type="radio" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>"
		<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>" operator="equals"><ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/> <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_VALUE_TODAY_NEW %>" operator="notEquals"> Checked </ffi:cinclude> </ffi:cinclude> >
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
		 <sj:a 
				button="true"  cssStyle="width:192px; vertical-align:middle;"
				><s:text name="jsp.servicecenter_current_session"></s:text></sj:a>
		<select onchange= "selectRadio('relative', this.form);setDateRange( this.form );" class="txtbox hidden" id="dateRangeDropDown" name="DateRangeDropDown">
			
			<option value=""
			<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="" operator="equals"> Selected </ffi:cinclude> >
				<s:text name="jsp.default_375"/>
			</option>
			
			<option value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_SESSION %>"
			<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_SESSION %>" operator="equals"> Selected
			</ffi:cinclude> ><s:text name="jsp.servicecenter_current_session"/></option>
		</select> <span id="relDateRangeError"></span></span>

<%-- we only want to show the third option - for intra day new transactions for today, if the ShowTodayIntradayNew variable is set to true --%>
<ffi:cinclude value1="${ShowTodayIntradayNew}" value2="TRUE" operator="equals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
		<input type="radio" onclick="setDateRange(this.form);" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>"
		<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>" operator="equals"> <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/> <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_VALUE_TODAY_NEW %>" operator="equals"> Checked </ffi:cinclude> </ffi:cinclude> > Today's new intra-day transactions only.
</ffi:cinclude>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
<input type="Hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue"/>">

<ffi:removeProperty name="DisplayTime" />
