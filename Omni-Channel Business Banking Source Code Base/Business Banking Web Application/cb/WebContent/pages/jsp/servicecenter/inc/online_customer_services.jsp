<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<ffi:removeProperty name="TransferInquiry" />
<div id="onlineCustomerServicesTab">
	<ffi:help id="servicecenter_services_onlineCustomerServices" />
	<%-- <ul class="portlet-header">
		<li><a id="achPayeesPendingApprovalLink"
			href="#onlineCustomerServicesDiv"><s:text
					name="jsp.servicecenter_online_customer_services" /> </a></li>
	</ul> --%>
	<div id="achPayeesPendingApprovalChangeDiv" >
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td class="instructions" colspan="5" style="padding:0 3px 20px 3px; color:#000"><ffi:getL10NString
						rsrcFile="cb"
						msgKey="jsp/servicecenter/online_customer_services.jsp-1" /></td>
			</tr>
			<tr valign="top">
				<td width="32%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left"><span> <s:url id="addAccountsURL"
										value="/pages/jsp/servicecenter/inc/add_bill_pay_account.jsp"
										escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									</s:url> <sj:a id="addAccountsLink" href="%{addAccountsURL}"
										indicator="indicator" targets="inputDiv"
										onClickTopics="beforeLoadServicesForm"
										onCompleteTopics="loadServicesFormComplete"
										onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
										<s:text
											name="jsp.servicecenter_add_bill_pay" />
									</sj:a> </span></td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left"><span> <s:url id="addOnlineAccountsURL"
										value="/pages/jsp/servicecenter/inc/add_online_accounts.jsp"
										escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									</s:url> <sj:a id="addOnlineAccountsLink" href="%{addOnlineAccountsURL}"
										indicator="indicator" targets="inputDiv"
										onClickTopics="beforeLoadServicesForm"
										onCompleteTopics="loadServicesFormComplete"
										onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
										<s:text
											name="jsp.servicecenter_add_online_accounts" />
									</sj:a> </span></td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class=""><span>
									<s:url id="ajax" action="ModifyUserProfileAction_init"
										namespace="/pages/jsp/user" escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									</s:url>
									<sj:a id="changeContactDetailButton"
										href="javascript:void(0)"
										onclick="ns.common.openDialog('updateProfile');" cssClass="anchorText">
										<s:text
											name="jsp.servicecenter_change_address_email_or_phone_numbers" />
									</sj:a> </span></td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class=""><span> <s:url
										id="ajax" action="ModifyUserProfileAction_init"
										namespace="/pages/jsp/user" escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									</s:url> <sj:a id="changeUserNamePasswordButton"
										href="javascript:void(0)"
										onclick="ns.common.openDialog('updateProfile');" cssClass="anchorText">
										<s:text name="jsp.servicecenter_change_username_or_password" />
									</sj:a> </span></td>
						</tr>
					</table></td>
				<td align="left" width="1%">&nbsp;</td>
				<td width="32%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" class=""><span> <s:url
										id="orderClearedChecksURL"
										value="/pages/jsp/servicecenter/order_cleared_checks.jsp"
										escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
									</s:url> <sj:a id="orderClearedChecksLink"
										href="%{orderClearedChecksURL}" indicator="indicator"
										targets="inputDiv" onClickTopics="beforeLoadServicesForm"
										onCompleteTopics="loadServicesFormComplete"
										onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
										<s:text
											name="jsp.servicecenter_order_copies_of_cleared_checks" />
									</sj:a> </span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class=""><span> <s:url
										id="orderCopyStatementURL"
										value="/pages/jsp/servicecenter/order_copy_statement.jsp"
										escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
									</s:url> <sj:a id="orderCopyStatementLink"
										href="%{orderCopyStatementURL}" indicator="indicator"
										targets="inputDiv" onClickTopics="beforeLoadServicesForm"
										onCompleteTopics="loadServicesFormComplete"
										onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
										<s:text name="jsp.servicecenter_order_statement_copies" />
									</sj:a> </span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class=""><span> <s:url
										id="orderChecksURL"
										value="/pages/jsp/servicecenter/order_checks.jsp"
										escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
									</s:url> <sj:a id="orderChecksLink" href="%{orderChecksURL}"
										indicator="indicator" targets="inputDiv"
										onClickTopics="beforeLoadServicesForm"
										onCompleteTopics="loadServicesFormComplete"
										onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
										<s:text name="jsp.servicecenter_order_checks" />
									</sj:a> </span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class=""><span> <s:url
										id="orderDepositEnvelopesURL"
										value="/pages/jsp/servicecenter/order_deposit_envelopes.jsp"
										escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
									</s:url> <sj:a id="orderDepositEnvelopesLink"
										href="%{orderDepositEnvelopesURL}" indicator="indicator"
										targets="inputDiv" onClickTopics="beforeLoadServicesForm"
										onCompleteTopics="loadServicesFormComplete"
										onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
										<s:text name="jsp.servicecenter_order_deposit_envelopes" />
									</sj:a> </span>
							</td>
						</tr>
					</table></td>
				<td align="left" width="1%">&nbsp;</td>
				<td width="32%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="left" class=""><span>
								<s:url id="generalQuestionsURL" namespace="/pages/jsp/message" action="SendMessageAction_init.action" escapeAmp="false">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
									<s:param name="initResponse" value="%{'generalQuestions'}"/>
								</s:url>
								<sj:a id="generalQuestionsLink" href="%{generalQuestionsURL}"
										indicator="indicator" targets="inputDiv"
										onClickTopics="beforeLoadServicesForm"
										onCompleteTopics="loadServicesFormComplete"
										onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
										<s:text name="jsp.servicecenter_general_comments_or_questions" />
								</sj:a> </span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS %>">
							<tr>
								<td align="left" class=""><span> <s:url
											id="billPaymentInquiryURL"
											value="/pages/jsp/servicecenter/bill_payment_inquiry.jsp"
											escapeAmp="false">
											<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
										</s:url> <sj:a id="billPaymentInquiryLink"
											href="%{billPaymentInquiryURL}" indicator="indicator"
											targets="inputDiv" onClickTopics="beforeLoadServicesForm"
											onCompleteTopics="loadServicesFormComplete"
											onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
											<s:text name="jsp.servicecenter_bill_payment_inquiry" />
										</sj:a> </span>
								</td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
							</tr>
						</ffi:cinclude>
						<ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS %>">
							<tr>
								<td align="left" class=""><span> <s:url
											id="transferInquiryURL"
											value="/pages/jsp/servicecenter/transfer_inquiry.jsp"
											escapeAmp="false">
											<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
										</s:url> <sj:a id="transferInquiryLink" href="%{transferInquiryURL}"
											indicator="indicator" targets="inputDiv"
											onClickTopics="beforeLoadServicesForm"
											onCompleteTopics="loadServicesFormComplete"
											onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
											<s:text name="jsp.servicecenter_transfer_inquiry" />
										</sj:a> </span>
								</td>
							</tr>
							<tr>
								<td align="left">&nbsp;</td>
							</tr>
						</ffi:cinclude>
						<tr>
							<td align="left" class=""><span> <s:url
										id="bankFeeInquiryURL"
										value="/pages/jsp/servicecenter/bank_fee_inquiry.jsp"
										escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
									</s:url> <sj:a id="bankFeeInquiryLink" href="%{bankFeeInquiryURL}"
										indicator="indicator" targets="inputDiv"
										onClickTopics="beforeLoadServicesForm"
										onCompleteTopics="loadServicesFormComplete"
										onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
										<s:text name="jsp.servicecenter_bank_fee_inquiry" />
									</sj:a> </span>
							</td>
						</tr>
						<tr>
							<td align="left">&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class=""><span> <s:url
										id="accountHistoryInquiryURL"
										value="/pages/jsp/servicecenter/account_history_inquiry.jsp"
										escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
									</s:url> <sj:a id="accountHistoryInquiryLink"
										href="%{accountHistoryInquiryURL}" indicator="indicator"
										targets="inputDiv" onClickTopics="beforeLoadServicesForm"
										onCompleteTopics="loadServicesFormComplete"
										onErrorTopics="errorLoadServicesForm" cssClass="anchorText">
										<s:text name="jsp.servicecenter_account_history_inquiry" />
									</sj:a> </span>
							</td>
						</tr>
					</table></td>
				<td align="left" width="2%">&nbsp;</td>
			</tr>
			<tr>
				<td align="left" colspan="6">&nbsp;</td>
			</tr>
		</table>
	</div>
</div>

<script>
	/* $('#onlineCustomerServicesTab').tabs();
	$('#onlineCustomerServicesTab').portlet(
			{
					helpCallback: function(){
					var helpFile = $('#onlineCustomerServicesTab').find('.moduleHelpClass').html();
					callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
				}
			}); */
</script>