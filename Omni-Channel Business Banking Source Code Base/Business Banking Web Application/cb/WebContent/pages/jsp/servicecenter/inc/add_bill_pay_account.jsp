<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="servicecenter_services_onlineCustomerServices_addAccounts" />
<ffi:cinclude value1="${NonBillPayAccounts}" value2="" operator="equals">
	<ffi:object id="GetNonBillPayAccounts"
		name="com.ffusion.tasks.billpay.GetNonBillPayAccounts" />
	<ffi:process name="GetNonBillPayAccounts" />
	<ffi:removeProperty name="GetNonBillPayAccounts" />
</ffi:cinclude>

<ffi:setProperty name="NonBillPayAccounts" property="Filter"
	value="HIDE=0,COREACCOUNT=1,AND" />


<%-- Retrieve the latest account summary values from banksim and update the accounts in session with those latest values --%>
<ffi:object id="GetAccountSummaries"
	name="com.ffusion.tasks.banking.GetSummariesForAccountDate"
	scope="session" />
<ffi:setProperty name="GetAccountSummaries"
	property="UpdateAccountBalances" value="true" />
<ffi:setProperty name="GetAccountSummaries" property="AccountsName"
	value="NonBillPayAccounts" />
<ffi:setProperty name="GetAccountSummaries" property="SummariesName"
	value="LatestSummaries" />
<ffi:process name="GetAccountSummaries" />
<ffi:removeProperty name="GetAccountSummaries" />
<ffi:removeProperty name="LatestSummaries" />

<ffi:cinclude value1="${AddBillPayAccount}" value2="" operator="equals">
	<ffi:object id="AddBillPayAccount"
		name="com.ffusion.tasks.messages.SendAddBillPayAccountMessage" />
</ffi:cinclude>

<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" />
<ffi:setProperty name="SetAccount" property="AccountsName"
	value="NonBillPayAccounts" />
<ffi:setProperty name="SetAccount" property="AccountName"
	value="SelectedAccount" />

<%
	session.setAttribute("FFISetAccount",
			session.getAttribute("SetAccount"));
	session.setAttribute("FFIAddBillPayAccount",
			session.getAttribute("AddBillPayAccount"));
%>

<span id="PageHeading" style="display:none;"><s:text name="jsp.servicecenter_add_bill_pay_to_an_account" /></span>
<span class="shortcutPathClass" style="display:none;">serviceCenter_services,addAccountsLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>
<s:form id="addBillPayAccountsForm" namespace="/pages/jsp/servicecenter"
	action="addBillPayAccountAction" validate="false" method="post"
	name="addBillPayAccountsForm" theme="simple">
	<input type="hidden" name="CSRF_TOKEN"
		value="<ffi:getProperty name='CSRF_TOKEN'/>" />
<table width="98%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr>
	<td>
	<%-- <div class="paneWrapper" >
	   	<div class="paneInnerWrapper">
			<div class="header">
				<s:text name="jsp.servicecenter_add_bill_pay_to_an_account" />
			</div>
			<div class="paneContentWrapper"> --%>
				<table width="35%" border="0" cellspacing="0" cellpadding="0"  align="center">
					<tr>
						<ffi:cinclude value1="${NonBillPayAccounts.Size}" value2="0"
							operator="equals">
							<td colspan="2" align="center"><ffi:getL10NString rsrcFile="cb"
									msgKey="jsp/servicecenter/add_bill_pay_account.jsp-1" />
							</td>
						</ffi:cinclude>
			
						<ffi:cinclude value1="${NonBillPayAccounts.Size}" value2="0"
							operator="notEquals">
							<td class="columndata" align="center" colspan="2">
								 <div class="selectBoxHolder">
									<select id="accountNumber"
										name="AccountNumber" class="txtbox">
											<option value="" align="center"><s:text name="jsp.default_375" /></option>
											<ffi:setProperty name="Compare" property="Value1"
												value="${AccountNumber}" />
											<ffi:list collection="NonBillPayAccounts" items="Account">
												<ffi:setProperty name="Compare" property="Value2"
													value="${Account.ID}" />
												<option align="left"
													value="<ffi:getProperty name="Account" property="ID" />"
													<ffi:getProperty name="selected${Compare.Equals}" />
													><ffi:getProperty name="Account" property="ConsumerMenuDisplayText" /></option>
											</ffi:list>
									</select>
								</div>
								<script type="text/javascript">
									$("#accountNumber").combobox({
										'size' : '50'
									});
								</script> 
							</td>
							</tr>
							<tr>
								<td colspan="2">&nbsp;&nbsp;<span id="AccountNumberError" class="marginLeft10"></span></td>
							</tr>
							<!-- <tr>
								<td colspan="2">&nbsp;</td>
							</tr> -->
							<tr>
							<td align="right" width="45%">
							<form name="CancelForm" method="post" action="<ffi:urlEncrypt url="${BackURL}" />" >
								<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							    <table align="right"  border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<% if(session.getAttribute("backLinkURL") != null) { %>
											<s:hidden id="backLinkURL" value="%{#session.backLinkURL}"/>
										<% } %>
										<sj:a button="true" onClickTopics="cancelServicesForm"><s:text name="jsp.default_82"/></sj:a>
									</td>
								</tr>
							    </table>
							</form>	
							</td>
							<td valign="top">
								<%-- <% if(session.getAttribute("backLinkURL") != null) { %>
									<s:hidden id="backLinkURL" value="%{#session.backLinkURL}"/>
								<% } %> --%>
								&nbsp;&nbsp;	
								<sj:a
									id="addBillPayAccountsFormSubmit" targets="resultmessage"
									formIds="addBillPayAccountsForm" button="true" validate="true" validateFunction="customValidation"
									onBeforeTopics="beforeConfirmAddBillPayAccountsForm"
									onCompleteTopics="confirmAddBillPayAccountsComplete"
									onErrorTopics="errorConfirmAddBillPayAccountsForm"
									onSuccessTopics="successConfirmAddBillPayAccountsForm"
									cssClass="">
									<s:text name="jsp.servicecenter_add_bill_pay" />
								</sj:a>&nbsp;
							</td>
						</ffi:cinclude>	
						
					</tr>
					<tr>
						<td align="left" colspan="2">&nbsp;</td>
					</tr>
				</table>
	<!-- </div>
	</div> 
	</div>-->
</td></tr>
</table>

</s:form>
