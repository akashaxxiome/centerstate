<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="${SecureUser.LocaleLanguage}" value2="en_US" operator="equals">
    <ffi:setProperty name="termsURL" value="/web/forms/Terms.pdf" />
    <ffi:setProperty name="signURL" value="/web/forms/Signature.pdf" />
    <ffi:setProperty name="privacyURL" value="/web/forms/PrivacyPolicy.pdf" />
    <ffi:setProperty name="externalXferURL" value="/web/forms/Manage-External-Enrollment.pdf" />
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.LocaleLanguage}" value2="en_US" operator="notEquals">
    <ffi:setProperty name="termsURL" value="/web/${UserLocale.LocaleName}/forms/Terms.pdf" />
    <ffi:setProperty name="signURL" value="/web/${UserLocale.LocaleName}/forms/Signature.pdf" />
    <ffi:setProperty name="privacyURL" value="/web/${UserLocale.LocaleName}/forms/PrivacyPolicy.pdf" />
    <ffi:setProperty name="externalXferURL" value="/web/${UserLocale.LocaleName}/forms/Manage-External-Enrollment.pdf" />
</ffi:cinclude>

<div id="documentCenterTab">
	<ffi:help id="servicecenter_services_documentCenter" />
	<%-- <ul class="portlet-header">
   	    <li>
	        <a id="documentCenterLink" href="#documentCenterDiv"><s:text name="jsp.servicecenter_document_center"/></a>
	    </li>
	</ul> --%>
	<div id="documentCenterContentDiv">
		<table border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td align="left" class="filter_table" > 
					<span class="txt_normal marginRight20" >
						<s:url id="termsURL" value="%{#session.termsURL}" escapeAmp="false" />
						<s:a href="#" onclick="ns.common.openDocumentCenter('%{termsURL}')" cssClass="anchorText"><s:text name="jsp.servicecenter_terms_and_conditions"/></s:a>
					</span>
				</td>
			    <td align="left" class="filter_table" > 
					<span class="txt_normal marginRight20" >
						<s:url id="signURL" value="%{#session.signURL}" escapeAmp="false" />
						<s:a href="#" onclick="ns.common.openDocumentCenter('%{signURL}')" cssClass="anchorText"><s:text name="jsp.servicecenter_signature"/></s:a>
					</span>
			    </td>
			    <td align="left" class="filter_table" > 
					<span class="txt_normal marginRight20" >
						<s:url id="privacyURL" value="%{#session.privacyURL}" escapeAmp="false" />
						<s:a href="#" onclick="ns.common.openDocumentCenter('%{privacyURL}')" cssClass="anchorText"><s:text name="jsp.servicecenter_privacy_policy"/></s:a>
					</span>
			    </td>
			    <td align="left" class="filter_table" >
					<span class="txt_normal" >
						<s:url id="externalXferURL" value="%{#session.externalXferURL}" escapeAmp="false" />
						<s:a href="#"  onclick="ns.common.openDocumentCenter('%{externalXferURL}')" cssClass="anchorText"><s:text name="jsp.servicecenter_external_transfer_account_enrollment"/></s:a>
					</span>
			    </td>
			</tr>
		</table>
	</div>
	<br/>
</div>


<ffi:removeProperty name="termsURL,signURL,privacyURL,externalXferURL" />

<script>    
	/* $('#documentCenterTab').tabs();
	$('#documentCenterTab').portlet({
		helpCallback: function(){
			var helpFile = $('#documentCenterTab').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
	}); */
</script>