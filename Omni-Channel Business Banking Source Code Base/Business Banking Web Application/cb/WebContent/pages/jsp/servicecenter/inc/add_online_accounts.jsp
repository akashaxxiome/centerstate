<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page
	import="com.ffusion.tasks.messages.SendAddOnlineBankingAccountMessage"%>

<ffi:help id="servicecenter_services_onlineCustomerServices_addAccounts" />
<ffi:setProperty name="topMenu" value="serviceCenter" />
<ffi:setProperty name="subMenu" value="services" />
<ffi:setProperty name="sub2Menu" value="" />
<ffi:setL10NProperty name="PageTitle" value="Service Center" />

<ffi:cinclude value1="${BackURLOld}" value2="" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${BackURLOld}" />
</ffi:cinclude>

<!--<ffi:cinclude value1="${AddOnlineAccounts}" value2="" operator="equals" >-->
<ffi:object id="AddOnlineAccounts"
	name="com.ffusion.tasks.messages.SendAddOnlineBankingAccountMessage" />
<!--</ffi:cinclude>-->

<ffi:cinclude value1="${AccountTypesList}" value2="" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.ResourceList"
		id="AccountTypesList" scope="session" />
	<ffi:setProperty name="AccountTypesList" property="ResourceFilename"
		value="com.ffusion.beansresources.accounts.resources" />
	<ffi:setProperty name="AccountTypesList" property="ResourceID"
		value="AccountTypesIDList" />
	<ffi:process name="AccountTypesList" />
	<ffi:getProperty name="AccountTypesList" property="Resource" />
</ffi:cinclude>

<ffi:cinclude value1="${AccountTypes}" value2="" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.Resource" id="AccountTypes"
		scope="session" />
	<ffi:setProperty name="AccountTypes" property="ResourceFilename"
		value="com.ffusion.beansresources.accounts.resources" />
	<ffi:process name="AccountTypes" />
</ffi:cinclude>

<%
session.setAttribute("FFIAddOnlineAccounts", session.getAttribute("AddOnlineAccounts"));
%>

<%
    int numAccounts = 3;
    SendAddOnlineBankingAccountMessage addOnlineAccounts = (SendAddOnlineBankingAccountMessage) session.getAttribute ( "AddOnlineAccounts" );
    String lastSelected = null;

    if ( addOnlineAccounts != null ) {
	numAccounts = addOnlineAccounts.getNumberOfAccounts();
    }
%>

<span id="PageHeading" style="display:none;"><s:text name="jsp.servicecenter_add_online_accounts" /></span>

<ul id="formerrors"></ul>
<s:form id="addOnlineForm" namespace="/pages/jsp/servicecenter"
	action="addOnlineAccountsAction" validate="false" method="post"
	name="FormName" theme="simple">
	<input type="hidden" name="CSRF_TOKEN"
		value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		
<table width="80%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr>
	<td>
	<div class="paneWrapper" >
	   	<div class="paneInnerWrapper">
			<div class="header">
				<s:text name="jsp.service.center.account.information" />
			</div>
			<div class="paneContentWrapper">	
			   	 <table width="100%" cellpadding="0" cellspacing="0" border="0" class="formTable tableData" align="center">
					<tr>
						<td colspan="2" class="required" align="center"><s:text name="jsp.service.center.account.not.currently.viewable.msg"/></td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<% for ( int i = 1; i <= numAccounts; i++ ) { %>
					<ffi:setProperty name="index" value="<%= String.valueOf ( i ) %>" />
					<ffi:getProperty name="${AddOnlineAccounts.AccountTypePrefix}${index}" assignTo="lastSelected" />
					<tr>
						<ffi:cinclude value1="${firstDisplayed}" value2="true"
							operator="equals">
							<td width="50%">
								<span class="sectionsubhead"><s:text name="jsp.default_20" /><s:text name="jsp.default_colon" /></span></td>
						</ffi:cinclude>
						<ffi:cinclude value1="${firstDisplayed}" value2="true" operator="notEquals">
							<td width="50%">
								<span class="sectionsubhead"><s:text name="jsp.default_20" /><s:text name="jsp.default_colon" /></span><span class="required">*</span>
							</td>
						</ffi:cinclude>
						
						<ffi:cinclude value1="${firstDisplayed}" value2="true"
							operator="equals">
							<td width="50%">
								<span class="sectionsubhead"><s:text name="jsp.default_19" /><s:text name="jsp.default_colon" /> </span>
							</td>
						</ffi:cinclude>
						<ffi:cinclude value1="${firstDisplayed}" value2="true" operator="notEquals">
							<td width="50%">
							<span class="sectionsubhead"><s:text name="jsp.default_19" /><s:text name="jsp.default_colon" /> </span><span class="required">*</span>
							</td>
						</ffi:cinclude>
					</tr>
					<tr>
						<td>
							<select id="accountType-<ffi:getProperty name="index" />"
								name="<ffi:getProperty name="AddOnlineAccounts" property="AccountTypePrefix" /><ffi:getProperty name="index" />"
								class="txtbox">
									<option value="-1" align="center"><s:text name="jsp.default_375"/></option>
									<ffi:list collection="AccountTypesList" items="type">
										<ffi:setProperty name="AccountTypes" property="ResourceID"
											value="AccountType${type}" />
										<option value='<ffi:getProperty name="type" />'
											<ffi:cinclude value1="${lastSelected}" value2="${type}" operator="equals">selected
											</ffi:cinclude>><ffi:getProperty name="AccountTypes" property="Resource" /></option>
									</ffi:list>
							</select>
							<script type="text/javascript">
								$("#accountType-<ffi:getProperty name="index" />").selectmenu({width: 250});
							</script>
							
						<td>
							<input id="accountNumber-<ffi:getProperty name="index" />"
							name="<ffi:getProperty name="AddOnlineAccounts" property="AccountNumberPrefix" /><ffi:getProperty name="index" />"
							type="text" size="25" maxlength="40"
							class="ui-widget-content ui-corner-all txtbox"
							value='<ffi:getProperty name="${AddOnlineAccounts.AccountNumberPrefix}${index}" />'>
							
						</td>
					</tr>
					<tr>
						<td><span id="<ffi:getProperty name="AddOnlineAccounts" property="AccountTypePrefix" /><ffi:getProperty name="index" />Error"></span></td>
						<td><span id="<ffi:getProperty name="AddOnlineAccounts" property="AccountNumberPrefix" /><ffi:getProperty name="index" />Error"></span></td>
					</tr>
					<ffi:setProperty name="firstDisplayed" value="true" />
					<% } %>
					<ffi:removeProperty name="index" />
					<ffi:removeProperty name="firstDisplayed" />
					
					<tr>
						<td><span class="sectionsubhead"><s:text name="jsp.servicecenter_contact_phone_number" /><s:text name="jsp.default_colon" /></span></td>
						<td><span class="sectionsubhead"><s:text name="jsp.servicecenter_comments_or_questions" /><s:text name="jsp.default_colon" /></span></td>
					</tr>
					<tr>
						<td valign="top">
							<input name="ContactPhoneNumber" class="ui-widget-content ui-corner-all txtbox" type="text" size="25"
							maxlength="40" value='<ffi:getProperty name="AddOnlineAccounts" property="ContactPhoneNumber"/>'>
						</td>
						<td>
							<textarea name="Question" type="text" cols="40" rows="5" maxlength="255" class="ui-widget-content ui-corner-all txtbox" wrap="virtual"><ffi:getProperty name="AddOnlineAccounts" property="Question" /></textarea>
						</td>
					</tr>
					<tr>
			           <td colspan="2">&nbsp;</td>
			       </tr>
					<tr>
			           <td align="center" class="required" colspan="2">* <s:text name="jsp.default_240"/></td>
			       </tr>
					<tr>
						<td align="right">
							<form name="CancelForm" method="post" action="<ffi:urlEncrypt url="${BackURL}" />" >
								<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							    <table width="" border="0" cellspacing="0" cellpadding="0" align="right">
								<tr>
									<td>
										<% if(session.getAttribute("backLinkURL") != null) { %>
											<s:hidden id="backLinkURL" value="%{#session.backLinkURL}"/>
										<% } %>
										<sj:a button="true" onClickTopics="cancelServicesForm"><s:text name="jsp.default_82"/></sj:a>
									</td>
								</tr>
							    </table>
							</form>		
						</td>
						<td align="left">
							<% if(session.getAttribute("backLinkURL") != null) { %>
								<s:hidden id="backLinkURL" value="%{#session.backLinkURL}"/>
							<% } %>
							<sj:a
								id="addOnlineFormSubmit" targets="resultmessage" button="true"
								formIds="addOnlineForm" validate="true" validateFunction="customValidation"
								onBeforeTopics="beforeConfirmAddOnlineAccountsForm"
								onCompleteTopics="confirmAddOnlineAccountsComplete"
								onErrorTopics="errorConfirmAddOnlineAccountsForm"
								onSuccessTopics="successConfirmAddOnlineAccountsForm">
								<s:text name="jsp.servicecenter_add_accounts" />
							</sj:a>	
						</td>
						
					</tr>
					<tr>
						
					</tr>
				</table>		
			</div>
		</div>
	</div>
</td></tr>
</table>
</s:form>
