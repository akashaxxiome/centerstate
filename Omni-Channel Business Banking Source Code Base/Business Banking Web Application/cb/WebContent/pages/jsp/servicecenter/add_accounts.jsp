<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<ffi:help id="servicecenter_services_onlineCustomerServices_addAccounts" />
<ffi:setProperty name="topMenu" value="serviceCenter"/>
<ffi:setProperty name="subMenu" value="services"/>
<ffi:setProperty name="sub2Menu" value=""/>
<ffi:setL10NProperty name="PageTitle" value="Service Center"/>


<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

<ffi:cinclude value1="${BackURLOld}" value2="" operator="notEquals" >
    <ffi:setProperty name="BackURL" value="${BackURLOld}" />
</ffi:cinclude>


<span id="PageHeading" style="display:none;"><s:text name="jsp.servicecenter_add_bill_pay_and_online_accounts" /></span>
<span class="shortcutPathClass" style="display:none;">serviceCenter_services,addAccountsLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>
    <div>
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="30%" valign="top">
					<div class="paneWrapper marginTop20">
					   	<div class="paneInnerWrapper">
							<div class="header">
								<s:text name="jsp.servicecenter_add_bill_pay_to_an_account" />
							</div>
							<div class="paneContentWrapper">
								<s:include value="/pages/jsp/servicecenter/inc/add_bill_pay_account.jsp"/>
							</div>
						</div>
					</div>
				</td>
				<td width="3%">&nbsp;</td>
				<td align="left" class="adminBackground" width="67%" valign="top">
					<s:include value="/pages/jsp/servicecenter/inc/add_online_accounts.jsp"/>
				</td>
			</tr>
		 </table>	
							

			<form name="CancelForm" method="post" action="<ffi:urlEncrypt url="${BackURL}" />" >
		                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			    <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
				    <td width="33%">&nbsp;</td>
				    <td width="67%" align="left" class="txt_normal" width="100%" >&nbsp;</td>
				</tr>
		        <tr>
		        	<td>&nbsp;</td>
		            <td align="center" class="required">* <s:text name="jsp.default_240"/></td>
		        </tr>
				<tr>
					<td>&nbsp;</td>
				    <td align="left" class="txt_normal" width="100%" >&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td align="center">
						<% if(session.getAttribute("backLinkURL") != null) { %>
							<s:hidden id="backLinkURL" value="%{#session.backLinkURL}"/>
						<% } %>
						<sj:a button="true" onClickTopics="cancelServicesForm"><s:text name="jsp.default_82"/></sj:a>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				    <td align="left" class="txt_normal" width="100%" >&nbsp;</td>
				</tr>
			    </table>
			</form>
	</div>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
<ffi:removeProperty name="backLinkURL"/>