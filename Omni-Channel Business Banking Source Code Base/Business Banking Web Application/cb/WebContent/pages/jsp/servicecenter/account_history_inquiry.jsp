<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:author page="servicecenter/account_history_inquiry.jsp"/>
<ffi:help id="servicecenter_services_onlineCustomerServices_acctHistoryInq" />
<%-- clean up --%>
<ffi:removeProperty name="AccountHistoryInquiry" />

<ffi:setProperty name="topMenu" value="serviceCenter"/>
<ffi:setProperty name="subMenu" value="services"/>
<ffi:setProperty name="sub2Menu" value=""/>
<ffi:setL10NProperty name="PageTitle" value="Service Center"/>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

<%-- Retrieve the latest account summary values from banksim and update the accounts in session with those latest values --%>
<ffi:object id="GetAccountSummaries" name="com.ffusion.tasks.banking.GetSummariesForAccountDate" scope="session"/>
<ffi:setProperty name="GetAccountSummaries" property="UpdateAccountBalances" value="true"/>
<ffi:setProperty name="GetAccountSummaries" property="SummariesName" value="LatestSummaries"/>
<ffi:process name="GetAccountSummaries"/>
<ffi:removeProperty name="GetAccountSummaries"/>
<ffi:removeProperty name="LatestSummaries"/>

<ffi:cinclude value1="${BackURLOld}" value2="" operator="notEquals" >
    <ffi:setProperty name="BackURL" value="${BackURLOld}" />
</ffi:cinclude>

<ffi:cinclude value1="${AccountHistoryInquiry}" value2="" operator="equals" >
    <ffi:object id="AccountHistoryInquiry" name="com.ffusion.tasks.messages.SendAccountHistoryInquiryMessage" />
    <ffi:setProperty name="AccountHistoryInquiry" property="DateFormat" value="${UserLocale.DateFormat}" />
    <ffi:setProperty name="AccountHistoryInquiry" property="Date" value="${UserLocale.DateFormat}" />

    <% String question = null; %>
    <ffi:getL10NString rsrcFile="cb" msgKey="jsp/servicecenter/inquiry.jsp-1" assignTo="question" />
    <ffi:setProperty name="InquiryMessage" value="<%= question %>" />
</ffi:cinclude>

<ffi:cinclude value1="${TransactionTypesList}" value2="" operator="equals" >
    <ffi:object id="GetTransactionTypes" name="com.ffusion.tasks.util.GetTransactionTypes" />
    <ffi:setProperty name="GetTransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CONSUMER %>" />
    <ffi:setProperty name="GetTransactionTypes" property="AddListToSession" value="true" />
    <ffi:process name="GetTransactionTypes" />
    <ffi:removeProperty name="GetTransactionTypes" />
</ffi:cinclude>

<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" />
<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts" />
<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount" />

<%
session.setAttribute("FFISetAccount", session.getAttribute("SetAccount"));
session.setAttribute("FFIAccountHistoryInquiry", session.getAttribute("AccountHistoryInquiry"));
%>

<span id="PageHeading" style="display: none;"><s:text
		name="jsp.servicecenter_account_history_inquiry" /></span>
<span class="shortcutPathClass" style="display:none;">serviceCenter_services,accountHistoryInquiryLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>
<div>
<ul id="formerrors"></ul>
<s:form id="accountHistoryInquiryForm" namespace="/pages/jsp/servicecenter" validate="true" action="accountHistoryInquiryAction" method="post" name="accountHistoryInquiryForm" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<div style="padding-left:10px;padding-right:10px;">
	    <table width="80%" border="0" cellspacing="0" cellpadding="3" align="center">
	    <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND" />
		<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="equals">
			<tr>
				<td colspan="2" class="sectionsubhead ltrow2_color" align="center">
				    <ffi:getL10NString rsrcFile="cb" msgKey="jsp/servicecenter/account_history_inquiry.jsp-1" />
				</td>
			</tr>
		</ffi:cinclude>
		<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" >
					<span class="sectionsubhead" ><s:text name="jsp.default_439" /> <s:text name="jsp.default_colon" /></span><span class="required" >*</span>
			    </td>
			    <td align="left" class="columndata ltrow2_color" width="75%" >
					<div class="selectBoxHolder">
						<select class="txtbox" id="transactionType" name="TransactionType" >
						    <option value="-1"><s:text name="jsp.default_375" /></option>
						    <ffi:setProperty name="Compare" property="Value1" value="${AccountHistoryInquiry.TransactionType}" />
						    <ffi:list collection="TransactionTypesList" items="transaction">
							<ffi:setProperty name="Compare" property="Value2" value="${transaction.Key}" />
							<option value="<ffi:getProperty name="transaction" property="Key"/>" <ffi:getProperty name="selected${Compare.Equals}"/>
							><ffi:getProperty name="transaction" property="Value"/></option>
						    </ffi:list>
						</select>
					</div>
				<script type="text/javascript">
					$("#transactionType").selectmenu({width: 150});
				</script>
				<br><br><span id="TransactionTypeError"></span>
			    </td>
			</tr>
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" >
				<span class="sectionsubhead" ><s:text name="jsp.default_171" /></span>
			    </td>
			    <td align="left" width="75%" class="columndata ltrow2_color" >
				<input name="Description" type="text" size="30" maxlength="40" border="0" class="txtbox ui-widget-content ui-corner-all" value='<ffi:getProperty name="AccountHistoryInquiry" property="Description"/>'>
			    <br><span id="DescriptionError"></span>
			    </td>
			</tr>
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" >
				<span class="sectionsubhead" ><s:text name="jsp.default_21" /></span><span class="required" >*</span>
			    </td>
			    <td align="left" class="columndata ltrow2_color" width="75%" >
				<div class="selectBoxHolder">
					<select class="txtbox" id="accountID" name="AccountNumber" >
					    <option value=""><s:text name="jsp.default_375" /></option>
					    <ffi:setProperty name="Compare" property="Value1" value="${AccountHistoryInquiry.AccountNumber}" />
					    <ffi:list collection="BankingAccounts" items="BAccount">
						<ffi:setProperty name="Compare" property="Value2" value="${BAccount.ID}" />
						<option value="<ffi:getProperty name="BAccount" property="ID"/>" <ffi:getProperty name="selected${Compare.Equals}"/>
						><ffi:getProperty name="BAccount" property="ConsumerMenuDisplayText"/></option>
					    </ffi:list>
					</select>
				</div>
				<script type="text/javascript">
					$("#accountID").combobox({'size':'30'});
				</script>
				<br><br><span id="AccountNumberError"></span>
			    </td>
			</tr>
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" ><span class="sectionsubhead" ><s:text name="jsp.default_143" /></span><span class="required" >*</span></td>
			    <td align="left" width="75%" class="columndata ltrow2_color" >
			    	<sj:datepicker
						id="Date"
						name="Date"
						maxlength="10"
						buttonImageOnly="true"
						cssClass="ui-widget-content ui-corner-all" 
						cssStyle="width:127px;"/>
					<br><span id="DateError"></span>
					<script>
						var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=accountHistoryInquiryForm&calTarget=AccountHistoryInquiry.Date"/>';
	                    ns.common.enableAjaxDatepicker("PayDate", tmpUrl);
					</script>
			    </td>
			</tr>
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" ><span class="sectionsubhead" ><s:text name="jsp.default_45" /></span><span class="required" >*</span></td>
			    <td align="left" width="75%" class="columndata ltrow2_color" >
				<input name="Amount" type="text" size="15" maxlength="25" border="0" class="txtbox ui-widget-content ui-corner-all" value='<ffi:getProperty name="AccountHistoryInquiry" property="Amount"/>'>
				<br><span id="AmountError"></span>
			    </td>
			</tr>
			<tr>
			    <td align="right" class="sectionsubhead ltrow2_color" width="25%" >
				<span class="sectionsubhead" ><s:text name="jsp.default_347" /><s:text name="jsp.default_colon" /></span>
			    </td>
			    <td align="left" width="75%" class="columndata ltrow2_color" >
				<input name="ReferenceNumber" type="text" size="25" maxlength="25" border="0" class="txtbox ui-widget-content ui-corner-all" value='<ffi:getProperty name="AccountHistoryInquiry" property="ReferenceNumber"/>'>
			    <br><span id="ReferenceNumberError"></span>
			    </td>
			</tr>
			<tr>
				<td align="right" class="sectionsubhead ltrow2_color" style="vertical-align:top;">
					<ffi:getProperty name="InquiryMessage" /><span class="required" >*</span>
				</td> 
			    <td width="75%" class="columndata ltrow2_color">
				<textarea name="Question" id="accHistoryMemo" type="text" cols="50" rows="5" align="left" class="txtbox ui-widget-content ui-corner-all" wrap="virtual" ><ffi:getProperty name="AccountHistoryInquiry" property="Question" /></textarea>
				<br><br><span id="QuestionError"></span>
			    </td>
			</tr>
		</ffi:cinclude>
		<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
	        <tr>
	            <td align="center" class="required" colspan="2">* <s:text
							name="jsp.default_240" /></td>
	        </tr>
        </ffi:cinclude>
		<tr>
		    <td align="left" class="sectionsubhead ltrow2_color" colspan="2" >&nbsp;</td>
		</tr>
		<tr>
		    <td colspan="2" align="center" class="columndata ltrow2_color" >
		    	<sj:a button="true" onClickTopics="cancelServicesForm"><s:text name="jsp.default_82" /></sj:a>
		    	<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
			    	<sj:a id="accountHistoryInquiryFormSubmit"
						formIds="accountHistoryInquiryForm"
						targets="resultmessage" button="true" validate="true"
						validateFunction="customValidation"
						onBeforeTopics="beforeConfirmAccountHistoryInquiryForm"
						onCompleteTopics="confirmAccountHistoryInquiryComplete"
						onErrorTopics="errorConfirmAccountHistoryInquiryForm"
						onSuccessTopics="successConfirmAccountHistoryInquiryForm"
						><s:text name="jsp.default_378" /></sj:a>
				</ffi:cinclude>
		    </td>
		</tr>
	    </table>
	   </div>
	</s:form>
</div>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
<script>
	$(document).ready(function() {
		setTimeout(function(){ns.common.renderRichTextEditor('accHistoryMemo','550');}, 500);
	});
</script>