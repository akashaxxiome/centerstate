<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:author page="messages-create.jsp"/>
<ffi:help id="servicecenter_services_onlineCustomerServices_generalQuestions"/>

<span id="PageHeading" style="display: none;"><s:text name="jsp.servicecenter_general_comments_or_questions"/> </span>
<span class="shortcutPathClass" style="display: none;">serviceCenter_services,generalQuestionsLink</span>
<span class="shortcutEntitlementClass" style="display: none;">NO_ENTITLEMENT_CHECK</span>

<div align="left">
	<ul id="formerrors"></ul>

	<s:form id="generalQuestionsForm" namespace="/pages/jsp/message" validate="false" action="SendMessageAction" method="post" name="generalQuestionsForm" theme="simple">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

		<div style="padding-left:10px;padding-right:10px;">
			<table width="90%" border="0" cellspacing="0" cellpadding="3" class="greenClayBackground" align="center">
				<tr>
					<td colspan="3"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/messages-create.jsp-1" parm0="${ProductName}"/></td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td align="right"  width="20%">
						<span class="sectionsubhead ltrow2_color"><s:text name="jsp.servicecenter_topic"/></span><span class="required">*</span>
					</td>
					<td width="45%">
						<select id="new_message_topics" class="ui-widget-content ui-corner-all"  name="messageModel.to" size="1">
							<ffi:list collection="MessageQueues" items="Queue1">
								<option value="<ffi:getProperty name="Queue1" property="QueueID"/>" <ffi:cinclude value1="${MessageModel.To}" value2="${Queue1.QueueID}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="Queue1" property="QueueName"/></option>
							</ffi:list>
						</select><span id="toError"></span>
						<script>
							$('#new_message_topics').selectmenu({width:300});
						</script>
						<s:hidden name="messageModel.from" value="%{#session.User.id}"></s:hidden>
					</td>
					<td>
						<span><s:text name="jsp.default_137"/></span>:&nbsp;
						<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateFormat}"/>
						<ffi:getProperty name="GetCurrentDate" property="Date"/>
					</td>
				</tr>
				<tr>
					<td align="right" class="sectionsubhead ltrow2_color">
						<span><s:text name="jsp.default_394"/></span><span class="required">*</span>
					</td>
					<td colspan="2">
						<s:textfield cssClass="txtbox ui-widget-content ui-corner-all" id="subject" name="messageModel.subject" size="70" maxlength="100"></s:textfield>
							<br/><span id="subjectError"></span>
					</td>
				</tr>
				<tr>
					<td align="right" valign="top" class="sectionsubhead ltrow2_color">
						<span><s:text name="jsp.servicecenter_message"/></span><span class="required">*</span>
					</td>
					<td colspan="2">
						<s:textarea  cssClass="txtbox ui-widget-content ui-corner-all" id="sendMemo" name="messageModel.memo" rows="9" cols="70" wrap="physical"></s:textarea>
						<br/><span id="memoError"></span>
					</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<span class="required">*<s:text name="jsp.default_240"/> </span>
					</td>
				</tr>
				<tr>
					<td colspan="3" align="center">
						<sj:a button="true" id="clearMessageBoxButton" onClickTopics="services.clearMessageBoxArea"><s:text name="jsp.servicecenter_clear"/></sj:a>
						<sj:a button="true" onClickTopics="cancelServicesForm"><s:text name="jsp.default_82"/></sj:a>

						<sj:a id="generalQuestionsFormSubmit"
							  formIds="generalQuestionsForm" targets="resultmessage"
							  button="true" validate="true" validateFunction="customValidation"
							  onBeforeTopics="beforeConfirmGeneralQuestionsForm"
							  onCompleteTopics="confirmGeneralQuestionsComplete"
							  onErrorTopics="errorConfirmGeneralQuestionsForm"
							  onSuccessTopics="successConfirmGeneralQuestionsForm">
							<s:text name="jsp.default_378"/>
						</sj:a>
					</td>
				</tr>
			</table>
		</div>
	</s:form>
</div>
<script>
	$(document).ready(function() {
		setTimeout(function(){ns.common.renderRichTextEditor('sendMemo','600');}, 500);
	});
</script>