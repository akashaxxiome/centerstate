<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="messageKey" value="jsp/servicecenter/bill_payment_inquiry_done.jsp-1" />
<s:include value="/pages/jsp/servicecenter/done.jsp"/>


<ffi:removeProperty name="BillPayInquiry" />
<ffi:removeProperty name="BillPayInquiryPayee" />
<ffi:removeProperty name="BillPayInquiryAccount" />
<ffi:removeProperty name="Payees"/>
<ffi:removeProperty name="BackURLOld" />
<ffi:removeProperty name="InquiryMessage" />
