<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<ffi:author page="servicecenter/order_checks.jsp" />
<ffi:help id="servicecenter_services_onlineCustomerServices_orderChecks" />

<ffi:setProperty name="topMenu" value="serviceCenter" />
<ffi:setProperty name="subMenu" value="services" />
<ffi:setProperty name="sub2Menu" value="" />
<ffi:setL10NProperty name="PageTitle" value="Service Center" />

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

<ffi:cinclude value1="${BackURLOld}" value2="" operator="notEquals">
	<ffi:setProperty name="BackURL" value="${BackURLOld}" />
</ffi:cinclude>

<ffi:object id="BankIdentifierDisplayTextTask"
	name="com.ffusion.tasks.affiliatebank.GetBankIdentifierDisplayText"
	scope="session" />
<ffi:process name="BankIdentifierDisplayTextTask" />
<ffi:setProperty name="TempBankIdentifierDisplayText"
	value="${BankIdentifierDisplayTextTask.BankIdentifierDisplayText}" />
<ffi:removeProperty name="BankIdentifierDisplayTextTask" />

<!--<ffi:setProperty name="orderChecksURLPage" value="http://www.clarkeamerican.com" />-->
<span id="PageHeading" style="display: none;"><s:text
		name="jsp.servicecenter_order_checks" /> </span>
<span class="shortcutPathClass" style="display:none;">serviceCenter_services,orderChecksLink</span>
<span class="shortcutEntitlementClass" style="display:none;">NO_ENTITLEMENT_CHECK</span>
<div align="center">
	<form name="orderChecksForm" method="post">
		<input type="hidden" name="CSRF_TOKEN"
			value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<%-- grab only checking and line of credit accounts --%>
			<ffi:setProperty name="BankingAccounts" property="Filter"
				value="HIDE=0,COREACCOUNT=1,AND" />
			<ffi:setProperty name="BankingAccounts" property="FilterOnFilter"
				value="TYPE=1,TYPE=7,OR" />
			<ffi:cinclude value1="0" value2="${BankingAccounts.Size}"
				operator="equals">
				<tr>
					<td colspan="3" class="sectionsubhead ltrow2_color" align="center">
						<ffi:getL10NString rsrcFile="cb"
							msgKey="jsp/servicecenter/order_checks.jsp-1" />
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude value1="0" value2="${BankingAccounts.Size}"
				operator="notEquals">
				<tr>
					<td class="instructions" colspan="2"><ffi:getL10NString
							rsrcFile="cb" msgKey="jsp/servicecenter/order_checks.jsp-2" />
					</td>
				</tr>
				<tr>
					<td class="instructions" colspan="2"><ffi:getL10NString
							rsrcFile="cb" msgKey="jsp/servicecenter/order_checks.jsp-3" />
					</td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead ltrow2_color" colspan="2">&nbsp;</td>
				</tr>
				<tr valign="middle">
					<td colspan="2" align="center">
						<div class="paneWrapper">
						   	<div class="paneInnerWrapper">
								<div class="header">
									<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
										<tr class="header" align="left">
											<td width="30%"><s:text name="jsp.default_18" />
											</td>
											<td width="30%"><ffi:getProperty
													name="TempBankIdentifierDisplayText" />
											</td>
											<td width="30%"><s:text name="jsp.default_19" />
											</td>
										</tr>
									</table>
								</div>
								<div class="paneContentWrapper">
									<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
										<ffi:list collection="BankingAccounts" items="Account">
											<tr align="left">
												<td width="30%" class="filter_table"><span
													class="sectionsubhead ltrow2_color"><ffi:getProperty
															name="Account" property="NickName" /> </span>
												</td>
												<td width="30%" class="filter_table"><span
													class="sectionsubhead ltrow2_color"><ffi:getProperty
															name="Account" property="RoutingNum" /> </span>
												</td>
												<td width="30%" class="filter_table"><span
													class="sectionsubhead ltrow2_color"><ffi:getProperty
															name="Account" property="Number" /> </span>
												</td>
											</tr>
										</ffi:list>
									</table>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</ffi:cinclude>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr valign="middle">
				<td colspan="2" align="center" class="filter_table"><sj:a
						button="true" onClickTopics="cancelServicesForm">
						<s:text name="jsp.default_82" />
					</sj:a> <ffi:cinclude value1="0" value2="${BankingAccounts.Size}"
						operator="notEquals">
						<sj:a button="true"
							onClick="window.open('http://www.clarkeamerican.com');">
							<s:text name="jsp.servicecenter_go" />
						</sj:a>
					</ffi:cinclude>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
	</form>
</div>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
<!--<ffi:removeProperty name="orderChecksURLPage" />-->

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
