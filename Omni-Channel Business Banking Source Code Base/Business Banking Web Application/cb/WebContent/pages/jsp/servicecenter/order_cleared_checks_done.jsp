<%@ page import="com.ffusion.beans.ach.ACHPayee"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="parameter0" value="${SelectedAccount.ConsumerDisplayText}" />
<ffi:setProperty name="messageKey" value="jsp/servicecenter/order_cleared_checks_done.jsp-1" />
<s:include value="/pages/jsp/servicecenter/done.jsp"/>


<ffi:removeProperty name="${OrderClearedChecks.CheckNumberPrefix}" startsWith="true" />
<ffi:removeProperty name="OrderClearedChecks" />
<ffi:removeProperty name="SelectedAccount" />
<ffi:removeProperty name="AccountForClearedCheck" />
