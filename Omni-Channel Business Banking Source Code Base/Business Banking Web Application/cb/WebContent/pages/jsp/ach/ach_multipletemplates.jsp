<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_ACHMultipleTemplatesSummary" className="moduleHelpClass"/>

	<ffi:setGridURL grid="GRID_multiACHTemplates" name="LoadURL" url="/cb/pages/jsp/ach/loadMultiACHTemplateAction_initLoad.action?loadMultipleBatch=true&TemplateID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_multiACHTemplates" name="ViewURL" url="/cb/pages/jsp/ach/viewMultiACHTemplateAction_initView.action?TemplateID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_multiACHTemplates" name="EditURL" url="/cb/pages/jsp/ach/editMultiACHTemplateAction_init.action?Initialize=true&TemplateID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_multiACHTemplates" name="DeleteURL" url="/cb/pages/jsp/ach/deleteMultiACHTemplateAction_init.action?TemplateID={0}" parm0="ID"/>
<%--<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getPagedACHMultiTemplatesAction.action?PagedACHTaskName=GetMultipleACHBatchTemplates&GridURLs=GRID_multiACHTemplates" URLEncrypt="true"/> --%>
	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getACHMultiTemplatesAction.action?multipleACHBatchTemplateSearchCriteria.achType=${ACHType}&GridURLs=GRID_multiACHTemplates" URLEncrypt="true"/>
    <s:url id="multipleACHTemplatesUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
			id="multipleACHTemplatesGridID"
			caption=""
			sortable="true"
			dataType="local"
			href="%{multipleACHTemplatesUrl}"
			pager="true"
			gridModel="gridModel"
			rowList="%{#session.StdGridRowList}"
			rowNum="%{#session.StdGridRowNum}"
			rownumbers="false"
			sortname="templateName"
			shrinkToFit="true"
			navigator="true"
			navigatorAdd="false"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			scroll="false"
			scrollrows="true"
			viewrecords="true"
			onGridCompleteTopics="addGridControlsEvents,multipleACHTemplatesGridCompleteEvents"
			>

        <sjg:gridColumn name="templateName" width="150" index="templateName" title="%{getText('ach.grid.templateName')}" sortable="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="batchScope" width="100" index="batchScope" title="%{getText('ach.grid.batchScope')}" sortable="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="batchCount" width="50"  index="batchCount" title="%{getText('ach.grid.batchCount')}" sortable="true" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="batchCreditSumCurrency" width="120"  index="batchCreditSumCurrency" title="%{getText('ach.grid.batchCreditTotal')}" sortable="true" formatter="ns.ach.formatCreditAmountColumn" formatoptions="{suffix:' USD'}" cssClass="datagrid_amountColumn"/>
        <sjg:gridColumn name="batchDebitSumCurrency" width="120" index="batchDebitSumCurrency" title="%{getText('ach.grid.batchDebitTotal')}" sortable="true" formatter="ns.ach.formatDebitAmountColumn" formatoptions="{suffix:' USD'}" cssClass="datagrid_amountColumn"/>
        <sjg:gridColumn name="batchStatusDisplay" width="120" index="status" title="Status" sortable="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.ach.formatMultipleACHTemplatesActionLinks" width="90" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>

        <sjg:gridColumn name="resultOfApproval" width="100"  index="approval" title="Approval" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="approverName" width="100"  index="approverName" title="approverName" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="userName" width="100"  index="userName" title="userName" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="rejectReason" width="100"  index="rejectReason" title="rejectReason" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="approverIsGroup" width="100"  index="approverIsGroup" title="approverIsGroup" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="batchStatus" width="120" index="status" title="Status" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="approverInfos" index="approverInfos" title="" hidden="true" hidedlg="true" formatter="ns.ach.formatApproversInfo" />
	</sjg:grid>

	<script type="text/javascript">
	<!--
		$("#multipleACHTemplatesGridID").jqGrid('setColProp','ID',{title:false});
			/*  <ffi:cinclude value1="${subMenuSelected}" value2="child" operator="equals">
			  $("#quickSearchLink").hide();
			 </ffi:cinclude>

			  <ffi:cinclude value1="${subMenuSelected}" value2="tax" operator="equals">
			  $("#quickSearchLink").hide();
			 </ffi:cinclude>
			 <ffi:cinclude value1="${subMenuSelected}" value2="ach" operator="equals">
			  $("#quickSearchLink").show();
			 </ffi:cinclude> */
        //$("#templatessearchcriteria").hide();
        //$("#multitemplatessearchcriteria").show();
	//-->
	</script>

