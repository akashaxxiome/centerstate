<%@ page import="com.ffusion.beans.ach.TaxForm,
                 java.util.ArrayList"%>
<%@ page import="java.util.HashMap" %>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_taxChildspTypeadd" className="moduleHelpClass"/>
<% String subMenu = "tax"; %>

<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
	<% subMenu = "tax";	%>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_childsp,addChildspPayeeLink</span>
	<span class="shortcutEntitlementClass" style="display: none;">CCD + DED</span>
	<% subMenu = "child";	%>
</ffi:cinclude>
<% if (subMenu.equals("tax")) { %>
	<span style="display:none;" id="PageHeading"><s:text name="Add Tax Form"/></span>

    <s:action namespace="/pages/jsp/ach" name="addChildTaxTypeAction_init_TaxPayment" />

<% } else { %>
	<span style="display:none;" id="PageHeading"><s:text name="Add Child Support Form"/></span>

    <s:action namespace="/pages/jsp/ach" name="addChildTaxTypeAction_init_ChildSupport" />
<% } %>

<%  String bus_or_pers = (String)request.getAttribute("BusinessORPersonalFlag");
    String businessSelected = "";
    String personalSelected = "";

    if (bus_or_pers != null && bus_or_pers.equalsIgnoreCase("Business"))
    {
        businessSelected = " selected ";
    } else {
        personalSelected = " selected ";
    }
%>


<% HashMap stateNameHashMap = new HashMap();
    // order of this HashMap is key=state name localized, value=state name abbreviated
    if (request.getAttribute("StateNamesWithTaxFormsHashMap") != null && request.getAttribute("StateNamesWithTaxFormsHashMap") instanceof HashMap)
        stateNameHashMap.putAll((HashMap)request.getAttribute("StateNamesWithTaxFormsHashMap"));
%>

<div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td></td>
      <td class="lightBackground">
        <div align="center">
<% if (subMenu.equals("tax")) { %>
					<s:if test="#parameters.isFederal">						
						<span class="shortcutPathClass" style="display:none;">pmtTran_tax,addFederalPayeeLink</span>
						<span class="shortcutEntitlementClass" style="display: none;">CCD + TXP</span>
						<form id="FedTaxId" name="FedTax" method="post" >
							<table border="0" cellspacing="0" cellpadding="3" id="FedTaxWrapper" class="tableData">
								<tr>
									<td class="lightBackground">
										<div align="left" class="sectionsubhead"><s:text name="jsp.ach.Add.Federal.Tax.Type" /></div>
									</td>
								</tr>
								<tr>
									<td align="left">
										
		                    				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		                                    <input type="hidden" name="taxType" value="TaxPayment"/>
												<select id="FederalTaxList" width="40" class="" name="ID" size="5" multiple onchange="disableAdd();" style="width:500px">
													<option title="" selected value=""><s:text name="jsp.ach.Select.Federal.Tax.Type"/></option>
													<ffi:list collection="FederalTaxForms" items="TaxForm">
													<option title="<ffi:getProperty name="TaxForm" property="Name"/>" value="<ffi:getProperty name='TaxForm' property='ID'/>">
		                                                <% String taxName = ""; %>
														<ffi:getProperty name="TaxForm" property="Name" assignTo="taxName" />
		                                                <% if (taxName.length() > 60)
		                                                    taxName = taxName.substring(0, 60); %>
		                                                <%= taxName %>
													</option>
													</ffi:list>
												</select>
												<select width="10" class="txtbox" id="fedTaxBusinessId" name="isBusiness" >
													<option <%=personalSelected%> value="false"><s:text name="jsp.ach.Personal.Use" /></option>
													<option <%=businessSelected%> value="true"><s:text name="jsp.ach.Business.Use" /></option>
												</select>
									</td>
								</tr>
								<tr>
									<td></td>
								</tr>
								<tr>
									<td align="center">
										<sj:a 
							                button="true" 
											summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelAddTaxForm">
											<s:text name="jsp.default_82" /><!-- CANCEL -->
							        	</sj:a>
										<input id="fedTaxAddId1" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only formSubmitBtn hidden" type="hidden" name="add" value="Add" onClick="ns.tax.fedTaxAdd();">
										<sj:a 
											id="fedTaxAddId"
							                button="true" 
							                onClickTopics="loadSummaryPg"
							        	><s:text name="jsp.default_29" />
							        	</sj:a>
                                        <div class="sectionsubhead" id="TruncatedTextFed"></div>
									</td>
								</tr>
							</table>
						</form>
					</s:if>
					<s:else>
					<span class="shortcutPathClass" style="display:none;">pmtTran_tax,addTaxPayeeLink</span>
					<span class="shortcutEntitlementClass" style="display: none;">CCD + TXP</span>
					<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData" id="StateTaxWrapper">
						<tr>
							<td class="lightBackground">
								<div align="left" class="sectionsubhead"><s:text name="jsp.ach.Add.State.Tax.Type"/></div>
							</td>
						</tr>
						<tr>
							<td>
							<form id="StateTaxId" name="StateTax" method="post" class="stateTax">
								<table border="0" cellspacing="0" cellpadding="3">
                    			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                <input type="hidden" name="taxType" value="TaxPayment"/>
                                    <% String stateName = "", stateAbbr = ""; %>
									<tr>
										<td valign="top">
											<select id="stateId" class="txtbox" name="State" onchange="changeState(this);">
												<option value=""><!--L10NStart-->-State-<!--L10NEnd--></option>
												<ffi:list collection="StateNamesWithTaxForms" items="State">
													<ffi:getProperty name="State" assignTo="stateName"/>
                                                    <% if (stateNameHashMap.size() > 0)
                                                    {
                                                        stateAbbr = (String)stateNameHashMap.get(stateName);
                                                    } %>
                                                    <ffi:setProperty name="StateAbbr" value='<%=""+stateAbbr%>'/>
													<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CCD_TXP %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.STATE %>" objectId="${StateAbbr}">
														<option value="<ffi:getProperty name='StateAbbr' />" <ffi:cinclude value1="${StateAbbr}" value2="${FormsForState.State}">selected</ffi:cinclude>>
															<ffi:getProperty name="State"/>
														</option>
													</ffi:cinclude>
												</ffi:list>
											</select>
										</td>
										<td>
											<select id="StateTaxList" class="" name="ID" size="5" multiple onchange="disableAdd();" style="width:500px" >
												<% boolean taxSelected = false; %>
												<ffi:cinclude value1="${FormsForState.size}" value2="1" operator="equals">
												<% taxSelected = true; %>
												</ffi:cinclude>
												<option title="" value="" <%=!taxSelected?"selected ":""%> ><s:text name="jsp.ach.Select.State.Tax.Type"/></option>
												<ffi:list collection="FormsForState" items="TaxForm">
												<option title="<ffi:getProperty name="TaxForm" property="Name"/>" <%=taxSelected?"selected ":""%> value="<ffi:getProperty name='TaxForm' property='ID'/>">
                                                    <% String taxName = ""; %>
                                                    <ffi:getProperty name="TaxForm" property="Name" assignTo="taxName" />
                                                    <% if (taxName.length() > 60)
                                                        taxName = taxName.substring(0, 60); %>
                                                    <%= taxName %>
												</option>
												</ffi:list>
											</select>
                                            <select width="10" class="txtbox" id="stateBusinessId" name="isBusiness" >
                                                <option <%=personalSelected%> value="false"><s:text name="jsp.ach.Personal.Use"/></option>
                                                <option <%=businessSelected%> value="true"><s:text name="jsp.ach.Business.Use"/></option>
                                            </select>
										</td>
									</tr>
									<tr><td colspan="2"></td></tr>
									<tr>
										<td colspan="2" align="center">
											<sj:a 
								                button="true" 
												summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelAddTaxForm"
								        	><s:text name="jsp.default_82" /><!-- CANCEL -->
								        	</sj:a>
											<input id="stateAddId1" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only formSubmitBtn hidden" type="hidden" name="add" value="Add" onClick="ns.tax.stateTaxAdd();">
											<sj:a 
								                button="true" 
												id="stateAddId" 
												onClickTopics="loadSummaryPg"
								        	><s:text name="jsp.default_29" />
								        	</sj:a>
                                            <div class="sectionsubhead" id="TruncatedTextState"></div>
										</td>
									</tr>
								</table>
								</form>
							</td>
						</tr>
					<ffi:cinclude value1="${OtherTaxForms.size}" value2="0" operator="notEquals">
						<tr>
							<td class="lightBackground">
								<br>
								<div align="left" class="sectionsubhead"><s:text name="jsp.ach.Add.Other.Tax.Type"/></div>
							</td>
						</tr>
						<tr>
							<td>
									<form id="OtherTaxId" name="OtherTax" method="post" >
                    	                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                                <input type="hidden" name="taxType" value="TaxPayment"/>
										<select id="OtherTaxIDId" class="txtbox" name="ID" onchange="disableAdd();">
											<option value="" selected>-<s:text name="jsp.ach.Select.Other.Tax.Type"/>-</option>
											<ffi:list collection="OtherTaxForms" items="TaxForm">
											<option value="<ffi:getProperty name='TaxForm' property='ID'/>">
												<ffi:getProperty name="TaxForm" property="Name"/>
											</option>
											</ffi:list>
										</select>
										<select id="OtherTaxBusinessId" width="10" class="txtbox" name="isBusiness" >
											<option <%=personalSelected%> value="false"><s:text name="jsp.ach.Personal.Use" /></option>
											<option <%=businessSelected%> value="true"><s:text name="jsp.ach.Business.Use" /></option>
										</select>
									</form>
							</td>
						</tr>
						<tr><td></td></tr>
						<tr>
							<td align="center">
								<sj:a 
					                button="true" 
									summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelAddTaxForm"
					        	><s:text name="jsp.default_82" /><!-- CANCEL -->
					        	</sj:a>
								<input id="otherAddId1" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only formSubmitBtn hidden" type="hidden" name="add" value="Add" onClick="ns.ach.otherTaxAdd();">
								<sj:a 
					                button="true" 
									id="otherAddId"
					        	><s:text name="jsp.default_29" />
					        	</sj:a>
							</td>
						</tr>
					</ffi:cinclude>
						<tr>
							<form method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<td align="center">
										<input type="hidden" value="Cancel" class="submitbutton" onclick="document.location='taxforms.jsp';return false;">
								</td>
							</form>
						</tr>
					</table>
					</s:else>
<% } else { %>
                    <table border="0" cellspacing="0" cellpadding="3" width="98%">
						<tr>
							<td class="lightBackground">
								<br>
								<div align="left" class="sectionsubhead"><s:text name="jsp.ach.Add.State.Child.Support.Type"/></div>
							</td>
						</tr>
						<tr>
							<td>
							<s:form id="StateTaxId" validate="false" action="/pages/jsp/ach/addChildTaxTypeAction.action" method="post" name="StateTax" theme="simple">
								<table border="0" cellspacing="0" cellpadding="3" class="CSstateTax">
                    			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" >
                                <input type="hidden" name="taxType" value="ChildSupport"/>
                                    <% String stateName = "", stateAbbr = ""; %>
								<ffi:removeProperty name="tmp_url"/>
									<tr>
										<td valign="top">
											<select id="stateId" class="txtbox" name="State" onchange="changeState(this);">
												<option value=""><s:text name="jsp.default_386" /></option>
                                                <% String tempState = ""; %>
												<ffi:list collection="StateNamesWithTaxForms" items="State">
                                                    <ffi:getProperty name="State" assignTo="stateName"/>
                                                    <% if (stateNameHashMap.size() > 0)
                                                    {
                                                        stateAbbr = (String)stateNameHashMap.get(stateName);
                                                    } %>
                                                    <ffi:setProperty name="StateAbbr" value='<%=""+stateAbbr%>'/>
                                                    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CCD_DED %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.STATE %>" objectId="${StateAbbr}">
                                                        <option value="<ffi:getProperty name='StateAbbr' />" <ffi:cinclude value1="${StateAbbr}" value2="${ChildSupportForState.State}">selected</ffi:cinclude>>
                                                            <ffi:getProperty name="State"/>
                                                        </option>
													</ffi:cinclude>
												</ffi:list>
											</select>
										</td>
										<td>
											<select id="taxFormId" class="ui-widget-content ui-corner-all" name="ID" size="5" multiple onchange="disableAdd();" style="width:500px">
												<% boolean childSelected = false; %>
												<ffi:cinclude value1="${ChildSupportForState.size}" value2="1" operator="equals">
												<% childSelected = true; %>
												</ffi:cinclude>
												<option value="" <%=!childSelected?"selected ":""%>>-<s:text name="jsp.ach.Select.State.Child.Support.Type"/>-</option>
												<ffi:list collection="ChildSupportForState" items="TaxForm">
												<option <%=childSelected?"selected ":""%> value="<ffi:getProperty name='TaxForm' property='ID'/>">
													<ffi:getProperty name="TaxForm" property="Name"/>
												</option>
												</ffi:list>
											</select>
										</td>
										<td valign="top">
                                            <select width="10" id="stateBusinessId" class="txtbox" name="isBusiness" >
                                                <option <%=personalSelected%> value="false"><s:text name="jsp.ach.Personal.Use" /></option>
                                                <option <%=businessSelected%> value="true"><s:text name="jsp.ach.Business.Use" /></option>
                                            </select>
										</td>
									</tr>
									<tr><td colspan="3"></td></tr>
									<tr>
										<td valign="top" align="center" colspan="3">
											<sj:a 
							                button="true" 
											onClickTopics="cancelAddChildSupportTypeForm"
								        	><s:text name="jsp.default_82" /><!-- CANCEL -->
								        	</sj:a>
											<input id="stateAddId1" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only formSubmitBtn hidden" type="hidden" name="add" value="Add" onClick="ns.childsp.stateTaxAdd();">
											<sj:a 
								                button="true" 
								                id="stateAddId"
												class="ui-state-disabled"
								        	><s:text name="jsp.default_29" /><!-- CANCEL -->
								        	</sj:a>
								        	
											
										</td>
									</tr>
									
								</table>
								</s:form>
							</td>
						</tr>
					<ffi:cinclude value1="${OtherTaxForms.size}" value2="0" operator="notEquals">
						<form id="OtherTaxId" name="OtherTax" method="post" >
						<tr>
							<td class="lightBackground">
								<br>
								<div align="left" class="sectionsubhead"><s:text name="jsp.ach.Add.Other.Child.Support.Type"/></div>
							</td>
						</tr>
						<tr>
							<td>
									
                    				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                    <input type="hidden" name="taxType" value="ChildSupport"/>
									<ffi:removeProperty name="tmp_url"/>
										<select id="OtherTaxIDId" class="txtbox" name="ID" onchange="disableAdd();">
											<option value="" selected>-<s:text name="jsp.ach.Select.Other.Tax.Type"/>-</option>
											<ffi:list collection="OtherTaxForms" items="TaxForm">
											<option value="<ffi:getProperty name='TaxForm' property='ID'/>">
												<ffi:getProperty name="TaxForm" property="Name"/>
											</option>
											</ffi:list>
										</select>
										<select id="OtherTaxBusinessId" width="10" class="txtbox" name="isBusiness" >
											<option <%=personalSelected%> value="false"><s:text name="jsp.ach.Personal.Use" /></option>
											<option <%=businessSelected%> value="true"><s:text name="jsp.ach.Business.Use" /></option>
										</select>
							 </td>
						</tr>
						<tr>
						    <td align="center">
								<!-- <input id="otherAddId" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only formSubmitBtn" type="button" name="add" value="Add" onClick="ns.ach.otherTaxAdd();"> -->
								<sj:a 
					                button="true" 
									onClickTopics="cancelAddChildSupportTypeForm"
					        	><s:text name="jsp.default_82" /><!-- CANCEL -->
					        	</sj:a>
								<sj:a 
					                button="true" 
					                id="otherAddId"
									onClickTopics="ns.ach.otherTaxAdd();"
					        	><s:text name="jsp.default_29" /><!-- CANCEL -->
					        	</sj:a>
							</td>
						</tr>
						</form>
					</ffi:cinclude>
						<tr>
							<form method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<td align="center">
										<input type="hidden" value="Cancel" class="submitbutton" onclick="document.location='taxforms.jsp';return false;">
								</td>
							</form>
						</tr>
					</table>
<% } %>
				</div>
      </td>
      <td align="center" ><br>
        <br>
        
      </td>
    </tr>
  </table>
				<%-- <sj:a 
	                button="true" 
					onClickTopics="cancelACHForm"
	        	><s:text name="jsp.default_82" /><!-- CANCEL -->
	        	</sj:a> --%>
	        	
</div>
<script language="JavaScript">
<!--
$(document).ready(function(){
	disableAdd();
	$("#StateTaxList").extmultiselect().multiselectfilter();
	$("#FederalTaxList").extmultiselect().multiselectfilter();
	$("#taxFormId").extmultiselect().multiselectfilter();
	$("#fedTaxBusinessId").selectmenu({width: 200});
	$("#stateId").selectmenu({width: 200});
	$("#stateBusinessId").selectmenu({width: 200});
	$("#OtherTaxBusinessId").selectmenu({width: 250});
	$("#OtherTaxIDId").selectmenu({width: 250});
	$("#stateAddId").addClass("ui-state-disabled");
	$( "#stateAddId").unbind( "click" );
});


function changeState(obj) {
	var urlString = '/cb/pages/jsp/ach/taxformadd.jsp';
	var stateValue = obj.options[obj.selectedIndex].value;
	$.ajax({
		   type: "POST",
		   url: urlString,
		   data: {State: stateValue, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
		   success: function(data){
		 
		   	$("#inputDiv").html(data);
		   	
		   	$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#inputDiv").setFocus();
			}
			 var isChildSupport=false;
			 if( $('.CSstateTax').length)
			 {
				isChildSupport=true;
			 }
			 if(isChildSupport)
			 {
					$( ".CSstateTax #stateAddId" ).removeClass("ui-state-disabled");
					 $( ".CSstateTax #stateAddId" ).bind( "click", function() {
						ns.childsp.stateTaxAdd();
					});
			}
			
		
			
	   }
	});
}

function disableAdd() {
	frm = document.FedTax;
	if (frm != null)
	{
		if ($('select#FederalTaxList option:selected').val() == "" && $('select#FederalTaxList option:selected').size() == 1){
			frm['add'].disabled = true;
			$("#fedTaxAddId").addClass("ui-state-disabled");
			$("#fedTaxAddId").unbind( "click" );
		}else{
			frm['add'].disabled = false;
			$("#fedTaxAddId").removeClass("ui-state-disabled");
			$( "#fedTaxAddId" ).bind( "click", function() {
				ns.tax.fedTaxAdd();
			});
			if($('#ui-extmultiselect-FederalTaxList-option-0').attr('aria-selected')!="false")
			$('#ui-extmultiselect-FederalTaxList-option-0').trigger('click');
		}
		if($('select#FederalTaxList option:selected').size() == 0){
			$('#ui-extmultiselect-FederalTaxList-option-0').trigger('click');
		}
	}
	frm = document.StateTax;
	if (frm != null)
	{
	
			 var isChildSupport=false;
			 if( $('.CSstateTax').length)
			 {
				isChildSupport=true;
			 }
			 
			 if(!isChildSupport)
			 {
			 
					if ($('select#StateTaxList option:selected').val() == "" && $('select#StateTaxList option:selected').size() == 1){	
						frm['add'].disabled = true;
						$("#stateAddId").addClass("ui-state-disabled");
						$("#stateAddId").unbind( "click" );
					}else if($('select#StateTaxList option:selected').val() == undefined && $('select#StateTaxList option:selected').size() == 0){
						//condition to check if Uncheck all is clicked
						frm['add'].disabled = true;
						$("#stateAddId").addClass("ui-state-disabled");
						$("#stateAddId").unbind( "click" );
					}else{
						frm['add'].disabled = false;
						if($('#ui-extmultiselect-StateTaxList-option-0').attr('aria-selected')!="false")
						$('#ui-extmultiselect-StateTaxList-option-0').trigger('click');
						$("#stateAddId").removeClass("ui-state-disabled");	
						$( ".stateTax #stateAddId" ).bind( "click", function() {
							ns.tax.stateTaxAdd();
						});			
					}
					if($('select#StateTaxList option:selected').size() == 0){
						$('#ui-extmultiselect-StateTaxList-option-0').trigger('click');
					}
			}
			else
			{
				if ($('select#taxFormId option:selected').val() == "" && $('select#taxFormId option:selected').size() == 1){	
						frm['add'].disabled = true;
						$("#stateAddId").addClass("ui-state-disabled");
						$("#stateAddId").unbind( "click" );
					} else if ($('select#taxFormId option:selected').val() == undefined && $('select#taxFormId option:selected').size() == 0){
						//condition to check if Uncheck all is clicked
						frm['add'].disabled = true;
						$("#stateAddId").addClass("ui-state-disabled");
						$("#stateAddId").unbind( "click" );
					}	else{
						frm['add'].disabled = false;
						if($('#ui-extmultiselect-taxFormId-option-0').attr('aria-selected')!="false")
						$('#ui-extmultiselect-taxFormId-option-0').trigger('click');
						$("#stateAddId").removeClass("ui-state-disabled");	
						$( ".CSstateTax #stateAddId" ).bind( "click", function() {
							ns.childsp.stateTaxAdd();
						});			
					}
					if($('select#taxFormId option:selected').size() == 0){
						$('#ui-extmultiselect-taxFormId-option-0').trigger('click');
					}
			}
	}
	frm = document.OtherTax;
	if (frm != null)
	{
		if ($('select#OtherTaxIDId option:selected').val() == "") {
			frm['add'].disabled = true;
			$("#otherAddId").addClass("ui-state-disabled");
		}
		else {
			frm['add'].disabled = false;
			$("#otherAddId").removeClass("ui-state-disabled");
		}
	}
    showTruncated();
    
    /* $( "#fedTaxAddId" ).bind( "click", function() {
		ns.tax.fedTaxAdd();
	}); */
	
    $( "#otherAddId1" ).bind( "click", function() {
		ns.ach.otherTaxAdd();
	});
	

}


    function showTruncated()
    {
        var truncatedTextFed = document.getElementById("TruncatedTextFed");
        var truncatedTextState = document.getElementById("TruncatedTextState");
        var federalListSel = document.getElementById("FederalTaxList");
        var stateListSel = document.getElementById("StateTaxList");

        if (truncatedTextFed != null)
        {
            taxtitle = federalListSel.options[ federalListSel.selectedIndex ].title;
            $('#TruncatedTextFed').html(taxtitle);
            if (taxtitle.length > 60)
            {
                truncatedTextFed.style.visibility = "visible";
                truncatedTextFed.style.display = "";
            }
            else
            {
                truncatedTextFed.style.visibility = "hidden";      // only show if too long for drop-down
                truncatedTextFed.style.display = "none";
            }
         }

        if (truncatedTextState != null)
        {
            taxtitle = stateListSel.options[ stateListSel.selectedIndex ].title;
            $('#TruncatedTextState').html(taxtitle);
            if (taxtitle.length > 60)
            {
                truncatedTextState.style.visibility = "visible";
                truncatedTextState.style.display = "";
            }
            else
            {
                truncatedTextState.style.visibility = "hidden";      // only show if too long for drop-down
                truncatedTextState.style.display = "none";
            }
         }
    }
// --></script>

<ffi:removeProperty name="StateNamesWithTaxForms"/>
<ffi:removeProperty name="OtherTaxForms"/>
