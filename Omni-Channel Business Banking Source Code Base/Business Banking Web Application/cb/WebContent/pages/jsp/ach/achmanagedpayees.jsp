<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:help id="payments_ACHPayeeSummary" className="moduleHelpClass"/>
<s:action namespace="/pages/jsp/ach" name="viewACHPayeeAction_anyPendingDAPayees" />
	<br/>
	<span class="sectionhead">&gt; <s:text name="ach.payeeSelectHeading"/></span>
	<br/>
	<br/>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
	<ffi:setProperty name="canManageAchParticipants" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
	<ffi:setProperty name="canManageAchParticipants" value="false"/>
</ffi:cinclude>
	<ffi:removeProperty name="ManagedGridPayee" />
	
	<% 
		String selectedSECCode=""; 
	%>

	<ffi:setProperty name="tempPayeeType" value="Domestic"/>
	<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="selectedSECCode" />
	<% 
    // if can have addenda, need to know if they are only entitled to the + Addenda code
        if (selectedSECCode != null && selectedSECCode.length() > 3)
            selectedSECCode = selectedSECCode.substring(0,3);
        if ("IAT".equals(selectedSECCode))
        { %>
			<ffi:setProperty name="tempPayeeType" value="International"/>
		  <%
        }
    %>

	<ffi:setGridURL grid="GRID_allACHPayee" name="ViewURL" url="/cb/pages/jsp/ach/viewACHPayeeAction_init.action?PayeeID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_allACHPayee" name="EditURL" url="/cb/pages/jsp/ach/editACHPayeeAction_init.action?IsManagedParticipants=true&ManagedGridPayee=true&PayeeID={0}&Action={1}" parm0="ID" parm1="Action"/>
	<ffi:setGridURL grid="GRID_allACHPayee" name="InsertURL" url="/cb/pages/jsp/ach/editACHEntryAction_insertManagedParticipant.action?ManagedPayee=true&PayeeID={0}" parm0="ID"/>
	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getACHParticipantsAction.action?searchOper=cn&PayeeType=${tempPayeeType}&CanInsert=true&ACHBatchName=AddEditACHBatch&GridURLs=GRID_allACHPayee" URLEncrypt="true"/>
	<ffi:removeProperty name="tempPayeeType"/>
    <s:url id="allACHPayeeUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="allACHPayeeGridId"
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{allACHPayeeUrl}"
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,allACHPayeeGridCompleteEvents"
		> 
		
        <sjg:gridColumn name="payeeType" edittype="select" editoptions="{value:'1:Standard;2:IAT Payee'}" width="120" index="PAYEETYPE" title="%{getText('ach.grid.payeeType')}" sortable="true" search="false" hidden="true" hidedlg="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="nickNameSecure" width="80" index="NICKNAME" title="%{getText('jsp.default_294')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="nameSecure" width="80" index="NAME" title="%{getText('ach.grid.payeeName')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" searchoptions="{searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="userAccountNumber" width="80"  index="USERACCOUNTNUMBER" title="%{getText('ach.grid.payeeID')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="displayACHCompany" width="120" index="COMPANYID" title="%{getText('ach.grid.companyIDOrName')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" formatter="ns.ach.formatACHPayeesCompanyNameID" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="payeeGroup" edittype="select" editoptions="{value:'1:User;2:ACH Company;3:Business'}" width="120" index="PAYEEGROUP" title="%{getText('ach.grid.payeeScope')}" sortable="true" search="false" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="bankNameSecure" width="120"  index="BANKNAME" title="%{getText('jsp.default_63')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="routingNumberSecure" width="80" index="ROUTINGNUM" title="%{getText('jsp.default_365')}" hidden="true" hidedlg="true" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="accountNumberSecure" width="120" index="ACCOUNTNUMBER" title="%{getText('jsp.default_19')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="accountTypeSecure" edittype="select" hidden="true" hidedlg="true" editoptions="{value:'1:Checking;2:Savings;3:Loan;4:General Ledger'}" width="80" index="accountType" title="%{getText('jsp.default_20')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="securePayee" edittype="checkbox" editoptions="{value:'true:false'}" search="false" width="120" index="securePayee" title="%{getText('ach.grid.securePayee')}" sortable="false" hidden="true" hidedlg="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="displayAsSecurePayee" width="120" index="displayAsSecurePayee" search="false" title="%{getText('ach.grid.displaySecurePayee')}" sortable="false" hidden="true" hidedlg="true" editable="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="canInsert" width="10" index="canInsert" search="false" title="Can insert payee into batch" sortable="false" hidden="true" hidedlg="true" editable="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="prenoteStatus" width="120" index="prenote" search="false" title="%{getText('ach.grid.prenote')}" sortable="false" hidden="true" hidedlg="true" editable="false" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="daStatus" index="daStatus" title="%{getText('jsp.approval_daStatus_8')}" sortable="false" hidedlg="true" hidden="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="rejectReason" index="rejectReason" title="Reject reason" sortable="false"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="rejectedBy" index="rejectedBy" title="Rejected By" sortable="false"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" search="false" formatter="ns.ach.formatAllACHPayeeActionLinks"
			formatoptions="{
			canManageParticipant : '%{#session.canManageAchParticipants}', dualApprovalMode : '%{#session.Business.dualApprovalMode}'
			}"	width="90" cssClass="datagrid_actionIcons"/>
	</sjg:grid>
	
	<script type="text/javascript">
	<!--
		$("#allACHPayeeGridId").data("supportSearch",true);		
		$("#allACHPayeeGridId").jqGrid('setColProp','ID',{title:false}); 
        ns.ach.setInputTabText('#normalInputTab');
        ns.ach.setVerifyTabText('#normalVerifyTab');
        ns.ach.setConfirmTabText('#normalConfirmTab');
    //-->
    </script>

	<br><br>


	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
<% if ("true".equals(request.getAttribute("anyPendingDAPayees"))) {%>

			<div id="managedPendingACHPayees_gridID">
				<s:include value="/pages/jsp/ach/achmanagedpayee_pendingapproval.jsp"/>
			</div>

			<script>
				$('#managedPendingACHPayees_gridID').portlet({
					generateDOM: true,
					helpCallback: function(){
						var helpFile = $('#managedPendingACHPayees_gridID').find('.moduleHelpClass').html();
						callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
					}
				});
				$('#managedPendingACHPayees_gridID').portlet('title', js_achpayee_pending_portlet_title);

			</script>
			<style>
				#managedPendingACHPayees_gridID .ui-jqgrid-ftable td{
					border:none;
				}
			</style>
			<br><br>
<% } %>
	</ffi:cinclude>

	<div align="center">
		<input id="batchId" type="hidden" value='<%=session.getAttribute("BatchID") %>'>
		<input id="collection" type="hidden" value='<%=session.getAttribute("Collection") %>'>
		<input id="entryId" type="hidden" value='<%=session.getAttribute("EntryID") %>'>
	  
		<sj:a 
            button="true" 
			onClick="ns.ach.cancelManagedPayee('/cb/pages/jsp/ach/editACHEntryAction_cancelManagedParticipants.action');"
     	>
			<s:text name="jsp.default_82"/>
     	</sj:a>
     	
     	<s:url id="addACHPayeeUrl2" value="/pages/jsp/ach/addACHPayeeAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="ManagedGridPayee">true</s:param>
        </s:url>
        <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>" >
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
				<sj:a id="addACHPayeeLink2" href="%{addACHPayeeUrl2}" targets="achAddEditEntrySection" button="true"
					onClickTopics="beforeLoadManageNewPayeeForm" onCompleteTopics="loadManageNewPayeeFormComplete" onErrorTopics="errorLoadACHForm">
					  &nbsp;<s:text name="ach.newPayee"/>
				</sj:a>
			</ffi:cinclude>
        </ffi:cinclude>
     </div>
<ffi:removeProperty name="EntryID"/>
