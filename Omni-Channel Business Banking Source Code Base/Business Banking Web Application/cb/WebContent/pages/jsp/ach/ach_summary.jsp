<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>


	<div id="ACHGridTabs" class="portlet gridPannelSupportCls" style="float: left; width: 100%;" role="section" aria-labelledby="summaryHeader">
	    <div class="portlet-header">
	    <h1 id="summaryHeader" class="portlet-title"><span><s:text name="jsp.ach.batchessummary"/></span></h1>
	    <br>
		    <div class="searchHeaderCls">
				<span class="searchPanelToggleAreaCls" onclick="$('#quicksearchcriteria').slideToggle();">
					<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
				</span>
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.default_531"/>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>

					<div class="gridTabDropdownHolder">
						<span class="gridTabDropdownItem" id='approving' onclick="ns.common.showGridSummary('approving')" >
							<s:text name="jsp.default_531"/>
						</span>
						<span class="gridTabDropdownItem" id='pending' onclick="ns.common.showGridSummary('pending')">
							<s:text name="jsp.default_530"/>
						</span>
						<span class="gridTabDropdownItem" id='completed' onclick="ns.common.showGridSummary('completed')">
							<s:text name="jsp.default_518"/>
						</span>
					</div>
				</div>
			</div>
			<div class="cutoffTimeMsgTblHolder" style="margin-left: 10px;margin-top: 3px;">
					<s:include value="inc/cutoff-text.jsp"/>
			</div>

	    </div>

	    <div class="portlet-content">
	    	<div class="searchDashboardRenderCls">
				<%
				    String type = (String)session.getAttribute("subMenuSelected");
				    if ("ach".equals(type)) { %>
				    	<s:include value="/pages/jsp/ach/inc/achBatchSearchCriteria.jsp"/>
				    <%} else if ("tax".equals(type)) { %>
				    	<s:include value="/pages/jsp/ach/inc/taxBatchSearchCriteria.jsp"/>
				    <%} else if ("child".equals(type)) { %>
				    	<s:include value="/pages/jsp/ach/inc/childSupportBatchSearchCriteria.jsp"/>
				   	<%}
				%>
			</div>
			<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="approvingSummaryGrid" gridId="pendingApprovalACHID" class="gridSummaryContainerCls" >
					<s:include value="/pages/jsp/ach/ach_pendingapproval_summary.jsp"/>
				</div>
				<div id="pendingSummaryGrid" gridId="pendingACHGridID" class="gridSummaryContainerCls hidden" >
					<s:include value="/pages/jsp/ach/ach_pending_summary.jsp"/>
				</div>
				<div id="completedSummaryGrid" gridId="completedACHGridID" class="gridSummaryContainerCls hidden" >
					<s:include value="/pages/jsp/ach/ach_completed_summary.jsp"/>
				</div>
			</div>
	    </div>

		<div id="achBatchSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('ACHGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>

	</div>


	<div id="view_delete_ACH_dialog"></div>

	 <s:url id="calendarURL" value="/pages/jsp/ach/loadCalendar_showAchBatch.action" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="moduleName" value="%{'achCalendar'}"></s:param>
	</s:url>
	<sj:a
		id="openCalendar"
		href="%{calendarURL}"
		targets="summary"
		onSuccessTopics="calendarLoadedSuccess"
		cssClass="titleBarBtn"
		cssStyle="display:none"
		>
		<s:text name="jsp.billpay_calender"/><span class="ui-icon ui-icon-calendar floatleft"></span>
	</sj:a>

	<input type="hidden" id="getTransID" value="<ffi:getProperty name='ACHtabs' property='TransactionID'/>" />
	<s:hidden name="ACHSummaryType" value="PendingACH" id="ACHSummaryTypeID"/>

<ffi:removeProperty name="AddEditACHBatch"/>
<ffi:removeProperty name="ApprovalsViewTransactionDetails"/>
<ffi:removeProperty name="ViewTemplateBatch"/>

<script>
	//Initialize portlet with settings & calendar icon
	ns.common.initializePortlet("ACHGridTabs", false, true, function( ) {
	    $("#openCalendar").click();
	});
</script>