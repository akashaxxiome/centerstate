<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

 <% 
 	request.setAttribute("actionName","/pages/fileupload/ACHFileUploadAction.action"); 
 %>  

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	<ffi:process name="SetMappingDefinition"/>
</ffi:cinclude>
<ffi:setProperty name="actionNameNewReportURL" value="NewReportBaseAction.action?NewReportBase.ReportName=${ReportName}&flag=generateReport" URLEncrypt="true" />
<ffi:setProperty name="actionNameGenerateReportURL" value="GenerateReportBaseAction_display.action?doneCompleteDirection=${doneCompleteDirection}" URLEncrypt="true" />
 <%--this is used for public --%>
<s:include value="/pages/jsp/common/publicuploader.jsp"/>

<script>
	ns.common.popupAfterUpload = true;
</script>