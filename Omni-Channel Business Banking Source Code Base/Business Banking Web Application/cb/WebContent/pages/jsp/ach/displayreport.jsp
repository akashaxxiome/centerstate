<%@ page import="com.ffusion.beans.ach.ACHReportConsts"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%  boolean canSaveReport = true;
    boolean showDone = false; %>
<ffi:cinclude value1="${ReportData.ReportCriteria.ReportOptions.REPORTTYPE}" value2="<%=ACHReportConsts.RPT_TYPE_FILE_UPLOAD%>" operator="equals">
   	<%  showDone = true;
	    canSaveReport = false; %>
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.ReportOptions.REPORTTYPE}" value2="<%=ACHReportConsts.RPT_TYPE_FILE_IMPORT%>" operator="equals">
   	<%  showDone = true;
	    canSaveReport = false; %>
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.ReportOptions.REPORTTYPE}" value2="<%=ACHReportConsts.RPT_TYPE_FILE_IMPORT_BATCHES%>" operator="equals">
   	<%  showDone = true;
	    canSaveReport = false; %>
</ffi:cinclude>

<span id="PageHeading" style="display:none;">ACH File Upload Report</span>


<%-- If the report is not of destination object, then the report isn't meant for this JSP.
	 The user has probably run a saved export report and has navigating back to this page.
	 If that has happened, we should load the saved ReportData from session and use it, as
	 the current data won't display here --%>
<ffi:setProperty name="needReload" value="false"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" />
<ffi:cinclude value1="${GenerateReportBase.LastDestination}" value2="" operator="equals">
	<%-- GenerateReportBase.LastDestination is empty, so it will use the ReportCriteria destination --%>
	<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>" value2="${ReportData.ReportCriteria.CurrentReportOptionValue}" operator="notEquals">
		<ffi:setProperty name="needReload" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${GenerateReportBase.LastDestination}" value2="" operator="notEquals">
	<%-- GenerateReportBase.LastDestination is not empty, so it will use its destination --%>
	<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>" value2="${GenerateReportBase.LastDestination}" operator="notEquals">
		<ffi:setProperty name="needReload" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${needReload}" value2="true" operator="equals">
<%
	com.ffusion.beans.reporting.Report report = (com.ffusion.beans.reporting.Report) session.getAttribute( "ReportDataSaved" );
	if( report != null ){
	    session.setAttribute( "ReportData", report );
	    //need to regenerate the data, since it has been removed
%>
		<ffi:object id="GenerateSavedReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" />
	    <ffi:setProperty name="GenerateSavedReportBase" property="Destination" value="${ReportData.ReportCriteria.CurrentReportOptionValue}"/>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>" />
	    <ffi:setProperty name="GenerateSavedReportBase" property="Format" value="${ReportData.ReportCriteria.CurrentReportOptionValue}"/>
	    <ffi:process name="GenerateSavedReportBase"/>
<%
	}
%>
</ffi:cinclude>
<ffi:removeProperty name="needReload"/>
		
<script type="text/javascript"><!--
	function validateName() {
<ffi:cinclude value1="${ReportData.ReportID.ReportName}" value2="" operator="equals">
		// Validation has been removed in order to support i18n specific validation.
		// Validation is already done by the tasks AddReport and ModifyReport.
</ffi:cinclude>
		return true;
	}
// --></script>

<%-- Currently the data source load start time, and data source load end time search criteria are used only internally
     by default, therefore, they will be hidden from the user --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATASOURCE_LOAD_START_TIME%>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.HiddenSearchCriterion" value="true"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATASOURCE_LOAD_END_TIME%>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.HiddenSearchCriterion" value="true"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TIME_FORMAT%>" />
<ffi:setProperty name="ReportData" property="ReportCriteria.HiddenSearchCriterion" value="true"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATE_FORMAT%>" />
<ffi:setProperty name="ReportData" property="ReportCriteria.HiddenSearchCriterion" value="true"/>

	<div align="center">

		<% String opt_pagewidth; %>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH %>"/>
		<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" assignTo="opt_pagewidth"/>
		<%
			String real_pagewidth = String.valueOf( Integer.parseInt( opt_pagewidth ) + 2 );

			session.setAttribute( "real_pagewidth", real_pagewidth );
		%>

		<table width="750" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="dkback" width="10">&nbsp;</td>
				<td valign="top" width="708" align="left" class="dkback">
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
					<tr>
<% if (canSaveReport) { %>
						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="equals">
							<td class="columndata" align="left" nowrap colspan="2"><span class="sectionhead"><s:text name="jsp.default_370"/></span></td>
						</ffi:cinclude>
						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="notEquals">
							<td class="columndata" align="left" nowrap colspan="2"><span class="sectionhead"><s:text name="jsp.default_180"/></span></td>
						</ffi:cinclude>
<% } %>
						<td>&nbsp;</td>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
							<td><img src="/cb/web/multilang/grafx/spacer.gif" width="100" height="2" border="0"></td>
							<td class="columndata" align="left" nowrap><span class="sectionhead"><s:text name="jsp.default_198"/></span></td>
							<td></td>
						</ffi:cinclude>
						<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
							<td colspan=3>&nbsp;</td>
						</ffi:cinclude>
					</tr>
					<tr>
<% if (canSaveReport) { %>
						<td class="sectionsubhead" align="left" nowrap colspan="2">
						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="equals">
							<form action="reportsaved.jsp" method="post" name="SaveFormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                <table>
                                    <tr>
                                        <td align="right">
                                            <span class="sectionsubhead"><s:text name="jsp.default_283" />:&nbsp;</span>
                                        </td>
                                        <td>
                                            <input class="txtbox" type="text" name="ReportData.ReportID.ReportName" size="20" maxlength="40" value="<s:text name="jsp.default_353" />">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <span class="sectionsubhead"><s:text name="jsp.default_170" />:&nbsp;</span>
                                        </td>


                                        <td nowrap>
											<input class="txtbox" type="text" name="ReportData.ReportID.Description" size="30" maxlength="255" value="<s:text name="jsp.default_170" />">&nbsp;&nbsp;<input class="submitbutton" type="submit" value="SAVE" onclick="return validateName();">
                                        </td>
                                    </tr>
                                </table>
							</form>
						</ffi:cinclude>
						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="notEquals">
							<form action="reportcriteria.jsp" method="post" name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                <input type="hidden" name="reportID" value="<ffi:getProperty name="ReportData" property="ReportID.ReportID"/>">
                                <input type="hidden" name="resetReportOptions" value="TRUE">
                                <table>
                                    <tr>
                                        <td align="right" valign="top"><span class="sectionsubhead"><s:text name="jsp.default_283" />:&nbsp;</span></td>
                                        <td valign="top"><span style="padding: 0px;" class="columndata"><ffi:getProperty name="ReportData" property="ReportID.ReportName"/></span></td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><span class="sectionsubhead"><s:text name="jsp.default_170" />:&nbsp;</span></td>
                                        <td valign="top"><span style="padding: 0px;" class="columndata"><ffi:getProperty name="ReportData" property="ReportID.Description"/></span></td>
                                    </tr>
                                </table>
							</form>
						</ffi:cinclude>
						<ffi:removeProperty name="mode"/>
						</td>
<% } %>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
						<td>&nbsp;</td>
						<%String requestId = String.valueOf(System.currentTimeMillis()); %>
						<ffi:setProperty name="PageScope_RequestId" value="<%=requestId%>" />
                        <ffi:setProperty name="tempURL" value='${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecureServletPath}GenerateReportBase${PageScope_RequestId}' URLEncrypt="true"/>
							<td>&nbsp;</td>
							<td align="left" nowrap>
                                <form method="post" name="FormName1" action="<ffi:getProperty name="tempURL"/>">
                                    <ffi:removeProperty name="PageScope_RequestId,tempURL" />
                                    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                <select class="txtbox" name="GenerateReportBase.Format">
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>">
										<!--L10NStart-->HTML<!--L10NEnd-->
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>">
										<!--L10NStart-->Comma Delimited<!--L10NEnd-->
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>">
										<!--L10NStart-->Tab Delimited<!--L10NEnd-->
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>">
										<!--L10NStart-->PDF<!--L10NEnd-->
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>">
										<!--L10NStart-->BAI2<!--L10NEnd-->
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>">
										<!--L10NStart-->Plain Text<!--L10NEnd-->
									</option>
								</ffi:cinclude>
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="true" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>">
										<!--L10NStart-->CSV Data<!--L10NEnd-->
									</option>
								</ffi:cinclude>
								</select>&nbsp;&nbsp;

                            <input type="hidden" name="GenerateReportBase.Destination" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>">
							<input class="submitbutton" type="submit" name="Submit2" value="EXPORT">
                                </form>
							</td>
						</ffi:cinclude>
						<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
							<td colspan=3>&nbsp;</td>
						</ffi:cinclude>
					</tr>
					</table>
				</td>
				<td class="dkback" width="10">&nbsp;</td>
			</tr>
		</table>
		<br>
		<form name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
            <% if (showDone) { %>
                <input class="submitbutton" type="button" name="back" value="Done" onClick="closeACHTabs();">
            <% } else { %>
                <input class="submitbutton" type="button" name="back" value="Back" onClick="closeACHTabs();">
            <% } %>         
		</form>
		<table width="<ffi:getProperty name="real_pagewidth"/>" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table border="0" cellspacing="0" cellpadding="0"><tr>
					<td background="/cb/web/multilang/grafx/reports/sechdr_blank_left.gif" height="14" class="sectiontitle">&nbsp;&nbsp;&nbsp;<s:text name="jsp.default_352"/></td>
					<td background="/cb/web/multilang/grafx/reports/sechdr_blank_middle.gif" height="14" width="100%" class="sectiontitle"></td>
					<td><img src="/cb/web/multilang/grafx/reports/sechdr_blank_right.gif" height="14"></td>
					</tr></table>
				</td>
			</tr>	
			<tr>
				<td class="tbrd_lr dkback">
					<ffi:object id="exportHeader" name="com.ffusion.tasks.reporting.ExportHeaderOptions"/>
					<ffi:setProperty name="exportHeader" property="ReportName" value="ReportData"/>
					<ffi:setProperty name="exportHeader" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
					<ffi:process name="exportHeader"/>
					<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_HEADER_NAME %>" encode="false"/>
					<ffi:removeProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_HEADER_NAME %>"/>
					<ffi:removeProperty name="exportHeader"/>
				</td>
			</tr>
			<tr>
				<td class="tbrd_ltr"><br>
					<ffi:object id="exportForDisplay" name="com.ffusion.tasks.reporting.ExportReport"/>
					<ffi:setProperty name="exportForDisplay" property="ReportName" value="ReportData"/>
					<ffi:setProperty name="exportForDisplay" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
					<ffi:process name="exportForDisplay"/>
					<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_NAME %>" encode="false"/>
					<ffi:removeProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_NAME %>"/>
					<ffi:removeProperty name="exportForDisplay"/>
				</td>
			</tr>
			<tr>
				<td class="tbrd_ltr dkback">
					<ffi:object id="exportFooter" name="com.ffusion.tasks.reporting.ExportFooterOptions"/>
					<ffi:setProperty name="exportFooter" property="ReportName" value="ReportData"/>
					<ffi:setProperty name="exportFooter" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
					<ffi:process name="exportFooter"/>
					<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_FOOTER_NAME %>" encode="false"/>
					<ffi:removeProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_FOOTER_NAME %>"/>
					<ffi:removeProperty name="exportFooter"/>
				</td>
			</tr>
			<tr>
				<td><img src="/cb/web/multilang/grafx/reports/sechdr_btm_<ffi:getProperty name="real_pagewidth"/>.gif" width="100%" height="11"></td>
			</tr>
			<ffi:removeProperty name="opt_pagewidth"/>
			<ffi:removeProperty name="real_pagewidth"/>
		</table>
<% if (canSaveReport) { %>
		<br>
		<ffi:include page="${PathExt}reports/savedreports.jsp"/>
<% } %>
        <br>
		<ffi:setProperty name="BackURL" value="${tmpBackURL}"/>
		<%-- <ffi:removeProperty name="tmpBackURL"/> --%>
		<form name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input class="submitbutton" type="button" name="PrintFriendly" value="Printer Ready" onClick="window.open('<ffi:getProperty name="SecurePath" />reports/inc/printreport.jsp', 'printWindow', 'toolbar=no, directories=no, location=yes, status=yes, menubar=yes, resizable=no, scrollbars=yes, width=700, height=600'); return false" >
            
            <% if (showDone) { %>                    
            <input class="submitbutton" type="button" name="back" value="Done" onClick="closeACHTabs();">
            <% } else { %>
            <input class="submitbutton" type="button" name="back" value="Back" onClick="closeACHTabs();">
            <% } %>
		</form>
	</div>

<%-- since the report data may be very large, we remove it from session.  the report criteria
     should be left alone to avoid breaking the "save report" page --%>
<% ( (com.ffusion.beans.reporting.Report)pageContext.findAttribute( "ReportData" ) ).setReportResult( null ); %>

<%-- save copy of ReportData in case user navigates back from non-display page report--%>
<%
	com.ffusion.beans.reporting.Report report = (com.ffusion.beans.reporting.Report) session.getAttribute( "ReportData" );
	com.ffusion.beans.reporting.Report report2 = (com.ffusion.beans.reporting.Report) report.clone();
    session.setAttribute( "ReportDataSaved", report2 );
%>
<ffi:removeProperty name="GenerateSavedReportBase"/>
