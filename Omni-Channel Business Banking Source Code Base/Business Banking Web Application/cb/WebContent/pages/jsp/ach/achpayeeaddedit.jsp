<%@page import="com.ffusion.struts.util.spring.SpringUtil"%>
<%@ page import="com.ffusion.beans.ach.ACHPayee,
				 com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.efs.adapters.profile.interfaces.CustomerAdapter" %>
<%@ page import="com.ffusion.beans.user.User" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>

<ffi:help id="payments_achpayeeaddedit" className="moduleHelpClass"/>
<ffi:setProperty name="CallerURL" value="achpayeeaddedit.jsp"/>

<s:action namespace="/pages/jsp/ach" name="ACHInitAction_initACHPayees" />

<%
	boolean isEdit = true;
	boolean isDAEdit = true;
	boolean entitled = false;
    boolean canModifyDestination = true;
	boolean showSameDayPrenote=false;
	boolean disableSameDayPrenote=true;
	// to view an ACH Company, we need to either have TEMPLATE or PaymentEntry entitlement
	String entToCheck1 = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
 	String entToCheck2 = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_TEMPLATE );

    if (request.getAttribute("isAdd") != null) {
        isEdit = false;
        isDAEdit = false;
    }
%>

<s:if test="%{showSameDayPrenote=='true'}">
    <% showSameDayPrenote = true; %>
</s:if>

<s:if test="%{disableSameDayPrenote=='false'}">
    <% disableSameDayPrenote = false; %>
</s:if>

        <%-- javascript for BankLookUp pop up window --%>
        <script type="text/javascript">
        <!--

    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
    <ffi:process name="ACHResource" />
    var accountTypeList = null;

/* Mappings of banks to indices into the account lists.*/
var countries = null;
var iatAccountList = null;
var iatCurrencyList = null;
var iatForeignExchangeIndicatorFF = null;
var iatForeignExchangeIndicatorFV = null;
var iatForeignExchangeIndicatorVF = null;
var BIC_IBANCountries = null;
var OTHER_RTNCountries = null;
var iatSupportedCurrencyList = null;
var iatSupportedCurrencyNamesList = null;
var bankIdentTypeList = null;
var userUpdatedBranchCountry = "false";

function initIATFeatures()
{
    if ( $("#PayeeType").val() == "1" )        // domestic, don't show
        return;             // nothing to do
    if (countries != null)
        return;
    var countrySel = document.getElementById("COUNTRY");
    var branchCountrySel = document.getElementById("BranchCountry");

    var newOptDefault = null;
    var newBranchOptDefault = null;

    var countryName = ""; var title = "";
    countries = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCodes" property="Size"/>);
    iatAccountList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCodes" property="Size"/>);
    iatCurrencyList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCodes" property="Size"/>);
<% int countryIndex = 0; %>
<ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
        countryCode = "<ffi:getProperty name="countryCode"/>";
        countries[ <%= countryIndex%> ] = countryCode;
    iatAccountList[ <%= countryIndex%> ] = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryAccountTypes${countryCode}"/><ffi:getProperty name="ACHResource" property="Resource"/>";
    iatCurrencyList[ <%= countryIndex%> ] = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyList${countryCode}"/><ffi:getProperty name="ACHResource" property="Resource"/>";
        countryName = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/><ffi:getProperty name="ACHResource" property="Resource" encode="false"/>";
        if (countryName == null) countryName = "";
        title = countryName;
        if (countryName.length > 25){
            countryName = countryName.substring(0,25);
        }
    <% countryIndex++; %>
        // country #0 is "Select Country"
        newOptDefault = new Option(countryName, countryCode);
        newBranchOptDefault = new Option(countryName, countryCode);

        <ffi:cinclude value1="${countryCode}" value2="${AddEditACHPayee.DestinationCountry}">
            newOptDefault.selected = true;
        </ffi:cinclude>
        <ffi:cinclude value1="${countryCode}" value2="${AddEditACHPayee.BankCountryCode}">
            newBranchOptDefault.selected = true;
        </ffi:cinclude>

        newOptDefault.title = title;
        newBranchOptDefault.title = title;

        countrySel.options[ <%= countryIndex%> ] = newOptDefault;
        branchCountrySel.options[ <%= countryIndex%> ] = newBranchOptDefault;
</ffi:list>

    iatForeignExchangeIndicatorFF = "<ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicatorFF"/><ffi:getProperty name="ACHResource" property="Resource"/>";
    iatForeignExchangeIndicatorFV = "<ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicatorFV"/><ffi:getProperty name="ACHResource" property="Resource"/>";
    iatForeignExchangeIndicatorVF = "<ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicatorVF"/><ffi:getProperty name="ACHResource" property="Resource"/>";

    BIC_IBANCountries = "<ffi:setProperty name="ACHResource" property="ResourceID" value="BIC_IBANCountries"/><ffi:getProperty name="ACHResource" property="Resource" />";
    OTHER_RTNCountries = "<ffi:setProperty name="ACHResource" property="ResourceID" value="OTHER_RTNCountries"/><ffi:getProperty name="ACHResource" property="Resource" />";

    var currencySel = document.getElementById("CURRENCY");
    var currencyText = "";
    var newCurrencyOptDefault = null;
    iatSupportedCurrencyList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCurrencyList" property="Size"/>);
    iatSupportedCurrencyNamesList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCurrencyList" property="Size"/>);
<% int currencyIndex = 0; %>
<ffi:list collection="ACHIAT_DestinationCountryCurrencyList" items="currencyCode">
        currencyCode = "<ffi:getProperty name="currencyCode"/>";
        iatSupportedCurrencyList [ <%= currencyIndex%> ] = currencyCode;
        iatSupportedCurrencyNamesList [ <%=currencyIndex%> ] = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${currencyCode}"/><ffi:getProperty name="ACHResource" property="Resource" encode="false"/>";

        currencyText = currencyCode+"-"+iatSupportedCurrencyNamesList [ <%=currencyIndex%> ];
        title = currencyText;
        if (currencyText.length > 25){
            currencyText = currencyText.substring(0,25);
        }
        newCurrencyOptDefault = new Option(currencyText, currencyCode);
        newCurrencyOptDefault.title = title;

        <ffi:cinclude value1="${currencyCode}" value2="${AddEditACHPayee.CurrencyCode}">
            newCurrencyOptDefault.selected = true;
        </ffi:cinclude>

    <% currencyIndex++; %>
        currencySel.options[ <%= currencyIndex%> ] = newCurrencyOptDefault;
</ffi:list>
}

    function userUpdatedBC( )
    {
        userUpdatedBranchCountry = "true";
    }
    function reloadBankIdentifierTypes(  )
    {
        if ( $("#PayeeType").val() == "1" )        // domestic, don't show
            return;             // nothing to do
        var bankIdentTypeSel = document.getElementById("BankIdentifierType");
		if (bankIdentTypeList == null)       // copy all of the default items
		{
            bankIdentTypeList = new Array();
			for (var i=0; i < bankIdentTypeSel.length; i++)
			{
			  bankIdentTypeList[ i ]=bankIdentTypeSel.options[ i ].value + "~" + bankIdentTypeSel.options[ i ].text;
			}
        }
		var selectedValue = bankIdentTypeSel.options[ bankIdentTypeSel.selectedIndex ].value;
        //clear out entries in the lists
		while( bankIdentTypeSel.options.length > 0 ){
			bankIdentTypeSel.options[ 0 ] = null;
		}

		var newCount = -1;

        // check if Payee Type is International
        // Add type 01
        // check if Destination Country is NOT set, we are done.  (only routing number)
        // Check if destination country is a gateway country, add 02, 03, if not, add 99
        var countrySel = document.getElementById("COUNTRY");

        for (var j=0; j < bankIdentTypeList.length; j++)
		{
			var loc = bankIdentTypeList[ j ].indexOf('~');
			var value = bankIdentTypeList[ j ].substring(0,loc);
			var text = bankIdentTypeList[ j ].substring(loc+1);

            var insert = 0;

            if (value == "01")
            {
                insert = 1;
            }
            if (countrySel != null && countrySel.selectedIndex != 0)
            {
                // get the country selected value.
                var country = countrySel.options[ countrySel.selectedIndex ].value;
                if (BIC_IBANCountries.indexOf( country ) != -1)
                {
                    if (value == "02" || value == "03")
                        insert = 1;
                } else
                if (OTHER_RTNCountries.indexOf( country ) != -1)
                {
                    if (value == "99")
                        insert = 1;
                }
            }
            if (insert == 1)
            {
                var newOpt = new Option(text, value);
                if (selectedValue == value)
                    newOpt.selected = true;     // reselect the previously selected option
                newCount++;
                bankIdentTypeSel.options[ newCount ] = newOpt;
            }
		}
        $('#BankIdentifierType').selectmenu('destroy').selectmenu({width: 250});
    }

    function updateForeignExchangeMethod(currency)
    {
        if ( $("#PayeeType").val() == "1" )        // domestic, don't show
            return;             // nothing to do
        // There is a one-to-one mapping of currency to Foreign Exchange Method
        var currencySel = document.getElementById("CURRENCY");
        var foreignSel = document.getElementById("FOREIGNEXCHANGE");
        var countrySel = document.getElementById("COUNTRY");

        if (countrySel == null || currencySel == null || foreignSel == null || currency == null)
            return;

        //clear out entries in the lists
        while( foreignSel.options.length > 0 ){
            foreignSel.options[ 0 ] = null;
        }

        // assume FF for default
        var value = "FF";
        var text = value + "-" + iatForeignExchangeIndicatorFF;
        // for USA, support USD, USD (Same Day), USD (Next Day)
        if (currency.value.indexOf("USD") != 0 && currency.value.indexOf("USS") != 0 && currency.value.indexOf("USN") != 0)
        {
            value = "FV";
            text = value + "-" + iatForeignExchangeIndicatorFV;
        }
// VF not supported when USD is originating currency
<%--            if (supportedForeignExchange.indexOf("VF") == 0)--%>
<%--            {--%>
<%--                value = "VF";--%>
<%--                text = value + "-" + iatForeignExchangeIndicatorVF;--%>
<%--            }--%>
        if (currency.selectedIndex == 0)
        {
            value = "";
            text = currency.options[0].text;
        }
        var newOpt = new Option(text, value);
        foreignSel.options[ 0 ] = newOpt;
        $("#FOREIGNEXCHANGE").selectmenu('destroy').selectmenu({width: 250});

    }

    function updateCurrencyInfo(country)
    {
        if ( $("#PayeeType").val() == "1" )        // domestic, don't show
            return;             // nothing to do
        var currencySel = document.getElementById("CURRENCY");
        var foreignSel = document.getElementById("FOREIGNEXCHANGE");
        var countrySel = document.getElementById("COUNTRY");
        var branchCountrySel = document.getElementById("BranchCountry");
        if (currencySel == null || foreignSel == null || country == null)
            return;
        var supportedCurrencies = "";
        // if they haven't chosen a branch country, make it the same as Destination Country
        if (branchCountrySel.selectedIndex == 0 || userUpdatedBranchCountry == "false")
        {
            branchCountrySel.selectedIndex = countrySel.selectedIndex;
            $('#BranchCountry').selectmenu('destroy').selectmenu({width: 250})
            updateTruncatedText("BranchCountry");
            updateAccountInfo(country);
            userUpdatedBranchCountry = "false";
        }

        reloadBankIdentifierTypes();        // when country changes, also change Bank Identifier Types

        for (var i = 0; i < countries.length; i++)
        {
            if (countries[ i ] == country.value)
            {
                supportedCurrencies = iatCurrencyList[ i ];

                var found = "";
                currencySel.selectedIndex = 0;
                while (true)
                {
                    for (var j = 0; j < iatSupportedCurrencyList.length; j++)
                    {
                        var value = iatSupportedCurrencyList[ j ];
                        var text = iatSupportedCurrencyNamesList[ j ];

                        if (value.length == 0 || supportedCurrencies.indexOf(value) == 0)
                        {
                            if (found == "")
                            {
                                found = "1";
                                currencySel.selectedIndex = j+1;        // "Select Currency" is first item
                                $('#CURRENCY').selectmenu('destroy').selectmenu({width: 250});
                                updateForeignExchangeMethod(currencySel);
                                break;
                            }
                        }
                    }
                    if (supportedCurrencies.length <= 3)
                        break;
                    supportedCurrencies = supportedCurrencies.substr(4);        // skip the first currency and comma
                }
                // we updated the ForeignExchangeMethod with the first currency displayed
                break;
            } // if found country
        }   // for loop
        if (supportedCurrencies == "")
        {
            currencySel.selectedIndex = 0;      // "Select Country First"
            $('#CURRENCY').selectmenu('value',0);
            if (foreignSel != null){
                //clear out entries in the lists
                while( foreignSel.options.length > 0 ){
                    foreignSel.options[ 0 ] = null;
                }
                var newOpt = new Option(currencySel.options[0].text, "");
                foreignSel.options[ 0 ] = newOpt;
                $("#FOREIGNEXCHANGE").selectmenu('destroy');
                $('#FOREIGNEXCHANGE').selectmenu({width: 250});
            }
        }
    }

    function updateAccountInfo(country)
    {
        if ( $("#PayeeType").val() == "1" )        // domestic, don't show
            return;             // nothing to do
        if (accountTypeList == null)
        {
            var fromSel = document.getElementById("AccountType");
            if (fromSel != null)
            {
                accountTypeList = new Array();
                var k;
                for(k = 0; k < fromSel.length; k++)
                {
                    accountTypeList[ k ]=fromSel.options[ k ].value + "~" + fromSel.options[ k ].text;
                }
            }
        }
        var fromSel = document.getElementById("AccountType");
        if (fromSel != null)
        {
            var supportedAccounts = "";
            for (var i = 0; i < countries.length; i++)
            {
                if (countries[ i ] == country.value)
                {
                    supportedAccounts = iatAccountList[ i ];
                    if (supportedAccounts == "")            // if nothing is listed, assume Checking.
                        supportedAccounts="1";

                    //clear out entries in the lists
                    while( fromSel.options.length > 0 ){
                        fromSel.options[ 0 ] = null;
                    }

                    var newCount = -1;
                    for (var j = 0; j < accountTypeList.length; j++)
                    {
                        var loc = accountTypeList[ j ].indexOf('~');
                        var value = accountTypeList[ j ].substring(0,loc);
                        var text = accountTypeList[ j ].substring(loc+1);

                        if (value.length == 0 || supportedAccounts.indexOf(value) >= 0)
                        {
                            var newOpt = new Option(text, value);
                            newCount++;
                            fromSel.options[ newCount ] = newOpt;
                        }
                    }
                    break;
                }
            }
        }
        $("#AccountType").selectmenu('destroy').selectmenu({width: 250});
    }

    function updateTruncatedText(controlName)
    {
        if ( $("#PayeeType").val() == "1" )        // domestic, don't show
            return;             // nothing to do
        if (controlName == "ALL")
    {
            updateTruncatedText("BranchCountry");
            updateTruncatedText("COUNTRY");
            updateTruncatedText("CURRENCY");
			updateTruncatedText('achPayeeCountry');
            return;
        }
        var truncatedText = document.getElementById(controlName + "TruncatedText");
        var countrySel = document.getElementById(controlName);
        if (truncatedText != null)
        {
            taxtitle = countrySel.options[ countrySel.selectedIndex ].title;
            truncatedText.innerHTML = taxtitle;
            if (taxtitle.length > 25)
            {
                $(truncatedText).show();
            }
            else
            {
                $(truncatedText).hide();
            }
         }
    }

        function showHide()
        {

        	removeValidationErrors();
            initIATFeatures();
            reloadBankIdentifierTypes();
            // if the form element with ID of "PayeeType" == 1
			if ( $("#PayeeType").val() == "1" )        // domestic, don't show
            {
                // if the DOM uses the "IAT" class, hide it
                // if the DOM uses the "ManagedPrenote" class, show it
                $('.IAT').hide();
                $('.NotIAT').show();
                $('.ManagedPrenote').show();
                $('.SameDayPrenote').show();
                ns.ach.payeeType = 1;
            	if($('#noPrenote').is(':checked'))
				{
            		 $('input:checkbox[name="prenoteType"]').attr("disabled", true);
				}

				$('input:radio[name="prenoteStatus"]').change(function(){


					var isPrenoteDisabled=true;
					<%	if(!disableSameDayPrenote){%>
							isPrenoteDisabled=false;
					<%	}%>

							if($(this).val() == 'PRENOTE_REQUESTED'){
							if(!isPrenoteDisabled){
									$('input:checkbox[name="prenoteType"]').attr("disabled", false);
							   }
							}
							else
							{
							if(!isPrenoteDisabled){
								  $('input:checkbox[name="prenoteType"]').attr("checked", false );
								  $('input:checkbox[name="prenoteType"]').attr("disabled", true);
							  }
							}
				});
            }
            else
            {
                // if the DOM uses the "IAT" class, show it
                // if the DOM uses the "ManagedPrenote" class, hide it
                updateTruncatedText("ALL");
                $('.IAT').show();
                $('.NotIAT').hide();
                $('.ManagedPrenote').hide();
                $('.SameDayPrenote').hide();
                updateAccountInfo(document.getElementById("BranchCountry"));
                ns.ach.payeeType = 2;
            }
        }

		/** This function is used to set DA old values for Participant type, secure payee,
		 account type, and bank identifier type. */
		function setDAOldValues() {
			if ($('#prenoteStatusCss').hasClass('sectionsubhead')) {
				$('#prenoteStatusCssOld').html('');
			}
			if ($('#payeeTypeCss').hasClass('sectionsubhead')) {
				$('#payeeTypeCssOld').html('');
			}
			if ($('#payeeGroupCss').hasClass('sectionsubhead')) {
				$('#payeeGroupCssOld').html('');
			}
			if ($('#bankIdentifierTypeCss').hasClass('sectionsubhead')) {
				$('#bankIdentifierTypeCssOld').html('');
			}
			if ($('#securePayeeCss').hasClass('sectionsubhead')) {
				$('#securePayeeCssOld').html('');
			}
			if ($('#accountTypeCss').hasClass('sectionsubhead')) {
				$('#accountTypeCssOld').html('');
			}
		}


        // -->
        </script>


<ffi:cinclude value1="${AddEditACHPayee.CanModifyDestination}" value2="false">
    <% canModifyDestination = false; %>
</ffi:cinclude>


<div align="center">
	<div align="left">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td class="columndata lightBackground">

            <s:actionerror />

<% if (isEdit) { %>
            <input type="hidden" name="isEdit" value="true"/>
<% } else { %>
            <input type="hidden" name="isAdd" value="true"/>
<% } %>
        <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    <div align="left">

                        <table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData" id="achPayeeAddEditDiv">
                            <tr>
                                <td colspan="4" align="center"><ul id="formerrors"></ul></td>
                            </tr>
                            <tr><td style="cursor:auto" class="sectionhead nameTitle"><h2 id="addAchPayeeParticipantInfoHeader"><s:text name="jsp.ach.Participant.Info" /></h2></td></tr>
                           	<tr>
                           		<td id="achPayeeTypeLabel" class="columndata" width="25%">
                                       <span id="payeeTypeCss" class='<ffi:getPendingStyle fieldname="payeeType" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>'><label for="PayeeType"> <s:text name="jsp.ach.Participant.Type" /></lable></span>
                                </td>
                                <td id="achCompanyNameLabel" width="25%" class="<ffi:getPendingStyle fieldname="companyID" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>">
                                	<label for="CompanyID"><s:text name="ach.grid.companyName" /></label><span class="required"  title="required">*</span>
                                </td>
                                <td id="achPayeeNameLabel" width="25%">

                                   <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.name}" oldFieldValue="${AddEditACHPayee.oldDAPayee.name}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for="Name"><s:text name="jsp.ach.Participant.Name" /> </label></span><span class="required"  title="required">*</span>
                                </td>
                                <td id="achPayeeNickNameLabel" width="25%">
                                   <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.nickName}" oldFieldValue="${AddEditACHPayee.oldDAPayee.nickName}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for="NickName"><s:text name="jsp.default_294" /></label></span>
                                </td>
                           	</tr>
                                        <tr>
                                            <td class="columndata">
                                                <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                                <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                                <ffi:process name="ACHResource" />
                                                <ffi:cinclude value1="1" value2="${AddEditACHPayee.PayeeTypeValue}">
                                                	<script>
                                                		ns.ach.payeeType = 1;
                                                	</script>
                                                </ffi:cinclude>
                                                <ffi:cinclude value1="2" value2="${AddEditACHPayee.PayeeTypeValue}">
                                                	<script>
                                                		ns.ach.payeeType = 2;
                                                	</script>
                                                </ffi:cinclude>
                                                <div class="selectBoxHolder">
                                                <select id="PayeeType" class="txtbox" <%=isEdit || isDAEdit?"disabled":""%> name="payeeType" value="<ffi:getProperty name="AddEditACHPayee" property="PayeeType"/>" onChange="showHide();" aria-labelledby="PayeeType">
                                                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeType1"/>
                                                    <option value="1" <ffi:cinclude value1="1" value2="${AddEditACHPayee.PayeeTypeValue}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
<ffi:cinclude ifEntitled="<%=com.ffusion.csil.core.common.PaymentEntitlementsDefines.IAT_ADDENDA%>">
                                                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeType2"/>
                                                    <option value="2" <ffi:cinclude value1="2" value2="${AddEditACHPayee.PayeeTypeValue}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
</ffi:cinclude>
                                                </select>
                                                </div>
                                            </td>
                                            <td class="columndata">
					                            <select id="CompanyID" class="txtbox" <%=isEdit || isDAEdit?"disabled":""%> name="companyID"  aria-labelledby="CompanyID" aria-required="true">
					                            <ffi:cinclude value1="${AddEditACHPayee.CompanyID}" value2="" operator="equals" >
					                                <option value='' ><s:text name="ach.select.an.ACH.company" /></option>
					                            </ffi:cinclude>
					                            <ffi:list collection="ACHCOMPANIES" items="ACHCompany">
					                            <% entitled = false; %>
					                            <ffi:cinclude value1="${ACHCompany.ACHPaymentEntitled}" value2="TRUE">
					                                <ffi:cinclude ifEntitled="<%= entToCheck1 %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCompany.CompanyID}" checkParent="true">
					                                    <% entitled = true; %>
					                                </ffi:cinclude>
					                                <ffi:cinclude ifEntitled="<%= entToCheck2 %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCompany.CompanyID}" checkParent="true">
					                                    <% entitled = true; %>
					                                </ffi:cinclude>
					                            </ffi:cinclude>
					                            <% if (entitled || isEdit || isDAEdit) { %>
					                                <option value='<ffi:getProperty name="ACHCompany" property="ID" />' <ffi:cinclude value1="${AddEditACHPayee.CompanyID}" value2="${ACHCompany.ID}">selected</ffi:cinclude>><ffi:getProperty name="ACHCompany" property="CompanyName" />-<ffi:getProperty name="ACHCompany" property="CompanyID" /></option>
					                            <% } %>
					                            </ffi:list>
					                            </select>
					                        </td>
					                        <td class="columndata">
                                                <input id="Name" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" style="width:240px;"  maxlength="22" name="name" value="<ffi:getProperty name="AddEditACHPayee" property="Name"/>">
                                            </td>
                                        	<td class="columndata" >
                                                <input id="NickName" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="50" name="nickName" style="width:240px;" value="<ffi:getProperty name="AddEditACHPayee" property="NickName"/>">
                                            </td>
                                        </tr>
					                    <tr>
					                    	 <td>
					                    	 	<span id="payeeTypeError"></span>
					                    	 	<!--<span id="payeeTypeCssOld" class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="PayeeType" /></span>-->
					                    	 </td>
					                         <td>
					                         	<span id="companyIDError"></span>
					                         	<!--<span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="CompanyID" /></span>-->
					                         </td>
                                            <td>
                                                <span id="nameError"></span>
                                                <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
												<ffi:cinclude value1="${AddEditACHPayee.name}" value2="${AddEditACHPayee.oldDAPayee.name}" operator="notEquals">
													<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.name" /></span>
												</ffi:cinclude>
												</ffi:cinclude>
                                            </td>
                                            <td>
                                                <span id="nickNameError"></span>
                                                <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
												<ffi:cinclude value1="${AddEditACHPayee.nickName}" value2="${AddEditACHPayee.oldDAPayee.nickName}" operator="notEquals">
													<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.nickName" /></span>
												</ffi:cinclude>
												</ffi:cinclude>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="achPayeeIdentificationNoLabel">
                                                <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.userAccountNumber}" oldFieldValue="${AddEditACHPayee.oldDAPayee.userAccountNumber}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for="UserAccountNumber"><s:text name="jsp.ach.Identification" /></label></span><span class="NotIAT required" title="required">*</span>
                                            </td>
                                            <td id="achPayeeScopeLabel" class="columndata">
                                                <span id="payeeGroupCss" class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.payeeGroup}" oldFieldValue="${AddEditACHPayee.oldDAPayee.payeeGroup}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for="payeeGroupId" ><s:text name="jsp.ach.Participant.Scope" /></label></span>
                                            </td>
                                            <td id="achPayeePrenoteLabel" class="ManagedPrenote">
                                         		<span id="prenoteStatusCss" class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.prenoteStatus}" oldFieldValue="${AddEditACHPayee.oldDAPayee.prenoteStatus}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for=""> <s:text name="ach.grid.prenote" /> </label></span>
		                                     </td>
		                                     <td id="achPayeeSecureLabel" class="columndata">
		                                         <span id="securePayeeCss" class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.securePayee}" oldFieldValue="${AddEditACHPayee.oldDAPayee.securePayee}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for=""><s:text name="jsp.ach.Secure.Participant"/></label></span>
		                                     </td>
                                        </tr>
                                        <tr>
                                        	<td class="columndata">
                                                <input id="UserAccountNumber" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="15" style="width:240px;" name="userAccountNumber" value="<ffi:getProperty name="AddEditACHPayee" property="UserAccountNumber"/>">
                                            </td>
                                            <td class="columndata">
								                <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
								                <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
								                <ffi:process name="ACHResource" />
                                                <select id="payeeGroupId" class="txtbox" name="payeeGroup" value="<ffi:getProperty name="AddEditACHPayee" property="PayeeGroup"/>"  aria-labelledby="payeeGroupId">
                                                        <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
                                                            <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup1"/>
                                                            <option value="1" <ffi:cinclude value1="1" value2="${AddEditACHPayee.PayeeGroupValue}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                                                        </ffi:cinclude>
                                                        <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup2"/>
                                                        <option value="2" <ffi:cinclude value1="2" value2="${AddEditACHPayee.PayeeGroupValue}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
														<s:if test="%{!consumerUser}">
                                                        <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup3"/>
                                                        <option value="3" <ffi:cinclude value1="3" value2="${AddEditACHPayee.PayeeGroupValue}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
														</s:if>
                                                </select>
                                            </td>
                                            <td class="columndata ManagedPrenote">
		                                         <input id="noPrenote" type="radio" name="prenoteStatus"  value="PRENOTE_NOT_REQUIRED" border="0" <ffi:cinclude value1="${AddEditACHPayee.PrenoteStatus}" value2="PRENOTE_NOT_REQUIRED" operator="equals" >checked</ffi:cinclude> <ffi:cinclude value1="${AddEditACHPayee.PrenoteStatus}" value2="PRENOTE_SUCCEEDED" operator="equals" >checked</ffi:cinclude>><s:text name="jsp.ach.Not.Required" />
		                                         <input id="requestPrenote" type="radio" name="prenoteStatus"  value="PRENOTE_REQUESTED" border="0" <ffi:cinclude value1="${AddEditACHPayee.PrenoteStatus}" value2="PRENOTE_PENDING" operator="equals" >checked</ffi:cinclude><ffi:cinclude value1="${AddEditACHPayee.PrenoteStatus}" value2="PRENOTE_REQUESTED" operator="equals" >checked</ffi:cinclude>><s:text name="jsp.ach.Requested"/>
		                                         <input type="hidden" name="prenoteSubmitDate" value="<%=(new com.ffusion.beans.DateTime(java.util.Locale.getDefault()))%>" />
		                                         	<%if(showSameDayPrenote){%>

														<div class="SameDayPrenote">


																<%boolean sameDayPrenoteChecked=false;%>
																<ffi:cinclude value1="${AddEditACHPayee.PrenoteType}" value2="SameDay Prenote" operator="equals" >
																<%sameDayPrenoteChecked=true;%>
																</ffi:cinclude>

																<%-- Show only when prenote status is still prenote pending --%>
																<ffi:cinclude value1="${AddEditACHPayee.PrenoteStatus}" value2="PRENOTE_SUCCEEDED" operator="equals" >
																	<%sameDayPrenoteChecked=false;%>
																</ffi:cinclude>

				                                                <input type="checkbox" name="prenoteType" value="SameDay Prenote" border="0"  <%if(sameDayPrenoteChecked){%>checked<%}%> <%if(disableSameDayPrenote){%> disabled <%}%>><!--L10NStart-->Same Day Prenote<!--L10NEnd-->
														</div>

													<%if(disableSameDayPrenote){%>
													<div class="SameDayPrenote">
													<span  align="left" style="color:red">
													<b><!--L10NStart-->Same Day Prenote Cut-off has passed for Today<!--L10NEnd--></b><br/></span>
													</div>
														<%}%>
														<%}%>
		                                     </td>
		                                     <td class="columndata">
											 <s:if test="%{consumerUser}">
											  <input id="normalPayee" type="radio" name="securePayee" value="false" border="0" disabled checked ><s:text name="jsp.ach.Normal" />
											 </s:if>
											 <s:else>
											  <input id="securePayee" type="radio" name="securePayee" value="true" border="0" <ffi:cinclude value1="${AddEditACHPayee.SecurePayee}" value2="true" operator="equals" >checked</ffi:cinclude>><s:text name="jsp.ach.Secure" />
		                                         <input id="normalPayee" type="radio" name="securePayee" value="false" border="0" <ffi:cinclude value1="${AddEditACHPayee.SecurePayee}" value2="false" operator="equals" >checked</ffi:cinclude>><s:text name="jsp.ach.Normal" />
											 </s:else>

		                                     </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span id="userAccountNumberError"></span>
                                                <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
												<ffi:cinclude value1="${AddEditACHPayee.userAccountNumber}" value2="${AddEditACHPayee.oldDAPayee.userAccountNumber}" operator="notEquals">
									         		<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.userAccountNumber" /></span>
									         	</ffi:cinclude>
												</ffi:cinclude>
                                            </td>
                                            <td>
                                         		<span id="payeeGroupError"></span>
                                         		<ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
												<ffi:cinclude value1="${AddEditACHPayee.payeeGroup}" value2="${AddEditACHPayee.oldDAPayee.payeeGroup}" operator="notEquals">
									         		<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.payeeGroup" /></span>
									         	</ffi:cinclude>
												</ffi:cinclude>
	                                     	</td>
	                                     	<td class="ManagedPrenote">
		                                        <span id="prenoteStatusError"></span>
                                         		<ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
												<ffi:cinclude value1="${AddEditACHPayee.prenoteStatus}" value2="${AddEditACHPayee.oldDAPayee.prenoteStatus}" operator="notEquals">
													<span id="prenoteStatusCssOld" class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.prenoteStatus" /></span>
									         	</ffi:cinclude>
												</ffi:cinclude>
	                                     	</td>
	                                     	<td>
	                                         <span id="securePayeeError"></span>
	                                         <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
											 <ffi:cinclude value1="${AddEditACHPayee.securePayee}" value2="${AddEditACHPayee.oldDAPayee.securePayee}" operator="notEquals">
									         	<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.securePayee" /></span>
									         </ffi:cinclude>
											 </ffi:cinclude>
	                                     	</td>
                                        </tr>
                                        <tr>

<% if (isEdit) { %>
                                        <tr>
                                            <td class="sectionsubhead" >
                                               <s:text name="jsp.default_398" />:
                                            </td>
                                            <td class="sectionsubhead ManagedPrenote" nowrap>
                                                <s:text name="jsp.ach.Last.Prenote.Status" />:
                                            </td>
                                            <td class="sectionsubhead ManagedPrenote" nowrap>
                                                <s:text name="jsp.default_508" />:
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="columndata">
                                                <%	String userID = "";
                                                    String userName = "";
                                                %>
                                                    <ffi:getProperty name="AddEditACHPayee" property="SubmittedBy" assignTo="userID"/>
                                                <%
                                                    // Need to find the UserName for the User
                                                    if (userID != null && userID.length() > 0) {
                                                        userName = userID;				// in case it is not an integer
                                                        try
                                                        {
                                                            // It's a business employee, find username
                                                            int id = Integer.parseInt(userID);
                                                            CustomerAdapter customerAdapter = (CustomerAdapter)SpringUtil.getBean(CustomerAdapter.class);
                                                            User user = customerAdapter.getUserById(id);
                                                            userName = user.getName();
                                                        } catch (Exception e)
                                                        {
                                                        }
                                                    }
                                                %>
                                                <div align="left">
                                                    <%= userName %>
                                                </div>
                                            </td>
                                            <td class="columndata ManagedPrenote">
                                                <ffi:getProperty name="OrigEditACHPayeePrenoteStatus" />
                                            </td>
                                            <td class="columndata ManagedPrenote">
                                                <ffi:setProperty name="AddEditACHPayee" property="DateFormat" value="${UserLocale.DateFormat}" />
                                                <ffi:getProperty name="AddEditACHPayee" property="PrenoteSubmitDate" />
                                            </td>
                                            <td></td>
                                        </tr>
<% } %>

                            <tr class="IAT">
	                            <td id="achPayeeDestinationCountryLabel" nowrap>
	                                <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.destinationCountry}" oldFieldValue="${AddEditACHPayee.oldDAPayee.destinationCountry}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.ach.Destination.Country"/> </span><span class="required">*</span>
	                            </td>
	                            <td id="achPayeeBranchCountryLabel">
	                                <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.bankCountryCode}" oldFieldValue="${AddEditACHPayee.oldDAPayee.bankCountryCode}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.ach.Branch.Country" /> </span><span class="required">*</span>
	                            </td>
	                            <td id="achPayeeCurrencyCodeLabel" nowrap>
	                                <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.currencyCode}" oldFieldValue="${AddEditACHPayee.oldDAPayee.currencyCode}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.default_126" /> </span><span class="required">*</span>
	                            </td>
	                            <td id="achPayeeFXMethodLabel" nowrap>
	                                <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.foreignExchangeMethod}" oldFieldValue="${AddEditACHPayee.oldDAPayee.foreignExchangeMethod}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.ach.Foreign.Exchange.Method" /> </span><span class="required">*</span>
	                            </td>
	                        </tr>
	                        <tr class="IAT">
	                            <td class="columndata">
	                                <div class="selectBoxHolder">
		                                <select id="COUNTRY" <%=canModifyDestination?"":"disabled"%> class="txtbox" name="destinationCountry" onchange="updateCurrencyInfo(this);updateTruncatedText('COUNTRY');">
		                                    <option value=""><!--L10NStart-->(select Country)<!--L10NEnd--></option>
		                                    <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
		                                       <% String countryName =""; %>
		                                       <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
		                                       <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName" />
		                                       <%
		                                           if (countryName == null) countryName = "";
		                                       %>
		                                       <option title="<%=countryName%>" value="<ffi:getProperty name="countryCode"/>"
		                                           <ffi:cinclude value1="${countryCode}" value2="${AddEditACHPayee.BankCountryCode}">
		                                               selected
		                                           </ffi:cinclude>>
		                                           <%
		                                               if (countryName.length() > 25)
		                                               {
		                                                   countryName = countryName.substring(0, 25);
		                                               }
		                                           %>
		                                           <%=countryName%>
		                                       </option>
		                                    </ffi:list>
		                                </select>
		                             </div>
									 <div id="COUNTRYTruncatedText" style="display:block; clear:both;"></div>
	                            </td>
	                            <td class="columndata">
	                                <select id="BranchCountry" class="txtbox" name="bankCountryCode" onchange="userUpdatedBC();updateAccountInfo(this);updateTruncatedText('BranchCountry');">
	                                <option value=""><!--L10NStart-->(select Country)<!--L10NEnd--></option>
	                                <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
	                                    <% String countryName =""; %>
	                                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
	                                    <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName" />
	                                    <%
	                                        if (countryName == null) countryName = "";
	                                    %>
	                                    <option title="<%=countryName%>" value="<ffi:getProperty name="countryCode"/>"
	                                        <ffi:cinclude value1="${countryCode}" value2="${AddEditACHPayee.BankCountryCode}">
	                                            selected
	                                        </ffi:cinclude>>
	                                        <%
	                                            if (countryName.length() > 25)
	                                            {
	                                                countryName = countryName.substring(0, 25);
	                                            }
	                                        %>
	                                        <%=countryName%>
	                                    </option>
	                                    </ffi:list>
	                                </select>

	                                <div id="BranchCountryTruncatedText"></div>
	                            </td>
	                            <td class="columndata">
	                                            <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	                                            <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
	                                            <ffi:process name="ACHResource" />
	                                <select id="CURRENCY" <%=canModifyDestination?"":"disabled"%> class="txtbox" name="currencyCode" onchange="updateForeignExchangeMethod(this);updateTruncatedText('CURRENCY');">
	                                    <option value="">(<s:text name="ach.currencySelect.defaultOption" />)</option>
	                                    <ffi:list collection="ACHIAT_DestinationCurrencyCodes" items="CurrencyCode">
	                                                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${CurrencyCode}"/>
	                                                    <option value="<ffi:getProperty name="CurrencyCode" />" <ffi:cinclude value1="${CurrencyCode}" value2="${AddEditACHPayee.CurrencyCode}">selected</ffi:cinclude>><ffi:getProperty name="CurrencyCode" />-<ffi:getProperty name="ACHResource" property="Resource"/></option>
	                                    </ffi:list>
	                                </select>

	                                <div id="CURRENCYTruncatedText"></div>
	                            </td>
	                            <td class="columndata">
	                                <select id="FOREIGNEXCHANGE" <%=canModifyDestination?"":"disabled"%> class="txtbox" name="foreignExchangeMethod">
	                                <ffi:cinclude value1="${AddEditACHPayee.DestinationCountry}" value2="" operator="equals" >
	                                <option value="">(<s:text name="ach.currencySelect.defaultOption" />)</option>
	                                </ffi:cinclude>
	                                <ffi:cinclude value1="${AddEditACHPayee.DestinationCountry}" value2="" operator="notEquals" >
	                                    <ffi:list collection="ACHIAT_ForeignExchangeIndicatorSupported" items="ForeignExchangeType">
	                                        <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${ForeignExchangeType}"/>
	                                        <ffi:cinclude value1="${ForeignExchangeType}" value2="${AddEditACHPayee.ForeignExchangeMethod}">
	                                            <option value="<ffi:getProperty name="ForeignExchangeType"/>" selected><ffi:getProperty name="ForeignExchangeType"/>-<ffi:getProperty name="ACHResource" property="Resource"/></option>
	                                        </ffi:cinclude>
	                                    </ffi:list>
	                                </ffi:cinclude>
	                            </select>
	                            </td>
                        </tr>
                         <tr class="IAT">
                             <td>
                                 <span id="destinationCountryError"></span>
                                 <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								 <ffi:cinclude value1="${AddEditACHPayee.destinationCountry}" value2="${AddEditACHPayee.oldDAPayee.destinationCountry}" operator="notEquals">
					         	 	<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.destinationCountry" /></span>
					         	 </ffi:cinclude>
								 </ffi:cinclude>
                             </td>
                             <td>
                                 <span id="bankCountryCodeError"></span>
                                 <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								 <ffi:cinclude value1="${AddEditACHPayee.bankCountryCode}" value2="${AddEditACHPayee.oldDAPayee.bankCountryCode}" operator="notEquals">
					         	 	<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.bankCountryCode" /></span>
					         	 </ffi:cinclude>
								 </ffi:cinclude>
                             </td>
                             <td>
                                 <span id="currencyCodeError"></span>
                                 <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								 <ffi:cinclude value1="${AddEditACHPayee.currencyCode}" value2="${AddEditACHPayee.oldDAPayee.currencyCode}" operator="notEquals">
					         		<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.currencyCode" /></span>
					         	 </ffi:cinclude>
								 </ffi:cinclude>
                             </td>
                              <td>
                                 <span id="foreignExchangeMethodError"></span>
                                 <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								 <ffi:cinclude value1="${AddEditACHPayee.foreignExchangeMethod}" value2="${AddEditACHPayee.oldDAPayee.foreignExchangeMethod}" operator="notEquals">
					         		<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.foreignExchangeMethod" /></span>
					         	 </ffi:cinclude>
								 </ffi:cinclude>
                             </td>
                         </tr>
                         <tr>
                         	<td colspan="4" class="IAT">
                         	<%-- todo: here is the International Address information --%>
                            <tr class="IAT"><td valign="top" colspan="4"><hr class="thingrayline"></td></tr>
                            <tr class="IAT">
                                <td id="achPayeeAddressLabel" colspan="4" class="nameTitle"><!--L10NStart-->Participant Address<!--L10NEnd--></td>
                            </tr>
                           	<tr class="IAT">
                               <td id="achPayeeStreetLabel" width="25%">
                                   <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.street}" oldFieldValue="${AddEditACHPayee.oldDAPayee.street}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.ach.Street.Address" /></span><span class="required">*</span>
                               </td>
                               <td id="achPayeeCityLabel" width="25%">
                                 <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.city}" oldFieldValue="${AddEditACHPayee.oldDAPayee.city}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.default_101" /></span><span class="required">*</span>
                              </td>
                              <td id="achPayeeCountryLabel" width="25%">
                                  <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.country}" oldFieldValue="${AddEditACHPayee.oldDAPayee.country}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.default_115" /> </span><span class="required">*</span>
                              </td>
                              <td id="achPayeePostalLabel" width="25%">
                                  <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.zipCode}" oldFieldValue="${AddEditACHPayee.oldDAPayee.zipCode}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.ach.Postal.Code" /> </span><span class="required">*</span>
                              </td>
                           </tr>
                           <tr class="IAT">
                               <td class="columndata">
                                   <input id="achPayeeStreet" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="35" name="street" value="<ffi:getProperty name="AddEditACHPayee" property="Street"/>"  style="width:240px">
                               </td>
                               <td class="columndata">
                                   <input id="achPayeeCity" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="14" name="city" value="<ffi:getProperty name="AddEditACHPayee" property="City"/>"  style="width:240px">
                               </td>
                               <td class="columndata">
                                   <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                   <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                   <ffi:process name="ACHResource" />
                                   <select id="achPayeeCountry"  class="txtbox" name="country" onchange="updateTruncatedText('achPayeeCountry');" >
                                   <option value=""><!--L10NStart-->(select Country)<!--L10NEnd--></option>
                                   <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
                                       <% String countryName2 =""; %>
                                       <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
                                       <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName2" />
                                       <%
                                           if (countryName2 == null) countryName2 = "";
                                       %>
                                       <option title="<%=countryName2%>" value="<ffi:getProperty name="countryCode"/>"
                                           <ffi:cinclude value1="${countryCode}" value2="${AddEditACHPayee.country}">
                                               selected
                                           </ffi:cinclude>>
                                           <%
                                               if (countryName2.length() > 25)
                                               {
                                                   countryName2 = countryName2.substring(0, 25);
                                               }
                                           %>
                                           <%=countryName2%>
                                       </option>
                                       </ffi:list>
                                   </select>
								   <div id="achPayeeCountryTruncatedText"></div>
                               </td>
                               <td class="columndata">
                                    <input id="achPayeePostal" class="ui-widget-content ui-corner-all txtbox" type="text" maxlength="9" name="zipCode" value="<ffi:getProperty name="AddEditACHPayee" property="ZipCode"/>"  style="width:240px">
                                </td>
                           </tr>
                            <tr class="IAT">
                                <td>
                                    <span id="streetError"></span>
                                    <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
									<ffi:cinclude value1="${AddEditACHPayee.street}" value2="${AddEditACHPayee.oldDAPayee.street}" operator="notEquals">
						         		<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.street" /></span>
						         	</ffi:cinclude>
									</ffi:cinclude>
                                </td>
                                <td>
                                    <span id="cityError"></span>
                                    <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
									<ffi:cinclude value1="${AddEditACHPayee.city}" value2="${AddEditACHPayee.oldDAPayee.city}" operator="notEquals">
						         		<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.city" /></span>
						         	</ffi:cinclude>
									</ffi:cinclude>
                                </td>
                                <td>
                                  <span id="countryError"></span>
                                  <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
								  <ffi:cinclude value1="${AddEditACHPayee.country}" value2="${AddEditACHPayee.oldDAPayee.country}" operator="notEquals">
					         		<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.country" /></span>
					         	  </ffi:cinclude>
								  </ffi:cinclude>
                              </td>
                              <td>
                                  <span id="zipCodeError"></span>
                                  	<ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
									<ffi:cinclude value1="${AddEditACHPayee.zipCode}" value2="${AddEditACHPayee.oldDAPayee.zipCode}" operator="notEquals">
										<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.zipCode" /></span>
						         	</ffi:cinclude>
									</ffi:cinclude>
                              </td>
                            </tr>
                            <tr class="IAT">
                                <td id="achPayeeStateLabel" colspan="4">
                                    <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.state}" oldFieldValue="${AddEditACHPayee.oldDAPayee.state}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.default_387" /> </span></div>
                                </td>
                           </tr>
                           <tr class="IAT">
                                <td class="columndata" colspan="4">
                                    <input id="achPayeeState" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="19" name="state" value="<ffi:getProperty name="AddEditACHPayee" property="State"/>"  style="width:240px">
                                </td>
                            </tr>
                            <tr class="IAT">
                                <td colspan="4">
                                    <span id="stateError"></span>
                                    <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
									<ffi:cinclude value1="${AddEditACHPayee.state}" value2="${AddEditACHPayee.oldDAPayee.state}" operator="notEquals">
										<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.state" /></span>
						         	</ffi:cinclude>
									</ffi:cinclude>
                                </td>
                            </tr>
                         <tr><td valign="top" colspan="4"><hr class="thingrayline"></td></tr>
                         <tr><td style="cursor:auto" class="sectionhead nameTitle"><h2 id="addAchPayeeHeader">Bank Info</h2></td></tr>
                          <tr>
                              <td id="achPayeeBankNameLabel">
                                  <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.bankName}" oldFieldValue="${AddEditACHPayee.oldDAPayee.bankName}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for="BANKNAME"> <s:text name="jsp.default_63"/> </label></span><span class="required" title="required">*</span>
                              </td>
                              <td id="achPayeeBankIdentifierLabel">
                                  <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.routingNumber}" oldFieldValue="${AddEditACHPayee.oldDAPayee.routingNumber}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for="ROUTINGNUMBER"><s:text name="jsp.ach.Bank.Identifier"/></label></span><span class="required" title="required">*</span>
                              </td>
                              <td id="achPayeeAccountNoLabel">
                                  <span class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.accountNumber}" oldFieldValue="${AddEditACHPayee.oldDAPayee.accountNumber}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for="ACCOUNTNUMBER"><s:text name="jsp.default_16"/></label></span><span class="required" title="required">*</span>
                              </td>
                              <td id="achPayeeAccountTypeLabel">
                                  <span id="accountTypeCss" class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.accountType}" oldFieldValue="${AddEditACHPayee.oldDAPayee.accountType}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><label for="AccountType"><s:text name="jsp.default_20" /></label></span>
                              </td>
                          </tr>
                          <tr>
                          	<td class="columndata">
                                  <input id="BANKNAME" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" style="width:170px;" maxlength="64" name="bankName" value="<ffi:getProperty name="AddEditACHPayee" property="BankName"/>">

                                  <sj:a id="searchBank" button="true" title="SEARCH" onClickTopics="achPayee_selectDestBankTopics"><s:text name="jsp.default_373" /></sj:a>
                              </td>

                              <td class="columndata" align="left">
                                 <input id="ROUTINGNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="34" name="routingNumber" value="<ffi:getProperty name="AddEditACHPayee" property="RoutingNumber"/>" style="width:240px">
                             </td>
                             <td class="columndata">
                                <input id="ACCOUNTNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="17" name="accountNumber" value="<ffi:getProperty name="AddEditACHPayee" property="AccountNumber"/>" style="width:240px">
                            </td>
                            <td class="columndata">
                               <select class="txtbox" name="accountType" id="AccountType" value="<ffi:getProperty name="AddEditACHPayee" property="AccountType"/>" aria-labelledby="AccountType">
                                   <% int acctType = 0; %>
                                   <ffi:list collection="ACHAccountTypes" items="ACHAccountType">
                                       <% acctType++; %>
                                       <option value="<%=acctType%>" <ffi:cinclude value1="${ACHAccountType}" value2="${AddEditACHPayee.AccountType}">selected</ffi:cinclude>><ffi:getProperty name="ACHAccountType"/></option>
                                   </ffi:list>
                               </select>
                           </td>
                          </tr>
                            <tr>
                                <td>
                                    <span id="bankNameError"></span>
                                    <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
									<ffi:cinclude value1="${AddEditACHPayee.bankName}" value2="${AddEditACHPayee.oldDAPayee.bankName}" operator="notEquals">
										<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.bankName" /></span>
						         	</ffi:cinclude>
									</ffi:cinclude>
                                </td>
                                <td>
                                    <span id="routingNumberError"></span>
                                    <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
									<ffi:cinclude value1="${AddEditACHPayee.routingNumber}" value2="${AddEditACHPayee.oldDAPayee.routingNumber}" operator="notEquals">
										<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.routingNumber" /></span>
						         	</ffi:cinclude>
									</ffi:cinclude>
                                </td>
                                <td>
                                    <span id="accountNumberError"></span>
                                    <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
									<ffi:cinclude value1="${AddEditACHPayee.accountNumber}" value2="${AddEditACHPayee.oldDAPayee.accountNumber}" operator="notEquals">
										<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.accountNumber" /></span>
						         	</ffi:cinclude>
									</ffi:cinclude>
                                </td>
                                <td>
                                    <span id="accountTypeError"></span>
                                    <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
									<ffi:cinclude value1="${AddEditACHPayee.accountType}" value2="${AddEditACHPayee.oldDAPayee.accountType}" operator="notEquals">
										<span class="sectionhead_greyDA"><ffi:getProperty name="AddEditACHPayee" property="oldDAPayee.accountType" /></span>
						         	</ffi:cinclude>
									</ffi:cinclude>
                                </td>
                             </tr>
                             <tr class="IAT">
                                 <td id="achPayeeBankIdentifierTypeLabel">
                                     <span id="bankIdentifierTypeCss" class='<ffi:daPendingStyle newFieldValue="${AddEditACHPayee.bankIdentifierType}" oldFieldValue="${AddEditACHPayee.oldDAPayee.bankIdentifierType}" daUserAction="${AddEditACHPayee.action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><s:text name="jsp.ach.Bank.Identifier.Type" /> </span><span class="required">*</span>
                                 </td>
                             </tr>
                             <tr class="IAT">
                             	<td class="columndata" colspan="4">
                                     <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                     <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                     <ffi:process name="ACHResource" />
                                     <select id="BankIdentifierType" class="txtbox" name="bankIdentifierType" value="<ffi:getProperty name="AddEditACHPayee" property="BankIdentifierType"/>">
                                         <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier01"/>
                                         <option value="01" <ffi:cinclude value1="01" value2="${AddEditACHPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                                         <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier02"/>
                                         <option value="02" <ffi:cinclude value1="02" value2="${AddEditACHPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                                         <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier03"/>
                                         <option value="03" <ffi:cinclude value1="03" value2="${AddEditACHPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                                         <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier99"/>
                                         <option value="99" <ffi:cinclude value1="99" value2="${AddEditACHPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                                     </select>

                                 </td>
                             </tr>
                            <tr class="IAT">
                                <td colspan="4">
                                    <span id="bankIdentifierTypeError"></span>
                                    <ffi:cinclude value1="${AddEditACHPayee.action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
									<ffi:cinclude value1="${AddEditACHPayee.bankIdentifierType}" value2="${AddEditACHPayee.oldDAPayee.bankIdentifierType}" operator="notEquals">
	                                    <span id="bankIdentifierTypeCssOld" class="sectionhead_greyDA">
	                                       <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	                                       <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier${oldDAObject.BankIdentifierType}"/>
	                                       <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
	                                       <ffi:process name="ACHResource" />
	                                       <ffi:getProperty name="ACHResource" property="Resource"/>
	                                       <ffi:removeProperty name="ACHResource"/>
	                                   </span>
                                   	</ffi:cinclude>
								   	</ffi:cinclude>
                                </td>
                            </tr>
                        </table>
                    </div>

<%-- End of International ACH Payee address section --%>
    </table>
<div align="center"><br><span class="required">* <!--L10NStart-->indicates a required field<!--L10NEnd--></span></div>
	</div>
</div>
<%-- Start: Clean up DA information --%>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
<ffi:removeProperty name="<%=IDualApprovalConstants.OLD_DA_OBJECT%>"/>
<script>
	$("#PayeeType").selectmenu({width: 250});
	$("#CompanyID").selectmenu({width: 250});
	$("#payeeGroupId").selectmenu({width: 250});
	$("#AccountType").selectmenu({width: 250});
	$("#COUNTRY").selectmenu({width: 250});
	$("#BranchCountry").selectmenu({width: 250});
    $("#achPayeeCountry").selectmenu({width: 250});
	$("#CURRENCY").selectmenu({width: 250});
	$("#FOREIGNEXCHANGE").selectmenu({width: 250});
	$("#BankIdentifierType").selectmenu({width: 250});

    jQuery(document).ready(function () {
        showHide();
        setDAOldValues();
    });
</script>
