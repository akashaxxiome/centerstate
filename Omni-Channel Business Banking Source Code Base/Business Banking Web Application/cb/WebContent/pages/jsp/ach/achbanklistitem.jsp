<%@ page import="com.ffusion.beans.ach.ACHPayee"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_ACHbanklistitem" className="moduleHelpClass"/>
<ffi:removeProperty name="ACHParticipantBank"/>
<% String bankID = request.getParameter("SetACHParticipantBank.Id"); %>
<ffi:setProperty name="SetACHParticipantBank" property="Id" value="<%=bankID%>"/>
<ffi:process name="SetACHParticipantBank"/>

	<% String bankIDType = "";%>

   <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td align="left">
                <s:form name="achbanklistitem.jsp" method="post" id="ach_insertSelectedDestinationBankFormID">
                    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    <table border="0" cellspacing="0" cellpadding="3" width="100%">
                        <tr>
                            <td class="sectionsubhead" colspan="3" align="center">
	                            <span class="headerTitle">
	                            	<ffi:getProperty name="ACHParticipantBank" property="InstitutionName"/>
	                            </span>
                            </td>
                        </tr>
                        <tr>
					    	<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_31"/></p></td>
					    </tr>
						<tr>
					    	<td colspan="3">
					    		<table width="100%" border="0" cellspacing="0" cellpadding="5">
						    		<tr>
								    	<td width="40%" class="labelCls">Street</td>
								    	<td width="20%" class="labelCls"><s:text name="jsp.default_101"/></td>
								    	<td width="20%" class="labelCls"><s:text name="jsp.default_386"/></td>
								    	<td width="20%" class="labelCls"><s:text name="jsp.default_115"/></td>
								    </tr>
							    </table>	
					    	</td>
				    	</tr>
				    	<tr>
				    		<td colspan="3">
					    		<table width="100%" border="0" cellspacing="0" cellpadding="5">
						    		<tr>
							    		<td width="40%">
											<span class="columndata valueCls">
												<ffi:getProperty name="ACHParticipantBank" property="Street"/>
											</span>
										</td>
								    	<td width="20%" valign="top">
											<span class="columndata valueCls">
												<ffi:getProperty name="ACHParticipantBank" property="City"/>,&nbsp;
                            					<ffi:cinclude value1="${ACHParticipantBank.State}" value2="" operator="notEquals" ><ffi:getProperty name="ACHParticipantBank" property="State" /></ffi:cinclude>
                          						<ffi:cinclude value1="${ACHParticipantBank.State}" value2="" operator="equals" ><ffi:getProperty name="ACHParticipantBank" property="StateCode" /></ffi:cinclude>
											</span>
										</td>
								    	<td width="20%" valign="top">
											<span class="columndata valueCls">
                            					<ffi:cinclude value1="${ACHParticipantBank.State}" value2="" operator="notEquals" ><ffi:getProperty name="ACHParticipantBank" property="State" /></ffi:cinclude>
                          						<ffi:cinclude value1="${ACHParticipantBank.State}" value2="" operator="equals" ><ffi:getProperty name="ACHParticipantBank" property="StateCode" /></ffi:cinclude>
											</span>
										</td>
								    	<td width="20%" valign="top">
											<span class="columndata">
												<ffi:getProperty name="ACHParticipantBank" property="Country"/>
											</span>
										</td>
									</tr>
							    </table>	
					    	</td>	
					    </tr>
				    	<tr>
					    	<td colspan="4" height="20">&nbsp;</td>
					    </tr>
					    <tr>
					    	<td colspan="4"><p class="transactionHeading"><s:text name="admin.banklookup.identification.codes"/></p></td>
					    </tr>
					    <tr>
					    	<td colspan="4">
						    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
							    	<tr>
								    	<td width="33%"><s:text name="jsp.default_201"/></td>
								    	<td width="33%"><s:text name="jsp.default_99"/></td>
								    	<td width="33%"><s:text name="jsp.default_289"/></td>
								    </tr>
							    </table>
						    </td>	
					    </tr>
					    <tr>
					    	<td colspan="4">
						    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
							    	<tr>
								    	<td width="33%" valign="top">
									    	<span class="columndata"><ffi:getProperty name="ACHParticipantBank" property="AchRoutingNumber"/></span>
								    	</td>
								    	<td width="33%" valign="top">
								    		<span class="columndata">
												<%--displaying for all types as implemented in wires 
												<% if (bankIDType != null && bankIDType.equals(ACHPayee.PAYEE_BANK_IDENTIFIER_TYPE_BIC)) { 
					                                        <s:text name="ach.swiftid" />&nbsp;<ffi:getProperty name="ACHParticipantBank" property="SwiftBIC"/>
					                            <% } else {%>
					                            &nbsp;
					                            <% } %> --%>
					                            <ffi:getProperty name="ACHParticipantBank" property="chipsUID"/>
											</span>
								    	</td>
								    	<td width="33%" valign="top">
											<span class="columndata">
												<%-- displaying for all types as implemented in wires
												<% if (bankIDType != null && bankIDType.equals(ACHPayee.PAYEE_BANK_IDENTIFIER_TYPE_OTHER_RTN)) { %>
					                                        <s:text name="ach.nationalId" />&nbsp;<ffi:getProperty name="ACHParticipantBank" property="NationalID"/>
					                            <% } else {%>
					                            &nbsp;
					                            <% } %> --%>
					                            <ffi:getProperty name="ACHParticipantBank" property="NationalID"/>
				                            </span>
										</td>
								    </tr>
							    </table>
						    </td>	
					    </tr>
					    
					    
					    
                        <%-- <tr>
                            <td class="columndata" width="49%"><ffi:getProperty name="ACHParticipantBank" property="Street"/></td>
                            <td class="tbrd_l" rowspan="4" nowrap width="1"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="50" border="0"></td>
                            <td class="columndata" align="right" width="49%">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="columndata"><ffi:getProperty name="ACHParticipantBank" property="City"/>,&nbsp;
                            <ffi:cinclude value1="${ACHParticipantBank.State}" value2="" operator="notEquals" ><ffi:getProperty name="ACHParticipantBank" property="State" /></ffi:cinclude>
                            <ffi:cinclude value1="${ACHParticipantBank.State}" value2="" operator="equals" ><ffi:getProperty name="ACHParticipantBank" property="StateCode" /></ffi:cinclude>
                            </td>
                            <td align="right" class="columndata">
                            <% if (bankIDType != null && bankIDType.equals(ACHPayee.PAYEE_BANK_IDENTIFIER_TYPE_BIC)) { %>
                                        <s:text name="ach.swiftid" />&nbsp;<ffi:getProperty name="ACHParticipantBank" property="SwiftBIC"/>
                            <% } else {%>
                            &nbsp;
                            <% } %>
                        </tr>
                        <tr>
                            <td class="columndata"><ffi:getProperty name="ACHParticipantBank" property="ZipCode"/></td>
                            <td align="right" class="columndata"><s:text name="jsp.default_201" />&nbsp;<ffi:getProperty name="ACHParticipantBank" property="AchRoutingNumber"/></td>
                        </tr>
                        <tr>
                            <td class="columndata"><ffi:getProperty name="ACHParticipantBank" property="Country"/></td>
                            <td align="right" class="columndata">
                            <% if (bankIDType != null && bankIDType.equals(ACHPayee.PAYEE_BANK_IDENTIFIER_TYPE_OTHER_RTN)) { %>
                                        <s:text name="ach.nationalId" />&nbsp;<ffi:getProperty name="ACHParticipantBank" property="NationalID"/>
                            <% } else {%>
                            &nbsp;
                            <% } %>
                        </td>
                        </tr> --%>
                        <tr>
                            <td colspan="3" height="20">&nbsp;</td>
                        </tr>
                        <tr>
                        <td colspan="3" align="center"  class="ui-widget-header customDialogFooter">
                            <input type="hidden" name="bankName" value="<ffi:getProperty name="ACHParticipantBank" property="InstitutionName"/>"/>
                            <input type="hidden" name="rtNum" value="<ffi:getProperty name="ACHParticipantBank" property="AchRoutingNumber"/>"/>
                            <%--
                            <input class="submitbutton" name="insertInfo" type="submit" value="INSERT BANK INFO" border="0">&nbsp;&nbsp;
                            <input class="submitbutton" type="button" value="CLOSE" onclick="self.close();return false;">
                            --%>
                            <sj:a id="ach_insertSelectedBankForm"
                                button="true"
                                onClickTopics="ach_insertSelectedACHBankFormClickTopics"
                            ><s:text name="jsp.default_250" /><!-- INSERT BANK INFO -->
                            </sj:a>
                            &nbsp;&nbsp;
					        <script>
								ns.ach.researchBankListNewURL = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achbanklist.jsp?searchAgain=true"/>';
					 		</script>
                            <sj:a id="ach_searchAgainSelectedBankForm"
                                button="true"
                                onClickTopics="ach_researchBankListWithResultOnClickTopics"
                            ><s:text name="jsp.default_374" /><!-- SEARCH AGAIN -->
                            </sj:a>
                            &nbsp;&nbsp;
                            <sj:a id="ach_cancelSelectedBankForm"
                                button="true"
                                onClickTopics="ach_closeSelectedBankFormClickTopics"
                            ><s:text name="jsp.default_102" /><!-- CLOSE -->
                            </sj:a>
                        </td>
                        </tr>
<%--
                        <tr>
                        <td colspan="3" align="center"><input class="submitbutton" type="button" value="SEARCH AGAIN" onClick="document.location='/cb/pages/jsp/ach/achbanklist.jsp?searchAgain=true';">
                        </td>
                        </tr>
--%>
                    </table>
                     &nbsp;
                </s:form>
            </td>
        </tr>
    </table>
    </div>
