<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:help id="payments_ACHtransferInquiry" className="moduleHelpClass"/>
<ffi:cinclude value1="${messaging_init_touched}" value2="true" operator="notEquals" >
	<s:include value="%{#session.PagesPath}inc/init/messaging-init.jsp" />
</ffi:cinclude>


<div align="center">
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<ffi:cinclude value1="${TransactionType}" value2="<%= Integer.toString( com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_ACH_BATCH ) %>">
                <tr>
                    <td class="columndata lightBackground">
                        <div align="right">
                            <span class="sectionsubhead"><!-- L10NStart -->Topic:<!-- L10NEnd --></span></div>
                    </td>
                    <td  class="columndata lightBackground"><div align="left"><ffi:getProperty name="subject" /></div></td>
                </tr>
            </ffi:cinclude>
            <ffi:cinclude value1="${TransactionType}" value2="<%= Integer.toString( com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_ACH_BATCH ) %>">
                <tr>
                    <td class="columndata lightBackground">
                        <div align="right">
                            <span class="sectionsubhead"><!-- L10NStart -->Topic:<!-- L10NEnd --></span></div>
                    </td>
                    <td class="columndata lightBackground"><div align="left"><!-- L10NStart -->Recurring ACH Batch Inquiry<!-- L10NEnd --></div></td>
                </tr>
            </ffi:cinclude>
				
				
				<tr>
					<td class="columndata lightBackground" colspan="2">
						<div align="center">
							<span class="sectionsubhead"><s:text name="jsp.default.inquiry.message.RTE.label" /><span class="required">*</span></span>
						</div>
					</td>					
				</tr>
				<tr>
					<td class="columndata lightBackground" colspan="2">
						<s:form id="achinquiryForm" name="form4" namespace="/pages/jsp/ach" theme="simple" method="post" action="sendACHInqMessage_sendACHFundsInquiry.action" style="text-align: left;">
							<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<input type="hidden" name="fundTransactionMessage.subject" value="<ffi:getProperty name='Subject'/>"/>
							
							<s:hidden name="fundsID" value="%{fundsID}"></s:hidden>
							<s:hidden name="recurringId" value="%{recurringId}"></s:hidden>
							<s:hidden name="achType" value="%{achBatch.ACHType}"></s:hidden>
							<s:hidden name="fundTransactionMessage.from" value="%{#session.User.id}"></s:hidden>
							<s:hidden name="fundTransactionMessage.comment" value="%{achBatch.inquireComment}"></s:hidden>
							<div align="left">
								<p>
									<textarea class="txtbox ui-widget-content ui-corner-all" name="fundTransactionMessage.memo" rows="10" cols="75" style="overflow-y: auto;" wrap="virtual" ></textarea>
								</p>
							</div>
						</s:form>
					</td>
				</tr>
			</table>
			<div class="ffivisible" style="height:50px;">&nbsp;</div>
			<div  class="ui-widget-header customDialogFooter">
				<sj:a id="closeInquireACHLink" button="true"
					  onClickTopics="closeDialog" 
					  title="Cancel">
					  	<s:text name="jsp.default_82" />
				</sj:a>
					  
				<sj:a id="cancelACHSubmitButton"
					  formIds="achinquiryForm" 
					  targets="resultmessage" 
					  button="true" 
					  validate="true"
					  validateFunction="customValidation"
					  title="Send Inquiry"
					  onSuccessTopics="sendInquiryACHFormSuccess">
					  	<s:text name="jsp.default_378" />
				</sj:a>
			</div>
			<ffi:removeProperty name="TransactionType"/>
		</div>

<script>
	setTimeout(function(){ns.common.renderRichTextEditor('fundTransactionMessage.memo','600',true);}, 600);
</script>
