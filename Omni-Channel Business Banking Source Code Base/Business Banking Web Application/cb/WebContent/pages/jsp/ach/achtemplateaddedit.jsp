<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achtemplateaddedit" className="moduleHelpClass"/>

<%-- QTS 636909: remove possible recurring session values Frequency and NumberPayments --%>
<ffi:removeProperty name="Frequency"/>
<ffi:removeProperty name="NumberPayments"/>
<ffi:removeProperty name="isTemplateEdit"/>

<%--QTS 731286:Remove AddEditMultiACHTemplate, since we are working with single template   --%>
<ffi:removeProperty name="AddEditMultiACHTemplate"/>

<ffi:cinclude value1="${SetACHTemplate}" value2="" operator="equals">
	<ffi:object id="SetACHTemplate" name="com.ffusion.tasks.banking.SetFundsTransactionTemplate" scope="session"/>
	<ffi:setProperty name="SetACHTemplate" property="Name" value="ACHBatchTemplates" />
</ffi:cinclude>
<% 	
	
	String tempEntToCheck = null;
	int companyCount = 0;
	String companyID = null;

	if (request.getParameter("Initialize") != null) { %>
    <ffi:removeProperty name="Initialize" />
	<ffi:setProperty name="OpenEnded" value="false" />
    <ffi:cinclude value1="${subMenu}" value2="ach" operator="equals" >
        <% if (session.getAttribute("ACHPayees") == null){ %>
            <ffi:object id="GetACHPayees" name="com.ffusion.tasks.ach.GetACHPayees" scope="session"/>
            <ffi:process name="GetACHPayees"/>
            <ffi:removeProperty name="GetACHPayees"/>
            <ffi:setProperty name="ACHPayees" property="SortedBy" value="NAME" />
        <% } %>
    </ffi:cinclude>

<%	if (request.getParameter("TemplateSessionName") != null) { %>
	<ffi:setProperty name="SetACHTemplate" property="Name" value='<%=request.getParameter("TemplateSessionName") %>' />
	<ffi:removeProperty name="TemplateSessionName"/>
<% } %>

<%
	session.setAttribute("strutsActionName", "editACHBatchAction");
%>
<%  if (request.getParameter("TemplateID") != null) { %>
	<ffi:setProperty name="SetACHTemplate" property="ID" value='<%=request.getParameter("TemplateID") %>' />
	<ffi:process name="SetACHTemplate"/>
	<ffi:object id="AddEditACHTemplate" name="com.ffusion.tasks.ach.ModifyACHTemplate" scope="session"/>
	
<% 
		if(request.getParameter("isTemplateEdit") != null) {
			session.setAttribute("isTemplateEdit","true");
		}else{
			session.setAttribute("isTemplateEdit",null);
		}

	} else {
	
// we are creating a new template
%>
	<ffi:object id="AddEditACHTemplate" name="com.ffusion.tasks.ach.AddACHTemplate" scope="session"/>
	<ffi:setProperty name="AddEditACHTemplate" property="FundsType" value="ACH"/>
<ffi:cinclude value1="${subMenuSelected}" value2="child" >
	<ffi:setProperty name="AddEditACHTemplate" property="FundsType" value="CHILD"/>
	<ffi:setProperty name="AddEditACHTemplate" property="TemplateGroup" value="CHILD"/>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
	<ffi:setProperty name="AddEditACHTemplate" property="FundsType" value="TAX"/>
	<ffi:setProperty name="AddEditACHTemplate" property="TemplateGroup" value="TAX"/>
</ffi:cinclude>

	<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
		<span class="shortcutPathClass" style="display:none;">pmtTran_ach,ACHtemplatesLink,addSingleTemplateLink</span>
		<%
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_TEMPLATE );
		%>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
		<span class="shortcutPathClass" style="display:none;">pmtTran_tax,TaxtemplatesLink,addSingleTemplateLink</span>
		<%
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_TEMPLATE );
		%>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
		<span class="shortcutPathClass" style="display:none;">pmtTran_childsp,ChildsptemplatesLink,addSingleTemplateLink</span>
		<%
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_TEMPLATE );
		%>
	</ffi:cinclude>
	<ffi:list collection="ACHCOMPANIES" items="ACHCompany">
	<% boolean include_company = true; %>
    <ffi:cinclude ifEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCompany.CompanyID}" >
		<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
			<ffi:cinclude value1="${ACHCompany.ACHPaymentEntitled}" value2="FALSE">
				<% include_company = false; 		// there are no SEC Codes available for this ACH Company - don't display it %>
			</ffi:cinclude>
		</ffi:cinclude>
		<%
			if (include_company) { 
				// if there is only one ACH Company that we are entitled to use, set that ACH Company
				if (companyCount == 0)
				{
				%>
					<ffi:getProperty name="ACHCompany" property="CompanyID" assignTo="companyID" />
				<%
				}
				companyCount++;
			}
		%>
		</ffi:cinclude>
	</ffi:list>
	<% } %>

	<ffi:setProperty name="AddEditACHTemplate" property="Initialize" value="true"/>
	<ffi:setProperty name="AddEditACHTemplate" property="AchBatchName" value="AddEditACHBatch"/>
	<ffi:setProperty name="AddEditACHTemplate" property="SessionName" value="ACHBatchTemplates"/>
	<ffi:setProperty name="AddEditACHTemplate" property="AccountsCollection" value="BankingAccounts"/>
	<ffi:setProperty name="AddEditACHTemplate" property="Type" value="3"/>
	<ffi:process name="AddEditACHTemplate"/>

    <ffi:setProperty name="AddEditACHBatch" property="CreateNoAddFlag" value="true" />
    <ffi:setProperty name="AddEditACHBatch" property="DateFormat" value="${UserLocale.DateFormat}"/>

	<ffi:setProperty name="AddEditACHBatch" property="PayDate" value="${GetCurrentDate.Date}"/>
	<%
		if (companyCount == 1) {
	%>
		<ffi:setProperty name="AddEditACHBatch" property="CompanyID" value="<%=companyID%>"/>
	<% } %>

	<% } // initialize
	%>
	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
		<ffi:setProperty name="AddEditACHBatch" property="UseDefaultTaxForm" value="true" />
		<ffi:setProperty name="AddEditACHBatch" property="DefaultTaxFormID" value="${AddEditACHBatch.TaxForm.ID}"/>
	</ffi:cinclude>
	
<%
	session.setAttribute("FFIAddEditACHBatch", session.getAttribute("AddEditACHBatch"));
%>

<ffi:setProperty name="Initialize" value="true"/>
<s:include value="%{#session.PagesPath}/ach/achbatchaddedit.jsp"/>
