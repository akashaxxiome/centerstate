<%@ page import="com.ffusion.beans.ach.ACHBatch"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achpayeeeditsave" className="moduleHelpClass"/>
<% if (request.getParameter("Cancel") == null) {
	String task = "";
%>
	<ffi:cinclude value1="${OpenEnded}" value2="true" ><ffi:setProperty name="NumberPayments" value="999"/></ffi:cinclude>
	<ffi:setProperty name="AddEditACHBatch" property="NumberPayments" value="${NumberPayments}"/>
	<ffi:setProperty name="AddEditACHBatch" property="Frequency" value="${Frequency}"/>
	<ffi:setProperty name="AddEditACHBatch" property="Template" value="vvvv"/>
	<ffi:setProperty name="AddEditACHBatch" property="CoEntryDesc" value="123"/>
	<ffi:setProperty name="AddEditACHBatch" property="Validate" value="BATCHSIZE,STANDARDENTRYCLASSCODE,COID,COENTRYDESC,CODISCRETIONARYDATA,ENTRIES,NOENTRYVALIDATION"/>
	<ffi:setProperty name="AddEditACHBatch" property="Process" value="false"/>

	<ffi:object name="com.ffusion.tasks.ach.ValidateACHBatch" id="ValidateACHBatch" scope="session"/>
    	<ffi:setProperty name="ValidateACHBatch" property="SuccessURL" value=""/>
	    <ffi:setProperty name="ValidateACHBatch" property="ValidationErrorsURL" value="achbatchaddedit.jsp"/>
	<ffi:process name="ValidateACHBatch"/>

<ffi:object id="GetDupACHBatchTemplates" name="com.ffusion.tasks.ach.GetACHBatchTemplates" scope="session"/>
    <ffi:setProperty name="GetDupACHBatchTemplates" property="ACHType" value="${achType}"/>
    <ffi:setProperty name="GetDupACHBatchTemplates" property="SessionName" value="ACHBatchTemplates" />
    <ffi:setProperty name="GetDupACHBatchTemplates" property="LoadAllTemplates" value="false"/>
    <ffi:setProperty name="GetDupACHBatchTemplates" property="PageSize" value="5"/>
    <ffi:setProperty name="GetDupACHBatchTemplates" property="DuplicateName" value="${AddEditACHTemplate.TemplateName}"/>
    <ffi:setProperty name="GetDupACHBatchTemplates" property="TemplateCategory" value="${achType}" />
    <ffi:setProperty name="GetDupACHBatchTemplates" property="Reload" value="true"/>
<ffi:process name="GetDupACHBatchTemplates"/>

	<ffi:getProperty name="AddEditACHTemplate" property="AchBatchName" assignTo="task" />
	<ffi:cinclude value1="${OpenEnded}" value2="true" ><ffi:setProperty name="NumberPayments" value="999"/></ffi:cinclude>
	<ffi:setProperty name="AddEditACHTemplate" property="FundsTransaction.NumberPayments" value="${NumberPayments}"/>
	<ffi:setProperty name="AddEditACHTemplate" property="FundsTransaction.Frequency" value="${Frequency}"/>
	<ffi:setProperty name="AddEditACHTemplate" property="Process" value="true"/>
	<ffi:setProperty name="AddEditACHTemplate" property="SessionName" value="ACHBatchTemplates"/>
	<% if ("BUSINESS".equals(((ACHBatch)session.getAttribute(task)).getBatchScope())) { %>
		<ffi:setProperty name="AddEditACHTemplate" property="BusinessTemplate" value="true"/>
	<% } else { %>
		<ffi:setProperty name="AddEditACHTemplate" property="BusinessTemplate" value="false"/>
	<% } %>
	<ffi:setProperty name="AddEditACHTemplate" property="Validate" value="TEMPLATENAME"/>
	<ffi:process name="AddEditACHTemplate" />
<% } %>
<% boolean processAndClose = false; %>
<ffi:removeProperty name="AddEditACHTemplate" />
<ffi:removeProperty name="ACHBatchTemplate" />
<ffi:removeProperty name="CallerURL"/>
<ffi:removeProperty name="CallerForm"/>
<ffi:cinclude value1="${ProcessAndClose}" value2="true" operator="equals">
	<% processAndClose = true; %>
</ffi:cinclude>
<ffi:removeProperty name="ProcessAndClose" />

<% if (processAndClose) { %>
	<script>self.close();</script>
<% } else { %>
	<ffi:include page="${PathExt}payments/achtemplates.jsp" />
<% } %>
{}