<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achbatch-reverse" className="moduleHelpClass"/>

<script>
ns.ach.checkState = function(state) {
	var urlStr;
    if (state == "true")
    {
        urlStr = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achbatch-reverse.jsp?DoCheckAll=true"/>';
    } else {
    	urlStr = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achbatch-reverse.jsp?DoCheckAll=false"/>'
    }

    $.ajax({
		url: urlStr,
		success: function(data){
    		$('#viewACHBatchDetailsDialogID').html(data).dialog('open');
			ns.common.resizeDialog('viewACHBatchDetailsDialogID');
		}
	});
}
function changeHold(holdCheckBox, ActiveID){
	var setCheckboxVar = "";
	var activeIDVar = ActiveID;
	if (holdCheckBox.checked == true){
		document.getElementById("hold"+ActiveID).value = "true";
		setCheckboxVar = "true";
	} else {
		document.getElementById("hold"+ActiveID).value = "false";
		setCheckboxVar = "false";
	}
    ns.ach.reverseDoCheck(setCheckboxVar, activeIDVar);
}

</script>

		<div align="center" class="approvalDialogHt390" style="width:98%;">
			<div align="center">
				<div align="left">
					<table width="750" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td></td>
							<td class="columndata lightBackground">
									<div align="left">
								<s:form id="ViewFormId" validate="false" action="/pages/jsp/ach/achbatch-reverse-confirm.jsp" method="post" name="ViewForm" theme="simple">
                    			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<table width="720" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
									<tr>
										<td class="sectionhead lightBackground" colspan="6"><span class="sectionhead"><s:text name="ach.reverse.ACH_batch" /></span></td>
									</tr>
									<tr>
										<td class="sectionhead lightBackground" colspan="6"><div align="center"><s:text name="ach.verify.batch" /></div><br></td>
									</tr>

									<%-- include batch header --%>
									<s:include value="inc/include-view-achbatch-header.jsp" />
									
<ffi:cinclude value1="${ReverseACHBatch.DateWithinRangeForReverse}" value2="true">
									<tr>
										<td colspan="3">
										<input type="button" value="CHECK ALL" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="ns.ach.checkState('true');">&nbsp;&nbsp;&nbsp;
										<input type="button" value="CHECK NONE" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="ns.ach.checkState('false');">
										</td>
									</tr>
</ffi:cinclude>
									<tr>
										<td colspan="3">
											<div id="checkErrorId"></div>
										</td>
									</tr>
									<tr>
										<td class="lightBackground" colspan="6">
		<table border="0" cellspacing="0" cellpadding="3">
		<tr>

	    <td class="sectionhead" width="36%"><span class="sectionhead">&gt; <s:text name="ach.grid.entries" /><br></span></td>
		<td class="sectionsubhead" width="32%">
			<s:text name="jsp.default_513" /> <ffi:getProperty name="ACHBatch" property="TotalNumberCredits"/>
			<br>
			 <s:text name="jsp.default_433" />  (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
			<ffi:getProperty name="ACHBatch" property="TotalCreditAmount" />&nbsp;&nbsp;&nbsp;
		</td>
		<td class="sectionsubhead" width="32%">
			<s:text name="jsp.default_433" /> <ffi:getProperty name="ACHBatch" property="TotalNumberDebits"/>
			<br>
			<s:text name="jsp.default_434" /> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
			<ffi:getProperty name="ACHBatch" property="TotalDebitAmount" />
		</td>
		<td>&nbsp;</td>
		</tr>
	<ffi:cinclude value1="${ACHBatch.BatchType}" value2="Entry Balanced" operator="equals">
	<ffi:list collection="ACHBatch.OffsetAccounts" items="offsetAcct">
		<tr>

			<td class="sectionsubhead" width="36%"><s:text name="jsp.default_495" /><br>
			<ffi:getProperty name="offsetAcct" property="RoutingNum"/>:<ffi:getProperty name="offsetAcct" property="Number"/>:<ffi:getProperty name="offsetAcct" property="NickName"/>(<ffi:getProperty name="offsetAcct" property="Type"/>)
			</td>
			<td class="sectionsubhead" width="32%">
				<s:text name="jsp.default_513" /> <ffi:getProperty name="offsetAcct" property="TotalNumberCredits"/>
				<br>
				<s:text name="jsp.default_433" /> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
				<ffi:getProperty name="offsetAcct" property="TotalCreditAmount" />&nbsp;&nbsp;&nbsp;
			</td>
			<td class="sectionsubhead" width="32%">
				<s:text name="jsp.default_514" /> <ffi:getProperty name="offsetAcct" property="TotalNumberDebits"/>
				<br>
				<s:text name="jsp.default_434" /> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
				<ffi:getProperty name="offsetAcct" property="TotalDebitAmount" />
			</td>
			<td>&nbsp;</td>
	    </tr>
	</ffi:list>
	</ffi:cinclude>
	<ffi:cinclude value1="${ACHBatch.BatchType}" value2="Batch Balanced" operator="equals">
		<tr>

	    <td class="sectionsubhead" width="36%"> <s:text name="jsp.default_496" /> <br>
		<ffi:getProperty name="ACHBatch" property="OffsetAccount.RoutingNum"/>:<ffi:getProperty name="ACHBatch" property="OffsetAccount.Number"/>:<ffi:getProperty name="ACHBatch" property="OffsetAccount.NickName"/>(<ffi:getProperty name="ACHBatch" property="OffsetAccount.Type"/>)
	    </td>
		<td class="sectionsubhead" width="32%">
			<s:text name="jsp.default_513" /> <ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalNumberCredits"/>
			<br>
			<s:text name="jsp.default_433" /> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
			<ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalCreditAmount" />&nbsp;&nbsp;&nbsp;
		</td>
		<td class="sectionsubhead" width="32%">
			<s:text name="jsp.default_514" /> <ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalNumberDebits"/>
			<br>
			<s:text name="jsp.default_434" /> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
			<ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalDebitAmount" />
		</td>
		<td>&nbsp;</td>
    </tr>
	</ffi:cinclude>
		</table>
										</td>
									</tr>
									<tr>
										<td class="sectionsubhead" colspan="7"><hr class="thingrayline"></td>
									</tr>
								</table>

								<%-- include batch entries --%>
								<div>
									 <s:action name="viewAchBatchEntriesInit" namespace="/pages/jsp/ach" executeResult="true">
									 	<s:param name="isReverse" value="true"></s:param>
									 </s:action>
								</div>
							</s:form>
							</div>
							</td>
							<td align="right">
							</td>
						</tr>
					</table>
					<br>
				</div>
			</div>
		</div>
