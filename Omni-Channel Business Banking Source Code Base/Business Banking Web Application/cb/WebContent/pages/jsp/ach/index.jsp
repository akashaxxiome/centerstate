<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

	<script type="text/javascript" src="<s:url value='/web/js/ach/ach%{#session.minVersion}.js'/>"></script>
    <script type="text/javascript" src="<s:url value='/web/js/ach/ach_grid%{#session.minVersion}.js'/>"></script>
    <ffi:setProperty name="subMenuSelected" value="ach"/>
    <s:set var="ACHType" value="%{@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_ACH}" scope="session"/>
	<ffi:setProperty name="heading" value="ACH"/>

<%
    String type = (String)session.getAttribute("subMenuSelected");
    String entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
    if ("ach".equals(type)) {
        entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
    } else if ("tax".equals(type)) {
        entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_PAYMENT_ENTRY );
    } else if ("child".equals(type)) {
        entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_PAYMENT_ENTRY );
    }
    session.setAttribute( "tempEntToCheck", entitlementName );

%>
	<s:include value="inc/index-pre.jsp"/>

        <div id="desktop" align="center">

            <div id="operationresult">
                <div id="resultmessage">Operation Result</div>
            </div><!-- result -->

            <div id="appdashboard">
               <s:action namespace="/pages/jsp/ach" name="achDashBoardAction_execute" executeResult="true"/>

            </div>

			<div id="details" style="display:none;">
				<s:include value="/pages/jsp/ach/ach_details.jsp"/>
			</div>


			<div id="summary">
				<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
					<ffi:cinclude value1="${summaryToLoad}" value2="ACHBatch"	operator="equals">
							 <s:action namespace="/pages/jsp/ach" name="achSummaryAction" executeResult="true">
								  <s:param name="module" value="#session.subMenuSelected"/>
							 </s:action>
					</ffi:cinclude>
					<ffi:cinclude value1="${summaryToLoad}" value2="ACHBatchTemplate"	operator="equals">
						<s:action namespace="/pages/jsp/ach" name="achTemplateSummary" executeResult="true"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${summaryToLoad}" value2="ACHPayee"	operator="equals">
							<s:include value="/pages/jsp/ach/achpayee_summary.jsp"/>
					</ffi:cinclude>
				</ffi:cinclude>


			</div>

			<div id="customMappingDetails">
				<s:include value="/pages/jsp/custommapping_workflow.jsp"/>
			</div>

			<div id="mappingDiv">

 	    	</div>

    </div>

	<sj:dialog id="checkFileACHImportResultsDialogID" cssClass="pmtTran_achDialog" title="File Import Results" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="790" height="310">
	</sj:dialog>

	<sj:dialog id="openFileACHImportDialogID" cssClass="pmtTran_achDialog" title="File Import Results" modal="false" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="320" height="200">
	</sj:dialog>

	<script type="text/javascript">
	$(document).ready(function(){
		ns.ach.showACHDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	});
</script>

