<%@ page import="com.ffusion.beans.ach.ACHPayee"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_ACHbanklistselect" className="moduleHelpClass"/>
<% String bankName = ""; String fedRTN = ""; String bankIDType = "";
   String bankCity = ""; String bankState = ""; String bankCountry = "";
%>
<%
    if( request.getParameter("bankName") != null && request.getParameter("bankName").length() > 0 ){ bankName = (String)request.getParameter("bankName"); }
    if( request.getParameter("fedRTN") != null && request.getParameter("fedRTN").length() > 0 ){ fedRTN = (String)request.getParameter("fedRTN"); }

    if( request.getParameter("bankCity") != null && request.getParameter("bankCity").length() > 0 ){ bankCity = (String)request.getParameter("bankCity"); }
    if( request.getParameter("bankState") != null && request.getParameter("bankState").length() > 0 ){ bankState = (String)request.getParameter("bankState"); }
    if( request.getParameter("bankCountry") != null && request.getParameter("bankCountry").length() > 0 ){ bankCountry = (String)request.getParameter("bankCountry"); }

    if( request.getParameter("bankIDType") != null && request.getParameter("bankIDType").length() > 0 ){ bankIDType = (String)request.getParameter("bankIDType"); }
    if( request.getParameter("ACHBankFormUsed") != null && request.getParameter("ACHBankFormUsed").length() > 0 ){ session.setAttribute("ACHBankFormUsed", request.getParameter("ACHBankFormUsed")); }
%>

<ffi:removeProperty name="ACHParticipantBank"/>
<ffi:object id="SetACHParticipantBank" name="com.ffusion.tasks.ach.SetACHParticipantBank" scope="session"/>

<ffi:object id="GetBankLookupSettings" name="com.ffusion.tasks.user.GetBankLookupSettings" scope="session" />
<ffi:process name="GetBankLookupSettings"/>

<script type="text/javascript">
$(document).ready(function(){
        $("#ach_bankListRowsID a").click(function(event){
        event.preventDefault();
        var url = $( this ).attr( 'href' );
        ns.ach.searchDestinationBankForm( url );
    });
 });

</script>

<ffi:cinclude value1="${DONOTEXECUTE}" value2="true" operator="notEquals">
	<ffi:process name="GetACHParticipantBanks"/>
</ffi:cinclude>
<ffi:removeProperty name="DONOTEXECUTE"/>

<ffi:cinclude value1="${ACHBankFormUsed}" value2="true" operator="notEquals">
	<ffi:object id="GetACHParticipantBanks" name="com.ffusion.tasks.ach.GetACHParticipantBanks" scope="session"/>

    <% if (bankName != null && bankName.length() > 0) { %>
	    <ffi:setProperty name="GetACHParticipantBanks" property="BankName" value="<%= bankName %>"/>
    <% } %>
    <% if (bankIDType != null && bankIDType.length() > 0) { %>
        <ffi:setProperty name="GetACHParticipantBanks" property="BankIdentifierType" value="<%=bankIDType%>" />
    <% } %>
    <% if (bankCountry != null && bankCountry.length() > 0) { %>
        <ffi:setProperty name="GetACHParticipantBanks" property="BankCountry" value="<%=bankCountry%>"/>
    <% } %>
    <% if (bankState != null && bankState.length() > 0) { %>
        <ffi:setProperty name="GetACHParticipantBanks" property="BankState" value="<%=bankState%>"/>
    <% } %>
    <% if (bankCity != null && bankCity.length() > 0) { %>
        <ffi:setProperty name="GetACHParticipantBanks" property="BankCity" value="<%=bankCity%>"/>
    <% } %>

    <% if (fedRTN != null && fedRTN.length() > 0) { %>
        <% if (ACHPayee.PAYEE_BANK_IDENTIFIER_TYPE_ROUTINGNUM.equals(bankIDType) || bankIDType == null || bankIDType.length() == 0) { %>
            <ffi:setProperty name="GetACHParticipantBanks" property="AchRTN" value="<%= fedRTN %>"/>
        <%} if (ACHPayee.PAYEE_BANK_IDENTIFIER_TYPE_OTHER_RTN.equals(bankIDType)) { %>
            <ffi:setProperty name="GetACHParticipantBanks" property="NationalID" value="<%= fedRTN %>"/>
        <%} if (ACHPayee.PAYEE_BANK_IDENTIFIER_TYPE_BIC.equals(bankIDType)) { %>
            <ffi:setProperty name="GetACHParticipantBanks" property="SwiftRTN" value="<%= fedRTN %>"/>
        <% } %>
    <% } %>
</ffi:cinclude>
<ffi:removeProperty name="ACHBankFormUsed"/>

<ffi:setProperty name="GetACHParticipantBanks" property="Reload" value="true"/>
<ffi:process name="GetACHParticipantBanks"/>
<% session.setAttribute("FFIGetACHParticipantBanks", session.getAttribute("GetACHParticipantBanks")); %>

<ffi:setProperty name="ACHParticipantBanks" property="SortedBy" value="NAME" />

    <div align="center" class="approvalDialogHt" style="margin-bottom:40px;">
    <table width="600" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
        <tr>
            <td class="columndata" align="center" class="ltrow2_color">
                <form action="achbanklist.jsp" method="post" name="BankForm" id="ach_researchACHBankListFormID">
                <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                <table width="100%" border="0" cellspacing="0" cellpadding="3" class="ltrow2_color tableData tableAlerternateRowColor">
                 	<tr>
                    	<th align="left"><s:text name="jsp.default_61" /></th>
                    	<th align="left"><s:text name="jsp.default_35" /></th>
                    	<th align="left"><s:text name="jsp.default_401" /></th>
                    	<th align="left"><s:text name="jsp.default_201" /></th>
                    </tr>
                    <ffi:list collection="ACHParticipantBanks" items="Bank1">
                    <tr id="ach_bankListRowsID">
                        <td class="columndata" align="left" height="30" valign="top"><ffi:link url="/cb/pages/jsp/ach/achbanklistitem.jsp?SetACHParticipantBank.Id=${Bank1.InstitutionId}"><ffi:getProperty name="Bank1" property="InstitutionName"/></ffi:link></td>
                        <td class="columndata" align="left" height="30" valign="top">
                                <ffi:cinclude value1="${Bank1.Street}" value2="" operator="notEquals">
                                    <ffi:getProperty name="Bank1" property="Street"/>,
                                </ffi:cinclude>
                                <ffi:cinclude value1="${Bank1.Street2}" value2="" operator="notEquals">
                                    <ffi:getProperty name="Bank1" property="Street2"/>,
                                </ffi:cinclude>
                                <ffi:getProperty name="Bank1" property="City"/>,&nbsp;
                                <ffi:cinclude value1="${Bank1.State}" value2="" operator="notEquals" ><ffi:getProperty name="Bank1" property="State" /></ffi:cinclude>
                                <ffi:cinclude value1="${Bank1.State}" value2="" operator="equals" ><ffi:getProperty name="Bank1" property="StateCode" /></ffi:cinclude>
                                , 
                                <ffi:getProperty name="Bank1" property="Country"/>
                        </td>
                         <td class="columndata">
                      		<ffi:cinclude value1="${Bank1.SwiftBIC}" value2="" operator="equals">
                  				N/A
              				</ffi:cinclude>
              				<ffi:cinclude value1="${Bank1.SwiftBIC}" value2="" operator="NotEquals">
                  				<ffi:getProperty name="Bank1" property="SwiftBIC"/>
              				</ffi:cinclude>
						</td>
						<td class="columndata">
							<ffi:cinclude value1="${Bank1.AchRoutingNumber}" value2="" operator="equals">
                     				N/A
                 				</ffi:cinclude>
                 				<ffi:cinclude value1="${Bank1.AchRoutingNumber}" value2="" operator="NotEquals">
                     				<ffi:getProperty name="Bank1" property="AchRoutingNumber"/>
                 				</ffi:cinclude>
						</td>
                    </tr>
                    </ffi:list>
                    <tr>
                        <td colspan="4" class="columndata"><br>
                        *<s:text name="jsp.user_172" /> </td>
                    </tr>
                    <tr>
                        <td colspan="4" height="30">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"  class="ui-widget-header customDialogFooter">
                            <%--
                        <input class="submitbutton" type="button" value="SEARCH AGAIN" onClick="document.location='/cb/pages/jsp/ach/achbanklist.jsp?searchAgain=true';">
                        <input class="submitbutton" type="button" value="CLOSE" onClick="self.close();return false;">
                            --%>
                            <script>
								ns.ach.researchBankListURL = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achbanklist.jsp?searchAgain=true"/>';
					 		</script>
                            <sj:a
                                    id="ach_researchBankListID"
                                    button="true"
                                    onClickTopics="ach_researchBankListOnClickTopics"
                            ><s:text name="jsp.default_374" /><!-- SEARCH AGAIN -->
                            </sj:a>
                            <sj:a id="ach_closeSerachBankListID"
                                    button="true"
                                    onClickTopics="ach_closeSearchBankListOnClickTopics"
                            ><s:text name="jsp.default_102" /><!-- CLOSE -->
                            </sj:a>
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
    </table>
    </div>
