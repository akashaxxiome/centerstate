<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_ACHCompleteSummary" className="moduleHelpClass"/>

	<ffi:setGridURL grid="GRID_completedACH" name="ViewURL" url="/cb/pages/jsp/ach/viewAchBatch.action?ID={0}&BatchIsland=Scheduled&RecID={1}" parm0="ID" parm1="RecID"/>
	<ffi:setGridURL grid="GRID_completedACH" name="InquireURL" url="/cb/pages/jsp/ach/sendACHInqMessage_initACHInquiry.action?FundsID={0}&recurringId={1}&CollectionName=CompletedACHBatches&Subject=ACH Inquiry" parm0="ID" parm1="RecID"/>
	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getPagedACHbatchesAction_getCompletedACHBatches.action?ACHType=${ACHType}&GridURLs=GRID_completedACH" URLEncrypt="true"/>
    <s:url id="completedACHUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
		id="completedACHGridID"
		caption=""  
		sortable="true" 
		dataType="local"  
		href="%{completedACHUrl}"
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		sortname="date"
		sortorder="desc"
		onGridCompleteTopics="addGridControlsEvents,completedACHGridCompleteEvents"
		> 
		
        <sjg:gridColumn name="date" width="100" index="date" title="%{getText('ach.grid.effectiveDate')}" sortable="true" />
        <sjg:gridColumn name="name" width="60" index="name" title="%{getText('ach.grid.batchName')}" sortable="true" />
        <sjg:gridColumn name="companyID" width="100"  index="companyID" title="%{getText('ach.grid.companyID')}" sortable="true"/>
        <sjg:gridColumn name="coName" width="100"  index="coName" title="%{getText('ach.grid.companyName')}" sortable="true" />
        <sjg:gridColumn name="numberEntries" width="50" index="numberEntries" title="%{getText('ach.grid.entries')}" sortable="true"/>
        <ffi:cinclude value1="${ACHType}" value2="TaxPayment" operator="notEquals">
            <sjg:gridColumn name="frequency" width="80" index="frequency" title="%{getText('ach.grid.frequency')}" sortable="true" />
        </ffi:cinclude>
        <sjg:gridColumn name="status" width="90" index="status" title="%{getText('jsp.default_388')}" sortable="true"/>
        <sjg:gridColumn name="totalCreditAmountValue.currencyStringNoSymbol" width="100" index="totalCreditAmount" title="%{getText('ach.grid.totalCredits')}" align="right" sortable="true" formatter="ns.ach.formatCreditAmountColumn" formatoptions="{suffix:' USD'}"/>
        <sjg:gridColumn name="totalDebitAmountValue.currencyStringNoSymbol" width="100"  index="totalDebitAmount" title="%{getText('ach.grid.totalDebits')}" align="right" sortable="true" formatter="ns.ach.formatDebitAmountColumn" formatoptions="{suffix:' USD'}"/>
        <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.ach.formatCompletedACHActionLinks" width="90" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
 	</sjg:grid>
 	
 	<script type="text/javascript">
	<!--		
		$("#completedACHGridID").jqGrid('setColProp','ID',{title:false});
	//-->
	</script>	
