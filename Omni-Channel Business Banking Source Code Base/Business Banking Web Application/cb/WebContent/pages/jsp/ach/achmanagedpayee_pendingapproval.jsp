<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_ACHPayeePendingApprovalSummary" className="moduleHelpClass"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getPendingApprovalACHParticipantsAction.action?Show=Pending&PayeeType=&searchOper=cn" URLEncrypt="true"/>
    <s:url namespace="/pages/jsp/ach" id="pendingApprovalACHPayeeUrl" value="%{#session.tempURL}" escapeAmp="false"/>

	<sjg:grid
		id="pendingApprovalManagedACHPayeeGridId"
		caption=""
		sortable="true"
		sortname="NICKNAME"
		dataType="json"
		href="%{pendingApprovalACHPayeeUrl}"
		pager="true"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}"
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,pendingApprovalACHManagedPayeeGridCompleteEvents"
		>

        <sjg:gridColumn name="payeeType" edittype="select" editoptions="{value:'1:Standard;2:IAT Payee'}" width="120" index="PAYEETYPE" title="%{getText('ach.grid.payeeType')}" sortable="true" search="false" hidden="true" hidedlg="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="nickNameSecure" width="100" index="NICKNAME" title="%{getText('jsp.default_294')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="nameSecure" width="100" index="NAME" title="%{getText('ach.grid.payeeName')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" searchoptions="{searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="userAccountNumber" width="100"  index="USERACCOUNTNUMBER" title="%{getText('ach.grid.payeeID')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="displayACHCompany" width="120" index="COMPANYDISPLAYNAME" title="%{getText('ach.grid.companyIDOrName')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" formatter="ns.ach.formatACHPayeesCompanyNameID" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="payeeGroup" edittype="select" editoptions="{value:'1:User;2:ACH Company;3:Business'}" width="120" index="PAYEEGROUP" title="%{getText('ach.grid.payeeScope')}" sortable="true" search="false" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="bankNameSecure" width="120"  index="BANKNAME" title="%{getText('jsp.default_63')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="routingNumberSecure" width="80" index="ROUTINGNUM" title="%{getText('jsp.default_365')}" hidden="true" hidedlg="true" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="accountNumberSecure" width="120" index="ACCOUNTNUMBER" title="%{getText('jsp.default_19')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="accountTypeSecure" edittype="select" hidden="true" hidedlg="true" editoptions="{value:'1:Checking;2:Savings;3:Loan;4:General Ledger'}" width="80" index="accountType" title="%{getText('jsp.default_20')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="securePayee" edittype="checkbox" editoptions="{value:'true:false'}" search="false" width="120" index="securePayee" title="%{getText('ach.grid.securePayee')}" sortable="false" hidden="true" hidedlg="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="displayAsSecurePayee" width="120" index="displayAsSecurePayee" search="false" title="%{getText('ach.grid.displaySecurePayee')}" sortable="false" hidden="true" hidedlg="true" editable="false"  cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="prenoteStatus" width="120" index="prenote" search="false" title="%{getText('ach.grid.prenote')}" sortable="false" hidden="true" hidedlg="true" editable="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="daItemId" index="daItemId" title="daItemId" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="daStatus" index="daStatus" title="daStatus" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="action" index="action" title="%{getText('ach.grid.userAction')}" sortable="true" width="80" search="false" cssClass="datagrid_actionIcons"/>
        <sjg:gridColumn name="edit" index="edit" title="Edit" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="discard" index="discard" title="Discard" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="modifyChange" index="modifyChange" title="Modify" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="approve" index="approve" title="Approve" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="view" index="view" title="View" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="reject" index="reject" title="Reject" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="submitForApproval" index="submitForApproval" title="Submit for approval" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="submittedBy" index="submittedBy" title="Submitted By" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="rejectReason" index="rejectReason" title="Reject reason" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="rejectedBy" index="rejectedBy" title="Rejected By" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
	</sjg:grid>

<script type="text/javascript">
<!--
	$("#pendingApprovalManagedACHPayeeGridId").jqGrid('setColProp','ID',{title:false});
//-->
</script>
