<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achentryimportaddenda" />
<ffi:object name="com.ffusion.tasks.ach.ImportAddenda" id="ImportAddenda" scope="session"/>
<ffi:setProperty name="ImportAddenda" property="EntryName" value="ModifyACHEntry" />
<%
    session.setAttribute("FFIImportAddendaTask", session.getAttribute("ImportAddenda"));
%>

<div align="center">
    <table width="282" border="0" cellspacing="0" cellpadding="0" style="margin-top: 15px;">
        <tr>
            <td class="columndata lightBackground" colspan="2">
                <div align="left">
                    <span class="columndata"><s:text name="jsp.default_187"/></span></div>
            </td>
        </tr>
        <tr>
            <td class="columndata lightBackground" colspan="2">

                <s:actionerror />
                <s:fielderror />
                <s:actionmessage />
                <s:form id="ACHEntryImportAddendaForm" validate="false" name="FileForm" enctype="multipart/form-data" theme="simple" namespace="/pages/jsp/ach" action="ACHEntryImportAddendaAction" method="post">
	                <div align="center">
		                <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" id="CSRF_TOKEN"/>
						<input type="text" id="fakeImportFileID" readonly="true" class="ui-widget-content ui-corner-all"/>
						<span class="fileUploadSpan">
							<sj:a
								id="browseFileButton"
								cssClass="fakeUploadLink"
								button="true" 
								onClickTopics=""
							><s:text name="jsp.default.label.browse"/></sj:a>
							<s:file id="importFileInputID" name="userImage" accept="text/plain" onchange="ns.common.updateFilePath();"/>
						</span><span id="FileError"></span>
						<br/><span id="errorFileType"></span>
					</div>
                </s:form>
				<div class="submitButtonsDiv">
					<sj:a id="closeACHEntryImportAddendaLink" button="true"
						  onClickTopics="closeACHEntryImportAddenda"
						  title="Cancel">Cancel</sj:a>
					<sj:a id="ACHEntryImportAddendaButton" button="true"
						  formIds="ACHEntryImportAddendaForm"
						  targets="ImportAddendaResult"
						  title="Import Addenda"
						  onBeforeTopics="beforeACHEntryImportAddendaForm"
						  onErrorTopics="errorACHEntryImportAddendaForm"
						  onSuccessTopics="successACHEntryImportAddendaForm"
						  value="Import Addenda"><s:text name="ach.importAddendaDialog"/></sj:a>
				</div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
	$("#ImportAddendaResult").html("");     // clear previous contents
</script>