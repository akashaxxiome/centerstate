<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_federalTaxTypeSummary" className="moduleHelpClass"/>

	<ffi:setGridURL grid="GRID_federal" name="DeleteURL" url="/cb/pages/jsp/ach/taxformdelete.jsp?ID={0}&isBusiness={1}&type=federal" parm0="ID" parm1="isBusiness"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getChildTaxTypesAction.action?formType=FEDERAL&GridURLs=GRID_federal" URLEncrypt="true"/>
    <s:url id="taxTypeUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
		id="taxFederalTypesGridId"
		caption=""  
		sortable="true"  
		dataType="local"  
		href="%{taxTypeUrl}"
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,taxTypeGridCompleteEvents"
		> 
		
        <sjg:gridColumn name="name" width="700" index="name" title="%{getText('jsp.common_79')} %{getText('jsp.label.taxes')}" sortable="true" formatter="ns.tax.formatTaxStateTypeNameLinks" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="ID" index="action" title="Action" sortable="false" search="false" formatter="ns.tax.formatTaxTypeActionLinks" width="90" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
        
        <sjg:gridColumn name="map.BusinessID" width="100" index="businessID" title="BusinessID" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
        
	</sjg:grid>
	
	<script type="text/javascript">
	<!--
		$("#taxFederalTypesGridId").data("supportSearch",true);
		$("#taxFederalTypesGridId").jqGrid('setColProp','ID',{title:false});
	//-->
	</script>	
