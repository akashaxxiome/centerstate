<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<div class="dashboardUiCls">
	<div class="moduleSubmenuItemCls">
		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-tax"></span>
			<span class="moduleSubmenuLbl">
				<s:text name="jsp.default_405" />
			</span>
			<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>

			<!-- dropdown menu include -->    		
			<s:include value="/pages/jsp/home/inc/transferSubmenuDropdown.jsp" />
	</div>
	
	<!-- dashboard items listing for Child Support -->
    <div id="dbACHSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
		<s:url id="TaxUrl" value="/pages/jsp/ach/achSummaryAction.action" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a
			id="TaxBatchesLink"
			href="%{TaxUrl}"
			targets="summary"
			indicator="indicator"
			button="true"
			cssClass="summaryLabelCls"
			onSuccessTopics="taxSummaryLoadedSuccess,calendarToSummaryLoadedSuccess"
		><s:text name="jsp.default_405" />
		</sj:a>
		
        <s:url id="goBackSummaryUrl" value="/pages/jsp/ach/tax/tax_summary.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
        </s:url>
	    <sj:a
			id="goBackSummaryLink"
			href="%{goBackSummaryUrl}"
			targets="summary"
			button="true"
	    	title="Go Back Summary"
	    	cssStyle="display:none"
			onCompleteTopics="calendarToSummaryLoadedSuccess"
			><s:text name="jsp.default_400" />
	    </sj:a>		
	
		<ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
			<s:url id="singleTaxBatchUrl" action="addTaxBatch_init"  namespace="/pages/jsp/ach" escapeAmp="false">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="Initialize" value="true"/>
	        </s:url>
	        
		    <sj:a id="addSingleTaxBatchLink" href="%{singleTaxBatchUrl}" targets="inputDiv" button="true"
		    	title="Add Single Tax Batch"
		    	onClickTopics="beforeLoadACHForm,loadSingleTaxBatch" onCompleteTopics="loadACHFormComplete,showWizardFormOnComplete" onErrorTopics="errorLoadACHForm">
			  	<s:text name="jsp.default.single.batch" />
		    </sj:a>
	
			 <s:url id="multipleACHBatchUrl" value="/pages/jsp/ach/loadMultiACHTemplateAction_initLoad.action">
				<s:param name="loadMultipleBatch">true</s:param>
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
		    <sj:a id="addMultipleTaxBatchLink" href="%{multipleACHBatchUrl}"
				targets="inputDiv" button="true"
				title="Multiple ACH Batch from Template"
				onClickTopics="beforeLoadACHForm,loadMultipleTaxBatch"
				onCompleteTopics="loadACHFormComplete,showWizardFormOnComplete"
				onErrorTopics="errorLoadACHForm">
				<s:text name="ach.newMultipleBatch.fromTemplate"/>
			</sj:a>
			<div class="hidden">
			    <sj:a id="loadTemplateLink"
			    	button="true"
			    	title="Load Template"
			    	onClick="$('#quicksearchcriteria').hide();$('#loadTemplateId').toggle();ns.ach.getTemplates();"
				  	><s:text name="jsp.default_264" />
			    </sj:a>
		    </div>
		</ffi:cinclude>	
	
	</div>
	
	<!-- dashboard items listing for Templates -->
    <div id="dbTemplateSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
		<s:url id="templatesUrl" value="/pages/jsp/ach/achTemplateSummary.action" escapeAmp="false">
	    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a
			id="TaxtemplatesLink"
			href="%{templatesUrl}"
			targets="summary"
			indicator="indicator"
			button="true"
			cssClass="summaryLabelCls"
			onSuccessTopics="taxTemplatesSummaryLoadedSuccess"
		><s:text name="jsp.default_5" />
		</sj:a>
		
	    <s:url id="singleTaxBatchTemplateUrl" action="addTaxBatchTemplate_init" namespace="/pages/jsp/ach" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="Initialize" value="true"/>
            </s:url>
	    <sj:a id="addSingleTemplateLink" href="%{singleTaxBatchTemplateUrl}" targets="inputDiv" button="true" 
	    	title="Add Single Tax Template"
	    	onClickTopics="beforeLoadACHForm,loadACHSingleBatchTemplate" onCompleteTopics="loadACHFormComplete,showWizardFormOnComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="jsp.default.single.template" />
	    </sj:a>

	    <s:url id="multipleTaxTemplateUrl" value="/pages/jsp/ach/addMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
        </s:url>
	    <sj:a id="addMultipleTemplateLink" href="%{multipleTaxTemplateUrl}" targets="inputDiv" button="true"
	    	title="Add Multiple Batch Template"
	    	onClickTopics="beforeLoadACHForm,loadACHBatchTemplate" onCompleteTopics="loadACHFormComplete,showWizardFormOnComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="ach.newMultiBatch" />
	    </sj:a>
		
	    <s:url id="editMultipleTaxTemplateUrl" value="/pages/jsp/ach/editMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	    </s:url>
	    <sj:a id="editMultipleACHTemplateLink" href="%{editMultipleTaxTemplateUrl}" targets="inputDiv" button="true" 
	    	 cssStyle="display:none;" title="Edit Multiple Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name="ach.editMultiTemplate" />
	    </sj:a>		  
	</div>
	
	<!-- dashboard items listing for Payee -->
    <div id="dbPayeeSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
	    <s:url id="TaxTypesUrl" value="/pages/jsp/ach/tax/taxtype_summary.jsp" escapeAmp="false">
	        <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	        </s:url>
	    <sj:a
	        id="TaxPayeesLink"
	        href="%{TaxTypesUrl}"
	        targets="summary"
	        indicator="indicator"
	        button="true"
	        cssClass="summaryLabelCls"
	        onSuccessTopics="taxTypesSummaryLoadedSuccess"
	    ><s:text name="jsp.default_Manage_Tax_Types" />
	    </sj:a>
        
        <s:url id="addStateTaxPayeeUrl" value="/pages/jsp/ach/taxformadd.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
        <sj:a id="addTaxPayeeLink" href="%{addStateTaxPayeeUrl}" targets="inputDiv" button="true" 
            title="State Tax Type"
            onClickTopics="beforeLoadACHForm,loadAddTaxPayee" onCompleteTopics="loadACHFormCompleteForTax,hideWizardFormOnComplete" onErrorTopics="errorLoadACHForm">
              <s:text name="State tax type" /><!-- New State Tax Type -->
        </sj:a>
        
        <s:url id="addFederalTaxPayeeUrl" value="/pages/jsp/ach/taxformadd.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="isFederal">true</s:param>
		</s:url>
        <sj:a id="addFederalPayeeLink" href="%{addFederalTaxPayeeUrl}" targets="inputDiv" button="true" 
            title="Federal Tax Type"
            onClickTopics="beforeLoadACHForm,loadAddFederalPayee" onCompleteTopics="loadACHFormCompleteForTax,hideWizardFormOnComplete" onErrorTopics="errorLoadACHForm">
              <s:text name="Federal tax type" /><!-- New State Tax Type -->
        </sj:a>
	</div>
	
	<!-- Summary link added to fix issue in IE -->
      <div id="dbFileUploadSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
            <s:url id="addAchFileImporUrl" value="/pages/jsp/ach/achimport.jsp" escapeAmp="false">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
             <sj:a id="fileUploadSummary" cssClass="summaryLabelCls"  button="true" title="File Import">
            	 <s:text name="jsp.billpay_57" /><!-- File Import -->
      		 </sj:a>
      </div>
	
	
	<!-- More list option listing -->
	<div class="dashboardSubmenuItemCls">
		<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
	
		<!-- UL for rendering tabs -->
		<ul class="dashboardSubmenuItemList">
			<li class="dashboardSubmenuItem"><a id="showdbACHSummary" onclick="ns.common.refreshDashboard('showdbACHSummary',true)"><s:text name="ach.batch" /></a></li>
		
			<li class="dashboardSubmenuItem"><a id="showdbTemplateSummary" onclick="ns.common.refreshDashboard('showdbTemplateSummary',true)"><s:text name="ach.templates"/></a></li>
			
			<li class="dashboardSubmenuItem"><a id="showdbPayeeSummary" onclick="ns.common.refreshDashboard('showdbPayeeSummary',true)"><s:text name="jsp.default_Manage_Tax_Types" /></a></li>
		</ul>
	</div>
</div>

<div id="loadTemplateId"  style="display:none;">
</div>
<%-- 
    <div id="appdashboard-left" class="formLayout">
		<sj:a
			id="quickSearchLink"
			button="true"
			buttonIcon="ui-icon-search"
			onClick="$('#loadTemplateId').hide(); $('#quicksearchcriteria').toggle();"
		><s:text name="jsp.default_4" />
		</sj:a>

        <s:url id="goBackSummaryUrl" value="/pages/jsp/ach/tax/tax_summary.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
        </s:url>
	    <sj:a
			id="goBackSummaryLink"
			href="%{goBackSummaryUrl}"
			targets="summary"
			button="true"
			buttonIcon="ui-icon-transfer-e-w"
	    	title="Go Back Summary"
			onCompleteTopics="calendarToSummaryLoadedSuccess"
			><s:text name="jsp.default_400" />
	    </sj:a>

	    <s:url id="singleTaxBatchUrl" action="addTaxBatch_init"  namespace="/pages/jsp/ach" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
        </s:url>
<ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
	    <sj:a id="addSingleTaxBatchLink" href="%{singleTaxBatchUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
	    	title="Add Single Tax Batch"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="jsp.default_591" />
	    </sj:a>

		 <s:url id="multipleACHBatchUrl" value="/pages/jsp/ach/loadMultiACHTemplateAction_initLoad.action">
			<s:param name="loadMultipleBatch">true</s:param>
		</s:url>
	    <sj:a id="addMultipleTaxBatchLink" href="%{multipleACHBatchUrl}"
			targets="inputDiv" button="true"
			title="Multiple ACH Batch from Template"
			onClickTopics="beforeLoadACHForm"
			onCompleteTopics="loadACHFormComplete"
			onErrorTopics="errorLoadACHForm">
			<span class="sapUiIconCls icon-add-activity-2"></span><s:text name="ach.newMultipleBatch.fromTemplate"/>

		</sj:a>
		<div class="hidden">
	    <sj:a id="loadTemplateLink"
	    	button="true"
	    	buttonIcon="ui-icon-plus"
	    	title="Load Template"
	    	onClick="$('#quicksearchcriteria').hide();$('#loadTemplateId').toggle();ns.ach.getTemplates();"
		  	><s:text name="jsp.default_264" />
	    </sj:a>
	    </div>
</ffi:cinclude>
        <s:url id="addStateTaxPayeeUrl" value="/pages/jsp/ach/taxformadd.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		
        <sj:a id="addTaxPayeeLink" href="%{addStateTaxPayeeUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
            title="State Tax Type"
            onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormCompleteForTax" onErrorTopics="errorLoadACHForm">
              <s:text name="State tax type" /><!-- New State Tax Type -->
        </sj:a>
        
        <s:url id="addFederalTaxPayeeUrl" value="/pages/jsp/ach/taxformadd.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="isFederal">true</s:param>
		</s:url>
		
         <sj:a id="addFederalPayeeLink" href="%{addFederalTaxPayeeUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
            title="Federal Tax Type"
            onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormCompleteForTax" onErrorTopics="errorLoadACHForm">
              <s:text name="Federal tax type" /><!-- New State Tax Type -->
        </sj:a>
        
        <sj:a id="addStateTaxPayeeLink" href="%{addTaxPayeeUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
            title="New Tax Type"
            onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormCompleteForTax,addStateTaxPayeeTopic" onErrorTopics="errorLoadACHForm">
              New Federal Tax Type
        </sj:a>

	    <s:url id="singleTaxBatchTemplateUrl" action="addTaxBatchTemplate_init" namespace="/pages/jsp/ach" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="Initialize" value="true"/>
            </s:url>
	    <sj:a id="addSingleTemplateLink" href="%{singleTaxBatchTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
	    	title="Add Single Tax Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="NEW_TEMPLATE" />
	    </sj:a>

	    <s:url id="multipleTaxTemplateUrl" value="/pages/jsp/ach/addMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
        </s:url>
	    <sj:a id="addMultipleTemplateLink" href="%{multipleTaxTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plusthick"
	    	title="Add Multiple Batch Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="ach.newMultiBatch" />
	    </sj:a>

		edit multiple template
	    <s:url id="editMultipleTaxTemplateUrl" value="/pages/jsp/ach/editMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	    </s:url>
	    <sj:a id="editMultipleACHTemplateLink" href="%{editMultipleTaxTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
	    	 cssStyle="display:none;" title="Edit Multiple Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name="ach.editMultiTemplate" />
	    </sj:a>

		

    </div>
    <div id="appdashboard-right" >
    	<div style="float:right;">
            <s:url id="TaxTypesUrl" value="/pages/jsp/ach/tax/taxtype_summary.jsp" escapeAmp="false">
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
                </s:url>
            <sj:a
                id="TaxPayeesLink"
                href="%{TaxTypesUrl}"
                targets="summary"
                indicator="indicator"
                button="true"
                buttonIcon="ui-icon-copy"
                onSuccessTopics="taxTypesSummaryLoadedSuccess"
            ><s:text name="jsp.default_Manage_Tax_Types" />
            </sj:a>
 		
		    <s:url id="TaxUrl" value="/pages/jsp/ach/achSummaryAction.action" escapeAmp="false">
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
                </s:url>
			<sj:a
				id="TaxBatchesLink"
				href="%{TaxUrl}"
				targets="summary"
				indicator="indicator"
				button="true"
				buttonIcon="ui-icon-pencil"
				onSuccessTopics="taxSummaryLoadedSuccess"
			><s:text name="jsp.default_405" />
			</sj:a>
			<s:url id="templatesUrl" value="/pages/jsp/ach/ach_templates_summary.jsp" escapeAmp="false">
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            </s:url>
			<sj:a
				id="TaxtemplatesLink"
				href="%{templatesUrl}"
				targets="summary"
				indicator="indicator"
				button="true"
				buttonIcon="ui-icon-copy"
				onSuccessTopics="taxTemplatesSummaryLoadedSuccess"
			><s:text name="jsp.default_5" />
			</sj:a>

		</div>
		<span class="ui-helper-clearfix">&nbsp;</span>
    </div>
	<span class="ui-helper-clearfix">&nbsp;</span> --%>


        

	<%-- Following three dialogs are used for view, delete and inquiry Tax Batch --%>

    <sj:dialog id="inquiryACHBatchDialogID" cssClass="pmtTran_taxDialog" title="Tax Batch Inquiry" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
    </sj:dialog>

	<sj:dialog id="viewACHBatchDetailsDialogID" cssClass="pmtTran_taxDialog" title="Tax Batch Detail" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800"  height="500"  cssStyle="overflow:hidden;">
	</sj:dialog>

    <sj:dialog id="viewACHEntryErrorDetailsDialogID" cssClass="pmtTran_taxDialog" title="Tax Batch Entry Error Detail" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
    </sj:dialog>

	<sj:dialog id="viewACHMultiBatchDetailsDialogID" cssClass="pmtTran_taxDialog" title="ACH Multiple Batch Template Detail" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

    <sj:dialog id="editPendingACHBatchDialogID" cssClass="pmtTran_taxDialog" title="Edit Tax Batch Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
    </sj:dialog>

	<sj:dialog id="deleteACHBatchDialogID" cssClass="pmtTran_taxDialog" title="Delete Tax Batch Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800"  height="500" cssStyle="overflow:hidden;">
	</sj:dialog>

    <sj:dialog id="deleteTaxTypeDialogID" cssClass="pmtTran_taxDialog" title="Delete Tax Type Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
    </sj:dialog>

    <sj:dialog id="deleteACHTemplateDialogID" cssClass="pmtTran_taxDialog" title="Delete Tax Batch Template Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

    <sj:dialog id="deleteSingleACHTemplatesDialogID" cssClass="pmtTran_taxDialog" title="Delete Tax Batch Template Confirm" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500">
	</sj:dialog>

    <sj:dialog id="addEditACHEntryDialogID" cssClass="pmtTran_taxDialog" title="Add/Edit Entry" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="820">
    </sj:dialog>

    <sj:dialog id="importEntryAddendaDialogID" cssClass="pmtTran_taxDialog" title="%{getText('ach.importAddendaDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
    </sj:dialog>

    <sj:dialog id="setAllAmountDialogID" cssClass="pmtTran_taxDialog" title="Set All Amount" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
    </sj:dialog>

    <sj:dialog id="saveAsACHTemplateDialogID" cssClass="pmtTran_taxDialog" title="Tax Templates" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
    </sj:dialog>

    <sj:dialog id="ACHsearchDestinationBankDialogID" cssClass="pmtTran_taxDialog" title="Bank Lookup Results" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
	</sj:dialog>

	<sj:dialog id="deleteMultiACHTemplateDialogID" cssClass="pmtTran_taxDialog" title="Delete Multiple ACH Template Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
	
	<sj:dialog id="setACHBatchTypeDialogID" cssClass="pmtTran_taxDialog" title="%{getText('ach.changeBatchType.header')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="700">
    </sj:dialog>
    
    <sj:dialog id="skipACHBatchDialogID" cssClass="pmtTran_taxDialog" title="Skip Tax Batch Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800"  height="500" cssStyle="overflow:hidden;">
	</sj:dialog>
<script>

	$('#importEntryAddendaDialogID').addHelp(function(){
		var helpFile = $('#importEntryAddendaDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	
	$("#TaxBatchesLink").find("span").addClass("dashboardSelectedItem");
</script>
