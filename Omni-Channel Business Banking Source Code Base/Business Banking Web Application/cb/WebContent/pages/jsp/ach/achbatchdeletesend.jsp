<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achbatchdeletesend" className="moduleHelpClass"/>
<ffi:process name="DeleteACHBatch" />
<ffi:removeProperty name="ViewMultipleBatchTemplate"/>
