<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div id="achEntryGridDiv"  style="padding-top: 10px; clear:both;" class="sectionsubhead">
		<s:include value="%{#session.PagesPath}/ach/achbatchaddeditentrygrid.jsp" />
</div>
<%-- <div id="achEntryHeaderDiv"  style="" class="sectionsubhead">
	<s:include value="%{#session.PagesPath}/ach/achbatchaddeditentryheader.jsp" />
</div> --%>
<ffi:cinclude value1="${AddEditACHBatch.TaxFormChangedCoDiscretionaryData}" value2="true">
    <span id="TaxFormChangedCoDiscretionaryDataSpan" style="display:none;">
        <ffi:getProperty name="AddEditACHBatch" property="CoDiscretionaryData"/>
    </span>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.TaxFormChangedCoEntryDesc}" value2="true">
    <span id="TaxFormChangedCoEntryDescSpan" style="display:none;">
        <ffi:getProperty name="AddEditACHBatch" property="CoEntryDesc"/>
    </span>
</ffi:cinclude>
