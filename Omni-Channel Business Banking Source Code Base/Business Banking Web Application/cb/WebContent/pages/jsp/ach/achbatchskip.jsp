<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achbatchskip" className="moduleHelpClass"/>

<%-- required to access the common ach batch pages--%>
<s:include value="%{#session.PagesPath}/ach/inc/include-view-transaction-details-constants.jsp" />
		<div align="center" class="approvalDialogHt390">
			<div align="left">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="columndata lightBackground">
					<s:form action="/pages/jsp/ach/skipACHBatch.action" method="post" name="achBatchSkip" id="skipACHBatchFormId" >
						<s:hidden name="ID" value="%{#attr.ID}"/>
        				<s:hidden name="RecID" value="%{#attr.RecID}"/>
        				<s:hidden id="skipRecurringModel" name="skipRecurringModel" value="%{#attr.skipRecurringModel}"/>
        				<s:hidden id="isSkipInstance" name="isSkipInstance" value="%{#attr.isSkipInstance}"/>        				
        				
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<%-- <ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="equals" >
                <input type="hidden" name="DeleteACHBatch.Process" value="true" >
</ffi:cinclude>
 --%>
								<table width="98%" border="0" cellspacing="0" cellpadding="3">
							<%-- <s:if test="%{#attr.ACHBatch.FrequencyValue > 0}">
								<s:if test="%{skipRecurringModel}">
									<tr>
										<td class="sectionsubhead" colspan="3" style="color: red"
											align="center">
											<s:text name="jsp.ach.warning.deleting.recurring.achBatch" />
										<br>
										</td>
									</tr>
								</s:if>
								<s:else>
									<tr>
										<td class="sectionsubhead" colspan="3" style="color: red"
											align="center">
											<s:text name="jsp.ach.warning.deleting.recurring.achBatch2" />
										<br>
										</td>
									</tr>
								</s:else>
							</s:if> --%>
									<tr>
										<td class="lightBackground" colspan="6">
						<div>
						<s:include value="%{#session.PagesPath}/ach/achbatchcommon.jsp" />
					    </div>
										</td>
									</tr>
									<!-- <tr>
										<td class="sectionsubhead" colspan="6"><hr class="thingrayline"></td>
									</tr> -->
								</table>

								<%-- include batch entries --%>
								
								</s:form>
							</td>
							<td></td>
						</tr>
					</table>
					
<div align="center" class="sectionhead" style="color:red">
		<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
				<s:text name="jsp.ach.skip.ACHBatch.msg" />
		</ffi:cinclude>
		<ffi:cinclude value1="${subMenuSelected}" value2="child" >
				<s:text name="jsp.ach.skip.ChildSupportBatch.msg"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
				<s:text name="jsp.ach.skip.TaxBatch.msg" />
		</ffi:cinclude>
</div>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div  class="ui-widget-header customDialogFooter">
						<sj:a 
			                button="true" 
							onClickTopics="closeDialog,reloadPendingACHBatchSummaryGrid"
			        	><s:text name="jsp.default_82"/>
			        	</sj:a>

	<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
				    <input id="skipBatchMessageId" class="submitbutton" type="hidden" value="ACH batch has been skipped!">
				    	<sj:a id="skipACHLink" 
				        	formIds="skipACHBatchFormId" 
				        	targets="resultmessage" 
				        	button="true" 						  							
							title="Skip ACH Batch" 
							onCompleteTopics="skipSingleACHBatchComplete" 
							onSuccessTopics="skipACHSuccessTopics" 
							onErrorTopics="cancelACHErrorTopics"
							effectDuration="1500" ><s:text name="jsp.default_Skip" />
						</sj:a>
	</ffi:cinclude>
	   	<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
  				    <input id="skipBatchMessageId" class="submitbutton" type="hidden" value="Tax batch has been skipped!">
  				    <sj:a 
  				        	formIds="skipACHBatchFormId" 
  				        	targets="resultmessage" 
  				        	button="true" 						  							
  							title="Skip Tax Batch" 
  							onCompleteTopics="skipSingleACHBatchComplete" 
  							onSuccessTopics="skipACHSuccessTopics" 
						    onErrorTopics="cancelACHErrorTopics"
						    effectDuration="1500" ><s:text name="jsp.default_Skip" />
 					</sj:a>
 				    
   	</ffi:cinclude>
   	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
  				    <input id="skipBatchMessageId" class="submitbutton" type="hidden" value="Child Support batch has been skipped!">
  				    	<sj:a
  				        	formIds="skipACHBatchFormId" 
  				        	targets="resultmessage" 
  				        	button="true" 						  							
  							title="Skip Child Support Batch" 
  							onCompleteTopics="skipSingleACHBatchComplete" 
							onSuccessTopics="skipACHSuccessTopics" 
							onErrorTopics="cancelACHErrorTopics"
  							effectDuration="1500" ><s:text name="jsp.default_Skip" />
  						</sj:a>
 
   	</ffi:cinclude>


			        	
					</div>
			</div>
		</div>
