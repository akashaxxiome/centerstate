<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-jquery-grid-tags" prefix="sjg"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
<!--
    ns.ach.setInputTabText('#normalInputTab');
    ns.ach.setVerifyTabText('#normalVerifyTab');
    ns.ach.setConfirmTabText('#normalConfirmTab');
    ns.ach.showInputDivSteps();
//-->
</script>

<ffi:setProperty name="OrigEditACHPayeePrenoteStatus" value="Not Required" />
<ffi:cinclude value1="PRENOTE_PENDING" value2="${AddEditACHPayee.PrenoteStatus}"><ffi:setProperty name="OrigEditACHPayeePrenoteStatus" value="Requested" /></ffi:cinclude>
<ffi:cinclude value1="PRENOTE_REQUESTED" value2="${AddEditACHPayee.PrenoteStatus}"><ffi:setProperty name="OrigEditACHPayeePrenoteStatus" value="Requested" /></ffi:cinclude>
<ffi:cinclude value1="PRENOTE_SUCCEEDED" value2="${AddEditACHPayee.PrenoteStatus}"><ffi:setProperty name="OrigEditACHPayeePrenoteStatus" value="Prenote Succeeded" /></ffi:cinclude>
<ffi:cinclude value1="PRENOTE_FAILED" value2="${AddEditACHPayee.PrenoteStatus}"><ffi:setProperty name="OrigEditACHPayeePrenoteStatus" value="Prenote Failed" /></ffi:cinclude>

<%
	if(request.getParameter("IsManagedParticipants") != null) {
		session.setAttribute("IsManagedParticipants", "true");
	}
	
	if(request.getParameter("ManagedGridPayee") != null) {
		session.setAttribute("ManagedGridPayee", "true");
	} else session.removeAttribute("ManagedGridPayee");

	if(request.getParameter("IsPending") != null) {
		session.setAttribute("IsPending", "true");
	}
	if (request.getParameter("Action") != null) {
		session.setAttribute("Action", request.getParameter("Action"));
	}
	
%>
<span id="PageHeading" style="display:none;"><s:text name="ach.editPayee" /></span>

	<input id="entryId" type="hidden" value='<%=session.getAttribute("EntryID") %>'>
	
    <s:form id="ACHPayeeForm" validate="false" namespace="/pages/jsp/ach" action="editACHPayeeAction_verify"
        method="post" name="FormName" theme="simple" >
        <input type="hidden" name="isEdit" value="true"/>
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
            <div class="type-text">
                <%-- using s:include caused buffer overflow --%>
                <ffi:include page="/pages/jsp/ach/achpayeeaddedit.jsp" />
            </div>
        <table width="100%">
            <tr>
                <td align="center">
        <%if(session.getAttribute("ManagedGridPayee") != null) { %>        
	        <sj:a id="cancelFormButtonOnInput"
	                button="true"
	                onClick="ns.ach.gotoManagedPayees();"
	        ><s:text name="jsp.default_82" /></sj:a>
        <%} else if(session.getAttribute("IsManagedParticipants") != null) { %>        
	        <sj:a id="cancelFormButtonOnInput"
	                button="true"
					onClick="ns.ach.cancelManagedPayee('/cb/pages/jsp/ach/editACHEntryAction_cancelManagedParticipants.action');"
	        ><s:text name="jsp.default_82" /></sj:a>
        <%} else { %>
        	<sj:a id="cancelFormButtonOnInput"
                button="true"
                onClickTopics="showSummary,cancelACHPayeeForm"
        ><s:text name="jsp.default_82" /></sj:a>
        <%} %>
        <sj:a
        id="verifyACHPayeeSubmit2"
        targets="verifyDiv"
		formIds="ACHPayeeForm"
        button="true"
        validate="true"
        validateFunction="customValidation"
        onBeforeTopics="beforeVerifyACHPayeeForm"
        onCompleteTopics="verifyACHPayeeFormComplete"
        onErrorTopics="errorVerifyACHPayeeForm"
        onSuccessTopics="successVerifyACHPayeeForm"
        ><s:text name="jsp.common.verifypayee" /></sj:a>
		</td></tr></table>
    </s:form>
