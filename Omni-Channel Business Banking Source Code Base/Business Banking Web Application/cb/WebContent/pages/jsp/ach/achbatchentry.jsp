<%@ page import="com.ffusion.beans.ach.ACHPayee,
                 com.ffusion.beans.SecureUser,
		         com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.ach.ACHOffsetAccount,
				 com.ffusion.beans.ach.ACHAccountTypes,
				 com.ffusion.beans.ach.ACHEntry"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:action namespace="/pages/jsp/ach" name="ACHInitAction_initACHBatches" />

<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
<ffi:process name="ACHResource" />
<ffi:object id="StateResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="StateResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="StateResource" />
<ffi:removeProperty name="IsManagedParticipants"/>
<script>
	$("#PayeeGroupID").selectmenu({width: 310});
	$("#AccountType").selectmenu({width: 160});
	$("#BranchCountry").selectmenu({width: 310});
    $("#Country").selectmenu({width: 310});
	$("#BankIdentifierType").selectmenu({width: 160});
	$("#TransactionTypeCode").selectmenu({width: 310});
    $("#OFFSETACCOUNT").selectmenu({width: 200});
    $("#TERMINALSTATE").selectmenu({width: 160});

    <%
	String selectedSECCode="";
	%>

	<ffi:setProperty name="tempPayeeType" value="Domestic"/>
	<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="selectedSECCode" />
	<%
	// if can have addenda, need to know if they are only entitled to the + Addenda code
	    if (selectedSECCode != null && selectedSECCode.length() > 3)
	        selectedSECCode = selectedSECCode.substring(0,3);
	    if ("IAT".equals(selectedSECCode))
	    { %>
			<ffi:setProperty name="tempPayeeType" value="International"/>
		  <%
	    }
	%>

    $(function(){
    	$("#participantID").lookupbox({
    		"controlType":"server",
    		"collectionKey":"1",
    		"module":"ach",
    		"size":"35",
    		"source":"/cb/pages/jsp/ach/achPayeesLookupBoxAction.action?searchOper=cn&payeeType=<ffi:getProperty name="tempPayeeType"/>&CanInsert=true&ACHBatchName=AddEditACHBatch&Show=approved"
    	});
    });

    function openParticipantDetails(){
		if(!$('#expand').is(":visible"))
		{
			$('.payeeDetails').show();
		}
		else
		{
			$('.payeeDetails').hide();
		}
		if($.trim($('#participantID').val())!='' && $.trim($('#participantID').val())!='-1')
		{
			$('.noParticipantAddedMsg').hide();
		}
    }

	function managedPayeeValidation(form, errors)
	{
	customValidation(form, errors);
	openParticipantDetails();
	}

    function toggleParticipantDetails(){
    	$('.payeeDetails').toggle();
    	$('.test').toggle();
    }
    function loadPayee(id){
    		ns.ach.insertACHPayee("/cb/pages/jsp/ach/editACHEntryAction_insertManagedParticipant.action?ManagedPayee=true&PayeeID="+$('#participantID').val());
    }

    function addNewEntry(){
    	$('#isNew').val('true');
    	$('#addEntryButtonId').trigger('click');
    }

</script>

<script>
	 $(document).ready(function(){

		if(($.trim($('#participantID').val())!='' && $.trim($('#participantID').val())!='-1')
			|| ($.trim($('#entryID').val())!='')){
			$('#expand').toggle();
			$('.noParticipantAddedMsg').toggle();
		}

		if($('#isNew').val()=='true'){
			openParticipantDetails();
		}

	 	if($('#isDeleteEntry').val()){
	 		$('#expand').toggle();
			$('.noParticipantAddedMsg').toggle();
		}

	 	if($("#achEntryImportAddendaId").hasClass("importDisable")){
		 	$(this).unbind("click");
	 	}
	});
function ACHEntryViewAddenda(){
	$("#achEntryViewAddendaId").trigger('click');
}

</script>

<style>
input#NickName, input#NAME, input#USERACCOUNTNUMBER, input#STREET, input#CITY, input#ZIPCODE, input#STATE{width: 300px;}
</style>
<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
    <ffi:process name="ACHResource" />

<%
boolean isManaged = false;
boolean addEntry = false;	// default to edit
boolean deleteEntry = false;	// default to edit
boolean creditDisabled = false;
boolean debitDisabled = false;
boolean creditChecked = false;
boolean debitChecked = false;
boolean isInternational = false;
boolean showDiscretionaryData = true;
String entToCheck1 = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
String entToCheck2 = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_TEMPLATE );
String batchCompanyID = "";
boolean isTemplate = false;
boolean showSameDayPrenote=false;
boolean disableSameDayPrenote=true;
if (request.getAttribute("DeleteEntry") != null){
	deleteEntry = true;
}

String classCode = null;
String debitBatch = null;
boolean fromTemplate = false;
String divStyle="";
%>
<ffi:cinclude value1="${AddEditACHBatch.FromTemplate}" value2="true" >
	<% fromTemplate = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsTemplate}" value2="true" >
	<% isTemplate = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${ModifyACHEntry.Action}" value2="ADD" >
    <% addEntry = true; %>
</ffi:cinclude>

<ffi:cinclude value1="${ACHEntryPayee.Scope}" value2="<%=ACHPayee.SCOPE_ACHCOMPANY%>" operator="equals" >
	<% isManaged = true; %>
</ffi:cinclude>
<ffi:getProperty name="AddEditACHBatch" property="CoID" assignTo="batchCompanyID" />

<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="classCode" />
<ffi:getProperty name="AddEditACHBatch" property="DebitBatch" assignTo="debitBatch" />
<%
	if (classCode.equalsIgnoreCase("ARC") ||
		classCode.equalsIgnoreCase("BOC") ||
		classCode.equalsIgnoreCase("POP") ||
		classCode.equalsIgnoreCase("TEL") ||
		classCode.equalsIgnoreCase("RCK") ||
		classCode.equalsIgnoreCase("XCK"))
		creditDisabled = true;
	if (classCode.equalsIgnoreCase("CIE"))
		debitDisabled = true;
	if (debitBatch.startsWith("0"))		// resource file says credit, disable debit
		debitDisabled = true;
	if (debitBatch.startsWith("1"))		// resource file says debit, disable credit
		creditDisabled = true;
	if (creditDisabled)
		debitChecked = true;
	if (debitDisabled)
		creditChecked = true;
	if (classCode.equalsIgnoreCase("WEB") ||
        classCode.equalsIgnoreCase("TEL") ||
		classCode.equalsIgnoreCase("IAT"))
		showDiscretionaryData = false;
    if (classCode.equalsIgnoreCase("IAT")) {
        isInternational = true;
        divStyle="style='height:580px;'";
    }
	if (debitBatch == null || debitBatch.length() == 0) { %>
		<ffi:setProperty name="ACHResource" property="ResourceID" value='<%="ACHMixedDefault"+classCode%>'/>
		<ffi:getProperty name="ACHResource" property="Resource" assignTo="debitBatch" />
	<% 		if (debitBatch.startsWith("0")) creditChecked = true;
			if (debitBatch.startsWith("1")) debitChecked = true;
	}
%>
    <ffi:cinclude value1="${ModifyACHEntry.AmountIsDebit}" value2="false">
        <% creditChecked = true; %>
    </ffi:cinclude>
    <ffi:cinclude value1="${ModifyACHEntry.AmountIsDebit}" value2="true">
        <% debitChecked = true; %>
    </ffi:cinclude>
    <ffi:cinclude value1="${debitEntitled}" value2="false">
        <% if (!debitChecked) {
            debitDisabled = true;
            // QTS 637733: if we are disabling, make sure the other item is checked
            creditChecked = true;
        }
        %>
    </ffi:cinclude>
    <ffi:cinclude value1="${creditEntitled}" value2="false">
        <% if (!creditChecked) {
            creditDisabled = true;
            // QTS 637733: if we are disabling, make sure the other item is checked
            debitChecked = true;
        } %>
    </ffi:cinclude>
    <% if (fromTemplate || deleteEntry) {
        creditDisabled = true;
        // QTS 637733: if we are disabling, make sure the other item is checked
        debitDisabled = true;
    } %>




<s:if test="showSameDayPrenote=='true'">
    <% showSameDayPrenote = true; %>
</s:if>

<s:if test="disableSameDayPrenote=='false'">
    <% disableSameDayPrenote = false; %>
</s:if>

<script language="JavaScript">
<!--

// NOTE:  ns.ach.ACHEntryImportAddenda belongs here instead of ach_grid.js
// because Multiple Child Templates can change ACH TYPE to ACH Batch and
// then if child_grid.js was loaded, the ACHEntryImportAddenda button wouldn't work
ns.ach.ACHEntryImportAddenda = function() {
	var urlString = "/cb/pages/jsp/ach/achentryimportaddenda.jsp";
	$.ajax({
		url : urlString,
		success : function(data) {
			$('#importEntryAddendaDialogID').html(data).dialog('open');
		}
	});
};

    var payeeType = "<ffi:getProperty name="ACHEntryPayee" property="PayeeTypeValue"/>";


var accountTypeList = null;

/* Mappings of banks to indices into the account lists.*/
var countries = null;
var iatAccountList = null;
var iatCurrencyList = null;
var iatForeignExchangeIndicatorFF = null;
var iatForeignExchangeIndicatorFV = null;
var iatForeignExchangeIndicatorVF = null;
var BIC_IBANCountries = null;
var OTHER_RTNCountries = null;
var iatSupportedCurrencyList = null;
var iatSupportedCurrencyNamesList = null;
var bankIdentTypeList = null;

function initIATFeatures()
{
if ( payeeType == "1" )        // domestic, don't show
    return;             // nothing to do
if (countries != null)
    return;
var branchCountrySel = document.getElementById("BranchCountry");

var newOptDefault = null;
var newBranchOptDefault = null;

var countryName = ""; var title = "";
countries = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCodes" property="Size"/>);
iatAccountList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCodes" property="Size"/>);
iatCurrencyList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCodes" property="Size"/>);
<% int countryIndex = 0; %>
<ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
    countryCode = "<ffi:getProperty name="countryCode"/>";
    countries[ <%= countryIndex%> ] = countryCode;
iatAccountList[ <%= countryIndex%> ] = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryAccountTypes${countryCode}"/><ffi:getProperty name="ACHResource" property="Resource"/>";
iatCurrencyList[ <%= countryIndex%> ] = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyList${countryCode}"/><ffi:getProperty name="ACHResource" property="Resource"/>";
    countryName = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/><ffi:getProperty name="ACHResource" property="Resource" encode="false"/>";
    if (countryName == null) countryName = "";
    title = countryName;
    if (countryName.length > 25){
        countryName = countryName.substring(0,25);
    }
<% countryIndex++; %>
    // country #0 is "Select Country"
    newOptDefault = new Option(countryName, countryCode);
    newBranchOptDefault = new Option(countryName, countryCode);

    <ffi:cinclude value1="${countryCode}" value2="${ACHEntryPayee.DestinationCountry}">
        newOptDefault.selected = true;
    </ffi:cinclude>
    <ffi:cinclude value1="${countryCode}" value2="${ACHEntryPayee.BankCountryCode}">
        newBranchOptDefault.selected = true;
    </ffi:cinclude>

    newOptDefault.title = title;
    newBranchOptDefault.title = title;
    if (branchCountrySel != null)
        branchCountrySel.options[ <%= countryIndex%> ] = newBranchOptDefault;
</ffi:list>

iatForeignExchangeIndicatorFF = "<ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicatorFF"/><ffi:getProperty name="ACHResource" property="Resource"/>";
iatForeignExchangeIndicatorFV = "<ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicatorFV"/><ffi:getProperty name="ACHResource" property="Resource"/>";
iatForeignExchangeIndicatorVF = "<ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicatorVF"/><ffi:getProperty name="ACHResource" property="Resource"/>";

BIC_IBANCountries = "<ffi:setProperty name="ACHResource" property="ResourceID" value="BIC_IBANCountries"/><ffi:getProperty name="ACHResource" property="Resource" />";
OTHER_RTNCountries = "<ffi:setProperty name="ACHResource" property="ResourceID" value="OTHER_RTNCountries"/><ffi:getProperty name="ACHResource" property="Resource" />";

var currencySel = document.getElementById("CURRENCY");
var currencyText = "";
var newCurrencyOptDefault = null;
iatSupportedCurrencyList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCurrencyList" property="Size"/>);
iatSupportedCurrencyNamesList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCurrencyList" property="Size"/>);
<%
if(!(isManaged || fromTemplate || deleteEntry)){
	int currencyIndex = 0; %>
<ffi:list collection="ACHIAT_DestinationCountryCurrencyList" items="currencyCode">
    currencyCode = "<ffi:getProperty name="currencyCode"/>";
    iatSupportedCurrencyList [ <%= currencyIndex%> ] = currencyCode;
    iatSupportedCurrencyNamesList [ <%=currencyIndex%> ] = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${currencyCode}"/><ffi:getProperty name="ACHResource" property="Resource" encode="false"/>";

    currencyText = currencyCode+"-"+iatSupportedCurrencyNamesList [ <%=currencyIndex%> ];
    title = currencyText;
    if (currencyText.length > 25){
        currencyText = currencyText.substring(0,25);
    }
    newCurrencyOptDefault = new Option(currencyText, currencyCode);
    newCurrencyOptDefault.title = title;

    <ffi:cinclude value1="${currencyCode}" value2="${ACHEntryPayee.CurrencyCode}">
        newCurrencyOptDefault.selected = true;
    </ffi:cinclude>

	<% currencyIndex++; %>
    if (currencySel != null)
        currencySel.options[ <%= currencyIndex%> ] = newCurrencyOptDefault;
</ffi:list>
<% } %>

}

    function updateAccountInfo(country)
    {
         updateTruncatedText("BranchCountry");
        if (accountTypeList == null)
        {
            var fromSel = document.getElementById("AccountType");
            if (fromSel != null)
            {
                accountTypeList = new Array();
                var i;
                for(i = 0; i < fromSel.length; i++)
                {
                    accountTypeList[ i ]=fromSel.options[ i ].value + "~" + fromSel.options[ i ].text;
                }
            }
        }
        var fromSel = document.getElementById("AccountType");
        if (fromSel != null)
        {
            var supportedAccounts = "";
            for (i = 0; i < countries.length; i++)
            {
                if (countries[ i ] == country.value)
                {
                    supportedAccounts = iatAccountList[ i ];
                    if (supportedAccounts == "")            // if nothing is listed, assume Checking.
                        supportedAccounts="1";

                    //clear out entries in the lists
                    while( fromSel.options.length > 0 ){
                        fromSel.options[ 0 ] = null;
                    }

                    var newCount = -1;
                    for (i = 0; i < accountTypeList.length; i++)
                    {
                        var loc = accountTypeList[ i ].indexOf('~');
                        var value = accountTypeList[ i ].substring(0,loc);
                        var text = accountTypeList[ i ].substring(loc+1);

                        if (value.length == 0 || supportedAccounts.indexOf(value) >= 0)
                        {
                            var newOpt = new Option(text, value);
                            newCount++;
                            fromSel.options[ newCount ] = newOpt;
                        }
                    }
                    break;
                }
            }
        }
        $("#AccountType").selectmenu('destroy').selectmenu({width: 120});
    }

	function clearAddenda(){
		document.getElementById("ACHADDENDA").value='';
	}



	function updateTruncatedText(controlName)
	{
		if (controlName == "ALL")
		{
			updateTruncatedText("BranchCountry");
			updateTruncatedText("Country");
			return;
		}
		var truncatedText = document.getElementById(controlName + "TruncatedText");
		var countrySel = document.getElementById(controlName);
		if (truncatedText != null)
		{
			taxtitle = countrySel.options[ countrySel.selectedIndex ].title;
			truncatedText.innerHTML = taxtitle;
			if (taxtitle.length > 25)
			{
				$(truncatedText).show();
			}
			else
			{
				$(truncatedText).hide();
			}
		 }
	}
    function showCreditDebit()
    {
        var creditDisabled = <%=creditDisabled%>;
        var debitDisabled = <%=debitDisabled%>;
        var creditChecked = <%=creditChecked%>;
        var debitChecked = <%=debitChecked%>;
//        alert (" creditDisabled="+creditDisabled+
//               " debitDisabled="+debitDisabled+
//               " creditChecked="+creditChecked+
//               " debitChecked="+debitChecked );

        if (creditDisabled)         // don't display if disabled
        {
            if (!creditChecked)
                document.getElementById("CreditDisabled").style.display = "none";
            document.getElementById("DebitRadio").style.display = "none";
        }
        if (debitDisabled)          // don't display if disabled
        {
            if (!debitChecked)
                document.getElementById("DebitDisabled").style.display = "none";
            document.getElementById("CreditRadio").style.display = "none";
        }
    }
    function showHide()
    {
        initIATFeatures();
        // disable the International Address
        if ( payeeType == "1" )        // domestic, don't show
        {
            // if the DOM uses the "IAT" class, hide it
            // if the DOM uses the "ManagedPrenote" class, show it
            $('.IAT').hide();
            $('.ManagedPrenote').show();
        	$('.SameDayPrenote').show();
			if($('#radio_PRENOTE_NOT_REQUIRED').is(':checked'))
				{
				  $('input:checkbox[name="achPayee.prenoteType"]').attr("disabled", true);
				}


				$('input:radio[name="achPayee.prenoteStatus"]').change(function(){

				var isPrenoteDisabled=true;
				<%	if(!disableSameDayPrenote){%>
						isPrenoteDisabled=false;
				<%	}%>

						if($(this).val() == 'PRENOTE_REQUESTED'){
						if(!isPrenoteDisabled){
								$('input:checkbox[name="achPayee.prenoteType"]').attr("disabled", false);
						   }
						}
						else
						{
						if(!isPrenoteDisabled){
							  $('input:checkbox[name="achPayee.prenoteType"]').attr("checked", false );
							  $('input:checkbox[name="achPayee.prenoteType"]').attr("disabled", true);
						  }
						}
				});

        }
        else
        {
            $('.IAT').show();
            $('.ManagedPrenote').hide();
        	$('.SameDayPrenote').hide();
			updateTruncatedText("ALL");
        }
    }

    function showHideTransactions( )
    {
        var trnType = document.getElementById("TransactionTypeCode");
        if(trnType != null)
        {
            if ( trnType.value == "ARC" || trnType.value == "BOC" || trnType.value == "RCK")
            {
                document.getElementById("IAT_CHECKSERIALNUMBER").style.display = "";
                document.getElementById("IAT_TERMINALCITYSTATE").style.display = "none";
            }
            else
            if ( trnType.value == "POP")
            {
                document.getElementById("IAT_CHECKSERIALNUMBER").style.display = "";
                document.getElementById("IAT_TERMINALCITYSTATE").style.display = "";
            }
            else
            {
                document.getElementById("IAT_CHECKSERIALNUMBER").style.display = "none";
                document.getElementById("IAT_TERMINALCITYSTATE").style.display = "none";
            }
        }
    }

	function prenoteChanged( value )
	{
		var hiddenPrenote = document.getElementById("hiddenPrenote");
        if (hiddenPrenote != null)
		    hiddenPrenote.value = value;
	}

	function addToListChanged() {
		var hiddenAddToList = document.getElementById("hiddenAddToList");
        var addToList = document.getElementById("AddToList");
        var payeeGroupID = document.getElementById("PayeeGroupID");
        var nickname = document.getElementById("NickName");
        var hiddenPrenote = document.getElementById("hiddenPrenote");
        var prenote = document.getElementById("prenote");
        var name = document.getElementById("NAME");
        var userAcctNum = document.getElementById("USERACCOUNTNUMBER");
		var classCode = "<%= classCode %>";
        var debitBatch = "<%= debitBatch %>";
		var isManaged = "<%= isManaged %>";
		if (isManaged == "true")		// nothing to do
			return;


		if (addToList != null)
		{
			if (addToList.checked == true) {
				hiddenAddToList.value = "true";
				payeeGroupID.disabled = false;
				if(classCode!='IAT')
				{
					$("#Managed_IdRequired").html("<span class='required'>*</span>");
					$("#Managed_IdRequired").show();
				}
				$("#PayeeGroupID").selectmenu('enable');
				$("input[name='achPayee.prenoteStatus']").removeAttr("disabled");
				$("input[name='achPayee.securePayee']").removeAttr("disabled");
				if (prenote != null)
				{
					prenote.checked = false;
					prenote.disabled = true;
				}

				if($('#radio_PRENOTE_NOT_REQUIRED').is(':checked'))
				{
				  $('input:checkbox[name="achPayee.prenoteType"]').attr("disabled", true);
				}
				else
				{
				 $('input:checkbox[name="achPayee.prenoteType"]').removeAttr("disabled");
				}


                if (hiddenPrenote != null)
		    		hiddenPrenote.value = 'false';
				if (classCode == "XCK")
				{
					if (nickname.value == "Not Required")
						nickname.value = "";
					if (name.value == "Not Required")
						name.value = "";
					if (userAcctNum.value == "Not Required")
						userAcctNum.value = "";
					nickname.disabled = false;
					name.disabled = false;
					userAcctNum.disabled = false;
				}
			} else {
				hiddenAddToList.value = "false";
				payeeGroupID.disabled = true;
				$("#PayeeGroupID").selectmenu('disable');
				$("input[name='achPayee.prenoteStatus']").attr("disabled", true);
				$("input[name='achPayee.securePayee']").attr("disabled", true);
				$("input[name='achPayee.prenoteType']").attr("disabled", true);
				if (prenote != null)
				{
					prenote.disabled = false;
				}
                if (classCode != "CIE" && !(classCode == "WEB" && debitBatch== "0"))
                {
                    $("#Managed_IdRequired").hide();
                }

				if (classCode == "XCK")
				{
					if (nickname.value == "")
						nickname.value = "Not Required";
					if (name.value == "")
						name.value = "Not Required";
					if (userAcctNum.value == "")
						userAcctNum.value = "Not Required";
					nickname.disabled = true;
					name.disabled = true;
					userAcctNum.disabled = true;
				}
			}
		} else
        if (classCode == "XCK")
        {
            if (nickname.value == "")
                nickname.value = "Not Required";
            if (name.value == "")
                name.value = "Not Required";
            if (userAcctNum.value == "")
                userAcctNum.value = "Not Required";
            nickname.disabled = true;
            name.disabled = true;
            userAcctNum.disabled = true;
        }

	}


// --></script>

		<% String pageHeading = "";
		   String pageHeadingNext="";
		   String batchName = "";
		   String batchType = "";
           String companyName = "";
           String companyID = "";
		if (addEntry) { %>
			<title><ffi:getProperty name="product"/>: Add ACH Entry</title>
			<% pageHeading = ""; %>
		<% } else if (deleteEntry) { %>
			<title><ffi:getProperty name="product"/>: Delete ACH Entry</title>
			<% pageHeading = ""; %>
		<% } else { %>
			<title><ffi:getProperty name="product"/>: Edit ACH Entry</title>
			<% pageHeading = ""; %>
		<% } %>
		<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}"/>
		<ffi:getProperty name="ACHResource" property="Resource" assignTo="batchType" />
		<ffi:getProperty name="AddEditACHBatch" property="Name" assignTo="batchName"/>
        <ffi:getProperty name="AddEditACHBatch" property="CoName" assignTo="companyName" />
        <ffi:getProperty name="AddEditACHBatch" property="CompanyID" assignTo="companyID" />
		<% if (isTemplate) { %>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
        <ffi:getProperty name="AddEditACHBatch" property="TemplateName" assignTo="batchName"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
		<ffi:getProperty name="AddEditACHBatch" property="TemplateName" assignTo="batchName"/>
</ffi:cinclude>
		<% }
		pageHeading +=(isTemplate?"Template Name: ":"Batch Name: ") + batchName +"  (" + companyName + " - " + companyID + ")";
		pageHeadingNext += batchType;
		%>
		<ffi:setProperty name="PageHeading" value="<%=pageHeading%>" />
		<ffi:setProperty name="pageHeadingNext" value="<%=pageHeadingNext%>" />
<%-- <div align="left" <%=divStyle %>> --%>
<div align="left">
<div class="ui-widget ui-widget-content ui-corner-all addACHEntry ui-tabs">
<% if (deleteEntry == true) {
    request.setAttribute("entry_strutsActionName", "deleteACHEntryAction");
 } else if (addEntry == true) {
    request.setAttribute("entry_strutsActionName", "addACHEntryAction_addEntry");
 } else {
    request.setAttribute("entry_strutsActionName", "editACHEntryAction_editEntry");
 } %>
<s:form action="%{#request.entry_strutsActionName}" namespace="/pages/jsp/ach" id="ACHEntryForm" name="ACHEntryForm" validate="false" theme="simple" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<span id="formerrors"></span><span id="entryFormError" style="font-size: 12px; font-weight: bold;"></span><span id="formTitleError"></span>

<%-- <div align="center">
	<span style="display:inline-block; margin:0 10px 0 0;"><ffi:getProperty name="PageHeading"/></span>
	<span style="display:inline-block;"><ffi:getProperty name="pageHeadingNext"/></span>
</div> --%>

<%
boolean isSecure = false;       // should secure information be shown?
ACHPayee payee = (ACHPayee)request.getAttribute("ACHEntryPayee");
if (payee != null && payee.getDisplayAsSecurePayeeValue() == true)
	isSecure = true;
%>

<% if (deleteEntry == true) { %>
	<!-- <div class="sectionhead" align="center" style="color:red; font-weight:bold">L10NStartAre you sure you want to delete this ACH Entry?L10NEnd</div> -->
	<input type="hidden" value="true" id="isDeleteEntry"/>
	<div class="leftPaneWrapper">
		<div class="leftPaneInnerWrapper">
		<div class="header"><s:text name="jsp.ach.ACH.Batch.details" /></div>
		<!-- ACHBatch Entry header and Grid details -->
			<div id="" class="paneContentWrapper">
				<div class="blockWrapper">
					<div>
						<ffi:getProperty name="PageHeading"/>
					</div>
					<div>
						<ffi:getProperty name="pageHeadingNext"/>
					</div>
				</div>
			</div>
		</div>
	</div>
<% } else { %>
<div class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header"><s:text name="jsp.default_502" />
		<% if (fromTemplate) { %>(<s:text name="jsp.ach.from.template.cannot.change" />)<% } %>
		</div>
		<div class="leftPaneInnerBox setWidthForIE">
		<input type="hidden" value="<s:property value="%{entryID}"/>" id="entryID"/>
				<select class="txtbox" id="participantID" name="participantID" size="1" onChange="loadPayee(this)">
					<option>
						<% if (isSecure) { %>
													<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
						<% } else { %>
													<ffi:getProperty name="ACHEntryPayee" property="Name"/>
						<% } %>
					</option>
				</select>

		<% if(!fromTemplate){ %>
			<div class="marginTop10 floatleft" align="center">OR</div>
			<div class="marginTop10 floatleft" align="center">
			<!-- <input type="button" value="Add New" onclick="addNewEntry();"></div> -->
				<sj:a
                button="true"
				onClick="addNewEntry()"
        	><s:text name="jsp.ach.Add.New" />
        	</sj:a>
			<input type="hidden" value="false" id="isNew">
		</div>
		<% } %>
	</div>
	<span id="participantIDError"></span>
</div>
<div class="leftPaneInnerWrapper">
	<div class="header"><s:text name="jsp.ach.ACH.Batch.details" /></div>
	<!-- ACHBatch Entry header and Grid details -->
		<div id="" class="paneContentWrapper">
			<div class="blockWrapper">
				<div>
					<ffi:getProperty name="PageHeading"/>
				</div>
				<div class="marginTop10">
					<ffi:getProperty name="pageHeadingNext"/>
				</div>
			</div>
		</div>
	</div>
</div>
<% } %>
<div class="rightPaneWrapper payeeDetailsWrapper">

<%-- <div class="paneWrapper">
   	<div class="paneInnerWrapper">
		<div class="header">ACH Batch details</div>
<!-- ACHBatch Entry header and Grid details -->
		<div id="" class="paneContentWrapper">
			<div class="blockWrapper">
				<div class="inlineBlock" style="width: 50%">
					<ffi:getProperty name="PageHeading"/>
				</div>
				<div class="inlineBlock">
					<ffi:getProperty name="pageHeadingNext"/>
				</div>
			</div>
		</div>
	</div>
</div> --%>
 <div class="paneWrapper">
   	<div class="paneInnerWrapper">
	   	<div class="header"><s:text name="jsp.ach.Participant.Details"/></div>
		<div class="paneContentWrapper">
			<div class="noParticipantAddedMsg"><s:text name="jsp.ach.No.Participant.added" /></div>


				<a id="expand" onClick="toggleParticipantDetails()" class="hidden nameTitle test">
	        	<% if (isSecure) { %>
											<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
				<% } else { %>
											<ffi:getProperty name="ACHEntryPayee" property="Name"/>
				<% } %>
				<span class="nameSubTitle">(
				<% if (isSecure) { // QTS 428174 %>
									<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
				<% } else { %>
									<ffi:getProperty name="ACHEntryPayee" property="NickName"/>
				<% } %>
				-
				<s:property value="achCompanyID"/>
				-
				<ffi:getProperty name="ACHEntryPayee" property="PayeeGroup"/>
				) </span><span class="sapUiIconCls icon-positive"></span>
	        	</a>
	        	<a onClick="toggleParticipantDetails()" id="collapse" class="hidden nameTitle test">
	        		<% if (isSecure) { %>
											<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
					<% } else { %>
												<ffi:getProperty name="ACHEntryPayee" property="Name"/>
					<% } %>
					<span class="nameSubTitle">(
					<% if (isSecure) { // QTS 428174 %>
										<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
					<% } else { %>
										<ffi:getProperty name="ACHEntryPayee" property="NickName"/>
					<% } %>
					-
					<s:property value="achCompanyID"/>
					-
					<ffi:getProperty name="ACHEntryPayee" property="PayeeGroup"/>
					) </span><span class="sapUiIconCls icon-negative"></span>
	        	</a>

					<% if (payee.getID() != null) { %>
					<%-- Start: Dual approval processing --%>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
						<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
						<ffi:setProperty name="GetDAItem" property="itemId" value="<%=payee.getID()%>"/>
						<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>"/>
						<ffi:process name="GetDAItem"/>
						<ffi:removeProperty name="GetDAItem"/>

						<% String daStatus = ""; %>
						<ffi:getProperty name="DAItem" property="Status" assignTo="daStatus"/>
						<ffi:cinclude value1="<%=daStatus%>" value2="<%=IDualApprovalConstants.STATUS_TYPE_PENDING_APPROVAL%>">
							<ffi:getL10NString rsrcFile="cb" msgKey="da.errormessage.ach.ACHParticipantIsPendingApproval"/>
						</ffi:cinclude>
						<ffi:removeProperty name="DAItem"/>
					</ffi:cinclude>
					<%-- End: Dual approval processing--%>
				 <% } %>
				 <%-- <ffi:cinclude value1="" value2="${ACHEntryPayee.Name}" operator="notEquals" >
				 show labels only
				 </ffi:cinclude>
				  <ffi:cinclude value1="" value2="${ACHEntryPayee.Name}" operator="equals" >
				 show input to fill data
				 </ffi:cinclude> --%>
				 <!-- Show readonly data when participant is selected from select box -->
				 <% if (isManaged) { %>
				 <div class="columndata payeeDetails hidden label130">
					 <div class="blockWrapper">
						<div  class="blockHead"><s:text name="jsp.ach.Participant.Info" /></div>
						<div class="blockContent">
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionLabel">
									<%-- we now ALWAYS show Identification Number because of the check-box for "Add Participant to Managed List" --%>
                                       <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="notEquals">
                                       <s:text name="da.field.ach.name" />
                                       </ffi:cinclude>
                                       <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="equals">
                                       <s:text name="jsp.ach.Receiver.Name"/>
                                        </ffi:cinclude>
										<ffi:cinclude value1="XCK" value2="${AddEditACHBatch.StandardEntryClassCode}" operator="notEquals">
											 :
										</ffi:cinclude>
									</span>
									<span>
										<%-- CIE only allows 15 length - need to check, so add a hidden field, so warning can be given --%>
										<%-- CTX only allows 16 length - need to check, so add a hidden field, so warning can be given --%>
																		<input id="NAME" type="Hidden" name="achPayee.name" value="<ffi:getProperty name="ACHEntryPayee" property="Name"/>">
										<% if (isSecure) { %>
																	<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
										<% } else { %>
																	<ffi:getProperty name="ACHEntryPayee" property="Name"/>
										<% } %>
									</span>
								</div>
								<div class="inlineBlock">
									<span class="sectionLabel">
										<%-- we now ALWAYS show Identification Number because of the check-box for "Add Participant to Managed List" --%>
										<s:text name="jsp.default_294" />:
									</span>
									<span>
										<% if (isSecure) { // QTS 428174 %>
																<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
										<% } else { %>
																<ffi:getProperty name="ACHEntryPayee" property="NickName"/>
										<% } %>
									</span>
								</div>
							</div>
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionLabel">
                                       <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="notEquals">
                                           <s:text name="da.field.ach.userAccountNumber" />
                                       </ffi:cinclude>
                                       <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="equals">
                                           <s:text name="jsp.ach.Sender.Name" />
                                       </ffi:cinclude>
										<%-- Identification Number only required for CIE, but optional for all others where identNumRequired=true --%>
			                            <span id="Managed_IdRequired">
										<ffi:cinclude value1="CIE" value2="${AddEditACHBatch.StandardEntryClassCode}" operator="equals">

										</ffi:cinclude>
			                            <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="equals">

			                            </ffi:cinclude>
			                            </span>
									</span>
									<span>
									<%-- XCK is not identNumRequired, so no need to check for XCK --%>
											<% if (isManaged) { %>
											    <input type="Hidden" id="USERACCOUNTNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" name="achPayee.userAccountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="UserAccountNumber"/>">
												<ffi:getProperty name="ACHEntryPayee" property="UserAccountNumber"/>
											<% } %>
									</span>
								</div>
								<div class="inlineBlock">
									<span class="sectionLabel">
										<% if (isManaged || (isTemplate && !classCode.equalsIgnoreCase("XCK"))) { %>
											<s:text name="ach.grid.prenote" />:
										<% } %>
									</span>
									<span>
										<% if (isManaged || (isTemplate && !classCode.equalsIgnoreCase("XCK"))) { %>

										<ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_NOT_REQUIRED" operator="equals" >Not required
										<input type="hidden" value="PRENOTE_NOT_REQUIRED" name="achPayee.prenoteStatus"/>
										</ffi:cinclude>

										<ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_REQUESTED" operator="equals" >Requested
										<input type="hidden" value="PRENOTE_REQUESTED" name="achPayee.prenoteStatus"/>
										</ffi:cinclude>

										<ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_PENDING" operator="equals" >Requested
										<input type="hidden" value="PRENOTE_PENDING" name="achPayee.prenoteStatus"/>
										</ffi:cinclude>
										<% } %>
									</span>
								</div>
							</div>
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<%-- Start: Dual approval processing --%>
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<span class="sectionLabel">
										<s:text name="da.field.ach.payeeGroup" />:
									</span>
									</ffi:cinclude>
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<span>
										<%-- Start: Dual approval processing --%>
											<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
											<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
											<ffi:process name="ACHResource" />
											<ffi:getProperty name="ACHEntryPayee" property="PayeeGroup"/>

									</span>
									</ffi:cinclude>
								</div>
								<div class="inlineBlock">
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<span class="sectionLabel">
											<s:text name="da.field.ach.securePayee" />:
										<%-- End: Dual approval processing--%>
									</span>
									</ffi:cinclude>
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<span>
									<ffi:cinclude value1="${ACHEntryPayee.SecurePayee}" value2="true" operator="equals" >Secure
										<input type="hidden" value="true" name="achPayee.securePayee"/>
									</ffi:cinclude>

									<ffi:cinclude value1="${ACHEntryPayee.SecurePayee}" value2="false" operator="equals" >Normal
										<input type="hidden" value="false" name="achPayee.securePayee"/>
									</ffi:cinclude>
										<%-- End: Dual approval processing--%>
									</span>
									</ffi:cinclude>
								</div>
							</div>
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionLabel">
											<s:text name="jsp.ach.Last.Prenote.Status" />:
									</span>
									<span>
											<ffi:cinclude value1="PRENOTE_PENDING" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Requested<!--L10NEnd--></ffi:cinclude>
											<ffi:cinclude value1="PRENOTE_REQUESTED" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Requested<!--L10NEnd--></ffi:cinclude>
											<ffi:cinclude value1="PRENOTE_NOT_REQUIRED" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Not Required<!--L10NEnd--></ffi:cinclude>
											<ffi:cinclude value1="PRENOTE_SUCCEEDED" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Prenote Succeeded<!--L10NEnd--></ffi:cinclude>
											<ffi:cinclude value1="PRENOTE_FAILED" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Prenote Failed<!--L10NEnd--></ffi:cinclude>
									</span>
								</div>
								<div class="inlineBlock">
									<span class="sectionLabel">
											<s:text name="jsp.default_508" />:
									</span>
									<span class="ManagedPrenote">
										<ffi:setProperty name="ACHEntryPayee" property="DateFormat" value="${UserLocale.DateFormat}" />
										<ffi:getProperty name="ACHEntryPayee" property="PrenoteSubmitDate" />
									</span>
								</div>
							</div>
							<div class="blockRow IAT">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionLabel">
				                       <s:text name="da.field.ach.destinationCountry" />: <span class="required"></span>
				                    </span>
				                  <span><ffi:getProperty name="ACHEntryPayee" property="DestinationCountryName"/></span>
								</div>
								<div class="inlineBlock">
									<span class="sectionLabel"><s:text name="da.field.ach.bankCountryCode" />:</span>
									<span>
				                         <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
				                         <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
				                         <ffi:process name="ACHResource" />
				                         <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
				                             <% String countryName =""; %>
				                             <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
				                             <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName" />
				                             <%
				                                 if (countryName == null) countryName = "";
				                             %>
				                                 <ffi:cinclude value1="${countryCode}" value2="${ACHEntryPayee.BankCountryCode}">
				                                     <%
				                                     if (countryName.length() > 25)
				                                     {
				                                         countryName = countryName.substring(0, 25);
				                                     }
				                               		  %>
				                                     <%=countryName%>
				                                     <input name="achPayee.bankCountryCode" value="<ffi:getProperty name="countryCode"/>" type="hidden"/>
				                                 </ffi:cinclude>

				                             </ffi:list>
	                         			<div id="BranchTruncatedText"></div>
	                      			</span>
								</div>
							</div>
							<div class="blockRow IAT">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionLabel"><s:text name="da.field.ach.currencyCode"/>:</span>
									<span class="columndata">
				                        <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${ACHEntryPayee.CurrencyCode}"/>
				                        <ffi:getProperty name="ACHEntryPayee" property="CurrencyCode" />-<ffi:getProperty name="ACHResource" property="Resource"/>
				                    </span>
								</div>
								<div class="inlineBlock">
									<span class="sectionLabel"><s:text name="da.field.ach.foreignExchangeMethod"/>:</span>
									<span class="columndata">
				                        <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${ACHEntryPayee.ForeignExchangeMethod}"/>
				                        <ffi:getProperty name="ACHEntryPayee" property="ForeignExchangeMethod" />-<ffi:getProperty name="ACHResource" property="Resource"/>
				                    </span>
								</div>
							</div>
						</div>
					</div>
					<div class="blockWrapper IAT">
						<div  class="blockHead"><s:text name="jsp.ach.Participant.Address" />:</div>
						<div class="blockContent">
							<div class="blockRow IAT">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionLabel"><s:text name="da.field.ach.street" />:</span>
									<span><ffi:getProperty name="ACHEntryPayee" property="Street"/></span>
								</div>
								<div class="inlineBlock">
									<span class="sectionLabel"><s:text name="jsp.default_101" />: </span>
									<span><ffi:getProperty name="ACHEntryPayee" property="City"/></span>
								</div>
							</div>
							<div class="blockRow IAT">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionLabel"><s:text name="jsp.default_115" />:</span>
									<span>
				                         <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
				                         <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
				                         <ffi:process name="ACHResource" />
				                         <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
				                             <% String countryName2 =""; %>
				                             <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
				                             <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName2" />
				                             <%
				                                 if (countryName2 == null) countryName2 = "";
				                             %>
				                                 <ffi:cinclude value1="${countryCode}" value2="${ACHEntryPayee.Country}">
				                                    <%
				                                     if (countryName2.length() > 25)
				                                     {
				                                         countryName2 = countryName2.substring(0, 25);
				                                     }
				                                	 %>
				                                     <%=countryName2%>
				                                      <input name="achPayee.country" value="<%=countryName2%>" type="hidden"/>
				                                 </ffi:cinclude>

				                             </ffi:list>
				                     </span>
								</div>
								<div class="inlineBlock">
									<span class="sectionLabel"><s:text name="da.field.ach.zipCode" />: </span>
									<span><ffi:getProperty name="ACHEntryPayee" property="ZipCode"/></span>
								</div>
							</div>
							<div class="blockRow IAT">
								<span class="sectionLabel"><s:text name="jsp.default_387" />: </span>
								<span><ffi:getProperty name="ACHEntryPayee" property="State"/></span>
							</div>
						</div>
					</div>
					<div class="blockWrapper">
						<div  class="blockHead"><s:text name="jsp.ach.Bank.Info" /> </div>
						<div class="blockContent">
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionLabel"><s:text name="da.field.ach.bankName" />:</span>
									<td class="columndata" nowrap>
										<% if (isManaged) { %>
										<% if (isSecure) { %>
											<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
										<% } else { %>
											<ffi:getProperty name="ACHEntryPayee" property="BankName"/>
										<% } %>
										<% } %>
									</td>
								</div>
								<div class="inlineBlock">
									<span class="sectionLabel"><s:text name="da.field.ach.routingNumber" />:</span>
									<span class="columndata">
										<% if (isManaged) { %>
											<% if (isSecure) { %>
											<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
										<% } else { %>
											<ffi:getProperty name="ACHEntryPayee" property="RoutingNumber"/>
										<% } %>
										<% } %>
									</span>
								</div>
							</div>
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionLabel"><s:text name="da.field.ach.bankIdentifierType" /> :</span>
									<span class="columndata">
				                       <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
				                       <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
				                       <ffi:process name="ACHResource" />
				                       <%-- <select id="BankIdentifierType" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="txtbox" name="achPayee.bankIdentifierType"
				                        value="<ffi:getProperty name="ACHEntryPayee" property="BankIdentifierType"/>"> --%>
				                           <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier01"/>
				                           <ffi:cinclude value1="01" value2="${ACHEntryPayee.BankIdentifierType}">
				                            <input name="achPayee.bankIdentifierType" value="01" type="hidden"/>
				                           	<ffi:getProperty name="ACHResource" property="Resource"/>
				                           </ffi:cinclude>
										<%if ("IAT".equals(classCode)) { %>
				                           <ffi:setProperty name="ACHResource" property="ResourceID" value="BIC_IBANCountries"/>
				                           <% String iban = ""; String other = ""; String destCntry = ""; %>
				                           <ffi:getProperty name="ACHResource" property="Resource" assignTo="iban" />
				                           <ffi:setProperty name="ACHResource" property="ResourceID" value="OTHER_RTNCountries"/>
				                           <ffi:getProperty name="ACHResource" property="Resource" assignTo="other" />
				                           <ffi:getProperty name="ACHEntryPayee" property="DestinationCountry" assignTo="destCntry"/>
				                           <% if (iban.indexOf(destCntry) != -1) { %>
				                               <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier02"/>
				                               <ffi:cinclude value1="02" value2="${ACHEntryPayee.BankIdentifierType}">
				                               <ffi:getProperty name="ACHResource" property="Resource"/>
				                               <input name="achPayee.bankIdentifierType" value="02" type="hidden"/>
				                               </ffi:cinclude>
				                               <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier03"/>
				                               <ffi:cinclude value1="03" value2="${ACHEntryPayee.BankIdentifierType}">selected</ffi:cinclude>
				                               <input name="achPayee.bankIdentifierType" value="03" type="hidden"/>
				                               <ffi:getProperty name="ACHResource" property="Resource"/>
				                               </option>
				                           <% } else if (other.indexOf(destCntry) != -1){ %>
				                               <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier99"/>
				                               <ffi:cinclude value1="99" value2="${ACHEntryPayee.BankIdentifierType}">
				                               <input name="achPayee.bankIdentifierType" value="99" type="hidden"/>
				                               				                               <ffi:getProperty name="ACHResource" property="Resource"/>
				                               				                               </ffi:cinclude>
				                           <% } %>
										<% } %>
				                       <%-- </select> --%>
				                   </span>
								</div>
								<div class="inlineBlock">
									<span class="sectionLabel"><s:text name="jsp.default_16" /> :</span>
									<span class="columndata">
										<% if (isManaged) { %>
					                                       <input type="Hidden" <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="ACCOUNTNUMBER" class="ui-widget-content ui-corner-all txtbox" style="width:300px;" type="text" size="32" maxlength="17" name="achPayee.accountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="AccountNumber"/>">
										<% if (isSecure) { %>
											<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
										<% } else { %>
											<ffi:getProperty name="ACHEntryPayee" property="AccountNumber"/>
										<% } %>
										<% } %>
									</span>
								</div>
							</div>
							<div class="blockRow">
								<span class="sectionLabel"><s:text name="jsp.default_20" />:</span>
								<span class="columndata">
									<% if (isManaged) { %>
											<input type="Hidden" name="achPayee.accountType" value="<ffi:getProperty name="ACHEntryPayee" property="AccountType"/>">
										<% if (isSecure) { %>
														<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
										<% } else { %>
														<ffi:getProperty name="ACHEntryPayee" property="AccountType" />
										<% } %>
										<br/><span id="achPayee.accountTypeError"></span>
									<% } %>
								</span>
							</div>
						</div>
					</div>

</div>



				 <% } else { %>
				 <table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData marginTop10">
				 <tr>
					<td class="columndata payeeDetails hidden" valign="top" style="padding: 0">
							<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData">
							<tr>
			                    <td class="sectionhead nameTitle" colspan="2"><s:text name="jsp.ach.Participant.Details"/>
									<span class="nameSubTitle">
										<%-- Start: Dual approval processing --%>
										<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
												<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
													(<input id="hiddenAddToList" <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> type="hidden" name="achPayee.addToList" value="<ffi:getProperty name="ACHEntryPayee" property="AddToList" />" >
													<input  style="padding:0; margin:0" id="AddToList" <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> type="checkbox" name="AddToList" value="true" onclick="addToListChanged()" <ffi:cinclude value1="${ACHEntryPayee.AddToList}" value2="true">checked</ffi:cinclude>>
													<!--L10NStart--><label for="AddToList">Save for future use<!--L10NEnd--></label>)
												</ffi:cinclude>
										</ffi:cinclude>
									</span>
			                    </td>
			                </tr>
								<tr>
									<td width="50%" class="sectionsubhead">
										<%-- we now ALWAYS show Identification Number because of the check-box for "Add Participant to Managed List" --%>
                                       <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="notEquals">
                                       <s:text name="da.field.ach.name" />
                                       </ffi:cinclude>
                                       <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="equals">
                                       <s:text name="jsp.ach.Receiver.Name" />
                                        </ffi:cinclude>
										<ffi:cinclude value1="XCK" value2="${AddEditACHBatch.StandardEntryClassCode}" operator="notEquals">
											<span class="required">*</span>
										</ffi:cinclude>
									</td>
									<td width="50%" class="sectionsubhead">
										<%-- we now ALWAYS show Identification Number because of the check-box for "Add Participant to Managed List" --%>
										<s:text name="jsp.default_294"/>
									</td>
								</tr>
								<tr>
									<td class="columndata">
										<% if (isManaged) { %>
										<%-- CIE only allows 15 length - need to check, so add a hidden field, so warning can be given --%>
										<%-- CTX only allows 16 length - need to check, so add a hidden field, so warning can be given --%>
																		<input id="NAME" type="Hidden" name="achPayee.name" value="<ffi:getProperty name="ACHEntryPayee" property="Name"/>">
										<% if (isSecure) { %>
																	<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
										<% } else { %>
																	<ffi:getProperty name="ACHEntryPayee" property="Name"/>
										<% } %>
										<% } else { %>
										<% int nameLen = 22; %>
																	<ffi:cinclude value1="CIE" value2="${AddEditACHBatch.StandardEntryClassCode}" operator="equals">
										<%-- CIE only allows 15 length --%>
										<% nameLen = 15; %>
																	</ffi:cinclude>
																	<ffi:cinclude value1="CTX" value2="${AddEditACHBatch.StandardEntryClassCode}" operator="equals">
										<%-- CTX only allows 16 length --%>
										<% nameLen = 16; %>
																	</ffi:cinclude>
																	<input id="NAME" <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="<%= nameLen %>" name="achPayee.name" value="<ffi:getProperty name="ACHEntryPayee" property="Name"/>">

										<% } %>
									</td>
									<td class="columndata">
										<% if (isManaged) { %>
										<% if (isSecure) { // QTS 428174 %>
																<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
										<% } else { %>
																<ffi:getProperty name="ACHEntryPayee" property="NickName"/>
										<% } %>
										<% } else { %>
											<input id="NickName" <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="50" name="achPayee.nickName" value="<ffi:getProperty name="ACHEntryPayee" property="NickName"/>">
										<% } %>
									</td>
								</tr>
								<tr>
									<td><span id="achPayee.nameError"></span></td>
									<td><span id="achPayee.nickNameError"></span></td>
								</tr>
								<tr>
									<td class="sectionsubhead">
                                       <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="notEquals">
                                           <s:text name="da.field.ach.userAccountNumber" />
                                       </ffi:cinclude>
                                       <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="equals">
                                           <s:text name="jsp.ach.Sender.Name" />
                                       </ffi:cinclude>
										<%-- Identification Number only required for CIE, but optional for all others where identNumRequired=true --%>
			                            <span id="Managed_IdRequired">
										<ffi:cinclude value1="CIE" value2="${AddEditACHBatch.StandardEntryClassCode}" operator="equals">
										<span class="required">*</span>
										</ffi:cinclude>
			                            <ffi:cinclude value1="WEB0" value2="${AddEditACHBatch.StandardEntryClassCodeDebitBatch}" operator="equals">
			                            <span class="required">*</span>
			                            </ffi:cinclude>
			                            </span>
									</td>
									<td class="sectionsubhead">
										<% if (isManaged || (isTemplate && !classCode.equalsIgnoreCase("XCK"))) { %>
											<s:text name="ach.grid.prenote" />
										<% } %>
										<% if (isManaged) { %>
										<% } else { %>
										<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
										<% if (!(isTemplate && !classCode.equalsIgnoreCase("XCK"))) { %>
											<s:text name="jsp.ach.Managed.Prenote" />
										<% } %>
										</ffi:cinclude>
										<% } %>
									</td>
								</tr>
								<tr>
									<td class="columndata">
									<%-- XCK is not identNumRequired, so no need to check for XCK --%>
											<% if (isManaged) { %>
									    <input type="Hidden" id="USERACCOUNTNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" name="achPayee.userAccountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="UserAccountNumber"/>">
										<ffi:getProperty name="ACHEntryPayee" property="UserAccountNumber"/>
											<% } else { %>
									<% int userAcctLen = 15; %>
											<ffi:cinclude value1="CIE" value2="${AddEditACHBatch.StandardEntryClassCode}" operator="equals">
									<%-- CIE only allows 22 length --%>
									<% userAcctLen = 22; %>
											</ffi:cinclude>
									<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="USERACCOUNTNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="<%= userAcctLen %>" name="achPayee.userAccountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="UserAccountNumber"/>">
									<% } %>
									</td>
									<td>
										<% if (isManaged || (isTemplate && !classCode.equalsIgnoreCase("XCK"))) { %>
											<input <%=(isManaged) ? "disabled" : ""%> id="radio_PRENOTE_NOT_REQUIRED" type="radio" name="achPayee.prenoteStatus" value="PRENOTE_NOT_REQUIRED" border="0" <ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_NOT_REQUIRED" operator="equals" >checked</ffi:cinclude>><label for="PRENOTE_NOT_REQUIRED" style="margin: 0 10px 0 0;"><!--L10NStart-->Not Required<!--L10NEnd--></label>
											<input <%=(isManaged) ? "disabled" : ""%> id="radio_PRENOTE_REQUIRED" type="radio" name="achPayee.prenoteStatus" value="PRENOTE_REQUESTED" border="0" <ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_PENDING" operator="equals" >checked</ffi:cinclude><ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_REQUESTED" operator="equals" >checked</ffi:cinclude>><label for="PRENOTE_REQUESTED"><!--L10NStart-->Requested<!--L10NEnd--></label>
										<% } %>
										<% if (isManaged) { %>
										<% } else { %>
										<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
										<% if (!(isTemplate && !classCode.equalsIgnoreCase("XCK"))) { %>
											<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="radio_PRENOTE_NOT_REQUIRED" type="radio" name="achPayee.prenoteStatus" value="PRENOTE_NOT_REQUIRED" id="PRENOTE_NOT_REQUIRED" border="0" <ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_NOT_REQUIRED" operator="equals">checked</ffi:cinclude>><label for="PRENOTE_NOT_REQUIRED" style="margin: 0 10px 0 0;"><!--L10NStart-->Not Required<!--L10NEnd--></label>
											<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="radio_PRENOTE_REQUIRED" type="radio" name="achPayee.prenoteStatus" value="PRENOTE_REQUESTED" id="PRENOTE_PENDING" border="0" <ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_PENDING" operator="equals" >checked</ffi:cinclude><ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_PENDING" operator="equals" >checked</ffi:cinclude>><label for="achPayee.prenoteStatus" style="margin: 0 10px 0 0;"><!--L10NStart-->Requested<!--L10NEnd-->
										<% } %>
										</ffi:cinclude>
										<% } %>
										<%if(showSameDayPrenote){%>

														<div class="SameDayPrenote">


																<%boolean sameDayPrenoteChecked=false;%>
																<ffi:cinclude value1="${ACHEntryPayee.PrenoteType}" value2="SameDay Prenote" operator="equals" >
																<%sameDayPrenoteChecked=true;%>
																</ffi:cinclude>

																<%-- Show only when prenote status is still prenote pending --%>
																<ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_SUCCEEDED" operator="equals" >
																	<%sameDayPrenoteChecked=false;%>
																</ffi:cinclude>

				                                                <input type="checkbox" name="achPayee.prenoteType" value="SameDay Prenote" border="0"  <%if(sameDayPrenoteChecked){%>checked<%}%> <%if(disableSameDayPrenote){%> disabled <%}%>><!--L10NStart-->Same Day Prenote<!--L10NEnd-->
														</div>

													<%if(disableSameDayPrenote){%>
													<div class="SameDayPrenote">
													<span  align="left" style="color:red">
													<b><!--L10NStart-->Same Day Prenote Cut-off has passed for Today<!--L10NEnd--></b><br/></span>
													</div>
														<%}%>
										<% } %>
									</td>
								</tr>
								<tr>
                                     <td><span id="achPayee.userAccountNumberError"></span></td>
                                     <td></td>
                                </tr>
								<tr>
									<%-- Start: Dual approval processing --%>
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<td class="sectionsubhead">
										<s:text name="da.field.ach.payeeGroup" />
									</td>
									<td class="sectionsubhead">
											<s:text name="da.field.ach.securePayee" />
										<%-- End: Dual approval processing--%>
									</td>
									</ffi:cinclude>
								</tr>
								<tr>
								<s:if test="%{consumerUser}">
								<td>

											<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
											<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
											<ffi:process name="ACHResource" />
											<select id="PayeeGroupID" class="txtbox" name="achPayee.payeeGroup" value="<ffi:getProperty name="ACHEntryPayee" property="PayeeGroup"/>" <ffi:cinclude value1="${ACHEntryPayee.AddToList}" value2="true" operator="notEquals">disabled</ffi:cinclude>>
												<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup1"/>
												<option value="1" <ffi:cinclude value1="1" value2="${ACHEntryPayee.PayeeGroupValue}">selected</ffi:cinclude>>
													<ffi:getProperty name="ACHResource" property="Resource"/></option>
												<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup2"/>
												<option value="2" <ffi:cinclude value1="2" value2="${ACHEntryPayee.PayeeGroupValue}">selected</ffi:cinclude>>
													<ffi:getProperty name="ACHResource" property="Resource"/></option>

											</select>
									</td>
									<td>


											<input disabled type="radio" name="achPayee.securePayee" value="false" border="0" checked ><label><!--L10NStart-->Normal<!--L10NEnd--></label>
											<%-- End: Dual approval processing--%>


								</td>
								</s:if>
								<s:else>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<td>
										<%-- Start: Dual approval processing --%>
											<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
											<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
											<ffi:process name="ACHResource" />
											<select id="PayeeGroupID" class="txtbox" name="achPayee.payeeGroup" value="<ffi:getProperty name="ACHEntryPayee" property="PayeeGroup"/>" <ffi:cinclude value1="${ACHEntryPayee.AddToList}" value2="true" operator="notEquals">disabled</ffi:cinclude>>
												<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup1"/>
												<option value="1" <ffi:cinclude value1="1" value2="${ACHEntryPayee.PayeeGroupValue}">selected</ffi:cinclude>>
													<ffi:getProperty name="ACHResource" property="Resource"/></option>
												<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup2"/>
												<option value="2" <ffi:cinclude value1="2" value2="${ACHEntryPayee.PayeeGroupValue}">selected</ffi:cinclude>>
													<ffi:getProperty name="ACHResource" property="Resource"/></option>
												<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup3"/>
												<option value="3" <ffi:cinclude value1="3" value2="${ACHEntryPayee.PayeeGroupValue}">selected</ffi:cinclude>>
													<ffi:getProperty name="ACHResource" property="Resource"/></option>
											</select>
									</td>
									<td>

											<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> type="radio" name="achPayee.securePayee" value="true" border="0" <ffi:cinclude value1="${ACHEntryPayee.SecurePayee}" value2="true" operator="equals" >checked</ffi:cinclude> <ffi:cinclude value1="${ACHEntryPayee.AddToList}" value2="true" operator="notEquals">disabled</ffi:cinclude>><label style="margin: 0 10px 0 0"><!--L10NStart-->Secure<!--L10NEnd--></label>
											<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> type="radio" name="achPayee.securePayee" value="false" border="0" <ffi:cinclude value1="${ACHEntryPayee.SecurePayee}" value2="false" operator="equals" >checked</ffi:cinclude> <ffi:cinclude value1="${ACHEntryPayee.AddToList}" value2="true" operator="notEquals">disabled</ffi:cinclude>><label><!--L10NStart-->Normal<!--L10NEnd--></label>
											<%-- End: Dual approval processing--%>


									</td>
								</ffi:cinclude>
								</s:else>

								</tr>
								<%  if (isManaged) { %>
									<tr class="ManagedPrenote">
										<td class="sectionsubhead">
											<s:text name="jsp.ach.Last.Prenote.Status" />
										</td>
										<td class="sectionsubhead">
											<s:text name="jsp.default_508" />
										</td>
									</tr>
								<% } %>
								<%  if (isManaged) { %>
									<tr class="ManagedPrenote">
										<td class="columndata">
											<ffi:cinclude value1="PRENOTE_PENDING" value2="${ACHEntryPayee.PrenoteStatus}"><s:text name="jsp.ach.Requested" /></ffi:cinclude>
											<ffi:cinclude value1="PRENOTE_REQUESTED" value2="${ACHEntryPayee.PrenoteStatus}"><s:text name="jsp.ach.Requested" /></ffi:cinclude>
											<ffi:cinclude value1="PRENOTE_NOT_REQUIRED" value2="${ACHEntryPayee.PrenoteStatus}"><s:text name="jsp.ach.Not.Required"/></ffi:cinclude>
											<ffi:cinclude value1="PRENOTE_SUCCEEDED" value2="${ACHEntryPayee.PrenoteStatus}"><s:text name="jsp.ach.Prenote.Succeeded" /></ffi:cinclude>
											<ffi:cinclude value1="PRENOTE_FAILED" value2="${ACHEntryPayee.PrenoteStatus}"><s:text name="jsp.ach.Prenote.Failed"/></ffi:cinclude>
										</td>
										<td class="columndata">
											<ffi:setProperty name="ACHEntryPayee" property="DateFormat" value="${UserLocale.DateFormat}" />
											<ffi:getProperty name="ACHEntryPayee" property="PrenoteSubmitDate" />
										</td>
									</tr>
								<% } %>
										<% if (isManaged || (isTemplate && !classCode.equalsIgnoreCase("XCK"))) { %>
										<%-- <tr class="ManagedPrenote">
											<td class="columndata lightBackground" height="20" nowrap>
												<div align="right"><span class="sectionsubhead"><!--L10NStart-->Prenote<!--L10NEnd--></span></div>
											</td>
											<td class="columndata" colspan="2">
												<input <%=(isManaged) ? "disabled" : ""%> type="radio" name="achPayee.prenoteStatus" value="PRENOTE_NOT_REQUIRED" border="0" <ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_NOT_REQUIRED" operator="equals" >checked</ffi:cinclude>><!--L10NStart-->Not Required<!--L10NEnd-->
												<input <%=(isManaged) ? "disabled" : ""%> type="radio" name="achPayee.prenoteStatus" value="PRENOTE_REQUESTED" border="0" <ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_PENDING" operator="equals" >checked</ffi:cinclude><ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_REQUESTED" operator="equals" >checked</ffi:cinclude>><!--L10NStart-->Requested<!--L10NEnd-->
											</td>
										</tr> --%>
										<% } %>
										<tr>
											<td class="columndata lightBackground" colspan="3">&nbsp;</td>
										</tr>
<%
	    if (isManaged) {
%>
						<%-- <tr class="ManagedPrenote">
							<td class="columndata lightBackground" height="20" nowrap>
								<div align="right"><span class="sectionsubhead"><!--L10NStart-->Last Prenote Status<!--L10NEnd--></span></div>
							</td>
							<td class="columndata" colspan="2">
								<ffi:cinclude value1="PRENOTE_PENDING" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Requested<!--L10NEnd--></ffi:cinclude>
								<ffi:cinclude value1="PRENOTE_REQUESTED" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Requested<!--L10NEnd--></ffi:cinclude>
								<ffi:cinclude value1="PRENOTE_NOT_REQUIRED" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Not Required<!--L10NEnd--></ffi:cinclude>
								<ffi:cinclude value1="PRENOTE_SUCCEEDED" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Prenote Succeeded<!--L10NEnd--></ffi:cinclude>
								<ffi:cinclude value1="PRENOTE_FAILED" value2="${ACHEntryPayee.PrenoteStatus}"><!--L10NStart-->Prenote Failed<!--L10NEnd--></ffi:cinclude>
							</td>
						</tr>
						<tr class="ManagedPrenote">
							<td class="columndata lightBackground" height="20" nowrap>
								<div align="right"><span class="sectionsubhead"><!--L10NStart-->Submission Date<!--L10NEnd--></span></div>
							</td>
							<td class="columndata" colspan="2">
								<ffi:setProperty name="ACHEntryPayee" property="DateFormat" value="${UserLocale.DateFormat}" />
								<ffi:getProperty name="ACHEntryPayee" property="PrenoteSubmitDate" />
							</td>
						</tr> --%>
<% } else { %>

	<%-- Start: Dual approval processing --%>
	<%-- <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<% if (!(isTemplate && !classCode.equalsIgnoreCase("XCK"))) { %>
		<tr class="ManagedPrenote">
			<td class="columndata lightBackground" nowrap>
				<div align="right"><span class="sectionsubhead"><!--L10NStart-->Managed Prenote<!--L10NEnd--></span></div>
			</td>
			<td class="columndata" colspan="2" align="left">
				<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> type="radio" name="achPayee.prenoteStatus" value="PRENOTE_NOT_REQUIRED" border="0" <ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_NOT_REQUIRED" operator="equals" >checked</ffi:cinclude>><!--L10NStart-->Not Required<!--L10NEnd-->
				<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> type="radio" name="achPayee.prenoteStatus" value="PRENOTE_REQUESTED" border="0" <ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_PENDING" operator="equals" >checked</ffi:cinclude><ffi:cinclude value1="${ACHEntryPayee.PrenoteStatus}" value2="PRENOTE_REQUESTED" operator="equals" >checked</ffi:cinclude>><!--L10NStart-->Requested<!--L10NEnd-->
			</td>
		</tr>
	<% } %>
	</ffi:cinclude> --%>
	<%-- End: Dual approval processing--%>

<% } %>

	<%-- Start: Dual approval processing --%>
	<%-- <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">

		<tr>
			<td class="columndata lightBackground" nowrap>
				<div align="right"><span class="sectionsubhead"><!--L10NStart-->Participant Scope<!--L10NEnd--></span></div>
			</td>
			<td class="columndata" colspan="2">
				<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
				<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
				<ffi:process name="ACHResource" />
				<select id="PayeeGroupID" class="txtbox" name="achPayee.payeeGroup" value="<ffi:getProperty name="ACHEntryPayee" property="PayeeGroup"/>" <ffi:cinclude value1="${ACHEntryPayee.AddToList}" value2="true" operator="notEquals">disabled</ffi:cinclude>>
					<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup1"/>
					<option value="1" <ffi:cinclude value1="1" value2="${ACHEntryPayee.PayeeGroupValue}">selected</ffi:cinclude>>
						<ffi:getProperty name="ACHResource" property="Resource"/></option>
					<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup2"/>
					<option value="2" <ffi:cinclude value1="2" value2="${ACHEntryPayee.PayeeGroupValue}">selected</ffi:cinclude>>
						<ffi:getProperty name="ACHResource" property="Resource"/></option>
					<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPayeeGroup3"/>
					<option value="3" <ffi:cinclude value1="3" value2="${ACHEntryPayee.PayeeGroupValue}">selected</ffi:cinclude>>
						<ffi:getProperty name="ACHResource" property="Resource"/></option>
				</select>
			</td>
		</tr>
		<tr>
			<td class="columndata lightBackground" nowrap>
				<div align="right"><span class="sectionsubhead"><!--L10NStart-->Secure Participant<!--L10NEnd--></span></div>
			</td>
			<td class="columndata" colspan="2" align="left">
				<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> type="radio" name="achPayee.securePayee" value="true" border="0" <ffi:cinclude value1="${ACHEntryPayee.SecurePayee}" value2="true" operator="equals" >checked</ffi:cinclude> <ffi:cinclude value1="${ACHEntryPayee.AddToList}" value2="true" operator="notEquals">disabled</ffi:cinclude>><!--L10NStart-->Secure<!--L10NEnd-->
				<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> type="radio" name="achPayee.securePayee" value="false" border="0" <ffi:cinclude value1="${ACHEntryPayee.SecurePayee}" value2="false" operator="equals" >checked</ffi:cinclude> <ffi:cinclude value1="${ACHEntryPayee.AddToList}" value2="true" operator="notEquals">disabled</ffi:cinclude>><!--L10NStart-->Normal<!--L10NEnd-->
			</td>
		</tr>
	</ffi:cinclude> --%>
	<%-- End: Dual approval processing--%>

									</table>
						</td>
				</tr>

		<tr>
		<td class="payeeDetails hidden" colspan="3" valign="center" style="padding: 0">
			<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData">
				<!-- <tr class="IAT">
		            <td class="sectionhead nameTitle" colspan="2">&nbsp;L10NStartIAT PayeeL10NEnd</td>
		        </tr> -->
				<tr class="IAT">
					<td width="50%" nowrap class="sectionsubhead">
                       <s:text name="da.field.ach.destinationCountry" /><span class="required"></span>
                   </td>
                   <td width="50%" class="sectionsubhead"><s:text name="da.field.ach.bankCountryCode" /> <span class="required">*</span></td>
				</tr>
				<tr class="IAT">
					 <td class="columndata"><ffi:getProperty name="ACHEntryPayee" property="DestinationCountryName"/></td>
                     <td class="columndata">
                         <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                         <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                         <ffi:process name="ACHResource" />
                         <select id="BranchCountry" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="txtbox" name="achPayee.bankCountryCode" onchange="updateAccountInfo(this);updateTruncatedText('BranchCountry')">
                         <option value=""><!--L10NStart-->(select Country)<!--L10NEnd--></option>
                         <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
                             <% String countryName =""; %>
                             <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
                             <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName" />
                             <%
                                 if (countryName == null) countryName = "";
                             %>
                             <option title="<%=countryName%>" value="<ffi:getProperty name="countryCode"/>"
                                 <ffi:cinclude value1="${countryCode}" value2="${ACHEntryPayee.BankCountryCode}">
                                     selected
                                 </ffi:cinclude>>
                                 <%
                                     if (countryName.length() > 25)
                                     {
                                         countryName = countryName.substring(0, 25);
                                     }
                                 %>
                                 <%=countryName%>
                             </option>
                             </ffi:list>
                         </select>
                         <div id="BranchCountryTruncatedText"></div>
                      </td>
				</tr>
				<tr class="IAT">
                    <td></td>
                    <td><span id="achPayee.bankCountryCodeError"></span></td>
                </tr>
                <tr class="IAT">
                    <td nowrap><s:text name="da.field.ach.currencyCode" /></td>
                    <td nowrap><s:text name="da.field.ach.foreignExchangeMethod" /></td>
                </tr>
                <tr class="IAT">
                    <td class="columndata">
                        <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${ACHEntryPayee.CurrencyCode}"/>
                        <ffi:getProperty name="ACHEntryPayee" property="CurrencyCode" />-<ffi:getProperty name="ACHResource" property="Resource"/>
                    </td>
                    <td class="columndata">
                        <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${ACHEntryPayee.ForeignExchangeMethod}"/>
                        <ffi:getProperty name="ACHEntryPayee" property="ForeignExchangeMethod" />-<ffi:getProperty name="ACHResource" property="Resource"/>
                    </td>
                </tr>
                <tr class="IAT"><td valign="top" colspan="2"><hr class="hidden thingrayline lineBreak" style="display: block;"></td></tr>
				<tr class="IAT">
                    <td class="sectionhead nameTitle" colspan="2">&nbsp;<s:text name="jsp.ach.Participant.Address" /></td>
                </tr>
                <tr class="IAT">
                	<td class="sectionsubhead">
                        <s:text name="da.field.ach.street" /><span class="required">*</span>
                    </td>
                    <td class="sectionsubhead"><s:text name="jsp.default_101" /><span class="required">*</span></td>
                </tr>
                <tr class="IAT">
                	<td class="columndata">
                        <input id="STREET" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="35" name="achPayee.street" value="<ffi:getProperty name="ACHEntryPayee" property="Street"/>">
                    </td>
                    <td class="columndata">
                        <input id="CITY" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="14" name="achPayee.city" value="<ffi:getProperty name="ACHEntryPayee" property="City"/>">
                    </td>
                </tr>
                <tr class="IAT">
                	<td><span id="achPayee.streetError"></span></td>
                	<td><span id="achPayee.cityError"></span></td>
                </tr>
                <tr class="IAT">
                	<td nowrap class="sectionsubhead"><s:text name="jsp.default_115" /><span class="required">*</span></td>
                    <td nowrap class="sectionsubhead"><s:text name="da.field.ach.zipCode" /><span class="required">*</span></td>
                </tr>
                <tr class="IAT">
                	<td class="columndata">
                         <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                         <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                         <ffi:process name="ACHResource" />
                         <select id="Country" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="txtbox" name="achPayee.country"  onchange="updateTruncatedText('Country');">
                         <option value=""><!--L10NStart-->(select Country)<!--L10NEnd--></option>
                         <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
                             <% String countryName2 =""; %>
                             <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
                             <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName2" />
                             <%
                                 if (countryName2 == null) countryName2 = "";
                             %>
                             <option title="<%=countryName2%>" value="<ffi:getProperty name="countryCode"/>"
                                 <ffi:cinclude value1="${countryCode}" value2="${ACHEntryPayee.Country}">
                                     selected
                                 </ffi:cinclude>>
                                 <%
                                     if (countryName2.length() > 25)
                                     {
                                         countryName2 = countryName2.substring(0, 25);
                                     }
                                 %>
                                 <%=countryName2%>
                             </option>
                             </ffi:list>
                         </select>
						  <div id="CountryTruncatedText"></div>
                     </td>
                     <td class="columndata">
                         <input id="ZIPCODE" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="9" name="achPayee.zipCode" value="<ffi:getProperty name="ACHEntryPayee" property="ZipCode"/>">
                     </td>
                </tr>
                <tr class="IAT">
                	 <td><span id="achPayee.countryError"></span></td>
                	<td><span id="achPayee.zipCodeError"></span></td>
                </tr>
                <tr class="IAT">
                  <td colspan="2" class="sectionsubhead"><s:text name="jsp.default_387" /> </td>
               </tr>
               <tr class="IAT">
                  <td class="columndata">
                      <input id="STATE" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="19" name="achPayee.state" value="<ffi:getProperty name="ACHEntryPayee" property="State"/>">
                  </td>
              </tr>
              <tr class="IAT">
                  <td></td>
                  <td><span id="achPayee.stateError"></span></td>
              </tr>
                <tr><td valign="top" colspan="2"><hr class="thingrayline" style="display: block;"></td></tr>
                <tr>
                    <td class="sectionhead nameTitle" colspan="2"><s:text name="jsp.ach.Bank.Info" /></td>
                </tr>
                <tr>
					<td class="sectionsubhead" width="50%">
						<s:text name="da.field.ach.bankName" />
					</td>
					<td class="sectionsubhead" width="50%">
						<s:text name="da.field.ach.routingNumber" /><span class="required">*</span>
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap>
						<% if (isManaged) { %>
						<% if (isSecure) { %>
										<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
						<% } else { %>
										<ffi:getProperty name="ACHEntryPayee" property="BankName"/>
						<% } %>
						<% } else { %>
							<input id="BANKNAME" <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" style="width:150px;" maxlength="35" name="achPayee.bankName" value="<ffi:getProperty name="ACHEntryPayee" property="BankName"/>">
						<% } %>
                        <% if (!isManaged && !(fromTemplate || deleteEntry)) { %>
                            <sj:a button="true" title="Search" onClickTopics="achEntry_selectDestBankTopics"><s:text name="jsp.default_373" /></sj:a>
                        <% } else { %>
						<% } %>
					</td>
					<td class="columndata" nowrap>
						<% if (isManaged) { %>
							<% if (isSecure) { %>
							<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
						<% } else { %>
							<ffi:getProperty name="ACHEntryPayee" property="RoutingNumber"/>
						<% } %>
						<% } else { %>
							<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="ROUTINGNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" style="width:300px;" maxlength="34" name="achPayee.routingNumber" value="<ffi:getProperty name="ACHEntryPayee" property="RoutingNumber"/>">

						<% } %>
					</td>
				</tr>
                <tr>
                     <td><span id="achPayee.bankNameError"></span></td>
                     <td><span id="achPayee.routingNumberError"></span></td>
                </tr>
				<tr>
                     <td class="sectionsubhead"><s:text name="da.field.ach.bankIdentifierType" /> <span class="required">*</span></td>
                     <td class="sectionsubhead"><s:text name="jsp.default_16" /><span class="required">*</span></td>
               	</tr>
               	<tr>
               		<td class="columndata" style="text-align:left">
                       <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                       <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                       <ffi:process name="ACHResource" />
                       <select id="BankIdentifierType" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="txtbox" name="achPayee.bankIdentifierType" value="<ffi:getProperty name="ACHEntryPayee" property="BankIdentifierType"/>">
                           <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier01"/>
                           <option value="01" <ffi:cinclude value1="01" value2="${ACHEntryPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
						<%if ("IAT".equals(classCode)) { %>
                           <ffi:setProperty name="ACHResource" property="ResourceID" value="BIC_IBANCountries"/>
                           <% String iban = ""; String other = ""; String destCntry = ""; %>
                           <ffi:getProperty name="ACHResource" property="Resource" assignTo="iban" />
                           <ffi:setProperty name="ACHResource" property="ResourceID" value="OTHER_RTNCountries"/>
                           <ffi:getProperty name="ACHResource" property="Resource" assignTo="other" />
                           <ffi:getProperty name="ACHEntryPayee" property="DestinationCountry" assignTo="destCntry"/>
                           <% if (iban.indexOf(destCntry) != -1) { %>
                               <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier02"/>
                               <option value="02" <ffi:cinclude value1="02" value2="${ACHEntryPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                               <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier03"/>
                               <option value="03" <ffi:cinclude value1="03" value2="${ACHEntryPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                           <% } else if (other.indexOf(destCntry) != -1){ %>
                               <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier99"/>
                               <option value="99" <ffi:cinclude value1="99" value2="${ACHEntryPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                           <% } %>
						<% } %>
                       </select>
                   </td>
                   <td class="columndata" nowrap>
						<% if (isManaged) { %>
	                                       <input type="Hidden" <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="ACCOUNTNUMBER" class="ui-widget-content ui-corner-all txtbox" style="width:300px;" type="text" size="32" maxlength="17" name="achPayee.accountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="AccountNumber"/>">
						<% if (isSecure) { %>
							<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
						<% } else { %>
							<ffi:getProperty name="ACHEntryPayee" property="AccountNumber"/>
						<% } %>
						<% } else { %>
							<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="ACCOUNTNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" style="width:300px;" maxlength="17" name="achPayee.accountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="AccountNumber"/>">

						<% } %>
					</td>
               	</tr>
				<tr>
					<td></td>
                    <td><span id="achPayee.accountNumberError"></span></td>
				</tr>
				<tr>
					<td colspan="2" class="sectionsubhead">
						<s:text name="jsp.default_20" />
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap  colspan="2">
							<% if (isManaged) { %>
								<input type="Hidden" name="achPayee.accountType" value="<ffi:getProperty name="ACHEntryPayee" property="AccountType"/>">
							<% if (isSecure) { %>
											<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
							<% } else { %>
											<ffi:getProperty name="ACHEntryPayee" property="AccountType" />
							<% } %>
							<% } else { %>
									<select <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="AccountType" class="txtbox" name="achPayee.accountType">
										<% int acctType = 0; %>
										<ffi:list collection="ACHAccountTypes" items="ACHAccountType">
										<%
														acctType ++;
														boolean displayType = true;
							// Loan is only valid if Credit type of transaction
														if (acctType == ACHAccountTypes.LOAN && creditDisabled)
															displayType = false;
	                                                    // QTS 637869: Ledger is only for CCD and CTX
														if (acctType == ACHAccountTypes.LEDGER && !((classCode.equalsIgnoreCase("CCD")
																|| classCode.equalsIgnoreCase("CTX"))))
															displayType = false;
														if (displayType) {
													%>
							<option value="<%=acctType%>"
								<ffi:cinclude value1="${ACHAccountType}" value2="${ACHEntryPayee.AccountType}">	selected </ffi:cinclude>>
								<ffi:getProperty name="ACHAccountType"/></option>
						<%	} %>
						</ffi:list>
					</select>

				<% } %>
					</td>
				</tr>
				<tr>
                     <td></td>
                     <td><span id="achPayee.accountTypeError"></span></td>
				</tr>
                                       <%-- <tr class="IAT">
                                           <td nowrap>
                                               <div align="right"><span class="sectionsubhead"><!--L10NStart-->Destination Country<!--L10NEnd--> </span><span class="required">&nbsp;</span></div>
                                           </td>
                                           <td class="columndata">
                                               <ffi:getProperty name="ACHEntryPayee" property="DestinationCountryName"/>
                                           </td>
                                       </tr>
                                       <tr class="IAT">
                                           <td>
                                               <div align="right"><span class="sectionsubhead"><!--L10NStart-->Branch Country<!--L10NEnd--> </span><span class="required">*</span></div>
                                           </td>
                                           <td class="columndata">
                                               <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                               <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                               <ffi:process name="ACHResource" />
                                               <select id="BranchCountry" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="txtbox" name="achPayee.bankCountryCode" onchange="updateAccountInfo(this);">
                                               <option value=""><!--L10NStart-->(select Country)<!--L10NEnd--></option>
                                               <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
                                                   <% String countryName =""; %>
                                                   <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
                                                   <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName" />
                                                   <%
                                                       if (countryName == null) countryName = "";
                                                   %>
                                                   <option title="<%=countryName%>" value="<ffi:getProperty name="countryCode"/>"
                                                       <ffi:cinclude value1="${countryCode}" value2="${ACHEntryPayee.BankCountryCode}">
                                                           selected
                                                       </ffi:cinclude>>
                                                       <%
                                                           if (countryName.length() > 25)
                                                           {
                                                               countryName = countryName.substring(0, 25);
                                                           }
                                                       %>
                                                       <%=countryName%>
                                                   </option>
                                                   </ffi:list>
                                               </select>
                                               <div id="BranchTruncatedText"></div>
                                           </td>
                                       </tr>
                                       <tr class="IAT">
                                           <td class="columndata" align="right" valign="baseline"></td>
                                           <td><span id="achPayee.bankCountryCodeError"></span></td>
                                       </tr> --%>
                                       <%-- <tr class="IAT">
                                           <td nowrap>
                                               <div align="right"><span class="sectionsubhead"><!--L10NStart-->Currency Code<!--L10NEnd--> </span><span class="required">&nbsp;</span></div>
                                           </td>
                                           <td class="columndata" style="text-align:left">
                                               <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${ACHEntryPayee.CurrencyCode}"/>
                                               <ffi:getProperty name="ACHEntryPayee" property="CurrencyCode" />-<ffi:getProperty name="ACHResource" property="Resource"/>
                                           </td>
                                       </tr>
                                       <tr class="IAT">
                                           <td nowrap>
                                               <div align="right"><span class="sectionsubhead"><!--L10NStart-->Foreign Exchange Method<!--L10NEnd--> </span><span class="required">&nbsp;</span></div>
                                           </td>
                                           <td class="columndata" style="text-align:left">
                                               <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${ACHEntryPayee.ForeignExchangeMethod}"/>
                                               <ffi:getProperty name="ACHEntryPayee" property="ForeignExchangeMethod" />-<ffi:getProperty name="ACHResource" property="Resource"/>
                                           </td>
                                       </tr> --%>
									<%-- <tr>
										<td class="columndata" height="20" nowrap>
											<div align="right"><span class="sectionsubhead"><!--L10NStart-->Bank Name<!--L10NEnd--></span></div>
										</td>
										<td class="columndata" nowrap>
									<% if (isManaged) { %>
<% if (isSecure) { %>
										<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
<% } else { %>
										<ffi:getProperty name="ACHEntryPayee" property="BankName"/>
<% } %>
									<% } else { %>
										<input id="BANKNAME" <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" style="width:150px;" maxlength="35" name="achPayee.bankName" value="<ffi:getProperty name="ACHEntryPayee" property="BankName"/>">
									<% } %>
										</td>
										<td class="columndata" nowrap>
											<div align="left">
                                                   <% if (!isManaged && !(fromTemplate || deleteEntry)) { %>
                                                       <sj:a button="true" title="Search" onClickTopics="achEntry_selectDestBankTopics">Search</sj:a>
                                                   <% } else { %>
												&nbsp;
												<% } %>
                                                   <br>
											</div>
										</td>
									</tr>
                                       <tr>
                                           <td class="columndata" align="right" valign="baseline"></td>
                                           <td><span id="achPayee.bankNameError"></span></td>
                                       </tr>
									<tr>
										<td height="20" nowrap>
											<div align="right"><span class="sectionsubhead"><!--L10NStart-->Bank Identifier<!--L10NEnd--> </span><span class="required">*</span></div>
										</td>
										<td class="columndata" nowrap>
									<% if (isManaged) { %>
<% if (isSecure) { %>
										<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
<% } else { %>
										<ffi:getProperty name="ACHEntryPayee" property="RoutingNumber"/>
<% } %>
									<% } else { %>
										<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="ROUTINGNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" style="width:150px;" maxlength="34" name="achPayee.routingNumber" value="<ffi:getProperty name="ACHEntryPayee" property="RoutingNumber"/>">

									<% } %>
										</td>
									</tr>
									<tr>
                                           <td class="columndata" align="right" valign="baseline"></td>
                                           <td><span id="achPayee.routingNumberError"></span></td>
									</tr> --%>

                                       <%-- <tr class="IAT">
                                           <td>
                                               <div align="right"><span class="sectionsubhead"><!--L10NStart-->Bank Identifier Type<!--L10NEnd--> </span><span class="required">*</span></div>
                                           </td>
                                           <td class="columndata" style="text-align:left">
                                               <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                               <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                               <ffi:process name="ACHResource" />
                                               <select id="BankIdentifierType" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="txtbox" name="achPayee.bankIdentifierType" value="<ffi:getProperty name="ACHEntryPayee" property="BankIdentifierType"/>">
                                                   <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier01"/>
                                                   <option value="01" <ffi:cinclude value1="01" value2="${ACHEntryPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>

<%if ("IAT".equals(classCode)) { %>
                                                   <ffi:setProperty name="ACHResource" property="ResourceID" value="BIC_IBANCountries"/>
                                                   <% String iban = ""; String other = ""; String destCntry = ""; %>
                                                   <ffi:getProperty name="ACHResource" property="Resource" assignTo="iban" />
                                                   <ffi:setProperty name="ACHResource" property="ResourceID" value="OTHER_RTNCountries"/>
                                                   <ffi:getProperty name="ACHResource" property="Resource" assignTo="other" />
                                                   <ffi:getProperty name="ACHEntryPayee" property="DestinationCountry" assignTo="destCntry"/>
                                                   <% if (iban.indexOf(destCntry) != -1) { %>
                                                       <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier02"/>
                                                       <option value="02" <ffi:cinclude value1="02" value2="${ACHEntryPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                                                       <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier03"/>
                                                       <option value="03" <ffi:cinclude value1="03" value2="${ACHEntryPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                                                   <% } else if (other.indexOf(destCntry) != -1){ %>
                                                       <ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier99"/>
                                                       <option value="99" <ffi:cinclude value1="99" value2="${ACHEntryPayee.BankIdentifierType}">selected</ffi:cinclude>><ffi:getProperty name="ACHResource" property="Resource"/></option>
                                                   <% } %>
<% } %>
                                               </select>
                                           </td>
                                           <td class="columndata">&nbsp;</td>
                                       </tr>
									<tr>
										<td height="20" nowrap>
											<div align="right"><span class="sectionsubhead"><!--L10NStart-->Account #<!--L10NEnd--> </span><span class="required">*</span></div>
										</td>
										<td class="columndata" nowrap>
									<% if (isManaged) { %>
                                           <input type="Hidden" <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="ACCOUNTNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="17" name="achPayee.accountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="AccountNumber"/>">
<% if (isSecure) { %>
										<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
<% } else { %>
										<ffi:getProperty name="ACHEntryPayee" property="AccountNumber"/>
<% } %>
									<% } else { %>
										<input <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="ACCOUNTNUMBER" class="ui-widget-content ui-corner-all txtbox" type="text" size="32" style="width:150px;" maxlength="17" name="achPayee.accountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="AccountNumber"/>">

									<% } %>
										</td>
									</tr>
									<tr>
                                           <td class="columndata" align="right" valign="baseline"></td>
                                           <td><span id="achPayee.accountNumberError"></span></td>
									</tr>
									<tr>
										<td class="columndata" height="20" nowrap>
											<div align="right"><span class="sectionsubhead"><!--L10NStart-->Account Type<!--L10NEnd--></span></div>
										</td>
										<td class="columndata" nowrap align="left">
								<% if (isManaged) { %>
									<input type="Hidden" name="achPayee.accountType" value="<ffi:getProperty name="ACHEntryPayee" property="AccountType"/>">
<% if (isSecure) { %>
										<ffi:getProperty name="ACHEntryPayee" property="SecurePayeeMask"/>
<% } else { %>
										<ffi:getProperty name="ACHEntryPayee" property="AccountType" />
<% } %>
									<% } else { %>
										<select <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="AccountType" class="txtbox" name="achPayee.accountType">
											<% int acctType = 0; %>
											<ffi:list collection="ACHAccountTypes" items="ACHAccountType">
											<%
													acctType ++;
													boolean displayType = true;
// Loan is only valid if Credit type of transaction
													if (acctType == ACHAccountTypes.LOAN && creditDisabled)
														displayType = false;
                                                    // QTS 637869: Ledger is only for CCD and CTX
													if (acctType == ACHAccountTypes.LEDGER && !((classCode.equalsIgnoreCase("CCD")
															|| classCode.equalsIgnoreCase("CTX"))))
														displayType = false;
													if (displayType) {
												%>
												<option value="<%=acctType%>"
													<ffi:cinclude value1="${ACHAccountType}" value2="${ACHEntryPayee.AccountType}">	selected </ffi:cinclude>>
													<ffi:getProperty name="ACHAccountType"/></option>
											<%	} %>
											</ffi:list>
										</select>

									<% } %>

										</td>
									</tr>
									<tr>
                                           <td class="columndata" align="right" valign="baseline"></td>
                                           <td><span id="achPayee.accountTypeError"></span></td>
									</tr> --%>
								</table>
								</td>
							</tr>
                            <%-- <tr valign="bottom" class="IAT">
                                <td class="tbrd_b columndata" colspan="5" height="20" align="left"><span class="sectionhead">&gt;&nbsp;<!--L10NStart-->Participant Address<!--L10NEnd--></span></td>
                            </tr> --%>
                           </table>
                           <% } %>
</div>
</div>
</div>
<div class="paneWrapper marginTop20">
   	<div class="paneInnerWrapper">
		<div class="header"><s:text name="jsp.ach.Entry.Details"/></div>
<!-- ACHBatch Entry header and Grid details -->
<div class="paneContentWrapper">
						<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData ">

							<%-- <tr class="lightBackground">
									<td class="tbrd_b sectionhead" colspan="2"><span class="">&gt; <!--L10NStart-->Participant<!--L10NEnd-->
									<% if (fromTemplate) { %>
										&nbsp;<!--L10NStart-->(from template, cannot change)<!--L10NEnd-->
									<% } %>
									<br></span></td>
submit the form to submit modified data
									<td class="tbrd_b columndata" colspan="3" align="right">
                                        <input id="managedPayeesId" class='formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%=(fromTemplate || deleteEntry) ?"ui-state-disabled":"" %>' <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> type="button" value="Managed Payees" onclick="ns.ach.managedPayees();" >
                                    </td>
							</tr>
<% if (deleteEntry == true) { %>
							<tr>
								<td class="sectionhead lightBackground" colspan="5" align="center"><span class="sectionhead"><!--L10NStart-->Are you sure you want to delete this ACH Entry?<!--L10NEnd-->
									<br>
									<br>
									</span>
								</td>
							</tr>
<% } else { %> --%>
<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.NONE_DUAL_APPROVAL %>">
    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_EDIT_VIEW_SECURE_PAYEE %>">
       <ffi:cinclude value1="${ACHEntryPayee.SecurePayee}" value2="true">
           <ffi:cinclude value1="${ACHEntryPayee.ID}" value2="" operator="notEquals">
       						<%-- <script>
       							ns.ach.editSecurePayeeUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/editACHPayeeAction_init.action?IsManagedParticipants=true&ReloadPayee=true&PayeeID=${ACHEntryPayee.ID}"/>';
       						</script>
							<tr class="lightBackground" valign="bottom">
                                <td colspan="5" align="right">
                                     <input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%=(fromTemplate) ?"ui-state-disabled":"" %>" <%=(fromTemplate) ? "disabled" : ""%> type="button" value="Edit Secure Payee" onclick="ns.ach.editSecurePayee(ns.ach.editSecurePayeeUrl);" >
                                </td>
                            </tr> --%>
        </ffi:cinclude>
    </ffi:cinclude>
    </ffi:cinclude>
</ffi:cinclude>


							<tr>
								<td>
									<%-- <select class="txtbox" id="participantID" name="participantID" size="1" onChange="loadPayee(this)">
										<option>
											<ffi:getProperty name="ACHEntryPayee" property="Name"/>
										</option>
									</select> --%>

						<%-- <span class="sapUiIconCls icon-drill-down" id="expand" onclick="openDetail();" title="Show Details"></span>
						<span class="sapUiIconCls icon-drill-up hidden" id="collapse" onclick="openDetail();" title="Show Details"></span> --%>

									<!-- <input type="button" value="Add New" onclick="addNewEntry();">
									<input type="hidden" value="false" id="isNew"> -->

								</td>
							</tr>

                            <%-- <tr class="IAT">
                                <td class="columndata" colspan="2" style="padding-left:20px;">
                                    <div align="left">
                                    <table border="0" cellspacing="0" cellpadding="3" width="49%">
                                        <tr>
                                            <td nowrap>
                                                <div align="right"><span class="sectionsubhead"><!--L10NStart-->Street Address<!--L10NEnd--></span><span class="required">*</span></div>
                                            </td>
                                            <td class="columndata">
                                                <input id="STREET" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="35" name="achPayee.street" value="<ffi:getProperty name="ACHEntryPayee" property="Street"/>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="columndata" align="right" valign="baseline"></td>
                                            <td><span id="achPayee.streetError"></span></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>
                                                <div align="right"><span class="sectionsubhead"><!--L10NStart-->City<!--L10NEnd--></span><span class="required">*</span></div>
                                            </td>
                                            <td class="columndata">
                                                <input id="CITY" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="14" name="achPayee.city" value="<ffi:getProperty name="ACHEntryPayee" property="City"/>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="columndata" align="right" valign="baseline"></td>
                                            <td><span id="achPayee.cityError"></span></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>
                                                <div align="right"><span class="sectionsubhead"><!--L10NStart-->State/Province<!--L10NEnd--> </span></div>
                                            </td>
                                            <td class="columndata">
                                                <input id="STATE" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="19" name="achPayee.state" value="<ffi:getProperty name="ACHEntryPayee" property="State"/>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="columndata" align="right" valign="baseline"></td>
                                            <td><span id="achPayee.stateError"></span></td>
                                        </tr>
                                    </table>
                                    </div>
                                </td>
                                <td class="tbrd_l" colspan="3" style="padding-left:40px;">
                                    <div align="left">
                                    <table border="0" cellspacing="0" cellpadding="3" width="49%">
                                        <tr>
                                            <td nowrap>
                                                <div align="right"><span class="sectionsubhead"><!--L10NStart-->Country<!--L10NEnd--> </span><span class="required">*</span></div>
                                            </td>
                                            <td class="columndata">
                                                <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                                <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                                <ffi:process name="ACHResource" />
                                                <select id="COUNTRY" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="txtbox" name="achPayee.country" >
                                                <option value=""><!--L10NStart-->(select Country)<!--L10NEnd--></option>
                                                <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
                                                    <% String countryName2 =""; %>
                                                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
                                                    <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName2" />
                                                    <%
                                                        if (countryName2 == null) countryName2 = "";
                                                    %>
                                                    <option title="<%=countryName2%>" value="<ffi:getProperty name="countryCode"/>"
                                                        <ffi:cinclude value1="${countryCode}" value2="${ACHEntryPayee.Country}">
                                                            selected
                                                        </ffi:cinclude>>
                                                        <%
                                                            if (countryName2.length() > 25)
                                                            {
                                                                countryName2 = countryName2.substring(0, 25);
                                                            }
                                                        %>
                                                        <%=countryName2%>
                                                    </option>
                                                    </ffi:list>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="columndata" align="right" valign="baseline"></td>
                                            <td><span id="achPayee.countryError"></span></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>
                                                <div align="right"><span class="sectionsubhead"><!--L10NStart-->Postal Code<!--L10NEnd--> </span><span class="required">*</span></div>
                                            </td>
                                            <td class="columndata">
                                                <input id="ZIPCODE" <%=(isManaged || fromTemplate || deleteEntry) ? "disabled" : ""%> class="ui-widget-content ui-corner-all txtbox" type="text" size="32" maxlength="9" name="achPayee.zipCode" value="<ffi:getProperty name="ACHEntryPayee" property="ZipCode"/>">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="columndata" align="right" valign="baseline"></td>
                                            <td><span id="achPayee.zipCodeError"></span></td>
                                        </tr>
                                    </table>
                                    </div>
                                </td>
                            </tr> --%>
							<!-- <tr valign="bottom">
								<td class="sectionhead nameTitle" colspan="2">L10NStartEntry DetailL10NEnd</td>
							</tr> -->
							<tr>
								<td class="sectionsubhead" width="50%">

									<% if (!isManaged && !classCode.equalsIgnoreCase("IAT")) { %>
										<div style="display:inline-block">
											<%-- Hidden field for ModifyACHEntry.Prenote so uncheck will pass through to task --%>
											<input id="hiddenPrenote" <%=deleteEntry ? "disabled" : ""%> type="hidden" name="prenote" value="<ffi:getProperty name="ModifyACHEntry" property="Prenote" />" >
											<input id="prenote" <%=deleteEntry ? "disabled" : ""%> type="checkbox" onclick="prenoteChanged(this.checked)" name="Prenote" value="true" <ffi:cinclude value1="${ModifyACHEntry.Prenote}" value2="true">checked</ffi:cinclude>>
											<label class="sectionsubhead" for="prenote"><!--L10NStart-->Prenote Transaction<!--L10NEnd--></label>
										</div>
									<% } %>

									</br></br>
									<span><s:text name="jsp.default_43" /> &nbsp; <span class="required">*</span> </span>
							</td>
							<% if (showDiscretionaryData) { %>
								<td class="sectionsubhead" width="50%" valign="bottom"><s:text name="ach.Discretionary.Data" /></td>
							<% } %>
							<% if ("IAT".equals(classCode)) { %>
	                                   <td nowrap>
	                                      <span class="sectionsubhead"><s:text name="jsp.default_439" /></span><span class="required">*</span>
	                                   </td>
						     <% } %>
							</tr>
							<tr>
							<td class="columndata" nowrap>
                             <input <%=deleteEntry ? "disabled" : ""%> type="text" id="AMOUNT" class="ui-widget-content ui-corner-all txtbox" maxlength="11" name="amount" value="<ffi:getProperty name="ModifyACHEntry" property="AmountValue.CurrencyStringNoSymbol"/>" size="16" border="0">
								<div style="display:inline-block">
								(<s:text name="jsp.ach.usd" /> /
										<span id="DebitDisabled">
											<span class="sectionsubhead" id="DebitRadio">
											<input id="DebitRadioCheck" type="radio" name="amountIsDebit" value="true"
												<% if (debitChecked) { %>
													checked
												<% }
													if (debitDisabled) { %>
												    disabled
												<% } %>
		 											>
											</span>
											<label for="DebitRadioCheck" class="columndata"><s:text name="jsp.default_146" /></label>
										</span>
											<span id="CreditDisabled">
											<span class="sectionsubhead" id="CreditRadio">
											<input id="CreditRadioCheck" type="radio" name="amountIsDebit" value="false"
											<% if (creditChecked) { %>
												checked
											<% }
											if (creditDisabled) { %>
											    disabled
											<% } %>
											>
											</span>
											<label for="CreditRadioCheck" class="columndata"><s:text name="jsp.default_120" /></label>
										</span>
	                                          	 </div>)
                            </td>
                            <% if (showDiscretionaryData) { %>
								<td class="columndata"><input <%=deleteEntry ? "disabled" : ""%> type="text" id="DISCRETIONARYDATA" class="ui-widget-content ui-corner-all txtbox" maxlength="2" name="discretionaryData" value="<ffi:getProperty name="ModifyACHEntry" property="DiscretionaryData"/>" size="4" border="0" ></td>
							<% } %>
							<% if ("IAT".equals(classCode)) { %>
                                  <td class="columndata">
                                      <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                      <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                      <ffi:process name="ACHResource" />
										<select id="TransactionTypeCode" <%=deleteEntry ? "disabled" : ""%> class="txtbox" name="transactionTypeCode" onchange="showHideTransactions();">
														<ffi:cinclude value1="" value2="${ModifyACHEntry.TransactionTypeCode}">
							                                            <option value='' ><s:text name="jsp.ach.select.Transaction.Type" /></option>
							                                        </ffi:cinclude>
							                                        <% if (creditChecked) { %>
											<ffi:list collection="ACHIAT_TransactionTypesCredit" items="TransactionType">
													<option value="<ffi:getProperty name="TransactionType"/>"
													<ffi:cinclude value1="${TransactionType}" value2="${ModifyACHEntry.TransactionTypeCode}">
														selected
													</ffi:cinclude>>
														<ffi:setProperty name="ACHResource" property="ResourceID" value="TransactionType${TransactionType}"/>
														<ffi:getProperty name="ACHResource" property="Resource"/>
													</option>
											</ffi:list>
							                                        <% } else { %>
											<ffi:list collection="ACHIAT_TransactionTypesDebit" items="TransactionType">
													<option value="<ffi:getProperty name="TransactionType"/>"
													<ffi:cinclude value1="${TransactionType}" value2="${ModifyACHEntry.TransactionTypeCode}">
														selected
													</ffi:cinclude>>
														<ffi:setProperty name="ACHResource" property="ResourceID" value="TransactionType${TransactionType}"/>
														<ffi:getProperty name="ACHResource" property="Resource"/>
													</option>
											</ffi:list>
                                          <% } %>
                                      </select>
                                  </td>
							     <% } %>
							</tr>
							<tr>
								<td><span id="amountError"></span></td>
								<% if (showDiscretionaryData) { %>
									<td><span id="discretionaryDataError"></span></td>
								<% } %>
								<% if ("IAT".equals(classCode)) { %>
                                     <td><span id="transactionTypeCodeError"></span></td>
							     <% } %>
							</tr>
							<tr>
								<td class="lightBackground" colspan="2" style="padding:0">
									<table width="100%"  border="0" align="left">
									<% boolean healthAddenda = false;
									if ("CCD".equals(classCode)) { %>
									<ffi:cinclude ifEntitled="CCD + Healthcare" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCOMPANY.CompanyID}" checkParent="true">
									    <ffi:cinclude value1="HCCLAIMPMT" value2="${AddEditACHBatch.CoEntryDesc}">
									<% healthAddenda = true; %>
									        <tr>
									            <td width="50%"><span class="sectionsubhead"><s:text name="jsp.ach.Reference.ID" /></span><span class="required">*</span></td>
									            <td  width="50%"><span class="sectionsubhead"><s:text name="jsp.ach.Originating.Company.Identifier" /> </span><span class="required">*</span></td>
									        </tr>

									        <tr>
									            <td class="columndata"><input <%=deleteEntry ? "disabled" : ""%> type="text" id="REFERENCEID" class="ui-widget-content ui-corner-all txtbox" name="addenda.referenceID" value="<ffi:getProperty name="ModifyACHEntry" property="Addenda.ReferenceID"/>" size="20" maxlength="50" border="0">
									            </td>
									            <td class="columndata"><input <%=deleteEntry ? "disabled" : ""%> type="text" id="ORIGCOMPANYID" class="ui-widget-content ui-corner-all txtbox" name="addenda.originCompanyID" value="<ffi:getProperty name="ModifyACHEntry" property="Addenda.OriginCompanyID"/>" size="20" maxlength="10" border="0">
									            </td>
									        </tr>
									        <tr>
									            <td><span id="addenda.referenceIDError"></span></td>
									            <td><span id="addenda.originCompanyIDError"></span></td>
									        </tr>
									        <tr>
									            <td colspan="2"><span class="sectionsubhead"><s:text name="jsp.ach.Optional.Reference.ID" /> </span></td>
									        </tr>
									        <tr>
									        	<td class="columndata" colspan="2"><input <%=deleteEntry ? "disabled" : ""%> type="text" id="REFERENCEID2" class="ui-widget-content ui-corner-all txtbox" name="addenda.referenceID2" value="<ffi:getProperty name="ModifyACHEntry" property="Addenda.ReferenceID2"/>" size="20" maxlength="50" border="0">
									            </td>
									        </tr>
									        <tr>
									            <td colspan="2"><span id="addenda.referenceID2Error"></span></td>
									        </tr>
									    </ffi:cinclude>
									</ffi:cinclude>
									<% }
									if ("POP".equals(classCode)) { %>
										<tr>
											<td width="50%"><span class="sectionsubhead"><s:text name="jsp.ach.Terminal.City" /> </span><span class="required">*</span></td>
											<td width="50%"><span class="sectionsubhead"><s:text name="jsp.ach.Terminal.State" /> </span><span class="required">*</span></td>
										</tr>
										<tr>

											<td class="columndata"><input <%=deleteEntry ? "disabled" : ""%> type="text" id="TERMINALCITY" class="ui-widget-content ui-corner-all txtbox" name="terminalCity" value="<ffi:getProperty name="ModifyACHEntry" property="TerminalCity"/>" size="16" maxlength="4" border="0">
											</td>
											<td class="columndata">
												<select <%=deleteEntry ? "disabled" : ""%> id="TERMINALSTATE" class="ui-widget-content  txtbox" name="terminalState" value="<ffi:getProperty name="ModifyACHEntry" property="TerminalState"/>">
													<ffi:cinclude value1="${ModifyACHEntry.TerminalState}" value2="" operator="equals">
													<option selected value=""><!--L10NStart-->Please select a state<!--L10NEnd--></option>
													</ffi:cinclude>
	 												<ffi:list collection="StateList" items="Value">
														<ffi:setProperty name="StateResource" property="ResourceID" value="State${Value}" />
														<option <ffi:cinclude value1="${ModifyACHEntry.TerminalState}" value2="${Value}">selected </ffi:cinclude>value="<ffi:getProperty name="Value"/>"><ffi:getProperty name='StateResource' property='Resource'/></option>
													</ffi:list>
												</select>
											</td>
										</tr>
										<tr>
	                                        <td><span id="terminalCityError"></span></td>
	                                        <td><span id="terminalStateError"></span></td>
	                                    </tr>
									<% }
									if ("XCK".equals(classCode)) { %>
										<tr>
											<td width="50%"><span class="sectionsubhead"><s:text name="jsp.ach.Process.Control.Field" /> </span><span class="required">*</span></td>
											<td width="50%"><span class="sectionsubhead"><s:text name="jsp.ach.Item.Research.Number" /> </span><span class="required">*</span></td>
										</tr>
										<tr>

											<td class="columndata"><input <%=deleteEntry ? "disabled" : ""%> type="text" id="PROCESSCONTROLFIELD" class="ui-widget-content ui-corner-all txtbox" name="processControlField" value="<ffi:getProperty name="ModifyACHEntry" property="ProcessControlField"/>" size="16" maxlength="6" border="0">
											</td>
											<td class="columndata"><input <%=deleteEntry ? "disabled" : ""%> type="text" id="ITEMRESEARCHNUMBER" class="ui-widget-content ui-corner-all txtbox" name="itemResearchNumber" value="<ffi:getProperty name="ModifyACHEntry" property="ItemResearchNumber"/>" size="16" maxlength="16" border="0">
											</td>
										</tr>
	                                    <tr>
	                                        <td><span id="processControlFieldError"></span></td>
	                                        <td><span id="itemResearchNumberError"></span></td>
	                                    </tr>
									<% }
									if ("IAT".equals(classCode)) { %>

										<tr id="IAT_TERMINALCITYSTATE">
	                                        <td colspan="2">
	                                            <table width="100%">
	                                                <tr>
	                                                    <td width="50%"><span class="sectionsubhead"><s:text name="jsp.ach.Terminal.City" /> </span><span class="required">*</span></td>
	                                                    <td width="50%"><span class="sectionsubhead"><s:text name="jsp.ach.Terminal.State" />(<!--L10NStart-->Foreign Country<!--L10NEnd-->) </span><span class="required">*</span></td>
	                                                </tr>
	                                               	<tr>
	                                                    <td class="columndata"><input <%=deleteEntry ? "disabled" : ""%> type="text" id="TERMINALCITY_IAT" class="ui-widget-content ui-corner-all txtbox" name="terminalCity" value="<ffi:getProperty name="ModifyACHEntry" property="TerminalCity"/>" size="16 maxlength="4" border="0">
	                                                    </td>
	                                                    <td class="columndata"><input <%=deleteEntry ? "disabled" : ""%> type="text" id="TERMINALSTATE_IAT" class="ui-widget-content ui-corner-all txtbox" name="terminalState" value="<ffi:getProperty name="ModifyACHEntry" property="TerminalState"/>" size="16" maxlength="2" border="0">
	                                                    </td>
	                                                </tr>
	                                            </table>
	                                        </td>
										</tr>
	                                    <tr>
	                                        <td><span id="terminalCityError"></span></td>
	                                        <td><span id="terminalStateError"></span></td>
	                                    </tr>

	                                    <tr id="IAT_CHECKSERIALNUMBER">
											<td colspan="2">
												<table>
													<tr>
														<td nowrap colspan="2"><span class="sectionsubhead">
															<s:text name="jsp.ach.Check.Serial.Number" /> </span><span class="required">*</span>
														</td>
													</tr>
													<tr id="IAT_CHECKSERIALNUMBER" colspan="2">
														<td class="columndata">
															<input <%=deleteEntry ? "disabled" : ""%> type="text" id="CHECKSERIALNUMBER" class="ui-widget-content ui-corner-all txtbox" name="checkSerialNumber" value="<ffi:getProperty name="ModifyACHEntry" property="CheckSerialNumber"/>" size="16" maxlength="15" border="0">

														</td>
													</tr>
													<tr id="IAT_CHECKSERIALNUMBER">
														<td colspan="2"><span id="checkSerialNumberError"></span></td>
													</tr>
												</table>
											</td>
											</tr>
									<% } %>
											<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Entry Balanced" operator="equals">
				<% if (classCode.startsWith("IAT"))
				 {
				// QTS 577543: If IAT, we don't support Batch Balanced or Entry Balanced batches
				%>
													<tr class="lightBackground">
				                                        <td class="sectionsubhead" colspan='2'>
				                                        <s:text name="jsp.ach.IAT.Batches.msg" />
				                                        </td>
													</tr>
				 <% } else { %>
												<tr>
													<td colspan="2"><span class="sectionsubhead"><!--L10NStart-->Offset Account<!--L10NEnd--> </span><span class="required">*</span></td>
												</tr>
												<tr>
													<td colspan="2" class="columndata">
														<select <%=(fromTemplate || deleteEntry) ? "disabled" : ""%> id="OFFSETACCOUNT" class="ui-widget-content ui-corner-all txtbox" name="offsetAccountID">
															<ffi:cinclude value1="${ACHOffsetAccounts.Size}" value2="1" operator="notEquals" >
															<option value=""><s:text name="jsp.default_375"/></option>
															</ffi:cinclude>
															<ffi:setProperty name="ACHOffsetAccounts" property="Filter" value="All" />
															<ffi:list collection="ACHOffsetAccounts" items="OffsetAccount">

				            <ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetAccountEntitlementObjectID" scope="request"/>
				                <ffi:setProperty name="GetAccountEntitlementObjectID" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>
				                <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>'/>
				                <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${OffsetAccount.Account.ID}"/>
				                <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>'/>
				                <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${OffsetAccount.RoutingNum}"/>
				                <ffi:process name="GetAccountEntitlementObjectID"/>

				            <% boolean isEntitled = false; %>
				            <ffi:cinclude ifEntitled="<%=  com.ffusion.csil.core.common.EntitlementsDefines.ACH_BATCH %>" objectType="<%=  com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="${GetAccountEntitlementObjectID.ObjectID}">
				                <% isEntitled = true; %>
				            </ffi:cinclude>
				                        <% if (isEntitled) { %>



															<% String selected=""; %>
															<ffi:cinclude value1="${OffsetAccount.RoutingNum}-${OffsetAccount.Number}-${OffsetAccount.TypeValue}" value2="${ModifyACHEntry.OffsetAccountBankID}-${ModifyACHEntry.OffsetAccountNumber}-${ModifyACHEntry.OffsetAccountTypeValue}">
																<% selected = "selected"; %>
															</ffi:cinclude>
															<ffi:cinclude value1="${OffsetAccount.ID}" value2="${ModifyACHEntry.OffsetAccountID}">
																<% selected = "selected"; %>
															</ffi:cinclude>
				                                                        <option value="<ffi:getProperty name="OffsetAccount" property="ID"/>" <%=selected%>><ffi:getProperty name="OffsetAccount" property="Number"/>-<ffi:getProperty name="OffsetAccount" property="Type"/></option>
				                        <% } %>

															</ffi:list>
														</select>
													</td>
												</tr>
				                                <tr>
				                                    <td colspan="2" ><span id="offsetAccountIDError"></span></td>
				                                </tr>
				<% } %>
											</ffi:cinclude>
							<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Entry Balanced" operator="notEquals">
							</ffi:cinclude>

							<% if ("ARC".equals(classCode) || "BOC".equals(classCode) || "POP".equals(classCode) || "RCK".equals(classCode) || "XCK".equals(classCode)) {
									int csn_length = 15;
									if ("POP".equals(classCode)) csn_length = 9;%>
									<tr>
										<td colspan="2"><span class="sectionsubhead">
											<s:text name="jsp.ach.Check.Serial.Number" /> </span>
										<% if (!isTemplate) { %><span class="required">*</span><%} %>
										</td>
									</tr>
									<tr>
										<td class="columndata" colspan="2">
											<input <%=deleteEntry ? "disabled" : ""%> type="text" id="CHECKSERIALNUMBER" class="ui-widget-content ui-corner-all txtbox" name="checkSerialNumber" value="<ffi:getProperty name="ModifyACHEntry" property="CheckSerialNumber"/>" size="16" maxlength="<%=csn_length%>" border="0">

										</td>
									</tr>
									<tr>
										<td colspan="2"><span id="checkSerialNumberError"></span></td>
									</tr>
									<% } %>

							</table></td>
							</tr>

<% boolean canAddenda = false; %>
							<%-- Most can have max 1 addenda, but CTX can have up to 9,999 addendas! --%>
								<%-- Need to check if the ACH Company is Addenda Entitled --%>
<% if (isTemplate) { %>
        <ffi:setProperty name="ACHCOMPANY" property="CurrentClassCodeTemplate" value="${AddEditACHBatch.StandardEntryClassCode}" />
<% } else { %>
        <ffi:setProperty name="ACHCOMPANY" property="CurrentClassCode" value="${AddEditACHBatch.StandardEntryClassCode}" />
<% } %>
								<ffi:cinclude value1="${ACHCOMPANY.ClassCodeAddendaEntitled}" value2="TRUE" operator="equals" >
<%
	if (classCode.equalsIgnoreCase("CCD") ||
		classCode.equalsIgnoreCase("CIE") ||
		classCode.equalsIgnoreCase("CTX") ||
		classCode.equalsIgnoreCase("IAT") ||
		classCode.equalsIgnoreCase("PPD") ||
		classCode.equalsIgnoreCase("WEB"))
		canAddenda = true;
%>
								</ffi:cinclude>

<% if (canAddenda) { %>
						<tr>
							<td colspan="2" class="sectionhead"><s:text name="jsp.default_477" /></td>
						</tr>
						<tr>
							<td colspan="5" class="columndata">
							<ffi:cinclude value1="CTX" value2="${AddEditACHBatch.StandardEntryClassCode}" operator="equals">
								<textarea <%=deleteEntry || healthAddenda ? "disabled" : ""%> id="ACHADDENDA" name="addendaString" rows="9" cols="80" wrap="physical" border="0"><ffi:getProperty name="ModifyACHEntry" property="AddendaString"/></textarea>
							</ffi:cinclude>
							<ffi:cinclude value1="CTX" value2="${AddEditACHBatch.StandardEntryClassCode}" operator="notEquals">
                            <% String addendaSize = "80";
                            if (classCode.equalsIgnoreCase("IAT"))
                                addendaSize = "160"; %>
								<input style="margin:0 10px 0 0; width:350px;" <%=deleteEntry || healthAddenda ? "disabled" : ""%> type="text" id="ACHADDENDA" class="ui-widget-content ui-corner-all txtbox" name="addendaString" value="<ffi:getProperty name="ModifyACHEntry" property="AddendaString"/>" size="80" maxlength="<%=addendaSize%>" border="0">
							</ffi:cinclude>
							<% if (!healthAddenda) { %>
								<%-- <input id="achEntryImportAddendaId" <%=deleteEntry ? "disabled" : ""%> class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= deleteEntry?"ui-state-disabled":"" %>' type="button" value="Import Addenda" onclick="ns.ach.ACHEntryImportAddenda();"> --%>

								<a id="achEntryImportAddendaId" href="#" title="Import Addenda"
<%if(!deleteEntry){%>onClick="ns.ach.ACHEntryImportAddenda();"<%}%> class="<%=deleteEntry?"ui-state-disabled":""%> "><span class="sapUiIconCls icon-upload"></span></a>


							<% } else if (!deleteEntry) { %>
								<a id="TriggerAchEntryViewAddendaId" href="#" title="View Addenda" onClick="ACHEntryViewAddenda();"><span class="sapUiIconCls icon-show"></span></a>
							                                <sj:a id="achEntryViewAddendaId"
							                                      button="true"
							                                      formIds="ACHEntryForm"
							                                      targets="tempDiv"
							                                      title="View Addenda"
							                                      validate="true"
							                                      validateFunction="customValidation"
							                                      onClickTopics="viewAddendaClick"
							                                      onErrorTopics="errorAddACHEntryForm"
							                                      onSuccessTopics="viewACHEntryAddendaSuccess"
							                                      cssStyle="display:none"
							                                      >View Addenda
							                                  </sj:a>
							<% } %>
						    <% if (healthAddenda && !deleteEntry) {%>
						                                (<s:text name="jsp.ach.fields.mgs" />)
						    <% } else if (deleteEntry) { %>
													<!-- <img src="/cb/web/multilang/grafx/payments/i_trash_dim.gif" alt="Cannot Delete" height="14" width="14" border="0" hspace="3" > -->
													<span class='sapUiIconCls icon-delete ui-state-disabled'></span>
						    <% } else { %>
													<a href="#" title="Delete Addenda" onclick="clearAddenda();"><!-- <img src="/cb/web/multilang/grafx/payments/i_trash.gif" alt="Delete Addenda" height="14" width="14" border="0" hspace="3" onclick="clearAddenda();"> -->
														<span class='sapUiIconCls icon-delete'></span>
													</a>
						    <% } %>
                            <span id="ImportAddendaResult" style="font-size: 12px"></span>
							<span id="addendaError"></span>
							</td>
						</tr>
<% } %>
						</table>
</div>
</div>
</div>
			<div align="center" class="marginTop20">
                    <span class="required">* <s:text name="jsp.default_240" /></span>
            </div>
            <div class="btn-row">
                <input id="isDeleteId" type="hidden" name="isDelete" value="true">
                <input id="viewAddendaId" name="viewAddenda" type="hidden" value="false">
					<input class="submitbutton" type="hidden" value="Cancel" onclick="document.location='<ffi:urlEncrypt url="${SecurePath}payments/achbatchaddedit.jsp?DeleteACHEntry=${ModifyACHEntry.ID}"/>';return false;">
										<sj:a  button="true"
						  onClickTopics="cancelACHEntry"
                      					title="Cancel"><s:text name="jsp.default_82" /></sj:a>
<% if (addEntry == true){ %>
										&nbsp;
<%-- todo: in order to change to use TASK Validation, change type to submit and remove the ONCLICK portion --%>
										<input class="submitbutton" type="hidden" value="ADD ENTRY" border="0" onclick="document.EditACHPayeeForm.submit();">
										<%if(isManaged)
										{%>
										<sj:a id="addACHEntryLink" button="true"
						                      formIds="ACHEntryForm"
						                      targets="resultmessage"
						                      title="Add"
                                              validate="true"
                                              validateFunction="managedPayeeValidation"
						                      onBeforeTopics="beforeAddACHEntryForm"
                                              onClickTopics="saveACHEntryClick"
						                      onErrorTopics="errorAddACHEntryForm"
						                      onCompleteTopics="completeAddACHEntryForm"
                                                ><s:text name="jsp.default_29" />
                                            </sj:a>
										<%}
										else
										{%>
										<sj:a id="addACHEntryLink" button="true"
						                      formIds="ACHEntryForm"
						                      targets="resultmessage"
						                      title="Add"
                                              validate="true"
                                              validateFunction="customValidation"
						                      onBeforeTopics="beforeAddACHEntryForm"
                                              onClickTopics="saveACHEntryClick"
						                      onErrorTopics="errorAddACHEntryForm"
						                      onCompleteTopics="completeAddACHEntryForm"
                                                ><s:text name="jsp.default_29" />
                                            </sj:a>
										<%}%>

<% } else if (deleteEntry == true) { %>
										&nbsp;
                                        <ffi:setProperty name="tempURL" value="/cb/pages/jsp/ach/deleteACHEntryAction.action?DeleteEntryID=${ModifyACHEntry.ID}" URLEncrypt="true"/>

										<script>
                                            ns.ach.deleteAchEntryByIDUrl = '<ffi:getProperty name="tempURL"/>';
										</script>
										<input id="deleteACHEntryInputId" class="submitbutton" type="hidden" value='<ffi:getProperty name="ModifyACHEntry" property="ID"/>' border="0" onclick="document.location='<ffi:urlEncrypt url="${SecurePath}payments/achbatchaddedit.jsp?DeleteACHEntry=${ModifyACHEntry.ID}"/>';return false;">
										<sj:a id="deleteACHEntryLink"
                                              button="true"
									          title="Delete"
									          onClickTopics="deleteACHEntryClick"
									          ><s:text name="jsp.default_162" />
									    </sj:a>
<% } else { %>
										&nbsp;
<%-- todo: in order to change to use TASK Validation, change type to submit and remove the ONCLICK portion --%>
										<input class="submitbutton" type="hidden" value="SAVE ENTRY" border="0" onclick="document.EditACHPayeeForm.submit();">
										<sj:a
											id="editACHEntryLink"
											formIds="ACHEntryForm"
											targets="resultmessage"
								            button="true"
								            validate="true"
											validateFunction="customValidation"
                                            onClickTopics="saveACHEntryClick"
											onBeforeTopics="beforeAddACHEntryForm"
											onSuccessTopics="completeAddACHEntryForm"
								            ><s:text name="jsp.default_366" />
								        </sj:a>
<% } %>


			    </div>

        <%--onClick="onSubmitCheck(false)"--%>

</div>
</s:form>
</div>
<div style="display:none">
	<div id="tempDiv"></div>
</div>
</div>
<script type="text/javascript">

// need to hide/show transactions before changing to selectmenu
// because it changes the ID of the object
    showHideTransactions();
    showHide();
    addToListChanged();
    showCreditDebit();
	ns.common.addHoverEffect("managedPayeesId");
	//ns.common.addHoverEffect("achEntryImportAddendaId");
</script>

