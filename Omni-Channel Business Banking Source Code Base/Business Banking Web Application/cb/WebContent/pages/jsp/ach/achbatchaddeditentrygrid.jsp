<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%	String showErrors = "false";
    boolean isShowErrors = false;
    String curPage = "1";
    String curRowNum = "10";
    // QTS 702629: Remember the paging size as well as the current page upon refresh
    if (session.getAttribute("StdGridRowNum") != null)
        curRowNum = "" + session.getAttribute("StdGridRowNum");
    if (request.getParameter("ShowErrors") != null && "true".equals(request.getParameter("ShowErrors"))) {
        showErrors = "true";
        isShowErrors = true;
    }
    if (request.getParameter("curPage") != null) {
        curPage = "" + request.getParameter("curPage");
    }
    if (request.getParameter("curRowNum") != null) {
        curRowNum = "" + request.getParameter("curRowNum");
    }
    request.setAttribute("curRowNum", curRowNum);
    request.setAttribute("curPage", curPage);
	request.setAttribute("ShowErrors", showErrors);
    %>

	<%-- If DA, get the DA status for the payee and stick it in the map --%>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:list collection="ACHEntries" items="ACHEntry">
			<ffi:cinclude value1="${ACHEntry.AchPayee.ID}" value2="" operator="notEquals">
				<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" scope="request"/>
				<ffi:setProperty name="GetDAItem" property="itemId" value="${ACHEntry.AchPayee.ID}"/>
				<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>"/>
				<ffi:process name="GetDAItem"/>
				<ffi:setProperty name="ACHEntry" property="AchPayee.DAStatus" value="${DAItem.Status}"/>
			</ffi:cinclude>
		</ffi:list>
	</ffi:cinclude>

	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getACHEntriesAction.action?ShowErrors=${ShowErrors}" URLEncrypt="true"/>
    <s:url id="achEntryUrl" value="%{#session.tempURL}" escapeAmp="false"/>
    <s:set id="curPage" value="%{#request.curPage}"/>
    <s:set id="curRowNum" value="%{#request.curRowNum}"/>

    <sjg:grid
        id="achEntriesGridId"
        caption=""
        sortable="true"
        dataType="json"
        href="%{achEntryUrl}"
        pager="true"
        gridModel="gridModel"
		rowList="%{#session.StdGridRowList}"
        rowNum="%{curRowNum}"
        page="%{curPage}"
        rownumbers="true"
        shrinkToFit="true"
        navigator="true"
        navigatorAdd="false"
        navigatorDelete="false"
        navigatorEdit="false"
        navigatorRefresh="false"
        onSelectAllTopics="triggerHiddenHoldAllIdButton"
        onCellSelectTopics="triggerRowSelection"
        navigatorSearch="false"
        multiselect="true"
        navigatorView="false"
        scroll="false"
        scrollrows="true"
        footerrow="true"
        userDataOnFooter="false"
        viewrecords="true"
        onPagingTopics="ACHEntryGridPagingTopicEvents"
        onGridCompleteTopics="ACHEntryGridCompleteEvents"
    >
        <sjg:gridColumn name="hold" index="hold" title="Hold" sortable="false" search="false" width="40" formatter="ns.ach.formatHoldLinks" cssClass="datagrid_textColumn" hidden=""/>
<s:if test="#session.AddEditACHBatch.StandardEntryClassCode.equals('XCK')">
        <sjg:gridColumn name="itemResearchNumber" index="itemResearchNumber" title="Item Research Number" sortable="true" width="160" cssClass="datagrid_numberColumn"/>
</s:if>

<s:if test="#session.AddEditACHBatch.StandardEntryClassCode.equals('CCD') || #session.AddEditACHBatch.StandardEntryClassCode.equals('CTX') || #session.AddEditACHBatch.StandardEntryClassCode.equals('CIE')|| #session.AddEditACHBatch.StandardEntryClassCode.equals('PPD') || #session.AddEditACHBatch.StandardEntryClassCode.equals('TEL')">
        <sjg:gridColumn name="achPayee.userAccountNumber" index="userAccountNumber" title="Participant ID" sortable="true" width="100" cssClass="datagrid_numberColumn"/>
</s:if>

<s:if test="!#session.AddEditACHBatch.StandardEntryClassCode.equals('XCK') && !#session.AddEditACHBatch.StandardEntryClassCode.equals('WEB')">
        <%-- <sjg:gridColumn name="achPayee.nickNameSecure" index="nickName" title="Nick Name" sortable="true" width="160" cssClass="datagrid_textColumn"/> --%>
        <sjg:gridColumn name="achPayee.nameSecure" index="nameSecure" title="Participant Name" sortable="true" width="160" cssClass="datagrid_textColumn"/>
</s:if>

<s:if test="#session.AddEditACHBatch.StandardEntryClassCodeDebitBatch.equals('WEB1')">
        <sjg:gridColumn name="achPayee.nickNameSecure" index="nickName" title="Nick Name" sortable="true" width="160" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="achPayee.nameSecure" index="nameSecure" title="Participant Name" sortable="true" width="160" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="achPayee.userAccountNumber" index="userAccountNumber" title="Participant ID" sortable="true" width="100" cssClass="datagrid_numberColumn"/>
</s:if>

<s:if test="#session.AddEditACHBatch.StandardEntryClassCodeDebitBatch.equals('WEB0')">
        <sjg:gridColumn name="achPayee.nickNameSecure" index="nickName" title="Nick Name" sortable="true" width="160" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="achPayee.nameSecure" index="nameSecure" title="Receiver Name" sortable="true" width="160" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="achPayee.userAccountNumber" index="userAccountNumber" title="Sender Name" sortable="true" width="100" cssClass="datagrid_numberColumn"/>
</s:if>
        <%-- <sjg:gridColumn name="achPayee.routingNumberSecure" index="routingNumber" title="ABA #" sortable="true" width="100" cssClass="datagrid_numberColumn"/> --%>
        <sjg:gridColumn name="achPayee.accountNumberSecure" index="accountNumber" title="Account #" sortable="true" width="100" cssClass="datagrid_numberColumn"/>
       <%--  <sjg:gridColumn name="achPayee.accountTypeSecure" index="accountType" title="Account Type" sortable="true" width="100" cssClass="datagrid_textColumn"/> --%>
<s:if test="#session.AddEditACHBatch.StandardEntryClassCode.equals('BOC') || #session.AddEditACHBatch.StandardEntryClassCode.equals('XCK') || #session.AddEditACHBatch.StandardEntryClassCode.equals('POP') || #session.AddEditACHBatch.StandardEntryClassCode.equals('RCK')">
        <sjg:gridColumn name="checkSerialNumber" index="checkSerialNumber" title="Check Number" sortable="true" width="100" cssClass="datagrid_numberColumn"/>
</s:if>
<s:if test="!#session.AddEditACHBatch.StandardEntryClassCode.equals('IAT') && !#session.AddEditACHBatch.StandardEntryClassCode.equals('WEB') && !#session.AddEditACHBatch.StandardEntryClassCode.equals('TEL')">
        <%-- <sjg:gridColumn name="discretionaryData" index="discretionaryData" title="DD" sortable="true" width="100" cssClass="datagrid_textColumn"/> --%>
</s:if>
        <sjg:gridColumn name="amount" index="amount" title="Amount" sortable="true" width="100" formatter="ns.ach.formatAmountLinks" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="amountIsDebit" index="amountIsDebit" title="Debit/Credit" sortable="true" width="105" formatter="ns.ach.formatDebitLinks" />
        <sjg:gridColumn name="active" index="active" title="Active" sortable="false" hidden="true" width="100" hidedlg="true" cssClass="datagrid_actionIcons"/>
        <sjg:gridColumn name="action" index="action" title="Action" sortable="false" search="false" formatter="ns.ach.formatACHEntryActionLinks" width="90" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
        <sjg:gridColumn name="ID" index="ID" title="ID" sortable="false" hidden="true" width="100" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="hasValidationErrors" index="hasValidationErrors" title="hasValidationErrors" sortable="false" hidden="true" hidedlg="true" width="10" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="payeeDAStatus" index="daStatus" title="DAStatus" sortable="false" hidden="true" hidedlg="true" />
        <sjg:gridColumn name="map.CanEdit" width="100" index="CanEdit" title="CanEdit" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
        <sjg:gridColumn name="map.CanDelete" width="100" index="CanDelete" title="CanDelete" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>

        <sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="Amount" hidden="true" hidedlg="true" sortable="true" width="100" cssClass="datagrid_textColumn"/>

    </sjg:grid>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals">
    <script type="text/javascript">
    <!--
        $("#achEntriesGridId").addClass("TaxChild");
    //-->
    </script>
</ffi:cinclude>

<% if (isShowErrors) { %>
<script type="text/javascript">
<!--
    $("#achEntriesGridId").addClass("ShowErrors");
    // if already showing errors, show "Showing Errors" message
    $("#showingErrorRows").show();
    $("#showErrorRows").hide();
//-->

</script>
<% } %>

<script type="text/javascript">
$(document).ready(function(){
		$('#achEntriesGridId').on('onGridCompleteTopics',function(){
			alert("afer");
		});
	});
</script>	
