<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@taglib uri="/struts-jquery-tags" prefix="sj"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<div align="center">
	<div align="center">
		<div align="left">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata lightBackground">
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="lightBackground"><img
									src="/cb/web/multilang/grafx/payments/spacer.gif" alt=""
									width="107" height="1" border="0"></td>
							</tr>
							<tr>
								<td class="tbrd_b lightBackground"><span
									class="sectionhead">&gt;&nbsp; <s:text name="ach.changeBatchType.header"/>  <br></span></td>
							</tr>
							<tr>
								<td class="sectionhead lightBackground">
									<div align="center">
									<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_TAX)}">
										<s:text name="ach.resetACHBatchType.fromTAX"/>
									</s:if>
									<s:elseif test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
										<s:text name="ach.resetACHBatchType.fromChild"/>
									</s:elseif>
									<s:elseif test="%{#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_TAX)}">
										<s:text name="ach.resetACHBatchType.fromTAX"/>
									</s:elseif>
									<s:elseif test="%{#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
										<s:text name="ach.resetACHBatchType.fromChild"/>
									</s:elseif>
									</div>
								</td>
							</tr>
							<tr>
								<td class="sectionhead lightBackground" align="center">
									<div align="center">
										<sj:a button="true" onClickTopics="closeDialog">
										                <s:text name="jsp.default_82"/>
										        	</sj:a>
										<sj:a id="changeBatchType"
											button="true" 
											onclick="setACHBatchType(); ns.common.closeDialog('setACHBatchTypeDialogID');return false;"
											effectDuration="1500">
												<s:text name="jsp.default_111"/>
										</sj:a>
									</div>
								</td>								
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
