
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.ach.ACHOffsetAccount,
		 com.ffusion.beans.ach.ACHClassCode" %>
		 

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>		 
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/struts-tags" prefix="s" %>
		
 <div align="center">
	<br/>
	<form action="/pages/jsp/ach/achbatchdelete.jsp" method="post" name="deleteACHInstanceForm" id="deleteACHInstanceForm">	
       	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
        <s:hidden name="ID" value="%{#parameters.ID}"/>
        <s:hidden name="RecID" value="%{#parameters.RecID}"/>
        
	<div align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tr class="lightBackground">
             		<td width="25%"/>
        	     <td class="sectionsubhead" align="right"><input type="radio" name="deleteRecurringModel" checked=checked value="false" border="0"></td>
        	      
       		     <td class="sectionsubhead">
              
              		<s:text name="jsp.ach.Delete.recurring.ACH.Batch.msg" />
              
              </td>
	</tr>

	<tr class="lightBackground">
 		<td width="25%"/>
        <td class="sectionsubhead" align="right"><input  type="radio" name="deleteRecurringModel"  value="true" border="0"></td>
        
        <td class="sectionsubhead" >
           
           <s:text name="jsp.ach.Delete.all.recurring.ACH.Batch.msg"/> 
           
        </td>
	<tr>

		<td height=2 colspan=3></td>
	</tr>
	<tr>

		<td colspan="15" align="center">
			<sj:a 
			button="true" 
			onClickTopics="closeDialog"
			><s:text name="jsp.default_82" />
			</sj:a>
					
			&nbsp;&nbsp;
			<sj:a 
				button="true" 
				onClick="ns.ach.continueDeletePendingRec();"
				><s:text name="jsp.default_111" />
			</sj:a>
		</td>			
	</tr>			
	</table>

	</div>
	</form>
 </div>