<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_ACHSingleTemplatesSummary" className="moduleHelpClass"/>
	<input id="achTypeId" type="hidden" value='<ffi:getProperty name="ACHType"/>'/>

	<!-- Decide Edit URL depending module type -->
	<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
		<ffi:setGridURL grid="GRID_singleACHTemplates" name="LoadURL" url="/cb/pages/jsp/ach/addChildSupportBatch_initLoadTemplate.action?ID={0}&RecID={1}" parm0="ID" parm1="RecID"/>
	</s:if>
	<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_TAX)}">
		<ffi:setGridURL grid="GRID_singleACHTemplates" name="LoadURL" url="/cb/pages/jsp/ach/addTaxBatch_initLoadTemplate.action?ID={0}&RecID={1}" parm0="ID" parm1="RecID"/>
	</s:if>
	<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_ACH)}">
		<ffi:setGridURL grid="GRID_singleACHTemplates" name="LoadURL" url="/cb/pages/jsp/ach/addACHBatch_initLoadTemplate.action?ID={0}&RecID={1}" parm0="ID" parm1="RecID"/>
	</s:if>


	<ffi:setGridURL grid="GRID_singleACHTemplates" name="ViewURL" url="/cb/pages/jsp/ach/viewAchBatchTemplate.action?ID={0}&RecID={1}" parm0="ID" parm1="RecID"/>

	<!-- Decide Edit URL depending module type -->
	<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
		<ffi:setGridURL grid="GRID_singleACHTemplates" name="EditURL" url="/cb/pages/jsp/ach/editChildSupportTemplate_init.action?ID={0}&RecID={1}" parm0="ID" parm1="RecID"/>
	</s:if>
	<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_TAX)}">
		<ffi:setGridURL grid="GRID_singleACHTemplates" name="EditURL" url="/cb/pages/jsp/ach/editTaxBatchTemplate_init.action?ID={0}&RecID={1}" parm0="ID" parm1="RecID"/>
	</s:if>
	<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_ACH)}">
		<ffi:setGridURL grid="GRID_singleACHTemplates" name="EditURL" url="/cb/pages/jsp/ach/editACHBatchTemplate_init.action?ID={0}&RecID={1}" parm0="ID" parm1="RecID"/>
	</s:if>

	<ffi:setGridURL grid="GRID_singleACHTemplates" name="DeleteURL" url="/cb/pages/jsp/ach/deleteACHTemplate_init.action?ID={0}&RecID={1}" parm0="ID" parm1="RecID"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getSingleACHTemplatesAction.action?singleACHBatchTemplateSearchCriteria.templateCategory=${ACHType}&GridURLs=GRID_singleACHTemplates" URLEncrypt="true"/>
	<s:url id="singleACHTemplatesUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
			id="singleACHTemplatesGridID"
			caption=""
			sortable="true"
			sortname="templateName"
			dataType="json"
			href="%{singleACHTemplatesUrl}"
			pager="true"
			gridModel="gridModel"
			rowList="%{#session.StdGridRowList}"
			rowNum="%{#session.StdGridRowNum}"
			rownumbers="false"
			shrinkToFit="true"
			navigator="true"
			navigatorAdd="false"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			scroll="false"
			scrollrows="true"
			viewrecords="true"
			onGridCompleteTopics="addGridControlsEvents,singleACHTemplatesGridCompleteEvents"
			>

        <sjg:gridColumn name="templateName" width="60" index="templateName" title="%{getText('ach.grid.templateName')}" sortable="true" search="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.companyID" width="100"  index="fundsTransaction.companyID" title="%{getText('ach.grid.companyIDOrName')}"  formatter="ns.ach.formatAchCompanyIDandName" sortable="true" search="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.coName" width="100"  index="fundsTransaction.coName" title="%{getText('ach.grid.companyName')}" sortable="true" search="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.status" width="90" index="status" title="%{getText('jsp.default_388')}" sortable="true" search="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.templateScope" width="90" index="templateScope" title="%{getText('ach.grid.scope')}" sortable="true" search="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.templateType" width="150" index="templateType" title="%{getText('ach.grid.templateType')}" formatter="ns.ach.formatSingleACHTemplatesType" sortable="true" search="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.ach.formatSingleACHTemplatesActionLinks" width="90" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>

		<sjg:gridColumn name="fundsTransaction.approverName" index="approverName" title="ApproverName" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="fundsTransaction.userName" index="userName" title="UserName" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="fundsTransaction.rejectReason" index="rejectReason" title="RejectReason" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="fundsTransaction.approverIsGroup" index="approverIsGroup" title="ApproverIsGroup" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.resultOfApproval" width="100"  index="approval" title="Approval" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.canLoad" width="100"  index="load" title="Load" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.canEdit" width="100"  index="edit" title="Edit" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.canDelete" width="100"  index="delete" title="Delete" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="fundsTransaction.statusValue" width="100"  index="delete" title="statusValue" hidden="true" sortable="false" search="false" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="fundsTransaction.approverInfos" index="approverInfos" title="" hidden="true" hidedlg="true" formatter="ns.ach.formatSingleTemplateApproversInfo" />
	</sjg:grid>

	<script type="text/javascript">
	<!--
		$("#singleACHTemplatesGridID").jqGrid('setColProp','ID',{title:false});
        //$("#quickSearchLink").show();
        //$("#templatessearchcriteria").show();
        //$("#multitemplatessearchcriteria").hide();

	//-->
	</script>


