<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getPagedACHTemplatesAction.action?PagedACHTaskName=GetACHBatchTemplates&templateScope=BUSINESS&search=true" URLEncrypt="true"/>
    <s:url id="businessTaxTemplatesUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
			id="businessTaxTemplatesGridID"
			caption=""  
			sortable="true" 
			dataType="json"  
			href="%{businessTaxTemplatesUrl}"
			pager="true"  
			gridModel="gridModel" 
			rowList="%{#session.StdGridRowList}" 
			rowNum="%{#session.StdGridRowNum}" 
			rownumbers="true"
			shrinkToFit="true"
			navigator="true"
			navigatorAdd="false"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			scroll="false"
			scrollrows="true"
			viewrecords="true"
			onGridCompleteTopics="addGridControlsEvents,singleTemplatesGridCompleteEvents"
	> 
			
        <sjg:gridColumn name="templateName" width="60" index="templateName" title="%{getText('ach.grid.templateName')}" sortable="true" search="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.companyID" width="100"  index="companyID" title="%{getText('ach.grid.companyID')}" sortable="true" search="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.coName" width="100"  index="coName" title="%{getText('ach.grid.companyName')}" sortable="true" search="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="templateType" width="50" index="templateType" title="%{getText('ach.grid.templateType')}" sortable="false" search="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="fundsTransaction.status" width="90" index="status" title="%{getText('jsp.default_388')}" sortable="true" search="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="ID" index="action" title="Action" sortable="false" formatter="ns.tax.formatSingleTaxTemplatesActionLinks" width="90" cssClass="datagrid_actionIcons"/>

        <sjg:gridColumn name="fundsTransaction.resultOfApproval" index="resultOfApproval" title="Submitted For" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
        <sjg:gridColumn name="fundsTransaction.canLoad" index="canLoad" title="canLoad" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>


	</sjg:grid>
	
	<script type="text/javascript">
	<!--		
		$("#businessTaxTemplatesGridID").jqGrid('setColProp','ID',{title:false}); 
	//-->
	</script>	
	
