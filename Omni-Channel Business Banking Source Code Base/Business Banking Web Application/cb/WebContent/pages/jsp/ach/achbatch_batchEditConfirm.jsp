<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-jquery-grid-tags" prefix="sjg"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page contentType="text/html; charset=UTF-8" %>

<%  String classCode = null;
%>

<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="classCode" />
<span id="PageHeading" style="display:none;"><s:text name="ach.ACHBatchEdit" /></span>

<% if (request.getParameter("templateEdit") != null) { 
%>
    <ffi:removeProperty name="SearchString"/>
<ffi:cinclude value1="${SetACHTemplate}" value2="" operator="equals">
	<ffi:object id="SetACHTemplate" name="com.ffusion.tasks.banking.SetFundsTransactionTemplate" scope="session"/>
	<ffi:setProperty name="SetACHTemplate" property="Name" value="ACHBatchTemplates" />
</ffi:cinclude>
	<ffi:object id="AddEditACHTemplate" name="com.ffusion.tasks.ach.ModifyACHTemplate" scope="session"/>
	<%	if (request.getParameter("TemplateSessionName") != null) { %>
		<ffi:setProperty name="SetACHTemplate" property="Name" value="${TemplateSessionName}" />
		<ffi:setProperty name="AddEditACHTemplate" property="SessionName" value="${TemplateSessionName}"/>
		<ffi:removeProperty name="TemplateSessionName"/>
	<% } %>
	<ffi:setProperty name="SetACHTemplate" property="ID" value="${TemplateID}" />
	<ffi:process name="SetACHTemplate"/>
	<ffi:setProperty name="AddEditACHTemplate" property="Initialize" value="true"/>
	<ffi:setProperty name="AddEditACHTemplate" property="AchBatchName" value="ACHBatch"/>
	<ffi:setProperty name="AddEditACHTemplate" property="AccountsCollection" value="BankingAccounts"/>
	<ffi:setProperty name="AddEditACHTemplate" property="Type" value="3"/>
	<ffi:process name="AddEditACHTemplate"/>
	<%-- we should now have a "ACHBatch" that came from a template, so we can view it --%>
	<ffi:removeProperty name="AddEditACHTemplate" />
<% } %>


    <s:form id="ACHBatchEditConformForm" validate="false" namespace="/pages/jsp/ach" action="ModifyACHBatchAction"
        method="post" name="FormName" theme="simple" >
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
        <fieldset>
            <legend><s:text name="ach.modify.ACH.BatchInformation" /></legend>
            <div class="type-text">
                <%-- using s:include caused buffer overflow --%>
                <ffi:include page="/pages/jsp/ach/achbatchaddeditconfirm.jsp" />
            </div>
        </fieldset>
        <table width="100%">
            <tr>
                <td align="center">
        <sj:a id="cancelFormButtonOnInput2"
                button="true"
                onClickTopics="cancelACHForm"
        ><s:text name="jsp.default_82"/>
        </sj:a>
        <sj:a id="backFormButtonOnInput2"
                button="true"
                onClickTopics="ach_backToInput"
        ><s:text name="jsp.default_57"/>
        </sj:a>
        <sj:submit
        id="verifyACHBatchSubmit3"
        targets="confirmDiv"
        formIds="ACHBatchEditConformForm"
        button="true"
        onBeforeTopics="beforeConfirmACHBatchForm"
        onCompleteTopics="confirmACHBatchFormComplete"
        onErrorTopics="errorConfirmACHBatchForm"
        onSuccessTopics="successConfirmACHBatchForm"
        value="Confirm/Submit ACH Payment"
        />
        <ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
            <ffi:object name="com.ffusion.tasks.ach.CheckACHSECEntitlement" id="CheckACHSECEntitlement" scope="session"/>
            <ffi:setProperty name="CheckACHSECEntitlement" property="ACHBatchName" value="AddEditACHBatch"/>
            <ffi:setProperty name="CheckACHSECEntitlement" property="OperationName" value="<%= EntitlementsDefines.ACH_TEMPLATE %>"/>
            <ffi:process name="CheckACHSECEntitlement" />
            <ffi:cinclude value1="${CheckACHSECEntitlement.IsEntitledTo}" value2="TRUE" operator="equals">
                <%
                    if (classCode != null && classCode.length() > 3)
                        classCode = classCode.substring(0,3);
                    // RCK, POP, TEL, CIE, ARC, BOC are NOT allowed in templates
                    if (!"RCK".equals(classCode) && !"POP".equals(classCode) && !"BOC".equals(classCode) &&
                        !"TEL".equals(classCode) && !"CIE".equals(classCode) && !"ARC".equals(classCode) )
                    {
                %>
                    <sj:a id="SaveTemplateFormButtonOnInput2"
                            button="true"
                            onClickTopics="ach_backToInput"
                    >
                            <s:text name="jsp.default_366" />
                    </sj:a>
                <% 	} %>
            </ffi:cinclude>
            <ffi:removeProperty name="CheckACHSECEntitlement" />
        </ffi:cinclude>

                </td></tr></table>
    </s:form>
