<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
				 java.util.ArrayList,
				 com.ffusion.beans.fileimporter.MappingDefinition"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<ffi:help id="payments_achimport" className="moduleHelpClass"/>
<% boolean importBatches = false;%>
<% if (request.getParameter("ImportBatches") != null) {
    importBatches = true;
    session.setAttribute("ImportBatches", request.getParameter("ImportBatches"));
 }
 if (request.getParameter("PartialImport") != null) {
     session.setAttribute("PartialImport", request.getParameter("PartialImport"));
 }
%>

<ffi:setProperty name="saveBackURL" value=""/>
<ffi:setProperty name="dontCleanupACHSession" value="true"/>

<%  boolean partialImport = false;
    String partialImportStr = null;
	// default values are used by NACHA import and NOT doing a PARTIAL IMPORT (amounts only)
	String defaultUpdateBy = "" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING_NEW;
	String defaultMatchBy = "" + MappingDefinition.MATCH_RECORDS_BY_ID_NAME_ACCOUNT;
%>
    <ffi:getProperty name="PartialImport" assignTo="partialImportStr" />
	<ffi:removeProperty name="PartialImport" />
<%
	if (partialImportStr == null) { %>
   		<ffi:getProperty name="ProcessACHImport" property="PartialImport" assignTo="partialImportStr" />
<% } %>
<ffi:object id="CheckACHImport" name="com.ffusion.tasks.fileImport.CheckACHImportTask" scope="session"/>
<ffi:object id="ProcessACHImport" name="com.ffusion.tasks.fileImport.ProcessACHImportTask" scope="session"/>
<ffi:setProperty name="ProcessACHImport" property="DefaultUpdateRecordsBy" value="<%= defaultUpdateBy %>"/>
<ffi:setProperty name="ProcessACHImport" property="DefaultMatchRecordsBy" value="<%= defaultMatchBy %>"/>
<% if (importBatches) { %>
    <ffi:setProperty name="CheckACHImport" property="MultipleBatchImport" value="true"/>
    <ffi:setProperty name="ProcessACHImport" property="MultipleBatchImport" value="true"/>
<% } %>

<ffi:object id="ValidateACHBatch" name="com.ffusion.tasks.ach.ValidateACHBatch" scope="session"/>

<ffi:object id="GetMappingDefinitions" name="com.ffusion.tasks.fileImport.GetMappingDefinitionsTask" scope="session"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	<ffi:process name="GetMappingDefinitions"/>
</ffi:cinclude>

<ffi:object id="SetMappingDefinition" name="com.ffusion.tasks.fileImport.SetMappingDefinitionTask" scope="session"/>
<% session.setAttribute("FFISetMappingDefinition", session.getAttribute("SetMappingDefinition")); %>

<ffi:object id="GetOutputFormat" name="com.ffusion.tasks.fileImport.GetOutputFormatTask" scope="session"/>
<%
	if (partialImportStr != null) {
		partialImport = Boolean.valueOf(partialImportStr).booleanValue(); %>
		<ffi:setProperty name='ProcessACHImport' property='PartialImport' value='<%=partialImportStr%>'/>
		<ffi:setProperty name='CheckACHImport' property='PartialImport' value='<%=partialImportStr%>'/>
<% } %>

<%-- Set up the flow of control for  fileimport --%>
   <ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>
   <ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
   <ffi:setProperty name="GenerateReportBase" property="Target" value="${SecurePath}reports/displayreport.jsp"/>

   <ffi:object id="GetReportsByCategoryACH" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
<% if (!importBatches) { %>
   <ffi:setProperty name="GetReportsByCategoryACH" property="ReportCategory" value="<%= com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_FILE_IMPORT %>" />
<% } else { %>
   <ffi:setProperty name="GetReportsByCategoryACH" property="ReportCategory" value="<%= com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_FILE_IMPORT_BATCHES %>" />
<% } %>
   <ffi:setProperty name="GetReportsByCategoryACH" property="ReportsName" value="ReportsACH" />
   <ffi:process name="GetReportsByCategoryACH" />

	<ffi:setProperty name="ReportName" value="${GetReportsByCategoryACH.ReportCategory}" />
	<ffi:setProperty name="ProcessImportTask" value="ProcessACHImport"/>
<% 
	session.setAttribute("FFIProcessACHImport", session.getAttribute("ProcessACHImport"));
	session.setAttribute("FFINewReportBase", session.getAttribute("NewReportBase"));
	session.setAttribute("FFIGenerateReportBase", session.getAttribute("GenerateReportBase"));   
	session.setAttribute("FFICheckACHImport", session.getAttribute("CheckACHImport"));
%>
<script type="text/javascript"><!--

	/* sets the checkboxes on a form to checked or unchecked.
	 * form - the form to operate on
	 * value - to set the checkbox.checked property
	 * exp - set only checkboxes matching exp to value
	 */
	function setClearEntries( value ) {
        var clearEntries1 = document.getElementById("clearEntries1");
        var clearEntries2 = document.getElementById("clearEntries2");
        clearEntries1.value = value;
        clearEntries2.value = value;
	}

	function updateACHCustomMapping(fileType, customFileType, transactionsOnly)
	{
		if (fileType == null || customFileType == null)
			return;
		if (fileType.value == 'Custom')
		{
			customFileType.disabled = false;
			transactionsOnly.disabled = true;
			$("#SetMappingDefinitionIDID").selectmenu('enable');
		}
		else
		{
			customFileType.selectedIndex = 0;
			customFileType.disabled = true;
			transactionsOnly.disabled = false;
			$("#SetMappingDefinitionIDID").selectmenu('disable'); 
		}
	}
	
	var theChild = null;

	$("#FileUploadFileTypeID").selectmenu({width:'20em'});
	$("#SetMappingDefinitionIDID").selectmenu({width:'20em'});
	
	$.subscribe('openFileUploadTopics', function(event,data){
	    $("#openFileImportDialogID").dialog('open');
	});
// --></script>

<br/>
<span class="sectionhead">Please select a file to import entries to the ACH Batch.</span>
<br/>
<br/>

<fieldset id="fileUploadSelectFormatFieldSetID">
<legend>File Import</legend>
		<div>
            <form id="FileTypeID" name="FileType" action="/cb/pages/fileupload/ACHFileUploadAction_openACHEntryImportWidget.action" method="post" target="FileUpload">
            	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                <table border="0" cellspacing="0" cellpadding="3" class="tableData">
                    <tr>
                        <td class="columndata" width="410">
<% if (importBatches) { %>
                            <span class="sectionsubhead"><s:text name="jsp.ach.Import.ACH.Batches" /></span>
<% } else if (partialImport) { %>
                            <span class="sectionsubhead"><s:text name="jsp.ach.Import.Amount.Only" /></span>
<% } else { %>
                            <span class="sectionsubhead"><s:text name="jsp.default_236" /></span>
<% } %>
                        </td>
                        <td width="30"></td>
                        <td><span class="sectionsubhead"><s:text name="jsp.default_14" /></span></td>
                    </tr>
                    <tr>
                        <td class="columndata" width="410" rowspan="5"><s:text name="jsp.ach.fusionbank.msg.part1" /><br>
                            <br><s:text name="jsp.ach.fusionbank.msg.part1" /><!--L10NEnd--></td>
                        <td class="columndata" nowrap width="30"></td>

<% if (importBatches) { %>
                    <td class="columndata" valign="top">
<%-- only supports NACHA files --%>
                        <input type="Hidden" name="fileType" value="ACH Import">
                        	<s:text name="jsp.ach.ACH.File.in.NACHA.Format" />
                        <input id="clearEntries1" type="hidden" name="CheckACHImport.ClearACHEntries" value="false" >
                        <input id="clearEntries2" type="hidden" name="ProcessACHImport.ClearACHEntries" value="false" >
                        <input id="MultiBatches" type="hidden" name="multipleBatches" value="true" >
                    </td>
<% } else { %>
                    <td class="columndata" valign="top">
                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                        <select id="FileUploadFileTypeID" name="fileType" class="txtbox" onChange="updateACHCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID'], document.FileType['transactionsOnly'])">
                            <option value="ACH Import"><!--L10NStart-->ACH File in NACHA Format<!--L10NEnd--></option>
                            <option value="Custom"><s:text name="jsp.default_132" /></option>
                        </select>
                    </ffi:cinclude>
                    <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                        <input type="Hidden" name="fileType" value="ACH Import">
                        <s:text name="jsp.ach.ACH.File.in.NACHA.Format" />
                    </ffi:cinclude>
                    </td>
<% } %>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="bottom"><span class="sectionsubhead">
<% if (!importBatches) { %>
                        	<s:text name="jsp.ach.Validate.Entries.Only" />
                        <input type="checkbox" checked name="transactionsOnly" value="true" border="0" >
<% } else { %>
                        <input type="Hidden" name="transactionsOnly" value="false">
<% } %>
                        </span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="bottom"><span class="sectionsubhead">
<% if (!importBatches && !partialImport) { %>
                      	  <s:text name="jsp.ach.Clear.Destination.Entries"/>
                        <input type="checkbox" name="clearEntries" value="true" border="0" onclick="setClearEntries(this.checked);">
                        <input id="clearEntries1" type="hidden" name="CheckACHImport.ClearACHEntries" value="false" >
                        <input id="clearEntries2" type="hidden" name="ProcessACHImport.ClearACHEntries" value="false" >
<% } %>
                        </span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="bottom"><span class="sectionsubhead">
<% if (!importBatches) { %>
                        <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                            <s:text name="jsp.cash_1" />
                        </ffi:cinclude>
<% } %>
                        </span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td valign="top" class="columndata" nowrap>
<% if (!importBatches) { %>
                        <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                            <select name="SetMappingDefinition.ID" class="txtbox" id="SetMappingDefinitionIDID">
                                <option value="0">- <!--L10NStart-->Select Custom Mapping<!--L10NEnd--> -</option>
                                <ffi:list collection="MappingDefinitions" items="MappingDefinition">
                                    <% String entitlementName = ""; %>
                                    <ffi:cinclude value1="${MappingDefinition.OutputFormatName}" value2="" operator="notEquals">
                                        <ffi:setProperty name="GetOutputFormat" property="Name" value="${MappingDefinition.OutputFormatName}"/>
                                        <ffi:process name="GetOutputFormat"/>
                                        <ffi:setProperty name="OutputFormat" property="CurrentCategory" value="ACH"/>

                                        <ffi:getProperty name="OutputFormat" property="EntitlementName" assignTo="entitlementName" />

                                        <ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
                                            <% boolean displayMap = true;
                                            if (entitlementName != null && entitlementName.length() > 0) { %>
                                                <ffi:cinclude ifNotEntitled="<%=entitlementName%>" >
                                                    <% displayMap = false; %>
                                                </ffi:cinclude>
                                            <% }
                                            String updateRecordsBy = null; %>
                                            <ffi:getProperty name="MappingDefinition" property="UpdateRecordsBy" assignTo="updateRecordsBy" />
                                            <%
                                            if (partialImport && updateRecordsBy != null && !updateRecordsBy.equals("" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING_AMOUNTS_ONLY))
                                                displayMap = false;
                                            if (!partialImport && updateRecordsBy != null && updateRecordsBy.equals("" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING_AMOUNTS_ONLY))
                                                displayMap = false;
                                            if (displayMap)
                                            { %>
                                            <option value="<ffi:getProperty name='MappingDefinition' property='MappingID'/>"><ffi:getProperty name="MappingDefinition" property="Name"/></option>
                                            <% } %>
                                        </ffi:cinclude>
                                    </ffi:cinclude>
                                </ffi:list>
                            </select><br/>
                            <span id="mappingTypeError"></span>
                        </ffi:cinclude>
                        <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                            <input type="Hidden" name="SetMappingDefinition.ID" value="0">
                        </ffi:cinclude>
<% } else { %>
                            <input type="Hidden" name="SetMappingDefinition.ID" value="0">
<% } %>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center" nowrap>
                            <%
                            	if(importBatches) {
                            
                            %>
	                            <sj:a
	                                id="cancelFileImportACHTypeID"
	                                onClickTopics="closeImportACHEntryBackToMultiACHTemplate"
	                                button="true"
	                                cssClass="cancelImportFormButton"
	                                ><s:text name="jsp.default_82" /><!-- CANCEL -->
	                            </sj:a>
                            <%
                            	}else {
                            %>
                            <!-- For Import Amount and Import Entries -->
                              <sj:a
	                                id="cancelFileImportACHTypeID"
	                                onClickTopics="closeImportACHEntryForm"
	                                button="true"
    	                            cssClass="cancelImportFormButton"
	                                ><s:text name="jsp.default_82" /><!-- CANCEL -->
	                            </sj:a>
	                        <%} %>
                            
                            <sj:a
                                id="fileImportACHTypeID"
                                formIds="FileTypeID"
                                targets="openFileImportDialogID"
                                button="true"
                                validate="true" 
                                validateFunction="customValidation"
                                onclick="removeValidationErrors();"
                                onErrorTopics="errorOpenFileUploadTopics"
                                onSuccessTopics="openFileUploadTopics"
                                ><s:text name="jsp.default_235" /><!-- IMPORT FILE -->
                            </sj:a>
                        </td>
                    </tr>
                </table>
			</form>
		</div>
    </fieldset>
<ffi:removeProperty name="GetOutputFormat"/>
<script>
	updateACHCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID'], document.FileType['transactionsOnly']);
	$(document).ready(function(){		
		//load import dialog if not loaded
		var config = ns.common.dialogs["fileImport"];
		config.autoOpen = "false";
		ns.common.openDialog("fileImport");
		
		var configImportResults = ns.common.dialogs["fileImportResults"];
		configImportResults.autoOpen = "false";
		ns.common.openDialog("fileImportResults");	
		});
</script>
<ffi:setProperty name="actionNameNewReportURL" value="NewReportBaseAction.action?NewReportBase.ReportName=${ReportName}&flag=generateReport" URLEncrypt="true" />
<ffi:setProperty name="actionNameGenerateReportURL" value="GenerateReportBaseAction_display.action?doneCompleteDirection=${doneCompleteDirection}" URLEncrypt="true" />