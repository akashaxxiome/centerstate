<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	<ffi:process name="SetMappingDefinition"/>
</ffi:cinclude>
		
<script>
	//isHomePage = true;

    /**
 *  the method is for upload file function of upload buttion onclick method
 * @param formID
 */
function updateACHRequestParams(formID){
    var originalURL= "/cb/pages/jsp/ach/ACHEntryFileUploadAction.action";
    submitURL = originalURL + "?";
    var inputValue = $('#' + formID)[0].CSRF_TOKEN.value;
    submitURL = submitURL + "CSRF_TOKEN=" + inputValue;
    $('#' + formID).attr('action',submitURL);
}

	uploadedFile = "";
	var loading = "<img src='/cb/web/images/loading.gif'/>";
	
	/**
	 * Send wire transfer end.
	 */
	$.subscribe('beforeImportFileFormTopics', function(event,data) {
		//Hide the import form
		$("#importedFileFormDivID").hide();
		beginUpload("#uploadprogressbar", "", "");
		//$("#importedFileFormDivID").attr('style','display:none');
		//show the status of uploading
		showImportedFileName();
		
	});
	
	$.subscribe('onCompleteImportFileFormTopics', function(event,data) {
		$("#showImportFileNameID").html("Uploaded " + uploadedFile); 
		$("#showImportFileNameID").after("<br><a onclick='checkFileImportResults()' href='javascript:void(0)'>Check Import Results</a>");
		
	});
	
	
	$.subscribe('closeUploadFileWindowTopic', function(event,data) {		
		$('#selectFileOFFileUploadFormID').html("");
	});
	
	function closeUploadFileWindow(){
		$('#selectFileOFFileUploadFormID').html("");
	}

	function showImportedFileName(){
		var importedFile = $("#importFileInputID").val();
		var length = importedFile.length;
		var x = importedFile.lastIndexOf("\\");
		x++; 
		var fileName = importedFile.substring(x,length);
		uploadedFile = fileName;
		
		$("#showImportFileNameID").html("Uploading " + fileName + loading);

		//$('#showImportFileNameID').removeAttr('style');
	}

    function checkFileImportResults( urlString ){
    	$('#checkFileACHImportResultsDialogID').dialog('open');
    };
    
</script>

<div align="center">
	<fieldset id="fileUploadSelectFileFieldSetID" class="dialogueData">
	<legend>File Upload</legend>
			
				<span class="progressbar" style="display:none;" id="uploadprogressbar">0%</span>
			
			<%-- <sj:progressbar id="uploadprogressbar" cssStyle="height: 50%;"/>--%>
			
			<div id="uploadFileErrorMessageDivID"><s:actionerror/></div>
			<div id="showImportFileNameID"></div>
			<div id="importedFileFormDivID">
			<table width="282" border="0" cellspacing="0" cellpadding="0">
				<%-- 
				<tr>
					<td colspan="2" class="sectiontitle sectionsubhead" align="left" width="282" height="14" background="/cb/web/multilang/grafx/payments/sechdr_blank_282.gif">
						&nbsp;&gt; <!--L10NStart-->File Upload<!--L10NEnd-->
					</td>
				</tr>
				--%>
				<tr>
					<td class="columndata lightBackground" colspan="2">
						<div align="left">
							<span class="columndata"><s:text name="jsp.ach.ACHENTRY.msg"/></span></div>
					</td>
				</tr>
				<tr>
					<td class="columndata lightBackground" colspan="2" align="center">
						<ffi:setProperty name="successAddr" value="/pages/jsp/ach/achimportwait.jsp"/>
						<s:form name="FileForm" enctype="multipart/form-data" action="/cb/pages/jsp/ach/ACHEntryFileUploadAction.action" method="post" id="uploaderID">
               				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" id="CSRF_TOKEN"/>
							<%--<INPUT TYPE="FILE" NAME="FileUpload.FileName" VALUE="" MAXLENGTH=255 SIZE=25><P>--%>
							<%--<INPUT TYPE="FILE" NAME="file" VALUE="" MAXLENGTH=255 SIZE=25><P>--%>
							<s:file id="importFileInputID" name="file" accept="text/plain" size="25"/><span id="FileError"></span>
							<input type="hidden" name="overWrite" value="true">
							<input type="hidden" name="ProcessImportTask" value="ProcessACHImport">
							<input type="hidden" name="SuccessURL" value='<ffi:getProperty name="successAddr"/>'>
						</s:form>
						<ffi:removeProperty name="successAddr"/>
					</td>
				</tr>
				<tr>
					<td class="columndata lightBackground" colspan="2" align="center">
						<sj:a 
								id="cancelFileUploadingID"
                                button="true"
								onClickTopics="closeUploadFileWindowTopic,tabifyNotes"								
		                        ><s:text name="jsp.default_82"/>
		            	</sj:a>
						<sj:a 
								id="fileUploaderFormID"
								formIds="uploaderID"
								targets="checkFileACHImportResultsDialogID"
                                button="true" 
                                onBeforeTopics="beforeImportFileFormTopics"
								onclick="updateACHRequestParams('uploaderID');"
								onCompleteTopics="onCompleteImportFileFormTopics,tabifyNotes"
		                        ><s:text name="jsp.default_452"/><!-- UPLOAD -->
		            	</sj:a>
					</td>
				</tr>
				<%-- 
				<tr>
					<td colspan="2"><img src="/cb/web/multilang/grafx/payments/sechdrs_btm_282.gif" alt="" width="282" height="11" border="0"></td>
				</tr>
				--%>
			</table>
			</div>
	<div id="uploadingStatus1" ></div>
	<iframe style="display: none;" name="progressFrame"></iframe>
	</fieldset>
</div>
