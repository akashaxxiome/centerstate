<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>


<ffi:cinclude value1="${ReverseACHBatch.CanReverse}" value2="true" operator="notEquals" >
		<%-- include the rest of this file to confirm --%>


		<div align="center">
			<div align="center">
				<div align="center">
					<table width="750" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>&nbsp;</td>
							<td class="columndata lightBackground">
								<form action="achbatch-reverse-send.jsp" method="post" name="ViewForm">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<div align="center">
										<table width="720" border="0" cellspacing="0" cellpadding="3">
											<tr>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											</tr>
											<tr>
												<td class="sectionhead lightBackground" colspan="6"><span class="sectionhead">&gt; <s:text name="ach.ACHBatchReversal" /> </span></td>
											</tr>
											<tr>
												<td class="columndata" colspan="6"><hr class="thingrayline"></td>
											</tr>
											<tr>
												<td class="columndata lightBackground" colspan="6"><s:text name="ach.ACH.regulations" /><br></td>
											</tr>
											<tr>
												<td class="columndata lightBackground"><div align="right"></div></td>
												<td colspan="4"><div align="center">
														<input type="Hidden" name="ReverseACHBatch.AcceptedDateRestriction" value="true">
														<input class="submitbutton" type="button" value="Cancel" onclick="document.location='achpayments.jsp';return false;">&nbsp;&nbsp;&nbsp;
														<input class="submitbutton" type="button" value="Reverse" onclick="document.location='achbatch-reverse-send.jsp';return false;">
													</div>
												</td>
											</tr>
										</table>
									</div>
								</form>
							</td>
							<td align="right"></td>
						</tr>
					</table>
					<br>
				</div>
			</div>
		</div>

</ffi:cinclude>