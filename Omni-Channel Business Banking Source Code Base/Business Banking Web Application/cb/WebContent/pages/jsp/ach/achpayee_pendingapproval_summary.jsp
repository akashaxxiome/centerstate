<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_ACHPayeePendingApprovalSummary" className="moduleHelpClass"/>

<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
	<ffi:setProperty name="canManageAchParticipants" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
	<ffi:setProperty name="canManageAchParticipants" value="false"/>
</ffi:cinclude>

	<%-- Instantiate Dual Approval Action Tasks --%>
	<ffi:setGridURL grid="GRID_DAACHPayee" name="ViewURL" url="/cb/pages/jsp/ach/viewACHPayeeAction_init.action?PayeeID={0}&IsPending=true" parm0="ID"/>
	<ffi:setGridURL grid="GRID_DAACHPayee" name="EditURL" url="/cb/pages/jsp/ach/editACHPayeeAction_init.action?PayeeID={0}&daItemId={1}&Action={2}&IsPending=true" parm0="ID" parm1="DaItemId" parm2="Action"/>
	<ffi:setGridURL grid="GRID_DAACHPayee" name="ModifyURL" url="/cb/pages/jsp/ach/achpayee_payeeModify.jsp?PayeeID={0}&itemId={1}&Nickname={2}&itemType={3}&Action={4}&IsPending=true" parm0="ID" parm1="ID" parm2="NickNameSecure" parm3="ItemType" parm4="Action"/>
	<ffi:setGridURL grid="GRID_DAACHPayee" name="DiscardURL" url="/cb/pages/jsp/ach/achpayee_payeeDiscard.jsp?PayeeID={0}&itemId={1}&Nickname={2}&itemType={3}&Action={4}&IsPending=true" parm0="ID" parm1="ID" parm2="NickNameSecure" parm3="ItemType" parm4="Action"/>
	
	<ffi:setProperty name="pendingApprovalACHPayeeTempURL" value="/pages/jsp/ach/getPendingApprovalACHParticipantsAction.action?Show=Pending&PayeeType=&searchOper=cn&GridURLs=GRID_DAACHPayee" URLEncrypt="true"/>
    
    <s:url namespace="/pages/jsp/ach" id="pendingApprovalACHPayeeUrl" value="%{#session.pendingApprovalACHPayeeTempURL}" escapeAmp="false"/>
	
	<sjg:grid
		id="pendingApprovalACHPayeeGridId"
		caption=""  
		sortable="true"
		sortname="NICKNAME"
		dataType="json"  
		href="%{pendingApprovalACHPayeeUrl}"
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,pendingApprovalACHPayeeGridCompleteEvents"
		> 

        <sjg:gridColumn name="payeeType" edittype="select" editoptions="{value:'1:Standard;2:IAT Payee'}" width="120" index="PAYEETYPE" title="Payee Type" sortable="true" search="false" hidden="true" hidedlg="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="nickNameSecure" width="100" index="NICKNAME" title="Nickname" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="nameSecure" width="100" index="NAME" title="Payee Name" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" searchoptions="{searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="userAccountNumber" width="100"  index="USERACCOUNTNUMBER" title="Payee ID" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="displayACHCompany" width="120" index="COMPANYDISPLAYNAME" title="Company ID/Name" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" formatter="ns.ach.formatACHPayeesCompanyNameID" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="payeeGroup" edittype="select" editoptions="{value:'1:User;2:ACH Company;3:Business'}" width="120" index="PAYEEGROUP" title="Payee Scope" sortable="true" search="false" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="bankNameSecure" width="120"  index="BANKNAME" title="Bank Name" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="routingNumberSecure" width="80" index="ROUTINGNUM" title="Routing Number" hidden="true" hidedlg="true" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="accountNumberSecure" width="120" index="ACCOUNTNUMBER" title="Account Number" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="accountTypeSecure" edittype="select" hidden="true" hidedlg="true" editoptions="{value:'1:Checking;2:Savings;3:Loan;4:General Ledger'}" width="80" index="accountType" title="Account Type" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="securePayee" edittype="checkbox" editoptions="{value:'true:false'}" search="false" width="120" index="securePayee" title="Secure Payee" sortable="false" hidden="true" hidedlg="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="displayAsSecurePayee" width="120" index="displayAsSecurePayee" search="false" title="Display as Secure Payee" sortable="false" hidden="true" hidedlg="true" editable="false"  cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="prenoteStatus" width="120" index="prenote" search="false" title="Prenote" sortable="false" hidden="true" hidedlg="true" editable="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="daItemId" index="daItemId" title="daItemId" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="daStatus" index="daStatus" title="daStatus" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="action" index="action" title="User Action" sortable="true" width="80" search="false" cssClass="datagrid_actionIcons"/>
        <sjg:gridColumn name="edit" index="edit" title="Edit" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="discard" index="discard" title="Discard" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="modifyChange" index="modifyChange" title="Modify" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="approve" index="approve" title="Approve" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="view" index="view" title="View" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="reject" index="reject" title="Reject" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="submitForApproval" index="submitForApproval" title="Submit for approval" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="submittedBy" index="submittedBy" title="Submitted By" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="rejectReason" index="rejectReason" title="Reject reason" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="rejectedBy" index="rejectedBy" title="Rejected By" sortable="true"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.ViewURL" index="map.ViewURL" title="View" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="map.EditURL" index="map.EditURL" title="Edit" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="map.ModifyURL" index="map.ModifyURL" title="Modify" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="map.DiscardURL" index="map.DiscardURL" title="Discard" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
        <sjg:gridColumn name="ID" index="action" title="Action" sortable="false" search="false" formatter="ns.ach.formatPendingApprovalACHPayeeActionLinks" 
        	formatoptions="{ 
			canManageParticipant : '%{#session.canManageAchParticipants}'			
			}"	width="90" hidden="true" hidedlg="true" cssClass="__gridActionColumn" />
	</sjg:grid>

<script type="text/javascript">
<!--
	$("#pendingApprovalACHPayeeGridId").data("supportSearch",true);
	$("#pendingApprovalACHPayeeGridId").jqGrid('setColProp','ID',{title:false}); 
//-->
</script>
