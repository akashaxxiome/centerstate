<%@page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-jquery-grid-tags" prefix="sjg"%>
<%@page contentType="text/html; charset=UTF-8" %>

<span id="PageHeading" style="display:none;">ACH Batch Add</span>

<%
    session.setAttribute("subMenuSelected", "ach");
%>
<%  String classCode = null;
%>

<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="classCode" />

    <ffi:removeProperty name="BatchID"/>
    <s:form id="ACHBatchAddConfirmForm" validate="false" namespace="/pages/jsp/ach" action="AddACHBatchAction"
        method="post" name="FormName" theme="simple" >
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
            <fieldset>
                <legend><s:text name="ach.enter.ach.batch.information" /></legend>
                <div class="type-text">
                    <%-- using s:include caused buffer overflow --%>
                    <ffi:include page="/pages/jsp/ach/achbatchaddeditconfirm.jsp" />
                </div>
            </fieldset>
        <table width="100%">
            <tr>
                <td align="center">
            <sj:a id="cancelFormButtonOnInput1"
                    button="true"
                    onClickTopics="cancelACHForm"
            ><s:text name="jsp.default_82"/>
            </sj:a>
            <sj:a id="backFormButtonOnInput1"
                    button="true"
                    onClickTopics="ach_backToInput"
            ><s:text name="jsp.default_57"/>
            </sj:a>
            <sj:submit
            id="verifyACHBatchSubmit3"
            targets="confirmDiv"
            formIds="ACHBatchAddConfirmForm"
            button="true"
            onBeforeTopics="beforeConfirmACHBatchForm"
            onCompleteTopics="confirmACHBatchFormComplete"
            onErrorTopics="errorConfirmACHBatchForm"
            onSuccessTopics="successConfirmACHBatchForm"
            value="Confirm/Submit ACH Payment"
            />
            <ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
                <ffi:object name="com.ffusion.tasks.ach.CheckACHSECEntitlement" id="CheckACHSECEntitlement" scope="session"/>
                <ffi:setProperty name="CheckACHSECEntitlement" property="ACHBatchName" value="AddEditACHBatch"/>
                <ffi:setProperty name="CheckACHSECEntitlement" property="OperationName" value="<%= EntitlementsDefines.ACH_TEMPLATE %>"/>
                <ffi:process name="CheckACHSECEntitlement" />
                <ffi:cinclude value1="${CheckACHSECEntitlement.IsEntitledTo}" value2="TRUE" operator="equals">
                    <%
                        if (classCode != null && classCode.length() > 3)
                            classCode = classCode.substring(0,3);
                        // RCK, POP, TEL, CIE, ARC, BOC are NOT allowed in templates
                        if (!"RCK".equals(classCode) && !"POP".equals(classCode) && !"BOC".equals(classCode) &&
                            !"TEL".equals(classCode) && !"CIE".equals(classCode) && !"ARC".equals(classCode) )
                        {
                    %>
                        <sj:a id="SaveTemplateFormButtonOnInput2"
                                button="true"
                                onClickTopics="ach_backToInput"
                        >
                               <s:text name="jsp.default_366" />
                        </sj:a>
                    <% 	} %>
                </ffi:cinclude>
                <ffi:removeProperty name="CheckACHSECEntitlement" />
            </ffi:cinclude>
                    
        <%--onClick="onSubmitCheck(false)"--%>
        </td></tr></table>
    </s:form>
