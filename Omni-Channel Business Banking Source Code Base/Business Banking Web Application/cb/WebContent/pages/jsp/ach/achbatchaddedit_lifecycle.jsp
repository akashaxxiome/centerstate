<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<script type="text/javascript">
var textarea_ACH_maxlen = {
  isMax : function (){
	
	var textarea = 
		document.getElementById("achTemplateCommentID");
	var max_length = 985;
	if(textarea.value.length > max_length){
	  textarea.value = 
		textarea.value.substring(0, max_length);
	  document.getElementById("ACHTemplateCommentError").innerHTML=js_ach_template_comment_error;
	}
  },
  disabledRightMouse : function (){
	document.oncontextmenu = 
	  function (){ return false; }
  },
  enabledRightMouse : function (){
	document.oncontextmenu = null;
  }
};
</script>

			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData">
                           <!--  <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
	                           <td  class="sectionhead nameSubTitle" colspan="3" style="cursor:auto">
	                           	L10NStartTemplate LifecycleL10NEnd
							   </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="15"></td>
                             </tr>-->
                            <tr>
								<td align="right" width="20%"><span class="sectionsubhead" ><s:text name="jsp.default_130" /></span></td>
								<td class="columndata">
                                    &nbsp;<ffi:getProperty name="AddEditACHBatch" property="Status"/>
								</td>
							</tr>
                            <tr>
							    <td align="right" width="10%" valign="top" style="padding-top:5px"><span class="sectionsubhead"><s:text name="jsp.default_33" /></span></td>
								<td class="columndata">
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
				                    &nbsp;<textarea id="achTemplateCommentID" onkeyup="textarea_ACH_maxlen.isMax()" onfocus="textarea_ACH_maxlen.disabledRightMouse()" onblur="textarea_ACH_maxlen.enabledRightMouse()" class="ui-widget-content ui-corner-all txtbox" name="AddEditACHBatch.TemplateComment" rows="2" cols="40"><ffi:getProperty name="AddEditACHBatch" property="TemplateComment"/></textarea><span id="ACHTemplateCommentError" style="font-weight:bold; color:red"></span>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
				                    &nbsp;<textarea id="achTemplateCommentID" onkeyup="textarea_ACH_maxlen.isMax()" onfocus="textarea_ACH_maxlen.disabledRightMouse()" onblur="textarea_ACH_maxlen.enabledRightMouse()" class="ui-widget-content ui-corner-all txtbox" name="AddEditACHBatch.TemplateComment" rows="2" cols="40" ><ffi:getProperty name="AddEditACHBatch" property="TemplateComment"/></textarea><span id="ACHTemplateCommentError" style="font-weight:bold; color:red"></span>
</ffi:cinclude>
                                </td>
							</tr>
							<s:if test="%{#attr.auditHistoryRecords.size() > 0}">
	                            <tr>
	                                <td class="sectionsubhead ltrow2_color" align="right"><s:text name="jsp.default_411" /></td>
	                                <td class="columndata ltrow2_color" colspan="2">
	                                    <a href="javascript:tempHistoryDisplay(true)" onclick="tempHistoryDisplay(true)" class="ui-button" title="View History" id="viewTempHistory">
	                                    	<span class="sapUiIconCls icon-positive"></span>
	                                    </a>
	                                    <a href="javascript:tempHistoryDisplay(false)" class="ui-button" onclick="tempHistoryDisplay(false)" id="hideTempHistory" title="Hide History" style="display:none">
	                                       <span class="sapUiIconCls icon-negative"></span>
	                                    </a>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td colspan="2">
	                                    <span id="tempHistory" style="display:none">
	                                        <ffi:include page="/pages/jsp/ach/inc/include-view-transaction-history.jsp"/>
	                                    </span>
	                                </td>
	                            </tr>
                            </s:if>
                         </table>
