<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.beans.ach.ACHBatch,
				 com.ffusion.beans.SecureUser,
                 com.ffusion.csil.core.common.EntitlementsDefines,
				 com.ffusion.beans.ach.ACHClassCode,
				 java.lang.Double,
				 com.ffusion.beans.common.Currency,
				 java.text.DecimalFormat,
                 java.math.BigDecimal" %>
<%--
	This file is used by both Add/Edit ACH AND Add/Edit ACH Templates
	If you modify this file, make sure your change works for both please.
--%>
<%
	boolean disabledControls = false;
	boolean showValidationErrors = true;
	boolean addEntryDisabled = false;
  	boolean isEdit = false;
    boolean isView = false;
    boolean isDelete = false;
    boolean canDelete = true;
	boolean isTemplate = true;
	boolean fromTemplate = false;
    String tempEntToCheck="";
    String batchSECCode="";
    %>
<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="View">
    <ffi:help id="payments_achmultibatchview" className="moduleHelpClass"/>
    <% disabledControls = true; isView = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="Delete">
    <ffi:help id="payments_achmultitemplatedelete" className="moduleHelpClass"/>
    <% disabledControls = true; isDelete = true;  %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="Load">
    <ffi:help id="payments_achmultibatchnew" className="moduleHelpClass"/>
    <% fromTemplate = true; isTemplate = false; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="Add">
    <ffi:help id="payments_achmultibatchaddedit" className="moduleHelpClass"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="Edit">
    <ffi:help id="payments_achmultibatchaddedit" className="moduleHelpClass"/>
    <% isEdit = true; %>
</ffi:cinclude>

<ffi:cinclude value1="${AddEditMultiACHTemplate.BatchType}" value2="TEMPLATE" operator="notEquals">
	<ffi:cinclude value1="${subMenuSelected}" value2="ach" operator="equals" >
		<span class="shortcutPathClass" style="display: none;">pmtTran_ach,addMultipleACHBatchLink</span>
		<span class="shortcutEntitlementClass" style="display: none;">ACHBatch</span>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="tax" operator="equals" >
		<span class="shortcutPathClass" style="display:none;">pmtTran_tax,addMultipleTaxBatchLink</span>
		<span class="shortcutEntitlementClass" style="display: none;">CCD + TXP</span>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="child" operator="equals" >
		<span class="shortcutPathClass" style="display:none;">pmtTran_childsp,addMultipleChildSupportBatchLink</span>
		<span class="shortcutEntitlementClass" style="display: none;">CCD + DED</span>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${AddEditMultiACHTemplate.BatchType}" value2="TEMPLATE" operator="equals">
	<ffi:cinclude value1="${subMenuSelected}" value2="ach" operator="equals" >
		<span class="shortcutPathClass" style="display: none;">pmtTran_ach,addMultipleTemplateLink</span>
		<span class="shortcutEntitlementClass" style="display: none;">Overall-ACH Template</span>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="tax" operator="equals" >
		<span class="shortcutPathClass" style="display:none;">pmtTran_tax,addMultipleTemplateLink</span>
		<span class="shortcutEntitlementClass" style="display: none;">CCD + TXP</span>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="child" operator="equals" >
		<span class="shortcutPathClass" style="display:none;">pmtTran_childsp,addMultipleTemplateLink</span>
		<span class="shortcutEntitlementClass" style="display: none;">CCD + DED</span>
	</ffi:cinclude>
</ffi:cinclude>
<%
    if (request.getParameter("ToggleSortedBy") != null) {
  	String toggle = ""; %>
	<ffi:getProperty name="ToggleSortedBy" assignTo="toggle"/>
  	<ffi:setProperty name="AddEditMultiACHTemplate" property="ACHBatches.ToggleSortedBy" value="<%= toggle %>" />
<%    } %>

    <ffi:removeProperty name="ImportErrors" />

<s:action namespace="/pages/jsp/ach" name="ACHInitAction_initACHBatches" />
<%

                                    String[] titleStrings;
                                    String title = "";
                                    int titleIndex = 0;
	String[] achStrings = {
						      "<!--L10NStart-->Create ACH Multiple Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create ACH Multiple Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create ACH Multiple Batches (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit ACH Multiple Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit ACH Multiple Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit ACH Multiple Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->View ACH Multiple Batch<!--L10NEnd-->",
						      "<!--L10NStart-->View ACH Multiple Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->View ACH Multiple Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Delete ACH Multiple Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Delete ACH Multiple Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Delete ACH Multiple Batch (from template)<!--L10NEnd-->"
    };

    if (isEdit)
        titleIndex = 3;
    if (isView)
        titleIndex = 6;
    if (isDelete)
        titleIndex = 9;
    if (isTemplate)
        titleIndex ++;
    if (fromTemplate)
        titleIndex += 2;
    title = achStrings[titleIndex];
	String pageHeading = title;
	session.setAttribute("PageHeading", pageHeading );
 %>

<script language="JavaScript">
<!--

function importBatches()
{
    // we need to serialize the data so it doesn't get lost before importing entries
    var strutsActionName = 'editMultiACHTemplateAction_saveParameters.action';
	<%if(fromTemplate){%>
    var importBatchesUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achimport.jsp?PartialImport=false&ImportBatches=true&fromTemplate=true"/>';
	<%}else{%>
	  var importBatchesUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achimport.jsp?PartialImport=false&ImportBatches=true&fromTemplate=false"/>';
	<%}%>
    $.ajax({
        type: "POST",
        url: "/cb/pages/jsp/ach/" + strutsActionName,
        data: $("#AddBatchFormId").serialize(),
        success: function(data) {
            ns.ach.importBatches(importBatchesUrl);
        }
    });
}

function holdAllBatches(holdCheckBox){
    var size = <ffi:getProperty name="AddEditMultiACHTemplate" property="ACHBatches.Size"/>;        // number of entries
    var Checked = holdCheckBox.checked;
    for (i = 0; i < size; i++)
    {
        var hold = document.getElementById("hold"+ i);
        // if we changeHold when already on/off hold, totals get messed up
        if (hold.checked != Checked)       // only change state if not already at that state
        {
            hold.checked = Checked;
            changeHold(hold, i);
        }
    }
}

function changeHold(holdCheckBox, ActiveID){
    var totals = document.getElementById("totals"+ActiveID).value;


    tagCredit = frm['TotalNumberCredits'];
    tagDebit = frm['TotalNumberDebits'];
	spanCredit=$('#TotalNumberCreditsVal');
	spanDebit=$('#TotalNumberDebitsVal');

	valueCredit = tagCredit.value;
	valueDebit = tagDebit.value;
	totalCredit = parseInt(valueCredit);
	totalDebit = parseInt(valueDebit);
    changeCredit = parseInt(totals.substr(0, totals.indexOf('_')));
    totals = totals.substr(totals.indexOf('_') + 1);
    changeDebit = parseInt(totals.substr(0, totals.indexOf('_')));
    totals = totals.substr(totals.indexOf('_') + 1);

    creditAmount = totals.substr(0, totals.indexOf('_'));
    debitAmount  = totals.substr(totals.indexOf('_') + 1);

    var creditAmt = parseFloat(creditAmount);
    if (isNaN(creditAmt))
        creditAmt = parseFloat("0.00");
    var debitAmt = parseFloat(debitAmount);
    if (isNaN(debitAmt))
        debitAmt = parseFloat("0.00");

    tagCreditAmount = frm['TotalCreditAmount'];
    tagDebitAmount = frm['TotalDebitAmount'];

	spanCreditAmount = $('#TotalCreditAmountVal');
    spanDebitAmount = $('#TotalDebitAmountVal');

	valueCreditAmount = tagCreditAmount.value;
	while (valueCreditAmount.indexOf(',') > -1)
	{
		valueCreditAmount = valueCreditAmount.substr(0,valueCreditAmount.indexOf(',')) + valueCreditAmount.substr(valueCreditAmount.indexOf(',')+1);
	}
	valueDebitAmount = tagDebitAmount.value;
	while (valueDebitAmount.indexOf(',') > -1)
	{
		valueDebitAmount = valueDebitAmount.substr(0,valueDebitAmount.indexOf(',')) + valueDebitAmount.substr(valueDebitAmount.indexOf(',')+1);
	}

	creditamtTotal = parseFloat(valueCreditAmount);
	debitamtTotal = parseFloat(valueDebitAmount);

	if (holdCheckBox.checked == true){
		document.getElementById("active"+ActiveID).value = "false";
		totalDebit -= changeDebit;			// subtract out previous count
		totalCredit -= changeCredit;			// subtract out previous count
        creditamtTotal -= creditAmt;
        debitamtTotal -= debitAmt;
	} else {
		document.getElementById("active"+ActiveID).value = "true";
		totalDebit += changeDebit;			// Add in  previous count
		totalCredit += changeCredit;			// Add in previous count
        creditamtTotal += creditAmt;
        debitamtTotal += debitAmt;
	}
    amtStr = "" + totalDebit;
    tagDebit.value = amtStr;
	spanDebit.text(amtStr);
    amtStr = "" + totalCredit;
    tagCredit.value = amtStr;
	spanCredit.text(amtStr);

	debitamtTotal = Math.round(debitamtTotal*Math.pow(10,2))/Math.pow(10,2);
	debitamtStr = "" + debitamtTotal;
// WARNING.... Custom Code for US follows, Decimal = ".", grouping separator = ","
	if (debitamtStr.indexOf('.') == -1) { debitamtStr = debitamtStr+".0" } // Add .0 if a decimal point doesn't exist
	temp=(debitamtStr.length-debitamtStr.indexOf('.')); // Find out if the # ends in a tenth (ie 145.5)
	if (temp <= 2) { debitamtStr=debitamtStr+"0"; }; // if it ends in a tenth, add an extra zero to the string
// add in commas again
	var x = debitamtStr.indexOf('.') - 3;
	while (x > 0)
	{
		debitamtStr = debitamtStr.substr(0,x) + ',' + debitamtStr.substr(x, debitamtStr.length);
		x = debitamtStr.indexOf(',')-3;
	}
	tagDebitAmount.value = debitamtStr;
	spanDebitAmount.text('$'+debitamtStr);

	creditamtTotal = Math.round(creditamtTotal*Math.pow(10,2))/Math.pow(10,2);
	creditamtStr = "" + creditamtTotal;
// WARNING.... Custom Code for US follows, Decimal = ".", grouping separator = ","
	if (creditamtStr.indexOf('.') == -1) { creditamtStr = creditamtStr+".0" } // Add .0 if a decimal point doesn't exist
	temp=(creditamtStr.length-creditamtStr.indexOf('.')); // Find out if the # ends in a tenth (ie 145.5)
	if (temp <= 2) { creditamtStr=creditamtStr+"0"; }; // if it ends in a tenth, add an extra zero to the string
// add in commas again
	var x = creditamtStr.indexOf('.') - 3;
	while (x > 0)
	{
		creditamtStr = creditamtStr.substr(0,x) + ',' + creditamtStr.substr(x, creditamtStr.length);
		x = creditamtStr.indexOf(',')-3;
	}
	tagCreditAmount.value = creditamtStr;
	spanCreditAmount.text('$'+creditamtStr);



    if (document.getElementById("holdBatches") != null)
    {
        if (holdCheckBox.checked == true)
        {
            if (frm['TotalNumberCredits'].value == "0" && frm['TotalNumberDebits'].value == "0")
                document.getElementById("holdBatches").checked = true;
        } else
        {
            if (frm['TotalNumberCredits'].value != "0" || frm['TotalNumberDebits'].value != "0")
                document.getElementById("holdBatches").checked = false;
        }
    }


}


function doSort(urlString)
{
    refreshACHBatchEntries(urlString);
	return false;
}
<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="View">
function refreshACHBatchEntries( urlString ){
    var urlStr = "/cb/pages/jsp/ach/achmultibatchaddedit.jsp";
    if (urlString != null)
        urlStr = urlString;
    $.ajax({
        url: urlStr,
        success: function(data){
            $('#viewACHMultiBatchDetailsDialogID').html(data);
        }
    });
}
</ffi:cinclude>

<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="View" operator="notEquals">
function refreshACHBatchEntries( urlString ){
    var urlStr = "/cb/pages/jsp/ach/achmultibatchaddedit.jsp";
    if (urlString != null)
        urlStr = urlString;
    $.ajax({
        url: urlStr,
        success: function(data){
            $('#inputDiv').html(data);
        }
    });
}
</ffi:cinclude>

function totalAmounts() {
	frm = document.AddBatchForm;
	var creditAmtTotal = 0.0;
	var debitAmtTotal = 0.0;
	tag = frm['TotalDebitAmount'];
    if (frm['CalculatedTotalDebits'] == null || frm['CalculatedTotalDebits'] == undefined || frm['CalculatedTotalDebits'] == 'undefined')
        return;
	var value = frm['CalculatedTotalDebits'].value;

	tag.value = value;

	tag = frm['TotalCreditAmount'];
	value = frm['CalculatedTotalCredits'].value;
	tag.value = value;

	tag = frm['TotalNumberCredits'];
	value = frm['CalculatedNumberCredits'].value;
	tag.value = value;

	tag = frm['TotalNumberDebits'];
	var value = frm['CalculatedNumberDebits'].value;
	tag.value = value;

	var item = document.getElementById('holdBatches');
    if (item != null)
    {
        var value = frm['CalculatedHoldBatches'].value;
        if (value == "true")
            item.checked = true;
        else item.checked = false;
    }

	return;
}


// --></script>
<s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />
<span id="PageHeading" style="display:none;"><%="" + session.getAttribute("PageHeading")%></span>

<s:form id="AddBatchFormId" namespace="/pages/jsp/ach" validate="false" action="editMultiACHTemplateAction_verify" method="post" name="AddBatchForm" theme="simple">
<%-- <s:include value="/pages/jsp/ach/inc/loadTemplate.jsp" /> --%>
<ffi:cinclude value1="true" value2="${loadMultipleBatch}" operator="equals">
<div class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header"><h2 id="loadTemplatePanelHeader"> <s:text name='jsp.default_264' /> </h2></div>
		<div class="leftPaneInnerBox leftPaneLoadPanel">
 <s:include value="/pages/jsp/ach/inc/loadAchMultipleBatchTemplate.jsp" />
 <ffi:cinclude value1="" value2="${AddEditMultiACHTemplate.TemplateName}" operator="notEquals">
  <div id="templateScopeLabel" class="marginTop20 floatleft">
		<span class="sectionsubhead sectionLabel"><!--L10NStart-->Template Scope<!--L10NEnd--> </span>:
		<strong><ffi:getProperty name="AddEditMultiACHTemplate" property="BatchScope"/></strong>
	</div>
 </ffi:cinclude>
		</div>
	</div>
</div>
</ffi:cinclude>
<ffi:cinclude value1="${loadMultipleBatch}" value2="true" operator="notEquals">
<div class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header"><h2 id="achSaveTemplateHeader"><s:text name="jsp.default_371" /></h2></div>
		<div class="leftPaneInnerBox leftPaneLoadPanel">
			<div class="inputBlockSection">
				<span id="templateNameLabel" class="sectionsubhead">
					<label for="templateName"><s:text name="jsp.default_416" /></label>
                                      <% if (!disabledControls && !fromTemplate) { %>
                                          <span class="required" title="required">*</span>
                                      <% } %></span>
                  <span class="columndata" >
					<input id="templateName" <%=disabledControls || fromTemplate ?"disabled":""%> type="text" class="ui-widget-content ui-corner-all" size="24" maxlength="32" border="0" style="width:191px" name="templateName" value="<ffi:getProperty name="AddEditMultiACHTemplate" property="TemplateName"/>">

				</span>

				<% SecureUser sUser=(SecureUser)session.getAttribute("SecureUser"); %>
				<%if(sUser.isConsumerUser()) {%>
				<% if (!isView) { %>
										<input type="hidden" name="batchScope" value="USER"/>
				<% } %>
				<% }else{ %>


				<span id="templateScopeLabel" class="sectionsubhead marginTop10">
					<label for="tempScopeID"><s:text name="jsp.default_418" /> </label>
	                                    <% if (!disabledControls && !fromTemplate) { %>
	                                        <span class="required" title="required">*</span>
	                                    <% } %>
				</span>
				<span id="templateScope" class="columndata">
	                                 <% if (isView) { %>
					<ffi:getProperty name="AddEditMultiACHTemplate" property="BatchScope"/>
	                                 <% } else { %>
					<select  id="tempScopeID" <%=disabledControls || fromTemplate?"disabled":""%> class="selectMenuClass" name="batchScope"  aria-labelledby="tempScopeID" aria-required="true">
						<option value="BUSINESS" <ffi:cinclude value1="BUSINESS" value2="${AddEditMultiACHTemplate.BatchScope}" operator="equals" >selected</ffi:cinclude>><s:text name="jsp.default_77"/></option>
						<option value="USER" <ffi:cinclude value1="USER" value2="${AddEditMultiACHTemplate.BatchScope}" operator="equals" >selected</ffi:cinclude>><s:text name="jsp.default_454"/></option>
					</select>

	                                 <% } %>
				</span>
				 <% } %>
				<span id="multiTemplateNameError"></span>
				<span id="templateScopeError"></span>
			</div>
		</div>
	</div>
</div>
</ffi:cinclude>
<div class="rightPaneWrapper w71">
 <div class="paneWrapper">
   	<div class="paneInnerWrapper">
   	<div class="payeViewDeleteInfo" style="display:none">
   		<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.default_413" /></div>
	<div class="blockContent">
		<div class="blockRow label130">
			<div class="inlineBlock" style="width: 50%">
				<span id="templateNameLabel" class="sectionLabel"><s:text name="ach.grid.templateName" />:</span>
                  <span class="" ><ffi:getProperty name="AddEditMultiACHTemplate" property="TemplateName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="templateScopeLabel" class="sectionLabel">
					<s:text name="jsp.default_418"/>
				</span>
				<span id="templateScope" class=""><ffi:getProperty name="AddEditMultiACHTemplate" property="BatchScope"/></span>
			</div>
		</div>
	</div>
</div>
   	</div>
   	<ffi:cinclude value1="true" value2="${loadMultipleBatch}" operator="equals">
	</ffi:cinclude>
	<ffi:cinclude value1="${loadMultipleBatch}" value2="true" operator="notEquals">
		<%-- <div class="header"><s:text name="ach.newMultipleBatch.fromTemplate"/></div> --%>
	</ffi:cinclude>
		<div align="center" id="achMultipleBatch">
			<div align="center">
				<div align="left" >
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData">
						<tr>
							<td class="columndata lightBackground">
                    		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">
									<%-- <tr>
										<td class="sectionhead lightBackground" colspan="7"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="692" height="1" border="0"></td>
									</tr>
									<tr valign="top">
										<td class="sectionhead lightBackground" colspan="7"><span class="sectionhead">&gt; <%=title%><br>
										</span></td>
									</tr> --%>

<%-- <% if (isDelete) { %>
									<tr>
										<td class="sectionhead lightBackground" colspan="7" align="center"><span class="sectionhead"><!--L10NStart-->Are you sure you want to delete this Multiple ACH Batch Template?<!--L10NEnd-->
											</span>
										</td>
									</tr>
<% } %> --%>

<%-- Create a new table with two columns, the first column will be the left side,
the second column will be the right side (repeating and Offset Account) --%>
<%-- <ffi:cinclude value1="${loadMultipleBatch}" value2="true" operator="notEquals">
                                    <tr class="lightBackground">
                                        <td colspan="7">
                                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                        <td>
                                        <table border="0" cellspacing="0" cellpadding="3" width="100%">

											<tr>
												<td id="templateNameLabel" width="50%">
													<span class="sectionsubhead"><!--L10NStart-->Template Name<!--L10NEnd--> </span>
		                                            <% if (!disabledControls && !fromTemplate) { %>
		                                                <span class="required">*</span>
		                                            <% } %>
												</td>
												<td id="templateScopeLabel"  width="50%">
													<span class="sectionsubhead"><!--L10NStart-->Template Scope<!--L10NEnd--> </span>
		                                           <% if (!disabledControls && !fromTemplate) { %>
		                                               <span class="required">*</span>
		                                           <% } %>
												</td>
											</tr>
											<tr>
												<td class="columndata" >
													<input id="templateName" <%=disabledControls || fromTemplate ?"disabled":""%> type="text" class="ui-widget-content ui-corner-all" size="24" maxlength="32" border="0" style="width:191px" name="templateName" value="<ffi:getProperty name="AddEditMultiACHTemplate" property="TemplateName"/>">

												</td>
												<td id="templateScope" class="columndata">
		                                        <% if (isView) { %>
													<ffi:getProperty name="AddEditMultiACHTemplate" property="BatchScope"/>
		                                        <% } else { %>
													<select  id="tempScopeID" <%=disabledControls || fromTemplate?"disabled":""%> class="selectMenuClass" name="batchScope">
														<option value="BUSINESS" <ffi:cinclude value1="BUSINESS" value2="${AddEditMultiACHTemplate.BatchScope}" operator="equals" >selected</ffi:cinclude>><!--L10NStart-->Business (General Use)<!--L10NEnd--></option>
														<option value="USER" <ffi:cinclude value1="USER" value2="${AddEditMultiACHTemplate.BatchScope}" operator="equals" >selected</ffi:cinclude>><!--L10NStart-->User (My Use)<!--L10NEnd--></option>
													</select>

		                                        <% } %>
												</td>
											</tr>
											<tr>
												<td><span id="multiTemplateNameError"></span></td>
												<td><span id="templateScopeError"></span></td>
											</tr>
                                		</table>
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>
</ffi:cinclude> --%>
                                <tr>
                                	<td><span id="formError"></span></td>
                                </tr>

<%-- end of table --%>

                               <%--  <% if (!disabledControls && !fromTemplate) { %>
                                    <tr>
                                        <td colspan="4"><div align="center"><span class="required">* <!--L10NStart-->indicates a required field<!--L10NEnd--></span></div></td>
										<td align="right" colspan='3'>
                                            &nbsp;
										</td>
                                    </tr>
                                <% } %>--%>
								<ffi:cinclude operator="equals" value1="true" value2="${AddEditMultiACHTemplate.BatchValidationErrors}">
									<tr>
										<td class="lightBackground" colspan="7" height="20"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="1" border="0"></td>
									</tr>
 									<tr>
										<td class="lightBackground mainfontbold errortext" colspan="7">(<s:text name="jsp.ach.ACH.batch.errorMsg"/>)</td>
									</tr>
 									<tr>
										<td class="lightBackground mainfont" colspan="7"><s:text name="jsp.ach.error.msg1" /></td>
									</tr>
								</ffi:cinclude>
									<!-- <tr class="lightBackground">
										<td></td>
										<td height="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="157" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="87" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
									</tr> -->
									<ffi:cinclude value1="${loadMultipleBatch}" value2="true" operator="notEquals">
									<tr>
										<td colspan="7">
			<% if (!fromTemplate && !disabledControls) { %>
										<div class="acntDashboard_itemHolder marginBottom20">
											<span class="sectionsubhead"><label for="newBatchTypeId"><s:text name="jsp.ach.Add.New.Batch.Type" /></label></span>
											<span class="columndata">
                                                <select  class="selectMenuClass" name="NewBatchType" id="newBatchTypeId" aria-labelledby="standardEntryClassCodeId">
			<%
				String entitlementACHName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( com.ffusion.csil.core.common.EntitlementsDefines.ACH_BATCH, com.ffusion.csil.core.common.EntitlementsDefines.ACH_TEMPLATE );
				String entitlementTAXName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( com.ffusion.csil.core.common.EntitlementsDefines.TAX_PAYMENTS, com.ffusion.csil.core.common.EntitlementsDefines.ACH_TEMPLATE );
				String entitlementChildName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( com.ffusion.csil.core.common.EntitlementsDefines.CHILD_SUPPORT, com.ffusion.csil.core.common.EntitlementsDefines.ACH_TEMPLATE );
			%>
			    <ffi:cinclude ifEntitled="<%= entitlementACHName %>" checkParent="true">
                                                    <option value="AddBatch" ><s:text name="jsp.default_23" /></option>
                </ffi:cinclude>

												<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CCD_TXP %>">
			    <ffi:cinclude ifEntitled="<%= entitlementTAXName %>" checkParent="true">
                                                    <option value="AddTaxPaymentBatch"
                                                            <ffi:cinclude value1="${subMenuSelected}" value2="tax" >
                                                                selected
                                                            </ffi:cinclude>
                                                            ><s:text name="jsp.ach.Tax.Payment.Batch" /></option>
                </ffi:cinclude>
                                                </ffi:cinclude>
												<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CCD_DED %>">
			    <ffi:cinclude ifEntitled="<%= entitlementChildName %>" checkParent="true">
                                                    <option value="AddChildSupportBatch"
                                                            <ffi:cinclude value1="${subMenuSelected}" value2="child" >
                                                                selected
                                                            </ffi:cinclude>
                                                            ><s:text name="jsp.ach.Child.Support.Batch" /></option>
                </ffi:cinclude>
                                                </ffi:cinclude>
                                                </select>
                                            </span>
											</div>
											<% SecureUser sUser=(SecureUser)session.getAttribute("SecureUser"); %>
											<%if(!sUser.isConsumerUser()) {%>
												<sj:a
														id="importBatchesSubmitId"
						                                button="true"
						                                onClick="importBatches();"
						                                cssClass="floatleft;"
								                        ><s:text name="jsp.ach.Import.Batches" />
						                        </sj:a>
						                        <%} %>
												<sj:a
														id="addBatchSubmitId"
						                                button="true"
						                                onClickTopics="clickAddNewBatchTopic"
								                        ><s:text name="jsp.ach.Add.New.Batch" />
						                        </sj:a>
<% } %>
										</td>
									</tr>
									</ffi:cinclude>
								<%-- 	<tr>

		<td colspan="7">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		    <td class="sectionhead nameSubTitle" colspan="4"><!--L10NStart-->Batches<!--L10NEnd-->
		 </tr>
		 <tr>
		 <td valign="top">
			<% if (!disabledControls) { %>
						<span for="checkbox"><!--L10NStart-->Hold Batches<!--L10NEnd--></span>sss
						<input id="holdBatches" type="checkbox" name="checkbox" onclick="holdAllBatches(this);" >
			<% } %>
	     </td>

			<td id="viewAchMultipleBatchTemplateTotalNumberCreditLabel" class="sectionsubhead" valign="center">
				<span class="marginRight5"><!--L10NStart-->Total # Credits<!--L10NEnd--></span>
				<input id="viewAchMultipleBatchTemplateTotaltalNumberCredit" type="text" readonly name="TotalNumberCredits" maxlength="6" size="8" value="" class="ui-widget-content ui-corner-all" style="margin-bottom: 5px;">
			</td>
			<td id="viewAchMultipleBatchTemplateTotalNumberDebitLabel" class="sectionsubhead">
				<span class="marginRight5"><!--L10NStart-->Total # Debits<!--L10NEnd--></span>
				<input id="viewAchMultipleBatchTemplateTotalNumberDebit" type="text" readonly name="TotalNumberDebits" maxlength="6" size="8" value="" class="ui-widget-content ui-corner-all" style="margin-bottom: 5px;">
			</td>
			<td id="viewAchMultipleBatchTemplateTotalCreditsLabel" class="sectionsubhead">
				<span class="marginRight5"><!--L10NStart-->Total Credits<!--L10NEnd--> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)</span>
				<input id="viewAchMultipleBatchTemplateTotalCredits" type="text" readonly name="TotalCreditAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all">
			</td>
			<td id="viewAchMultipleBatchTemplateTotalDebitLabel" class="sectionsubhead" >
				<span class="marginRight5"><!--L10NStart-->Total Debits<!--L10NEnd--> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)</span>
				<input id="viewAchMultipleBatchTemplatetotalDebit" type="text" readonly name="TotalDebitAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all">
			</td>
		</tr>

		</table>
		</td>
		</tr> --%>
		</table>

<s:if test="%{#session.AddEditMultiACHTemplate.ACHBatches.size>0 || !#parameters.loadMultipleBatch}">
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="1" class="tableData">
		<tr class="header">
			<td id="viewAchMultipleBatchTemplateHoldLabel" class="sectionsubhead" width="10%">
			<% if (!disabledControls) { %>
						<input id="holdBatches" type="checkbox" name="checkbox" onclick="holdAllBatches(this);" >
			<% } %>
			<s:text name="jsp.default_228" />
			<br>
			<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
			<ffi:setProperty name="SortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="Sort" width="24" height="8" border="0">'/>
			<ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="Reverse Sort" width="24" height="8" border="0">'/>
			<ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="Sort" width="24" height="8" border="0">'/>
			<ffi:setProperty name="SortImage" property="Collection" value="AddEditMultiACHTemplate.ACHBatches"/>
			<ffi:process name="SortImage"/>
			<ffi:setProperty name="SortImage" property="Compare" value="ACTIVE"/>
			<script>ns.ach.doSortAchMultipleEntriesActiveUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achmultibatchaddedit.jsp?ToggleSortedBy=ACTIVE"/>';</script>
			<a onclick="doSort(ns.ach.doSortAchMultipleEntriesActiveUrl);"><ffi:getProperty name="SortImage" encode="false"/></a></td>
			<td id="viewAchMultipleBatchTemplateNameLabel" class="sectionsubhead" width="11%"><s:text name="jsp.ach.SEC.Code" /><br>
	        <s:text name="ach.grid.templateName" /><br>
			<ffi:setProperty name="SortImage" property="Compare" value="STANDARDENTRYCLASSCODE,TEMPLATENAME"/>
			<script>ns.ach.doSortAchMultipleEntriesSTANDARDTEMPLATENAMEUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achmultibatchaddedit.jsp?ToggleSortedBy=STANDARDENTRYCLASSCODE,TEMPLATENAME"/>';</script>
			<a onclick="doSort(ns.ach.doSortAchMultipleEntriesSTANDARDTEMPLATENAMEUrl);"><ffi:getProperty name="SortImage" encode="false"/></a></td>

			<td id="viewAchMultipleBatchTemplateCompanyDescriptionLabel" class="sectionsubhead" width="15%"><s:text name="jsp.default_106" /> <br> <s:text name="jsp.default_170" /><br>
	        <s:text name="jsp.default_24" /><br>
			<ffi:setProperty name="SortImage" property="Compare" value="COMPANYDESC"/>
			<script>ns.ach.doSortAchMultipleEntriesCOMPANYDESCUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achmultibatchaddedit.jsp?ToggleSortedBy=COMPANYDESC"/>';</script>
			<a onclick="doSort(ns.ach.doSortAchMultipleEntriesCOMPANYDESCUrl);"><ffi:getProperty name="SortImage" encode="false"/></a></td>

			<td id="viewAchMultipleBatchTemplateTotalNumCreditsLabel" class="sectionsubhead" width="10%"><s:text name="jsp.default_513" /><br>
			<ffi:setProperty name="SortImage" property="Compare" value="TOTALCREDITS"/>
			<script>ns.ach.doSortAchMultipleEntriesTOTALCREDITSUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achmultibatchaddedit.jsp?ToggleSortedBy=TOTALCREDITS"/>';</script>
			<a onclick="doSort(ns.ach.doSortAchMultipleEntriesTOTALCREDITSUrl);"><ffi:getProperty name="SortImage" encode="false"/></a></td>

			<td id="viewAchMultipleBatchTemplateTotalCreditUSDLabel" class="sectionsubhead" width="13%"><s:text name="jsp.ach.Total.Credits.USD" /><br>
			<ffi:setProperty name="SortImage" property="Compare" value="TOTAL_CREDIT"/>
			<script>ns.ach.doSortAchMultipleEntriesTOTAL_CREDITUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achmultibatchaddedit.jsp?ToggleSortedBy=TOTAL_CREDIT"/>';</script>
			<a onclick="doSort(ns.ach.doSortAchMultipleEntriesTOTAL_CREDITUrl);"><ffi:getProperty name="SortImage" encode="false"/></a></td>

			<td id="viewAchMultipleBatchTemplateTotalNumDebitLabel" class="sectionsubhead" width="10%"><s:text name="jsp.default_514" /><br>
			<ffi:setProperty name="SortImage" property="Compare" value="TOTALDEBITS"/>
			<script>ns.ach.doSortAchMultipleEntriesTOTALDEBITSUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achmultibatchaddedit.jsp?ToggleSortedBy=TOTALDEBITS"/>';</script>
			<a onclick="doSort(ns.ach.doSortAchMultipleEntriesTOTALDEBITSUrl);"><ffi:getProperty name="SortImage" encode="false"/></a></td>

			<td id="viewAchMultipleBatchTemplateTotalDebitUSDLabel" class="sectionsubhead" width="10%"><s:text name="jsp.ach.Total.Debits.USD" /><br>
			<ffi:setProperty name="SortImage" property="Compare" value="TOTAL_DEBIT"/>
			<script>ns.ach.doSortAchMultipleEntriesTOTAL_DEBITUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achmultibatchaddedit.jsp?ToggleSortedBy=TOTAL_DEBIT"/>';</script>
			<a onclick="doSort(ns.ach.doSortAchMultipleEntriesTOTAL_DEBITUrl);"><ffi:getProperty name="SortImage" encode="false"/></a></td>

			<td id="viewAchMultipleBatchTemplateTotalEntries" class="sectionsubhead" width="4%"><s:text name="jsp.ach.Total.Entries" /><br>
			<ffi:setProperty name="SortImage" property="Compare" value="NUMBERENTRIES"/>
			<script>ns.ach.doSortAchMultipleEntriesNUMBERENTRIESUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achmultibatchaddedit.jsp?ToggleSortedBy=NUMBERENTRIES"/>';</script>
			<a onclick="doSort(ns.ach.doSortAchMultipleEntriesNUMBERENTRIESUrl);"><ffi:getProperty name="SortImage" encode="false"/></a></td>
			<% if (fromTemplate) { %>
	            <td width="15%" class="sectionsubhead dvcDateTd">&nbsp;<s:text name="ach.grid.effectiveDate" /><span class="">*</span></td>
	   		 <% } %>
			<td  class="sectionsubhead" align="center"><s:text name="jsp.default_27" /></td>
		</tr>
		<%
			Double totalCredits = 0.0;
			Double totalDebits = 0.0;
			int numberCredits = 0;
			int numberDebits = 0;
	        boolean holdBatches = true;
		%>
        <%--Getting the Individual Transfer object by iterating over the transfers  --%>
        <s:set var="count" value="0" />
        <ffi:list collection="AddEditMultiACHTemplate.ACHBatches" items="ACHBatch">
		<% boolean isActive = false;
		ACHBatch batch = (ACHBatch)request.getAttribute("ACHBatch");

		%>
	    <ffi:cinclude value1="${ACHBatch.Active}" value2="true" >
			<% isActive = true; holdBatches = false;%>
	    </ffi:cinclude>
		<%
        String totals = "" + batch.getTotalNumberCredits() + "_" + batch.getTotalNumberDebits() +"_" + batch.getTotalCreditAmountValue().getCurrencyStringNoSymbolNoComma() + "_" + batch.getTotalDebitAmountValue().getCurrencyStringNoSymbolNoComma();
		String ID = batch.getID();
        if (isActive)
        {

            if (batch.getTotalDebitAmountValue() != null)
                totalDebits += Double.parseDouble(batch.getTotalDebitAmountValue().getCurrencyStringNoSymbolNoComma());
            numberDebits += batch.getTotalNumberDebits();
            if (batch.getTotalCreditAmountValue() != null)
                totalCredits += Double.parseDouble(batch.getTotalCreditAmountValue().getCurrencyStringNoSymbolNoComma());
            numberCredits += batch.getTotalNumberCredits();
        }
		%>
	<tr class="columndata">
		    <td align="left">
                <% String ACHBatchActive = "";  %>
                <ffi:getProperty name="ACHBatch" property="Active" assignTo="ACHBatchActive" />
                <% session.setAttribute("ACHBatchActive", ACHBatchActive); %>

                <input id="hold<s:property value="#count" />" type="checkbox" <%=disabledControls?"disabled":""%> name="checkbox" onclick="changeHold(this,'<s:property value="#count" />');" <ffi:cinclude value1="${ACHBatch.Active}" value2="false">checked</ffi:cinclude>>
                <input id="active<s:property value="#count" />" type="hidden" <%=disabledControls?"disabled":""%> name="ACHBatches[<s:property value="#count" />].Active" value="<s:property value="#session.ACHBatchActive" />" >

            <input type="hidden" id="totals<s:property value="#count" />" name="totals<s:property value="#count" />" value='<%=totals %>'>
			</td>
			<td id="viewAchMultipleBatchTemplateTemplateName<s:property value="#count" />">
                <ffi:getProperty name="ACHBatch" property="StandardEntryClassCode"/><br>
                <ffi:getProperty name="ACHBatch" property="TemplateName"/>
            </td>
			<td id="viewAchMultipleBatchTemplateCompanyDescipation<s:property value="#count" />">
                <ffi:getProperty name="ACHBatch" property="CoEntryDesc"/><br>
                <ffi:getProperty name="ACHBatch" property="CoName"/>/ <ffi:getProperty name="ACHBatch" property="CompanyID"/>
            </td>
			<td id="viewAchMultipleBatchTemplateTotalNumberCredit<s:property value="#count" />">
				<ffi:getProperty name="ACHBatch" property="TotalNumberCredits"/>
			</td>
			<td id="viewAchMultipleBatchTemplateTotalAmount<s:property value="#count" />">
				<ffi:getProperty name="ACHBatch" property="TotalCreditAmount"/>
			</td>
			<td id="viewAchMultipleBatchTemplateTotalNumberDebits<s:property value="#count" />">
				<ffi:getProperty name="ACHBatch" property="TotalNumberDebits"/>
			</td>
			<td id="viewAchMultipleBatchTemplateTotalDebitAmount<s:property value="#count" />">
				<ffi:getProperty name="ACHBatch" property="TotalDebitAmount" />
            </td>
            <td id="viewAchMultipleBatchTemplatetotalEntry<s:property value="#count" />">
                <ffi:getProperty name="ACHBatch" property="ACHEntries.Size" />
            </td>
<%-- tabindex='1' added so amounts are all first to tab to --%>
			<% if (fromTemplate) { %>
	            <td class="columndata">
	                <!-- ffi:setProperty name="ACHBatchDate"  value="${ACHBatch.Date }"/-->
	                <% String ACHBatchDate = "";  %>
	                <ffi:getProperty name="ACHBatch" property="Date" assignTo="ACHBatchDate" />
	                <% session.setAttribute("ACHBatchDate", ACHBatchDate); %>

	                <sj:datepicker
	                	value="%{#session.ACHBatchDate}"
	                	id="%{'Date'+#count}"
						name="%{'ACHBatches['+#count+'].Date'}"
						label="Date"
						maxlength="10"
						cssStyle="width:70px"
						buttonImageOnly="true"
						cssClass="ui-widget-content ui-corner-all" />

	                <script>
	                    var calTarget = 'ACHBatches[<s:property value='#count'/>].Date';        // the default for Tax and Child Support
	                    var secCode = '<ffi:getProperty name="ACHBatch" property="StandardEntryClassCode"/>';        // the default for Tax and Child Support
	                    var debitBatch = '<ffi:getProperty name="ACHBatch" property="DebitBatch"/>';
	                    <%--var urlStr = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=9&calDisplay=select&calForm=AddBatchForm&fromACHorTax=true&ACHCompanyID=${ACHBatch.CoID}"/>';--%>
	                    var urlStr = '<ffi:getProperty name="FullPagesPath" />calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=9&calDisplay=select&calForm=AddBatchForm&fromACHorTax=true&ACHCompanyID=<ffi:getProperty name="ACHBatch" property="CoID"/>';
	                    ns.common.enableAjaxDatepicker("Date<s:property value='#count' />", urlStr + '&SECCode=' + secCode + debitBatch );//+ '&calTarget=' + calTarget);
	                </script>
	                <ffi:removeProperty name="calTargetValue"/>
	            </td>
	      <% } %>
		<td align="center">

            <ffi:getProperty name="ACHBatch" property="SECEntitlementName" assignTo="batchSECCode" />
			<% if (!fromTemplate) {
			    tempEntToCheck = "" + com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( batchSECCode, EntitlementsDefines.ACH_TEMPLATE );
			 } else {
			            tempEntToCheck = "" + com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( batchSECCode, EntitlementsDefines.ACH_PAYMENT_ENTRY );
			 } %>
		<% if (isDelete) { %>
            <ffi:cinclude ifNotEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHBatch.CompanyID}" checkParent="true">
                <% canDelete = false; %>
            </ffi:cinclude>
        <% } %>
		<% if (!disabledControls) { %>
            <%-- <ffi:cinclude ifEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHBatch.CompanyID}" checkParent="true"> --%>
                <a class="ui-button" onclick="ns.ach.editACHMultiTemplateBatch('<ffi:urlEncrypt url="/cb/pages/jsp/ach/editMultiACHTemplateAction_editbatch.action?EditBatchID=${ACHBatch.ID}"/>');"><span class="sapUiIconCls icon-wrench"></span></a>
                <% if (!fromTemplate) { %>
                    <a class="ui-button" onclick="ns.ach.deleteACHMultiTemplateBatch('<ffi:urlEncrypt url="/cb/pages/jsp/ach/editMultiACHTemplateAction_deletebatch.action?DeleteBatchID=${ACHBatch.ID}"/>');"><span class="sapUiIconCls icon-delete"></span></a>
                <% } %>
            <%-- </ffi:cinclude> --%>
            <%-- <ffi:cinclude ifNotEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHBatch.CompanyID}" checkParent="true">
                <span class="sapUiIconCls icon-wrench"></span>
                <% if (!fromTemplate) { %>
                    <span class="ui-icon ui-icon-trash"></span>
                <% } %>
            </ffi:cinclude> --%>

		<% } else { %>
			&nbsp;
		<% } %>
	</td>
	</tr>
	<tr><td colspan="10" align="center"><span name="Batch_<s:property value="#count" />_date"></span></td></tr>
   <%--  <% if (fromTemplate) { %>
        <tr class="lightBackground">
            <td align="right" colspan='2'>
                <span class="sectionsubhead"><!--L10NStart-->Effective Date<!--L10NEnd--></span><span class="required">*</span>
            </td>
            <td class="columndata" colspan='3'>
                <!-- ffi:setProperty name="ACHBatchDate"  value="${ACHBatch.Date }"/-->
                <% String ACHBatchDate = "";  %>
                <ffi:getProperty name="ACHBatch" property="Date" assignTo="ACHBatchDate" />
                <% session.setAttribute("ACHBatchDate", ACHBatchDate); %>

                <sj:datepicker
                	value="%{#session.ACHBatchDate}"
                	id="%{'Date'+#count}"
					name="%{'ACHBatches['+#count+'].Date'}"
					label="Date"
					maxlength="10"
					buttonImageOnly="true"
					cssClass="ui-widget-content ui-corner-all" />

                <script>
                    var calTarget = 'ACHBatches[<s:property value='#count'/>].Date';        // the default for Tax and Child Support
                    var secCode = '<ffi:getProperty name="ACHBatch" property="StandardEntryClassCode"/>';        // the default for Tax and Child Support
                    var debitBatch = '<ffi:getProperty name="ACHBatch" property="DebitBatch"/>';
                    var urlStr = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=9&calDisplay=select&calForm=AddBatchForm&fromACHorTax=true&ACHCompanyID=${ACHBatch.CoID}"/>';
                    var urlStr = '<ffi:getProperty name="FullPagesPath" />calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=9&calDisplay=select&calForm=AddBatchForm&fromACHorTax=true&ACHCompanyID=<ffi:getProperty name="ACHBatch" property="CoID"/>';
                    ns.common.enableAjaxDatepicker("Date<s:property value='#count' />", urlStr + '&SECCode=' + secCode + debitBatch );//+ '&calTarget=' + calTarget);
                </script>
                <ffi:removeProperty name="calTargetValue"/>
            </td>

        </tr>
        <tr>
        	<td align="right" colspan='2'><span id="Batch_<s:property value="#count" />_Entry_Error" class="dateError"></td>
        	<td class="columndata" colspan='3'><span id="Batch_<s:property value="#count" />_dateError" class="dateError"></span></td>
        </tr>
    <% } %> --%>

	<% if (showValidationErrors) { %>
	<ffi:cinclude operator="equals" value1="${ACHBatch.hasValidationErrors}" value2="true">
		<% String lineContent = ""; String lineNumber = ""; String recordNumber = ""; %>
		<ffi:list collection="ACHBatch.validationErrors" items="validationError">
			<ffi:getProperty name="ACHBatch" property="lineContent" assignTo="lineContent"/>
			<ffi:getProperty name="ACHBatch" property="lineNumber" assignTo="lineNumber"/>
			<ffi:getProperty name="ACHBatch" property="recordNumber" assignTo="recordNumber"/>
		<tr>
			<td colspan="10" class="columndata_error columndata_bold">&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="validationError" property="title" encode="false"/></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="10" class="columndata_error">
			<% if (lineNumber != null && recordNumber != null) { %>
      	<span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-1" parm0="${ACHBatch.lineNumber}" parm1="${ACHBatch.recordNumber}"/>:</span>
			<% } %>
			<% if (lineNumber != null && recordNumber == null) { %>
        <span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-2" parm0="${ACHBatch.lineNumber}"/>:</span>
			<% } %>
			<% if (lineNumber == null && recordNumber != null) { %>
        <span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-3" parm0="${ACHBatch.recordNumber}"/>:</span>
			<% } %>
        <ffi:getProperty name="validationError" property="message" encode="false" />
			</td>
		</tr>
			<% if (lineContent != null) { %>
			<tr>
				<td></td>
				<td colspan="10" class="columndata_error">
					<span class="columndata_bold"><!--L10NStart-->File data<!--L10NEnd-->:</span>
					<%= lineContent %>
				</td>
			</tr>
			<tr>
				<% if (fromTemplate) { %>
		        	<td align="right" colspan='5'><span id="Batch_<s:property value="#count" />_Entry_Error" class="dateError"></td>
		        	<td class="columndata" colspan='5'><span id="Batch_<s:property value="#count" />_dateError" class="dateError"></span></td>
		       <% } %>
			</tr>
			<% } %>
		</ffi:list>
	</ffi:cinclude>
	<% } %>
	 <tr class=""><td colspan="10">
		<span name="Batch_<s:property value="#count" />_Entry_"></span></td></tr>
            <s:set var="count" value="%{#count+1}" />
        </ffi:list>
        <!-- <tr><td valign="top" colspan="10"><hr style="display: block;" class="thingrayline"></td></tr> -->
        <tr class="totalAmountRow">
        	<td colspan="3">Total:</td>
        	<td id="viewAchMultipleBatchTemplateTotalNumberCreditLabel" class="sectionsubhead" valign="center">
				<input id="viewAchMultipleBatchTemplateTotaltalNumberCredit" type="hidden" readonly name="TotalNumberCredits" maxlength="6" size="8" value="" class="ui-widget-content ui-corner-all">
				<span id="TotalNumberCreditsVal">
					<%="" + numberCredits%>
				</span>
			</td>
			<%
				SecureUser suser = (SecureUser) session.getAttribute("SecureUser");
				java.util.Locale userLocale = suser.getLocale();
			%>
			<td id="viewAchMultipleBatchTemplateTotalCreditsLabel" class="sectionsubhead">
				<input id="viewAchMultipleBatchTemplateTotalCredits" type="hidden" readonly name="TotalCreditAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all">
				<span id="TotalCreditAmountVal">
				<%Currency totalCreditsCurrency=new Currency(totalCredits.toString(),userLocale); %>
				<%=totalCreditsCurrency.toString()%>
				</span>
			</td>
			<td id="viewAchMultipleBatchTemplateTotalNumberDebitLabel" class="sectionsubhead">
				<input id="viewAchMultipleBatchTemplateTotalNumberDebit" type="hidden" readonly name="TotalNumberDebits" maxlength="6" size="8" value="" class="ui-widget-content ui-corner-all">
				<span id="TotalNumberDebitsVal">
					<%="" + numberDebits%>
				</span>
			</td>
			<td id="viewAchMultipleBatchTemplateTotalDebitLabel" class="sectionsubhead" >
				<input id="viewAchMultipleBatchTemplatetotalDebit" type="hidden" readonly name="TotalDebitAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all">
				<span id="TotalDebitAmountVal">
				<%Currency totalDebitsCurrency=new Currency(totalDebits.toString(),userLocale); %>
				<%=totalDebitsCurrency.toString()%>
				</span>
			</td>
			<td colspan="3"></td>
        </tr>
	<input type="hidden" name="CalculatedTotalDebits" value="<%="" + totalDebits%>" >
	<input type="hidden" name="CalculatedTotalCredits" value="<%="" + totalCredits%>" >
	<input type="hidden" name="CalculatedNumberDebits" value="<%="" + numberDebits%>" >
	<input type="hidden" name="CalculatedNumberCredits" value="<%="" + numberCredits%>" >
    <input type="hidden" name="CalculatedHoldBatches" value="<%="" + holdBatches%>">
</table>
	</div>
</div>
</s:if>
<s:else>
	<div class="marginleft10">Please select template.</div>
</s:else>


 <% if (!disabledControls && !fromTemplate) { %>
<div align="center" class="marginTop10"><span class="required">* <!--L10NStart-->indicates a required field<!--L10NEnd--></span></div>
<% } %>
<% if (isDelete) { %>
<div class="sectionhead marginTop10" style="color:red" align="center"><!--L10NStart-->Are you sure you want to delete this Multiple ACH Batch Template?<!--L10NEnd--></div>
<% } %>


                 <%--
             <%  String cancelGoesWhere;
                 if (isTemplate || goToTemplatePage) {
                     cancelGoesWhere = "payments/achtemplates.jsp";
                 } else {
                     cancelGoesWhere = "payments/achpayments.jsp";
                 }
             %>
             --%>

<% if (isView) {    // QTS 552058 - CANCEL should be DONE for VIEW %>
<%--
<input class="submitbutton" type="button" value="DONE" onclick="document.location='<ffi:getProperty name="SecurePath"/><%= cancelGoesWhere %>';return false;">
--%>
<div class="ffivisible" style="height:100px;">&nbsp;</div>
<div align="center" class="ui-widget-header customDialogFooter">
<sj:a
     id="doneAchBatch"
             button="true"
	onClickTopics="closeDialog"
     	>
             <s:text name="jsp.default_175"/>
     	</sj:a>
<% } else { %>
<div class="btn-row">
<s:if test="%{AddEditMultiACHTemplate.ACHBatches.size>0 || !#parameters.loadMultipleBatch}">
	<ffi:cinclude operator="equals" value1="true" value2="<%= String.valueOf( fromTemplate ) %>">

		<sj:a
		button="true"
		summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHForm"
	    ><s:text name="jsp.default_82"/></sj:a>
	</ffi:cinclude>
	<ffi:cinclude operator="notEquals" value1="true" value2="<%= String.valueOf( fromTemplate ) %>">

		<sj:a
		button="true"
		summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHMultipleTemplateForm"
	    ><s:text name="jsp.default_82"/></sj:a>
	</ffi:cinclude>
</s:if>
<s:else>

	<sj:a
	button="true"
	summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHForm"
    ><s:text name="jsp.default_82"/></sj:a>
</s:else>
<% } %>

                     &nbsp;
<% if (!isView && !isDelete) { %>
<ffi:cinclude operator="equals" value1="false" value2="${AddEditMultiACHTemplate.BatchValidationErrors}">
	<ffi:cinclude operator="equals" value1="true" value2="<%= String.valueOf( isTemplate ) %>">
		<%--
		<input class="submitbutton" type="button" value="SAVE MULTI BATCH TEMPLATE" onClick="document.forms['AddBatchForm'].action='<ffi:getProperty name="SecurePath"/>payments/achmultitemplateaddeditsave.jsp'; document.forms['AddBatchForm'].submit();return false;">
		--%>
						<sj:a
							id="saveMultiBatchTemplateSubmitId"
							formIds="AddBatchFormId"
                               targets="resultmessage"
                               button="true"
                               validate="true"
                               validateFunction="customValidation"
                               onBeforeTopics="beforeVerifyACHForm"
                               onCompleteTopics="completeSaveMultiTempForm"
                               onSuccessTopics="successSaveMultiTempForm"
							   onErrorTopics="achmultibatchCustomValidation"
	                        ><s:text name="jsp.default_366" />
                       	</sj:a>
	</ffi:cinclude>
	<ffi:cinclude operator="notEquals" value1="true" value2="<%= String.valueOf( isTemplate ) %>">
		<input type="hidden" name="verifyFromMultiTemplate" value="true"/>
		<%--
		<input class="submitbutton" type="button" value="CONFIRM BATCHES FOR PAYMENT" onClick="document.forms['AddBatchForm'].action='<ffi:getProperty name="SecurePath"/>payments/achmultibatchaddeditconfirm.jsp'; document.forms['AddBatchForm'].submit();return false;">
		--%>
						<sj:a
							id="confirmBatchesSubmitId"
							formIds="AddBatchFormId"
                               targets="verifyDiv"
                               button="true"
                               validate="true"
                               validateFunction="customValidation"
                               onCompleteTopics="completeConfirmBatchesForm"
	                        ><s:text name="jsp.ach.Confirm.Batches.For.Payment" />
                       	</sj:a>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude operator="equals" value1="true" value2="${AddEditMultiACHTemplate.BatchValidationErrors}">
                         <input class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'  type="hidden" value="RECHECK VALIDATION" onClick="ns.ach.recheckValidatonMultiTemplate();">
                         					<sj:a
							id="recheckBatchesSubmitId"
							formIds="AddBatchFormId"
                               targets="inputDiv"
                               button="true"
                               validate="true"
                               validateFunction="customValidation"
                               onClickTopics="clickCheckBatchesFormTopic"
	                        ><s:text name="jsp.ach.Recheck.Validation" />
                       	</sj:a>
</ffi:cinclude>
<%  } %>
<% if (isDelete) { %>
                     <ffi:cinclude operator="equals" value1="true" value2="<%= String.valueOf( isTemplate ) %>">
                         <input type="hidden" id="deleteMultiBatchTemplate" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= canDelete?"":"ui-state-disabled" %>' <%=canDelete?"":" disabled "%> type="button" value="Delete Template" onClick="ns.ach.deleteMultiBatchTemplate();">
                         <sj:a
                             id="deleteMultiBatchTemplateId"
                             button="true"
                             onClick="ns.ach.deleteMultiBatchTemplate();"
                             ><s:text name="jsp.default_162" />
                         </sj:a>
                     </ffi:cinclude>
<%  } %>
</div>

							<td></td>
						</tr>
					</table>
					<br>
				</div>
			</div>
		</div>
		</div></div></div>
</s:form>
<%-- This div will be used to show import Batches section. --%>
<div id="achBatchImportDiv" ></div>
<script type="text/javascript">
<!--
		totalAmounts();

		$("#tempScopeID").selectmenu({width: "90%"});
		$("#newBatchTypeId").selectmenu({width: 180});

//-->

</script>

<% if (fromTemplate) { %>
<script type="text/javascript">
<!--
    ns.ach.setInputTabText('#templateInputTab');
    ns.ach.setVerifyTabText('#templateVerifyTab');
    ns.ach.setConfirmTabText('#templateConfirmTab');
    ns.ach.showInputDivSteps();
//-->
</script>
<% } else
 if (isTemplate) { %>
<script type="text/javascript">
<!--
    ns.ach.setInputTabText('#templateInputTab');
    ns.ach.setVerifyTabText('');
    ns.ach.setConfirmTabText('');
    ns.ach.showInputDivSteps();
//-->
</script>
<% } %>


<ffi:removeProperty name="ModifyACHEntry" />
<ffi:removeProperty name="ACHEntryPayee" />
<ffi:removeProperty name="coEntrySecureDesc"/>
