<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achtemplatedelete" className="moduleHelpClass"/>
		<div align="center">
			<div align="center">
				<div align="left">
<s:form action="/pages/jsp/ach/deleteACHTemplate.action" method="post" name="achTemplateDelete" id="deleteACHTemplateFormId" >
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<s:hidden value="%{#attr.ACHBatch.ID}" name="ID"></s:hidden>
                    	<s:hidden value="%{#attr.ACHBatch.RecID}" name="RecID"></s:hidden>
                    	<s:hidden value="%{#attr.ACHBatch.ACHType}" name="batchAchType"></s:hidden>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td></td>
							<td class="columndata lightBackground">
						
										<table width="720" border="0" cellspacing="0" cellpadding="3">
											<!-- <tr>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
												<td class="lightBackground"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											</tr> -->
											<%-- <tr>
												<td class="nameSubTitle lightBackground" colspan="6"><span class="sectionhead">
												<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
													<!--L10NStart-->Delete ACH Batch Template<!--L10NEnd-->
													<input id="deleteBatchMessageId" class="submitbutton" type="hidden" value="ACH batch template has been deleted!">
												</ffi:cinclude>
												<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
													<!--L10NStart-->Delete Tax Batch Template<!--L10NEnd-->
													<input id="deleteBatchMessageId" class="submitbutton" type="hidden" value="Tax batch template has been deleted!">
												</ffi:cinclude>
												<ffi:cinclude value1="${subMenuSelected}" value2="child" >
													<!--L10NStart-->Delete Child Support Batch Template<!--L10NEnd-->
													<input id="deleteBatchMessageId" class="submitbutton" type="hidden" value="Child Support batch template has been deleted!">
												</ffi:cinclude>
													<br>
													</span></td>
											</tr> --%>
											<%-- <tr>
												<td class="sectionhead lightBackground"></td>
												<td class="sectionhead lightBackground" colspan="4">
													<div align="center" style="color:red">Are you sure you want to delete the template "<s:property value="%{#attr.ACHBatch.TemplateName}"/>"?</div>
												</td>
												<td class="sectionhead lightBackground"></td>
											</tr>
											<tr>
												<td class="sectionhead lightBackground"></td>
												<td class="sectionhead lightBackground"></td>
												<td class="sectionhead lightBackground" colspan="2">
												<div align="center">
													<sj:a 
										                button="true" 
														onClickTopics="closeDialog"
										        	>
										                Cancel
										        	</sj:a>
										        	<sj:a id="deleteSingleACHTemplateId" 
											        	formIds="deleteACHTemplateFormId" 
											        	targets="resultmessage" 
											        	button="true" 						  							
														title="Delete Template" 
														onCompleteTopics="cancelSingleACHTemplateComplete" 
														onSuccessTopics="cancelSingleACHTemplateSuccess" 
														effectDuration="1500" >Delete Template
													</sj:a>
												</div>
												</td>

												<td class="sectionhead lightBackground"></td>
												<td class="sectionhead lightBackground"></td>
											</tr> --%>
										</table>
										
								
							</td>
							<td></td>
						</tr>
					</table>
<div align="center" style="color:red"><s:text name="jsp.ach.delete.template.msg"/> "<s:property value="%{#attr.ACHBatch.TemplateName}"/>"?</div>
<div class="ffivisible" style="height:30px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
	<sj:a 
            button="true" 
onClickTopics="closeDialog"
    	>
Cancel
</sj:a>
<sj:a id="deleteSingleACHTemplateId" 
     	formIds="deleteACHTemplateFormId" 
     	targets="resultmessage" 
     	button="true" 						  							
title="Delete Template" 
onCompleteTopics="cancelSingleACHTemplateComplete" 
onSuccessTopics="cancelSingleACHTemplateSuccess" 
effectDuration="1500" ><s:text name="jsp.default_162" />
</sj:a>
</div>
</s:form>
				</div>
			</div>
		</div>
