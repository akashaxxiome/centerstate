<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.ach.ACHOffsetAccount,
				 com.ffusion.beans.ach.ACHClassCode" %>
<%--
	This file is used by both Add/Edit ACH AND Add/Edit ACH Templates
	If you modify this file, make sure your change works for both please.
--%>
<ffi:help id="payments_achbatchaddedit" className="moduleHelpClass"/>
<script type="text/javascript" src="<s:url value='/web/js/ach/achAddEdit%{#session.minVersion}.js'/>"></script>
<%
	String subMenuSelected = "ach";
	boolean addEntryDisabled = false;
	boolean importedBatch = false;
	boolean isTemplate = false;
	boolean fromTemplate = false;
	boolean companySelected = false;
  	boolean isEdit = true;
%>




<ffi:cinclude value1="${AddEditACHBatch.CompanyID}" value2="" operator="equals">
    <% addEntryDisabled = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.CoID}" value2="0" operator="equals">
    <% addEntryDisabled = true; %>
</ffi:cinclude>

<ffi:cinclude value1="${AddEditACHBatch.FromTemplate}" value2="true" >
	<% fromTemplate = true; %>
</ffi:cinclude>


<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
<% isTemplate = true;%>
</s:if>

<ffi:setProperty name="ProcessACHImport" property="ClearACHEntries" value="false"/> <%-- just in case our referring page is the cancel button of the achimportconfirm page --%>
<ffi:removeProperty name="ImportErrors" />

<%  boolean anyEntries = false;
	String headerTitle = "";		// used in assignTo below
   	String headerValue = "";		// used in assignTo below
	String pageHeading = "";
	if (subMenuSelected.equals("tax"))
	{
		pageHeading = "<!--L10NStart-->Tax Payment<!--L10NEnd-->";
	} else
	if (subMenuSelected.equals("child"))
	{
		pageHeading = "<!--L10NStart-->Child Support Payment<!--L10NEnd-->";
	}
	else
	{
		pageHeading = "<!--L10NStart-->ACH Payment<!--L10NEnd-->";
	}
	session.setAttribute("PageHeading", ("<!--L10NStart-->Edit<!--L10NEnd--> ") + pageHeading + (isTemplate?" <!--L10NStart-->Template<!--L10NEnd-->":"") );

%>
<span id="PageHeading" style="display:none;"><%="" + session.getAttribute("PageHeading")%></span>
    <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999"><ffi:setProperty name="OpenEnded" value="true" /></ffi:cinclude>
	<ffi:setProperty name="NumberPayments" value="${AddEditACHBatch.NumberPayments}" />
    <ffi:setProperty name="Frequency" value="${AddEditACHBatch.Frequency}" />

<ffi:cinclude value1="${OpenEnded}" value2="true" >
	<ffi:setProperty name="NumberPayments" value="999"/>
	<ffi:setProperty name="AddEditACHBatch" property="NumberPayments" value="${NumberPayments}"/>
</ffi:cinclude>

<%-- VERY IMPORTANT - AFTER ENTRY ADD SHOULD DO THIS --%>
<%-- <ffi:setProperty name="AddEditACHBatch" property="RecalculateTotals" value="true"/>
<ffi:process name="AddEditACHBatch"/> --%>

<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
	<% subMenuSelected = "ach";
	%>
</ffi:cinclude>
<s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />
<div id="achBatchInputDiv">

<ffi:cinclude value1="${AddEditACHBatch.AnyEntries}" value2="true" operator="equals" >
	<% anyEntries = true; %>
	</ffi:cinclude>
<s:form id="AddBatchNew" namespace="/pages/jsp/ach" validate="false" action="%{strutsActionName + '_verify'}" method="post" name="AddBatchForm" theme="simple">
<s:hidden value="%{'true'}" name="isEdit" id="isEdit"></s:hidden>
 <s:hidden id="editRecurringModel" name="editRecurringModel" value="%{editRecurringModel}"/>
 <s:hidden id="strutsActionName" value="%{strutsActionName}" />
 <s:hidden id="achType" name="achType" value="%{#session.AddEditACHBatch.ACHType}"></s:hidden>
 <% if (isTemplate) { %>
 <s:hidden id="ID" name="AddEditACHBatch.ID" value="%{#session.AddEditACHBatch.ID}"></s:hidden>
 <% } else {%>
 <s:hidden id="ID" name="ID" value="%{#session.AddEditACHBatch.ID}"></s:hidden>
 <% } %>
 <s:hidden id="RecID" name="RecID" value="%{#session.AddEditACHBatch.RecID}"></s:hidden>
 <s:hidden id="OriginalDate" name="OriginalDate" value="%{#session.AddEditACHBatch.Date}"></s:hidden>
 <s:hidden id="TransactionType" name="TransactionType" value="%{#session.AddEditACHBatch.TransactionType}"></s:hidden>
 <%
									String title = "";
                                    String[] titleStrings;
	String[] achStrings = {
						      "<!--L10NStart-->Create ACH Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create ACH Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create ACH Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit ACH Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit ACH Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit ACH Batch (from template)<!--L10NEnd-->",
    };
	String[] taxStrings = {
						      "<!--L10NStart-->Create Tax Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create Tax Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create Tax Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit Tax Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Tax Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Tax Batch (from template)<!--L10NEnd-->",
    };
	String[] childStrings = {
						      "<!--L10NStart-->Create Child Support Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create Child Support Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create Child Support Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit Child Support Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Child Support Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Child Support Batch (from template)<!--L10NEnd-->",
    };
                                        int titleIndex = 0;
										if (subMenuSelected.equals("tax")) {
                                            titleStrings = taxStrings;
										} else if (subMenuSelected.equals("child")) {
                                            titleStrings = childStrings;
										} else {
                                            titleStrings = achStrings;
										}
    if (isEdit)
        titleIndex = 3;
//    if (isView)
//        titleIndex = 6;
//    if (isDelete)
//        titleIndex = 9;
    if (isTemplate)
        titleIndex ++;
    else
    if (fromTemplate)
        titleIndex += 2;
    title = titleStrings[titleIndex];
									String tempEntToCheck;
									if (isTemplate) {
										if (subMenuSelected.equals("tax")) {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_TEMPLATE );
										} else if (subMenuSelected.equals("child")) {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_TEMPLATE );
										} else {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_TEMPLATE );
										}
									} else
									{
										if (subMenuSelected.equals("tax")) {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_PAYMENT_ENTRY );
										} else if (subMenuSelected.equals("child")) {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_PAYMENT_ENTRY );
										} else {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
										}
									}
									%>
<% if (isTemplate) { %>
				<div class="leftPaneWrapper">
					<div class="leftPaneInnerWrapper">
						<div class="header"><s:text name="jsp.default_366" /></div>
						<div class="leftPaneInnerBox leftPaneLoadPanel">
							<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
								<div class="inputBlockSection">
									<span id="achBatchNameLabel"class="sectionsubhead"><s:text name="ach.grid.templateName" /><span class="required">*</span></span><input type="hidden" name="template" value="true">
									<span class="columndata"><input id="AchBatchTemplateName" type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="32" border="0" name="AddEditACHBatch.TemplateName" value="<ffi:getProperty name="AddEditACHBatch" property="TemplateName"/>"></span>
									<span id="templateNameError"></span>
									<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="false" operator="equals" >
										<s:if test="%{consumerUser}">
											<input type="hidden" name="AddEditACHBatch.BatchScope" value="USER"/>
										</s:if>
									<s:else>
									<span class="sectionsubhead marginTop10"><s:text name="jsp.default_418"/> <span class="required">*</span></span>
									<span class="columndata">
										<select id="AddEditACHBatchBatchScopeID" class="txtbox" name="AddEditACHBatch.BatchScope">
											<s:if test="%{!#session.SecureUser.ConsumerUser}">
												<option value="BUSINESS" <ffi:cinclude value1="BUSINESS" value2="${AddEditACHBatch.BatchScope}" operator="equals" >selected</ffi:cinclude>><s:text name="jsp.default_77" /></option>
											</s:if>
											<!-- Start: Dual approval processing -->
											<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
												<option value="USER" <ffi:cinclude value1="USER" value2="${AddEditACHBatch.BatchScope}" operator="equals" >selected</ffi:cinclude>><s:text name="jsp.default_454" /></option>
											</ffi:cinclude>
											<!-- End: Dual approval processing -->
										</select>
									</span>
									</s:else>
									</ffi:cinclude>
								</div>
							</s:if>
						</div>
					</div>
				</div>
<% } else{ %>
	<div class="leftPaneWrapper">
					<div class="leftPaneInnerWrapper">
						<div class="header"><s:text name="ach.details" /></div>
						<div class="leftPaneInnerBox leftPaneLoadPanel">
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr class="lightBackground">
										<td width="50%" id="achBatchCompanyLabel"><span class="sectionsubhead"><s:text name="ach.grid.companyName" /> </span><span class="required">*</span></td>
									</tr>
									<tr class="lightBackground">
										<td class="columndata">
											<ffi:cinclude ifEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" checkParent="true">
												<s:select list="AchCompanies" name="AddEditACHBatch.CoID"  id="AddEditACHBatchCompanyID" value="%{#session.AddEditACHBatch.CoID}"
												listKey="ID" listValue="CompanyName +'-'+CompanyID" onchange="ns.ach.changeACHCompany(this.value);"
												headerKey="-1" headerValue="Please select ACH Company" disabled="true"></s:select>
											</ffi:cinclude>
										</td>
									</tr>
									<tr>
										<td width="50%" id="achBatchTypeLabel"><span class="sectionsubhead"><s:text name="jsp.default_67" />  </span><span class="required">*</span></td>
									</tr>
									<tr>
										<td class="columndata">
											<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
											<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
											<ffi:process name="ACHResource" />

											<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch">
												<s:select list="ClassCodeItems" listKey="id" listValue="label" name="AddEditACHBatch.StandardEntryClassCodeFromScreen" id="standardEntryClassCodeId"
												onchange="ns.ach.changeACHClassCode();" value="%{#session.AddEditACHBatch.StandardEntryClassCodeDebitBatch}"
												headerKey="-1" headerValue="Please select ACH Batch Type" disabled="true"></s:select>
												<br><span id="standardEntryClassCodeError" />
											</ffi:cinclude>
											<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
													<s:property value="%{selectedSECCodeDisplay}"/>
											</ffi:cinclude>
										</td>
									</tr>
							</table>
							 <% if (!fromTemplate) { %>
								<s:if test='%{!editRecurringModel && !(#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_TAX)) && RecID != "null" && RecID != ""}'>
									<div class="marginTop20 floatleft fxActionBtnCls"><sj:a id="loadRecModelFormButton"
								                button="true"
								                onclick="loadRecModel()"
								            ><s:text name="jsp.default.edit_orig_trans"/></sj:a>
								    </div>
								</s:if>
							<% } %>
						</div>
					</div>
				</div>
<% } %>
<% //if (isTemplate) { %>
<div class="rightPaneWrapper w71">
<% //} %>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<div class="header"><s:text name="jsp.ach.Company.Details" /></div>
		<div class="paneContentWrapper">

			<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="tableData">
						<tr>
							<td class="columndata lightBackground">
                    		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    		<input type="hidden" id="batchTypeId" name="batchTypeh" value="<ffi:getProperty name='subMenuSelected'/>"/>
							<div align="center">
								<div align="left">
								<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData formTablePadding5">
									<!-- <tr>
										<td class="sectionhead lightBackground" colspan="7"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="692" height="1" border="0"></td>
									</tr>
									<tr valign="top"> -->

										<%-- <td class="sectionhead ltrow2_color" colspan="7"><span class="sectionhead"><%= title %>
										</span>

										</td>
									</tr> --%>
						<% if (isTemplate) { %>
									<tr class="lightBackground">
										<td width="50%" id="achBatchCompanyLabel"><span class="sectionsubhead"><s:text name="ach.grid.companyName"/> </span><span class="required">*</span></td>
										<td width="50%" id="achBatchTypeLabel"><span class="sectionsubhead"><s:text name="jsp.default_67" /> </span><span class="required">*</span></td>
									</tr>
									<tr class="lightBackground">
										<td class="columndata">
											<ffi:cinclude ifEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" checkParent="true">
												<s:select list="AchCompanies" name="AddEditACHBatch.CoID"	id="AddEditACHBatchCompanyID" value="%{#session.AddEditACHBatch.CoID}"
												listKey="ID" listValue="CompanyName +'-'+CompanyID" onchange="ns.ach.changeACHCompany(this.value);"
												headerKey="-1" headerValue="Please select ACH Company" disabled="true"></s:select>
											</ffi:cinclude>
										</td>
										<td class="columndata">
											<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
											<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
											<ffi:process name="ACHResource" />

											<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch">
												<s:select list="ClassCodeItems" listKey="id" listValue="label" name="AddEditACHBatch.StandardEntryClassCodeFromScreen" id="standardEntryClassCodeId"
												onchange="ns.ach.changeACHClassCode();" value="%{#session.AddEditACHBatch.StandardEntryClassCodeDebitBatch}"
												headerKey="-1" headerValue="Please select ACH Batch Type" disabled="true"></s:select>
												<br><span id="standardEntryClassCodeError" />
											</ffi:cinclude>
											<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
													<s:property value="%{selectedSECCodeDisplay}"/>
											</ffi:cinclude>
										</td>
									</tr>
							<%} %>
									<tr>
										<td colspan="2"><span id="coIDError"></span></td>
									</tr>
									<s:if test="%{#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
	    								<tr>
											<td class="sectionsubhead"><s:text name="jsp.default_481" />  <span class="required">*</span></td>
										</tr>
										<tr>
											<td class="columndata">
		                                        <select class="txtbox" id="defaultTaxFormID" name="AddEditACHBatch.TaxFormID" onchange="changeChildspType();">
													<option value='-1' ><s:text name="ach.child.support.type" /> </option>
													<s:iterator value="taxForms" var="TaxForm">
															<option value="<ffi:getProperty name='TaxForm' property='ID'/>"
																<s:if test="%{#TaxForm.ID.equals(#session.AddEditACHBatch.TaxFormID)}">selected</s:if>>
																<s:if test="%{'CHILDSUPPORT'.equals(#TaxForm.type)}"><ffi:getProperty name="TaxForm" property="state" /> <s:text name="jsp.default_386" /></s:if>
																<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /></b> - <ffi:getProperty name="TaxForm" property="TaxFormDescription" />
			                                                       <ffi:cinclude value1="${TaxForm.BusinessID}" value2="" operator="notEquals">
			                                                                        (<s:text name="jsp.reports_526" />)
			                                                       </ffi:cinclude>
															</option>
													</s:iterator>
												</select>
											</td>
										</tr>
										<tr>
											<td><span id="taxFormIDError"></span></td>
										</tr>
								</s:if>

<%-- Create a new table with two columns, the first column will be the left side,
the second column will be the right side (repeating and Offset Account) --%>
                                    <tr>
                                        <td colspan="2">
                                        <div id="addEditVariableSection">
                                        	<s:include value="inc/achBatchAddEditVariableSection.jsp" />
                                        </div>
                                	</td>
                                </tr>
									<tr>
										<td class="sectionsubhead" colspan="2" nowrap>
										<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
											<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
												<input id="setACHBatchTypeId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only formSubmitBtn' type="button"
												value="Set ACHBatch Type" onClick="openACHBatchTypeDialog();">
												&nbsp;
										    </ffi:cinclude>
										</ffi:cinclude>
		<input id="setAllMountsButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only formSubmitBtn' type="button"
		 value="Set All Amounts" onClick="setAllAmounts();">
		&nbsp;
		<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
		<s:if test="%{!#session.SecureUser.ConsumerUser}">
			<input id="importAmmounts" class="formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button"
			name="Submit3" value="Import Amounts" onclick="ns.ach.importAmounts();">&nbsp;

			<s:if test="%{!#session.AddEditACHBatch.FromTemplateValue}">
			<s:if test="%{!#session.SecureUser.ConsumerUser}">
				<input id="IMPORTENTRIES" class='formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= addEntryDisabled?"ui-state-disabled":"" %>'
				type="button" <%= addEntryDisabled?"disabled":"" %> name="Submit3" value="Import Entries" onclick="ns.ach.importEntries();">&nbsp;
				</s:if>
			</s:if>
		</s:if>
		</ffi:cinclude>
<s:if test="%{!#session.AddEditACHBatch.FromTemplateValue}">

					<input id="addEntryButtonId" class='formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= addEntryDisabled?"ui-state-disabled":"" %>' type="button" <%= addEntryDisabled?"disabled":"" %>
					 name="addEntry" value="Add Entry" onclick="addACHEntry('<s:property value="%{#session.AddEditACHBatch.ACHType}"/>');">
</s:if>
										</td>
									</tr>


</table>
<ffi:setProperty name="ACHEntries" property="FilterPaging" value="none"/>


 										</div>
									</div>
							</td>
						</tr>
</table>
</div>
</div>
</div>
<!-- <div class="marginTop10"></div> -->
<!-- ACHBatch Entry header and Grid details -->
<div class="paneWrapper marginTop20">
	<div class="paneInnerWrapper">
		<div class="header"><s:text name="ach.grid.entries"/></div>
		<div class="paneContentWrapper">
			<div id="achEntryDetails">
				<s:include value="%{#session.PagesPath}/ach/achbatchentryaddeditdetails.jsp"></s:include>
			</div>
		</div>
	</div>
</div>
<!-- </div> -->
<s:if test="%{#session.AddEditACHBatch.IsTemplateValue}">
<s:set var="showLifeCycle" value="%{'true'}"/>

<s:if test="%{#session.AddEditMultiACHTemplate.Action == 'Add'}">
	<s:set var="showLifeCycle" value="%{'false'}"/>
</s:if>
<s:if test="%{#showLifeCycle == 'true'}">
<div class="marginTop10"></div>
<!-- ACHBatch Entry header and Grid details -->
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<div class="header"><s:text name="jsp.default_414" /></div>
		<div class="paneContentWrapper">
			<s:include value="%{#session.PagesPath}/ach/achbatchaddedit_lifecycle.jsp" />
		</div>
	</div>
</div>
</s:if>
</s:if>
<% //if (isTemplate) { %>
</div>
<% //} %>
<%-- <table align="center" width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td>
			<s:if test="%{#session.AddEditACHBatch.IsTemplateValue}">
				<s:include value="%{#session.PagesPath}/ach/achbatchaddedit_lifecycle.jsp" />
			</s:if>
		</td>
	</tr>
</table> --%>
<div class="btn-row">
			<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >

					<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
						<sj:a
			                button="true"
							summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHTemplateForm"
			        	><s:text name="jsp.default_82" />
			        	</sj:a>
					</s:if>
					<s:else>
						<sj:a
			                button="true"
							summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHForm"
			        	><s:text name="jsp.default_82" />
			        	</sj:a>
					</s:else>
			</ffi:cinclude>
			<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
								<sj:a
	                                button="true"
	                                onClickTopics="backToACHMultiBatchForm"
			                        ><s:text name="jsp.default_82" /><!-- CANCEL -->
	                        	</sj:a>
			</ffi:cinclude>
							&nbsp;
    		<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
							<s:if test="%{!#session.AddEditACHBatch.FromTemplateValue}">
								<sj:a
									id="verifyACHSubmit"
									formIds="AddBatchNew"
	                                targets="inputDiv"
	                                button="true"
	                                validate="true"
	                                validateFunction="ns.ach.batchCustomValidation"
	                                onBeforeTopics="beforeVerifyACHForm"
	                                onClickTopics="clickEditToMultiTempForm"
	                               onErrorTopics="errorAddACHTempForm"
			                        ><s:text name="jsp.default_366" />
	                        	</sj:a>
	                        </s:if>
	                     <s:if test="%{#session.AddEditACHBatch.FromTemplateValue}">
	                     	<sj:a
										id="verifyACHSubmit"
										formIds="AddBatchNew"
		                                targets="verifyDiv"
		                                button="true"
		                                validate="true"
		                                validateFunction="ns.ach.batchCustomValidation"
		                                onBeforeTopics="beforeVerifyACHForm"
		                                onCompleteTopics="verifyACHFormComplete"
										onErrorTopics="errorVerifyACHForm"
		                                onSuccessTopics="successVerifyACHForm"
		                                onClickTopics="clickVerifyACHForm"
				                        ><s:text name="jsp.default_291" />
		                        </sj:a>
	                     </s:if>

			</ffi:cinclude>


							<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
							<%-- 		<input type="hidden" name="isTemplateEdit" value='<ffi:getProperty name="isTemplateEdit"/>'>
									<sj:a
										id="verifyACHSubmit"
										formIds="AddBatchNew"
		                                targets="resultmessage"
		                                button="true"
		                                validate="true"
		                                validateFunction="ns.ach.batchCustomValidation"
		                                onBeforeTopics="beforeVerifyACHForm"
		                                onClickTopics="clickEditACHTempForm"
		                                onCompleteTopics="completeAddACHTempForm"
		                                onSuccessTopics="successAddACHTempForm"
		                                onErrorTopics="errorAddACHTempForm"
				                        >SAVE TEMPLATE
		                        	</sj:a>
 --%>

									<sj:a
										id="verifyACHSubmit"
										formIds="AddBatchNew"
		                                targets="verifyDiv"
		                                button="true"
		                                validate="true"
		                                validateFunction="ns.ach.batchCustomValidation"
		                                onBeforeTopics="beforeVerifyACHForm"
		                                onCompleteTopics="verifyACHFormComplete"
										onErrorTopics="errorVerifyACHForm"
		                                onSuccessTopics="successVerifyACHForm"
		                                onClickTopics="clickVerifyACHForm"
				                        ><s:text name="jsp.default_291" />
		                        	</sj:a>
		                        </ffi:cinclude>
					<%-- </ffi:cinclude> --%>
			<%-- <ffi:cinclude operator="equals" value1="true" value2="${AddEditACHBatch.EntryValidationErrors}">
									<sj:a
										id="verifyCheckACHSubmit"
										formIds="AddBatchNew"
		                                targets="verifyDiv"
		                                button="true"
		                                validate="true"
		                                validateFunction="ns.ach.batchCustomValidation"
		                                onCompleteTopics="recheckACHFormComplete"
		                                onClickTopics="clickVerifyACHForm"
				                        >RECHECK VALIDATION
		                        	</sj:a>
		<script>
			ns.ach.showErrorEntriesURL = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achbatchaddedit.jsp?ShowErrors=true"/>';
 		</script>
									<sj:a
                                        id="showErrorRows"
                                        formIds="AddBatchNew"
                                        targets="inputDiv"
                                        button="true"
                                        onCompleteTopics="showErrorRowsACHFormComplete"
                                        onClickTopics="clickVerifyACHForm"
                                        >SHOW ERROR ENTRIES
                                    </sj:a>
                                    <span
                                        id="showingErrorRows"
                                        style="display:none;"
                                        >&nbsp;SHOWING ONLY ERROR ENTRIES
                                    </span>

            </ffi:cinclude>
 --%>
 </div>
	</s:form>
</div>
<%-- This div will be used to show import Amount or import entries section. --%>
<div id="achBatchImportDiv"></div>

<s:if test="%{#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
<script type="text/javascript">
changeChildspType();
</script>
</s:if>

<% if (isTemplate) { %>
<script language="javascript">
$(document).ready(function(){
	$("#AddEditACHBatchCompanyID").selectmenu({width: 400});
	$("#standardEntryClassCodeId").selectmenu({width: 400});
});
</script>
<% } else{ %>
<script language="javascript">
$(document).ready(function(){
	$("#AddEditACHBatchCompanyID").selectmenu({width: 280});
	$("#standardEntryClassCodeId").selectmenu({width: 280});
});

function loadRecModel(){
	var transactionType = $('#OriginalDate').val();
	var recId = $('#RecID').val();
	var achType = $('#achType').val();
	var url = "/cb/pages/jsp/ach/editACHBatch_init.action?EditRecurringModel=true&ID=" + $('#ID').val() + "&RecID="+ recId + "&OriginalDate=" + $('#OriginalDate').val();
	if (achType == "ACHBatch") {
		url = "/cb/pages/jsp/ach/editACHBatch_init.action?EditRecurringModel=true&ID=" + $('#ID').val() + "&RecID="+ recId + "&OriginalDate=" + $('#OriginalDate').val();
	}
	if (achType == "ChildSupportPayment") {
		url = "/cb/pages/jsp/ach/editChildSupportBatch_init.action?EditRecurringModel=true&ID=" + $('#ID').val() + "&RecID="+ recId + "&OriginalDate=" + $('#OriginalDate').val();
	}
	if (achType == "TaxPayment") {
		url = "/cb/pages/jsp/ach/editTaxBatch_init.action?EditRecurringModel=true&ID=" + $('#ID').val() + "&RecID="+ recId + "&OriginalDate=" + $('#OriginalDate').val();
	}
	ns.ach.editPendingACHBatch(url, transactionType, recId);
}
</script>
<%} %>
<ffi:setProperty name="OpenEnded" value="false"/>
<ffi:removeProperty name="ModifyACHEntry" />
<ffi:removeProperty name="ACHEntryPayee" />
<ffi:removeProperty name="coEntrySecureDesc"/>
<ffi:removeProperty name="NumberPayments" />

