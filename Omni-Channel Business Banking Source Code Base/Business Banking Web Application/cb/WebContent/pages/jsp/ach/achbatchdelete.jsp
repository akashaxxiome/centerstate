<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achbatchdelete" className="moduleHelpClass"/>

<%-- required to access the common ach batch pages--%>
<s:include value="%{#session.PagesPath}/ach/inc/include-view-transaction-details-constants.jsp" />
		<div align="center" class="approvalDialogHt390">
			<div align="left">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="columndata lightBackground">
					<s:form action="/pages/jsp/ach/deleteACHBatch.action" method="post" name="achBatchDelete" id="deleteACHBatchFormId" >
						<s:hidden name="ID" value="%{#attr.ID}"/>
        				<s:hidden name="RecID" value="%{#attr.RecID}"/>
        				<s:hidden id="deleteRecurringModel" name="deleteRecurringModel" value="%{#attr.deleteRecurringModel}"/>
        				
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="equals" >
                <input type="hidden" name="DeleteACHBatch.Process" value="true" >
</ffi:cinclude>
<s:if test='%{RecID != "null" && RecID != ""}'>
	<div class="marginTop10 columndata_error" align="left"><s:text name="jsp.ach.delete.warning"/></div>
</s:if>

								<table width="98%" border="0" cellspacing="0" cellpadding="3">
							<s:if test="%{#attr.ACHBatch.FrequencyValue > 0}">
<%-- 								<s:if test="%{deleteRecurringModel}"> --%>
<!-- 									<tr> -->
<!-- 										<td class="sectionsubhead" colspan="3" style="color: red" -->
<!-- 											align="center"> -->
<%-- 											<s:text name="jsp.ach.warning.deleting.recurring.achBatch" /> --%>
<!-- 										<br> -->
<!-- 										</td> -->
<!-- 									</tr> -->
<%-- 								</s:if> --%>
<%-- 								<s:else> --%>
<!-- 									<tr> -->
<!-- 										<td class="sectionsubhead" colspan="3" style="color: red" -->
<!-- 											align="center"> -->
<%-- 											<s:text name="jsp.ach.warning.deleting.recurring.achBatch2" /> --%>
<!-- 										<br> -->
<!-- 										</td> -->
<!-- 									</tr> -->
<%-- 								</s:else> --%>
							</s:if>
							<%-- <tr>
										<td class="sectionhead lightBackground" colspan="6" align="center"><span class="sectionhead">
										<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
												<!--L10NStart-->Are you sure you want to delete this ACH Batch?<!--L10NEnd-->
										</ffi:cinclude>
										<ffi:cinclude value1="${subMenuSelected}" value2="child" >
												<!--L10NStart-->Are you sure you want to delete this Child Support Batch?<!--L10NEnd-->
										</ffi:cinclude>
										<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
												<!--L10NStart-->Are you sure you want to delete this Tax Batch?<!--L10NEnd-->
										</ffi:cinclude>
											<br>
											<br>
											</span>
										</td>
									</tr> --%>
									<tr>
										<td class="lightBackground" colspan="6">
						<div>
						<s:include value="%{#session.PagesPath}/ach/achbatchcommon.jsp" />
					    </div>
										</td>
									</tr>
									<!-- <tr>
										<td class="sectionsubhead" colspan="6"><hr class="thingrayline"></td>
									</tr> -->
								</table>

								<%-- include batch entries --%>
								
								</s:form>
							</td>
							<td></td>
						</tr>
					</table>
					
<!-- 					
<div align="center" class="sectionhead" style="color:red">
		<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
				<s:text name="jsp.ach.delete.ACHBatch.msg" />
		</ffi:cinclude>
		<ffi:cinclude value1="${subMenuSelected}" value2="child" >
				<s:text name="jsp.ach.delete.ChildSupportBatch.msg"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
				<s:text name="jsp.ach.delete.TaxBatch.msg" />
		</ffi:cinclude>
</div>
 -->
<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div  class="ui-widget-header customDialogFooter">
						<sj:a 
			                button="true" 
							onClickTopics="closeDialog,reloadPendingACHBatchSummaryGrid"
			        	><s:text name="jsp.default_82"/>
			        	</sj:a>
<%
	boolean isDeleteFromMulti = false;
	if (session.getAttribute("ViewMultipleBatchTemplate") != null) {
        session.removeAttribute("ViewMultipleBatchTemplate");
		isDeleteFromMulti = true;
	}
%>

<%if(isDeleteFromMulti) {%>
	<script>ns.ach.cancelACHMultiTemplateBatchUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/editMultiACHTemplateAction_deletebatch_save.action"/>';</script>
	<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
				    <input class="submitbutton" type="hidden" value="Delete ACH Batch">
				    <sj:a id="deleteACHBatchId" 
				        	button="true" 						  							
							onClickTopics="cancelACHMultiTemplateBatchComplete" 
							><s:text name="jsp.default_162" />
					</sj:a>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
		    <input id="deleteBatchMessageId" class="submitbutton" type="hidden" value="Tax batch has been deleted!">
		    <sj:a id="deleteTaxLink" 
		        	button="true" 						  							
					title="Delete Tax Batch" 
					onClickTopics="cancelACHMultiTemplateBatchComplete"
					><s:text name="jsp.default_162" />
			</sj:a>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
		    <input id="deleteBatchMessageId" class="submitbutton" type="hidden" value="Child Support batch has been deleted!">
	    	<sj:a id="deleteChildspLink" 
	        	button="true" 						  							
				title="Delete Child Support Batch" 
				onClickTopics="cancelACHMultiTemplateBatchComplete"
				><s:text name="jsp.default_162" />
			</sj:a>
	</ffi:cinclude>
<%}  else {%>
	
	<s:if test='%{RecID != "null" && RecID != ""}'>
		<sj:a id="loadRecModelForDelete" 
					        	formIds="deleteACHBatchFormId" 
					        	targets="resultmessage" 
					        	button="true" 						  							
								title="Delete ACH Batch" 
								onClickTopics="setRecModel" 
								onCompleteTopics="cancelSingleACHBatchComplete" 
								onSuccessTopics="cancelACHSuccessTopics" 
								onErrorTopics="cancelACHErrorTopics"
								effectDuration="1500" ><s:text name="jsp.default.delete_orig_trans" />
							</sj:a>
	</s:if>
	<s:if test='%{!deleteRecurringModel}'>
		<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
					    <input id="deleteBatchMessageId" class="submitbutton" type="hidden" value="ACH batch has been deleted!">
					    	<sj:a id="deleteACHLink" 
					        	formIds="deleteACHBatchFormId" 
					        	targets="resultmessage" 
					        	button="true" 						  							
								title="Delete ACH Batch" 
								onCompleteTopics="cancelSingleACHBatchComplete" 
								onSuccessTopics="cancelACHSuccessTopics" 
								onErrorTopics="cancelACHErrorTopics"
								effectDuration="1500" ><s:text name="jsp.default.delete_this_trans" />
							</sj:a>
		</ffi:cinclude>
		   	<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
	  				    <input id="deleteBatchMessageId" class="submitbutton" type="hidden" value="Tax batch has been deleted!">
	  				    <sj:a id="deleteACHLink"
	  				        	formIds="deleteACHBatchFormId" 
	  				        	targets="resultmessage" 
	  				        	button="true" 						  							
	  							title="Delete Tax Batch" 
	  							onCompleteTopics="cancelSingleACHBatchComplete" 
	  							onSuccessTopics="cancelACHSuccessTopics" 
							    onErrorTopics="cancelACHErrorTopics"
							    effectDuration="1500" ><s:text name="jsp.default.delete_this_trans" />
	 					</sj:a>
	 				    
	   	</ffi:cinclude>
	   	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
	  				    <input id="deleteBatchMessageId" class="submitbutton" type="hidden" value="Child Support batch has been deleted!">
	  				    	<sj:a id="deleteACHLink"
	  				        	formIds="deleteACHBatchFormId" 
	  				        	targets="resultmessage" 
	  				        	button="true" 						  							
	  							title="Delete Child Support Batch" 
	  							onCompleteTopics="cancelSingleACHBatchComplete" 
								onSuccessTopics="cancelACHSuccessTopics" 
								onErrorTopics="cancelACHErrorTopics"
	  							effectDuration="1500" ><s:text name="jsp.default.delete_this_trans" />
	  						</sj:a>
	 
	   	</ffi:cinclude>
	</s:if>
<%} %>
			        	
					</div>
			</div>
		</div>
<script type="text/javascript">
$(document).ready(function(){
//For fixing the issue 1680167890:Transaction History not displayed for Delete (Batches)
//The same function is available in the  pages/jsp/ach/inc/include-view-transaction-history.jsp hence removing from here
	$.subscribe('setRecModel', function(event,data) {
		document.getElementById('deleteRecurringModel').value = 'true';
	});

});

</script>