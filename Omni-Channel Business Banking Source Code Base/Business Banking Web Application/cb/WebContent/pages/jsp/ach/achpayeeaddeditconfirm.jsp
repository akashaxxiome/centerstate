<%@ page import="com.ffusion.beans.ach.ACHPayee"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="payments_achpayeeaddeditconfirm" className="moduleHelpClass"/>
    <div align="center">
            <ffi:include page="/pages/jsp/ach/inc/payee_view.jsp" />
    </div>
