<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- ----------------------------------------
	this file is used to display the top portion, batch-related items for the following files:
		achbatchdelete.jsp
		achbatchview.jsp
		achbatch-reverse.jsp
	if you make changes, test with all of the files
--------------------------------------------- --%>

    <ffi:setProperty name="OpenEnded" value="false" />
    <ffi:cinclude value1="${ACHBatch.NumberPayments}" value2="999"><ffi:setProperty name="OpenEnded" value="true" /></ffi:cinclude>
    <ffi:setProperty name="NumberPayments" value="${ACHBatch.NumberPayments}" />
    <ffi:setProperty name="Frequency" value="${ACHBatch.Frequency}" />
    

<%-- ACH Company is now needed to display the batch properly --%>
	<%-- <ffi:object id="SetACHCompany" name="com.ffusion.tasks.ach.SetACHCompany" scope="session"/>
	<ffi:setProperty name="SetACHCompany" property="CompanyID" value="${ACHBatch.CompanyID}" />
	<ffi:process name="SetACHCompany" />
	<ffi:removeProperty name="SetACHCompany"/> --%>

<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true">
			<s:set var="isRecurringAvailable" value="false"/>
</ffi:cinclude> 

<s:set var="isTemplate" value="false" scope="request"/>
<s:if test="%{#session.ViewTemplateBatch != null}">
	<s:set var="isTemplate" value="true" scope="request"/>
</s:if>

<ffi:cinclude value1="${ACHBatch.TemplateScope}" value2="" operator="notEquals">
    <ffi:cinclude value1="${ACHBatch.TemplateID}" value2="" operator="equals">
        <s:set var="isTemplate" value="true" scope="request"/>
    </ffi:cinclude>
</ffi:cinclude>

</table>

<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
<div class="confirmationDetails">
	<span id="viewAchTrackingIdLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Tracking ID:<!--L10NEnd--></span>
	<span id="viewAchTrackingIdValue" class="columndata"><ffi:getProperty name="ACHBatch" property="TrackingID" /></span>
</div>

<div class="blockWrapper label130">
	<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
	<div class="blockContent">
		<div class="blockRow">
			<span id="viewAchBatchStatusLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Status:<!--L10NEnd--></span>
			<span id="viewAchBatchStatusValue" class="columndata"><ffi:getProperty name="ACHBatch" property="Status" /></span>
		</div>
	</div>
</div>
</ffi:cinclude>
<div class="blockWrapper label130">
	<div  class="blockHead">
		<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
				<!--L10NStart-->ACH Batch<!--L10NEnd-->
		</ffi:cinclude>
		<ffi:cinclude value1="${subMenuSelected}" value2="child" >
				<!--L10NStart-->Child Support Batch<!--L10NEnd-->
		</ffi:cinclude>
		<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
				<!--L10NStart-->Tax Batch<!--L10NEnd-->
		</ffi:cinclude>
		<ffi:cinclude value1="${ViewTemplateBatch}" value2="" operator="notEquals">
				<!--L10NStart-->Template<!--L10NEnd-->
		</ffi:cinclude>
	</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewAchBatchcompanyNameLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Company Name:<!--L10NEnd--></span>
				<span id="viewAchBatchCompanuNamevlaue" class="columndata">
					<ffi:getProperty name="ACHBatch" property="achCompany.CompanyName" />&nbsp;-&nbsp;<ffi:getProperty name="ACHBatch" property="achCompany.CompanyID" />
				</span>
			</div>
			<div class="inlineBlock">
				<span id="viewAchBatchTypeLabel" class="sectionsubhead sectionLabel" ><!--L10NStart-->Batch Type:<!--L10NEnd--></span>
				<span id="viewAchBatchTypeValue" class="columndata">
					<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ACHBatch" operator="equals">
						<s:property value="#session.ACHBatch.achClassCodeDescDisplayName"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${ACHBatch.ACHType}" value2="TaxPayment" operator="equals">
						<!--L10NStart-->Tax Payment (CCD + TXP Credit)<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ChildSupportPayment" operator="equals">
						<!--L10NStart-->Child Support Payment (CCD + DED Credit)<!--L10NEnd-->
					</ffi:cinclude>
				</span>
			</div>
		</div>
		
		<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ChildSupportPayment" >
			<div class="blockRow">
				<span class="sectionsubhead sectionLabel" ><!--L10NStart-->Child Support Type:<!--L10NEnd--></span>
				<span class="columndata">
					<s:if test="%{'CHILDSUPPORT'.equals(#session.ACHBatch.TaxForm.Type)}" >
						<s:property value="#session.ACHBatch.TaxForm.State"/> <!--L10NStart-->State<!--L10NEnd-->
					</s:if>
					<s:property value="#session.ACHBatch.TaxForm.IRSTaxFormNumber"/> - <s:property value="#session.ACHBatch.TaxForm.TaxFormDescription"/> 
				</span>
			</div>
		</ffi:cinclude>
		
			<div class="blockRow"> 
				<div class="inlineBlock" style="width: 50%">
					<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
						<span id="viewAchBatchBatchNameLabel" class="sectionsubhead sectionLabel">
							<s:if test="%{!#attr.isTemplate}">
								<!--L10NStart-->Batch Name:<!--L10NEnd-->
							</s:if>	
							<s:else>
								<!--L10NStart-->Template Name:<!--L10NEnd-->
							</s:else>
						</span>
						<span id="viewAchBatchNameValue" class="columndata">
							<s:if test="%{!#attr.isTemplate}">
								<ffi:getProperty name="ACHBatch" property="Name"/>
							</s:if>	
								<s:else>
									<ffi:getProperty name="ACHBatch" property="TemplateName"/>
							</s:else>
						</span>
					</ffi:cinclude>
					<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="equals" >
			           		<span class="sectionsubhead sectionLabel">
			                    <!--L10NStart-->Template Name:<!--L10NEnd-->
			                </span>
			                <span class="columndata">
			                    <ffi:getProperty name="ACHBatch" property="TemplateName"/>
			                </span>
					</ffi:cinclude>
				</div>
				<div class="inlineBlock">
					<span id="viewAchBatchCompanyDescriptionLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Company Description:<!--L10NEnd--></span>
					<span id="viewAchBatchCompanyDescriptionValue" class="columndata"><ffi:getProperty name="ACHBatch" property="CoEntryDesc"/></span>
				</div>
				
			</div>
		<s:if test="%{!@com.ffusion.beans.ach.ACHDefines@ACH_CLASSCODE_IAT.equals(#session.ACHBatch.StandardEntryClassCode)}">
		    <div class="blockRow">
				<span id="viewAchBatchDiscretionaryDataLabel" class="sectionsubhead sectionLabel">
						<!--L10NStart-->Discretionary Data:<!--L10NEnd-->
				</span>
				<span id="viewAchBatchDiscretionaryDataValue" class="columndata">
						<ffi:getProperty name="ACHBatch" property="CoDiscretionaryData"/>
				</span>
			</div>
		</s:if>	
<s:else>
	<!-- IAT doesn't have Discretionary Data -->
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span class="sectionsubhead sectionLabel" >
			<!--L10NStart-->Originating Currency<!--L10NEnd-->
			</span>
			<span class="columndata">
					<!--L10NStart-->USD - United States Dollar<!--L10NEnd-->
			</span>
		</div>
		<div class="inlineBlock">
			<span >
	       	 	<span class="sectionsubhead sectionLabel"><!--L10NStart-->Destination Country<!--L10NEnd--> </span>
            </span>
            <span class="columndata">
               <s:property value="#session.ACHBatch.ISO_DestinationCountryDisplayName"/>
            </span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span >
        		<span class="sectionsubhead sectionLabel"><!--L10NStart-->Destination Currency<!--L10NEnd--> </span>
            </span>
            <span class="columndata">
                <s:property value="#session.ACHBatch.ISO_DestinationCurrencyDisplayName"/>
            </span>
		</div>
		<div class="inlineBlock">
			<span >
        		<span class="sectionsubhead sectionLabel"><!--L10NStart-->Foreign Exchange Method<!--L10NEnd--> </span>
             </span>
             <span class="columndata">
              	<s:property value="#session.ACHBatch.foreignExchangeIndicatorDisplayName"/>
             </span>
		</div>
	</div>
</s:else>
	 <div class="blockRow">
	 	<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
		 	<div class="inlineBlock" style="width: 50%">
				<span id="viewAchBatchEffectiveDateLabel" class="sectionsubhead sectionLabel">
					<s:if test="%{!#attr.isTemplate}">
							<!--L10NStart-->Effective Date:<!--L10NEnd-->
					</s:if>
					<s:else>
							<!--L10NStart-->Template Scope:<!--L10NEnd-->
					</s:else>
				</span>
				<span id="viewAchBatchEffectiveDateValue" class="columndata">
					<s:if test="%{!#attr.isTemplate}">
					
					        <ffi:getProperty name="ACHBatch" property="Date" />
					</s:if>
					<s:else>
					        <ffi:getProperty name="ACHBatch" property="BatchScope" />
					</s:else>
				</span>
			</div>
		</ffi:cinclude>		
	 </div>
        
        <s:if test="%{#attr.isRecurringAvailable && #attr.viewRecurringModel || #attr.isTemplate}">    
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">

				<span id="viewAchBatchRepeatingLabel" class="sectionsubhead sectionLabel">	 			
					<!--L10NStart-->Repeating:<!--L10NEnd-->	 			
				</span>

				<span id="viewAchBatchrepeatingValue" class="columndata">

					 <ffi:getProperty name="Frequency"/>	
					 
				</span>
				
				
			</div>
			
			<div class="inlineBlock">
			
				<span id="viewAchBatchPaymentsLabel" class="sectionsubhead sectionLabel">
					Payments:
				</span>
							
				<span id="viewAchBatchPaymentsValue" class="columndata">
					<ffi:cinclude value1="${NumberPayments}" value2="999">Unlimited</ffi:cinclude>
					<ffi:cinclude value1="${NumberPayments}" value2="999" operator="notEquals" >
					<ffi:getProperty name="NumberPayments"/>
					</ffi:cinclude>
				</span>
			</div>

		 </div>
		 <s:if test="%{!#attr.isTemplate}">
		 <div class="blockRow">
			<div class="inlineBlock" style="width: 50%">

				<span id="viewAchBatchEndDateLabel" class="sectionsubhead sectionLabel">	 			
					<s:text name="jsp.default.end.date"/> :	 			
				</span>
				
				<span id="viewAchBatchEndDateValue" class="columndata">
				
				<ffi:cinclude value1="${NumberPayments}" value2="999"><s:text name="jsp.default.not.applicable"/></ffi:cinclude>
				
				<ffi:cinclude value1="${NumberPayments}" value2="999" operator="notEquals" >
					<ffi:getProperty name="ACHBatch" property="EndDate" />
				</ffi:cinclude>

				</span>


			</div>
			
			<div class="inlineBlock">

				<span id="viewAchBatchRemainingPmtsCountLabel" class="sectionsubhead sectionLabel">
					<s:text name="jsp.default.future.payments"/>:
				</span>

				<span id="viewAchBatchRemainingPmtsCountValue" class="columndata">

					<ffi:cinclude value1="${NumberPayments}" value2="999"><s:text name="jsp.default.not.applicable"/></ffi:cinclude>
								
					<ffi:cinclude value1="${NumberPayments}" value2="999" operator="notEquals" >
						<ffi:getProperty name="ACHBatch" property="RemainingPmtsCount" />	
					</ffi:cinclude>
					
				</span>
			</div>
			
		 </div>
		 </s:if>
	 </s:if>
	 
	 <s:if test="%{#session.ACHBatch.achHeaderCompanyName != null && #session.ACHBatch.achHeaderCompanyName != ''}">
	 	<div class="blockRow">
			<span id="viewAchBatchOriginalPayeeNameLabel" class="sectionsubhead sectionLabel">
				<s:property value="#session.ACHBatch.achHeaderCompanyName"/>
			</span>
			<span id="viewAchBatchOriginalPayeeNameValue" class="columndata">
				<ffi:getProperty name="ACHBatch" property="HeaderCompName"/>
			</span>
		</div>
	</s:if>	
	
	<s:if test="%{@com.ffusion.beans.ach.ACHBatchTypes@BATCH_BALANCED == #session.ACHBatch.BatchTypeValue}">
		<s:if test="%{#session.ApprovalsViewTransactionDetails == null}">
			<div class="blockRow">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Offset Account<!--L10NEnd--></span>
				<span class="columndata">
	        		<s:property value="#session.ACHBatch.OffsetAccount.Number"/>-<s:property value="#session.ACHBatch.OffsetAccount.Type"/>
	        	</span>
	        </div>
		</s:if>
	</s:if>
	
	</div>
</div>
<table width="720" border="0" cellspacing="0" cellpadding="3">
<%-- <ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
<tr>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
<tr class="lightBackground">
</ffi:cinclude>

	<td id="viewAchBatchcompanyNameLabel" class="sectionsubhead" align="right" width="210"><!--L10NStart-->Company Name<!--L10NEnd--></td>
	<td id="viewAchBatchCompanuNamevlaue" class="columndata" align="left">
		<ffi:getProperty name="ACHBatch" property="achCompany.CompanyName" />&nbsp;-&nbsp;<ffi:getProperty name="ACHBatch" property="achCompany.CompanyID" />
	</td>
</tr> --%>
<%-- <ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
<tr>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
<tr class="lightBackground">
</ffi:cinclude>
	<td id="viewAchBatchTypeLabel" class="sectionsubhead" align="right"><!--L10NStart-->Batch Type<!--L10NEnd--></td>
	<td id="viewAchBatchTypeValue" class="columndata" align="left">
		<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ACHBatch" operator="equals">
			<s:property value="#session.ACHBatch.achClassCodeDescDisplayName"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${ACHBatch.ACHType}" value2="TaxPayment" operator="equals">
			<!--L10NStart-->Tax Payment (CCD + TXP Credit)<!--L10NEnd-->
		</ffi:cinclude>
		<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ChildSupportPayment" operator="equals">
			<!--L10NStart-->Child Support Payment (CCD + DED Credit)<!--L10NEnd-->
		</ffi:cinclude>
	</td>
</tr> --%>
<%-- <ffi:cinclude value1="${ACHBatch.ACHType}" value2="ChildSupportPayment" >
	<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
	<tr>
	</ffi:cinclude>
	<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
	<tr class="lightBackground">
	</ffi:cinclude>
		<td class="sectionsubhead" align="right"><!--L10NStart-->Child Support Type<!--L10NEnd--></td>
		<td class="columndata" colspan="7">
			<s:if test="%{'CHILDSUPPORT'.equals(#session.ACHBatch.TaxForm.Type)}" >
				<s:property value="#session.ACHBatch.TaxForm.State"/> <!--L10NStart-->State<!--L10NEnd-->
			</s:if>
			<s:property value="#session.ACHBatch.TaxForm.IRSTaxFormNumber"/> - <s:property value="#session.ACHBatch.TaxForm.TaxFormDescription"/> 
		</td>
	</tr>
</ffi:cinclude> --%>
</table>
<%-- <table width="720" border="0" cellspacing="0" cellpadding="3">
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
<tr>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
<tr class="lightBackground">
</ffi:cinclude>

    <td >
        <table width="720" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td width="60%"> --%>
<%--- first column of information --%>
                    <table>
<%-- <ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
                        <tr>
	<td id="viewAchBatchBatchNameLabel" class="sectionsubhead" align="right" width="210">
	<s:if test="%{!#attr.isTemplate}">
		<!--L10NStart-->Batch Name<!--L10NEnd-->
	</s:if>	
	<s:else>
		<!--L10NStart-->Template Name<!--L10NEnd-->
	</s:else>
	</td>

	<td id="viewAchBatchNameValue" class="columndata" colspan="1">
	
	<s:if test="%{!#attr.isTemplate}">
		<ffi:getProperty name="ACHBatch" property="Name"/>
</s:if>	
	<s:else>
		<ffi:getProperty name="ACHBatch" property="TemplateName"/>
</s:else>
	</td>
                        </tr>
</ffi:cinclude> --%>
<%-- <ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="equals" >
                        <tr>
                            <td class="sectionsubhead" align="right" width="210">
                                <!--L10NStart-->Template Name<!--L10NEnd-->
                            </td>

                            <td class="columndata" colspan="1">
                                <ffi:getProperty name="ACHBatch" property="TemplateName"/>
                            </td>
                        </tr>
</ffi:cinclude> --%>
                       <%--  <tr>
	<td id="viewAchBatchCompanyDescriptionLabel" class="sectionsubhead" align="right"><!--L10NStart-->Company Description<!--L10NEnd--></td>
	<td id="viewAchBatchCompanyDescriptionValue" class="columndata">
		<ffi:getProperty name="ACHBatch" property="CoEntryDesc"/>
	</td>
                        </tr> --%>
<%-- <s:if test="%{!@com.ffusion.beans.ach.ACHDefines@ACH_CLASSCODE_IAT.equals(#session.ACHBatch.StandardEntryClassCode)}">
    <tr>
	<td id="viewAchBatchDiscretionaryDataLabel" class="sectionsubhead" align="right">
			<!--L10NStart-->Discretionary Data<!--L10NEnd-->
	</td>
	<td id="viewAchBatchDiscretionaryDataValue" class="columndata">
			<ffi:getProperty name="ACHBatch" property="CoDiscretionaryData"/>
	</td>
	</tr>
</s:if>	
<s:else>
	<!-- IAT doesn't have Discretionary Data -->
    <tr>
	<td class="sectionsubhead" align="right">
			<!--L10NStart-->Originating Currency<!--L10NEnd-->
	</td>
	<td class="columndata">
			<!--L10NStart-->USD - United States Dollar<!--L10NEnd-->
	</td>
	</tr>
            <tr >
                <td align="right">
        <span class="sectionsubhead"><!--L10NStart-->Destination Country<!--L10NEnd--> </span>
                </td>
                <td class="columndata">
                   <s:property value="#session.ACHBatch.ISO_DestinationCountryDisplayName"/>
                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
            <tr>
                <td align="right">
        <span class="sectionsubhead"><!--L10NStart-->Destination Currency<!--L10NEnd--> </span>
                </td>
                <td class="columndata">
                    <s:property value="#session.ACHBatch.ISO_DestinationCurrencyDisplayName"/>
                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
            <tr>
                <td align="right">
        <span class="sectionsubhead"><!--L10NStart-->Foreign Exchange Method<!--L10NEnd--> </span>
                </td>
                <td class="columndata">
                 	<s:property value="#session.ACHBatch.foreignExchangeIndicatorDisplayName"/>
                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
</s:else> --%>
<%-- <ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
                        <tr>
	<td id="viewAchBatchEffectiveDateLabel" class="sectionsubhead" align="right">
<s:if test="%{!#attr.isTemplate}">
		<!--L10NStart-->Effective Date<!--L10NEnd-->
</s:if>
<s:else>
		<!--L10NStart-->Template Scope<!--L10NEnd-->
</s:else>
	</td>
	<td id="viewAchBatchEffectiveDateValue" class="columndata">
<s:if test="%{!#attr.isTemplate}">

        <ffi:getProperty name="ACHBatch" property="Date" />
</s:if>
<s:else>
        <ffi:getProperty name="ACHBatch" property="BatchScope" />
</s:else>
	</td>
                        </tr>
</ffi:cinclude> --%>
                      <%--   <tr>
<s:if test="%{#session.ACHBatch.achHeaderCompanyName != null}">
		<td id="viewAchBatchOriginalPayeeNameLabel" class="sectionsubhead" align="right">
			<s:property value="#session.ACHBatch.achHeaderCompanyName"/>
		</td>
		<td id="viewAchBatchOriginalPayeeNameValue" class="columndata">
			<ffi:getProperty name="ACHBatch" property="HeaderCompName"/>
		</td>
</s:if>		

                        </tr> --%>
<%-- <ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
	<tr>
		<td id="viewAchBatchStatusLabel" class="sectionsubhead" align="Right" ><!--L10NStart-->Status<!--L10NEnd--></td>
		<td id="viewAchBatchStatusValue" class="columndata"><ffi:getProperty name="ACHBatch" property="Status" /></td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td id="viewAchTrackingIdLabel" class="sectionsubhead" align="Right" ><!--L10NStart-->Tracking ID<!--L10NEnd--></td>
		<td id="viewAchTrackingIdValue" class="columndata"><ffi:getProperty name="ACHBatch" property="TrackingID" /></td>
		<td>&nbsp;</td>
	</tr>
</ffi:cinclude> --%>
                <!--     </table>
                </td>
                <td width="40%"> -->
<%--- SECOND column of information --%>
               <!--      <table> -->
                        <%-- <tr>
    <s:if test="%{#attr.isRecurringAvailable}">                    
		<td class="sectionsubhead tbrd_r" rowspan="4" width="10">&nbsp;</td>
	</s:if>
	<s:else>
		<td class="sectionsubhead" rowspan="4" width="10">&nbsp;</td>
	</s:else>
	<td class="sectionsubhead" rowspan="3" width="10">&nbsp;</td>
	<td id="viewAchBatchRepeatingLabel" class="sectionsubhead" align="right">
		<s:if test="%{#attr.isRecurringAvailable}">
			<!--L10NStart-->Repeating<!--L10NEnd-->
		</s:if>
	</td>
	<td id="viewAchBatchrepeatingValue" class="columndata">
		<s:if test="%{#attr.isRecurringAvailable}">
			<ffi:getProperty name="Frequency"/>
		</s:if>
	</td>
                        </tr>
                        <tr>
                       
	<td id="viewAchBatchCheckedLabel" class="sectionsubhead " align="right" colspan='2'>
		<s:if test="%{#attr.isRecurringAvailable}">
			<input id="viewAchBatchCheckedValue" type="radio" name="OpenEnded" disabled value="true" border="0" <ffi:cinclude value1="${NumberPayments}" value2="999">checked</ffi:cinclude>>
		</s:if>
	</td>
	<td id="viewAchBatchUnlimitedLabel" class="sectionsubhead">
		<s:if test="%{#attr.isRecurringAvailable}">
			<!--L10NStart-->Unlimited<!--L10NEnd-->
		</s:if>
	</td>
                        </tr>
                        <tr>
	<td class="sectionsubhead " align="right" colspan='2'>
		<s:if test="%{#attr.isRecurringAvailable}">
			<input id="viewAchBatchUnlimitedValue" disabled type="radio" name="OpenEnded" value="false" border="0" <ffi:cinclude value1="${NumberPayments}" value2="999" operator="notEquals" >checked</ffi:cinclude>>
		</s:if>
	</td>
	<td id="viewAchBatchofPaymentLabel" class="sectionsubhead">
		<s:if test="%{#attr.isRecurringAvailable}">
			<!--L10NStart--># of Payments<!--L10NEnd--> <ffi:getProperty name="NumberPayments"/>
		</s:if>
	</td>
                        </tr> --%>
                        <tr>
	
	<%-- <s:if test="%{@com.ffusion.beans.ach.ACHBatchTypes@BATCH_BALANCED == #session.ACHBatch.BatchTypeValue}">
		<s:if test="%{#session.ApprovalsViewTransactionDetails == null}">
			<td class="sectionsubhead " align="right" colspan="2"><!--L10NStart-->Offset Account<!--L10NEnd--></td>
			<td class="columndata" colspan="2">
        		<s:property value="#session.ACHBatch.OffsetAccount.Number"/>-<s:property value="#session.ACHBatch.OffsetAccount.Type"/>
        	</td>
		</s:if>
		<s:else>
			<td class="sectionsubhead " align="right" colspan="2">&nbsp;</td>
			<td colspan="2">&nbsp;</td>
		</s:else>
	</s:if> --%>
	<%-- <s:else>
		<td class="sectionsubhead " align="right">&nbsp;</td>
		<td colspan="2">&nbsp;</td>
	</s:else>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>


</tr>
</table> --%>
<table width="720" border="0" cellspacing="0" cellpadding="3">

<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
<tr>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
<tr class="lightBackground">
</ffi:cinclude>
</tr>