<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>




<div class="cutoffTimeMsgsHolder">
	<div class="cutoffTimeMsg">

		<s:text name="ach.payments.nextcutoff">
			<s:param>
				<s:property value="nextCutOffTime" />
			</s:param>
			<!-- Next Cutoff Time -->
		</s:text>
	</div>

	<s:if test="%{!nextCutOffTime.equals(finalCutOffTime)}">
		<div class="cutoffTimeMsg">

			<s:text name="ach.payments.finalcutoff">
				<!-- Final  Cutoff Time -->
				<s:param>
					<s:property value="finalCutOffTime" />
				</s:param>
			</s:text>
		</div>
	</s:if>

	<s:if test="showSameDayMessage == 'true'">
		<div class="cutoffTimeMsg">
			<s:text name="ach.payments.samedaynextcutoff">
				<s:param>
					<s:property value="sameDayNextCutOffTime" />
				</s:param>
				<!-- Same Cutoff Time -->
			</s:text>
		</div>
		<s:if test="%{!sameDayNextCutOffTime.equals(sameDayFinalCutOffTime)}">
			<div class="cutoffTimeMsg">
				<s:text name="ach.payments.samedayfinalcutoff">
					<!-- Same Final Cutoff Time -->
					<s:param>
						<s:property value="sameDayFinalCutOffTime" />
					</s:param>
				</s:text>
			</div>
		</s:if>
	</s:if>

</div>



