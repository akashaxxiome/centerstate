<%@ page import="com.ffusion.beans.ach.ACHPayee,
                 com.ffusion.beans.SecureUser"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- ----------------------------------------
	this file is used to display the payee view section for the following files:
		achpayeedelete.jsp
		achpayeeview.jsp
		achpayeeaddeditconfirm.jsp
	if you make changes, test with all of the files
	It also requires "AddEditACHPayee" in the session
--------------------------------------------- --%>

<%
	boolean isSecure = false;       // should secure information be shown?
%>
<ffi:cinclude value1="${AddEditACHPayee.DisplayAsSecurePayee}" value2="true" operator="equals">
<%
		isSecure = true;
%>
</ffi:cinclude>
<div class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header">Participant Summary</div>
		<div id="" class="paneContentWrapper summaryBlock">
			<div>
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><!--L10NStart-->Type:<!--L10NEnd--></span>
				<span id="viewAchPayeeParticipantTypeValue" class="inlineSection floatleft">
					<ffi:getProperty name="AddEditACHPayee" property="PayeeType"/>
				</span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span id="viewAchPayeeCompanyNameLabel" class="sectionsubhead sectionLabel floatleft inlineSection"><!--L10NStart-->Company Name:<!--L10NEnd--></span>
				<span id="viewAchPayeeCompanyNameValue" class="inlineSection floatleft">
					<ffi:getProperty name="ACHCompany" property="CompanyName" />&nbsp;-&nbsp;<ffi:getProperty name="ACHCompany" property="CompanyID" />
				</span>
			</div>
		</div>
	</div>
</div>

<div class="confirmPageDetails achPayeeDetailsDiv">
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Participant Details<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow payeViewDeleteInfo" style="display: none;">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Type:<!--L10NEnd--></span>
				<span id="viewAchPayeeParticipantTypeValue" class="columndata">
					<ffi:getProperty name="AddEditACHPayee" property="PayeeType"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="viewAchPayeeCompanyNameLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Company Name:<!--L10NEnd--></span>
				<span id="viewAchPayeeCompanyNameValue" class="columndata">
					<ffi:getProperty name="ACHCompany" property="CompanyName" />&nbsp;-&nbsp;<ffi:getProperty name="ACHCompany" property="CompanyID" />
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewAchPayeeParticipantNameLabel" class="sectionsubhead sectionLabel" style="display:inline-block">
					<!--L10NStart-->Name:<!--L10NEnd-->
				</span>
				<span id="viewAchPayeeParticipantNameValue" class="columndata">
					<% if (isSecure) { %>
					<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
					<% } else { %>
					<ffi:getProperty name="AddEditACHPayee" property="Name" />
					<% } %>
				</span>
			</div>
			<div class="inlineBlock">
					<span id="viewAchPayeeNickNameLabel" class="sectionsubhead sectionLabel" style="display:inline-block">
						<!--L10NStart-->Nickname:<!--L10NEnd-->
					</span>
					<span id="viewAchPayeeNickNameValue" class="columndata">
						<% if (isSecure) { // QTS 428174 %>
							<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
						<% } else { %>
							<ffi:getProperty name="AddEditACHPayee" property="NickName" />
						<% } %>
					</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
					<span id="viewAchPayeeIdentificationLabel" class="sectionsubhead sectionLabel">
						<!--L10NStart-->Identification #:<!--L10NEnd-->
					</span>
					<span id="viewAchPayeeIdentificationValue" class="columndata">
						<ffi:getProperty name="AddEditACHPayee" property="UserAccountNumber" />
					</span>
			</div>
			<div class="inlineBlock">
					<span id="viewAchPayeeParticipantScopeLabel" class="sectionsubhead sectionLabel">
						<!--L10NStart-->Scope:<!--L10NEnd-->
					</span>
					<span id="viewAchPayeeParticipantScopeValue" class="columndata" colspan="2">
						<ffi:getProperty name="AddEditACHPayee" property="PayeeGroup"/>
					</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
					<span id="viewAchPayeeSecureParticipantLabel" class="sectionsubhead sectionLabel">
						<!--L10NStart-->Secure Participant:<!--L10NEnd-->
					</span>
					<span id="viewAchPayeeSecureParticipantValue" class="columndata">
						<ffi:cinclude value1="${AddEditACHPayee.SecurePayee}" value2="true">
						<!--L10NStart-->True<!--L10NEnd-->
						</ffi:cinclude>
						<ffi:cinclude value1="${AddEditACHPayee.SecurePayee}" value2="false">
						<!--L10NStart-->False<!--L10NEnd-->
						</ffi:cinclude>
					</span>
			</div>
			<%-- only display Prenote information if Domestic (not IAT Payee) --%>
                <ffi:cinclude value1="${AddEditACHPayee.PayeeTypeValue}" value2="1" operator="equals">
				<div class="inlineBlock">
					<span id="viewAchPayeePrenoteLabel" class="sectionsubhead sectionLabel">
						<!--L10NStart-->Prenote:<!--L10NEnd-->
					</span>
					<span id="viewAchPayeePrenoteValue" class="columndata">

						<ffi:cinclude value1="PRENOTE_PENDING" value2="${AddEditACHPayee.PrenoteStatus}">
							<ffi:cinclude value1="SameDay Prenote" value2="${AddEditACHPayee.PrenoteType}">
								<!--L10NStart-->Same Day Requested<!--L10NEnd-->
							</ffi:cinclude>
							<ffi:cinclude value1="SameDay Prenote" value2="${AddEditACHPayee.PrenoteType}" operator="notEquals">
								<!--L10NStart-->Requested<!--L10NEnd-->
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="PRENOTE_REQUESTED" value2="${AddEditACHPayee.PrenoteStatus}">
							<ffi:cinclude value1="SameDay Prenote" value2="${AddEditACHPayee.PrenoteType}">
								<!--L10NStart-->Same Day Requested<!--L10NEnd-->
							</ffi:cinclude>
							<ffi:cinclude value1="SameDay Prenote" value2="${AddEditACHPayee.PrenoteType}" operator="notEquals">
								<!--L10NStart-->Requested<!--L10NEnd-->
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="PRENOTE_NOT_REQUIRED" value2="${AddEditACHPayee.PrenoteStatus}">Not Required</ffi:cinclude>
						<ffi:cinclude value1="PRENOTE_SUCCEEDED" value2="${AddEditACHPayee.PrenoteStatus}">Prenote Succeeded</ffi:cinclude>
						<ffi:cinclude value1="PRENOTE_FAILED" value2="${AddEditACHPayee.PrenoteStatus}">Prenote Failed</ffi:cinclude>
					</span>
				</div>
                </ffi:cinclude>
			</div>
			<%-- only display Address information if International --%>
            <ffi:cinclude value1="${AddEditACHPayee.PayeeTypeValue}" value2="2" operator="equals">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewAchPayeeDestinationCountryLabel" nowrap class="sectionsubhead sectionLabel">
                            <!--L10NStart-->Destination Country:<!--L10NEnd-->
                     </span>
                     <span id="viewAchPayeeDestinationCountryValue" class="columndata">
                         <ffi:getProperty name="AddEditACHPayee" property="DestinationCountryName"/>
                     </span>
				</div>
				<div class="inlineBlock">
					<span id="viewAchPayeeBranchCountryLabel" class="sectionsubhead sectionLabel">
                        <!--L10NStart-->Branch Country:<!--L10NEnd--> </span>
                    </span>
                    <span id="viewAchPayeeBranchCountryValue" class="columndata">
                        <ffi:getProperty name="AddEditACHPayee" property="BankCountryName"/>
                    </span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewAchPayeeCurrenyCodeLabel" nowrap class="sectionsubhead sectionLabel">
                        <!--L10NStart-->Currency Code:<!--L10NEnd-->
                    </span>
                    <span id="viewAchPayeeCurrencyCodeValue" class="columndata">
                        <ffi:getProperty name="AddEditACHPayee" property="CurrencyCode" />-<ffi:getProperty name="AddEditACHPayee" property="CurrencyCodeName"/>
                    </span>
				</div>
				<div class="inlineBlock">
					<span id="viewAchPayeeForignExchangeMethodLabel" nowrap class="sectionsubhead sectionLabel">
                        <!--L10NStart-->Foreign Exchange Method:<!--L10NEnd-->
                    </span>
                    <span id="viewAchPayeeForegineExchangeMethodValue" class="columndata">
                        <ffi:getProperty name="AddEditACHPayee" property="ForeignExchangeMethod" />-<ffi:getProperty name="AddEditACHPayee" property="ForeignExchangeMethodName"/>
                    </span>
				</div>
			</div>
        </ffi:cinclude>
		<%-- only display Address information if International --%>
        <ffi:cinclude value1="${AddEditACHPayee.PayeeTypeValue}" value2="2" operator="equals">
	        <div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewAchPayeeStreetAddressLabel" class="sectionsubhead sectionLabel">
	                    <!--L10NStart-->Street Address:<!--L10NEnd-->
	                </span>
	                <span id="viewAchPayeeStreetAddressValue" class="columndata">
	                    <ffi:getProperty name="AddEditACHPayee" property="Street"/>
	                </span>
				</div>
				<div class="inlineBlock">
					<span id="viewAchPayeeCityLabel" class="sectionsubhead sectionLabel">
	                    <!--L10NStart-->City:<!--L10NEnd-->
	                </span>
	                <span id="viewAchPayeeCityValue" class="columndata">
	                    <ffi:getProperty name="AddEditACHPayee" property="City"/>
	                </span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewAchPayeeStateProvinceLabel" class="sectionsubhead sectionLabel">
	                    <!--L10NStart-->State/Province:<!--L10NEnd-->
	                </span>
	                <span id="viewAchPayeeStateProvinceValue" class="columndata">
	                    <ffi:getProperty name="AddEditACHPayee" property="State"/>
	                </span>
				</div>
				<div class="inlineBlock">
					<span id="viewAchPayeeCountryLabel" class="sectionsubhead sectionLabel">
	                    <!--L10NStart-->Country:<!--L10NEnd-->
	                </span>
	                <span id="viewAchPayeeCountryValue" class="columndata">
	                    <ffi:getProperty name="AddEditACHPayee" property="CountryDisplayName"/>
	                    (<ffi:getProperty name="AddEditACHPayee" property="Country"/>)
	                </span>
				</div>
			</div>
			<div class="blockRow">
				<span id="viewAchPayeePostalCodeLabel" class="sectionsubhead sectionLabel">
                   <!--L10NStart-->Postal Code;<!--L10NEnd-->
               </span>
               <span id="viewAchPayeePostalCodeValue" class="columndata">
                   <ffi:getProperty name="AddEditACHPayee" property="ZipCode"/>
               </span>
			</div>
         </ffi:cinclude>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Bank Details<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewAchPayeeBankNameLabel" class="sectionsubhead sectionLabel">
					<!--L10NStart-->Bank Name:<!--L10NEnd-->
				</span>
				<span id="viewAchPayeeBankNameValue" class="columndata">
					<% if (isSecure) { %>
					<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
					<% } else { %>
					<ffi:getProperty name="AddEditACHPayee" property="BankName" />
					<% } %>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="viewAchPayeeBankIdentifierLabel" class="sectionsubhead sectionLabel">
					<!--L10NStart-->Bank Identifier:<!--L10NEnd-->
				</span>
				<span id="viewAchPayeeBankIdentifierValue" class="columndata">
				<% if (isSecure) { %>
					<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
				<% } else { %>
					<ffi:getProperty name="AddEditACHPayee" property="RoutingNumber" />
				<% } %>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewAchPayeeAccountLabel" class="sectionsubhead sectionLabel">
					<!--L10NStart-->Account #:<!--L10NEnd-->
				</span>
				<span id="viewAchPayeeAccountValue" class="columndata">
				<% if (isSecure) { %>
					<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
				<% } else { %>
					<ffi:getProperty name="AddEditACHPayee" property="AccountNumber" />
				<% } %>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="viewAchPayeeAccountTyeLabel" class="sectionsubhead sectionLabel">
					<!--L10NStart-->Account Type:<!--L10NEnd-->
				</span>
				<span id="viewAchPayeeAccountTypeValue" class="columndata">
				<% if (isSecure) { %>
					<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
				<% } else { %>
					<ffi:getProperty name="AddEditACHPayee" property="AccountType" />
				<% } %>
				</span>
			</div>
		</div>
		<%-- only display Bank Identifier Type information if International --%>
        <ffi:cinclude value1="${AddEditACHPayee.PayeeTypeValue}" value2="2" operator="equals">
		<div class="blockRow">
			<span id="viewAchPayeeBankIdentifierTypeLabel" class="sectionsubhead sectionLabel">
				<!--L10NStart-->Bank Identifier Type:<!--L10NEnd-->
			</span>
			<span id="viewAchPayeeBankIdentifierTypeValue" class="columndata">
				<% if (isSecure) { %>
					<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
				<% } else { %>
                      <ffi:getProperty name="AddEditACHPayee" property="BankIdentifierTypeDisplay"/>
				<% } %>
			</span>
		</div>
		</ffi:cinclude>
	</div>
</div>
</div>
<!-- 	<tr>
		<td class="columndata lightBackground">&nbsp;</td>
		<td class="columndata lightBackground" colspan="2">
			<div align="left">
			<table border="0" cellspacing="0" cellpadding="3"> -->
				<%-- <tr>
					<td id="viewAchPayeeParticipantTypeLabel" class="columndata lightBackground">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Participant Type<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeParticipantTypeValue" class="columndata" colspan="2">
						<ffi:getProperty name="AddEditACHPayee" property="PayeeType"/>
					</td>
				</tr>

				<tr class="lightBackground">
					<td id="viewAchPayeeCompanyNameLabel" class="sectionsubhead" align="right"><!--L10NStart-->Company Name<!--L10NEnd--></td>
					<td id="viewAchPayeeCompanyNameValue" class="columndata">
						<ffi:getProperty name="ACHCompany" property="CompanyName" />&nbsp;-&nbsp;<ffi:getProperty name="ACHCompany" property="CompanyID" />
					</td>
				</tr> --%>
				<%-- <tr>
					<td id="viewAchPayeeNickNameLabel" class="columndata lightBackground">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Nickname<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeNickNameValue" class="columndata">
<% if (isSecure) { // QTS 428174 %>
						<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
<% } else { %>
						<ffi:getProperty name="AddEditACHPayee" property="NickName" />
<% } %>
					</td>
				</tr>
				<tr>
					<td id="viewAchPayeeParticipantNameLabel" class="columndata lightBackground">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Participant Name<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeParticipantNameValue" class="columndata">
<% if (isSecure) { %>
						<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
<% } else { %>
						<ffi:getProperty name="AddEditACHPayee" property="Name" />
<% } %>
					</td>
				</tr> --%>
				<%-- <tr>
					<td id="viewAchPayeeIdentificationLabel" class="columndata lightBackground">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Identification #<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeIdentificationValue" class="columndata">
						<ffi:getProperty name="AddEditACHPayee" property="UserAccountNumber" />
					</td>
				</tr> --%>
<%-- only display Prenote information if Domestic (not IAT Payee) --%>
                <%-- <ffi:cinclude value1="${AddEditACHPayee.PayeeTypeValue}" value2="1" operator="equals">
				<tr>
					<td id="viewAchPayeePrenoteLabel" class="columndata lightBackground">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Prenote<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeePrenoteValue" class="columndata" colspan="2">
						<ffi:cinclude value1="PRENOTE_PENDING" value2="${AddEditACHPayee.PrenoteStatus}">Requested</ffi:cinclude>
						<ffi:cinclude value1="PRENOTE_REQUESTED" value2="${AddEditACHPayee.PrenoteStatus}">Requested</ffi:cinclude>
						<ffi:cinclude value1="PRENOTE_NOT_REQUIRED" value2="${AddEditACHPayee.PrenoteStatus}">Not Required</ffi:cinclude>
						<ffi:cinclude value1="PRENOTE_SUCCEEDED" value2="${AddEditACHPayee.PrenoteStatus}">Prenote Succeeded</ffi:cinclude>
						<ffi:cinclude value1="PRENOTE_FAILED" value2="${AddEditACHPayee.PrenoteStatus}">Prenote Failed</ffi:cinclude>
					</td>
				</tr>
                </ffi:cinclude> --%>
				<%-- <tr>
					<td id="viewAchPayeeParticipantScopeLabel" class="columndata lightBackground">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Participant Scope<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeParticipantScopeValue" class="columndata" colspan="2">
						<ffi:getProperty name="AddEditACHPayee" property="PayeeGroup"/>
					</td>
				</tr> --%>
				<%-- <tr>
					<td id="viewAchPayeeSecureParticipantLabel" class="columndata lightBackground">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Secure Participant<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeSecureParticipantValue" class="columndata" colspan="2">
						<ffi:getProperty name="AddEditACHPayee" property="SecurePayee"/>
					</td>
				</tr> --%>
<%-- only display Address information if International --%>
               <%--  <ffi:cinclude value1="${AddEditACHPayee.PayeeTypeValue}" value2="2" operator="equals">
                    <tr>
                        <td id="viewAchPayeeStreetAddressLabel" class="columndata lightBackground">
                            <div align="right"><span class="sectionsubhead"><!--L10NStart-->Street Address<!--L10NEnd--></span></div>
                        </td>
                        <td id="viewAchPayeeStreetAddressValue" class="columndata" colspan="2">
                            <ffi:getProperty name="AddEditACHPayee" property="Street"/>
                        </td>
                    </tr>
                    <tr>
                        <td id="viewAchPayeeCityLabel" class="columndata lightBackground">
                            <div align="right"><span class="sectionsubhead"><!--L10NStart-->City<!--L10NEnd--></span></div>
                        </td>
                        <td id="viewAchPayeeCityValue" class="columndata" colspan="2">
                            <ffi:getProperty name="AddEditACHPayee" property="City"/>
                        </td>
                    </tr>
                    <tr>
                        <td id="viewAchPayeeStateProvinceLabel" class="columndata lightBackground">
                            <div align="right"><span class="sectionsubhead"><!--L10NStart-->State/Province<!--L10NEnd--></span></div>
                        </td>
                        <td id="viewAchPayeeStateProvinceValue" class="columndata" colspan="2">
                            <ffi:getProperty name="AddEditACHPayee" property="State"/>
                        </td>
                    </tr>
                    <tr>
                        <td id="viewAchPayeeCountryLabel" class="columndata lightBackground">
                            <div align="right"><span class="sectionsubhead"><!--L10NStart-->Country<!--L10NEnd--></span></div>
                        </td>
                        <td id="viewAchPayeeCountryValue" class="columndata" colspan="2">
                            <ffi:getProperty name="AddEditACHPayee" property="CountryDisplayName"/>
                            (<ffi:getProperty name="AddEditACHPayee" property="Country"/>)
                        </td>
                    </tr>
                    <tr>
                        <td id="viewAchPayeePostalCodeLabel" class="columndata lightBackground">
                            <div align="right"><span class="sectionsubhead"><!--L10NStart-->Postal Code<!--L10NEnd--></span></div>
                        </td>
                        <td id="viewAchPayeePostalCodeValue" class="columndata" colspan="2">
                            <ffi:getProperty name="AddEditACHPayee" property="ZipCode"/>
                        </td>
                    </tr>
                </ffi:cinclude> --%>
			<!-- </table>
			</div>
		</td>
		<td class="tbrd_l lightBackground" colspan="2">
			<div align="left">
			<table border="0" cellspacing="0" cellpadding="3"> -->
<%-- only display Address information if International --%>
                <%-- <ffi:cinclude value1="${AddEditACHPayee.PayeeTypeValue}" value2="2" operator="equals">

                    <tr>
                        <td id="viewAchPayeeDestinationCountryLabel" nowrap>
                            <div align="right"><span class="sectionsubhead"><!--L10NStart-->Destination Country<!--L10NEnd--> </span></div>
                        </td>
                        <td id="viewAchPayeeDestinationCountryValue" class="columndata">
                            <ffi:getProperty name="AddEditACHPayee" property="DestinationCountryName"/>
                        </td>
                    </tr>
                    <tr>
                        <td id="viewAchPayeeBranchCountryLabel">
                            <div align="right"><span class="sectionsubhead"><!--L10NStart-->Branch Country<!--L10NEnd--> </span></div>
                        </td>
                        <td id="viewAchPayeeBranchCountryValue" class="columndata">
                            <ffi:getProperty name="AddEditACHPayee" property="BankCountryName"/>
                        </td>
                    </tr>
                    <tr>
                        <td id="viewAchPayeeCurrenyCodeLabel" nowrap>
                            <div align="right"><span class="sectionsubhead"><!--L10NStart-->Currency Code<!--L10NEnd--> </span></div>
                        </td>
                        <td id="viewAchPayeeCurrencyCodeValue" class="columndata">
                            <ffi:getProperty name="AddEditACHPayee" property="CurrencyCode" />-<ffi:getProperty name="AddEditACHPayee" property="CurrencyCodeName"/>
                        </td>
                    </tr>
                    <tr>
                        <td id="viewAchPayeeForignExchangeMethodLabel" nowrap>
                            <div align="right"><span class="sectionsubhead"><!--L10NStart-->Foreign Exchange Method<!--L10NEnd--> </span></div>
                        </td>
                        <td id="viewAchPayeeForegineExchangeMethodValue" class="columndata">
                            <ffi:getProperty name="AddEditACHPayee" property="ForeignExchangeMethod" />-<ffi:getProperty name="AddEditACHPayee" property="ForeignExchangeMethodName"/>
                        </td>
                    </tr>


                </ffi:cinclude> --%>
				<%-- <tr>
					<td id="viewAchPayeeBankNameLabel" class="columndata">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Bank Name<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeBankNameValue" class="columndata">
<% if (isSecure) { %>
						<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
<% } else { %>
						<ffi:getProperty name="AddEditACHPayee" property="BankName" />
<% } %>
					</td>
				</tr>
				<tr>
					<td id="viewAchPayeeBankIdentifierLabel" class="columndata">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Bank Identifier<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeBankIdentifierValue" class="columndata">
<% if (isSecure) { %>
						<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
<% } else { %>
						<ffi:getProperty name="AddEditACHPayee" property="RoutingNumber" />
<% } %>
					</td>
				</tr>
only display Bank Identifier Type information if International
                <ffi:cinclude value1="${AddEditACHPayee.PayeeTypeValue}" value2="2" operator="equals">
				<tr>
					<td id="viewAchPayeeBankIdentifierTypeLabel" class="columndata">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Bank Identifier Type<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeBankIdentifierTypeValue" class="columndata">
<% if (isSecure) { %>
						<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
<% } else { %>
                        <ffi:getProperty name="AddEditACHPayee" property="BankIdentifierTypeDisplay"/>
<% } %>
					</td>
				</tr>
                </ffi:cinclude>
				<tr>
					<td id="viewAchPayeeAccountLabel" class="columndata">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Account #<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeAccountValue" class="columndata">
<% if (isSecure) { %>
						<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
<% } else { %>
						<ffi:getProperty name="AddEditACHPayee" property="AccountNumber" />
<% } %>
					</td>
				</tr>
				<tr>
					<td id="viewAchPayeeAccountTyeLabel" class="columndata">
						<div align="right"><span class="sectionsubhead"><!--L10NStart-->Account Type<!--L10NEnd--></span></div>
					</td>
					<td id="viewAchPayeeAccountTypeValue" class="columndata">
<% if (isSecure) { %>
						<ffi:getProperty name="AddEditACHPayee" property="SecurePayeeMask"/>
<% } else { %>
						<ffi:getProperty name="AddEditACHPayee" property="AccountType" />
<% } %>
					</td>
				</tr> --%>
			<!-- </table>
			</div>
		</td>
	</tr> -->
