<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div class="achCalendarDetails calendarDetails">
<ffi:setProperty name="displayCurrency" value="${SecureUser.BaseCurrency}"/>

<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
	<ffi:cinclude value1="${APPROVALS_CB_PAYMENTS}" value2="true" operator="notEquals">
		<table class="ltrow4_color" width="720" border="0" cellspacing="0" cellpadding="3">
	</ffi:cinclude>
	<ffi:cinclude value1="${APPROVALS_CB_PAYMENTS}" value2="true" operator="equals">
		<table class="ltrow2_color" width="720" border="0" cellspacing="0" cellpadding="3">
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
	<table width="720" border="0" cellspacing="0" cellpadding="3">
</ffi:cinclude>
<!--     <tr class='ach_details_background_color'>
	    <td class="sectionhead" colspan="6">&nbsp;</td>
    </tr> -->

    <%-- include batch header --%>
    <s:include value="include-view-achbatch-header.jsp"></s:include>
<div id="ViewACHEntries" class="marginTop10 achCalendarDetails">
 <s:action name="viewAchBatchEntries" namespace="/pages/jsp/ach" executeResult="true">
 	<s:if test="%{#attr.isReverse}">
 		<s:param name="isReverse" value="true"></s:param>
 	</s:if>
 </s:action>

</div>
<div class="blockWrapper label130 achCalendarDetails">
	<div  class="blockHead"><!--L10NStart-->Entries<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total # Credits<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="ACHBatch" property="TotalNumberCredits"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total # Debits<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="ACHBatch" property="TotalNumberDebits"/></span>
			</div>
		</div>

		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total Credits<!--L10NEnd--> (<ffi:getProperty name="displayCurrency"/>)</span>
				<span class="columndata"><ffi:getProperty name="ACHBatch" property="TotalCreditAmount" /></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total Debits<!--L10NEnd--> (<ffi:getProperty name="displayCurrency"/>)</span>
				<span class="columndata"><ffi:getProperty name="ACHBatch" property="TotalDebitAmount" /></span>
			</div>
		</div>
	</div>
</div>

<div>
          <%-- we only want to reverse ACH batches, not TAX or CHILD SUPPORT
        we don't want to reverse FAILED payments, only status of completed, we also don't want to reverse REVERSAL batches--%>
        <s:if test="%{#session.ACHBatch.canReverse}">
          <input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" value="REVERSE" onclick="ns.ach.reverse(); return false;">
        </s:if>&nbsp;
        </div>
<s:if test="%{@com.ffusion.beans.ach.ACHBatchTypes@ENTRY_BALANCED == #session.ACHBatch.BatchTypeValue}">
	<ffi:list collection="ACHBatch.OffsetAccounts" items="offsetAcct">

	<div class="blockWrapper label130 achCalendarDetails">
		<div  class="blockHead">Offset Entries: <ffi:getProperty name="offsetAcct" property="RoutingNum"/>:<ffi:getProperty name="offsetAcct" property="Number"/>:<ffi:getProperty name="offsetAcct" property="NickName"/>(<ffi:getProperty name="offsetAcct" property="Type"/>)</div>
		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total # Credits<!--L10NEnd--></span>
					 <span class="columndata"><ffi:getProperty name="offsetAcct" property="TotalNumberCredits"/></span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total # Debits<!--L10NEnd--></span>
					<span class="columndata"><ffi:getProperty name="offsetAcct" property="TotalNumberDebits"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total Credits<!--L10NEnd--> (<ffi:getProperty name="displayCurrency"/>)</span>
					<span class="columndata"><ffi:getProperty name="offsetAcct" property="TotalCreditAmount" /></span>

				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total Debits<!--L10NEnd--> (<ffi:getProperty name="displayCurrency"/>)</span>
					<span class="columndata"><ffi:getProperty name="offsetAcct" property="TotalDebitAmount" /></span>
				</div>
			</div>
		</div>
	</div>
	</ffi:list>
	</s:if>
	<s:if test="%{@com.ffusion.beans.ach.ACHBatchTypes@BATCH_BALANCED == #session.ACHBatch.BatchTypeValue}">

		<div class="blockWrapper label130 achCalendarDetails">
		<div  class="blockHead">Offset Entries: <ffi:getProperty name="ACHBatch" property="OffsetAccount.RoutingNum"/>:<ffi:getProperty name="ACHBatch" property="OffsetAccount.Number"/>:<ffi:getProperty name="ACHBatch" property="OffsetAccount.NickName"/>(<ffi:getProperty name="ACHBatch" property="OffsetAccount.Type"/>)</div>
		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total # Credits<!--L10NEnd--></span>
					<span class="columndata"><ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalNumberCredits"/></span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total # Debits<!--L10NEnd--></span>
					<span class="columndata"><ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalNumberDebits"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total Credits<!--L10NEnd--> (<ffi:getProperty name="displayCurrency"/>)</span>
					<span class="columndata"><ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalCreditAmount" /></span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Total Debits<!--L10NEnd--> (<ffi:getProperty name="displayCurrency"/>)</span>
					<span class="columndata"><ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalDebitAmount" /></span>
				</div>
			</div>
		</div>
	</div>
	</s:if>
</table>
</table>
<ffi:removeProperty name="displayCurrency"/>
</div>
<%-- include batch entries --%>
<div class="marginTop10">
   <%--  <s:include value="include-view-achbatch-entries.jsp"/> --%>
   <s:action name="viewAchBatchEntriesInit" namespace="/pages/jsp/ach" executeResult="true"/>
</div>
