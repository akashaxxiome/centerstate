<%@ page import="com.ffusion.beans.ach.ACHPayee,
				 com.ffusion.beans.SecureUser,
				 com.ffusion.beans.ach.ACHEntry"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<%-- ----------------------------------------
	this file is used to display the entries portion for the following files:
		achbatchdelete.jsp
		achbatchview.jsp
		achbatch-reverse.jsp
	if you make changes, test with all of the files
	it requires being inside of a form named ViewForm
	It also requires "ACHBatch" in the session
--------------------------------------------- --%>
<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
<ffi:setProperty name="SortImage" property="NoSort" value='<img src="${sort_off_gif}" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="${sort_asc_gif}" alt="Reverse Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="${sort_desc_gif}" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="ACHEntries"/>
<ffi:process name="SortImage"/>

<script language="JavaScript">
<!--

function doSort(urlString, isReverse)
{
	refreshACHBatchEntries(urlString, isReverse);
	return false;
}
function refreshACHBatchEntries( urlString, isReverse ){
    if(isReverse === 'true'){
    	//Check if any query parameter is already added or not
	    if(urlString.indexOf("?") != -1){
	    	urlString = urlString + "&isReverse=true";
	    } else {
	    	urlString = urlString + "?isReverse=true";
	    }
    }

    $.ajax({
        url: urlString,
        success: function(data){
        	$('#ViewACHEntriesTable').html('');
            $('#ViewACHEntries').html(data);
        }
    });
}
function setReverseCheckbox( param ){
    var urlString = "/cb/pages/jsp/ach/achbatch-reverse-send.jsp";
    if (param != null)
        urlString = urlString + param;
    $.ajax({
        url: urlString
//        success: function(data){
//            $('#ViewACHEntries').html(data);
//        }
    });
}



function doSearch(isReverse)
{
	var urlString = "/cb/pages/jsp/ach/viewAchBatchEntries.action";
    var srch = document.getElementById("SearchString").value;
    $.ajax({
        type: "POST",
        url: urlString,
        data: {SearchString: srch, DoSearch: true, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>',isReverse:isReverse},
        success: function(data){
            $('#ViewACHEntries').html(data);
        }
    });
    return false;
}
function gotoPage(page)
{
    var urlString = "/cb/pages/jsp/ach/viewAchBatchEntries.action";
    $.ajax({
        type: "POST",
        url: urlString,
        data: {pageNumber: page.value, GoToPage: true, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>',isReverse:isReverse},
        success: function(data){
            $('#ViewACHEntries').html(data);
        }
    });
    return false;
}
// --></script>

<s:set var="spanCols" value="10"/>
<s:if test="%{#attr.isReverse}">
<s:set var="spanCols" value="11"/>
</s:if>


<table width="100%" border="0" cellspacing="0" cellpadding="1" class="tableAlerternateRowColor tdWithPadding" id="ViewACHEntriesTable">
	<s:if test="%{#session.ACHBatch.ACHEntries.IsPaged}">
		<tr>

			<td colspan="8">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="sectionhead" width="30%">&nbsp;</td>

						<td class="sectionsubhead" valign="center" width="34%"><input
							class="ui-widget-content ui-corner-all txtbox" type="text"
							id="SearchString" name="SearchString"
							value="<ffi:getProperty name="SearchString" />" size="25">
							&nbsp; <input
							class="submitbutton ui-widget-content ui-corner-all txtbox"
							type="button" value="SEARCH" onClick="doSearch(,'<s:property value="#attr.isReverse"/>');return false;">
							<br> <!--L10NStart-->Total filtered entries:<!--L10NEnd-->
							<ffi:getProperty name="ACHEntries" property="FilteredSize" /></td>
						<td class="sectionsubhead" width="36%">
						<s:if test="%{!#session.ACHBatch.ACHEntries.MaximumPageNumber.equals('0')}">
								<script>ns.ach.OnClickGetPrevPageUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?PreviousPage=true'/>"</script>
								<script>ns.ach.OnClickGetNextPageUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?NextPage=true'/>"</script>
								<input
									class="submitbutton ui-widget-content ui-corner-all txtbox"
									type="button"
									<ffi:cinclude value1="false" value2="${ACHBatch.ACHEntries.HasPrevPage}">disabled</ffi:cinclude>
									value="PREV PAGE"
									onClick="refreshACHBatchEntries(ns.ach.OnClickGetPrevPageUrl, ,'<s:property value="#attr.isReverse"/>');">
            &nbsp;
            <input
									class="submitbutton ui-widget-content ui-corner-all txtbox"
									type="button"
									<ffi:cinclude value1="false" value2="${ACHBatch.ACHEntries.HasNextPage}">disabled</ffi:cinclude>
									value="NEXT PAGE"
									onClick="refreshACHBatchEntries(ns.ach.OnClickGetNextPageUrl, ,'<s:property value="#attr.isReverse"/>');">
								<br>
								<!--L10NStart-->Page <!--L10NEnd-->
								<ffi:getProperty name="ACHEntries" property="CurrentPageNumber" />
								<!--L10NStart--> of <!--L10NEnd-->
								<ffi:getProperty name="ACHEntries" property="MaximumPageNumber" />
            &nbsp;&nbsp;
            <!--L10NStart-->Go to page:<!--L10NEnd-->
								<select class="ui-widget-content ui-corner-all txtbox"
									name="pageNumber" onchange="gotoPage(this, ,'<s:property value="#attr.isReverse"/>')">
									<ffi:object id="Paging" name="com.ffusion.beans.util.Paging"
										scope="session" />
									<ffi:setProperty name="Paging" property="Pages"
										value="${ACHBatch.ACHEntries.MaximumPageNumber}" />
									<ffi:list collection="Paging" items="PageNum">
										<option value='<ffi:getProperty name="PageNum"/>'
											<ffi:cinclude value1="${ACHBatch.ACHEntries.CurrentPageNumber}" value2="${PageNum}" >selected</ffi:cinclude>>
											<ffi:getProperty name="PageNum" />
										</option>
									</ffi:list>
								</select>
								</s:if>
							<s:if test="%{#session.ACHBatch.ACHEntries.MaximumPageNumber.equals('0}}">
								value2="0" operator="equals">
								<!--L10NStart-->All entries are hidden by SEARCH field<!--L10NEnd-->
							</s:if></td>
					</tr>
				</table>
			</td>
		</tr>
		</s:if>

	<ffi:setProperty name="sort_asc_gif" value="/cb/web/multilang/grafx/common/i_sort_asc.gif"/>
<ffi:setProperty name="sort_desc_gif" value="/cb/web/multilang/grafx/common/i_sort_desc.gif"/>
<ffi:setProperty name="sort_off_gif" value="/cb/web/multilang/grafx/common/i_sort_off.gif"/>

<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
<ffi:setProperty name="SortImage" property="NoSort" value='<img src="${sort_off_gif}" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="${sort_asc_gif}" alt="Reverse Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="${sort_desc_gif}" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="ACHEntries"/>
<ffi:process name="SortImage"/>
    <tr class="resetTableRowBGColor">
    <s:if test="%{isReverse}">
	    <td class="columndata" width="50">
		    <span class="sectionsubhead"><!--L10NStart-->Reverse<!--L10NEnd--></span>
	    </td>
    </s:if>
    <td id="viewAchBatchParticipantLabel" class="columndata" width="70">
        <% String participantNameTitle = "<!--L10NStart-->Participant<!--L10NEnd-->";
           String userAccountNumberTitle = "<!--L10NStart-->Participant ID<!--L10NEnd-->";
        %>
    <s:if test="%{'WEB0'.equals(#session.ACHBatch.StandardEntryClassCodeDebitBatch)}">
        <% participantNameTitle = "<!--L10NStart-->Receiver<!--L10NEnd-->";
           userAccountNumberTitle = "<!--L10NStart-->Sender<!--L10NEnd-->";
        %>
    </s:if>


    <s:if test="%{!'XCK'.equals(#session.ACHBatch.StandardEntryClassCode)}">
        <span class="sectionsubhead">
            <%= participantNameTitle %>
        </span><br>
	    <script>ns.ach.SortByPayeeNameAndIDUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=PAYEENAME,ID'/>"</script>
	    <ffi:setProperty name="SortImage" property="Compare" value="PAYEENAME,ID"/>
	    <a onclick="doSort(ns.ach.SortByPayeeNameAndIDUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
    </s:if>
    <s:else>
		<span class="sectionsubhead"><!--L10NStart-->Item Research Number<!--L10NEnd--></span>
		<script>ns.ach.SortByItemResearchNumUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=ITEMRESEARCHNUMBER,ID'/>"</script>
		<ffi:setProperty name="SortImage" property="Compare" value="ITEMRESEARCHNUMBER,ID"/>
	    <br><a onclick="doSort(ns.ach.SortByItemResearchNumUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	</s:else>
	</td>
		<s:if test="%{showParticipantID}">
			<td id="viewAchBatchParticipantIdLabel" class="sectionsubhead" width="48">
                <%= userAccountNumberTitle %>
			<script>ns.ach.SortByParticipantIDUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=USERACCOUNTNUMBER,ID'/>"</script>
			<ffi:setProperty name="SortImage" property="Compare" value="USERACCOUNTNUMBER,ID"/>
            <br><a onclick="doSort(ns.ach.SortByParticipantIDUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
           </td>
		</s:if>
		<td id="viewAchBatchAbaLabel" class="sectionsubhead" width="48"><!--L10NStart-->ABA #<!--L10NEnd-->
		<script>ns.ach.SortByABAUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=ROUTINGNUM,ID'/>"</script>
		<ffi:setProperty name="SortImage" property="Compare" value="ROUTINGNUM,ID"/>
	    <br><a onclick="doSort(ns.ach.SortByABAUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a></td>
		<td id="viewAchBatchAccountLabel" class="sectionsubhead" width="48"><!--L10NStart-->Account #<!--L10NEnd-->
		<script>ns.ach.SortByAccountNumberUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=ACCOUNTNUMBER,ID'/>"</script>
		<ffi:setProperty name="SortImage" property="Compare" value="ACCOUNTNUMBER,ID"/>
	    <br><a onclick="doSort(ns.ach.SortByAccountNumberUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a></td>
		<td id="viewAchAccountTypeLabel" class="sectionsubhead" width="30"><!--L10NStart-->Account Type<!--L10NEnd-->
		<script>ns.ach.SortByAccountTypeUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=ACCOUNTTYPE,ID'/>"</script>
		<ffi:setProperty name="SortImage" property="Compare" value="ACCOUNTTYPE,ID"/>
	    <br><a onclick="doSort(ns.ach.SortByAccountTypeUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a></td>
	<td id="viewAchBatchCheckSerialNumberLabel" class="columndata" width="20">
	<s:if test="%{checkSerNum}">
		<span class="sectionsubhead"><!--L10NStart-->Check Serial#<!--L10NEnd--></span>
		<ffi:setProperty name="SortImage" property="Compare" value="CheckSerialNumber,ID"/>
		<script>ns.ach.SortByCheckSerialNumberUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=CheckSerialNumber,ID'/>"</script>
	    <br><a onclick="doSort(ns.ach.SortByCheckSerialNumberUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	</s:if>
	&nbsp;</td>
	<td id="viewAchBatchDDLabel" class="columndata" width="15">
	<s:if test="%{showDiscretionaryData}">
		<span class="sectionsubhead"><!--L10NStart-->DD<!--L10NEnd--></span>
		<ffi:setProperty name="SortImage" property="Compare" value="DISCRETIONARYDATA,ID"/>
		<script>ns.ach.SortByDiscretionaryDataUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=DISCRETIONARYDATA,ID'/>"</script>
	    <br><a onclick="doSort(ns.ach.SortByDiscretionaryDataUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	</s:if>
	&nbsp;</td>
	<td id="viewAchAmountLabel" class="columndata" width="35">
		<span class="sectionsubhead"><!--L10NStart-->Amount<!--L10NEnd--></span>
		<ffi:setProperty name="SortImage" property="Compare" value="AMOUNT,ID"/>
		<script>ns.ach.SortByAmountUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=AMOUNT,ID'/>"</script>
	    <br><a onclick="doSort(ns.ach.SortByAmountUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	</td>
	<td id="viewAchBatchTypeLabel" class="columndata" width="25">
		<span class="sectionsubhead"><!--L10NStart-->Type<!--L10NEnd--></span>
		<ffi:setProperty name="SortImage" property="Compare" value="TYPE,ID"/>
		<script>ns.ach.SortByTypeUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatchEntries.action?ToggleSortedBy=TYPE,ID'/>"</script>
	    <br><a onclick="doSort(ns.ach.SortByTypeUrl,'<s:property value="#attr.isReverse"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
	</td>
	<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
			<td class="sectionsubhead"  width="10%"><!--L10NStart-->Tax Type<!--L10NEnd--></td>
			<% String taxformid; %>
			<ffi:getProperty name="ACHEntry" property="TaxFormID" assignTo="taxformid" />
			<% if (taxformid == null) { %>
				<ffi:getProperty name="ACHBatch" property="TaxFormID" assignTo="taxformid" />
			<% } %>
			<ffi:object name="com.ffusion.tasks.ach.GetTaxForm" id="TaxForm" scope="session"/>
			<ffi:setProperty name="TaxForm" property="ID" value="<%=taxformid%>" />
			<ffi:process name="TaxForm"/>
	</ffi:cinclude>
    </tr>
   <% int idCount=0; %>

	<s:set var="isSecure" value="false" />
	<s:set var="CanExportBatch" value="true" scope="request"/>
	<s:set var="disableCheckbox" value="disabled"/>
	<s:set var="styleColor" value="" scope="action"/>

	<!-- In case of Reverse batch operation refer entries directly from session. -->
	<s:if test="%{#attr.isReverse}">
		<s:set var="Entries" value="#session.ACHEntries"/>
	</s:if>
	<s:else>
		<s:set var="Entries" value="#session.ACHBatch.ACHEntries"/>
	</s:else>
	<s:iterator value="Entries" var="ACHEntry">
	<tr>
	<s:if test="%{#ACHEntry.AchPayee != null && #ACHEntry.AchPayee.DisplayAsSecurePayeeValue}">
		<s:set var="isSecure" value="true"/>
		<s:set var="CanExportBatch" value="false" scope="request"/>
	</s:if>
	<s:else>
		<s:set var="isSecure" value="false"/>
	</s:else>

	<s:if test="%{#attr.isReverse}">
	    <td class="columndata">
            <div class="columndata" align="left">
            <s:if test="%{#ACHEntry.canReverseValue}">
            	<s:set var="disableCheckbox" value=""/>
            </s:if>
            <s:else>

            	<s:set var="disableCheckbox" value="%{'disabled'}"/>
            </s:else>
            	<input id="hold<ffi:getProperty name='ACHEntry' property='ID'/>" type="checkbox" <s:property value="#attr.disableCheckbox"/> name="checkbox" onclick="changeHold(this,'<ffi:getProperty name='ACHEntry' property='ID'/>');" <ffi:cinclude value1="${ACHEntry.active}" value2="true">checked</ffi:cinclude>>
           	</div>
        </td>
   </s:if>

	<%-- Start: Dual approval processing--%>
	<s:set var="styleColor" value="" scope="action"/>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
    <s:if test="%{!#attr.isReverse}">
        <ffi:cinclude value1="${ACHBatch.IsCompletedStatus}" value2="true" operator="notEquals">
            <ffi:cinclude value1="${ACHBatch.CoEntryDesc}" value2="REVERSAL" operator="notEquals">
                <ffi:cinclude value1="${ACHEntry.AchPayee.Scope}" value2="ACHBatch">
                    <s:set var="styleColor" value="%{'red'}" scope="action"/>
                </ffi:cinclude>
                <ffi:cinclude value1="${ACHEntry.AchPayee.Scope}" value2="ACHTemplate">
                	<s:set var="styleColor" value="%{'red'}" scope="action"/>
                </ffi:cinclude>
            </ffi:cinclude>
        </ffi:cinclude>
   </s:if>
    </ffi:cinclude>
	<%-- End: Dual approval processing--%>
		<td id="viewAchBatchPayeeNameValue<%=idCount%>" style="color:<s:property value="%{#attr.styleColor}"/>" class="columndata">
		<s:if test="%{!'XCK'.equals(#session.ACHBatch.StandardEntryClassCode)}">
			<ffi:cinclude value1="${ACHEntry.AchPayee.NickName}" value2="" operator="notEquals" >
				<s:if test="%{#isSecure}">
					<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
				</s:if>
				<s:else>
						<ffi:getProperty name="ACHEntry" property="AchPayee.NickName"/>
				</s:else>
			</ffi:cinclude>
			<ffi:cinclude value1="${ACHEntry.AchPayee.NickName}" value2="" operator="equals" >
				<s:if test="%{#isSecure}">
					<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
				</s:if>
				<s:else>
					<ffi:getProperty name="ACHEntry" property="AchPayee.Name"/>
				</s:else>
			</ffi:cinclude>
	</s:if>
	<s:else>
			<ffi:getProperty name="ACHEntry" property="ItemResearchNumber"/>
	</s:else>
		</td>


		<s:if test="%{showParticipantID}">
			<td id="viewAchBatchParticipantIdValue<%=idCount%>" class="columndata"><ffi:getProperty name="ACHEntry" property="AchPayee.UserAccountNumber"/></td>
		</s:if>

			<td id="viewAchBatchAbaValue<%=idCount%>" class="columndata">
<s:if test="%{#isSecure}">
				<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
</s:if>
<s:else>
				<ffi:getProperty name="ACHEntry" property="AchPayee.RoutingNumber"/>
</s:else>
			</td>
			<td id="viewAchBatchAccountNumberValue<%=idCount%>"  class="columndata">
<s:if test="%{#isSecure}">
				<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
</s:if>
<s:else>
				<ffi:getProperty name="ACHEntry" property="AchPayee.AccountNumber"/>
</s:else>
			</td>
			<td id="viewAchBatchAccountTypeValue<%=idCount%>" class="columndata">
<s:if test="%{#isSecure}">
				<ffi:getProperty name="ACHEntry" property="AchPayee.SecurePayeeMask"/>
</s:if>
<s:else>
				<ffi:getProperty name="ACHEntry" property="AchPayee.AccountType"/>
</s:else>
			</td>
		<td id="viewAchBatchCheckSerialNumberValue<%=idCount%>" class="columndata">
		<s:if test="%{checkSerNum}">
			<ffi:getProperty name="ACHEntry" property="CheckSerialNumber"/>
		</s:if>
		&nbsp;</td>
		<td id="viewAchBatchDiscreationaryDataValue<%=idCount%>" class="columndata">
		<s:if test="%{showDiscretionaryData}">
			<ffi:getProperty name="ACHEntry" property="DiscretionaryData"/>
		</s:if>
		&nbsp;</td>
		<td id="viewAchBatchAmountValue<%=idCount%>" class="columndata">
			<ffi:getProperty name="ACHEntry" property="AmountValue.CurrencyStringNoSymbol"/>
		</td>
		<td id="viewAchBatchTypeValue<%=idCount++%>" class="columndata">
			<span class="columndata" align="center">
			<ffi:cinclude value1="${ACHEntry.AmountIsDebit}" value2="true"><!--L10NStart-->Debit<!--L10NEnd--></ffi:cinclude>
			<ffi:cinclude value1="${ACHEntry.AmountIsDebit}" value2="false"><!--L10NStart-->Credit<!--L10NEnd--></ffi:cinclude>
			</span>
		</td>
		<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
			<td width="10%" class="columndata" colspan="<s:property value="#attr.spanCols"/>">
				<ffi:cinclude value1="${TaxForm.type}" value2="STATE"><ffi:getProperty name="TaxForm" property="state" /> <!--L10NStart-->State<!--L10NEnd--></ffi:cinclude>
				<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /> - <ffi:getProperty name="TaxForm" property="TaxFormDescription" />
			</td>
		</ffi:cinclude>
	</tr>

	<%-- Start: Dual approval processing--%>
	<ffi:cinclude value1="${ACHEntry.PayeeDAStatus}" value2="<%=IDualApprovalConstants.STATUS_TYPE_PENDING_APPROVAL%>">
	<tr>
		<td class="sectionheadDA" colspan="5">
			&nbsp;&nbsp;&nbsp;<ffi:getL10NString rsrcFile="cb" msgKey="da.errormessage.ach.ACHParticipantIsPendingApproval"/>
		</td>
	</tr>
	</ffi:cinclude>
	<%-- End: Dual approval processing--%>

	<ffi:cinclude value1="${ACHBatch.BatchType}" value2="Entry Balanced" operator="equals">
	    <tr>
		    <td class="columndata" colspan="<s:property value="#attr.spanCols"/>" align="center">
			    <span class="sectionsubhead"><!--L10NStart-->Offset Account<!--L10NEnd-->: </span>
			    <ffi:getProperty name="ACHEntry" property="OffsetAccountNumber" /> - <ffi:getProperty name="ACHEntry" property="OffsetAccountType" />
		    </td>
	    </tr>
	</ffi:cinclude>


	<%-- Need to check if the ACH Company is Addenda Entitled --%>
		<ffi:cinclude value1="${ACHBatch.TemplateScope}" value2="" operator="notEquals">
    		<ffi:cinclude value1="${ACHBatch.TemplateID}" value2="" operator="equals">
       			 <s:set var="isTemplate" value="true" scope="request"/>
   			</ffi:cinclude>
		</ffi:cinclude>
		<s:if test="%{#attr.isTemplate}">
        	<ffi:setProperty name="ACHBatch" property="AchCompany.CurrentClassCodeTemplate" value="${ACHBatch.StandardEntryClassCode}" />
		</s:if>
		<s:else>
        	<ffi:setProperty name="ACHBatch" property="AchCompany.CurrentClassCode" value="${ACHBatch.StandardEntryClassCode}" />
		</s:else>
		<ffi:cinclude value1="${ACHBatch.AchCompany.ClassCodeAddendaEntitled}" value2="TRUE" operator="equals" >

			<ffi:cinclude value1="CTX" value2="${ACHBatch.StandardEntryClassCode}" operator="notEquals">

				<s:set var="showAddendaTitle" value="%{'false'}"/>
				<s:set var="breakloop" value="%{'false'}"/>
				<s:iterator value="#ACHEntry.Addendas" var="Addenda">

				<s:if test="%{#breakloop=='false'}">

					<s:if test="%{#Addenda.PmtRelatedInfo!=null && #Addenda.PmtRelatedInfo!=''}">

						<s:set var="breakloop" value="%{'true'}"/>
						<s:set var="showAddendaTitle" value="%{'true'}"/>
					</s:if>
				</s:if>
				</s:iterator>

				<s:if test="%{#showAddendaTitle=='true'}">
				<tr style="background: transparent none repeat scroll 0 0;">
					<td><div class="blockHead"><!--L10NStart-->Addenda<!--L10NEnd--></div></td>
				</tr>
				</s:if>

				<ffi:list collection="ACHEntry.Addendas" items="ACHAddenda">

				<tr>
					<td colspan="<s:property value="#attr.spanCols"/>" class="">
						<%-- <input class="" type="text" name="textfield5" value="<ffi:getProperty name="ACHAddenda" property="PmtRelatedInfo"/>" disabled size="110"> --%>
						<span class="addendaSpan"><ffi:getProperty name="ACHAddenda" property="PmtRelatedInfo"/></span>
					</td>
				</tr>
				</ffi:list>
			</ffi:cinclude>
			<ffi:cinclude value1="CTX" value2="${ACHBatch.StandardEntryClassCode}" operator="equals">
			<ffi:list collection="ACHEntry.Addendas" items="ACHAddenda">
						<ffi:setProperty name="AddendaString" value="${AddendaString}${ACHAddenda.PmtRelatedInfo}" />
			</ffi:list>
					<s:if test="%{#session.AddendaString!=null && #session.AddendaString!=''}">
					<tr style="background: transparent none repeat scroll 0 0;">
						<td><div class="blockHead"><!--L10NStart-->Addenda<!--L10NEnd--></div></td>
					</tr>
					<tr>
						<td colspan="<s:property value="#attr.spanCols"/>" class="">
						<textarea  id="textfield5.1" name="textfield5.1" rows="9" cols="80"  border="0" disabled size="110">
								<ffi:getProperty name="AddendaString" />
						</textarea>
						<%--<span class="addendaSpan"><ffi:getProperty name="AddendaString" /></span> --%>
						</td>
						<ffi:setProperty name="AddendaString" value="" />
					</tr>
					</s:if>
			</ffi:cinclude>
		</ffi:cinclude>


	<%-- <ffi:cinclude value1="${subMenuSelected}" value2="tax" >
		<tr class="lightBackground">
			<td class="sectionsubhead" align="right"><!--L10NStart-->Tax Type<!--L10NEnd--></td>
			<% String taxformid; %>
			<ffi:getProperty name="ACHEntry" property="TaxFormID" assignTo="taxformid" />
			<% if (taxformid == null) { %>
				<ffi:getProperty name="ACHBatch" property="TaxFormID" assignTo="taxformid" />
			<% } %>
			<ffi:object name="com.ffusion.tasks.ach.GetTaxForm" id="TaxForm" scope="session"/>
			<ffi:setProperty name="TaxForm" property="ID" value="<%=taxformid%>" />
			<ffi:process name="TaxForm"/>
			<td class="columndata" colspan="<s:property value="#attr.spanCols"/>">
				<ffi:cinclude value1="${TaxForm.type}" value2="STATE"><ffi:getProperty name="TaxForm" property="state" /> <!--L10NStart-->State<!--L10NEnd--></ffi:cinclude>
				<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /> - <ffi:getProperty name="TaxForm" property="TaxFormDescription" />
			</td>
		</tr>
	</ffi:cinclude> --%>
	<s:if test="%{#attr.isReverse && !#ACHEntry.canReverseValue}">
	    <tr>
		    <td class="sectionsubhead" colspan="<s:property value="#attr.spanCols"/>" align="center">
			    <!--L10NStart-->This entry has already been reversed in a previous batch and cannot be selected<!--L10NEnd-->
		    </td>
	    </tr>
	</s:if>
</s:iterator>
</table>


<ffi:setProperty name="ACHEntries" property="FilterPaging" value="none"/>
<ffi:removeProperty name="SortURL"/>
