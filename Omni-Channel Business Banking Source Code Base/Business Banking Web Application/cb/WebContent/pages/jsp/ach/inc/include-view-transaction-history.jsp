<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:if test="%{#attr.auditHistoryRecords.size() > 0}">
<div>
          <s:if test="%{#attr.isTemplate}">
             <span id="templateHis" class="toggleClick"><strong><!--L10NStart-->Template History </strong><span class="sapUiIconCls icon-navigation-down-arrow"></span><!--L10NEnd--></span>
          </s:if>
          <s:else>
              <span id="transactionHis" class="toggleClick"><strong><!--L10NStart-->Transaction History </strong><span class="sapUiIconCls icon-navigation-down-arrow"></span><!--L10NEnd--></span>
          </s:else>
		  <%-- ---- TRANSACTION HISTORY ---- --%>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableAlerternateRowColor tdWithPadding transactionHistoryTable marginTop10 tableData" style="display:none">
				<tr class="resetTableRowBGColor">
					<td class="sectionsubhead" nowrap><!--L10NStart-->Date/Time<!--L10NEnd--></td>
					<td class="sectionsubhead" nowrap>
						<s:if test="%{#attr.isTemplate}">
							<!--L10NStart-->Template Status<!--L10NEnd-->
						</s:if>
						<s:else>
							<!--L10NStart-->Transaction Status<!--L10NEnd-->
						</s:else>
						
					</td>
					<td class="sectionsubhead" nowrap><!--L10NStart-->Processed By<!--L10NEnd--></td>
					<td class="sectionsubhead" nowrap><!--L10NStart-->Description<!--L10NEnd--></td>
				</tr>
				<s:iterator value="#attr.auditHistoryRecords" id="historyItem">
				<ffi:setProperty name="historyItem" property="DateFormat" value="yyyy-MM-dd HH:mm:ss"/>
				<tr>
					<td class="columndata"><ffi:getProperty name="historyItem" property="TranDate"/></td>
					<td class="columndata"><ffi:getProperty name="historyItem" property="State"/></td>
					<td class="columndata"><ffi:getProperty name="historyItem" property="ProcessedBy"/></td>
					<td class="columndata"><div class="description"><ffi:getProperty name="historyItem" property="Message"/></div>	
					</td>
				</tr>
				</s:iterator>
			</table>
</div>

</s:if>
<ffi:removeProperty name="GetAuditTransferById"/>
<script type="text/javascript">
	$(document).ready(function(){
	<s:if test="%{#attr.auditHistoryRecords.size() > 0}">
	 <s:if test="%{#attr.isTemplate}">
	 $("#templateHis").next().slideDown();
		$("#templateHis").find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
		$("#templateHis").removeClass("expandArrow").addClass("collapseArrow");
	  </s:if>
          <s:else>
		  
		  $("#transactionHis").next().slideDown();
		$("#transactionHis").find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
		$("#transactionHis").removeClass("expandArrow").addClass("collapseArrow");
		    </s:else>
	</s:if> 
		
		$( ".toggleClick" ).click(function(){ 
				if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>