<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.ach.ACHOffsetAccount,
				 com.ffusion.beans.ach.ACHClassCode" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<style>
#achBatchDescretionaryData, #coEntryDesc, #achBatchName, #achBatchPayee, #AchBatchTemplateName{width:300px;}
</style>
<!-- If Selected SEC code is 'IAT' then do this -->
<s:if test="%{#session.AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">
<script type="text/javascript">

var accountTypeList = null;

/* Mappings of banks to indices into the account lists.*/
var countrySize = '<s:property value="%{#request[@com.ffusion.struts.ach.ACHDefines@COUNTRY_CURRENCY_CODES].size}"/>';
var countries = new Array(countrySize);
var iatCurrencyList = new Array(countrySize);
<% int countryIndex = 0;
%>
<ffi:list collection="CountryCurrencyCodes" items="countryCode">
countries[ <%= countryIndex%> ] = "<ffi:getProperty name="countryCode" property="Id"/>";
iatCurrencyList[ <%= countryIndex%> ] = "<ffi:getProperty name="countryCode" property="Label"/>";
<% countryIndex++; %>
</ffi:list>

var iatForeignExchangeIndicatorFF = "";
var iatForeignExchangeIndicatorFV = "";
var iatForeignExchangeIndicatorVF = "";
var foreingExchangeIndicator = "";
<ffi:list collection="ForeignExchangeIndicators" items="indicators">
foreingExchangeIndicator = "<ffi:getProperty name="indicators" property="Id"/>";
if(foreingExchangeIndicator == 'FF'){
	iatForeignExchangeIndicatorFF = "<ffi:getProperty name="indicators" property="Label"/>";
}
if(foreingExchangeIndicator == 'FV'){
	iatForeignExchangeIndicatorFV = "<ffi:getProperty name="indicators" property="Label"/>";
}
if(foreingExchangeIndicator == 'VF'){
	iatForeignExchangeIndicatorVF = "<ffi:getProperty name="indicators" property="Label"/>";
}
</ffi:list>


var currencySize = '<s:property value="%{#request[@com.ffusion.struts.ach.ACHDefines@CURRENCY_CODES].size}"/>';
var iatSupportedCurrencyList = new Array(currencySize);
var iatSupportedCurrencyNamesList = new Array(currencySize);
<% int currencyIndex = 0; %>
<ffi:list collection="CountryCurrencyCodes" items="currencyCode">
iatSupportedCurrencyList [ <%= currencyIndex%> ] = "<ffi:getProperty name="currencyCode" property="Label"/>";
iatSupportedCurrencyNamesList [ <%=currencyIndex%> ] = "<ffi:getProperty name="currencyCode" property="Label"/>";
<% currencyIndex++; %>
</ffi:list>




function updateTruncatedText()
{
    var truncatedText = document.getElementById("truncatedText");
    var countrySel = document.getElementById("countryID");
    if (truncatedText != null)
    {
        taxtitle = countrySel.options[ countrySel.selectedIndex ].title;
        truncatedText.innerHTML = taxtitle;
        if (taxtitle.length > 25)
        {
            $(truncatedText).show();
        }
        else
        {
            $(truncatedText).hide();
        }
     }
}

function updateCurrencyInfo(country) {
	var currencySel = document.getElementById("CURRENCY");
	var foreignSel = document.getElementById("FOREIGNEXCHANGE");
	var addEntry = document.getElementById("addEntryButtonId");
	var importEntry = document.getElementById("IMPORTENTRIES");
	var countrySel = document.getElementById("countryID");

	if (currencySel == null || foreignSel == null || country == null)
		return;
	var supportedCurrencies = "";

	for ( var i = 0; i < countries.length; i++) {
		if (countries[i] == country.value) {
			supportedCurrencies = iatCurrencyList[i];

			

			if (addEntry != null) {
				addEntry.disabled = false;
				$('#addEntryButtonId').removeClass('ui-state-disabled');
			}
			if (importEntry != null) {
				importEntry.disabled = false;
				$('#IMPORTENTRIES').removeClass('ui-state-disabled');
			}

			var found = "";
			currencySel.selectedIndex = 0;
			while (true) {
				for ( var j = 0; j < iatSupportedCurrencyList.length; j++) {
					var value = iatSupportedCurrencyList[j];
					var text = iatSupportedCurrencyNamesList[j];

					if (supportedCurrencies.indexOf(value) == 0 && value != "") {
						if (found == "") {
							found = "1";
							currencySel.value = value; // "Select
							// Currency" is
							// first item
							$('#CURRENCY').selectmenu('destroy').selectmenu({
								width : 160
							});
							updateForeignExchangeMethod(currencySel);
							break;
						}
					}
				}
				if (!found) {
					currencySel.selectedIndex = -1;
				}
				if (supportedCurrencies.length <= 3)
					break;
				supportedCurrencies = supportedCurrencies.substr(4); // skip
				// the
				// first
				// currency
				// and
				// comma
			}
			// we updated the ForeignExchangeMethod with the first currency
			// displayed
			break;
		} // if found country
	} // for loop
	updateTruncatedText();
	if (supportedCurrencies == "") {
		if (addEntry != null) {
			addEntry.disabled = true;
			$('#addEntryButtonId').addClass('ui-state-disabled');
		}
		if (importEntry != null) {
			importEntry.disabled = true;
			$('#IMPORTENTRIES').addClass('ui-state-disabled');
		}
		currencySel.selectedIndex = 0; // "Select Country First"
		$('#CURRENCY').selectmenu('destroy').selectmenu({
			width : 160
		});
		if (foreignSel != null) {
			// clear out entries in the lists
			while (foreignSel.options.length > 0) {
				foreignSel.options[0] = null;
			}
			var newOpt = new Option(currencySel.options[0].text, "");
			foreignSel.options[0] = newOpt;
			$("#FOREIGNEXCHANGE").selectmenu('destroy').selectmenu({
				width : 160
			});
		}
	}
}

function updateForeignExchangeMethod(currency) {
	// There is a one-to-one mapping of currency to Foreign Exchange Method
	var currencySel = document.getElementById("CURRENCY");
	var foreignSel = document.getElementById("FOREIGNEXCHANGE");
	var countrySel = document.getElementById("countryID");
	var addEntry = document.getElementById("addEntryButtonId");
	var importEntry = document.getElementById("IMPORTENTRIES");

	if (countrySel == null || currencySel == null || foreignSel == null
			|| currency == null)
		return;

	// clear out entries in the lists
	while (foreignSel.options.length > 0) {
		foreignSel.options[0] = null;
	}

	// assume FF for default
	var value = "FF";
	var text = value + "-" + iatForeignExchangeIndicatorFF;
	// for USA, support USD, USD (Same Day), USD (Next Day)
	if (currency.value.indexOf("USD") != 0
			&& currency.value.indexOf("USS") != 0
			&& currency.value.indexOf("USN") != 0) {
		value = "FV";
		text = value + "-" + iatForeignExchangeIndicatorFV;
	}
	// VF not supported when USD is originating currency
	/*
	 * if (supportedForeignExchange.indexOf("VF") == 0) { value = "VF"; text =
	 * value + "-" + iatForeignExchangeIndicatorVF; }
	 */
	if (currency.selectedIndex == 0) {
		value = "";
		text = currency.options[0].text;
		if (addEntry != null) {
			addEntry.disabled = true;
			$('#addEntryButtonId').addClass('ui-state-disabled');
		}
		if (importEntry != null) {
			importEntry.disabled = true;
			$('#IMPORTENTRIES').addClass('ui-state-disabled');
		}
	}
	var newOpt = new Option(text, value);
	foreignSel.options[0] = newOpt;
	$("#FOREIGNEXCHANGE").selectmenu('destroy').selectmenu({
		width : 160
	});

	if (currency.selectedIndex != 0 && countrySel.selectedIndex != 0) {
		if (addEntry != null) {
			addEntry.disabled = false;
			$('#addEntryButtonId').removeClass('ui-state-disabled');
		}
		if (importEntry != null) {
			importEntry.disabled = false;
			$('#IMPORTENTRIES').removeClass('ui-state-disabled');
		}
	}
}
</script>
</s:if>
<%
boolean isTemplate = false;
boolean fromTemplate = false;
boolean canRecurring = false;
boolean isScheduledACHBatch  = false;
boolean addEntryDisabled = false;
boolean isEdit = false;
boolean importedBatch = false;
boolean anyEntries = false;
boolean companySelected = true;
%>

<s:if test="%{#session.AddEditACHBatch.CompanyID != null}">
	<% companySelected = true; %>
</s:if>
 

<s:if test="%{#session.AddEditACHBatch.ACHEntries != null && #session.AddEditACHBatch.ACHEntries.length > 0}">
<% anyEntries = true; %>
</s:if>
		

<ffi:cinclude value1="${AddEditACHBatch.FromTemplate}" value2="true" >
	<% fromTemplate = true; %>
</ffi:cinclude>	

<ffi:cinclude value1="${AddEditACHBatch.BatchImported}" value2="true" >
		<% importedBatch = true; %>
</ffi:cinclude>

<ffi:cinclude value1="${AddEditACHBatch.BatchImported}" value2="true" >
		<% anyEntries = true; %>
</ffi:cinclude>

<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
<% isTemplate = true; %>
</s:if>
<table border="0" cellspacing="0" cellpadding="0" class="tableData" width="100%">
	
		<tr>
			<s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">	
				<td id="achBatchNameLabel"><label for="achBatchName"><span class="sectionsubhead" width="50%"><!--L10NStart-->Batch Name<!--L10NEnd--> </label></span><span class="required" title="required">*</span></td>
			</s:if>
			<s:else>
				<%-- <td id="achBatchNameLabel"><span class="sectionsubhead"><!--L10NStart-->Template Name<!--L10NEnd--></span><span class="required">*</span><input type="hidden" name="template" value="true"></td> --%>
			</s:else>
			<td width="50%" id="achBatchCompanyDescriptionLabel"><label for="coEntryDesc"><span class="sectionsubhead"><!--L10NStart-->Company Description<!--L10NEnd--></label> </span><span class="required" title="required">*</span></td>
		</tr>
		<tr>		
			<s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">	
				<td class="columndata" align="left" width="50%"><input id="achBatchName" <%=fromTemplate?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="32" border="0" name="AddEditACHBatch.Name" value="<ffi:getProperty name="AddEditACHBatch" property="Name"/>"></td>
			</s:if>
	        <s:else>
				<%-- <td width="50%" class="columndata"><input id="AchBatchTemplateName" type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="32" border="0" name="AddEditACHBatch.TemplateName" value="<ffi:getProperty name="AddEditACHBatch" property="TemplateName"/>"></td> --%>
			</s:else>
			<td class="columndata"> 
				<%-- 	<ffi:setProperty name="coEntrySecureDesc" value="<%= coEntryDesc %>"/> --%>
				<input type="text" class="ui-widget-content ui-corner-all txtbox" size="24" id="coEntryDesc" maxlength="10" border="0" name="AddEditACHBatch.coEntryDesc" value="<ffi:getProperty name="AddEditACHBatch" property="CoEntryDesc"/>"
				<s:if test="%{!allowChangeCompanyDescr}">disabled</s:if>>
				<input id="coEntryDescHidden" type="Hidden" name="AddEditACHBatch.CoEntryDesc" value="<ffi:getProperty name="AddEditACHBatch" property="CoEntryDesc"/>">
			</td>
		</tr>
		<tr>
			<s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">	
	        	<td width="50%"><span id="nameError"></span></td>
			</s:if>
			<s:else>
				 <%-- <td width="50%"><span id="templateNameError"></span></td> --%>
			</s:else>
			<td>
				<span id="coEntryDescError"></span>
			</td>
		</tr>
		<!-- Discretionary Data is fine for WEB, just at the ENTRY level, there is no Discretionary Data -->
<% boolean isMultipleBatchTemplate = false; %>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
    <% isMultipleBatchTemplate = true; %>
</ffi:cinclude>
		<!-- IAT doesn't have Discretionary Data in the batch header -->
		<tr>	
			<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">				
				<td><span class="sectionsubhead"><!--L10NStart-->Originating Currency<!--L10NEnd--></span></td>
			</s:if>
			<s:else>
				<td id="achBatchDescretionaryDataLabel"><label for="achBatchDescretionaryData"><span class="sectionsubhead"><!--L10NStart-->Discretionary Data<!--L10NEnd--></label></span></td>
			</s:else>
			<% if (!isMultipleBatchTemplate || fromTemplate) { %>
				<td id="achBatchDateLabel">
					<s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">
						<span class="sectionsubhead"><label for="AddEditACHBatchDateId"> <!--L10NStart-->Effective Date<!--L10NEnd--> </label></span><span class="required" title="required">*</span>
					</s:if>
					<s:else>
					<!--  NO Date required for Templates -->
						<%-- <span class="sectionsubhead"><!--L10NStart-->Template Scope<!--L10NEnd--> </span><span class="required">*</span> --%>
					</s:else>
				</td>
			<% } %>
		</tr>
		<tr>
			<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">
				<td class="columndata"><input type="Hidden" class="ui-widget-content ui-corner-all" size="20" maxlength="20" border="0" name="AddEditACHBatch.CoDiscretionaryData" value="">
			       <input type="Hidden" class="ui-widget-content ui-corner-all" size="24" maxlength="20" border="0" name="AddEditACHBatch.ISO_OriginatingCurrencyCode" value="USD">
				 <!--L10NStart-->USD - United States Dollar<!--L10NEnd--></td>
			 </s:if>
			 <s:else>
			 	<td class="columndata" align="left"><input id="achBatchDescretionaryData" type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="20" border="0"
					name="AddEditACHBatch.coDiscretionaryData" value="<ffi:getProperty name="AddEditACHBatch" property="CoDiscretionaryData"/>">
			 </s:else>
			 <% if (!isMultipleBatchTemplate || fromTemplate) { %>
			 <s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">
				<td class="columndata">
					<div id="effectiveDateDiv">
						<s:include value="achBatchEffectiveDate.jsp" />
					</div>
				</td>
			
			</s:if>
			<s:else>
				<%-- <td class="columndata">
					<select id="AddEditACHBatchBatchScopeID" class="txtbox" name="AddEditACHBatch.BatchScope">
						<option value="BUSINESS" <ffi:cinclude value1="BUSINESS" value2="${AddEditACHBatch.BatchScope}" operator="equals" >selected</ffi:cinclude>><!--L10NStart-->Business (General Use)<!--L10NEnd--></option>
						<!-- Start: Dual approval processing -->
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
							<option value="USER" <ffi:cinclude value1="USER" value2="${AddEditACHBatch.BatchScope}" operator="equals" >selected</ffi:cinclude>><!--L10NStart-->User (My Use)<!--L10NEnd--></option>
						</ffi:cinclude>
						<!-- End: Dual approval processing -->
					</select>
				</td> --%>
			</s:else>
			<% } %>
		</tr>
		<tr>
			<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">
				<td></td>
			</s:if>
			<s:else>
				<td><span id="coDiscretionaryDataError"></span></td>
			</s:else>
			<% if (!isMultipleBatchTemplate || fromTemplate) { %>
				<s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">
					<td><span id="dateError"></span></td>
				</s:if>
				<s:else>
					<td><span id="batchScopeError"></span></td>
				</s:else>
			<% } %>
		</tr>
			
<s:if test="%{achCompanyHeaderTitle != null}">
<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_XCK)}">	
</s:if>
<s:else>
	<tr>
			<td colspan="2" id="achBatchPayeeLabel"><span class="sectionsubhead"><s:property value="%{achCompanyHeaderTitle}"/> </span><span class="required">*</span></td>
	</tr>
	<tr>						
		<td colspan="2" class="columndata">
				<input id="achBatchPayee" type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="16" border="0" name="AddEditACHBatch.HeaderCompName" value="<ffi:getProperty name="AddEditACHBatch" property="HeaderCompName"/>">
		</td>
	</tr>
	<tr>
		<td colspan="2"><span id="headerCompNameError"></span></td>
	</tr>
</s:else>
</s:if>
<s:else>
<input type="Hidden" name="AddEditACHBatch.HeaderCompName" value="">
</s:else>
<ffi:removeProperty name="coHeaderCompName"/>
<!-- IAT specific items -->
<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">
	<tr>
		<td><span class="sectionsubhead"><!--L10NStart-->Destination Country<!--L10NEnd--> </span><span class="required">*</span></td>
		<td><span class="sectionsubhead"><!--L10NStart-->Foreign Exchange Method<!--L10NEnd--> </span><span class="required">*</span></td>
	</tr>
	<tr>
		 <td class="columndata">
		 <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
		 <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
		 <ffi:process name="ACHResource" />
			<select id="countryID" cssClass="txtbox" name="AddEditACHBatch.ISO_DestinationCountryCode" onchange="updateCurrencyInfo(this);">
				<option value="-1"><s:text name="ach.countrySelect.defaultOption"/></option>
				<s:iterator value="#request.CountryCodes" var="country">
				 <% String countryName =""; %>
				 <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${country.id}"/>
                 <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName" />
					<option title="<%=countryName%>" value="<s:property value="%{#country.id}"/>" <s:if test="%{#country.id.equals(#session.AddEditACHBatch.ISO_DestinationCountryCode)}">selected</s:if>>
					<s:property value="%{#country.label}"/>
					</option>
				</s:iterator>
			</select>
			<div id="truncatedText"></div>
		 </td>
		 <td class="columndata">
			 <select id="FOREIGNEXCHANGE" cssClass="txtbox" name="AddEditACHBatch.ForeignExchangeIndicator" onchange="updateForeignExchangeMethod(this);">
				<option value="-1"><s:text name="ach.foreignExchangeIndicator.defaultOption"/></option>
				<s:iterator value="#request.ForeignExchangeIndicators" var="foreignExcInd">
					<option value="<s:property value="%{#foreignExcInd.id}"/>" <s:if test="%{#foreignExcInd.id.equals(#session.AddEditACHBatch.ForeignExchangeIndicator)}">selected</s:if>>
					<s:property value="%{#foreignExcInd.label}"/>
					</option>
				</s:iterator>
			</select>
	 	 </td>
	</tr>
	<tr>
	  <td class="sectionsubhead"><span id="ISO_DestinationCountryCodeError"></span></td>
	  <td>	
		<span id="foreignExchangeIndicatorError"></span>
	  </td>
	</tr>
</s:if>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" operator="notEquals" >
	<ffi:cinclude value1="${AddEditACHBatch.TransactionType}" value2="ACH" >
		<ffi:cinclude value1="${AddEditACHBatch.RecID}" value2="" operator="notEquals">
			<ffi:cinclude value1="${AddEditACHBatch.ID}" value2="${AddEditACHBatch.RecID}" operator="notEquals">
				<% isScheduledACHBatch= true; %>
			</ffi:cinclude>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

	<tr>
		<s:if test="%{isRecurringAvailable}">
			<td id="achTemplateRepeatingLabel" class="sectionsubhead"><label for="FrequencyID"><!--L10NStart-->Repeating<!--L10NEnd--></label></td>
		</s:if>
		<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="equals" >
		<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_CCD)}">
		   <ffi:cinclude ifEntitled="CCD + Healthcare" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${AddEditACHBatch.AchCompany.ID}" checkParent="true">
		      <td><span class="sectionsubhead"><!--L10NStart-->Payment Type<!--L10NEnd--> </span><span class="required">*</span></td>
		   </ffi:cinclude>
		</s:if>                             
		</ffi:cinclude>
		<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">
			<td><span class="sectionsubhead"><!--L10NStart-->Destination Currency<!--L10NEnd--> </span><span class="required">*</span></td>
		</s:if>
		<s:if test="%{achCompanyHeaderTitle != null}">
			<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_XCK)}">	
				<td id="achBatchPayeeLabel"><span class="sectionsubhead"><s:property value="%{achCompanyHeaderTitle}"/> </span><span class="required">*</span></td>
			</s:if>
		</s:if>
	</tr>
    <tr>    
       <s:if test="%{isRecurringAvailable}">                                
	       <td class="columndata">
	             <select <%=fromTemplate || isScheduledACHBatch?"disabled":""%> id="FrequencyID" name="AddEditACHBatch.frequency" class="txtbox" onchange="ns.ach.modifyFrequency(<%=fromTemplate %>, <%=isEdit%>);">
	               <option value="None"><!--L10NStart-->None Selected<!--L10NEnd--></option>
	               <ffi:list collection="ACHFrequencies" items="Freq1">
	                   <option value="<ffi:getProperty name="Freq1"/>" <ffi:cinclude value1="${Freq1}" value2="${AddEditACHBatch.Frequency}">selected</ffi:cinclude>><ffi:getProperty name="Freq1"/></option>
	               </ffi:list>
	             </select>
	             <span id="lineBreakForDevice"></span><!-- dont remove this span, it is for adding line break in iPad device -->
	           <% if(!isScheduledACHBatch) {%>
				<s:if test="%{isRecurringAvailable}">
		       		<input <%=fromTemplate ?"disabled":""%> onclick="toggleRecurring();" id="unlimitedId" type="radio" name="OpenEnded" value="true" border="0" <ffi:cinclude value1="${OpenEnded}" value2="true">checked</ffi:cinclude>>
				    <lable for="unlimitedId"> <!--L10NStart-->Unlimited<!--L10NEnd--> </lable>
				</s:if>
				<s:if test="%{isRecurringAvailable}">
	               <input <%=fromTemplate ?"disabled":""%> onclick="toggleRecurring();" id="paymentsId" type="radio" name="OpenEnded" value="false" border="0" <ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals" >checked</ffi:cinclude>>
	               <lable id="paymentsId"><!--L10NStart--># of Payments<!--L10NEnd--></lable> <input <%=fromTemplate ?"disabled":""%> type="text" id="AddEditACHBatchNumberPayments" name="AddEditACHBatch.NumberPayments" value="<ffi:getProperty name="AddEditACHBatch" property="NumberPayments"/>" size="2" class="ui-widget-content ui-corner-all txtbox" maxlength="3">
				</s:if>
				<% } %>
				<% if(isScheduledACHBatch) {%>
					<s:if test="%{isRecurringAvailable}">
	                   <input disabled type="radio" name="StaticOpenEnded" value="true" border="0" <ffi:cinclude value1="${AddEditACHBatch.OpenEnded}" value2="true">checked</ffi:cinclude>>
	                   <!--L10NStart-->Unlimited<!--L10NEnd-->
					</s:if>	
					<s:if test="%{isRecurringAvailable}">
	                   <input disabled  type="radio" name="StaticOpenEnded" value="false" border="0" <ffi:cinclude value1="${AddEditACHBatch.OpenEnded}" value2="true" operator="notEquals" >checked</ffi:cinclude>>
	                   <!--L10NStart--># of Payments<!--L10NEnd--> <ffi:getProperty name="AddEditACHBatch" property="NumberPayments"/>
					</s:if>
				<% } %>
	       </td>
      </s:if>
       <ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="equals" >
		<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_CCD)}">
		   <ffi:cinclude ifEntitled="CCD + Healthcare" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${AddEditACHBatch.AchCompany.ID}" checkParent="true">
	         <td class="columndata" colspan="2">
		         <select class="txtbox" id="paymentTypeID" name="paymentTypeId" onchange="changePaymentType('true');">
		             <option value='-1' <s:if test="%{paymentTypeId.equals('-1')}"> selected </s:if>><!--L10NStart-->Regular<!--L10NEnd--></option>
		             <option value='Healthcare' <s:if test="%{paymentTypeId.equals('Healthcare')}"> selected </s:if>><!--L10NStart-->Healthcare<!--L10NEnd--></option>
		         </select>
	         </td>
		   </ffi:cinclude>
		</s:if>                             
		</ffi:cinclude>
		<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">
			<td class="columndata">
	           <select id="CURRENCY" cssClass="txtbox" name="AddEditACHBatch.ISO_DestinationCurrencyCode" onchange="updateForeignExchangeMethod(this);">
					<option value="-1"><s:text name="ach.currencySelect.defaultOption"/></option>
					<s:iterator value="#request.CurrencyCodes" var="currency">
						<option value="<s:property value="%{#currency.id}"/>" <s:if test="%{#currency.id.equals(#session.AddEditACHBatch.ISO_DestinationCurrencyCode)}">selected</s:if>>
						<s:property value="%{#currency.id}"/> - <s:property value="%{#currency.value}"/>
						</option>
					</s:iterator>
				</select>
		
	         </td>
		</s:if>
		
		<s:if test="%{achCompanyHeaderTitle != null}">
		<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_XCK)}">	
			<td class="columndata">
				<!-- XCK /RCK case -->
				<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_XCK)}">										
					<input type="Hidden" id="hiddenHeaderCompName" name="AddEditACHBatch.HeaderCompName" value="<ffi:getProperty name="AddEditACHBatch" property="HeaderCompName"/>">
					<input disabled type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="16" border="0" name="HeaderCompName" value="<ffi:getProperty name="AddEditACHBatch" property="HeaderCompName"/>">
				</s:if>
			</td>
			</s:if>
		</s:if>	
	</tr>
	<tr>
		<s:if test="%{isRecurringAvailable}">
       		<td><span id="numberPaymentsError"></span></td>
       	</s:if>
       	<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">
		<td class="sectionsubhead"><span id="ISO_DestinationCurrencyCodeError"></span></td>
		</s:if>
   </tr>

 <s:if test="%{#session.AddEditACHBatch.BatchTypeValue == @com.ffusion.beans.ach.ACHBatchTypes@BATCH_BALANCED}">
 	<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null  && AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">
	<!-- // QTS 577543: If IAT, we don't support Batch Balanced or Entry Balanced batches -->
	  <tr><td class="sectionsubhead" colspan='2'><s:text name="ach.IAT.noOffsetAcct"/>
	  <span id="offsetAccountIDError"></span>
	  </td></tr>
	</s:if>
	<s:else>
      <tr><td colspan="2"><span class="sectionsubhead"><!--L10NStart-->Offset Account<!--L10NEnd--> </span><span class="required">*</span></td></tr>
      <tr>
        <td colspan="2" class="columndata">
        <select id="AddEditACHBatchOffsetAccountID" class="txtbox" name="AddEditACHBatch.OffsetAccountID">
           <option value=""><s:text name="ach.offesetAcct.defaultOption"/></option>
           <s:iterator value="OffsetAccounts" var="OffsetAccount">
           		<option value="<ffi:getProperty name="OffsetAccount" property="ID"/>" 
           			<ffi:cinclude value1="${OffsetAccount.ID}" value2="${AddEditACHBatch.OffsetAccountID}">selected</ffi:cinclude>>
           			<ffi:getProperty name="OffsetAccount" property="Number"/>-<ffi:getProperty name="OffsetAccount" property="Type"/></option>
           </s:iterator>
          </select>
        </td>
       </tr> 
       <tr>
       	  <td colspan="2"><span id="offsetAccountIDError"></span></td>
       </tr>
	</s:else>
</s:if>
 <s:if test="%{#session.AddEditACHBatch.BatchTypeValue != @com.ffusion.beans.ach.ACHBatchTypes@BATCH_BALANCED}"></s:if>
</table>

                                
<script type="text/javascript">
$(document).ready(function(){
	$("#AddEditACHBatchBatchScopeID").selectmenu({width: "90%"});
	$("#FrequencyID").selectmenu({width: 120});
	$("#AddEditACHBatchOffsetAccountID").selectmenu({width: 160});
	$("#FOREIGNEXCHANGE").selectmenu({width: 200});
	$("#countryID").selectmenu({width: 310});
	$("#CURRENCY").selectmenu({width: 310});
	$("#paymentTypeID").selectmenu({width: 160});
});
</script>

<s:if test="%{#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_ACH)}">
<s:if test="%{AddEditACHBatch.StandardEntryClassCode != null 
	&& AddEditACHBatch.StandardEntryClassCode.startsWith(@com.ffusion.beans.ach.ACHClassCode@ACH_CLASSCODE_TEXT_IAT)}">
<script type="text/javascript">
<!--
$(document).ready(function(){
	enableDisableImportAddEntryBtns($("#countryID").val());
});
//-->
</script>
</s:if>
<s:else>
<script type="text/javascript">
<!--
$(document).ready(function(){
	enableDisableImportAddEntryBtns($("#standardEntryClassCodeId").val());
});
//-->
</script>
</s:else>
</s:if>