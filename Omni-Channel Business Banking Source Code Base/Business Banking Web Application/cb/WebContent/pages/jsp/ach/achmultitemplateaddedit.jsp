<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achmultitemplateaddedit" className="moduleHelpClass"/>
<ffi:cinclude value1="${subMenuSelected}" value2="ach" operator="equals" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_ach,ACHtemplatesLink,addMultipleTemplateLink</span>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" operator="equals" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_tax,TaxtemplatesLink,addMultipleTemplateLink</span>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" operator="equals" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_childsp,ChildsptemplatesLink,addMultipleTemplateLink</span>
</ffi:cinclude>

<%

	if (request.getParameter("Initialize") != null) { %>

        <%  if (request.getParameter("TemplateID") != null) { %>
            <% session.setAttribute("strutsActionName", "editACHBatchAction"); %>
            <s:action namespace="/pages/jsp/ach" name="editMultiACHTemplateAction_init" executeResult="true"/>
        <% } else { %>
            <% session.setAttribute("strutsActionName", "addACHBatchAction"); %>
            <s:action namespace="/pages/jsp/ach" name="addMultiACHTemplateAction_init" executeResult="true"/>
        <% } %>
	<% } else // initialize
	if (request.getParameter("RecheckValidation") != null) { %>
        <s:action namespace="/pages/jsp/ach" name="editMultiACHTemplateAction_recheckValidation" executeResult="true"/>
    <% } else
	if (request.getParameter("SaveBatch") != null) { %>
        <s:action namespace="/pages/jsp/ach" name="editMultiACHTemplateAction_addeditbatch_save" executeResult="true"/>
    <% } else { %>

        <ffi:setProperty name="Initialize" value="true"/>
        <s:include value="%{#session.PagesPath}/ach/achmultibatchaddedit.jsp"/>
    <% } %>
