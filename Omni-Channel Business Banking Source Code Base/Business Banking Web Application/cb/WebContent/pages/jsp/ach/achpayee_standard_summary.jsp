<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_ACHPayeeStandardSummary" className="moduleHelpClass"/>
	<s:url namespace="/pages/jsp/ach" id="remoteurl" action="getACHParticipants">
    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
    </s:url>
	
	<s:url namespace="/pages/jsp/ach" id="editurl" action="editACHPayee">
    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
    </s:url>

	<ffi:removeProperty name="ACHCOMPANY"/>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
	<ffi:setProperty name="canManageAchParticipants" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
	<ffi:setProperty name="canManageAchParticipants" value="false"/>
</ffi:cinclude>
	
	<ffi:setGridURL grid="GRID_ACHPayee" name="ViewURL" url="/cb/pages/jsp/ach/viewACHPayeeAction_init.action?PayeeID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_ACHPayee" name="EditURL" url="/cb/pages/jsp/ach/editACHPayeeAction_init.action?PayeeID={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_ACHPayee" name="DeleteURL" url="/cb/pages/jsp/ach/deleteACHPayeeAction_init.action?PayeeID={0}" parm0="ID"/>
	<ffi:setProperty name="standardACHPayeeTempURL" value="/pages/jsp/ach/getACHParticipantsAction.action?PayeeType=Domestic&searchOper=cn&GridURLs=GRID_ACHPayee" URLEncrypt="true"/>
<ffi:cinclude value1="${anyPendingDAPayees}" value2="true">
    <ffi:setProperty name="standardACHPayeeTempURL" value="/pages/jsp/ach/getACHParticipantsAction.action?Show=Approved&PayeeType=Domestic&searchOper=cn&GridURLs=GRID_ACHPayee" URLEncrypt="true"/>
</ffi:cinclude>
    <s:url id="standardACHPayeeUrl" value="%{#session.standardACHPayeeTempURL}" escapeAmp="false"/>
	<sjg:grid
		id="standardACHPayeeGridId"
		caption=""  
		sortable="true"
		sortname="NICKNAME"
		dataType="json"  
		href="%{standardACHPayeeUrl}"
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,standardACHPayeeGridCompleteEvents"
		> 

        <sjg:gridColumn name="payeeType" edittype="select" editoptions="{value:'1:Standard;2:IAT Payee'}" width="120" index="PAYEETYPE" title="%{getText('ach.grid.payeeType')}" sortable="true" search="false" hidden="true" hidedlg="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="nickNameSecure" width="100" index="NICKNAME" title="%{getText('ach.grid.payee.nickname')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="nameSecure" width="100" index="NAME" title="%{getText('ach.grid.payeeName')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" searchoptions="{searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="userAccountNumber" width="100"  index="USERACCOUNTNUMBER" title="%{getText('ach.grid.payeeID')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="displayACHCompany" width="120" index="COMPANYDISPLAYNAME" title="%{getText('ach.grid.companyIDOrName')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" formatter="ns.ach.formatACHPayeesCompanyNameID" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="payeeGroup" edittype="select" editoptions="{value:'1:User;2:ACH Company;3:Business'}" width="120" index="PAYEEGROUP" title="%{getText('ach.grid.payeeScope')}" sortable="true" search="false" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="bankNameSecure" width="120"  index="BANKNAME" title="%{getText('ach.grid.payee.bankname')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="routingNumberSecure" width="80" index="ROUTINGNUM" title="%{getText('ach.grid.payee.routingnumber')}" hidden="true" hidedlg="true" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="accountNumberSecure" width="120" index="ACCOUNTNUMBER" title="%{getText('ach.grid.payee.accountnumber')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="accountTypeSecure" edittype="select" hidden="true" hidedlg="true" editoptions="{value:'1:Checking;2:Savings;3:Loan;4:General Ledger'}" width="80" index="accountType" title="%{getText('ach.grid.payee.accounttype')}" sortable="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="securePayee" edittype="checkbox" editoptions="{value:'true:false'}" search="false" width="120" index="securePayee" title="%{getText('ach.grid.securePayee')}" sortable="false" hidden="true" hidedlg="true" editable="true" editrules="{edithidden:true, searchhidden:true}" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="displayAsSecurePayee" width="120" index="displayAsSecurePayee" search="false" title="%{getText('ach.grid.displaySecurePayee')}" sortable="false" hidden="true" hidedlg="true" editable="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="prenoteStatus" width="120" index="prenote" search="false" title="%{getText('ach.grid.prenote')}" sortable="false" hidden="true" hidedlg="true" editable="false" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="ID" index="action" title="%{getText('ach.grid.payee.action')}" sortable="false" search="false" formatter="ns.ach.formatStandardACHPayeeActionLinks" width="90"
			formatoptions="{canManageParticipant : '%{#session.canManageAchParticipants}'}" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
	</sjg:grid>

<script type="text/javascript">
<!--
	$("#standardACHPayeeGridId").data("supportSearch",true);
	$("#standardACHPayeeGridId").jqGrid('setColProp','ID',{title:false});
//-->
</script>

