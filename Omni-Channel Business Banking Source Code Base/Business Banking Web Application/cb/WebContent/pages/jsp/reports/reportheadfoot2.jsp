<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

	<%
		session.setAttribute("format", request.getParameter("format"));
	%>
    
    
	<div align="center">
		<s:form action="/pages/jsp/reports/reportheadfoot2_update.jsp" method="post" name="HeaderFooter" id="HeaderFooterForm" theme="simple">
	    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

		<div align="center">
		    <table class="dkback" width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="columndata"><br></td>
			</tr>
			<tr>
				<td class="sectionsubhead"  align="left"><s:text name="jsp.reports_665"/></td>
			</tr>
			<tr>
				<td class="columndata"><br></td>
			</tr>

				<ffi:cinclude value1="${format}" value2="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>" operator="equals">
				<ffi:cinclude value1="TRUE" value2="${ReportData.ShowPrintHeaderOption}" operator="equals">

					<script type="text/javascript">

						$(document).ready(function() {
							initHeaderOptions();
						});
					
						function initHeaderOptions(){
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_HEADER %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
								switchHeader(false);
							</ffi:cinclude>
						}
						function switchHeader(enabledisable){
							document.HeaderFooter["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_COMPANY_NAME %>"].disabled = !enabledisable;
							document.HeaderFooter["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TITLE %>"].disabled = !enabledisable;
							document.HeaderFooter["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_DATE %>"].disabled = !enabledisable;
							document.HeaderFooter["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TIME %>"].disabled = !enabledisable;
                            document.HeaderFooter["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_BANK_LOGO %>"].disabled = !enabledisable;
                        }
					</script>
	
					<tr>
						<td align="left" class="columndata" width="115">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td width="10">
									<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_HEADER %>" />
									<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
										<s:checkbox name="ROpt_SHOWHEADER" fieldValue="TRUE" value="true" onClick="switchHeader(this.checked)"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
										<s:checkbox name="ROpt_SHOWHEADER" fieldValue="TRUE" onClick="switchHeader(this.checked)"/>
									</ffi:cinclude>
									</td>
									<td class="columndata" nowrap>
										&nbsp;&nbsp;<s:text name="jsp.reports_637"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</ffi:cinclude>
				</ffi:cinclude>

				<tr>
					<td align="left" class="columndata" width="115">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="10">
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_COMPANY_NAME %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
									<s:checkbox name="ROpt_SHOWCOMPANYNAME" fieldValue="TRUE" value="true"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
									<s:checkbox name="ROpt_SHOWCOMPANYNAME" fieldValue="TRUE"/>
								</ffi:cinclude>
								</td>
								<td class="columndata" nowrap>
									&nbsp;&nbsp;<s:text name="jsp.reports_527"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<tr>
				<td align="left" class="columndata" width="115">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td width="10">
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TITLE %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
								<s:checkbox name="ROpt_SHOWTITLE" fieldValue="TRUE" value="true"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
								<s:checkbox name="ROpt_SHOWTITLE" fieldValue="TRUE"/>
							</ffi:cinclude>
							</td>
							<td class="columndata"   nowrap>
								&nbsp;&nbsp;<s:text name="jsp.reports_655"/>
							</td>
							<td>&nbsp;</td>
							<%-- <td>
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_TITLE %>" />
								<input class="txtbox" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_TITLE %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue"/>" size="24" maxlength="255">
							</td> --%>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="paddingLeft10">
					<span class="paddingLeft10">&nbsp;</span><ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_TITLE %>" />
					&nbsp;&nbsp;<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_TITLE %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue"/>" size="24" maxlength="255">
				</td>
			</tr>
			<tr>
				<td align="left" class="columndata" width="115">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td width="10">
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_DATE %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
								<s:checkbox name="ROpt_SHOWDATE" fieldValue="TRUE" value="true"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
								<s:checkbox name="ROpt_SHOWDATE" fieldValue="TRUE"/>
							</ffi:cinclude>
							</td>
							<td class="columndata" nowrap>
								&nbsp;&nbsp;<s:text name="jsp.reports_553"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="left" class="columndata" width="115">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td width="10">
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_TIME %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
								<s:checkbox name="ROpt_SHOWTIME" fieldValue="TRUE" value="true"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
								<s:checkbox name="ROpt_SHOWTIME" fieldValue="TRUE"/>
							</ffi:cinclude>
							</td>
							<td class="columndata" nowrap>
								&nbsp;&nbsp;<s:text name="jsp.reports_682"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
            <ffi:cinclude value1="${format}" value2="" operator="notEquals">
				<s:if test="%{#session.format.equals(@com.ffusion.beans.reporting.ExportFormats@FORMAT_HTML) || #session.format.equals(@com.ffusion.beans.reporting.ExportFormats@FORMAT_PDF)}" >
		            <tr>
						<td align="left" class="columndata" width="115">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td width="10">
									<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_BANK_LOGO %>" />
									<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
										<s:checkbox name="ROpt_SHOWBANKLOGO" fieldValue="TRUE" value="true"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
										<s:checkbox name="ROpt_SHOWBANKLOGO" fieldValue="TRUE"/>
									</ffi:cinclude>
									</td>
									<td class="columndata" nowrap>
										&nbsp;&nbsp;<s:text name="jsp.reports_515"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
            	</s:if>
			</ffi:cinclude>
            <ffi:cinclude value1="${format}" value2="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" operator="equals">
            <input type="hidden" name="ROpt_FORMAT" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>"/>
				<tr>
					<td align="left" class="columndata" width="115">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="10">
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_HEADER %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
									<s:checkbox name="ROpt_SHOWHEADER" fieldValue="TRUE" value="true"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
									<s:checkbox name="ROpt_SHOWHEADER" fieldValue="TRUE"/>
								</ffi:cinclude>
								</td>
								<td class="columndata" nowrap>
									&nbsp;&nbsp;<s:text name="jsp.reports_638"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</ffi:cinclude>
			<tr>
				<td class="columndata" width="115"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" height="10" width="0" border="0"></td>
			</tr>
			<tr>
				<td align="left" class="sectionsubhead"><s:text name="jsp.reports_664"/></td>
			</tr>
			<tr>
				<td class="columndata"><br></td>
			</tr>
			<ffi:cinclude value1="${format}" value2="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" operator="equals">
				<tr>
					<td align="left" class="columndata" width="115">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="10">
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_PAGE_NUMBER_FORMAT %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
									<s:checkbox name="ROpt_SHOWPAGENUMBERFORMAT" fieldValue="TRUE" value="true"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
									<s:checkbox name="ROpt_SHOWPAGENUMBERFORMAT" fieldValue="TRUE"/>
								</ffi:cinclude>
								</td>
								<td class="columndata" nowrap>
								&nbsp;&nbsp;<s:text name="jsp.reports_666"/></td>
							</tr>
						</table>
					</td>
				</tr>
			</ffi:cinclude>
			<tr>
				<td align="left" class="columndata" width="115">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td width="10">
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_EXTRA_FOOTER_LINE %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
								<s:checkbox name="ROpt_SHOWEXTRAFOOTERLINE" fieldValue="TRUE" value="true"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
								<s:checkbox name="ROpt_SHOWEXTRAFOOTERLINE" fieldValue="TRUE"/>
							</ffi:cinclude>
							</td>
							<td class="columndata"   nowrap>
								&nbsp;&nbsp;<s:text name="jsp.reports_579"/>
							</td>
							<td>&nbsp;</td>
							<%-- <td>
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_EXTRA_FOOTER_LINE %>" />
								<input class="txtbox" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_EXTRA_FOOTER_LINE %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue"/>" size="24" maxlength="255">
							</td> --%>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="paddingLeft10">
					<span class="paddingLeft10">&nbsp;</span><ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_EXTRA_FOOTER_LINE %>" />
					&nbsp;&nbsp;<input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_EXTRA_FOOTER_LINE %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue"/>" size="24" maxlength="255">
				</td>
			</tr>
			<ffi:cinclude value1="${format}" value2="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" operator="equals">
				<tr>
					<td align="left" class="columndata" width="115">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="10">
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_FOOTER %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
									<s:checkbox name="ROpt_SHOWFOOTER" fieldValue="TRUE" value="true"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
									<s:checkbox name="ROpt_SHOWFOOTER" fieldValue="TRUE"/>
								</ffi:cinclude>
								</td>
								<td class="columndata" nowrap>
									&nbsp;&nbsp;<s:text name="jsp.reports_636"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude value1="${ShowColumnHeaderOption}" value2="TRUE" operator="equals">
				<tr>
					<td class="columndata" width="115"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" height="10" width="0" border="0"></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><s:text name="jsp.reports_663"/></td>
				</tr>
				<tr>
					<td class="columndata"><br></td>
				</tr>
				<tr>
					<td align="left" class="columndata" width="115">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td width="10">
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_REPEAT_COLUMN_HEADER %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="equals">
									<s:checkbox name="ROpt_REPEAT_COLUMN_HEADER" fieldValue="TRUE" value="true"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="TRUE" operator="notEquals">
									<s:checkbox name="ROpt_REPEAT_COLUMN_HEADER" fieldValue="TRUE"/>
								</ffi:cinclude>
								</td>
								<td class="columndata" nowrap>
									&nbsp;&nbsp;<s:text name="jsp.reports_648"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</ffi:cinclude>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="center">
					<br>
					<div align="center" class="ui-widget-header customDialogFooter">
						<sj:a 
							         button="true" 
					         onClickTopics="closeDialog"
					         ><s:text name="jsp.default_82"/></sj:a>
					         
						<sj:a 
							formIds="HeaderFooterForm"
							targets="setheadfootdialog"
							button="true"
							onBeforeTopics="beforeSaveHeadFootSetting" 
							onSuccessTopics="successSaveHeadFootSetting"
							onErrorTopics="errorSaveHeadFootSetting"  
							onCompleteTopics="completeSaveHeadFootSetting" 
							><s:text name="jsp.default_366"/></sj:a>	
						</div>				
				</td>
			</tr>
			<tr>
				<td class="columndata"><br></td>
			</tr>
		    </table>
		</div>
	    </s:form>
	</div>
