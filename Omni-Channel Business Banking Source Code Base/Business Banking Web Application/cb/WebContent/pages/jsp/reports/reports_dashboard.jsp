<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

    
    <div class="dashboardUiCls">
	   	<div class="moduleSubmenuItemCls">
		<%
			String subpage = (String) session.getAttribute("subpage");
		
			if( subpage != null && subpage.equals("audit") ) {
		%>
		    <span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-audit"></span>
	   		<span class="moduleSubmenuLbl">
	   			<s:text name="jsp.default_53" />
	   		</span>
		<% } else if( subpage != null && subpage.equals("cashmgmt")) { %>
		    <span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-cashFlow"></span>
	   		<span class="moduleSubmenuLbl">
	   			<s:text name="jsp.home_50" />
	   		</span>
		<% } else if( subpage != null && subpage.equals("pandt")) { %>
		    <span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-transfer"></span>
	   		<span class="moduleSubmenuLbl">
	   			<s:text name="jsp.home_152" />
	   		</span>
		<% } else{ %>
			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-accountHistory"></span>
	   		<span class="moduleSubmenuLbl">
	   			<s:text name="jsp.home_21" />
	   		</span>
		<% } %>
	   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
	   		<s:include value="/pages/jsp/home/inc/reportingSubmenuDropdown.jsp" />
	  	</div>
	  	
  	</div>
  	
