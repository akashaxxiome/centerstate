<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="BalanceSummaryReport" operator="equals">
	<ffi:help id="bal-summary_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="CustomSummaryReport" operator="equals">
	<ffi:help id="custom-summary_report" className="moduleHelpClass" />
</ffi:cinclude>


<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>

<%-- data classification --%>
<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_354"/>&nbsp;</td>
	
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
		<select style="width:160px" id="reports_balsum_rpton_list" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" size="1" onchange="ns.report.refresh();" >
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_634"/>
			</option>
</ffi:cinclude> <%-- End if entitled to summary previous day --%>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_603"/>
			</option>
</ffi:cinclude> <%-- End if entitled to summary intra day --%>
		</select>
	</td>
	
	<td>
	</td>
	
	<td>
	</td>
</tr>

<%-- account --%>
<ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
<%-- This is a summary report so we will filter accounts on the summary view entitlement --%>
<ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
<%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
<%-- entitled to view under that data classification --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>"/>
</ffi:cinclude>
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>"/>
</ffi:cinclude>

<ffi:process name="AccountEntitlementFilterTask"/>

<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_15"/>&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
		<select class="" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" style="width:350px;" id="balSummaryAccountDropdown" size="5" multiple="multiple">
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		  <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		  <ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="bankAccount">
			<ffi:setProperty name="tmp_val" value="${bankAccount.ID}:${bankAccount.BankID}:${bankAccount.RoutingNum}:${bankAccount.NickNameNoCommas}:${bankAccount.CurrencyCode}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<ffi:cinclude value1="${bankAccount.RoutingNum}" value2="" operator="notEquals">
					<ffi:getProperty name="bankAccount" property="RoutingNum"/> :
				</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${bankAccount.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="bankAccount" property="NickName"/>
				</ffi:cinclude>
				 - <ffi:getProperty name="bankAccount" property="CurrencyCode"/>
			</option>
		  </ffi:list>
		  <ffi:removeProperty name="tmp_val"/>
		  <ffi:removeProperty name="bankAccount"/>
		  <ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>

<%-- start/end dates --%>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime-previous-biz-day.jsp" />

	
<script>
	$("#balSummaryAccountDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>	


<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="CheckPerAccountReportingEntitlements" />
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
