<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ page import="java.util.*" %>
<ffi:help id="cashflowforecast_report" className="moduleHelpClass" />
<s:include value="%{#session.PagesPath}inc/datetime_js.jsp" />
<script type="text/javascript"><!--	
ns.report.validateForm = function( formName )
{
	return true;
}
// --></script>

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
 <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
 <ffi:process name="CheckPerAccountReportingEntitlements"/>
 
 <%-- data classification --%>
<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
<%-- This is a detail report so we will filter accounts on the detail view entitlement --%>
<ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
<%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
<%-- entitled to view under that data classification --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>"/>
</ffi:cinclude>
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>"/>
</ffi:cinclude>
<ffi:process name="AccountEntitlementFilterTask"/>

<% 
boolean ent_previous_day=false;
boolean ent_intra_day=false;
%>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>" >
	<% ent_previous_day=true; %>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>" >
	<% ent_intra_day=true; %>
</ffi:cinclude>
<tr>
	<td align="right" class="sectionsubhead">
	<% 
	if( ent_previous_day || ent_intra_day ) { 
	%>
		<s:text name="jsp.default_354"/>&nbsp;
	<%
	}	// end if
	%>
	</td>
	<td>
	<% 
	if( ent_previous_day || ent_intra_day ) { 
	%>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
		<select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" size="1" onchange="ns.report.refresh();" >
			<% 
			if( ent_previous_day ) { 
			%>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_634"/>
			</option>
			<%
			}
			if( ent_intra_day ) {
			%>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_603"/>
			</option>
			<%
			}
			%>
		</select>
	<%
	}	// end if
	%>
	</td>
	<td>
		&nbsp;
	</td>
	<td>
		&nbsp;
	</td>
</tr>

<%-- account --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_15"/>&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
		<select id="cashFlowForeCastAccDropdown"  style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>" />
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		  <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		  <ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
		  <ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="bankAccount">
			<ffi:setProperty name="tmp_val" value="${bankAccount.ID}:${bankAccount.BankID}:${bankAccount.RoutingNum}:${bankAccount.NickNameNoCommas}:${bankAccount.CurrencyCode}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<ffi:cinclude value1="${bankAccount.RoutingNum}" value2="" operator="notEquals" >
					<ffi:getProperty name="bankAccount" property="RoutingNum"/> :
				</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${bankAccount.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="bankAccount" property="NickName"/>
				</ffi:cinclude>
				 - <ffi:getProperty name="bankAccount" property="CurrencyCode"/>
			</option>
		  </ffi:list>
		  <ffi:removeProperty name="tmp_val"/>
		  <ffi:removeProperty name="bankAccount"/>
		  <ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime.jsp"/>
<%-- forecast date --%>
<tr>	
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_584"/>&nbsp;<span class="required">*</span></td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_FORECAST_DATE %>" />
		<input 
			id="forecastDateInput"
			class="ui-widget-content ui-corner-all"
			style="margin-right:5px;"
			type="text" 
			name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_FORECAST_DATE %>" 
			value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />"
		 	size="12">
		 <script type="text/javascript">
		 	if( $("#forecastDateInput").val() == null || $("#forecastDateInput").val().length == 0 )
		 	{
		 		$("#forecastDateInput").val("<% 
		 				UserLocale userLocale = (UserLocale)session.getAttribute("UserLocale");
						Locale locale = userLocale.getLocale();
					%><%=
						com.ffusion.util.DateFormatUtil.getFormatter(userLocale.getDateFormat()).format( new GregorianCalendar( locale ).getTime() )
				%>");
		 	}
	 		
		 	$("#forecastDateInput").datepicker({
		 		constrainInput: false,
				showOn: "both",
				buttonImage: '<s:url value="/struts/js/"/>calendar.gif',
				buttonImageOnly: true
		 	});
		 </script>
		<input type="Hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.DATE_FLAG_PREFIX %><%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_FORECAST_DATE %>" value="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.DATETIME_FLAG_ENABLE_STRING %>">
	</td>
	<td>
		&nbsp;
	</td>
	<td>
		&nbsp;
	</td>
</tr>

<%-- <tr><td colspan="4"><div align="center"><span class="required">* <s:text name="jsp.default_240"/></span></div></td></tr> --%>
<ffi:removeProperty name="CriterionListChecker" />
<script>
	$("#cashFlowForeCastAccDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>