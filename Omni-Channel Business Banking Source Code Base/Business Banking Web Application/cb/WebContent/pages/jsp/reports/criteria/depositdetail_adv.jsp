<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="deposit-detail_report" className="moduleHelpClass" />
<%-- Submit a list of comma-delimited transaction type codes. (From com.ffusion.beans.banking.TransactionTypes).  The following line
     is an example of how to use multiple types (in this case Deposit (1) and Direct Deposit (29)
<ffi:setProperty name="TypeCodes" value="1,29"/>
--%>
<ffi:setProperty name="TypeCodes" value="1"/>
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>" value="<ffi:getProperty name="TypeCodes"/>">

<%-- This property is used to display the option of displaying a new header for each transaction.  The variable is cleared
     on the reporting list page, and when the user leaves the reporting sub menu.  If the option should be displayed, 
     this variable should be TRUE. --%>
<ffi:setProperty name="ShowColumnHeaderOption" value="TRUE"/>

<s:include value="%{#session.PagesPath}common/checkAmount_js.jsp"/>
<script type="text/javascript"><!--
ns.report.validateForm = function( formName )
{
	return true;
}
// --></script>

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />
 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
 <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
 <ffi:process name="CheckPerAccountReportingEntitlements"/>
 
 <%-- data classification --%>
<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>
 <tr>
 	<td align="right" class="sectionsubhead"><s:text name="jsp.default_354"/>&nbsp;</td>
 	
 	<td>
 		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" size="1" onchange="ns.report.refresh();" >
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_634"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to detail previous day --%>
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_603"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to detail intra day --%>
 		</select>
 	</td>
 	
 	<td>
 	</td>
 	
 	<td>
 	</td>
 </tr>
 
<%-- account --%>
 <ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
 <%-- This is a detail report so we will filter accounts on the detail view entitlement --%>
 <ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
 <%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
 <%-- entitled to view under that data classification --%>
 <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>"/>
 </ffi:cinclude>
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>"/>
 </ffi:cinclude>
 
 <ffi:process name="AccountEntitlementFilterTask"/>
 
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_15"/>&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
		<select id="depositDetailDropDown"  style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		  <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		  <ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="bankAccount">
			<ffi:setProperty name="tmp_val" value="${bankAccount.ID}:${bankAccount.BankID}:${bankAccount.RoutingNum}:${bankAccount.NickNameNoCommas}:${bankAccount.CurrencyCode}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<ffi:cinclude value1="${bankAccount.RoutingNum}" value2="" operator="notEquals" >
					<ffi:getProperty name="bankAccount" property="RoutingNum"/> :
				</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${bankAccount.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="bankAccount" property="NickName"/>
				</ffi:cinclude>
				 - <ffi:getProperty name="bankAccount" property="CurrencyCode"/>
			</option>
		  </ffi:list>
		  <ffi:removeProperty name="tmp_val"/>
		  <ffi:removeProperty name="bankAccount"/>
		  <ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>



<%-- start/end dates --%>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<%-- if the data classification is intra day, we wish the date include page to display the "intra day new" option --%> 
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="ShowTodayIntradayNew" value="TRUE"/>
</ffi:cinclude>
<s:include value="%{#session.PagesPath}inc/datetime.jsp"/>
<ffi:removeProperty name="ShowTodayIntradayNew"/>

<%-- amount --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>	
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>" />
		<input class="ui-widget-content ui-corner-all"
			type="text" style="width:72px" 
			name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
		<span class="columndata">&nbsp;&nbsp;<s:text name="jsp.default_423.1"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>" />
		<input class="ui-widget-content ui-corner-all" 
			type="text" style="width:71px" 
			name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
	</td>
</tr>

<%-- Customer Reference Number --%>
<%-- Bank Reference Number --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_551"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOMER_REF_NUM %>" />
		<input class="ui-widget-content ui-corner-all"
			type="text" style="width:188px" 
			name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOMER_REF_NUM %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="20" maxlength="<%= com.ffusion.dataconsolidator.constants.DCConstants.CUSTREFNUM_MAXLENGTH %>">
	<%-- </td>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_517"/>&nbsp;</td>
	<td> --%>
		<span class="reportsCriteriaContent"><s:text name="jsp.reports_517"/> &nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_BANK_REF_NUM %>" />
		<input style="width:190px" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_BANK_REF_NUM %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="20" maxlength="<%= com.ffusion.dataconsolidator.constants.DCConstants.BANKREFNUM_MAXLENGTH %>">
	</td>
</tr>

<%-- Transaction codes, empty row (above Sort 1, Sort 2) --%>
<ffi:object name="com.ffusion.tasks.util.ConvertTransTypesToBAITypes" id="GetBAICodes"/>
<ffi:setProperty name="GetBAICodes" property="TransactionTypes" value="${TypeCodes}"/>
<ffi:setProperty name="GetBAICodes" property="SessionName" value="BAICodes"/>
<ffi:process name="GetBAICodes"/>
<ffi:object name="com.ffusion.tasks.util.LookupBAICodeInfo" id="LookupBAICodes"/>
<ffi:setProperty name="LookupBAICodes" property="BAICodes" value="${BAICodes}"/>
<ffi:process name="LookupBAICodes"/>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.reports_686"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_SUBTYPE %>" />
		<ffi:setProperty name="currTransType" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
		<select  id="depositDetailTransDropDown"  style="width:350px;"  name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_SUBTYPE %>" size="5" MULTIPLE>

			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currTransType" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_SUBTYPE %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_SUBTYPE %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.reports_502"/>
			</option>
			<ffi:object name="com.ffusion.tasks.banking.GetBAITypeCodeDescription" id="GetBAITypeCodeDescription" scope="request" />
			<ffi:setProperty name="GetBAITypeCodeDescription" property="BAITypeCodeInfoItemName" value="codeInfoItem" />
			<ffi:setProperty name="GetBAITypeCodeDescription" property="LocaleName" value="<%= com.ffusion.tasks.banking.Task.LOCALE %>" />
			<ffi:list collection="LookupBAICodes.CodeInfoList" items="codeInfoItem">
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${codeInfoItem.Code}" />
				<option
					value="<ffi:getProperty name='codeInfoItem' property='Code'/>"
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="codeInfoItem" property="Code"/>&nbsp;-&nbsp; 
					<ffi:process name="GetBAITypeCodeDescription" />
					<ffi:getProperty name="GetBAITypeCodeDescription" property="Description"/>
				</option>
			</ffi:list>
			<ffi:removeProperty name="GetBAITypeCodeDescription" />
		</select>
	</td>
	<!-- <td colspan="2">&nbsp;</td> -->
</tr>

<%-- Transaction Codes (from above), Sort 1 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<%-- Set default sort --%>
		<ffi:cinclude value1="" value2="${currSort}" operator="equals" >
			<ffi:setProperty name="currSort" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" />
		</ffi:cinclude>
		<select class="txtbox" style="width:200px" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_19"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_439"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_TRANS_SUBTYPE%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_TRANS_SUBTYPE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_685"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_AMOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_518"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_552"/>
			</option>
		</select>
	</td>
</tr>
<%-- Transaction Codes (from above), Sort 2 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_668"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<%-- Set default sort --%>
		<ffi:cinclude value1="" value2="${currSort}" operator="equals" >
			<ffi:setProperty name="currSort" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" />
		</ffi:cinclude>
		<select class="txtbox" style="width:200px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_19"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_439"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_TRANS_SUBTYPE%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_TRANS_SUBTYPE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_685"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_AMOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_518"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_552"/>
			</option>
		</select>
	</td>
</tr>

<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_PAYEE_PAYOR %>" value="" size="20">
<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_PAYOR_NUM %>" value="" size="20">
<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ORIG_USER %>" value="" size="20">
<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_PO_NUM %>" value="" size="20">
<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_NUM %>" value="" size="20">


<script>
	$("#depositDetailDropDown").extmultiselect({header: ""}).multiselectfilter();
	$("#depositDetailTransDropDown").extmultiselect({header: ""}).multiselectfilter();
</script>
<%-- Housekeeping --%>
<ffi:removeProperty name="currSort"/>
<ffi:removeProperty name="TypeCodes"/>
<ffi:removeProperty name="GetBAICodes"/>
<ffi:removeProperty name="LookupBAICodes"/>
<ffi:removeProperty name="BAICodes"/>
<ffi:removeProperty name="TypeCodes"/>
<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="CheckPerAccountReportingEntitlements" />
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
