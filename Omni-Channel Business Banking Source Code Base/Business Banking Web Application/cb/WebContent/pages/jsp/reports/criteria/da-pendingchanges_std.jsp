<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

<tr>
	<td class="sectionsubhead" align="right"><s:text name="jsp.default_225"/></td>
	<td class="sectionsubhead">
		<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
		<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
		<ffi:process name="GetGroupsAdministeredBy"/>
		<ffi:removeProperty name="GetGroupsAdministeredBy"/>

		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_GROUP %>"/>
		<select class="txtbox" 
			name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_GROUP %>" 
			id="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_GROUP %>"
			style="width: 300px" 
			onchange="ns.report.refresh();">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" 
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
				<s:text name="jsp.reports_495"/>
			</option>
									
			<ffi:list collection="Entitlement_EntitlementGroups" items="ENT_GROUPS_1">
				<ffi:setProperty name="group-pre" value=""/>
				<ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Division" operator="equals">
					<ffi:setProperty name="group-pre" value="&nbsp;&nbsp;&nbsp;"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Group" operator="equals">
					<ffi:setProperty name="group-pre" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
				</ffi:cinclude>

				<option value="<ffi:getProperty name="ENT_GROUPS_1" property="GroupId"/>" 
				<ffi:cinclude value1="${ENT_GROUPS_1.GroupId}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
						
						<%
						String groupPre = ((String) session.getAttribute("group-pre")).equals("") ? "":(String)session.getAttribute("group-pre");
						%>
						<%=groupPre%><ffi:getProperty name="ENT_GROUPS_1" property="GroupName"/>
<%
	String entGroupType;
	java.util.Locale locale = (java.util.Locale)session.getAttribute(com.ffusion.tasks.Task.LOCALE);
%>
<ffi:getProperty name="ENT_GROUPS_1" property="EntGroupType" assignTo="entGroupType"/>
					( <%= com.ffusion.beans.reporting.ReportConsts.getEntGroupType( entGroupType, locale)%> )
				</option>
			</ffi:list>
		</select>
	</td>
</tr>
<tr>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" operator="equals">
		<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroups" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementGroupsSessionName" value="Entitlement_EntitlementGroups"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="BusinessEmployeesSessionName" value="AdministeredBusinessEmployees"/>
		<ffi:process name="GetBusinessEmployeesByEntGroups"/>
		<ffi:removeProperty name="GetBusinessEmployeesByEntGroups"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" operator="notEquals">
		<ffi:object id="GetBusinessEmployeesByEntGroupId" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroupId" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroupId" property="EntitlementGroupId" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroupId" property="BusinessEmployeesSessionName" value="AdministeredBusinessEmployees"/>
		<ffi:process name="GetBusinessEmployeesByEntGroupId"/>
	</ffi:cinclude>
	<td class="sectionsubhead" align="right"><s:text name="jsp.reports_688"/></td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_USER %>"/>
		<select class="txtbox" 
			name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_USER %>"
			id="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_USER %>"
			style="width: 300px" 
			>
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>"
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
				<s:text name="jsp.reports_504"/>
			</option>
			<ffi:list collection="AdministeredBusinessEmployees" items="BusinessEmployeeItem">
				<option value="<ffi:getProperty name="BusinessEmployeeItem" property="Id"/>" 
				<ffi:cinclude value1="${BusinessEmployeeItem.Id}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<ffi:getProperty name="BusinessEmployeeItem" property="SortableFullNameWithLoginId"/>
				</option>
			</ffi:list>
		</select>
	</td>
</tr>
<ffi:removeProperty name="AdministeredBusinessEmployees"/>
