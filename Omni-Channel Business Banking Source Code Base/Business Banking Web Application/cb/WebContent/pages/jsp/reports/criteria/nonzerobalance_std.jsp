<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="nzb_report" className="moduleHelpClass" />
<%-- set the AFFILIATE_BANK to the business' affiliate bank ID --%>
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_AFFILIATE_BANK %>" value="${SecureUser.AffiliateID}">
<%-- set the BUSINESS to the business' ID --%>
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_BUSINESS %>" value="${SecureUser.BusinessID}">

<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>

<%-- start/end dates --%>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE" />
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />
<ffi:removeProperty name="ShowPreviousBusinessDay"/>

<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_569"/>&nbsp;</td>
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_DISPLAY %>" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_DISPLAY %>">
			<option value="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_VALUE_DISPLAY_SUMMARY_ONLY %>" <ffi:cinclude value1="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_VALUE_DISPLAY_SUMMARY_ONLY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> > <s:text name="jsp.reports_676"/> </option>
			<option value="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_VALUE_DISPLAY_SUMMARY_AND_DETAIL %>" <ffi:cinclude value1="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_VALUE_DISPLAY_SUMMARY_AND_DETAIL %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> > <s:text name="jsp.reports_674"/> </option>
		</select>
	</td>
</tr>

<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_656"/>&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_ROLLED_NZB %>" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_ROLLED_NZB %>">
			<option value="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_VALUE_ROLLED_NZB_INCLUDE %>" <ffi:cinclude value1="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_VALUE_ROLLED_NZB_INCLUDE %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> > <s:text name="jsp.reports_601"/> </option>
			<option value="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_VALUE_ROLLED_NZB_EXCLUDE %>" <ffi:cinclude value1="<%= com.ffusion.beans.treasurydirect.TDReportConsts.SEARCH_CRITERIA_VALUE_ROLLED_NZB_EXCLUDE %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> > <s:text name="jsp.reports_577"/></option>
		</select>
		
	</td>
</tr>


<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="CheckPerAccountReportingEntitlements" />
<ffi:removeProperty name="AccountEntitlementFilterTask"/>


