<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="ach-file-upload_report" className="moduleHelpClass" />
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />
<ffi:setProperty name="NoConsumer" value="TRUE" />

<tr>
    <td align="right" class="sectionsubhead" nowrap><s:text name="jsp.reports_688"/></td>
    <td>
	    <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_USER %>">
		    <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>" ><s:text name="jsp.reports_504"/></option>

        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_FILTER_USER_NAME %>" />
		<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="" operator="notEquals">

        <ffi:object name="com.ffusion.tasks.user.GetFilteredBusinessEmployees" id="GetFilteredBusinessEmployees"/>
	    <%-- here, a business has been selected.  so do not need to set affiliate info, since the business belongs to
									     the selected affiliate bank --%>
	    <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_FILTER_BUSINESS_NAME %>" />

        <ffi:setProperty name="GetFilteredBusinessEmployees" property="BusinessName" value="${SecureUser.BusinessName}"/>

		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_FILTER_USER_NAME %>" />
		<ffi:setProperty name="GetFilteredBusinessEmployees" property="LastName" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
		<ffi:process name="GetFilteredBusinessEmployees"/>
		<ffi:setProperty name="BusinessEmployees" property="SortedBy" value="LAST"/>

		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_USER %>" />
		<ffi:list collection="BusinessEmployees" items="Employee">
			<option value="<ffi:getProperty name="Employee" property="Id"/>"
				<ffi:cinclude value1="${Employee.Id}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<ffi:cinclude value1="${NameConvention}" value2="dual" operator="equals">
					<ffi:getProperty name="Employee" property="LastName"/>,
					<ffi:getProperty name="Employee" property="FirstName"/>
					<ffi:getProperty name="Employee" property="MiddleName"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${NameConvention}" value2="dual" operator="notEquals">
					<ffi:getProperty name="Employee" property="LastName"/>
				</ffi:cinclude>
				&nbsp;
				(<ffi:getProperty name="Employee" property="UserName"/>:
				<ffi:getProperty name="Employee" property="BusinessCustId"/>)
			</option>
		</ffi:list>
        </ffi:cinclude>
        </select>
    </td>

    <td align="right" class="sectionsubhead"><s:text name="jsp.reports_689"/></td>
	<td align="left">
	<table border="0" cellspacing="1" cellpadding="0">
		<tr>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_FILTER_USER_NAME %>" />
			<td align="left"><input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_FILTER_USER_NAME %>" size="25" border="0" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />"></td>
			<td><span class="columndata">&nbsp;&nbsp;</span></td>
			<td align="left" nowrap><sj:a
								onClick="document.criteriaFormName.FilterUsers.value=true;ns.report.refresh();"
								button="true"
								><s:text name="jsp.default_373"/></sj:a></td>
		</tr>
	</table>
	<input type="hidden" name="FilterUsers">

    </td>
</tr>

<tr>
    <td class="sectionsubhead" align="right" nowrap><s:text name="jsp.default_388"/></td>
    <td>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_STATUS %>" />
        <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_STATUS %>" size="1">
            <ffi:object id="StatusList" name="com.ffusion.tasks.util.ResourceList" scope="session"/>
            <ffi:setProperty name="StatusList" property="ResourceFilename" value="com.ffusion.beansresources.reporting.ach_status" />
            <ffi:setProperty name="StatusList" property="ResourceID" value="ACHFileUploadStatuses" />
            <ffi:process name="StatusList"/>

            <ffi:object id="StatusResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
            <ffi:setProperty name="StatusResource" property="ResourceFilename" value="com.ffusion.beansresources.reporting.ach_status" />
            <ffi:process name="StatusResource" />

            <ffi:getProperty name="StatusList" property="Resource"/>
            <ffi:list collection="StatusList" items="item">
                <ffi:setProperty name="StatusResource" property="ResourceID" value="${item}" />
                <option <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="${item}">selected</ffi:cinclude> value="<ffi:getProperty name="item"/>"><ffi:getProperty name='StatusResource' property='Resource'/></option>
            </ffi:list>
        </select>
    </td>
    <td align="right" class="sectionsubhead" nowrap><s:text name="jsp.reports_556"/></td>
    <td class="sectionsubhead">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_DEBIT_FROM_AMOUNT %>" />
        <input class="ui-widget-content ui-corner-all" type="text" size="12" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_DEBIT_FROM_AMOUNT %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" >
        <s:text name="jsp.default_423.1"/>&nbsp;
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_DEBIT_TO_AMOUNT %>" />
        <input class="ui-widget-content ui-corner-all" type="text" size="12" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_DEBIT_TO_AMOUNT %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" >
    </td>
</tr>

<s:set var="showNote" value="%{'false'}"/>

	<ffi:cinclude value1="${SameDayACHEnabledForUser}" value2="true">
		<s:set var="showNote" value="%{'true'}"/>
	</ffi:cinclude>



	<ffi:cinclude value1="${SameDayChildSPEnabledForUser}" value2="true">
		<s:set var="showNote" value="%{'true'}"/>
	</ffi:cinclude>

	<ffi:cinclude value1="${SameDayTaxEnabledForUser}" value2="true">
		<s:set var="showNote" value="%{'true'}"/>
	</ffi:cinclude>


<s:if test="%{#showNote=='true'}">
<tr>
<td align="right" class="sectionsubhead"><s:text name="jsp.report.SameDayACH"/>&nbsp;</td>
<td class="columndata" align="left">
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH %>" />
<input type="checkbox"  name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH %>" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH_TRUE %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH_TRUE %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >checked</ffi:cinclude> />
</td>
</tr>
</s:if>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.reports_567"/>&nbsp;</td>
    <td align="left">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_ACH_FILE_DETAIL_LEVEL %>" />

        <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_ACH_FILE_DETAIL_LEVEL %>" size="1">
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_FILE_SUMMARY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_FILE_SUMMARY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_583"/>
                </option>
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_BATCH_SUMMARY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_BATCH_SUMMARY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_523"/>
                </option>
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_565"/>
                </option>
        </select>
    </td>

    <td align="right" class="sectionsubhead" nowrap><s:text name="jsp.reports_545"/> </td>
    	<td class="sectionsubhead">
       		 <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_CREDIT_FROM_AMOUNT %>" />
        		<input class="ui-widget-content ui-corner-all" type="text" size="12" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_CREDIT_FROM_AMOUNT %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" >
        		<s:text name="jsp.default_423.1"/>&nbsp;
       		 <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_CREDIT_TO_AMOUNT %>" />
       		 <input class="ui-widget-content ui-corner-all" type="text" size="12" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_CREDIT_TO_AMOUNT %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" >
    </td>
</tr>
<tr>
    <td class="sectionsubhead" align="right" ><s:text name="jsp.default_384"/></td>
    <td>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
        <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
            <option value="<%=com.ffusion.beans.bcreport.BCReportConsts.SORT_CRITERIA_PROCESS_DATE%>"
                <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.SORT_CRITERIA_PROCESS_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_137"/>
            </option>
        </select>
    </td>
    <td></td>
    <td></td>
</tr>

