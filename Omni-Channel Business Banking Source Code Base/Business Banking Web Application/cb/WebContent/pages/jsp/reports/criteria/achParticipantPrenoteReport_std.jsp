<%@ page import="com.ffusion.beans.XMLStrings,
                 com.ffusion.beans.ach.ACHReportConsts"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="ach-participant-prenote_report" className="moduleHelpClass" />
<%-- Get ACH companies --%>
<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies"/>
	<ffi:setProperty name="GetACHCompanies" property="CustID" value="${User.BUSINESS_ID}" />
	<ffi:setProperty name="GetACHCompanies" property="FIID" value="${User.BANK_ID}" />
<ffi:process name="GetACHCompanies"/>
<ffi:setProperty name="${GetACHCompanies.CompaniesInSessionName}" property="SortedBy" value="CONAME" />

<%-- ach company --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_24"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_COMPANY_ID %>" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_COMPANY_ID %>" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_489"/>
			</option>
		<ffi:list collection="${GetACHCompanies.CompaniesInSessionName}" items="company">
            <ffi:cinclude value1="${company.ACHPaymentEntitled}" value2="TRUE" operator="equals" >
                <option value="<ffi:getProperty name='company' property='CompanyID'/>"
                <ffi:cinclude value1="${company.CompanyID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                    <ffi:getProperty name="company" property="CompanyName"/>
                </option>
            </ffi:cinclude>
		</ffi:list>
		<ffi:removeProperty name="company" />
	</td>
</tr>


<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- prenote status --%>
<ffi:object id="PrenoteStatus" name="com.ffusion.tasks.util.Resource" scope="session"/>
<ffi:setProperty name="PrenoteStatus" property="ResourceFilename" value="com.ffusion.beansresources.reporting.ach_status" />
<ffi:process name="PrenoteStatus" />





<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_633"/>&nbsp;</td>
	<td>
		  <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_PRENOTE_STATUS %>" />
		    <select style="width:194px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_PRENOTE_STATUS %>" size="1">

			<option value="<%=ACHReportConsts.CC_LOCATION_PRENOTE_PENDING%>"
			<ffi:cinclude value1="<%=ACHReportConsts.CC_LOCATION_PRENOTE_PENDING%>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
            <ffi:setProperty name="PrenoteStatus" property="ResourceID" value="<%=ACHReportConsts.CC_LOCATION_PRENOTE_PENDING%>" />
            <ffi:getProperty name='PrenoteStatus' property='Resource'/>
			</option>

			<option value="<%=ACHReportConsts.CC_LOCATION_PRENOTE_FAILED%>"
			<ffi:cinclude value1="<%=ACHReportConsts.CC_LOCATION_PRENOTE_FAILED%>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
            <ffi:setProperty name="PrenoteStatus" property="ResourceID" value="<%=ACHReportConsts.CC_LOCATION_PRENOTE_FAILED%>" />
            <ffi:getProperty name='PrenoteStatus' property='Resource'/>
			</option>

			<option value="<%=ACHReportConsts.CC_LOCATION_PRENOTE_SUCCESS%>"
			<ffi:cinclude value1="<%=ACHReportConsts.CC_LOCATION_PRENOTE_SUCCESS%>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
            <ffi:setProperty name="PrenoteStatus" property="ResourceID" value="<%=ACHReportConsts.CC_LOCATION_PRENOTE_SUCCESS%>" />
            <ffi:getProperty name='PrenoteStatus' property='Resource'/>
			</option>

			<option value="<%=ACHReportConsts.SEARCH_CRITERIA_VALUE_ALL%>"
            <ffi:cinclude value1="<%=ACHReportConsts.SEARCH_CRITERIA_VALUE_ALL%>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
            <s:text name="jsp.reports_499"/>
			</option>
	    </select>
	</td>
</tr>

<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_88"/>&nbsp;</td>
	<td>
		  <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= ACHReportConsts.SEARCH_CRITERIA_PRENOTE_CATEGORY %>" />
		    <select style="width:194px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= ACHReportConsts.SEARCH_CRITERIA_PRENOTE_CATEGORY %>" size="1">

			<option value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_MANAGED%>"
			<ffi:cinclude value1="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_MANAGED%>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
            <ffi:setProperty name="PrenoteStatus" property="ResourceID" value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_MANAGED%>" />
            <ffi:getProperty name='PrenoteStatus' property='Resource'/>
			</option>

			<option value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_ALL_PRENOTES%>"
			<ffi:cinclude value1="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_ALL_PRENOTES%>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
            <ffi:setProperty name="PrenoteStatus" property="ResourceID" value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_ALL_PRENOTES%>" />
            <ffi:getProperty name='PrenoteStatus' property='Resource'/>
			</option>

			<option value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_ACH_ONLY%>"
			<ffi:cinclude value1="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_ACH_ONLY%>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
            <ffi:setProperty name="PrenoteStatus" property="ResourceID" value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_ACH_ONLY%>" />
            <ffi:getProperty name='PrenoteStatus' property='Resource'/>
			</option>

			<option value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_TAX_ONLY%>"
			<ffi:cinclude value1="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_TAX_ONLY%>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
            <ffi:setProperty name="PrenoteStatus" property="ResourceID" value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_TAX_ONLY%>" />
            <ffi:getProperty name='PrenoteStatus' property='Resource'/>
			</option>

			<option value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_CHILD_ONLY%>"
			<ffi:cinclude value1="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_CHILD_ONLY%>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
            <ffi:setProperty name="PrenoteStatus" property="ResourceID" value="<%=ACHReportConsts.ACH_PRENOTE_CATEGORY_CHILD_ONLY%>" />
            <ffi:getProperty name='PrenoteStatus' property='Resource'/>
			</option>

	    </select>
	</td>
</tr>


<s:set var="showNote" value="%{'false'}"/>

<%-- Same Day Configuration check for ACH Batch --%>

<ffi:cinclude value1="${SameDayACHEnabledForUser}" value2="true">
	<s:set var="showNote" value="%{'true'}"/>
</ffi:cinclude>


<%-- Same Day Configuration check for Child Support --%>
<ffi:cinclude value1="${SameDayChildSPEnabledForUser}" value2="true">
	<s:set var="showNote" value="%{'true'}"/>
</ffi:cinclude>


<%-- Same Day Configuration check for Tax --%>
<ffi:cinclude value1="${SameDayTaxEnabledForUser}" value2="true">
	<s:set var="showNote" value="%{'true'}"/>
</ffi:cinclude>


<s:if test="%{#showNote=='true'}">
<tr>
<td align="right" class="sectionsubhead"><s:text name="jsp.report.SameDayACH"/>&nbsp;</td>
<td class="columndata" align="left">
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH %>" />
<input type="checkbox"  name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH %>" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH_TRUE %>" <ffi:cinclude value1="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_SAME_DAY_BATCH_TRUE %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals" >checked</ffi:cinclude> />
</td>
</tr>
</s:if>

<%-- housekeeping --%>
<ffi:removeProperty name="${GetACHCompanies.CompaniesInSessionName}" />
<ffi:removeProperty name="GetACHCompanies" />

