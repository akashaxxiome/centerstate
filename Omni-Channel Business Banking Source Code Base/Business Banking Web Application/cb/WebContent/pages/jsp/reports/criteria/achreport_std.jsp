<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Completed ACH Payments" operator="equals">
	<ffi:help id="ach-payments_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Total Tax Payments" operator="equals">
	<ffi:help id="tax-payments_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Total Child Support Payments" operator="equals">
	<ffi:help id="child-support-payments_report" className="moduleHelpClass" />
</ffi:cinclude>


<%-- Get ACH companies --%>
<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies"/>
	<ffi:setProperty name="GetACHCompanies" property="CustID" value="${User.BUSINESS_ID}" />
	<ffi:setProperty name="GetACHCompanies" property="FIID" value="${User.BANK_ID}" />
<ffi:process name="GetACHCompanies"/>
<ffi:setProperty name="${GetACHCompanies.CompaniesInSessionName}" property="SortedBy" value="CONAME" />

<%-- ach company --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_24"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_COMPANY_ID %>" />
		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.ach.ACHReportConsts.SEARCH_CRITERIA_COMPANY_ID %>" size="1">
			<option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_489"/>
			</option>
		<ffi:list collection="${GetACHCompanies.CompaniesInSessionName}" items="company">
            <ffi:cinclude value1="${company.ACHPaymentEntitled}" value2="TRUE" operator="equals" >
                <option value="<ffi:getProperty name='company' property='CompanyID'/>"
                <ffi:cinclude value1="${company.CompanyID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                    <ffi:getProperty name="company" property="CompanyName"/>
                </option>
            </ffi:cinclude>
		</ffi:list>
		<ffi:removeProperty name="company" />
	</td>
</tr>

<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.reports_567"/>&nbsp;</td>
    <td align="left">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_ACH_FILE_DETAIL_LEVEL %>" />

        <select style="width:194px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_ACH_FILE_DETAIL_LEVEL %>" size="1">
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_BATCH_SUMMARY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_BATCH_SUMMARY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_523"/>
                </option>
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_565"/>
                </option>
                <option value="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY_W_ADDENDA %>"
                        <ffi:cinclude value1="<%= com.ffusion.beans.bcreport.BCReportConsts.ACH_FILE_DETAIL_ENTRY_W_ADDENDA %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
                        <s:text name="jsp.reports_566"/>
                </option>
        </select>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>

<%-- housekeeping --%>
<ffi:removeProperty name="${GetACHCompanies.CompaniesInSessionName}" />
<ffi:removeProperty name="GetACHCompanies" />

