<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Transfer History Report" operator="equals">
	<ffi:help id="Transaction_History_Reports" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Transfer Detail Report" operator="equals">
	<ffi:help id="transfer-detail_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Pending Transfer Report" operator="equals">
	<ffi:help id="pending-transfer_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="Transfer By Status" operator="equals">
	<ffi:help id="transferby-status_report" className="moduleHelpClass" />
</ffi:cinclude>
<%-- If there is no transfer accounts in session, get transfer accounts --%>
<% if( session.getAttribute( com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS ) == null ) { %>
	<ffi:object id="GetTransferAccounts" name="com.ffusion.tasks.banking.GetTransferAccounts" scope="session"/>
		<ffi:setProperty name="GetTransferAccounts" property="AccountsName" value="BankingAccounts"/>
        <ffi:setProperty name="GetTransferAccounts" property="Reload" value="true"/>
	<ffi:process name="GetTransferAccounts"/>
<% } %>

<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.EXTRA_PREFIX %><%= com.sap.banking.common.constants.ServiceConstants.SERVICE %>" value="<%= com.ffusion.tasks.banking.Task.BANKING %>"/>

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<ffi:cinclude value1="${SameDayExtTransferEnabled}" value2="">
	<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
	<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayExtTransferEnabled"/>
	<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.transfer.external.sameday.support"/> 
	<ffi:process name="GetBPWProperty"/>
</ffi:cinclude>

<ffi:cinclude value1="${SameDayExtTransferEnabled}" value2="true">
	 <ffi:cinclude value1="${SameDayExtTransferForBusiness}" value2="">
		<ffi:object id="CheckSameDayEnabledForBusiness" name="com.ffusion.tasks.util.CheckSameDayEnabledForBusiness" scope="session" />
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="BusinessID" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="ModuleName" value="<%=com.ffusion.ffs.bpw.interfaces.ACHConsts.ACH_BATCH_EXT_TRANSFER%>"/>
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="SessionName" value="SameDayExtTransferForBusiness"/>
		<ffi:process name="CheckSameDayEnabledForBusiness"/>
	 </ffi:cinclude>
 </ffi:cinclude>
 
<%-- <!--L10NStart-->From Account<!--L10NEnd--> --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_217"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_FROM_ACCOUNT %>" />
		<select  id="transferHistAccDropdown"  style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_FROM_ACCOUNT %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value=""
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		<ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		<ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" property="Filter" value="All"/>
		<ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" items="acct">
			<ffi:setProperty name="tmp_val" value="${acct.ID}"/>
            <% boolean displayEntitled = true; %>
            <ffi:cinclude value1="${acct.CoreAccount}" value2="<%=com.ffusion.beans.accounts.Account.CORE%>" operator="notEquals">
                <ffi:setProperty name="acct" property="IsFilterableCheck" value="<%=com.ffusion.beans.accounts.AccountFilters.EXTERNAL_TRANSFER_FROM_FILTER%>"/>
                <ffi:cinclude value1="${acct.IsFilterable}" value2="false">
                    <% displayEntitled = false; %>
                </ffi:cinclude>
            </ffi:cinclude>
            <ffi:cinclude value1="${acct.CoreAccount}" value2="<%=com.ffusion.beans.accounts.Account.CORE%>" operator="equals">
                <ffi:setProperty name="acct" property="IsFilterableCheck" value="<%=com.ffusion.beans.accounts.AccountFilters.TRANSFER_FROM_FILTER%>"/>
                <ffi:cinclude value1="${acct.IsFilterable}" value2="false">
                    <% displayEntitled = false; %>
                </ffi:cinclude>
            </ffi:cinclude>
            <% if (displayEntitled) { %>
                <option value="<ffi:getProperty name='tmp_val'/>"
                    <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
                    <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
                >
                    <ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals" >
                        <ffi:getProperty name="acct" property="RoutingNum"/> :
                    </ffi:cinclude>

                    <ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${acct.ID}" />
                    <ffi:process name="GetAccountDisplayText" />
                    <ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

                    <ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
                         - <ffi:getProperty name="acct" property="NickName"/>
                    </ffi:cinclude>
                     - <ffi:getProperty name="acct" property="CurrencyCode"/>
                </option>
            <% } %>
		</ffi:list>
		<ffi:removeProperty name="tmp_val"/>
		<ffi:removeProperty name="acct" />
		<ffi:removeProperty name="GetAccountDisplayText"/>
	</td>
</tr>

<%-- <!--L10NStart-->To Account<!--L10NEnd--> --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_424"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_TO_ACCOUNT %>" />
		<select  id="transferHistToAccDropdown"  style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_TO_ACCOUNT %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value=""
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		<ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		<ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" property="Filter" value="All"/>
		<ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" items="acct">
			<ffi:setProperty name="tmp_val" value="${acct.ID}"/>
            <% boolean displayEntitled = true; %>
            <ffi:cinclude value1="${acct.CoreAccount}" value2="<%=com.ffusion.beans.accounts.Account.CORE%>" operator="notEquals">
                <ffi:setProperty name="acct" property="IsFilterableCheck" value="<%=com.ffusion.beans.accounts.AccountFilters.EXTERNAL_TRANSFER_TO_FILTER%>"/>
                <ffi:cinclude value1="${acct.IsFilterable}" value2="false">
                    <% displayEntitled = false; %>
                </ffi:cinclude>
            </ffi:cinclude>
            <ffi:cinclude value1="${acct.CoreAccount}" value2="<%=com.ffusion.beans.accounts.Account.CORE%>" operator="equals">
                <ffi:setProperty name="acct" property="IsFilterableCheck" value="<%=com.ffusion.beans.accounts.AccountFilters.TRANSFER_TO_FILTER%>"/>
                <ffi:cinclude value1="${acct.IsFilterable}" value2="false">
                    <% displayEntitled = false; %>
                </ffi:cinclude>
            </ffi:cinclude>
            <% if (displayEntitled) { %>
                <option value="<ffi:getProperty name='tmp_val'/>"
                    <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
                    <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
                >
                    <ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals" >
                        <ffi:getProperty name="acct" property="RoutingNum"/> :
                    </ffi:cinclude>

                    <ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${acct.ID}" />
                    <ffi:process name="GetAccountDisplayText" />
                    <ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

                    <ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
                         - <ffi:getProperty name="acct" property="NickName"/>
                    </ffi:cinclude>
                     - <ffi:getProperty name="acct" property="CurrencyCode"/>
                </option>
            <% } %>
		</ffi:list>
		<ffi:removeProperty name="tmp_val"/>
		<ffi:removeProperty name="acct" />
		<ffi:removeProperty name="GetAccountDisplayText"/>
	</td>
</tr>

<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- Amounts --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" />
		<input style="width:71px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
		<span class="columndata">&nbsp;&nbsp;<s:text name="jsp.default_423.1"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" />
		<input style="width:72px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
	</td>

</tr>
	<ffi:cinclude value1="${SameDayExtTransferForBusiness}" value2="true">
		<tr>
			<td align="right" class="sectionsubhead"><s:text name="jsp.report.SameDayTransfer"/>&nbsp;</td>
			<td colspan="3"><input type="checkbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SAME_DAY_TRANSFER %>" ></td>	
		</tr>
	</ffi:cinclude>

<%-- sort 1, sort 2 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
			<option value="" <ffi:cinclude value1="" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_FROM_ACCOUNT %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_FROM_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_217"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TO_ACCOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TO_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_424"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_AMOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_DATE %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_DATE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_STATUS %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_STATUS %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_388"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TRACE_NO %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TRACE_NO %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_683"/>
			</option>
		</select>

		<span class="reportsCriteriaContent"> <s:text name="jsp.reports_668"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
			<option value="" <ffi:cinclude value1="" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_FROM_ACCOUNT %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_FROM_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_217"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TO_ACCOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TO_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_424"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_AMOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_DATE %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_DATE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_STATUS %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_STATUS %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_388"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TRACE_NO %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TRACE_NO %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_683"/>
			</option>
		</select>
	</td>
</tr>

<%-- final housekeeping --%>
<ffi:removeProperty name="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" />
<ffi:removeProperty name="GetTransferAccounts"/>
<ffi:removeProperty name="currSort"/>
<ffi:removeProperty name="CriterionListChecker" />
<script>
	$("#transferHistAccDropdown").extmultiselect({header: ""}).multiselectfilter();
	$("#transferHistToAccDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>

