<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="wireby-status_report" className="moduleHelpClass" />
<%-- If there is no wire accounts in session, get transfer accounts --%>
<% if( session.getAttribute( com.ffusion.efs.tasks.SessionNames.WIRESACCOUNTS ) == null ) { %>
    <ffi:object id="GetWiresAccounts" name="com.ffusion.tasks.wiretransfers.GetWiresAccounts" scope="session"/>
        <ffi:setProperty name="GetWiresAccounts" property="Reload" value="true"/>
    <ffi:process name="GetWiresAccounts"/>
<% } %>

<%-- get wire transfer payees --%>
<% if( session.getAttribute( com.ffusion.efs.tasks.SessionNames.WIRE_TRANSFER_PAYEES ) == null ) { %>
        <ffi:object id="GetWireTransferPayees" name="com.ffusion.tasks.wiretransfers.GetWireTransferPayees" />
        <ffi:setProperty name="GetWireTransferPayees" property="PayeeDestination" value=""/>
        <ffi:process name="GetWireTransferPayees"/>
<% } %>

<script type="text/javascript"><!--

ns.report.validateForm = function( formName )
{
    var form = document.forms[formName];
    var s = "";
    var p = form.statusType;
    var t = p.options;
    if (!p.disabled)
    {
        for(var i = 0; i < p.length; i++){
            if (t[i].selected)
                s+=t[i].value+",";
        }
        // if no status is selected, select all statuses
        if (s == "") {
            for(var i = 0; i < p.length; i++) s+=t[i].value+",";
        }
    }
    form.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_STATUS %>.value= s;

    return true;
}

function statuses(s) {
    frm = document.criteriaFormName;
    if (s == "all") {
        frm.statusType.disabled = true;
        //document.getElementsByName("statusType").item(0).style.backgroundColor="#CCC";
    } else {
        frm.statusType.disabled = false;
        //document.getElementsByName("statusType").item(0).style.backgroundColor="#FFF";
    }
}
// --></script>

<%--
    Get the task used to look through the values for a given search criterion
    in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<%-- account --%>
<tr>
    <td align="right" class="sectionsubhead" valign="top"><s:text name="jsp.default_217"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_ACCOUNT %>" />
        <select id="wireAccStatus"  style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_ACCOUNT %>" size="5" MULTIPLE>
            <ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
            <ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
            <ffi:process name="CriterionListChecker" />

            <option value=""
                <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="" />
                <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
            >
                    <s:text name="jsp.reports_488"/>
            </option>

        <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
        <ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.WIRESACCOUNTS %>" property="Filter" value="All"/>
        <ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.WIRESACCOUNTS %>" items="acct">
            <ffi:setProperty name="tmp_val" value="${acct.ID}"/>
            <option value="<ffi:getProperty name='tmp_val'/>"
                <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
                <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
            >
                <ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals" >
                    <ffi:getProperty name="acct" property="RoutingNum"/> :
                </ffi:cinclude>

                <ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${acct.ID}" />
                <ffi:process name="GetAccountDisplayText" />
                <ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

                <ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
                     - <ffi:getProperty name="acct" property="NickName"/>
                </ffi:cinclude>
                 - <ffi:getProperty name="acct" property="CurrencyCode"/>
            </option>
        </ffi:list>
        <ffi:removeProperty name="tmp_val"/>
        <ffi:removeProperty name="acct" />
        <ffi:removeProperty name="GetAccountDisplayText"/>
    </td>
</tr>

<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- amount, sort1 --%>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" />
        <input style="width:71px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
        <span class="columndata">&nbsp;&nbsp;<s:text name="jsp.default_423.1" />&nbsp;</span>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" />
        <input style="width:71px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
    </td>
</tr>

<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
        <ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
        <select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
            <option value="" <ffi:cinclude value1="" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_ACCOUNT %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_15"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_AMOUNT %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_43"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_DATE %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_DATE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_137"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_PAYEE %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_PAYEE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_313"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_REF_NO %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_REF_NO %>" value2="${currSort}" operator="equals" >selected</ffi:cinclude> >
                <s:text name="jsp.reports_643"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_STATUS %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_STATUS %>" value2="${currSort}" >selected</ffi:cinclude> >
                <s:text name="jsp.default_388"/>
            </option>
        </select>

		<span class="reportsCriteriaContent"> <s:text name="jsp.reports_668"/>&nbsp;</span>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
        <ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
        <select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
            <option value="" <ffi:cinclude value1="" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_ACCOUNT %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_15"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_AMOUNT %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_43"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_DATE %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_DATE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_137"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_PAYEE %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_PAYEE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_313"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_REF_NO %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_REF_NO %>" value2="${currSort}" operator="equals" >selected</ffi:cinclude> >
                <s:text name="jsp.reports_643"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_STATUS %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SORT_CRITERIA_STATUS %>" value2="${currSort}" >selected</ffi:cinclude> >
                <s:text name="jsp.default_388"/>
            </option>
        </select>
    </td>
</tr>

<%-- payee, sort2 --%>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_313"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_PAYEE %>" />
        <select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_PAYEE %>" size="1">
            <option value="" <ffi:cinclude value1="" value2="${ReportData.ReportCriteria.CurrentSearchCriteriaValue}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.reports_491"/>
            </option>
            <ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.WIRE_TRANSFER_PAYEES %>" property="Filter" value="All"/>
            <ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.WIRE_TRANSFER_PAYEES %>" items="payee_1">
                <option value="<ffi:getProperty name='payee_1' property='ID' />"
                <ffi:cinclude value1='${ReportData.ReportCriteria.CurrentSearchCriterionValue}' value2='${payee_1.ID}' operator='equals' >selected</ffi:cinclude> >
                <ffi:getProperty name="payee_1" property="PayeeName"/>
                <ffi:cinclude value1="${payee_1.NickName}" value2="" operator="notEquals"> (<ffi:getProperty name="payee_1" property="NickName"/>)</ffi:cinclude>
                </option>
            </ffi:list>
            <ffi:removeProperty name="payee_1"/>
        </select>
    </td>
</tr>
<%-- statusType --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_STATUS %>" />
<%
    com.ffusion.beans.reporting.Report rpt = (com.ffusion.beans.reporting.Report)session.getAttribute( "ReportData" );
    String stati = rpt.getReportCriteria().getCurrentSearchCriterionValue();
    java.util.ArrayList statusList = new java.util.ArrayList();
    if( stati!=null && stati.length() > 0 ) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer( stati, "," );
        while( tokenizer.hasMoreTokens() ) {
            String s = tokenizer.nextToken();
            if( s!=null && s.length() > 0 ) {
                statusList.add( s );

            }
        }
    }

    //System.out.println("stati " + stati);
%>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_388"/>&nbsp;</td>
    <td class="sectionsubhead" colspan="3">
        <input type="Radio" name="reportAllStatuses" value="all" onclick="statuses(this.value)" <%= statusList.size() == 0 ? "checked" : "" %>> <s:text name="jsp.reports_500"/>
    	&nbsp;&nbsp;
    	<input type="Radio" name="reportAllStatuses" value="select" onclick="statuses(this.value)" <%= statusList.size() > 0 ? "checked" : "" %>> <s:text name="jsp.reports_661"/>
    </td>
</tr>
<%-- <tr>
    <td><br></td>
    <td class="sectionsubhead">
        <input type="Radio" name="reportAllStatuses" value="select" onclick="statuses(this.value)" <%= statusList.size() > 0 ? "checked" : "" %>> <s:text name="jsp.reports_661"/>
    </td>
    <td colspan="2"><br></td>
</tr> --%>
<tr>
    <td><br></td>
    <td>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_STATUS %>" />
        <select style="width:182px;" class="txtbox" size="12" multiple name="statusType" <%= statusList.size() == 0 ? "disabled" : "" %>>
                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACKNOWLEDGED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACKNOWLEDGED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_482"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_CREATED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_CREATED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_544"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_COMPLETED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_COMPLETED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_538"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_CANCELLED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_CANCELLED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_528"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_SCHEDULED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_SCHEDULED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_660"/>
            </option>

            		<option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_IMMED_INPROCESS %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_IMMED_INPROCESS)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_596"/>
            </option>

            		<option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_INPROCESS %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_INPROCESS)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_599"/>
            </option>

            		<option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_IN_FUNDS_APPROVAL %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_IN_FUNDS_APPROVAL)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_597"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_BACKENDREJECTED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_BACKENDREJECTED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_644"/>
            </option>

          			<option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_FUNDS_REVERTED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_FUNDS_REVERTED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_592"/>
            </option>

					<option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_RELEASEREJECTED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_RELEASEREJECTED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_646"/>
            </option>

                  	<option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_FUNDS_REVERT_FAILED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_FUNDS_REVERT_FAILED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_591"/>
            </option>
                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACCEPTED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACCEPTED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_477"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_CONFIRMED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_CONFIRMED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_539"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_HELD %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_HELD)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_514"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_RELEASEPENDING %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_RELEASEPENDING)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_645"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_FUNDS_PENDING %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_FUNDS_PENDING)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_590"/>
            </option>


            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_BACKENDFAILED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACKNOWLEDGED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_581"/>
            </option>

                    <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_NO_FUNDS %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACKNOWLEDGED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_620"/>
            </option>
         <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_FUNDS_FAILED_ON %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACKNOWLEDGED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_588"/>
            </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_PENDING_APPROVAL %>"

            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACKNOWLEDGED)) { %>
                        selected
            <% } %>
                     >
                        <s:text name="jsp.reports_508"/>
                     </option>
                               <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_APPROVAL_REJECTED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACKNOWLEDGED)) { %>
                        selected
            <% } %>
                     >
                        <s:text name="jsp.reports_509"/>
                </option>
                               <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_APPROVAL_FAILED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_ACKNOWLEDGED)) { %>
                        selected
            <% } %>
                     >
                        <s:text name="jsp.reports_507"/>
                </option>


            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_POSTEDON %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_POSTEDON)) { %>
                        selected
            <% } %>
                     >
                        <s:text name="jsp.reports_641"/>
                </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_FUNDS_APPROVED %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_FUNDS_APPROVED)) { %>
                        selected
            <% } %>
                     >
                        <s:text name="jsp.reports_589"/>
                </option>

            <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_IN_FUNDS_REVERT %>"
            <% if (statusList.contains(com.ffusion.beans.wiretransfers.WireReportConsts.RPT_STATUS_IN_FUNDS_REVERT)) { %>
                        selected
            <% } %>
                     >
                        <s:text name="jsp.reports_598"/>
                </option>

			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_SKIPPED %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_SKIPPED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_697"/>
            </option>


        </select>
        <%
            stati = null;
            statusList.clear();
            statusList = null;
        %>
    </td>

    <td>
    </td>
    <td>
    </td>
</tr>

<%-- Detail Level --%>
<tr>
        <td align="right" class="sectionsubhead"><s:text name="jsp.default_259"/></td>
        <td align="left">
                <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL %>" />
                <select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL %>" size="1">
                        <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL_SUMMARY %>" <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL_SUMMARY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_400"/></option>
                        <option value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL_DETAILED %>" <ffi:cinclude value1="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_LEVEL_DETAILED %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude>  ><s:text name="jsp.reports_568"/></option>
                </select>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
</tr>

<%-- hidden field that holds the status type string used in backend --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_STATUS %>" />
<input type="hidden" name='<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_STATUS %>' value='<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue"/>' />

<%-- housekeeping --%>
<ffi:removeProperty name="currSort"/>
<ffi:removeProperty name="<%= com.ffusion.efs.tasks.SessionNames.WIRESACCOUNTS %>" />
<ffi:removeProperty name="GetWiresAccounts"/>
<ffi:removeProperty name="<%= com.ffusion.efs.tasks.SessionNames.WIRE_TRANSFER_PAYEES %>" />
<ffi:removeProperty name="GetWireTransferPayees" />

<script>
	$("#wireAccStatus").extmultiselect({header: ""}).multiselectfilter();
</script>