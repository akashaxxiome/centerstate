<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="transferby-status_report" className="moduleHelpClass" />
<%-- If there is no transfer accounts in session, get transfer accounts --%>
<% if( session.getAttribute( com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS ) == null ) { %>
    <ffi:object id="GetTransferAccounts" name="com.ffusion.tasks.banking.GetTransferAccounts" scope="session"/>
        <ffi:setProperty name="GetTransferAccounts" property="AccountsName" value="BankingAccounts"/>
    <ffi:process name="GetTransferAccounts"/>
<% } %>

<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.EXTRA_PREFIX %><%= com.sap.banking.common.constants.ServiceConstants.SERVICE %>" value="<%= com.ffusion.tasks.banking.Task.BANKING %>"/>

<script type="text/javascript"><!--

ns.report.validateForm = function( formName )
{
    var form = document.forms[formName];
    var s = "";
    var p = form.statusType;
    var t = p.options;
    if (!p.disabled)
    {
        for(var i = 0; i < p.length; i++){
            if (t[i].selected)
                s+=t[i].value+",";
        }
        // if no status is selected, select all statuses
        if (s == "") {
            for(var i = 0; i < p.length; i++) s+=t[i].value+",";
        }
    }
    form.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.wiretransfers.WireReportConsts.SEARCH_CRITERIA_STATUS %>.value= s;

    return true;
}

function statuses(s) {
    frm = document.criteriaFormName;
    if (s == "all") {
        frm.statusType.disabled = true;
        //document.getElementsByName("statusType").item(0).style.backgroundColor="#CCC";
    } else {
        frm.statusType.disabled = false;
        //document.getElementsByName("statusType").item(0).style.backgroundColor="#FFF";
    }
}
// --></script>
<%--
    Get the task used to look through the values for a given search criterion
    in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<ffi:cinclude value1="${SameDayExtTransferEnabled}" value2="">
	<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
	<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayExtTransferEnabled"/>
	<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.transfer.external.sameday.support"/> 
	<ffi:process name="GetBPWProperty"/>
</ffi:cinclude>

<ffi:cinclude value1="${SameDayExtTransferEnabled}" value2="true">
	 <ffi:cinclude value1="${SameDayExtTransferForBusiness}" value2="">
		<ffi:object id="CheckSameDayEnabledForBusiness" name="com.ffusion.tasks.util.CheckSameDayEnabledForBusiness" scope="session" />
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="BusinessID" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="ModuleName" value="<%=com.ffusion.ffs.bpw.interfaces.ACHConsts.ACH_BATCH_EXT_TRANSFER%>"/>
			<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="SessionName" value="SameDayExtTransferForBusiness"/>
		<ffi:process name="CheckSameDayEnabledForBusiness"/>
	 </ffi:cinclude>
 </ffi:cinclude>

<%-- <!--L10NStart-->From Account<!--L10NEnd--> --%>
<tr>
    <td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_217"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_FROM_ACCOUNT %>" />
        <select id="transferHistAccDropdown"  style="width:350px;"  name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_FROM_ACCOUNT %>" size="5" MULTIPLE>
            <ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
            <ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
            <ffi:process name="CriterionListChecker" />

            <option value=""
                <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="" />
                <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
            >
                    <s:text name="jsp.reports_488"/>
            </option>

        <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
        <ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" property="Filter" value="All"/>
        <ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" items="acct">
            <ffi:setProperty name="tmp_val" value="${acct.ID}"/>
            <option value="<ffi:getProperty name='tmp_val'/>"
                <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
                <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
            >
                <ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals" >
                    <ffi:getProperty name="acct" property="RoutingNum"/> :
                </ffi:cinclude>
                
                <ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${acct.ID}" />
                <ffi:process name="GetAccountDisplayText" />
                <ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />
                
                <ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
                     - <ffi:getProperty name="acct" property="NickName"/>
                </ffi:cinclude>
                 - <ffi:getProperty name="acct" property="CurrencyCode"/>
            </option>
        </ffi:list>
        <ffi:removeProperty name="tmp_val"/>
        <ffi:removeProperty name="acct" />
        <ffi:removeProperty name="GetAccountDisplayText"/>
    </td>
</tr>

<%-- To Account --%>
<tr>
    <td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_424"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_TO_ACCOUNT %>" />
        <select id="transferHistToAccDropdown"  style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_TO_ACCOUNT %>" size="5" MULTIPLE>
            <ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
            <ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
            <ffi:process name="CriterionListChecker" />

            <option value=""
                <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="" />
                <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
            >
                    <s:text name="jsp.reports_488"/>
            </option>

        <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
        <ffi:setProperty name="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" property="Filter" value="All"/>
        <ffi:list collection="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" items="acct">
            <ffi:setProperty name="tmp_val" value="${acct.ID}"/>
            <option value="<ffi:getProperty name='tmp_val'/>"
                <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
                <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
            >
                <ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals" >
                    <ffi:getProperty name="acct" property="RoutingNum"/> :
                </ffi:cinclude>
                
                <ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${acct.ID}" />
                <ffi:process name="GetAccountDisplayText" />
                <ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />
                
                <ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
                     - <ffi:getProperty name="acct" property="NickName"/>
                </ffi:cinclude>
                 - <ffi:getProperty name="acct" property="CurrencyCode"/>
            </option>
        </ffi:list>
        <ffi:removeProperty name="tmp_val"/>
        <ffi:removeProperty name="acct" />
        <ffi:removeProperty name="GetAccountDisplayText"/>
    </td>
</tr>
<ffi:cinclude value1="${SameDayExtTransferForBusiness}" value2="true">
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.report.SameDayTransfer"/>&nbsp;</td>
	<td colspan="3"> <input type="checkbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SAME_DAY_TRANSFER %>" ></td>
</tr>
</ffi:cinclude>
<%-- dates --%>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- Amounts, sort 1 --%>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" />
        <input style="width:71px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_AMOUNT_FROM %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
        <span class="columndata">&nbsp;&nbsp;<s:text name="jsp.default_423.1" />&nbsp;</span>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" />
        <input style="width:71px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_AMOUNT_TO %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
    </td>
</tr>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
    <td>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
        <ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
        <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
            <option value="" <ffi:cinclude value1="" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_FROM_ACCOUNT %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_FROM_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_217"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TO_ACCOUNT %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TO_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_424"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_AMOUNT %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_43"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_DATE %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_DATE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_137"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TRACE_NO %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TRACE_NO %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.reports_683"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_STATUS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_STATUS %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_388"/>
            </option>
        </select>

		<span class="reportsCriteriaContent"> <s:text name="jsp.reports_668"/>&nbsp;</span>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
        <ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
        <select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
            <option value="" <ffi:cinclude value1="" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_FROM_ACCOUNT %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_FROM_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_217"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TO_ACCOUNT %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TO_ACCOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_424"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_AMOUNT %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_43"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_DATE %>" 
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_DATE %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_137"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TRACE_NO %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_TRACE_NO %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.reports_683"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_STATUS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.banking.BankingReportConsts.SORT_CRITERIA_STATUS %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_388"/>
            </option>
        </select>
    </td>
</tr>

<%-- statusType --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_STATUS %>" />
<%
    com.ffusion.beans.reporting.Report rpt = (com.ffusion.beans.reporting.Report)session.getAttribute( "ReportData" );
    String stati = rpt.getReportCriteria().getCurrentSearchCriterionValue();
    java.util.ArrayList statusList = new java.util.ArrayList();
    if( stati!=null && stati.length() > 0 ) {
        java.util.StringTokenizer tokenizer = new java.util.StringTokenizer( stati, "," );
        while( tokenizer.hasMoreTokens() ) {
            String s = tokenizer.nextToken();
            if( s!=null && s.length() > 0 ) {
                statusList.add( s );
            }
        }
    }
%>

<%-- status (line 1), sort 2 --%>
<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_388"/>&nbsp;</td>
    <td class="sectionsubhead" colspan="3">
        <input type="Radio" name="reportAllStatuses" value="all" onclick="statuses(this.value)" <%= statusList.size() == 0 ? "checked" : "" %>> <s:text name="jsp.reports_500"/>
    	&nbsp;&nbsp;
    	 <input type="Radio" name="reportAllStatuses" value="select" onclick="statuses(this.value)" <%= statusList.size() > 0 ? "checked" : "" %>> <s:text name="jsp.reports_661"/>
    </td>
</tr>
<%-- status (line 2) --%>
<%-- <tr>
    <td><br></td>
    <td class="sectionsubhead">
        <input type="Radio" name="reportAllStatuses" value="select" onclick="statuses(this.value)" <%= statusList.size() > 0 ? "checked" : "" %>> <s:text name="jsp.reports_661"/>
    </td>
    <td colspan="2"><br></td>
</tr> --%>
<%-- status (multiselect) --%>
<tr>
    <td><br></td>
    <td>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_STATUS %>" />
        <select class="txtbox" size="8" multiple name="statusType" <%= statusList.size() == 0 ? "disabled" : "" %>>
                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_COMPLETED %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_COMPLETED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_538"/>
            </option>

                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_CANCELLED %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_CANCELLED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_528"/>
            </option>

                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_NOFUNDSON %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_NOFUNDSON)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_621"/>
            </option>

                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_FAILEDON %>" 
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_FAILEDON)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_580"/>
            </option>

                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_SCHEDULED %>" 
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_SCHEDULED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_660"/>
            </option>

                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_BATCH_INPROCESS %>" 
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_BATCH_INPROCESS)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_520"/>
            </option>

                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_LIMIT_CHECK_FAILED %>" 
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_LIMIT_CHECK_FAILED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_606"/>
            </option>

                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_LIMIT_REVERT_FAILED %>" 
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_LIMIT_REVERT_FAILED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_607"/>
            </option>

                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_APPROVAL_FAILED %>" 
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_APPROVAL_FAILED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_507"/>
            </option>
                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_APPROVAL_PENDING %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_APPROVAL_PENDING)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_508"/>
            </option>

                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_APPROVAL_REJECTED %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_APPROVAL_REJECTED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_509"/>
            </option>
                    <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_IMMED_INPROCESS %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_IMMED_INPROCESS)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_596"/>
            </option>


			<!-- added by ypjin -->

            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_INPROCESS %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_INPROCESS)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.default_237"/>
            </option>

            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_BACKEND_FAILED %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_BACKEND_FAILED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_581"/>
            </option>
            <option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_SKIPPED %>"
            <% if (statusList.contains(com.ffusion.beans.banking.BankingReportConsts.RPT_STATUS_SKIPPED)) { %>
                selected
            <% } %>
            >
                <s:text name="jsp.reports_697"/>
            </option>

        </select>
    </td>

    <td>
    </td>
    <td>
    </td>
</tr>

<%-- hidden field that holds the status type string used in backend --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_STATUS %>" />
<input type="hidden" name='<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_STATUS %>' value='<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue"/>' />

<%-- final housekeeping --%>
<ffi:removeProperty name="<%= com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS %>" />
<ffi:removeProperty name="GetTransferAccounts"/>
<ffi:removeProperty name="currSort"/>
<ffi:removeProperty name="CriterionListChecker"/>

<script>
	$("#transferHistAccDropdown").extmultiselect({header: ""}).multiselectfilter();
	$("#transferHistToAccDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>
