<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />
<ffi:help id="cashcon-activity_report" className="moduleHelpClass" />
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
        <ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:process name="GetEntitlementGroup"/>
<ffi:removeProperty name="GetEntitlementGroup"/>
<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayCashConEnabled"/>
<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.cashcon.sameday.support"/>
<ffi:process name="GetBPWProperty"/>


<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
	<ffi:object id="CheckSameDayEnabledForBusiness" name="com.ffusion.tasks.util.CheckSameDayEnabledForBusiness" scope="session" />
		<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="BusinessID" value="${User.BUSINESS_ID}"/>
		<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="ModuleName" value="<%=com.ffusion.ffs.bpw.interfaces.ACHConsts.ACH_BATCH_CASH_CON%>"/>
		<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="FiId" value="${SecureUser.BPWFIID}" />
		<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
		<ffi:setProperty name="CheckSameDayEnabledForBusiness" property="SessionName" value="SameDayCashConEnabledForBusiness"/>
	<ffi:process name="CheckSameDayEnabledForBusiness"/>
</ffi:cinclude>

<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="notEquals">	
<tr>
    <td class="sectionsubhead" align="right"><s:text name="jsp.default_174"/></td>
    <td class="columndata" colspan="3">

        <ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
        <ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${SecureUser.EntitlementID}"/>
        <ffi:process name="GetEntitlementGroup"/>
	<ffi:removeProperty name="GetEntitlementGroup"/>
        <ffi:setProperty name="DivisionName" value="${Entitlement_EntitlementGroup.GroupName}"/>
		<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Division" operator="equals">
			<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.GroupId}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Division" operator="notEquals">			
			<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.ParentId}"/>			
		</ffi:cinclude>

        <ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
        <ffi:setProperty name="GetGroupsAdministeredBy" property="GroupId" value="${SecureUser.EntitlementID}"/>
        <ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
        <ffi:process name="GetGroupsAdministeredBy"/>
	<ffi:removeProperty name="GetGroupsAdministeredBy"/>

        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>"/>

        <ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
            <ffi:getProperty name="DivisionName"/>
            <ffi:setProperty name="ReportData.ReportCriteria.CurrentSearchCriterionValue" value="${DivisionId}"/>
        </ffi:cinclude>

        <ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
            <select style="width:240px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>" onchange="ns.report.refresh();">
                <option value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
                    <s:text name="jsp.reports_494"/>
                </option>

                <ffi:list collection="Entitlement_EntitlementGroups" items="ENT_GROUPS_1">
                    <ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Division" operator="equals">
                        <option value="<ffi:getProperty name='ENT_GROUPS_1' property='GroupId'/>"
                        <ffi:cinclude value1="${ENT_GROUPS_1.GroupId}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                            <ffi:getProperty name="ENT_GROUPS_1" property="GroupName"/>
<%
	String entGroupType;
	java.util.Locale locale = (java.util.Locale)session.getAttribute(com.ffusion.tasks.Task.LOCALE);
%>
<ffi:getProperty name="ENT_GROUPS_1" property="EntGroupType" assignTo="entGroupType"/>
					( <%= com.ffusion.beans.reporting.ReportConsts.getEntGroupType( entGroupType, locale)%> )
                        </option>
                    </ffi:cinclude>
                </ffi:list>
            </select>
        </ffi:cinclude>
    </td>
</tr>
</ffi:cinclude>

<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="equals">
	<ffi:setProperty name="DivisionName" value="${Entitlement_EntitlementGroup.GroupName}"/>		
	<ffi:setProperty name="DivisionId" value="${Entitlement_EntitlementGroup.ParentId}"/>
	
	<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
        <ffi:setProperty name="GetGroupsAdministeredBy" property="GroupId" value="${SecureUser.EntitlementID}"/>
        <ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
    <ffi:process name="GetGroupsAdministeredBy"/>
	<ffi:removeProperty name="GetGroupsAdministeredBy"/>
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>">	
<tr>
    <td class="sectionsubhead" align="right"><s:text name="jsp.default_174"/></td>
    <td class="columndata">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>"/>
        <ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
<%
	int parentId = ((com.ffusion.csil.beans.entitlements.EntitlementGroup)session.getAttribute("Entitlement_EntitlementGroup")).getParentId();
	com.ffusion.csil.beans.entitlements.EntitlementGroup division = com.sap.banking.web.util.entitlements.EntitlementsUtil.getEntitlementGroup(parentId);
	String divisionName = division.getGroupName();
%>
			<ffi:setProperty name="DivisionName" value="<%= divisionName %>"/>	        		
            <ffi:getProperty name="DivisionName"/>
            <ffi:setProperty name="ReportData.ReportCriteria.CurrentSearchCriterionValue" value="${DivisionId}"/>
        </ffi:cinclude>

        <ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
            <select style="width:240px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>" onchange="ns.report.refresh();">
                <option value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
                    <s:text name="jsp.reports_494"/>
                </option>

                <ffi:list collection="Entitlement_EntitlementGroups" items="ENT_GROUPS_1">
                    <ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Division" operator="equals">
                        <option value="<ffi:getProperty name='ENT_GROUPS_1' property='GroupId'/>"
                        <ffi:cinclude value1="${ENT_GROUPS_1.GroupId}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                            <ffi:getProperty name="ENT_GROUPS_1" property="GroupName"/>
<%
	String entGroupType;
	java.util.Locale locale = (java.util.Locale)session.getAttribute(com.ffusion.tasks.Task.LOCALE);
%>
<ffi:getProperty name="ENT_GROUPS_1" property="EntGroupType" assignTo="entGroupType"/>
					( <%= com.ffusion.beans.reporting.ReportConsts.getEntGroupType( entGroupType, locale)%> )
                        </option>
                    </ffi:cinclude>
                </ffi:list>
            </select>
        </ffi:cinclude>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
	</ffi:cinclude>
</ffi:cinclude>

<tr>
    <td class="sectionsubhead" align="right"><s:text name="jsp.default_266"/></td>
    <td class="columndata" colspan="3">

        <ffi:object id="GetEntitledLocations" name="com.ffusion.efs.tasks.entitlements.GetEntitledLocations" scope="session"/>
        <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="equals">
        	<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>">
        	<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${DivisionId}"/>
        	</ffi:cinclude>
        	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>">	
        		<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
            		<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${DivisionId}"/>
        		</ffi:cinclude>
        		<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
            		<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
        		</ffi:cinclude>
        	</ffi:cinclude>
        </ffi:cinclude>
        <ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="notEquals">
        	<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="equals">
            		<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${DivisionId}"/>
        	</ffi:cinclude>
        	<ffi:cinclude value1="${Entitlement_EntitlementGroups.Size}" value2="0" operator="notEquals">
            		<ffi:setProperty name="GetEntitledLocations" property="DivEntId" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
        	</ffi:cinclude>
        </ffi:cinclude>        

        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_LOCATION_NAME %>" />
        <ffi:setProperty name="GetEntitledLocations" property="LocationName" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
        <ffi:process name="GetEntitledLocations"/>
	<ffi:removeProperty name="GetEntitledLocations"/>

        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_LOCATION %>"/>
        <ffi:cinclude value1="${Locations.Size}" value2="1" operator="notEquals">
            <select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_LOCATION %>" onchange="ns.report.refresh();">
            <option value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_LOCATIONS %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_LOCATIONS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
                <s:text name="jsp.reports_496"/>
            </option>

            <ffi:list collection="Locations" items="ENT_GROUPS_1">
                <option value="<ffi:getProperty name='ENT_GROUPS_1' property='LocationBPWID'/>"
                <ffi:cinclude value1="${ENT_GROUPS_1.LocationBPWID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                    <ffi:getProperty name="ENT_GROUPS_1" property="LocationName"/>
                </option>
            </ffi:list>
            </select>
        </ffi:cinclude>

        <ffi:cinclude value1="${Locations.Size}" value2="1" operator="equals">
            <ffi:list collection="Locations" items="ENT_GROUPS_1">
                <ffi:getProperty name="ENT_GROUPS_1" property="LocationName"/>
            </ffi:list>
            <ffi:setProperty name="ReportData.ReportCriteria.CurrentSearchCriterionValue" value="${LocationName}"/>
        </ffi:cinclude>

    </td>
  </tr>
  
  <tr>  
    <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_LOCATION_NAME %>" />
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_267"/></td>
    <td align="left" colspan="3">
        <table border="0" cellspacing="1" cellpadding="0">
            <tr>
                <td align="left"><input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_LOCATION_NAME %>" size="25" border="0" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />"></td>
                <td><span class="columndata">&nbsp;&nbsp;</span></td>
                <td align="left" nowrap><sj:a
								onClick="ns.report.refresh();"
								button="true"
								><s:text name="jsp.default_373"/></sj:a></td>
            </tr>
        </table>
    </td>
</tr>


<tr>
    <td class="sectionsubhead" align="right" nowrap><s:text name="jsp.default_388"/></td>
    <td>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_STATUS %>" />
        <select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_STATUS %>" size="1">
            <ffi:object id="StatusList" name="com.ffusion.tasks.util.ResourceList" scope="session"/>
            <ffi:setProperty name="StatusList" property="ResourceFilename" value="com.ffusion.beansresources.reporting.cashcon_status" />
            <ffi:setProperty name="StatusList" property="ResourceID" value="CashConStatuses" />
            <ffi:process name="StatusList"/>

            <ffi:object id="StatusResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
            <ffi:setProperty name="StatusResource" property="ResourceFilename" value="com.ffusion.beansresources.reporting.cashcon_status" />
            <ffi:process name="StatusResource" />

            <ffi:getProperty name="StatusList" property="Resource"/>
            <ffi:list collection="StatusList" items="item">
                <ffi:setProperty name="StatusResource" property="ResourceID" value="${item}" />
                <option <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="${item}">selected</ffi:cinclude> value="<ffi:getProperty name="item"/>"><ffi:getProperty name='StatusResource' property='Resource'/></option>
            </ffi:list>
	    <ffi:removeProperty name="StatusList"/>
        </select>
    </td>
    <td></td>
    <td></td>
</tr>



<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>" />
<tr>
    <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>" operator="equals">
        <ffi:object id="GetFilteredBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetFilteredBusinessEmployeesByEntGroups" scope="session"/>
        <ffi:setProperty name="GetFilteredBusinessEmployeesByEntGroups" property="EntitlementGroupsSessionName" value="Entitlement_EntitlementGroups"/>
        <ffi:setProperty name="GetFilteredBusinessEmployeesByEntGroups" property="BusinessEmployeesSessionName" value="AdministeredBusinessEmployees"/>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_USER_NAME %>" />
        <ffi:setProperty name="GetFilteredBusinessEmployeesByEntGroups" property="LastName" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
        <ffi:process name="GetFilteredBusinessEmployeesByEntGroups"/>
    </ffi:cinclude>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DIVISION %>" />
    <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_DIVISIONS %>" operator="notEquals">
        <ffi:object id="GetChildrenGroups" name="com.ffusion.efs.tasks.entitlements.GetChildrenEntitlementGroups" scope="session"/>
		<ffi:setProperty name="GetChildrenGroups" property="GroupId" value="${DivisionId}" />
		<ffi:process name="GetChildrenGroups" />
        <ffi:object id="GetFilteredBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetFilteredBusinessEmployeesByEntGroups" scope="session"/>
        <ffi:setProperty name="GetFilteredBusinessEmployeesByEntGroups" property="EntitlementGroupsSessionName" value="Entitlement_EntitlementGroups"/>
        <ffi:setProperty name="GetFilteredBusinessEmployeesByEntGroups" property="BusinessEmployeesSessionName" value="AdministeredBusinessEmployees"/>
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_USER_NAME %>" />
        <ffi:setProperty name="GetFilteredBusinessEmployeesByEntGroups" property="LastName" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
        <ffi:process name="GetFilteredBusinessEmployeesByEntGroups"/>
    </ffi:cinclude>
    <td class="sectionsubhead" align="right"><s:text name="jsp.reports_688"/></td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_USER %>"/>
        <select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_USER %>">
            <option value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>"
            <ffi:cinclude value1="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                <s:text name="jsp.reports_504"/>
            </option>
            <ffi:list collection="AdministeredBusinessEmployees" items="BusinessEmployeeItem">
                <option value="<ffi:getProperty name='BusinessEmployeeItem' property='Id'/>"
                <ffi:cinclude value1="${BusinessEmployeeItem.Id}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                    <ffi:getProperty name="BusinessEmployeeItem" property="SortableFullNameWithLoginId"/>
                </option>
            </ffi:list>
        </select>
    </td>
 </tr>
 <tr>   
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_455"/></td>
    <td align="left" colspan="3">
    <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_USER_NAME %>" />
        <table border="0" cellspacing="1" cellpadding="0">
            <tr>
                <td align="left"><input class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_FILTER_USER_NAME %>" size="25" border="0" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />"></td>
                <td><span class="columndata">&nbsp;&nbsp;</span></td>
                <td align="left" nowrap><sj:a
								onClick="ns.report.refresh();"
								button="true"
								><s:text name="jsp.default_373"/></sj:a></td>
            </tr>
        </table>
    </td>
</tr>

<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<tr>
    <td align="right" class="sectionsubhead"><s:text name="jsp.default_439"/>&nbsp;</td>
    <td align="left">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_TRANS_TYPE %>" />
        <ffi:setProperty name="currTransType" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />

        <%-- set default transaction type --%>
        <ffi:cinclude value1="" value2="${currTransType}" operator="equals">
            <ffi:setProperty name="currTransType" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS %>" />
        </ffi:cinclude>

		<ffi:object id="StatusList" name="com.ffusion.tasks.util.ResourceList" scope="session"/>
		<ffi:setProperty name="StatusList" property="ResourceFilename" value="com.ffusion.beansresources.reporting.cashcon_status" />
		<ffi:setProperty name="StatusList" property="ResourceID" value="CashConTransactions" />
		<ffi:process name="StatusList"/>
        <ffi:getProperty name="StatusList" property="Resource"/>

        <select id="cashConTransType" style="width:350px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_TRANS_TYPE %>" size="<ffi:getProperty name="StatusList" property="Size" />" MULTIPLE>

        <ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currTransType" />
        <ffi:process name="CriterionListChecker" />

            <ffi:object id="StatusResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
            <ffi:setProperty name="StatusResource" property="ResourceFilename" value="com.ffusion.beansresources.reporting.cashcon_status" />
            <ffi:process name="StatusResource" />

            <ffi:list collection="StatusList" items="item">
           		<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${item}" />
                <ffi:setProperty name="StatusResource" property="ResourceID" value="${item}" />
                <option <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude> value="<ffi:getProperty name="item"/>"><ffi:getProperty name='StatusResource' property='Resource'/></option>
            </ffi:list>
        </select>
    </td>
</tr>
<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
	<ffi:cinclude value1="${SameDayCashConEnabledForBusiness}" value2="true" operator="equals">
	<tr>
		<td align="right" class="sectionsubhead" nowrap><s:text name="jsp.report.SameDayTransaction"/></td>
		<td class="sectionsubhead"  colspan="3"><input type="checkbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SAME_DAY_CASHCON %>" ></td>
	</tr>	
	</ffi:cinclude>
		<ffi:cinclude value1="${SameDayCashConEnabledForBusiness}" value2="true" operator="notEquals">
			<tr><td>&nbsp;</td><td></td></tr>		
		</ffi:cinclude>
</ffi:cinclude>	
<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="notEquals">
	<tr><td>&nbsp;</td><td></td></tr>
</ffi:cinclude>
<tr>
    <td align="right" class="sectionsubhead" nowrap><s:text name="jsp.reports_545"/> </td>
    <td class="sectionsubhead" colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CREDIT_FROM_AMOUNT %>" />
        <input class="ui-widget-content ui-corner-all" type="text" size="12" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CREDIT_FROM_AMOUNT %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" >
        <s:text name="jsp.default_423.1"/>&nbsp;
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CREDIT_TO_AMOUNT %>" />
        <input class="ui-widget-content ui-corner-all" type="text" size="12" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_CREDIT_TO_AMOUNT %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" >
    </td>
</tr>

<tr>
    <td align="right" class="sectionsubhead" nowrap><s:text name="jsp.reports_556"/> </td>
    <td class="sectionsubhead" colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DEBIT_FROM_AMOUNT %>" />
        <input class="ui-widget-content ui-corner-all" type="text" size="12" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DEBIT_FROM_AMOUNT %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" >
        <s:text name="jsp.default_423.1"/>&nbsp;
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DEBIT_TO_AMOUNT %>" />
        <input class="ui-widget-content ui-corner-all" type="text" size="12" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DEBIT_TO_AMOUNT %>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" >
    </td>

</tr>

<tr>
    <td class="sectionsubhead" align="right" ><s:text name="jsp.default_384"/></td>
    <td colspan="3">
        <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
        <select style="width:200px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
            <option value="<%=com.ffusion.beans.cashcon.CashConReportConsts.SORT_CRITERIA_PROCESS_DATE%>"
                <ffi:cinclude value1="<%= com.ffusion.beans.cashcon.CashConReportConsts.SORT_CRITERIA_PROCESS_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.default_137"/>
            </option>
            <option value="<%=com.ffusion.beans.cashcon.CashConReportConsts.SORT_CRITERIA_ABS_TRAN_AMOUNT%>"
                <ffi:cinclude value1="<%= com.ffusion.beans.cashcon.CashConReportConsts.SORT_CRITERIA_ABS_TRAN_AMOUNT %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
                <s:text name="jsp.reports_476"/>
            </option>
        </select>
    </td>
  </tr>
  
  <tr>  
    <td class="sectionsubhead" align="right" ><s:text name="jsp.default_436"/></td>
    <td colspan="3">
    <input name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DEBIT_CREDIT %>" type="radio" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_DEBIT %>">
    <span class="columndata"><s:text name="jsp.reports_557"/></span> &nbsp; &nbsp;
    <input name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_DEBIT_CREDIT %>" type="radio" value="<%= com.ffusion.beans.cashcon.CashConReportConsts.SEARCH_CRITERIA_VALUE_CREDIT %>">
    <span class="columndata"><s:text name="jsp.reports_546"/></span>
    </td>
</tr>
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.bcreport.BCReportConsts.SEARCH_CRITERIA_BUSINESS %>" value="<ffi:getProperty name="Business" property="Id" />" >

<ffi:removeProperty name="DivisionName" />
<ffi:removeProperty name="StatusList" />
<ffi:removeProperty name="Entitlement_EntitlementGroup" />
<ffi:removeProperty name="Entitlement_EntitlementGroups" />
<ffi:removeProperty name="Locations" />
<ffi:removeProperty name="StatusResource" />


<ffi:removeProperty name="AdministeredBusinessEmployees"/>


<script>
	$("#cashConTransType").extmultiselect({header: ""}).multiselectfilter();
</script>
