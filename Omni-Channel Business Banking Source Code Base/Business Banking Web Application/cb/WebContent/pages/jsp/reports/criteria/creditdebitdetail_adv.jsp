<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="CreditReport" operator="equals">
	<ffi:help id="credit_report" className="moduleHelpClass" />
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="DebitReport" operator="equals">
	<ffi:help id="debit_report" className="moduleHelpClass" />
</ffi:cinclude>
<s:include value="%{#session.PagesPath}common/checkAmount_js.jsp"/>
<script type="text/javascript"><!--
ns.report.validateForm = function( formName )
{
	return true;
}
// --></script>

<%-- Get the transaction types map. --%>
<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />
<ffi:setProperty name="TransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CORPORATE %>" />
<ffi:process name="TransactionTypes" />

<%-- Get the transaction groups list (for customer-defined transaction groups) --%>
<ffi:object name="com.ffusion.tasks.business.GetTransactionGroups" id="CustomTransactionGroups" scope="request" />
<ffi:process name="CustomTransactionGroups" />

<%-- Get the list of BAI type code info --%>
<ffi:object name="com.ffusion.tasks.util.GetBAICodes" id="BAICodes" scope="request" />
<ffi:setProperty name="BAICodes" property="Levels" value="<%= Integer.toString( com.ffusion.dataconsolidator.constants.DCConstants.BAI_TYPE_CODE_LEVEL_DETAIL ) %>" />
<ffi:process name="BAICodes" />

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />
 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
 <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
 <ffi:process name="CheckPerAccountReportingEntitlements"/>
 
 <%-- data classification --%>
<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>
 <tr>
 	<td align="right" class="sectionsubhead"><s:text name="jsp.default_354"/>&nbsp;</td>
 	
 	<td>
 		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" size="1" onchange="ns.report.refresh();" >
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_634"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to detail previous day --%>
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_603"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to detail intra day --%>
 		</select>
 	</td>
 	
 	<td>
 	</td>
 	
 	<td>
 	</td>
 </tr>
 
<%
 	int nAccountsSelected=0;
 String zbaDisplay = com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH;
 %>
 
<%-- account --%>
 <ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
 <%-- This is a detail report so we will filter accounts on the detail view entitlement --%>
 <ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
 <%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
 <%-- entitled to view under that data classification --%>
 <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION%>" />
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL%>"/>
 </ffi:cinclude>
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%=com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL%>"/>
 </ffi:cinclude>
 
 <ffi:process name="AccountEntitlementFilterTask"/>
 
 <input type='hidden' name='hidden_refreshZBA' value="">
 
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_15"/>&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS%>" />
		<select id="transacAccDropdown" style="width:350px"  name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS%>" 
		size="5" MULTIPLE onchange="this.form['hidden_refreshZBA'].value='true';">
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals"><% nAccountsSelected=2; %>selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		  <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		  <ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="bankAccount">
			<ffi:setProperty name="tmp_val" value="${bankAccount.ID}:${bankAccount.BankID}:${bankAccount.RoutingNum}:${bankAccount.NickNameNoCommas}:${bankAccount.CurrencyCode}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">
					<% 
					nAccountsSelected++;
					if( nAccountsSelected==1 ) {
					%>
					<ffi:getProperty name="bankAccount" property="ZBAFlag" assignTo="zbaDisplay"/>
					<%
					}
					%>
					selected
				</ffi:cinclude>
			>
				<ffi:cinclude value1="${bankAccount.RoutingNum}" value2="" operator="notEquals" >
					<ffi:getProperty name="bankAccount" property="RoutingNum"/> :
				</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${bankAccount.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="bankAccount" property="NickName"/>
				</ffi:cinclude>
				 - <ffi:getProperty name="bankAccount" property="CurrencyCode"/>
			</option>
		  </ffi:list>
		  <ffi:removeProperty name="tmp_val"/>
		  <ffi:removeProperty name="bankAccount"/>
		  <ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>



<%-- start/end dates --%>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<%-- if the data classification is intra day, we wish the date include page to display the "intra day new" option --%> 
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION%>" />
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="ShowTodayIntradayNew" value="TRUE"/>
</ffi:cinclude>
<s:include value="%{#session.PagesPath}inc/datetime.jsp"/>
<ffi:removeProperty name="ShowTodayIntradayNew"/>

<%-- transactions, amount --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.reports_687"/>&nbsp;</td>
	
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE%>" />
		<ffi:setProperty name="currTransType" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
		<%-- set default transaction type --%>
		<ffi:cinclude value1="" value2="${currTransType}" operator="equals"> 
			<ffi:setProperty name="currTransType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE%>" />
		</ffi:cinclude>
		<select id="transacGrpDropdown" style="width:350px"  name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE%>" size="5" MULTIPLE>

			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currTransType" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_42"/>
			</option>

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_CREDIT_TRANSACTIONS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_CREDIT_TRANSACTIONS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_40"/>
			</option>

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_DEBIT_TRANSACTIONS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_DEBIT_TRANSACTIONS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_41"/>
			</option>

			<ffi:setProperty name="TransactionTypes" property="SortField" value="<%=com.ffusion.tasks.util.GetTransactionTypes.SORTFIELD_DESCRIPTION%>" />
			<ffi:list collection="TransactionTypes.TypeList" items="keyValuePair">
				<ffi:cinclude value1="${keyValuePair.Value}" value2 ="Unknown" operator="notEquals">
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${keyValuePair.Key}" />
				<option
					value="<ffi:getProperty name='keyValuePair' property='Key'/>"
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="keyValuePair" property="Value"/>
				</option>
				</ffi:cinclude>
			</ffi:list>

			<option value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DIVIDER%>"><s:text name="jsp.default_9"/></option>

			<ffi:setProperty name="custTransGroupPrefix" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX%>"/>
			<ffi:list collection="CustomTransactionGroups.TransactionGroups" items="customGroupName">
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${custTransGroupPrefix}${customGroupName}" />
				<option
					value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %><ffi:getProperty name='customGroupName'/>"
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="customGroupName"/>
				</option>
			</ffi:list>
			<ffi:removeProperty name="custTransGroupPrefix"/>
		</select>
	</td>
</tr>

<%-- Transaction codes, empty row (above Sort 1, Sort 2) --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.reports_686"/>&nbsp;</td>
	<td valign="top">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_SUBTYPE%>" />
		<ffi:setProperty name="currTransType" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
		<select id="transacCodeDropdown" style="width:350px" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_SUBTYPE%>" size="5" MULTIPLE>

			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currTransType" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_SUBTYPE %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_SUBTYPE %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.reports_502"/>
			</option>
			<ffi:object name="com.ffusion.tasks.banking.GetBAITypeCodeDescription" id="GetBAITypeCodeDescription" scope="request" />
			<ffi:setProperty name="GetBAITypeCodeDescription" property="BAITypeCodeInfoItemName" value="codeInfoItem" />
			<ffi:setProperty name="GetBAITypeCodeDescription" property="LocaleName" value="<%=com.ffusion.tasks.banking.Task.LOCALE%>" />
			<ffi:list collection="BAICodes.CodeInfoList" items="codeInfoItem">
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${codeInfoItem.Code}" />
				<option
					value="<ffi:getProperty name='codeInfoItem' property='Code'/>"
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="codeInfoItem" property="Code"/>&nbsp;-&nbsp; 
					<ffi:process name="GetBAITypeCodeDescription" />
					<ffi:getProperty name="GetBAITypeCodeDescription" property="Description"/>
				</option>
			</ffi:list>
			<ffi:removeProperty name="GetBAITypeCodeDescription" />
		</select>
	</td>
	<td colspan=2>&nbsp;</td>
</tr>


<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_43"/>&nbsp;</td>	
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN%>" />
		<input style="width:72px" class="ui-widget-content ui-corner-all" type="text" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN%>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
		<span class="columndata">&nbsp;&nbsp;<s:text name="jsp.default_423.1"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX%>" />
		<input style="width:72px" class="ui-widget-content ui-corner-all" type="text" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX%>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="10">
	</td>
</tr>

<%-- Customer Reference Number --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_551"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOMER_REF_NUM%>" />
		<input style="width:190px" class="ui-widget-content ui-corner-all" type="text" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOMER_REF_NUM%>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="20" maxlength="<%=com.ffusion.dataconsolidator.constants.DCConstants.CUSTREFNUM_MAXLENGTH%>">
	</td>
</tr>

<%-- Bank Reference Number --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_517"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_BANK_REF_NUM%>" />
		<input style="width:190px" class="ui-widget-content ui-corner-all" type="text" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_BANK_REF_NUM%>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" />" size="20" maxlength="<%=com.ffusion.dataconsolidator.constants.DCConstants.BANKREFNUM_MAXLENGTH%>">
	</td>
</tr>

<%-- ZBA display( only shows if there is exactly 1 account selected ), if more than 2 selected, remove this ZBA criterion from search criteria --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ZBA_DISPLAY%>" />
<ffi:cinclude value1="${hidden_refreshZBA}" value2="true" operator="equals">
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value=""/>
</ffi:cinclude>
<%
	if( nAccountsSelected==1 ) {
	if( zbaDisplay==null || zbaDisplay.length()<=0 ) {
		zbaDisplay=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH;
	}
	String currZBAFlag=null;
%>
<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" assignTo="currZBAFlag"/>
<%
	if( currZBAFlag==null || currZBAFlag.length()<=0 ) {
		currZBAFlag=zbaDisplay;
	} else if ( zbaDisplay.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE ) ) {
		currZBAFlag=zbaDisplay;
	} else if( zbaDisplay.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY ) ) {
		if( currZBAFlag.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY ) ) {
	currZBAFlag = zbaDisplay;
		}
	} else if( zbaDisplay.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY ) ) {
		if( zbaDisplay.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY ) ) {
	currZBAFlag = zbaDisplay;
		}
	}
%>
	
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_695"/>&nbsp;</td>

	<ffi:cinclude value1="<%=currZBAFlag%>" value2="" operator="equals">
		<%
			currZBAFlag=zbaDisplay;
		%>
	</ffi:cinclude>
	<td>
 		<select style="width:160px" class="txtbox" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ZBA_DISPLAY%>" >
 			<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE%>"
 				<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.default_296"/>
 			</option>
			<ffi:cinclude value1="<%=zbaDisplay%>" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>" operator="equals">
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.default_471"/>
				</option>
			</ffi:cinclude>
			<ffi:cinclude value1="<%=zbaDisplay%>" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>" operator="equals">
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.default_472"/>
				</option>
			</ffi:cinclude>
			<ffi:cinclude value1="<%=zbaDisplay%>" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH%>" operator="equals">
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.default_471"/>
				</option>
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.default_472"/>
				</option>
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH%>" value2="<%= currZBAFlag %>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.reports_524"/>
				</option>
			</ffi:cinclude>
		</select>
	</td>
	<td/>
	<td/>
</tr>
<%
} else {
%>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value=""/>
	<tr></tr>
<%
}
%>
<tr></tr>

<%-- Sort 1, sort 2 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<%-- Set default sort --%>
		<ffi:cinclude value1="" value2="${currSort}" operator="equals" >
			<ffi:setProperty name="currSort" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" />
		</ffi:cinclude>
		<select style="width:200px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_19"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_439"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_TRANS_SUBTYPE%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_TRANS_SUBTYPE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_685"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_AMOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_518"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_552"/>
			</option>
		</select>
	<%-- </td>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_668"/>&nbsp;</td>
	<td> --%>
		<span class="reportsCriteriaContent"> <s:text name="jsp.reports_668"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<%-- Set default sort --%>
		<ffi:cinclude value1="" value2="${currSort}" operator="equals" >
			<ffi:setProperty name="currSort" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" />
		</ffi:cinclude>
		<select style="width:200px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_19"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_439"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_TRANS_SUBTYPE%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_TRANS_SUBTYPE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_685"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_AMOUNT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_AMOUNT %>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_43"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_518"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_552"/>
			</option>
		</select>
	</td>
</tr>

<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_PAYEE_PAYOR %>" value="" size="20">
<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_PAYOR_NUM %>" value="" size="20">
<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ORIG_USER %>" value="" size="20">
<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_PO_NUM %>" value="" size="20">
<input class="txtbox" type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_NUM %>" value="" size="20">
<script>
	$("#transacCodeDropdown").extmultiselect({header: ""}).multiselectfilter();
	$("#transacGrpDropdown").extmultiselect({header: ""}).multiselectfilter();
	$("#transacAccDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>	
<%-- Housekeeping --%>
<ffi:removeProperty name="currSort"/>
<ffi:removeProperty name="hidden_refreshZBA"/>
<ffi:removeProperty name="TransactionTypes" />
<ffi:removeProperty name="CustomTransactionGroups" />
<ffi:removeProperty name="BAICodes" />
<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="CheckPerAccountReportingEntitlements" />
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
