<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="custom-summary_report" className="moduleHelpClass" />
<%-- Get the list of BAI type code info --%>
<ffi:object name="com.ffusion.tasks.util.GetBAICodes" id="BAICodes" scope="request" />
<ffi:setProperty
	name="BAICodes" property="Levels"
	value="<%= com.ffusion.dataconsolidator.constants.DCConstants.BAI_TYPE_CODE_LEVEL_STATUS + \",\" + com.ffusion.dataconsolidator.constants.DCConstants.BAI_TYPE_CODE_LEVEL_SUMMARY %>"
/>
<ffi:process name="BAICodes" />

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />
 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
 <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
 <ffi:process name="CheckPerAccountReportingEntitlements"/>
 
 <%-- data classification --%>
<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>
 <tr>
 	<td align="right" class="sectionsubhead"><s:text name="jsp.default_354"/>&nbsp;</td>
 	
 	<td>
 		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" size="1" onchange="ns.report.refresh();" >
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_634"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to summary previous day --%>
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_603"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to summary intra day --%>
 		</select>
 	</td>
 	
 	<td>
 	</td>
 	
 	<td>
 	</td>
 </tr>
 

<%-- account --%>
 <ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
 <%-- This is a summary report so we will filter accounts on the summary view entitlement --%>
 <ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
 <%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
 <%-- entitled to view under that data classification --%>
 <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>"/>
 </ffi:cinclude>
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>"/>
 </ffi:cinclude>
 
 <ffi:process name="AccountEntitlementFilterTask"/>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_15"/>&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
		<select id="customSummAccDropdown"  style="width:350px;"  name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		  <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		  <ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="bankAccount">
			<ffi:setProperty name="tmp_val" value="${bankAccount.ID}:${bankAccount.BankID}:${bankAccount.RoutingNum}:${bankAccount.NickNameNoCommas}:${bankAccount.CurrencyCode}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<ffi:cinclude value1="${bankAccount.RoutingNum}" value2="" operator="notEquals" >
					<ffi:getProperty name="bankAccount" property="RoutingNum"/> :
				</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${bankAccount.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="bankAccount" property="NickName"/>
				</ffi:cinclude>
				 - <ffi:getProperty name="bankAccount" property="CurrencyCode"/>
			</option>
		  </ffi:list>
		  <ffi:removeProperty name="tmp_val"/>
		  <ffi:removeProperty name="bankAccount"/>
		  <ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>

<%-- start/end dates, and date range selecter --%>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime.jsp" />

<%-- summary code, empty columns --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.reports_675"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SUMMARY_CODE %>" />
		<ffi:setProperty name="currSummaryCode" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
		<select id="customSummCodeDropdown" style="width:350px" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SUMMARY_CODE %>" size="5" MULTIPLE>

			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currSummaryCode" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_SUMMARY_CODE %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_SUMMARY_CODE %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.reports_492"/>
			</option>
			<ffi:object name="com.ffusion.tasks.banking.GetBAITypeCodeDescription" id="GetBAITypeCodeDescription" scope="request" />
			<ffi:setProperty name="GetBAITypeCodeDescription" property="BAITypeCodeInfoItemName" value="codeInfoItem" />
			<ffi:setProperty name="GetBAITypeCodeDescription" property="LocaleName" value="<%= com.ffusion.tasks.banking.Task.LOCALE %>" />
			<ffi:list collection="BAICodes.CodeInfoList" items="codeInfoItem">
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${codeInfoItem.Code}" />
				<option
					value="<ffi:getProperty name='codeInfoItem' property='Code'/>"
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="codeInfoItem" property="Code"/>&nbsp;-&nbsp; 
					<ffi:process name="GetBAITypeCodeDescription" />
					<ffi:getProperty name="GetBAITypeCodeDescription" property="Description"/>
				</option>
			</ffi:list>
			<ffi:removeProperty name="GetBAITypeCodeDescription" />
		</select>
	</td>
</tr>

<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<%-- Set default sort --%>
		<ffi:cinclude value1="" value2="${currSort}" operator="equals" >
			<ffi:setProperty name="currSort" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" />
		</ffi:cinclude>
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
			<option value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_19"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_PROCESS_DATE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
		</select>
	</td>
</tr>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_668"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<%-- Set default sort --%>
		<ffi:cinclude value1="" value2="${currSort}" operator="equals" >
			<ffi:setProperty name="currSort" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" />
		</ffi:cinclude>
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
			<option value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_19"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.SORT_CRITERIA_PROCESS_DATE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>
		</select>
	</td>
</tr>

<script>
	$("#customSummAccDropdown").extmultiselect({header: ""}).multiselectfilter();
	$("#customSummCodeDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>	
<ffi:removeProperty name="BAICodes" />
<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="CheckPerAccountReportingEntitlements" />
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
