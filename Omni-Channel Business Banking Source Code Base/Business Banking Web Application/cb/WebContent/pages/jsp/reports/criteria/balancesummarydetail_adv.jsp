<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- This property is used to display the option of displaying a new header for each transaction.  The variable is cleared
     on the reporting list page, and when the user leaves the reporting sub menu.  If the option should be displayed, 
     this variable should be TRUE. --%>
<ffi:help id="bal-sum-detail_reports" className="moduleHelpClass" />
<ffi:setProperty name="ShowColumnHeaderOption" value="TRUE"/>
<ffi:setProperty name="ReportData" property="ShowPrintHeaderOption" value="TRUE"/>

<script type="text/javascript">
function checkboxChanged( form ) {
	if ( form["tempSupressSubAccounts"] != null) {
	    if ( form["tempSupressSubAccounts"].checked  == true ) {
		    form["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SUPPRESS_SUB_ACCOUNTS%>"].value = true;
	    } else {
		    form["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SUPPRESS_SUB_ACCOUNTS%>"].value = false;
	    }
	}
	if ( form["tempSortByLocation"] != null) {
	    if ( form["tempSortByLocation"].checked  == true ) {
		    form["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SORT_BY_LOCATION%>"].value = true;
	    } else {
		    form["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SORT_BY_LOCATION%>"].value = false;
	    }
	}
}
</script>

<%-- Get the transaction types map. --%>
<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />
<ffi:setProperty name="TransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CORPORATE %>" />
<ffi:process name="TransactionTypes" />

<%-- Get the transaction groups list (for customer-defined transaction groups) --%>
<ffi:object name="com.ffusion.tasks.business.GetTransactionGroups" id="CustomTransactionGroups" scope="request" />
<ffi:process name="CustomTransactionGroups" />

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />
 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
 <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
 <ffi:process name="CheckPerAccountReportingEntitlements"/>
 

 <ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
 <%-- This is a detail report so we will filter accounts on the detail view entitlement --%>
 <ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
 <%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
 <%-- entitled to view under that data classification --%>
 <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>"/>
 </ffi:cinclude>
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>"/>
 </ffi:cinclude>
 
 <ffi:setProperty name="AccountEntitlementFilterTask" property="FilteredAccountsName" value="AllAvailableAccounts"/>

 <ffi:process name="AccountEntitlementFilterTask"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
<ffi:object id="GetMasterSubStatistics" name="com.ffusion.tasks.treasurydirect.GetMasterSubStatistics" scope="request"/>
<ffi:setProperty name="GetMasterSubStatistics" property="AvailableAccountsSessionName" value="AllAvailableAccounts"/>
<ffi:process name="GetMasterSubStatistics"/>
<ffi:removeProperty name="AllAvailableAccounts"/>

 <%-- data classification --%>
<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAndDetailPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>
 <tr>
 	<td align="right" class="sectionsubhead"><s:text name="jsp.default_354"/>&nbsp;</td>
 	
 	<td>
 		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" size="1" onchange="ns.report.refresh();" >
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAndDetailPrev}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_634"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to summary previous day --%>
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAndDetailIntra}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_603"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to summary intra day --%>
 		</select>
 	</td>
 	
 	<td>
 	</td>
 	
 	<td>
 	</td>
 </tr>
 
<%
 	int nAccountsSelected=0;
 String zbaDisplay = com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH;
 %>
 
<%-- account --%>
 <input type='hidden' name='hidden_refreshZBA' value="">
 
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.default_15"/>&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS%>" />
		<select id="balSummaryDetailDropdown"  style="width:370px;" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS%>" size="5" MULTIPLE onchange="this.form['hidden_refreshZBA'].value='true';ns.report.refresh();">
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals"><% nAccountsSelected=2; %>selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		  <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		  <ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="bankAccount">
			<ffi:setProperty name="tmp_val" value="${bankAccount.ID}:${bankAccount.BankID}:${bankAccount.RoutingNum}:${bankAccount.NickNameNoCommas}:${bankAccount.CurrencyCode}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">
					<% 
					nAccountsSelected++;
					if( nAccountsSelected==1 ) {
					%>
					<ffi:getProperty name="bankAccount" property="ZBAFlag" assignTo="zbaDisplay"/>
					<ffi:setProperty name="acctShowPrevDayOL" value="${bankAccount.ShowPreviousDayOpeningLedger}" />
					<ffi:cinclude value1="${acctShowPrevDayOL}" value2="" operator="equals">
						<ffi:setProperty name="acctShowPrevDayOL" value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_SHOW %>" />
					</ffi:cinclude>
					<%
					} else {
					%>
					<ffi:removeProperty name="acctShowPrevDayOL"/>
					<%
					}
					%>
					selected
				</ffi:cinclude>
			>
				<ffi:cinclude value1="${bankAccount.RoutingNum}" value2="" operator="notEquals" >
					<ffi:getProperty name="bankAccount" property="RoutingNum"/> :
				</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${bankAccount.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="bankAccount" property="NickName"/>
				</ffi:cinclude>
				 - <ffi:getProperty name="bankAccount" property="CurrencyCode"/>
			</option>
		  </ffi:list>
		  <ffi:removeProperty name="tmp_val"/>
		  <ffi:removeProperty name="bankAccount"/>
		  <ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>

<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<%-- if the data classification is intra day, we wish the date include page to display the "intra day new" option --%> 
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION%>" />
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="ShowTodayIntradayNew" value="TRUE"/>
</ffi:cinclude>
<s:include value="%{#session.PagesPath}inc/datetime-previous-biz-day.jsp"/>
<ffi:removeProperty name="ShowTodayIntradayNew"/>

<%-- Transaction Groups --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead" rowspan="3"><s:text name="jsp.reports_687"/>&nbsp;</td>
	
	<td rowspan="3" valign="top">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE%>" />
		<ffi:setProperty name="currTransType" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
		<%-- set default transaction type --%>
		<ffi:cinclude value1="" value2="${currTransType}" operator="equals"> 
			<ffi:setProperty name="currTransType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE%>" />
		</ffi:cinclude>
		<select id="balSummaryTransDropdown" style="width:300px" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE%>" size="5" MULTIPLE>

			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currTransType" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_42"/>
			</option>

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_CREDIT_TRANSACTIONS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_CREDIT_TRANSACTIONS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_40"/>
			</option>

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_DEBIT_TRANSACTIONS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_DEBIT_TRANSACTIONS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_41"/>
			</option>

			<ffi:setProperty name="TransactionTypes" property="SortField" value="<%=com.ffusion.tasks.util.GetTransactionTypes.SORTFIELD_DESCRIPTION%>" />
			<ffi:list collection="TransactionTypes.TypeList" items="keyValuePair">
				<ffi:cinclude value1="${keyValuePair.Value}" value2 ="Unknown" operator="notEquals">
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${keyValuePair.Key}" />
				<option
					value="<ffi:getProperty name='keyValuePair' property='Key'/>"
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="keyValuePair" property="Value"/>
				</option>
				</ffi:cinclude>
			</ffi:list>

			<option value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DIVIDER%>"><s:text name="jsp.default_9"/></option>

			<ffi:setProperty name="custTransGroupPrefix" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX%>"/>
			<ffi:list collection="CustomTransactionGroups.TransactionGroups" items="customGroupName">
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${custTransGroupPrefix}${customGroupName}" />
				<option
					value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %><ffi:getProperty name='customGroupName'/>"
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="customGroupName"/>
				</option>
			</ffi:list>
			<ffi:removeProperty name="custTransGroupPrefix"/>
		</select>
	</td>

	<td colspan="2"></td>
</tr>

<tr>
	<%
		String numAccountsString = null;	
		   String numSubAccountsString = null;
	%>
	<ffi:getProperty name="GetMasterSubStatistics" property="NumAccounts" assignTo="numAccountsString"/>
	<ffi:getProperty name="GetMasterSubStatistics" property="NumSubAccounts" assignTo="numSubAccountsString"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SUPPRESS_SUB_ACCOUNTS%>" />
        <%
        	int numAccounts = -1;
        	   int numSubAccounts = -1;
                   if ( numAccountsString != null && !numAccountsString.equals( "" ) ) {
        		numAccounts = Integer.parseInt( numAccountsString );
                   }	
                   if ( numSubAccountsString != null && !numSubAccountsString.equals( "" ) ) {
        		numSubAccounts = Integer.parseInt( numSubAccountsString );
                   }	

        	   if ( numAccounts > 1 && numSubAccounts > 0 ) {
        %>
	<td align="right" >
		<%-- this hidden form field will be set by the javascript function checkboxChanged() --%>
		<input type="hidden" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SUPPRESS_SUB_ACCOUNTS%>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue"/>" />
		<input type="checkbox" name="tempSupressSubAccounts"  
		<ffi:cinclude value1="true" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> checked </ffi:cinclude>
		onClick="checkboxChanged( this.form );"
		/>
	</td>
	<td align="left" class="sectionsubhead" >
		<s:text name="jsp.reports_677"/>
	</td>
	<%
		} else {
	%>
		<input type="hidden" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SUPPRESS_SUB_ACCOUNTS%>" value="false" />
	     <td>&nbsp;</td>
	     <td>&nbsp;</td>
	<%
		}
	%>
</tr>
<tr>
	<td colspan="4">&nbsp;</td>
</tr>
<tr>
<%-- ZBA display, Show Prev Day Opening ledger --%>
<%-- ZBA Display, ( only shows if there is exactly 1 account selected ), if more than 2 selected, remove this ZBA criterion from search criteria --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ZBA_DISPLAY%>" />
<ffi:cinclude value1="${hidden_refreshZBA}" value2="true" operator="equals">
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value=""/>
</ffi:cinclude>
<%
	if( nAccountsSelected==1 ) {
	if( zbaDisplay==null || zbaDisplay.length()<=0 ) {
		zbaDisplay=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH;
	}
	String currZBAFlag=null;
%>
<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" assignTo="currZBAFlag"/>
<%
	if( currZBAFlag==null || currZBAFlag.length()<=0 ) {
		currZBAFlag=zbaDisplay;
	} else if ( zbaDisplay.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE ) ) {
		currZBAFlag=zbaDisplay;
	} else if( zbaDisplay.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY ) ) {
		if( currZBAFlag.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY ) ) {
	currZBAFlag = zbaDisplay;
		}
	} else if( zbaDisplay.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY ) ) {
		if( zbaDisplay.equalsIgnoreCase( com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY ) ) {
	currZBAFlag = zbaDisplay;
		}
	}
%>
	
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_695"/>&nbsp;</td>

	<ffi:cinclude value1="<%=currZBAFlag%>" value2="" operator="equals">
		<%
			currZBAFlag=zbaDisplay;
		%>
	</ffi:cinclude>
	<td>
 		<select style="width:160px" class="txtbox" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ZBA_DISPLAY%>" >
 			<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE%>"
 				<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.default_296"/>
 			</option>
			<ffi:cinclude value1="<%=zbaDisplay%>" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>" operator="equals">
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.default_471"/>
				</option>
			</ffi:cinclude>
			<ffi:cinclude value1="<%=zbaDisplay%>" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>" operator="equals">
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.default_472"/>
				</option>
			</ffi:cinclude>
			<ffi:cinclude value1="<%=zbaDisplay%>" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH%>" operator="equals">
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.default_471"/>
				</option>
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.default_472"/>
				</option>
				<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH%>"
					<ffi:cinclude value1="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH%>" value2="<%=currZBAFlag%>" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.reports_524"/>
				</option>
			</ffi:cinclude>
		</select>
	</td>
<%
	} else {
%>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value=""/>
	<td/>
	<td/>
<%
	}
%>


<%
	String isAnyMasterOrSubString = null;
	String isAnyIncludeZBAInRollupString = null;
%>
<ffi:getProperty name="GetMasterSubStatistics" property="IsAnyAccountMasterOrSub" assignTo="isAnyMasterOrSubString"/>
<ffi:getProperty name="GetMasterSubStatistics" property="IsAnyIncludeZBAInRollup" assignTo="isAnyIncludeZBAInRollupString"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DISPLAY_ZBA_TRANSACTIONS%>" />
<%
	boolean isAnyMasterOrSub = ( new Boolean( isAnyMasterOrSubString ) ).booleanValue();
	boolean isAnyIncludeZBAInRollup = ( new Boolean( isAnyIncludeZBAInRollupString ) ).booleanValue();

	if ( numAccounts > 1 && isAnyMasterOrSub && isAnyIncludeZBAInRollup ) {
%>
<ffi:setProperty name="ReportData" property="ReportCriteria.HiddenSearchCriterion" value="false"/>
<td align="right" class="sectionsubhead"><s:text name="jsp.reports_573"/>&nbsp;</td>
<td>
	<select style="width:160px" class="txtbox" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DISPLAY_ZBA_TRANSACTIONS%>" >
		<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_DISPLAY_ZBA_TRANS_ALL%>"
			<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_DISPLAY_ZBA_TRANS_ALL %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
			<s:text name="jsp.reports_612"/>
		</option>
		<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_DISPLAY_ZBA_TRANS_MASTER_ONLY%>"
			<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_DISPLAY_ZBA_TRANS_MASTER_ONLY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
			<s:text name="jsp.reports_611"/>
		</option>
		<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_DISPLAY_ZBA_TRANS_SUB_ONLY%>"
			<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_DISPLAY_ZBA_TRANS_SUB_ONLY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
			<s:text name="jsp.reports_673"/>
		</option>
		<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_DISPLAY_ZBA_TRANS_NONE%>"
			<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_DISPLAY_ZBA_TRANS_NONE %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
			<s:text name="jsp.default_296"/>
		</option>			
	</selct>
</td>
<%
	} else {
%>
		<input type="hidden" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DISPLAY_ZBA_TRANSACTIONS%>" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_DISPLAY_ZBA_TRANS_ALL%>" />
		<td>&nbsp;</td>
		<td>&nbsp;</td>
<%
	}
%>

</tr>

<%-- Show previous-day opening ledger( only shows if there is exactly 1 account selected ), if more than 2 selected, remove this criterion from search criteria --%>
<%
	if( nAccountsSelected==1 ){
%>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SHOW_PREV_DAY_OPENING_LEDGER%>" />
	<ffi:cinclude value1="${hidden_refreshZBA}" value2="true" operator="equals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value=""/>
	</ffi:cinclude>
	<ffi:setProperty name="currShowOL" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
	<ffi:cinclude value1="${currShowOL}" value2="" operator="equals">
		<ffi:setProperty name="currShowOL" value="${acctShowPrevDayOL}"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${currShowOL}" value2="" operator="equals">
		<ffi:setProperty name="currShowOL" value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_SHOW%>" />
	</ffi:cinclude>
		<tr>
			
			<td align="right" class="sectionsubhead"><s:text name="jsp.reports_635"/>&nbsp;</td>
			<td>
				<select style="width:80px" class="txtbox" name="<%=com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX%><%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SHOW_PREV_DAY_OPENING_LEDGER%>" >
					<ffi:cinclude value1="${acctShowPrevDayOL}" value2="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_HIDE %>" operator="notEquals">
						<option value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_SHOW %>"
							<ffi:cinclude value1="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_SHOW %>" value2="${currShowOL}" operator="equals">selected</ffi:cinclude> >
							<s:text name="jsp.default_381"/>
						</option>
					</ffi:cinclude>
					<option value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_HIDE %>"
						<ffi:cinclude value1="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.PREV_DAY_OPENING_LEDGER_HIDE %>" value2="${currShowOL}" operator="equals">selected</ffi:cinclude> >
						<s:text name="jsp.reports_594"/>
					</option>
				</select>
			</td>
			<td></td>
			<td></td>
		</tr>
<%
} else {
%>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value=""/>
<%
}
%>

<tr>
	<td align="right">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SORT_BY_LOCATION %>" />
		<%-- this hidden form field will be set by the java function checkboxChanged() --%>
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SORT_BY_LOCATION%>" value="<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue"/>"/>
		<input type="checkbox" name="tempSortByLocation"  
		<ffi:cinclude value1="true" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> checked </ffi:cinclude>
		onClick="checkboxChanged( this.form );"
		/>	
	</td>
	<td align="left" class="sectionsubhead"><s:text name="jsp.reports_669"/></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
</tr>

<%-- Sort 1, Sort 2 --%>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_667"/>&nbsp;</td>
	<td colspan="3">
		<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_ACCOUNT_NUMBER%>"/>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="3" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<%-- Set default sort --%>
		<ffi:cinclude value1="" value2="${currSort}" operator="equals" >
			<ffi:setProperty name="currSort" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE%>" />
		</ffi:cinclude>
		<select style="width:200px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>3" size="1">
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>			
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_439"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CREDIT_DEBIT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CREDIT_DEBIT%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_547"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_DEBIT_CREDIT%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_DEBIT_CREDIT%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_158"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_518"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_552"/>
			</option>
		</select>
	<%-- </td>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_668"/>&nbsp;</td>
	<td> --%>
		<span class="reportsCriteriaContent"><s:text name="jsp.reports_668" />&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="4" />
		<ffi:setProperty name="currSort" value="${ReportData.ReportCriteria.CurrentSortCriterionValue}"/>
		<%-- Set default sort --%>
		<ffi:cinclude value1="" value2="${currSort}" operator="equals" >
			<ffi:setProperty name="currSort" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CREDIT_DEBIT%>" />
		</ffi:cinclude>
		<select style="width:200px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>4" size="1">
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_137"/>
			</option>			
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_SUB_TYPE_DESC%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_439"/>
			</option>			
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CREDIT_DEBIT %>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CREDIT_DEBIT%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_547"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_DEBIT_CREDIT%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_DEBIT_CREDIT%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_158"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_BANK_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_518"/>
			</option>
			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>"
				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_CUSTOMER_REF_NUM%>" value2="${currSort}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_552"/>
			</option>
		</select>
	</td>
</tr>

<script>
	$("#balSummaryDetailDropdown").extmultiselect({header: ""}).multiselectfilter();
	$("#balSummaryTransDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>
<%-- Housekeeping --%>
<ffi:removeProperty name="currSort"/>
<ffi:removeProperty name="hidden_refreshZBA"/>
<ffi:removeProperty name="TransactionTypes" />
<ffi:removeProperty name="CustomTransactionGroups" />
<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="CheckPerAccountReportingEntitlements" />
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
<ffi:removeProperty name="acctShowPrevDayOL"/>
<ffi:removeProperty name="currShowOL"/>
