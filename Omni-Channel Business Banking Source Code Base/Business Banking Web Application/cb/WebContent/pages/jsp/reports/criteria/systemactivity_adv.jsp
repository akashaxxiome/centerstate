<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale,
                 java.util.ArrayList,
                 com.ffusion.beans.bcreport.BCReportConsts" %>
                
<script type="text/javascript"><!--
ns.report.validateForm = function( formName )
{
	return true;
}
// --></script>
<ffi:help id="system_activity" className="moduleHelpClass" />
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DATE_FORMAT %>" value="${UserLocale.DateFormat}">
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_TIME_FORMAT %>" value="${UserLocale.TimeFormat}">

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />

<tr>
	<td class="sectionsubhead" align="right"><s:text name="jsp.default_225"/></td>
	<td class="sectionsubhead" colspan="3">
		<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
		<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="SecureUser"/>
		<ffi:process name="GetGroupsAdministeredBy"/>
		<ffi:removeProperty name="GetGroupsAdministeredBy"/>

		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_GROUP %>"/>
		<%--<ffi:getProperty name="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/> --%>
		<select style="width:240px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_GROUP %>" onchange="ns.report.refresh();">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" 
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">Selected</ffi:cinclude> >
				<s:text name="jsp.reports_495"/>
			</option>
									
			<ffi:list collection="Entitlement_EntitlementGroups" items="ENT_GROUPS_1">
				<ffi:setProperty name="group_pre" value=""/>
				<ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Division" operator="equals">
					<ffi:setProperty name="group_pre" value="&nbsp;&nbsp;&nbsp;"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${ENT_GROUPS_1.EntGroupType}" value2="Group" operator="equals">
					<ffi:setProperty name="group_pre" value="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"/>
				</ffi:cinclude>

				<option value="<ffi:getProperty name='ENT_GROUPS_1' property='GroupId'/>" 
				<ffi:cinclude value1="${ENT_GROUPS_1.GroupId}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<ffi:getProperty name="group_pre" encode="false"/>
					<ffi:getProperty name="ENT_GROUPS_1" property="GroupName"/>
<%
	String entGroupType;
	java.util.Locale locale = (java.util.Locale)session.getAttribute(com.ffusion.tasks.Task.LOCALE);
%>
<ffi:getProperty name="ENT_GROUPS_1" property="EntGroupType" assignTo="entGroupType"/>
					( <%= com.ffusion.beans.reporting.ReportConsts.getEntGroupType( entGroupType, locale)%> )
				</option>
			</ffi:list>
		</select>
	</td>
</tr>

<tr>	
	<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" operator="equals">
		<ffi:object id="GetBusinessEmployeesByEntGroups" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroups" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="EntitlementGroupsSessionName" value="Entitlement_EntitlementGroups"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroups" property="BusinessEmployeesSessionName" value="AdministeredBusinessEmployees"/>
		<ffi:process name="GetBusinessEmployeesByEntGroups"/>
		<ffi:removeProperty name="GetBusinessEmployeesByEntGroups"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_GROUPS %>" operator="notEquals">
		<ffi:object id="GetBusinessEmployeesByEntGroupId" name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroupId" scope="session"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroupId" property="EntitlementGroupId" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
		<ffi:setProperty name="GetBusinessEmployeesByEntGroupId" property="BusinessEmployeesSessionName" value="AdministeredBusinessEmployees"/>
		<ffi:process name="GetBusinessEmployeesByEntGroupId"/>
	</ffi:cinclude>
	<td class="sectionsubhead" align="right"><s:text name="jsp.reports_688"/></td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_USER %>"/>
		<select class="txtbox" style="width:200px" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_USER %>">
			
		<% int userNumber = 0; %>
		<ffi:list collection="AdministeredBusinessEmployees" items="CountUserNumber">
			<% userNumber++; %>
		</ffi:list>
		<% String userNumberString = new Integer(userNumber).toString(); %>
		
		<ffi:cinclude value1="<%= userNumberString %>" value2="0" operator="notEquals">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>"
			<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_USERS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
				<s:text name="jsp.reports_504"/>
			</option>
            <ffi:setProperty name="AdministeredBusinessEmployees" property="SortedBy" value="LAST,FIRST"/>
		</ffi:cinclude>
		
			<ffi:list collection="AdministeredBusinessEmployees" items="BusinessEmployeeItem">
				<option value="<ffi:getProperty name='BusinessEmployeeItem' property='Id'/>" 
				<ffi:cinclude value1="${BusinessEmployeeItem.Id}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<ffi:getProperty name="BusinessEmployeeItem" property="SortableFullNameWithLoginId"/>
				</option>
			</ffi:list>
			<ffi:cinclude value1="${BusinessEmployeeItem.Id}" value2="" operator="equals"> 
				<option value="<ffi:getProperty name='BusinessEmployee' property='Id'/>"
					<ffi:cinclude value1="<ffi:getProperty name='BusinessEmployee' property='Id'/>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
					<ffi:getProperty name='BusinessEmployee' property='lastName'/>, <ffi:getProperty name='BusinessEmployee' property='firstName'/>  (<ffi:getProperty name='BusinessEmployee' property='userName'/>)
				</option>
			</ffi:cinclude>
<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
        <ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${SecureUser.EntitlementID}"/>
<ffi:process name="GetEntitlementGroup"/>
<ffi:removeProperty name="GetEntitlementGroup"/>
<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Business" operator="equals">
            <ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="BizEmployeeCriteria" scope="session" />
                <ffi:setProperty name="BizEmployeeCriteria" property="BusinessId" value="${Business.Id}"/>
                <ffi:setProperty name="BizEmployeeCriteria" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>
                <ffi:setProperty name="BizEmployeeCriteria" property="AccountStatus" value="<%=com.ffusion.beans.user.BusinessEmployee.STATUS_DELETED%>"/>

            <ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="SearchBusinessEmployees" scope="request" />
                <ffi:setProperty name="SearchBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="BizEmployeeCriteria"/>
                <ffi:setProperty name="SearchBusinessEmployees" property="BusinessEmployeesSessionName" value="DeletedUserList"/>

            <ffi:process name="SearchBusinessEmployees"/>
            <ffi:cinclude value1="${DeletedUserList.Size}" value2="0" operator="notEquals">
                <ffi:setProperty name="DeletedUserList" property="SortedBy" value="LAST,FIRST"/>
                <option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_AND_ALL_USERS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_AND_ALL_USERS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                    <s:text name="jsp.reports_506"/>
                </option>
                <option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_USERS %>"
                <ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_USERS %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                    <s:text name="jsp.reports_563"/>
                </option>
                <ffi:list collection="DeletedUserList" items="BusinessEmployeeItem">
                	<ffi:setProperty name="DeletedPrefix" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_PREFIX%>" />
                    <ffi:setProperty name="DeletedUserID" value="${DeletedPrefix}${BusinessEmployeeItem.Id}" />
                    <option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_DELETED_PREFIX %><ffi:getProperty name='BusinessEmployeeItem' property='Id'/>"
                    <ffi:cinclude value1="${DeletedUserID}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals"> Selected </ffi:cinclude> >
                        <ffi:getProperty name="BusinessEmployeeItem" property="SortableFullNameWithLoginId"/>(<s:text name="jsp.reports_562"/>)

                    </option>
                    <ffi:removeProperty name="DeletedPrefix" />
                    <ffi:removeProperty name="DeletedUserID" />
                </ffi:list>
            </ffi:cinclude>
            <ffi:removeProperty name="DeletedUserList,SearchBusinessEmployees,BizEmployeeCriteria"/>
</ffi:cinclude>
		</select>
	</td>
</tr>
<ffi:setProperty name="DisplayTime" value="TRUE"/>
<s:include value="%{#session.PagesPath}inc/datetime.jsp"/>
<% String tempValue; %>
<tr>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_TRAN_ID %>"/>
	<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" assignTo="tempValue"/>
	<td valign="middle" align="right" class="sectionsubhead"><s:text name="jsp.reports_684"/></td>
	<td valign="middle" class="sectionsubhead" colspan="3">
		<input style="width:186px;" class="ui-widget-content ui-corner-all" type="text" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_TRAN_ID %>" value="<ffi:getProperty name="tempValue"/>" size="20" maxlength="<%= com.ffusion.beans.admin.AdminRptConsts.TRAN_ID_MAXLENGTH %>">
	</td>
</tr>

<tr>	
	<td class="sectionsubhead" align="right" ><s:text name="jsp.reports_667"/></td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="1" />	
		<select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>1" size="1">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_PROCESS_DATE %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_PROCESS_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_640"/>
			</option>	
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_USER %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_USER %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_688"/>
			</option>	
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_TRAN_ID %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_TRAN_ID %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_435"/>
			</option>	
		</select>

		<span class="reportsCriteriaContent"> <s:text name="jsp.reports_668"/>&nbsp;</span>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSortCriterion" value="2" />	
		<select style="width:160px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SORT_CRIT_PREFIX %>2" size="1">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_PROCESS_DATE %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_PROCESS_DATE %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_640"/>
			</option>	
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_USER %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_USER %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports_688"/>
			</option>	
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_TRAN_ID %>" 
				<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SORT_CRITERIA_TRAN_ID %>" value2="${ReportData.ReportCriteria.CurrentSortCriterionValue}" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.default_435"/>
			</option>	
		</select>
	</td>
</tr>
<tr>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports_614"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_MODULE %>" />
		<select style="width:194px;" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_MODULE %>" onchange="ns.report.refresh();">
			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_MODULES %>"
					<ffi:cinclude value1="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_MODULES %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.reports_497"/>
			</option>
			
			<ffi:object id="GetModules" name="com.ffusion.tasks.bcreport.GetModules"/>
			<ffi:setProperty name="GetModules" property="ModuleListName" value="ModuleList"/>
			<ffi:setProperty name="GetModules" property="ApplicationName" value="<%= com.ffusion.tasks.bcreport.GetModules.BUSINESS_APPLICATION %>"/>
			<ffi:process name="GetModules"/>
			<ffi:removeProperty name="GetModules"/>
			<ffi:list collection="ModuleList" items="module">
                <% boolean isEntitled = true; %>
                <ffi:cinclude value1="${module.EntitlementName}" value2=""  operator="notEquals">
                    <ffi:cinclude ifNotEntitled="${module.EntitlementName}">
                        <% isEntitled = false; %>
                    </ffi:cinclude>
                </ffi:cinclude>
                <% if (isEntitled) { %>
				<option value="<ffi:getProperty name="module" property="Id"/>"
						<ffi:cinclude value1="${module.Id}" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
						<ffi:getProperty name="module" property="Name"/>
				</option>
                <% } %>
			</ffi:list>
			<ffi:removeProperty name="ModuleList"/>
		</select>
	</td>
</tr>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.reports_483"/>&nbsp;</td>
	<td colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>" />
		<select id="Activity"  style="width:350px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentTransactions" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentTransactions" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_490"/>
			</option>
			
			<ffi:object id="GetActivitiesForModule" name="com.ffusion.tasks.bcreport.GetActivitiesForModule"/>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_MODULE %>" />
			<ffi:setProperty name="GetActivitiesForModule" property="ApplicationName" value="<%= com.ffusion.tasks.bcreport.GetModules.BUSINESS_APPLICATION %>"/>
			<ffi:setProperty name="GetActivitiesForModule" property="Module" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
			<ffi:setProperty name="GetActivitiesForModule" property="ActivityListName" value="ActivityList"/>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>" />			
			<ffi:process name="GetActivitiesForModule"/>
			<ffi:removeProperty name="GetActivitiesForModule"/>
			<ffi:list collection="ActivityList" items="activity">
                <option value="<ffi:getProperty name="activity" property="Id"/>"
                    <ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${activity.Id}" />
                    <ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
                >
                        <ffi:getProperty name="activity" property="Name"/>
                </option>
			</ffi:list>
			<ffi:removeProperty name="ActivityList"/>
		</select>
	</td>
</tr>
<tr>
	<%-- Channel --%>
	<td align="right" class="sectionsubhead"><s:text name="jsp.reports.Criteria_Channel"/>&nbsp;</td>
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_CHANNEL %>" />
		<select style="width:160px" class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.banking.BankingReportConsts.SEARCH_CRITERIA_CHANNEL %>" size="1">
			<ffi:setProperty name="currentchannel" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<option value=""
				<ffi:cinclude value1="${currentchannel}" value2="" operator="equals">selected</ffi:cinclude> >
					<s:text name="jsp.reports.Channel_All"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_CHANNEL_WEB %>"
				<ffi:cinclude value1="${currentchannel}" value2="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_CHANNEL_WEB %>" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports.Channel_Web"/>
			</option>
			<option value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_CHANNEL_MOBILE %>"
				<ffi:cinclude value1="${currentchannel}" value2="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_CHANNEL_MOBILE %>" operator="equals">selected</ffi:cinclude> >
				<s:text name="jsp.reports.Channel_Mobile"/>
			</option>
		</select>
	</td>
	<td></td>
	<td></td>

</tr>

<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="AdministeredBusinessEmployees"/>

	
<script>
	$("#Activity").extmultiselect({header: ""}).multiselectfilter();
</script>	