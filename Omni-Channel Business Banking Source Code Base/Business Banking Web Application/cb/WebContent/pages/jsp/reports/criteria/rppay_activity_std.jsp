<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<ffi:help id="rppay-activity_report" className="moduleHelpClass" />
<ffi:cinclude value1="${TransactionModules}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.Resource" id="TransactionModules" scope="session"/>
    <ffi:setProperty name="TransactionModules" property="ResourceFilename" value="com.ffusion.beansresources.banking.resources"/>
    <ffi:process name="TransactionModules"/>
</ffi:cinclude>

<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DATE_FORMAT %>" value="${UserLocale.DateFormat}">
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_TIME_FORMAT %>" value="${UserLocale.TimeFormat}">
<ffi:setProperty name="TransactionModules" property="ResourceID" value="TransactionModuleIdReverse_Positive_Pay"/>
<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.admin.AdminRptConsts.SEARCH_CRITERIA_MODULE %>" value="<ffi:getProperty name='TransactionModules' property='Resource'/>">

<s:include value="%{#session.PagesPath}inc/datetime.jsp"/>
<ffi:removeProperty name="AdministeredBusinessEmployees"/>
