<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="bal-sum-detail_reports" className="moduleHelpClass" />
<%-- This property is used to display the option of displaying a new header for each transaction.  The variable is cleared
     on the reporting list page, and when the user leaves the reporting sub menu.  If the option should be displayed, 
     this variable should be TRUE. --%>
<ffi:setProperty name="ShowColumnHeaderOption" value="TRUE"/>
<ffi:setProperty name="ReportData" property="ShowPrintHeaderOption" value="TRUE"/>

<%-- Get the transaction types map (for bank-defined transaction groups) --%>
<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />
<ffi:setProperty name="TransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CORPORATE %>" />
<ffi:process name="TransactionTypes" />

<%-- Get the transaction groups list (for customer-defined transaction groups) --%>
<ffi:object name="com.ffusion.tasks.business.GetTransactionGroups" id="CustomTransactionGroups" scope="request" />
<ffi:process name="CustomTransactionGroups" />

<%--
	Get the task used to look through the values for a given search criterion
	in a comma-delimited list form.
--%>
<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />
 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
 <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
 <ffi:process name="CheckPerAccountReportingEntitlements"/>
 
 <%-- data classification --%>
<%-- If user not entitled to the default data classification (Previous day), set it to Intra day. (if not entitled to both, we wouldn't even be here) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" value2="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" operator="equals">
	<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAndDetailPrev}" value2="true" operator="notEquals">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>
 <tr>
 	<td align="right" class="sectionsubhead"><s:text name="jsp.default_354"/>&nbsp;</td>
 	
 	<td>
 		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 		<select class="txtbox" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" size="1" onchange="ns.report.refresh();" >
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAndDetailPrev}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_634"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to detail previous day --%>
 <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAndDetailIntra}" value2="true" operator="equals">
 			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>"
 				<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">selected</ffi:cinclude> >
 				<s:text name="jsp.reports_603"/>
 			</option>
 </ffi:cinclude> <%-- End if entitled to detail intra day --%>
 		</select>
 	</td>
 	
 	<td>
 	</td>
 	
 	<td>
 	</td>
 </tr>
 
<%-- account --%>
 <ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
 <%-- This is a summary and detail report so we will filter accounts on the summary and detail view entitlement --%>
 <ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
 <%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
 <%-- entitled to view under that data classification --%>
 <ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_PREVIOUSDAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>"/>
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>"/>
 </ffi:cinclude>
 <ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>"/>
 	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>"/>
 </ffi:cinclude>
 
 <ffi:process name="AccountEntitlementFilterTask"/>
 
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.default_15"/>&nbsp;</td>
	
	<td align="left" colspan="3">
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
		<select class=""  style="width:370px;" id="balSummaryDetailDropdown" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" size="5" MULTIPLE>
			<ffi:setProperty name="currentAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currentAccounts" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_ACCOUNTS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
					<s:text name="jsp.reports_488"/>
			</option>

		  <ffi:object name="com.ffusion.tasks.util.GetAccountDisplayText" id="GetAccountDisplayText" scope="request"/>
		  <ffi:list collection="AccountEntitlementFilterTask.FilteredAccounts" items="bankAccount">
			<ffi:setProperty name="tmp_val" value="${bankAccount.ID}:${bankAccount.BankID}:${bankAccount.RoutingNum}:${bankAccount.NickNameNoCommas}:${bankAccount.CurrencyCode}"/>
			<option value="<ffi:getProperty name='tmp_val'/>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${tmp_val}" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<ffi:cinclude value1="${bankAccount.RoutingNum}" value2="" operator="notEquals" >
					<ffi:getProperty name="bankAccount" property="RoutingNum"/> :
				</ffi:cinclude>

				<ffi:setProperty name="GetAccountDisplayText" property="AccountID" value="${bankAccount.ID}" />
				<ffi:process name="GetAccountDisplayText" />
				<ffi:getProperty name="GetAccountDisplayText" property="AccountDisplayText" />

				<ffi:cinclude value1="${bankAccount.NickName}" value2="" operator="notEquals" >
					 - <ffi:getProperty name="bankAccount" property="NickName"/>
				</ffi:cinclude>
				 - <ffi:getProperty name="bankAccount" property="CurrencyCode"/>
			</option>
		  </ffi:list>
		  <ffi:removeProperty name="tmp_val"/>
		  <ffi:removeProperty name="bankAccount"/>
		  <ffi:removeProperty name="GetAccountDisplayText"/>
		</select>
	</td>
</tr>

<%-- start/end dates --%>
<ffi:setProperty name="ShowPreviousBusinessDay" value="TRUE"/>
<%-- if the data classification is intra day, we wish the date include page to display the "intra day new" option --%> 
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>" />
<ffi:cinclude value1="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_INTRADAY %>" value2="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" operator="equals">
	<ffi:setProperty name="ShowTodayIntradayNew" value="TRUE"/>
</ffi:cinclude>
<s:include value="%{#session.PagesPath}inc/datetime-previous-biz-day.jsp"/>
<ffi:removeProperty name="ShowTodayIntradayNew"/>

<%-- transactions --%>
<tr>
	<td align="right" valign="top" class="sectionsubhead"><s:text name="jsp.reports_687"/>&nbsp;</td>
	
	<td>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>" />
		<ffi:setProperty name="currTransType" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
		<%-- set default transaction type --%>
		<ffi:cinclude value1="" value2="${currTransType}" operator="equals"> 
			<ffi:setProperty name="currTransType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>" />
		</ffi:cinclude>
		<select  id="balSummaryTransDropdown"  style="width:370px;" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>" size="5" MULTIPLE>

			<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currTransType" />
			<ffi:process name="CriterionListChecker" />

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_42"/>
			</option>

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_CREDIT_TRANSACTIONS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_CREDIT_TRANSACTIONS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_40"/>
			</option>

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_DEBIT_TRANSACTIONS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_DEBIT_TRANSACTIONS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_41"/>
			</option>

			<ffi:setProperty name="TransactionTypes" property="SortField" value="<%= com.ffusion.tasks.util.GetTransactionTypes.SORTFIELD_DESCRIPTION %>" />
			<ffi:list collection="TransactionTypes.TypeList" items="keyValuePair">
				<ffi:cinclude value1="${keyValuePair.Value}" value2 ="Unknown" operator="notEquals">
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${keyValuePair.Key}" />
				<option
					value="<ffi:getProperty name='keyValuePair' property='Key'/>"
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="keyValuePair" property="Value"/>
				</option>
				</ffi:cinclude>
			</ffi:list>

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DIVIDER %>"><s:text name="jsp.default_9"/></option>

			<ffi:setProperty name="custTransGroupPrefix" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %>"/>
			<ffi:list collection="CustomTransactionGroups.TransactionGroups" items="customGroupName">
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${custTransGroupPrefix}${customGroupName}" />
				<option
					value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %><ffi:getProperty name='customGroupName'/>"
					<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
				>
					<ffi:getProperty name="customGroupName"/>
				</option>
			</ffi:list>
			<ffi:removeProperty name="custTransGroupPrefix"/>
		</select>
	</td>

	<td>
	</td>

	<td>
	</td>
</tr>
<script>
	$("#balSummaryDetailDropdown").extmultiselect({header: ""}).multiselectfilter();
	$("#balSummaryTransDropdown").extmultiselect({header: ""}).multiselectfilter();
</script>
<ffi:removeProperty name="TransactionTypes" />
<ffi:removeProperty name="CustomTransactionGroups" />
<ffi:removeProperty name="CriterionListChecker" />
<ffi:removeProperty name="CheckPerAccountReportingEntitlements" />
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
