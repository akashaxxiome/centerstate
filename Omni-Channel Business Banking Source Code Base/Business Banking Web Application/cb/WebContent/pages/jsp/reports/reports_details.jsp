<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="detailsPortlet" class="portlet multipleStepsCls ui-widget ui-widget-content ui-corner-all">
	<div class="portlet-header ui-widget-header ui-corner-all">
		<span class="portlet-title"><s:text name="jsp.reports_560"/></span>
	</div>

	<div id="detailsDiv" class="portlet-content">
		
		<%-- <sj:tabbedpanel id="criteriaTabs"> --%>
	            <%-- <sj:tab id="reportCriteriaTab" target="targetRptCriteriaDiv" label="%{getText('jsp.reports_649')}"/>
	            <sj:tab id="reportResultTab" target="targetRptResultDiv" label="%{getText('jsp.reports_653')}"/> --%>
	            <div id="targetRptCriteriaDiv" style="border:1px">
	                <s:text name="jsp.reports_576"/>
	                <br><br>
	            </div>
	            <div id="targetRptResultDiv" style="border:1px">
	            	
	            	
	            	<div id="targetRptResultDivContent">
					
	            		<%-- <s:text name="jsp.reports_654"/> --%>
	                	<br><br>
	               	</div>
	                <div id="targetRptResultDivController" style="display:none;padding-top:0.5em">
	                <div align='center'><sj:a id="rptTxtResultCancelBtn" button="true" onClickTopics="onCancelDisplayReportClick"
							><s:text name="jsp.default_82"/></sj:a></div>
	                </div>
	            </div>
				
	  <%--   </sj:tabbedpanel> --%>
    </div>
   
		<div id="detailsPortlet-menu" class="portletHeaderMenuContainer" style="display:none;">
				 <ul class="portletHeaderMenu" style="background:#fff">
					<li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
				 </ul>
			</div>
</div>

<script>
	//Initialize portlet with settings icon
	ns.common.initializePortlet("detailsPortlet");

	/* $('#detailsPortlet').portlet({
		close: true,
		bookmark: true,
		closeCallback: function(){
			$('#details').slideUp();
			$('#reportTabs').portlet('expand');
		},
		helpCallback: function(){
		 	var helpFile;
		 	
			if($("#reportCriteriaTab").hasClass('ui-tabs-selected'))
				helpFile = $('#targetRptCriteriaDiv').find('.moduleHelpClass').html(); // the content is in the reportscreteria.jsp
			else 
				helpFile = $('#targetRptResultDiv').find('.moduleHelpClass').html();
			
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		},
		bookMarkCallback: function(){
			var settingsArray = ns.shortcut.getShortcutSettingWithPortlet( '#detailsPortlet', true );
			var path = settingsArray[0];
			var ent  = settingsArray[1];
			ns.shortcut.openShortcutWindow( path, ent );
		}
	}); */
</script>
