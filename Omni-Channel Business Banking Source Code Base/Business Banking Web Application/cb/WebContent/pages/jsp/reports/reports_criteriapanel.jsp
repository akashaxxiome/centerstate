<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page contentType="text/html; charset=UTF-8" %>

    <div id="reportCriteriaPanel" class="criteriaPanel" align="center">
        <div id="reportCriteriaDiv" class="" align="center">
	        <s:url id="ajax" value="reportcriteria.jsp">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		    </s:url>
			<s:include value="%{ajax}" />
        </div><!-- reportCriteriaForm -->
        
        
       	<%-- <s:url id="saved_reports_url" value="reports_savedreports.jsp">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>		
		</s:url>	
		<div id="savedReportsDiv" class="savedReportsPane">
			<s:include value="%{saved_reports_url}" />
		</div> --%>
        <br>
    </div><!-- reportCriteriaPanel -->