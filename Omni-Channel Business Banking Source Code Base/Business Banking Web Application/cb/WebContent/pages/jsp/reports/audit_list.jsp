<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setProperty name="subMenuSelected" value="report_audit"/>

<s:set var="tmpI18nStr" value="%{getText('jsp.reports_512')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>

<%-- Make a reference to this page, so the report criteria page can go back to it on cancel --%>
<ffi:setProperty name="reportListPage" value="${FullPagesPath}/reports/audit_list.jsp"/>

<ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>
<ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
<ffi:setProperty name="GenerateReportBase" property="Target" value="${FullPagesPath}reports/displayreport.jsp"/>

<ffi:setProperty name="reportTopMenuId" value="reporting_audit"/>
  
  <div id="auditSummaryTab" class="portlet gridPannelSupportCls">
	<div class="portlet-header" >
	    <div class="searchHeaderCls">
			<div class="summaryGridTitleHolder">
				<s:if test="%{#parameters.dashboardElementId[0] == 'permissionContent'}">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.default_569" />
					</span>
				</s:if>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'historyContent'}">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.reports_595" />
					</span>
				</s:elseif>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'changesContent'}">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.reports_532" />
					</span>
				</s:elseif>
				<s:else>
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.default_53" />
					</span>
				</s:else>
				
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight marginRight10"></span>
				
				<div class="gridTabDropdownHolder">
					<span class="gridTabDropdownItem" id='auditContent' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/audit_list.jsp',this)" >
						<s:text name="jsp.default_53" />
					</span>
					<span class="gridTabDropdownItem" id='permissionContent' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/audit_list.jsp',this)">
						<s:text name="jsp.default_569" />
					</span>
					<span class="gridTabDropdownItem" id='historyContent' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/audit_list.jsp',this)">
						<s:text name="jsp.reports_595" />
					</span>
					<%-- Start: Dual approval processing --%>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<span class="gridTabDropdownItem" id='changesContent' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/audit_list.jsp',this)">
						<s:text name="jsp.reports_532" />
					</span>
					</ffi:cinclude>
					<%-- End: Dual approval processing --%>
				</div>
			</div>
		</div>
	</div>
	<%
	if( session.getAttribute("ReportNamesAndCategories")==null ) {
	%>
		<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
		<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
		<ffi:process name="GetHashmapInSession"/>
		<ffi:removeProperty name="GetHashmapInSession"/>
	<%
	}
	%>
	<div class="portlet-content" id="auditSummaryTabContent">
		<s:if test="%{#parameters.dashboardElementId[0] != ''}">
			<s:if test="%{#parameters.dashboardElementId[0] == 'permissionContent'}">
				<s:include value="%{#session.PagesPath}reports/inc/audit_list_perm.jsp" />
			</s:if>
			<s:elseif test="%{#parameters.dashboardElementId[0] == 'historyContent'}">
				<s:include value="%{#session.PagesPath}reports/inc/audit_list_history.jsp" />
			</s:elseif>
			<s:elseif test="%{#parameters.dashboardElementId[0] == 'changesContent'}">
					<s:include value="%{#session.PagesPath}reports/inc/da-audit_list_pending.jsp" />
			</s:elseif>
			<s:else>
					<s:include value="%{#session.PagesPath}reports/inc/audit_list_audit.jsp" />
			</s:else>
		</s:if>
		<s:else>
			<s:include value="%{#session.PagesPath}reports/inc/audit_list_audit.jsp" />
		</s:else>
	</div>
	<div id="accountSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('auditSummaryTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<script type="text/javascript">
	//Initialize portlet with settings icon
	ns.common.initializePortlet("auditSummaryTab");
	
</script>

<ffi:setProperty name="BackURL" value="${FullPagesPath}reports/audit_list.jsp"/>
<ffi:setProperty name="saveBackURL" value=""/>
<ffi:removeProperty name="reportNames"/>
<ffi:removeProperty name="reportNamesSize"/>
<ffi:removeProperty name="category"/>
<ffi:removeProperty name="Sort"/>
<ffi:removeProperty name="reportTopMenuId"/>
<ffi:removeProperty name="reportTypeID"/>