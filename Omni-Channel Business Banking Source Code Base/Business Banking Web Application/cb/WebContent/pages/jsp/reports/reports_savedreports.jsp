<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>


<%
request.setAttribute("reportTopMenuId",request.getParameter("reportTopMenuId"));
request.setAttribute("reportTypeID",request.getParameter("reportTypeID"));
%>
<ffi:setGridURL grid="GRID_savedreports" name="ViewURL" url="/cb/pages/jsp/reports/GetReportFromNameAction_generate.action?reportName={0}" parm0="ReportName"/>
<ffi:setGridURL grid="GRID_savedreports" name="EditURL" url="/cb/pages/jsp/reports/GetReportFromNameAction_edit.action?reportName={0}&reportTopMenuId=${reportTopMenuId}&reportTypeID=${reportTypeID}" parm0="ReportName"/>
<ffi:setGridURL grid="GRID_savedreports" name="DeleteURL" url="/cb/pages/jsp/reports/rptconfirmdelete.jsp?reportID={0}&reportName={1}&reportDesc={2}" parm0="ReportID" parm1="ReportName" parm2="Description"/>

<ffi:setProperty name="tempURL" value="/pages/jsp/reports/GetSavedReportsAction.action?GridURLs=GRID_savedreports" URLEncrypt="true"/>
    <s:url id="savedReportsUrl" value="%{#session.tempURL}" escapeAmp="false"/>
    
	<sjg:grid  
		id="savedReportsGridId"  
		caption="%{getText('jsp.reports_saved')}"  
		sortable="true"  
		dataType="json"  
		href="%{savedReportsUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="false"
		onGridCompleteTopics="savedReportsGridCompleteEvents"
		> 
		
		<sjg:gridColumn 
			name="reportName" 
			index="name" 
			title="%{getText('jsp.default_353')}" 
			sortable="true" 
			formatter="ns.report.formatReportNameLink" 
			width="90"/>
			
		<sjg:gridColumn 
			name="description" 
			index="desc" 
			title="%{getText('jsp.default_170')}" 
			sortable="true" 
			width="120"/>
			
		<sjg:gridColumn 
			name="reportID" 
			index="id" 
			title="%{getText('jsp.default_27')}" 
			hidedlg="true"  
			sortable="false" 
			search="false" 
			hidden="false" 
			formatter="ns.report.formatSavedReportsActionLinks" 
			width="50"/>
		
		<sjg:gridColumn 
			name="isCurrentReport" 
			index="iscurrent" 
			title="%{getText('jsp.reports_604')}" 
			hidedlg="true" 
			sortable="false" 
			hidden="true" 
			width="50"/>
			
		<sjg:gridColumn name="ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		
		
	</sjg:grid>
	

    <script type="text/javascript">

	    $(document).ready(function () {

	    	//jQuery("#list1").jqGrid('navGrid','#pager1',{edit:false,add:false,del:false,search:false,refresh:false}); 
	    	$("#savedReportsGridId").jqGrid('navButtonAdd', "#savedReportsGridId_pager", {
	    	    caption: "",
	    	    title: js_search,
	    	    buttonicon: 'ui-icon-search',
	    	    onClickButton: function(){
	    			$("#savedReportsGridId")[0].toggleToolbar()
	    	    }
	    	});

	    	$("#savedReportsGridId").jqGrid('navButtonAdd', "#savedReportsGridId_pager", {
	    	    caption: "",
	    	    title: js_clear_search,
	    	    buttonicon: 'ui-icon-refresh',
	    	    onClickButton: function(){
	    			$("#savedReportsGridId")[0].clearToolbar()
	    	    }
	    	});

	    	/*
	    	$("#savedReportsGridId").jqGrid('navButtonAdd', "#savedReportsGridId_pager", {
	    	    caption: "",
	    	    title: "Set Columns",
	    	    buttonicon: 'ui-icon-grip-dotted-vertical',
	    	    onClickButton: function(){
	    			$("#savedReportsGridId").jqGrid('setColumns');
	    	    }
	    	});
	    	*/

    	
	    	$("#savedReportsGridId").jqGrid('filterToolbar');
	    	$("#savedReportsGridId")[0].toggleToolbar();

	    	if(ns.common.isInitialized($("#savedReportsGridId tbody"),"ui-jqgrid")){	    	
	    		$("#savedReportsGridId tbody").sortable("destroy");
	    	}

	    });

    </script>