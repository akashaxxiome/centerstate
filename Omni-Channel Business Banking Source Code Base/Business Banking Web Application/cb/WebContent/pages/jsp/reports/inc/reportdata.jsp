<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<% String opt_pagewidth; %>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PRINTER_READY_PAGEWIDTH %>"/>
<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" assignTo="opt_pagewidth"/>
<%
	String real_pagewidth = String.valueOf( Integer.parseInt( opt_pagewidth ) + 2 );
	
	session.setAttribute( "real_pagewidth", real_pagewidth );
%>


<table width="<ffi:getProperty name="real_pagewidth"/>" border="0" cellspacing="0" cellpadding="0">
	<tr><%-- Yes, we use top for bottom and bottom for top so we can seal off the data table. --%>
		<td class=tbrd_b >&nbsp;</td>
	</tr>
	<tr>
		<td class="tbrd_lr dkback">
			<ffi:object id="exportHeader" name="com.ffusion.tasks.reporting.ExportHeaderOptions"/>
			<ffi:setProperty name="exportHeader" property="ReportName" value="ReportData"/>
			<ffi:setProperty name="exportHeader" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
			<ffi:process name="exportHeader"/>
			<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_HEADER_NAME %>" encode="false"/>
		</td>
	</tr>
	<tr>
		<td class="tbrd_ltr"><br>
			<ffi:object id="exportForDisplay" name="com.ffusion.tasks.reporting.ExportReport"/>
			<ffi:setProperty name="exportForDisplay" property="ReportName" value="ReportData"/>
			<ffi:setProperty name="exportForDisplay" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
			<ffi:process name="exportForDisplay"/>
			<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_NAME %>" encode="false"/>
		</td>
	</tr>
	<tr>
		<td class="tbrd_ltr dkback">
			<ffi:object id="exportFooter" name="com.ffusion.tasks.reporting.ExportFooterOptions"/>
			<ffi:setProperty name="exportFooter" property="ReportName" value="ReportData"/>
			<ffi:setProperty name="exportFooter" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
			<ffi:process name="exportFooter"/>
			<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_FOOTER_NAME %>" encode="false"/>
		</td>
	</tr>
	<tr><%-- Yes, we use top for bottom and bottom for top so we can seal off the data table. --%>
		<td class=tbrd_t>&nbsp;</td>
	</tr>
</table>

<ffi:removeProperty name="opt_pagewidth"/>
<ffi:removeProperty name="real_pagewidth"/>

<%-- set opt_printer_ready to false (default value) --%>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value='<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PRINTER_READY %>'/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" value='<%= com.ffusion.beans.reporting.ReportCriteria.VAL_FALSE %>'/>

<%-- Nullify report result to save memory --%>
<%
com.ffusion.beans.reporting.Report report = (com.ffusion.beans.reporting.Report)session.getAttribute( "ReportData" );
report.setReportResult(null);
%>
