<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.ffusion.beans.affiliatebank.AffiliateBank" %>

<ffi:help id="reports_audit_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="1">
	<td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
	<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports_511')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
</tr>

<%
	HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_SYSTEM_ACTIVITY %>"/>
<ffi:process name="CheckReportEntitlement"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

<tr>
	<ffi:object id="GetSystemActivityRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetSystemActivityRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetSystemActivityRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetSystemActivityRpt" property="ReportsIDsName" value="ReportsSysAct"/>
	<ffi:setProperty name="GetSystemActivityRpt" property="ReportName" value="ReportData"/>

	<ffi:object id="GenerateSystemActivityRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateSystemActivityRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateSystemActivityRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateSystemActivityRpt" property="ReportsIDsName" value="ReportsSysAct"/>
	<ffi:setProperty name="GenerateSystemActivityRpt" property="ReportName" value="ReportData"/>

	<%
	String category = com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_SYSTEM_ACTIVITY;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.System_Activity')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_OBO %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
	<ffi:object id="GetOBOActivityRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetOBOActivityRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetOBOActivityRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetOBOActivityRpt" property="ReportsIDsName" value="ReportsOBOAct"/>
	<ffi:setProperty name="GetOBOActivityRpt" property="ReportName" value="ReportData"/>

	<ffi:object id="GenerateOBOActivityRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateOBOActivityRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateOBOActivityRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateOBOActivityRpt" property="ReportsIDsName" value="ReportsOBOAct"/>
	<ffi:setProperty name="GenerateOBOActivityRpt" property="ReportName" value="ReportData"/>

	<%
	String category = com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_OBO;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.On_Behalf_Of')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
		
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_TEMPLATE_HISTORY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

    <ffi:object id="GetTempleHistoryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetTempleHistoryRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetTempleHistoryRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetTempleHistoryRpt" property="ReportsIDsName" value="ReportsTempHist"/>
	<ffi:setProperty name="GetTempleHistoryRpt" property="ReportName" value="ReportData"/>

	<ffi:object id="GenerateTempleHistoryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateTempleHistoryRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateTempleHistoryRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateTempleHistoryRpt" property="ReportsIDsName" value="ReportsTempHist"/>
	<ffi:setProperty name="GenerateTempleHistoryRpt" property="ReportName" value="ReportData"/>


	<%
	String category = com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_TEMPLATE_HISTORY;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Payment_Template_History')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
		
</ffi:cinclude>

<ffi:cinclude value1="${AffiliateBank.RestrictConcurrentLogin}" value2="<%=AffiliateBank.NO_RESTRICT_CONCURRENT_LOGIN %>" operator="notEquals">
	<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

	<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_CONCURRENT_LOGIN %>"/>
	<ffi:process name="CheckReportEntitlement"/>

	<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

	<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
		<ffi:object id="GetConcurrentLoginRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
		<ffi:setProperty name="GetConcurrentLoginRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
		<ffi:setProperty name="GetConcurrentLoginRpt" property="IDInSessionName" value="reportID" />
		<ffi:setProperty name="GetConcurrentLoginRpt" property="ReportsIDsName" value="ReportsConcLogin"/>
		<ffi:setProperty name="GetConcurrentLoginRpt" property="ReportName" value="ReportData"/>

		<ffi:object id="GenerateConcurrentLoginRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
		<ffi:setProperty name="GenerateConcurrentLoginRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
		<ffi:setProperty name="GenerateConcurrentLoginRpt" property="IDInSessionName" value="reportID" />
		<ffi:setProperty name="GenerateConcurrentLoginRpt" property="ReportsIDsName" value="ReportsConcLogin"/>
		<ffi:setProperty name="GenerateConcurrentLoginRpt" property="ReportName" value="ReportData"/>

		<%
		String category = com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_CONCURRENT_LOGIN;
		session.setAttribute("category", category);
		ArrayList reportNames = (ArrayList)(hashmap.get(category));
		int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
		session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
		session.setAttribute("reportNames", reportNames);
		%>
		<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Concurrent_Login')}" scope="request" />
		<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
		<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="IsUserPrimaryAdmin" name="com.ffusion.tasks.user.IsUserPrimaryAdmin"/>
<ffi:process name="IsUserPrimaryAdmin"/>

<ffi:cinclude value1="${IsUserPrimaryAdmin.PrimaryAdmin}" value2="true">
    <ffi:object id="GetContactAdminRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetContactAdminRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetContactAdminRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetContactAdminRpt" property="ReportsIDsName" value="ReportsContactAdmin"/>
	<ffi:setProperty name="GetContactAdminRpt" property="ReportName" value="ReportData"/>

	<ffi:object id="GenerateContactAdminRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateContactAdminRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateContactAdminRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateContactAdminRpt" property="ReportsIDsName" value="ReportsContactAdmin"/>
	<ffi:setProperty name="GenerateContactAdminRpt" property="ReportName" value="ReportData"/>


	<%
	String category = com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_CONTACT_ADMIN;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Contact_My_Admin')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
	
</ffi:cinclude>
</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>
<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategorySysAct"/>
<ffi:removeProperty name="GetReportsByCategoryOBOAct"/>
<ffi:removeProperty name="GetReportsByCategoryTempHist"/>
