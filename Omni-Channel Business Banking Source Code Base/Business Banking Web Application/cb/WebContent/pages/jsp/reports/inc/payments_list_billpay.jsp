<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%-- Make sure that the PAYEES and BILLPAYACCOUNTS are in session --%>
<ffi:cinclude value1="${payments_init_touched}" value2="true" operator="notEquals" ><s:include value="%{#session.PagesPath}inc/init/payments-init.jsp" /></ffi:cinclude>

<ffi:help id="reports_payments_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="1">
    <td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
	<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_74')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
</tr>

<%
    HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_PMT_HISTORY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
    
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

    <ffi:object id="GetPaymentHistoryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GetPaymentHistoryRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
    <ffi:setProperty name="GetPaymentHistoryRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GetPaymentHistoryRpt" property="ReportsIDsName" value="ReportsPayHist"/>
    <ffi:setProperty name="GetPaymentHistoryRpt" property="ReportName" value="ReportData"/>
    
    <ffi:object id="GeneratePaymentHistoryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GeneratePaymentHistoryRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
    <ffi:setProperty name="GeneratePaymentHistoryRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GeneratePaymentHistoryRpt" property="ReportsIDsName" value="ReportsPayHist"/>
    <ffi:setProperty name="GeneratePaymentHistoryRpt" property="ReportName" value="ReportData"/>
    
    <%
    String category = com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_PMT_HISTORY;
    session.setAttribute("category", category);
    ArrayList reportNames = (ArrayList)(hashmap.get(category));
    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
    session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Payment_History')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_UNPROCESSED_PMT %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
    
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

    <ffi:object id="GetUnprocessedPaymentRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GetUnprocessedPaymentRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
    <ffi:setProperty name="GetUnprocessedPaymentRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GetUnprocessedPaymentRpt" property="ReportsIDsName" value="ReportsUnprocPmt"/>
    <ffi:setProperty name="GetUnprocessedPaymentRpt" property="ReportName" value="ReportData"/>
    
    <ffi:object id="GenerateUnprocessedPaymentRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GenerateUnprocessedPaymentRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
    <ffi:setProperty name="GenerateUnprocessedPaymentRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GenerateUnprocessedPaymentRpt" property="ReportsIDsName" value="ReportsUnprocPmt"/>
    <ffi:setProperty name="GenerateUnprocessedPaymentRpt" property="ReportName" value="ReportData"/>
    
    <%
    String category = com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_UNPROCESSED_PMT;
    session.setAttribute("category", category);
    ArrayList reportNames = (ArrayList)(hashmap.get(category));
    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
    session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Unprocessed_Payment')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_EXPENSE_BY_PAYEE_SUMMARY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
    
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

    <ffi:object id="GetExpenseByPayeeRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GetExpenseByPayeeRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
    <ffi:setProperty name="GetExpenseByPayeeRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GetExpenseByPayeeRpt" property="ReportsIDsName" value="ReportsExpByPayeeSummary"/>
    <ffi:setProperty name="GetExpenseByPayeeRpt" property="ReportName" value="ReportData"/>
    
    <ffi:object id="GenerateExpenseByPayeeRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GenerateExpenseByPayeeRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
    <ffi:setProperty name="GenerateExpenseByPayeeRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GenerateExpenseByPayeeRpt" property="ReportsIDsName" value="ReportsExpByPayeeSummary"/>
    <ffi:setProperty name="GenerateExpenseByPayeeRpt" property="ReportName" value="ReportData"/>
    
    <%
    String category = com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_EXPENSE_BY_PAYEE_SUMMARY;
    session.setAttribute("category", category);
    ArrayList reportNames = (ArrayList)(hashmap.get(category));
    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
    session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Expense_by_Payee_Summary')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_EXPENSE_BY_PAYEE_DETAIL %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
    
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

    <ffi:object id="GetExpenseByPayeeDetailRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GetExpenseByPayeeDetailRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
    <ffi:setProperty name="GetExpenseByPayeeDetailRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GetExpenseByPayeeDetailRpt" property="ReportsIDsName" value="ReportsExpByPayeeDetail"/>
    <ffi:setProperty name="GetExpenseByPayeeDetailRpt" property="ReportName" value="ReportData"/>
    
    <ffi:object id="GenerateExpenseByPayeeDetailRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GenerateExpenseByPayeeDetailRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
    <ffi:setProperty name="GenerateExpenseByPayeeDetailRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GenerateExpenseByPayeeDetailRpt" property="ReportsIDsName" value="ReportsExpByPayeeDetail"/>
    <ffi:setProperty name="GenerateExpenseByPayeeDetailRpt" property="ReportName" value="ReportData"/>
    
    <%
    String category = com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_EXPENSE_BY_PAYEE_DETAIL;
    session.setAttribute("category", category);
    ArrayList reportNames = (ArrayList)(hashmap.get(category));
    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
    session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Expense_by_Payee_Detail')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>

    <ffi:object id="GetBillPayByStatusRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GetBillPayByStatusRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
    <ffi:setProperty name="GetBillPayByStatusRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GetBillPayByStatusRpt" property="ReportsIDsName" value="ReportsBillPayByStatus"/>
    <ffi:setProperty name="GetBillPayByStatusRpt" property="ReportName" value="ReportData"/>

    <ffi:object id="GenerateBillPayByStatusRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GenerateBillPayByStatusRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
    <ffi:setProperty name="GenerateBillPayByStatusRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GenerateBillPayByStatusRpt" property="ReportsIDsName" value="ReportsBillPayByStatus"/>
    <ffi:setProperty name="GenerateBillPayByStatusRpt" property="ReportName" value="ReportData"/>

    <%
    String category = com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_BILLPAY_BY_STATUS;
    session.setAttribute("category", category);
    ArrayList reportNames = (ArrayList)(hashmap.get(category));
    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
    session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.BillPay_By_Status')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>

<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryPayHist"/>
<ffi:removeProperty name="GetReportsByCategoryUnprocPmt"/>
<ffi:removeProperty name="GetReportsByCategoryExpByPayeeSummary"/>
<ffi:removeProperty name="GetReportsByCategoryExpByPayeeDetail"/>
