<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>

<ffi:help id="reports_payments_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="1">
    
    <td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
	<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_443')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
</tr>

<%
    HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_TRANSFER_HISTORY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

    <ffi:object id="GetTransferHistoryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GetTransferHistoryRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
    <ffi:setProperty name="GetTransferHistoryRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GetTransferHistoryRpt" property="ReportsIDsName" value="ReportsTransferHistory"/>
    <ffi:setProperty name="GetTransferHistoryRpt" property="ReportName" value="ReportData"/>

    <ffi:object id="GenerateTransferHistoryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GenerateTransferHistoryRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
    <ffi:setProperty name="GenerateTransferHistoryRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GenerateTransferHistoryRpt" property="ReportsIDsName" value="ReportsTransferHistory"/>
    <ffi:setProperty name="GenerateTransferHistoryRpt" property="ReportName" value="ReportData"/>

    <%
    String category = com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_TRANSFER_HISTORY;
    session.setAttribute("category", category);
    ArrayList reportNames = (ArrayList)(hashmap.get(category));
    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
    session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Transfer_History')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_TRANSFER_DETAIL %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">


    <ffi:object id="GetTransferDetailRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GetTransferDetailRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
    <ffi:setProperty name="GetTransferDetailRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GetTransferDetailRpt" property="ReportsIDsName" value="ReportsTransferDetail"/>
    <ffi:setProperty name="GetTransferDetailRpt" property="ReportName" value="ReportData"/>

    <ffi:object id="GenerateTransferDetailRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GenerateTransferDetailRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
    <ffi:setProperty name="GenerateTransferDetailRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GenerateTransferDetailRpt" property="ReportsIDsName" value="ReportsTransferDetail"/>
    <ffi:setProperty name="GenerateTransferDetailRpt" property="ReportName" value="ReportData"/>

    <%
    String category = com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_TRANSFER_DETAIL;
    session.setAttribute("category", category);
    ArrayList reportNames = (ArrayList)(hashmap.get(category));
    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
    session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Transfer_Detail')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />


</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_PENDING_TRANSFER %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

    <ffi:object id="GetPendingTransferRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GetPendingTransferRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
    <ffi:setProperty name="GetPendingTransferRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GetPendingTransferRpt" property="ReportsIDsName" value="ReportsPendingTransfer"/>
    <ffi:setProperty name="GetPendingTransferRpt" property="ReportName" value="ReportData"/>

    <ffi:object id="GeneratePendingTransferRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GeneratePendingTransferRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
    <ffi:setProperty name="GeneratePendingTransferRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GeneratePendingTransferRpt" property="ReportsIDsName" value="ReportsPendingTransfer"/>
    <ffi:setProperty name="GeneratePendingTransferRpt" property="ReportName" value="ReportData"/>

    <%
    String category = com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_PENDING_TRANSFER;
    session.setAttribute("category", category);
    ArrayList reportNames = (ArrayList)(hashmap.get(category));
    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
    session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Pending_Transfer')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>

    <ffi:object id="GetTransferByStatusRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GetTransferByStatusRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
    <ffi:setProperty name="GetTransferByStatusRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GetTransferByStatusRpt" property="ReportsIDsName" value="ReportsTransferByStatus"/>
    <ffi:setProperty name="GetTransferByStatusRpt" property="ReportName" value="ReportData"/>

    <ffi:object id="GenerateTransferByStatusRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
    <ffi:setProperty name="GenerateTransferByStatusRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
    <ffi:setProperty name="GenerateTransferByStatusRpt" property="IDInSessionName" value="reportID" />
    <ffi:setProperty name="GenerateTransferByStatusRpt" property="ReportsIDsName" value="ReportsTransferByStatus"/>
    <ffi:setProperty name="GenerateTransferByStatusRpt" property="ReportName" value="ReportData"/>

    <%
    String category = com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_TRANSFER_BY_STATUS;
    session.setAttribute("category", category);
    ArrayList reportNames = (ArrayList)(hashmap.get(category));
    int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
    session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
    session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Transfer_By_Status')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>
<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryTransferHistory"/>
<ffi:removeProperty name="GetReportsByCategoryTransferDetail"/>
<ffi:removeProperty name="GetReportsByCategoryPendingTransfer"/>
