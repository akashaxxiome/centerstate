<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>

<ffi:help id="reports_cash_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr height="1">
						<td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
						<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
						<s:set var="tmpI18nStr" value="%{getText('jsp.default_327')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
					</tr>

<%
	HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_ISSUE_SUMMARY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetPPayIssuesRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetPPayIssuesRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetPPayIssuesRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetPPayIssuesRpt" property="ReportsIDsName" value="ReportsPPayIssues"/>
	<ffi:setProperty name="GetPPayIssuesRpt" property="ReportName" value="ReportData"/>

	<ffi:object id="GeneratePPayIssuesRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GeneratePPayIssuesRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GeneratePPayIssuesRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GeneratePPayIssuesRpt" property="ReportsIDsName" value="ReportsPPayIssues"/>
	<ffi:setProperty name="GeneratePPayIssuesRpt" property="ReportName" value="ReportData"/>

	<%
	String category = com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_ISSUE_SUMMARY;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Exceptions_Summary')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_DECISION_HISTORY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetPPayDecisionsRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetPPayDecisionsRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetPPayDecisionsRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetPPayDecisionsRpt" property="ReportsIDsName" value="ReportsPPayDecis"/>
	<ffi:setProperty name="GetPPayDecisionsRpt" property="ReportName" value="ReportData"/>

	<ffi:object id="GeneratePPayDecisionsRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GeneratePPayDecisionsRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GeneratePPayDecisionsRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GeneratePPayDecisionsRpt" property="ReportsIDsName" value="ReportsPPayDecis"/>
	<ffi:setProperty name="GeneratePPayDecisionsRpt" property="ReportName" value="ReportData"/>

	<%
	String category = com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_DECISION_HISTORY;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports_559')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>

<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_ACTIVITY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>

<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetPPayActivityRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetPPayActivityRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetPPayActivityRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetPPayActivityRpt" property="ReportsIDsName" value="ReportsPPayActivity"/>
	<ffi:setProperty name="GetPPayActivityRpt" property="ReportName" value="ReportData"/>

	<ffi:object id="GeneratePPayActivityRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GeneratePPayActivityRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GeneratePPayActivityRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GeneratePPayActivityRpt" property="ReportsIDsName" value="ReportsPPayActivity"/>
	<ffi:setProperty name="GeneratePPayActivityRpt" property="ReportName" value="ReportData"/>

	<%
	String category = com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_ACTIVITY;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Positive_Pay_Activity')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>
<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryPPayIssues"/>
<ffi:removeProperty name="GetReportsByCategoryPPayDecis"/>
