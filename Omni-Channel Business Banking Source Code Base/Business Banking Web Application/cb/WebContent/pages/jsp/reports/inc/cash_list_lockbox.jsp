<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>

<ffi:help id="reports_cash_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
							<%-- <td width="15%" align="center"><span class="sectionsubhead"><s:text name="jsp.home.column.label.action"/></span></td> --%>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MGMT_REPORTING %>" >

<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>

<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryAnyDataClassification}" value2="false" operator="equals">
	<%-- <tr height="1">
		<td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
		<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
		<s:set var="tmpI18nStr" value="%{getText('jsp.default_268')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
	</tr> --%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_268')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
	<tr>
		<ffi:setProperty name="TypeColumnValue" value=""/>
		<td colspan="2" align="center">
			<s:text name="jsp.reports_679"/>
		</td>
	</tr>
</ffi:cinclude> <%-- end include on report NOT entitled to detail or summary for either intra day or previous day --%>

<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryAnyDataClassification}" value2="true" operator="equals">

<%-- Get lockbox accounts --%>
<ffi:setProperty name="DateFormat" value="${UserLocale.DateFormat}"/>

<ffi:object id="LockboxSummaries" name="com.ffusion.tasks.lockbox.LockboxSummariesTask" scope="session"/>
<ffi:setProperty name="LockboxSummaries" property="DateFormat" value="${DateFormat}"/>
<ffi:setProperty name="LockboxSummaries" property="StartDate" value="${LockboxCriteriaDate}"/>
<ffi:setProperty name="LockboxSummaries" property="EndDate" value="${LockboxCriteriaDate}"/>
<ffi:setProperty name="LockboxSummaries" property="DataClassification" value="${LockboxDataClassification}"/>
<ffi:process name="LockboxSummaries"/>
<ffi:removeProperty name="LockboxSummaries"/>

<ffi:removeProperty name="DateFormat"/>
<%-- No, we don't need lockbox summaries here --%>
<ffi:removeProperty name="<%= com.ffusion.tasks.lockbox.LockboxTaskConstants.LOCKBOX_SUMMARIES %>" />


<%-- <tr height="1">
	<td colspan="2">&nbsp;</td>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_268')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
</tr> --%>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_268')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
<%
	HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.RPT_TYPE_LOCKBOX_SUMMARY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="equals">
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetLockboxSummaryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetLockboxSummaryRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetLockboxSummaryRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetLockboxSummaryRpt" property="ReportsIDsName" value="ReportsLBSum"/>
	<ffi:setProperty name="GetLockboxSummaryRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateLockboxSummaryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateLockboxSummaryRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateLockboxSummaryRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateLockboxSummaryRpt" property="ReportsIDsName" value="ReportsLBSum"/>
	<ffi:setProperty name="GenerateLockboxSummaryRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.lockbox.LockboxRptConsts.RPT_TYPE_LOCKBOX_SUMMARY;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.cash_66')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.RPT_TYPE_LOCKBOX_DEPOSIT_REPORT %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}" value2="true" operator="equals">
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetLockboxDetailRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetLockboxDetailRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetLockboxDetailRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetLockboxDetailRpt" property="ReportsIDsName" value="ReportsLBDetail"/>
	<ffi:setProperty name="GetLockboxDetailRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateLockboxDetailRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateLockboxDetailRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateLockboxDetailRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateLockboxDetailRpt" property="ReportsIDsName" value="ReportsLBDetail"/>
	<ffi:setProperty name="GenerateLockboxDetailRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.lockbox.LockboxRptConsts.RPT_TYPE_LOCKBOX_DEPOSIT_REPORT;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Lockbox_Deposit')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />


</ffi:cinclude>
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.lockbox.LockboxRptConsts.RPT_TYPE_DEPOSIT_ITEM_SEARCH %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}" value2="true" operator="equals">
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetLockboxDepositItemRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetLockboxDepositItemRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetLockboxDepositItemRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetLockboxDepositItemRpt" property="ReportsIDsName" value="ReportsDepItem"/>
	<ffi:setProperty name="GetLockboxDepositItemRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateLockboxDepositItemRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateLockboxDepositItemRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateLockboxDepositItemRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateLockboxDepositItemRpt" property="ReportsIDsName" value="ReportsDepItem"/>
	<ffi:setProperty name="GenerateLockboxDepositItemRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.lockbox.LockboxRptConsts.RPT_TYPE_DEPOSIT_ITEM_SEARCH;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Deposit_Item_Search')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</ffi:cinclude>
</ffi:cinclude> <%-- End is entitled to detail or summary on intra day or previous day --%>
</ffi:cinclude> <%-- End is entitled to management (information) reporting --%>
</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>
<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryLBSum"/>
<ffi:removeProperty name="GetReportsByCategoryLBDetail"/>
<ffi:removeProperty name="GetReportsByCategoryDepItem"/>
