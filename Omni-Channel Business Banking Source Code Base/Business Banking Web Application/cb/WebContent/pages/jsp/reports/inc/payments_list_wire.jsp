<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>

<ffi:help id="reports_payments_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="1">
	<td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
	<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_465')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
</tr>

<%
	HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_COMPLETED_WIRES %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetCompletedWiresRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetCompletedWiresRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetCompletedWiresRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetCompletedWiresRpt" property="ReportsIDsName" value="ReportsCompletedWires"/>
	<ffi:setProperty name="GetCompletedWiresRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateCompletedWiresRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateCompletedWiresRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateCompletedWiresRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateCompletedWiresRpt" property="ReportsIDsName" value="ReportsCompletedWires"/>
	<ffi:setProperty name="GenerateCompletedWiresRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_COMPLETED_WIRES;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Completed_Wires')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
	
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_FAILED_WIRES %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetFailedWiresRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetFailedWiresRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetFailedWiresRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetFailedWiresRpt" property="ReportsIDsName" value="ReportsFailedWires"/>
	<ffi:setProperty name="GetFailedWiresRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateFailedWiresRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateFailedWiresRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateFailedWiresRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateFailedWiresRpt" property="ReportsIDsName" value="ReportsFailedWires"/>
	<ffi:setProperty name="GenerateFailedWiresRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_FAILED_WIRES;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Failed_Wires')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
	
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_INPROCESS_WIRES %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetInprocessWiresRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetInprocessWiresRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetInprocessWiresRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetInprocessWiresRpt" property="ReportsIDsName" value="ReportsInProcWires"/>
	<ffi:setProperty name="GetInprocessWiresRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateInprocessWiresRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateInprocessWiresRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateInprocessWiresRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateInprocessWiresRpt" property="ReportsIDsName" value="ReportsInProcWires"/>
	<ffi:setProperty name="GenerateInprocessWiresRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_INPROCESS_WIRES;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.InProcess_Wires')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
	
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_BY_STATUS %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetByStatusRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetByStatusRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetByStatusRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetByStatusRpt" property="ReportsIDsName" value="ReportsWireByStatus"/>
	<ffi:setProperty name="GetByStatusRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateByStatusRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateByStatusRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateByStatusRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateByStatusRpt" property="ReportsIDsName" value="ReportsWireByStatus"/>
	<ffi:setProperty name="GenerateByStatusRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_BY_STATUS;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Wire_By_Status')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
	
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_BY_SOURCE %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetBySourceRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetBySourceRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetBySourceRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetBySourceRpt" property="ReportsIDsName" value="ReportsWireBySource"/>
	<ffi:setProperty name="GetBySourceRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateBySourceRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateBySourceRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateBySourceRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateBySourceRpt" property="ReportsIDsName" value="ReportsWireBySource"/>
	<ffi:setProperty name="GenerateBySourceRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_BY_SOURCE;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Wire_By_Source')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_BY_TEMPLATE %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetByTemplateRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetByTemplateRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetByTemplateRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetByTemplateRpt" property="ReportsIDsName" value="ReportsWireByTempl"/>
	<ffi:setProperty name="GetByTemplateRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateByTemplateRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateByTemplateRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateByTemplateRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateByTemplateRpt" property="ReportsIDsName" value="ReportsWireByTempl"/>
	<ffi:setProperty name="GenerateByTemplateRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_BY_TEMPLATE;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Wire_By_Template')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_TEMPLATE_WIRES %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetTemplateWiresRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetTemplateWiresRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetTemplateWiresRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetTemplateWiresRpt" property="ReportsIDsName" value="ReportsWireTemplates"/>
	<ffi:setProperty name="GetTemplateWiresRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateTemplateWiresRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateTemplateWiresRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateTemplateWiresRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateTemplateWiresRpt" property="ReportsIDsName" value="ReportsWireTemplates"/>
	<ffi:setProperty name="GenerateTemplateWiresRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_TEMPLATE_WIRES;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.user_414')}" scope="request" />
    <ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>
<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryCompletedWires"/>
<ffi:removeProperty name="GetReportsByCategoryFailedWires"/>
<ffi:removeProperty name="GetReportsByCategoryInProcWires"/>
<ffi:removeProperty name="GetReportsByCategoryWireByStatus"/>
<ffi:removeProperty name="GetReportsByCategoryWireBySource"/>
<ffi:removeProperty name="GetReportsByCategoryWireByTempl"/>
<ffi:removeProperty name="GetReportsByCategoryWireTemplates"/>
