<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>

<ffi:help id="reports_cash_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
							<%-- <td width="15%" align="center"><span class="sectionsubhead"><s:text name="jsp.home.column.label.action"/></span></td> --%>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MGMT_REPORTING %>" >

<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>


			
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryAnyDataClassification}" value2="false" operator="equals">
	<tr>
		<td colspan="2" align="center">
			<s:text name="jsp.reports_679"/>
		</td>
	</tr>
</ffi:cinclude> <%-- end include on report NOT entitled to detail or summary for either intra day or previous day --%>

<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryAnyDataClassification}" value2="true" operator="equals">


<%-- <tr height="1">
	<td width="40%" height="1">&nbsp;</td>
	<td width="60%" height="1">&nbsp;</td>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_87')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
</tr> --%>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_87')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
<%
	HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_CASH_FLOW %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="equals">
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetCashFlowRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetCashFlowRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetCashFlowRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetCashFlowRpt" property="ReportsIDsName" value="ReportsCashFlow"/>
	<ffi:setProperty name="GetCashFlowRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateCashFlowRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateCashFlowRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateCashFlowRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateCashFlowRpt" property="ReportsIDsName" value="ReportsCashFlow"/>
	<ffi:setProperty name="GenerateCashFlowRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_CASH_FLOW;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.default_87')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_CASH_FLOW_FORECAST %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="equals">
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetCashFlowForecastRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetCashFlowForecastRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetCashFlowForecastRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetCashFlowForecastRpt" property="ReportsIDsName" value="ReportsCashFlowFore"/>
	<ffi:setProperty name="GetCashFlowForecastRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateCashFlowForecastRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateCashFlowForecastRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateCashFlowForecastRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateCashFlowForecastRpt" property="ReportsIDsName" value="ReportsCashFlowFore"/>
	<ffi:setProperty name="GenerateCashFlowForecastRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_CASH_FLOW_FORECAST;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Cash_Flow_Forecast')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_BALANCE_SHEET_SUM %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="equals">
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetBalanceSheetSummaryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetBalanceSheetSummaryRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetBalanceSheetSummaryRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetBalanceSheetSummaryRpt" property="ReportsIDsName" value="ReportsBalSheetSum"/>
	<ffi:setProperty name="GetBalanceSheetSummaryRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateBalanceSheetSummaryRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateBalanceSheetSummaryRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateBalanceSheetSummaryRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateBalanceSheetSummaryRpt" property="ReportsIDsName" value="ReportsBalSheetSum"/>
	<ffi:setProperty name="GenerateBalanceSheetSummaryRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_BALANCE_SHEET_SUM;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Balance_Sheet_Summary')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_BALANCE_SHEET %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="equals">
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetBalanceSheetRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetBalanceSheetRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetBalanceSheetRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetBalanceSheetRpt" property="ReportsIDsName" value="ReportsBalSheet"/>
	<ffi:setProperty name="GetBalanceSheetRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateBalanceSheetRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateBalanceSheetRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateBalanceSheetRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateBalanceSheetRpt" property="ReportsIDsName" value="ReportsBalSheet"/>
	<ffi:setProperty name="GenerateBalanceSheetRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_BALANCE_SHEET;
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Balance_Sheet_Detail')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />

</ffi:cinclude>
</ffi:cinclude>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_GENERAL_LEDGER %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}" value2="true" operator="equals">
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetGeneralLedgerRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetGeneralLedgerRpt" property="Target" value="${SecurePath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetGeneralLedgerRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetGeneralLedgerRpt" property="ReportsIDsName" value="ReportsGenLedger"/>
	<ffi:setProperty name="GetGeneralLedgerRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GenerateGeneralLedgerRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GenerateGeneralLedgerRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GenerateGeneralLedgerRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GenerateGeneralLedgerRpt" property="ReportsIDsName" value="ReportsGenLedger"/>
	<ffi:setProperty name="GenerateGeneralLedgerRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category = com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_GENERAL_LEDGER; 
	session.setAttribute("category", category);
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.General_Ledger')}" scope="request" />
	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
</ffi:cinclude>
</ffi:cinclude>

</ffi:cinclude> <%-- End is entitled to detail or summary on intra day or previous day --%>
</ffi:cinclude> <%-- End is entitled to management (information) reporting --%>

</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>

<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryCashFlow"/>
<ffi:removeProperty name="GetReportsByCategoryCashFlowFore"/>
<ffi:removeProperty name="GetReportsByCategoryBalSheetSum"/>
<ffi:removeProperty name="GetReportsByCategoryBalSheet"/>
<ffi:removeProperty name="GetReportsByCategoryGenLedger"/>

