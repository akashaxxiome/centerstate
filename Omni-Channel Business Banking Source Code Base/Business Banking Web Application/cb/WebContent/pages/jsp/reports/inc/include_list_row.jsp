<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Enumeration"%>
<tr>
	<%
		String formName = (String)session.getAttribute("category");
		session.setAttribute("formName", formName.replace(" ", "_") + "Form");
		session.setAttribute("reportTypeID", formName.replace("-", "").replace(" ", ""));  //the id indicates the specific report of the reports list.
	%>
	<ffi:setProperty name="disableEdit" value=""/>
		<ffi:setProperty name="disableClick" value="ns.report.go('${formName}')" />
		<ffi:setProperty name="disableEditClick" value="ns.report.editReport('${formName}')" />
		<ffi:setProperty name="disableDeleteClick" value="ns.report.deleteReport('${formName}')" />
		<ffi:setProperty name="disableStyle" value="ui-button ui-state-default" />
		<ffi:cinclude value1="${reportNamesSize}" value2="0" operator="equals">
			<ffi:setProperty name="disableEdit" value="disabled"/>
			<ffi:setProperty name="disableClick"  value="{return false;}" />
			<ffi:setProperty name="disableEditClick"  value="{return false;}" />
			<ffi:setProperty name="disableDeleteClick"  value="{return false;}" />
			<ffi:setProperty name="disableStyle" value="ui-button ui-state-default ui-state-disabled" />
	</ffi:cinclude>
	
	
		<%-- <td class="tbrd_l " nowrap align="left"><span class="columndata"> 
			<ffi:getProperty name="TypeColumnValue" encode="false" /></span></td> 
			<ffi:setProperty name="TypeColumnValue" value="" /> --%>

		
		<td class="columndata"  width="40%"><s:url id="new_report_url"
			value="/pages/jsp/reports/NewReportBaseAction.action" escapeAmp="false">
			<s:param name="reportName" value="%{#session.category}" />
			<s:param name="criteriaPage" value="%{'Standard'}" />
			<s:param name="reportTopMenuId" value="%{#session.reportTopMenuId}" />
			<s:param name="reportTypeID" value="%{#session.reportTypeID}" />
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
		</s:url><sj:a href="%{new_report_url}" targets="targetRptCriteriaDiv"
		    id="%{#session.reportTypeID}"
			button="false" buttonIcon="ui-icon-gear"
			onClickTopics="clickOnReportTitle"
			onBeforeTopics="clickOnReportTitleBefore"
			onCompleteTopics="clickOnReportTitleComplete"
			onSuccessTopics="clickOnReportTitleSuccess"
			onErrorTopics="clickOnReportTitleError" 
			cssClass="anchorText">
			<!--L10NStart--><ffi:getProperty name="rowTitle" /><!--L10NEnd-->
		</sj:a></td>
		
		
		<!-- <td class="tbrd_l ">33&nbsp;</td> -->
		<td class="columndata" width="60%">
		<s:form id="%{#session.formName}" action="GetReportFromNameAction_edit"
		namespace="/pages/jsp/reports" method="post" theme="simple"
		name="%{#session.formName}">
		<input type="hidden" name="CSRF_TOKEN"	value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		<input type="hidden" name="category"	value="<ffi:getProperty name="category"/>">
		<input type="hidden" name="reportTopMenuId"	value="<ffi:getProperty name="reportTopMenuId"/>">
		<input type="hidden" name="reportTypeID"	value="<ffi:getProperty name="reportTypeID"/>">
		
		<script>
			$("#<%= session.getAttribute("formName") %>_select").selectmenu({width: "300px"});
		</script>
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<!-- the SELECT in the 'Balance Detail Report' line -->
				<td><select
						id="<%= session.getAttribute("formName") %>_select"
						class="txtbox"
						name="reportName" 
						size="1"
						<ffi:getProperty name="disableEdit"/>
						>
						<ffi:cinclude value1="${reportNamesSize}" value2="0"
							operator="equals">
							<option><s:text name="jsp.reports_681"/></option>
						</ffi:cinclude>
		<ffi:object id="GetReportCriteriaCheck" name="com.ffusion.tasks.reporting.GetReportCriteria"/>
		<ffi:setProperty name="GetReportCriteriaCheck" property="IdentificationsName" value="reportNames"/>
		<ffi:setProperty name="GetReportCriteriaCheck" property="ReportName" value="ReportCheck"/>

					<!-- <option value="0">Select Report</option> -->
						<ffi:list collection="reportNames" items="Report_BD">
				<ffi:setProperty name="GetReportCriteriaCheck" property="ID" value="${Report_BD.ReportID}"/>
				<ffi:process name="GetReportCriteriaCheck" />

							<option
								format="<ffi:getProperty property="ReportCriteria.ReportOptions.FORMAT" name="ReportCheck"/>"
								dest="<ffi:getProperty property="ReportCriteria.ReportOptions.DESTINATION" name="ReportCheck"/>"
								value="<ffi:getProperty property="ReportName" name="Report_BD"/>"><ffi:getProperty
								name="Report_BD" property="ReportName" /></option>
						</ffi:list>
						<ffi:removeProperty name="reportNames" />
						<ffi:removeProperty name="GetReportCriteriaCheck" />
					</select></td>
				<td><span class="columndata">&nbsp;&nbsp;&nbsp;</span></td>

				<!-- the GO button in the 'Balance Detail Report' line -->
				<td>
				<span>
					<a id="<%= session.getAttribute("formName") %>_btnGo" title='<s:text name="jsp.reports_593" />' href='#'
					class="<ffi:getProperty name="disableStyle" /> anchotBtn"
					onClick="<ffi:getProperty name="disableClick" />"> <span
					class="ui-button-text">&nbsp;&nbsp;<s:text name="jsp.reports_593" />&nbsp;&nbsp;</span> </a>
				</span>
					&nbsp;
					<a id="<%= session.getAttribute("formName") %>_btnEdit"
						class="<ffi:getProperty name="disableStyle" /> anchotBtn" title='<s:text name="jsp.default_179" />' href='#'
						onClick="<ffi:getProperty name="disableEditClick" />"><span
					class="ui-button-text">&nbsp;&nbsp;<s:text name="jsp.default_178"/>&nbsp;&nbsp;</span> </a> 
					&nbsp;
					<a  id="<%= session.getAttribute("formName") %>_btnDelete"
						class="<ffi:getProperty name="disableStyle" /> anchotBtn" title='<s:text name="jsp.default_163" />'
						href='#' onClick="<ffi:getProperty name="disableDeleteClick" />"><span
					class="ui-button-text">&nbsp;&nbsp; <s:text name="jsp.default_163"/>&nbsp;&nbsp;</span></a>	
							
				</td>
			</tr>
		</table>
	</s:form>
		</td>
		<!-- the EDIT & DELETE button in the 'Balance Detail Report' line -->
		<%-- <td align="center" class="columndata"  width="15%">
			<a id="<%= session.getAttribute("formName") %>_btnEdit"
				class="<ffi:getProperty name="disableStyle" />" title='<s:text name="jsp.default_179" />' href='#'
				onClick="<ffi:getProperty name="disableEditClick" />"> <s:text name="jsp.default_178"/> </a> 
		
			<a  id="<%= session.getAttribute("formName") %>_btnDelete"
				class="<ffi:getProperty name="disableStyle" />" title='<s:text name="jsp.default_163" />'
				href='#' onClick="<ffi:getProperty name="disableDeleteClick" />"><s:text name="jsp.default_163"/></a>
	 </td> --%>
		<%-- <td class="tbrd_r "><s:url id="new_report_url"
			value="/pages/jsp/reports/NewReportBaseAction.action" escapeAmp="false">
			<s:param name="reportName" value="%{#session.category}" />
			<s:param name="criteriaPage" value="%{'Custom'}" />
			<s:param name="reportTopMenuId" value="%{#session.reportTopMenuId}" />
			<s:param name="reportTypeID" value="%{#session.reportTypeID}" />
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
		</s:url> <sj:a href="%{new_report_url}" targets="targetRptCriteriaDiv"
            id="%{#session.reportTypeID}_adv"
			button="false" buttonIcon="ui-icon-gear"
			onClickTopics="clickOnReportTitle"
			invokerlink="false"
			onBeforeTopics="clickOnReportTitleBefore"
			onCompleteTopics="clickOnReportTitleComplete"
			onSuccessTopics="clickOnReportTitleSuccess"
			onErrorTopics="clickOnReportTitleError"><s:text name="jsp.reports_617"/></sj:a></td> --%>
</tr>
