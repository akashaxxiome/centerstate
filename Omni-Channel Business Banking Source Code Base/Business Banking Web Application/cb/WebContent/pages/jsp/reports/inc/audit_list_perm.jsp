<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>

<ffi:help id="reports_audit_list" />
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="marginTop10">
<tr>
	<td align="center"><s:actionerror /></td>
</tr>
<tr>
	<td>
		<div class="paneWrapper">
		   	<div class="paneInnerWrapper">
				<div class="header">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.create.new.report"/></span></td>
							<td width="60%"><span class="sectionsubhead">&nbsp;&nbsp;&nbsp;<s:text name="jsp.load.saved.report"/></span></td>
						</tr>
				</table>
			</div>
			<%
			if( session.getAttribute("ReportNamesAndCategories")==null ) {
			%>
				<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
				<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
				<ffi:process name="GetHashmapInSession"/>
				<ffi:removeProperty name="GetHashmapInSession"/>
			<%
			}
			%>
			<div class="paneContentWrapper">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr height="1">
	<td width="40%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="150" height="1" border="0"></td>
	<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="380" height="1" border="0"></td>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports_630')}" scope="request" /><ffi:setProperty name="TypeColumnValue" value="${tmpI18nStr}"/>
</tr>

<ffi:object name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" id="CanAdministerAnyGroup" scope="session" />
<ffi:process name="CanAdministerAnyGroup"/>
<ffi:removeProperty name="CanAdministerAnyGroup"/>

<%
	String adminUser = (String)session.getAttribute("Entitlement_CanAdministerAnyGroup");
	HashMap hashmap = (HashMap)(session.getAttribute("ReportNamesAndCategories"));
	BusinessEmployee businessEmployee = (BusinessEmployee)session.getAttribute( com.ffusion.tasks.Task.BUSINESS_EMPLOYEE );
	session.setAttribute("BusinessEmployee", businessEmployee);
%>
<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>

<s:if test="%{#session.Entitlement_CanAdministerAnyGroup == 'TRUE' && #session.BusinessEmployee.UsingEntProfiles}">
	<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_PROFILE_ENTITLEMENT %>"/>
</s:if>
<s:else>
	<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%= com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_USER_ENTITLEMENT %>"/>
</s:else>
	
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
	
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">

	<ffi:object id="GetPermissionRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GetPermissionRpt" property="Target" value="${FullPagesPath}reports/reportcriteria.jsp"/>
	<ffi:setProperty name="GetPermissionRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GetPermissionRpt" property="ReportsIDsName" value="ReportsPerm"/>
	<ffi:setProperty name="GetPermissionRpt" property="ReportName" value="ReportData"/>
	
	<ffi:object id="GeneratePermissionRpt" name="com.ffusion.tasks.reporting.GetReportFromName"/>
	<ffi:setProperty name="GeneratePermissionRpt" property="SuccessURL" value="${SecureServletPath}GenerateReportBase"/>
	<ffi:setProperty name="GeneratePermissionRpt" property="IDInSessionName" value="reportID" />
	<ffi:setProperty name="GeneratePermissionRpt" property="ReportsIDsName" value="ReportsPerm"/>
	<ffi:setProperty name="GeneratePermissionRpt" property="ReportName" value="ReportData"/>
	
	<%
	String category =null;
	
	if(businessEmployee!=null && businessEmployee.getUsingEntProfiles()=="true" && adminUser!=null && adminUser.equalsIgnoreCase("TRUE"))
				category = com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_PROFILE_ENTITLEMENT;
			else
				category = com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_USER_ENTITLEMENT;
			
			session.setAttribute("category", category);
	
	ArrayList reportNames = (ArrayList)(hashmap.get(category));
	int reportNamesSize = 0; if( reportNames != null) reportNamesSize = reportNames.size(); else reportNames = new ArrayList();
	session.setAttribute("reportNamesSize", String.valueOf(reportNamesSize));
	session.setAttribute("reportNames", reportNames);
	%>

	<s:if test="%{#session.Entitlement_CanAdministerAnyGroup == 'TRUE' && #session.BusinessEmployee.UsingEntProfiles}">
		<s:set var="tmpI18nStr" value="%{getText('jsp.reports.Profile_Permission')}" scope="request" />
	</s:if>
	<s:else>
	<s:set var="tmpI18nStr" value="%{getText('jsp.reports.User_Permission')}" scope="request" />
	</s:else>

	<ffi:setProperty name="rowTitle" value="${tmpI18nStr}" />
	<s:include	value="/pages/jsp/reports/inc/include_list_row.jsp" />
	
</ffi:cinclude>
</table>
			</div>
		</div>
	</div>		
</td>
</tr>
</table>
<ffi:removeProperty name="CheckReportEntitlement"/>

<%-- SESSION CLEANUP --%>
<ffi:removeProperty name="GetReportsByCategoryPerm"/>