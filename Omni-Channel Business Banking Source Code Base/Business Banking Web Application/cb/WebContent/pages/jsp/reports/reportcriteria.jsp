<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:if test="%{criteriaPage!='Custom'}">
    <span class="shortcutPathClass" style="display:none;"><s:property value="#parameters['reportTopMenuId']"/>,<s:property value="#parameters['reportTypeID']"/></span>
</s:if>
<s:if test="%{criteriaPage=='Custom'}">
    <span class="shortcutPathClass" style="display:none;"><s:property value="#parameters['reportTopMenuId']"/>,<s:property value="#parameters['reportTypeID']"/>_adv</span>
</s:if>

<%-- The LastIntradayTransactionViewDateForExport is used to ensure that when the last intraday option is selected, the exported report's data matches the data on the screen.  If we get to this page, then we need to clear this variable, as we are seeking to run a New Report --%>
<ffi:removeProperty name="LastIntradayTransactionViewDateForExport"/>


<%String requestId = String.valueOf(System.currentTimeMillis()); %>
<ffi:setProperty name="PageScope_RequestId" value="<%=requestId%>" />


<%--  Determine if we are displaying a saved report or not.  The only way that we can
	  know for sure if the report has been saved successfully is by looking at the
	  report ID.  --%>

<%-- Set attributes needed for reportsaved.jsp --%>
<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="equals">
	<ffi:setProperty name="edit" value="FALSE"/>
</ffi:cinclude>
<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="notEquals">
	<ffi:setProperty name="criteriaPage" value="Custom"/>
	<ffi:setProperty name="edit" value="TRUE"/>
	<ffi:setProperty name="curr_rptID" value="${ReportData.ReportID.ReportID}"/>
	<ffi:setProperty name="curr_rptName" value="${reportsName}"/>
	<ffi:setProperty name="reportKey" value="ReportData"/>
</ffi:cinclude>

<script type="text/javascript"><!--
	ns.report.masterValidate = function( formName ) {
		removeValidationErrors();

		if( !ns.report.validateForm( formName ) ) return false;
	
		if( !ns.report.validateDate( formName ) ) return false;
	
		ns.report.updateDestination();
	
		ns.report.updateSortAsc();
	
		return true;
	
	}
	
	ns.report.validateForm = function( formName ) {
		return true;
	}
	
	ns.report.validateDate = function( formName ) {

		var form = document.forms[formName];
		obj = form["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"];
		if(!obj) return true; //no date criteria at all
		if( obj[0].checked )
		{
		    startDateStr = form.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_DATE %>.value;
		    endDateStr = form.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_DATE %>.value;

		    if ( !ns.report.validateDateRange( startDateStr, endDateStr ) )
			    return false;

		<ffi:cinclude value1="${DisplayTime}" value2="TRUE" operator="equals">		
		    startTimeStr = form.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_START_TIME %>.value;
		    if ( !validateTime( startTimeStr, "Start" ) )
			    return false;
		    endTimeStr = form.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_END_TIME %>.value;
		    if ( !validateTime( endTimeStr, "End" ) )
			    return false;
		</ffi:cinclude>

		}

		if( obj[1].checked )
		{
		    dateRangeStr = form["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.SEARCH_CRIT_PREFIX %><%= com.ffusion.beans.reporting.ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"].value;
		    if( dateRangeStr == "" )
		    {
				ns.report.showDateError('relDateRangeError',js_invalid_date_range);
			    return false;
		    }
		}

		return true;
	}
	
	ns.report.updateDestination = function() {
		
		if( document.criteriaFormName[ "destination" ].value == "<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>" )
		{
		    document.criteriaFormName[ "<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" ].value = "<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>";
		}

		if( document.criteriaFormName[ "destination" ].value == "<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_HTTP %>"
		    && document.criteriaFormName[ "<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>" ].value != "<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" )
		{
		    document.criteriaFormName[ "<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" ].value = "<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_HTTP %>";
		}

		if( document.criteriaFormName[ "destination" ].value == "<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_HTTP %>"
		    && document.criteriaFormName[ "<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>" ].value == "<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" )
		{
		    document.criteriaFormName[ "<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" ].value = "<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>";
		}
	}

	ns.report.updateSortAsc = function() {
    }
	
	ns.report.resetAction = function(e) {
	    
		if (e.target) {
			if (e.target.id == "GenerateReport") {
			    document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecureServletPath}GenerateReportBase${PageScope_RequestId}" />';		    
			} else if (e.target.id == "CustomReporting") {
			    document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Custom" />';
			} else if (e.target.id == "SaveReport") {
				<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="equals">
			    	document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecurePath}reports/savereport.jsp" />';
				</ffi:cinclude>
				<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="notEquals">
			    	document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecurePath}reports/reportsaved.jsp" />';
				</ffi:cinclude>
			} else if (e.target.id == "BankReportingSelectAllAccounts") {
		    	// Don't do anything
			} else {
		    	document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecurePath}reports/reportcriteria.jsp" />';
			}
	    } else if (e.srcElement) {
		    
			if (e.srcElement.id == "GenerateReport") {
			    document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecureServletPath}GenerateReportBase${PageScope_RequestId}" />';
			} else if (e.srcElement.id == "CustomReporting") {
			    document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecurePath}reports/reportcriteria.jsp&criteriaPage=Custom" />';
			} else if (e.srcElement.id == "SaveReport") {
				<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="equals">
					document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecurePath}reports/savereport.jsp" />';
				</ffi:cinclude>
				<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="notEquals">
					document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecurePath}reports/reportsaved.jsp" />';
				</ffi:cinclude>
			} else if (e.srcElement.id == "BankReportingSelectAllAccounts") {
			    // Don't do anything
			} else {
			    document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecurePath}reports/reportcriteria.jsp" />';
			}
		} else {
		    document.forms["criteriaFormName"].action = '<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecurePath}reports/reportcriteria.jsp" />';
		}
	}
	//-->
    </script>
 	
 	<div align="left" id="reportCriteriaDivContent">
		<s:form action="UpdateReportCriteria_input" namespace="/pages/jsp/reports" method="post" theme="simple" id="criteriaForm" name="criteriaFormName" onclick="ns.report.resetAction(event);">
           	<s:actionerror />
			<s:hidden key="criteriaPage"/>
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DATE_FORMAT %>" value='<ffi:getProperty name="UserLocale" property="DateFormat"/>'>
			<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_TIME_FORMAT %>" value='<ffi:getProperty name="UserLocale" property="TimeFormat"/>'>
			<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.user.UserLocale.LANGUAGE %>" value='<ffi:getProperty name="UserLocale" property="Language"/>'>
			<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.user.UserLocale.COUNTRY %>" value='<ffi:getProperty name="UserLocale" property="Country"/>'>
			<input type="hidden" name="reportTopMenuId" id="reportTopMenuId" value="<s:property value="#parameters['reportTopMenuId']"/>">
			<input type="hidden" name="reportTypeID" id="reportTypeID" value="<s:property value="#parameters['reportTypeID']"/>">
			
	

			<table width="100%" border="0" cellspacing="5" cellpadding="3">
				<tr>
					<td class="tbrd_b" align="left" colspan="4" nowrap><span class="sectionhead">

						<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>"/>

						<ffi:object id="ReportResource" name="com.ffusion.tasks.util.Resource"/>
						<ffi:setProperty name="ReportResource" property="ResourceFilename" value="com.ffusion.beansresources.reporting.reports" />
						<ffi:process name="ReportResource" />

						<% String reportDisplayName; %>
						<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" assignTo="reportDisplayName"/>
						<%
							// Replace all the spaces in the report type and criteria name with underscores
							StringBuffer reportTypeBuf = new StringBuffer( reportDisplayName );
							for( int i = 0; i < reportTypeBuf.length(); i++ ) {
							    char curChar = reportTypeBuf.charAt( i );

								if( curChar == ' ' ) {
									reportTypeBuf.setCharAt( i, '_' );
								}
							}

							String key = reportTypeBuf.toString();
							session.setAttribute( "key", key );
						%>
						<ffi:setProperty name="ReportResource" property="ResourceID" value="ReportName_${key}"/>

						<ffi:cinclude value1="${edit}" value2="TRUE" operator="equals">
							<s:text name="jsp.default_179"/> <ffi:getProperty name="ReportResource" property="Resource"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${edit}" value2="TRUE" operator="notEquals">
							<ffi:getProperty name="ReportResource" property="Resource"/>
						</ffi:cinclude>
					</span></td>
				</tr>
				<tr>
					<td colspan="4"><img src="/cb/web/multilang/grafx/spacer.gif" width="120" height="1"></td>
				</tr>
				<ffi:cinclude value1="${edit}" value2="TRUE" operator="equals">
					<tr>
						<td align="right" class="sectionsubhead"><s:text name="jsp.default_283"/></td>
						<td align="left">
							<input class="ui-widget-content ui-corner-all" type="text" name="name" size="40" maxlength="40" border="0" value="<ffi:getProperty name="ReportData" property="ReportID.ReportName"/>">
						</td>
					</tr>
					<tr>
						<td align="right" class="sectionsubhead"><s:text name="jsp.default_170"/></td>
						<td align="left">
							<input class="ui-widget-content ui-corner-all" type="text" name="description" size="40" maxlength="255" border="0" value="<ffi:getProperty name="ReportData" property="ReportID.Description"/>">
						</td>
					</tr>
				</ffi:cinclude>
				
				<s:if test="%{criteriaPage=='Standard'}">
					<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_STANDARD_CRIT_PAGE %>"/>
				</s:if>
				<s:if test="%{criteriaPage!='Standard'}">
					<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_CUSTOM_CRIT_PAGE %>"/>
				</s:if>
				<%-- Include page --%>
				<s:include value="%{#session.PagesPath}reports/criteria/%{#session.ReportData.ReportCriteria.CurrentReportOptionValue}"/>
				
				
<script type="text/javascript">

	ns.report.enableOrientation = function() {
		 if ((document.criteriaFormName.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %> != undefined)
				&& (document.criteriaFormName.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>.value 
				== "<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>")) 
		 {
			$('#report_orientation').removeAttr('disabled');
			$("#report_orientation").selectmenu('enable');
		} else {
			$('#report_orientation').attr('disabled','true');
			$("#report_orientation").selectmenu('disable');
		}
	};

	function calculateHeight() {
		popupWindowHeight = 320;
		<ffi:cinclude value1="${ShowColumnHeaderOption}" value2="TRUE" operator="equals">
			popupWindowHeight += 70;
		</ffi:cinclude>
		
		if ( document.criteriaFormName.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>.value == "<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" ) {
			popupWindowHeight += 100;
		} else if ( document.criteriaFormName.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>.value == "<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>" ) {
			<ffi:cinclude value1="${ReportData.ShowPrintHeaderOption}" value2="TRUE" operator="equals">
				popupWindowHeight += 25;
			</ffi:cinclude>
		} else if ( document.criteriaFormName.<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>.value == "<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" ) {
            popupWindowHeight += 25; // add to Show Bank Logo option
		}
	}

	$(document).ready(function(){
		var selectWidth = "160px";
		var defaultselectWidth = "160px";
		$("#reportCriteriaDivContent").find('select').each(function(){
			
			if( $(this).attr('multiple') == 'multiple')
			{			
				$(this).addClass('ui-widget-content').addClass('ui-corner-all').removeClass('txtbox');
				return;
			}			
			selectWidth = $(this).css("width")=='auto'?"160px":$(this).css("width");			
			selectWidth = $(this).attr("width")?$(this).attr("width"):selectWidth;
			
			//if the with attribute or style is not defined then set default width
			if($(this).width() === 0){
				selectWidth = defaultselectWidth;
			}
			$(this).selectmenu({escapeHtml:true, width: selectWidth});

		});

		ns.report.enableOrientation();
	});

</script>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
				<tr>
					<td width="20%" align="right" class="sectionsubhead"><s:text name="jsp.reports_585"/>&nbsp;</td>
					<td width="80%" align="left" colspan="3">
						<% String curFormat; %>
						<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>" />
						<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" assignTo="curFormat"/>
		
						<select class="txtbox" style="width:160px" id="report_format" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>" size="1" onChange="ns.report.enableOrientation();">
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
								<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" value2="${curFormat}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.default_231"/>
								</option>
							</ffi:cinclude>
		
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
								<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>" value2="${curFormat}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.default_105"/>
								</option>
							</ffi:cinclude>
		
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
								<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>" value2="${curFormat}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.default_402"/>
								</option>
							</ffi:cinclude>
		
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
								<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" value2="${curFormat}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.default_323"/>
								</option>
							</ffi:cinclude>
		
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
								<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>" value2="${curFormat}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.default_59"/>
								</option>
							</ffi:cinclude>
		
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
								<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>" value2="${curFormat}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.default_325"/>
								</option>
							</ffi:cinclude>
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>" />
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="true" operator="equals">
								<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>" value2="${curFormat}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.default_124"/>
								</option>
							</ffi:cinclude>
						</select>
					<%-- </td>
					<td width="10%" align="right" class="sectionsubhead"><s:text name="jsp.reports_625"/>&nbsp;</td>
					<td width="40%" align="left"> --%>
						<span class="reportsCriteriaContent"><s:text name="jsp.reports_625"/>&nbsp;</span>
						<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_ORIENTATION %>" />
						<select class="txtbox" style="width: 160px;" id="report_orientation" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_ORIENTATION %>" size="1" >
							<option value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_PORTRAIT %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_PORTRAIT %>" value2="${ReportData.ReportCriteria.CurrentReportOptionValue}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.reports_632"/>
							</option>
		
							<option value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_LANDSCAPE %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_LANDSCAPE %>" value2="${ReportData.ReportCriteria.CurrentReportOptionValue}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.reports_605"/>
							</option>
						</select>
					</td>
				</tr>
				<tr>
					<td align="right" class="sectionsubhead"><s:text name="jsp.reports_564"/>&nbsp;</td>
					<td align="left" colspan="3">
						<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" />
						<select class="txtbox" style="width:160px;" id="report_rptcrtr_dest_list" name="destination" size="1" onchange="ns.report.updateDestination();">
							<option value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_HTTP %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>" value2="${ReportData.ReportCriteria.CurrentReportOptionValue}" operator="equals">selected</ffi:cinclude>
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_HTTP %>" value2="${ReportData.ReportCriteria.CurrentReportOptionValue}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.reports_571"/>
							</option>
							<option value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>"
									<ffi:cinclude value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>" value2="${ReportData.ReportCriteria.CurrentReportOptionValue}" operator="equals">selected</ffi:cinclude> >
									<s:text name="jsp.reports_578"/>
							</option>
						</select>
						
						<input type="hidden"
							name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>"
						    <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="notEquals">
							value="<ffi:getProperty name='ReportData' property='ReportCriteria.CurrentReportOptionValue'/>">
						    </ffi:cinclude>
						    <ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
							value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_HTTP %>">
						    </ffi:cinclude>
						&nbsp;
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
					<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" >
					<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_ORIENTATION %>" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_PORTRAIT %>" >
					<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>" >
					<input type="hidden" name="destination" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_HTTP %>" >
			</ffi:cinclude>
				<tr>
					<td></td>
					<td align="left" colspan="3">
					</td>
				</tr>
				<tr>
					<td></td>
					<td align="left" colspan="3">
						<a onclick="ns.report.setHeaderFooter();" href="#" csclick="B9E5DF3349"><img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/button_reportheadfoot.gif" alt="" width="158" border="0"></a>
					</td>
				</tr>
		
				<tr>
					<td colspan="4"><img src="/cb/web/multilang/grafx/spacer.gif" width="100" height="20"></td>
				</tr>
				<tr class="sectionsubhead">
					<td align="right">&nbsp;</td>
					<td align="left" nowrap colspan="4">
							<sj:a button="true" 
						          onClickTopics="cancelReportForm"					         
						          ><s:text name="jsp.default_82"/></sj:a>
							<sj:a
								onclick="ns.report.generateReport();"
								button="true"
								><s:text name="jsp.default_224"/></sj:a>

						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="equals">
							<sj:a 
								button="true"
								onclick="ns.report.saveReport();"
								><s:text name="jsp.reports_658"/></sj:a>
						</ffi:cinclude>

						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="notEquals">
							<sj:a 
								button="true"
								onclick="ns.report.modifyReport();"
								><s:text name="jsp.reports_613"/></sj:a>
						</ffi:cinclude>
						
						<s:if test="%{criteriaPage=='Standard'}">
							<%
								String stdPage;
							%>
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_STANDARD_CRIT_PAGE %>"/>
							<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" assignTo="stdPage"/>
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_CUSTOM_CRIT_PAGE %>"/>
							<ffi:cinclude value1="${stdPage}" value2="${ReportData.ReportCriteria.CurrentReportOptionValue}" operator="notEquals">
								<sj:a 
									button="true"
									onclick="ns.report.customReport();"
									><s:text name="jsp.reports_548"/></sj:a>	
							</ffi:cinclude>
						</s:if>
					</td>
				</tr>
			</table>
		</s:form>
	</div>
<ffi:setProperty name="BackURL" value="${SecurePath}reports/reportcriteria.jsp"/>
<ffi:removeProperty name="PageScope_RequestId" />
