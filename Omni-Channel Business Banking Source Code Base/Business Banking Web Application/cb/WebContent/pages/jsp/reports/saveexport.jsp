<%@ page import="com.ffusion.beans.ach.ACHReportConsts"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

		<table width="750" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><img src="/cb/web/multilang/grafx/corner_al.gif" alt="" width="11" height="7" border="0"></td>
				<td class="tbrd_t dkback" colspan="3"><img src="/cb/web/multilang/grafx/spacer.gif" width="728" height="6" border="0"></td>
				<td><img src="/cb/web/multilang/grafx/corner_ar.gif" alt="" width="11" height="7" border="0"></td>
			</tr>
			<tr>
				<td background="/cb/web/multilang/grafx/corner_cellbg.gif"><br><br></td>
				<td class="dkback" width="10">&nbsp;</td>
				<td valign="top" width="708" align="left" class="dkback">
					<table width="98%" border="0" cellspacing="0" cellpadding="0">
					<tr>
<%-- set it to false as we don't want to display the form here in single page app --%>
<% boolean canSaveReport = false; boolean showDone = false; 
if (canSaveReport) { %>
						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="equals">
							<td class="columndata" align="left" nowrap colspan="2"><span class="sectionhead"><s:text name="jsp.default_370"/></span></td>
						</ffi:cinclude>
						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="notEquals">
							<td class="columndata" align="left" nowrap colspan="2"><span class="sectionhead"><s:text name="jsp.default_180"/></span></td>
						</ffi:cinclude>
<% } %>
						<td>&nbsp;</td>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
							<td><img src="/cb/web/multilang/grafx/spacer.gif" width="100" height="2" border="0"></td>
							<td class="columndata" align="left" nowrap><span class="sectionhead"><s:text name="jsp.default_198"/></span></td>
							<td></td>
						</ffi:cinclude>
						<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
							<td colspan=3>&nbsp;</td>
						</ffi:cinclude>
					</tr>
					<tr>
<% if (canSaveReport) { %>
						<td class="sectionsubhead" align="left" nowrap colspan="2">
						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="equals">
							<form action="reportsaved.jsp" method="post" name="SaveFormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                <table>
                                    <tr>
                                        <td align="right">
                                            <span class="sectionsubhead"><s:text name="jsp.default_283"/>:&nbsp;</span>
                                        </td>
                                        <td>
                                            <input class="txtbox" type="text" name="ReportData.ReportID.ReportName" size="20" maxlength="40" value="<s:text name="jsp.default_353"/>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <span class="sectionsubhead"><s:text name="jsp.default_170"/>:&nbsp;</span>
                                        </td>


                                        <td nowrap>
											<input class="txtbox" type="text" name="ReportData.ReportID.Description" size="30" maxlength="255" value="<s:text name="jsp.default_170"/>">&nbsp;&nbsp;<input class="submitbutton" type="submit" value="<s:text name="jsp.default_366"/>" onclick="return validateName();">
                                        </td>
                                    </tr>
                                </table>
							</form>
						</ffi:cinclude>
						<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0" operator="notEquals">
							<form action="reportcriteria.jsp" method="post" name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                <input type="hidden" name="reportID" value="<ffi:getProperty name="ReportData" property="ReportID.ReportID"/>">
                                <input type="hidden" name="resetReportOptions" value="TRUE">
                                <table>
                                    <tr>
                                        <td align="right" valign="top"><span class="sectionsubhead"><s:text name="jsp.default_283"/>:&nbsp;</span></td>
                                        <td valign="top"><span style="padding: 0px;" class="columndata"><ffi:getProperty name="ReportData" property="ReportID.ReportName"/></span></td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><span class="sectionsubhead"><s:text name="jsp.default_170"/>:&nbsp;</span></td>
                                        <td valign="top"><span style="padding: 0px;" class="columndata"><ffi:getProperty name="ReportData" property="ReportID.Description"/></span></td>
                                    </tr>
                                </table>
							</form>
						</ffi:cinclude>
						<ffi:removeProperty name="mode"/>
						</td>
<% } %>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
						<td>&nbsp;</td>
						<%String requestId = String.valueOf(System.currentTimeMillis()); %>
						<ffi:setProperty name="PageScope_RequestId" value="<%=requestId%>" />
						<form action="<ffi:urlEncrypt url="${SecureServletPath}UpdateReportCriteria?UpdateReportCriteria.Target=${SecureServletPath}GenerateReportBase${PageScope_RequestId}" />" method="post" name="FormName">
						<ffi:removeProperty name="PageScope_RequestId" />
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<td>&nbsp;</td>
							<td align="left" nowrap><select class="txtbox" name="GenerateReportBase.Format">
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>">
										<s:text name="jsp.default_231"/>
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>">
										<s:text name="jsp.default_105"/>
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>">
										<s:text name="jsp.default_402"/>
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>">
										<s:text name="jsp.default_323"/>
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>">
										<s:text name="jsp.default_59"/>
									</option>
								</ffi:cinclude>

								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>">
										<s:text name="jsp.default_325"/>
									</option>
								</ffi:cinclude>
								<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>" />
								<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="true" operator="equals">
									<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>">
										<s:text name="jsp.default_124"/>
									</option>
								</ffi:cinclude>
								</select>&nbsp;&nbsp;
<% if (canSaveReport) { %>
								</td>
							<td align="left" nowrap><input class="submitbutton" type="submit" name="Submit2" value="<s:text name="jsp.default_195"/>"></td>
<% } else { %>
							<input class="submitbutton" type="submit" name="Submit2" value="<s:text name="jsp.default_195"/>"></td>
<% } %>
							<input type="hidden" name="GenerateReportBase.Destination" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>">
						</form>
						</ffi:cinclude>
						<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
							<td colspan=3>&nbsp;</td>
						</ffi:cinclude>
					</tr>
					</table>
				</td>
				<td class="dkback" width="10">&nbsp;</td>
				<td align="right" background="/cb/web/multilang/grafx/corner_cellbg.gif"><br><br></td>
			</tr>
			<tr>
				<td><img src="/cb/web/multilang/grafx/corner_bl.gif" alt="" width="11" height="7" border="0"></td>
				<td class="tbrd_b dkback" colspan="3"><img src="/cb/web/multilang/grafx/spacer.gif" width="728" height="6" border="0"></td>
				<td><img src="/cb/web/multilang/grafx/corner_br.gif" alt="" width="11" height="7" border="0"></td>
			</tr>
		</table>
		<br>
		<form name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
            <% if (showDone) { %>
                <input class="submitbutton" type="button" name="back" value="<s:text name="jsp.default_175"/>" onClick="location.replace( '<ffi:urlEncrypt url="${SecurePath}redirect.jsp?target=${tmpBackURL}" />' )">
            <% } else { %>
                <input class="submitbutton" type="button" name="back" value="<s:text name="jsp.default_57"/>" onClick="location.replace( '<ffi:urlEncrypt url="${SecurePath}redirect.jsp?target=${tmpBackURL}" />' )">
            <% } %>         
		</form>