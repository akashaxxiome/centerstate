<%@ page import="com.ffusion.beans.ach.ACHReportConsts"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<ffi:help id="reports_displayreport" className="moduleHelpClass" />

<%-- If the report is not of destination object, then the report isn't meant for this JSP.
	 The user has probably run a saved export report and has navigating back to this page.
	 If that has happened, we should load the saved ReportData from session and use it, as
	 the current data won't display here --%>
<ffi:setProperty name="needReload" value="false" />
<ffi:setProperty name="ReportData"	property="ReportCriteria.CurrentReportOption"
	value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" />
<ffi:cinclude value1="${GenerateReportBase.LastDestination}" value2=""	operator="equals">
	<%-- GenerateReportBase.LastDestination is empty, so it will use the ReportCriteria destination --%>
	<ffi:cinclude
		value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>"
		value2="${ReportData.ReportCriteria.CurrentReportOptionValue}"
		operator="notEquals">
		<ffi:setProperty name="needReload" value="true" />
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${GenerateReportBase.LastDestination}" value2=""
	operator="notEquals">
	<%-- GenerateReportBase.LastDestination is not empty, so it will use its destination --%>
	<ffi:cinclude
		value1="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>"
		value2="${GenerateReportBase.LastDestination}" operator="notEquals">
		<ffi:setProperty name="needReload" value="true" />
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${needReload}" value2="true" operator="equals">
	<%
	com.ffusion.beans.reporting.Report report = (com.ffusion.beans.reporting.Report) session.getAttribute( "ReportDataSaved" );
	if( report != null ){
	    session.setAttribute( "ReportData", report );
	    //need to regenerate the data, since it has been removed
	%>
	<ffi:object id="GenerateSavedReportBase"
		name="com.ffusion.tasks.reporting.GenerateReportBase" />
	<ffi:setProperty name="ReportData"
		property="ReportCriteria.CurrentReportOption"
		value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>" />
	<ffi:setProperty name="GenerateSavedReportBase" property="Destination"
		value="${ReportData.ReportCriteria.CurrentReportOptionValue}" />
	<ffi:setProperty name="ReportData"
		property="ReportCriteria.CurrentReportOption"
		value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>" />
	<ffi:setProperty name="GenerateSavedReportBase" property="Format"
		value="${ReportData.ReportCriteria.CurrentReportOptionValue}" />
	<ffi:process name="GenerateSavedReportBase" />
	<%
	}
%>
</ffi:cinclude>
<ffi:removeProperty name="needReload" />

<%-- Currently the data source load start time, and data source load end time search criteria are used only internally
     by default, therefore, they will be hidden from the user --%>
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentSearchCriterion"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATASOURCE_LOAD_START_TIME%>" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.HiddenSearchCriterion" value="true" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentSearchCriterion"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATASOURCE_LOAD_END_TIME%>" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.HiddenSearchCriterion" value="true" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentSearchCriterion"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TIME_FORMAT%>" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.HiddenSearchCriterion" value="true" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentSearchCriterion"
	value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATE_FORMAT%>" />
<ffi:setProperty name="ReportData"
	property="ReportCriteria.HiddenSearchCriterion" value="true" />

<div align="center" id="rptDisplayMain">
<% String opt_pagewidth; %> 
<ffi:setProperty name="ReportData"
	property="ReportCriteria.CurrentReportOption"
	value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH %>" />
<ffi:getProperty name="ReportData"
	property="ReportCriteria.CurrentReportOptionValue"
	assignTo="opt_pagewidth" /> 
	<%
		String real_pagewidth = String.valueOf( Integer.parseInt( opt_pagewidth ) + 2 );

		session.setAttribute( "real_pagewidth", real_pagewidth );
	%>
	<ffi:object id="FFIUpdateReportCriteria" name="com.ffusion.tasks.reporting.UpdateReportCriteria"/>
	<ffi:setProperty name="FFIUpdateReportCriteria" property="ReportName" value="ReportData"/>

	

		<div align="center">		
		<iframe name="displayreport2_exportFrm" style="width:0px; height:0px; opacity:0"></iframe>
		<s:form id="innerExportReportForm2" name="generateExportForm" action="UpdateReportCriteriaAction_generateReport.action" method="post" theme="simple" target="_blank">
			<input type="hidden" name="GenerateReportBase.Destination" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS %>">
			<input type="hidden" name="CSRF_TOKEN"	value="<ffi:getProperty name='CSRF_TOKEN'/>" />
			<input type="hidden" name="returnjsp"   value="false" />
			<s:hidden key="reportID" />
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
				<p id="exportDialogMessage"><b><s:text name="jsp.reports_696"/></b></p>
			<select class="txtbox" id="GenerateReportBaseFormat" name="GenerateReportBase.Format">
				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option	value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"
						>HTML</option>
				</ffi:cinclude>
				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>"
						>Comma Delimited</option>
				</ffi:cinclude>
				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>"
					>Tab Delimited</option>
				</ffi:cinclude>

				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF %>"
					>PDF</option>
				</ffi:cinclude>

				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2 %>"
						>BAI2</option>
				</ffi:cinclude>

				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>"
						>Plain Text</option>
				</ffi:cinclude>
				<ffi:setProperty name="ReportData"
					property="ReportCriteria.CurrentReportOption"
					value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>" />
				<ffi:cinclude
					value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
					value2="true" operator="equals">
					<option value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>"
						>CSV Data</option>
				</ffi:cinclude>
			</select>
			</ffi:cinclude>
			<br />
			<br />
			
		</s:form>

		<%-- This button is jqueryfied by ns.common.jqueryfyImportReport in common.js --%>
		<sj:a id="anchor_displayreport2_done" href="#" button="true" onClickTopics="closeDialog,common.topics.reportDoneListener"><s:text name="jsp.default_175"></s:text></sj:a>		
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
		<%-- This button is jqueryfied by ns.common.jqueryfyImportReport in common.js --%>		
		<sj:a id="anchor_displayreport2_export" href="#" button="true"	onClickTopics="exportFileImportingReport" 
		><s:text name="jsp.default_195"></s:text></sj:a>
		</ffi:cinclude>
		</div>
	


<ffi:cinclude value1="${ReportData.ReportID.ReportID}" value2="0"
	operator="notEquals">
	<form action="reportcriteria.jsp" method="post" name="FormName">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" /> 
		<input type="hidden" name="reportID"  value="<ffi:getProperty name="ReportData" property="ReportID.ReportID"/>">
		<input type="hidden" name="resetReportOptions" value="TRUE">
	
		<table width="750px" border="2">
			<tr>
				<td style="padding-top: 0.8em; width: 50%" align="left" valign="top"><span
					style="padding-left: 1.3em" class="sectionsubhead">Name:&nbsp;</span>
				<span style="padding: 0px;" class="columndata"><ffi:getProperty
					name="ReportData" property="ReportID.ReportName" /></span></td>
				<td style="padding-top: 0.8em; width: 50%" align="left" valign="top"><span
					style="padding-left: 0" class="sectionsubhead">Description:&nbsp;</span>
				<span style="padding: 0px;" class="columndata"><ffi:getProperty
					name="ReportData" property="ReportID.Description" /></span></td>
			</tr>
		</table>
	</form>
</ffi:cinclude>
<table width="<ffi:getProperty name="real_pagewidth"/>" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="tbrd_b">&nbsp;</td>
	</tr>
	<tr>
		<td class="tbrd_lr dkback"><ffi:object id="exportHeader"
			name="com.ffusion.tasks.reporting.ExportHeaderOptions" /> <ffi:setProperty
			name="exportHeader" property="ReportName" value="ReportData" /> <ffi:setProperty
			name="exportHeader" property="ExportFormat"
			value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
		<ffi:process name="exportHeader" /> <ffi:getProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_HEADER_NAME %>"
			encode="false" /> <ffi:removeProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_HEADER_NAME %>" />
		<ffi:removeProperty name="exportHeader" /></td>
	</tr>
	<tr>
		<td class="tbrd_ltr"><br>
		<ffi:object id="exportForDisplay"
			name="com.ffusion.tasks.reporting.ExportReport" /> <ffi:setProperty
			name="exportForDisplay" property="ReportName" value="ReportData" /> <ffi:setProperty
			name="exportForDisplay" property="ExportFormat"
			value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
		<ffi:process name="exportForDisplay" /> <ffi:getProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_NAME %>"
			encode="false" /> <ffi:removeProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_NAME %>" />
		<ffi:removeProperty name="exportForDisplay" /></td>
	</tr>
	<tr>
		<td class="tbrd_ltr dkback"><ffi:object id="exportFooter"
			name="com.ffusion.tasks.reporting.ExportFooterOptions" /> <ffi:setProperty
			name="exportFooter" property="ReportName" value="ReportData" /> <ffi:setProperty
			name="exportFooter" property="ExportFormat"
			value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>" />
		<ffi:process name="exportFooter" /> <ffi:getProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_FOOTER_NAME %>"
			encode="false" /> <ffi:removeProperty
			name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_FOOTER_NAME %>" />
		<ffi:removeProperty name="exportFooter" /></td>
	</tr>
	<tr>
		<td class="tbrd_t">&nbsp;</td>
	</tr>
	<ffi:removeProperty name="opt_pagewidth" />
	<ffi:removeProperty name="real_pagewidth" />
</table>

 <ffi:setProperty name="BackURL" value="${tmpBackURL}" />
		<div align="center" id="rptDisplayControlls">
		
		<%-- This button is jqueryfied by ns.common.jqueryfyImportReport in common.js --%>
		<sj:a id="anchor_displayreport2_printerready" href="#" button="true" indicator="indicator" onclick="printerReady();"
		><s:text name="jsp.default_332"></s:text></sj:a>
		

	<ffi:cinclude value1="${doneCompleteDirection}" value2="account">

		<%-- This button is jqueryfied by ns.common.jqueryfyImportReport in common.js --%>		
		<sj:a id="anchor_displayreport2_done2" href="#" button="true" onClickTopics="closeDialog"><s:text name="jsp.default_175"></s:text></sj:a>

	</ffi:cinclude>
</div>

	<%-- This dialog is jqueryfied by ns.common.jqueryfyImportReport in common.js --%>
	
	<sj:dialog id="printerReadyReportDialog" autoOpen="false" modal="true"
	cssClass="reportDisplayInnerDialog" title="%{getText('jsp.reports_639')}" height="600" width="700" overlayColor="#000"
	overlayOpacity="0.7"
	buttons="{ 
    		 	'%{getText(\"jsp.default_82\")}':function() { ns.common.closeDialog('printerReadyReportDialog'); } 
			,	'%{getText(\"jsp.default_331\")}': function(){ printReport();}
   		}">
   	</sj:dialog>

</div>

<%-- Dialog Definitions --%>

<%-- since the report data may be very large, we remove it from session.  the report criteria
     should be left alone to avoid breaking the "save report" page --%>
<% ( (com.ffusion.beans.reporting.Report)pageContext.findAttribute( "ReportData" ) ).setReportResult( null ); %>

<%-- save copy of ReportData in case user navigates back from non-display page report--%>
<%
	com.ffusion.beans.reporting.Report report = (com.ffusion.beans.reporting.Report) session.getAttribute( "ReportData" );
	com.ffusion.beans.reporting.Report report2 = (com.ffusion.beans.reporting.Report) report.clone();
    session.setAttribute( "ReportDataSaved", report2 );
%>
<ffi:removeProperty name="GenerateSavedReportBase" />
<ffi:removeProperty name="doneCompleteDirection" />
<script type="text/javascript">
	$(document).ready(function(){
		$("#GenerateReportBaseFormat").selectmenu({width: 170});
	})
   
    function  printReport(){
		$('#printerReadyReportDialog').print();
	}
	/* TO show the PRINTER READY window */
	 function printerReady(){
		var url = "/cb/pages/jsp/reports/inc/printreport.jsp";
		$.ajax({
			url: url,
			success: function(data){
				$("#printerReadyReportDialog").html(data).dialog('open');
			}
		});
	}

</script>