<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<ffi:setProperty name="subMenuSelected" value="report_cash"/>

<s:set var="tmpI18nStr" value="%{getText('jsp.reports_530')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>

<%-- Make a reference to this page, so the report criteria page can go back to it on cancel --%>
<ffi:setProperty name="reportListPage" value="${FullPagesPath}/reports/cash_list.jsp"/>

<ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>
<ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
<ffi:setProperty name="GenerateReportBase" property="Target" value="${FullPagesPath}reports/displayreport.jsp"/>



<ffi:setProperty name="reportTopMenuId" value="reporting_cashmgt"/>

<div id="cashFlowSummaryTab" class="portlet gridPannelSupportCls">

	<div class="portlet-header" >
	    <div class="searchHeaderCls">
			<div class="summaryGridTitleHolder">
				<s:if test="%{#parameters.dashboardElementId[0] == 'lockBoxContent'}">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.default_268" />
					</span>
				</s:if>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'pPayContent'}">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.default_327" />
					</span>
				</s:elseif>
				<s:elseif test="%{#parameters.dashboardElementId[0] == 'rpPayContent'}">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.home_263" />
					</span>
				</s:elseif>
				<s:else>
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.default_87" />
					</span>
				</s:else>
				
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight marginRight10"></span>
				
				<div class="gridTabDropdownHolder">
					<span class="gridTabDropdownItem" id='cashFlowContent' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/cash_list.jsp',this)" >
						<s:text name="jsp.default_87" />
					</span>
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCKBOX %>" >
						<span class="gridTabDropdownItem" id='lockBoxContent' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/cash_list.jsp',this)">
							<s:text name="jsp.default_268" />
						</span>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>" >
						<span class="gridTabDropdownItem" id='pPayContent' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/cash_list.jsp',this)">
							<s:text name="jsp.default_327" />
						</span>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
						<span class="gridTabDropdownItem" id='rpPayContent' onclick="ns.report.renderSelectedReport('/cb<s:property value="%{#session.PagesPath}"/>reports/cash_list.jsp',this)">
							<s:text name="jsp.home_263" />
						</span>
					</ffi:cinclude>
				</div>
			</div>
		</div>
	</div>
	<%
	if( session.getAttribute("ReportNamesAndCategories")==null ) {
	%>
		<ffi:object id="GetHashmapInSession" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
		<ffi:setProperty name="GetHashmapInSession" property="reportCategory" value="all"/>
		<ffi:process name="GetHashmapInSession"/>
		<ffi:removeProperty name="GetHashmapInSession"/>
	<%
	}
	%>
	<div class="portlet-content" id="cashFlowSummaryTabContent">
		<ffi:setProperty name="table_bgstyle" value=""/>
		<s:if test="%{#parameters.dashboardElementId[0] != ''}">
			<!-- do nothing this is a reverse check for fail save scenario -->
			<s:if test="%{#parameters.dashboardElementId[0] == 'lockBoxContent'}">
				<s:include value="%{#session.PagesPath}reports/inc/cash_list_lockbox.jsp" />
			</s:if>
			<s:elseif test="%{#parameters.dashboardElementId[0] == 'pPayContent'}">
				<s:include value="%{#session.PagesPath}reports/inc/cash_list_ppay.jsp" />
			</s:elseif>
			<s:elseif test="%{#parameters.dashboardElementId[0] == 'rpPayContent'}">
				<s:include value="%{#session.PagesPath}reports/inc/cash_list_rppay.jsp" />
			</s:elseif>
			<s:else>
					<s:include value="%{#session.PagesPath}reports/inc/cash_list_cashflow.jsp" />
			</s:else>
		</s:if>
		<s:else>
			<s:include value="%{#session.PagesPath}reports/inc/cash_list_cashflow.jsp" />
		</s:else>
	</div>
	<div id="cashFlowSummaryTabDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('cashFlowSummaryTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<script type="text/javascript">
	//Initialize portlet with settings icon
	ns.common.initializePortlet("cashFlowSummaryTab");
	
</script>

<ffi:setProperty name="saveBackURL" value=""/>
<ffi:removeProperty name="reportNames"/>
<ffi:removeProperty name="reportNamesSize"/>
<ffi:removeProperty name="category"/>
<ffi:removeProperty name="Sort"/>
<ffi:removeProperty name="reportTopMenuId"/>
<ffi:removeProperty name="reportTypeID"/>