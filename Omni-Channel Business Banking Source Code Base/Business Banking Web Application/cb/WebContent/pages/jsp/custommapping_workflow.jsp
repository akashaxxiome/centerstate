<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

	<div id="customMappingPortlet" class="portlet wizardSupportCls" style="float: left; width: 100%;" >
		<div class="portlet-header" role="section" aria-labelledby="summaryHeader">
			<h1 id="summaryHeader" class="portlet-title">Add Custom Mapping</h1>
		</div>
		<div class="portlet-content">
			<sj:tabbedpanel id="customMappingWizardTabs" cssClass="marginTop20" >
		        <sj:tab id="inputCustomMappingTab" target="inputCustomMappingDiv" label="%{getText('jsp.default_390')}"/>
		        <sj:tab id="verifyCustomMappingTab" target="verifyCustomMappingDiv" label="%{getText('jsp.default_391')}"/>
		        <div id="inputCustomMappingDiv" style="border:1px">
					<s:text name="jsp.default_186"/>
		            <br><br>
		        </div>
		        <div id="verifyCustomMappingDiv" style="border:1px">
		            <s:text name="jsp.default_458"/>
		            <br><br>
		        </div>
		    </sj:tabbedpanel>
		</div>
		<div id="customMappingDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('customMappingPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>	
    </div>

<script>
//Initialize portlet with settings icon
ns.common.initializePortlet("customMappingPortlet");
</script>