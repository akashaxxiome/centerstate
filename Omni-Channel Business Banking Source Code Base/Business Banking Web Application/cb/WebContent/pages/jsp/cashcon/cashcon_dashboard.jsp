<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="com.ffusion.beans.FundsTransactionTypes,com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-cashCon"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_48" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/transferSubmenuDropdown.jsp" />
    	</div>
    	<!-- dashboard items listing for transfers -->
        <div id="dbcashConSummary" class="dashboardSummaryHolderDiv">
        	<s:url id="goBackSummaryUrl" value="/pages/jsp/cashcon/cashcon_summary.jsp" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
	
			<!-- CashCon summary link -->
        	<sj:a id="goBackSummaryLink" href="%{goBackSummaryUrl}" targets="summary"
				button="true" cssClass="summaryLabelCls"
				title="%{getText('jsp.billpay_go_back_summary')}"
				onClickTopics="summaryLoadFunction">
				  	<s:text name="jsp.billpay_summary" />
			</sj:a>
			
			
        	<s:url id="addDepositUrl" value="/pages/jsp/cashcon/addDepositCashconAction_init.action" escapeAmp="false">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		    	<s:param name="Type"><%=FundsTransactionTypes.FUNDS_TYPE_CASH_CONCENTRATION%></s:param>
			</s:url>
			
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>" >
		    	<sj:a id="addDepositLink" href="%{addDepositUrl}" targets="inputDiv" button="true"
		    	title="%{getText('jsp.cashcon_41')}"
		    	onClickTopics="beforeLoadCashconForm" onCompleteTopics="loadCashconFormComplete" onErrorTopics="errorLoadCashconForm">
				<s:text name="jsp.cashcon_42" /></sj:a>
			</ffi:cinclude>
	
	        <s:url id="addDisbursementUrl" value="/pages/jsp/cashcon/addDisbursementCashconAction_init.action" escapeAmp="false">
	        	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	        	<s:param name="Type"><%=FundsTransactionTypes.FUNDS_TYPE_CASH_DISBURSEMENT%></s:param>
			</s:url>
			
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST %>" >
				<sj:a id="addDisbursementLink" href="%{addDisbursementUrl}" targets="inputDiv" button="true"
					    	title="%{getText('jsp.cashcon_43')}"
					    	onClickTopics="beforeLoadCashconForm" onCompleteTopics="loadCashconFormComplete" onErrorTopics="errorLoadCashconForm">
						  	<s:text name="jsp.cashcon_44" /></sj:a>
			</ffi:cinclude>
        </div>
       
</div>

 <%--    <div id="appdashboard-left" class="formLayout" style="width:90%">
		<sj:a
			id="quickSearchLink"
			button="true"
			buttonIcon="ui-icon-search"
			onClick="$('#quicksearchcriteria').toggle();"
		><s:text name="jsp.default_4"/></sj:a> 

	</div>

	<span class="ui-helper-clearfix">&nbsp;</span>--%>

	<%-- Following three dialogs are used for view, delete and inquiry cash concentration --%>
	<sj:dialog id="inquiryCashconDialogID" cssClass="pmtTran_cashconDialog" title="%{getText('jsp.default_249')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

	<sj:dialog id="viewCashconDetailsDialogID" cssClass="pmtTran_cashconDialog" title="%{getText('jsp.cashcon_7')}" modal="true" resizable="false" autoOpen="false"  closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="500" cssStyle="overflow:hidden;">
	</sj:dialog>

	<sj:dialog id="deleteCashconDialogID" cssClass="pmtTran_cashconDialog" title="%{getText('jsp.cashcon_13')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

	<%-- Following three dialogs are used for view, delete and inquiry cash concentration --%>

<script>

$("#goBackSummaryLink").find("span").addClass("dashboardSelectedItem");

</script>

