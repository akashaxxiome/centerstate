<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ffusion.beans.FundsTransactionTypes,
                 com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_cashconconfirm" className="moduleHelpClass"/>

<s:form id="cashconNewSendFormId" namespace="/pages/jsp/cashcon" validate="false" action="editDisbursementCashconAction_execute" method="post" name="cashConNewSendForm" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<ffi:removeProperty name="tmp_url"/>
<div class="leftPaneWrapper"role="form" aria-labelledby="disbursementRequestSummary">
	<div class="leftPaneInnerWrapper">
		<div class="header"><h2 id="disbursementRequestSummary"><ffi:getProperty name="CashCon" property="TypeString" /> Summary</h2></div>
		<div class="leftPaneInnerBox summaryBlock"  style="padding: 15px 5px;">
			<div style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_174"/>:</span>
					<span class="inlineSection floatleft labelValue"><ffi:getProperty name="CashCon" property="DivisionName"/></span>
				</div>
		</div>
	</div>
</div>
<div class="rightPaneWrapper w71">
  	<div class="paneWrapper label130" role="form" aria-labelledby="disbursementRequestDetails">
  		<div class="paneInnerWrapper">
	   		<div class="header"><h2 id="disbursementRequestDetails"><ffi:getProperty name="CashCon" property="TypeString" /> Details</h2></div>
	   		<div class="paneContentWrapper">
	   		<div class="blockContent">
	   			<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_266"/>:</span>
						<span class="columndata">
							<ffi:getProperty name="CashCon" property="LocationName"/>
						</span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_86"/>:</span>
						<span class="columndata">
							<ffi:getProperty name="CashConCompany" property="CompanyName" /> / <ffi:getProperty name="CashConCompany" property="CompanyID" />
						</span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_603"/>:</span>
						<span class="columndata">
							<ffi:getProperty name="CashConCompany" property="DisbNextCutOffTime" />
						</span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel"  >
							<s:text name="jsp.default_485"/>:
						</span>
						<span class="columndata">
							<ffi:getProperty name="CashCon" property="AmountValue.CurrencyStringNoSymbol"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
						</span>
					</div>
				</div>
				
				<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">					
						<ffi:cinclude value1="true" value2="${CashConCompany.DisbSameDayCutOffDefined}" operator="equals" >

						<div class="blockRow">
							<div class="inlineBlock" style="width: 50%">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_604"/>:</span>
								<span class="columndata ltrow2_color">
										<ffi:getProperty name="CashConCompany" property="DisbNextSameDayCutOffTime" />
								</span>
							</div>
							<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel">
									<s:text name="jsp.default_602"/>:
								</span>
								<span class="columndata ltrow2_color">
									<ffi:cinclude value1="${sameDayCashCon}" value2="true" operator="equals"><s:text name="jsp.cashcon_47"/></ffi:cinclude>
									<ffi:cinclude value1="${sameDayCashCon}" value2="false" operator="equals"><s:text name="jsp.cashcon_48"/></ffi:cinclude>
								</span>
							</div>
						</div>
					
						<ffi:cinclude value1="${sameDayCashCon}" value2="true" operator="equals">
							<div class="blockRow">
								<span class="columndata ltrow2_color"><s:text name="jsp.cashcon_46"/></span>
							</div>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>	
				
	 		</div>
	    
	<div class="btn-row">
	<sj:a button="true" summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelCashconForm"><s:text name="jsp.default_82"/></sj:a>
	<sj:a button="true" onClickTopics="backToInput"><s:text name="jsp.default_57"/></sj:a>
	<sj:a 
	id="sendCashconSubmit"
	formIds="cashconNewSendFormId"
	targets="confirmDiv" 
	button="true" 
	onBeforeTopics="beforeSendCashconForm"
	onSuccessTopics="sendCashconFormSuccess"
	onErrorTopics="errorSendCashconForm"
	onCompleteTopics="sendCashconFormComplete"
	><s:text name="jsp.default_107" /></sj:a>
	</div>	
  </div>
  </div>
 </div>
</div> 	
</s:form>