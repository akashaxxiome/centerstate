<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_cashconpendingApprovalSummary" className="moduleHelpClass"/>

	<ffi:setGridURL grid="GRID_pendingapprovalCashcon" name="ViewDepositURL" url="/cb/pages/jsp/cashcon/viewDepositCashconAction.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingapprovalCashcon" name="EditDepositURL" url="/cb/pages/jsp/cashcon/editDepositCashconAction_init.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingapprovalCashcon" name="DeleteDepositURL" url="/cb/pages/jsp/cashcon/deleteDepositCashconAction_init.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingapprovalCashcon" name="InquireDepositURL" url="/cb/pages/jsp/cashcon/sendCashConFundsTransMessageAction_initCashConInquiry.action?CashConID={0}&CashConType={1}&Subject=Cashcon Inquiry" parm0="ID" parm1="Type"/>

	<ffi:setGridURL grid="GRID_pendingapprovalCashcon" name="ViewDisbursementURL" url="/cb/pages/jsp/cashcon/viewDisbursementCashconAction.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingapprovalCashcon" name="EditDisbursementURL" url="/cb/pages/jsp/cashcon/editDisbursementCashconAction_init.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingapprovalCashcon" name="DeleteDisbursementURL" url="/cb/pages/jsp/cashcon/deleteDisbursementCashconAction_init.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingapprovalCashcon" name="InquireDisbursementURL" url="/cb/pages/jsp/cashcon/sendCashConFundsTransMessageAction_initCashConInquiry.action?CashConID={0}&CashConType={1}&Subject=Cashcon Inquiry" parm0="ID" parm1="Type"/>

<%-- 		<ffi:setProperty name="tempURL" value="/pages/jsp/cashcon/getCashconAction.action?collectionName=PendingApprovalCashCons&GridURLs=GRID_pendingapprovalCashcon" URLEncrypt="true"/> --%>
		<ffi:setProperty name="tempURL" value="/pages/jsp/cashcon/getPendingApprovalCashconAction_execute.action?GridURLs=GRID_pendingapprovalCashcon" URLEncrypt="true"/>

        <s:url id="pendingApprovalCashconUrl" value="%{#session.tempURL}" escapeAmp="false"/>
		<sjg:grid
			id="pendingApprovalCashconGridId"
			caption=""
			sortable="true"
			dataType="json"
			href="%{pendingApprovalCashconUrl}"
			pager="true"
			gridModel="gridModel"
			rowList="%{#session.StdGridRowList}"
			rowNum="%{#session.StdGridRowNum}"
			rownumbers="false"
			shrinkToFit="true"
			scroll="false"
			scrollrows="true"
			navigator="true"
			navigatorAdd="false"
			navigatorDelete="false"
			navigatorEdit="false"
			navigatorRefresh="false"
			navigatorSearch="false"
			navigatorView="false"
			viewrecords="true"
			sortname="submitDate"
			sortorder="asc"
			onGridCompleteTopics="addGridControlsEvents,pendingApprovalCashconGridCompleteEvents"
			>

			<sjg:gridColumn name="submitDate" index="date" title="%{getText('jsp.default_137')}" sortable="true" width="55"/>
			<sjg:gridColumn name="divisionName" index="divisionName" title="%{getText('jsp.default_174')}" sortable="true" width="55"/>
			<sjg:gridColumn name="locationName" index="locationName" title="%{getText('jsp.default_266')}" sortable="true" width="55"/>
			<sjg:gridColumn name="typeString" index="typeString" title="%{getText('jsp.default_444')}" sortable="true" width="55"/>
			<sjg:gridColumn name="statusName" index="statusName" title="%{getText('jsp.default_388')}" sortable="true" width="55"/>
			<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_43')}" sortable="true" width="55"/>

			<sjg:gridColumn name="ID" index="id" title="%{getText('jsp.default_27')}" width="100" sortable="false" formatter="ns.cashcon.formatPendingApprovalCashconActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>

			<sjg:gridColumn name="approverName" index="approverName" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="userName" index="userName" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="status" index="status" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="rejectReason" index="rejectReason" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="approverIsGroup" index="approverIsGroup" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canEdit" index="canEdit" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="canDelete" index="canDelete" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="frequency" index="frequency" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="resultOfApproval" index="resultOfApproval" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="type" index="type" title="" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="approverInfos" index="approverInfos" title="" hidden="true" hidedlg="true" formatter="ns.cashcon.formatApproversInfo" />
			<sjg:gridColumn name="ViewDepositURL" index="ViewDepositURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="EditDepositURL" index="EditDepositURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="DeleteDepositURL" index="DeleteDepositURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="InquireDepositURL" index="InquireDepositURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>

			<sjg:gridColumn name="ViewDisbursementURL" index="ViewDisbursementURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="EditDisbursementURL" index="EditDisbursementURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="DeleteDisbursementURL" index="DeleteDisbursementURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="InquireDisbursementURL" index="InquireDisbursementURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>

		</sjg:grid>

		<script type="text/javascript">
		<!--
			$("#pendingApprovalCashconGridId").jqGrid('setColProp','ID',{title:false});
		//-->
		</script>
