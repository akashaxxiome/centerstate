<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="com.ffusion.beans.FundsTransactionTypes,com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<script type="text/javascript">
$(function(){
	$("#viewAllCashConId").selectmenu({width: 160});
});

var aConfig = {
	startDateBox:$("#StartDateID"),
	endDateBox:$("#EndDateID")
}
$("#cashconDateRangeBox").extdaterangepicker(aConfig);

</script>

<div id="quicksearchcriteria" style="display:none;" class="marginTop10">
			<s:form action="/pages/jsp/cashcon/getCashconAction_verify.action" method="post" id="QuickSearchCashconFormID" name="QuickSearchCashconForm" theme="simple">
				<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
				
				<!-- quick search item holder -->
				<div class="acntDashboard_masterHolder dashboardLabelMargin marginleft10">
				
					<!-- date range holder -->
					<div class="acntDashboard_itemHolder marginRight10">
						<span><s:text name="jsp.default.label.date.range"/></span></br>
						<input type="text" id="cashconDateRangeBox" />
						<input type="hidden" value="<ffi:getProperty name='cashConSearchCriteria' property='StartDate' />" id="StartDateID" name="StartDate" />
						<input type="hidden" value="<ffi:getProperty name='cashConSearchCriteria' property='EndDate' />" id="EndDateID" name="EndDate" />
					</div>
					
					<!-- show dropdown -->
					<div class="acntDashboard_itemHolder marginRight10">
						<span><s:text name="jsp.default_381"/></span></br>
						<select id="viewAllCashConId" class="txtbox" name="viewAll" >
							<option value="false" <ffi:cinclude value1="${cashConSearchCriteria.viewAll}" value2="false">selected</ffi:cinclude>>
								<s:text name="jsp.cashcon_24"/>
							</option>
							<option value="true" <ffi:cinclude value1="${viewAllCashCon}" value2="false" operator="notEquals" >selected</ffi:cinclude>>
								<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
    										<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${SecureUser.EntitlementID}"/>
								<ffi:process name="GetEntitlementGroup"/>
								<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="equals">
									<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>"><s:text name="jsp.default_42"/></ffi:cinclude>
									<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>"><s:text name="jsp.cashcon_23"/></ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="notEquals"><s:text name="jsp.default_42"/></ffi:cinclude>
							</option>
						</select>
					</div>
					
					<!-- search button -->
					<div class="acntDashboard_itemHolder">
						<span>&nbsp;</span></br>
						<sj:a targets="quick"
							id="quicksearchbutton"
							formIds="QuickSearchCashconFormID"
							button="true"
	                        validate="true"
	                        validateFunction="customValidation"
							onclick="removeValidationErrors();"
							onBeforeTopics="quickSearchCashconBefore"
							onCompleteTopics="quickSearchCashconComplete"
							>
								<s:text name="jsp.default_6" />
						</sj:a>
					</div>
					<br>
					<div style="float:left;"><span id="dateRangeValueError"></span></div>
				</div>
				
				
				<%-- <table border="0">
					<tr>
						<td colspan="3"><ul id="formerrors"></ul></td>
					</tr>
					<tr>
						<td>
							<label for=""><s:text name="jsp.default_137"/></label>
							<sj:datepicker title="%{getText('Choose_start_date')}" value="%{#session.cashConSearchCriteria.startDate}" id="StartDateID" name="startDate" label="From Date" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all txtbox"/>
							<label for=""><s:text name="jsp.default_423.1"/></label>
							<sj:datepicker title="%{getText('Choose_to_date')}" value="%{#session.cashConSearchCriteria.endDate}" id="EndDateID" name="endDate" label="To Date" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all txtbox"/>
						</td>
						<td>
							<label for=""><s:text name="jsp.default_381"/></label>
								<select id="viewAllCashConId" class="txtbox" name="viewAllCashCon" >
									<option value="false" <ffi:cinclude value1="${viewAllCashCon}" value2="false">selected</ffi:cinclude>>
										<s:text name="jsp.cashcon_24"/>
									</option>
									<option value="true" <ffi:cinclude value1="${viewAllCashCon}" value2="false" operator="notEquals" >selected</ffi:cinclude>>
										<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
	     										<ffi:setProperty name="GetEntitlementGroup" property ="GroupId" value="${SecureUser.EntitlementID}"/>
										<ffi:process name="GetEntitlementGroup"/>
										<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="equals">
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>"><s:text name="jsp.default_42"/></ffi:cinclude>
											<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>"><s:text name="jsp.cashcon_23"/></ffi:cinclude>
										</ffi:cinclude>
										<ffi:cinclude value1="${Entitlement_EntitlementGroup.EntGroupType}" value2="Group" operator="notEquals"><s:text name="jsp.default_42"/></ffi:cinclude>
									</option>
								</select>
						</td>
						<td>
							<sj:a targets="quick"
								id="quicksearchbutton"
								formIds="QuickSearchCashconFormID"
								button="true"
	                            validate="true"
	                            validateFunction="customValidation"
								onclick="removeValidationErrors();"
								onBeforeTopics="quickSearchCashconBefore"
								onCompleteTopics="quickSearchCashconComplete">
							<span class="ui-icon ui-icon-search"></span>
						</sj:a>
						</td>
						
						<td>
							<sj:a targets="quick" title="Reset Form" id="quicksearchrefresh" formIds="QuickSearchCashconFormID" button="true" onclick="$('#QuickSearchCashconFormID').resetForm();" onCompleteTopics="quickSearchCashconComplete" cssStyle="position: relative \9; top: 2px \9;"><span class="ui-icon ui-icon-refresh"></span></sj:a>
						</td>
						
					</tr>
					<tr>
						<td colspan="3">
							<span id="startDateError"></span>
							<span id="endDateError"></span>
						</td>
					</tr>
				</table> --%>
			</s:form>
		</div>