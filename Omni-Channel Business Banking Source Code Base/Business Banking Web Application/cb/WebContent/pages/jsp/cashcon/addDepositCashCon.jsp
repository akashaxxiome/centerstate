<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_cashconnew" className="moduleHelpClass"/>

<span class="shortcutPathClass" style="display:none;">pmtTran_cashcon,addDepositLink</span>

<script type="text/javascript">
<!--

$(function(){
	$("#divisionID").selectmenu({width: 160});
	$("#locationID").selectmenu({width: 160});
	ns.common.updatePortletTitle('detailsPortlet',js_cashcon_deposit_entry,false);
	ns.common.selectDashboardItem("addDepositLink");
});

function reloadDivision() {	
	var urlString = '/cb/pages/jsp/cashcon/addDepositCashconAction_reloadDivision.action';
	$.ajax({
		url: urlString,
		type: "POST",
		data: $("#cashconNewFormId").serialize(),
		success: function(data){
			$("#inputDiv").html(data);
		}
	});
}


function reloadLocation() {
	var urlString = '/cb/pages/jsp/cashcon/addDepositCashconAction_reloadLocation.action';
	$.ajax({
		url: urlString,
		type: "POST",
		data: $("#cashconNewFormId").serialize(),
		success: function(data){
			$("#inputDiv").html(data);
		}
	});
}


function sameDayCashConChecked(cashConId) {
	if (cashConId.checked) {
		$('#sameDayCashConId').val(true);
	} else {	
		$('#sameDayCashConId').val(false);
	}
}



//-->
//This function is used to check event of enter key.
$("#cashconNewFormId").keypress(function(e)
{
		// if the key pressed is the enter key
		if (e.which == 13)
		{
			$('#verifyCashconSubmit').click();
			return false;
		}
});
</script>
<s:form id="cashconNewFormId" namespace="/pages/jsp/cashcon" validate="false" action="addDepositCashconAction_verify" method="post" name="cashConNewForm" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper" role="form" aria-labelledby="division" aria-required="true">
<div class="leftPaneInnerWrapper">
	<div class="marginTop10"><span id="formerrors"></span></div>	

	<div class="header"><h2 id="division"><s:text name="jsp.default_174"/>*</h2></div>

	
	<div class="leftPaneInnerBox">
		<div style="width:100%">
			<s:if test="%{divisions.size > 1}">
				<s:select id="divisionID" cssClass="ui-widget-content" name="depositCashCon.divisionID" list="divisions" listKey="groupId" listValue="groupName" onchange="reloadDivision();"></s:select>
			</s:if>
			<s:else>																						
				<s:property value="division.groupName"/>
			</s:else>
		</div>
		<div class="marginTop10"><span id="divisionIDError"></span></div>
		</div>
	</div>
</div>
<% boolean isDisabled = false;
%>
<div class="rightPaneWrapper w71">
	<div class="paneWrapper" role="form" aria-labelledby="depositEntryDetail">
	  	<div class="paneInnerWrapper">
	   		<div class="header"><h2 id="depositEntryDetail"><ffi:getProperty name="DepositCashCon" property="TypeString" /> Details</h2></div>
	   		<div class="paneContentWrapper">
	   	  <s:if test="%{locations.size > 0}">
	   		<% boolean displayLocation = false; %>
	   			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData tdWithPadding">
	   				<tr>
	   					<td width="25%" class="sectionsubhead"><s:text name="jsp.default_266"/> <span class="required">*</span></td>
	   					<% if (displayLocation) { %>
						<td class="sectionsubhead"><s:text name="jsp.default_267"/></td>
						<% } else { %>
						<% } %>
						
						
						<td  class="sectionsubhead" ><s:text name="jsp.default_86"/></td>
						<td class="sectionsubhead"><s:text name="jsp.default_603"/></td>
	
					<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
						<ffi:cinclude value1="true" value2="${CashConCompany.ConcSameDayCutOffDefined}" operator="equals" >
							<td class="sectionsubhead"><s:text name="jsp.default_604"/></td>
							<td class="sectionsubhead"><s:text name="jsp.default_602"/></td>	
						</ffi:cinclude>		
					</ffi:cinclude>	
						
						<td class="sectionsubhead"><label for="amount"><s:text name="jsp.default_484"/><span class="required">*</span></label> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)</td>
	   				</tr>
					<tr class="ltrow2_color">
						<td class="columndata">
							<s:if test="%{locations.size > 1}">
								<s:select id="locationID" cssClass="ui-widget-content" name="depositCashCon.locationID" list="locations" listKey="locationBPWID" listValue="locationName" onchange="reloadLocation();">
								</s:select>
							</s:if>
							<s:else>
								<s:property value="location.locationName"/>
							</s:else>
						</td>
						<% if (displayLocation) { %>
							<td class="columndata">
								<input type="text" name="locationName" size="12" value="<ffi:getProperty name="LocationName"/>" class="ui-widget-content ui-corner-all">
								<input class="submitbutton" type="button" name="Submit3" value="<s:text name="jsp.default_373"/>" onclick="document.CashConNew.action='<ffi:getProperty name="SecurePath"/>payments/cashconnew.jsp';document.CashConNew.submit();">&nbsp;
							</td>
						<% } else { %>
						<% } %>
				
						<td class="columndata">
							<s:if test="%{cashConCompany.companyID != ''}">
								<s:property value="cashConCompany.companyName"/> / <s:property value="cashConCompany.companyID"/>
							</s:if>
						</td>		
						<td class="columndata">
							<ffi:getProperty name="CashConCompany" property="ConcNextCutOffTime" />
						</td>
					<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
							
						<ffi:cinclude value1="true" value2="${CashConCompany.ConcSameDayCutOffDefined}" operator="equals" >
		
							<input id="sameDayCashConId" type="hidden" name="sameDayCashCon"/>	
							<td class="columndata">
								  <ffi:getProperty name="CashConCompany" property="ConcNextSameDayCutOffTime" />
							</td>
							<td class="columndata">
								  <input onchange="sameDayCashConChecked(this);" type="checkbox" name="sameDayCashCon" border="0"
									<ffi:cinclude value1="true" value2="${CashConCompany.ConcCutOffsPassed}" operator="equals"> disabled="disabled" </ffi:cinclude>
								  >
							</td>
						</ffi:cinclude>		
					</ffi:cinclude>		
						<td class="columndata">
                            <input id="amount" type="text" name="depositCashCon.amount" size="11" maxlength="11" aria-required="true" value="<ffi:getProperty name="DepositCashCon" property="AmountValue.CurrencyStringNoSymbol"/>" class="ui-widget-content ui-corner-all" <ffi:cinclude value1="" value2="${Location.LocationName}">disabled</ffi:cinclude>>
						</td>			
					</tr>
					<tr>
						<td><span id="locationIDError"></span></td>
						<td></td>
						<td></td>
						<ffi:cinclude value1="true" value2="${CashConCompany.ConcSameDayCutOffDefined}" operator="equals" >
							<td></td>
							<td></td>
						</ffi:cinclude>	
						<td><span id="amountError"></span></td>
					</tr>
</table>
</s:if>
<s:else>
	<s:text name="jsp.default_597"/>
</s:else>
<%-- include javascript for checking the amount field. Note: Make sure you include this file after the collection has been populated.--%>
<s:include value="/pages/jsp/common/checkAmount_js.jsp"/>

<ffi:cinclude value1="" value2="${Location.LocationName}">
	<% isDisabled = true; %>
</ffi:cinclude>

	   		</div>

<div align="center" class="required">* <s:text name="jsp.default_240"/></div>
<div class="btn-row">
<sj:a button="true" summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelCashconForm"><s:text name="jsp.default_82"/></sj:a>
<% if (isDisabled) { %>
<input id="disableVerifyCashconSubmitID" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-disabled formSubmitBtn" type="Button"  value='<s:text name="jsp.default_291"/>' />	
<% } %>
<% if (!isDisabled) { %>

<sj:submit
id="verifyCashconSubmit"
formIds="cashconNewFormId"
targets="verifyDiv" 
button="true"
validate="true"
validateFunction="customValidation"
onBeforeTopics="beforeVerifyCashconForm"
onCompleteTopics="verifyCashconFormComplete"
onErrorTopics="errorVerifyCashconForm"
onSuccessTopics="successVerifyCashconForm"
value="%{getText('jsp.default_291')}"
cssClass="formSubmitBtn"
/>
<% } %>
</div>	   	
</div>
</div>
</div>
</s:form>
