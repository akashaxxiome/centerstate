<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_cashconCompletedSummary" className="moduleHelpClass"/>

	<ffi:setGridURL grid="GRID_completedCashcon" name="ViewDepositURL" url="/cb/pages/jsp/cashcon/viewDepositCashconAction.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_completedCashcon" name="InquireDepositURL" url="/cb/pages/jsp/cashcon/sendCashConFundsTransMessageAction_initCashConInquiry.action?CashConID={0}&CashConType={1}&Subject=Cashcon Inquiry" parm0="ID" parm1="Type"/>
	
	<ffi:setGridURL grid="GRID_completedCashcon" name="ViewDisbursementURL" url="/cb/pages/jsp/cashcon/viewDisbursementCashconAction.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_completedCashcon" name="InquireDisbursementURL" url="/cb/pages/jsp/cashcon/sendCashConFundsTransMessageAction_initCashConInquiry.action?CashConID={0}&CashConType={1}&Subject=Cashcon Inquiry" parm0="ID" parm1="Type"/>
	
<%-- 	<ffi:setProperty name="tempURL" value="/pages/jsp/cashcon/getCashconAction.action?collectionName=CompletedCashCons&GridURLs=GRID_completedCashcon" URLEncrypt="true"/> --%>
	<ffi:setProperty name="tempURL" value="/pages/jsp/cashcon/getCompletedCashconAction_execute.action?GridURLs=GRID_completedCashcon" URLEncrypt="true"/>
    <s:url id="completedCashconUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="completedCashconGridId"  
		caption=""  
		sortable="true"  
		dataType="local"  
		href="%{completedCashconUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		viewrecords="true"
		sortname="submitDate"
		sortorder="desc"
		onGridCompleteTopics="addGridControlsEvents,completedCashconGridCompleteEvents"
	> 
		
		<sjg:gridColumn name="submitDate" index="date" title="%{getText('jsp.default_137')}" sortable="true" width="55"/>
		<sjg:gridColumn name="divisionName" index="divisionName" title="%{getText('jsp.default_174')}" sortable="false" width="55"/>
		<sjg:gridColumn name="locationName" index="locationName" title="%{getText('jsp.default_266')}" sortable="true" width="55"/>
		<sjg:gridColumn name="typeString" index="typeString" title="%{getText('jsp.default_444')}" sortable="true" width="55"/>
		<sjg:gridColumn name="statusName" index="statusName" title="%{getText('jsp.default_388')}" sortable="true" width="55"/>
		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_43')}" sortable="true" width="55"/>
		<sjg:gridColumn name="type" index="type" title="" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="ID" index="id" title="%{getText('jsp.default_27')}" width="100" sortable="false" formatter="ns.cashcon.formatCompletedCashconActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
		
		<sjg:gridColumn name="ViewDepositURL" index="ViewDepositURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="InquireDepositURL" index="InquireDepositURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
	
		<sjg:gridColumn name="ViewDisbursementURL" index="ViewDisbursementURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="InquireDisbursementURL" index="InquireDisbursementURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
	
	</sjg:grid>
	
	<script type="text/javascript">
	<!--
		$("#completedCashconGridId").jqGrid('setColProp','ID',{title:false});
	//-->
	</script>	
