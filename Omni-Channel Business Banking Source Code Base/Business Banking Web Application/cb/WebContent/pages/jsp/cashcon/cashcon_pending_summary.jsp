<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_cashconpendingSummary" className="moduleHelpClass"/>

	<ffi:setGridURL grid="GRID_pendingCashcon" name="ViewDepositURL" url="/cb/pages/jsp/cashcon/viewDepositCashconAction.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingCashcon" name="EditDepositURL" url="/cb/pages/jsp/cashcon/editDepositCashconAction_init.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingCashcon" name="DeleteDepositURL" url="/cb/pages/jsp/cashcon/deleteDepositCashconAction_init.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingCashcon" name="InquireDepositURL" url="/cb/pages/jsp/cashcon/sendCashConFundsTransMessageAction_initCashConInquiry.action?CashConID={0}&CashConType={1}&Subject=Cashcon Inquiry" parm0="ID" parm1="Type"/>
	
	<ffi:setGridURL grid="GRID_pendingCashcon" name="ViewDisbursementURL" url="/cb/pages/jsp/cashcon/viewDisbursementCashconAction.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingCashcon" name="EditDisbursementURL" url="/cb/pages/jsp/cashcon/editDisbursementCashconAction_init.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingCashcon" name="DeleteDisbursementURL" url="/cb/pages/jsp/cashcon/deleteDisbursementCashconAction_init.action?CashConID={0}&Type={1}" parm0="ID" parm1="Type"/>
	<ffi:setGridURL grid="GRID_pendingCashcon" name="InquireDisbursementURL" url="/cb/pages/jsp/cashcon/sendCashConFundsTransMessageAction_initCashConInquiry.action?CashConID={0}&CashConType={1}&Subject=Cashcon Inquiry" parm0="ID" parm1="Type"/>
	
<%-- 	<ffi:setProperty name="tempURL" value="/pages/jsp/cashcon/getCashconAction.action?collectionName=PendingCashCons&GridURLs=GRID_pendingCashcon" URLEncrypt="true"/> --%>
	<ffi:setProperty name="tempURL" value="/pages/jsp/cashcon/getPendingCashconAction_execute.action?GridURLs=GRID_pendingCashcon" URLEncrypt="true"/>
    <s:url id="pendingCashconUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="pendingCashconGridId"  
		caption=""  
		sortable="true"  
		dataType="local"
		href="%{pendingCashconUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		sortname="submitDate"
		sortorder="asc"
		onGridCompleteTopics="addGridControlsEvents,pendingCashconGridCompleteEvents"
	> 
		
		<sjg:gridColumn name="submitDate" index="date" title="%{getText('jsp.default_137')}" sortable="true" width="55"/>
		<sjg:gridColumn name="divisionName" index="divisionName" title="%{getText('jsp.default_174')}" sortable="true" width="55"/>
		<sjg:gridColumn name="locationName" index="locationName" title="%{getText('jsp.default_266')}" sortable="true" width="55"/>
		<sjg:gridColumn name="typeString" index="typeString" title="%{getText('jsp.default_444')}" sortable="true" width="55"/>
		<sjg:gridColumn name="statusName" index="statusName" title="%{getText('jsp.default_388')}" sortable="true" width="55"/>
		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_43')}" sortable="true" width="55"/>
		<sjg:gridColumn name="type" index="type" title="" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.default_27')}" width="100" sortable="false" search="false"  formatter="ns.cashcon.formatPendingCashconActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
		
		<sjg:gridColumn name="ViewDepositURL" index="ViewDepositURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="EditDepositURL" index="EditDepositURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="DeleteDepositURL" index="DeleteDepositURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="InquireDepositURL" index="InquireDepositURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="ViewDisbursementURL" index="ViewDisbursementURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="EditDisbursementURL" index="EditDisbursementURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="DeleteDisbursementURL" index="DeleteDisbursementURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="InquireDisbursementURL" index="InquireDisbursementURL" title="%{getText('jsp.default_248')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
	
	</sjg:grid>
	
	<script type="text/javascript">
	<!--
		$("#pendingCashconGridId").jqGrid('setColProp','ID',{title:false});
	//-->
	</script>	
