<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<span class="shortcutPathClass" style="display:none;">pmtTran_billpay,addBillpayPayeeLink</span>
<span class="shortcutEntitlementClass" style="display:none;">Payments</span>
<ffi:help id="payments_billpayaddpayee" className="moduleHelpClass"/>
<script language="javascript">
$(function(){
	$("#selectPayeeCountryID").combobox({'size': '28'});
	$("#selectPayeeStateID").combobox({'size': '28',searchFromFirst:true});
	$("#addPayeeAccountType").selectmenu({width: 150});
	$("#addPayeeCurrencyCode").selectmenu({width: 190});
	
	$("#searchPayeeNameID").lookupbox({
		"controlType":"server",
		"collectionKey":"1",
		"module":"billpaypayee",
		"size":"35",
		"source":"/cb/pages/jsp/billpay/BillPayGlobalPayeesLookupBoxAction.action"
		});

	$.ajax({
		   url: "/cb/pages/jsp/billpay/addBillpayPayeeAction_loadState.action",
		   type: "POST",
		   data:  $("#BillpayPayeeNew").serialize(),
		   success: function(data){
		   if(data.length>0){
		   	   		$("#stateNameId").removeClass('ffiHidden');
			   		$("#stateSelectId").removeClass('ffiHidden');
			   		$("#stateErrorId").removeClass('ffiHidden');
		   	}
		   	else{
		   		$("#stateNameId").addClass('ffiHidden');
		   		$("#stateSelectId").addClass('ffiHidden');
		   		$("#stateErrorId").addClass('ffiHidden');
		   	}
	 	  }
	}); 
	
});

function showState(id) {
	$.ajax({
		   url: "/cb/pages/jsp/billpay/addBillpayPayeeAction_loadState.action",
		   type: "POST",
		   data:  $("#BillpayPayeeNew").serialize(),
		   success: function(data){
			   
		   	 if(data.length>0){
		   		 
		   		$('#selectPayeeStateID').empty();
				var stateOpts = '<option value="" selected="selected"></option>';
			   	for(var i=0;i<data.length;i++){
					stateOpts += '<option value='+data[i].stateKey+ '>'+data[i].name+ '</option>';
			   	}	
			   	$('#selectPayeeStateID').html(stateOpts);
			   	$('#selectPayeeStateID').combobox('clear');  	
			   	$("#stateNameId").removeClass('ffiHidden');
			   		$("#stateSelectId").removeClass('ffiHidden');
			   		$("#stateErrorId").removeClass('ffiHidden');
		   	}
		   	else{
		   		$("#stateNameId").addClass('ffiHidden');
		   		$("#stateSelectId").addClass('ffiHidden');
		   		$("#stateErrorId").addClass('ffiHidden');
		   	}
	 	  }
	}); 
	
}

</script>
<form action="<ffi:urlEncrypt url="${SecurePath}payments/billpaypayee-company-Showall-wait.jsp"/>" method="post" name="formAddCompanyShowAllPayee">
  	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
</form>
<!-- move to global payee jsp -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData">
				<%-- <tr>
				  <td class="columndata ltrow2_color">
             <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			  <table width="100%" border="0" cellspacing="0" cellpadding="3">
				 <tr>
				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
				</tr>
				<tr>
				<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_94"/><br></span></td>
                </tr> --%>
				<%--<tr>
				<td class="instructions" colspan=6>
				 <ffi:cinclude value1="${GetLanguageList.LanguagesList.size}" value2="1" operator="equals">
				<ffi:cinclude value1="${languageFlag}" value2="true" operator="equals">
				<s:text name="jsp.billpay_67"/>
				 </ffi:cinclude>
				 </ffi:cinclude>
				</td>
				</tr> --%>
		        <tr>
				  <td class="ltrow2_color" width="40%"/>
				  <td class="ltrow2_color" nowrap width="10%" ALIGN="right">
				  </td>
				  <td class="ltrow2_color" nowrap width="50%" ALIGN="left">
<ffi:cinclude value1="${GetLanguageList.LanguagesList.size}" value2="1" operator="equals">
 <ffi:cinclude value1="${languageFlag}" value2="true" operator="equals">
                      <sj:a id="searchCompanyBillPayPayeeShowAll"
                               button="true"
                               onClickTopics="searchCompanyBillpayPayeeShowAllTopics"
                           ><s:text name="jsp.billpay_101"/></sj:a>
</ffi:cinclude>
 </ffi:cinclude>
				  </td>
				  </tr>
				 <!--  <tr><td class="instructions" colspan="6" height="30" valign="bottom" >
				  </td>
				  </tr> -->
</table>
<!-- move to global payee jsp -->
<s:form id="BillpayPayeeNew" namespace="/pages/jsp/billpay" validate="false" action="addBillpayPayeeAction_verify" method="post" name="BillpayPayeeNew" theme="simple">
<h1 style="display:none;" id="PageHeading" class="portlet-title"><s:text name="jsp.billpay_15"/></h1>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tableData formTablePadding10 tableDataForDvc">
				<%--	<tr>
						<td class="columndata ltrow2_color">
				
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										 <tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_15"/><br>
												</span></td>
										</tr>
										<tr>
											<td class="sectionsubhead"></td>
											<td></td>
											<td></td>
											<td><br>
											</td>
											<td></td>
											<td></td>
										</tr> --%>
										<tr><td style="cursor:auto" class="sectionhead nameTitle" colspan="4"><h2><s:text name="jsp.billpay.payee.info"/></h2></td></tr>
										<tr>
											<td id="payeeNameLabel" class="sectionsubhead" width="25%"><label for="payeeName"><s:text name="jsp.default_314"/></label><span class="required" title="required">*</span></td>
											<td id="payeeNickNameLabel" class="sectionsubhead" width="25%"><label for="nickName"><s:text name="jsp.default_294"/></label><span class="required" title="required">*</span></td>
											<td id="payeeContacLabel" class="sectionsubhead" width="25%"><label for="payeeContact"><s:text name="jsp.billpay_36"/></label></td>
											<td id="payeePhoneLabel" class="sectionsubhead" width="25%"><label for="phone"><s:text name="jsp.billpay_37"/></label><span class="required" title="required">*</span></td>
										</tr>
										<tr>
											<td><input id="payeeName" class="ui-widget-content ui-corner-all" aria-required="true" type="text" name="name" size="30" maxlength="32" tabindex="1"></td>
											<td><input id="nickName" class="ui-widget-content ui-corner-all" aria-required="true" type="text" name="nickName" size="30" maxlength="30" tabindex="2"></td>
											<td><input id="payeeContact" class="ui-widget-content ui-corner-all" type="text" name="contactName" size="30" tabindex="3"></td>
											<td><input id="phone" class="ui-widget-content ui-corner-all" aria-required="true" type="text" name="phone" size="30" maxlength="18" tabindex="4">
											</td>
										</tr>
										<tr>
											<td><span id="nameError"></span>
												<span id="PayeeName.existsError"></span>
											</td>
											<td><span id="nickNameError"></span></td>
											<td></td>
											<td><span id="phoneError"></span></td>
										</tr>
										<tr>
											<td id="payeeAddress1Label"><label for="streetOne"><s:text name="jsp.default_36"/></label><span class="required" aria-required="true">*</span></td>
											<td id="payeeAddress2Label"><label for="streetTwo"><s:text name="jsp.default_37"/></label></td>
											<td id="payeeAddress3Label" class="sectionsubhead"><label for="streetThree"><s:text name="jsp.default_38"/></label></td>
											<td id="payeeCityLabel" class="sectionsubhead"><label for="city"><s:text name="jsp.default_101"/></label><span class="required" title="required">*</span></td>
										</tr>
										<tr>
											<td><input id="streetOne" class="ui-widget-content ui-corner-all" type="text" aria-required="true" name="street" size="30" maxlength="32" tabindex="11"></td>
											<td><input id="streetTwo" class="ui-widget-content ui-corner-all" type="text" name="street2" size="30" maxlength="32" tabindex="12"></td>
											<td><input id="streetThree" class="ui-widget-content ui-corner-all" type="text" name="street3" size="30" maxlength="32" tabindex="13"></td>
											<td><input id="city" class="ui-widget-content ui-corner-all" type="text" aria-required="true" name="city" size="30" maxlength="32" tabindex="14"></td>
										</tr>
										<tr>
											<td><span id="streetError"></span></td>
											<td><span id="street2Error"></span></td>
											<td></td>
											<td><span id="cityError"></span></td>
										</tr>
										<tr>
											<td id="stateNameId" class=""><label for="selectPayeeStateID"><s:text name="jsp.billpay_104"/></label><span class="required" title="required">*</span></td>
											<td id="payeeZipLabel" class="sectionsubhead"><label for="zipCode"><s:text name="jsp.default_473"/></label><span class="required" title="required">*</span></td>
											<td id="payeeCountryLabel" class="sectionsubhead"><label for="selectPayeeCountryID"><s:text name="jsp.default_115"/></label><span class="required" title="required">*</span></td>
											<td id="payeeAccountLabel" class="sectionsubhead"><label for="userAccount"><s:text name="jsp.default_19"/></label><span class="required" title="required">*</span></td>
										</tr>
										<tr>
											<td id="stateSelectId">
												<!-- Checking data length is removed from here as if initial country has no states in it, this selectbox will never exist-->
													<div class="selectBoxHolder">
												<s:select list="stateProvinceDefns" listKey="stateKey" listValue="Name" emptyOption="true" aria-labelledby="selectPayeeStateID"
					aria-required="true" value="%{addPayee.state}"
												class="txtbox" id="selectPayeeStateID"  name="state" tabindex="16"></s:select>
												</div>
												
												<!-- <input type="hidden" name="checkState" value="true"> -->
											</td>
											<td><input id="zipCode" class="ui-widget-content ui-corner-all" type="text" name="zipCode" size="12" maxlength="32" tabindex="17"></td>
											<td>
												<div class="selectBoxHolder">
													<s:select list="countryDefns" listKey="CountryCode" listValue="Name" value="%{#session.SecureUser.Locale.ISO3Country}"
													id="selectPayeeCountryID" aria-labelledby="selectPayeeCountryID" class="txtbox" name="country" onchange="showState(this);" tabindex="15">
													</s:select>
												</div>
											</td>
											<td><input id="userAccount" class="ui-widget-content ui-corner-all" type="text" name="userAccountNumber" size="30" maxlength="30" tabindex="5"></td>
										</tr>
										<tr>
											<td id="stateErrorId"><span id="stateError"></span></td>
											<td><span id="zipCodeError"></span></td>
											<td><span id="countryError"></span></td>
											<td><span id="userAccountNumberError"></span></td>
										</tr>
										<tr><td style="cursor:auto" class="sectionhead nameTitle" colspan="4" role="section" aria-labelledby="bankinfo"><h2 id=bankinfo"><s:text name="jsp.billpay.payee.bank.info"/></h2></td></tr>
										<tr>
											<td id="payeeRoutingNoLabel" class="sectionsubhead">
													<label for="payeeRouteBankIdentifier"><s:property value="%{bankIdentifierDisplayText}"/></label>
													<s:hidden value="%{bankIdentifierDisplayText}" name="bankIdentifierDisplayText"></s:hidden>
											</td>
											<td id="payeeBankAccountNoLabel" class="sectionsubhead"><label for="bankAccount"><s:text name="jsp.billpay_26"/></label></td>
											<td id="payeeBankAccountTypeLabel" class="sectionsubhead"><label for=""addPayeeAccountType"><s:text name="jsp.billpay_27"/></label></td>
											<label for="addPayeeCurrencyCode"><ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
												<td id="payeeBankAccountCurrencyLabel" class="sectionsubhead"><s:text name="jsp.billpay_25"/></td>
											</ffi:cinclude>
										</tr>
										<tr>
											<td><input id="payeeRouteBankIdentifier" class="ui-widget-content ui-corner-all" type="text" name="payeeRoute.BankIdentifier" size="15" value=""tabindex="7">
												<sj:a id="searchBankBillPayPayee2" 
													  button="true" 
												      onClickTopics="searchBankBillpayAddPayeeTopic"
												><s:text name="jsp.default_373"/></sj:a>
											</td>
											<td><input id="bankAccount" class="ui-widget-content ui-corner-all" type="text" name="payeeRoute.AccountID" size="30" tabindex="8"></td>
										 	<td>
												<s:select list="accountTypeSelectList" id="addPayeeAccountType" name="payeeRoute.acctType" class="txtbox" value="%{addPayee.payeeRoute.acctType}"></s:select>
									 	 	</td>
											<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
												<td><s:select list="forexCurrency" class="txtbox" id="addPayeeCurrencyCode" name="payeeRoute.currencyCode" tabindex="10" value="#session.SecureUser.BaseCurrency">
											  		</s:select></td>
											</ffi:cinclude>
											<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
												<input type="hidden" name="payeeRoute.currencyCode" value="<s:property value="#session.SecureUser.BaseCurrency"/>"/>
											</ffi:cinclude>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td>
												<span id="payeeRoute.acctTypeError"></span>
											</td>
											<td>
												<span id="payeeRoute.currencyCodeError"></span>
											</td>
										</tr>
										<tr>
											<td colspan="4"><div align="center">
									            <span class="required">&nbsp;<br>
									            * <s:text name="jsp.default_240"/><br>
									            </span>
													<br>
													<sj:a id="cancelBillPayPayee" 
										                button="true" 
										                summaryDivId="summary" buttonType="cancel"
														onClickTopics="showSummary,cancelBillpayForm,cancelPayeeLoadForm"
										        	><s:text name="jsp.default_82"/></sj:a>
										        	
										        	<sj:a 
														id="verifyBillpayPayeeSubmit"
														formIds="BillpayPayeeNew"
						                                targets="verifyDiv" 
						                                button="true" 
						                                validate="true" 
						                                validateFunction="customValidation"
						                                onBeforeTopics="beforeVerifyBillpayForm"
						                                onCompleteTopics="verifyBillpayFormComplete"
														onErrorTopics="errorVerifyBillpayForm"
						                                onSuccessTopics="successVerifyBillpayForm"
								                        ><s:text name="jsp.default_291"/></sj:a>
													</div></td>
											<td></td>
										</tr>
									</table>
</s:form>
