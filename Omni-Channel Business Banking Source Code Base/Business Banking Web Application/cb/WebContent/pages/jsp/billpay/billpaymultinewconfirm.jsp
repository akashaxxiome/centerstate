<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaynewmulticonfirm" className="moduleHelpClass"/>
<div style="display:none">
			<span id="billpayResultMessage"><s:text name="jsp.billpay_121"/></span>
			<span id="billpayResultMessageTemplate"><s:text name="billpay_multiple_template_added"/></span>
		</div>
<s:form id="MultiBillpayNew" namespace="/pages/jsp/billpay" validate="false" action="addBillpayBatchAction_execute" method="post" name="frmPayBill" theme="simple">
<div class="leftPaneWrapper" role="form" aria-labelledby="paymentSummary">
	<div class="leftPaneInnerWrapper">
		<div class="header"><h2 id="paymentSummary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
		<div class="leftPaneLoadPanel summaryBlock"  style="padding: 15px 10px;">
			<div style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyMultipleBillPay_fromAccount"><s:text name="jsp.default_217"/>:</span>
					<span class="inlineSection floatleft labelValue"><s:property value="%{multiplePayment.account.accountDisplayText}"/></span>
				</div>
		</div>
	</div>
	<div class="leftPaneInnerWrapper totalAmountWrapper" id="verifyMultipleBillPay_total">
	                <span id="esimatedTotalLabel"><s:text name="jsp.default_431"/>:&nbsp;</span>
	                <%-- <ffi:cinclude value1="${isMultiCurrency}" value2="true" operator="equals"> </ffi:cinclude> --%>
	                <s:if test="%{isMultiCurrency}">
	                	&#8776;
	                </s:if>
	                 <span id="esimatedTotal">
	                 	<s:property value="%{total}"/>
	                 </span> 
	                 <span id="estimatedTotalCurrency">
	                 <%-- <ffi:getProperty name="selectedCurrencyCode"/> --%>
	                 <s:property value="%{multiplePayment.account.currencyCode}"/>
	                 </span>
	</div>
</div>

			<%--	<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">


                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
 <table width="100%" border="0" cellspacing="0" cellpadding="3" valign="top">
	<tr>
        <td colspan="2" class="sectionhead tbrd_b ltrow2_color">&gt; <s:text name="jsp.billpay_87"/></td>
    </tr>
    <tr>
        <td colspan="2" height="10"></td>
    </tr>
	<tr>
        <td width="20"></td>
        <td id="verifyMultipleBillPay_fromAccount">
            <span class="sectionsubhead"><s:text name="jsp.default_217"/>&nbsp;&nbsp;</span>
            <span class="columndata">
			<s:property value="%{multiplePayment.payments[0].account.accountDisplayText}"/>
            </span>
        </td>
	</tr>
</table> --%>
<div class="confirmPageDetails">	
<ffi:setProperty name="AddPaymentBatch" property="Payments.Filter" value="All" />
<s:iterator value="%{multiplePayment.payments}" status="listStatus" var="payment">
<div class="blockWrapper">
	<s:if test="%{#payment.AmountValue.CurrencyStringNoSymbol!='0.00'}">
		<h3 class="transactionHeading">
			<span id="verifyMultipleBillPay_payee[<s:property value="%{#listStatus.index}"/>]" class="columndata" >
						<s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
						<s:property value="#payment.payee.userAccountNumber"/>)
			</span>
		</h3>
		<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="verifyMultipleBillPay_amountLabel" class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_43"/>:</span>
					<span id="verifyMultipleBillPay_amount[<s:property value="%{#listStatus.index}"/>]" class="columndata"  >
						<s:property value="#payment.AmountValue.CurrencyStringNoSymbol"/>
						<s:property value="#payment.payee.PayeeRoute.CurrencyCode"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="verifyMultipleBillPay_sendDateLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_99"/>:</span>
					<span id="verifyMultipleBillPay_sendDate[<s:property value="%{#listStatus.index}"/>]" class="columndata">
	                <%-- <ffi:getProperty name="Payment1" property="PayDate"/> --%>
	                <s:property value="#payment.PayDate"/>	</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="verifyMultipleBillPay_deliverByDateLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_45"/>:</span>
					<span id="verifyMultipleBillPay_deliverByDate[<s:property value="%{#listStatus.index}"/>]" class="columndata">
		                <%-- <ffi:getProperty name="Payment1" property="DeliverByDate"/> --%>
		                <s:property value="#payment.deliverByDate"/>	
	                </span> 
				</div>
				<div class="inlineBlock">
					<span id="verifyMultipleBillPay_memoLabel" class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_279"/>:</span>
					<span id="verifyMultipleBillPay_memo[<s:property value="%{#listStatus.index}"/>]" class="columndata">
                	<s:property value="#payment.memo"/>	</span>
				</div>
			</div>
			<div class="blockRow">
					<span id="verifyMultipleBillPay_lastPayementLabel" class="sectionsubhead sectionLabel"  ><s:text name="jsp.billpay_last_payment_to_payee"/>:</span>
					<span id="verifyMultipleBillPay_lastPayement[<s:property value="%{#listStatus.index}"/>]" class="columndata instructions"   style="padding:2px;">
						<s:if test="%{lastPaymentToPayeeMap[#payment.payee.ID].size >0}">
						<s:iterator value="%{lastPaymentToPayeeMap[#payment.payee.ID]}" var="lastPaymentsList">
							<s:property value="#lastPaymentsList.Amount"/> - <s:property value="#lastPaymentsList.payDate"/>
						</s:iterator>
						</s:if>
						<s:else>
							<s:text name="jsp.billpay_none"/>
						</s:else>
					</span>
			</div>
		</div>
	</s:if>
</div>


</s:iterator>
<%-- <table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData ">
    <tr class="header">
		<td id="verifyMultipleBillPay_payeeNameLabel" class="sectionsubhead" width="20%"><s:text name="jsp.default_313"/></td>
		<td id="verifyMultipleBillPay_amountLabel" class="sectionsubhead" width="15%" align="right"><s:text name="jsp.default_43"/>&nbsp;&nbsp;</td>
        <td class="sectionsubhead" width="5%">&nbsp;</td>
        <ffi:cinclude value1="${PaymentTemplateFlag}" value2="true" operator="notEquals">
        <td id="verifyMultipleBillPay_sendDateLabel" class="sectionsubhead" width="10%"><s:text name="jsp.billpay_99"/>&nbsp;&nbsp;</td>
        <td id="verifyMultipleBillPay_deliverByDateLabel" class="sectionsubhead" width="10%"><s:text name="jsp.billpay_45"/>&nbsp;&nbsp;</td>
        </ffi:cinclude>
        <td id="verifyMultipleBillPay_memoLabel" class="sectionsubhead" width="20%" nowrap><s:text name="jsp.default_279"/></td>
        <td id="verifyMultipleBillPay_registerCategoryLabel" class="sectionsubhead" width="15%"><ffi:cinclude value1="${RegisterEnabled}" value2="true"><s:text name="jsp.billpay_89"/>&nbsp;&nbsp;</ffi:cinclude></td>
		<td id="verifyMultipleBillPay_lastPayementLabel" class="sectionsubhead" width="20%" nowrap><s:text name="jsp.billpay_last_payment_to_payee"/></td>
		
	</tr>
	<% int idCount=0; %>
	<ffi:setProperty name="AddPaymentBatch" property="Payments.Filter" value="All" />
	<s:iterator value="%{multiplePayment.payments}" status="listStatus" var="payment" >
		<s:if test="%{#payment.AmountValue.CurrencyStringNoSymbol!='0.00'}">
				<td id="verifyMultipleBillPay_payee[<s:property value="%{#listStatus.index}"/>]" class="columndata" height="20">
					<s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
				<s:property value="#payment.payee.userAccountNumber"/>)
				</td>
				<td id="verifyMultipleBillPay_amount[<s:property value="%{#listStatus.index}"/>]" class="columndata" nowrap align="right">
				
				<s:property value="#payment.AmountValue.CurrencyStringNoSymbol"/>
				<s:property value="#payment.payee.PayeeRoute.CurrencyCode"/>
				
				
				</td>
                <td class="columndata">&nbsp;</td>
                <ffi:cinclude value1="${PaymentTemplateFlag}" value2="true" operator="notEquals">
                <td id="verifyMultipleBillPay_sendDate[<s:property value="%{#listStatus.index}"/>]" class="columndata">
                <ffi:getProperty name="Payment1" property="PayDate"/>
                <s:property value="#payment.PayDate"/>	</td>
                </td>
                <td id="verifyMultipleBillPay_deliverByDate[<s:property value="%{#listStatus.index}"/>]" class="columndata">
                <ffi:getProperty name="Payment1" property="DeliverByDate"/>
                <s:property value="#payment.deliverByDate"/>	
                </td>                
                </ffi:cinclude>
                <td id="verifyMultipleBillPay_memo[<s:property value="%{#listStatus.index}"/>]" class="columndata">
                <ffi:getProperty name="Payment1" property="Memo"/>
                <s:property value="#payment.memo"/>	</td>
                <td class="columndata">
				   <!-- ====================== BEGIN REGISTER INTEGRATION ===================== -->
					<ffi:cinclude value1="${RegisterEnabled}" value2="true" operator="equals">
						<ffi:setProperty name="RegisterCategories" property="Current" value="${Payment1.REGISTER_CATEGORY_ID}"/>
						<ffi:setProperty name="Parentfalse" value=""/>
						<ffi:setProperty name="Parenttrue" value=": "/>
						<ffi:getProperty name="RegisterCategories" property="ParentName"/><ffi:getProperty name="Parent${RegisterCategories.HasParent}"/><ffi:getProperty name="RegisterCategories" property="Name"/>
						<br>
					</ffi:cinclude>
					<!-- ====================== END REGISTER INTEGRATION ===================== -->
				</td>
				
				
				<!-- last payments -->
				<td>
				</td>
				<td id="verifyMultipleBillPay_lastPayement[<s:property value="%{#listStatus.index}"/>]" class="columndata instructions" colspan="2" align="left" style="padding:2px;">
					<s:if test="%{lastPaymentToPayeeMap[#payment.payee.ID].size >0}">
					<s:iterator value="%{lastPaymentToPayeeMap[#payment.payee.ID]}" var="lastPaymentsList">
						<s:property value="#lastPaymentsList.Amount"/> - <s:property value="#lastPaymentsList.payDate"/>
					</s:iterator>
					</s:if>
					<s:else>
						<s:text name="jsp.billpay_none"/>
					</s:else>
				</td>
				<% idCount++; %>
			</tr>
		</ffi:cinclude>
	</ffi:list>
	</s:if>
	</s:iterator>
	<tr>
        <tr>
            <td id="verifyMultipleBillPay_total" class="sectionsubhead" align="right" colspan="2">
                <span id="esimatedTotalLabel"><s:text name="jsp.default_431"/>:&nbsp;</span>
                <ffi:cinclude value1="${isMultiCurrency}" value2="true" operator="equals"> </ffi:cinclude>
                <s:if test="%{isMultiCurrency}">
                	&#8776;
                </s:if>
                 <span id="esimatedTotal">
                 	<s:property value="%{total}"/>
                 </span> 
                 <span id="estimatedTotalCurrency">
                 <ffi:getProperty name="selectedCurrencyCode"/>
                 <s:property value="%{multiplePayment.account.currencyCode}"/>
                 </span>
            </td>
        </tr>

 <tr>
											<td colspan="8"><div align="center">
<br>
    <ffi:cinclude value1="${DisplayEstimatedAmountKey}" value2="true">
        <span class="required"><ffi:cinclude value1="${isMultiCurrency}" value2="true" operator="equals">&#8776; <s:text name="jsp.default_241"/></span></ffi:cinclude><br><br>
        <ffi:removeProperty name="DisplayEstimatedAmountKey"/>
    </ffi:cinclude>
													<sj:a id="cancelMultiBillpaySend" 
										                button="true" 
														onClickTopics="cancelBillpayForm"
										        	><s:text name="jsp.default_82"/></sj:a>
										        	
												     <s:url action="addBillpayBatchAction_initializeBillpayBatch" escapeAmp="false" 
										        		namespace="/pages/jsp/billpay" id="intializeTaskUrl"></s:url>
										        	<s:hidden id="hiddenInitTaskUrl" value="%{intializeTaskUrl}"></s:hidden>
										        	<sj:a id="backInitiateMultiFormButton" 
					            						button="true" 
														onClickTopics="backInitiateMultiFormButton"
													><s:text name="jsp.default_57"/></sj:a>
											   
													<sj:a 
														id="sendMultiBillpaySubmit"
														formIds="MultiBillpayNew"
						                                targets="confirmDiv" 
						                                button="true" 
						                                onBeforeTopics="beforeSendBillpayForm"
						                                onSuccessTopics="sendBillpayFormSuccess"
						                                onErrorTopics="errorSendBillpayForm"
						                                onCompleteTopics="sendBillpayFormComplete"
			                        				><s:text name="jsp.billpay_100"/></sj:a>

<br><br>
</td></tr>
</table> --%>
<%-- ================= MAIN CONTENT END ================= --%>
<div class="btn-row">
<sj:a id="cancelMultiBillpaySend" 
            button="true" 
            summaryDivId="summary" buttonType="cancel"
onClickTopics="showSummary,cancelBillpayForm"
    	><s:text name="jsp.default_82"/></sj:a>

<s:url action="addBillpayBatchAction_initializeBillpayBatch" escapeAmp="false" 
 		namespace="/pages/jsp/billpay" id="intializeTaskUrl"></s:url>
<s:hidden id="hiddenInitTaskUrl" value="%{intializeTaskUrl}"></s:hidden>
<sj:a id="backInitiateMultiFormButton" 
    						button="true" 
	onClickTopics="backInitiateMultiFormButton"
><s:text name="jsp.default_57"/></sj:a>

<sj:a 
	id="sendMultiBillpaySubmit"
	formIds="MultiBillpayNew"
       targets="confirmDiv" 
       button="true" 
       onBeforeTopics="beforeSendBillpayForm"
       onSuccessTopics="sendBillpayFormSuccess"
       onErrorTopics="errorSendBillpayForm"
       onCompleteTopics="sendBillpayFormComplete"
><s:text name="jsp.default_378"/></sj:a>
</div>
</div>
</s:form>
<%-- <script language="javascript">
 var total = 0;
 var total1 = 0;
 var tempValue=0;
 var tempValue1=0;
 var frm = document.getElementById("MultiBillpayNew");
 var length=0;
 var length1=0;
 if(frm.convertedAmount != null)
 
 length =frm.convertedAmount.length;
 if(frm.convertedAmount1 != null)
  length1=frm.convertedAmount1.length;
 
 var cnt=0;
  
 if(length == 0 ||isNaN(length)){
 
  if(frm.convertedAmount != null){
 tempValue= frm.convertedAmount.value;
 if (tempValue != "" && !isNaN(tempValue))
          {
              total += parseFloat(tempValue);
         }
 
  }
 }
 else{
 
 for(cnt=0;cnt<length;cnt++){
 
 tempValue= frm.convertedAmount[cnt].value;

 
 if (tempValue != "" && !isNaN(tempValue))
         {
             total += parseFloat(tempValue);
        }
 
     }
 }
 /*Code for ESt total calculation if currency is same*/
 
 if(length1 == 0 ||isNaN(length1)){
  if(frm.convertedAmount1 != null)
  {
  
  tempValue1= frm.convertedAmount1.value;
 
  if (tempValue1 != "" && !isNaN(tempValue1))
           {
               total1 += parseFloat(tempValue1);
          }
  
   }
  
  }
  else{
  
  for(cnt=0;cnt<length1;cnt++){
  
  tempValue1= frm.convertedAmount1[cnt].value;
 
  if (tempValue1 != "" && !isNaN(tempValue1))
          {
              total1 += parseFloat(tempValue1);
         }
  
      }
 }
 
 
 total=total+total1;
 total = ns.common.formatTotal(total);
 var checkMultiCurrency= document.getElementById("checkMultiCurrencyValue").value;
 var payeeCommonCurrency=document.getElementById("payeeCommonCurrency").value;
 var bankAccountCurrency=document.getElementById("bankAccountCurrency").value;
  if(checkMultiCurrency =="true")
{	

   document.getElementById("esimatedTotal").innerHTML = total;
   document.getElementById("calculatedAmount").value = total;
   
}
 if(checkMultiCurrency == "false"){
 if(payeeCommonCurrency == "" || payeeCommonCurrency != bankAccountCurrency){
 	
   document.getElementById("esimatedTotal").innerHTML = total;
   document.getElementById("calculatedAmount").value = total;
 }
 else{	
 
  document.getElementById("esimatedTotal").innerHTML ="";
  document.getElementById("esimatedTotalLabel").innerHTML ="";
  document.getElementById("esimatedTotalCurrency").innerHTML ="";
  }
 }
</script>
 --%>