<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:help id="payments_billpaySingleTemplatesSummary" className="moduleHelpClass"/>
	<ffi:setGridURL grid="GRID_singleBillpayTemplates" name="ViewURL" url="/cb/pages/jsp/billpay/billpayViewBillForTemplate_single.action?ID={0}&PaymentType={1}" parm1="PaymentType" parm0="ID"/>
	<ffi:setGridURL grid="GRID_singleBillpayTemplates" name="LoadURL" url="/cb/pages/jsp/billpay/addBillpayAction_loadTemplateData.action?ID={0}&PaymentType={1}" parm0="ID" parm1="PaymentType" />
	<ffi:setGridURL grid="GRID_singleBillpayTemplates" name="EditURL" url="/cb/pages/jsp/billpay/editBillPayTemplateAction_init.action?ID={0}&PaymentType={1}" parm1="PaymentType" parm0="ID"/>
	<ffi:setGridURL grid="GRID_singleBillpayTemplates" name="DeleteURL" url="/cb/pages/jsp/billpay/deleteBillPayTemplateAction_init.action?ID={0}&Collection=PaymentTemplates&PaymentType={1}" parm0="ID" parm1="PaymentType"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/billpay/getBillpayTemplatesAction_getSingleBillpayTemplate.action?collectionName=singlePaymentTemplates&GridURLs=GRID_singleBillpayTemplates" URLEncrypt="true"/>
    <s:url id="billpayTempUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="singleBillpayTempGridId"  
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{billpayTempUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,singleBillpayTempGridCompleteEvents"
	> 
		
		<sjg:gridColumn name="templateName" index="templateSearchCriteria.templateName" title="%{getText('jsp.default_416')}" sortable="true" width="55"/>
		<sjg:gridColumn name="payeeName" index="templateSearchCriteria.payeeName" title="%{getText('jsp.default_598')}" formatter="ns.billpay.formaAccountAndPayee" sortable="true" width="100"/>
		
		<%-- <ffi:cinclude value1="${UserType}" value2="Corporate" operator="equals">
			<sjg:gridColumn name="account.nickName" id="accountNickName" index="templateSearchCriteria.accountNickName" title="%{getText('jsp.default_15')}" sortable="true" width="100"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${UserType}" value2="Consumer" operator="equals">
			<sjg:gridColumn name="account.consumerDisplayText" id="accountNickName" index="accountNickName" title="%{getText('jsp.default_15')}" sortable="true" width="100"/>
		</ffi:cinclude>
		 --%>
		<sjg:gridColumn name="account.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.default_15')}" search="false" sortable="false" width="120" hidden=""/>
		<sjg:gridColumn name="account.currencyCode" index="accountCurrencyCode" title="%{getText('jsp.billpay_39')}" search="false" sortable="false" width="120" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.default_27')}" width="100" sortable="false" search="false" formatter="ns.billpay.formatSingleBillpayTemplatesActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
		
		<sjg:gridColumn name="map.canLoad" index="map.canLoad" title="%{getText('jsp.default_79')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.canDelete" index="map.canDelete" title="%{getText('jsp.default_79')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.canEdit" index="map.canEdit" title="%{getText('jsp.default_80')}" sortable="true" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>		
		<sjg:gridColumn name="LoadURL" index="LoadURL" title="%{getText('jsp.default_265')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		
	</sjg:grid>
	
	<script type="text/javascript">
	<!--
		$("#singleBillpayTempGridId").jqGrid('setColProp','ID',{title:false});
	//-->
	</script>	
