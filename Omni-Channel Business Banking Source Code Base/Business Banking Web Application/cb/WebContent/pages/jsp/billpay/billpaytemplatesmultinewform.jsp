<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page contentType="text/html; charset=UTF-8"
	import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<ffi:help id="payments_billpaynewmultitemplate" className="moduleHelpClass"/>
<% boolean canImport = false; %>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
 	<% canImport = true; %>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS_SWIFT_FILE_UPLOAD %>">
	<% canImport = true; %>
</ffi:cinclude>

<%-- if Logged in user is consumer then dont show file import --%>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<% canImport = false; %>
</ffi:cinclude>
<span class="shortcutPathClass" style="display:none;">pmtTran_billpay,addMultipleTemplateLink</span>
<span class="shortcutEntitlementClass" style="display:none;">Payments</span>

<ffi:setProperty name='PageText' value=''/>


<script language="javascript">
$(function(){
	$("#AccountID").lookupbox({
		"controlType":"server",
		"collectionKey":"1",
		"module":"billpaymultinew",
		"size":"32",
		"source":"/cb/pages/jsp/billpay/BillPayAccountsLookupBoxAction.action"
	});
	
});
function clearRow(index)
{
	$('#amount'+index).val('');
	$('#memo'+index).val('');
}
</script>
<s:form id="BillpayMultipleNew" namespace="/pages/jsp/billpay" validate="true" action="addBillpayTemplateBatchAction_verify" method="post" name="BillpayMultipleNew" theme="simple">
<span style="display:none;" id="PageHeading"><s:text name="jsp.billpay_133"/></span>
		<input type="hidden" name="template" value="true">



<ffi:cinclude value1="0" value2="${BillPayAccounts.Size}" operator="equals">
<%-- <!-- IF BillPayAccounts COLLECTION IS EMPTY ie) not entitled to any accounts -->--%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td class="txt_normal" colspan="8" align="center">(<s:text name="jsp.billpay_108"/>)</td>
   </tr>
</table>
<br>
<center>
	<input type="button" name="BACK" value="<s:text name="jsp.default_57"/>" class="form_buttons" onClick="window.location='<ffi:urlEncrypt url="${SecurePath}payments/billpay.jsp"/>';">
</center>
</ffi:cinclude>
<ffi:cinclude value1="0" value2="${BillPayAccounts.Size}" operator="notEquals">

<%-- ================ MAIN CONTENT START ================ --%>
 
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>


<div class="instructions"></div>
<div class="leftPaneWrapper" role="form" aria-labelledby="template/accountDetail">

		<div class="leftPaneInnerWrapper leftPaneLoadPanel">
			<div class="header" id="multipleBillPayTemplate_accountLabel"><h2 id="template/accountDetail"><s:text name="jsp.billpay.Template_Account_Details"></s:text></h2></div>
			<div class="leftPaneInnerBox">
				<div class="marginTop10"><label for="TemplateName"><s:text name="jsp.billpay_template_name"/> </label><span class="required" >*</span></div>
				<div class="marginTop10">
					<input type="text" id="TemplateName" aria-required="true" class="ui-widget-content ui-corner-all" name="multiplePayment.templateName" size="35" maxlength="32" 
					value="<ffi:getProperty name="AddPaymentBatch" property="TemplateName"/>">
					
				</div>
				<div class="marginTop10"><span id="templateNameError"></span></div>
				<div class="marginTop10"><span id="multiplePayment.templateNameError"></span></div>
				<div class="marginTop10"><label for="AccountID"><s:text name="jsp.transfers_46"/></label> <span class="required" title="required">*</span></div>
				<div class="marginTop10">
					<select class="txtbox" id="AccountID" name="multiplePayment.acountId" aria-labelledby="AccountID" aria-required="true"></select>
				</div>
				<div class="marginTop10"><span id="multiplePayment.accountIDError"></span> <span id="acountIdError"></span></div>
			</div>
		</div>
		<% if (canImport) { %>
			<div class="leftPaneInnerWrapper" role="form" aria-labeledby="fileimport">
					<div class="header"><h2 id="fileimport"><s:text name="jsp.default_539" /></h2></div>
					<div class="leftPaneInnerBox" align="center">
						<sj:a name="fileImportBtn" id="fileImportBtn" onclick="ns.common.setModuleType('billpayTemplate',function(){$('#BillPayFileImportEntryLink').trigger('click');})" button="true">
							<s:text name="jsp.default_539.1" />
						</sj:a>
					</div>
			</div>
		<% } %>
	
</div>
<div class="rightPaneWrapper w71">
<div><span id="amountError"></span></div>
<div><span id="paymentsError"></span></div>
<div class="paneWrapper" role="form" aria-labelledby="paymentDetail">
<div class="paneInnerWrapper">
<div class="header"><h2 id="paymentDetail"><s:text name="jsp.billpay_77"/></h2></div>
<div class="paneContentWrapper">
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="tableData formTablePadding10">
	<s:iterator value="%{multiplePayment.payments}" status="listStatus" var="payment">
	<tr>
		<td colspan="2" id="multipleBillPayTemplate_payeeNickNameValue<s:property value="%{#listStatus.index}"/>" class="">
			<h3 class="transactionHeading">
				<s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
				<s:property value="#payment.payee.userAccountNumber"/>)
			</h3>
		</td>
	</tr>
	<tr>
		<td id="multipleBillPayTemplate_amountLabel" class="sectionsubhead nowrap" width="50%">
		<label for="amount<s:property value="%{#listStatus.index}"/>"><s:text name="jsp.default_43"/></label><span class="required" title="required">*</span></td>
		<td id="multipleBillPayTemplate_memoLabel" class="sectionsubhead" width="50%">
		<label for="memo<s:property value="%{#listStatus.index}"/>"><s:text name="jsp.default_279"/></label></td>
	</tr>
        
		<tr>
			<td class="columndata" nowrap align="left">
			<input type="text" class="amountCls ui-widget-content ui-corner-all" value="<s:property value="#payment.AmountValue.AmountValue"/>"
			name="multiplePayment.payments[<s:property value="%{#listStatus.index}"/>].amount"
			size="12" maxlength="16" aria-required="true" id="amount<s:property value="%{#listStatus.index}"/>">
 			<s:property value="#payment.payee.payeeRoute.currencyCode"/>
 			<s:hidden value="%{#payment.payee.payeeRoute.currencyCode}" 
			name="multiplePayment.payments[%{#listStatus.index}].amtCurrency"></s:hidden>
			</td>
            <td align="left">
                <input type="text" class="ui-widget-content ui-corner-all"  value="<s:property value="#payment.memo"/>"
                name="multiplePayment.payments[<s:property value="%{#listStatus.index}"/>].memo" 
                maxlength="64" size="35" 
                id="memo<s:property value="%{#listStatus.index}"/>">
            </td>
        </tr>
        <tr>
			<td><span id="amount<s:property value='%{#listStatus.index}'/>Error"></span></td>
			<td><span id="memo<s:property value='%{#listStatus.index}'/>Error"></span></td>
		</tr>
		<tr>
        </tr>
	</s:iterator>
        <input type="hidden" name="checkMultiCurrency" id="checkMultiCurrency" value="<ffi:getProperty name="checkMultiCurrency"/>">
        <input type="hidden" name="showAmountTotal" id="showAmountTotal" value="<ffi:getProperty name="showAmountTotal"/>">
        
        <ffi:cinclude value1="${showAmountTotal}" value2="true" operator="equals">
        <tr>
			<td class="sectionsubhead" nowrap align="right"><s:text name="jsp.default_431"/>:</td>
            <td class="sectionsubhead" nowrap colspan="2">
                &nbsp;
                <span id="Total">--------</span>
                <span id="payeecommoncurrency1"></span>
            </td>
		</tr>
        </ffi:cinclude>
	</ffi:cinclude>
</table>
<div align="center" class="required marginTop10">* <s:text name="jsp.default_240"/></div>
<div class="btn-row">
<ffi:cinclude value1="0" value2="${AddPaymentBatch.Payments.Size}" operator="equals">
    <sj:a id="cancelMultiFormButton" 
	    button="true" 
	    summaryDivId="summary" buttonType="cancel"
		onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
	><s:text name="jsp.default_82"/></sj:a>
</ffi:cinclude>
    <ffi:cinclude value1="0" value2="${AddPaymentBatch.Payments.Size}" operator="notEquals">
<ffi:cinclude value1="${PaymentTemplateFlag}" value2="true" operator="equals">
   <sj:a id="cancelMultiFormButton" 
	    button="true" 
	    summaryDivId="summary" buttonType="cancel"
		onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
	><s:text name="jsp.default_82"/></sj:a>

	<sj:a 
		id="verifyMutiBillpaySubmit"
		formIds="BillpayMultipleNew"
	    targets="verifyDiv" 
        button="true" 
        validate="true" 
        validateFunction="customValidation"
        onBeforeTopics="beforeVerifyBillpayForm"
        onCompleteTopics="verifyBillpayFormComplete"
		onErrorTopics="errorVerifyBillpayForm"
	    onSuccessTopics="successVerifyBillpayForm"
	 ><s:text name="jsp.default_291"/></sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${PaymentTemplateFlag}" value2="true" operator="notEquals">
	
	<sj:a id="cancelMultiFormButton" 
	    button="true" 
	    summaryDivId="summary" buttonType="cancel"
		onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
	><s:text name="jsp.default_82"/></sj:a>
	     	
	<sj:a 
		id="verifyMutiBillpaySubmit"
		formIds="BillpayMultipleNew"
	    targets="verifyDiv" 
        button="true" 
        validate="true" 
        validateFunction="customValidation"
        onBeforeTopics="beforeVerifyBillpayForm"
        onCompleteTopics="verifyBillpayFormComplete"
		onErrorTopics="errorVerifyBillpayForm"
	    onSuccessTopics="successVerifyBillpayForm"
	 ><s:text name="jsp.default_291"/></sj:a>
</ffi:cinclude>
</ffi:cinclude>
</div></div></div></div></div>
<%-- ================= MAIN CONTENT END ================= --%>
</s:form>
