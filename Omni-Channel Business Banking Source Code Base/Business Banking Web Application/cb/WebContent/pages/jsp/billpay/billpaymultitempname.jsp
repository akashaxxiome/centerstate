<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
<!--
	//This function is used to check event of enter key.
	$("#MultiBillpayName").keypress(function(e) {  
		if (e.which == 13) {  
			$("#saveMultiBillpayTemplateName").click();
			return false;
		}   
	});	
//-->



</script>

				<div align="center">
					<s:form id="MultiBillpayName" validate="false" action="addBillpayTemplateBatchAction_saveAsTemplate" namespace="/pages/jsp/billpay" method="post" name="NewMultiBillpayTemplate" theme="simple">
                	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<input type="hidden" name="template" value="true">
					<table width="100%" border="0" cellspacing="0" cellpadding="3" >
						<%-- <tr>
							<td class="tbrd_b sectionhead" align="left">&gt; <s:text name="jsp.billpay_91"/></td>
						</tr>
						<tr><td height="10"></td></tr>
						<tr>
							<td align="left" class="sectionsubhead"><s:text name="jsp.default_290"/></td>
						</tr> --%>
						<tr>
							<td align="center">
								<input placeholder="Enter Template Name" name="templateName" value="" size="25" maxlength="32" class="ui-widget-content ui-corner-all"/>
								 <sj:submit 
										id="saveMultiBillpayTemplateName"
										targets="resultmessage"
		                                button="true" 
		                                validate="true" 
					                    validateFunction="customValidation"
		                                onBeforeTopics="beforeSaveSingleBillpayTemplate"
		                                onSuccessTopics="saveBillpayBatchTemplateSuccess"
		                                onErrorTopics="errorSaveSingleBillpayTemplate"
		                                onCompleteTopics="saveBillpayTemplateComplete"
		                                value="%{getText('jsp.default_366')}" 
		                                cssClass="floatRight formSubmitBtn"
			                        />
							</td>
						</tr>
						<tr>
							<td align="center">
								<span id="multiplePayment.templateNameError"></span>
								<span id="templateNameError"></span>
							</td>
						</tr>
						<%-- <tr>
						
							<td align="center"><br>
						            <sj:submit 
										id="saveMultiBillpayTemplateName"
										targets="resultmessage"
		                                button="true" 
		                                validate="true" 
					                    validateFunction="customValidation"
		                                onBeforeTopics="beforeSaveSingleBillpayTemplate"
		                                onSuccessTopics="saveBillpayBatchTemplateSuccess"
		                                onErrorTopics="errorSaveSingleBillpayTemplate"
		                                onCompleteTopics="saveBillpayTemplateComplete"
		                                value="%{getText('jsp.default_366')}" 
			                        />
							</td>
						</tr> --%>
					</table>
			</s:form>
		</div>

