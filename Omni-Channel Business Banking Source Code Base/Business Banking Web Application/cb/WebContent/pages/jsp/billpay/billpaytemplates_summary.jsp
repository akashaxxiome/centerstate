<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<!-- Check AppType of user -->
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:setProperty name="UserType" value="Corporate"/>
</ffi:cinclude>	
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:setProperty name="UserType" value="Consumer"/>
</ffi:cinclude>


	<%-- <ffi:object id="billtemptabs" name="com.ffusion.tasks.util.TabConfig"/>
	<ffi:setProperty name="billtemptabs" property="TransactionID" value="BillTemplate"/>
	
	<ffi:setProperty name="billtemptabs" property="Tab" value="single"/>
	<ffi:setProperty name="billtemptabs" property="Href" value="/cb/pages/jsp/billpay/billpaytemplates_single_summary.jsp"/>
	<ffi:setProperty name="billtemptabs" property="LinkId" value="single"/>
	<ffi:setProperty name="billtemptabs" property="Title" value="jsp.billpay_103"/>
	
	<ffi:setProperty name="billtemptabs" property="Tab" value="multiple"/>
	<ffi:setProperty name="billtemptabs" property="Href" value="/cb/pages/jsp/billpay/billpaytemplates_multi_summary.jsp"/>
	<ffi:setProperty name="billtemptabs" property="LinkId" value="multiple"/>
	<ffi:setProperty name="billtemptabs" property="Title" value="jsp.billpay_62"/>
			
	<ffi:process name="billtemptabs"/> --%>
	
	<div id="billpayTempGridTabs" class="portlet gridPannelSupportCls" style="float: left; width: 100%;" role="section" aria-labelledby="summaryHeader">
	
		<div class="portlet-header">
		<h1 id="summaryHeader" class="portlet-title"><s:text name="jsp.billpay.template_summary" /></h1>
		    <div class="searchHeaderCls">
				<div class="summaryGridTitleHolder" style="margin-left: 20px;">
					<span id="selectedGridTab" class="selectedTabLbl" style="padding-left: 0;">
						<s:text name="jsp.billpay_103"/>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
					
					<div class="gridTabDropdownHolder">
						<s:iterator value="%{orderedBillpayTemplateTabs}" var="tabName">
							<s:if test="%{#tabName == 'single'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('single')" >
									<s:text name="jsp.billpay_103"/>
								</span>
							</s:if>
							<s:elseif test="%{#tabName == 'multiple'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('multiple')">
									<s:text name="jsp.billpay_62"/>
								</span>
							</s:elseif>
						</s:iterator>
					</div>
				</div>
			</div>
	    </div>
	   <div class="portlet-content">
	    	<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="singleSummaryGrid" gridId="singleBillpayTempGridId" class="gridSummaryContainerCls" >
					<s:action namespace="/pages/jsp/billpay" name="getBillpayTemplateGridTabAction_getSingleTemplate" executeResult="true"/>
				</div>
				<div id="multipleSummaryGrid" gridId="multiBillpayTempGridId" class="gridSummaryContainerCls hidden" >
					<s:action namespace="/pages/jsp/billpay" name="getBillpayTemplateGridTabAction_getBatchTemplate" executeResult="true"/>
				</div>
			</div>
	    </div>
	   
	   
	   
	  <%--   <ul>
			<s:iterator value="%{orderedBillpayTemplateTabs}" var="tabName">
			<s:if test="%{#tabName == 'single'}">
				<li id="pendingApprovalTransfersListTab">
					<a href='/cb/pages/jsp/billpay/getBillpayTemplateGridTabAction_getSingleTemplate.action' id='<s:property value="%{#tabName}"/>'>
						<s:text name="jsp.transfers_72"/>
					</a>
				</li>
			</s:if>
			<s:elseif test="%{#tabName == 'multiple'}">
				<li id="pendingTransfersListTab">
					<a href='/cb/pages/jsp/billpay/getBillpayTemplateGridTabAction_getBatchTemplate.action' id='<s:property value="%{#tabName}"/>'>
						<s:text name="jsp.transfers_56"/>
					</a>
				</li>
			</s:elseif>
		</s:iterator>
		
			<a id="tabChangeNote" style="height:0px;  position:relative; top:3px; left:10px; display:inline-block;"></a>
			<a id="tabRevertNote" style="height:0px;  position:relative; top:3px; left:20px; display:inline-block;"></a>
	    </ul> --%>
		<input type="hidden" id="getTransID" value="<s:property value="%{moduleId}"/>" />	
		<div id="billPayTemplateSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('billpayTempGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	</div>
	
	
<script>    

	/* $('#billpayTempGridTabs').portlet({
		helpCallback: function(){
			
			var helpFile = "";
			var helpFile = "";
			$('#billpayTempGridTabs').find('.ui-tabs-panel').each(function(){
			    if($(this).attr('aria-hidden') == "false"){
			    	helpFile = $(this).find('.moduleHelpClass').html();
			    }
			})
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
	});
	
	$("#billpayTempGridTabs").hover(
			  function () {
			    $("#billpayTempGridTabs .headerctls").show();
			  }, 
			  function () {
			    $("#billpayTempGridTabs .headerctls").hide();
			  }
			); */
	//Initialize portlet with settings icon
	ns.common.initializePortlet("billpayTempGridTabs");
</script>