<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@page import="com.ffusion.tasks.billpay.Task"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<style type="text/css">
#appdashboard-left, #appdashboard-right{display:inline-block; margin-top:10px; margin-left:10px;}
</style>

<ffi:setProperty name="IsConsumerUser" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true"/>
</ffi:cinclude>
<!-- New dashboard implementation starts -->
<div class="dashboardUiCls">
	<div class="moduleSubmenuItemCls dropdown">
		<ul class="dropdownMenuBar">
			<li class="dropdownMenuParent" title='<s:text name="jsp.submenuDropDown.title"/>'>
	    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-billPay"></span>
	    		<span class="moduleSubmenuLbl">
	    			<s:text name="jsp.default_74" />
	    		</span>
	    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
				
				<!-- dropdown menu include -->    		
	    		<s:include value="/pages/jsp/home/inc/transferSubmenuDropdown.jsp" />
    		</li>
    	</ul>	
   	</div>
    
    <!-- dashboard items listing for transfers -->
        <div id="dbBillPaySummary" class="dashboardSummaryHolderDiv">
        	
        	<s:url id="goBackSummaryUrl"
				value="/pages/jsp/billpay/initBillpay_summary.action" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
		
			<sj:a id="goBackSummaryLink" href="%{goBackSummaryUrl}"
				targets="summary" button="true" cssClass="summaryLabelCls"
				title="%{getText('jsp.billpay_go_back_summary')}"
				onClickTopics="summaryLoadFunction"
				onCompleteTopics="calendarToSummaryLoadedSuccess">
				  	<s:text name="jsp.billpay_summary" />
			</sj:a>
			
        	<s:url id="singleBillpayUrl"
				value="/pages/jsp/billpay/addBillpayAction_init.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="addSingleBillpayLink" href="%{singleBillpayUrl}"
				targets="inputDiv" button="true"
				title="%{getText('jsp.billpay_129')}"
				onClickTopics="beforeLoadBillpayForm,loadSingleBillpayForm"
				onSuccessTopics="loadBillpayFormComplete"
				onErrorTopics="errorLoadBillpayForm"
				>
					<s:text name="jsp.billpay.single_payment" />
			</sj:a>
		
			<s:url id="multipleBillpayUrl"
				value="/pages/jsp/billpay/addBillpayBatchAction_init.action"
				escapeAmp="false">
				<s:param name="Initialize" value="%{'true'}" />
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="addMultipleBillpayLink" href="%{multipleBillpayUrl}"
				targets="inputDiv" button="true"
				title="%{getText('jsp.billpay_131')}"
				onClickTopics="beforeLoadBillpayForm,loadMultipleForm,loadMultipleBillPayForm"
				onCompleteTopics="loadMultipleBillpayFormComplete"
				onErrorTopics="errorLoadBillpayForm">
				  	<s:text name="jsp.billpay.multiple_payment" />
			</sj:a>
			
			<s:url id="editSingleBillpayUrl"
				value="/pages/jsp/billpay/editBillpayAction_init.action"
				escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="ID" value="%{ID}"></s:param>
				<s:param name="recPaymentID" value="%{recPaymentID}"></s:param>
			</s:url>
			<sj:a id="editSingleBillpayLink" href="%{editSingleBillpayUrl}"
				targets="inputDiv" button="true"
				cssStyle="display:none;" title="%{getText('jsp.billpay_134')}"
				onClickTopics="beforeLoadBillpayForm"
				onCompleteTopics="loadBillpayFormComplete"
				onErrorTopics="errorLoadBillpayForm">
				<s:text name="EDIT_SINGLE_BILL" />
			</sj:a>
				
        </div>	
        
    <!-- dashboard items listing for transfers -->
        <div id="dbPayeeSummary" class="dashboardSummaryHolderDiv">
        	
        		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYEES %>">
				<s:url id="payeesUrl"
					value="/pages/jsp/billpay/getBillpayPayeesAction_goToPayees.action"
					escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				<sj:a id="payeesLink" href="%{payeesUrl}" targets="summary"
					indicator="indicator" button="true"
					onErrorTopics="errorLoadBillpayPayees" cssClass="summaryLabelCls" onClickTopics="loadPayeeSummary"
					onSuccessTopics="billpayPayeeSummaryLoadedSuccess">
						<s:text name="jsp.billpay.payee_summary" />
				</sj:a>
				</ffi:cinclude>
				
        	<s:url id="billpayPayeeUrl"
				value="/pages/jsp/billpay/addBillpayPayeeAction_init.action"
				escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="Initialize" value="true"></s:param>
			</s:url>
			<sj:a id="addBillpayPayeeLink" href="%{billpayPayeeUrl}"
				targets="inputDiv" button="true"
				title="%{getText('jsp.billpay_129')}"
				onClickTopics="beforeLoadPayeeForm,loadPayeeForm"
				onBeforeTopics="beforeLoadBillpayForm"
				onCompleteTopics="loadBillpayFormComplete"
				onErrorTopics="errorLoadBillpayForm">
					<s:text name="jsp.billpay.add_payee" />
			</sj:a>
			
			
			<s:url id="billpayGlobalPayeeUrl"
				value="/pages/jsp/billpay/billpayGlobalPayeenewform.jsp"
				escapeAmp="false">
			</s:url>
			
			<sj:a id="addGlobalBillpayPayeeLink" href="%{billpayGlobalPayeeUrl}"
				targets="inputDiv" button="true"
				title="%{getText('jsp.billpay_129')}"
				onBeforeTopics="beforeLoadBillpayForm,loadGlobalPayeeForm"
				onClickTopics="beforeLoadPayeeForm"
				onCompleteTopics="loadBillpayFormComplete"
				onErrorTopics="errorLoadBillpayForm">
					<s:text name="jsp.billpay_11" />
			</sj:a>
        </div>	
    
    <!-- dashboard items listing for templates, hidden by default -->
		<div id="dbTemplateSummary" class="dashboardSummaryHolderDiv ">
			
			<s:url id="templatesUrl"
				value="/pages/jsp/billpay/getBillpayTemplatesAction_loadSummaryPage.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="templatesLink" href="%{templatesUrl}" targets="summary"
				indicator="indicator" button="true" cssClass="summaryLabelCls"
				onClickTopics="loadTemplateSummary"
				onSuccessTopics="billpaytemplatesSummaryLoadedSuccess">
				<s:text name="jsp.default.templates.summary"/>
			</sj:a>
			
			<s:url id="singleBillpayTemplateUrl"
				value="/pages/jsp/billpay/addBillPayTemplateAction_init.action"
				escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="Initialize" value="true"></s:param>
			</s:url>
			<sj:a id="addSingleTemplateLink" href="%{singleBillpayTemplateUrl}"
				targets="inputDiv" button="true"
				title="%{getText('jsp.billpay_132')}"
				onClickTopics="beforeLoadBillpayForm,loadSingleBillpayTemplateForm"
				onCompleteTopics="loadBillpayFormComplete"
				onErrorTopics="errorLoadBillpayForm">
				  	<s:text name="jsp.billpay.single_template" />
			</sj:a>
		
			<s:url id="multipleBillpayTemplateUrl"
				value="/pages/jsp/billpay/addBillpayTemplateBatchAction_init.action"
				escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="Initialize" value="true"></s:param>
			</s:url>
			<sj:a id="addMultipleTemplateLink" href="%{multipleBillpayTemplateUrl}"
				targets="inputDiv" button="true"
				title="%{getText('jsp.billpay_133')}"
				onClickTopics="beforeLoadBillpayForm,loadMultipleForm,loadSingleBillpayMultiTemplateForm"
				onCompleteTopics="loadMultipleBillpayFormComplete"
				onErrorTopics="errorLoadBillpayForm">
				  	<s:text name="jsp.billpay.multiple_template" />
			</sj:a>
		</div>
		
	<!-- dashboard items listing for file upload, hidden by default -->
		<div id="dbFileUploadSummary" class="dashboardSummaryHolderDiv ">
				<% boolean canImport = false; %>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
				    <% canImport = true; %>
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS_SWIFT_FILE_UPLOAD %>">
				    <% canImport = true; %>
				</ffi:cinclude>
				
				<sj:a
					id="billPayFileUploadSummary"
					indicator="indicator"
					cssClass="summaryLabelCls"
					button="true"
				><s:text name="jsp.default.file.upload.summary"/></sj:a>
				
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
				<s:url id="CustomMappingsUrl" value="/pages/jsp/fileuploadcustom.jsp"
					escapeAmp="false">
					<s:param name="Section" value="%{'PAYMENT'}" />
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				<sj:a id="customMappingsLink" href="%{CustomMappingsUrl}"
					targets="mappingDiv" title="%{getText('jsp.default_135')}"
					indicator="indicator" button="true" 
					onClickTopics="customMappingsOnClickTopics"
					onCompleteTopics="customMappingsOnCompleteTopics"
					>
								  <s:text name="CUSTOM_MAPPINGS" />
				</sj:a>
				<s:url id="addCustomMappingUrl" value="/pages/jsp/custommapping.jsp"
					escapeAmp="false">
					<s:param name="SetMappingDefinitionID" value="0" />
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
				</s:url>
				<sj:a id="addCustomMappingsLink" href="%{addCustomMappingUrl}"
					targets="inputCustomMappingDiv" title="%{getText('jsp.default_135')}"
					indicator="indicator" button="true"
					onClickTopics="beforeCustomMappingsOnClickTopics"
					onCompleteTopics="addCustomMappingsOnCompleteTopics"
					onErrorTopics="errorCustomMappingsOnErrorTopics" style="display:none;">
					<s:text name="jsp.default_34" />
				</sj:a>
				

					</ffi:cinclude>
				</ffi:cinclude>
				<%-- edit single bill --%>
				
				<%-- if Logged in user is consumer then dont show file import --%>
				<% if (canImport) { %>
				<!-- File upload summary -->
				<ffi:cinclude value1="${IsConsumerUser}" value2="false">
					<s:url id="FileImportUrl"
						value="/pages/jsp/billpay/paymentimport.jsp">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					</s:url>
					<sj:a id="BillPayFileImportEntryLink" href="%{FileImportUrl}"
						targets="selectImportFormatOfFileUploadID"
						title="%{getText('jsp.billpay_135')}" indicator="indicator"
						button="true" onClickTopics="onClickUploadBillpayFormTopics"
						onCompleteTopics="onCompleteUploadBillPayFormTopics,tabifyNotes">
							 <s:text name="jsp.default_539.1" />
					</sj:a>
				</ffi:cinclude>
				<% } %>
				
				
		</div>
		
		<!-- More list option listing -->
		<div class="dashboardSubmenuItemCls dropdown">
			<ul class="dropdownMenuBar">
				<li class="dropdownMenuParent" title='<s:text name="jsp.submenuDropDown.title"/>'>
					<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
		    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
				
					<!-- UL for rendering tabs -->
					<ul class="dashboardSubmenuItemList dropdownOptionHolder">
					
							<s:url id="billpayUrl" value="/pages/jsp/billpay/initBillpay_summary.action">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							</s:url>
							<li class="dashboardSubmenuItem dropdownMenuItem"><a id="showdbBillPaySummary" onclick="ns.common.refreshDashboard('showdbBillPaySummary',true)"><s:text name="jsp.billpay_1"/></a></li>
						
						
						<s:url id="templatesUrl"
							value="/pages/jsp/billpay/getBillpayTemplatesAction_loadSummaryPage.action">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<li class="dashboardSubmenuItem dropdownMenuItem"><a id="showdbTemplateSummary" onclick="ns.common.refreshDashboard('showdbTemplateSummary',true)"><s:text name="jsp.default_5"/></a></li>
						
						<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYEES %>">
							<s:url id="payeesUrl"
								value="/pages/jsp/billpay/getBillpayPayeesAction_goToPayees.action"
								escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							</s:url>
							<li class="dashboardSubmenuItem dropdownMenuItem"><a id="showdbPayeeSummary" onclick="ns.common.refreshDashboard('showdbPayeeSummary',true)"><s:text name="jsp.billpay_2"/></a></li>
						</ffi:cinclude>
					</ul>
				</li>
			</ul>				
		</div>
	
</div>
<!-- New dashboard implementation ends -->
<%-- <div id="appdashboard-left" class="formLayout floatleft">
	<sj:a id="quickSearchLink" button="true"
		onClickTopics="onClickQuickSearchBillpayFormTopics">
		<span class="sapUiIconCls icon-search"></span><s:text name="jsp.default_4" />
	</sj:a>

</div> --%>

<%-- <div id="appdashboard-right" class="floatleft">

	<span class="ui-helper-clearfix">&nbsp;</span>
</div> --%>
<%-- <span class="ui-helper-clearfix">&nbsp;</span> --%>

<%-- Following three dialogs are used for view, delete and inquiry bill pay --%>
<sj:dialog id="inquiryBillpayDialogID" cssClass="pmtTran_billpayDialog"
	title="%{getText('jsp.default_249')}" resizable="false" modal="true"
	autoOpen="false" closeOnEscape="true" showEffect="fold"
	hideEffect="clip" width="800">
</sj:dialog>

<sj:dialog id="viewBillpayDetailsDialogID"
	cssClass="pmtTran_billpayDialog" title="%{getText('jsp.billpay_28')}"
	modal="true" resizable="false" autoOpen="false" closeOnEscape="true"
	showEffect="fold" hideEffect="clip" width="900" height="500" cssStyle="overflow:hidden;">
</sj:dialog>

<sj:dialog id="viewPayeeDetailsDialogID"
	cssClass="pmtTran_billpayDialog"
	title="%{getText('jsp.billpay_payee_details')}" modal="true"
	resizable="false" autoOpen="false" closeOnEscape="true"
	showEffect="fold" hideEffect="clip" width="800" height="500" cssStyle="overflow:hidden;">
</sj:dialog>

<sj:dialog id="deleteBillpayDialogID" cssClass="pmtTran_billpayDialog"
	title="%{getText('jsp.billpay_43')}" modal="true" resizable="false"
	autoOpen="false" closeOnEscape="true" showEffect="fold"
	hideEffect="clip" width="900" height="500" cssStyle="overflow:hidden;">
</sj:dialog>

<sj:dialog id="skipBillpayDialogID" cssClass="pmtTran_billpayDialog"
	title="%{getText('jsp.billpay_141')}" modal="true" resizable="false"
	autoOpen="false" closeOnEscape="true" showEffect="fold"
	hideEffect="clip" width="900" height="500" cssStyle="overflow:hidden;">
</sj:dialog>

<sj:dialog id="deletePayeeDialogID" cssClass="pmtTran_billpayDialog"
	title="%{getText('jsp.billpay_payee_delete')}" modal="true"
	resizable="false" autoOpen="false" closeOnEscape="true"
	showEffect="fold" hideEffect="clip" width="800" height="500" cssStyle="overflow:hidden;">
</sj:dialog>

<sj:dialog id="deleteBillpayTempDialogID"
	cssClass="pmtTran_billpayDialog" title="%{getText('jsp.billpay_44')}"
	modal="true" resizable="false" autoOpen="false" closeOnEscape="true"
	showEffect="fold" hideEffect="clip" width="800" height="500" cssStyle="overflow:hidden;">
</sj:dialog>

<sj:dialog id="searchBankingDialogID" cssClass="pmtTran_billpayDialog"
	title="%{getText('jsp.billpay_93')}" resizable="false" modal="true"
	autoOpen="false" closeOnEscape="true" showEffect="fold"
	hideEffect="clip" width="800">
</sj:dialog>

<sj:dialog id="viewBillpayMultiTempDialogID"
	cssClass="pmtTran_billpayDialog" title="%{getText('jsp.billpay_28')}"
	modal="true" resizable="false" autoOpen="false" closeOnEscape="true"
	showEffect="fold" hideEffect="clip" width="900" height="500" cssStyle="overflow:hidden;">
</sj:dialog>

<%-- Following three dialogs are used for view, delete and inquiry bill pay --%>
<ffi:removeProperty name="IsConsumerUser" />
<script type="text/javascript">
	$(document).ready(function(){
		// Dashboard Top section dropdown menu
		$(".moduleSubmenuItemCls").dropdownmenu({
			menuPopOverClass : "moduleSubmenuDropdownHolderHover",
		});
		
		$(".dashboardSubmenuItemCls").dropdownmenu({
			menuPopOverClass : "dashboardSubmenuItemListHover",
		});
	});	
</script>