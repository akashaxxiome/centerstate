<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaypayee-company-add" className="moduleHelpClass"/>	
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_95')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>


<%-- Figure out display settings for the account search tables --%>
<%-- <ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount"/>
<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%= com.ffusion.tasks.GetDisplayCount.GLOBALPAYEES %>"/>
<ffi:setProperty name="GetDisplayCount" property="DisplayCountName" value="GlobalPayeeDisplaySize"/>
<ffi:process name="GetDisplayCount"/>
<ffi:removeProperty name="GetDisplayCount"/>
<ffi:setProperty name="numPayees" value="${AddGlobalPayees.Size}"/> --%>

<%-- <%!
int globalPayeeDisplaySize=0;
int noOfRows =0; %>
<%
	globalPayeeDisplaySize = Integer.parseInt( (String)session.getAttribute("GlobalPayeeDisplaySize") );
	String showall = request.getParameter("ShowAll");
	if(showall == null) {
		showall = "false";
	}
	session.setAttribute("ShowAll", showall);
%> --%>
	
<script type="text/javascript">

function showAddCompany(id,name,language,token) {
//	var	urlString = "/cb/pages/jsp/billpay/billpaypayee-company-add3.jsp";
	var	urlString = "/cb/pages/jsp/billpay/addBillpayPayeeAction_loadGlobalPayeeDetails.action";
	
	$.ajax({
		url: urlString,
		type: "POST",	
		/* data: 'payeeID=' + id + "&selectedPayeeName=" + name + "&selectedLanguage=" + language + "&CSRF_TOKEN=" + token, */
		data: 'payeeID=' + id +"&CSRF_TOKEN=" + token,
		success: function(data){
			$('#companyPayeeResultID').html(data);
			$('#companyPayeeResultID').show();
			$('#mainCompanyPayeeID').hide();
			$.publish("common.topics.tabifyDialog");
		}
	});
}

	function chkEnterKey(event) {
		if(event.keyCode == 13) {
			$(this).trigger("click");
		}
	}
</script>
    <div id="mainCompanyPayeeID">
    			<div>
    			
    			<tr><td>
    			<form action="<ffi:getProperty  name="SecurePath"/>billpaypayee-company-showall-wait.jsp" method="post" name="formAddCompanyShowAllPayee">
    			                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			    </form>
			
			    <form action="<ffi:urlEncrypt url="${SecurePath}billpaypayee-company-add3.jsp"/>" method="post" name="formAddGlobalPayees2">

     			      <div>
    			      <table width="750" border="0" cellspacing="0" cellpadding="0">
    				<table width="750" border="0" cellspacing="0" cellpadding="0">
    							
    			
    				
    										
    				<tr>
    				  <td></td>
    				  <td class="columndata ltrow2_color">
    			          <form action="billpayaddpayeeconfirm.jsp" method="post" name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
    				  <div>
    				  <table width="720" border="0" cellspacing="0" cellpadding="3">
    				 <tr>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
    				</tr>
    				<tr>
    				<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt;<s:text name="jsp.billpay_18"/><br>
    				</span></td></tr>		
    				     <tr><td colspan="6">
    				     
        <ffi:cinclude value1="${ShowAll}" value2="true" operator="equals">
        <TABLE>
            <TR>
                <TD CLASS="instructions"><s:text name="jsp.billpay_86"/></TD>
            </TR>
        </TABLE>
        <TABLE>
            <tr>

                <ffi:setProperty name="AddGlobalPayees" property="Filter" value="All"/>

                <ffi:setProperty name="Alpha" value=" ABCDEFGHIJKLMNOPQRSTUVWXYZ{"/>
                <ffi:object id="StringUtil" name="com.ffusion.beans.util.StringUtil" scope="request"/>
                <ffi:setProperty name="StringUtil" property="Value1" value="${Alpha}" />
                <ffi:object id="Paging" name="com.ffusion.beans.util.Paging" scope="session"/>
                <ffi:setProperty name="Paging" property="Pages" value="26" />

                <ffi:list collection="Paging" items="PageNum"><ffi:setProperty name="StringUtil" property="Range" value="${PageNum},${PageNum}" />
                    <ffi:setProperty name="startLetter" value="${StringUtil.Substring}"/>
                    <td CLASS="txt_normal" >
                        <sj:a cssClass="ui-button ui-widget ui-state-default ui-button-text-only" onclick="ns.billpay.payeeShowAll('%{#session.startLetter}')"><ffi:getProperty name="startLetter"/></sj:a>
                    </td>
                </ffi:list>
            </TR>
        </TABLE>
        <BR>
        <table>
            <TR>
                <TD CLASS="txt_normal"><FONT SIZE="10"><ffi:getProperty name="SearchName"/></FONT></TD>
            </TR>
        </TABLE>
        <BR>
        </ffi:cinclude>   				     
        <ffi:cinclude value1="${NoPayeeMatched}" value2="true">
            <s:text name="jsp.billpay_64"/>
        </ffi:cinclude>
        <%-- <ffi:cinclude value1="${NoPayeeMatched}" value2="false">
				    		         <ffi:cinclude value1="${AddGlobalPayees.Size}" value2="0" operator="notEquals" > --%>
					 				  	        <TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
					 				  	          
					 				  	            <ffi:cinclude value1="${ShowAll}" value2="true" operator="notEquals">
					 				  	            <TR>
					 				  	                <TD CLASS="instructions"><s:text name="jsp.billpay_85"/></TD>
					 				  	            </TR>
					 				  	            </ffi:cinclude> 
					 				  		</TABLE>
					 				  		<%-- <% noOfRows=0;%>
											<% int idCount=0;%> --%>
					 				  	         <TABLE width="100%" border="0" cellpadding="0" cellspacing="0">
					 				  	            <ffi:setProperty name="tempId" value=""/>
					 				  	            <%-- <ffi:list collection="AddGlobalPayees" items="Payee1">
					 				  	            	<%if(noOfRows < globalPayeeDisplaySize) {%>
					 				  				    <ffi:cinclude value1= "" value2="${tempId}" operator="equals">
					 				  				    	<ffi:setProperty name="tempId" value="${Payee1.ID}"/>
					 				  				    </ffi:cinclude>
					 				  			    
					 				  				    <ffi:cinclude value1= "${Payee1.ID}" value2="${tempId}" operator="notEquals">
					 				  					    <BR>
					 				  					    <BR>
					 				  					   
		<a id="globalPayee<%=idCount%>" href="#" onclick="showAddCompany('<ffi:getProperty name="Payee1" property="ID"/>','<ffi:getProperty name="Payee1" property="Name"/>','<ffi:getProperty name="Payee1" property="searchLanguage"/>','<ffi:getProperty name='CSRF_TOKEN'/>')"><ffi:getProperty name="Payee1" property="Name"/></a>
					 				  					    <% noOfRows++;%>
					 				  				    </ffi:cinclude>
					 				  				
					 				  				    <ffi:cinclude value1= "${Payee1.ID}" value2="${tempId}" operator="equals">
					 				  					     
		<a id="globalPayee<%=idCount%>" href="#" onclick="showAddCompany('<ffi:getProperty name="Payee1" property="ID"/>','<ffi:getProperty name="Payee1" property="Name"/>','<ffi:getProperty name="Payee1" property="searchLanguage"/>','<ffi:getProperty name='CSRF_TOKEN'/>')"><ffi:getProperty name="Payee1" property="Name"/></a>
					 				  				    </ffi:cinclude>
					 				  			   
					 				  				    <ffi:setProperty name="tempId" value="${Payee1.ID}"/>
					 				  			
					 				  		    	<% } %>
					 				  		    	<% idCount++;%>
					 				  	             </ffi:list> --%>
					 				  	             <s:iterator value="payeesList" var="payee">
					 				  	             <a class="link" onkeydown="chkEnterKey(event);" href="#" onclick="showAddCompany
					 				  	             ('<s:property value="%{#payee.iD}"/>',
					 				  	             '<s:property value="%{#payee.name}"/>',
					 				  	             '<s:property value="%{#payee.searchLanguage}"/>',
					 				  	             '<ffi:getProperty name='CSRF_TOKEN'/>')">
					 				  	             <%-- <ffi:getProperty name="Payee1" property="Name"/> --%>
					 				  	             <s:property value="%{#payee.name}"/>
					 				  	             </a>
					 				  	             <br>
					 				  	             </s:iterator>
					 				  		</TABLE>	      
              <%--  </ffi:cinclude>
            </ffi:cinclude> --%>
    				  </td>
    				  </tr>
    			        </table>
    			            </div>
    			            </form>
    			            </td>
    			            <td></td>
    				</tr>
    				<tr><td colspan="4"><div align="center">
				<br>
				<sj:a 
               		button="true" 
					onClickTopics="closeDialog"
       			><s:text name="jsp.default_102"/></sj:a>
			   </tr>
    				   
    					   </table>	  
    	</table>			
    	</div>
    	</form>
    	</td>
    	</tr>
    </div>
</div>
<div id = "companyPayeeResultID"></div>    
