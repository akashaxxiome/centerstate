<%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- <ffi:object name="com.ffusion.tasks.billpay.GetAllPaymentTemplates" id="GetAllPaymentTemplates" scope="session"/>
<ffi:setProperty name="GetAllPaymentTemplates" property="DataSetName" value="PaymentTemplates" />
<ffi:process name="GetAllPaymentTemplates"/> --%>

<%-- <ffi:object id="BuildIndex" name="com.ffusion.tasks.util.BuildIndex" scope="session"/>
	<ffi:setProperty name="BuildIndex" property="CollectionName" value="<%= SessionNames.PAYMENTTEMPLATES %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexName" value="<%= SessionNames.BILLPAY_TEMPLATE_INDEX %>"/>
	<ffi:setProperty name="BuildIndex" property="IndexType" value="<%= BuildIndex.INDEX_TYPE_TRIE %>"/>
	<ffi:setProperty name="BuildIndex" property="BeanProperty" value="TemplateUniqueKey"/>
	<ffi:setProperty name="BuildIndex" property="Rebuild" value="true"/>
<ffi:process name="BuildIndex"/>
<ffi:removeProperty name="BuildIndex"/>
 --%>

<script language="JavaScript" type="text/javascript">
<!--

$(function(){
	$("#SingleBillpayTemplateList").lookupbox({
		"source":"/cb/pages/jsp/billpay/BillPayTemplatesLookupBoxAction_loadSinglePaymentTemplate.action",
		"controlType":"server",
		"collectionKey":"1",
		"size":"25",
		"module":"billpayTemplates",
	});
	
	

});

	enableLoadButton();
	var selectedvalue="";
	function enableLoadButton()
	{
		selectedvalue = $('#SingleBillpayTemplateList').val();
		if ( selectedvalue == "" || selectedvalue == "-1" || selectedvalue == null ){
			$('#loadSingleBillpayTemplateSubmit').hide();			
		}
		else{
			$('#loadSingleBillpayTemplateSubmit').trigger('click');
			/* $('#loadSingleBillpayTemplateSubmit').removeAttr("style");	
			$('#loadSingleBillpayTemplateSubmit').show(); 
			$('#loadSingleBillpayTemplateSubmit').removeAttr("style");	*/
		}
	}

	$.subscribe('getTemplateData', function(event,data) {		
		$("#templateId").val($('#SingleBillpayTemplateList').val());
		
	});
// --></script>
<div align="center">
<s:form id="LoadSingleBillpayTemplateForm" name="LoadSingleBillpayTemplateForm" method="post" action="/pages/jsp/billpay/addBillpayAction_loadTemplateData.action" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden id="templateId" name="templateSearchCriteria.templateId"></s:hidden>
<ffi:setProperty name="FirstOption" value="true"/>
<div class="selectBoxHolder">
<s:set var="type" value="%{addRecPayment.paymentType}"></s:set>
					<s:if test='%{#type=="TEMPLATE" || #type=="RECTEMPLATE"}'>
						<s:select list="addRecPayment.templateName" id="SingleBillpayTemplateList" class="txtbox" name="LoadPaymentTemplate"
						 listKey="addRecPayment.ID" listValue="addRecPayment.templateName" onchange="enableLoadButton()" aria-labelledby="lblTemplateLoad">
						</s:select>
					</s:if>
					<s:else>
					<select class="txtbox" id="SingleBillpayTemplateList" title="Template select" name="LoadPaymentTemplate" onChange="enableLoadButton()" aria-labelledby="lblTemplateLoad">
</select>
					</s:else>
</div>
<%-- <ffi:setProperty name="PaymentTemplates" property="Filter" value="All"/> --%>
</s:form>
                
<sj:a
	id="loadSingleBillpayTemplateSubmit"
	targets="inputDiv"
    button="true"
    cssStyle="display:none"
    formIds="LoadSingleBillpayTemplateForm"
    onClickTopics="getTemplateData"
    onBeforeTopics="beforeLoadSingleBillpayTemplate"
    onSuccessTopics="loadSingleBillpayTemplateSuccess"
    onErrorTopics="errorLoadSingleBillpayTemplate"
    onCompleteTopics="loadSingleBillpayTemplateComplete"
><s:text name="jsp.default_263" />  </sj:a>

</div>
