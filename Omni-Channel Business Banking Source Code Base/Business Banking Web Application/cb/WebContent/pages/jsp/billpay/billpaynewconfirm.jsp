<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="payments_billpaynewconfirm" className="moduleHelpClass"/>

<script language="JavaScript">
/* function openSaveTemplate(URLExtra)  {
	MyWindow=window.open(''+URLExtra,'','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=750,height=340');
} */
</script>
<div style="display:none">
	<%-- <span id="billpayResultMessage"><s:text name="jsp.billpay_119"/></span> --%>
	<span id="billpayResultMessageTemplate"><s:text name="jsp.billpay_120"/></span>
</div>
<div class="SaveTemplate" role="form" aria-labeledby="summary">
	<div class="header"><h2 id="summary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
	<div class="leftPaneLoadPanel summaryBlock"  style="padding: 15px 10px;" id="leftPaneForDvc">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="addRecPayment.payee" property="Name" /></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_15"/>:</span>
				<span class="inlineSection floatleft labelValue"> <s:property value="%{addRecPayment.account.accountDisplayText}"/>
				</span>
			</div>
	</div>
</div>
<div class="confirmPageDetails rightPaneConfirmDvc">
<s:form id="BillpayNewSend" namespace="/pages/jsp/billpay" validate="false" action="addBillpayAction_execute" method="post" name="BillpayNewSend" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:if test="%{isDateChanged}">
	<div id="verifyBillPayWarningMessage" style="color:red" align="center">
		<s:text name="jsp.billpay_118"/>
	</div>
</s:if>
<div  class="blockHead toggleClick expandArrow">
<s:text name="jsp.billpay.payee_summary" />: <ffi:getProperty name="addRecPayment.payee" property="Name" /> (<s:property value="addRecPayment.Payee.NickName"/> - <ffi:getProperty name="addRecPayment.payee" property="UserAccountNumber" />)
<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
<div class="blockWrapper">
	<div  class="blockHead">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBillPayPayeeNicknameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_75"/>:</span>
				<span id="verifyBillPayPayeeNicknameValue" class="columndata"><s:property value="addRecPayment.Payee.NickName"/></span>
			</div>
			<!-- last payments -->
			<div class="inlineBlock">
				<span id="verifyBillPayLastPaymentLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_last_payment_to_payee"/>:</span>
				<span id="verifyBillPayLastPaymentValue" class="columndata">
					<ffi:setProperty name="LastPayments" property="Filter" value="PAYEEID=${AddPayment.PayeeID}"/>
					 <ffi:list collection="lastPaymentToPayee" items="lastPayment" startIndex="1" endIndex="1">
							<ffi:getProperty name="lastPayment" property="Amount"/>-<ffi:getProperty name="lastPayment" property="PayDate"/>
					</ffi:list>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  id="verifyBillPayPayeeNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_314"/>:</span>
				<span id="verifyBillPayPayeeNameValue" class="columndata"><ffi:getProperty name="addRecPayment.payee" property="Name" /></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyBillPayAccountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_15"/>:</span>
				<span id="verifyBillPayAccountValue"  class="columndata"><ffi:getProperty name="addRecPayment.payee" property="UserAccountNumber" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBillPayFirstAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
				<span id="verifyBillPayFirstAddressValue" class="columndata"><ffi:getProperty name="addRecPayment.payee" property="Street" /></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyBillPaySecondAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
				<span id="verifyBillPaySecondAddressValue" class="columndata"><ffi:getProperty name="addRecPayment.payee" property="Street2" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBillPayThirdAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
				<span id="verifyBillPayThirdAddressValue" class="columndata"><ffi:getProperty name="addRecPayment.payee" property="Street3" /></span>
			</div>
			<div class="inlineBlock">	
				<span id="verifyBillPayCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
				<span id="verifyBillPayCityValue" class="columndata"><ffi:getProperty name="addRecPayment.payee" property="City" /></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBillPayStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
				<span id="verifyBillPayStateValue" class="columndata"><s:property value="%{addRecPayment.payee.stateDisplayName}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyBillPayZipLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
				<span id="verifyBillPayZipValue" class="columndata"><ffi:getProperty name="addRecPayment.payee" property="ZipCode" /></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBillPayCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
				<%-- <ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" /> --%>
				<span id="verifyBillPayCountryValue" class="columndata">
					<ffi:getProperty name='addRecPayment.countryResource' property='Resource'/>
					<s:property value="%{addRecPayment.payee.countryDisplayName}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="verifyBillPayContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
				<span id="verifyBillPayContactValue" class="columndata"><ffi:getProperty name="addRecPayment.payee" property="ContactName" /></span>
			</div>
			</div>
			<div class="blockRow">
				<span id="verifyBillPayPhoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_83"/>:</span>
				<span id="verifyBillPayPhoneValue" class="columndata"><ffi:getProperty name="addRecPayment.payee" property="Phone" /></span>
			</div>
		</div>
</div>
</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart--><s:text name="jsp.transaction.summary.label" /><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBillPayAmountLabel" class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_43"/>:
				</span>
				<span id="verifyBillPayAmountValue" class="columndata">
					 <s:property value="addRecPayment.AmountValue.CurrencyStringNoSymbol"/>
					 <s:property value="addRecPayment.payee.PayeeRoute.CurrencyCode"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="verifyBillPayCurrencyLabel" class="sectionsubhead sectionLabel">
                     <s:if test="%{addRecPayment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else> <s:text name="jsp.billpay_40"/>:</s:else>
	            </span>
	            <span class="columndata" id="verifyBillPayCurrencyValue">
                  	 <s:if test="%{addRecPayment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else>
	        	    	&#8776; <s:property value="%{addRecPayment.convertedAmount}"/>  <s:property value="%{addRecPayment.account.currencyCode}"/>
	        	    </s:else>
				</span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBillPayDateLabel" class="sectionLabel sectionsubhead"><s:text name="jsp.billpay_76"/>:</span>
				<span id="verifyBillPayDateValue" class="columndata" >
					<ffi:getProperty name="addRecPayment" property="PayDate" />
				</span>
			</div>
			<div class="inlineBlock">
				<span id="verifyBillPayDeliveredByLabel" class="sectionsubhead sectionLabel">
						<s:text name="jsp.billpay_deliver_by"/>:
				</span>
				<span id="verifyBillPayDeliveredByValue" class="columndata">
					<ffi:setProperty name="AddPayment" property="DateFormat" value="${UserLocale.DateFormat}" />
					<%-- <ffi:getProperty name="addRecPayment" property="DeliverByDate"/> --%>	
					<s:property value="%{addRecPayment.deliverByDate}"/>											
				</span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBillPayRecurringLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_351"/>:</span>
				<span id="verifyBillPayRecurringValue" class="columndata">
					<ffi:getProperty name="addRecPayment" property="Frequency" />
				</span>	
			</div>
			<div class="inlineBlock">
				<span id="verifyBillPayFrequencyLabel" class="sectionLabel sectionsubhead">
					<s:text name="jsp.default_321"/>:
				</span>
				<span id="verifyBillPayFrequencyValue" class="columndata">
					<s:if test="%{addRecPayment.openEnded}">
						<s:text name="jsp.default_446"/>
					</s:if>
					<s:else>
						<s:property value="%{addRecPayment.NumberPayments}"/>
					</s:else>
				</span>	
			</div>
		</div>
		<div class="blockRow">
			<ffi:cinclude value1="${AddPayment.REGISTER_CATEGORY_ID}" value2="">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBillPayMemoLabel" class="sectionLabel sectionsubhead">
					<s:text name="jsp.default_279"/>:
				</span>
				<span id="verifyBillPayMemoValue" class="columndata"><ffi:getProperty name="addRecPayment" property="Memo" /></span>
			</div>
			</ffi:cinclude>
			<ffi:cinclude value1="${AddPayment.REGISTER_CATEGORY_ID}" value2="" operator="notEquals">
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_89"/>:</span>
					<span class="columndata">
						<ffi:setProperty name="RegisterCategories" property="Filter" value="All"/>
						<ffi:list collection="RegisterCategories" items="Category">
							<ffi:cinclude value1="${Category.Id}" value2="${AddPayment.REGISTER_CATEGORY_ID}">
								<ffi:setProperty name="RegisterCategories" property="Current" value="${Category.Id}"/>
								<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="">
									<ffi:getProperty name="Category" property="Name"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="" operator="notEquals">
									<ffi:getProperty name="RegisterCategories" property="ParentName"/>: <ffi:getProperty name="addRecPayment.category" property="Name"/>
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:list>
					</span>
				</div>
			</ffi:cinclude>
		</div>	
		
		
		
	</div>
</div>
			<!-- 	<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr> -->
										<%-- <ffi:cinclude value1="${AddPayment.DateChanged}" value2="true">
										<tr>	
											<td id="verifyBillPayWarningMessage" class="sectionsubhead" colspan="6" style="color:red" align="center">
											<s:text name="jsp.billpay_118"/><br><br>
											</td>
										</tr>
										</ffi:cinclude> --%>
										<%-- <s:if test="%{isDateChanged}">
											<td id="verifyBillPayWarningMessage" class="sectionsubhead" colspan="6" style="color:red" align="center">
											<s:text name="jsp.billpay_118"/><br><br>
											</td>
										</s:if> --%>
										<%-- <tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_84"/><br>
												</span></td>
										</tr> --%>
                                        <!-- <tr>
											<td class="columndata ltrow2_color"></td>
											<td></td>
											<td></td>
											<td class="sectionsubhead"><br>
											</td>
											<td></td>
											<td></td>
										</tr> -->
                                      <%--   <tr>
											<td id="verifyBillPayPayeeNicknameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td id="verifyBillPayPayeeNicknameValue" class="columndata" colspan="2">
											<s:property value="addRecPayment.Payee.NickName"/>
											</td> 
											<td id="verifyBillPayAmountLabel" class="columndata tbrd_l">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>
											</td>
											<td id="verifyBillPayAmountValue" class="columndata" colspan="2">
												 <s:property value="addRecPayment.AmountValue.CurrencyStringNoSymbol"/>
												 <s:property value="addRecPayment.payee.PayeeRoute.CurrencyCode"/>
											     <ffi:getProperty name="addRecPayment" property="AmountValue.CurrencyStringNoSymbol" />
												 <ffi:getProperty name="addRecPayment" property="AmountValue.CurrencyCode"/>
											</td>
										</tr> --%>
										
										
										<%-- <tr>
											<td id="verifyBillPayLastPaymentLabel" align="right" class="ltrow2_color instructions">
											<div align="right">
													<s:text name="jsp.billpay_last_payment_to_payee"/></div>
													</td>
											<td id="verifyBillPayLastPaymentValue" class="columndata instructions" colspan="2" align="left" style="padding:2px;">
												<ffi:setProperty name="LastPayments" property="Filter" value="PAYEEID=${AddPayment.PayeeID}"/>
												<ffi:cinclude value1="${LastPayments.Size}" value2="0" operator="notEquals">													
														<ffi:list collection="LastPayments" items="lastPayment" startIndex="1" endIndex="1">
															<ffi:setProperty name="lastPayment" property="DateFormat" value="${UserLocale.DateFormat}" />
															<ffi:getProperty name="lastPayment" property="Amount"/>&nbsp;-&nbsp;<ffi:getProperty name="lastPayment" property="PayDate"/>
															</ffi:list>													
													</ffi:cinclude>
													<ffi:cinclude value1="${LastPayments.Size}" value2="0" operator="equals"><s:text name="jsp.billpay_none"/></ffi:cinclude>
												
												 <ffi:list collection="lastPaymentToPayee" items="lastPayment" startIndex="1" endIndex="1">
														<ffi:setProperty name="lastPayment" property="DateFormat"/>
														<ffi:getProperty name="lastPayment" property="Amount"/>-<ffi:getProperty name="lastPayment" property="PayDate"/>
												</ffi:list>
												 </td>
												<td class="columndata tbrd_l"></td>
												<td class="columndata" colspan="2"></td>
												
										</tr>					 --%>																				
										<%-- <tr>
											<td  id="verifyBillPayPayeeNameLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="verifyBillPayPayeeNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="addRecPayment.payee" property="Name" /></td>
											<td id="verifyBillPayCurrencyLabel" class="columndata tbrd_l sectionsubhead" align="right">
                                                <ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="notEquals" >
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
											    </ffi:cinclude>
                                                <ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="equals" >
                                                    &nbsp;
                                                </ffi:cinclude>
                                                  <s:if test="%{addRecPayment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else> <s:text name="jsp.billpay_40"/> </s:else>
                                            </td>
                                            <td colspan="2" class="columndata" id="verifyBillPayCurrencyValue">
                                            	 <s:if test="%{addRecPayment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	&#8776; <s:property value="%{addRecPayment.convertedAmount}"/>  <s:property value="%{addRecPayment.account.currencyCode}"/>
								        	    </s:else>
											</td>
											<td id="verifyBillPayCurrencyValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="notEquals" >
                                                    <ffi:object id="ConvertToTargetCurrency" name="com.ffusion.tasks.fx.ConvertToTargetCurrency" scope="session"/>
                                                    <ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmount" value="${AddPayment.AmountForBPW}" />
                                                    <ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmtCurrency" value="${AddPayment.AmountValue.CurrencyCode}" />
                                                    <ffi:setProperty name="ConvertToTargetCurrency" property="TargetAmtCurrency" value="${AddPayment.Account.CurrencyCode}" />
                                                    <ffi:process name="ConvertToTargetCurrency"/>
                                                    <ffi:setProperty name="CONVERTED_CURRENCY_AMOUNT"  value="${ConvertToTargetCurrency.TargetAmount}"/>
                                                    &#8776; <s:property value="#attr.ConvertToTargetCurrency.targetAmountWithCommaSeparator"/>
		 						                    <ffi:getProperty name="addRecPayment" property="Account.CurrencyCode"/>
												</ffi:cinclude>
											</td>
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPayAccountLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td>
											<td id="verifyBillPayAccountValue"  class="columndata" colspan="2">
											<ffi:getProperty name="addRecPayment.payee" property="UserAccountNumber" /></td>
											
											<td id="verifyBillPayFromAccountLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td>
											<td id="verifyBillPayFromAccountValue" class="columndata" colspan="2">
												<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
												<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" 
													value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY %>"/>			
												<s:set var="AccountDisplayTextTaskBean" value="#session.recPayment.Account" scope="request" />
												<ffi:process name="AccountDisplayTextTask"/>
												<ffi:getProperty name="AccountDisplayTextTask" property="accountDisplayText"/>
												<ffi:getProperty name="AddPayment" property="Account.DisplayText"/>
												<ffi:cinclude value1="${AddPayment.Account.NickName}" value2="" operator="notEquals" >
													 - <ffi:getProperty name="AddPayment" property="Account.NickName"/>
										 		</ffi:cinclude>	
												 - <ffi:getProperty name="AddPayment" property="Account.CurrencyCode"/>
												 
												 <s:property value="%{addRecPayment.account.accountDisplayText}"/>
											</td>
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPayFirstAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/> </span></div>
											</td>
											<td id="verifyBillPayFirstAddressValue" class="columndata" colspan="2"
											><ffi:getProperty name="addRecPayment.payee" property="Street" /></td>
											<td id="verifyBillPayDateLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_76"/></span></div>
											</td>
											<td id="verifyBillPayDateValue" class="columndata" colspan="2">
											<ffi:getProperty name="addRecPayment" property="PayDate" /></td>
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPaySecondAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="verifyBillPaySecondAddressValue" class="columndata" colspan="2">
											<ffi:getProperty name="addRecPayment.payee" property="Street2" /></td>
											<td id="verifyBillPayDeliveredByLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_deliver_by"/></span></div>
											</td>
											<td id="verifyBillPayDeliveredByValue" class="columndata" colspan="2" valign="middle">
												<ffi:setProperty name="AddPayment" property="DateFormat" value="${UserLocale.DateFormat}" />
												<ffi:getProperty name="addRecPayment" property="DeliverByDate"/>	
												<s:property value="%{addRecPayment.deliverByDate}"/>											
											</td>											
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPayThirdAddressLabel" class="ltrow2_color">
												<div align="right">
												<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/> </span></div>
												</td>
											<td id="verifyBillPayThirdAddressValue" class="columndata" colspan="2">
											<ffi:getProperty name="addRecPayment.payee" property="Street3" /></td>
											
											<td id="verifyBillPayRecurringLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_351"/></span></div>
											</td>
											<td id="verifyBillPayRecurringValue" class="columndata" colspan="2" valign="middle">
												<ffi:getProperty name="addRecPayment" property="Frequency" />
											</td>											
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPayCityLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td id="verifyBillPayCityValue" class="columndata" colspan="2">
											<ffi:getProperty name="addRecPayment.payee" property="City" /></td>
											<td id="verifyBillPayFrequencyLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_321"/></span>
												</div>
											</td>
											<td id="verifyBillPayFrequencyValue" class="columndata" colspan="2" valign="middle">
												<ffi:cinclude value1="${OpenEnded}" value2="true">
 													<s:text name="jsp.default_446"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals">
 													<ffi:getProperty name="addRecPayment" property="NumberPayments" />
												</ffi:cinclude>
												<s:if test="%{addRecPayment.openEnded}">
													<s:text name="jsp.default_446"/>
												</s:if>
												<s:else>
													<s:property value="%{addRecPayment.NumberPayments}"/>
												</s:else>
											</td>											
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPayStateLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="verifyBillPayStateValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="Payee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{addRecPayment.payee.stateDisplayName}"/>
											</td>
											<ffi:cinclude value1="${AddPayment.REGISTER_CATEGORY_ID}" value2="">
												<td id="verifyBillPayMemoLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
											</td>
											<td id="verifyBillPayMemoValue" colspan="2" valign="middle" class="columndata"><ffi:getProperty name="addRecPayment" property="Memo" /></td>
											</ffi:cinclude>
											<ffi:cinclude value1="${AddPayment.REGISTER_CATEGORY_ID}" value2="" operator="notEquals">
												<td class="columndata tbrd_l">
													<div align="right">
														<span class="sectionsubhead"><s:text name="jsp.billpay_89"/></span>
													</div>
												</td>
												<td colspan="2" class="columndata">
													<ffi:setProperty name="RegisterCategories" property="Filter" value="All"/>
													<ffi:list collection="RegisterCategories" items="Category">
														<ffi:cinclude value1="${Category.Id}" value2="${AddPayment.REGISTER_CATEGORY_ID}">
															<ffi:setProperty name="RegisterCategories" property="Current" value="${Category.Id}"/>
															<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="">
																<ffi:getProperty name="Category" property="Name"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="" operator="notEquals">
																<ffi:getProperty name="RegisterCategories" property="ParentName"/>: <ffi:getProperty name="addRecPayment.category" property="Name"/>
															</ffi:cinclude>
														</ffi:cinclude>
													</ffi:list>
												</td>
											</ffi:cinclude>
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPayZipLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td id="verifyBillPayZipValue" class="columndata" colspan="2">
											<ffi:getProperty name="addRecPayment.payee" property="ZipCode" /></td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPayCountryLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_115"/> </span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" />
											<td id="verifyBillPayCountryValue" class="columndata" colspan="2">
											<ffi:getProperty name='addRecPayment.countryResource' property='Resource'/>
											<s:property value="%{addRecPayment.payee.countryDisplayName}"/>
											</td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPayContactLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_36"/> </span></div>
											</td>
											<td id="verifyBillPayContactValue" class="columndata" colspan="2">
											<ffi:getProperty name="addRecPayment.payee" property="ContactName" /></td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr> --%>
										<%-- <tr>
											<td id="verifyBillPayPhoneLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_83"/></span></div>
											</td>
											<td id="verifyBillPayPhoneValue" class="columndata" colspan="2">
											<ffi:getProperty name="addRecPayment.payee" property="Phone" /></td>
											<td class="columndata tbrd_l">&nbsp;</td>
											<td></td>
											<td></td>
										</tr> --%>
                                       <%--  <ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="notEquals" >
                                        <tr>
											<td class="required" colspan="6">
                                                <br><div align="center">&#8776; <s:text name="jsp.default_241"/></div>
                                            </td>
                                            <td></td>
										</tr>
                                        </ffi:cinclude> --%>
                                          <%-- <s:if test="%{addRecPayment.AmtCurrency!=addRecPayment.Account.CurrencyCode}">
									    	<tr>
									        <td class="required" colspan="4">
									           <div align="right">&#8776; <s:text name="jsp.default_241"/></div>
									        </td>
									    </tr>
									    </s:if> --%>
					<!-- </tr>
				</table> -->
<s:if test="%{addRecPayment.AmtCurrency!=addRecPayment.Account.CurrencyCode}">
<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="btn-row">
<sj:a id="cancelBillpayConfirm" 
button="true" 
summaryDivId="summary" buttonType="cancel"
onClickTopics="showSummary,cancelBillpayForm"
><s:text name="jsp.default_82"/></sj:a>

<sj:a id="backInitiateBillpayFormButton" 
button="true" 
onClickTopics="backToBillpayInput"
><s:text name="jsp.default_57"/></sj:a>

<sj:a 
	id="sendSingleBillpaySubmit"
	formIds="BillpayNewSend"
    targets="confirmDiv" 
    button="true" 
    onBeforeTopics="beforeSendBillpayForm"
    onSuccessTopics="sendBillpayFormSuccess"
    onErrorTopics="errorSendBillpayForm"
    onCompleteTopics="sendBillpayFormComplete"
><s:text name="jsp.billpay_69"/></sj:a>
</div>
</s:form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>