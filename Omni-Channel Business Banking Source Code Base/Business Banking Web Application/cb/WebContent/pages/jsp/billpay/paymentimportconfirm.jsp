<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div align="left">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
	        <td>
			<s:form id="paymentuploadconfirmform" name="paymentuploadconfirmform" namespace="/pages/fileupload" action="billpayFileUploadAction_continueProcess" method="post" theme="simple">
               	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
               	    <%-- 
               		<div>
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="20" height="1" border="0"></td>
							<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="20" height="1" border="0"></td>
							<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="20" height="1" border="0"></td>
							<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="660" height="1" border="0"></td>
						</tr>
						<tr>
							<td class="columndata" colspan="4"><span class="sectionhead">&gt; <s:text name="jsp.billpay_35"/><br></span></td>
						</tr>
						<tr valign="bottom">
							<td class="tbrd_b" colspan="4" align="right"><span class="columndata"></span></td>
						</tr>
						<tr>
							<td class="mainfontbold" colspan="4"><s:text name="jsp.default_421"/>:</td>
						</tr>
						<% String lastTitle = null; String title; String line; String lineNumber; String recordNumber;
							boolean canContinue = true;
						com.ffusion.beans.fileimporter.ErrorMessages errMsg = (com.ffusion.beans.fileimporter.ErrorMessages)session.getAttribute(SessionNames.IMPORT_ERRORS);
						if (errMsg.operationCanContinue() == false)
							canContinue = false;
						%>

                                       <ffi:list collection="ImportErrors" items="importError">
                                        <ffi:getProperty name="importError" property="title" assignTo="title"/>
                                        <ffi:getProperty name="importError" property="line" assignTo="line"/>
                                        <ffi:getProperty name="importError" property="lineNumber" assignTo="lineNumber"/>
                                        <ffi:getProperty name="importError" property="recordNumber" assignTo="recordNumber"/>

                                       <% if (lastTitle == null || !lastTitle.equals(title)) { %>
						<tr>
							<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="10" border="0"></td>
						</tr>
						<tr>
							<td></td>
							<td colspan="3" class="mainfontbold">
                                               <ffi:getProperty name="importError" property="title"/>
							</td>
						</tr>
							<% lastTitle = title; %>
						<% } %>
						<tr>
							<td></td>
							<td></td>
							<td colspan="2" class="mainfont">
							<% if (lineNumber != null && recordNumber != null) { %>
                                               <span class="mainfontbold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/paymentimportconfirm.jsp-1" parm0="${importError.lineNumber}" parm1="${importError.recordNumber}"/>:</span>
							<% } %>
							<% if (lineNumber != null && recordNumber == null) { %>
                                               <span class="mainfontbold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/paymentimportconfirm.jsp-2" parm0="${importError.lineNumber}"/>:</span>
							<% } %>
							<% if (lineNumber == null && recordNumber != null) { %>
                                               <span class="mainfontbold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/paymentimportconfirm.jsp-3" parm0="${importError.recordNumber}"/>:</span>
							<% } %>
                                               <ffi:getProperty name="importError" property="message"/>
							</td>
						</tr>
						<% if (line != null) { %>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td colspan="1" class="mainfont">
                                               <span class="mainfontbold"><s:text name="jsp.default_136"/>:</span>
                                               <ffi:getProperty name="importError" property="line"/>
							</td>
						</tr>
						<ffi:removeProperty name="line"/>
						<ffi:removeProperty name="lineNumber"/>
						<ffi:removeProperty name="recordNumber"/>
						<tr>
							<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="5" border="0"></td>
						</tr>
						<% } %>
                                       </ffi:list>
						<tr>
							<td colspan="4"></td>
						</tr>
						<tr>
							<td colspan="4" valign="bottom">
								<div align="center">
										<a id="anchor_billimport_confirm_cancel"><s:text name="jsp.default_82"/></a>
									<% if (canContinue) { %>
										<a id="anchor_billimport_confirm_continue"><s:text name="jsp.default_111"/></a>
									<% } %>
								</div>
							</td>
						</tr>
					</table>
					</div>
					--%>
					<%-- --%>
					<div id="importResultsDiv">
								<br/><br/>
								<span class="sectionhead">&gt; <s:text name="jsp.billpay_35"/></span><br/><br/>
								<span class="sectionhead"><s:text name="jsp.default_421"/>:</span>
								<br/><br/>
									<ffi:cinclude value1="${ImportErrors}" value2="" operator="notEquals">
										
											<s:include value="%{#session.PagesPath}/common/fileImportErrorsGrid.jsp"/>
									
									</ffi:cinclude>
									
									<br/>
									
									<div align="center" valign="bottom">
												<sj:a
												id="cancelImport"
												button="true" 
												summaryDivId="summary" buttonType="cancel"
												onClickTopics="showSummary,cancelBillpayForm,billpaymappingCancel"
												><s:text name="jsp.default_82"/></sj:a>
											
											<ffi:cinclude value1="${ImportErrors.operationCanContinue}" value2="false" operator="notEquals">
												<sj:a
												id="continueImport" 
												targets="inputDiv"
												formIds="paymentuploadconfirmform"
												button="true" 
												onCompleteTopics="completeConfirmUploadedPaymentsProcess"
												onSuccessTopics="successConfirmUploadedPaymentsProcess"
												onErrorTopics="errorConfirmUploadedPaymentsProcess"
												onBeforeTopics="beforeConfirmUploadedPaymentsProcess"
												><s:text name="jsp.default_111"/></sj:a>
											</ffi:cinclude>
									</div>
									<br/>
					</div>
			</s:form>
			</td>
		</tr>
	</table>
</div>
