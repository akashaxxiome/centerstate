<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
 <%@ page contentType="text/html; charset=UTF-8" %>
 
<ffi:help id="payments_billpaypayee-company-add" className="moduleHelpClass"/>	
<ffi:author page="billpaypayee-company-add.jsp"/>
<ffi:object name="com.ffusion.tasks.billpay.GetGlobalPayees" id="GetGlobalPayees"></ffi:object>
<ffi:object name="com.ffusion.tasks.billpay.AddGlobalPayees" id="AddGlobalPayees"></ffi:object>

<ffi:setProperty name="GetGlobalPayees" property="PayeeName" value="GlobalPayees" />
<ffi:setProperty name="GetGlobalPayees" property="PrefixPayeeName" value='<%=request.getParameter("SearchName") %>' />
<ffi:setProperty name="GetGlobalPayees" property="FindFirstPayeeLetter" value="false"/>
<ffi:process name="GetGlobalPayees" />
<ffi:cinclude value1="${GlobalPayees.Size}" value2="0" operator="equals" >
	<ffi:setProperty name="NoPayeeMatched" value="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${GlobalPayees.Size}" value2="0" operator="notEquals">
	<ffi:setProperty name="NoPayeeMatched" value="false"/>
	<ffi:setProperty name="AddGlobalPayees" property="Initialize" value="true" />
	<ffi:setProperty name="AddGlobalPayees" property="PayeeName" value="GlobalPayees" />
	<ffi:setProperty name="AddGlobalPayees" property="SuccessURL" value=""/>
	<ffi:process name="AddGlobalPayees" />
</ffi:cinclude>
<% if (request.getParameter("StartShowAll") != null) { %>
    <ffi:setProperty name="NoPayeeMatched" value="true"/>
<% } %>
<s:include value="/pages/jsp/billpay/billpaypayee-company-add2.jsp"></s:include>
<ffi:removeProperty name="PayeeSearch"/>
