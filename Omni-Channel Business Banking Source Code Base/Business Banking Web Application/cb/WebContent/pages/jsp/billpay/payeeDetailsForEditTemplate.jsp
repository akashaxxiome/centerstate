<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript">
$(document).ready(function() {
	/* to load the message on top on selection of payee */
	
	/* checks if a payee is present by checking the account number */
	  var instruction=$('#accNumber').val();
	/* if not null loads the message and sets the payee currency */
	  if(instruction!=''){
		  $('#payeeCurrencyCode').html($('#payeeCurrency').val());
	  }
	  
	});
</script>
<s:hidden value="%{editPayment.payee.userAccountNumber}" id="accNumber"></s:hidden>
<s:hidden value="%{editPayment.payee.PayeeRoute.CurrencyCode}" id="payeeCurrency" name="editPayment.payee.payeeRoute.currencyCode"></s:hidden>

<a id="expandBillpayEditTemplatePayee" onclick="$('#expandBillpayEditTemplatePayee').toggle(),$('#collapseBillpayEditTemplatePayee').toggle(),$('.payeeDetailEditTemplate').slideToggle()" class="nameTitle expandArrow" title="Show Beneficiary Details">
	<ffi:getProperty name="editPayment.payee" property="Name" />
		<span class="nameSubTitle"> (<ffi:getProperty name="editPayment.payee" property="NickName"/> - 
		<ffi:getProperty name="editPayment.payee" property="UserAccountNumber" />) <span class="sapUiIconCls icon-navigation-down-arrow "></span></span>
</a>
<a id="collapseBillpayEditTemplatePayee" onclick="$('#expandBillpayEditTemplatePayee').toggle(),$('#collapseBillpayEditTemplatePayee').toggle(),$('.payeeDetailEditTemplate').slideToggle()" class="hidden nameTitle collapseArrow" title="Hide Beneficiary Details">
	<ffi:getProperty name="editPayment.payee" property="Name" />
		<span class="nameSubTitle">(<ffi:getProperty name="editPayment.payee" property="NickName"/> - 
		<ffi:getProperty name="editPayment.payee" property="UserAccountNumber" />) <span class="sapUiIconCls icon-navigation-up-arrow "></span></span>
</a>
<div class="blockContent label130 hidden payeeDetailEditTemplate marginTop10">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplatePayeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
				<span id="singleBillPayTemplatePayeeNickNameValue" class="columndata"  >
				<ffi:getProperty name="editPayment.payee" property="NickName"/></span>
			</div>
			<div class="inlineBlock">
				 <span id="singleBillPayTemplateContactLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_36"/>:</span>
             <span id="singleBillPayTemplateContactValue" class="columndata"  >
             <ffi:getProperty name="editPayment.payee" property="ContactName" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplatePhoneLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_37"/>:</span>
             <span id="singleBillPayTemplatePhoneValue" class="columndata"  >
             <ffi:getProperty name="editPayment.payee" property="Phone" /></span>
			</div>
			<div class="inlineBlock">
				 <span id="singleBillPayTemplateAddress1Label" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_36"/>:</span>
             <span id="singleBillPayTemplateAddress1Value" class="columndata"  >
             <ffi:getProperty name="editPayment.payee" property="Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateAddress2Label" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_37"/>:</span>
             <span id="singleBillPayTemplateAddress2Value" class="columndata"  >
             <ffi:getProperty name="editPayment.payee" property="Street2"/></span>
			</div>
			<div class="inlineBlock">
				 <span id="singleBillPayTemplateAddress3Label" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_38"/>:</span>
             <span id="singleBillPayTemplateAddress3Value" class="columndata"  >
             <ffi:getProperty name="editPayment.payee" property="Street3"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateCityLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_101"/>:</span>
             <span id="singleBillPayTemplateCityValue" class="columndata"  >
             <ffi:getProperty name="editPayment.payee" property="City"/></span>
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateStateLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_104"/>:</span>
             <span id="singleBillPayTemplateStateValue" class="columndata"  >
                 <s:property value="%{editPayment.payee.stateDisplayName}"/>
             </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				 <span id="singleBillPayTemplateZipCodeLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_473"/>:</span>
             <span id="singleBillPayTemplateZipCodeValue" class="columndata"  >
             <ffi:getProperty name="editPayment.payee" property="ZipCode"/></span>
			</div>
			<div class="inlineBlock">	
				<span id="singleBillPayTemplateCountryLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_115"/>:</span>
            <%--  <ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" /> --%>
             <span id="singleBillPayTemplateCountryValue" class="columndata"  >
             <%-- <ffi:getProperty name='CountryResource' property='Resource'/> --%>
             <s:property value="%{editPayment.payee.countryDisplayName}"/></span>
			</div>
		</div>
	</div>
