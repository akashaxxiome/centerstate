<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
				 java.util.ArrayList,
				 com.ffusion.beans.fileimporter.MappingDefinition"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="acctMgmt_fileupload" />

<%  if (session.getAttribute("PaymentTemplateFlag") != null) { %>
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_80')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<% } else { %>
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_78')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<% } %>
	<ffi:setProperty name='PageText' value=''/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
        <ffi:setProperty name="PageText2" value="${SecurePath}fileuploadcustom.jsp?Section=PAYMENT" URLEncrypt="true"/>
		<ffi:setProperty name='PageText' value='<a href="${PageText2}"><img src="/cb/pages/${ImgExt}grafx/account/i_custommappings.gif" alt="" width="91" height="16" border="0"></a>'/>
        <ffi:removeProperty name="PageText2"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:setProperty name="saveBackURL" value=""/>
<ffi:object id="FileUpload" name="com.ffusion.tasks.fileImport.FileUploadTask" scope="session"/>
<ffi:setProperty name="FileUpload" property="FileType" value="Payment Import" />

<% session.setAttribute("FFIFileUpload", session.getAttribute("FileUpload")); %>

<%  // default values are used by import
	String defaultUpdateBy = "" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING_NEW;
	String defaultMatchBy = "" + MappingDefinition.MATCH_RECORDS_BY_ID_NAME_ACCOUNT;
%>
<ffi:object id="CheckPaymentImport" name="com.ffusion.tasks.fileImport.CheckPaymentImportTask" scope="session"/>
<ffi:object id="ProcessPaymentImport" name="com.ffusion.tasks.fileImport.ProcessPaymentImportTask" scope="session"/>
    <%
	 	session.setAttribute("FFIProcessPaymentImport", session.getAttribute("ProcessPaymentImport"));
	%>
<ffi:setProperty name="ProcessPaymentImport" property="DefaultUpdateRecordsBy" value="<%= defaultUpdateBy %>"/>
<ffi:setProperty name="ProcessPaymentImport" property="DefaultMatchRecordsBy" value="<%= defaultMatchBy %>"/>

<ffi:object id="GetMappingDefinitions" name="com.ffusion.tasks.fileImport.GetMappingDefinitionsTask" scope="session"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	<ffi:process name="GetMappingDefinitions"/>
</ffi:cinclude>

<ffi:object id="SetMappingDefinition" name="com.ffusion.tasks.fileImport.SetMappingDefinitionTask" scope="session"/>
<% session.setAttribute("FFISetMappingDefinition", session.getAttribute("SetMappingDefinition")); %>
<ffi:object id="GetOutputFormat" name="com.ffusion.tasks.fileImport.GetOutputFormatTask" scope="session"/>
<%-- Set up the flow of control for  fileimport --%>
<%-- there is no SUCCESS report for Transfers, so just go to the Transfer Batch page
   <ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>
   <ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
   <ffi:setProperty name="GenerateReportBase" property="Target" value="${SecurePath}reports/displayreport.jsp"/>

   <ffi:object id="GetReportsByCategoryACH" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
   <ffi:setProperty name="GetReportsByCategoryACH" property="ReportCategory" value="<%= com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_FILE_IMPORT %>" />
   <ffi:setProperty name="GetReportsByCategoryACH" property="ReportsName" value="ReportsACH" />
   <ffi:process name="GetReportsByCategoryACH" />
   <ffi:setProperty name="ProcessTransferImport" property="SuccessURL" value="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryACH.ReportCategory}&NewReportBase.Target=${SecureServletPath}GenerateReportBase"/>
--%>
<ffi:setProperty name="ReportName" value="${GetReportsByCategoryACH.ReportCategory}" />
<ffi:setProperty name="CheckPaymentImport" property="ImportErrorsURL" value="confirm"/>
<%-- 
<ffi:setProperty name="BackURL" value="ReportsACH" />
<ffi:setProperty name="FileUpload" property="SuccessURL" value="${SecureServletPath}CheckPaymentImport"/>
<ffi:setProperty name="CheckPaymentImport" property="SuccessURL" value="${SecureServletPath}ProcessPaymentImport"/>
<ffi:setProperty name="CheckPaymentImport" property="ImportErrorsURL" value="${SecurePath}payments/paymentimportconfirm.jsp"/>
<ffi:setProperty name="ProcessPaymentImport" property="SuccessURL" value="${SecurePath}payments/billpaynewmulti.jsp" />
--%>
<script type="text/javascript"><!--

function updateCustomMapping(fileType, customFileType)
{
	if (fileType == null || customFileType == null)
		return;
	if (fileType.value == 'Custom')
	{   
		$("#SetMappingDefinitionIDID").selectmenu('enable');
	}
	else
	{   
		$("#SetMappingDefinitionIDID").val('0');
		$("#SetMappingDefinitionIDID").selectmenu('destroy').selectmenu({width:'20em'});
   		$("#SetMappingDefinitionIDID").selectmenu('disable');
	}
}

$("#FileUploadFileTypeID").selectmenu({width:'20em'});
$("#SetMappingDefinitionIDID").selectmenu({width:'20em'});
$(document).ready(function(){
	   updateCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID']);
});
// --></script>
	<fieldset id="fileUploadSelectFormatFieldSetID">
	<legend><s:text name="jsp.billpay_57"/></legend> 
		<div align="center">
                 <form name="FileType" action="/cb/pages/fileupload/billpayFileUploadAction_verify.action" method="post" id="FileTypeID">
                 <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td></td>
                        <td class="lightBackground" align="center">
                            <table width="100%" border="0" cellspacing="0" cellpadding="3">
                                <tr>
                                    <td class="columndata" width="480">
                                        <span class="sectionsubhead"><s:text name="jsp.default_236"/></span>
                                    </td>
                                    <td width="30"></td>
                                    <td><span class="sectionsubhead"><s:text name="jsp.default_14"/></span></td>
                                </tr>
                                <tr>
                                    <td class="columndata" width="480" rowspan="3"><s:text name="jsp.default_223"/></td>
                                    <td class="columndata" nowrap width="30"></td>

                                <td class="columndata" valign="top">
                                <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS_SWIFT_FILE_UPLOAD %>">
                                        <select id="FileUploadFileTypeID" name="fileType" class="txtbox" onChange="updateCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID'])">
                                                <option value="Payment Import"><s:text name="jsp.billpay_30"/></option>
                                                <option value="Custom"><s:text name="jsp.default_132"/></option>
                                        </select>
                                    </ffi:cinclude>
                                </ffi:cinclude>
                                <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS_SWIFT_FILE_UPLOAD %>">
                                        <input type="Hidden" name="fileType" value="Payment Import">
                                        <s:text name="jsp.billpay_30"/>
                                    </ffi:cinclude>
                                </ffi:cinclude>
                                <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.PAYMENTS_SWIFT_FILE_UPLOAD %>">
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                        <input type="Hidden" name="fileType" value="Custom">
                                        <s:text name="jsp.default_132"/>
                                    </ffi:cinclude>
                                </ffi:cinclude>
                                </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td valign="bottom"><span class="sectionsubhead">
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                        <s:text name="jsp.default_135"/>
                                    </ffi:cinclude>
                                    </span></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td valign="top" class="columndata" nowrap>
                                    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                        <select name="SetMappingDefinition.ID" class="txtbox"  id="SetMappingDefinitionIDID">
                                            <option value="0">- <s:text name="jsp.default_377"/> -</option>
                                            <ffi:list collection="MappingDefinitions" items="MappingDefinition">
                                                <% String entitlementName = ""; %>
                                                <ffi:cinclude value1="${MappingDefinition.OutputFormatName}" value2="" operator="notEquals">
                                                    <ffi:setProperty name="GetOutputFormat" property="Name" value="${MappingDefinition.OutputFormatName}"/>
                                                    <ffi:process name="GetOutputFormat"/>
                                                    <ffi:setProperty name="OutputFormat" property="CurrentCategory" value="PAYMENT"/>

                                                    <ffi:getProperty name="OutputFormat" property="EntitlementName" assignTo="entitlementName" />

                                                    <ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
                                                        <% boolean displayMap = true;
                                                        if (entitlementName != null && entitlementName.length() > 0) { %>
                                                            <ffi:cinclude ifNotEntitled="<%=entitlementName%>" >
                                                                <% displayMap = false; %>
                                                            </ffi:cinclude>
                                                        <% }
                                                        String updateRecordsBy = null; %>
                                                        <ffi:getProperty name="MappingDefinition" property="UpdateRecordsBy" assignTo="updateRecordsBy" />
                                                        <%
                                                        if (updateRecordsBy != null && updateRecordsBy.equals("" + MappingDefinition.UPDATE_RECORDS_BY_EXISTING_AMOUNTS_ONLY))
                                                            displayMap = false;
                                                        if (displayMap)
                                                        { %>
                                                        <option value="<ffi:getProperty name='MappingDefinition' property='MappingID'/>"><ffi:getProperty name="MappingDefinition" property="Name"/></option>
                                                        <% } %>
                                                    </ffi:cinclude>
                                                </ffi:cinclude>
                                            </ffi:list>
                                        </select>
                                        <span id="mappingTypeError"></span>
                                    </ffi:cinclude>
                                    <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
                                        <input type="Hidden" name="SetMappingDefinition.ID" value="0">
                                    </ffi:cinclude>
                                    </td>
                                </tr>
								<tr>
									<td colspan="3" align="center" nowrap>
										<sj:a 
											id="cancelFileImportTypeID"
			                                button="true" 
			                                onClickTopics="cancelMultiBillpayForm"
					                        ><s:text name="jsp.default_82"/></sj:a>
										<sj:a 
											id="fileImportTypeID"
											formIds="FileTypeID"
											timeout="0"
			                                targets="openFileImportDialogID" 
			                                button="true" 
											onclick="removeValidationErrors();"
											validate="true"
											validateFunction="customValidation"
											onErrorTopics="errorOpenFileUploadTopics"
											onSuccessTopics="openBillpayFileUploadTopics"
					                        ><s:text name="jsp.default_235"/></sj:a>
									</td>
								</tr>
                            </table>
                        </td>
                        <td></td>
                    </tr>
                </table>
			</form>
		</div>
		</fieldset>
<ffi:removeProperty name="GetOutputFormat"/>
<ffi:setProperty name="actionNameNewReportURL" value="NewReportBaseAction.action?NewReportBase.ReportName=${ReportName}&flag=generateReport" URLEncrypt="true" />
<ffi:setProperty name="actionNameGenerateReportURL" value="GenerateReportBaseAction_display.action?doneCompleteDirection=${doneCompleteDirection}" URLEncrypt="true" />