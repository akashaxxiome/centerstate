<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaytemplatesingleconfirm" className="moduleHelpClass"/>
<%-- <%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %> --%>

<%-- 
<ffi:removeProperty name="Payment" />
<ffi:cinclude value1="${OpenEnded}" value2="true" ><ffi:setProperty name="NumberPayments" value="999"/></ffi:cinclude>
<ffi:setProperty name="AddPayment" property="NumberPayments" value="${NumberPayments}"/> --%>
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_10')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<%-- 
<ffi:setProperty name='PageText' value=''/>
<ffi:cinclude value1="${PaymentReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/payments_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0" hspace="3"></a>'/>
</ffi:cinclude>
<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}payments/billpayadd.jsp"><img src="/cb/pages/${ImgExt}grafx/payments/i_payees.gif" alt="" border="0" hspace="3"></a><a href="${SecurePath}payments/billpaytemplates.jsp"><img src="/cb/pages/${ImgExt}grafx/payments/i_templates.gif" alt="" border="0" hspace="3"></a>'/>
 --%>
<%-- for localizing state name --%>
<%-- <ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${Payee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${Payee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>
<ffi:setProperty name="AddPayment" property="AmountValue.CurrencyCode" value="${AddPayment.Payee.PayeeRoute.CurrencyCode}"/>
<ffi:setProperty name="AddPayment" property="AmtCurrency" value="${AddPayment.AmountValue.CurrencyCode}"/>
 --%>



<script language="JavaScript">
function openSaveTemplate(URLExtra)  {
	MyWindow=window.open(''+URLExtra,'','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=750,height=340');
}
</script>
<s:form id="BillpayNewTempSend" namespace="/pages/jsp/billpay" validate="false" action="addBillPayTemplateAction_execute" method="post" name="BillpayNewSend" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="template" value="true"/>
<div class="SaveTemplate" role="form" aria-labelledby="billpaySummary">
	<div class="header"><h2 id="billpaySummary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
	<div class="leftPaneLoadPanel summaryBlock"  style="padding: 15px 10px;" id="leftPaneForDvc">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue"><s:property value="%{addRecPayment.payee.Name}"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_15"/>:</span>
				<span class="inlineSection floatleft labelValue"><s:property value="%{addRecPayment.account.accountDisplayText}"/></span>
			</div>
	</div>
</div>
<div class="confirmPageDetails rightPaneConfirmDvc">
<ffi:cinclude value1="${AddPayment.DateChanged}" value2="true">
<div id="singleBillPayTemplateVerifyWarningMessage" class="sectionsubhead columndata_error" align="center"><s:text name="jsp.billpay_118"/></div>
</ffi:cinclude>
<div  class="blockHead toggleClick expandArrow">
<s:text name="jsp.billpay.payee_summary" />: <s:property value="%{addRecPayment.payee.Name}"/> (<s:property value="%{addRecPayment.payee.NickName}"/> - <s:property value="%{addRecPayment.payee.userAccountNumber}"/>)
<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
	<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="singleBillPayTemplateVerifyPayeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_314"/>:</span>
					<span id="singleBillPayTemplateVerifyPayeeNameValue" class="columndata" >
					    <s:property value="%{addRecPayment.payee.Name}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="singleBillPayTemplateVerifyPayeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
					<span id="singleBillPayTemplateVerifyPayeeNickNameValue" class="columndata">
					     <s:property value="%{addRecPayment.payee.NickName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="singleBillPayTemplateVerifyPayeeAccountLabel" class="columndata  sectionsubhead sectionLabel" ><s:text name="jsp.default_15"/>:</span>
					<span id="singleBillPayTemplateVerifyPayeeAccountValue" class="columndata" >
						<s:property value="%{addRecPayment.payee.userAccountNumber}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="singleBillPayTemplateVerifyContactLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_36"/>:</span>
					<span id="singleBillPayTemplateVerifyContactValue" class="columndata" >
						<s:property value="%{addRecPayment.payee.contactName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="singleBillPayTemplateVerifyPhoneLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_83"/>:</span>
					<span id="singleBillPayTemplateVerifyPhoneValue" class="columndata" ><s:property value="%{addRecPayment.payee.phone}"/></span>
				</div>
				<div class="inlineBlock">
					<span id="singleBillPayTemplateVerifyAddress1Label" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_36"/>:</span>
					<span id="singleBillPayTemplateVerifyAddress1Value" class="columndata" ><s:property value="%{addRecPayment.payee.street}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="singleBillPayTemplateVerifyAddress2Label" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_37"/>:</span>
					<span id="singleBillPayTemplateVerifyAddress2Value" class="columndata" >
						<s:property value="%{addRecPayment.payee.street2}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="singleBillPayTemplateVerifyAddress3Label"  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_38"/>:</span>
					<span id="singleBillPayTemplateVerifyAddress3Value" class="columndata"  >
						<s:property value="%{addRecPayment.payee.street3}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="singleBillPayTemplateVerifyCityLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_101"/>:</span>
					<span id="singleBillPayTemplateVerifyCityValue" class="columndata"  >
						<s:property value="%{addRecPayment.payee.city}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="singleBillPayTemplateVerifyStateLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_104"/>:</span>
					<span id="singleBillPayTemplateVerifyStateValue"  class="columndata">
						<s:property value="%{addRecPayment.payee.stateDisplayName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="singleBillPayTemplateVerifyZipCodeLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_473"/>:</span>
					<span id="singleBillPayTemplateVerifyZipCodeValue" class="columndata"><s:property value="%{addRecPayment.payee.zipCode}"/></span>		
				</div>
				<div class="inlineBlock">
					<span id="singleBillPayTemplateVerifyCountryLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_115"/>:</span>
					<span id="singleBillPayTemplateVerifyCountryValue" class="columndata" ><s:property value="%{addRecPayment.payee.countryDisplayName}"/></span>
				</div>
			</div>
</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart--><s:text name="jsp.transaction.summary.label" /><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateVerifyTemplateNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_416"/>:</span>
				<span id="singleBillPayTemplateVerifyTemplateNameValue" class="columndata" > <s:property value="%{addRecPayment.templateName}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateVerifyMemoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
				<span id="singleBillPayTemplateVerifyMemoValue" class="columndata" ><s:property value="%{addRecPayment.memo}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateVerifyAmountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
				<span id="singleBillPayTemplateVerifyAmountValue" class="columndata" >
					<s:property value="%{addRecPayment.amountValue.currencyStringNoSymbol}"/> <s:property value="addRecPayment.payee.PayeeRoute.CurrencyCode"/>
				</span>
				<%--required by validator as for template zero amount is allowed. --%>
				<input type="hidden" id="amount" name="amount" value="<ffi:getProperty name="AddPayment" property="AmountValue.CurrencyStringNoSymbol"/>" />
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateVerifyDebitAmountLabel" class="sectionsubhead sectionLabel">
                     <s:if test="%{addRecPayment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else>
						<s:text name="jsp.billpay_40"/>:
					</s:else>											
				</span>
				<span id="singleBillPayTemplateVerifyDebitAmountValue" class="columndata" >
					<s:if test="%{addRecPayment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else>
	        	    	&#8776; <s:property value="%{addRecPayment.convertedAmount}"/>  <s:property value="%{addRecPayment.account.currencyCode}"/>
	        	    </s:else>
				</span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateVerifyRecurringLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_351"/>:</span>
				<span id="singleBillPayTemplateVerifyRecurringValue" class="columndata" ><s:property value="%{addRecPayment.frequency}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateVerifyFrequencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/>:</span>
				<span id="singleBillPayTemplateVerifyFrequencyValue" class="columndata" >
					<s:if test="%{addRecPayment.openEnded}">
						<s:text name="jsp.default_446"/>
					</s:if>
					<s:else>
						<s:property value="%{addRecPayment.NumberPayments}"/>
					</s:else>
				</span>
			</div>
		</div>
	</div>
</div>
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
							
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<ffi:cinclude value1="${AddPayment.DateChanged}" value2="true">
										<tr>	
											<td id="singleBillPayTemplateVerifyWarningMessage" class="sectionsubhead" colspan="6" style="color:red" align="center">
											<s:text name="jsp.billpay_118"/><br><br>
											</td>
										</tr>
										</ffi:cinclude>
										<tr>
											<td id="singleBillPayTemplateVerifyTitleLabel" class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_84"/><br>
												</span></td>
										</tr>
                                        <tr>
											<td class="columndata ltrow2_color"></td>
											<td></td>
											<td></td>
											<td class="sectionsubhead"><br>
											</td>
											<td></td>
											<td></td>
										</tr>
                                        <tr>
                                        	<td id="singleBillPayTemplateVerifyPayeeNickNameLabel" class="columndata ltrow2_color" colspan="2">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyPayeeNickNameValue" class="columndata">
											     <ffi:getProperty name="Payee" property="NickName" />
											     <s:property value="%{addRecPayment.payee.NickName}"/>
											</td>
											<td id="singleBillPayTemplateVerifyTemplateNameLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_416"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyTemplateNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayment" property="TemplateName" />
											 <s:property value="%{addRecPayment.templateName}"/>
											</td>
										</tr>
										<tr>
											<td id="singleBillPayTemplateVerifyPayeeNameLabel" class="columndata ltrow2_color" colspan="2">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyPayeeNameValue" class="columndata" >
											    <ffi:getProperty name="Payee" property="Name" />
											    <s:property value="%{addRecPayment.payee.Name}"/>
											</td>
											<td id="singleBillPayTemplateVerifyAmountLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyAmountValue" class="columndata" > 
											<ffi:getProperty name="AddPayment" property="AmountValue.CurrencyStringNoSymbol" />
											<ffi:getProperty name="AddPayment" property="AmountValue.CurrencyCode"/>
											<s:property value="%{addRecPayment.amountValue.currencyStringNoSymbol}"/>
											<s:property value="addRecPayment.payee.PayeeRoute.CurrencyCode"/>
											</td>
											required by validator as for template zero amount is allowed.
											<input type="hidden" id="amount" name="amount" 
											value="<ffi:getProperty name="AddPayment" property="AmountValue.CurrencyStringNoSymbol"/>"></input>
										</tr>
										<tr>
											<td id="singleBillPayTemplateVerifyPayeeAccountLabel" class="columndata  ltrow2_color" colspan="2">
                                               <div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
                                            </td>
											<td id="singleBillPayTemplateVerifyPayeeAccountValue" class="columndata" >
												<ffi:getProperty name="Payee" property="UserAccountNumber" />
												<s:property value="%{addRecPayment.payee.userAccountNumber}"/>
											</td>
											<td id="singleBillPayTemplateVerifyDebitAmountLabel" class="tbrd_l">
                                                  <s:if test="%{addRecPayment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
												</s:else>											
											</td>
											<td id="singleBillPayTemplateVerifyDebitAmountValue" class="columndata" colspan="2">
												<s:if test="%{addRecPayment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	&#8776; <s:property value="%{addRecPayment.convertedAmount}"/>  <s:property value="%{addRecPayment.account.currencyCode}"/>
								        	    </s:else>
												</td>
										</tr>
										<tr>
											<td id="singleBillPayTemplateVerifyAddress1Label" class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/> </span></div>
											</td>
											<td id="singleBillPayTemplateVerifyAddress1Value" class="columndata" >
												<ffi:getProperty name="Payee" property="Street" />
												<s:property value="%{addRecPayment.payee.street}"/>
											</td>
											<td id="singleBillPayTemplateVerifyFromAccountLabel" class="tbrd_l"><div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>											
											</td>
											<td id="singleBillPayTemplateVerifyFromAccountValue" class="columndata" colspan="2">
												 <s:property value="%{addRecPayment.account.accountDisplayText}"/>
											</td>											
										</tr>
										<tr>
											<td id="singleBillPayTemplateVerifyAddress2Label" class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyAddress2Value" class="columndata" >
											<ffi:getProperty name="Payee" property="Street2" />
											<s:property value="%{addRecPayment.payee.street2}"/>
											</td>
											<td id="singleBillPayTemplateVerifyRecurringLabel" class="tbrd_l">
												<div align="right">
												<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_351"/> </span></div>
												</td>
											<td id="singleBillPayTemplateVerifyRecurringValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayment" property="Frequency" />
											<s:property value="%{addRecPayment.frequency}"/>
											</td>
										</tr>
										<tr>
											<td id="singleBillPayTemplateVerifyAddress3Label" valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyAddress3Value" class="columndata"  valign="middle">
												<ffi:getProperty name="Payee" property="Street3" />
												<s:property value="%{addRecPayment.payee.street3}"/>
											</td>
											<td id="singleBillPayTemplateVerifyFrequencyLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_321"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyFrequencyValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${OpenEnded}" value2="true">
 													<s:text name="jsp.default_446"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals">
 													<ffi:getProperty name="AddPayment" property="NumberPayments" />
												</ffi:cinclude>
												<s:if test="%{addRecPayment.openEnded}">
													<s:text name="jsp.default_446"/>
												</s:if>
												<s:else>
													<s:property value="%{addRecPayment.NumberPayments}"/>
												</s:else>
											</td>
											</tr>
										<tr>
											<td id="singleBillPayTemplateVerifyCityLabel" valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_101"/></span>
												</div>
											</td>
											<td id="singleBillPayTemplateVerifyCityValue" class="columndata"  valign="middle">
											<ffi:getProperty name="Payee" property="City" />
											<s:property value="%{addRecPayment.payee.city}"/>
											</td>
											<td id="singleBillPayTemplateVerifyMemoLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyMemoValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayment" property="Memo" />
											<s:property value="%{addRecPayment.memo}"/>
											</td>
										</tr>
										<tr>
											<td id="singleBillPayTemplateVerifyStateLabel" valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyStateValue" valign="middle" class="columndata">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="Payee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{addRecPayment.payee.stateDisplayName}"/>
											</td>

											
										</tr>

										<tr>											
											<td id="singleBillPayTemplateVerifyZipCodeLabel" valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td id="singleBillPayTemplateVerifyZipCodeValue" class="columndata">
											<ffi:getProperty name="Payee" property="ZipCode" />
											<s:property value="%{addRecPayment.payee.zipCode}"/>
											</td>											
											<td class="tbrd_l">
												&nbsp;
											</td>											
											<td class="columndata" colspan="2"></td>
										</tr>
										<tr>											
											<td id="singleBillPayTemplateVerifyCountryLabel" valign="middle" class="ltrow2_color" colspan="2">
											<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_115"/> </span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" />
											<td id="singleBillPayTemplateVerifyCountryValue" class="columndata" valign="middle">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{addRecPayment.payee.countryDisplayName}"/>
											</td>
											<td class="tbrd_l">												
											</td>
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>											
											<td id="singleBillPayTemplateVerifyContactLabel" valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_36"/> </span></div>
											</td>
											<td id="singleBillPayTemplateVerifyContactValue" class="columndata" valign="middle">
											<ffi:getProperty name="Payee" property="ContactName" />
											<s:property value="%{addRecPayment.payee.contactName}"/>
											</td>
											<td class="tbrd_l">												
											</td>
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>											
											<td id="singleBillPayTemplateVerifyPhoneLabel" valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_83"/></span></div></td>
											<td id="singleBillPayTemplateVerifyPhoneValue" class="columndata" valign="middle">
											<ffi:getProperty name="Payee" property="Phone" />
											<s:property value="%{addRecPayment.payee.phone}"/>
											</td>
											<td class="tbrd_l">												
											</td>
											<td ></td>											
										</tr>

                                        <ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="notEquals" >
                                        <tr>
											<td id="singleBillPayTemplateVerifyEstimatedAmountLabel" class="required" colspan="6">
                                                <br><div align="center">&#8776; <s:text name="jsp.default_241"/></div>
                                            </td>
                                            <td></td>
										</tr>
                                        </ffi:cinclude>
                                        <s:if test="%{addRecPayment.AmtCurrency!=addRecPayment.Account.CurrencyCode}">
									    	<tr>
									        <td class="required" colspan="4">
									           <div align="right">&#8776; <s:text name="jsp.default_241"/></div>
									        </td>
									    </tr>
									    </s:if>
                                        <tr>
											<td colspan="6"><div align="center">
													<br>
													<br>
													
													<sj:a id="cancelBillpayConfirm" 
										                button="true" 
														onClickTopics="cancelBillpayForm"
										        	><s:text name="jsp.default_82"/></sj:a>
										        	
										        	<sj:a id="backInitiateFormButton" 
					            						button="true" 
														onClickTopics="backToBillpayInput"
					        						><s:text name="jsp.default_57"/></sj:a>
	                        				
													<sj:a 
														id="sendSingleBillpaySubmit"
														formIds="BillpayNewTempSend"
						                                targets="confirmDiv" 
						                                button="true"
						                                validate="false" 														
						                                onBeforeTopics="beforeSendBillpayForm"
						                                onSuccessTopics="sendBillpayTemplateFormSuccess"
						                                onErrorTopics="errorSendBillpayForm"
						                                onCompleteTopics="sendBillpayFormComplete"
			                        				><s:text name="jsp.default_371"/></sj:a>
			                        			</div>
			                        			</td>
											<td></td>
										</tr>
									</table>
							
						</td>
						<td></td>
					</tr>
				</table> --%>
<s:if test="%{addRecPayment.AmtCurrency!=addRecPayment.Account.CurrencyCode}">
<div align="center" class="required marginTop10">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="btn-row">
	<sj:a id="cancelBillpayConfirm" 
            button="true" 
            summaryDivId="summary" buttonType="cancel"
onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
    	><s:text name="jsp.default_82"/></sj:a>

<sj:a id="backInitiateFormButton" 
   						button="true" 
onClickTopics="backToBillpayInput"
					><s:text name="jsp.default_57"/></sj:a>

<sj:a 
	id="sendSingleBillpaySubmit"
	formIds="BillpayNewTempSend"
                         targets="confirmDiv" 
                         button="true"
                         validate="false" 														
                         onBeforeTopics="beforeSendBillpayForm"
                         onSuccessTopics="sendBillpayTemplateFormSuccess"
                         onErrorTopics="errorSendBillpayForm"
                         onCompleteTopics="sendBillpayFormComplete"
              				><s:text name="jsp.default_366"/></sj:a>
</div>
</div>
</s:form>
<ffi:removeProperty name="GetStateProvinceDefnForState"/>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>