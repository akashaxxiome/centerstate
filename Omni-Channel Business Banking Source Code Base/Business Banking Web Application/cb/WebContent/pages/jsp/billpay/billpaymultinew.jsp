	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page import="com.ffusion.tasks.billpay.Task"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"
	import="com.ffusion.csil.core.common.EntitlementsDefines"%>
	
<ffi:help id="payments_billpaynewmulti" className="moduleHelpClass"/>
<%@ page contentType="text/html; charset=UTF-8" %>

<% boolean canImport = false; %>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
 	<% canImport = true; %>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS_SWIFT_FILE_UPLOAD %>">
	<% canImport = true; %>
</ffi:cinclude>

<%-- if Logged in user is consumer then dont show file import --%>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<% canImport = false; %>
</ffi:cinclude>
<%-- 
	In case of file upload this file will be loaded in Iframe used to upload file.
	In that case it will show javascript error as JQuery won't be available.
	Added following functionality to make sure error won't be shown.
 --%>
<s:include value="%{#session.PagesPath}/common/commonUploadIframeCheck_js.jsp" />
<s:if test="%{multiplePayment.templateName!= ''}">
<% canImport = false; %>
</s:if>
	<%-- Remove session attribute added to identify File upload operation by user, as this is new multiple billpay request --%>
	<ffi:removeProperty name="<%=Task.BILLPAY_FILE_UPLOAD %>"/>
<div id="multipleBillpayPanel" class="formPanel" align="center">
	<div class="leftPaneWrapper">
		<div class="leftPaneInnerWrapper" role="form" aria-labelledby="loadTemplate">
			<% if (canImport) { %>
				<div class="header"><h2 id="loadTemplate"><s:text name="jsp.default.load_template_file_import" /></h2></div>
			<% } else {%>
				<div class="header"><h2 id="loadTemplate"><s:text name="jsp.default_264" /></h2></div>
			<% } %>
			<div class="leftPaneInnerBox leftPaneLoadPanel">	
			 	<div id="templateForMultipleBillpay">
					<s:include value="billpaymultitempload.jsp" />
				</div>
			</div>
			<% if (canImport) { %>
				<div  id="fileImportBtnDivId" style="width:100%; text-align:center">
					<span class="toUpperCase"><s:text name="jsp.default_306" /></span><br><br>
					<sj:a name="fileImportBtn" id="fileImportBtn" onclick="ns.common.setModuleType('billpay',function(){$('#BillPayFileImportEntryLink').trigger('click');})" button="true" cssStyle="margin-left:10px; margin-bottom: 10px;">
						<s:text name="jsp.default_539.1" />
					</sj:a>
				</div>
			<% } %>
		</div>
		<div class="leftPaneInnerWrapper" role="form" aria-labelledby="fromAccount" aria-required="true">
			<div class="header"><h2 id="fromAccount"><s:text name="jsp.default_217" />*</h2></div>
			<div class="leftPaneInnerBox leftPaneLoadPanel">
				<div class="floatleft"><s:if test='%{multiplePayment.batchType == "TEMPLATE"}'>
					<s:select 
						id="AccountID" 
						list="multiplePayment.acountId"
						listKey="multiplePayment.acountId" listValue="multiplePayment.account.accountDisplayText" 
						onchange="loadAccountDetails()"
						class="txtbox" >
					</s:select>
					</s:if>				
					<s:else>
						<select class="txtbox" id="AccountID" name="multiplePayment.acountId" onchange="loadAccountDetails()">
					</select>
					</s:else>
				</div>
				<div class="marginTop10 floatleft" align="center" style="color:#990000"><span id="accountIDFinalError"></span></div>
		</div></div>
</div>
<div class="rightPaneWrapper w71">
	
      <div id="multipleBillpayForm" align="center">
	<s:include value="billpaymultinewform.jsp" />
     </div><!-- MultipleBillpayForm -->
</div><!-- MultipleBillpayPanel --></div>
<script type="text/javascript">
function loadAccountDetails(){
	var accountId = $('#AccountID').val();
	$('#accountIdHidden').val(accountId);
}

</script>