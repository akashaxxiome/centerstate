<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


<ffi:help id="payments_billpaynewmultiverifytemplateedit" className="moduleHelpClass"/>

<s:form id="MultiBillpayNew" namespace="/pages/jsp/billpay" validate="false" action="addBillpayTemplateBatchAction_execute" method="post" name="frmPayBill" theme="simple">
<input type="hidden" name="template" value="true">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper" role="form" aria-labelledby="summary">
	<div class="leftPaneInnerWrapper">
		<div class="header"><h2 id="summary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
		<div class="leftPaneLoadPanel summaryBlock"  style="padding: 15px 5px;">
			<div style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="multipleBillPayTemplateVerify_templateName"><s:text name="jsp.billpay_template_name"/>:</span>
					<span class="inlineSection floatleft labelValue"><s:property value="%{multiplePayment.TemplateName}"/>
            <s:hidden value="%{multiplePayment.TemplateName}" name="multiplePayment.templateName"></s:hidden></span>
			</div>
			<div style="width:100%" class="marginTop10 floatleft">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="multipleBillPayTemplateVerify_fromAccount"><s:text name="jsp.default_217"/>:</span>
					<span class="inlineSection floatleft labelValue"><s:property value="%{multiplePayment.account.accountDisplayText}"/></span>
			</div>
		</div>
	</div>
	<div class="leftPaneInnerWrapper totalAmountWrapper" id="multipleBillPayTemplateVerify_total">
	                <span id="esimatedTotalLabel"><s:text name="jsp.default_431"/>:&nbsp;</span>
	                <s:if test="%{isMultiCurrency}">
	                	&#8776;
	                </s:if>
	                 <span id="esimatedTotal">
	                 	<s:property value="%{multiplePayment.amount}"/>
	                 </span> 
	                 <span id="estimatedTotalCurrency">
	                 <%-- <ffi:getProperty name="selectedCurrencyCode"/> --%>
	                 <s:property value="%{multiplePayment.account.currencyCode}"/>
	                 </span>
	</div>
</div>
<div class="confirmPageDetails">
<div class="blockWrapper">
<s:set var="payeeCount" value="1"></s:set>
	<s:iterator value="%{multiplePayment.payments}" status="listStatus" var="payment">
		<s:if test="%{#payment.AmountValue.CurrencyStringNoSymbol!='0.00'}">
			<h3 class="transactionHeading">
				<span id="multipleBillPayTemplateVerify_payeeNickNameValue[<s:property value="%{#listStatus.index}"/>]" class="columndata">
						<s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
						<s:property value="#payment.payee.userAccountNumber"/>)</span>
			</h3>
			<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
			<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="multipleBillPayTemplateVerify_amountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
					<span id="multipleBillPayTemplateVerify_amountValue[<s:property value="%{#listStatus.index}"/>]" class="columndata">
						<s:property value="#payment.AmountValue.CurrencyStringNoSymbol"/>
						<s:property value="#payment.payee.PayeeRoute.CurrencyCode"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="multipleBillPayTemplateVerify_memoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
					<span id="multipleBillPayTemplateVerify_memoValue[<s:property value="%{#listStatus.index}"/>]" class="columndata">
	                	<s:property value="#payment.memo"/>
	                </span>
				</div>
			</div>
 		</div>
 		<s:set var="payeeCount" value="#payeeCount+1"></s:set>
	</s:if>
	</s:iterator>
</div>
<div class="btn-row">
	<sj:a id="cancelMultiBillpaySend" 
              button="true" 
              summaryDivId="summary" buttonType="cancel"
		onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
      	><s:text name="jsp.default_82"/></sj:a>
      	<s:url action="addBillpayBatchAction_initializeBillpayBatch" escapeAmp="false" 
      		namespace="/pages/jsp/billpay" id="intializeTaskUrl"></s:url>
      	<sj:a id="backInitiateMultiFormButton" 
     						button="true"
     						onClickTopics="backInitiateMultiFormButton"					            																				
 						><s:text name="jsp.default_57"/></sj:a>
	<sj:a 
		id="sendMultiBillpaySubmit"
		formIds="MultiBillpayNew"
        targets="confirmDiv" 
        button="true" 
        onBeforeTopics="beforeSendBillpayForm"
        onSuccessTopics="sendMultiBillpayTemplateFormSuccess"
        onErrorTopics="errorSendBillpayForm"
        onCompleteTopics="sendBillpayFormComplete"
	><s:text name="jsp.default_366"/></sj:a>
</div>
</div>
</s:form>
