<%@ page import="com.ffusion.beans.billpay.PaymentDefines"%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="payments_billpaytempdelete" className="moduleHelpClass"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_41')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<div class="approvalDialogHt2">
<ffi:setProperty name='PageText' value=''/>
<ffi:cinclude value1="${PaymentReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/payments_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0" hspace="3"></a>'/>
</ffi:cinclude>
<%-- <%
	session.setAttribute("SetPaymentTemplateID", request.getParameter("SetPaymentTemplateID"));
	session.setAttribute("PaymentType", PaymentDefines.PAYMENT_TYPE_TEMPLATE);
%>

<ffi:removeProperty name="CancelPaymentBatchTemplate,CancelPaymentTemplate"/>
    <ffi:list collection="PaymentTemplates" items="Template" filter="ID=${SetPaymentTemplateID}" >
    </ffi:list>
 --%>			<%-- <ffi:cinclude value1="${Template.Class.Name}" value2="com.ffusion.beans.billpay.PaymentBatch" operator="notEquals">
Single Template (not batch)
                    <ffi:cinclude value1="${Template.Class.Name}" value2="com.ffusion.beans.billpay.Payment" operator="equals">
                        <ffi:object id="GetPaymentByID" name="com.ffusion.tasks.billpay.GetPaymentByID" scope="session"/>
                            <ffi:setProperty name="GetPaymentByID" property="ID" value="${SetPaymentTemplateID}" />
                        <ffi:process name="GetPaymentByID" />
                        <ffi:removeProperty name="GetPaymentByID"/>
                     </ffi:cinclude>
                    <ffi:cinclude value1="${Template.Class.Name}" value2="com.ffusion.beans.billpay.RecPayment" operator="equals">
                        <ffi:object id="GetPaymentByID" name="com.ffusion.tasks.billpay.GetRecPaymentByID" scope="session"/>
                            <ffi:setProperty name="GetPaymentByID" property="ID" value="${SetPaymentTemplateID}" />
                        <ffi:process name="GetPaymentByID" />
                        <ffi:removeProperty name="GetPaymentByID"/>
                     </ffi:cinclude> 
                <ffi:object id="SetPayee" name="com.ffusion.tasks.billpay.SetPayee"/>
                    <ffi:setProperty name="SetPayee" property="id" value="${Payment.Payee.ID}"/>
                <ffi:process name="SetPayee"/>--%>
                <ffi:removeProperty name="OpenEnded" />
              <%--   <ffi:object id="CancelPaymentTemplate" name="com.ffusion.tasks.billpay.CancelPayment" scope="session"/> --%>


<%-- for localizing state name --%>
<%-- <ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${Payment.Payee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${Payment.Payee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/> --%>

<%-- <ffi:object id="GetDefaultCurrency" name="com.ffusion.tasks.billpay.GetDefaultCurrency" scope="session"/>
<ffi:process name="GetDefaultCurrency"/>
<ffi:cinclude value1="${Payment.Payee.PayeeRoute.CurrencyCode}" value2="" operator="equals">
<ffi:setProperty name="Payment" property="Payee.PayeeRoute.CurrencyCode" value="${DEFAULT_CURRENCY.CurrencyCode}"/>
</ffi:cinclude> --%>

<ffi:setProperty name="Payment" property="AmountValue.CurrencyCode" value="${Payment.AmtCurrency}"/>
<s:form action="/pages/jsp/billpay/deleteBillPayTemplateAction.action" method="post" name="billpayDelete" id="deleteBillpayTempFormId" >
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden value="%{ID}" name="ID"></s:hidden>
<s:hidden value="%{paymentType}" name="paymentType"></s:hidden>
<div  class="blockHead toggleClick expandArrow">
<s:text name="jsp.billpay.payee_summary" />: <ffi:getProperty name="Payment" property="Payee.Name"/> (<ffi:getProperty name="Payment" property="Payee.NickName"/> - <s:property value="%{payment.payee.UserAccountNumber}"/>)
<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_314"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.Name"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.NickName"/></span>
											
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_36"/></span>
				<span  ><ffi:getProperty name="Payment" property="Payee.ContactName"/></span>						
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_83"/>:</span>
				<span  ><ffi:getProperty name="Payment" property="Payee.Phone"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>	
				<span class="columndata" >
					<ffi:getProperty name="Payment" property="Payee.Street"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_37"/>:</span>
				<span class="columndata"  >
					<ffi:getProperty name="Payment" property="Payee.Street2"/>
				</span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" >
						<s:text name="jsp.default_38"/>:
				</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.Street3"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_101"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_104"/>:</span>
				<span class="columndata"  ><s:property value="%{payment.Payee.StateName}"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_473"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Payee.ZipCode"/></span>
											
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_115"/>:</span>
			<span><s:property value="%{payment.Payee.CountryName}"/></span>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart--><s:text name="jsp.transaction.summary.label" /><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_416"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="TemplateName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_15"/>:</span>
				<span class="columndata"  ><s:property value="%{payment.account.accountDisplayText}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_43"/>:</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="AmountValue.CurrencyStringNoSymbol" />
					<s:property value="%{payment.AmountValue.CurrencyCode}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" >
                                            <!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
                  <s:if test="%{payment.convertedAmount!=''}">
        	    	<!-- do nothing --> 
        	    </s:if>
        	    <s:else><s:text name="jsp.billpay_40"/>:</s:else>					
				</span>
				<span class="columndata"  >
					<!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
					 <s:if test="%{payment.convertedAmount!=''}">
	        	    	<!-- do nothing -->
	        	    </s:if>
	        	    <s:else>
	        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
	        	    </s:else>
				</span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" >
					<s:text name="jsp.default_351"/>:
				</span>
				<span class="columndata"  ><ffi:getProperty name="Payment" property="Frequency"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/>:</span>
				<span class="columndata"  >
					<ffi:cinclude value1="${Payment.NumberPayments}" value2="999" operator="equals">
						<s:text name="jsp.default_446"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Payment.NumberPayments}" value2="999" operator="notEquals">
						<ffi:getProperty name="Payment" property="NumberPayments"/>
					</ffi:cinclude>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_279"/>:</span>
			<span class="columndata"  ><ffi:getProperty name="Payment" property="Memo"/></span>
		</div>
	</div>
</div>
		<%-- <div align="center">
			<div align="left">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
						
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_24"/><br>
												</span></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color"></td>
											<td></td>
											<td></td>
											<td class="sectionsubhead"><br>
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle"><ffi:getProperty name="Payment" property="Payee.Name"/></td>
											<td class="tbrd_l" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_416"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle"><ffi:getProperty name="Payment" property="TemplateName"/></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle"><ffi:getProperty name="Payment" property="Payee.NickName"/>
											</td>
											<td class="tbrd_l" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle"><ffi:getProperty name="Payment" property="AmountValue.CurrencyStringNoSymbol" />
											<s:property value="%{payment.AmountValue.CurrencyCode}"/>
											</td>
										</tr>
										
										<tr>
										<td class="columndata ltrow2_color">
                                           <div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
										</td>										
										<td class="columndata" colspan="2">
											<ffi:getProperty name="Payment" property="Payee.Street"/>
										</td>
											<td class="tbrd_l" valign="middle">
											<ffi:cinclude value1="${Payment.AmountValue.CurrencyCode}" value2="${Payment.Account.CurrencyCode}" operator="notEquals" >
                                                <div align="right">
                                                    <span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
                                            </ffi:cinclude>
                                            <ffi:cinclude value1="${Payment.AmountValue.CurrencyCode}" value2="${Payment.Account.CurrencyCode}" operator="equals" >
                                                &nbsp;
                                            </ffi:cinclude>	
                                            <!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
                                            <div align="right">
								        	    <span class="sectionsubhead">		
                                            <s:if test="%{payment.convertedAmount!=''}">
							        	    	<!-- do nothing --> 
							        	    </s:if>
							        	    <s:else> <s:text name="jsp.billpay_40"/> </s:else></span></div>							
											</td>
											<td class="columndata" colspan="2" valign="middle">
												<ffi:cinclude value1="${Payment.AmountValue.CurrencyCode}" value2="${Payment.Account.CurrencyCode}" operator="notEquals" >
												<ffi:cinclude value1="${Payment.AmountValue.CurrencyCode}" value2="" operator="notEquals" >
												<ffi:cinclude value1="${Payment.Account.CurrencyCode}" value2="" operator="notEquals" >
												
												<ffi:object id="ConvertToTargetCurrency" name="com.ffusion.tasks.fx.ConvertToTargetCurrency" scope="session"/>
												<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmount" value="${Payment.AmountForBPW}" />
												<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmtCurrency" value="${Payment.AmountValue.CurrencyCode}" />
												<ffi:setProperty name="ConvertToTargetCurrency" property="TargetAmtCurrency" value="${Payment.Account.CurrencyCode}" />
												<ffi:process name="ConvertToTargetCurrency"/>
												<ffi:setProperty name="CONVERTED_CURRENCY_AMOUNT"  value="${ConvertToTargetCurrency.TargetAmount}"/>
					
														 &#8776; <s:property value="#attr.ConvertToTargetCurrency.targetAmountWithCommaSeparator"/>
														 <ffi:getProperty name="Payment" property="Account.CurrencyCode"/>
												</ffi:cinclude>
												</ffi:cinclude>
												</ffi:cinclude>
												<!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
												 <s:if test="%{payment.convertedAmount!=''}">
								        	    	<!-- do nothing -->
								        	    </s:if>
								        	    <s:else>
								        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
								        	    </s:else>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_37"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle">
												<ffi:getProperty name="Payment" property="Payee.Street2"/>
											</td>
											<td class="tbrd_l" valign="middle">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_15"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle">
												<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
												<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY %>"/>			
												<s:set var="AccountDisplayTextTaskBean" value="#attr.Payment.Account" scope="request" />
												<ffi:process name="AccountDisplayTextTask"/>
												<ffi:getProperty name="AccountDisplayTextTask" property="accountDisplayText"/>
												 <s:property value="%{payment.account.accountDisplayText}"/></td>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_38"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle"><ffi:getProperty name="Payment" property="Payee.Street3"/></td>
											<td class="tbrd_l" valign="middle">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_351"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle"><ffi:getProperty name="Payment" property="Frequency"/></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_101"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle"><ffi:getProperty name="Payment" property="Payee.City"/></td>
											<td class="tbrd_l" valign="middle">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_321"/> </span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle">
												<ffi:cinclude value1="${Payment.NumberPayments}" value2="999" operator="equals">
													<s:text name="jsp.default_446"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${Payment.NumberPayments}" value2="999" operator="notEquals">
													<ffi:getProperty name="Payment" property="NumberPayments"/>
												</ffi:cinclude>
											</td>

											</tr>
										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle">												
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="Payment" property="Payee.State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
													<s:property value="%{payment.Payee.StateName}"/>
											</td>
											<td class="tbrd_l" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><span><s:text name="jsp.default_279"/></span></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle"><ffi:getProperty name="Payment" property="Memo"/></td>
											</tr>
										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td class="columndata" colspan="2" valign="middle"><ffi:getProperty name="Payment" property="Payee.ZipCode"/></td>
											<td class="tbrd_l" valign="middle">												
											</td>											
											<td class="columndata" colspan="2"></td>	
										</tr>
										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_115"/></span></div>
											</td>
											<td colspan="2" valign="middle"><s:property value="%{payment.Payee.CountryName}"/></td>
											<td class="tbrd_l" valign="middle">												
											</td>											
											<td class="columndata" colspan="2"></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_36"/>
													</span>
												</div>
											</td>
											<td colspan="2" valign="middle"><ffi:getProperty name="Payment" property="Payee.ContactName"/></td>
											<td class="tbrd_l ltrow2_color" valign="middle">												
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr>

										<tr>
											<td class="columndata ltrow2_color" valign="middle">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_37"/></span></div>
											</td>
											<td colspan="2" valign="middle"><ffi:getProperty name="Payment" property="Payee.Phone"/></td>
											<td class="tbrd_l ltrow2_color" valign="middle">												
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr>
                                        <tr>
											<td class="columndata ltrow2_color">
												<div align="right">

												</div>
											</td>
											<td colspan="4"><div align="center">
                                                    <ffi:cinclude value1="${Payment.AmountValue.CurrencyCode}" value2="${Payment.Account.CurrencyCode}" operator="notEquals" >
                                                        <br><span class="required">&#8776; <s:text name="jsp.default_241"/></span><br>
                                                    </ffi:cinclude>
                                                     <s:if test="%{IncludeViewPayment.AmtCurrency!=IncludeViewPayment.Account.CurrencyCode}">
												    	<tr>
												        <td class="required" colspan="4">
												            <div align="right">&#8776; <s:text name="jsp.default_241"/></div>
												        </td>
												    </tr>
												    </s:if>
                                                    <br>
											    <sj:a id="cancelDeleteMultiTempFormButton" 
											             button="true" 
												onClickTopics="closeDialog"
											     	><s:text name="jsp.default_82"/></sj:a>
											     <sj:a id="deleteMultiTempLink" formIds="deleteBillpayTempFormId" targets="resultmessage" button="true"   
													title="%{getText('Delete_Template')}" onCompleteTopics="cancelSingleBillpayTemplateComplete" onSuccessTopics="cancelSingleBillpayTemplateSuccessTopics" onErrorTopics="errorDeleteBillpay" 
													effectDuration="1500" ><s:text name="DELETE_TEMPLATE"/>
												</sj:a>
											<td></td>
										</tr>
									</table>
							
						</td>
						<td></td>
					</tr>
				</table>
			</div>
		</div> --%>
<s:if test="%{IncludeViewPayment.AmtCurrency!=IncludeViewPayment.Account.CurrencyCode}">
	<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="ffivisible" style="height:150px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
	<sj:a id="cancelDeleteMultiTempFormButton" 
											             button="true" 
												onClickTopics="closeDialog"
											     	><s:text name="jsp.default_82"/></sj:a>
											     <sj:a id="deleteMultiTempLink" formIds="deleteBillpayTempFormId" targets="resultmessage" button="true"   
													title="%{getText('Delete_Template')}" onCompleteTopics="cancelSingleBillpayTemplateComplete" onSuccessTopics="cancelSingleBillpayTemplateSuccessTopics" onErrorTopics="errorDeleteBillpay" 
													effectDuration="1500" ><s:text name="jsp.default_162" />
												</sj:a>
</div>
</div>
</s:form>
	</body>

</html>

<%-- 			</ffi:cinclude> --%>
			
	<%-- 		<ffi:cinclude value1="${Template.Class.Name}" value2="com.ffusion.beans.billpay.PaymentBatch">
			multiple
Multiple Template (is a batch)
                <ffi:object id="GetPaymentBatch" name="com.ffusion.tasks.billpay.GetPaymentBatch" scope="request"/>
                <ffi:setProperty name="GetPaymentBatch" property="ID" value="${SetPaymentTemplateID}"/>
                <ffi:process name="GetPaymentBatch"/>
                <ffi:removeProperty name="GetPaymentBatch"/>
                <ffi:object id="CancelPaymentBatchTemplate" name="com.ffusion.tasks.billpay.CancelPaymentBatch" scope="session"/>


		<div align="center">
			<div align="left">
				<table width="750" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">

<ffi:object id="GetDefaultCurrency" name="com.ffusion.tasks.billpay.GetDefaultCurrency" scope="session"/>
<ffi:process name="GetDefaultCurrency"/>


<s:form action="/pages/jsp/billpay/cancelBillpayBatchAction_execute.action" method="post" name="billpayMultiTempDelete" id="deleteBillpayMultiTempBatchFormId" >
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<table width="100%" border="0" cellspacing="0" cellpadding="">
    <tr>
        <td class="tbrd_b ltrow2_color" colspan="2"><span class="sectionhead">&gt; <s:text name="jsp.billpay_24"/><br>
            </span></td>
    </tr>
    <tr>
        <td class="columndata ltrow2_color"></td>
        <td></td>
        <td></td>
        <td class="sectionsubhead"><br>
        </td>
        <td></td>
        <td></td>
    </tr>
<ffi:cinclude value1="${PaymentBatch.BatchType}" value2="<%=PaymentDefines.PAYMENT_TYPE_TEMPLATE%>" operator="equals">
	<tr>
        <td width="20"></td>
        <td>
            <span class="sectionsubhead"><s:text name="jsp.billpay_79"/>&nbsp;&nbsp;</span>
            <span class="columndata"><ffi:getProperty name="PaymentBatch" property="TemplateName" /></span>
		</td>
    </tr>
</ffi:cinclude>
	<tr>
		<td width="20"></td>
		<td>
            <span class="sectionsubhead"><s:text name="jsp.default_217"/>&nbsp;&nbsp;</span>
            <span class="columndata">
            <ffi:list collection="PaymentBatch.Payments" items="Payment" startIndex="1" endIndex="1" >
	            <ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
				<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY %>"/>			
				<s:set var="AccountDisplayTextTaskBean" value="#attr.Payment.Account" scope="request" />
				<ffi:process name="AccountDisplayTextTask"/>
				<ffi:getProperty name="AccountDisplayTextTask" property="accountDisplayText"/>
				<ffi:setProperty name="selectedcurrency" value="${Payment.Account.CurrencyCode}"/>
			</ffi:list>
            </span>
        </td>
	</tr>
</table>
<br>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td class="sectionsubhead" width="30%" height="20">&nbsp;<s:text name="jsp.default_313"/></td>
		<td class="sectionsubhead" width="10%" align="right"><s:text name="jsp.default_43"/></td>
        <td class="sectionsubhead" width="5%">&nbsp;</td>
        <td class="sectionsubhead" width="30%"><ffi:cinclude value1="${RegisterEnabled}" value2="true"><s:text name="jsp.default_88"/></ffi:cinclude></td>
		<td class="sectionsubhead" align="left" width="30%" nowrap><s:text name="jsp.default_279"/></td>
	</tr>
	<ffi:setProperty name="PaymentBatch" property="Payments.Filter" value="All" />
    <ffi:setProperty name="checkMultiCurrency" value="false"/>
    <ffi:setProperty name="currency" value="${DEFAULT_CURRENCY.CurrencyCode}"/>
   
	<ffi:list collection="PaymentBatch.Payments" items="Payment1">
		<ffi:setProperty name="PaymentBatch" property="CurrentPayment" value="${Payment1.ID}" />
		<ffi:cinclude value1="${Payment1.AmountValue.IsZero}" value2="false" operator="equals">
			<tr>
				<td height="20" class="columndata">
					<ffi:cinclude value1="" value2="${Payment1.PayeeName}" operator="notEquals" ><ffi:getProperty name="Payment1" property="PayeeName"/></ffi:cinclude>
					<ffi:cinclude value1="" value2="${Payment1.PayeeNickName}" operator="notEquals" > (<ffi:getProperty name="Payment1" property="PayeeNickName"/></ffi:cinclude><ffi:cinclude value1="" value2="${Payment1.Payee.UserAccountNumber}" operator="notEquals" > - <ffi:getProperty name="Payment1" property="Payee.UserAccountNumber"/></ffi:cinclude>)
					<br>
				</td>
				<td align="right" class="columndata" nowrap="nowrap">
			<ffi:cinclude value1="${Payment1.Payee.PayeeRoute.CurrencyCode}" value2="" operator="equals">
			<ffi:setProperty name="Payment1" property="Payee.PayeeRoute.CurrencyCode" value="${DEFAULT_CURRENCY.CurrencyCode}"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${checkMultiCurrency}" value2="false" operator="equals" >
			<ffi:cinclude value1="${currency}" value2="" operator="notEquals">
			<ffi:cinclude value1="${currency}" value2="${Payment1.Payee.PayeeRoute.CurrencyCode}" operator="notEquals" >
			<ffi:setProperty name="checkMultiCurrency" value="true"/>
			</ffi:cinclude>
			</ffi:cinclude>
			</ffi:cinclude>
			<ffi:setProperty name="currency" value="${Payment1.Payee.PayeeRoute.CurrencyCode}"/>
			<ffi:cinclude value1="${checkMultiCurrency}" value2="false" operator="equals" >
				<ffi:setProperty name="PayeeCommonCurrency" value="${Payment1.Payee.PayeeRoute.CurrencyCode}"/>
			</ffi:cinclude>
				
				<ffi:getProperty name="Payment1" property="AmountValue.CurrencyStringNoSymbol"/> <ffi:getProperty name="Payment1" property="AmtCurrency"/>
				<ffi:setProperty name="Payment1" property="AmountValue.CurrencyCode" value="${Payment1.AmtCurrency}"/>

				<ffi:cinclude value1="${Payment1.AmountValue.CurrencyCode}" value2="${selectedcurrency}" operator="notEquals" >
				<ffi:cinclude value1="${Payment1.AmountValue.CurrencyCode}" value2="" operator="notEquals" >
				<ffi:cinclude value1="${selectedcurrency}" value2="" operator="notEquals" >
					<ffi:object id="ConvertToTargetCurrency" name="com.ffusion.tasks.fx.ConvertToTargetCurrency" scope="session"/>
					<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmount" value="${Payment1.AmountForBPW}" />
					<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmtCurrency" value="${Payment1.AmountValue.CurrencyCode}" />
					<ffi:setProperty name="ConvertToTargetCurrency" property="TargetAmtCurrency" value="${selectedcurrency}"/>
					<ffi:process name="ConvertToTargetCurrency"/>
				<ffi:setProperty name="CONVERTED_CURRENCY_AMOUNT"  value="${ConvertToTargetCurrency.TargetAmount}"/>
				  		 	
				<input type="hidden" name="convertedAmount" id="convertedAmount" value="<ffi:getProperty name="CONVERTED_CURRENCY_AMOUNT"/>">
				</ffi:cinclude>
				</ffi:cinclude>
				</ffi:cinclude>
							
				<ffi:cinclude value1="${Payment1.AmountValue.CurrencyCode}" value2="${selectedcurrency}" operator="equals" >
				<input type="hidden" name="convertedAmount1" id="convertedAmount1" value="<ffi:getProperty name="Payment1" property="AmountForBPW"/>">
				</ffi:cinclude>
								
				</td>
                <td>&nbsp;</td>

                <td class="columndata">
				   <!-- ====================== BEGIN REGISTER INTEGRATION ===================== -->
					<ffi:cinclude value1="${RegisterEnabled}" value2="true" operator="equals">
						<ffi:setProperty name="RegisterCategories" property="Current" value="${Payment1.REGISTER_CATEGORY_ID}"/>
						<ffi:setProperty name="Parentfalse" value=""/>
						<ffi:setProperty name="Parenttrue" value=": "/>
						<ffi:getProperty name="RegisterCategories" property="ParentName"/><ffi:getProperty name="Parent${RegisterCategories.HasParent}"/><ffi:getProperty name="RegisterCategories" property="Name"/>
						<br>
					</ffi:cinclude>
					<!-- ====================== END REGISTER INTEGRATION ===================== -->
				</td>
				<td><ffi:getProperty name="Payment1" property="Memo"/></td>
			</tr>
		</ffi:cinclude>
	</ffi:list>
	
	<tr>	
		<td class="totals_row" nowrap valign="top">&nbsp;</td>
		<td class="totals_row" nowrap valign="top">&nbsp;</td>
		<td class="totals_row" nowrap valign="top">&nbsp;</td>
		<td class="totals_row" nowrap valign="top">&nbsp;</td>
	</tr>

    <input type="hidden" name="checkMultiCurrencyValue" id="checkMultiCurrencyValue" value="<ffi:getProperty name="checkMultiCurrency"/>">
    <input type="hidden" name="PayeeCommonCurrency" id="payeeCommonCurrency" value="<ffi:getProperty name="PayeeCommonCurrency"/>"/>
	<input type="hidden" name="bankAccountCurrency" id="bankAccountCurrency" value="<ffi:getProperty name="selectedcurrency"/>">
    <input type="hidden" name="calculatedAmount" id="calculatedAmount">
    <ffi:setProperty name="bankAccountCurrency" value="${selectedcurrency}"/>
	    
    <ffi:cinclude value1="${checkMultiCurrency}" value2="false" operator="equals">
    <tr>
		<td class="sectionsubhead" nowrap align="right" colspan="2">
			<s:text name="jsp.default_431"/>:&nbsp;
            <span id="total" ></span> <ffi:getProperty name="PayeeCommonCurrency"/>
        </td>
    </tr>    
    </ffi:cinclude>
    <ffi:cinclude value1="${checkMultiCurrency}" value2="true" operator="equals">
    <tr>
        <td class="sectionsubhead" align="right" colspan="2">
            <span id="estimatedTotalLabel"><s:text name="jsp.default_431"/>:&nbsp;</span>
            &#8776; <span id="estimatedTotal2"></span> <span id="estimatedTotalCurrency"><ffi:getProperty name="selectedcurrency"/></span>
        </td>
    </tr>
    </ffi:cinclude>
    </table>
</s:form>
<br>
<center>
   <sj:a id="cancelDeleteMultiTempFormButton" 
             button="true" 
	onClickTopics="closeDialog"
     	><s:text name="jsp.default_82"/></sj:a>
     <sj:a id="deleteMultiTempLink" formIds="deleteBillpayMultiTempBatchFormId" targets="resultmessage" button="true"   
		title="%{getText('Delete_Template')" onCompleteTopics="cancelMultipleBillpayTemplateComplete" onSuccessTopics="cancelMultipleBillpayTemplateSuccessTopics" onErrorTopics="errorDeleteBillpay" 
		effectDuration="1500" ><s:text name="DELETE_TEMPLATE" />
	</sj:a>
</center>

================= MAIN CONTENT END =================


						</td>
						<td></td>
					</tr>
					
				</table>
				<br>
			</div>
		</div>
	
<script language="javascript">
var total = 0;
var total1 = 0;
var tempValue=0;
var tempValue1=0;
var frm = document.getElementById("deleteBillpayMultiTempBatchFormId");
var length=0;
var length1=0;
if(frm.convertedAmount != null)
length =frm.convertedAmount.length;
if(frm.convertedAmount1 != null)
 length1=frm.convertedAmount1.length;

var cnt=0;
 
if(length == 0 ||isNaN(length)){

	  if(frm.convertedAmount != null){
		 tempValue= frm.convertedAmount.value;
		 if (tempValue != "" && !isNaN(tempValue))
		          {
		              total += parseFloat(tempValue);
		         }
	 
	  }
}
else{

	 for(cnt=0;cnt<length;cnt++){

		 tempValue= frm.convertedAmount[cnt].value;
	
		 
		 if (tempValue != "" && !isNaN(tempValue))
		         {
		             total += parseFloat(tempValue);
		        }

    }
}
/*Code for ESt total calculation if currency is same*/

if(length1 == 0 ||isNaN(length1)){
	  if(frm.convertedAmount1 != null)
	  {
	  
	  tempValue1= frm.convertedAmount1.value;
	 
	  if (tempValue1 != "" && !isNaN(tempValue1))
	           {
	               total1 += parseFloat(tempValue1);
	          }
	  
	   }
 
 }
 else{
 
	  for(cnt=0;cnt<length1;cnt++){
	  
		  tempValue1= frm.convertedAmount1[cnt].value;
		 
		  if (tempValue1 != "" && !isNaN(tempValue1))
		  {
             total1 += parseFloat(tempValue1);
         }	
	  
	    }
}


total=total+total1;
total = ns.common.formatTotal(total);
var checkMultiCurrency= frm['checkMultiCurrencyValue'].value;
var payeeCommonCurrency=frm['payeeCommonCurrency'].value;
var bankAccountCurrency=frm['bankAccountCurrency'].value;

if(checkMultiCurrency == "true") {
 	$('#deleteBillpayMultiTempBatchFormId').find('#estimatedTotal2').text(total);
 	if ($('#deleteBillpayMultiTempBatchFormId').find('#calculatedAmount').is(':visible')) {
 		$('#deleteBillpayMultiTempBatchFormId').find('#calculatedAmount').text(total);
 	}
} else {
	$('#deleteBillpayMultiTempBatchFormId').find('#total').text(total);
	if ($('#deleteBillpayMultiTempBatchFormId').find('#calculatedAmount').is(':visible')) {
	 	$('#deleteBillpayMultiTempBatchFormId').find('#calculatedAmount').text(total);
 	}
 	
} 
</script>



			</ffi:cinclude>
    

 --%>
 <script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>