<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_billpayeditbill" className="moduleHelpClass"/>
<script language="JavaScript">

$(function(){
	//$("#AccountID").combobox({'size':'40'});
	$("#AccountID").lookupbox({
		"controlType":"server",
		"collectionKey":"1",
		"module":"billpay",
		"size":"40",
		"source":"/cb/pages/jsp/billpay/BillPayAccountsLookupBoxAction.action"
	});

	// disable Repeating frequency by default, enable only if rec model is being edited and hide once rec model editing is selected
	if($('#isRecModelEdit').val()=='false'){
		$("#frequencyId").attr('disabled',true);
	}else{
		$('#loadRecModelFormButton').hide();
		$('#loadRecModelForDelete').hide();
	}
	$("#frequencyId").selectmenu({width: 120});

});

function setRecurring(state){
	if (state == true){
		document.EditpayNewForm.numberPayments.disabled = true;
		document.EditpayNewForm.numberPayments.value= '';
	}
}



function enableDisableNumPayments()
{
	if ($('input[name="editPayment.openEnded"]')[1].checked == true){
		// disable number of payment field by default, enable only if rec model is being edited
		if($('#isRecModelEdit').val()=='true')
		$('input[name="editPayment.numberPayments"]').prop('disabled',false);
	} else {
		$('input[name="editPayment.numberPayments"]').prop('disabled',true);
	}
}


function updateAccountName(){
	var displayText = $.trim($('select#AccountID option:selected').html());
	$('#accountName').html(displayText)
}
// -->
</script>

<!-- only load js for recurring if payment is or recurring type -->
<s:if test='%{recPaymentID != "null"}'>
<script type="text/javascript">
$(function(){
	enableDisableNumPayments();
});
</script>
</s:if>
<s:form id="BillpayEdit" namespace="/pages/jsp/billpay" validate="false" action="editBillpayAction_verify" method="post" name="EditpayNewForm" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden name="recPaymentID" value="%{recPaymentID}" id="recPaymentID"></s:hidden>
<input id="ID" name="ID" value="<s:property value="%{ID}"/>" type="hidden"/>
<input id="isRecModelEdit" name="isRecModel" value="<s:property value="%{isRecModel}"/>" type="hidden"/>
<%-- <input type="hidden" name="RegEnabledAccounts" value="<ffi:list collection="PaymentAccounts" items="Account"><ffi:cinclude value1="${Account.REG_ENABLED}" value2="true"><ffi:getProperty name="Account" property="ID"/>,</ffi:cinclude></ffi:list>"> --%>
<input type="hidden" name="status" value='<ffi:getProperty name="editPayment" property="Status" />'>
<span style="display:none;" id="PageHeading"><s:text name="jsp.billpay_134"/></span>
<div align="center">
<div class="leftPaneWrapper" role="form" aria-labeledby="billPaySummary">
	<div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="billPaySummary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue"><s:property value="%{editPayment.payeeName}"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_15"/>:</span>
				<span class="inlineSection floatleft labelValue" id="accountName"> <s:property value="%{editPayment.Account.accountDisplayText}"/>
				</span>
			</div>
	
	<div class="marginTop20 floatleft fxActionBtnCls" style="width:100%"><s:if test='%{recPaymentID != "null"}'>
		<sj:a id="loadRecModelFormButton"
			                button="true"
			                onclick="loadRecModel()"
			            ><s:text name="jsp.default.edit_orig_payment"/></sj:a>
	</s:if></div></div>
</div>

</div>
<div class="confirmPageDetails">
<span id="payee.idError"></span>
<div class="clearBoth"></div>
<div  class="blockHead toggleClick expandArrow">
<s:text name="jsp.billpay.payee_summary" />: <s:property value="%{editPayment.payeeName}"/> (<s:property value="%{editPayment.payee.NickName}"/> - <s:property value="%{editPayment.payee.userAccountNumber}"/>)
<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
	<div class="blockWrapper">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_75"/>:</span>
				<span id="editBillPay_payeeNickNameValue" class="columndata" ><s:property value="%{editPayment.payee.NickName}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editBillPay_lastPaymentLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_last_payment_to_payee"/>:</span>
				<span id="editBillPay_lastPaymentValue" class="columndata instructions">
				 <ffi:list collection="lastPaymentToPayee" items="lastPayment" startIndex="1" endIndex="1">
					<ffi:setProperty name="lastPayment" property="DateFormat"/>
					<ffi:getProperty name="lastPayment" property="Amount"/>-<ffi:getProperty name="lastPayment" property="PayDate"/>
				</ffi:list>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editBillPay_payeeAccountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_15"/>:</span>
				<span id="editBillPay_payeeAccountValue" class="columndata" ><s:property value="%{editPayment.payee.userAccountNumber}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editBillPay_payeeFirstAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
				<span id="editBillPay_payeeFirstAddressValue" class="columndata" ><s:property value="%{editPayment.payee.street}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editBillPay_payeeSecondAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
				<span id="editBillPay_payeeSecondAddressValue" class="columndata" ><s:property value="%{editPayment.payee.street2}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editBillPay_payeeThirdAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
				<span id="editBillPay_payeeThirdAddressValue" class="columndata" ><s:property value="%{editPayment.payee.street3}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editBillPay_cityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
				<span id="editBillPay_cityValue" class="columndata" ><s:property value="%{editPayment.payee.city}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editBillPay_stateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
				<span id="editBillPay_stateValue" class="columndata" ><s:property value="%{editPayment.payee.stateDisplayName}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editBillPay_zipCodeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
				<span id="editBillPay_zipCodeValue" class="columndata" ><s:property value="%{editPayment.payee.zipCode}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editBillPay_countryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
				<span id="editBillPay_countryValue" class="columndata" ><s:property value="%{editPayment.payee.countryDisplayName}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editBillPay_contactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
				<span id="editBillPay_contactValue" class="columndata" ><s:property value="%{editPayment.payee.contactName}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editBillPay_phoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_83"/></span>
				<span id="editBillPay_phoneValue" class="columndata" ><s:property value="%{editPayment.payee.phone}"/></span>
			</div>
		</div>
	</div>
</div>
</div>
<div align="left" style="vertical-align: top;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding5 marginTop10 ">
										<%-- <tr>
											<td class="tbrd_b ltrow2_color" colspan="5"><span class="sectionhead">&gt; <s:text name="jsp.billpay_46"/><br>
												</span></td>

							<td  class="sectionhead tbrd_b ltrow2_color" align="right">
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FOREIGN_EXCHANGE_UTILITY %>">
														<sj:a
															tooltip="%{getText('jsp.default_214')}"
													    	onclick="ns.common.openDialog('fx');"
													    	button="true"
													    >
															<img src="/cb/web/multilang/grafx/common/i_fx.gif" alt="" width="33" height="14" border="0">
													    </sj:a>
						</ffi:cinclude>
											</td>
										</tr> --%>
									<!-- 	<tr>
										</tr> -->
										<!-- <tr>
											<td class="columndata ltrow2_color">&nbsp;</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td class="sectionsubhead"><br>
											</td>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr> -->
									 <!-- 	<tr>
                                            <td colspan=3 align="center">
                                                <table>
                                                   <tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
                                                    </tr> -->
                                                    <%-- <tr>
                                                    	<td align="center" colspan="2"><span id="payee.idError"></span><br></td>
                                                    </tr> --%>
                                                    <%-- <tr>
											<td id="editBillPay_payeeNickNameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td id="editBillPay_payeeNickNameValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.NickName}"/></td>
                                                    </tr>
													
													<!-- last payments -->
											<tr>
											<td id="editBillPay_lastPaymentLabel" align="right" class="columndata ltrow2_color instructions">
											<div align="right">
													<s:text name="jsp.billpay_last_payment_to_payee"/></div>
													</td>
											<td id="editBillPay_lastPaymentValue" class="columndata instructions" colspan="2" align="left" style="padding:2px;">
												 <ffi:list collection="lastPaymentToPayee" items="lastPayment" startIndex="1" endIndex="1">
													<ffi:setProperty name="lastPayment" property="DateFormat"/>
													<ffi:getProperty name="lastPayment" property="Amount"/>-<ffi:getProperty name="lastPayment" property="PayDate"/>
												</ffi:list>
												</td>
										</tr>		
                                                    <tr>
											<td id="editBillPay_payeeNameLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="editBillPay_payeeNameValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payeeName}"/>
											<ffi:getProperty name="editPayment" property="PayeeName" /></td>
                                                    </tr>
                                                    <tr>
											<td id="editBillPay_payeeAccountLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td>
											<td id="editBillPay_payeeAccountValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.userAccountNumber}"/>
											<ffi:getProperty name="editPayment" property="Payee.UserAccountNumber" /></td>
                                                    </tr>
                                                    <tr>
											<td id="editBillPay_payeeFirstAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td id="editBillPay_payeeFirstAddressValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.street}"/>
											<ffi:getProperty name="editPayment" property="Payee.Street" /></td>
                                                    </tr>
                                                    <tr>
											<td id="editBillPay_payeeSecondAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="editBillPay_payeeSecondAddressValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.street2}"/>
											<ffi:getProperty name="editPayment" property="Payee.Street2" /></td>
                                                    </tr>
                                                    <tr>
											<td id="editBillPay_payeeThirdAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="editBillPay_payeeThirdAddressValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.street3}"/>
											<ffi:getProperty name="editPayment" property="Payee.Street3" /></td>
                                                    </tr>
                                                    <tr>
											<td id="editBillPay_cityLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td id="editBillPay_cityValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.city}"/>
											<ffi:getProperty name="editPayment" property="Payee.City" /></td>
                                                    </tr>
                                                    <tr>
											<td id="editBillPay_stateLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>

											<td id="editBillPay_stateValue" class="columndata" colspan="2">
												<s:property value="%{editPayment.payee.stateDisplayName}"/>
											</td>
                                                    </tr>
										<tr>
											<td id="editBillPay_zipCodeLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td id="editBillPay_zipCodeValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.zipCode}"/>
											<ffi:getProperty name="editPayment" property="Payee.ZipCode" /></td>
										</tr>
										<tr>
											<td id="editBillPay_countryLabel" class="">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_115"/></span></div>
											</td>
											<td id="editBillPay_countryValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.countryDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="editBillPay_contactLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="editBillPay_contactValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.contactName}"/>
										</tr>
										<tr>
											<td id="editBillPay_phoneLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_83"/></span></div>
											</td>
											<td id="editBillPay_phoneValue" class="columndata" colspan="2">
											<s:property value="%{editPayment.payee.phone}"/>
										</tr> 
                                                </table>
                                            </td>

                                            <td class="columndata tbrd_l" colspan=3>
                                                <table>--%>
                                             <!--        <tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
                                                    </tr> -->
<tr>
	<td width="50%" id="editBillPay_amountLabel" class="columndata"><span class="sectionsubhead"><label for="Amount"><s:text name="jsp.default_43"/></label></span><span class="required" title="required">*</span> (<s:property value="%{editPayment.payee.payeeRoute.currencyCode}"/>)</td>
	<td width="50%" id="editBillPay_fromAccountLabel" class="columndata"><span class="sectionsubhead"><label for="AccountID"><s:text name="jsp.default_15"/></label></span><span class="required" title="required">*</span></td>
</tr>
<tr>
	<td>
		<s:textfield name="editPayment.amount" id="Amount" aria-required="true" size="12" maxlength="16" value="%{editPayment.AmountValue.AmountValue}" cssClass="ui-widget-content ui-corner-all"/>
		<%-- <ffi:getProperty name="Payee" property="PayeeRoute.CurrencyCode"/> --%>
	</td>
	<td>
		<s:select 
		id="AccountID" 
		list="editPayment.Account.accountDisplayText"
		listKey="editPayment.Account.ID" listValue="editPayment.Account.accountDisplayText"
		onchange="updateAccountName()" 
		name="editPayment.accountID" 
		aria-labeledby="AccountID"
		aria-required="true"
		value="%{editPayment.Account.accountDisplayText}"
		class="txtbox"></s:select>
	</td>
</tr>	
<tr>
	<td><span id="amountError"></span></td>
	<td><span id="accountIDError"></span></td>
</tr>												
<tr>
	<td id="editBillPay_dateLabel" class="columndata"><label for="PayDate"><s:text name="jsp.billpay_76"/></label><span class="required" title="required">*</span></td>
	<td id="editBillPay_memoLabel" valign="middle"><label for="memo"><s:text name="jsp.default_279"/></label></td>
</tr>
<tr>
	<td><sj:datepicker
		value="%{editPayment.PayDate}"
		id="PayDate"
		name="editPayment.payDate"
		label="%{getText('jsp.default_137')}"
		maxlength="10"
		buttonImageOnly="true"
		aria-labeledby="PayDate"
		aria-required="true"
		cssClass="ui-widget-content ui-corner-all" />
                                     <script>
		var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=3&calDisplay=select&calForm=FormName&calTarget=EditPayment.PayDate"/>';
                 ns.common.enableAjaxDatepicker("PayDate", tmpUrl);
                 $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>');
	</script></td>
	<td colspan="2" valign="middle"><s:textfield name="editPayment.memo" id="memo" size="40" maxlength="255" value="%{editPayment.memo}" cssClass="ui-widget-content ui-corner-all"/></td>
	</tr>
	<tr><td><span id="payDateError"></span></td>
		<td></td>
	</tr>
	<s:if test='%{recPaymentID != "null"}'>
	<tr>
		<td colspan="2" id="editBillPay_recurringLabel" valign="middle"><s:text name="jsp.default_351"/></td>
	</s:if>
	<s:if test='%{recPaymentID != "null"}'>
		<tr>
		<td colspan="2" valign="middle">
		
		<s:select id="frequencyId" list="frequencyList" name="editPayment.frequency" value="%{editPayment.frequency}" class="txtbox"></s:select>
		
											<input type="radio" name="editPayment.openEnded" value="true" onClick="enableDisableNumPayments();" <s:if test="%{isRecModel == 'false'}">disabled</s:if>
												<s:if test="%{editPayment.openEnded}">checked</s:if>
											>
											<span class="columndata"><s:text name="jsp.default_446"/></span>
											<input type="radio" name="editPayment.openEnded" value="false" onClick="enableDisableNumPayments();" <s:if test="%{isRecModel == 'false'}">disabled</s:if>
												<s:if test="%{!editPayment.openEnded}">checked</s:if>
											>
											<span class="columndata"><s:text name="jsp.default_2"/></span>
											<input type="text" name="editPayment.numberPayments" maxlength="3" <s:if test="%{isRecModel == 'false'}">disabled</s:if>
											value='<s:property value="%{editPayment.numberPayments}"/>' size="4" 
											class="ui-widget-content ui-corner-all" >
		</td>
		</tr>
	</s:if>
    <tr><td colspan="2"><span id="frequencyError"></span><span id="numberPaymentsError"></span></td></tr>
                                                    <tr>

										<%-- <tr>
											<td colspan="6"><div align="center">
													<br>

													<br>
													<s:hidden value="%{isPortalPage}" name="isPortalPage"></s:hidden>
													<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
													<s:if test="%{isPortalPage=='true'}">
													All topics subscribed here can be found in pendingTransactionPortal.js
														<sj:a id="cancelBillPayEditFormButton"
										                button="true"
										                onClickTopics="cancelPendingBillPaymentsForm"
										            ><s:text name="jsp.default_82"/></sj:a>
										           <sj:a
														id="verifyBillpaySubmit"
														formIds="BillpayEdit"
						                                targets="verifyDiv"
						                                button="true"
						                                validate="true"
						                                validateFunction="customValidation"
						                                onBeforeTopics="beforeVerifyPortalForm"
						                                onErrorTopics="errorVerifyBillpayPortalForm"
						                                onSuccessTopics="successVerifyBillpayPortalForm"
								                        ><s:text name="jsp.default_291"/></sj:a>
								                        <s:hidden value="true" name="fromPortalPage"></s:hidden>
								                     </s:if>
								                     <s:else>
													</ffi:cinclude>
													<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals">
														<sj:a id="cancelEditFormButton"
											                button="true"
															onClickTopics="cancelBillpayForm"
											        	><s:text name="jsp.default_82"/></sj:a>
														<sj:a
															tooltip="%{getText('jsp.transfers_59')}"
															openDialog="innerDeleteBillpayDialog"
															button="true"
														><s:text name="jsp.billpay_128"/></sj:a>

											        	<sj:a
															id="verifyBillpaySubmit"
															formIds="BillpayEdit"
							                                targets="verifyDiv"
							                                button="true"
							                                validate="true"
							                                validateFunction="customValidation"
							                                onBeforeTopics="beforeVerifyBillpayForm"
							                                onCompleteTopics="verifyBillpayFormComplete"
															onErrorTopics="errorVerifyBillpayForm"
							                                onSuccessTopics="successVerifyBillpayForm"
									                        ><s:text name="jsp.default_291"/></sj:a>
								                     </ffi:cinclude>
								                     </s:else> 
								                     </div>
										</tr> --%>
</table>
<div class="btn-row">
				<s:hidden value="%{isPortalPage}" name="isPortalPage"></s:hidden>
				<%-- <ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals"> --%>
				<s:if test="%{isPortalPage=='true'}">
				<%-- All topics subscribed here can be found in pendingTransactionPortal.js --%>
					<sj:a id="cancelBillPayEditFormButton"
	                button="true"
	                onClickTopics="cancelPendingBillPaymentsForm"
	            ><s:text name="jsp.default_82"/></sj:a>
	           <sj:a
					id="verifyBillpaySubmit"
					formIds="BillpayEdit"
                             targets="PendingTransactionWizardTabs #verifyDiv"
                             button="true"
                             validate="true"
                             validateFunction="customValidation"
                             onBeforeTopics="beforeVerifyPortalForm"
                             onErrorTopics="errorVerifyBillpayPortalForm"
                             onSuccessTopics="successVerifyBillpayPortalForm"
                       ><s:text name="jsp.default_291"/></sj:a>
                       <s:hidden value="true" name="fromPortalPage"></s:hidden>
                    </s:if>
                    <s:else>
				<%-- </ffi:cinclude> --%>
				<%-- <ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals"> --%>
					<sj:a id="cancelEditFormButton"
		                button="true"
						onClickTopics="cancelBillpayForm"
		        	><s:text name="jsp.default_82"/></sj:a>
					<sj:a
						tooltip="%{getText('jsp.transfers_59')}"
						openDialog="innerDeleteBillpayDialog"
						button="true"
						resizable="false"
						title="%{#request.deleteDialogTitle}"
					><s:text name="jsp.default_162" /></sj:a>

		        	<sj:a
						id="verifyBillpaySubmit"
						formIds="BillpayEdit"
                              targets="verifyDiv"
                              button="true"
                              validate="true"
                              validateFunction="customValidation"
                              onBeforeTopics="beforeVerifyBillpayForm"
                              onCompleteTopics="verifyBillpayFormComplete"
						onErrorTopics="errorVerifyBillpayForm"
                              onSuccessTopics="successVerifyBillpayForm"
                        ><s:text name="jsp.default_291"/></sj:a>
                    <%-- </ffi:cinclude> --%>
                    </s:else> 
</div>				
				</div>
			</div>
		</div>
</s:form>
		<%-- <ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals"> --%>
		<s:if test="%{isPortalPage!='true'}">
			<sj:dialog
				id="innerDeleteBillpayDialog"
				autoOpen="false"
				modal="true"
				cssClass="pmtTran_billpayDialog staticContentDialog"
				title="%{getText('jsp.billpay_43')}"
				resizable="false"
				height="180"
				width="450"
				overlayColor="#000"
				overlayOpacity="0.7"
				showEffect="fold" hideEffect="clip">
				<div align="center">
				<s:url id="formUrl" value="/pages/jsp/billpay/cancelBillpayAction_execute"/>
				<s:form validate="false" name="innerDeleteBillpayForm" id="innerDeleteBillpayForm" theme="simple">
					<input type="hidden" id="ID" name="ID" value="<s:property value="%{ID}"/> "/>
					<input type="hidden" id="recPaymentID" name="recPaymentID" value="<s:property value="%{recPaymentID}"/> "/>
					<input type="hidden" id="paymentType" name="paymentType" value="<s:property value="%{paymentType}"/> "/>
	               	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<p id="deleteDialogMessage"><s:text name="jsp.billpay_23"/> </p>
					<ffi:cinclude value1="${templateEdit}" value2="true" operator="notEquals">
						<%-- <% if(recurring) { %>
						<span style="color:red" align="center">
							<s:text name="jsp.billpay_117"/><br><br>
						</span>
						<% } %> --%>
					</ffi:cinclude>

					<div id="deleteDialogReturnMessage"></div>
					<div class="btn-row">
						<sj:a id="deleteDialogCancelBtn" button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" ><s:text name="jsp.default_82" /></sj:a>
	
						<%-- <ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals"> --%>
						<s:if test="%{isPortalPage=='true'}">
							<sj:a id="deleteSingleBillpayLink"
							formIds="innerDeleteBillpayForm"
							targets="loadingstatus"
							button="true"
							title="%{getText('jsp.default_162')}"
							onSuccessTopics="successDeleteBillpay"
							effectDuration="1500" ><s:text name="jsp.default_162"/></sj:a>
						</s:if>
						<%-- </ffi:cinclude>
						<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals"> --%>
						<s:else>
							<sj:a 	id="deleteDialogSubmitBtn"
									button="true"
									onclick="deletePayment()"
				                   	onErrorTopics="errorDeleteBillpay"
				                    onSuccessTopics="successDeleteBillpay"
								><s:text name="jsp.default_162"/></sj:a>
						</s:else>
						<%-- <s:if test='%{recPaymentID != "null"}'>
						<sj:a id="loadRecModelForDelete"
							                button="true"
							                onclick="deleteRecModelFromEdit()"
							            >Delete Model</sj:a>
						</s:if> --%>
												
					</div>
					<%-- </ffi:cinclude> --%>
				</s:form>
				<s:if test="%{isRecModel == 'true'}">
						<div class="marginTop10 columndata_error" align="center"><s:text name="jsp.billpay_117"/></div>
				</s:if>
				</div>

			</sj:dialog>
			</s:if>
		<%-- </ffi:cinclude> --%>
<script type="text/javascript">

function deletePayment(){
	$.ajax({
		url : "/cb/pages/jsp/billpay/cancelBillpayAction_execute.action",
		type : "post",
		data : {
		"ID":$('#ID').val().trim(),
		"recPaymentID":$('#recPaymentID').val().trim(),
		"paymentType":$('#paymentType').val().trim(),
		"CSRF_TOKEN" : ns.home.getSessionTokenVarForGlobalMessage,
		"isRecModel" : $('#isRecModelEdit').val()
		},
		success : function(data) {
			$('#innerDeleteBillpayDialog').parent().remove();
			$('#innerDeleteBillpayDialog').empty()
			ns.common.selectDashboardItem("goBackSummaryLink");
			$('#goBackSummaryLink').click();
			$('#resultmessage').html(data);
			$.publish('successDeleteBillpay');
		},
		error:function(){
			$.publish('errorDeleteBillpay');
		}
	});
}

$(document).ready(function(){
	$( ".toggleClick" ).click(function(){ 
		if($(this).next().css('display') == 'none'){
			$(this).next().slideDown();
			$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
			$(this).removeClass("expandArrow").addClass("collapseArrow");
		}else{
			$(this).next().slideUp();
			$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
			$(this).addClass("expandArrow").removeClass("collapseArrow");
		}
	});
	
});

function loadRecModel(){
	//debugger
	var fromPortal = false;
	<s:if test="%{isPortalPage=='true'}">
		fromPortal = true;
	</s:if>	
	if(!fromPortal) {
	var url = "/cb/pages/jsp/billpay/editBillpayAction_init.action?isRecModel=true&recPaymentID="+$('#recPaymentID').val();
	ns.billpay.editSingleBillpay(url);
	} else {
		var url = "/cb/pages/jsp/billpay/editBillpayAction_init.action?isRecModel=true&isPortalPage=true&recPaymentID="+$('#recPaymentID').val();
		ns.pendingTransactionsPortal.editSingleBillpay(url);
	}
}
</script>
<ffi:removeProperty name="addRecPayment"/>