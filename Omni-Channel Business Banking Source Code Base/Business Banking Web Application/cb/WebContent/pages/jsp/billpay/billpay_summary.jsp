<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<!-- Check AppType of user -->
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:setProperty name="UserType" value="Corporate"/>

</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:setProperty name="UserType" value="Consumer"/>
</ffi:cinclude>

	<%-- Set flag to show pending approval tab or not. It will be based on application type. --%>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>">
		<ffi:setProperty name="ShowPendingApprovalTab" value="true"/>		
	</ffi:cinclude>
			
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>">
			<%-- load pending approval bill payments in session if there are any for bank employee as a approver --%>
			<ffi:process name="GetPagedApprovalPayments"/>
	
			<ffi:cinclude value1="${PendingApprovalPayments.Size}" value2="0" operator="notEquals">
				<ffi:setProperty name="ShowPendingApprovalTab" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${PendingApprovalPayments.Size}" value2="0">
				<ffi:setProperty name="ShowPendingApprovalTab" value="false"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${PendingApprovalPayments.Size}" value2="">
				<ffi:setProperty name="ShowPendingApprovalTab" value="false"/>
			</ffi:cinclude>
		</ffi:cinclude>		
	</ffi:cinclude>
	
	<div id="billpayGridTabs" class="portlet gridPannelSupportCls" style="float: left; width: 100%;" role="section" aria-labelledby="summaryHeader">	
	<div class="portlet-header">
 	    	<h1 id="summaryHeader" class="portlet-title">Payment Summary</h1>
 	    	<div class="searchHeaderCls">
				<span class="searchPanelToggleAreaCls" onclick="$('#quicksearchcriteria').slideToggle();">
					<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
				</span>
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
							<s:text name="jsp.default_531"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
							<s:text name="jsp.default_530"/>
						</ffi:cinclude>
					</span>
				
						<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
				
					<div class="gridTabDropdownHolder">
				
						<s:iterator value="%{orderedBillpayTabs}" var="tabName">
							<s:if test="%{#tabName == 'approving'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('approving')" >
									<s:text name="jsp.default_531"/>									
								</span>
							</s:if>
							<s:elseif test="%{#tabName == 'pending'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('pending')">
									<s:text name="jsp.default_530"/>
								</span>
							</s:elseif>
							<s:elseif test="%{#tabName == 'completed'}">
								<span class="gridTabDropdownItem" id='<s:property value="%{#tabName}"/>' onclick="ns.common.showGridSummary('completed')">
									<s:text name="jsp.default_518"/>
								</span>
							</s:elseif>
						</s:iterator>
				
					</div>
				</div>
			</div>
	    </div>
	    <% 
			String pendingApprovalGridSummaryCssCls =""; 
			String pendingGridSummaryCssCls ="hidden";
		%>
		
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			<%
				pendingApprovalGridSummaryCssCls = "hidden";
				pendingGridSummaryCssCls = "";
			%>
		</ffi:cinclude>	    
	    
	    <div class="portlet-content">
	    		<div class="searchDashboardRenderCls">
					<s:include value="/pages/jsp/billpay/billpay_searchCriteria.jsp" />
				</div>
			<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="approvingSummaryGrid" gridId="pendingApprovalBillpayGridId" class="gridSummaryContainerCls  <%=pendingApprovalGridSummaryCssCls%>" >
					<s:action namespace="/pages/jsp/billpay" name="getBillpayPaymentGridTabAction_getPendingApprovalPayment" executeResult="true"/>
				</div>
				<div id="pendingSummaryGrid" gridId="pendingBillpayGridId" class="gridSummaryContainerCls  <%=pendingGridSummaryCssCls%>" >
					<s:action namespace="/pages/jsp/billpay" name="getBillpayPaymentGridTabAction_getPendingPayment" executeResult="true"/>
				</div>
				<div id="completedSummaryGrid" gridId="completedBillpayGridId" class="gridSummaryContainerCls hidden" >
					<s:action namespace="/pages/jsp/billpay" name="getBillpayPaymentGridTabAction_getCompletedPayment" executeResult="true"/>
				</div>
			</div>
	    </div>
	    <div id="billpaySummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('billpayGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	    
	</div>
	        <s:url id="calendarURL" value="/pages/jsp/billpay/loadCalender_showPayments.action" escapeAmp="false">
	        <%-- <s:url id="calendarURL" value="/pages/jsp/billpay/addBillpayAction_init.action"> --%>
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="moduleName" value="%{'BillpayCalendar'}"></s:param>
			</s:url>
	        <sj:a
				id="openCalendar"
				href="%{calendarURL}"
				targets="summary"
				onClickTopics="closeBillpaymentTabs" 
				onSuccessTopics="calendarLoadedSuccess"
				cssClass="titleBarBtn"
				
				>
				<s:text name="jsp.billpay_calender"/><span class="ui-icon ui-icon-calendar floatleft"></span>
			</sj:a>
		<%-- <input type="hidden" id="getTransID" value="<ffi:getProperty name='billpaytabs' property='TransactionID'/>" /> --%>


		<s:hidden name="billpaySummaryType" value="PendingBillpay" id="billpaySummaryTypeID"/>
			<%-- Set following hidden field to identify the module to which tab belong to, this is required in case of save/load tab settings --%>
		<input type="hidden" id="getTransID" value="<s:property value="%{moduleId}"/>" />
<script>

	/* $('#billpayGridTabs').portlet({
		helpCallback: function(){

			var helpFile = "";
			$('#billpayGridTabs').find('.ui-tabs-panel').each(function(){
			    if($(this).attr('aria-hidden') == "false"){
			    	helpFile = $(this).find('.moduleHelpClass').html();
			    }
			});
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
	});
	
	$("#billpayGridTabs").hover(
			  function () {
			    $("#billpayGridTabs .headerctls").show();
			  }, 
			  function () {
			    $("#billpayGridTabs .headerctls").hide();
			  }
			); */
	ns.common.initializePortlet("billpayGridTabs", false, true, function( ) {
	    $("#openCalendar").click();
	});
			
		
</script>
