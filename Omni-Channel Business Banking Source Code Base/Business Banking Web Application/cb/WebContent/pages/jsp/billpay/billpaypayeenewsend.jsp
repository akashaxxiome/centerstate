<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="payments_billpaypayeesend" className="moduleHelpClass"/>


<div class="leftPaneWrapper" role="form" aria-labeledby="summary">
<div class="leftPaneInnerWrapper label130">
	<div class="header"><h2 id="summary"><s:text name="jsp.billpay.payee_summary" /></h2></div>
	<div class="leftPaneInnerBox summaryBlock"  style="padding: 15px 5px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="confirmPayeeNameLabel"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue"  style="width:50%" id="confirmPayeeNameValue"><s:property value="%{name}"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="confirmPayeeAccountLabel"><s:text name="jsp.default_19"/>:</span>
				<span class="inlineSection floatleft labelValue" style="width:50%" id="confirmPayeeAccountValue"><s:property value="%{userAccountNumber}"/></span>
			</div>
	</div>
</div>
</div>
<div class="confirmPageDetails label130">
   		<div  class="blockHead"><s:text name="jsp.billpay.payee.info"/></div>
   		<div class="blockRow completed">
			<span id="billpayResultMessage"><span class="sapUiIconCls icon-accept"></span> <s:text name="jsp.billpay_124"/></span>
		</div>
   		<div class="blockContent marginTop10">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmPayeeNickNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_294"/>:</span>
					<span id="confirmPayeeNickNameValue" class="columndata" ><s:property value="%{nickName}"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmPayeeContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
					<span id="confirmPayeeContactValue" class="columndata" ><s:property value="%{contactName}"/></span>
				</div>
			</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmPayeePhoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_37"/>:</span>
				<span id="confirmPayeePhoneValue" class="columndata" ><s:property value="%{phone}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmPayeeAddress1Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
				<span id="confirmPayeeAddress1Value" class="columndata" ><s:property value="%{street}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmPayeeAddress2Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
				<span id="confirmPayeeAddress2Value" class="columndata" ><s:property value="%{street2}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmPayeeAddress3Label"  class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
				<span id="confirmPayeeAddress3Value" class="columndata" ><s:property value="%{street3}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmPayeeCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
				<span id="confirmPayeeCityValue" class="columndata" ><s:property value="%{city}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmPayeeStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
				<span id="confirmPayeeStateValue" class="columndata" >
					<s:property value="%{stateDisplayName}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmPayeeZipLabel" class="sectionsubhead sectionLabel"><span><s:text name="jsp.default_473"/></span>:</span>
				<span id="confirmPayeeZipValue" class="columndata" >
					<s:property value="%{zipCode}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="confirmPayeeCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
				<span id="confirmPayeeCountryValue" class="columndata" ><s:property value="%{countryDisplayName}"/></span>
			</div>
		</div>
   		</div>
   		<div  class="blockHead"><s:text name="jsp.billpay.payee.bank.info"/></div>
   		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmPayeeRountingNoLabel" class="sectionsubhead sectionLabel"><ffi:getProperty name="TempBankIdentifierDisplayText"/>
													<s:property value="%{bankIdentifierDisplayText}"/>:</span>
					<span id="confirmPayeeRountingNoValue" class="columndata">
						<s:property value="%{payeeRoute.bankIdentifier}"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="confirmPayeeBankAccountNoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_26"/>:</span>
					<span id="confirmPayeeBankAccountNoValue" class="columndata"><ffi:getProperty name="AddPayee" property="PayeeRoute.AccountID" /><s:property value="%{payeeRoute.accountId}"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmPayeeBankAccountTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_27"/>:</span>
					<span id="confirmPayeeBankAccountTypeValue" class="columndata"><s:property value="%{accountTypeSelectList[payeeRoute.acctType]}"/></span>
				</div>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
					<div class="inlineBlock">
						<span id="confirmPayeeBankAccountCurrencyLabel" class="sectionsubhead sectionLabel">
								<s:text name="jsp.billpay_25"/>:
						</span>
						<span id="confirmPayeeBankAccountCurrencyValue" class="columndata">
							<s:property value="%{payeeRoute.currencyCode}"/>
						</span>
						</div>
				</ffi:cinclude>
			</div>
   		</div>

<div class="btn-row">
	<sj:a id="doneBillpayPayeeConfirm" 
            button="true" 
            summaryDivId='dbPayeeSummary' 
            buttonType='done'
			onClickTopics="showSummary,cancelPayeeLoadForm"
    	><s:text name="jsp.default_175"/></sj:a>
</div></div>
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_15"/><br>
												</span></td>
										</tr>
										<tr>
											<td class="sectionsubhead"></td>
											<td></td>
											<td></td>
											<td><br>
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td id="confirmPayeeNameLabel" class="sectionsubhead">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="confirmPayeeNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Name" />
											<s:property value="%{name}"/>
											</td>
											<td id="confirmPayeeAddress1Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td id="confirmPayeeAddress1Value" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Street" />
											<s:property value="%{street}"/>
											</td>
										</tr>
										<tr>
											<td id="confirmPayeeNickNameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_294"/></span></div>
											</td>
											<td id="confirmPayeeNickNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="NickName" />
											<s:property value="%{nickName}"/>
											</td>
											<td id="confirmPayeeAddress2Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="confirmPayeeAddress2Value" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Street2" />
												<s:property value="%{street2}"/>
											</td>
										</tr>
										<tr>
											<td id="confirmPayeeContactLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="confirmPayeeContactValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="ContactName" />
											<s:property value="%{contactName}"/>
											</td>
											<td id="confirmPayeeAddress3Label"  class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="confirmPayeeAddress3Value" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Street3" />
										<s:property value="%{street3}"/>
											</td>
										</tr>
										<tr>
											<td id="confirmPayeePhoneLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_37"/></span></div>
											</td>
											<td id="confirmPayeePhoneValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Phone" />
												<s:property value="%{phone}"/></td>
											<td id="confirmPayeeCityLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td id="confirmPayeeCityValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="City" />
											<s:property value="%{city}"/>
											</td>
										</tr>
										<tr>
											<td id="confirmPayeeAccountLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_19"/></span></div>
											</td>
											<td id="confirmPayeeAccountValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="UserAccountNumber" />
											<s:property value="%{userAccountNumber}"/>
											</td>
											<td id="confirmPayeeStateLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="confirmPayeeStateValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="AddPayee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{stateDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="confirmPayeeRountingNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">
													<ffi:getProperty name="TempBankIdentifierDisplayText"/>
													<s:property value="%{bankIdentifierDisplayText}"/>
													</span></div>
											</td>
											<td id="confirmPayeeRountingNoValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="PayeeRoute.BankIdentifier" />
											<s:property value="%{payeeRoute.bankIdentifier}"/>
											</td>
											<td id="confirmPayeeZipLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><span><s:text name="jsp.default_473"/></span></span></div>
											</td>
											<td id="confirmPayeeZipValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="ZipCode" />
											<s:property value="%{zipCode}"/>
											</td>
										</tr>
										<tr>
											<td id="confirmPayeeBankAccountNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_26"/></span></div>
											</td>
											<td id="confirmPayeeBankAccountNoValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="PayeeRoute.AccountID" />
											<s:property value="%{payeeRoute.accountId}"/>
											</td>
											<td id="confirmPayeeCountryLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_115"/></span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${AddPayee.Country}" />
											<td id="confirmPayeeCountryValue" class="columndata" colspan="2">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{countryDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="confirmPayeeBankAccountTypeLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_27"/></span></div>
											</td>
											<ffi:object id="GetTypeName" name="com.ffusion.tasks.util.Resource" />
											<ffi:setProperty name="GetTypeName" property="ResourceFilename" value="com.ffusion.beansresources.accounts.resources" />
											<ffi:setProperty name="GetTypeName" property="ResourceID" value="${AddPayee.PayeeRoute.AcctType}" />
											<ffi:process name="GetTypeName" />
											<td id="confirmPayeeBankAccountTypeValue" class="columndata" colspan="2">
											<ffi:getProperty name="GetTypeName" property="Resource" />
											<s:property value="%{accountTypeSelectList[payeeRoute.acctType]}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										 <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
										<tr>
											<td id="confirmPayeeBankAccountCurrencyLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_25"/></span></div>
											</td>
											<td id="confirmPayeeBankAccountCurrencyValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="PayeeRoute.CurrencyCode" />
											<s:property value="%{payeeRoute.currencyCode}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										</ffi:cinclude>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right"></div>
											</td>
											<td colspan="4"><div align="center">
													<br>
													<sj:a id="doneBillpayPayeeConfirm" 
										                button="true" 
														onClickTopics="cancelBillpayForm"
										        	><s:text name="jsp.default_175"/></sj:a>
										        	
													</div></td>
											<td></td>
										</tr>
									</table>
						</td>
						<td></td>
					</tr>
				</table> --%>
