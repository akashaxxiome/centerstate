<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<head>
	<script>
		// Need to show throbber as DOM parsing taking time for large records.
		ns.common.showLoading();
	</script>
</head>

<ffi:help id="payments_billpaynewmulti" className="moduleHelpClass"/>
<span class="shortcutPathClass" style="display:none;">pmtTran_billpay,addMultipleBillpayLink</span>
<script language="javascript">


$(document).ready(function(){
	// this checks from where the flow is coming
	// if value is present means form is loading data from load tempalte
	if($('#hiddenAccountDisplayText').val() !=null 
		&& $.trim($('#hiddenAccountDisplayText').val()) != "" ){
		var displayText=$('#hiddenAccountDisplayText').val();
		var accountIdHidden = $('#accountIdHidden').val();

		// this checks if widget is already created i.e. template is loaded from multiple payment screen
		// hence we need to detroy it & create a new one
		try{
			if($('#AccountID').lookupbox('widget')){
				$('#AccountID').lookupbox('destroy');
			}	
		}catch (err){
			// do nothing
		}
		
		$('#AccountID').append('<option value='+ accountIdHidden +' selected="selected">'+displayText+'</option>');
		prepareAccountDropdown();
	}
	// this means form is just opened just create a lookup bx
	else{
		prepareAccountDropdown();
	}
	
	function prepareAccountDropdown(){
		$("#AccountID").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"module":"billpay",
			"size":"35",
			"source":"/cb/pages/jsp/billpay/BillPayAccountsLookupBoxAction.action"
		});
	}
})

function clearRow(index)
{
    $('#amount'+index).val('');
    $('#memo'+index).val('');
}

window.onload = function() {
    //calculateTotal();
}

</script>
<input type="hidden" id="hiddenAccountDisplayText" value="<s:property value="%{multiplePayment.account.accountDisplayText}"/>"/>
<s:form id="BillpayMultipleNew" namespace="/pages/jsp/billpay" validate="false" action="addBillpayBatchAction_verify" method="post" name="BillpayMultipleNew" theme="simple">
<input name="multiplePayment.acountId" id="accountIdHidden" type="hidden" value='<s:property value="%{multiplePayment.acountId}"/>'/>
<div class="hidden">
<span id="acountIdError"></span>
</div>
<div><span id="amountError"></span></div>
<div><span id="paymentsError"></span></div>
<div class="paneWrapper">	

<ffi:cinclude value1="0" value2="${PaymentAccounts.Size}" operator="equals">
<%-- <!-- IF BillPayAccounts COLLECTION IS EMPTY ie) not entitled to any accounts -->--%>
<div class="txt_normal" align="center">(<s:text name="jsp.billpay_108"/>)</div>
<%-- <input type="button" name="BACK" value="<s:text name="jsp.default_57"/>" class="form_buttons" onClick="window.location='<ffi:urlEncrypt url="${SecurePath}payments/billpay.jsp"/>';"> --%>
<div>
<sj:a id="cancelMultiFormButton" summaryDivId="summary" buttonType="cancel"
	    button="true"
		onClickTopics="showSummary,cancelBillpayForm"
	><s:text name="jsp.default_82"/></sj:a></div>
</ffi:cinclude>

<%-- ================ MAIN CONTENT START ================ --%>

 
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<span style="display:none;" id="PageHeading"><s:text name="jsp.billpay_add_multiple_payment"/></span>
<div class="paneInnerWrapper" role="form" aria-labelledby="paymentDetails">
<div class="header"><h2 id="paymentDetails"><s:text name="jsp.billpay_77"/></h2></div>
<div class="paneContentWrapper">
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="tableData formTablePadding5">
					<td id="addMultipleBillPay_businessDayMessage" width="100%" class="instructions"  colspan="4">
						<s:text name="jsp.billpay_mulitple_template_business_days_message"/>
    &nbsp;
                    </td>
				</tr>
	<tr><td colspan="4"><ul id="formerrors" style="margin:0"></ul></td></tr>
<s:iterator value="%{multiplePayment.payments}" status="listStatus" var="payment" >
	<tr>
		<td colspan="3" id="addMultipleBillPay_payee<s:property value="%{#listStatus.index}"/>" class="">
			
			<h3 class="transactionHeading"><s:property value="#payment.payee.name"/> (<s:property value="#payment.payee.nickName"/> - 
			<s:property value="#payment.payee.userAccountNumber"/>)
			<span class="instructions" style="">(<s:text name="jsp.billpay_59"/>:

			<s:if test="%{lastPaymentToPayeeMap[#payment.payee.ID].size >0}">
				<s:iterator value="%{lastPaymentToPayeeMap[#payment.payee.ID]}" var="lastPaymentsList">
					<s:property value="#lastPaymentsList.Amount"/> - <s:property value="#lastPaymentsList.payDate"/>
				</s:iterator>
			</s:if>
			<s:else>
				<s:text name="jsp.billpay_none"/>
			</s:else>)</span></h3>
		</td>
	</tr>
	<tr>
		<td width="25%" id="addMultipleBillPay_amountLabel" class="sectionsubhead nowrap" width="15%"><label for="amount<s:property value="%{#listStatus.index}"/>""><s:text name="jsp.default_43"/></label> <span class="required" title="required">*</span></td>
		<td width="40%" id="addMultipleBillPay_sendDateLabel" class="sectionsubhead" width="15%" nowrap>
           <label for="payDate<s:property value="%{#listStatus.index}"/>"> <s:text name="jsp.multiple.billpay.date"/><span class="required">*</span>
            <s:set value="#payment.payee.daysToPay" var="day"></s:set>
			(<span class=""><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/billpaynewmulti.jsp-4" parm0="${day}"/></label></span>)
        </td>
        <td width="35%" id="addMultipleBillPay_memoLabel" class="sectionsubhead" width="" nowrap colspan="2">
            <ffi:cinclude value1="${RegisterEnabled}" value2="true" operator="equals">
                <s:text name="jsp.billpay_61"/>
            </ffi:cinclude>
            <ffi:cinclude value1="${RegisterEnabled}" value2="true" operator="notEquals">
                &nbsp;<label for="memo<s:property value="%{#listStatus.index}"/>"><s:text name="jsp.default_279"/></label>
            </ffi:cinclude>
        </td>
       </tr>
		<tr>
			<td class="columndata" nowrap align="left">
			<input type="text" class="amountCls ui-widget-content ui-corner-all" aria-required="true" value="<s:property value="#payment.AmountValue.AmountValue"/>"
			name="multiplePayment.payments[<s:property value="%{#listStatus.index}"/>].amount" size="10" maxlength="16" 
			id="amount<s:property value="%{#listStatus.index}"/>" 
			onChange=""/>
			<s:property value="#payment.payee.payeeRoute.currencyCode"/>
			<s:hidden value="%{#payment.payee.payeeRoute.currencyCode}" 
			name="multiplePayment.payments[%{#listStatus.index}].amtCurrency"></s:hidden>
			</td>
            <td nowrap align="left">
				<input id="payDate<s:property value="%{#listStatus.index}"/>" aria-required="true" value="<s:property value="%{#payment.payDate}"/>" class="ui-widget-content ui-corner-all"
				name="multiplePayment.payments[<s:property value="%{#listStatus.index}"/>].payDate" onclick="createDatePicker(<s:property value="%{#listStatus.index}"/>)"/>
				<img id="payDateImage<s:property value="%{#listStatus.index}"/>" class="ui-datepicker-trigger" onclick="createDatePicker(<s:property value="%{#listStatus.index}"/>)" src="/cb/web/grafx/icons/calendar.png"/>
			</td>
			<td align="left">
               <input type="text" class="ui-widget-content ui-corner-all" value="<s:property value="#payment.memo"/>"
               name="multiplePayment.payments[<s:property value="%{#listStatus.index}"/>].memo"
                maxlength="64" size="20" id="memo<s:property value="%{#listStatus.index}"/>"/>
				<%-- <span id="Memo<ffi:getProperty name='Payment1' property='ID'/>Error"></span> --%>
            </td>
		</tr>
		<tr>
			<td>
			<span id="amount<s:property value="%{#listStatus.index}"/>Error"></span>
			</td>
			<td>
				 <span id="payDate<s:property value="%{#listStatus.index}"/>Error"></span>
			</td>
			<td> <span id="memo<s:property value="%{#listStatus.index}"/>Error"></span></td>
		</tr>
 </s:iterator>
 </table>
 <div align="center" class="marginTop10"><span class="required">* <s:text name="jsp.default_240"/></span></div>
 <div class="btn-row">
 
 <sj:a id="cancelMultiFormButton"
		    button="true"
		    summaryDivId="summary" buttonType="cancel"
			onClickTopics="showSummary,cancelBillpayForm"
			cssClass="marginRight20"
		><s:text name="jsp.default_82"/></sj:a>
		<sj:a
				id="verifyMutiBillpaySubmit"
				formIds="BillpayMultipleNew"
			    targets="verifyDiv"
		        button="true"
		        validate="true"
		        validateFunction="customValidation"
		        onAfterValidationTopics="verifyValidation"
		        onBeforeTopics="beforeVerifyBillpayForm"
		        onCompleteTopics="verifyBillpayFormComplete"
				onErrorTopics="errorVerifyBillpayForm"
			    onSuccessTopics="successVerifyBillpayForm"
			 ><s:text name="jsp.default_291"/></sj:a>
 </div></div>
			</div>
		</div>
</s:form>


<script>

$(document).ready(function(){

	//hide throbber once page loaded.
	ns.common.showLoading();
	
	var totalAmount = '';
	var enteredAmount = '';
		$('.amountCls').on('blur',function(){
			totalAmount = 0;
			enteredAmount = 0;
			$('.amountCls').each(function(){
	
				enteredAmount = parseInt($(this).val());
				if(enteredAmount > 0){
					totalAmount=totalAmount+enteredAmount;
				}		
			});
			$('#multiplePaymentAmount').val('');
			$('#multiplePaymentAmount').val(totalAmount);
		});
		$('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>');
	});
	
function createDatePicker(id){
		if(!$('#payDate'+id).data('datepicker')) {
			$('#payDate'+id).datepicker({
				showOn: "both",
	            buttonImageOnly: true
			});
			var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=3&calDisplay=select&calForm=FormName&calTarget="/>';
			ns.common.enableAjaxDatepicker("payDate"+id, tmpUrl);
			$('#payDateImage'+id).remove();
			$('#payDate'+id).focus();
		
		}
}

</script>