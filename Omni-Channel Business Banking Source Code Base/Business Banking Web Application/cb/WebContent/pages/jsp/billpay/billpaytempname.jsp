<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_billpay_saveastemplate" className="moduleHelpClass" />
<script type="text/javascript">
<!--	  
	//This function is used to check event of enter key.
	$("#NewSingleBillpayTemplate").keypress(function(e) {  
		if (e.which == 13) {  
			$("#saveSingleBillpayTemplateSubmit").click();
			return false;
		}   
	});	
//-->
</script>

<%-- <%
String strutsActionName = (String)request.getAttribute("strutsActionName");
if("AddBillpayAction".equals(strutsActionName)) {
	session.setAttribute("FFIAddPayment", session.getAttribute("AddPayment"));
} else {
	session.setAttribute("FFIEditPayment", session.getAttribute("EditPayment"));
}

//Get Portal Page flag from session
request.setAttribute("fromPortalPage", request.getParameter("fromPortalPage"));
%> --%>
<div align="center">
	<s:form id="NewSingleBillpayTemplate" namespace="/pages/jsp/billpay" validate="true" action="addBillPayTemplateAction_saveAsTemplate" method="post" name="NewSingleBillpayTemplate" theme="simple">
            	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<%-- <tr>
			<td class="sectionsubhead"><s:text name="jsp.default_290"/></td>
		</tr> --%>
		<tr>
			<td>
				<input name="templateName" value="" size="22" maxlength="32"  class="ui-widget-content ui-corner-all" placeholder="<s:text name='jsp.template.name.label'/>" style="width:60%" />
				<s:hidden name="addRecPayment.iD" value="" />
					<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
					<span id="billpayTemplateResultMessage" class="hidden"><s:text name="jsp.billpay_120"/></span>
			    <sj:submit 
					id="saveSingleBillpayTemplateSubmit"
					targets="resultmessage"
                             button="true" 
                             validate="true" 
							validateFunction="customValidation"
                             onBeforeTopics="beforeSaveSingleBillpayTemplatePortal"
                             onSuccessTopics="saveBillpayTemplateSuccessPortal"
                             onErrorTopics="errorSaveSingleBillpayTemplatePortal"
                             value="%{getText('jsp.default_367')}"/>
                </ffi:cinclude>
      		 <ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals">
                	<sj:submit 
					id="saveSingleBillpayTemplateSubmit"
					targets="resultmessage"
	                button="true" 
	                validate="true" 
                    validateFunction="customValidation"
                    onBeforeTopics="beforeSaveSingleBillpayTemplate"
                    onSuccessTopics="saveBillpayTemplateSuccess"
                    onErrorTopics="errorSaveSingleBillpayTemplate"
                    onCompleteTopics="saveBillpayTemplateComplete"
                    value="%{getText('jsp.default_366')}" cssClass="floatRight formSubmitBtn"/>	
                 </ffi:cinclude>
			</td>
		</tr>
		<tr><td><span id="templateNameError"></span>
		<span id="Duplicate.TemplateNameError"></span></td></tr>
		<%-- <tr>
			<td align="center">
			<s:if test="#request.fromPortalPage == 'true'">
			    <sj:submit 
					id="saveSingleBillpayTemplateSubmit"
					targets="resultmessage"
                             button="true" 
                             validate="true" 
                    validateFunction="customValidation"
                             onBeforeTopics="beforeSaveSingleBillpayTemplatePortal"
                             onSuccessTopics="saveBillpayTemplateSuccessPortal"
                             onErrorTopics="errorSaveSingleBillpayTemplatePortal"
                             value="%{getText('jsp.default_367')}"/>
                </s:if>
                <s:else>
                	<sj:submit 
					id="saveSingleBillpayTemplateSubmit"
					targets="resultmessage"
                             button="true" 
                             validate="true" 
                    validateFunction="customValidation"
                             onBeforeTopics="beforeSaveSingleBillpayTemplate"
                             onSuccessTopics="saveBillpayTemplateSuccess"
                             onErrorTopics="errorSaveSingleBillpayTemplate"
                             onCompleteTopics="saveBillpayTemplateComplete"
                             value="%{getText('jsp.default_366')}"/>	
                </s:else>       
			</td>
		</tr> --%>
	</table>
</s:form>
</div>

