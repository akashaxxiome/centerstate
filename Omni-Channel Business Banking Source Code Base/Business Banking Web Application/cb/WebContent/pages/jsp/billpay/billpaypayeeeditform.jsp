<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="payments_billpayedit" className="moduleHelpClass"/>	


<script language="javascript">
$.subscribe('clearCountry', function(event,data){
	var countryName = $('#selectPayeeCountryID').val();
	$('#countryPassed').val(countryName);
});

$(function(){
	$("#selectPayeeCountryID").combobox({'size': '28'});
	$("#addPayeeAccountType").selectmenu({width: 150});
	$("#addPayeeCurrencyCode").selectmenu({width: 150});
	/* if there is any state in the payee then only the combo box needs to be displayed */ 
	if(!$("#selectPayeeStateID").hasClass('ffiHidden')){
		$("#selectPayeeStateID").combobox({'size': '28',searchFromFirst:true});
		/* hidding combo box */
		$("#selectPayeeStateID").combobox({width: 200,searchFromFirst:true});
		/* hidding state/province label */
		$("#stateNameId").removeClass('ffiHidden');
	}else {
		$("#stateSelectId").addClass('ffiHidden');
	}
	$("#addPayeeAccountType").selectmenu({width: 120});
	$("#addPayeeCurrencyCode").selectmenu({width: 160});
});



function showState(id) {
	var countryName = $('#selectPayeeCountryID').val();
	$('#countryPassed').val(countryName);
 	$.ajax({
 		   url: "/cb/pages/jsp/billpay/editBillpayPayeeAction_loadState.action",
 		   type: "POST",
 		   data:  $("#BillpayPayeeEdit").serialize(),
 		   success: function(data){
 		   
 			  if(data.length>0){
 		   		
 				$('#selectPayeeStateID').html(' ');
 				$('#selectPayeeStateID').append('<option value="" selected="selected"></option>');
 			   	for(var i=0;i<data.length;i++){
 					 $('#selectPayeeStateID').append('<option value='+data[i].stateKey+ '>'+data[i].name+ '</option>');		   				   	
 			   	}			
 			   		/* if the combo box was hidden and brought to visible on country change load the combo box */
 			   		$("#selectPayeeStateID").combobox({width: 200,searchFromFirst:true});
 			   		$("#stateNameId").removeClass('ffiHidden');
 			   		$("#stateSelectId").removeClass('ffiHidden');
 		   	}
 		   	else{
 		   		$("#stateNameId").addClass('ffiHidden');
 		   		$("#stateSelectId").addClass('ffiHidden');
 		   	}
 	 	  }
 	}); 
}

</script>
<s:form id="BillpayPayeeEdit" namespace="/pages/jsp/billpay" validate="false" action="editBillpayPayeeAction_verify" method="post" name="BillpayPayeeEdit" theme="simple">
<h1 class="portlet-title" style="display:none;" id="PageHeading"><s:text name="jsp.billpay_52"/></h1>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden name="hostID" value="%{addPayee.hostID}"></s:hidden>
<s:if test='%{addPayee.isGlobal}'>
<div class="leftPaneWrapper" role="form" aria-labeledby="payeeSummary">
<div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="payeeSummary"><s:text name="jsp.billpay.payee_summary" /></h2></div>
	<div class="leftPaneInnerBox summaryBlock"  style="padding: 15px 5px;">
		<div style="width:100%" class="floatleft">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id=""><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue" id=""><s:property value="%{addPayee.name}"/></span>
			</div>
	</div>
</div>
</div>
<div class="rightPaneWrapper w71 label130">
<div class="paneWrapper" role="form" aria-labeledby="editNickName">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding10">
		<tr>
			<td colspan="2" class="header" style="font-size:14px;"><h2 id="editNickName">Edit Nickname / Account Details</h2></td>
		</tr>
		<tr>
			<td width="50%" id="editPayeeNickNameLabel" class="columndata ltrow2_color"><label for="editPayeeNickName"><s:text name="jsp.default_294"/></label><span class="required">*</span></td>
			<td id="editPayeeAccountLabel" class="columndata ltrow2_color"><label for="editPayeeAccount"><s:text name="jsp.default_19"/></label>
				<s:if test='%{addPayee.payeeRoute.custAcctRequired}'>
					<span class="required">*</span>
				</s:if>
				<s:else>
					<!-- no need of required star -->
				</s:else>
			</td>
		</tr>
		<tr>
			<td><input id="editPayeeNickName" aria-required="true" class="ui-widget-content ui-corner-all" type="text" name="nickName" size="30" value="<s:property value="%{addPayee.nickName}"/>" tabindex="1"></td>
			<td><input id="editPayeeAccount" class="ui-widget-content ui-corner-all" type="text" name="userAccountNumber" size="30" maxlength="30" value="<s:property value="%{addPayee.userAccountNumber}"/>" tabindex="4"></td>
		</tr>
		<tr>
			<td><span id="nickNameError"></span></td>
			<td><span id="Payee.userAccountNumberError"></span></td>
		</tr>
	</table>
</div>
<div class="marginTop10"></div>
<div class="paneWrapper" role="form" aria-labeledby="payeeInfo">
  	<div class="paneInnerWrapper">
   		<div class="header"><h2 id="payeeInfo"><s:text name="jsp.billpay.payee.info"/></h2></div>
   		<div class="paneContentWrapper">
   		<div class="blockContent">
   			<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editPayeeContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
				<span><s:property value="%{addPayee.contactName}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editPayeePhoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_37"/>:</span>
				<span><s:property value="%{addPayee.phone}"/></span>
			</div>
		</div>
   		
   		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editPayeeAddress1Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
				<span><s:property value="%{addPayee.street}"/></span>
			</div>
			<div class="inlineBlock">	
				<span id="editPayeeAddress2Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
				<span><s:property value="%{addPayee.street2}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editPayeeAddress3Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
				<span><s:property value="%{addPayee.street3}"/></span>
			
			</div>
			<div class="inlineBlock">
				<span id="editPayeeCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
				<span><s:property value="%{addPayee.city}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editPayeeStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
				<span id="stateSelectId">
					<s:property value="%{addPayee.stateDisplayName}"/>
					<s:hidden value="%{addPayee.state}" name="state"></s:hidden>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="editPayeeZipLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
				<span>
					<s:property value="%{addPayee.zipCode}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
				<span id="editPayeeCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
				<span>
					<s:property value="%{addPayee.countryDisplayName}"/>
					<s:hidden value="%{addPayee.country}" name="country"></s:hidden>
				</span>
		</div>
		</div>
   	</div>
   </div>
 </div>
 <div class="marginTop10"></div>
 <div class="paneWrapper" role="form" aria-labeledby="bankInfo">
  	<div class="paneInnerWrapper">
   		<div class="header"><h2 id="bankInfo"><s:text name="jsp.billpay.payee.bank.info"/></h2></div>
   		<div class="paneContentWrapper">
   		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editPayeeRoutingNoLabel" class="sectionsubhead sectionLabel"><s:property value="%{bankIdentifierDisplayText}"/>:
						<s:hidden value="%{bankIdentifierDisplayText}" name="bankIdentifierDisplayText"></s:hidden>
						<s:hidden id="achRoutingNumber" name="financialInstitution.achRoutingNumber"></s:hidden>
					</span>
					<span>
						<s:property value="%{addPayee.payeeRoute.bankIdentifier}"/>
						<input id="bankIdentifier" class="txtbox" type="hidden" name="BankIdentifier" size="25" value="%{PayeeRoute.BankIdentifier}"></span>
				</div>
				<div class="inlineBlock">
					<span id="editPayeeBankAccountNoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_26"/>:</span>
					<span>
						<s:property value="%{addPayee.payeeRoute.accountID}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editPayeeBankAccountTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_27"/>:</span>	
					<span>
							<s:property value="%{addPayee.payeeRoute.acctType}"/>
				  	</span>
				</div>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
					<div class="inlineBlock">
						<span id="editPayeeBankAccountCurrencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_25"/>:</span>
						<span>
							<s:property value="%{addPayee.payeeRoute.currencyCode}"/>
						</span>
					</div>
				</ffi:cinclude>		
			</div>
 		</div>
 		<div class="btn-row">
			<sj:a id="cancelEditBillPayPayee" 
			            button="true" 
			onClickTopics="cancelBillpayForm,cancelPayeeLoadForm"
			    	><s:text name="jsp.default_82"/></sj:a>
			
			<sj:a 
			id="verifyBillpayPayeeEditSubmit"
			formIds="BillpayPayeeEdit"
			                        targets="verifyDiv" 
			                        button="true" 
			                        validate="true" 
			                        validateFunction="customValidation"
			                        onClickTopics="clearCountry"
			                        onBeforeTopics="beforeVerifyBillpayForm"
			                        onCompleteTopics="verifyBillpayFormComplete"
			onErrorTopics="errorVerifyBillpayForm"
			                        onSuccessTopics="successVerifyBillpayForm"
			                  ><s:text name="jsp.default_291"/></sj:a>
		</div>
   </div>
 </div>
</div>   		
</s:if>
<s:else>
<div class="leftPane">
	<div class="header"><s:text name="jsp.billpay.payee_summary" /></div>
	<div class="leftPaneInnerBox summaryBlock"  style="padding: 15px 5px;">
		<div style="width:100%" class="floatleft">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="editPayeeNameLabel"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue" id="editPayeeNameValue"><s:property value="%{addPayee.name}"/></span>
			</div>
	</div>
</div>
<div class="rightPaneWrapper label130" id="payeeInfoContent">
<div class="paneWrapper">
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding10">
<tr><td  width="100%" style="cursor:auto; font-size:14px;" class="header" colspan="2"><s:text name="jsp.billpay.payee.info"/></td></tr>
	<tr>
		<%-- <td width="25%" id="editPayeeNameLabel" class="sectionsubhead"><s:text name="jsp.default_314"/></td> --%>
		<td width="50%" id="editPayeeNickNameLabel" class="columndata ltrow2_color"><s:text name="jsp.default_294"/><span class="required">*</span></td>
		<td width="50%" id="editPayeeContactLabel" class="columndata ltrow2_color"><s:text name="jsp.billpay_36"/></td>
	</tr>
	<tr>
		<%-- <td id="editPayeeNameValue" class="columndata"><s:property value="%{addPayee.name}"/></td> --%>
		<td><input id="editPayeeNickName" class="ui-widget-content ui-corner-all" type="text" name="nickName" size="30" maxlength="30" value="<s:property value="%{addPayee.nickName}"/>" tabindex="1"></td>	
		<td>
			<input id="editPayeeContact" class="ui-widget-content ui-corner-all" type="text" name="contactName" size="30" maxlength="30" value="<s:property value="%{addPayee.contactName}"/>" tabindex="2">
		</td>									
	</tr>
	<tr>
		<td><span id="nickNameError"></span></td>
		<td></td>
		
	</tr>
	<tr>
		<td width="25%" id="editPayeePhoneLabel" class="columndata ltrow2_color"><s:text name="jsp.billpay_37"/><span class="required">*</span></td>
		<td id="editPayeeAddress1Label" class=""><s:text name="jsp.default_36"/><span class="required">*</span></td>
	</tr>
	<tr>
		<td>
		 	<input id="editPayeePhone" class="ui-widget-content ui-corner-all" type="text" name="phone" size="30" maxlength="18" value="<s:property value="%{addPayee.phone}"/>" tabindex="3">
		</td>
		<td>
			<input id="editPayeeStreetOne" class="ui-widget-content ui-corner-all" type="text" name="street" size="30" maxlength="32" value="<s:property value="%{addPayee.street}"/>" tabindex="10">
		</td>
	</tr>
	<tr>
		<td> <span id="phoneError"></span></td>
		<td><span id="streetError"></span></td>
	</tr>
	<tr>
		<td id="editPayeeAddress2Label" class=""><s:text name="jsp.default_37"/></td>
		<td id="editPayeeAddress3Label" class=""><s:text name="jsp.default_38"/></td>
	</tr>
	<tr>
		<td>
			 <input id="editPayeeStreetTwo" class="ui-widget-content ui-corner-all" type="text" name="street2" size="30" maxlength="32" value="<s:property value="%{addPayee.street2}"/>" tabindex="11">
		</td>
		<td>
			<input id="editPayeeStreetThree" class="ui-widget-content ui-corner-all" type="text" name="street3" size="30" maxlength="32" value="<s:property value="%{addPayee.street3}"/>" tabindex="12">
	 	</td>
	</tr>
	<tr>
		
		<td><span id="street2Error"></span></td>
		<td></td>
	</tr>	
	<tr>
		<td id="editPayeeCityLabel" class=""><s:text name="jsp.default_101"/><span class="required">*</span></td>
		<td id="editPayeeAccountLabel" class="columndata ltrow2_color"><s:text name="jsp.default_19"/><span class="required">*</span></td>
	</tr>	
	<tr>
		<td>
			<input id="editPayeeCity" class="ui-widget-content ui-corner-all" type="text" name="city" size="30" maxlength="32" value="<s:property value="%{addPayee.city}"/>" tabindex="13">
		</td>
		<td>
			<input id="editPayeeAccount" class="ui-widget-content ui-corner-all" type="text" name="userAccountNumber" size="30" maxlength="30" value="<s:property value="%{addPayee.userAccountNumber}"/>" tabindex="4">
		</td>
	</tr>
	<tr>
		<td><span id="cityError"></span></td>
		<td><span id="userAccountNumberError"></span></td>
	</tr>									
	<tr>
		<td  id="stateNameId" class="ffiHidden"><span><s:text name="jsp.billpay_104"/><span class="required">*</span></span></td>
		<td id="editPayeeZipLabel" class=""><s:text name="jsp.default_473"/><span class="required">*</span></td>
	</tr>	
	<tr>
		<td id="stateSelectId">
				<!-- if the payee has no state while creation then -->  
				<s:if test="%{stateProvinceDefns.size>0}">
				<s:select list="stateProvinceDefns" listKey="stateKey" listValue="Name" emptyOption="true"
				value="%{addPayee.state}"
				class="txtbox" id="selectPayeeStateID"  name="state" tabindex="16"></s:select>
				</s:if>
				<!-- an element is create so if state is changed the changed states can be loaded -->
				<s:else>
				<select id="selectPayeeStateID" class="ffiHidden" name="state">
				</select>
				</s:else>
		</td>
		<td>
				<input id="editPayeeZipCode" class="ui-widget-content ui-corner-all" type="text" name="zipCode" size="12" value="<s:property value="%{addPayee.zipCode}"/>" tabindex="16">
		</td>
	</tr>
	<tr>
		<td><span id="stateError"></span></td>
		<td><span id="zipCodeError"></span></td>
	</tr>						
	<tr>
		<td id="editPayeeCountryLabel" class=""><s:text name="jsp.default_115"/><span class="required">*</span></td>
	</tr>
	<tr>
		<td>
			<s:select list="countryDefns" listKey="CountryCode" listValue="Name" value="%{addPayee.country}" id="selectPayeeCountryID" class="txtbox" name="countryDisplay" onchange="showState(this);" tabindex="15">
			</s:select>
			<!-- this variable actually holds the value of country to be passed
			beacuse textbox cannt identify event on change when existing data is removed,
			it only identifies it if change happens from the drop down of the select box -->
			<s:hidden name="country" id="countryPassed"></s:hidden>
		</td>	
	</tr>	
	<tr>
		<td><span id="countryError"></span></td>
	</tr>		
<tr><td style="cursor:auto" class="sectionhead nameTitle" colspan="2"><s:text name="jsp.billpay.payee.bank.info"/></td></tr>		
	<tr>
		<td id="editPayeeRoutingNoLabel" class="sectionsubhead"><s:property value="%{bankIdentifierDisplayText}"/>
			<s:hidden value="%{bankIdentifierDisplayText}" name="bankIdentifierDisplayText"></s:hidden>
			<s:hidden id="achRoutingNumber" name="financialInstitution.achRoutingNumber"></s:hidden>
		</td>
		<td id="editPayeeBankAccountNoLabel" class="sectionsubhead"><s:text name="jsp.billpay_26"/></td>
	</tr>									
	<tr>
		<td>
			<!-- if global payee show detail in non editable format -->
			<s:if test="%{addPayee.isGlobal}">
				<s:property value="%{addPayee.payeeRoute.bankIdentifier}"/>
			</s:if>
			<s:else>
			<input id="payeeRouteBankIdentifier" class="ui-widget-content ui-corner-all" type="text" name="payeeRoute.BankIdentifier" size="15" value="<s:property value="%{addPayee.payeeRoute.bankIdentifier}"/>" tabindex="5">
				<sj:a id="searchBankBillPayPayee" 
		                button="true" 
						onClickTopics="searchBankBillpayEditPayeeTopic"
		        	><s:text name="jsp.default_373"/></sj:a>
			</s:else> 
			<input id="bankIdentifier" class="txtbox" type="hidden" name="BankIdentifier" size="25" value="%{PayeeRoute.BankIdentifier}"></td>
		<td>
			<!-- if global payee show detail in non editable format -->
			<s:if test="%{addPayee.isGlobal}">
				<s:property value="%{addPayee.payeeRoute.accountID}"/>
			</s:if>
			<s:else>
				<input id="editPayeeBankAccount" class="ui-widget-content ui-corner-all" type="text" name="payeeRoute.accountID" size="30" value="<s:property value="%{addPayee.payeeRoute.accountID}"/>" tabindex="6">
			</s:else>
		</td>
	</tr>
	<tr>
		<td id="editPayeeBankAccountTypeLabel" class="sectionsubhead"><s:text name="jsp.billpay_27"/></td>	
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
		   <td id="editPayeeBankAccountCurrencyLabel" class="sectionsubhead"><s:text name="jsp.billpay_25"/></td>
		</ffi:cinclude>
	</tr>
	<tr>
		<td>
			 <!-- if global payee show detail in non editable format -->
			<s:if test="%{addPayee.isGlobal}">
				<s:property value="%{addPayee.payeeRoute.acctType}"/>
			</s:if>
			<s:else>
				<s:select list="accountTypeSelectList" id="addPayeeAccountType" name="payeeRoute.acctType" class="txtbox"
			value="%{addPayee.payeeRoute.acctType}">
				</s:select>
			</s:else>
	  	</td>
	  	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
			<td>
				  <!-- if global payee show detail in non editable format -->
				<s:if test="%{addPayee.isGlobal}">
					<s:property value="%{addPayee.payeeRoute.currencyCode}"/>
				</s:if>
				<s:else>
					<s:select list="forexCurrency" value="%{addPayee.payeeRoute.currencyCode}"
				  class="txtbox" id="addPayeeCurrencyCode" name="payeeRoute.currencyCode" tabindex="10">
				</s:select>
				</s:else>
			</td>
		</ffi:cinclude>
	</tr>
<%-- <tr>
	<td colspan="4" align="center">
			<sj:a id="cancelEditBillPayPayee" 
            button="true" 
onClickTopics="cancelBillpayForm"
    	><s:text name="jsp.default_82"/></sj:a>

<sj:a 
id="verifyBillpayPayeeEditSubmit"
formIds="BillpayPayeeEdit"
                        targets="verifyDiv" 
                        button="true" 
                        validate="true" 
                        validateFunction="customValidation"
                        onClickTopics="clearCountry"
                        onBeforeTopics="beforeVerifyBillpayForm"
                        onCompleteTopics="verifyBillpayFormComplete"
onErrorTopics="errorVerifyBillpayForm"
                        onSuccessTopics="successVerifyBillpayForm"
                  ><s:text name="jsp.default_291"/></sj:a>
</tr> --%>
</table>
<div class="btn-row">
<sj:a id="cancelEditBillPayPayee" 
            button="true" 
onClickTopics="cancelBillpayForm,cancelPayeeLoadForm"
    	><s:text name="jsp.default_82"/></sj:a>

<sj:a 
id="verifyBillpayPayeeEditSubmit"
formIds="BillpayPayeeEdit"
                        targets="verifyDiv" 
                        button="true" 
                        validate="true" 
                        validateFunction="customValidation"
                        onClickTopics="clearCountry"
                        onBeforeTopics="beforeVerifyBillpayForm"
                        onCompleteTopics="verifyBillpayFormComplete"
onErrorTopics="errorVerifyBillpayForm"
                        onSuccessTopics="successVerifyBillpayForm"
                  ><s:text name="jsp.default_291"/></sj:a>
</div>
</div></div>
</s:else>
</div>
</s:form>