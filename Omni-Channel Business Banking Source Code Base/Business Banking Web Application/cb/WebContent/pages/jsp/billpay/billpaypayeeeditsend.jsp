<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="payments_billpaypayeesend" className="moduleHelpClass"/>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper" role="form" aria-labeledby="summary">
<div class="leftPaneInnerWrapper label130">
	<div class="header"><h2 id="summary"><s:text name="jsp.billpay.payee_summary" /></h2></div>
	<div class="leftPaneInnerBox summaryBlock"  style="padding: 15px 5px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="editConfirmPayeeNameLabel"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue"  style="width:50%" id="editConfirmPayeeNameValue"><s:property value="%{name}" /></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"id="editConfirmPayeeAccountLabel"><s:text name="jsp.default_19"/>:</span>
				<span class="inlineSection floatleft labelValue" style="width:50%" id="editConfirmPayeeAccountValue"><s:property value="%{userAccountNumber}"/></span>
			</div>
	</div>
</div>
</div>
<div class="confirmPageDetails label130">
   	<div  class="blockHead"><s:text name="jsp.billpay.payee.info"/></div>
   		<div class="blockRow completed">
			<span id="billpayResultMessage"><span class="sapUiIconCls icon-accept"></span> <s:text name="jsp.billpay_payee_edit" /></span>
		</div>
   		<div class="blockContent marginTop10">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editConfirmPayeeNickNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_294"/>:</span>
					<span id="editConfirmPayeeNickNameValue" class="columndata" ><s:property value="%{nickName}" /></span>
				</div>
				<div class="inlineBlock">
					<span id="editConfirmPayeeContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
					<span id="editConfirmPayeeContactValue" class="columndata" >
						<s:property value="%{contactName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editConfirmPayeePhoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_37"/>:</span>
					<span id="editConfirmPayeePhoneValue" class="columndata" >
						<s:property value="%{phone}" />
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editConfirmPayeeAddress1Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
					<span id="editConfirmPayeeAddress1Value" class="columndata" >
						<s:property value="%{street}" />
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editConfirmPayeeAddress2Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
					<span id="editConfirmPayeeAddress2Value" class="columndata" >
						<s:property value="%{street2}" />
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editConfirmPayeeAddress3Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
					<span id="editConfirmPayeeAddress3Value" class="columndata" >
						<s:property value="%{street3}" />
					</span>
				</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editConfirmPayeeCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
					<span id="editConfirmPayeeCityValue" class="columndata" >
						<s:property value="%{city}" />
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editConfirmPayeeStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
					<span id="editConfirmPayeeStateValue" class="columndata" >
						<s:property value="%{stateDisplayName}"/>
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
					<span id="editConfirmPayeeZipLabel" class="columndata" >
						<s:property value="%{zipCode}" />
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editConfirmPayeeCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
					<span id="editConfirmPayeeCountryValue" class="columndata" >
						<s:property value="%{countryDisplayName}"/>
					</span>
				</div>
			</div>
 		</div>
   		<div  class="blockHead"><s:text name="jsp.billpay.payee.bank.info"/></div>
   		<div class="blockContent">
   			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editConfirmPayeeRountingNoLabel" class="sectionsubhead sectionLabel"><s:property value="%{bankIdentifierDisplayText}"/>:</span>
					<span id="editConfirmPayeeRountingNoValue" class="columndata" >
						<s:property value="%{payeeRoute.bankIdentifier}" />
					</span>
				</div>
				<div class="inlineBlock">
					<span id="editConfirmPayeeBankAccountNoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_26"/>:</span>
					<span id="editConfirmPayeeBankAccountNoValue" class="columndata" >
						<s:property value="%{payeeRoute.accountID}" />
					</span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="editConfirmPayeeBankAccountTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_27"/>:</span>
					<span id="editConfirmPayeeBankAccountTypeValue" class="columndata" >
					<s:if test="%{addPayee.isGlobal}">
						<s:property value="%{payeeRoute.acctType}"/>
					</s:if>
					<s:else>
						<s:property value="%{accountTypeSelectList[payeeRoute.acctType]}"/>
					</s:else>
					</span>
				</div>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
					<div class="inlineBlock">
						<span id="editConfirmPayeeBankAccountCurrencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_25"/>:</span>
						<span id="editConfirmPayeeBankAccountCurrencyValue" class="columndata" >
							<s:property value="%{payeeRoute.currencyCode}" />
						</span>
					</div>
				</ffi:cinclude>
			</div>
 		</div>
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
                    	
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.account_237" /><br>
												</span></td>
										</tr>
										<tr>
											<td class="sectionsubhead"></td>
											<td></td>
											<td></td>
											<td><br>
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td id="editConfirmPayeeNameLabel" class="sectionsubhead">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="editConfirmPayeeNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="Name" />
											<s:property value="%{name}" />
											</td>
											<td id="editConfirmPayeeAddress1Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td id="editConfirmPayeeAddress1Value" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="Street" />
											<s:property value="%{street}" />
											</td>
										</tr>
										<tr>
											<td id="editConfirmPayeeNickNameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_294"/></span></div>
											</td>
											<td id="editConfirmPayeeNickNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="NickName" />
											<s:property value="%{nickName}" />
											</td>
											<td id="editConfirmPayeeAddress2Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="editConfirmPayeeAddress2Value" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="Street2" />
											<s:property value="%{street2}" />
											</td>
										</tr>
										<tr>
											<td id="editConfirmPayeeContactLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="editConfirmPayeeContactValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="ContactName" />
											<s:property value="%{contactName}"/>
											</td>
											<td id="editConfirmPayeeAddress3Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="editConfirmPayeeAddress3Value" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="Street3" />
											<s:property value="%{street3}" />
											</td>
										</tr>
										<tr>
											<td id="editConfirmPayeePhoneLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_37"/></span></div>
											</td>
											<td id="editConfirmPayeePhoneValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="Phone" />
											<s:property value="%{phone}" />
											</td>
											<td id="editConfirmPayeeCityLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td id="editConfirmPayeeCityValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="City" />
											<s:property value="%{city}" />
											</td>
										</tr>
										<tr>
											<td id="editConfirmPayeeAccountLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_19"/></span></div>
											</td>
											<td id="editConfirmPayeeAccountValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="UserAccountNumber" />
											<s:property value="%{userAccountNumber}" />
											</td>
											<td id="editConfirmPayeeStateLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="editConfirmPayeeStateValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="EditPayee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{stateDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="editConfirmPayeeRountingNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">
													<ffi:getProperty name="TempBankIdentifierDisplayText"/>
													<s:property value="%{bankIdentifierDisplayText}"/>
													</span></div>
											</td>
											<td id="editConfirmPayeeRountingNoValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="PayeeRoute.BankIdentifier" />
											<s:property value="%{payeeRoute.bankIdentifier}" />
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><span><s:text name="jsp.default_473"/></span></span></div>
											</td>
											<td id="editConfirmPayeeZipLabel" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="ZipCode" />
											<s:property value="%{zipCode}" />
											</td>
										</tr>
										<tr>
											<td id="editConfirmPayeeBankAccountNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_26"/></span></div>
											</td>
											<td id="editConfirmPayeeBankAccountNoValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="PayeeRoute.AccountID" />
											<s:property value="%{payeeRoute.accountID}" />
											</td>
											<td id="editConfirmPayeeCountryLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_115"/></span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${EditPayee.Country}" />
											<td id="editConfirmPayeeCountryValue" class="columndata" colspan="2">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{countryDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="editConfirmPayeeBankAccountTypeLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_27"/></span></div>
											</td>
											<ffi:object id="GetTypeName" name="com.ffusion.tasks.util.Resource" />
											<ffi:setProperty name="GetTypeName" property="ResourceFilename" value="com.ffusion.beansresources.accounts.resources" />
											<ffi:setProperty name="GetTypeName" property="ResourceID" value="${EditPayee.PayeeRoute.AcctType}" />
											<ffi:process name="GetTypeName" />
											<td id="editConfirmPayeeBankAccountTypeValue" class="columndata" colspan="2">
											<ffi:getProperty name="GetTypeName" property="Resource" />
											<s:property value="%{accountTypeSelectList[payeeRoute.acctType]}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
										<tr>
											<td id="editConfirmPayeeBankAccountCurrencyLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_25"/></span></div>
											</td>
											<td id="editConfirmPayeeBankAccountCurrencyValue" class="columndata" colspan="2">
											<ffi:getProperty name="EditPayee" property="PayeeRoute.CurrencyCode" />
											<s:property value="%{payeeRoute.currencyCode}" />
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										</ffi:cinclude>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right"></div>
											</td>
											<td colspan="4"><div align="center">
													<br>
													<sj:a id="doneBillpayPayeeEditConfirm" 
										                button="true" 
														onClickTopics="cancelBillpayForm"
										        	><s:text name="jsp.default_175"/></sj:a>
										        	
													</div></td>
											<td></td>
										</tr>
									</table>
						</td>
						<td></td>
					</tr>
				</table> --%>
<div class="btn-row"><sj:a id="doneBillpayPayeeEditConfirm" button="true" onClickTopics="cancelPayeeLoadForm"><s:text name="jsp.default_175"/></sj:a></div>
</div>