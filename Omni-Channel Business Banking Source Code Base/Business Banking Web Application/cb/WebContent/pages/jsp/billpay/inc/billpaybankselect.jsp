<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<ffi:help id="payments_billpaypayeebankselect" className="moduleHelpClass"/>
<%-- 

<%  
    String bankName = "";
	String bankCity = "";
	String bankState = "";
	String bankCountry = "";
	String achRTN = "";

    if( request.getParameter("BillPayBankFormUsed") != null && request.getParameter("BillPayBankFormUsed").length() > 0 ){ session.setAttribute("BillPayBankFormUsed", request.getParameter("BillPayBankFormUsed")); }
    if( request.getParameter("GetBillPayBanks.BankName") != null && request.getParameter("GetBillPayBanks.BankName").length() > 0 ){ bankName = request.getParameter("GetBillPayBanks.BankName"); }
    if( request.getParameter("GetBillPayBanks.BankCity") != null && request.getParameter("GetBillPayBanks.BankCity").length() > 0 ){ bankCity = request.getParameter("GetBillPayBanks.BankCity"); }
    if( request.getParameter("GetBillPayBanks.BankState") != null && request.getParameter("GetBillPayBanks.BankState").length() > 0 ){ bankState = request.getParameter("GetBillPayBanks.BankState"); }
    if( request.getParameter("GetBillPayBanks.BankCountry") != null && request.getParameter("GetBillPayBanks.BankCountry").length() > 0 ){ bankCountry = request.getParameter("GetBillPayBanks.BankCountry"); }
    if( request.getParameter("GetBillPayBanks.AchRTN") != null && request.getParameter("GetBillPayBanks.AchRTN").length() > 0 ){ achRTN = request.getParameter("GetBillPayBanks.AchRTN"); }
%>

<ffi:removeProperty name="BillPayBank"/>
<ffi:object id="SetBillPayBank" name="com.ffusion.tasks.billpay.SetBillPayBank" scope="session"/>

<ffi:object id="GetBankLookupSettings" name="com.ffusion.tasks.user.GetBankLookupSettings" scope="session" />
<ffi:process name="GetBankLookupSettings"/>

<ffi:cinclude value1="${BillPayBankFormUsed}" value2="true" operator="notEquals">
	<ffi:object id="GetBillPayBanks" name="com.ffusion.tasks.billpay.GetBillPayBanks" scope="session"/>

	<% String fedRTN = ""; %>
	<ffi:getProperty name="BankIdentifier" assignTo="fedRTN"/>
	<%System.out.println("value of bank is ******"+fedRTN);%>	
	
	<ffi:setProperty name="GetBillPayBanks" property="AchRTN" value="<%= fedRTN %>"/>

</ffi:cinclude>
<ffi:removeProperty name="BillPayBankFormUsed"/>

<ffi:setProperty name="GetBillPayBanks" property="Reload" value="true"/>
<% if (bankName != null && bankName.length() > 0) { %>
        <ffi:setProperty name="GetBillPayBanks" property="BankName" value="<%=bankName%>" />
<% } %>
<% if (bankCity != null && bankCity.length() > 0) { %>
        <ffi:setProperty name="GetBillPayBanks" property="BankCity" value="<%=bankCity%>" />
<% } %>
<% if (bankState != null && bankState.length() > 0) { %>
        <ffi:setProperty name="GetBillPayBanks" property="BankState" value="<%=bankState%>" />
<% } %>
<% if (bankCountry != null && bankCountry.length() > 0) { %>
        <ffi:setProperty name="GetBillPayBanks" property="BankCountry" value="<%=bankCountry%>" />
<% } %>
<% if (achRTN != null && achRTN.length() > 0) { %>
        <ffi:setProperty name="GetBillPayBanks" property="AchRTN" value="<%=achRTN%>" />
<% } %>

<ffi:process name="GetBillPayBanks"/>

<ffi:setProperty name="BillPayBanks" property="SortedBy" value="NAME" /> --%>
<%--
<HTML>

	<head>
<ffi:include page="${PathExt}inc/timeout.jsp" />
		<TITLE>Bank Lookup Results</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="<ffi:getProperty name='ServletPath'/>FF.css" rel="stylesheet" media="screen">
	</head>

	<body>
--%>
		<div class="approvalDialogHt" style="margin-bottom:40px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<tr>
					<td class="columndata" align="center" class="ltrow2_color">
						<s:form id="billpay_BankFormId" action="billpaybanklist.jsp" method="post" name="BankForm">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<table width="100%" border="0" cellspacing="0" cellpadding="3" class="ltrow2_color">
							
							<script type="text/javascript">
								function showAddInstitutions(id,token){
									var urlString = "/cb/pages/jsp/billpay/billpayBankLookUp_payeeBankSelection.action";
									$.ajax({				  
										  url: urlString,
										  data :'financialInstitution.institutionId=' + id +"&CSRF_TOKEN=" + token,
										  type: "POST",
										  success: function(data) {
											 $('#searchBankingDialogID').html(data).dialog('open');
											 $.publish("common.topics.tabifyDialog");
										  }
									});
								}
							</script>
							<tr>
		                    	<th align="left"><s:text name="jsp.default_61" /></th>
		                    	<th align="left"><s:text name="jsp.default_35" /></th>
		                    	<th align="left"><s:text name="jsp.default_401" /></th>
		                    	<th align="left"><s:text name="jsp.default_201" /></th>
		                    </tr>
							<s:iterator value="financialInstitutions" var="fis">
								<tr id="billpay_bankListRowsID">
									<td height="30" valign="top">
										 <a class="link anchorText" href="javascript:showAddInstitutions
										 ('<s:property value="%{#fis.institutionId}"/>',
		 				  	             '<ffi:getProperty name='CSRF_TOKEN'/>')">
		 				  	             <ffi:getProperty name="Payee1" property="Name"/>
		 				  	             <s:property value="%{#fis.institutionName}"/>
		 				  	             </a>
									</td>
									<td height="30" valign="top">
										<s:property value="%{#fis.Street}"/>
											<s:if test="%{#fis.Street2!=''}">
												<s:property value="%{#fis.Street2}"/>
											</s:if>
										<s:property value="%{#fis.City}"/>
										<s:property value="%{#fis.State}"/>,
										<s:property value="%{#fis.StateCode}"/>
										<s:property value="%{#fis.Country}"/>
									</td>
									<td class="columndata">
										<ffi:cinclude value1="${fis.SwiftBIC}" value2="" operator="equals">
	                       					N/A
	                   					</ffi:cinclude>
	                   					<ffi:cinclude value1="${fis.SwiftBIC}" value2="" operator="notEquals">
	                       					<ffi:getProperty name="fis" property="SwiftBIC"/>
	                   					</ffi:cinclude>
													
									</td>
									<td class="columndata">
										<ffi:cinclude value1="${fis.AchRoutingNumber}" value2="" operator="equals">
	                       					N/A
	                   					</ffi:cinclude>
	                   					<ffi:cinclude value1="${fis.AchRoutingNumber}" value2="" operator="notEquals">
	                       					<ffi:getProperty name="fis" property="AchRoutingNumber"/>
	                   					</ffi:cinclude>
									</td>
									
								</tr>
 				  	        </s:iterator>
 				  	       <tr>
 				  	       	<td class="columndata" colspan="4"><br style="">
								*If you don't find the bank, please refine your search and try again</td>
							</tr>
							<tr>
								<td colspan="4" height="30">&nbsp;</td>
							</tr>
							<%-- <ffi:list collection="BillPayBanks" items="Bank1">
							<tr id="billpay_bankListRowsID">
								<td class="columndata"><ffi:link url="/cb/pages/jsp/billpay/inc/billpaybanklistitem.jsp?SetBillPayBank.Id=${Bank1.InstitutionId}" ><ffi:getProperty name="Bank1" property="InstitutionName"/></ffi:link></td>
								
								<td class="columndata">
                                    					<ffi:cinclude value1="${Bank1.Street}" value2="" operator="notEquals">
                                        					<ffi:getProperty name="Bank1" property="Street"/><br>
                                    					</ffi:cinclude>
                                    					<ffi:cinclude value1="${Bank1.Street2}" value2="" operator="notEquals">
                                        					<ffi:getProperty name="Bank1" property="Street2"/><br>
                                    					</ffi:cinclude>
                                    					<ffi:getProperty name="Bank1" property="City"/>,&nbsp;
                                    					<ffi:cinclude value1="${Bank1.State}" value2="" operator="notEquals" ><ffi:getProperty name="Bank1" property="State" /></ffi:cinclude>
                                    					<ffi:cinclude value1="${Bank1.State}" value2="" operator="equals" ><ffi:getProperty name="Bank1" property="StateCode" /></ffi:cinclude>
                                    					<br>
                                    					<ffi:getProperty name="Bank1" property="Country"/>
                                				</td>
							</tr>
							</ffi:list> --%>
							<tr>
								<td colspan="2" align="center"  class="ui-widget-header customDialogFooter">
                                <%-- <input class="submitbutton" type="button" value="SEARCH AGAIN" onClick="document.location='<ffi:urlEncrypt url="billpaybanklist.jsp?searchAgain=true" />'"> --%>
								<script>
									ns.billpay.researchBank = function(){
										$.ajax({
											type: "POST",
											url: "/cb/pages/jsp/billpay/billpayBankLookUp_payeeBankLookUp.action",
											data: {CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
											success: function(data) {
												$('#searchBankingDialogID').html(data);
												$.publish("common.topics.tabifyDialog");
											}
										});
									}
								</script>
								<sj:a
										id="billpay_researchBankListbuttonId"
										button="true"
										onClickTopics="billpay_researchBankListButtonOnClickTopics"
								><s:text name="jsp.default_374"/></sj:a>
								
                                <%-- <input class="submitbutton" type="button" value="CLOSE" onClick="self.close();return false;"> --%>
								<sj:a id="billpay_closeSearchBankbuttonId"
										button="true"
										onClickTopics="billpay_closeSearchBankListOnClickTopics"
								><s:text name="jsp.default_102"/></sj:a>
								
								</td>
							</tr>
						</table>
						</s:form>
					</td>
				</tr>
			</table>
		</div>
<%--
	</body>
</html>
--%>