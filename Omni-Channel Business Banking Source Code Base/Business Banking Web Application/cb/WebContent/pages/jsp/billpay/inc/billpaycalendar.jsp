<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames,
					com.ffusion.beans.SecureUser, com.ffusion.util.DateConsts"%>

<%-- 
<%@ page import="com.ffusion.beans.user.UserLocale,
                 com.ffusion.beans.accounts.Accounts" %>
		
<%
session.setAttribute("moduleNameForCalendar", request.getParameter("moduleName"));
%>
	<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
	<ffi:process name="GetCurrentDate"/>		
    <%-- Format and display the current date/time --%>	
	<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="<%= DateConsts.FULL_CALENDAR_DATE_FORMAT %>"/>
	
	
	

	<%-- <ffi:setGridURL grid="Payments_Calendar_Event_URL" name="Payments_Calendar_Event_URL" url="/cb/pages/jsp/billpay/billpayviewbill.jsp?Collection={0}&ID={1}" parm0="collectionName" parm1="tranId"/> --%>
	
	<ffi:setGridURL grid="Payments_Calendar_Event_URL" name="Payments_Calendar_Event_URL" url="/cb/pages/jsp/billpay/billpayviewbill.action?ID={0}&PaymentType={1}&RecPaymentID={2}" parm2="recPaymentId" parm1="paymentType" parm0="tranId"/>
	
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar-extra.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.print.css' media='print' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.overrides.css' />
	
	<!-- <script type='text/javascript' src='/cb/web/js/calendar/fullcalendar.js'></script> -->
	<style type="text/css">
		.fc td.fc-sat{
			width: 122px;
			}
	</style>
	<script type='text/javascript'>
		$(document).ready(function() {
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		var dateArray=[];
		$('#billPayCalendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},			
			editable: true,
			disableDragging:true,
			events: "<ffi:urlEncrypt url='/cb/pages/jsp/calendar/getCalendarEvents.action?moduleName=BillpayCalendar&GridURLs=Payments_Calendar_Event_URL' />",
			eventClick: function(calEvent, jsEvent, view){
					if(view.name == "month"){
						moveToDay(calEvent.start);	
					}else if(view.name == "basicDay"){
						var isLoaded = false;
						for(var i =0;i<$.parseHTML(this.innerHTML).length;i++){
							if($.parseHTML(this.innerHTML)[i].localName =="table"
									&& $.parseHTML(this.innerHTML)[i].className.indexOf("transactionHistoryTable")!="-1"){
								isLoaded = true;
								break;
							}
						}
						if(!isLoaded){
							clearData();
							var temp = $(this);
							var isCorporateUser='<%= ((SecureUser)session.getAttribute(SessionNames.SECURE_USER)).isCorporateUser()%>';
							
							if(isCorporateUser=='true'){
								$.ajax({
									   url: "/cb/pages/jsp/billpay/billpayviewbill_calendarData.action",
									   data:{
										   ID:calEvent.tranId,
										   PaymentType:calEvent.paymentType,
										   RecPaymentID:calEvent.recPaymentId,
										   isCalendar:true
									   },
									   success: function(data){
										   temp.append(data);
										   var lHeight = $(temp).parent().height();
										   $("div .fc-content").height(lHeight+50);
										}
								});
							}
						}
					}else{
						moveToDay(calEvent.start);	
					}
					
			},
					
			eventRender: function(event, element) {
				//removing css class for events o
				element.removeClass("fc-event fc-event-skin");
				element.find("div").removeClass("fc-event fc-event-skin");
				
				var view  =  $('#billPayCalendar').fullCalendar('getView');		
				var y = event.start;
				var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
				var cal = $("#billPayCalendar")[0];
				var data = $.data(cal,d.toString());
				
				if(view.name == "month"){
					
					if(dateArray.indexOf(d)=='-1'){
					dateArray.push(d);
					text = "";
					localeKey="";
					for (var key in event.calendarDateData) {
						  if (event.calendarDateData.hasOwnProperty(key)) {
							  if(event.calendarDateData[key] > 0) {
								  if(key == "Pending") {
									  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarPendingApprovalRecordCls sapUiIconCls icon-history'>";
									  localeKey=js_pending;
								  } else if(key == "Pending Approval") {
									  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarPendingRecordCls sapUiIconCls icon-pending'>";
									  localeKey=js_appr_pending;
								  } else if (key == "Completed") {
									  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
									  localeKey=js_completed;
								  } else if (key == "Skipped") {
									  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarPendingApprovalRecordCls sapUiIconCls icon-history'>";
									  localeKey=js_status_skip;
								  }

								  text = text + holderDiv;
								  text = text+" " +event.calendarDateData[key] + " "+ localeKey;
								  text += "</div>";
							  }
						  }
					}
					$(".completedevent").css("position", "absolute");
					element.find('.fc-event-title').html(text);
					$.data(cal,d.toString(),data);
					}else{
						return false;
					}
				}
				if(view.name == "basicWeek") {
				  if (dateArray.indexOf(event.referenceNumber)=='-1') {
					  text = "";
					  dateArray.push(event.referenceNumber);
					  key = event.statusName;
					  if(event.statusName.trim() == "Pending" || event.statusName.trim() == "Scheduled" || "Funds Allocated" == event.statusName.trim() || event.statusName.trim() == "Skipped") {
						  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewPendingApprovalRecordCls sapUiIconCls icon-history'>";
					  } else if(event.statusName.trim() == "Pending Approval" || "Approval Pending" == event.statusName.trim() || "Funds Allocated"==key) {
						  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewPendingRecordCls sapUiIconCls icon-pending'>";
					  } else if (key == "Completed") {
						  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
					  }

					  text = text + holderDiv;
					  text = text+" " + ns.common.HTMLEncode(event.title);
					  text += "</div>";
					  $(".completedevent").css("position", "absolute");
						element.find('.fc-event-title').html(text);
						$.data(cal,d.toString(),data);
				  }
				}
				if(view.name == "basicDay"){
					text = "";
					var statusName = ""; 
					if(event.statusName.trim() == "Pending" || event.statusName.trim() == "Scheduled" || "Funds Allocated" == event.statusName.trim() || event.statusName.trim() == "Skipped") {
						statusName = "<div class='marginRight20 marginleft10 floatleft'>"+event.statusName+"<span class='marginleft5 calendarPendingApprovalRecordCls sapUiIconCls icon-history'></span></div>";
					} else if(event.statusName.trim() == "Pending Approval" || "Approval Pending" == event.statusName.trim()) {
						statusName = "<div class='marginRight20 floatleft'>"+event.statusName+"<span class='marginleft5 calendarPendingRecordCls sapUiIconCls icon-pending'></span></div>";
					} else if (event.statusName.trim() == "Completed") {
						statusName = "<div class='marginRight20 floatleft'>Completed<span class='marginleft5 calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'></span></div>";
					}
					var referenceNumber = "<div class='marginRight20 floatleft'>"+js_reference_number+": <span class='boldIt'>"+event.referenceNumber+"</span></div>";
					var amount = "<div class='marginRight20 floatleft'>"+js_amount+":  <span class='boldIt'>"+event.amount+ " " +event.currencyCode +"</span></div>";
					var toAccount = ns.common.HTMLEncode(event.toAccount);
					var details = "<div class='marginRight20 floatleft'>"+js_account+":  <span class='boldIt'>"+ toAccount +"<span class='marginleft10 sapUiIconCls icon-arrow-right'></span>"+event.payeeName+"</span></div>";
					
					var existingTitle = event.title;
					
					//var newTitle = statusName + "Reference Number: " + referenceNumber + existingTitle;
					var newTitle = "<span class='heading'>"+statusName+referenceNumber+details+amount+"</span>";
					
					//$(".completedevent").css("position", "relative");
					var isCorporateUser='<%= ((SecureUser)session.getAttribute(SessionNames.SECURE_USER)).isCorporateUser()%>';
					if(isCorporateUser == "true") {
						$("div[class*='fc-view-basicDay'] div .fc-event-hori").addClass("completedeventDayView");	
					} else {
						// don't show hand cursor for consumer
						$("div[class*='fc-view-basicDay'] div .fc-event-hori").addClass("completedeventDayView resetHandCursor");
					}
					element.find('.fc-event-title').html(newTitle);
				}
			},
			
			eventAfterRender :function( event, element, view ) { 
				$('#notes').scrollTop(0);
				var y = event.start;
				var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
				var cal = $("#billPayCalendar")[0];
				$.removeData(cal,d.toString());
				dateArray.length=0;
			},
			
			windowResize: function(view) {
				/* $('#billPayCalendar').fullCalendar('refetchEvents');
				$('#billPayCalendar').fullCalendar('render'); */
			}
			
		});	
		});
		function moveToDay(date){
			var toDate = new Date(date);
			$('#billPayCalendar').fullCalendar( 'changeView', 'basicDay' );
			$('#billPayCalendar').fullCalendar( 'gotoDate', toDate );
		}
				
		function getServerDate(){
		    var date = new Date('<ffi:getProperty name="GetCurrentDate" property="Date" />');
			return date;
		} 

		function clearData(){
			$('.transactionHistoryTable').each(function(){
					$(this).slideToggle();
					$(this).remove();
			});
		}
	</script>

<style type='text/css'>

		label {
			margin-top: 40px;
			text-align: left;
			font-size: 14px;
			font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
			}

		#billPayCalendar {
			width: 95%;
			margin: 0 auto;
			}
			
		/* style added to increase the height of the content when a transaction is expanded for DayView */
		
		.fc-view-basicDay {
			height: 100%;
		}
	
		.fc-border-separate {
		     height: 100%;
		}	
		
		.tableAlerternateRowColor{
			cursor: default;
		}
	</style>
	<div id='billPayCalendar'></div>
