<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<script language="javascript">
$(function(){
	$("#selectPayeeStateID").combobox({width: 200,searchFromFirst:true});
});
</script>
<%
    String billPayeeState = null;
%>

<!-- default selected country and state -->
<ffi:getProperty name="AddPayee" property="State" assignTo="billPayeeState"/>
<% if (billPayeeState == null) { billPayeeState = ""; } %>

<ffi:object id="GetStatesForCountry" name="com.ffusion.tasks.util.GetStatesForCountry" scope="session" />
    <ffi:setProperty name="GetStatesForCountry" property="CountryCode" value='<%=request.getParameter("countryCode") %>' />
<ffi:process name="GetStatesForCountry" />

<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="equals">
		<select class="txtbox" id="selectPayeeStateID"  name="state" value="<ffi:getProperty name="AddPayee" property="State"/>" tabindex="16">
				<option <ffi:cinclude value1="<%= billPayeeState %>" value2="">selected </ffi:cinclude>value=""><s:text name="jsp.default_376"/></option>
					<ffi:list collection="GetStatesForCountry.StateList" items="Value">
					<option <ffi:cinclude value1="<%= billPayeeState %>" value2="${Value.StateKey}">selected </ffi:cinclude>value="<ffi:getProperty name="Value" property="StateKey"/>"><ffi:getProperty name='Value' property='Name'/></option>
				</ffi:list>
		</select>
		<span id="stateError"></span>
		<input type="hidden" name="checkState" value="true">
</ffi:cinclude>
<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true" operator="notEquals">
	<ffi:setProperty name="AddPayee" property="State" value=""/>
	<ffi:setProperty name="EditPayee" property="State" value=""/>
</ffi:cinclude>