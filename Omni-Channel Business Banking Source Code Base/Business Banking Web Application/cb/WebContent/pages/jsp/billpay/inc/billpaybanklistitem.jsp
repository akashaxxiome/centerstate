<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%-- <ffi:help id="payments_billpaypayeebankselect" className="moduleHelpClass"/>
<ffi:removeProperty name="BillPayBank"/>

<% String bankID = request.getParameter("SetBillPayBank.Id"); %>
<ffi:setProperty name="SetBillPayBank" property="Id" value="<%=bankID%>"/>
<ffi:process name="SetBillPayBank"/>

<% String str = ""; String currentTask = ""; %>
<%
if (request.getParameter("insertInfo") != null) { %>
	
		<ffi:object id="SetBillPayBank" name="com.ffusion.tasks.billpay.SetBillPayBank" scope="session" />
			<ffi:setProperty name="SetBillPayBank" property="PayeeSessionName" value="${payeeSessionName}"/>
			<ffi:setProperty name="SetBillPayBank" property="BeanSessionName" value="BillPayBank"/>
			<ffi:setProperty name="SetBillPayBank" property="id" value="${BillPayBank.InstitutionId}"/>
		<ffi:process name="SetBillPayBank"/>
		<ffi:removeProperty name="SetBillPayBank"/> --%>
<%--
<html><head>
<script>
window.opener.location.replace('<ffi:getProperty name="CallerURL"/>');

self.close();
</script>
</head><body></body></html>
--%>
<%-- <% } %> --%>

<%--
<HTML>
	<head>
<ffi:include page="${PathExt}inc/timeout.jsp" />
		<TITLE>Bank Lookup Results</TITLE>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="<ffi:getProperty name='ServletPath'/>FF.css" rel="stylesheet" media="screen">
	</head>

	<body>
--%>
		<div align="center">
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata ltrow2_color" align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td>
									<s:form id="billpaybanklistitemFormId" name="billpaybanklistitem.jsp" method="post">
										<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
										<table width="100%" border="0" cellspacing="0" cellpadding="3">
											<tr>
												<td class="sectionsubhead" align="center" colspan="2">
												<%-- <ffi:getProperty name="BillPayBank" property="InstitutionName"/> --%>
												<span class="headerTitle">
													<s:property value="%{financialInstitution.InstitutionName}"/>
												</span>
												<input type="hidden" name="tmp_url" value="billbanklistitem"/>
												</td>
											</tr>
											<tr>
										    	<td colspan="2"><p class="transactionHeading"><s:text name="jsp.user_31"/></p></td>
										    </tr>
											<tr>
										    	<td colspan="2">
										    		<table width="100%" border="0" cellspacing="0" cellpadding="5">
											    		<tr>
													    	<td width="40%" class="labelCls">Street</td>
													    	<td width="20%" class="labelCls"><s:text name="jsp.default_101"/></td>
													    	<td width="20%" class="labelCls"><s:text name="jsp.default_386"/></td>
													    	<td width="20%" class="labelCls"><s:text name="jsp.default_115"/></td>
													    </tr>
												    </table>	
										    	</td>
									    	</tr>
									    	<tr>
									    		<td colspan="3">
										    		<table width="100%" border="0" cellspacing="0" cellpadding="5">
											    		<tr>
												    		<td width="40%">
																<span class="columndata valueCls">
																	<ffi:getProperty name="BillPayBank" property="Street"/>
																	<s:property value="%{financialInstitution.Street}"/>,
																</span>
																<span class="columndata valueCls">
																	<ffi:getProperty name="BillPayBank" property="ZipCode"/>
																	<s:property value="%{financialInstitution.ZipCode}"/>
																</span>
															</td>
													    	<td width="20%" valign="top">
																<span class="columndata valueCls">
																	<ffi:getProperty name="BillPayBank" property="City"/>
																	<s:property value="%{financialInstitution.City}"/>,&nbsp;
																	<s:property value="%{financialInstitution.StateCode}"/>
																	<ffi:cinclude value1="${GetBillPayBank.State}" value2="" operator="notEquals" >
																	<ffi:getProperty name="BillPayBank" property="State" />
																	</ffi:cinclude>
																	<ffi:cinclude value1="${GetBillPayBank.State}" value2="" operator="equals" >
																	<ffi:getProperty name="BillPayBank" property="StateCode" />
																	</ffi:cinclude>
																</span>
															</td>
													    	<td width="20%" valign="top">
																<span class="columndata valueCls">
																	<s:property value="%{financialInstitution.StateCode}"/>
																	<ffi:cinclude value1="${GetBillPayBank.State}" value2="" operator="notEquals" >
																	<ffi:getProperty name="BillPayBank" property="State" />
																	</ffi:cinclude>
																	<ffi:cinclude value1="${GetBillPayBank.State}" value2="" operator="equals" >
																	<ffi:getProperty name="BillPayBank" property="StateCode" />
																	</ffi:cinclude>
																</span>
															</td>
													    	<td width="20%" valign="top">
																<span class="columndata">
																	<s:property value="%{financialInstitution.Country}"/>
																</span>
															</td>
														</tr>
												    </table>	
										    	</td>	
										    </tr>
									    	<tr>
										    	<td colspan="4" height="20">&nbsp;</td>
										    </tr>
										    <tr>
										    	<td colspan="4"><p class="transactionHeading"><s:text name="admin.banklookup.identification.codes"/></p></td>
										    </tr>
										    <tr>
										    	<td colspan="4">
											    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
												    	<tr>
													    	<td width="33%"><s:text name="jsp.default_201"/></td>
													    	<td width="33%"><s:text name="jsp.default_99"/></td>
													    	<td width="33%"><s:text name="jsp.default_289"/></td>
													    </tr>
												    </table>
											    </td>	
										    </tr>
									    	<tr>
										    	<td colspan="4">
											    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
												    	<tr>
													    	<td width="33%" valign="top">
														    	<span class="columndata"><s:property value="%{financialInstitution.AchRoutingNumber}"/></span>
													    	</td>
													    	<td width="33%" valign="top">
													    		<span class="columndata"><s:property value="%{financialInstitution.chipsUID}"/></span>
													    	</td>
													    	<td width="33%" valign="top">
													    		<span class="columndata"><s:property value="%{financialInstitution.nationalID}"/></span>
																<input id="rtNumId" type="hidden" name="rtNum" value="<s:property value="%{financialInstitution.AchRoutingNumber}"/>"/>
															</td>
													    </tr>
												    </table>
											    </td>	
										    </tr>
									    
											<%-- <tr>
												<td class="columndata" width="49%">
													<ffi:getProperty name="BillPayBank" property="Street"/>
													<s:property value="%{financialInstitution.Street}"/>
												</td>
												<td class="columndata" align="right" width="49%">&nbsp;</td>
											</tr>
											<tr>
												<td class="columndata">
												<ffi:getProperty name="BillPayBank" property="City"/>
												<s:property value="%{financialInstitution.City}"/>,&nbsp;
												<s:property value="%{financialInstitution.StateCode}"/>
												<ffi:cinclude value1="${GetBillPayBank.State}" value2="" operator="notEquals" >
												<ffi:getProperty name="BillPayBank" property="State" />
												</ffi:cinclude>
												<ffi:cinclude value1="${GetBillPayBank.State}" value2="" operator="equals" >
												<ffi:getProperty name="BillPayBank" property="StateCode" />
												</ffi:cinclude>
											</tr>
											<tr>
												<td class="columndata">
												 <s:property value="%{financialInstitution.SwiftBIC}"/>
												<td>
											</tr>
											<tr>
												<td class="columndata">
												<ffi:getProperty name="BillPayBank" property="ZipCode"/>
												<s:property value="%{financialInstitution.ZipCode}"/>
												</td>
												<td align="right" class="columndata">FED ABA: 
												<ffi:getProperty name="BillPayBank" property="AchRoutingNumber"/>
												 <s:property value="%{financialInstitution.AchRoutingNumber}"/>
												</td>
												<input id="rtNumId" type="hidden" name="rtNum" 
												value="<ffi:getProperty name="BillPayBank" property="AchRoutingNumber"/>"/>
												<input id="rtNumId" type="hidden" name="rtNum" 
												value="<s:property value="%{financialInstitution.AchRoutingNumber}"/>"/>
											</tr>
											<tr>
												<td class="columndata">
												<s:property value="%{financialInstitution.Country}"/>
												</td>
												<td align="right" class="columndata">&nbsp;</td>
											</tr>  --%>
											<tr>
												<td colspan="3" height="20">&nbsp;</td>
											</tr>
											<tr>	
											<td colspan="3" align="center" class="ui-widget-header customDialogFooter">
																						
												<%-- <input class="submitbutton" name="insertInfo" type="submit" value="INSERT BANK INFO" border="0">&nbsp;&nbsp; --%>
												<sj:a id="billpay_insertBankButtonId"
													button="true"
													onClickTopics="billpay_insertBankButtonIdOnClickTopics"
												><s:text name="jsp.default_250"/></sj:a>
												
												<%-- <input class="submitbutton" type="button" value="CLOSE" onclick="self.close();return false;"> --%>
												<sj:a id="billpay_closeSearchBankbuttonId"
													button="true"
													onClickTopics="billpay_closeSearchBankListOnClickTopics"
												><s:text name="jsp.default_102"/></sj:a>
												
												
												<%-- <input class="submitbutton" type="button" value="SEARCH AGAIN" onClick="document.location='<ffi:urlEncrypt url="billpaybanklist.jsp?searchAgain=true" />'"> --%>
												<sj:a
													id="billpay_researchBankListbuttonId"
													button="true"
													onClickTopics="billpay_researchBankListButtonOnClickTopics">
													<s:text name="jsp.default_374"/>
												</sj:a>
											</td>
											</tr>
										</table>
									</s:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
<%--
	</body>
</html>
--%>
