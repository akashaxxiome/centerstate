<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript">
$(document).ready(function() {
	/* to load the message on top on selection of payee */
	
	/* checks if a payee is present by checking the account number */
	  var instruction=$('#accNumber').val();
	/* if not null loads the message and sets the payee currency */
	  if(instruction!=''){
		  $('.instructions').html($('.hiddenInstruction').html());
		  $('#payeeCurrencyCode').html($('#payeeCurrency').val());
	  }
	  
	});
</script>
<s:hidden value="%{addRecPayment.payee.PayeeRoute.CurrencyCode}" id="payeeCurrency" name="addRecPayment.amtCurrency"></s:hidden>
<div class="selecteBeneficiaryMessage">
		<s:text name="jsp.billpay.no.payee"/>
</div>
<a id="expandBillpayPayee" onclick="$('#expandBillpayPayee').toggle(),$('#collapseBillpayPayee').toggle(),$('.payeeDetails').slideToggle()" class="hidden nameTitle expandArrow" title="Show Beneficiary Details">
	<ffi:getProperty name="addRecPayment.payee" property="Name" />
		<span class="nameSubTitle"> (<s:property value="addRecPayment.Payee.NickName"/> - 
		<ffi:getProperty name="addRecPayment.payee" property="UserAccountNumber" />) <span class="sapUiIconCls icon-navigation-down-arrow "></span></span>
</a>
<a id="collapseBillpayPayee" onclick="$('#expandBillpayPayee').toggle(),$('#collapseBillpayPayee').toggle(),$('.payeeDetails').slideToggle()" class="hidden nameTitle collapseArrow" title="Hide Beneficiary Details">
	<ffi:getProperty name="addRecPayment.payee" property="Name" />
		<span class="nameSubTitle">(<s:property value="addRecPayment.Payee.NickName"/> - 
		<ffi:getProperty name="addRecPayment.payee" property="UserAccountNumber" />) <span class="sapUiIconCls icon-navigation-up-arrow "></span></span>
</a>
<div id='payeeDisplayNameText' class="hidden">
	<s:property value="%{addRecPayment.payee.Name}"/>
</div>
<div id='payeeDisplayNickNameText' class="hidden">
	<s:if test="%{addRecPayment.payee.NickName !='' }">
		<s:property value="%{addRecPayment.payee.NickName}"/>
	</s:if>
</div>
<div id='payeeDisplayUserAccountNumberText' class="hidden">
	<s:if test="%{addRecPayment.payee.UserAccountNumber !='' }">
		-<s:property value="%{addRecPayment.payee.UserAccountNumber}"/>
	</s:if>
</div>
<div class="payeeDetails hidden label130 marginTop10">
<div class="blockWrapper">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel lastPaymentinstructions" ><s:text name="jsp.billpay.last.payment"/></span>
				<span>
					<s:if test="%{lastPaymentToPayee.size>0}">
						<s:iterator value="lastPaymentToPayee" var="test">
						<s:property value="%{#test.amount}"/>-<s:property value="%{#test.PayDate}"/>
					</s:iterator>
					</s:if>
					<s:else>
						<s:text name="jsp.billpay_none"/>
					</s:else>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
				<span class="columndata"><s:property value="%{addRecPayment.payee.NickName}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_15"/>:</span>
				<span class="columndata">
					<s:if test="%{addRecPayment.payee.UserAccountNumber!=''}">
						<s:property value="%{addRecPayment.payee.UserAccountNumber}"/>
					</s:if>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_36"/>:</span>
				<span class="columndata"><s:property value="%{addRecPayment.payee.Street}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_37"/>:</span>
				<span class="columndata" ><s:property value="%{addRecPayment.payee.Street2}"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_38"/>:</span>
				<span class="columndata"><s:property value="%{addRecPayment.payee.Street3}"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_101"/>:</span>
				<span class="columndata"><s:property value="%{addRecPayment.payee.City}"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_104"/>:</span>
				<span class="columndata"><s:property value="%{addRecPayment.payee.stateDisplayName}"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_473"/>:</span>
				<span class="columndata"><s:property value="%{addRecPayment.payee.ZipCode}"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_115"/>:</span>
				<%-- <ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" /> --%>
				<span class="columndata">
					<s:property value="%{addRecPayment.payee.countryDisplayName}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_36"/>:</span>
				<span class="columndata"><s:property value="%{addRecPayment.payee.contactName}"/></span>
			</div>
			<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_83"/>:</span>
					<span class="columndata">
						<s:property value="%{addRecPayment.payee.Phone}"/>
					</span>									
			</div>
		</div>
	</div>
</div>
</div>
<div class="hiddenInstruction" style="display:none">
<s:hidden value="%{addRecPayment.payee.userAccountNumber}" id="accNumber"></s:hidden>
<s:hidden value="%{ID}" name="ID"></s:hidden>
<ffi:getL10NString rsrcFile="efs" msgKey="DaysToPay" parm0="${addRecPayment.payee.Name}" parm1="${addRecPayment.payee.DaysToPay}" encode="false"/>
</div>
