<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:help id="payments_billpayInquiry" className="moduleHelpClass"/>
<%-- <ffi:cinclude value1="${messaging_init_touched}" value2="true" operator="notEquals" >
	<s:include value="%{#session.PagesPath}inc/init/messaging-init.jsp" />
</ffi:cinclude> --%>


<style>
	/* #inquiryForm { margin-right: 180px \9; } */
</style>

<%	
	
	String Subject = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.message_36", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
	
%>
	
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
	<div class="blockContent">
	<%-- the table will be identical if the transaction type is either FUNDS_TYPE_BILL_PAYMENT or FUNDS_TYPE_REC_BILL_PAYMENT --%>
	<s:if test="%{payment.type == @com.ffusion.beans.FundsTransactionTypes@FUNDS_TYPE_BILL_PAYMENT}">					
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_316"/></span>
				<span class="columndata lightBackground"><s:property value="%{payment.payee.name}"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_21"/>:</span>
				<span class="columndata lightBackground"><s:property value="%{payment.account.accountDisplayText}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_143"/></span>
				<span class="columndata lightBackground"><s:property value="%{payment.payDate}"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_430"/></span>
				<span class="columndata lightBackground"><s:text name="jsp.default_320"/></span>	
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_45"/>:</span>
				<span class="columndata lightBackground"><s:property value="%{payment.AmountValue.CurrencyStringNoSymbol}"/> <s:property value="%{payment.AmtCurrency}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_debitAmountLabel" class="columndata ">
	        	    <!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
	        	    <s:if test="%{payment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else> <s:text name="jsp.billpay_40"/>:</s:else>
				</span>
				<%-- <span>
					<!-- if converted amount exists display that -->
					<s:if test='%{payment.convertedAmount}'>
						&#8776; <s:property value="%{payment.convertedAmount}" />
					</s:if>
					<!-- if not,means account currency and payee currency are same hence simply display the amount -->
					<s:else>
						<s:property value="%{payment.AmountValue.AmountValue}" />
					</s:else>
					&nbsp;
					<s:if test='%{payment.account.currencyCode==null}'>
						<s:property value="%{payment.amtCurrency}" />
					</s:if>
					<s:else>
						<s:property value="%{payment.account.currencyCode}" />
					</s:else>
				</span> --%>
				<span id="viewBillPay_debitAmountValue" class="columndata" id="estimatedAmount2">
							<!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
							<s:if test="%{payment.convertedAmount!=''}">
			        	    	<!-- do nothing -->
			        	    </s:if>
			        	    <s:else>
			        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
			        	    </s:else>
				</span>
			</div>
		</div>
		</s:if>
		<%-- the table will be identical if the transaction type is either FUNDS_TYPE_BILL_PAYMENT or FUNDS_TYPE_REC_BILL_PAYMENT --%>
		<s:if test="%{payment.type == @com.ffusion.beans.FundsTransactionTypes@FUNDS_TYPE_REC_BILL_PAYMENT}">	
				
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_316"/></span>
				<span class="columndata lightBackground"><s:property value="%{payment.payee.name}"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_21"/>:</span>
				<span class="columndata lightBackground"><s:property value="%{payment.account.accountDisplayText}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_143"/></span>
				<span class="columndata lightBackground"><s:property value="%{payment.payDate}"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_430"/></span>
				<span class="columndata lightBackground"><s:text name="jsp.default_320"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_45"/>:</span>
				<span class="columndata lightBackground"><s:property value="%{payment.AmountValue.CurrencyStringNoSymbol}"/> <s:property value="%{payment.AmtCurrency}"/></span>
					<%-- <s:property value="%{payment.convertedAmount}"/><s:property value="%{payment.account.currencyCode}"/> --%>
					
			</span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_debitAmountLabel" class="columndata ">
	        	    <!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
	        	    <s:if test="%{payment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else> <s:text name="jsp.billpay_40"/>:</s:else>
				</span>
				<%-- <span>
					<!-- if converted amount exists display that -->
					<s:if test='%{payment.convertedAmount}'>
						&#8776; <s:property value="%{payment.convertedAmount}" />
					</s:if>
					<!-- if not,means account currency and payee currency are same hence simply display the amount -->
					<s:else>
						<s:property value="%{payment.AmountValue.AmountValue}" />
					</s:else>
					&nbsp;
					<s:if test='%{payment.account.currencyCode==null}'>
						<s:property value="%{payment.amtCurrency}" />
					</s:if>
					<s:else>
						<s:property value="%{payment.account.currencyCode}" />
					</s:else>
				</span> --%>
				<span id="viewBillPay_debitAmountValue" class="columndata" id="estimatedAmount2">
							<!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
							<s:if test="%{payment.convertedAmount!=''}">
			        	    	<!-- do nothing -->
			        	    </s:if>
			        	    <s:else>
			        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
			        	    </s:else>
				</span>
			</div>
		</div>
		</s:if>
		<%-- the table will be identical if the transaction type is either FUNDS_TYPE_BILL_PAYMENT or FUNDS_TYPE_REC_BILL_PAYMENT --%>
		<%-- the table will be identical if the transaction type is either FUNDS_TYPE_BILL_PAYMENT or FUNDS_TYPE_REC_BILL_PAYMENT --%>
		<s:if test="%{payment.type == @com.ffusion.beans.FundsTransactionTypes@FUNDS_TYPE_TRANSFER}">		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_219"/></span>
				<span class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="FromAccountDisplayText" /></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_425"/></span>
				<span class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="ToAccountDisplayText" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_143"/></span>
				<span class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="TransactionDate" /></span>
			</div>
			 <ffi:cinclude value1="${FundTransactionMessage.TransactionUserAssignedAmountFlagName}" value2="single" operator="equals">
                 <div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_45"/></span>
					<span class="columndata lightBackground">
	                          <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyStringNoSymbol" />
	                          <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyCode" />
	                      </span>
				</div>
              </ffi:cinclude>
		</div>
		<ffi:cinclude value1="${FundTransactionMessage.TransactionUserAssignedAmountFlagName}" value2="single" operator="notEquals">
            <div class="blockRow"><div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_221"/></span>
				<span class="columndata lightBackground">
                    <ffi:cinclude value1="${FundTransactionMessage.IsAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                    <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyStringNoSymbol" />
                    <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyCode" />
                </span>
			</div>
          	<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_427"/></span>
				<span class="columndata lightBackground">
                     <ffi:cinclude value1="${FundTransactionMessage.IsToAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                     <ffi:getProperty name="FundTransactionMessage" property="TransactionToAmountValue.CurrencyStringNoSymbol" />
                     <ffi:getProperty name="FundTransactionMessage" property="TransactionToAmountValue.CurrencyCode" />
                </span>
			</div></div>
        </ffi:cinclude>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_430"/></span>
			<span class="columndata lightBackground"><s:text name="jsp.default_442"/></span>
		</div>
		</s:if>
		<s:if test="%{payment.type == @com.ffusion.beans.FundsTransactionTypes@FUNDS_TYPE_REC_TRANSFER}">
		<%-- the block below is copied from the above transfer cinclude, as the condition has an "OR" clause --%>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_219"/></span>
				<span class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="FromAccountDisplayText" /></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_425"/></span>
				<span class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="ToAccountDisplayText" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_143"/></span>
				<span class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="TransactionDate" /></span>
			</div>
			<ffi:cinclude value1="${FundTransactionMessage.TransactionUserAssignedAmountFlagName}" value2="single" operator="equals">
                  <div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_45"/></span>
					<span class="columndata lightBackground">
                          <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyStringNoSymbol" />
                          <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyCode" />
                    </span>
				</div>
            </ffi:cinclude>
		</div>
		
		<ffi:cinclude value1="${FundTransactionMessage.TransactionUserAssignedAmountFlagName}" value2="single" operator="notEquals">
        <div class="blockRow"> 
			<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_221"/></span>
						<span class="columndata lightBackground">
                            <ffi:cinclude value1="${FundTransactionMessage.IsAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyStringNoSymbol" />
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyCode" />
		    </div>
		    <div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_427"/></span>
				<span class="columndata lightBackground">
                   <ffi:cinclude value1="${FundTransactionMessage.IsToAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                   <ffi:getProperty name="FundTransactionMessage" property="TransactionToAmountValue.CurrencyStringNoSymbol" />
                   <ffi:getProperty name="FundTransactionMessage" property="TransactionToAmountValue.CurrencyCode" />
               </span>
			</div>
		</div>
        </ffi:cinclude>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_430"/></span>
			<span class="columndata lightBackground"><s:text name="jsp.default_442"/></span>
		</div>
		</s:if>
	</div>
</div>
<div class="sectionsubhead marginTop10"><s:text name="jsp.default.inquiry.message.RTE.label" /><span class="required">*</span></div>
<s:form id="inquiryForm" name="form4" theme="simple" method="post" action="/pages/jsp/billpay/sendBillPayFundsTransMessageAction_sendBillPayFundsInquiry.action" style="text-align: left;">							
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<input type="hidden" name="fundTransactionMessage.subject" value="<ffi:getProperty name='Subject'/>"/>																			
	<s:hidden name="paymentType" value="%{#attr.paymentType}"></s:hidden>
	<s:hidden name="fundTransactionMessage.from" value="%{#session.User.id}"></s:hidden>
	<s:hidden name="fundTransactionMessage.comment" value="%{payment.inquireComment}"></s:hidden>
	<div align="left">
		<p>
			<textarea class="txtbox ui-widget-content ui-corner-all" id="sendBillPayFundsInquiryText" name="fundTransactionMessage.memo" rows="10" cols="75" style="overflow-y: auto;" wrap="virtual" ></textarea>
		</p>
	</div>
<s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
</s:form>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div  class="ui-widget-header customDialogFooter">
				<sj:a id="closeInquireBillpayLink" button="true" 
			  			onClickTopics="closeDialog" 
			  			title="%{getText('jsp.default_83')}"><s:text name="jsp.default_82"/></sj:a>
			  
					<sj:a id="cancelBillpaySubmitButton" 
						  formIds="inquiryForm" 
						  targets="resultmessage" 
						  button="true" 
						  validate="true"
						  validateFunction="customValidation" 
						  title="%{getText('Send_Inquiry')}"
						  onSuccessTopics="sendInquiryBillpayFormSuccess"><s:text name="jsp.default_378" /></sj:a>
</div>

<script>
	setTimeout(function(){ns.common.renderRichTextEditor('sendBillPayFundsInquiryText','600',true);}, 600);
</script>
