<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<div style="display:none">

<%-- 			<span id="billpayResultMessage"><s:text name="jsp.billpay_119"/></span> --%>
<span id="billpayResultMessageTemplate"><s:text name="jsp.billpay_120"/></span>
</div>
<s:form id="BillpayEditSend" namespace="/pages/jsp/billpay" validate="false" action="editBillpayAction_execute" method="post" name="BillpayEditSend" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input id="ID" name="ID" value="<s:property value="%{ID}"/>" type="hidden"/>		
<input id="isRecModel" name="isRecModel" value="<s:property value="%{isRecModel}"/>" type="hidden"/>
<div class="leftPaneWrapper" role="form" aria-labeledby="billPaySummary">
<div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="billPaySummary"><s:text name="jsp.billpay_bill_pay_summary" /></h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="editVerifyBillPay_payeeNameLabel"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue" id="editVerifyBillPay_payeeNameValue"><s:property value="%{editPayment.payeeName}"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_15"/>:</span>
				<span class="inlineSection floatleft labelValue" id="accountName"> <s:property value="%{editPayment.Account.accountDisplayText}"/>
				</span>
			</div>
	</div>
</div></div>
<div class="confirmPageDetails">
<s:if test="%{isDateChanged}">
<div id="editVerifyBillPay_warningMessage" class="columndata_error" align="center"><s:text name="jsp.billpay_118"/></div>
</s:if>
<s:if test="%{isRecModel == 'true'}">
<div class="columndata_error"  align="center"><s:text name="jsp.billpay_116"/></div>
</s:if>
<div  class="blockHead toggleClick expandArrow">
<s:text name="jsp.billpay.payee_summary" />: <ffi:getProperty name="editPayment.payee" property="Name" /> (<s:property value="editPayment.payee.nickName"/> - <ffi:getProperty name="editPayment.payee" property="UserAccountNumber" />)
<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
	<div class="blockWrapper">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_75"/>:</span>
				<span id="editVerifyBillPay_payeeNickNameValue" class="columndata" ><s:property value="editPayment.payee.nickName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="editVerifyBillPay_lastPaymentLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_last_payment_to_payee"/></span>
				<span id="editVerifyBillPay_lastPaymentValue" class="columndata">
					<ffi:list collection="lastPaymentToPayee" items="lastPayment" startIndex="1" endIndex="1">
							<ffi:setProperty name="lastPayment" property="DateFormat"/>
							<ffi:getProperty name="lastPayment" property="Amount"/>-<ffi:getProperty name="lastPayment" property="PayDate"/>
					</ffi:list>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_payeeAccountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_15"/>:</span>
				<span id="editVerifyBillPay_payeeAccountValue" class="columndata" ><ffi:getProperty name="editPayment.payee" property="UserAccountNumber" /></span>
			</div>
			<div class="inlineBlock">
				<span id="editVerifyBillPay_firstAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
				<span id="editVerifyBillPay_firstAddressValue" class="columndata" ><ffi:getProperty name="editPayment.payee" property="Street" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_secondAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
				<span id="editVerifyBillPay_secondAddressValue" class="columndata" ><ffi:getProperty name="editPayment.payee" property="Street2" /></span>
			</div>
			<div class="inlineBlock">
				<span id="editVerifyBillPay_thirdAddressLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
				<span id="editVerifyBillPay_thirdAddressValue" class="columndata" ><ffi:getProperty name="editPayment.payee" property="Street3" /></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_cityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
				<span id="editVerifyBillPay_cityValue" class="columndata" ><ffi:getProperty name="editPayment.payee" property="City" /></span>
			</div>
			<div class="inlineBlock">
				<span id="editVerifyBillPay_stateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
				<span id="editVerifyBillPay_stateValue" class="columndata" ><s:property value="%{editPayment.payee.stateDisplayName}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_zipCodeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
				<span id="editVerifyBillPay_zipCodeValue" class="columndata" ><ffi:getProperty name="editPayment.payee" property="ZipCode" /></span>
			</div>
			<div class="inlineBlock">
				<span id="editVerifyBillPay_countryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
				<span id="editVerifyBillPay_countryValue" class="columndata" ><s:property value="%{editPayment.payee.countryDisplayName}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_contactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
				<span id="editVerifyBillPay_contactValue" class="columndata" ><ffi:getProperty name="editPayment.payee" property="ContactName" /></span>
			</div>
			<div class="inlineBlock">
				<span id="editVerifyBillPay_phoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_83"/>:</span>
				<span id="editVerifyBillPay_phoneValue" class="columndata" ><ffi:getProperty name="editPayment.payee" property="Phone" /></span>
			</div>
		</div>
	</div>
</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart--><s:text name="jsp.transaction.summary.label" /><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_amountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
				<span id="editVerifyBillPay_amountValue" class="columndata">
						<s:property value="editPayment.AmountValue.CurrencyStringNoSymbol"/> <s:property value="editPayment.payee.PayeeRoute.CurrencyCode"/>
				</span>								 
			</div>
			<div class="inlineBlock">
				<span id="editVerifyBillPay_debitAmountLabel" class="sectionsubhead sectionLabel">
						 <s:if test="%{editPayment.convertedAmount!=''}"><!-- do nothing --> </s:if>
						 <s:else><s:text name="jsp.billpay_40"/>:</s:else></span>
				<span id="editVerifyBillPay_debitAmountValue" class="columndata" colspan="2">
					 <s:if test="%{editPayment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else>
	        	    	&#8776; <s:property value="%{editPayment.convertedAmount}"/>  
	        	    	<s:property value="%{editPayment.account.currencyCode}"/>
	        	    </s:else>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_payDateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_76"/>:</span>
				<span id="editVerifyBillPay_payDateValue" class="columndata"><ffi:getProperty name="editPayment" property="PayDate" /></span>
			</div>
			<div class="inlineBlock">
				<span id="editVerifyBillPay_deliverByDateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_deliver_by"/>:</span>
				<span id="editVerifyBillPay_deliverByDateValue" class="columndata">
					<s:property value="%{editPayment.deliverByDate}"/>				
				</span>				
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_recurringLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_351"/>:</span>
				<span id="editVerifyBillPay_recurringValue" class="columndata">												
					<ffi:getProperty name="editPayment" property="Frequency" />
				</span>
			</div>
			<div class="inlineBlock">
				<span id="editVerifyBillPay_frequencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/>:</span>
				<span id="editVerifyBillPay_frequencyValue" class="columndata">
					<s:if test="%{editPayment.openEnded}">
						<s:text name="jsp.default_446"/>
					</s:if>
					<s:else>
						<s:property value="%{editPayment.NumberPayments}"/>
					</s:else>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="editVerifyBillPay_memoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
				<span id="editVerifyBillPay_memoValue" class="columndata"><ffi:getProperty name="editPayment" property="memo" /></span>
			</div>
			<div class="inlineBlock">
			</div>
		</div>
	</div>
</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
							
									<table width="100%" border="0" cellspacing="0" cellpadding="1">
									 	<!-- <tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr> -->
										<%-- <ffi:cinclude value1="${EditPayment.DateChanged}" value2="true"> --%>
										<%-- <s:if test="%{isDateChanged}">
										<tr>	
											<td id="editVerifyBillPay_warningMessage" class="sectionsubhead" colspan="6" style="color:red" align="center">
											<s:text name="jsp.billpay_118"/><br><br>
											</td>
										</tr>
										</s:if> --%>
										<%-- </ffi:cinclude> --%>
										<%-- <ffi:cinclude value1="${EditPayment.Type}" value2="<%= String.valueOf(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_BILL_PAYMENT) %>"> --%>
										<%-- <s:if test='%{recPaymentID != "null"}'>
											<tr>
												<td class="sectionsubhead" colspan="6" style="color:red" align="center">
												<s:text name="jsp.billpay_116"/><br><br>
												</td>
											</tr>
										</s:if> --%>
										<%-- </ffi:cinclude> --%>
										<%-- <tr>
											<td id="editVerifyBillPay_panelTitle"class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_84"/><br>
												</span></td>
										</tr>
                                         <tr>
											<td class="columndata ltrow2_color"></td>
											<td></td>
											<td></td>
											<td class="sectionsubhead"><br>
											</td>
											<td></td>
											<td></td>
										</tr> --%>
                                       <%--  <tr>
											<td id="editVerifyBillPay_payeeNickNameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td id="editVerifyBillPay_payeeNickNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="NickName" />
											<s:property value="editPayment.payee.nickName"/>
											</td>
											<td id="editVerifyBillPay_amountLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>
											</td>
											<td id="editVerifyBillPay_amountValue" class="columndata" colspan="2">
											
											<s:property value="editPayment.AmountValue.CurrencyStringNoSymbol"/>
												 <s:property value="editPayment.payee.PayeeRoute.CurrencyCode"/>
												 
											<ffi:getProperty name="EditPayment" property="AmountValue.CurrencyStringNoSymbol" />&nbsp;&nbsp;
											<ffi:getProperty name="EditPayment" property="AmountValue.CurrencyCode" />									
											
											</td>
										</tr> --%>
										
																			<!-- last payments -->
											<%-- <tr >
											<td id="editVerifyBillPay_lastPaymentLabel" align="right" class="columndata ltrow2_color instructions">
											<div align="right">
													<s:text name="jsp.billpay_last_payment_to_payee"/></div>
													</td>
											<td id="editVerifyBillPay_lastPaymentValue" class="columndata instructions" colspan="2" align="left" style="padding:2px;">
												<ffi:setProperty name="LastPayments" property="Filter" value="PAYEEID=${EditPayment.PayeeID}"/>
												<ffi:cinclude value1="${LastPayments.Size}" value2="0" operator="notEquals">
														<ffi:list collection="LastPayments" items="lastPayment" startIndex="1" endIndex="1">
															<ffi:setProperty name="lastPayment" property="DateFormat" value="${UserLocale.DateFormat}" />
															<ffi:getProperty name="lastPayment" property="Amount"/>&nbsp;-&nbsp;<ffi:getProperty name="lastPayment" property="PayDate"/>
															</ffi:list>
													</ffi:cinclude>
													<ffi:cinclude value1="${LastPayments.Size}" value2="0" operator="equals"><s:text name="jsp.billpay_none"/></ffi:cinclude>
												<ffi:list collection="lastPaymentToPayee" items="lastPayment" startIndex="1" endIndex="1">
														<ffi:setProperty name="lastPayment" property="DateFormat"/>
														<ffi:getProperty name="lastPayment" property="Amount"/>-<ffi:getProperty name="lastPayment" property="PayDate"/>
												</ffi:list>
												</td>
												<td class="columndata tbrd_l"></td>
												<td class="columndata" colspan="2"></td>												
										</tr>	 --%>
										<%-- <tr>
											<td id="editVerifyBillPay_payeeNameLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="editVerifyBillPay_payeeNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="editPayment.payee" property="Name" /></td>
											<td id="editVerifyBillPay_debitAmountLabel" class="columndata tbrd_l">
											 <s:if test="%{editPayment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
								        	    </s:else>
                                                <ffi:cinclude value1="${EditPayment.AmountValue.CurrencyCode}" value2="${EditPayment.Account.CurrencyCode}" operator="notEquals" >
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
											    </ffi:cinclude>
                                                <ffi:cinclude value1="${EditPayment.AmountValue.CurrencyCode}" value2="${EditPayment.Account.CurrencyCode}" operator="equals" >
                                                    &nbsp;
                                                </ffi:cinclude>
											</td>

											<td id="editVerifyBillPay_debitAmountValue" class="columndata" colspan="2">
											<ffi:cinclude value1="${EditPayment.AmountValue.CurrencyCode}" value2="${EditPayment.Account.CurrencyCode}" operator="notEquals" >
											<ffi:object id="ConvertToTargetCurrency" name="com.ffusion.tasks.fx.ConvertToTargetCurrency" scope="session"/>
											<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmount" value="${EditPayment.AmountForBPW}" />
											<ffi:setProperty name="ConvertToTargetCurrency" property="BaseAmtCurrency" value="${EditPayment.AmountValue.CurrencyCode}" />
											<ffi:setProperty name="ConvertToTargetCurrency" property="TargetAmtCurrency" value="${EditPayment.Account.CurrencyCode}" />
											<ffi:process name="ConvertToTargetCurrency"/>
											<ffi:setProperty name="CONVERTED_CURRENCY_AMOUNT"  value="${ConvertToTargetCurrency.TargetAmount}"/>

											&#8776; <s:property value="#attr.ConvertToTargetCurrency.targetAmountWithCommaSeparator"/>
                                            <ffi:getProperty name="EditPayment" property="Account.CurrencyCode"/>
											</ffi:cinclude>
											 <s:if test="%{editPayment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	&#8776; <s:property value="%{editPayment.convertedAmount}"/>  
								        	    	<s:property value="%{editPayment.account.currencyCode}"/>
								        	    </s:else>
											</td>
										</tr> --%>
										
									<%-- 	<tr>
											<td id="editVerifyBillPay_payeeAccountLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td>
											<td id="editVerifyBillPay_payeeAccountValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="UserAccountNumber" /></td>
											<ffi:getProperty name="editPayment.payee" property="UserAccountNumber" /></td>
											<td id="editVerifyBillPay_fromAccountLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
											</td> --%>
											<%-- <td id="editVerifyBillPay_fromAccountValue" class="columndata" colspan="2">
												<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
												<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" 
													value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY %>"/>			
												<s:set var="AccountDisplayTextTaskBean" value="#session.EditPayment.Account" scope="request" />
												<ffi:process name="AccountDisplayTextTask"/>
												<ffi:getProperty name="AccountDisplayTextTask" property="accountDisplayText"/>
												
												<ffi:getProperty name="EditPayment" property="Account.DisplayText"/>
												<ffi:cinclude value1="${EditPayment.Account.NickName}" value2="" operator="notEquals" >
													 - <ffi:getProperty name="EditPayment" property="Account.NickName"/>
										 		</ffi:cinclude>	
												 - <ffi:getProperty name="EditPayment" property="Account.CurrencyCode"/>
												  <s:property value="%{editPayment.account.accountDisplayText}"/>
											</td> 
										</tr>--%>
										<%-- <tr>
											<td id="editVerifyBillPay_firstAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td id="editVerifyBillPay_firstAddressValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street" />
											<ffi:getProperty name="editPayment.payee" property="Street" /></td></td>
											<td id="editVerifyBillPay_payDateLabel" class="columndata tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_76"/></span></div>
											</td>
											<td id="editVerifyBillPay_payDateValue" class="columndata" colspan="2">
											<ffi:getProperty name="editPayment" property="PayDate" /></td>
										</tr> --%>
										<%-- <tr>
											<td id="editVerifyBillPay_secondAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="editVerifyBillPay_secondAddressValue" class="columndata" colspan="2">
											><ffi:getProperty name="Payee" property="Street2" /></td>
											<ffi:getProperty name="editPayment.payee" property="Street2" /></td>
											<td id="editVerifyBillPay_deliverByDateLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_deliver_by"/></span></div>
											</td>
											<td id="editVerifyBillPay_deliverByDateValue" class="columndata" colspan="2" valign="middle">
												<ffi:setProperty name="EditPayment" property="DateFormat" value="${UserLocale.DateFormat}" />
												<ffi:getProperty name="EditPayment" property="DeliverByDate"/>												
												<s:property value="%{editPayment.deliverByDate}"/>				
											</td>											
										</tr> --%>
										<%-- <tr>
											<td id="editVerifyBillPay_thirdAddressLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="editVerifyBillPay_thirdAddressValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Street3" /></td> 
											<ffi:getProperty name="editPayment.payee" property="Street3" /></td>
											<td id="editVerifyBillPay_recurringLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_351"/></span></div>
											</td>
											<td id="editVerifyBillPay_recurringValue" class="columndata" colspan="2" valign="middle">												
												<ffi:getProperty name="editPayment" property="Frequency" />
											</td>
										</tr> --%>
										<%-- <tr>
											<td id="editVerifyBillPay_cityLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td id="editVerifyBillPay_cityValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="City" /></td>
											<ffi:getProperty name="editPayment.payee" property="City" /></td>
											<td id="editVerifyBillPay_frequencyLabel" valign="middle" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_321"/></span>
												</div>
											</td>
											<td id="editVerifyBillPay_frequencyValue" class="columndata" colspan="2" valign="middle">
												<ffi:cinclude value1="${OpenEnded}" value2="true">
 													<s:text name="jsp.default_446"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals">
 													<ffi:getProperty name="EditPayment" property="NumberPayments" />
												</ffi:cinclude>
												<s:if test="%{editPayment.openEnded}">
													<s:text name="jsp.default_446"/>
												</s:if>
												<s:else>
													<s:property value="%{editPayment.NumberPayments}"/>
												</s:else>
											</td>
										</tr> --%>
										<%-- <tr>
											<td id="editVerifyBillPay_stateLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="editVerifyBillPay_stateValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="Payee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{editPayment.payee.stateDisplayName}"/>
											</td>
											<td id="editVerifyBillPay_memoLabel" valign="middle" class="tbrd_l"><div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div></td>
											<td id="editVerifyBillPay_memoValue" colspan="2" valign="middle" class="columndata">
											<ffi:getProperty name="editPayment" property="memo" /></td>
											<ffi:cinclude value1="${EditPayment.REGISTER_CATEGORY_ID}" value2="">
												<td id="editVerifyBillPay_memoLabel" valign="middle" class="tbrd_l"><div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div></td>
												<td id="editVerifyBillPay_memoValue" colspan="2" valign="middle" class="columndata"><ffi:getProperty name="EditPayment" property="Memo" /></td>
											</ffi:cinclude>
											<ffi:cinclude value1="${EditPayment.REGISTER_CATEGORY_ID}" value2="" operator="notEquals">
												<td id="editVerifyBillPay_registerCategoryLabel" class="columndata tbrd_l">
													<div align="right">
														<span class="sectionsubhead"><s:text name="jsp.billpay_89"/></span>
													</div>
												</td>
												<td id="editVerifyBillPay_registerCategoryValue" colspan="2" class="columndata">
													<ffi:setProperty name="RegisterCategories" property="Filter" value="All"/>
													<ffi:list collection="RegisterCategories" items="Category">
														<ffi:cinclude value1="${Category.Id}" value2="${EditPayment.REGISTER_CATEGORY_ID}">
															<ffi:setProperty name="RegisterCategories" property="Current" value="${Category.Id}"/>
															<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="">
																<ffi:getProperty name="Category" property="Name"/>
															</ffi:cinclude>
															<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="" operator="notEquals">
																<ffi:getProperty name="RegisterCategories" property="ParentName"/>: <ffi:getProperty name="Category" property="Name"/>
															</ffi:cinclude>
														</ffi:cinclude>
													</ffi:list>
												</td>
											</ffi:cinclude>										
											
										</tr> --%>
										<%-- <tr>
											<td id="editVerifyBillPay_zipCodeLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td id="editVerifyBillPay_zipCodeValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ZipCode" /></td>
											<ffi:getProperty name="editPayment.payee" property="ZipCode" /></td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr> --%>
										<%-- <tr>
											<td id="editVerifyBillPay_countryLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_115"/></span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" />
											<td id="editVerifyBillPay_countryValue" class="columndata" colspan="2">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{editPayment.payee.countryDisplayName}"/></td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr> --%>
										<%-- <tr>
											<td id="editVerifyBillPay_contactLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="editVerifyBillPay_contactValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="ContactName" /></td>
											<ffi:getProperty name="editPayment.payee" property="ContactName" /></td>
											<td valign="middle" class="tbrd_l">&nbsp;
											</td>
											<td class="columndata" colspan="2" valign="middle"></td>
										</tr>
										<tr>
											<td id="editVerifyBillPay_phoneLabel" class="ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_83"/></span></div>
											</td>
											<td id="editVerifyBillPay_phoneValue" class="columndata" colspan="2">
											<ffi:getProperty name="Payee" property="Phone" /></td>
											<ffi:getProperty name="editPayment.payee" property="Phone" /></td>
											<td class="columndata tbrd_l">&nbsp;</td>
											<td></td>
											<td></td>
										</tr> --%>
                                       <%--  <ffi:cinclude value1="${EditPayment.AmountValue.CurrencyCode}" value2="${EditPayment.Account.CurrencyCode}" operator="notEquals" >
                                        <tr>
											<td id="editVerifyBillPay_messageIndicator" class="required" colspan="6">
                                                <br><div align="center">&#8776; <s:text name="jsp.default_241"/></div>
                                            </td>
                                            <td></td>
										</tr>
                                        </ffi:cinclude> --%>
                                          <%-- <s:if test="%{editPayment.AmtCurrency!=editPayment.Account.CurrencyCode}">
									    	<tr>
									        <td class="required" colspan="4">
									           <div align="right">&#8776; <s:text name="jsp.default_241"/></div>
									        </td>
									    </tr>
									    </s:if> --%>
                                        <%-- <tr>
											<td colspan="6"><div align="center">
													<br>
													<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
													All topics subscribed here can be found in pendingTransactionPortal.js
													<s:if test="%{isPortalPage=='true'}">
														<sj:a id="cancelBillpaySend" 
											                button="true" 
															onClickTopics="cancelPendingBillPaymentsForm"
											        	><s:text name="jsp.default_82"/></sj:a>
											        	
				                        				<sj:a id="backInitiateEditBillpayId" 
						            						button="true" 
															onClickTopics="backToBillPayEditFormPortalPage,hideOperationResult"
						        						><s:text name="jsp.default_57"/></sj:a>
						        						<sj:a 
															id="sendSingleBillpaySubmit"
															formIds="BillpayEditSend"
							                                targets="confirmDiv" 
							                                button="true"
							                                onSuccessTopics="sendBillpayPortalFormSuccess"
							                                onErrorTopics="errorSendBillpayPortalForm"
							                           ><s:text name="jsp.billpay_69"/></sj:a>
							                           <s:hidden name="fromPortalPage" value="true"></s:hidden>
							                         </s:if>
							                         <s:else>
													</ffi:cinclude>
													<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals">
														<sj:a id="cancelBillpaySend" 
											                button="true" 
															onClickTopics="cancelBillpayForm"
											        	><s:text name="jsp.default_82"/></sj:a>
											        	
				                        				<sj:a id="backInitiateEditBillpayId" 
						            						button="true" 
															onClickTopics="backToBillpayInput,hideOperationResult"
						        						><s:text name="jsp.default_57"/></sj:a>
						        						
														<sj:a 
															id="sendSingleBillpaySubmit"
															formIds="BillpayEditSend"
							                                targets="confirmDiv" 
							                                button="true" 
							                                onBeforeTopics="beforeSendBillpayForm"
							                                onSuccessTopics="sendBillpayFormSuccess"
							                                onErrorTopics="errorSendBillpayForm"
							                                onCompleteTopics="sendBillpayFormComplete"
				                        				><s:text name="jsp.billpay_69"/></sj:a>
			                        				</ffi:cinclude>
													</s:else>
											</div></td>

											<td></td>
										</tr> --%>
									</table>
							
						</td>
						<td></td>
					</tr>
				</table>
<s:if test="%{editPayment.AmtCurrency!=editPayment.Account.CurrencyCode}">
<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="btn-row">
	<%-- <ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals"> --%>
	<%-- All topics subscribed here can be found in pendingTransactionPortal.js --%>
	<s:if test="%{isPortalPage=='true'}">
		<sj:a id="cancelBillpaySend" 
               button="true" 
			onClickTopics="cancelPendingBillPaymentsForm"
       	><s:text name="jsp.default_82"/></sj:a>
       	
                				<sj:a id="backInitiateEditBillpayId" 
      						button="true" 
			onClickTopics="backToBillPayEditFormPortalPage,hideOperationResult"
  						><s:text name="jsp.default_57"/></sj:a>
  						<sj:a 
			id="sendSingleBillpaySubmit"
			formIds="BillpayEditSend"
                           targets="PendingTransactionWizardTabs #confirmDiv" 
                           button="true"
                           onSuccessTopics="sendBillpayPortalFormSuccess"
                           onErrorTopics="errorSendBillpayPortalForm"
                      ><s:text name="jsp.billpay_69"/></sj:a>
                      <s:hidden name="fromPortalPage" value="true"></s:hidden>
                    </s:if>
                    <s:else>
	<%-- </ffi:cinclude>
	<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="notEquals"> --%>
		<sj:a id="cancelBillpaySend" 
               button="true" 
			onClickTopics="cancelBillpayForm"
       	><s:text name="jsp.default_82"/></sj:a>
       	
                				<sj:a id="backInitiateEditBillpayId" 
      						button="true" 
			onClickTopics="backToBillpayInput,hideOperationResult"
  						><s:text name="jsp.default_57"/></sj:a>
  						
		<sj:a 
			id="sendSingleBillpaySubmit"
			formIds="BillpayEditSend"
                           targets="confirmDiv" 
                           button="true" 
                           onBeforeTopics="beforeSendBillpayForm"
                           onSuccessTopics="sendBillpayFormSuccess"
                           onErrorTopics="errorSendBillpayForm"
                           onCompleteTopics="sendBillpayFormComplete"
                				><s:text name="jsp.billpay_69"/></sj:a>
               				<%-- </ffi:cinclude> --%>
	</s:else>
</div>
</div>
</s:form>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>