<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_29')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<div class="approvalDialogHt">
<ffi:setProperty name='PageText' value=''/>
<ffi:help id="payments_billpayview" className="moduleHelpClass"/>
<s:if test="%{#session.SecureUser.AppType=='Business'}">
<s:include value="inc/include-view-transaction-details-constants.jsp"/>
</s:if>
<form action="" method="post" name="FormName">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="Refresh" value="true">
<div  class="blockHead toggleClick expandArrow">
<s:text name="jsp.billpay.payee_summary" />: <s:property value="%{payment.payee.name}"/> (<s:property value="%{payment.payee.nickName}"/> - <s:property value="%{payment.payee.UserAccountNumber}"/>)
<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_314"/>:</span>
				<span class="columndata" >
					<s:property value="%{payment.payee.name}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
				<span class="columndata" >
					<s:property value="%{payment.payee.nickName}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_15"/>:</span>
				<span class="columndata" >
					<s:property value="%{payment.payee.UserAccountNumber}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_36"/>:</span>
				<span class="columndata" >
					<s:property value="%{payment.payee.contactName}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_83"/>:</span>
				<span class="columndata" >
				<s:property value="%{payment.payee.phone}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="columndata sectionsubhead sectionLabel" ><s:text name="jsp.default_36"/>:</span>
				<span class="columndata" >
					<s:property value="%{payment.payee.street}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_37"/>:</span>
				<span class="columndata" >
					<s:property value="%{payment.payee.street2}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_38"/>:</span>
				<span class="columndata"  >
					<s:property value="%{payment.payee.street3}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_101"/>:</span>
				<span class="columndata"  >
					<s:property value="%{payment.payee.city}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_104"/>:</span>
				<span   class="columndata">
						<s:property value="%{payment.Payee.StateName}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_473"/>:</span>
				<span  class="columndata">
					<s:property value="%{payment.payee.zipCode}"/>
				</span>									
			</div>								
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel" >
													<s:text name="jsp.default_115"/> :
											</span>
				<span class="columndata"><s:property value="%{payment.Payee.CountryName}"/></span>
			</div>	
	</div>
</div></div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart--><s:text name="jsp.transaction.summary.label" /><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_416"/>:</span>										
				<span class="columndata" >
					<s:property value="%{payment.templateName}"/>
				</span>
			</div>
			<div class="inlineBlock">	
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_account"/>:</span>
				<span class="columndata" ><s:property value="%{payment.account.accountDisplayText}"/></span>			
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
				<span class="columndata" >
					<s:property value="%{payment.AmountValue.CurrencyStringNoSymbol}"/>
					<s:property value="%{payment.AmountValue.CurrencyCode}"/>
					 <%-- <ffi:getProperty name="payment" property="AmountValue.CurrencyStringNoSymbol" />
					 <ffi:getProperty name="payment" property="AmountValue.CurrencyCode"/> --%>
				</span>	
			</div>
			<div class="inlineBlock">
				<s:set var="pmtCurrency" value="#session.payment.AmtCurrency" scope="request"></s:set>
				<s:set var="acctCurrency" value="#session.payment.Account.CurrencyCode" scope="request"></s:set>
				<span class="sectionsubhead sectionLabel"> <!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->  
								        	    <s:if test="%{payment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else> <s:text name="jsp.billpay_40"/> : </s:else>
											</span>
											<span class="columndata" >
											<!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
											 <s:if test="%{payment.convertedAmount!=''}">
							        	    	<!-- do nothing -->
							        	    </s:if>
							        	    <s:else>
							        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
							        	    </s:else>
											</span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_351"/>:</span>
				<span class="columndata" >
					<s:property value="%{payment.frequency}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<s:if test="%{payment.openEnded}">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_446"/></span>
				</s:if>
				<s:else>
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/>#:</span>
					<span class="columndata" ><s:property value="%{payment.NumberPayments}"/></span>
				</s:else>
			</div>
		</div>
		
		<div class="blockRow">	
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
				<span class="columndata" >
					<s:property value="%{payment.memo}"/>
			</span>
		</div>
	</div>
</div>
		<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <tr>
			<td></td>
			<td class="columndata ltrow2_color">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td class="ltrow2_color" alt="" height="1" ></td>
					</tr>
					<tr>
						<td class="tbrd_b ltrow2_color" >
						    <span class="sectionhead">&gt; 
						   		<s:text name="jsp.billpay.viewTemplateDetails.heading"/>
						    <br></span>
						</td>
					</tr>
					<tr>
						<td class="ltrow2_color" alt="" height="1" ></td>
					</tr>
				</table>
			</td>
			<td align="right"><br><br>
			</td>
		    </tr>
		    <tr>
			<td></td>
			<td class="columndata ltrow2_color">
<table width="720" border="0" cellspacing="0" cellpadding="3">
    
    <tr>
	<td class='<ffi:getProperty name="payment_details_background_color"/>' colspan="6" alt="" height="1" ></td>
    </tr>
   
    	template
	    <tr>
	    	<td class="sectionsubhead" align="right" colspan="2">&nbsp;</td>
	    	<td class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.default_416"/></td>
	    	<td class="columndata"><ffi:getProperty name="payment" property="templateName"/></td>
	    </tr>
    
    <tr>
	<div align="center">
			include page header
			<div align="left">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="columndata ltrow2_color">
							
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
                                        	<td class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td class="columndata" >
												<s:property value="%{payment.payee.nickName}"/>
											     <ffi:getProperty name="payment" property="Payee.NickName" />
											</td>
											<td class="columndata tbrd_l">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_416"/></span></div>												
											</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="payment" property="TemplateName" />
											<s:property value="%{payment.templateName}"/>
											</td>
										</tr>

										<tr>
											<td class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td class="columndata" >
											<s:property value="%{payment.payee.name}"/>
											    <ffi:getProperty name="payment" property="Payee.Name" />
											</td>
											<td class="tbrd_l">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>												
											</td>
											<td class="columndata" colspan="2">
											<s:property value="%{payment.AmountValue.CurrencyStringNoSymbol}"/>
											<s:property value="%{payment.AmountValue.CurrencyCode}"/>
												 <ffi:getProperty name="payment" property="AmountValue.CurrencyStringNoSymbol" />
												 <ffi:getProperty name="payment" property="AmountValue.CurrencyCode"/>
											</td>											
										</tr>
										<tr>
											<td class="columndata" colspan="2">
                                               <div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
                                            </td>
											<td class="columndata" >
											<s:property value="%{payment.payee.UserAccountNumber}"/>
												<ffi:getProperty name="payment" property="Payee.UserAccountNumber" />
											</td>
											<s:set var="pmtCurrency" value="#session.payment.AmtCurrency" scope="request"></s:set>
											<s:set var="acctCurrency" value="#session.payment.Account.CurrencyCode" scope="request"></s:set>
											<td class="tbrd_l">
											<div align="right">
								        	    <span class="sectionsubhead">
								        	    <!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->  
								        	    <s:if test="%{payment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else> <s:text name="jsp.billpay_40"/> </s:else>
								        	    </span></div>
											</td>
											<td class="columndata" colspan="2">
											<!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
											 <s:if test="%{payment.convertedAmount!=''}">
							        	    	<!-- do nothing -->
							        	    </s:if>
							        	    <s:else>
							        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
							        	    </s:else>
											</td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/> </span></div>
											</td>
											<td class="columndata" >
												<ffi:getProperty name="payment" property="Payee.Street" />
												<s:property value="%{payment.payee.street}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_account"/></span></div>
												
											</td>
											<td class="columndata" colspan="2"><s:property value="#attr._fromAccount"/>
											<s:property value="%{payment.account.accountDisplayText}"/></td>												
										</tr>
										<tr>
											<td class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_37"/></span></div>
											</td>
											<td class="columndata" >
											<ffi:getProperty name="payment" property="Payee.Street2" />
											<s:property value="%{payment.payee.street2}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
												<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_351"/> </span></div>
												</td>
											<td class="columndata" colspan="2">
											<ffi:getProperty name="payment" property="Frequency" />
											<s:property value="%{payment.frequency}"/>
											</td>

										</tr>
										<tr>
											<td valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_38"/></span></div>
											</td>
											<td class="columndata"  valign="middle">
												<ffi:getProperty name="payment" property="Payee.Street3" />
												<s:property value="%{payment.payee.street3}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_321"/></span></div>
											</td>
											<td class="columndata" colspan="2">
												<ffi:cinclude value1="${OpenEnded}" value2="true">
 													<s:text name="jsp.default_446"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals">
 													<ffi:getProperty name="payment" property="NumberPayments" />
												</ffi:cinclude>
												
												<s:if test="%{payment.openEnded}">
													<s:text name="jsp.default_446"/>
												</s:if>
												<s:else>
													<s:property value="%{payment.NumberPayments}"/>
												</s:else>
												
											</td>

											</tr>
										<tr>
											<td valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_101"/></span>
												</div>
											</td>
											<td class="columndata"  valign="middle">
												<ffi:getProperty name="payment" property="Payee.City" />
												<s:property value="%{payment.payee.city}"/>
											</td>
											<td class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
											</td>
											<td class="columndata" colspan="2">
											<s:property value="%{payment.memo}"/>
												<ffi:getProperty name="payment" property="Memo" />
											</td>

											</tr>
										<tr>
											<td valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td  valign="middle" class="columndata">
													<s:property value="%{payment.Payee.StateName}"/>
											</td>
											<td class="tbrd_l">
												<div align="right"></div>
											</td>
											<td class="columndata" colspan="2">
											</td>
											<ffi:cinclude value1="${payment.REGISTER_CATEGORY_ID}" value2="">
												<td class="columndata tbrd_l">&nbsp;</td>
												<td colspan="2"></td>
											</ffi:cinclude>

											<ffi:cinclude value1="${payment.REGISTER_CATEGORY_ID}" value2="" operator="notEquals">
											<td class="columndata tbrd_l">
												<div align="right">
														<span class="sectionsubhead"><s:text name="jsp.billpay_89"/></span>
													</div>
											</td>
											<td class="columndata" colspan="2">
												<ffi:setProperty name="RegisterCategories" property="Filter" value="All"/>
												<ffi:list collection="RegisterCategories" items="Category">
													<ffi:cinclude value1="${Category.Id}" value2="${payment.REGISTER_CATEGORY_ID}">
														<ffi:setProperty name="RegisterCategories" property="Current" value="${Category.Id}"/>
														<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="">
															<ffi:getProperty name="Category" property="Name"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${RegisterCategories.ParentName}" value2="" operator="notEquals">
															<ffi:getProperty name="RegisterCategories" property="ParentName"/>: <ffi:getProperty name="Category" property="Name"/>
														</ffi:cinclude>
													</ffi:cinclude>
												</ffi:list>
											</td>
											</ffi:cinclude>
										</tr>
										<tr>											
											<td valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td  class="columndata">
											<ffi:getProperty name="payment" property="Payee.ZipCode" />
											<s:property value="%{payment.payee.zipCode}"/>
											</td>											
											<td class="tbrd_l">
												&nbsp;
											</td>											
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>		
											<td valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_115"/> </span></div>
											</td>
											<td class="columndata" valign="middle"><s:property value="%{payment.Payee.CountryName}"/></td>
											<td class="tbrd_l">
												&nbsp;
											</td>											
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>											
											<td valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_36"/> </span></div>
											</td>
											<td class="columndata" valign="middle">
											<ffi:getProperty name="payment" property="Payee.ContactName" />
											<s:property value="%{payment.payee.contactName}"/>
											</td>
											<td class="tbrd_l">												
											</td>
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>											
											<td valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_83"/></span></div></td>
											<td class="columndata" valign="middle">
											<ffi:getProperty name="payment" property="Payee.Phone" />
											<s:property value="%{payment.payee.phone}"/>
											</td>
											<td class="tbrd_l">												
											</td>
											<td ></td>											
										</tr>
                                         <s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
									    	<tr>
									        <td class="required" colspan="4">
									            <div align="right">&#8776; <s:text name="jsp.default_241"/></div>
									        </td>
									    </tr>
									    </s:if>
                                    </table>
							
						</td>
						<td align="right"><br>
							<br>
						</td>
					</tr>
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
							<tr>
								<td colspan="6">
									<!-- include batch Transaction History -->
									<ffi:include page="${PagesPath}common/include-view-transaction-history.jsp" />
								</td>
							</tr>
						</ffi:cinclude>
				</table>
			</div>
		</div>
			<td></td>
		    </tr>
		</table>
 --%>
<s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
	<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="ffivisible" style="height:150px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
	<sj:a id="done"
         button="true" 
onClickTopics="closeDialog"
 	><s:text name="jsp.default_175"/></sj:a>
    </div>
</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>