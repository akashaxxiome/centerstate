<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:help id="payments_billpayaddpayeeconfirm" className="moduleHelpClass"/>	
<s:form id="BillpayPayeeNewSend" namespace="/pages/jsp/billpay" validate="false" action="addBillpayPayeeAction_execute" method="post" name="BillpayPayeeNewSend" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper" role="form" aria-labeledby="summary">
<div class="leftPaneInnerWrapper label130">
	<div class="header"><h2 id="summary"><s:text name="jsp.billpay.payee_summary" /></h2></div>
	<div class="leftPaneInnerBox summaryBlock"  style="padding: 15px 5px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyPayeeNameLabel"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue"  style="width:50%"><s:property value="%{name}"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyPayeeContactLabel"><s:text name="jsp.default_19"/>:</span>
				<span class="inlineSection floatleft labelValue" style="width:50%"><s:property value="%{userAccountNumber}"/></span>
			</div>
	</div>
</div>
</div>
<div class="confirmPageDetails label130">
<div  class="blockHead"><s:text name="jsp.billpay.payee.info"/></div>
   		<div class="blockContent">
   			<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeNickNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_294"/>:</span>
				<span id="verifyPayeeNickNameValue" class="columndata"><s:property value="%{nickName}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeContactLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_36"/>:</span>
				<span id="verifyPayeeContactValue" class="columndata"><s:property value="%{contactName}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeePhoneLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_37"/>:</span>
				<span id="verifyPayeePhoneValue" class="columndata"><s:property value="%{phone}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeAddress1Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_36"/>:</span>
				<span id="verifyPayeeAddress1Value" class="columndata"><s:property value="%{street}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeAddress2Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_37"/>:</span>
				<span id="verifyPayeeAddress2Value" class="columndata"><s:property value="%{street2}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeAddress3Label" class="sectionsubhead sectionLabel"><s:text name="jsp.default_38"/>:</span>
				<span id="verifyPayeeAddress3Value" class="columndata"><s:property value="%{street3}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeCityLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_101"/>:</span>
				<span id="verifyPayeeCityValue" class="columndata"><s:property value="%{city}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeStateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_104"/>:</span>
				<span id="verifyPayeeStateValue" class="columndata"><s:property value="%{stateDisplayName}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeZipLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_473"/>:</span>
				<span id="verifyPayeeZipValue" class="columndata"><s:property value="%{zipCode}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeCountryLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_115"/>:</span>
				<span id="verifyPayeeCountryValue" class="columndata"><s:property value="%{countryDisplayName}"/></span>
			</div>
		</div>
   		</div>
   		<div  class="blockHead"><s:text name="jsp.billpay.payee.bank.info"/></div>
   		<div class="blockContent">
   			<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeRountingNoLabel" class="sectionsubhead sectionLabel"><s:property value="%{bankIdentifierDisplayText}"/>
				<s:hidden value="%{bankIdentifierDisplayText}" name="bankIdentifierDisplayText"></s:hidden>:</span>
				<span id="verifyPayeeRountingNoValue" class="columndata"><s:property value="%{payeeRoute.bankIdentifier}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeBankAccountNoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_26"/>:</span>
				<span id="verifyPayeeBankAccountNoValue" class="columndata"><ffi:getProperty name="AddPayee" property="PayeeRoute.AccountID" /><%-- <s:property value="%{payeeRoute.accountID}"/> --%></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeBankAccountTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_27"/>:</span>
				<span id="verifyPayeeBankAccountTypeValue" class="columndata"><s:property value="%{accountTypeSelectList[payeeRoute.acctType]}"/></span>
			</div>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
				<div class="inlineBlock">
					<span id="verifyPayeeBankAccountCurrencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_25"/>:</span>
					<span id="verifyPayeeBankAccountCurrencyValue" class="columndata"><s:property value="%{payeeRoute.currencyCode}"/></span>
				</div>
			</ffi:cinclude>
		</div>
   		</div>
 <div class="btn-row">
 <sj:a id="cancelBillpayPayeeConfirm" 
            button="true" 
            summaryDivId="summary" buttonType="done"
onClickTopics="showSummary,cancelBillpayForm,cancelPayeeLoadForm"
    	><s:text name="jsp.default_82"/></sj:a>

<sj:a id="backInitiateBillpayPayeeFormButton" 
   						button="true" 
onClickTopics="backToBillpayInput"
					><s:text name="jsp.default_57"/></sj:a>

<sj:a 
	id="sendSingleBillpayPayeeSubmit"
	formIds="BillpayPayeeNewSend"
                         targets="confirmDiv" 
                         button="true" 
                         onBeforeTopics="beforeSendBillpayForm"
                         onSuccessTopics="sendBillpayPayeeFormSuccess"
                         onErrorTopics="errorSendBillpayForm"
                         onCompleteTopics="sendBillpayPayeeFormComplete"
              				><s:text name="jsp.default_107" /></sj:a>
</div>
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
								
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_15"/><br>
												</span></td>
										</tr>
											<tr>
												<td id="verifyPayeeInfoMessage" class="columndata ltrow2_color" colspan="6">
													<div align="center">
														<span class="sectionsubhead"><s:text name="jsp.billpay_113"/><br>
															<br>
														</span></div>
												</td>
											</tr>
										<tr>
											<td class="sectionsubhead"></td>
											<td></td>
											<td></td>
											<td><br>
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
											<td id="verifyPayeeNameLabel" class="sectionsubhead">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="verifyPayeeNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Name" />
											<s:property value="%{name}"/>
											</td>
											<td id="verifyPayeeAddress1Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/></span></div>
											</td>
											<td id="verifyPayeeAddress1Value" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Street" />
											<s:property value="%{street}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyPayeeNickNameLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_294"/></span></div>
											</td>
											<td id="verifyPayeeNickNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="NickName" />
											<s:property value="%{nickName}"/>
											</td>
											<td id="verifyPayeeAddress2Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="verifyPayeeAddress2Value" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Street2" />
											<s:property value="%{street2}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyPayeeContactLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_36"/></span></div>
											</td>
											<td id="verifyPayeeContactValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="ContactName" />
											<s:property value="%{contactName}"/>
											</td>
											<td id="verifyPayeeAddress3Label" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="verifyPayeeAddress3Value" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Street3" />
											<s:property value="%{street3}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyPayeePhoneLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_37"/></span></div>
											</td>
											<td id="verifyPayeePhoneValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="Phone" />
											<s:property value="%{phone}"/></td>
											<td id="verifyPayeeCityLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_101"/></span></div>
											</td>
											<td id="verifyPayeeCityValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="City" />
											<s:property value="%{city}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyPayeeAccountLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_19"/></span></div>
											</td>
											<td id="verifyPayeeAccountValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="UserAccountNumber" />
											<s:property value="%{userAccountNumber}"/>
											</td>
											<td id="verifyPayeeStateLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_104"/></span></div>
											</td>
											<td id="verifyPayeeStateValue" class="columndata" colspan="2">
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
													<ffi:getProperty name="AddPayee" property="State"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
													<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
												</ffi:cinclude>
												<s:property value="%{stateDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyPayeeRountingNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead">
													<ffi:getProperty name="TempBankIdentifierDisplayText"/>
													<s:property value="%{bankIdentifierDisplayText}"/>
													<s:hidden value="%{bankIdentifierDisplayText}" name="bankIdentifierDisplayText"></s:hidden>
													</span></div>
											</td>
											<td id="verifyPayeeRountingNoValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="PayeeRoute.BankIdentifier" />
											<s:property value="%{payeeRoute.bankIdentifier}"/>
											</td>
											<td id="verifyPayeeZipLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><span><s:text name="jsp.default_473"/></span></span></div>
											</td>
											<td id="verifyPayeeZipValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="ZipCode" />
											<s:property value="%{zipCode}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyPayeeBankAccountNoLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_26"/></span></div>
											</td>
											<td id="verifyPayeeBankAccountNoValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="PayeeRoute.AccountID" />
											<s:property value="%{payeeRoute.accountID}"/>
											</td>
											<td id="verifyPayeeCountryLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_115"/></span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${AddPayee.Country}" />
											<td id="verifyPayeeCountryValue" class="columndata" colspan="2">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{countryDisplayName}"/>
											</td>
										</tr>
										<tr>
											<td id="verifyPayeeBankAccountTypeLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_27"/></span></div>
											</td>
											<ffi:object id="GetTypeName" name="com.ffusion.tasks.util.Resource" />
											<ffi:setProperty name="GetTypeName" property="ResourceFilename" value="com.ffusion.beansresources.accounts.resources" />
											<ffi:setProperty name="GetTypeName" property="ResourceID" value="${AddPayee.PayeeRoute.AcctType}" />
											<ffi:process name="GetTypeName" />
											<td id="verifyPayeeBankAccountTypeValue" class="columndata" colspan="2">
											<ffi:getProperty name="GetTypeName" property="Resource" />
											<s:property value="%{accountTypeSelectList[payeeRoute.acctType]}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										 <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS %>">
										<tr>
											<td id="verifyPayeeBankAccountCurrencyLabel" class="columndata ltrow2_color">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_25"/></span></div>
											</td>
											<td id="verifyPayeeBankAccountCurrencyValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayee" property="PayeeRoute.CurrencyCode" />
											<s:property value="%{payeeRoute.currencyCode}"/>
											</td>
											<td class="tbrd_l">
												
											</td>
											<td class="columndata" colspan="2"></td>											
										
										</tr>
										</ffi:cinclude>
										<tr>
											<td class="columndata ltrow2_color">
												<div align="right"></div>
											</td>
											<td colspan="4"><div align="center">
													<br>
													<sj:a id="cancelBillpayPayeeConfirm" 
										                button="true" 
														onClickTopics="cancelBillpayForm"
										        	><s:text name="jsp.default_82"/></sj:a>
										        	
										        	<sj:a id="backInitiateBillpayPayeeFormButton" 
					            						button="true" 
														onClickTopics="backToBillpayInput"
					        						><s:text name="jsp.default_57"/></sj:a>
	                        				
													<sj:a 
														id="sendSingleBillpayPayeeSubmit"
														formIds="BillpayPayeeNewSend"
						                                targets="confirmDiv" 
						                                button="true" 
						                                onBeforeTopics="beforeSendBillpayForm"
						                                onSuccessTopics="sendBillpayPayeeFormSuccess"
						                                onErrorTopics="errorSendBillpayForm"
						                                onCompleteTopics="sendBillpayPayeeFormComplete"
			                        				><s:text name="jsp.billpay_17"/></sj:a>
													</div></td>
											<td></td>
										</tr>
									</table>
							
						</td>
						<td></td>
					</tr>
				</table> --%>
</div>
</s:form>