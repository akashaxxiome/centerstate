<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:set var="tmpI18nStr" value="%{getText('jsp.billpay_29')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<div class="approvalDialogHt">
<ffi:setProperty name='PageText' value=''/>
<ffi:help id="payments_billpayview" className="moduleHelpClass"/>
<div class="templateConfirmationWrapper">
		<div class="confirmationDetails">
			<span id="viewBillPay_referenceLabel" class="sectionLabel"><s:text name="jsp.billpay_referencenum"/></span>
	        <span id="" class="columndata"><s:property value="%{payment.TrackingID}"/></span>
		</div>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.status.label" /> :</div>
<div class="blockContent">
		<%-- <span id="viewTransfer_statusLabelId" class="sectionLabel"><s:text name="jsp.default_388" />: </span> --%>
		<s:if test="%{payment.StatusName!='Scheduled'}">
				<s:set var="PMS_FAILED_TO_TRANSFER" value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FAILED_TO_PAY==payment.status}" />
				<s:set var="PMS_INSUFFICIENT_FUNDS"
					value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_INSUFFICIENT_FUNDS==payment.status}" />
				<s:set var="PMS_BPW_LIMITCHECK_FAILED"
					value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_BPW_LIMITCHECK_FAILED==payment.status}" />
				<s:set var="PMS_FAILED_APPROVAL"
					value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FAILED_APPROVAL==payment.status}" />
				
				<s:if test="%{#PMS_FAILED_TO_TRANSFER}">
						<s:set var="HighlightStatus" value="true" />
				</s:if>
				<s:elseif test="%{#PMS_INSUFFICIENT_FUNDS}">
						<s:set var="HighlightStatus" value="true" />
				</s:elseif>
				<s:elseif test="%{#PMS_BPW_LIMITCHECK_FAILED}">
						<s:set var="HighlightStatus" value="true" />
				</s:elseif>
				<s:elseif test="%{#PMS_FAILED_APPROVAL}">
						<s:set var="HighlightStatus" value="true" />
				</s:elseif>
				
				<s:set var="PMS_SCHEDULED"
					value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_SCHEDULED==payment.status}" />
					
				<s:set var="PMS_PENDING_APPROVAL"
						value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_PENDING_APPROVAL==payment.status}" />
						
				<s:set var="PMS_FUNDS_ALLOCATED"
						value="%{@com.ffusion.beans.billpay.PaymentStatus@PMS_FUNDS_ALLOCATED==payment.status}" />
						
				<s:if test="%{#HighlightStatus==true}">
					<div class="blockRow failed">
						<span id="viewTransfer_statusValueId">
							<span class="sapUiIconCls icon-decline"></span>
							<s:if test="%{payment.backendErrorDesc=='' || payment.backendErrorDesc==null }">
								<s:property	value="%{payment.statusName}" />
								</s:if> 
								<s:else>
								<s:property	value="%{payment.statusName}" /> - <s:property value="%{payment.backendErrorDesc}"/>
								</s:else>
						</span>
					</div>
				</s:if> 
				<s:else>
					<s:if test="%{#PMS_PENDING_APPROVAL}">
						<div class="blockRow pending"><span id="viewTransfer_statusValueId">
							<span class="sapUiIconCls icon-pending"></span>
							<s:property value="%{payment.statusName}" />
						</span></div>
					</s:if>
					<s:elseif test="%{#PMS_FUNDS_ALLOCATED}">
						<div class="blockRow pending"><span id="viewTransfer_statusValueId">
							<span class="sapUiIconCls icon-pending"></span>
							<s:text name="jsp.reports_660" />
						</span></div>
					</s:elseif>
					<s:else>
					<div class="blockRow completed"><span id="viewTransfer_statusValueId">
						<span class="sapUiIconCls icon-accept"></span>
						<s:property value="%{payment.statusName}" />
					</span></div>
					</s:else>
				</s:else>
		</s:if>
		<s:if test="%{payment.StatusName=='Scheduled'}">
			<div class="blockRow pending"><span id="viewTransfer_statusValueId">
				<span class="sapUiIconCls icon-future"></span>
				<s:property value="%{payment.statusName}" />
				</span></div>
		</s:if>
</div>
<%-- <ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
<div class="blockContent">
	<div class="blockRow">
		<span id="viewBillPay_statusLabel" class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
		<span id="viewBillPay_statusValue" class="columndata">
				<ffi:setProperty name="isStatusSet"  value="false"/>
				<ffi:cinclude value1="${isStatusSet}" value2="true"  operator="notEquals">
					<!--L10NStart--><s:text name="jsp.default_530"/><!--L10NEnd(pending)-->
				</ffi:cinclude>			
		</span>
	</div>
</div>
</ffi:cinclude> --%>
<ffi:setProperty name="viewPaymentDoneURL" value="${SecurePath}payments/billpay.jsp?Refresh=true" URLEncrypt="true"/>
<% if( session.getAttribute( "payment_details_post_jsp" ) != null ) { %>
    <ffi:setProperty name="viewPaymentDoneURL" value="${payment_details_post_jsp}"/>
<% } %>
<form action='<ffi:urlEncrypt url="${viewPaymentDoneURL}"/>' method="post" name="FormName">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div  class="blockHead toggleClick expandArrow"><s:text name="jsp.billpay.payee_summary" />: <s:property value="%{payment.payeeName}"/> (<s:property value="%{payment.PayeeNickName}"/> - <s:property value="%{payment.Payee.UserAccountNumber}"/>) <span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_payeeNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_payeename"/>:</span>
				<span id="viewBillPay_payeeNameValue" class="columndata"><s:property value="%{payment.payeeName}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_payeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
				<span id="viewBillPay_payeeNickNameValue" class="columndata"><s:property value="%{payment.PayeeNickName}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_payeeAccountLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_account"/>:</span>
				<span id="viewBillPay_payeeAccountValue" class="columndata"><s:property value="%{payment.Payee.UserAccountNumber}"/></span>
			</div>
			<div class="inlineBlock">
				 <span id="viewBillPay_firstAddressLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_20"/>:</span>
				<span id="viewBillPay_firstAddressValue" class="columndata"><s:property value="%{payment.Payee.Street}"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_secondAddressLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_21"/>:</span>
				<span id="viewBillPay_secondAddressValue" class="columndata"><s:property value="%{payment.Payee.Street2}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_thirdAddressLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_address3"/>:</span>
				<span id="viewBillPay_thirdAddressValue" class="columndata"><s:property value="%{payment.Payee.Street3}"/></span>
			</div>
		</div><div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_cityLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_city"/>:</span>
				<span id="viewBillPay_cityValue" class="columndata"><s:property value="%{payment.Payee.City}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_stateLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_state"/>:</span>
				<span id="viewBillPay_stateValue" class="columndata"><s:property value="%{payment.payee.stateDisplayName}"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_zipCodeLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_zippostalcode"/>:</span>
				<span id="viewBillPay_zipCodeValue" class="columndata"><s:property value="%{payment.Payee.ZipCode}"/></span>
	
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_countryLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_country"/>:</span>
				<span id="viewBillPay_countryValue" class="columndata"><s:property value="%{payment.payee.countryDisplayName}"/></span>
			</div>
		</div>
		<div class="blockRow">
				<span id="viewBillPay_phoneLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_83"/>:</span>
				<span id="viewBillPay_phoneValue" class="columndata"><s:property value="%{payment.Payee.Phone}"/></span>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_amountLabel" class="sectionsubhead sectionLabel " ><s:text name="jsp.billpay_amount"/>:</span>
				<span d="viewBillPay_amountValue" class="columndata"><s:property value="%{payment.AmountValue.CurrencyStringNoSymbol}"/> <s:property value="%{payment.AmtCurrency}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_debitAmountLabel" class="columndata ">
	        	    <!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
	        	    <s:if test="%{payment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else> <s:text name="jsp.billpay_40"/>:</s:else>
				</span>
				<span id="viewBillPay_debitAmountValue" class="columndata" id="estimatedAmount2">
							<!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
							<s:if test="%{payment.convertedAmount!=''}">
			        	    	<!-- do nothing -->
			        	    </s:if>
			        	    <s:else>
			        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
			        	    </s:else>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_fromAccountLabel" class="sectionsubhead sectionLabel " ><s:text name="jsp.billpay_account"/>:</span>
				<span id="viewBillPay_fromAccountValue" class="columndata"><s:property value="%{payment.account.accountDisplayText}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_dateLabel" class="sectionsubhead sectionLabel " ><s:text name="jsp.billpay_76"/>:</span>
				<span id="viewBillPay_dateValue" class="columndata"><s:property value="%{payment.PayDate}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_deliverByDateLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_deliver_by"/>:</span>
				<span id="viewBillPay_deliverByDateValue" class="columndata"><s:property value="%{payment.DeliverByDate}"/></span>	
			</div>
			
		</div>
		<s:set var="frequency" value="%{payment.Frequency}" />
		<s:if test="%{#frequency!=null}">
		<s:if test='%{recPaymentID != "null" && isRecModel =="true"}'>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
						<span id="viewBillPay_recurringLabel" class="sectionsubhead sectionLabel " ><s:text name="jsp.billpay_repeating"/>:</span>
						<span id="viewBillPay_recurringValue" class="columndata"><s:property value="%{payment.Frequency}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_frequencyLabel" class="sectionsubhead sectionLabel " ><s:text name="jsp.billpay_payments"/>:</span>
				<span id="viewBillPay_frequencyValue" class="columndata">
					<s:if test="%{payment.NumberPayments=='999'}">
						<!--L10NStart-->Unlimited<!--L10NEnd-->
					</s:if>
					<s:else>
						 <s:property value="%{payment.NumberPayments}"/>	
					</s:else>
				</span>
			</div>			
			
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_endDateLabel" class="sectionsubhead sectionLabel " ><s:text name="jsp.default.end.date"/>:</span>
				<span id="viewBillPay_endDateValue" class="columndata">
				<s:if test="%{payment.NumberPayments=='999'}">
					<s:text name="jsp.default.not.applicable"/>
				</s:if>
				<s:else>
					 <s:property value="%{payment.EndDate}"/>
				</s:else>				
				</span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_numberOfPendingPaymentsLabel" class="sectionsubhead sectionLabel " ><s:text name="jsp.default.future.payments"/>:</span>
				<span id="viewBillPay_numberOfPendingPaymentsValue" class="columndata">	
				
				<s:if test="%{payment.NumberPayments=='999'}">
					<s:text name="jsp.default.not.applicable"/>
				</s:if>
				<s:else>
					<s:property value="%{payment.NumberOfPendingPayments}"/>
				</s:else>
											
				</span>
			</div>			
					
		</div>
		</s:if>
		</s:if>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBillPay_submittedByLabel" class="sectionsubhead sectionLabel " ><s:text name="jsp.billpay_submittedby"/>:</span>
				<span id="viewBillPay_submittedByValue" class="columndata">
					<s:property value="%{payment.submittedByUserName}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="viewBillPay_memoLabel" class="sectionsubhead sectionLabel " ><s:text name="jsp.billpay_memo"/>:</span>
				<span id="viewBillPay_memoValue" class="columndata"><s:property value="%{payment.memo}"/></span>
			</div>
		</div>
	</div>
</div>
<%-- 		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <tr>
			<td></td>
			<td class="columndata ltrow2_color">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td class="ltrow2_color" background="/cb/web/multilang/grafx/payments/spacer.gif" alt="" height="1" ></td>
					</tr>
					<tr>
						<td class="tbrd_b ltrow2_color" >
						    <span class="sectionhead">&gt; 
						   		<s:text name="jsp.billpay_115"/>
						    <br></span>
						</td>
					</tr>
					<tr>
						<td class="ltrow2_color" background="/cb/web/multilang/grafx/payments/spacer.gif" alt="" height="1" ></td>
					</tr>
				</table>
			</td>
			<td></td>
		    </tr>
		    <tr>
			<td></td>
			<td class="columndata ltro w2_color">
			    <div align="left" >--%>
<%-- <ffi:setProperty name="viewPaymentDoneURL" value="${SecurePath}payments/billpay.jsp?Refresh=true" URLEncrypt="true"/>
<% if( session.getAttribute( "payment_details_post_jsp" ) != null ) { %>
    <ffi:setProperty name="viewPaymentDoneURL" value="${payment_details_post_jsp}"/>
<% } %> --%>

<%-- <table width="100%" border="0" cellspacing="0" cellpadding="3">
    <form action='<ffi:urlEncrypt url="${viewPaymentDoneURL}"/>' method="post" name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
    <tr>
	<td class='<ffi:getProperty name="payment_details_background_color"/>' colspan="6" background='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" ></td>
    </tr>
    <tr>
    <td id="viewBillPay_payeeNameLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_payeename"/></td>
	<td id="viewBillPay_payeeNameValue" class="columndata">
	<s:property value="%{payment.payeeName}"/>
	</td>
	<td id="viewBillPay_amountLabel" class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.billpay_amount"/></td>
	<td d="viewBillPay_amountValue" class="columndata">
	    <s:property value="%{payment.AmountValue.CurrencyStringNoSymbol}"/>
	    <s:property value="%{payment.AmtCurrency}"/>	
	</td>
    </tr>
    <tr>
	<td id="viewBillPay_payeeNickNameLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_75"/></td>
	<td id="viewBillPay_payeeNickNameValue" class="columndata">
	 <s:property value="%{payment.PayeeNickName}"/>	
	<td id="viewBillPay_debitAmountLabel" class="columndata tbrd_l">
	
	<div align="right">
        	     <span class="sectionsubhead">
        	    <!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
        	    <s:if test="%{payment.convertedAmount!=''}">
        	    	<!-- do nothing --> 
        	    </s:if>
        	    <s:else> <s:text name="jsp.billpay_40"/> </s:else>
        	    </span></div>
	</td>

	<td id="viewBillPay_debitAmountValue" class="columndata" colspan="2" id="estimatedAmount2">
				<!-- Reciprocating logic here, in case of converted Amount not present struts is unable to evaluate and thus goes as true -->
				<s:if test="%{payment.convertedAmount!=''}">
        	    	<!-- do nothing -->
        	    </s:if>
        	    <s:else>
        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
        	    </s:else>
	</td>
    
    </tr>
    <tr>
    <td id="viewBillPay_payeeAccountLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_account"/></td>
	<td id="viewBillPay_payeeAccountValue" class="columndata">
	 <s:property value="%{payment.Payee.UserAccountNumber}"/>	
	</td>
	<td id="viewBillPay_fromAccountLabel" class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.billpay_account"/></td>
	<td id="viewBillPay_fromAccountValue" class="columndata">
	<s:property value="%{payment.account.accountDisplayText}"/>
	</td>
    </tr>
    <tr>
    <td id="viewBillPay_firstAddressLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_20"/></td>
	<td id="viewBillPay_firstAddressValue" class="columndata">
	 <s:property value="%{payment.Payee.Street}"/>	
	<td id="viewBillPay_dateLabel" class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.billpay_76"/></td>
	<td id="viewBillPay_dateValue" class="columndata">
	 <s:property value="%{payment.PayDate}"/>	
    </tr>
    <tr>
    <td id="viewBillPay_secondAddressLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_21"/></td>
	<td id="viewBillPay_secondAddressValue" class="columndata">
	 <s:property value="%{payment.Payee.Street2}"/>	
	<td id="viewBillPay_deliverByDateLabel" valign="middle" class="tbrd_l">
		<div align="right">
			<span class="sectionsubhead"><s:text name="jsp.billpay_deliver_by"/></span></div>
	</td>
	<td id="viewBillPay_deliverByDateValue" class="columndata" colspan="2" valign="middle">
		<ffi:setProperty name="payment" property="DateFormat" value="${UserLocale.DateFormat}" />
		<ffi:getProperty name="payment" property="DeliverByDate"/>
		 <s:property value="%{payment.DeliverByDate}"/>													
	</td>	
    </tr>
    <tr>
    <td id="viewBillPay_thirdAddressLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_address3"/></td>
		<td id="viewBillPay_thirdAddressValue" class="columndata">
		 <s:property value="%{payment.Payee.Street3}"/>	
		<td id="viewBillPay_recurringLabel" class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.billpay_repeating"/></td>
		<td id="viewBillPay_recurringValue" class="columndata">
		 <s:property value="%{payment.Frequency}"/>	
    </tr>
    <tr>
   <td id="viewBillPay_cityLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_city"/></td>
	<td id="viewBillPay_cityValue" class="columndata">
	 <s:property value="%{payment.Payee.City}"/>	
	<td id="viewBillPay_frequencyLabel" class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.billpay_payments"/></td>
	<td id="viewBillPay_frequencyValue" class="columndata">
		<s:if test="%{payment.NumberPayments=='999'}">
			<!--L10NStart-->Unlimited<!--L10NEnd-->
		</s:if>
		<s:else>
			 <s:property value="%{payment.NumberPayments}"/>	
		</s:else>
	</td>
	 <tr>
		<td id="viewBillPay_stateLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_state"/></td>
		<td id="viewBillPay_stateValue" class="columndata">
			<s:property value="%{payment.payee.stateDisplayName}"/> 
		</td>
		<td id="viewBillPay_memoLabel" class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.billpay_memo"/></td>
		<td id="viewBillPay_memoValue" class="columndata">
		<s:property value="%{payment.memo}"/>
		</td>		
    </tr>
    <tr>
    <td id="viewBillPay_zipCodeLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_zippostalcode"/></td>
	<td id="viewBillPay_zipCodeValue" class="columndata">
	<s:property value="%{payment.Payee.ZipCode}"/>
	<td id="viewBillPay_submittedByLabel" class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.billpay_submittedby"/></td>
		<td id="viewBillPay_submittedByValue" class="columndata">
			<s:property value="%{payment.submittedByUserName}"/>
		</td>
    </tr>
    <tr>
    <td id="viewBillPay_countryLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_country"/></td>
		
		<td id="viewBillPay_countryValue" class="columndata">
		<s:property value="%{payment.payee.countryDisplayName}"/>
		</td>
		
	<td id="viewBillPay_referenceLabel" class="sectionsubhead tbrd_l" align="right"><s:text name="jsp.billpay_referencenum"/></td>
	<td class="columndata">
	<s:property value="%{payment.TrackingID}"/>
	<ffi:getProperty name="payment" property="TrackingID"/></td>

    </tr>
    <tr>
    <td id="viewBillPay_phoneLabel" class="sectionsubhead" align="right"><s:text name="jsp.billpay_83"/></td>
		<td id="viewBillPay_phoneValue" class="columndata">
		<s:property value="%{payment.Payee.Phone}"/>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<td id="viewBillPay_statusLabel" class="sectionsubhead tbrd_l" align="right">&nbsp;<s:text name="jsp.default_388"/></td>
		<td id="viewBillPay_statusValue" class="columndata">&nbsp;
				<ffi:setProperty name="isStatusSet"  value="false"/>
				<ffi:cinclude value1="${IncludeViewPayment.Status}" value2="<%= String.valueOf(com.ffusion.beans.billpay.PaymentStatus.PMS_BATCH_INPROCESS) %>">
					<!--L10NStart--><s:text name="jsp.default_237"/><!--L10NEnd(In Process)-->
					<ffi:setProperty name="isStatusSet"  value="true"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${IncludeViewPayment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_FUNDS_ALLOCATED) %>">
					<!--L10NStart--><s:text name="jsp.default_237"/><!--L10NEnd(In Process)-->
					<ffi:setProperty name="isStatusSet"  value="true"/>
				</ffi:cinclude>
				
				<ffi:cinclude value1="${IncludeViewPayment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_PAID) %>">
					<!--L10NStart--><s:text name="jsp.default_518"/><!--L10NEnd(Completed)-->
					<ffi:setProperty name="isStatusSet"  value="true"/>
				</ffi:cinclude>
				
				<ffi:cinclude value1="${IncludeViewPayment.Status}" value2="<%= String.valueOf(PaymentStatus.PMS_PENDING_APPROVAL) %>">
					<!--L10NStart--><s:text name="billPay.status.approvalPending"/><!--L10NEnd(Approval Pending)-->
					<ffi:setProperty name="isStatusSet"  value="true"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${isStatusSet}" value2="true"  operator="notEquals">
					<!--L10NStart--><s:text name="jsp.default_530"/><!--L10NEnd(pending)-->
				</ffi:cinclude>			
		</td>
		</ffi:cinclude>
    <tr>
    <s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
    	<tr>
        <td class="required" colspan="4">
           <div align="right">&#8776; <s:text name="jsp.default_241"/></div>
        </td>
    </tr>
    </s:if>
   	<tr>
		<td colspan="6">
			<s:if test="%{#session.SecureUser.AppType=='Business'}">
				<s:include value="../common/include-view-transaction-history.jsp"></s:include>
			</s:if>
		</td>
	</tr>
    <tr>
</table>

			</td>
		    </tr>
		</table> --%>

<s:if test="%{#session.SecureUser.AppType=='Business' && isVirtualInstance !='true'}">
<div class="marginTop10">
	<s:include value="../common/include-view-transaction-history.jsp"></s:include>
</div>
</s:if>
<s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
</form>
<div class="ffivisible" style="height:150px;">&nbsp;</div>
<div  class="ui-widget-header customDialogFooter">
	<sj:a id="printTransactionDetail" button="true" 
					onClick="ns.common.printPopup();" cssClass="hiddenWhilePrinting"><s:text name="jsp.default_331"/></sj:a>&nbsp;&nbsp;
	
	<s:if test='%{recPaymentID != "null" && isRecModel !="true"}'>
	 <sj:a 
		id="viewModelLink"
 		button="true"
 		onClick="viewModel()"
		><s:text name="jsp.default.view_orig_payment"/>
	 </sj:a>
	</s:if> 
	
	<sj:a id="done"
         button="true" 
onClickTopics="closeDialog"  cssClass="hiddenWhilePrinting"
 	><s:text name="jsp.default_175"/></sj:a>
</div>
</div>	
<ffi:removeProperty name="isTemplate"/>
<script type="text/javascript">
$(document).ready(function(){
	$( ".toggleClick" ).click(function(){ 
		if($(this).next().css('display') == 'none'){
			$(this).next().slideDown();
			$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
			$(this).removeClass("expandArrow").addClass("collapseArrow");
		}else{
			$(this).next().slideUp();
			$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
			$(this).addClass("expandArrow").removeClass("collapseArrow");
		}
	});
});

function viewModel() {
	var urlString = "<ffi:urlEncrypt url='/cb/pages/jsp/billpay/billpayviewbill.action?IsRecModel=true&RecPaymentID=${payment.RecPaymentID}&IsVirtualInstance=${payment.IsVirtualInstance}'/>";

	$.ajax({
		url: urlString,
		success: function(data){
			ns.common.closeDialog();
			$('#viewBillpayDetailsDialogID').html(data).dialog('open');
		}
	});
}

</script>
