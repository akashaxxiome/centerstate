<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

	<ffi:object id="billpayeetabs" name="com.ffusion.tasks.util.TabConfig"/>
	<ffi:setProperty name="billpayeetabs" property="TransactionID" value="BillPayee"/>
	
	<ffi:setProperty name="billpayeetabs" property="Tab" value="payeeList"/>
	<ffi:setProperty name="billpayeetabs" property="Href" value="/cb/pages/jsp/billpay/billpaypayees_list_summary.jsp"/>
	<ffi:setProperty name="billpayeetabs" property="LinkId" value="payeeList"/>
	<ffi:setProperty name="billpayeetabs" property="Title" value="jsp.billpay_74"/>
		
	<ffi:process name="billpayeetabs"/>
	
	
	<div id="billpayPayeeGridTabs" class="portlet">
	
		<div class="portlet-header">
			<h1 class="portlet-title" title="Payee"><s:text name="jsp.billpay_74" /></h1>
	            <%-- <a href='<ffi:getProperty name="tab" property="Href"/>' id='<ffi:getProperty name="tab" property="LinkId"/>'>
	            	<ffi:setProperty name="TabTitle" value="${tab.Title}"/><s:text name="%{#session.TabTitle}"/>
	            </a> --%>
	    </div>
	    <div class="portlet-content">
	    	<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="payeesummaryGrid" gridId="billpayPayeeGridId" class="gridSummaryContainerCls" >
					<s:include value="/pages/jsp/billpay/billpaypayees_list_summary.jsp" />
				</div>
			</div>
	    </div>
	    <input type="hidden" id="getTransID" value="<ffi:getProperty name='billpayeetabs' property='TransactionID'/>" />	
	    <div id="billPayTemplateSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('billpayPayeeGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	</div>

<script>    

	/* $('#billpayPayeeGridTabs').portlet({
		helpCallback: function(){
			
			var helpFile = $('#billpayPayeeGridTabs').find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
	});
	
	$("#billpayPayeeGridTabs").hover(
			  function () {
			    $("#billpayPayeeGridTabs .headerctls").show();
			  }, 
			  function () {
			    $("#billpayPayeeGridTabs .headerctls").hide();
			  }
			);
	 */
	//Initialize portlet with settings icon
		ns.common.initializePortlet("billpayPayeeGridTabs");
</script>	
