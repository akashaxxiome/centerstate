<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_billpaytemplatesinglesend" className="moduleHelpClass"/>

<%--=================== PAGE SECURITY CODE - Part 1 Start ====================--%>
<%-- <%	session.setAttribute( "Credential_Op_Type", ""+ com.ffusion.beans.authentication.AuthConsts.OPERATION_TYPE_TRANSACTION) ; %>

<ffi:setProperty name="securitySubMenu" value="${subMenuSelected}"/>
<ffi:setProperty name='securityPageHeading' value='${PageHeading}'/>

=================== PAGE SECURITY CODE - Part 1 End ======================
<ffi:removeProperty name="verified"/>
 --%>

<%-- for localizing state name --%>
<%-- <ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${Payee.Country}" />
<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${Payee.State}" />
<ffi:process name="GetStateProvinceDefnForState"/>
<ffi:setProperty name="AddPayment" property="AmountValue.CurrencyCode" value="${AddPayment.Payee.PayeeRoute.CurrencyCode}"/>
<ffi:setProperty name="AddPayment" property="AmtCurrency" value="${AddPayment.AmountValue.CurrencyCode}"/>

 --%>


<form action="" method="post" name="FormName">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="Refresh" value="true">
<div class="SaveTemplate">
	<div class="header"><s:text name="jsp.billpay_bill_pay_summary" /></div>
	<div class="leftPaneLoadPanel summaryBlock"  style="padding: 15px 10px;" id="leftPaneForDvc">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="singleBillPayTemplateConfirmPayeeNameLabel"><s:text name="jsp.default_314"/>:</span>
				<span class="inlineSection floatleft labelValue" id="singleBillPayTemplateConfirmPayeeNameValue"><s:property value="%{payment.payee.Name}"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="singleBillPayTemplateConfirmFromAccountLabel"><s:text name="jsp.default_15"/>:</span>
				<span id="singleBillPayTemplateConfirmFromAccountValue" class="inlineSection floatleft labelValue"><s:property value="%{payment.account.accountDisplayText}"/></span>
			</div>
	</div>
</div>
<div class="confirmPageDetails rightPaneConfirmDvc">
<div class="blockRow completed marginBottom10">
	<span id="billpayResultMessage"><span class="sapUiIconCls icon-accept "></span><s:text name="jsp.billpay_120"/></span>
</div>

<div  class="blockHead toggleClick expandArrow">
<s:text name="jsp.billpay.payee_summary" />: <s:property value="%{payment.payee.Name}"/> (<s:property value="%{payment.payee.NickName}"/> - <s:property value="%{payment.payee.userAccountNumber}"/>)
<span class="sapUiIconCls icon-navigation-down-arrow"></span></div>
<div class="toggleBlock hidden">
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateConfirmPayeeNickNameLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_75"/>:</span>
				<span id="singleBillPayTemplateConfirmPayeeNickNameValue" class="columndata" >
					<s:property value="%{payment.payee.NickName}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateConfirmPayeeAccountLabel" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_15"/>:</span>
				<span id="singleBillPayTemplateConfirmPayeeAccountValue" class="columndata" >
					<s:property value="%{payment.payee.userAccountNumber}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateConfirmContactLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_36"/>:</span>
				<span id="singleBillPayTemplateConfirmContactValue" class="columndata" >
					<s:property value="%{payment.payee.contactName}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateConfirmPhoneLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_83"/>:</span>
				<span id="singleBillPayTemplateConfirmPhoneValue" class="columndata" >
					<s:property value="%{payment.payee.phone}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateConfirmAddress1Label" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_36"/>:</span>
				<span id="singleBillPayTemplateConfirmAddress1Value" class="columndata" ><s:property value="%{payment.payee.street}"/></span>							
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateConfirmAddress2Label" class="sectionsubhead sectionLabel" ><s:text name="jsp.default_37"/>:</span>
				<span id="singleBillPayTemplateConfirmAddress2Value" class="columndata" >
					<s:property value="%{payment.payee.street2}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateConfirmAddress3Label"  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_38"/>:</span>
				<span id="singleBillPayTemplateConfirmAddress3Value" class="columndata"  >
					<s:property value="%{payment.payee.street3}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateConfirmCityLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_101"/>:</span>
				<span id="singleBillPayTemplateConfirmCityValue" class="columndata"  >
					<s:property value="%{payment.payee.city}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateConfirmStateLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.billpay_104"/>:</span>
				<span id="singleBillPayTemplateConfirmStateValue"  class="columndata">
					<s:property value="%{payment.payee.stateDisplayName}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateConfirmZipCodeLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_473"/>:</span>
				<span id="singleBillPayTemplateConfirmZipCodeValue" class="columndata">
					<s:property value="%{payment.payee.zipCode}"/>
				</span>										
											
			</div>
		</div>
		<div class="blockRow">
			<span id="singleBillPayTemplateConfirmCountryLabel"  class="sectionsubhead sectionLabel" ><s:text name="jsp.default_115"/>:</span>
			<span id="singleBillPayTemplateConfirmCountryValue" class="columndata" >
				<s:property value="%{payment.payee.countryDisplayName}"/>
			</span>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart--><s:text name="jsp.transaction.summary.label" /><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateConfirmTemplateNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_416"/>:</span>
				<span id="singleBillPayTemplateConfirmTemplateNameValue" class="columndata" ><s:property value="%{payment.templateName}"/></span>
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateConfirmMemoLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
				<span id="singleBillPayTemplateConfirmMemoValue" class="columndata" ><s:property value="%{payment.memo}"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateConfirmAmountLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/>:</span>
				<span id="singleBillPayTemplateConfirmAmountValue" class="columndata" >
					<s:property value="%{payment.amountValue.currencyStringNoSymbol}"/>
					<s:property value="%{payment.AmtCurrency}"/>
				</span>		
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateConfirmDebitAmountLabel" class="sectionsubhead sectionLabel">
	             <s:if test="%{payment.convertedAmount!=''}"><!-- do nothing --></s:if>
	       	     <s:else><s:text name="jsp.billpay_40"/>:</s:else>
			    </span>
				<span id="singleBillPayTemplateConfirmDebitAmountValue" class="columndata" >
					<s:if test="%{payment.convertedAmount!=''}">
	        	    	<!-- do nothing --> 
	        	    </s:if>
	        	    <s:else>
	        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
	        	    </s:else>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="singleBillPayTemplateConfirmReccuringLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_351"/>:</span>
				<span id="singleBillPayTemplateConfirmRecurringValue" class="columndata" >
					<s:property value="%{payment.frequency}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="singleBillPayTemplateConfirmFrequencyLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_321"/>:</span>
				<span id="singleBillPayTemplateConfirmFrequencyValue" class="columndata" >
					<s:if test="%{payment.openEnded}">
						<s:text name="jsp.default_446"/>
					</s:if>
					<s:else>
						<s:property value="%{payment.NumberPayments}"/>
					</s:else>
				</span>
			</div>
		</div>
	</div>
</div>
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					
					<tr>
						<td></td>
						<td class="columndata ltrow2_color">
							
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
											<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										</tr>
										<tr>
											<td id="singleBillPayTemplateConfirmTitleLabel" class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <s:text name="jsp.billpay_126"/><br>
												</span></td>
										</tr>
										<tr>
											<td class="columndata ltrow2_color"></td>
											<td></td>
											<td></td>
											<td class="sectionsubhead"><br>
											</td>
											<td></td>
											<td></td>
										</tr>
										<tr>
                                        	<td id="singleBillPayTemplateConfirmPayeeNickNameLabel" class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_75"/></span></div>
											</td>
											<td id="singleBillPayTemplateConfirmPayeeNickNameValue" class="columndata" >
											<ffi:getProperty name="Payee" property="NickName" />
											<s:property value="%{payment.payee.NickName}"/>
											</td>
											<td id="singleBillPayTemplateConfirmTemplateNameLabel" class="columndata tbrd_l">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_416"/></span></div>
											</td>
											<td id="singleBillPayTemplateConfirmTemplateNameValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayment" property="TemplateName" />
											<s:property value="%{payment.templateName}"/>
											</td>
											
										</tr>
										<tr>
											<td id="singleBillPayTemplateConfirmPayeeNameLabel" class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_314"/></span></div>
											</td>
											<td id="singleBillPayTemplateConfirmPayeeNameValue" class="columndata" >
											     <ffi:getProperty name="Payee" property="Name" />
											     <s:property value="%{payment.payee.Name}"/>
											</td>
											<td id="singleBillPayTemplateConfirmAmountLabel" class="tbrd_l"><div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_43"/></span></div>												
											</td>
											<td id="singleBillPayTemplateConfirmAmountValue" class="columndata" colspan="2">
											<ffi:getProperty name="AddPayment" property="AmountValue.CurrencyStringNoSymbol" />
												 <ffi:getProperty name="AddPayment" property="AmountValue.CurrencyCode"/>
												 <s:property value="%{payment.amountValue.currencyStringNoSymbol}"/>
											<s:property value="payment.payee.PayeeRoute.CurrencyCode"/>
											<s:property value="%{payment.AmtCurrency}"/>
											</td>											
										</tr>										
										<tr>
											<td id="singleBillPayTemplateConfirmPayeeAccountLabel" class="columndata" colspan="2">
                                                <div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>
                                            </td>
											<td id="singleBillPayTemplateConfirmPayeeAccountValue" class="columndata" >
												<ffi:getProperty name="Payee" property="UserAccountNumber" />
												<s:property value="%{payment.payee.userAccountNumber}"/>
											</td>
											<td id="singleBillPayTemplateConfirmDebitAmountLabel" class="tbrd_l">
												<ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="notEquals" >
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
											    </ffi:cinclude>
                                                <ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="equals" >
                                                    &nbsp;
                                                </ffi:cinclude>												
                                                 <s:if test="%{payment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_40"/></span></div>
												</s:else>
											</td>
											<td id="singleBillPayTemplateConfirmDebitAmountValue" class="columndata" colspan="2">
												<s:if test="%{payment.convertedAmount!=''}">
								        	    	<!-- do nothing --> 
								        	    </s:if>
								        	    <s:else>
								        	    	&#8776; <s:property value="%{payment.convertedAmount}"/>  <s:property value="%{payment.account.currencyCode}"/>
								        	    </s:else>
											</td>
										</tr>
										<tr>
											<td id="singleBillPayTemplateConfirmAddress1Label" class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_36"/> </span></div>
											</td>
											<td id="singleBillPayTemplateConfirmAddress1Value" class="columndata" >
											<ffi:getProperty name="Payee" property="Street" />				
											<s:property value="%{payment.payee.street}"/>								
											</td>
											<td id="singleBillPayTemplateConfirmFromAccountLabel" class="tbrd_l"><div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_15"/></span></div>											
											</td>
											<td id="singleBillPayTemplateConfirmFromAccountValue" class="columndata" colspan="2">
												 <s:property value="%{payment.account.accountDisplayText}"/>
											</td>											
										</tr>
										<tr>
											<td id="singleBillPayTemplateConfirmAddress2Label" class="columndata ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_37"/></span></div>
											</td>
											<td id="singleBillPayTemplateConfirmAddress2Value" class="columndata" >
											<ffi:getProperty name="Payee" property="Street2" />
											<s:property value="%{payment.payee.street2}"/>
											</td>
											<td id="singleBillPayTemplateConfirmReccuringLabel" class="tbrd_l">
												<div align="right">
												<span class="sectionsubhead">&nbsp;&nbsp;<s:text name="jsp.default_351"/> </span></div>
												</td>
											<td id="singleBillPayTemplateConfirmRecurringValue" class="columndata" colspan="2">
												<ffi:getProperty name="AddPayment" property="Frequency" />
												<s:property value="%{payment.frequency}"/>
											</td>
										</tr>
										<tr>
											<td id="singleBillPayTemplateConfirmAddress3Label" valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_38"/></span></div>
											</td>
											<td id="singleBillPayTemplateConfirmAddress3Value" class="columndata"  valign="middle">
											<ffi:getProperty name="Payee" property="Street3" />
											<s:property value="%{payment.payee.street3}"/>
											</td>
											<td id="singleBillPayTemplateConfirmFrequencyLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_321"/></span></div>
											</td>
											<td id="singleBillPayTemplateConfirmFrequencyValue" class="columndata" colspan="2">
												<s:if test="%{payment.openEnded}">
													<s:text name="jsp.default_446"/>
												</s:if>
												<s:else>
													<s:property value="%{payment.NumberPayments}"/>
												</s:else>
											</td>
										</tr>
										<tr>
											<td id="singleBillPayTemplateConfirmCityLabel" valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_101"/></span>
												</div>
											</td>
											<td id="singleBillPayTemplateConfirmCityValue" class="columndata"  valign="middle">
												<ffi:getProperty name="Payee" property="City" />
												<s:property value="%{payment.payee.city}"/>
											</td>
											<td id="singleBillPayTemplateConfirmMemoLabel" class="tbrd_l">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.default_279"/></span></div>
											</td>
											<td id="singleBillPayTemplateConfirmMemoValue" class="columndata" colspan="2">
												<ffi:getProperty name="AddPayment" property="Memo" />
												<s:property value="%{payment.memo}"/>
											</td>
											</tr>
										<tr>
											<td id="singleBillPayTemplateConfirmStateLabel" valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
													<span class="sectionsubhead"><s:text name="jsp.billpay_104"/></span></div>											
											</td>
											<td id="singleBillPayTemplateConfirmStateValue" valign="middle" class="columndata">
												<s:property value="%{payment.payee.stateDisplayName}"/>
											</td>
											
										</tr>
										<tr>											
											<td id="singleBillPayTemplateConfirmZipCodeLabel" valign="middle" class="ltrow2_color" colspan="2">
												<div align="right">
												<span class="sectionsubhead"><s:text name="jsp.default_473"/></span></div>
											</td>
											<td id="singleBillPayTemplateConfirmZipCodeValue" class="columndata">
											<ffi:getProperty name="Payee" property="ZipCode" />
											<s:property value="%{payment.payee.zipCode}"/>
											</td>											
											<td class="tbrd_l">
												&nbsp;
											</td>											
											<td class="columndata" colspan="2"></td>

										</tr>
										
										<tr>		
											<td id="singleBillPayTemplateConfirmCountryLabel" valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.default_115"/> </span></div>
											</td>
											<ffi:setProperty name="CountryResource" property="ResourceID" value="Country${Payee.Country}" />
											<td id="singleBillPayTemplateConfirmCountryValue" class="columndata" valign="middle">
											<ffi:getProperty name='CountryResource' property='Resource'/>
											<s:property value="%{payment.payee.countryDisplayName}"/>
											</td>
											<td class="tbrd_l">
												&nbsp;
											</td>											
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>											
											<td id="singleBillPayTemplateConfirmContactLabel" valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_36"/> </span></div>
											</td>
											<td id="singleBillPayTemplateConfirmContactValue" class="columndata" valign="middle">
											<ffi:getProperty name="Payee" property="ContactName" />
											<s:property value="%{payment.payee.contactName}"/>
											</td>
											<td class="tbrd_l">												
											</td>
											<td class="columndata" colspan="2"></td>
										</tr>

										<tr>											
											<td id="singleBillPayTemplateConfirmPhoneLabel" valign="middle" class="ltrow2_color" colspan="2"><div align="right">
													<span class="sectionsubhead">&nbsp;<s:text name="jsp.billpay_83"/></span></div></td>
											<td id="singleBillPayTemplateConfirmPhoneValue" class="columndata" valign="middle">
											<ffi:getProperty name="Payee" property="Phone" />
											<s:property value="%{payment.payee.phone}"/>
											</td>
											<td class="tbrd_l">												
											</td>
											<td ></td>											
										</tr>

                                        <ffi:cinclude value1="${AddPayment.AmountValue.CurrencyCode}" value2="${AddPayment.Account.CurrencyCode}" operator="notEquals" >
                                        <tr>
											<td id="singleBillPayTemplateConfirmEstimatedAmmountLabel" class="required" colspan="6">
                                                <br><div align="center">&#8776; <s:text name="jsp.default_241"/></div>
                                            </td>
                                            <td></td>
										</tr>
                                        </ffi:cinclude>
                                         <s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
									    	<tr>
									        <td class="required" colspan="4">
									           <div align="right">&#8776; <s:text name="jsp.default_241"/></div>
									        </td>
									    </tr>
									    </s:if>
                                        <tr>
											<td colspan="6"><div align="center">
													<br>
													<sj:a id="singleBillPayTemplateConfirmDoneBtn" 
										                button="true" 
										                buttonIcon="ui-icon-refresh"
														onClickTopics="cancelBillpayForm"
										        ><s:text name="jsp.default_175"/></sj:a>
									        </div></td>
											<td></td>
										</tr>
									</table>
							
						</td>
						<td></td>
					</tr>
					
				</table> --%>
 <s:if test="%{payment.AmtCurrency!=payment.Account.CurrencyCode}">
	<div class="required marginTop10" align="center">&#8776; <s:text name="jsp.default_241"/></div>
</s:if>
<div class="btn-row">
<sj:a id="singleBillPayTemplateConfirmDoneBtn" 
            button="true" 
            summaryDivId="summary" buttonType="done"
onClickTopics="showSummary,cancelBillpayForm,cancelLoadTemplateSummary"
    ><s:text name="jsp.default_175"/></sj:a>
</div>				
</div>
</form>


<%--=================== PAGE SECURITY CODE - Part 2 Start ====================--%>

<%--=================== PAGE SECURITY CODE - Part 2 End =====================--%>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>