<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:help id="custommapping_fixed" />

<%
	session.setAttribute("Initialize", request.getParameter("Initialize"));
%>

<%
	String MappingDefinitionName = request.getParameter("MappingDefinition.Name");
	String MappingDefinitionDescription = request.getParameter("MappingDefinition.Description");
	String MappingDefinitionOutputFormatName = request.getParameter("MappingDefinition.OutputFormatName");
	String MappingDefinitionInputFormatType = request.getParameter("InputFormatType");
	
%>

<% if(MappingDefinitionName != null) {%>
<ffi:setProperty name="MappingDefinition" property="Name" value="<%= MappingDefinitionName %>"/>
<% } if(MappingDefinitionDescription != null) {%>
<ffi:setProperty name="MappingDefinition" property="Description" value="<%= MappingDefinitionDescription %>"/>
<% } if(MappingDefinitionOutputFormatName != null) {%>
<ffi:setProperty name="MappingDefinition" property="OutputFormatName" value="<%= MappingDefinitionOutputFormatName %>"/>
<% } if(MappingDefinitionInputFormatType != null) {%>
<ffi:setProperty name="MappingDefinition" property="InputFormatType" value="<%= MappingDefinitionInputFormatType %>"/>
<% } %>

<ffi:cinclude value1="${MappingDefinition.MappingID}" value2="0" operator="equals">
	<%
		session.setAttribute("AddOrModify", "Add");
	%>
	<script>$('#customMappingPortlet').portlet('title', js_custom_mapping_add);</script>
</ffi:cinclude>
<ffi:cinclude value1="${MappingDefinition.MappingID}" value2="0" operator="notEquals">
	<%
		session.setAttribute("AddOrModify", "Modify");
	%>
	<script>$('#customMappingPortlet').portlet('title', js_custom_mapping_modify);</script>
</ffi:cinclude>

<%@ page import="java.util.ArrayList"%>
<%@ page import="com.ffusion.beans.fileimporter.FieldDefinitions"%>
<%@ page import="com.ffusion.beans.fileimporter.MappingDefinition"%>
<%@ page import="java.util.HashMap" %>

<s:set var="tmpI18nStr" value="%{getText('jsp.default_34')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name="ValidateURL" value="custommapping_fixed.jsp"/>
<ffi:setProperty name="BackURL" value="${SecurePath}${ValidateURL}"/>

<ffi:object id="GetOutputFormat" name="com.ffusion.tasks.fileImport.GetOutputFormatTask" scope="session"/>
<ffi:setProperty name="GetOutputFormat" property="Name" value="${MappingDefinition.OutputFormatName}"/>
<ffi:process name="GetOutputFormat"/>

<ffi:cinclude value1="${Initialize}" value2="true">
	<ffi:setProperty name="MappingDefinition" property="RecordDelimiterType" value="2"/>
	<ffi:removeProperty name="Initialize"/>
</ffi:cinclude>

<% String language = ""; %>
<ffi:getProperty name="SecureUser" property="LocaleLanguage" assignTo="language" />
<%
	com.ffusion.beans.fileimporter.OutputFormat temp_o_f = (com.ffusion.beans.fileimporter.OutputFormat)session.getAttribute( "OutputFormat" );
	// we clone the requiredFieldList because it can change if doing Amount Only - Partial Import
	ArrayList requiredList = (ArrayList)temp_o_f.getRequiredFieldList().clone();
	ArrayList memoList = temp_o_f.getMemoFieldList();
	ArrayList displayNames = temp_o_f.getMatchRecordOptionsDisplayNames();
	ArrayList matchRecord = temp_o_f.getMatchRecordOptions();
	ArrayList fieldList = temp_o_f.getFieldList();
	boolean isPartial = false;
    ArrayList localizedFieldList = temp_o_f.getFieldList( language );
    ArrayList localizedMemoList = temp_o_f.getMemoFieldList( language );
    ArrayList localizedOptionsList = temp_o_f.getOptionList( language );
    HashMap optionEntitlements = temp_o_f.getOptionsEntitlementList();
	String localizedName = temp_o_f.getName( language );
	temp_o_f = null;
	MappingDefinition md = (MappingDefinition)session.getAttribute("MappingDefinition");
	if (md != null && md.getUpdateRecordsBy() == MappingDefinition.UPDATE_RECORDS_BY_EXISTING_AMOUNTS_ONLY && displayNames.size() > 0)
	{
		requiredList.clear();			// redo the required list
		isPartial = true;
		String fieldName;
		for (int i = 0; i < fieldList.size(); i++)
		{
			fieldName = (String)fieldList.get(i);
			boolean required = false;
			if (fieldName.indexOf("Amount") != -1 || fieldName.indexOf("Credit or Debit") != -1)
				required = true;
			for (int idx = 0; idx < displayNames.size(); idx++)
			{
				String dName = (String)displayNames.get(idx);
				if (dName.indexOf(fieldName) != -1)
				{
					if (matchRecord.indexOf("Name") == idx &&
							(md.getMatchRecordsBy() == MappingDefinition.MATCH_RECORDS_BY_NAME ||
							 md.getMatchRecordsBy() == MappingDefinition.MATCH_RECORDS_BY_ID_NAME_ACCOUNT ||
							 md.getMatchRecordsBy() == MappingDefinition.MATCH_RECORDS_BY_ID_NAME))
						required = true;
					if (matchRecord.indexOf("ID") == idx &&
							(md.getMatchRecordsBy() == MappingDefinition.MATCH_RECORDS_BY_ID ||
							 md.getMatchRecordsBy() == MappingDefinition.MATCH_RECORDS_BY_ID_NAME_ACCOUNT ||
							 md.getMatchRecordsBy() == MappingDefinition.MATCH_RECORDS_BY_ID_NAME))
						required = true;
					if (matchRecord.indexOf("Account") == idx &&
							(md.getMatchRecordsBy() == MappingDefinition.MATCH_RECORDS_BY_ID_NAME_ACCOUNT ||
							 md.getMatchRecordsBy() == MappingDefinition.MATCH_RECORDS_BY_ACCOUNT))
						required = true;
				}
			}
			if (required)
				requiredList.add("TRUE");
			else
				requiredList.add("FALSE");
		}
	}
%>


		<script type="text/javascript"><!--
function validateChangeMatchOrUpdate( )
{
	if (document.MappingFixedForm["MappingDefinition.UpdateRecordsBy"] == null)
		return;
	if (document.MappingFixedForm["MappingDefinition.UpdateRecordsBy"].value == "<%= MappingDefinition.UPDATE_RECORDS_BY_NEW %>")
	{
		if (document.MappingFixedForm["MappingDefinition.MatchRecordsBy"] != null)
			document.MappingFixedForm["MappingDefinition.MatchRecordsBy"].disabled = true;
			$("#MatchRecordsByID").selectmenu('disable');
	} else
	{
		if (document.MappingFixedForm["MappingDefinition.MatchRecordsBy"] != null)
			document.MappingFixedForm["MappingDefinition.MatchRecordsBy"].disabled = false;
			$("#MatchRecordsByID").selectmenu('enable');
	}
	<% if (displayNames.size() > 0) { %>
		<% if (isPartial) { %>
				$.ajax({    
				  url: "/cb/pages/mapping/ModifyMappingDefinitionAction_updateRecord.action", 
				  type: "POST", 
				  data: $("#mappingFormID3").serialize(),    
				  success: function(data) {  
					 $("#verifyCustomMappingDiv").html(data);
				  }   
				});
		<% } else { %>
			if (document.MappingFixedForm["MappingDefinition.UpdateRecordsBy"].value == "<%= MappingDefinition.UPDATE_RECORDS_BY_EXISTING_AMOUNTS_ONLY %>")
			{
				$.ajax({    
				  url: "/cb/pages/mapping/ModifyMappingDefinitionAction_updateRecord.action", 
				  type: "POST", 
				  data: $("#mappingFormID3").serialize(),    
				  success: function(data) {  
					 $("#verifyCustomMappingDiv").html(data);
				  }   
				});
			}
		<% } %>
	<% } %>
}
function validateChangeDelimiterType( )
{
	if (document.MappingFixedForm["MappingDefinition.RecordDelimiterType"].value == "<%= MappingDefinition.RECORD_DELIMITER_TYPE_NONE %>")
	{
		document.MappingFixedForm["MappingDefinition.RecordLength"].value = document.MappingFixedForm["RecordLength"].value;
		document.MappingFixedForm["RecordLength"].disabled = false;
		document.MappingFixedForm["MappingDefinition.NumHeaderChars"].disabled = false;
		document.MappingFixedForm["MappingDefinition.NumHeaderLines"].disabled = true;
	} else
	{
		document.MappingFixedForm["RecordLength"].value = "";
		document.MappingFixedForm["RecordLength"].disabled = true;
		document.MappingFixedForm["MappingDefinition.RecordLength"].value = "0";
		document.MappingFixedForm["MappingDefinition.NumHeaderChars"].disabled = true;
		document.MappingFixedForm["MappingDefinition.NumHeaderLines"].disabled = false;
	}
}
function changeRecordLength( )
{
	document.MappingFixedForm["MappingDefinition.RecordLength"].value = document.MappingFixedForm["RecordLength"].value;
}

$(function(){
	$("#FieldDelimiterTypeID").selectmenu({width: 267});
	$("#RecordDelimiterTypeID").selectmenu({width: 267});
	$("#DateFormatTypeID").selectmenu({width: 267});
	$("#DateSeparatorTypeID").selectmenu({width: 267});
	$("#MoneyFormatTypeID").selectmenu({width: 267});
	$("#MatchRecordsByID").selectmenu({width: 267});
	$("#UpdateRecordsByID").selectmenu({width: 267});
});

function resetvalues()
{
	document.MappingFixedForm.reset();
	$("#FieldDelimiterTypeID").val(<ffi:getProperty name="fieldID"/>);
	$("#RecordDelimiterTypeID").val(<ffi:getProperty name="MappingDefinition" property="RecordDelimiterType"/>);
	$("#DateFormatTypeID").val(<ffi:getProperty name="MappingDefinition" property="DateFormatType"/>);
	$("#DateSeparatorTypeID").val(<ffi:getProperty name="MappingDefinition" property="DateSeparatorType"/>);
	$("#MoneyFormatTypeID").val(<ffi:getProperty name="MappingDefinition" property="MoneyFormatType"/>);
	$("#MatchRecordsByID").val(<ffi:getProperty name="MappingDefinition" property="MatchRecordsBy"/>);
	$("#UpdateRecordsByID").val(<ffi:getProperty name="MappingDefinition" property="UpdateRecordsBy"/>);
	
	$("#FieldDelimiterTypeID").selectmenu('destroy').selectmenu({width: 267});
	$("#RecordDelimiterTypeID").selectmenu('destroy').selectmenu({width: 267});
	$("#DateFormatTypeID").selectmenu('destroy').selectmenu({width: 267});
	$("#DateSeparatorTypeID").selectmenu('destroy').selectmenu({width: 267});
	$("#MoneyFormatTypeID").selectmenu('destroy').selectmenu({width: 267});
	$("#MatchRecordsByID").selectmenu('destroy').selectmenu({width: 267});
	$("#UpdateRecordsByID").selectmenu('destroy').selectmenu({width: 267});
}


$("#NumHeaderLinesID").keypress(function(event) { 
	  if ( event.which < 48 && event.which != 0 && event.which != 8 || event.which > 57) { 
		 event.preventDefault(); 
	   }
});
	
// --></script>

<%-- include javascripts for top navigation menu bar
Since Payments, Transfers, ACH and Wire all are in the SubDirectory payments, just make that the default. --%>
<% request.setAttribute("SubDirectory", "payments"); %>
<ffi:cinclude value1="${Section}" value2="PPAY">
	<% request.setAttribute("SubDirectory", "cash"); %>
</ffi:cinclude>

<BODY onload="validateChangeDelimiterType();">

		<div align="left">

			<s:form name="MappingFixedForm" id="mappingFormID3" namespace="/pages/mapping" action="%{#session.AddOrModify + 'MappingDefinitionAction_execute'}" method="post" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<%-- Remember previous values, if necessary --%>
			<ffi:cinclude value1="${prevName}" value2="" operator="equals">
				<ffi:setProperty name="prevName" value="${MappingDefinition.Name}"/>
			</ffi:cinclude>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_288.2"/></span><span class="required">*</span> 
					</td>
					<td>
					<%-- input class="txtbox" type="text" name="MappingDefinition.Name" value="<ffi:getProperty name='MappingDefinition' property='Name'/>" size="40" maxlength="255" border="0" --%> 
					<s:textfield name="MappingDefinition.Name" size="32" maxlength="32" value="%{#session.MappingDefinition.Name}" cssClass="txtbox ui-widget-content ui-corner-all"/>
					<ffi:setProperty name="CustomMappingErrors" property="Key" value="Name"/><span class="errortext"><ffi:getProperty name="CustomMappingErrors" property="Value"/></span>
						
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap align="right"></td>
					<td colspan="2" align="left"><span id="MappingDefinition.NameError"></span></td>
				</tr>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_172"/></span>
					</td>
					<td>
					<%-- input class="txtbox" type="text" name="MappingDefinition.Description" value="<ffi:getProperty name='MappingDefinition' property='Description'/>" size="40" maxlength="255" border="0" --%>
					<s:textfield name="MappingDefinition.Description" size="32" maxlength="32" value="%{#session.MappingDefinition.Description}" cssClass="txtbox ui-widget-content ui-corner-all"/>
					<ffi:setProperty name="CustomMappingErrors" property="Key" value="Description"/><span class="errortext"><ffi:getProperty name="CustomMappingErrors" property="Value"/></span>
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap align="right"></td>
					<td colspan="2" align="left"><span id="MappingDefinition.DescriptionError"></span></td>
				</tr>
					
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_310"/></span>
					</td>
					<td class="mainFont">
						<%= localizedName %>
					</td>
				</tr>
				<tr>
					<td colspan="2"><span class="sectionhead">&nbsp;</span></td>
				</tr>
				<tr>
					<td class="tbrd_b" colspan="2"><span class="sectionhead">&gt; <s:text name="jsp.default_245"/><br>
						</span></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_247"/></span>
					</td>
					<td class="mainFont">
						<s:text name="jsp.default_212"/>
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_342"/></span>
					</td>
					<td>
						<select name="MappingDefinition.RecordDelimiterType" id="RecordDelimiterTypeID" class="txtbox" onchange="validateChangeDelimiterType();" >
							<option value="<%=MappingDefinition.RECORD_DELIMITER_TYPE_NONE%>"
								<ffi:cinclude value1="${MappingDefinition.RecordDelimiterType}" value2="2">selected</ffi:cinclude>
							><s:text name="jsp.default_296"/></option>
							<option value="<%=MappingDefinition.RECORD_DELIMITER_TYPE_LF%>"
								<ffi:cinclude value1="${MappingDefinition.RecordDelimiterType}" value2="0">selected</ffi:cinclude>
							><s:text name="jsp.default_260"/></option>
							<option value="<%=MappingDefinition.RECORD_DELIMITER_TYPE_CRLF%>"
								<ffi:cinclude value1="${MappingDefinition.RecordDelimiterType}" value2="1">selected</ffi:cinclude>
							><s:text name="jsp.default_118"/></option>
						</select>

						<span class="plainsubhead">&nbsp;<s:text name="jsp.default_306"/>&nbsp;</span>

						<span class="sectionsubhead"><s:text name="jsp.default_343"/></span>
						<% String recordLength; %>
						<ffi:getProperty name='MappingDefinition' property='RecordLength' assignTo="recordLength" />
						<% if (recordLength == null || recordLength.equals("0"))
							recordLength = ""; %>
						<input class="txtbox ui-widget-content ui-corner-all" type="text" name="RecordLength" value="<%=recordLength %>" size="5" maxlength="5" border="0" onchange="changeRecordLength();">
						<input type="hidden" name="MappingDefinition.RecordLength" value="<%=recordLength %>" size="5" maxlength="5" border="0">
						&nbsp;<ffi:setProperty name="CustomMappingErrors" property="Key" value="DelimiterLength"/><span class="errortext"><ffi:getProperty name="CustomMappingErrors" property="Value"/></span>
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap align="right"></td>
					<td colspan="2" align="left"><span id="MappingDefinition.RecordDelimiterTypeError"></span></td>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_301"/></span>
					</td>
					<td>
					<input class="txtbox ui-widget-content ui-corner-all" type="text" name="MappingDefinition.NumHeaderChars" value="<ffi:getProperty name='MappingDefinition' property='NumHeaderChars'/>" size="34" border="0"></td>
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_302"/></span>
					</td>
					<td>
					<input class="txtbox ui-widget-content ui-corner-all" type="text" id="NumHeaderLinesID" name="MappingDefinition.NumHeaderLines" maxlength="9" value="<ffi:getProperty name="MappingDefinition" property="NumHeaderLines"/>" size="10" border="0"></td>
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_139"/></span>
					</td>
					<td>
					<select name="MappingDefinition.DateFormatType" id="DateFormatTypeID" class="txtbox">
						<option value="0"
							<ffi:cinclude value1="${MappingDefinition.DateFormatType}" value2="0">selected</ffi:cinclude>
						><s:text name="jsp.default_470"/></option>
						<option value="1"
							<ffi:cinclude value1="${MappingDefinition.DateFormatType}" value2="1">selected</ffi:cinclude>
						><s:text name="jsp.default_469"/></option>
						<option value="2"
							<ffi:cinclude value1="${MappingDefinition.DateFormatType}" value2="2">selected</ffi:cinclude>
						><s:text name="jsp.default_281"/></option>
						<option value="3"
							<ffi:cinclude value1="${MappingDefinition.DateFormatType}" value2="3">selected</ffi:cinclude>
						><s:text name="jsp.default_280"/></option>
						<option value="4"
							<ffi:cinclude value1="${MappingDefinition.DateFormatType}" value2="4">selected</ffi:cinclude>
						><s:text name="jsp.default_145"/></option>
						<option value="5"
							<ffi:cinclude value1="${MappingDefinition.DateFormatType}" value2="5">selected</ffi:cinclude>
						><s:text name="jsp.default_144"/></option>
					</select>
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_140"/></span>
					</td>
					<td>
					<select name="MappingDefinition.DateSeparatorType" id="DateSeparatorTypeID" class="txtbox">
						<option value="0"
							<ffi:cinclude value1="${MappingDefinition.DateSeparatorType}" value2="0">selected</ffi:cinclude>
						><s:text name="jsp.default_383"/></option>
						<option value="1"
							<ffi:cinclude value1="${MappingDefinition.DateSeparatorType}" value2="1">selected</ffi:cinclude>
						><s:text name="jsp.default_232"/></option>
						<option value="2"
							<ffi:cinclude value1="${MappingDefinition.DateSeparatorType}" value2="2">selected</ffi:cinclude>
						><s:text name="jsp.default_324"/></option>
						<option value="3"
							<ffi:cinclude value1="${MappingDefinition.DateSeparatorType}" value2="3">selected</ffi:cinclude>
						><s:text name="jsp.default_296"/></option>
					</select>
					</td>
				</tr>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_282"/></span>
					</td>
					<td>
					<select name="MappingDefinition.MoneyFormatType" id="MoneyFormatTypeID" class="txtbox">
						<option value="0"
							<ffi:cinclude value1="${MappingDefinition.MoneyFormatType}" value2="0">selected</ffi:cinclude>
						><s:text name="jsp.default_52"/></option>
						<option value="1"
							<ffi:cinclude value1="${MappingDefinition.MoneyFormatType}" value2="1">selected</ffi:cinclude>
						><s:text name="jsp.default_11"/></option>
						<option value="2"
							<ffi:cinclude value1="${MappingDefinition.MoneyFormatType}" value2="2">selected</ffi:cinclude>
						><s:text name="jsp.default_13"/></option>
					</select>
					</td>
				</tr>
			<%-- Update options are not applicable to Positive Pay Check Records, so hide
			     this section.
			     Note: The output format name "Positive Pay Check Record" comes from the
			     fileimporter.xml, so this page will need to be updated if it is changed
			     in the xml! --%>
			<ffi:cinclude value1="${OutputFormat.Name}" value2="Positive Pay Check Record" operator="notEquals">
				<tr>
					<td colspan="2"><span class="sectionhead">&nbsp;</span></td>
				</tr>
				<tr>
					<td class="tbrd_b" colspan="2"><span class="sectionhead">&gt; <s:text name="jsp.default_344"/><br>
						</span></td>
				</tr>
				<ffi:cinclude value1="${OutputFormat.ContainsMatchRecordOptions}" value2="true" operator="equals">
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_277"/></span>
					</td>
					<td class="mainFont">
					<ffi:setProperty name="OutputFormat" property="CurrentMatchRecordOption" value="Name,ID"/>
					<ffi:cinclude value1="${OutputFormat.ContainsMatchRecordOption}" value2="true" operator="equals">
					<select name="MappingDefinition.MatchRecordsBy" id="MatchRecordsByID" class="txtbox"
						<ffi:cinclude value1="${MappingDefinition.UpdateRecordsBy}" value2="1">disabled</ffi:cinclude>
						onchange="validateChangeMatchOrUpdate();" >
						<option value="<%=MappingDefinition.MATCH_RECORDS_BY_NAME%>"
							<ffi:cinclude value1="${MappingDefinition.MatchRecordsBy}" value2="0">selected</ffi:cinclude>
						><s:text name="jsp.default_244.2"/></option>
						<option value="<%=MappingDefinition.MATCH_RECORDS_BY_ID%>"
							<ffi:cinclude value1="${MappingDefinition.MatchRecordsBy}" value2="1">selected</ffi:cinclude>
						><s:text name="jsp.default_242"/></option>
						<option value="<%=MappingDefinition.MATCH_RECORDS_BY_ID_NAME%>"
							<ffi:cinclude value1="${MappingDefinition.MatchRecordsBy}" value2="2">selected</ffi:cinclude>
						><s:text name="jsp.default_285.2"/></option>
<ffi:setProperty name="OutputFormat" property="CurrentCategory" value="ACH" />
<ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
						<option value="<%=MappingDefinition.MATCH_RECORDS_BY_ACCOUNT%>"
							<ffi:cinclude value1="${MappingDefinition.MatchRecordsBy}" value2="3">selected</ffi:cinclude>
						><s:text name="jsp.default_15"/></option>
						<option value="<%=MappingDefinition.MATCH_RECORDS_BY_ID_NAME_ACCOUNT%>"
							<ffi:cinclude value1="${MappingDefinition.MatchRecordsBy}" value2="4">selected</ffi:cinclude>
						><s:text name="jsp.default_286.2"/></option>
</ffi:cinclude>
					</select>
					</ffi:cinclude>
<% boolean isPayments = false;
boolean isTransfer = false; %>
<ffi:setProperty name="OutputFormat" property="CurrentCategory" value="PAYMENT" />
<ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
<% isPayments = true; %>
</ffi:cinclude>
<ffi:setProperty name="OutputFormat" property="CurrentCategory" value="TRANSFER" />
<ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
<% isTransfer = true; %>
</ffi:cinclude>
					<ffi:cinclude value1="${OutputFormat.ContainsMatchRecordOption}" value2="true" operator="notEquals">
						<ffi:setProperty name="OutputFormat" property="CurrentMatchRecordOption" value="Name"/>
						<ffi:cinclude value1="${OutputFormat.ContainsMatchRecordOption}" value2="true" operator="equals">
<%
if (isPayments) {
%>
<s:text name="jsp.default_314"/>
<% } else { %>
<s:text name="jsp.default_243"/>
<% } %>
							<input type="hidden" name="MappingDefinition.MatchRecordsBy" value="<%=MappingDefinition.MATCH_RECORDS_BY_NAME%>">
						</ffi:cinclude>
						<ffi:setProperty name="OutputFormat" property="CurrentMatchRecordOption" value="ID"/>
						<ffi:cinclude value1="${OutputFormat.ContainsMatchRecordOption}" value2="true" operator="equals">
							<s:text name="jsp.default_242"/>
							<input type="hidden" name="MappingDefinition.MatchRecordsBy" value="<%=MappingDefinition.MATCH_RECORDS_BY_ID%>">
						</ffi:cinclude>
						<ffi:setProperty name="OutputFormat" property="CurrentMatchRecordOption" value="Account"/>
						<ffi:cinclude value1="${OutputFormat.ContainsMatchRecordOption}" value2="true" operator="equals">
<%
if (isTransfer) {
%>
<s:text name="jsp.default_218"/>
<% } else { %>
<s:text name="jsp.default_15"/>
<% } %>
							<input type="hidden" name="MappingDefinition.MatchRecordsBy" value="<%=MappingDefinition.MATCH_RECORDS_BY_ACCOUNT%>">
						</ffi:cinclude>
					</ffi:cinclude>
					</td>
				</tr>
				</ffi:cinclude>
				<tr>
					<td class="columndata" nowrap align="right">
					<span class="sectionsubhead"><s:text name="jsp.default_451"/></span>
					</td>
					<td>
					<select name="MappingDefinition.UpdateRecordsBy" id="UpdateRecordsByID" class="txtbox" onchange="validateChangeMatchOrUpdate();">
						<option value="<%= MappingDefinition.UPDATE_RECORDS_BY_NEW %>"
							<ffi:cinclude value1="${MappingDefinition.UpdateRecordsBy}" value2="1">selected</ffi:cinclude>
						><s:text name="jsp.default_32"/></option>
						<option value="<%= MappingDefinition.UPDATE_RECORDS_BY_EXISTING_NEW %>"
							<ffi:cinclude value1="${MappingDefinition.UpdateRecordsBy}" value2="2">selected</ffi:cinclude>
						><s:text name="jsp.default_449"/></option>
						<option value="<%= MappingDefinition.UPDATE_RECORDS_BY_EXISTING %>"
							<ffi:cinclude value1="${MappingDefinition.UpdateRecordsBy}" value2="0">selected</ffi:cinclude>
						><s:text name="jsp.default_448"/></option>
<ffi:setProperty name="OutputFormat" property="CurrentCategory" value="ACH" />
<ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
						<option value="<%= MappingDefinition.UPDATE_RECORDS_BY_EXISTING_AMOUNTS_ONLY %>"
							<ffi:cinclude value1="${MappingDefinition.UpdateRecordsBy}" value2="3">selected</ffi:cinclude>
						><s:text name="jsp.default_311"/></option>
</ffi:cinclude>
					</select>
					</td>
				</tr>
			</ffi:cinclude>
				<tr>
					<td colspan="2"><span class="sectionhead">&nbsp;</span></td>
				</tr>
				<tr>
					<td class="tbrd_b" colspan="2"><span class="sectionhead">&gt; <s:text name="jsp.default_205"/><br>
						</span></td>
				</tr>

				<tr align="center"><td colspan="2">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<th class="columndata" width="25%" align="right" style="padding:10px 0;"><span class="sectionhead"><s:text name="jsp.default_206"/></span></td>
							<th class="columndata" width="10" style="padding:10px 0;">&nbsp;</td>
							<th class="columndata" width="20%" align="left" style="padding:10px 0;"><span class="sectionhead"><s:text name="jsp.default_208"/></span></td>
							<th class="columndata" width="20%" align="left" style="padding:10px 0;"><span class="sectionhead"><s:text name="jsp.default_204"/></span></td>
							<th class="columndata" width="35%" align="left" style="padding:10px 0;"><span class="sectionhead"><s:text name="jsp.default_161"/></span></td>
						</tr>
						<%
							int array_idx = 0;
						%>
						<ffi:list collection="OutputFormat.FieldList" items="Field">
						<ffi:setProperty name="MappingDefinition" property="CurrentFieldDefinitionName" value="${Field}"/>
						<tr>
							<td class="columndata" valign="top" align="right"><%= localizedFieldList.get( array_idx ) %><% if (((String)requiredList.get(array_idx)).equalsIgnoreCase("true")){ %><span class="required">*</span><% } %></td>
							<td class="columndata" width="10">&nbsp;</td>
							<td class="columndata" valign="top" align="left">
								<ffi:cinclude value1="${MappingDefinition.CurrentFieldDefinition.FieldStart}" value2="0" operator="notEquals">
									<%-- input class="ui-widget-content ui-corner-all" type="text" name="MappingDefinition.CurrentFieldDefinitionName=<ffi:getProperty name='Field'/>&MappingDefinition.FieldStart" value="<ffi:getProperty name='MappingDefinition' property='CurrentFieldDefinition.FieldStart'/>" size="25" border="0" --%>
									
									<s:textfield name="MappingDefinition.CurrentFieldDefinitionName=%{#attr.Field}&MappingDefinition.FieldStart" size="32" maxlength="32" value="%{#session.MappingDefinition.CurrentFieldDefinition.FieldStart}" cssClass="ui-widget-content ui-corner-all"/>
									
								</ffi:cinclude>
								<ffi:cinclude value1="${MappingDefinition.CurrentFieldDefinition.FieldStart}" value2="0" operator="equals">
									<input class="ui-widget-content ui-corner-all" type="text" name="MappingDefinition.CurrentFieldDefinitionName=<ffi:getProperty name='Field'/>&MappingDefinition.FieldStart" value="<ffi:cinclude value1="${MappingDefinition.CurrentFieldDefinition.FieldStartString}" value2="0" operator="notEquals"><ffi:getProperty name='MappingDefinition' property='CurrentFieldDefinition.FieldStartString'/></ffi:cinclude>" size="25" border="0">
								</ffi:cinclude>
							</td>
							<td class="columndata" valign="top" align="left">
								<ffi:cinclude value1="${MappingDefinition.CurrentFieldDefinition.FieldEnd}" value2="0" operator="notEquals">
									<%-- input class="ui-widget-content ui-corner-all" type="text" name="MappingDefinition.CurrentFieldDefinitionName=<ffi:getProperty name='Field'/>&MappingDefinition.FieldEnd" value="<ffi:getProperty name='MappingDefinition' property='CurrentFieldDefinition.FieldEnd'/>" size="25" border="0" --%>
									
									<s:textfield name="MappingDefinition.CurrentFieldDefinitionName=%{#attr.Field}&MappingDefinition.FieldEnd" size="32" maxlength="32" value="%{#session.MappingDefinition.CurrentFieldDefinition.FieldEnd}" cssClass="ui-widget-content ui-corner-all"/>
									
								</ffi:cinclude>
								<ffi:cinclude value1="${MappingDefinition.CurrentFieldDefinition.FieldEnd}" value2="0" operator="equals">
									<input class="ui-widget-content ui-corner-all" type="text" name="MappingDefinition.CurrentFieldDefinitionName=<ffi:getProperty name='Field'/>&MappingDefinition.FieldEnd" value="<ffi:cinclude value1="${MappingDefinition.CurrentFieldDefinition.FieldEndString}" value2="0" operator="notEquals"><ffi:getProperty name='MappingDefinition' property='CurrentFieldDefinition.FieldEndString'/></ffi:cinclude>" size="25" border="0">
								</ffi:cinclude>
							</td>
							<td class="columndata" valign="top" align="left">

                                <% if (((com.ffusion.beans.util.KeyValueList)localizedOptionsList.get(array_idx)).size() > 0)
                                { %>
                                    <% String optionValue = "";
                                       String optionKey = "";
                                       com.ffusion.beans.util.KeyValueList options = ((com.ffusion.beans.util.KeyValueList)localizedOptionsList.get(array_idx));
                                       session.setAttribute("options", options);
                                    %>
                                    <ffi:setProperty name="options" property="SortedBy" value="Key"/>
                                        <select id="mappingDefOptions" class="txtbox" name="MappingDefinition.CurrentFieldDefinitionName=<ffi:getProperty name='Field'/>&MappingDefinition.DefaultValue">
                                            <option value="">No default value</option>
                                            <ffi:list collection="options" items="option">
                                                <ffi:getProperty name="option" property="value" assignTo="optionValue"/>
                                                <ffi:getProperty name="option" property="key" assignTo="optionKey"/>
                                                <% if (optionValue == null)
                                                         optionValue ="";
                                                    String optionEntitlement = "";
                                                    boolean isEntitled = true;
                                                    optionValue = optionValue.trim();
                                                    if (optionEntitlements.containsKey(optionKey))
                                                    {
                                                        optionEntitlement = (String)optionEntitlements.get(optionKey);
                                                %>
                                                        <ffi:cinclude ifNotEntitled="<%= optionEntitlement %>">
                                                            <% isEntitled = false; %>
                                                        </ffi:cinclude>
                                                <% } %>
                                                <% if (isEntitled) { %>
                                                <option value="<ffi:getProperty name="option" property="value" />"
                                                <ffi:cinclude value1="${MappingDefinition.CurrentFieldDefinition.DefaultValue}" value2="<%=optionValue%>">
                                                    selected
                                                </ffi:cinclude>><ffi:getProperty name="option" property="key" /></option>
                                                <% } %>
                                            </ffi:list>
                                            <ffi:removeProperty name="optionValue"/>
                                            <ffi:removeProperty name="options"/>
                                        </select>
                                <% } else { %>
								<%-- input class="ui-widget-content ui-corner-all" type="text" name="MappingDefinition.CurrentFieldDefinitionName=<ffi:getProperty name='Field'/>&MappingDefinition.DefaultValue" value="<ffi:getProperty name='MappingDefinition' property='CurrentFieldDefinition.DefaultValue'/>" size="40" border="0" --%>
								
								<s:textfield name="MappingDefinition.CurrentFieldDefinitionName=%{#attr.Field}&MappingDefinition.DefaultValue" size="32" maxlength="32" value="%{#session.MappingDefinition.CurrentFieldDefinition.DefaultValue}" cssClass="ui-widget-content ui-corner-all"/>
                                <% } %>
							<% if (((String)localizedMemoList.get(array_idx)).length() > 0)
							{ %>
                                <div>
								<%= (String)localizedMemoList.get(array_idx) %>
                                </div>                                    
							<% } %>
<% String errorMsg = ""; %>
<ffi:setProperty name="CustomMappingErrors" property="Key" value="Field_${Field}"/>
<ffi:getProperty name="CustomMappingErrors" property="Value" assignTo="errorMsg" />
<% if (errorMsg != null && errorMsg.length() > 0) { %>
<br><span class="errortext"><%=errorMsg%></span>
<ffi:removeProperty name="errorMsg"/>
<% } %>
							</td>
						</tr>
						
						<% String fieldName = ""; %>

						<ffi:getProperty name='Field' assignTo='fieldName' />
						
						<% String fieldNameTrim = fieldName.replace(" ","");
						   session.setAttribute("fieldNameTrim",fieldNameTrim);
						 %>
						
						<tr>
						<td colspan="2">&nbsp;</td>
						<td colspan="3" align="left">
						<span id="Start<ffi:getProperty name='fieldNameTrim'/>Error"></span>
						<span id="End<ffi:getProperty name='fieldNameTrim'/>Error"></span>
						</td></tr>
						<ffi:removeProperty name="fieldNameTrim"/>
						
						<% array_idx++; %>
						</ffi:list>
						<tr>
							<td align="center" class="columndata" colspan="5">
								<span class="required">* <s:text name="jsp.default_240"/></span>
							</td>
						</tr>
					</table>
				</td></tr>

				<tr>
					<td colspan="2"><span class="sectionhead">&nbsp;</span></td>
				</tr>
				<tr>
					<td align="center" nowrap colspan="2">
					<sj:a id="resetCustomMappingFixed" 
							button="true" 
							onclick="resetvalues()"
					><s:text name="jsp.default_358"/></sj:a>

					<%-- input class="submitbutton" type="button" value="CANCEL" onclick="window.location='<ffi:getProperty name="SecurePath"/>fileuploadcustom.jsp';" --%>
					
					<sj:a id="cancelFormButtonOnInput3" 
							button="true" 
							onClickTopics="cancelAddMapping"
					><s:text name="jsp.default_82"/></sj:a>
				    
					<%-- input class="submitbutton" type="submit" value="SAVE" --%>
					
				<ffi:cinclude value1="${MappingDefinition.MappingID}" value2="0" operator="equals">	
					<sj:a 
						id="addMappingSubmit3"
						formIds="mappingFormID3" 
						targets="mappingDiv" 
						button="true" 
						validate="true"
						validateFunction="customValidation"
						onBeforeTopics="customMappingBefore"
						onCompleteTopics="addMappingFormSaveComplete"
						><s:text name="jsp.default_366"/></sj:a>
				</ffi:cinclude>		
				<ffi:cinclude value1="${MappingDefinition.MappingID}" value2="0" operator="notEquals">		
					<sj:a 
						id="editMappingSubmit3"
						formIds="mappingFormID3" 
						targets="mappingDiv" 
						button="true" 
						validate="true"
						validateFunction="customValidation"
						onBeforeTopics="customMappingBefore"
						onCompleteTopics="editMappingFormSaveComplete"
						><s:text name="jsp.default_366"/></sj:a>
				</ffi:cinclude>		
					</td>
				</tr>
			</table>
			</s:form>
		</div>
	</BODY>


<ffi:removeProperty name="CustomMappingErrors"/>

<ffi:cinclude value1="${MappingDefinition.MappingID}" value2="0">
	<ffi:object id="AddMappingDefinition" name="com.ffusion.tasks.fileImport.AddMappingDefinitionTask" scope="session"/>
	<ffi:setProperty name="AddMappingDefinition" property="FailedValidationURL" value="${ValidateURL}"/>
	<ffi:setProperty name="AddMappingDefinition" property="ErrorCollectionName" value="CustomMappingErrors"/>
		<%
			session.setAttribute("FFIAddMappingDefinition", session.getAttribute("AddMappingDefinition"));
        %>
</ffi:cinclude>

<ffi:cinclude value1="${MappingDefinition.MappingID}" value2="0" operator="notEquals">
	<ffi:object id="ModifyMappingDefinition" name="com.ffusion.tasks.fileImport.ModifyMappingDefinitionTask" scope="session"/>
	<ffi:setProperty name="ModifyMappingDefinition" property="FailedValidationURL" value="${ValidateURL}"/>
	<ffi:setProperty name="ModifyMappingDefinition" property="ErrorCollectionName" value="CustomMappingErrors"/>
		<%
			session.setAttribute("FFIModifyMappingDefinition", session.getAttribute("ModifyMappingDefinition"));
        %>
</ffi:cinclude>
<script>
$(function(){
    $("#mappingDefOptions").selectmenu({width:'15em'});
});
</script>
