<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Remove items from the session that are created and used exclusively by the Admin tab. --%>

<ffi:removeProperty name="<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUPS %>"/>
<ffi:removeProperty name="<%= com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP %>"/>
<ffi:removeProperty name="EditBusinessGroup"/>
<ffi:removeProperty name="AddBusinessGroup2"/>
<ffi:removeProperty name="GroupSummaries"/>
<ffi:removeProperty name="GetCumulativeLimits"/>
<ffi:removeProperty name="EditBusinessUser"/>
<ffi:removeProperty name="GroupAccounts"/>
<ffi:removeProperty name="GetAncestorGroupByType"/>
<ffi:removeProperty name="EditAccountPermissions"/>
<ffi:removeProperty name="EditAccountAccessPermissions"/>
<ffi:removeProperty name="EditAccountGroupPermissions"/>
<ffi:removeProperty name="EditAccountGroupAccessPermissions"/>
<ffi:removeProperty name="EditCrossAccountPermissions"/>
<ffi:removeProperty name="ApprovalAdminByBusiness"/>
<ffi:removeProperty name="SetAdministrators"/>
<ffi:removeProperty name="PrimaryBusinessEmployee" />
<ffi:removeProperty name="onCancelGoto"/>
<ffi:removeProperty name="onDoneGoto"/>
<ffi:removeProperty name="CanAdminGroupStr"/>
<ffi:removeProperty name="TempBusEmployee"/>
<ffi:removeProperty name="AdminsSelected"/>
<ffi:removeProperty name="AdminEmployees"/>
<ffi:removeProperty name="AdminGroups"/>
<ffi:removeProperty name="NonAdminEmployees"/>
<ffi:removeProperty name="NonAdminGroups"/>
<ffi:removeProperty name="tempAdminEmps"/>
<ffi:removeProperty name="adminMembers"/>
<ffi:removeProperty name="adminIds"/>
<ffi:removeProperty name="userIds"/>
<ffi:removeProperty name="groupIds"/>
<ffi:removeProperty name="userMembers"/>
<ffi:removeProperty name="NonAdminEmployeesCopy"/>
<ffi:removeProperty name="AdminEmployeesCopy"/>
<ffi:removeProperty name="NonAdminGroupsCopy"/>
<ffi:removeProperty name="AdminGroupsCopy"/>
<ffi:removeProperty name="adminMembersCopy"/>
<ffi:removeProperty name="userMembersCopy"/>
<ffi:removeProperty name="tempAdminEmpsCopy"/>
<ffi:removeProperty name="groupIdsCopy"/>
<ffi:removeProperty name="GroupDivAdminEditedCopy"/>
<ffi:removeProperty name="SetAdministrators"/>

<ffi:removeProperty name="GetEntitlementGroup"/>
<ffi:removeProperty name="CanDeleteEntitlementGroup"/>
<ffi:removeProperty name="DeleteBusinessGroup"/>
<ffi:removeProperty name="EditBusinessGroup"/>
<ffi:removeProperty name="CheckEntitlementByGroupCB"/>
<ffi:removeProperty name="GetEntitlementObjectID"/>
<ffi:removeProperty name="GetAccountsForGroup"/>
<ffi:removeProperty name="admin_init_touched"/>
<ffi:removeProperty name="GetAccountsForUser"/>
<ffi:removeProperty name="CanAdminister"/>

<ffi:removeProperty name="DescendantItemDivision"/>
<ffi:removeProperty name="Supervisor"/>
<ffi:removeProperty name="EntitlementAdmins"/>

<ffi:removeProperty name="SetBusinessEmployee"/>
<ffi:removeProperty name="ModifyBusiness"/>
<ffi:removeProperty name="EditBusiness"/>
<ffi:removeProperty name="Country_List"/>
<ffi:removeProperty name="Country_Resource"/>
<ffi:removeProperty name="GetLanguageList"/>

<ffi:removeProperty name="EntitlementGroupMember"/>
<ffi:removeProperty name="GetSupervisorsFor"/>
<ffi:removeProperty name="BusinessEmployees"/>
<ffi:removeProperty name="NewBusinessEmployee"/>
<ffi:removeProperty name="GroupAdminEmployees"/>

<ffi:removeProperty name="ParentsAccounts"/>
<ffi:removeProperty name="MyAccounts"/>
<ffi:removeProperty name="BusEmpAccounts"/>
<ffi:removeProperty name="FilteredAccounts"/>
<ffi:removeProperty name="EntitledAccounts"/>

<ffi:removeProperty name="CheckForRedundantLimits"/>
<ffi:removeProperty name="_secEntCache"/>
<ffi:removeProperty name="Entitlement_Restricted_list"/>
<ffi:removeProperty name="SearchAcctsByNameNumType"/>
<ffi:removeProperty name="CheckEntitlementByGroup"/>
<ffi:removeProperty name="FILTERED_ACHCOMPANIES"/>
<ffi:removeProperty name="ActiveACHCompanies"/>

<ffi:removeProperty name="ModifyACHCompanyUser"/>
<ffi:removeProperty name="EditACHCompanyPermissions"/>
<ffi:removeProperty name="SearchACHCompaniesByNameID"/>
<ffi:removeProperty name="EditGroup_AccountID"/>
<ffi:cinclude value1="${EntGroupItem1.GroupName}" value2="" operator="notEquals">
	<ffi:removeProperty name="${EntGroupItem1.GroupName}Accounts"/>
</ffi:cinclude>
<ffi:removeProperty name="EditCrossAccountPermissions"/>
<ffi:removeProperty name="CheckEntitlementByMember"/>
<ffi:removeProperty name="GetRestrictedACHCompaniesForGroup"/>
<ffi:removeProperty name="CheckOperationEntitlement"/>
<ffi:removeProperty name="EditLocationPermissions"/>
<ffi:removeProperty name="Locations"/>
<ffi:removeProperty name="Location"/>
<ffi:removeProperty name="GetLocations"/>
<ffi:removeProperty name="AdminInitMap" />

<ffi:removeProperty name="EditACHCompanyAccess"/>
<ffi:removeProperty name="ApprovalAdminByBusiness"/>
<ffi:removeProperty name="CheckForRedundantACHLimits"/>
<ffi:removeProperty name="crossACHEntLists"/>
<ffi:removeProperty name="perACHEntLists"/>
<ffi:removeProperty name="limitInfoList"/>
<ffi:removeProperty name="ACHCompanyID"/>
<ffi:removeProperty name="ACHCompany"/>
<ffi:removeProperty name="perBatchMax"/>
<ffi:removeProperty name="dailyMax"/>

<%-- Approval Workflow Cleanup --%>
<ffi:removeProperty name="ApprovalsLevels"/>
<ffi:removeProperty name="ApprovalsLevel"/>
<ffi:removeProperty name="_groupAncestor"/>
<ffi:removeProperty name="_groupList"/>
<ffi:removeProperty name="GetApprovalsLevel"/>
<ffi:removeProperty name="AddApprovalsLevelObject"/>
<ffi:removeProperty name="GetWorkflowLevels"/>
<ffi:removeProperty name="ApprovalsChainItemUserName"/>
<ffi:removeProperty name="ApprovalsChainItemsGroupNames"/>
<ffi:removeProperty name="GetWorkflowChainItems"/>
<ffi:removeProperty name="AddWorkflowLevel"/>
<ffi:removeProperty name="ApprovalsChainItemsNames"/>

<ffi:removeProperty name="ApprovalsGroups"/>
<ffi:removeProperty name="ApprovalsGroup"/>

<ffi:removeProperty name="ReportNamesAndCategories"/>

<ffi:removeProperty name="EditWireTemplatePermissions"/>

<ffi:removeProperty name="PersonalBanker"/>
<ffi:removeProperty name="AccountRep"/>
<ffi:removeProperty name="Account"/>
<ffi:removeProperty name="ModifyAutoEntitleSettings"/>

<ffi:removeProperty name="GetStatesForCountry"/>
<ffi:removeProperty name="OldBusiness"/>

<ffi:removeProperty name="ModifyAccount"/>
<ffi:removeProperty name="AccountContact"/>

<ffi:removeProperty name="GetCumulativeSettings"/>
<ffi:removeProperty name="PerTransactionLimits"/>
<ffi:removeProperty name="PerMonthLimits"/>
<ffi:removeProperty name="PerDayLimits"/>
<ffi:removeProperty name="PerWeekLimits"/>

<ffi:removeProperty name="AccountGrp"/>
<ffi:removeProperty name="MyAccountGroups"/>
<ffi:removeProperty name="ParentsAccountGroups"/>

<ffi:removeProperty name="AccountEntitlementsWithLimits"/>
<ffi:removeProperty name="AccountEntitlementsWithoutLimits"/>
<ffi:removeProperty name="NonAccountEntitlementsWithLimits"/>
<ffi:removeProperty name="NonAccountEntitlementsWithoutLimits"/>
<ffi:removeProperty name="NonAccountEntitlementsMerged"/>
<ffi:removeProperty name="AccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:removeProperty name="AccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:removeProperty name="NonAccountEntitlementsWithLimitsBeforeFilter"/>
<ffi:removeProperty name="NonAccountEntitlementsWithoutLimitsBeforeFilter"/>
<ffi:removeProperty name="NonAccountEntitlementsMergedBeforeFilter"/>

<ffi:removeProperty name="HandleAccountRowDisplay"/>
<ffi:removeProperty name="HandleAccountAccessRowDisplay"/>
<ffi:removeProperty name="Entitlement_Limits"/>

<%-- Collection created by AdminSummaryGridAction--%>
<ffi:removeProperty name="AdminAccountSummaryCollection"/>
<ffi:removeProperty name="EntGroupItem1"/>

<ffi:removeProperty name="WireTemplate"/>
<ffi:removeProperty name="SetWireTransfer"/>
<ffi:removeProperty name="AllWireTemplates"/>

<ffi:removeProperty name="EditBusinessEmployee"/>

<s:include value="checkbox-limit-cleanup.jsp"/>

<%-- Clean for DA mode also. --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="1" operator="equals">
	<ffi:removeProperty name="DA_GRANT_ENTITLEMENTS" />
	<ffi:removeProperty name="DA_REVOKE_ENTITLEMENTS" />
	<ffi:removeProperty name="DA_ROOT_ENTITLEMENT" />
	<ffi:removeProperty name="PROCESS_TREE_FLAG" />
	<ffi:removeProperty name="ENTITLEMENT_TREE_GROUPS" />
	<ffi:removeProperty name="ENTITLEMENT_TREE_MEMBERS_MAP" />
	<ffi:removeProperty name="RootGroupId" />
	<ffi:removeProperty name="GROUP_FILTER" />
	<ffi:removeProperty name="AddBAIExportSettingsToDA" />
	<ffi:removeProperty name="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.OLD_BAIEXPORT_SETTINGS %>" />
	<ffi:removeProperty name="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.NEW_BAIEXPORT_SETTINGS %>" />
	<ffi:removeProperty name="<%=com.ffusion.tasks.dualapproval.AddBAIExportSettingsToDA.DA_BAIEXPORT_SETTINGS %>" />
	<%-- Following properties are set in jsp/index.jsp and are used in user/index.jsp --%>
	<ffi:removeProperty name="DACompanyCount"/>
	<ffi:removeProperty name="DADivisionCount"/>
	<ffi:removeProperty name="DAGroupCount"/>
	<ffi:removeProperty name="DAUserCount"/>
	<ffi:removeProperty name="<%=com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants.CATEGORY_SESSION_ENTITLEMENT%>"/>
</ffi:cinclude>

<%-- Requires Approval Processing --%>
<ffi:cinclude value1="${REQUIRES_APPROVAL}" value2="true" operator="equals">
	<ffi:removeProperty name="EditRequiresApproval" />
	<ffi:removeProperty name="CheckRequiresApproval" />
</ffi:cinclude>
