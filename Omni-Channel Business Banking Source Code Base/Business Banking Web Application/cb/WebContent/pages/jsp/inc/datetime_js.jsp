<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- This is a generic dynamic java script to get the date and time --%>

<ffi:object id="DateRangeData" name="com.ffusion.tasks.util.GetDatesFromDateRange"/>

<%-- If you need "4 Weeks Ending" range, please specify the date or current date will be used --%>

<script type="text/javascript"><!--
function setDatesByRange( dateField1, dateField2, rangeCode )
{
	/*
	* Range code:
		Unknown: -1, do nothing
		This month to date: 0
		Yesterday: 1
		Last week: 2
		Last week to date: 3
		Last 4 weeks: 4
		Last Month: 5
		Last month to date: 6
		Next week: 7
		Next 4 weeks: 8
		Current week: 9
		Current 4 weeks: 10
		Next month: 11
		Today: 12
		<%--
		4 weeks ending: 13
		--%>
		Current 7 days: 14
		Last 7 days: 15
		Last 30 days: 16
		
	*/
	
	if( isNaN( rangeCode ) ) return;
	
	
	//Set the start/end dates based on selected date range code
	switch( parseInt( rangeCode ) ) {
		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_THIS_MONTH_TO_DATE %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_THIS_MONTH_TO_DATE %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;

		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_YESTERDAY %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_YESTERDAY %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;

		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_WEEK %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_WEEK %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_WEEK_TO_DATE %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_WEEK_TO_DATE %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_4_WEEKS %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_4_WEEKS %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_MONTH  %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_MONTH  %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_MONTH_TO_DATE %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_MONTH_TO_DATE %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_NEXT_WEEK %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_NEXT_WEEK %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_NEXT_4_WEEKS %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_NEXT_4_WEEKS %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_CURRENT_WEEK %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_CURRENT_WEEK %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case  <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_CURRENT_4_WEEKS %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_CURRENT_4_WEEKS %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case  <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_NEXT_MONTH %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_NEXT_MONTH %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case  <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_TODAY %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_TODAY %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
<%--	We cannot make this to work as expected with a static javascript generated at runtime.
		The following code only generates the 4-week period ending on current date (default).
																		-- Xin Tan
		case <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_FOUR_WEEKS_ENDING %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_FOUR_WEEKS_ENDING %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
--%>
		
		case  <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_CURRENT_7_DAYS %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_CURRENT_7_DAYS %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case  <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_7_DAYS %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_7_DAYS %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		case  <%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_30_DAYS %>:
		<ffi:setProperty name="DateRangeData" property="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.VAL_LAST_30_DAYS %>"/>
		<ffi:process name="DateRangeData"/>
		dateField1.value="<ffi:getProperty name='DateRangeData' property='StartDate'/>";
		dateField2.value="<ffi:getProperty name='DateRangeData' property='EndDate'/>";
		break;
		
		default:
		// Leave the data as are
	}
}
// --></script>

<ffi:removeProperty name="DateRangeData"/>
