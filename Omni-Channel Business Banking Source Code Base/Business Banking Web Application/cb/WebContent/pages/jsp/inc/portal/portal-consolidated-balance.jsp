<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<% session.setAttribute("ConsolidatedBalanceDataClassification",request.getParameter("ConsolidatedBalanceDataClassification"));%>
<% session.setAttribute("AccountDisplayCurrencyCode",request.getParameter("AccountDisplayCurrencyCode"));%>
<script language="javascript">
    ns.account.linkDepositAccount = function(accountGroupid){
        var urlString = "/cb/pages/jsp/accountsbygroup.jsp";
        $.ajax({
			url: urlString,	
			type: "post",
			data: "ccountGroupID="+accountGroupid+"&AccountBalancesDataClassification=<ffi:getProperty name="AccountBalancesDataClassification"/>&CSRF_TOKEN=<ffi:getProperty name="CSRF_TOKEN"/>",
			success: function(data){
				$('#consolidatedBalanceSummary').html(data);
		       }
		    });
        }
</script>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CONSOLIDATED_BALANCE %>">
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
<%-- filter any accounts the user is not able to view (entitlements required are summary and previous day --%>
<ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="session"/>
<%-- This is a detail page so we will filter accounts on the detail view entitlement --%>
<ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
<ffi:setProperty name="AccountEntitlementFilterTask" property="FilteredAccountsName" value="EntitlementFilteredAccounts"/>

<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
	<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="ConsolidatedBalanceDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>

<%-- set default data classification as "P" --%>
<ffi:cinclude value1="" value2="${ConsolidatedBalanceDataClassification}" operator="equals">
 <ffi:setProperty name="ConsolidatedBalanceDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
</ffi:cinclude>

<ffi:cinclude value1="${ConsolidatedBalanceDataClassification}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${ConsolidatedBalanceDataClassification}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY%>" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${ConsolidatedBalanceDataClassification}" value2="" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>"/>
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>"/>
</ffi:cinclude>
<%
session.setAttribute("FFIAccountEntitlementFilterTask", session.getAttribute("AccountEntitlementFilterTask"));
%>

<ffi:process name="AccountEntitlementFilterTask"/>

<%-- to make sure that clicking an account group will carry through the data classification, we copy the variable --%>
<ffi:setProperty name="AccountBalancesDataClassification" value="${ConsolidatedBalanceDataClassification}"/>


<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="notEquals">

<ffi:object id="GetSummariesForAccountDate" name="com.ffusion.tasks.banking.GetSummariesForAccountDateFromDC" scope="session"/>
<ffi:setProperty name="GetSummariesForAccountDate" property="AccountsName" value="EntitlementFilteredAccounts"/>
<ffi:setProperty name="GetSummariesForAccountDate" property="DataClassification" value="${ConsolidatedBalanceDataClassification}"/>
<ffi:setProperty name="GetSummariesForAccountDate" property="SummariesName" value="ConsolidatedBalanceSummaries"/>
<ffi:setProperty name="GetSummariesForAccountDate" property="BatchSize" value="50"/>
<%
   session.setAttribute("FFIGetSummariesForAccountDate", session.getAttribute("GetSummariesForAccountDate"));
%>


<ffi:object id="GetHistoriesForAccountDate" name="com.ffusion.tasks.banking.GetHistoriesForAccountDateFromDC" scope="session"/>
<ffi:setProperty name="GetHistoriesForAccountDate" property="AccountsName" value="EntitlementFilteredAccounts"/>
<ffi:setProperty name="GetHistoriesForAccountDate" property="DataClassification" value="${ConsolidatedBalanceDataClassification}"/>
<ffi:setProperty name="GetHistoriesForAccountDate" property="HistoriesName" value="ConsolidatedBalanceHistories"/>
<ffi:setProperty name="GetHistoriesForAccountDate" property="BatchSize" value="50"/>
<%
   session.setAttribute("FFIGetHistoriesForAccountDate", session.getAttribute("GetHistoriesForAccountDate"));
%>                                                                             


<ffi:object id="FillAccountBalances" name="com.ffusion.tasks.accounts.FillAccountBalances" scope="session"/>
<ffi:setProperty name="FillAccountBalances" property="AccountsName" value="EntitlementFilteredAccounts"/>
<ffi:setProperty name="FillAccountBalances" property="SummariesName" value="ConsolidatedBalanceSummaries"/>
<ffi:setProperty name="FillAccountBalances" property="HistoriesName" value="ConsolidatedBalanceHistories"/>
<ffi:setProperty name="FillAccountBalances" property="DataClassification" value="${ConsolidatedBalanceDataClassification}"/>
<%
   session.setAttribute("FFIFillAccountBalances", session.getAttribute("FillAccountBalances"));
%>



<ffi:cinclude value1="${GetTargetCurrencies}" value2="" operator="equals">

<ffi:object name="com.ffusion.tasks.fx.GetTargetCurrencies" id="GetTargetCurrencies" scope="session"/>
<%
   session.setAttribute("FFIGetTargetCurrencies", session.getAttribute("GetTargetCurrencies"));
%>

</ffi:cinclude>

<%-- If displaycurrency is not set, set it to SecureUser's base currency --%>

<ffi:cinclude value1="${AccountDisplayCurrencyCode}" value2="" operator="equals">
<ffi:setProperty name="AccountDisplayCurrencyCode"  value="${SecureUser.BaseCurrency}"/>
</ffi:cinclude>

<ffi:cinclude value1="${GetDisplaySummariesForAccount}" value2="" operator="equals">
<ffi:object name="com.ffusion.tasks.banking.GetDisplaySummariesForAccount" id="GetDisplaySummariesForAccount" scope="session"/>
</ffi:cinclude>
<ffi:setProperty name="GetDisplaySummariesForAccount" property="DisplayCurrency"  value="${AccountDisplayCurrencyCode}"/>  

				<ffi:setProperty name="GetDisplaySummariesForAccount" property="AccountsName" value="EntitlementFilteredAccounts"/>
				<ffi:setProperty name="GetDisplaySummariesForAccount" property="SummariesName" value="ConsolidatedBalanceSummaries"/>
				<%
                    session.setAttribute("FFIGetDisplaySummariesForAccount", session.getAttribute("GetDisplaySummariesForAccount"));
                 %>

<ffi:removeProperty name="GetSummariesForAccountDate"/>
<ffi:removeProperty name="GetDisplaySummariesForAccount"/>
<ffi:removeProperty name="ConsolidatedBalanceSummaries"/>
<ffi:removeProperty name="GetHistoriesForAccountDate"/>
<ffi:removeProperty name="ConsolidatedBalanceHistories"/>
<ffi:removeProperty name="FillAccountBalances"/>

</ffi:cinclude> 
<%-- End the filtered account collection has some items in it --%>

<script language="JavaScript" type="text/javascript">
	$(function(){
         var urlString = "<ffi:urlEncrypt url='/cb/pages/jsp/account/consolidatedbalance_account_summaries_grid.jsp?dataClassfication=${ConsolidatedBalanceDataClassification}&currencyCode=${AccountDisplayCurrencyCode}'/>";
         ns.account.refreshConsolidatedSummaryForm(urlString);
				});
</script>          

<br>
<ffi:removeProperty name="EntitlementFilteredAccounts"/>
<ffi:removeProperty name="AccountDisplayCurrencyCode"/>
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
</ffi:cinclude>
                
