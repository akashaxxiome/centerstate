<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.exttransfers.ExtTransferAccountDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<script language="JavaScript" type="text/javascript"><!--
	/**
	* Transfer From/To drop-down javascript.  Will remove the selected FROM item from the TO drop-down.
	* Will also remove the External Transfer Accounts from the TO if the selected item in the FROM is External
	* Will do the same thing for the FROM account (remove selected TO item). Will also remove accounts in
	* the to list if multi-currency transfers is not entitled so that only same currency transfers are allowed.
    * Will also remove nonUSD accounts from opposite list when an ACH account is selected.
	*/
var CURRENCY_MARKER = ":";
var fromList = null;
var fromExternalIndex = -1;
var toList = null;
var toExternalIndex = -1;
	function reloadFromToAccounts( fromSel, toSel )
	{
		if (fromList == null)       // copy all of the default items
		{
            <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>" >
            removeSingleCurrencies(fromSel, toSel);
            </ffi:cinclude>
            fromList = new Array();
			for (var i=0; i < fromSel.length; i++)
			{
			  fromList[ i ]=fromSel.options[ i ].value + "~" + fromSel.options[ i ].text;
			  if (fromSel.options[ i ].text.indexOf('---') >= 0)
				fromExternalIndex = i;
			}
        }
		if (toList == null)       // copy all of the default items
		{
            <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>" >
            removeSingleCurrencies(toSel, fromSel);
            </ffi:cinclude>
            toList = new Array();
			for (var i=0; i < toSel.length; i++)
			{
			  toList[ i ]=toSel.options[ i ].value + "~" + toSel.options[ i ].text;
			  if (toSel.options[ i ].text.indexOf('---') >= 0)
				toExternalIndex = i;
			}
		}
		// now, figure out which one is selected on the From item
		fromSelectedText = fromSel.options[ fromSel.selectedIndex ].text;
		fromSelectedValue = fromSel.options[ fromSel.selectedIndex ].value;
		fromIsExternal = 0; if (fromSel.selectedIndex >= fromExternalIndex && fromExternalIndex != -1) fromIsExternal = 1;
		toSelectedText = toSel.options[ toSel.selectedIndex ].text;
		toSelectedValue = toSel.options[ toSel.selectedIndex ].value;
		toIsExternal = 0; if (toSel.selectedIndex >= toExternalIndex && toExternalIndex != -1) toIsExternal = 1;

        //clear out entries in the lists
		while( fromSel.options.length > 0 ){
			fromSel.options[ 0 ] = null;
		}
		while( toSel.options.length > 0 ){
			toSel.options[ 0 ] = null;
		}

		// disable account
		var newCount = -1;
        for (var i=0; i < toList.length; i++)
		{
			var loc = toList[ i ].indexOf('~');
			var value = toList[ i ].substring(0,loc);
			var text = toList[ i ].substring(loc+1);
			if (fromSelectedValue != "")
			{
				if (fromSelectedValue == value)
					continue;           // don't allow the "from selected" in the to list
				if (fromIsExternal > 0 && i >= toExternalIndex && toExternalIndex != -1)
					continue;       // FROM is External, don't add any external accounts to TO
                if (isACH(fromSelectedValue) && isNonUSD(value))
                    continue;       // FROM is ACH, don't add any non-USD acccounts to TO
                if (isNonUSD(fromSelectedValue) && isACH(value))
                    continue;       // FROM is non-USD account, don't add any ACH accounts to TO
                <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>" >
                if (differentCurrency(fromSelectedValue, value))
                    continue;  // don't include accounts of different currency in to list
                </ffi:cinclude>
            }
			var newOpt = new Option(text, value);
			//value may has been changed upon submit via ajax (currency code being removed)
			//so value comparsion will fail
			//if (toSelectedValue == value && toSelectedText == text)
			if (toSelectedText == text)
				newOpt.selected = true;     // reselect the previously selected option
			newCount++;
			toSel.options[ newCount ] = newOpt;
		}
        <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>" >
        toSelectedValue = toSel.options[toSel.selectedIndex].value;
        </ffi:cinclude>

        newCount = -1;
		for (var i=0; i < fromList.length; i++)
		{
			var loc = fromList[ i ].indexOf('~');
			var value = fromList[ i ].substring(0,loc);
			var text = fromList[ i ].substring(loc+1);
			if (toSelectedValue != "")
			{
                if (toSelectedValue == value)
					continue;       // don't allow the "to selected" in the from list
				if (toIsExternal > 0 && i >= fromExternalIndex && fromExternalIndex != -1)
					continue;       // TO is External, don't add any external accounts to FROM
                if (isACH(toSelectedValue) && isNonUSD(value))
                    continue;       // TO is ACH External, don't add any non-USD acccounts to FROM
                if (isNonUSD(toSelectedValue) && isACH(value))
                    continue;       // TO is non-USD account, don't add any ACH accounts to FROM
            }
			var newOpt = new Option(text, value);
			//value may has been changed upon submit via ajax (currency code being removed)
			//so value comparsion will fail
			//if (fromSelectedValue == value && fromSelectedText == text)
			if (fromSelectedText == text)
				newOpt.selected = true;     // reselect the previously selected option
			newCount++;
			fromSel.options[ newCount ] = newOpt;
		}
    }

function isACH(value)
{
    var currencyIndex = value.indexOf(CURRENCY_MARKER);
    if (currencyIndex == -1)
        return false;

    currencyIndex = value.indexOf(CURRENCY_MARKER, currencyIndex+1);
    if (currencyIndex == -1)
        return false;

    var achEntitled = false;
    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.EXTERNAL_TRANSFERS_VIA_ACH %>" >
        achEntitled = true;
    </ffi:cinclude>

    if (achEntitled && value.substr(currencyIndex+1) == "<%= ExtTransferAccountDefines.ETF_ACCTBANKRTNTYPE_FEDABA %>")
        return true;

    return false;
}

function isNonUSD(value)
{
    var currency = getCurrency(value);
    if (currency != null && currency != "USD")
        return true;
    else
        return false;
}

function getCurrency(value)
{
    if (value == null)
        return null;

    var currencyIndex = value.indexOf(CURRENCY_MARKER);

    if (currencyIndex == -1)
        return null;

    var secondIndex = value.indexOf(CURRENCY_MARKER, currencyIndex+1);

    if (secondIndex != -1)
        return value.substring(currencyIndex+1, secondIndex);
    else
        return value.substr(currencyIndex+1);
}

function removeCurrencyFromAcctID(fromSel, toSel)
{
    var fromAcctSelected = fromSel.options[fromSel.selectedIndex];
    var toAcctSelected = toSel.options[toSel.selectedIndex];

    if (fromAcctSelected.value != null)
    {
        var currencyIndex = fromAcctSelected.value.indexOf(CURRENCY_MARKER);
        if (currencyIndex != -1)
            fromAcctSelected.value = fromAcctSelected.value.substring(0, currencyIndex);
    }

    if (toAcctSelected.value != null)
    {
        var currencyIndex = toAcctSelected.value.indexOf(CURRENCY_MARKER);
        if (currencyIndex != -1)
            toAcctSelected.value = toAcctSelected.value.substring(0, currencyIndex);
    }
}

function removeCurrencyFromAcctIDs(fromID, toID, num)
{
    for (var i=0; i < num; i++)
    {
        removeCurrencyFromAcctID(document.getElementById(fromID+i), document.getElementById(toID+i));
    }
}

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>" >
function removeSingleCurrencies(sourceAccts, compareAccts)
{
	if (sourceAccts.length > 0) {
		for (var i=sourceAccts.length - 1; i >= 0 ; i--)
		{
			var currencyInBothLists = false;
			var sourceAcctValue = sourceAccts.options[i].value;
			var currLoc = sourceAcctValue.indexOf(CURRENCY_MARKER);

			if (currLoc != -1)
			{
				var sourceAcctID = sourceAcctValue.substring(0, currLoc);
				var sourceAcctCurrency = getCurrency(sourceAcctValue);

				for (var j=0; j < compareAccts.length; j++)
				{
					var compareAcctValue = compareAccts.options[j].value;
					currLoc = compareAcctValue.indexOf(CURRENCY_MARKER);

					if (currLoc != -1)
					{
						var compareAcctID = compareAcctValue.substring(0, currLoc);
						var compareAcctCurrency = getCurrency(compareAcctValue);

						if ((sourceAcctCurrency == compareAcctCurrency) && (sourceAcctID != compareAcctID))
						{
							currencyInBothLists = true;
							break;
						}
					}
				}

				if (!currencyInBothLists)
					sourceAccts.remove(i);
			}
		}
	}
}

function differentCurrency(selectedValue, currentValue)
{
    if (selectedValue == "" || currentValue == "")
        return false;

    var selectedCurrency = getCurrency(selectedValue);
    var currentCurrency = getCurrency(currentValue);

    if (selectedCurrency != currentCurrency)
        return true;
    else
        return false;
}
</ffi:cinclude>

function getCurrencies(fromSel, toSel)
{
	var currencies = new Array(2);
	if(fromSel.selectedIndex != -1  && toSel.selectedIndex!= -1){
		var fromAcctValue = fromSel.options[fromSel.selectedIndex].value;
		var toAcctValue = toSel.options[toSel.selectedIndex].value;
		currencies[0] = getCurrency(fromAcctValue);
		currencies[1] = getCurrency(toAcctValue);
	}
    return currencies;
}

// --></script>
