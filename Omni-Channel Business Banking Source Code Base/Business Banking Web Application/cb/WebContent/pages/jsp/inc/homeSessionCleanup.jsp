<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Remove items from the session that are created and used exclusively by the Home tab. --%>

<%-- Objects only used in submenus of Home tab --%>
<ffi:removeProperty name="Alerts"/>
<ffi:removeProperty name="AddAlert"/>
<ffi:removeProperty name="ModifyAlert"/>
<ffi:removeProperty name="DeleteAlert"/>
<ffi:removeProperty name="Alert"/>
<ffi:removeProperty name="ProcessAddEditAlert"/>
<ffi:removeProperty name="ModifyAlertStock"/>
<ffi:removeProperty name="AlertStock"/>

<ffi:removeProperty name="SendMessage"/>
<ffi:removeProperty name="DeleteMessage"/>
<ffi:removeProperty name="alerts_init_touched"/>

<ffi:removeProperty name="CalculatorsMaster"/>	<%-- Created by PortalInit --%>