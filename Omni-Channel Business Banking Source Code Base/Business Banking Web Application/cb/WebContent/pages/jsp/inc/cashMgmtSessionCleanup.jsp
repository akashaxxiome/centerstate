<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Remove items from the session that are created and used exclusively by the Cash Management tab. --%>

<ffi:setProperty name="ppayBuildTouched" value=""/>

<ffi:removeProperty name="LOCKBOX_SUMMARIES"/>
<ffi:removeProperty name="LockboxPagedTransactions"/>
<ffi:removeProperty name="LockboxPrevTransactions"/>
<ffi:removeProperty name="LockboxNextTransactions"/>
<ffi:removeProperty name="GetAccountData"/>
<ffi:removeProperty name="GetIssuesForAccount"/>
<ffi:removeProperty name="GetDisbursementSummariesForAccount"/>
<ffi:removeProperty name="CreateDecisions"/>
<ffi:removeProperty name="CreateCheckRecords"/>
<ffi:removeProperty name="DisbursementPresentmentSummaries"/>
<ffi:removeProperty name="DisbursementSummaries"/>
<ffi:removeProperty name="PPayCheckRecords"/>
<ffi:removeProperty  name="SubmitCheckRecords"/>
<ffi:removeProperty  name="SearchImage"/>
<ffi:removeProperty  name="LockboxSummaries"/>

<ffi:removeProperty name="PPaySummaries"/>
<ffi:removeProperty name="PPayIssues"/>

<ffi:removeProperty name="PPayIssue"/>
<ffi:removeProperty name="ImageRequest"/>
<ffi:removeProperty name="ImageResults"/>
<ffi:removeProperty name="CheckedImages"/>
<ffi:removeProperty name="AvailableImages"/>
<ffi:removeProperty name="CashDisplayCurrencyCode"/>
