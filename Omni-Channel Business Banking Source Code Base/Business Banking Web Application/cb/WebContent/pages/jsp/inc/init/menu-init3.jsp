<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="nav_menu_sub2" value=""/>
<ffi:setProperty name="nav_menu_sub3" value=""/>

<ffi:cinclude value1="${PermissionsWizard}" value2="TRUE" operator="notEquals">
	<ffi:removeProperty name="CurrentWizardPage"/>
</ffi:cinclude>
<ffi:cinclude value1="${PermissionsWizard}" value2="TRUE" operator="equals">
    <ffi:setProperty name="nav_menu_sub2" value="nav_menu_sub_permissions.jsp"/>
    <ffi:removeProperty name="PermissionsWizard"/>
    <ffi:cinclude value1="${PermissionsWizardSubMenu}" value2="TRUE" operator="equals">
    	<ffi:setProperty name="nav_menu_sub3" value="nav_menu_sub2_permissions.jsp"/>
    	<ffi:removeProperty name="PermissionsWizardSubMenu"/>
    </ffi:cinclude>
</ffi:cinclude>
