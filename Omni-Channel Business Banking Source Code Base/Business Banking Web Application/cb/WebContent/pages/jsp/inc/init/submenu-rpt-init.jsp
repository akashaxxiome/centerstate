<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%--build the sub-menu string for the reports directory--%>

<ffi:object id="CheckReportEntitlement" name="com.ffusion.tasks.reporting.CheckReportEntitlement"/>

<ffi:setProperty name="nav_default_reports" value=""/>
<ffi:setProperty name="AccountReportsDenied" value="true"/>
<ffi:setProperty name="CashReportsDenied" value="true"/>
<ffi:setProperty name="PaymentReportsDenied" value="true"/>
<ffi:setProperty name="AuditReportsDenied" value="true"/>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_DEPOSIT_DETAIL %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_TRANSACTION_DETAIL %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CREDIT_RPT %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_DEBIT_RPT %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_BALANCE_SUMMARY %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_BALANCE_DETAIL %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_BALANCE_SUMMARY_AND_DETAIL %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CUSTOM_SUMMARY %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="AccountReportMenuAvailable" value="FALSE"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MGMT_REPORTING %>" >
	<ffi:setProperty name="AccountReportMenuAvailable" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.BANK_REPORTING %>" >
	<ffi:setProperty name="AccountReportMenuAvailable" value="TRUE"/>
</ffi:cinclude>

<ffi:setProperty name="displayReportAcctMgmt" value="false"/>
<ffi:cinclude value1="${AccountReportMenuAvailable}" value2="TRUE" operator="equals">
	<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
		<ffi:setProperty name="displayReportAcctMgmt" value="true"/>
		<ffi:setProperty name="AccountReportsDenied" value="false"/>
		<ffi:cinclude value1="${nav_default_reports}" value2="" operator="equals">
			<ffi:setProperty name="nav_default_reports" value="pages/jsp/reports/index.jsp?Initialize=true?subpage=acctmgmt" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_CASH_FLOW %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_CASH_FLOW_FORECAST %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_BALANCE_SHEET_SUM %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_BALANCE_SHEET %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.accounts.CashMgmtRptConsts.RPT_TYPE_GENERAL_LEDGER %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.lockbox.LockboxRptConsts.RPT_TYPE_LOCKBOX_SUMMARY %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.lockbox.LockboxRptConsts.RPT_TYPE_LOCKBOX_DEPOSIT_REPORT %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.lockbox.LockboxRptConsts.RPT_TYPE_DEPOSIT_ITEM_SEARCH %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_ISSUE_SUMMARY %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_DECISION_HISTORY %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.positivepay.PPayRptConsts.RPT_TYPE_REVERSE_POSITIVE_PAY_DECISION_HISTORY %>"/>

<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="CashReportMenuAvailable" value="FALSE"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_FLOW %>" >
	<ffi:setProperty name="CashReportMenuAvailable" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCKBOX %>" >
	<ffi:setProperty name="CashReportMenuAvailable" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>" >
	<ffi:setProperty name="CashReportMenuAvailable" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
	<ffi:setProperty name="CashReportMenuAvailable" value="TRUE"/>
</ffi:cinclude>

<ffi:setProperty name="CashReportSubMenuPPay" value="FALSE"/>
<ffi:setProperty name="CashReportSubMenuRPPay" value="FALSE"/>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>" >
	<ffi:setProperty name="CashReportSubMenuPPay" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
	<ffi:setProperty name="CashReportSubMenuRPPay" value="TRUE"/>
</ffi:cinclude>

<ffi:setProperty name="displayReportCashMgmt" value="false"/>
<ffi:cinclude value1="${CashReportMenuAvailable}" value2="TRUE" operator="equals">
	<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
		<ffi:setProperty name="displayReportCashMgmt" value="true"/>
		<ffi:setProperty name="CashReportsDenied" value="false"/>

		<ffi:cinclude value1="${nav_default_reports}" value2="" operator="equals">
			<ffi:setProperty name="nav_default_reports" value="pages/jsp/reports/index.jsp?Initialize=true&subpage=cashmgmt" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_COMPLETED_ACH_PMTS %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_PMT_HISTORY %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_UNPROCESSED_PMT %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_EXPENSE_BY_PAYEE_SUMMARY %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.billpay.PaymentReportConsts.RPT_TYPE_EXPENSE_BY_PAYEE_DETAIL %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_TOTAL_TAX_PMTS %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_TOTAL_CHILD_SUPPORT_PMTS %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_TRANSFER_HISTORY %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_TRANSFER_DETAIL %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.banking.BankingReportConsts.RPT_TYPE_PENDING_TRANSFER %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_COMPLETED_WIRES %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_FAILED_WIRES %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_INPROCESS_WIRES %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_BY_STATUS %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_BY_SOURCE %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_BY_TEMPLATE %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.wiretransfers.WireReportConsts.RPT_TYPE_TEMPLATE_WIRES %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.cashcon.CashConReportConsts.RPT_TYPE_CASH_CON_ACTIVITY %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.cashcon.CashConReportConsts.RPT_TYPE_INACTIVE_DIVISIONS %>"/>

<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="displayReportTransPay" value="false"/>
<ffi:cinclude value1="${PaymentMenuDisplayed}" value2="TRUE" operator="equals">
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
<ffi:setProperty name="displayReportTransPay" value="true"/>
<ffi:setProperty name="PaymentReportsDenied" value="false"/>

    <ffi:cinclude value1="${nav_default_reports}" value2="" operator="equals">
	<ffi:setProperty name="nav_default_reports" value="pages/jsp/reports/index.jsp?Initialize=true&subpage=pandt" URLEncrypt="true"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${sub_menu_reports}" value2="" operator="equals">
	<ffi:setProperty name="TagToAdd" value=''/>
    </ffi:cinclude>
    <ffi:cinclude value1="${sub_menu_reports}" value2="" operator="notEquals">
	<ffi:setProperty name="TagToAdd" value='${BarImageTag}'/>
    </ffi:cinclude>
    <ffi:cinclude value1="${subMenuSelected}" value2="report_payment" operator="equals" >
	    <ffi:setProperty name="TagToAdd" value='${SelectedImageTag}'/>
    </ffi:cinclude>
    <ffi:setProperty name="sub_menu_reports" value='${sub_menu_reports}${TagToAdd}'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_322')}" scope="request" /><ffi:setProperty name="sub_menu_reports" value='${tmpI18nStr}'/>
</ffi:cinclude>
</ffi:cinclude>

<ffi:setProperty name="CheckReportEntitlement" property="ReportClear" value=""/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportDenied" value="false"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_SYSTEM_ACTIVITY %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_OBO %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_USER_ENTITLEMENT %>"/>
<ffi:setProperty name="CheckReportEntitlement" property="ReportType" value="<%=com.ffusion.beans.admin.AdminRptConsts.RPT_TYPE_PROFILE_ENTITLEMENT %>"/>
<ffi:process name="CheckReportEntitlement"/>

<ffi:setProperty name="displayReportAudit" value="false"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.AUDIT_REPORTING %>" >
<ffi:cinclude value1="${CheckReportEntitlement.ReportDenied}" value2="false" operator="equals">
<ffi:setProperty name="displayReportAudit" value="true"/>
<ffi:setProperty name="AuditReportsDenied" value="false"/>

    <ffi:cinclude value1="${sub_menu_reports}" value2="" operator="equals">
	<ffi:setProperty name="TagToAdd" value=''/>
    </ffi:cinclude>
    <ffi:cinclude value1="${sub_menu_reports}" value2="" operator="notEquals">
	<ffi:setProperty name="TagToAdd" value='${BarImageTag}'/>
    </ffi:cinclude>
    <ffi:cinclude value1="${subMenuSelected}" value2="report_audit" operator="equals" >
	    <ffi:setProperty name="TagToAdd" value='${SelectedImageTag}'/>
    </ffi:cinclude>
    <ffi:cinclude value1="${nav_default_reports}" value2="" operator="equals">
	<ffi:setProperty name="nav_default_reports" value="pages/jsp/reports/index.jsp?Initialize=true&subpage=audit" URLEncrypt="true"/>
    </ffi:cinclude>
    <ffi:setProperty name="sub_menu_reports" value='${sub_menu_reports}${TagToAdd}'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_53')}" scope="request" /><ffi:setProperty name="sub_menu_reports" value='${tmpI18nStr}'/>
</ffi:cinclude>
</ffi:cinclude>

<ffi:removeProperty name="CheckReportEntitlement"/>
