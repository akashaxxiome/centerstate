<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<ffi:setProperty name="register_init_touched" value="true"/>
<ffi:setProperty name="touchedvar" value="register_init_touched"/>

<ffi:cinclude value1="${payments_init_touched}" value2="true" operator="notEquals" >
	<s:include value="/pages/jsp/inc/init/payments-init.jsp" />
</ffi:cinclude>

<%--<!-- This task should have been executed in payments-init.jsp but check to make sure -->--%>
<ffi:cinclude value1="${SetRegisterAccountsData}" value2="">
	<ffi:object name="com.ffusion.tasks.register.SetRegisterAccountsData" id="SetRegisterAccountsData" scope="session"/>
	<ffi:process name="SetRegisterAccountsData"/>
</ffi:cinclude>

<%--<!-- This task should have been executed in payments-init.jsp but check to make sure -->--%>
<ffi:cinclude value1="${TransferDefaultCategories}" value2="">
	<ffi:object name="com.ffusion.tasks.register.TransferDefaultCategories" id="TransferDefaultCategories" scope="session"/>
	<ffi:process name="TransferDefaultCategories"/>
</ffi:cinclude>

<%--<!-- This task should have been executed in payments-init.jsp but check to make sure -->--%>
<ffi:cinclude value1="${GetRegisterCategories}" value2="">
	<ffi:object name="com.ffusion.tasks.register.GetRegisterCategories" id="GetRegisterCategories" scope="session"/>
	<ffi:process name="GetRegisterCategories"/>
	<% 
	   session.setAttribute("FFIGetRegisterCategories", session.getAttribute("GetRegisterCategories"));
	%>
</ffi:cinclude>
<ffi:list collection="RegisterCategories" items="Category">
	<ffi:cinclude value1="${Category.Id}" value2="0" operator="equals">
		<ffi:setProperty name="Category" property="IsDefaultCategory" value="true" />
		<ffi:setProperty name="Category" property="Locale" value="${UserLocale.Locale}" />
	</ffi:cinclude>
</ffi:list>

<ffi:object name="com.ffusion.tasks.register.GetRegisterPayees" id="GetRegisterPayees" scope="session"/>
    <% 
	   session.setAttribute("FFIGetRegisterPayees", session.getAttribute("GetRegisterPayees"));
	%>
<ffi:process name="GetRegisterPayees"/>

<ffi:object name="com.ffusion.tasks.register.GetRegisterTransactionTypes" id="GetRegisterTransactionTypes" scope="session"/>
<ffi:process name="GetRegisterTransactionTypes"/>

<ffi:object name="com.ffusion.tasks.register.DeleteOldUnreconciledTransactions" id="DeleteOldUnreconciledTransactions" scope="session"/>
<ffi:process name="DeleteOldUnreconciledTransactions"/>


<%--<!-- Set up tasks for following pages -->--%>
<ffi:object name="com.ffusion.tasks.banking.GetTransfers" id="GetTransfersRegister" scope="session"/>
<ffi:setProperty name="GetTransfersRegister" property="BankingServiceName" value="${BankingInitialize.ServiceName}"/>
<ffi:setProperty name="GetTransfersRegister" property="ServiceErrorURL" value=""/>
<ffi:object name="com.ffusion.tasks.register.ProcessTransferTransactions" id="ProcessTransferTransactions" scope="session"/>
<ffi:setProperty name="ProcessTransferTransactions" property="ServiceErrorURL" value=""/>
<ffi:setProperty name="TransfersUpdated" value="true"/>

<ffi:object name="com.ffusion.tasks.billpay.GetPayments" id="GetPaymentsRegister" scope="session"/>
<ffi:setProperty name="GetPaymentsRegister" property="ServiceErrorURL" value=""/>
<ffi:object name="com.ffusion.tasks.register.ProcessPaymentTransactions" id="ProcessPaymentTransactions" scope="session"/>
<ffi:setProperty name="ProcessPaymentTransactions" property="ServiceErrorURL" value=""/>
<ffi:setProperty name="PaymentsUpdated" value="true"/>

<ffi:object name="com.ffusion.tasks.register.SetDefaultRegisterAccount" id="SetDefaultRegisterAccount" scope="session"/>
<ffi:setProperty name="SetDefaultRegisterAccount" property="NoDefaultRegisterAccountURL" value=""/>

<ffi:object name="com.ffusion.tasks.banking.GetTransactions" id="GetTransactionsRegister" scope="session"/>
<ffi:setProperty name="GetTransactionsRegister" property="DateFormat" value="MM/dd/yyyy"/>

<ffi:object name="com.ffusion.tasks.register.AddBankTransactions" id="AddBankTransactions" scope="session"/>
<ffi:setProperty name="AddBankTransactions" property="ReverseOrder" value="false"/>

<ffi:object name="com.ffusion.tasks.register.GetRegisterTransactions" id="GetRegisterTransactions" scope="session"/>
<ffi:setProperty name="GetRegisterTransactions" property="DateFormat" value="${UserLocale.DateFormat}" />
<ffi:setProperty name="GetCurrentDate" property="AdditionalDays" value="-30" />

<ffi:object name="com.ffusion.tasks.register.ExportRegisterTransactions" id="ExportRegisterTransactions" scope="session"/>

<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetRegisterAccount" scope="session"/>
<ffi:setProperty name="SetRegisterAccount" property="AccountsName" value="BankingAccounts"/>

<ffi:object name="com.ffusion.tasks.register.AddRegisterPayee" id="AddRegisterPayee" scope="session"/>
<ffi:setProperty name="AddRegisterPayee" property="Validate" value="NAME"/>
	<% 
	   session.setAttribute("FFIAddRegisterPayee", session.getAttribute("AddRegisterPayee"));
	%>
<%-- Autoreconcile any bank transactions in the database older than the specified age.  This call ensures
that even if no payments, transfers, or bank transactions are added, the auto reconciliation process always works. --%>
<ffi:object name="com.ffusion.tasks.register.AutoReconcileOldTransactions" id="AutoReconcileOldTransactions" scope="session"/>
<ffi:process name="AutoReconcileOldTransactions"/>
<ffi:removeProperty name="AutoReconcileOldTransactions"/>
