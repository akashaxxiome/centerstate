<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="messaging_init_touched" value="true"/>
<ffi:setProperty name="touchedvar" value="messaging_init_touched"/>

<%-- Note to PS: set a variable in session called MaxMsgSize, which can be used to restric the length of the message to be send --%>
<ffi:setProperty name="MaxMsgSize" value="1024"/>

<ffi:object name="com.ffusion.tasks.messages.SignOn" id="SignOnMessageCenter" scope="session"/>
<ffi:setProperty name="SignOnMessageCenter" property="UserName" value="${User.Id},CUSTOMER,${User.Name}"/>
<ffi:process name="SignOnMessageCenter" />

<ffi:object id="GetMessages" name="com.ffusion.tasks.messages.GetMessages" scope="session"/>
<ffi:setProperty name="GetMessages" property="Refresh" value="true" />
<ffi:process name="GetMessages" />
<% session.setAttribute("FFIGetMessages", session.getAttribute("GetMessages")); %>
