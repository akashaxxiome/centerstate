<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- get the default sub-menu option for the payments directory --%>
<ffi:setProperty name="nav_default_payments" value=""/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS%>" >
	<ffi:cinclude value1="${nav_default_payments}" value2="" operator="equals" >
	    <ffi:setProperty name="nav_default_payments" value="pages/jsp/transfers/initTransferAction.action?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES %>" >
	<ffi:cinclude value1="${nav_default_payments}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_payments" value="pages/jsp/wires/index.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<% boolean showACH = false; %>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_BATCH %>" >
	<% showACH = true; %>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_FILE_UPLOAD %>" >
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD %>" >
		<% showACH = true; %>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>" >
	<%-- Only consider the Manage ACH Participants entitlement if
	     ACH Payments is enabled at the service package level --%>
	<% String businessGroupId=null; %>
	<ffi:getProperty name="Business" property="EntitlementGroupId" assignTo="businessGroupId"/>
	<ffi:object id="GetAncestorGroupByType" name="com.ffusion.tasks.admin.GetAncestorGroupByType" scope="session"/>
	<ffi:setProperty name="GetAncestorGroupByType" property="GroupID" value="<%= businessGroupId %>"/>
	<ffi:setProperty name="GetAncestorGroupByType" property="GroupType"
			 value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SERVICES_PACKAGE %>"/>
	<ffi:setProperty name="GetAncestorGroupByType" property="EntGroupSessionName" value="ServicePackageEntGroup"/>
	<ffi:process name="GetAncestorGroupByType"/>
	<ffi:removeProperty name="GetAncestorGroupByType"/>

	<ffi:object id="CheckEntACHBatchForSP" name="com.ffusion.efs.tasks.entitlements.CheckEntitlementByGroupCB" scope="session" />
	<ffi:setProperty name="CheckEntACHBatchForSP" property="GroupId" value="${ServicePackageEntGroup.GroupId}"/>
	<ffi:setProperty name="CheckEntACHBatchForSP" property="OperationName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACH_BATCH %>"/>
	<ffi:process name="CheckEntACHBatchForSP"/>
	<ffi:cinclude value1="${CheckEntACHBatchForSP.Entitled}" value2="TRUE" operator="equals">
		<% showACH = true; %>
	</ffi:cinclude>
	<ffi:removeProperty name="CheckEntACHBatchForSP"/>
</ffi:cinclude>
<ffi:setProperty name="displayACH" value="false"/>
<% if (showACH) { %>
	<ffi:setProperty name="displayACH" value="true"/>
	<ffi:cinclude value1="${nav_default_payments}" value2="" operator="equals" >
        <ffi:setProperty name="nav_default_payments" value="pages/jsp/ach/index.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
<% } %>
<%--
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CCD_DED %>" >
	<ffi:cinclude value1="${nav_default_payments}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_payments" value="xxx"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CCD_TXP %>" >
	<ffi:cinclude value1="${nav_default_payments}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_payments" value="xxx"/>
	</ffi:cinclude>
</ffi:cinclude>
--%>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS %>" >
	<ffi:cinclude value1="${nav_default_payments}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_payments" value="pages/jsp/billpay/index.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.STOPS %>" >
	<ffi:cinclude value1="${nav_default_payments}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_payments" value="pages/jsp/stops/index.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<% boolean showCASHCON = false; %>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>" >
	<% showCASHCON = true; %>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST %>" >
	<% showCASHCON = true; %>
</ffi:cinclude>
<ffi:setProperty name="displayCashConcentation" value="false"/>
<% if (showCASHCON) { %>
	<ffi:setProperty name="displayCashConcentation" value="true"/>
	<ffi:cinclude value1="${nav_default_payments}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_payments" value="pages/jsp/cashcon/index.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
<% } %>
<ffi:cinclude value1="${nav_default_payments}" value2="" operator="notEquals">
    <ffi:setProperty name="PaymentMenuDisplayed" value="TRUE"/>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="bill pay" operator="notEquals" >
    <ffi:removeProperty name="manageBillPayPayees" />
</ffi:cinclude>
