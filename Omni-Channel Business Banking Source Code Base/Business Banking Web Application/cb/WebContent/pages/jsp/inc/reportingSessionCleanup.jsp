<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="java.util.Enumeration" %>
<%-- Remove items from the session that are created and used exclusively by the Reports tab. --%>

<%
	/*	This scriptlet will iterate through every object in session and see
	 * 	if the object's name matches a reports-related task name. If it does match,
	 * 	an instanceof check will be made to make sure that the object is, indeed,
	 * 	related to the reports
	 */
	Enumeration rptEnum = session.getAttributeNames();
	while( rptEnum.hasMoreElements() ) {
		String name = (String)(rptEnum.nextElement());
		//Primary check - object name
		if( name.startsWith("GetBankRpt") || name.startsWith("GenerateBankRpt") || name.startsWith("Report") ||
		    (name.startsWith("Get") && name.endsWith("Rpt")) || (name.startsWith("Generate") && name.endsWith("Rpt")) )
		{
			Object candidate = session.getAttribute(name);
			//Secondary check - object class
			if( candidate instanceof com.ffusion.tasks.reporting.GetReportFromReports ||
			    candidate instanceof com.ffusion.tasks.reporting.GetReportFromName ||
			    candidate instanceof com.ffusion.beans.reporting.Report ||
			    candidate instanceof com.ffusion.beans.reporting.Reports )
			{
				session.removeAttribute(name);
			} //if
		} //if
	} //while
%>

<ffi:removeProperty name="tmpReportCriteria"/>
<ffi:removeProperty name="ReportDefinitions"/>
<ffi:removeProperty name="GenerateReportBase"/>
<ffi:removeProperty name="reportListPage"/>
<ffi:removeProperty name="NewReportBase"/>
<ffi:removeProperty name="ReportData"/>
<ffi:removeProperty name="GetReportsByCategory"/>
<ffi:removeProperty name="ReportResource"/>
<ffi:removeProperty name="GetReportFromReports"/>
<ffi:removeProperty name="SelectedBankReport"/>
<ffi:removeProperty name="AdministeredBusinessEmployees"/>
<ffi:removeProperty name="Entitlement_EntitlementGroups"/>
<ffi:removeProperty name="AddReport"/>
<ffi:removeProperty name="PPaySummaries"/>
<ffi:removeProperty name="GetModules"/>
<ffi:removeProperty name="ReportNamesAndCategories"/>
<ffi:removeProperty name="ShowColumnHeaderOption"/>
<ffi:removeProperty name="LastIntradayTransactionViewDateForExport"/>