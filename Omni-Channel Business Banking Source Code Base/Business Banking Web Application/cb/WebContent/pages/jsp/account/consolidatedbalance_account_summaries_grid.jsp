<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%
  String dataClassfication = request.getParameter("dataClassfication");
  String currencyCode = request.getParameter("currencyCode");
  request.setAttribute("dataClassfication",dataClassfication);
  request.setAttribute("currencyCode",currencyCode);
%>
<div id="consolidatedBalance_account_summaries_gridID">
<ffi:help id="account_consolidatedbalance" />

<ffi:setGridURL grid="GRID_accountBalanceSummaries" name="ViewURL" url="/cb/pages/jsp/accountsbygroup.jsp?AccountGroupID={0}&DataClassificationVariable={1}&AccountsType={2}&AccountDisplayCurrencyCode=${currencyCode}" parm0="AccountGroupID" parm1="DataClassfication" parm2="AccountsType"/>													
<ffi:setProperty name="tempURL" value="/pages/jsp/account/GetAccountEntitlementFilterTaskAction.action?accountCollectionName=EntitlementFilteredAccounts&dataClassfication=${dataClassfication}&currencyCode=${currencyCode}&GridURLs=GRID_accountBalanceSummaries" URLEncrypt="true"/>

<s:url id="consolidatedBalanceSummaryURL" value="%{#session.tempURL}"  escapeAmp="false"/>

	<%--Skip call to load grid data when this grid is not visible --%>
	<s:if test="%{#parameters.dashboardElementId == 'viewAsBar'}">
		<ffi:setProperty name="gridDataType" value="local"/>
	</s:if>
	<s:if test="%{#parameters.dashboardElementId == 'viewAsDonut'}">
		<ffi:setProperty name="gridDataType" value="local"/>
	</s:if>
	<s:else>
		<ffi:setProperty name="gridDataType" value="json"/>
	</s:else>
	
 	
<sjg:grid
     id="consolidatedBalanceSummaryID"
	 sortable="true" 
     dataType="%{#session.gridDataType}"  
     href="%{consolidatedBalanceSummaryURL}"
     pager="false"
     gridModel="gridModel"
	 rownumbers="false"
	 shrinkToFit="true"
	 footerrow="true"
	 scroll="false"
	 scrollrows="true"
     onGridCompleteTopics="consolidatedBalanceSummaryEvent"
     >
      
     <sjg:gridColumn name="accountsType" index="accountsType" title="%{getText('jsp.default_20')}" width="230" sortable="false" cssClass="datagrid_actionColumn"/>
     <sjg:gridColumn name="size" index="size" title="%{getText('jsp.default_300')}" width="260" sortable="false" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="balance" index="balance" title="%{getText('jsp.default_60_consolidate_balance')}" width="260" formatter="ns.account.consolidatedBalanceFormatter" sortable="false"  cssClass="datagrid_amountColumn"/>
    
     <sjg:gridColumn name="accountGroupID" index="accountGroupID" title="%{getText('jsp.account_9')}" width="260" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="dataClassfication" index="dataClassfication" title="%{getText('jsp.account_64')}" width="260" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="currencyCode" index="currencyCode" title="%{getText('jsp.account_60')}" width="260" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="totalValue" index="totalValue" title="%{getText('jsp.account_211')}" width="260" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     
     <sjg:gridColumn name="balanceAmountValue" index="balanceAmountValue" title="%{getText('jsp.account_36')}" width="260" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="totalAmountValue" index="totalAmountValue" title="%{getText('jsp.account_207')}" width="260" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="" sortable="" hidden="true" hidedlg="true"/>
     
</sjg:grid>

</div>
<script>	
					
		/* $('#consolidatedBalance_account_summaries_gridID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#consolidatedBalance_account_summaries_gridID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});

		$('#consolidatedBalance_account_summaries_gridID').portlet('title', js_consolidated_summary_portlet_title);
		ns.common.addSettingsIconToPortlet('consolidatedBalance_account_summaries_gridID') */
		if(ns.account.consolidatedBalanceDisplayType='')
		ns.account.consolidatedBalanceDisplayType='dataTable';
</script>
<style>
	#consolidatedBalance_account_summaries_gridID .ui-jqgrid-ftable td{
		border:none;
	}
</style>