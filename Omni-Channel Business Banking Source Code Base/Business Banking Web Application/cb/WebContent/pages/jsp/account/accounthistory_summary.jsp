<%@page import="com.ffusion.util.HTMLUtil"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div>
<div id="accountHistoryPortletsContainer" class="portlet">
	<div class="portlet-header">
		    <div class="searchHeaderCls">
				<span class="searchPanelToggleAreaCls" onclick="$('.acntDashboard_masterItemHolder').slideToggle();" id="transactionSearchToggle">
					<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
				</span>
				<div class="summaryGridTitleHolder resetHandCursor">
					<span id="selectedGridTab" class="selectedTabLbl marginRight10">
						<s:text name="jsp.default.searchResult"/>
					</span>
				</div>
			</div>
	</div>
	<div class="portlet-content">
	<ffi:help id="acctMgmt" />
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}" value2="true" operator="equals">
			<%-- Check if Active accounts available or not--%>
			<ffi:setProperty name="BankingAccounts" property="Filter" value="CoreAccount=1,HIDE=0,AND"/>
			<s:if test="#session.BankingAccounts.size != 0">
				<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
				<div id="appdashboard">
					 <s:include value="/pages/jsp/account/accounthistory_dashboard.jsp"/>
				</div>
			</s:if>
			<s:else>
				<div id="appdashboard" align="centre" style="text-align: center;">
					<%-- If no Active accounts available, then hide search area and show message. --%>
					<span id="noActiveAccount" class="txt_normal_bold"><s:text name="jsp.account_history.noActiveAccounts.message"></s:text></span>
				</div>
			</s:else>
		</ffi:cinclude>
		<div id="accountHistorySummaryGrid" class="ui-jqgrid marginTop10" style="float:left !important;">
		   	<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
				<s:include value="/pages/jsp/account/accounthistory_grid.jsp"/>
			</ffi:cinclude>
		</div>
	</div>
	
	
	
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<div id="accountHistoryLegend" align="center" style="display:none">
			<br>
			<img src="/cb/web/multilang/grafx/account/i_in_process.gif" width="12" height="12" align="absmiddle" vspace="3" hspace="3">
			<span class="txt_date">
			<s:text name="jsp.account_in_process"/>
			</span>
			<img src="/cb/web/multilang/grafx/account/gen-checkmark.gif" width="12" height="12" align="absmiddle" vspace="3" hspace="3">
			<span class="txt_date"><s:text name="jsp.account_cleared"/>
			</span>
		</div>
	</ffi:cinclude>
	
		<div id="accountHistoryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('accountHistoryPortletsContainer')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	
</div>

</div>
<ffi:setProperty name="AccountDisplayText" value=""/>
 
<%
com.ffusion.beans.accounts.Account acc = (com.ffusion.beans.accounts.Account)session.getAttribute("Account");
String nameStr="";
if(acc != null)
	nameStr = HTMLUtil.encode(acc.getConsumerAccountDisplayTextWithType()); 
%>
<s:if test="%{#session.SecureUser.AppType=='Business'}">

	<% 
if(acc != null)	nameStr = HTMLUtil.encode(acc.getDisplayTextRoutingNumNickNameCurrency()); %>
</s:if>

<script type="text/javascript">
$(document).ready(function(){
	
	//Initialize portlet with settings & calendar icon
	ns.common.initializePortlet("accountHistoryPortletsContainer", false, true, function( ) {
	    $("#openCalendar").click();
	});
	 
	var displayText = '<%=nameStr%>';     
	
	if(ns.account.appdashboardflag){  
			 $('#accountHistoryPortletsContainer').portlet('title', js_acc_history_portlet_title);
			 ns.account.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accounthistory_grid.jsp'/>";
			 ns.account.refreshSummaryForm(ns.account.refreshSummaryFormURL);
			 
	}else{ 
		$('#accountHistoryPortletsContainer').portlet('title',displayText);
	}		
});

$('.acntDashboard_masterItemHolder').slideToggle();
</script>
<ffi:removeProperty name="AccountDisplayText"/>
