<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>


<ffi:object name="com.ffusion.tasks.reporting.GetReportsByCategory" id="GetSavedSearches" scope="session"/>
<ffi:setProperty name="GetSavedSearches" property="ReportsName" value="SavedSearches" />
<ffi:setProperty name="GetSavedSearches" property="ReportCategory" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY %>" />

<%
	session.setAttribute("FFIGetSavedSearches", session.getAttribute("GetSavedSearches"));
%>

<ffi:setGridURL grid="GRID_savedSearch" name="ViewURL" url="/cb/pages/jsp/account/accounthistory_dashboard.jsp?loadSavedSearch=true&TransactionSearch=true&reportID={0}&reportName={1}&reportDesc={2}" parm0="ReportID.ReportID" parm1="ReportID.ReportName" parm2="ReportID.Description"/>
<ffi:setGridURL grid="GRID_savedSearch" name="EditURL" url="/cb/pages/jsp/account/accounthistory_dashboard.jsp?editSavedSearch=true&TransactionSearch=true&reportID={0}&reportName={1}&reportDesc={2}" parm0="ReportID.ReportID" parm1="ReportID.ReportName" parm2="ReportID.Description"/>
<ffi:setGridURL grid="GRID_savedSearch" name="DeleteURL" url="/cb/pages/jsp/account/savedSearchConfirmDelete.jsp?reportID={0}&reportName={1}&reportDesc={2}" parm0="ReportID.ReportID" parm1="ReportID.ReportName" parm2="ReportID.Description"/>

<ffi:setProperty name="tempURL" value="/pages/jsp/account/GetSavedSearchAction.action?GridURLs=GRID_savedSearch" URLEncrypt="true"/>
<s:url id="savedSearchUrl" value="%{#session.tempURL}" escapeAmp="false"/>

<%-- Set current page number for Load and Edit Search --%>
<% if(session.getAttribute("loadSavedSearch")!= null || session.getAttribute("editSavedSearch")!= null)  {%>
	<s:set var="currentPageNumber" value="%{#session.currentSearchPage}"/>
<% } else { %>
	<s:set var="currentPageNumber" value="1"/>
<% } %>

<sjg:grid
	id="savedSearchGridId"
	caption="%{getText('jsp.saved_search')}"
	sortable="true"
	dataType="json"
	href="%{savedSearchUrl}"
	pager="true"
	gridModel="gridModel"
	rowNum="5"
	rownumbers="false"
	shrinkToFit="true"
	navigator="true"
	navigatorAdd="false"
	navigatorDelete="false"
	navigatorEdit="false"
	navigatorRefresh="false"
	navigatorSearch="false"
	navigatorView="false"
	scroll="false"
	scrollrows="true"
	viewrecords="false"
	onGridCompleteTopics="savedSearchGridEvent"
	page="%{#currentPageNumber}"
	>

	<sjg:gridColumn
		name="reportName"
		index="name"
		title="%{getText('jsp.default_353')}"
		sortable="true"
		formatter="ns.account.formatReportNameLink"
		width="90"
		cssClass="datagrid_textColumn"/>

	<sjg:gridColumn
		name="description"
		index="desc"
		title="%{getText('jsp.default_170')}"
		sortable="true"
		width="120"
		cssClass="datagrid_textColumn"/>

	<sjg:gridColumn
		name="reportID"
		index="id"
		title="%{getText('jsp.default_27')}"
		hidedlg="true"
		sortable="false"
		search="false"
		hidden="false"
		formatter="ns.account.formatSavedReportsActionLinks"
		width="50"/>

	<sjg:gridColumn
		name="isCurrentReport"
		index="iscurrent"
		title="%{getText('jsp.reports_604')}"
		hidedlg="true"
		sortable="false"
		hidden="true"
		width="50"/>	
	
</sjg:grid>
