<%@page import="com.ffusion.util.HTMLUtil"%>
<%@page import="com.ffusion.util.UserLocaleConsts"%>
<%@page import="com.ffusion.beans.user.UserLocale"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
 <%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.*"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.accounts.AccountTypes"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummaries"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummary"%>
<%@ page import="com.ffusion.beans.TransactionType"%>
<%@ page import="com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.banking.Transactions"%>

<ffi:author page="account-history-wait.jsp"/>
<%-- display account information for consumer user starts --%>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
<%-- SetAccountID session variable can be set so that a default account --%>
<%-- is displayed, e.g. by the portal/accounts.jsp page. --%>

<%-- New: We need to separate aggregation.Account and accounts.Account types, and the --%>
<%-- tasks aggregation.SetAccount, aggregation.GetPagedTransactions and --%>
<%-- accounts.SetAccount, banking.GetPagedTransactions --%>

<%-- Need to distinguish between aggregation and accounts type Account objects --%>
<%-- If it is an aggregation type Account, we need to call the aggregation.GetPagedTransactions --%>
<%-- If it is an account type Account, we need to call the banking.GetPagedTransactions --%>
<%-- accountType should be used like an enumeration with values 'aggregation' and 'accounts' --%>
<ffi:setProperty name="accountType" value=""/>
<ffi:setProperty name="aggregation" value="aggregation"/>
<ffi:setProperty name="accounts" value="banking"/>

<%-- Value of call to Account.CoreAccount if it is a core account --%>
<ffi:setProperty name="getCoreAccountValue" value="1"/>
<%-- Value of call to Account.CoreAccount if it is an external account --%>
<ffi:setProperty name="getExternalAccountValue" value="0"/>

<ffi:object name="com.ffusion.beans.common.Currency" id="Currency" scope="request"/>
<ffi:object id="FloatMath" name="com.ffusion.beans.util.IntegerMath" scope="request"/>
<ffi:object id="StringUtil" name="com.ffusion.beans.util.StringUtil" scope="request"/>

<ffi:cinclude value1="${getExternalAccountValue}" value2="${Account.CoreAccount}" operator="equals">
	<%-- If this is an external account, use aggregration--%>
	<ffi:setProperty name="accountType" value="${aggregation}"/>
</ffi:cinclude>
<ffi:cinclude value1="${getCoreAccountValue}" value2="${Account.CoreAccount}" operator="equals">
	<%-- If this is an internal account, use accounts --%>
	<ffi:setProperty name="accountType" value="${accounts}"/>
</ffi:cinclude>

<%-- setting current account in session --%>
<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
	<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
	<ffi:setProperty name="SetAccount" property="AccountsName" value="SelectedAccounts"/>
	<ffi:setProperty name="SetAccount" property="ID" value="${setAccountID}"/>
	<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
	<ffi:process name="SetAccount"/>
	<ffi:removeProperty name="SetAccount" />
</ffi:cinclude>

<%-- Load Summary Info. for bankings accounts --%>
<ffi:cinclude value1="${accountType}" value2="${accounts}" operator="equals">
	<ffi:object name="com.ffusion.tasks.banking.GetSummaries" id="GetSummariesForBanking"/>
		<ffi:setProperty name="GetSummariesForBanking" property="AccountName" value="Account" />
		<ffi:setProperty name="GetSummariesForBanking" property="SummariesName" value="AccountSummaries" />
	<ffi:process name="GetSummariesForBanking"/>
	<ffi:removeProperty name="GetSummariesForBanking" />
</ffi:cinclude>
<%-- Load Summary Info. for aggregation accounts --%>
<ffi:cinclude value1="${accountType}" value2="${aggregation}" operator="equals">
	<ffi:object name="com.ffusion.tasks.aggregation.GetSummary" id="GetSummariesForAggregation"/>
		<ffi:setProperty name="GetSummariesForAggregation" property="AccountName" value="Account" />
		<ffi:setProperty name="GetSummariesForAggregation" property="SummaryName" value="AccountSummaries" />
	<ffi:process name="GetSummariesForAggregation"/>
	<ffi:removeProperty name="GetSummariesForAggregation" />
</ffi:cinclude>

							
<%-- Begin: Balance Summary --%>

						<table border="0" cellpadding="0" cellspacing="0" align="center" width="280">
							<%
								String nameStr = "";
								com.ffusion.beans.accounts.Account accountDetail = null;
								String selectedAccindex = request.getParameter("accountIndex");
								%>
								<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
								<% selectedAccindex="1"; %>
								</ffi:cinclude>
								<%
								com.ffusion.beans.accounts.Accounts accountsList = (com.ffusion.beans.accounts.Accounts)session.getAttribute("SelectedAccounts");
								if(accountsList != null && accountsList.size() > 0) {
									accountDetail = (com.ffusion.beans.accounts.Account)accountsList.get(Integer.parseInt(selectedAccindex) - 1);
									nameStr = HTMLUtil.encode(accountDetail.getAccountDisplayText());
								}
							%>
							<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
								
									<tr>
										<td class="account_grid_header">
										<s:text name="jsp.account.accountHistory.gridHeader.account"/>-
										</td>
					                	
					                	<td class="account_grid_header">
										
										&nbsp;&nbsp;<%=nameStr%> 
										&nbsp;&nbsp;
										<div class="acntDashboard_itemHolder dashboardSubmenuItemCls account_Expand" id="showMoreDiv_<s:property value="%{#session.gridIndex}"/>">
											<div onclick="javascript:showMoreOptionsOnclick('<s:property value="%{#session.gridIndex}"/>');">
												<span class="dashboardSubmenuLbl" id="m_<s:property value="%{#session.gridIndex}"/>"><s:text name="jsp.default.more" /></span>
												<span class="dashboardSubmenuLbl" id="l_<s:property value="%{#session.gridIndex}"/>" style="display:none;"><s:text name="jsp.default.less" /></span>
								    			<span id="icon-navigation-arrow_<s:property value="%{#session.gridIndex}"/>" class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			    							</div>	
										</div>
										</td>
					                	<ffi:removeProperty name="AccountDisplayTextTask"/>
									</tr>
								
							</ffi:cinclude>
							
							<%
								String acctId = null;
								int accountSystemType = 0;
								int accountType = 0;
								
								if(accountDetail != null) {
									acctId= accountDetail.getID();
									// remember the account type
									accountSystemType = Account.getAccountSystemTypeFromGroup(accountDetail.getAccountGroup() );
									accountType = accountDetail.getTypeValue();
								}
							
								// we need to make a lot of comparisons of account type, so we make the comparisons once and record the results
								boolean deposit = false;
								boolean asset = false;
								boolean creditCard = false;
								boolean loan = false;
								boolean moneyMarket = false;
								boolean savings = false;

								if ( accountSystemType == Account.SYSTEM_TYPE_DEPOSIT )
									deposit = true;
								else if ( accountSystemType == Account.SYSTEM_TYPE_ASSET )
									asset = true;
								else if ( accountSystemType == Account.SYSTEM_TYPE_CREDITCARD )
									creditCard = true;
								else if ( accountSystemType == Account.SYSTEM_TYPE_LOAN )
									loan = true;

								// Summaries are taken from account-history-wait.jsp
								AccountSummaries summaries = (AccountSummaries)session.getAttribute( "AccountSummaries" );
								AccountSummary summary;
								for (int i=0; i<summaries.size(); i++) {
									summary = (AccountSummary)summaries.get(i);
									if ((summary.getAccountID()).equals(acctId)) {
										session.setAttribute( "AccountSummary", summary );
										break;
									}
								}

								if ( accountType == AccountTypes.TYPE_MONEY_MARKET )
									moneyMarket = true;
								else if ( accountType == AccountTypes.TYPE_SAVINGS )
									savings = true;
							%>
					
                            <ffi:setProperty name="AccountSummary" property="DateFormat" value="${UserLocale.DateFormat}" />
			              	<%-- For each of these amounts, we are doing a check to see if the amount is negative, and if it is, were are
			              		 adding a span color to make the text red.  Also, we only include values that apply to the current account type	--%>
							<ffi:setProperty name="Currency" property="CurrencyCode" value="${Account.CurrentCode}"/>
							<ffi:setProperty name="Currency" property="Format" value="CURRENCY"/>
							<ffi:setProperty name="Currency" property="Format" value="DECIMAL"/>
						<tr>
							<td align="center" colspan="2">
							<div class="toggleTbl" style="display:none;" id="<s:property value="%{#session.gridIndex}"/>">
								<table border="0" cellpadding="0" cellspacing="0" align="center" width="150">		
							<% if ( deposit || moneyMarket ) { %>
				              	<tr>

				                	<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.ClosingLedger}"/>
				                	<ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<ffi:setProperty name="FloatMath" property="Value2" value="0"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.prior_Day_Balance"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span> <ffi:getProperty name="AccountSummary" property="ClosingLedger"/></span></td>
				             	</tr>
							<% } %>
							<% if ( loan ) { %>
				              	<tr>
				                	<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.OpenDate}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.open_Date"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="OpenDate"/></span></td>
				             	</tr>
			             	<% } %>
							<% if ( deposit || moneyMarket ) { %>
				              	<tr>
				                	<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.CurrentLedger}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.current_Balance"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="CurrentLedger"/></span></td>
				             	</tr>

			             	<% } %>
							<% if ( creditCard || loan ) { %>
				              	<tr>
				                	<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.CurrentBalance}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.current_Balance"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="CurrentBalance"/></span></td>
				             	</tr>
			             	<% } %>
							<% if ( deposit || moneyMarket ) { %>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.CurrentAvailBal}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.available_Balance"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="CurrentAvailBal"/></span></td>
				             	</tr>
				            <% } %>
							<% if ( savings || moneyMarket || loan ) { %>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.InterestRate}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.interest_Rate"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="InterestRateString"/></span></td>
				             	</tr>
				            <% } %>
							<% if ( loan ) { %>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.NextPaymentDate}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.next_Payment_Date"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="NextPaymentDate"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.NextPaymentAmt}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.next_Payment_Amount"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="NextPaymentAmt"/></span></td>
				             	</tr>
				            <% } %>
							<% if ( savings || moneyMarket || loan ) { %>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.InterestYTD}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.interest_YTD"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="InterestYTD"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.PriorYearInterest}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.prior_Year_Interest"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="PriorYearInterest"/></span></td>
				             	</tr>
			             	<% } %>
							<% if ( loan ) { %>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.LoanTerm}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.loan_Term"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="LoanTerm"/></span></td>
				             	</tr>
				             	<tr>

			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.TodaysPayoff}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.todays_Payoff"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="TodaysPayoff"/></span></td>
				             	</tr>

				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.PayoffGoodThru}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.payoff_Good_Through"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="PayoffGoodThru"/></span></td>
				             	</tr>
				            <% } %>
							<% if ( creditCard ) { %>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.CreditLimit}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.credit_Limit"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="CreditLimit"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.AvailCredit}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.available_Credit"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="AvailCredit"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.LastPaymentDate}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.last_Payment_Date"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="LastPaymentDate"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.LastPaymentAmt}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.last_Payment_Amount"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="LastPaymentAmt"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.LastAdvanceDate}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.last_Advance_Date"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="LastAdvanceDate"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.LastAdvanceAmt}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.last_Advance_Amount"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="LastAdvanceAmt"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.InterestRate}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.current_Interest_Rate"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="InterestRate"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.PayoffAmount}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.payoff_Amount"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="PayoffAmount"/></span></td>
				             	</tr>
				            <% } %>
							<% if ( asset && !moneyMarket ) { %>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.BookValue}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.book_Value"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="BookValue"/></span></td>
				             	</tr>
				             	<tr>
			                		<ffi:setProperty name="Currency" property="Amount" value="${AccountSummary.MarketValue}"/><ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}"/>
				                	<td class="sectionsubhead" align="right"><span style="margin-left: 11px; white-space: nowrap;" ><s:text name="jsp.account.accountHistory.gridHeader.market_Value"/>:</span></td>
				                	<td class="sectionsubhead" align="right"><span><ffi:getProperty name="AccountSummary" property="MarketValue"/></span></td>
				             	</tr>
				            <% } %>
						</table> 
						</div>
					</td>
				</tr>
			 </table>
<%-- End: Balance Summary --%>
<br style="height:0;"/>
	<%-- display account information for consumer user  ends --%>
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<%
			String displayText = "";
			String accountIndex = request.getParameter("accountIndex");
			%>
			
			<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
				<% accountIndex="1"; %>
			</ffi:cinclude>
								
			<%
			com.ffusion.beans.accounts.Accounts accounts = (com.ffusion.beans.accounts.Accounts) session.getAttribute("SelectedAccounts");
			if(accounts != null && accounts.size() > 0) {
				com.ffusion.beans.accounts.Account accountObj = (com.ffusion.beans.accounts.Account)accounts.get(Integer.parseInt(accountIndex) - 1);
				displayText = HTMLUtil.encode(accountObj.getAccountDisplayText());
			}
		    %>
		<br style="height:0;"/>	
		<table border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td class="account_grid_header" align="right">
					<span><s:text name="jsp.account.accountHistory.gridHeader.account"/>:</span></td>
               	<td width="7" >&nbsp;</td>
               	<td class="account_grid_header" align="right">
               		<span><%=displayText%></span>
				</td>
               	<ffi:removeProperty name="AccountDisplayTextTask"/>
               	<td width="7" class="sectionsubhead">&nbsp;</td>
			</tr>
		</table>
		<br style="height:0;"/>
</ffi:cinclude>

<script>
function showMoreOptionsOnclick(gridIndex)
{
	$( ".toggleTbl" ).each(function() {
		var id =$(this).attr('id');
		if(id == gridIndex)
		{
			$(this).slideToggle(500);
		
			var more = '#m_'+gridIndex;
			var less = '#l_'+gridIndex;
			var arrow = '#icon-navigation-arrow_'+gridIndex;
			var contentDiv = '#showMoreDiv_'+gridIndex;
			
			$(more).toggle();
			$(less).toggle();
			$(contentDiv).toggleClass('collapseArrow');
	
			$(arrow).toggleClass('icon-navigation-down-arrow icon-navigation-up-arrow');
		}
	});
}
</script>