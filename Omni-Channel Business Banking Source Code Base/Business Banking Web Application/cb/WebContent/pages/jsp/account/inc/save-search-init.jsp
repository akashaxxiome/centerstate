<%@page import="com.sap.banking.accountconfig.constants.AccountConstants"%>
<%@ page import="com.ffusion.beans.reporting.Report"%>
<%@ page import="com.ffusion.beans.reporting.ReportCriteria"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.accounts.AccountTypes"%>
<%@ page import="com.ffusion.beans.accounts.Accounts"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- Prepare the SaveReport --%>
<%-- <ffi:object name="com.ffusion.beans.reporting.Report" id="ReportData" scope="session" /> --%>

<%-- initialize the report for saving --%>
<%-- <% --%>
<!-- 	Properties reportOptions = new Properties(); -->
<!-- 	reportOptions.setProperty( ReportCriteria.OPT_RPT_TYPE, com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY); -->
<!-- 	reportOptions.setProperty( ReportCriteria.OPT_RPT_CATEGORY, com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY); -->
<!-- 	Report reportData = (Report) session.getAttribute( "ReportData" ); -->
<!-- 	ReportCriteria rptCriteria = new ReportCriteria(); -->
<!-- 	rptCriteria.setReportOptions( reportOptions ); -->
<!-- 	reportData.setReportCriteria( rptCriteria ); -->
<!-- 	session.setAttribute( "ReportData", reportData ); -->
<!-- %> -->

<%-- Set Selected Account values --%>
<%
String[] consumerAccountsMultiSelectDropDown = (String[]) request.getParameterValues(AccountConstants.TRANSACTION_HISTORY_SEARCH_MULTI_SELECT_ACCOUNTS);
String accounts = "";
for(int i=0; i < consumerAccountsMultiSelectDropDown.length; i++) {
	String[] selectedAccountValues = consumerAccountsMultiSelectDropDown[i].split(",");
	String selectedAccountID = selectedAccountValues[0];
%>
	<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
	<!-- fetch Account using account ID, bank ID and routing number -->
	<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" scope="session"/>
	<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
	<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
	<ffi:setProperty name="SetAccount" property="ID" value="<%= selectedAccountID %>"/>
	<ffi:process name="SetAccount"/>
<%
	Account account = (Account) session.getAttribute( "Account" );

	accounts += (accounts.length()==0 ? "" : ",") + account.getID() + ':' +
		account.getBankID() + ':' +
		account.getRoutingNum() + ':' +
		account.getNickName() + ':' +
		account.getCurrencyCode();
}
%>

<%
	session.setAttribute("TSDescription", request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_DESCRIPTION));
	session.setAttribute("TSMinimumAmount", request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_AMT_MIN));
	session.setAttribute("TSMaximumAmount", request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_AMT_MAX));
	session.setAttribute("TSReferenceStart", request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_REF_START));
	session.setAttribute("TSReferenceEnd", request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_REF_END));

	String startDate = (String)request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_START_DATE);
	String endDate = (String)request.getParameter(AccountConstants.TRANSACTION_HISTORY_SEARCH_END_DATE);
%>

	<%-- set the extra search criteria --%>
	<%	String[] values = request.getParameterValues(AccountConstants.TRANSACTION_HISTORY_SEARCH_TRANS_TYPE);  //   "TSTransactionType" 
		StringBuffer comma = new StringBuffer();
		if (values != null) {
			for(int i=0; i<values.length; i++) {
				if (i != 0) {
					comma.append(",");
				}
				String tranTy = null;  %>
		    <ffi:setProperty name="tempVar" value="<%= values[i] %>"/>
	 		  <ffi:getProperty name="tempVar" assignTo="tranTy"/>
	<%
				comma.append( tranTy );
			}
			session.setAttribute("TSTransactionType", comma.toString());
		}
	%>

<ffi:setProperty name="GetPagedTransactions" property="StartDate" value="<%=startDate%>"/>
<ffi:setProperty name="GetPagedTransactions" property="EndDate" value="<%=endDate%>"/>

<ffi:cinclude value1="${GetPagedTransactions.StartDate}" value2="" operator="notEquals">
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.util.DateConsts.SEARCH_CRITERIA_START_DATE %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${GetPagedTransactions.StartDate}" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="" />	
</ffi:cinclude>

<ffi:cinclude value1="${GetPagedTransactions.EndDate}" value2="" operator="notEquals">
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.util.DateConsts.SEARCH_CRITERIA_END_DATE %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${GetPagedTransactions.EndDate}" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="" />
</ffi:cinclude>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
<ffi:cinclude value1="${GetPagedTransactions.DateRangeValue}" value2="" operator="notEquals">
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${GetPagedTransactions.DateRangeValue}" />
	
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.util.DateConsts.SEARCH_CRITERIA_START_DATE %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.util.DateConsts.SEARCH_CRITERIA_END_DATE %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="" />
	
</ffi:cinclude>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${TSTransactionType}" />

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MIN %>"/>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MIN %>" />
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${TSReferenceStart}" />

	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MAX %>"/>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MAX %>" />
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${TSReferenceEnd}" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MIN %>"/>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MIN %>" />
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${TSReferenceStart}" />
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MAX %>"/>
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MAX %>" />
		<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${TSReferenceEnd}" />
</ffi:cinclude>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${TSMinimumAmount}" />

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${TSMaximumAmount}" />

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DESCRIPTION %>"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DESCRIPTION %>" />
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${TSDescription}" />

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SHOW_MEMO %>"/>
<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
	<ffi:setProperty name="ReportData" property="CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SHOW_MEMO %>" />
	<ffi:setProperty name="ReportData" property="CurrentSearchCriterionValue" value="${GetPagedTransactions.SearchCriteriaValue}" />
</ffi:cinclude>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="<%= accounts %>" />
