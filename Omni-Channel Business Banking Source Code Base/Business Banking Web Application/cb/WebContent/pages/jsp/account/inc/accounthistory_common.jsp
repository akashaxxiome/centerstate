<%@page import="com.ffusion.tasks.banking.GetPagedTransactions"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>

<%
String cancelModifySearch = request.getParameter("cancelModifySearch");
if(cancelModifySearch!=null && "true".equalsIgnoreCase(cancelModifySearch)){
	session.setAttribute("cancelModifySearch", true);
}
%>

<ffi:cinclude value1="${cancelModifySearch}" value2="true" operator="equals">
	<ffi:object id="GetDatesFromDateRangeTest" name="com.ffusion.tasks.util.GetDatesFromDateRange" scope="request"/>
	<ffi:setProperty name="DateRangeValueTest" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.LAST_30_DAYS %>"/>
	<ffi:setProperty name="GetDatesFromDateRangeTest" property="DateRangeValue" value="${DateRangeValueTest}"/>
	<ffi:process name="GetDatesFromDateRangeTest"/>
	 
	<ffi:setProperty name="DefaultStartDate" value="${GetDatesFromDateRangeTest.StartDate}" />
	<ffi:setProperty name="DefaultEndDate" value="${GetDatesFromDateRangeTest.EndDate}" />
	 <%-- Sets the Default date range values in session variables for Display in search criteria --%>
	<ffi:setProperty name="DisplayStartDate" value="${GetDatesFromDateRangeTest.StartDate}"/>
	<ffi:setProperty name="DisplayEndDate" value="${GetDatesFromDateRangeTest.EndDate}"/>
	
	<ffi:setProperty name="GetPagedTransactions" property="StartDate" value="${GetDatesFromDateRangeTest.StartDate}"/>
	<ffi:setProperty name="GetPagedTransactions" property="EndDate" value="${GetDatesFromDateRangeTest.EndDate}"/>
	<ffi:setProperty name="GetPagedTransactions" property="DateRangeValue" value=""/>
	
	<ffi:removeProperty name="<%=com.ffusion.tasks.admin.AdminTask.REPORT_DATA%>"/>
	<ffi:removeProperty name="DateRangeValue"/>
	<ffi:removeProperty name="DateRangeValueTest"/>
	<ffi:removeProperty name="GetDatesFromDateRangeTest"/>
	<ffi:removeProperty name="cancelModifySearch"/>
</ffi:cinclude>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${GetPagedTransactions.DateRangeValue}" />
<ffi:setProperty name="DisplayDataClassification" value="${GetPagedTransactions.DataClassification}"/>
<ffi:setProperty name="DisplayAccountID" value="${SetAccount.ID}"/>
<ffi:cinclude value1="${DateRangeValue}" value2="" operator="equals">
	<ffi:setProperty name="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.LAST_30_DAYS %>"/>
</ffi:cinclude>

<ffi:cinclude value1="${GetPagedTransactions}" value2="" operator="equals">
    <%-- Someone might have tried to set the DataClassification, StartDate and/or EndDate on the GetPagedTransactions task
         before it was created, so first try to respect that by preserving the variables here, an then setting them later. --%>
	<%
		String dataClassificationGPT = null;
		String startDateGPT = null;
		String endDateGPT = null;
		String dateRangeValueGPT = null;
	%>
	<ffi:getProperty name="GetPagedTransactions.DataClassification" assignTo="dataClassificationGPT"/>
	<ffi:getProperty name="GetPagedTransactions.StartDate" assignTo="startDateGPT"/>
	<ffi:getProperty name="GetPagedTransactions.EndDate" assignTo="endDateGPT"/>
	<ffi:getProperty name="GetPagedTransactions.DateRangeValue" assignTo="dateRangeValueGPT"/>

	<ffi:object id="GetPagedTransactions" name="com.ffusion.tasks.banking.GetPagedTransactions" scope="session"/>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
				<ffi:setProperty name="GetPagedTransactions" property="DefaultDateRange" value="180"/>
			</ffi:cinclude>
    		<ffi:setProperty name="GetPagedTransactions" property="AccountName" value="Account"/>
    		<ffi:setProperty name="GetPagedTransactions" property="TransactionsName" value="Transactions"/>
    		<ffi:setProperty name="GetPagedTransactions" property="DateFormat" value="${UserLocale.DateFormat}"/>
    		<ffi:setProperty name="GetPagedTransactions" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
    		<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedTransactions" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    		<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetPagedTransactions" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    		<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetPagedTransactions" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    		<ffi:setProperty name="GetPagedTransactions" property="ClearSortCriteria" value=""/>
    		<ffi:setProperty name="GetPagedTransactions" property="SortCriteriaOrdinal" value="1"/>
    		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
    			<ffi:setProperty name="GetPagedTransactions" property="SortCriteriaName" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_PROCESS_DATE %>"/>
    		</ffi:cinclude>
    		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
    			<ffi:setProperty name="GetPagedTransactions" property="SortCriteriaName" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_DATE %>"/>
    		</ffi:cinclude>
    		<ffi:setProperty name="GetPagedTransactions" property="SortCriteriaAsc" value="False"/>
    		<ffi:setProperty name="GetPagedTransactions" property="ClearSearchCriteria" value=""/>
    		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
    			<ffi:setProperty name="GetPagedTransactions" property="PageSize" value="10"/>
    		</ffi:cinclude>
    		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
    			<ffi:setProperty name="GetPagedTransactions" property="PageSize" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.LARGE_PAGE_SIZE %>"/>
    		</ffi:cinclude>

			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
				<%--If the maximum number of days allowed in a search needs to be modified, both the variables below need to be changed. --%>
				<ffi:setProperty name="OverrideDateSearchDefault" value="30"/>
				<ffi:setProperty name="GetPagedTransactions" property="MaxDaysInDateRange" value="180"/>
				<ffi:setProperty name="GetPagedTransactions" property="EnforceMaxDateRangeCheck" value="true"/>

				<%--In order to improve customer experience, we will not allow the user to enter a start date or end date beyond the current date/time. --%>
				<ffi:setProperty name="GetPagedTransactions" property="EnforceNoFutureDatesCheck" value="true"/>
			</ffi:cinclude>

			<%-- Set the values on GetPagedTransactions before GetPagedTransactions was initialized --%>
		    <ffi:cinclude value1="${dataClassificationGPT}" value2="" operator="notEquals">
				<ffi:setProperty name="GetPagedTransactions" property="DataClassification" value="${dataClassificationGPT}"/>
				<ffi:removeProperty name="GetPagedTransactions.DataClassification"/>
			</ffi:cinclude>
		    <ffi:cinclude value1="${startDateGPT}" value2="" operator="notEquals">
				<ffi:setProperty name="GetPagedTransactions" property="StartDate" value="${startDateGPT}"/>
				<ffi:removeProperty name="GetPagedTransactions.StartDate"/>
			</ffi:cinclude>
		    <ffi:cinclude value1="${endDateGPT}" value2="" operator="notEquals">
				<ffi:setProperty name="GetPagedTransactions" property="EndDate" value="${endDateGPT}"/>
				<ffi:removeProperty name="GetPagedTransactions.EndDate"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${dateRangeValueGPT}" value2="" operator="notEquals">
				<ffi:setProperty name="GetPagedTransactions" property="DateRangeValue" value="${dateRangeValueGPT}"/>
				<ffi:removeProperty name="GetPagedTransactions.DateRangeValue"/>
			</ffi:cinclude>
</ffi:cinclude>
<ffi:setProperty name="GetPagedTransactions" property="DateFormat" value="${UserLocale.DateFormat}"/>
<ffi:setProperty name="GetPagedTransactions" property="DateFormatParam" value="${UserLocale.DateFormat}"/>

<%--If the maximum number of days allowed in a search needs to be modified, both the variables below need to be changed. --%>
<%-- <ffi:setProperty name="OverrideDateSearchDefault" value="31"/> --%>
<%-- <ffi:setProperty name="GetPagedTransactions" property="MaxDaysInDateRange" value="31"/> --%>

<%-- Reset the start date into the task if it exists, otherwise use the start date for the range specified above as the default --%>
<ffi:setProperty name="StringUtil" property="Value2" value="${GetDatesFromDateRange.StartDate}" />
<ffi:setProperty name="StringUtil" property="Value1" value="${GetPagedTransactions.StartDate}" />
<ffi:setProperty name="GetPagedTransactions" property="StartDate" value="${StringUtil.NotEmpty}"/>

<%-- Reset the end date into the task if it exists, otherwise use the end date for the range specified above as the default --%>
<ffi:setProperty name="StringUtil" property="Value2" value="${GetDatesFromDateRange.EndDate}" />
<ffi:setProperty name="StringUtil" property="Value1" value="${GetPagedTransactions.EndDate}" />
<ffi:setProperty name="GetPagedTransactions" property="EndDate" value="${StringUtil.NotEmpty}"/>

<ffi:removeProperty name="GetDatesFromDateRange"/>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>"/>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
<%-- 		<ffi:setProperty name="GetPagedTransactions" property="DateRangeValue" value="" /> --%>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="equals">
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>"/>
</ffi:cinclude>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
<%-- fetch Account using account ID, bank ID and routing number --%>
<ffi:cinclude value1="${ProcessAccount}" value2="true" operator="equals">
	<%
	 
		String accountID = request.getParameter("AccountID"); 
		String accountBankID= request.getParameter("AccountBankID");
		String accountRoutingNum = request.getParameter("AccountRoutingNum");
		session.setAttribute("AcctId", accountID); // request.getParameter("SetAccount.ID")
		session.setAttribute("BankId", accountBankID);  // request.getParameter("SetAccount.BankID")
		session.setAttribute("RoutingNum",accountRoutingNum ); 
	%>
	<ffi:cinclude value1="${AcctId}" value2="" operator="notEquals">
	<ffi:setProperty name="SetAccount" property="ID" value="${AcctId}"/>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${BankId}" value2="" operator="notEquals">
	<ffi:setProperty name="SetAccount" property="BankId" value="${BankId}"/>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${RoutingNum}" value2="" operator="notEquals">
	<ffi:setProperty name="SetAccount" property="RoutingNum" value="${RoutingNum}"/>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${AcctId}" value2="">
		<ffi:setProperty name="SetAccount" property="IsDefault" value="true"/>
	</ffi:cinclude>

	<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
	<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
	<ffi:process name="SetAccount"/>
	<%-- <ffi:setProperty name="ProcessAccount" value="false"/> set this value to false later in jsp--%>
</ffi:cinclude>

<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
	<s:set var="tmpI18nStr" value="%{getText('jsp.account_8')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
	<ffi:setProperty name="TSView" value="true"/>
	<ffi:setProperty name="BackURL" value="${SecurePath}account/index.jsp?TransactionSearch=false" URLEncrypt="true"/>
	<ffi:setProperty name="subMenuSelected" value="account history"/>
	<ffi:setProperty name="GetPagedTransactions" property="PageSize" value="10"/>

	<%-- remove Transaction search specific properties unless they are needed on the Transaction Search page --%>
	<ffi:removeProperty name="DisplayTSTransactionType"/>
	<ffi:removeProperty name="DisplayTSReferenceStart"/>
	<ffi:removeProperty name="DisplayTSReferenceEnd"/>
	<ffi:removeProperty name="DisplayTSMinimumAmount"/>
	<ffi:removeProperty name="DisplayTSMaximumAmount"/>
	<ffi:removeProperty name="DisplayTSDescription"/>

	<%-- Remember the values last used if we were on the account history page, so that they can be redisplayed. --%>
	<ffi:cinclude value1="${unsuccessfulSearch}" value2="true" operator="equals">
		<%-- Use the values for the last known good search for the next search --%>
		<ffi:cinclude value1="${LastGoodStartDate}" value2="" operator="notEquals">
			<ffi:setProperty name="GetPagedTransactions" property="StartDate" value="${LastGoodStartDate}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${LastGoodEndDate}" value2="" operator="notEquals">
			<ffi:setProperty name="GetPagedTransactions" property="EndDate" value="${LastGoodEndDate}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${ProcessAccount}" value2="true" operator="notEquals"> <%-- QTS 692030 --%>
			<ffi:cinclude value1="${LastGoodDataClassification}" value2="" operator="notEquals">
				<ffi:setProperty name="GetPagedTransactions" property="DataClassification" value="${LastGoodDataClassification}"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:setProperty name="DisplayDataClassification" value="${GetPagedTransactions.DataClassification}"/>
		<%--
			Object lastGoodAccount = session.getAttribute( "LastGoodAccount" );
			if( lastGoodAccount != null ) {
				session.setAttribute( "Account", lastGoodAccount );
			}
		--%>
	</ffi:cinclude>
	<ffi:cinclude value1="${unsuccessfulSearch}" value2="true" operator="notEquals">
		<ffi:setProperty name="DisplayDataClassification" value="${GetPagedTransactions.DataClassification}"/>
		<ffi:setProperty name="DisplayTSZBADisplay" value="${TSZBADisplay}"/>
		<%
			session.setAttribute( "DisplayAccount", session.getAttribute( "Account" ) );
		%>
		<ffi:setProperty name="DisplayStartDate" value="${GetPagedTransactions.StartDate}"/>
		<ffi:setProperty name="DisplayEndDate" value="${GetPagedTransactions.EndDate}"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
	<s:set var="tmpI18nStr" value="%{getText('jsp.account_215')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
	<ffi:setProperty name="BackURL" value="${SecurePath}account/index.jsp?TransactionSearch=true" URLEncrypt="true"/>
	<ffi:setProperty name="subMenuSelected" value="transaction search"/>

	<%-- Remember the values last used if we were on the transaction search page, so that they can be redisplayed. --%>
	<ffi:cinclude value1="${unsuccessfulSearch}" value2="true" operator="equals">
		<%-- Use the values for the last known good search for the next search --%>
		<ffi:setProperty name="TSTransactionType" value="${LastGoodTSTransactionType}"/>
		<ffi:setProperty name="TSReferenceStart" value="${LastGoodTSReferenceStart}"/>
		<ffi:setProperty name="TSReferenceEnd" value="${LastGoodTSReferenceEnd}"/>
		<ffi:setProperty name="TSMinimumAmount" value="${LastGoodTSMinimumAmount}"/>
		<ffi:setProperty name="TSMaximumAmount" value="${LastGoodTSMaximumAmount}"/>
		<ffi:setProperty name="TSDescription" value="${LastGoodTSDescription}"/>

		<ffi:cinclude value1="${LastGoodStartDate}" value2="" operator="notEquals">
			<ffi:setProperty name="GetPagedTransactions" property="StartDate" value="${LastGoodStartDate}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${LastGoodEndDate}" value2="" operator="notEquals">
			<ffi:setProperty name="GetPagedTransactions" property="EndDate" value="${LastGoodEndDate}"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${LastGoodDataClassification}" value2="" operator="notEquals">
			<ffi:setProperty name="GetPagedTransactions" property="DataClassification" value="${LastGoodDataClassification}"/>
		</ffi:cinclude>
		<ffi:setProperty name="DisplayDataClassification" value="${GetPagedTransactions.DataClassification}"/>
		<s:include value="/pages/jsp/account/inc/transactionsearch_setup.jsp"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${unsuccessfulSearch}" value2="true" operator="notEquals">
		<s:include value="/pages/jsp/account/inc/transactionsearch_setup.jsp"/>

		<ffi:setProperty name="DisplayDataClassification" value="${GetPagedTransactions.DataClassification}"/>
		<ffi:setProperty name="DisplayTSTransactionType" value="${TSTransactionType}"/>
		<ffi:setProperty name="DisplayTSZBADisplay" value="${TSZBADisplay}"/>
		<ffi:setProperty name="DisplayTSReferenceStart" value="${TSReferenceStart}"/>
		<ffi:setProperty name="DisplayTSReferenceEnd" value="${TSReferenceEnd}"/>
		<ffi:setProperty name="DisplayTSMinimumAmount" value="${TSMinimumAmount}"/>
		<ffi:setProperty name="DisplayTSMaximumAmount" value="${TSMaximumAmount}"/>
		<ffi:setProperty name="DisplayTSDescription" value="${TSDescription}"/>
		<%
			session.setAttribute( "DisplayAccount", session.getAttribute( "Account" ) );
		%>
		<ffi:setProperty name="DisplayStartDate" value="${GetPagedTransactions.StartDate}"/>
		<ffi:setProperty name="DisplayEndDate" value="${GetPagedTransactions.EndDate}"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:setProperty name='PageText' value=''/>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%--
	Get the transaction types map.
--%>
 <ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />
	<ffi:setProperty name="TransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CORPORATE %>" />
	<ffi:process name="TransactionTypes" />
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />
	<ffi:setProperty name="TransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CONSUMER %>" />
	<ffi:process name="TransactionTypes" />
</ffi:cinclude>

<ffi:setProperty name="ProcessAccount" value="false"/>
