<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<ffi:setProperty name="CatBackURL" value="${SecureServletPath}GetRegisterCategories?GetRegisterCategories.SuccessURL=${SecureServletPath}CopyCollection" URLEncrypt="true" />
<ffi:setProperty name="CatListBackURL" value="${SecureServletPath}GetRegisterCategories?GetRegisterCategories.SuccessURL=${SecureServletPath}CopyCollection" URLEncrypt="true" />
<%
   String selectOptionValue = request.getParameter("RegisterTransaction.Current=0&RegisterTransaction.RegisterCategoryId"); 
   if(selectOptionValue!=null){%>
	   <ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=selectOptionValue %>"/>
<% }%>
<script>
multiField = false;
$(function(){
	   $("#singleCategorySelect").selectmenu({width: 250});
	   });
 
     ns.account.singleSelectOnChangeEdit = function(){
    	var urlString="/cb/pages/jsp/account/register-rec-edit.jsp?whichCat=single";
    	$.ajax({   
    		  type: "post",   
    		  url: urlString,   
    		  data: $('#EditTransaction').serialize(), 
    		  success: function(data) { 
    			$('#accountRegisterDashbord').html(data);
    		  }   
    	});
        }
</script>

<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
	<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
	<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
<ffi:process name="CopyCollection"/>

<ffi:list collection="RegisterCategories" items="Category">
	<ffi:cinclude value1="${Category.Id}" value2="0" operator="equals">
		<ffi:setProperty name="Category" property="Locale" value="${UserLocale.Locale}" />
	</ffi:cinclude>
</ffi:list>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width=1% align="left"><span class="mainfontbold"><s:text name="jsp.account_44"/></span><br>


		<ffi:setProperty name="RegisterTransaction" property="Current" value="0" />
<ffi:setProperty name="Compare" property="Value1" value="${RegisterTransaction.RegisterCategoryId}" />


		<select id="singleCategorySelect" class="txtbox" name="RegisterTransaction.Current=0&RegisterTransaction.RegisterCategoryId" onchange="ns.account.singleSelectOnChangeEdit()">
			<option value="0"><s:text name="jsp.default_445"/></option>
			<ffi:setProperty name="RegisterCategories" property="Filter" value="PARENT_CATEGORY=-1,ID!0,AND" />
<ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />
<ffi:list collection="RegisterCategories" items="Category">
<ffi:setProperty name="Compare" property="Value2" value="${Category.Id}" />

<ffi:cinclude value1="${Compare.Equals}" value2="true">
	<ffi:setProperty name="SelectedType" value="${Category.Type}"/>
</ffi:cinclude>

				<option value="<ffi:getProperty name="Category" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="Category" property="Name"/></option>
			<ffi:setProperty name="RegisterCategoriesCopy" property="Filter" value="PARENT_CATEGORY=${Category.Id}" />
<ffi:setProperty name="RegisterCategoriesCopy" property="FilterSortedBy" value="NAME" />
<ffi:list collection="RegisterCategoriesCopy" items="Category1">
<ffi:setProperty name="Compare" property="Value2" value="${Category1.Id}" />

<ffi:cinclude value1="${Compare.Equals}" value2="true">
	<ffi:setProperty name="SelectedType" value="${Category1.Type}"/>
</ffi:cinclude>

					<option value="<ffi:getProperty name="Category1" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>>&nbsp;&nbsp;<ffi:getProperty name="Category" property="Name"/>: <ffi:getProperty name="Category1" property="Name"/></option>
				</ffi:list>
</ffi:list>

		</select>

		</td>
		<td width="1%">&nbsp;</td>
		<td width="98%" class="mainfont" align="left"><span class="mainfontbold"><s:text name="jsp.default_444"/></span><br>
			<ffi:cinclude value1="${SelectedType}" value2="2" operator="equals">
				<s:text name="jsp.account_106"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${SelectedType}" value2="2" operator="notEquals">
				<s:text name="jsp.account_84"/>
			</ffi:cinclude>
			<ffi:removeProperty name="SelectedType"/>
 		</td>
		
	</tr>
</table>
<input type="hidden" name="EditRegisterTransaction.SplitCategories" value="false">
