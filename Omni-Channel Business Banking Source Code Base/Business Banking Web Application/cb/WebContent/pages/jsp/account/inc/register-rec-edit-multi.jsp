<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%      
        String select0 = request.getParameter("RegisterTransaction.Current=0&RegisterTransaction.RegisterCategoryId");
		String select1 = request.getParameter("RegisterTransaction.Current=1&RegisterTransaction.RegisterCategoryId");
		String select2 = request.getParameter("RegisterTransaction.Current=2&RegisterTransaction.RegisterCategoryId");
		String select3 = request.getParameter("RegisterTransaction.Current=3&RegisterTransaction.RegisterCategoryId");
		String select4 = request.getParameter("RegisterTransaction.Current=4&RegisterTransaction.RegisterCategoryId");
		String amount0 = com.ffusion.util.HTMLUtil.encode(request.getParameter("RegisterTransaction.Current=0&RegisterTransaction.Amount"));
		if(amount0!=null){
			%>
	    	<script>
	    	    $(document).ready(function(){
                    $("#RegisterTransactionAmount0").val('<%=amount0%>');
		    	    });
	    	</script>
	    	<%
		}
	    String amount1 = com.ffusion.util.HTMLUtil.encode(request.getParameter("RegisterTransaction.Current=1&RegisterTransaction.Amount"));
	    if(amount1!=null){
	    	%>
	    	<script>
	    	    $(document).ready(function(){
                    $("#RegisterTransactionAmount1").val('<%=amount1%>');
		    	    });
	    	</script>
	    	<%
	    }
	    String amount2 =com.ffusion.util.HTMLUtil.encode(request.getParameter("RegisterTransaction.Current=2&RegisterTransaction.Amount"));
	    if(amount2!=null){
	    	%>
	    	<script>
	    	    $(document).ready(function(){
                    $("#RegisterTransactionAmount2").val('<%=amount2%>');
		    	    });
	    	</script>
	    	<%
	    }
	    String amount3 = com.ffusion.util.HTMLUtil.encode(request.getParameter("RegisterTransaction.Current=3&RegisterTransaction.Amount"));
	    if(amount3!=null){
	    	%>
	    	<script>
	    	    $(document).ready(function(){
                    $("#RegisterTransactionAmount3").val('<%=amount3%>');
		    	    });
	    	</script>
	    	<%
	    }
	    String amount4 = com.ffusion.util.HTMLUtil.encode(request.getParameter("RegisterTransaction.Current=4&RegisterTransaction.Amount"));
	    if(amount4!=null){
	    	%>
	    	<script>
	    	    $(document).ready(function(){
                    $("#RegisterTransactionAmount4").val('<%=amount4%>');
		    	    });
	    	</script>
	    	<%
	    }
%>

<script>
multiField = true;

var categoryID = new Array(5);
var categoryIDmappedtype = new Array(5);

<ffi:setProperty name="RegisterCategories" property="Filter" value="All" />
<ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />

<ffi:object id="Paging" name="com.ffusion.beans.util.Paging" scope="session"/>
<ffi:setProperty name="Paging" property="Pages" value="5" />
<ffi:list collection="Paging" items="PageNum">
<ffi:setProperty name="Math" property="Value1" value="${PageNum}" />
<ffi:setProperty name="Math" property="Value2" value="1" />
<ffi:setProperty name="CurrentID" value="${Math.Subtract}"/>
<%String Current1 = null; %>
<ffi:getProperty name="CurrentID" assignTo="Current1"/>

<ffi:setProperty name="RegisterTransaction" property="Current" value="${CurrentID}" />
<%if(Current1.equalsIgnoreCase("1")){%>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select1 %>" />
<%}else if(Current1.equalsIgnoreCase("2")){ %>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select2 %>" />
<%}else if(Current1.equalsIgnoreCase("3")){ %>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select3 %>" />
<%}else if(Current1.equalsIgnoreCase("4")){ %>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select4 %>" />
<%}else{ %>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select0 %>" />
<%} %>
<ffi:setProperty name="RegisterTransaction" property="Current" value="${CurrentID}" />
<ffi:setProperty name="Compare" property="Value1" value="${RegisterTransaction.RegisterCategoryId}" />
 
categoryID[<ffi:getProperty name="CurrentID"/>] = "<ffi:getProperty name="RegisterTransaction" property="RegisterCategoryId"/>";
 
 	<ffi:list collection="RegisterCategories" items="Category">
 		<ffi:setProperty name="Compare" property="Value2" value="${Category.Id}" />
 		<ffi:cinclude value1="${Compare.Equals}" value2="true" operator="equals">
 			categoryIDmappedtype[<ffi:getProperty name="CurrentID"/>] = "<ffi:getProperty name="Category" property="Type"/>";
 		</ffi:cinclude>
	</ffi:list>
</ffi:list>


function stripChars(str) {
	// This function will strip everything but numbers and decimal points from a given string, then
	// convert the string to a Float Math object before returning the value.
	myTemp = str.replace(/[^0-9|\.]/g,"");
	if (myTemp == "") { myTemp = 0 }
	return parseFloat(myTemp);
}

function addCurrency(setTotalAmount) {
	y = 0;
	for (i=0;i<5;i++) {
		counter = elementNum + 1 + y;
		eval("value"+i+" = stripChars(document.EditTransaction["+counter+"].value)");
		y = y + 2;
	}
	// At this point we should have value0 - value4 populated with the contents of the fields.
	total = value0 + value1 + value2 + value3 + value4;
	total = Math.round(total*Math.pow(10,2))/Math.pow(10,2);
	totalValue = eval("\""+total+"\"");
	if (totalValue.indexOf('.') == -1) { totalValue = totalValue+".0" } // Add .0 if a decimal point doesn't exist
	temp=(totalValue.length-totalValue.indexOf('.')) // Find out if the # ends in a tenth (ie 145.5)
	if (temp <= 2) { totalValue=totalValue+"0"; }; // if it ends in a tenth, add an extra zero to the string
	document.EditTransaction.multiTotal.value = totalValue;
<ffi:cinclude value1="${TranStatus.Less}" value2="true">
   	if (setTotalAmount == 'true')
   	    document.EditTransaction["EditRegisterTransaction.TotalAmount"].value = totalValue;
</ffi:cinclude>
}

function addCurrency( theForm, setTotalAmount ) {
   	var totalAmt = 0.0;
   	var currAmt;
 	var deborcred;
   	var amtStr;
 	var currencyType;
 	currencyType = "<ffi:getProperty name="Account" property="CurrencyCode"/>";
   	<%
   	String fieldName=null;
   	for ( int i=0; i<5; i++ ) {
   		fieldName = "RegisterTransaction.Current=" + i + "&RegisterTransaction.Amount";
   	%>
   		amtStr = theForm["<%= fieldName %>" ].value;
   		if( amtStr!=null && amtStr.length>0 ) {
   			currAmt = stripChars( amtStr );
   			if( isNaN( currAmt ) ) {
 
   			} else if ( currAmt<=0 ) {
 
   			} else {

 				if( categoryIDmappedtype[<%= i %>] == 2 ) { // If it is an income
 					totalAmt -= currAmt;
 				}
 				else if ( categoryIDmappedtype[<%= i %>] == 3 ) { //If it is a debit
 					totalAmt += currAmt;
 				}
 				else {
 					// Check if it is the unassigned type
 					if( categoryID[<%= i %>] == "0" ) {
 						totalAmt += currAmt;
 					}
 				}
 				
   			}
   		}
   	<%
   	}
   	%>
   
 	deborcred = "Debit";
 	if( totalAmt < 0 ) {
 		deborcred = "Credit";
 		totalAmt *= -1;
 	}
   	

 	if( currencyType == 'JPY' ) {
		totalAmt = Math.round(totalAmt);
 		totalValue = totalAmt.toString();
 	} else {
 		totalAmt = Math.round( totalAmt*100)/100;
 		totalValue = totalAmt.toString();
 
 		if (totalValue.indexOf('.') == -1) { totalValue = totalValue+".0" } // Add .0 if a decimal point doesn't exist
 		temp=(totalValue.length-totalValue.indexOf('.')) // Find out if the # ends in a tenth (ie 145.5)
 		if (temp <= 2) { totalValue=totalValue+"0"; } // if it ends in a tenth, add an extra zero to the string
 	}
 
   	theForm.multiTotal.value = totalValue;
<ffi:cinclude value1="${TranStatus.Less}" value2="true">
	if (setTotalAmount == 'true') {
  	    theForm["EditRegisterTransaction.TotalAmount"].value = totalValue;
	}
</ffi:cinclude>
}

	$(document).ready(function(){
	    $("#multipleDIV").find("select")
	    .each(function(){
	        $(this).selectmenu({width: 250});
	        });  
	 });

	ns.account.multipleSelectOnChange = function(){
        var urlString = "/cb/pages/jsp/account/register-rec-edit.jsp?whichCat=multi";
    	$.ajax({   
    		  type: "post",   
    		  url: urlString,   
    		  data: $('#EditTransaction').serialize(), 
    		  success: function(data) { 
    			$('#accountRegisterDashbord').html(data);
    		  }   
    	});
        }
</script>

<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
	<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
	<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
<ffi:process name="CopyCollection"/>

<ffi:list collection="RegisterCategories" items="Category">
	<ffi:cinclude value1="${Category.Id}" value2="0" operator="equals">
		<ffi:setProperty name="Category" property="Locale" value="${UserLocale.Locale}" />
	</ffi:cinclude>
</ffi:list>

<ffi:setProperty name="RegisterCategories" property="Filter" value="PARENT_CATEGORY=-1,ID!0,AND" />
<ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />

<ffi:setProperty name="RegisterCategoriesCopy" property="Filter" value="PARENT_CATEGORY!-1" />
<ffi:setProperty name="RegisterCategoriesCopy" property="FilterSortedBy" value="NAME" />
<div id="multipleDIV">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="mainfontBold" width="1%" align="left"><s:text name="jsp.account_46"/></td>
		<td width="1%">&nbsp;</td>
 		<td class="mainfontBold" width="1%"><s:text name="jsp.account_218"/></td>
 		<td width="1%">&nbsp;</td>
 		<td class="mainfontBold" width="96%" align="left"><s:text name="jsp.default_45"/></td>
	</tr>
<ffi:object id="Paging" name="com.ffusion.beans.util.Paging" scope="session"/>
<ffi:setProperty name="Paging" property="Pages" value="5" />
<ffi:list collection="Paging" items="PageNum">
<ffi:setProperty name="Math" property="Value1" value="${PageNum}" />
<ffi:setProperty name="Math" property="Value2" value="1" />
<ffi:setProperty name="CurrentID" value="${Math.Subtract}"/>
<%String Current = null; %>
<ffi:getProperty name="CurrentID" assignTo="Current"/>
	<tr>
		<td>

		<ffi:setProperty name="RegisterTransaction" property="Current" value="${CurrentID}" />
		<%if(Current.equalsIgnoreCase("0")){ %>
		 <ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select0 %>" />
		<%}else if(Current.equalsIgnoreCase("1")){%>
		<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select1 %>" />
		<%}else if(Current.equalsIgnoreCase("2")){ %>
		<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select2 %>" />
		<%}else if(Current.equalsIgnoreCase("3")){ %>
		<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select3 %>" />
		<%}else if(Current.equalsIgnoreCase("4")){ %>
		<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select4 %>" />
		<%}%>
        <ffi:setProperty name="Compare" property="Value1" value="${RegisterTransaction.RegisterCategoryId}" />

			<select class="txtbox" name="RegisterTransaction.Current=<ffi:getProperty name="CurrentID"/>&RegisterTransaction.RegisterCategoryId" onchange="ns.account.multipleSelectOnChange()">
				<option value="-1"><s:text name="jsp.account_185"/></option>
				<ffi:setProperty name="Compare" property="Value2" value="0" />

				<option value="0" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.default_445"/></option>
				<ffi:cinclude value1="${Compare.Equals}" value2="true">
					<ffi:setProperty name="SelectedType" value="3"/>
				</ffi:cinclude>

			<ffi:setProperty name="RegisterCategories" property="Filter" value="PARENT_CATEGORY=-1,ID!0,AND" />
				<ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />
				<ffi:list collection="RegisterCategories" items="Category">
				<ffi:setProperty name="Compare" property="Value2" value="${Category.Id}" />


				<option value="<ffi:getProperty name="Category" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="Category" property="Name"/></option>
					<ffi:cinclude value1="${Compare.Equals}" value2="true">
						<ffi:setProperty name="SelectedType" value="${Category.Type}"/>
					</ffi:cinclude>
									<ffi:setProperty name="RegisterCategoriesCopy" property="Filter" value="PARENT_CATEGORY=${Category.Id}" />
					<ffi:setProperty name="RegisterCategoriesCopy" property="FilterSortedBy" value="NAME" />
					<ffi:list collection="RegisterCategoriesCopy" items="Category1">
					<ffi:setProperty name="Compare" property="Value2" value="${Category1.Id}" />
					
					<ffi:cinclude value1="${Compare.Equals}" value2="true">
						<ffi:setProperty name="SelectedType" value="${Category1.Type}"/>
					</ffi:cinclude>

				<option value="<ffi:getProperty name="Category1" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>>&nbsp;&nbsp;<ffi:getProperty name="Category" property="Name"/>: <ffi:getProperty name="Category1" property="Name"/></option>
				</ffi:list>
			</ffi:list>

			</select>
		</td>
		<td>&nbsp;</td>

   		<td class="mainfont">
			<ffi:cinclude value1="${SelectedType}" value2="2" operator="equals">
				<s:text name="jsp.account_106"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${SelectedType}" value2="3" operator="equals">
				<s:text name="jsp.account_84"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${SelectedType}" value2="" operator="equals">
				<s:text name="jsp.default_296"/>
			</ffi:cinclude>
			<ffi:removeProperty name="SelectedType"/>
		</td>
 		<td class="sectionsubhead">&nbsp;</td>
		
		<td align="left">
			<ffi:setProperty name="RegisterTransaction" property="AmountValue.CurrencyCode" value="${Account.CurrencyCode}" />
			<input id="RegisterTransactionAmount<ffi:getProperty name="CurrentID"/>" class="ui-widget ui-widget-content ui-corner-all" type="text" name="RegisterTransaction.Current=<ffi:getProperty name="CurrentID"/>&RegisterTransaction.Amount" size="22" onChange="addCurrency( document.EditTransaction, 'true')" value="<ffi:getProperty name="RegisterTransaction" property="AmountValue.CurrencyStringNoSymbolNoComma"/>">
		</td>
	</tr>
	<tr><td colspan="3"><img src="/cb/web/multilang/grafx/spacer.gif" width="1" height="5"></td></tr>
</ffi:list>

	<tr>
		<td>&nbsp;</td>
 		<td>&nbsp;</td>
 		<td>&nbsp;</td>
		<td class="mainfontbold" align="right"><s:text name="jsp.account_204"/>:&nbsp;</td>
		<td align="left"><input class="ui-widget ui-widget-content ui-corner-all" type="text" name="multiTotal" size="22" onFocus="blur();" ></td>
	</tr>
</table>
</div>
<input type="hidden" name="EditRegisterTransaction.SplitCategories" value="true">
<script>addCurrency( document.EditTransaction, 'true');</script>
