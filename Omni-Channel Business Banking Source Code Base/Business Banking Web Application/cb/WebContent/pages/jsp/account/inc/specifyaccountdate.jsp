<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.tasks.util.GetDatesFromDateRange"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>

<style>
	#reportOn-button  {margin: 3px 20px 5px 5px; margin-top: 4px \9}
	#select1ID-button {margin: 3px 0 5px 5px; margin-top: 4px \9}
	#zbaDisplay-button {margin: 3px 20px 5px 5px; margin-top: 4px \9}
	#startDate, #endDate{width: 69px; margin: 5px 0 0 5px;}
	#viewAccountHistoryID1 {margin: 0 0 0 20px;}
	#viewAccountHistoryID2 {margin: 0 0 0 34px; vertical-align: 20px; vertical-align: 10px \9;}
</style>

<script>
$(function(){
	$("#reportOn").selectmenu({width: 130});
	$("#select1ID").selectmenu({width: 130});
	$("#select2ID").combobox();
	$("#zbaDisplay").selectmenu({width: 100});
	$("#consumerAccountsDropDown").selectmenu({width: 350});
	$("#dateRangeValue").selectmenu({width: 130});
   });

	// clears the 2 text fields
	function clearDateTextFields() {
		$("#startDate").val('');
		$("#endDate").val('');
	}

	// resets the selected index of the given dropdown to 0
	function clearDropDown() {
		$("#dateRangeValue").selectmenu('value',0);
	}

   ns.account.reloadReportOn = function(){

		var	urlString = document.FormName.select2.options[document.FormName.select2.selectedIndex].value;

		$.ajax({
			url: urlString,
			type: 'post',
			data: $("#accountCriteriaForm").serialize(),
			success: function(data){
				$('#appdashboard').html(data);
			}
		});
	}

   ns.account.reloadAccount = function(appType){

		document.FormName.elements['ResetTSZBADisplay'].value='true';

		var	urlString = '';
		if(appType == 'corporate')
		{
			urlString = document.FormName.select2.options[document.FormName.select2.selectedIndex].value;
		}
		else
		{
			urlString = document.FormName.select22.options[document.FormName.select22.selectedIndex].value;
		}


		$.ajax({
			url: urlString,
			type: 'post',
			data: $("#accountCriteriaForm").serialize(),
			success: function(data){
				$('#appdashboard').html(data);
			}
		});
	}

   ns.account.reloadAccountType = function(){

		document.FormName.elements['ResetTSZBADisplay'].value='true';

		var	urlString = document.FormName.select1.options[document.FormName.select1.selectedIndex].value;
		$.ajax({
			url: urlString,
			type: 'post',
			data: $("#accountCriteriaForm").serialize(),
			success: function(data){
				$('#appdashboard').html(data);
			}
		});
	}

   ns.account.reloadView = function(){


		var	urlString = document.FormName.select2.options[document.FormName.select2.selectedIndex].value;

		$.ajax({
			url: urlString,
			type: 'post',
			data: $("#accountCriteriaForm").serialize(),
			success: function(data){
				$('#appdashboard').html(data);
			}
		});
	}
   ns.account.reloadConsumerView = function(urlString){

		//var	urlString = document.FormName.select2.options[document.FormName.select2.selectedIndex].value;

		$.ajax({
			url: urlString,
			type: 'post',
			data: $("#accountCriteriaForm").serialize(),
			success: function(data){
				$('#appdashboard').html(data);
			}
		});
	}
</script>

<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.CheckCriterionList" id="CriterionListChecker" scope="request" />
	<%-- Get the transaction types map (for bank-defined transaction groups) --%>
	<ffi:cinclude value1="${TransactionTypes}" value2="" operator="equals">
		<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />
		<ffi:setProperty name="TransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CORPORATE %>" />
		<ffi:process name="TransactionTypes" />
		<%-- Get the transaction groups list (for customer-defined transaction groups) --%>
		<ffi:object name="com.ffusion.tasks.business.GetTransactionGroups" id="CustomTransactionGroups" scope="request" />
		<ffi:process name="CustomTransactionGroups" />
	</ffi:cinclude>
</ffi:cinclude>

<%-- We need to determine whether we are entitled to intra day or previous day for any of the accounts --%>
<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="ACCOUNTGROUP!!0"/>
<ffi:setProperty name="BankingAccounts" property="FilterSortedBy" value="ACCOUNTGROUP,ID"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>

<%-- Calculate the range for last 30 days for default display --%>
<ffi:object id="GetDatesFromDateRange" name="com.ffusion.tasks.util.GetDatesFromDateRange" scope="request"/>
<%-- The date range value should be set in the calling page to the default range that should show up when the page is first loaded --%>
<ffi:cinclude value1="${getPagedTransactionsDateRangeValue}" value2="" operator="notEquals">
	<ffi:setProperty name="GetDatesFromDateRange" property="DateRangeValue" value="${getPagedTransactionsDateRangeValue}"/>
	<ffi:setProperty name="DateRangeValue" value="${getPagedTransactionsDateRangeValue}"/>
</ffi:cinclude>
<ffi:cinclude value1="${getPagedTransactionsDateRangeValue}" value2="" operator="equals">
<ffi:setProperty name="GetDatesFromDateRange" property="DateRangeValue" value="${DateRangeValue}"/>
</ffi:cinclude>
<ffi:process name="GetDatesFromDateRange"/>
<ffi:removeProperty name="getPagedTransactionsDateRangeValue"/>

<ffi:setProperty name="GetPagedTransactions" property="DateFormat" value="${UserLocale.DateFormat}"/>
<ffi:setProperty name="GetPagedTransactions" property="DateFormatParam" value="${UserLocale.DateFormat}"/>

<%-- This include jsp expects to be called from the an account history jsp --%>
<%-- This include jsp requires Account object to be set --%>

<ffi:cinclude value1="${ResetTSZBADisplay}" value2="true" operator="equals">
    <ffi:setProperty name="TSZBADisplay" value=""/>
</ffi:cinclude>

			<%-- AccountGroupID: AccountGroup to show in drop-down menus --%>
			<%-- Default is the AccountGroup for the current account --%>
			<ffi:setProperty name="StringUtil" property="Value1" value="${AccountGroupID}" />
			<ffi:setProperty name="StringUtil" property="Value2" value="${Account.AccountGroup}" />
			<ffi:setProperty name="AccountGroupID" value="${StringUtil.NotEmpty}" />
      <s:form method="post" name="FormName" id="accountCriteriaForm" theme="simple">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<input type="hidden" name="TSView" value="true" />
                <input type="hidden" name="ResetTSZBADisplay" value="false"/>

				<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="notEquals">
	 				<ffi:cinclude value1="${GetPagedTransactions.DataClassification}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>" operator="equals">
	 					<ffi:setProperty name="GetPagedTransactions" property="DataClassification" value="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY %>"/>
	 				</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="notEquals">
	 				<ffi:cinclude value1="${GetPagedTransactions.DataClassification}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY%>" operator="equals">
	 					<ffi:setProperty name="GetPagedTransactions" property="DataClassification" value="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY %>"/>
	 				</ffi:cinclude>
				</ffi:cinclude>

			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
				<ffi:setProperty name="corporateFieldStyle" value="display:block" />
				<ffi:setProperty name="consumerFieldStyle" value="display:none" />
			</ffi:cinclude>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
				<ffi:setProperty name="corporateFieldStyle" value="display:none" />
				<ffi:setProperty name="consumerFieldStyle" value="display:block" />
			</ffi:cinclude>


			<div id="corporateOnly" style="<ffi:getProperty name="corporateFieldStyle" />">
				<%-- Report On --%>
				<span id="reportOnSpan" class="sectionsubhead" style="margin-left: 11px; white-space: nowrap;"><s:text name="jsp.account_181"/></span></td>


				<select class="txtbox" name="GetPagedTransactions.DataClassification"  id="reportOn" onchange="ns.account.reloadReportOn();">
					<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
					<option value='<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY%>" value2="${DisplayDataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
					</ffi:cinclude>
					<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
					<option value='<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DisplayDataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
					</ffi:cinclude>
				</select>


					<%-- Account --%>
 				<ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
 				<%-- This is a detail page so we will filter accounts on the detail view entitlement --%>
 				<ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
 				<%-- we must retrieve the currently selected data classification and filter out any accounts that the user in not --%>
 				<%-- entitled to view under that data classification --%>
 				<ffi:cinclude value1="${GetPagedTransactions.DataClassification}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>" operator="equals">
 					<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>"/>
 				</ffi:cinclude>
 				<ffi:cinclude value1="${GetPagedTransactions.DataClassification}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY%>" operator="equals">
 					<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>"/>
 				</ffi:cinclude>

				<ffi:setProperty name="AccountEntitlementFilterTask" property="FilteredAccountsName" value="EntitlementFilteredAccounts"/>
 				<ffi:process name="AccountEntitlementFilterTask"/>

				<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="All"/>
				<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
				<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="EntitlementFilteredAccounts"/>
				<ffi:cinclude value1="${AccountFilter}" value2="" operator="notEquals">
				<ffi:setProperty name="SearchAcctsByNameNumType" property="GeneralSearch" value="${AccountFilter}"/>
				<%-- we are using the SAME list of accounts passed in as we are changing --%>
				<ffi:setProperty name="SearchAcctsByNameNumType" property="FilteredName" value="EntitlementFilteredAccounts"/>
				<ffi:process name="SearchAcctsByNameNumType"/>
				</ffi:cinclude>
				<ffi:removeProperty name="SearchAcctsByNameNumType,AccountFilter"/>

						<span class="sectionsubhead"><s:text name="jsp.default_21"/></span>

									<%-- StringUtil element to set selected option attribute --%>
									<ffi:setProperty name="StringUtil" property="Value1" value="${AccountGroupID}" />

									<% String defaultGroupID = null; %>
									<% String nonEmptyGroupIDs = ""; String emptyGroupIDs = ""; String thisGroupID = ""; %>

									<select class="txtbox" name="select1" id="select1ID" onChange="ns.account.reloadAccountType();">
										<%-- NOTE on the cincludes --%>
										<%-- we will filter the banking list for each of the account groups and then if the size of the filtered list is not 0 we will include that account type in this list --%>

										<ffi:setProperty name="URL1" value="/cb/pages/jsp/account/accounthistory.jsp" />
										<ffi:setProperty name="URL2" value="/cb/pages/jsp/account/accounthistory_asset.jsp" />
										<ffi:setProperty name="URL3" value="/cb/pages/jsp/account/accounthistory_loan.jsp" />
										<ffi:setProperty name="URL4" value="/cb/pages/jsp/account/accounthistory_ccard.jsp" />
										<ffi:setProperty name="URL5" value="/cb/pages/jsp/account/accounthistory_other.jsp" />

										<ffi:setProperty name="tmp_url" value="${URL1}?TransactionSearch=${TransactionSearch}&AccountGroupID=1&setaccount=true&LastGoodTSZBADisplay=" URLEncrypt="true" />

										<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="ACCOUNTGROUP=1"/>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="notEquals">
											<% nonEmptyGroupIDs += "1"; %>
											<ffi:cinclude value1="${defaultGroupID}" value2="" operator="equals">
												<ffi:setProperty name="defaultGroupID" value="1"/>
											</ffi:cinclude>

											<ffi:setProperty name="StringUtil" property="Value2" value="1" />
											<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="<ffi:getProperty name='tmp_url' />"><s:text name="jsp.account_76"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="equals">
											<% emptyGroupIDs += "1"; %>
											<ffi:cinclude value1="${AccountGroupID}" value2="1" operator="equals">
												<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
												<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}"/>
											</ffi:cinclude>
										</ffi:cinclude>

										<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="ACCOUNTGROUP=2"/>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="notEquals">
											<% nonEmptyGroupIDs += "2"; %>
											<ffi:cinclude value1="${defaultGroupID}" value2="" operator="equals">
												<ffi:setProperty name="defaultGroupID" value="2"/>
											</ffi:cinclude>
											<ffi:setProperty name="StringUtil" property="Value2" value="2" />
											<ffi:setProperty name="tmp_url" value="${URL2}?TransactionSearch=${TransactionSearch}&AccountGroupID=2&setaccount=true&LastGoodTSZBADisplay=" URLEncrypt="true" />
											<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value='<ffi:getProperty name="tmp_url" />'><s:text name="jsp.account_30"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="equals">
											<% emptyGroupIDs += "2"; %>
											<ffi:cinclude value1="${AccountGroupID}" value2="2" operator="equals">
												<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
												<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}"/>
											</ffi:cinclude>
										</ffi:cinclude>

										<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="ACCOUNTGROUP=4"/>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="notEquals">
											<% nonEmptyGroupIDs += "4"; %>
											<ffi:cinclude value1="${defaultGroupID}" value2="" operator="equals">
												<ffi:setProperty name="defaultGroupID" value="1"/>
											</ffi:cinclude>
											<ffi:setProperty name="StringUtil" property="Value2" value="4" />
											<ffi:setProperty name="tmp_url" value="${URL4}?TransactionSearch=${TransactionSearch}&AccountGroupID=4&setaccount=true&LastGoodTSZBADisplay=" URLEncrypt="true" />
											<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value='<ffi:getProperty name="tmp_url" />'><s:text name="jsp.account_58"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="equals">
											<% emptyGroupIDs += "4"; %>
											<ffi:cinclude value1="${AccountGroupID}" value2="4" operator="equals">
												<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
												<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}"/>
											</ffi:cinclude>
										</ffi:cinclude>

										<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="ACCOUNTGROUP=3"/>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="notEquals">
											<% nonEmptyGroupIDs += "3"; %>
											<ffi:cinclude value1="${defaultGroupID}" value2="" operator="equals">
												<ffi:setProperty name="defaultGroupID" value="1"/>
											</ffi:cinclude>
											<ffi:setProperty name="StringUtil" property="Value2" value="3" />
											<ffi:setProperty name="tmp_url" value="${URL3}?TransactionSearch=${TransactionSearch}&AccountGroupID=3&setaccount=true&LastGoodTSZBADisplay=" URLEncrypt="true" />
											<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value='<ffi:getProperty name="tmp_url" />'><s:text name="jsp.account_116"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="equals">
											<% emptyGroupIDs += "3"; %>
											<ffi:cinclude value1="${AccountGroupID}" value2="3" operator="equals">
												<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
												<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}"/>
											</ffi:cinclude>
										</ffi:cinclude>

										<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="ACCOUNTGROUP=5"/>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="notEquals">
											<% nonEmptyGroupIDs += "5"; %>
											<ffi:cinclude value1="${defaultGroupID}" value2="" operator="equals">
												<ffi:setProperty name="defaultGroupID" value="1"/>
											</ffi:cinclude>
											<ffi:setProperty name="StringUtil" property="Value2" value="5" />
											<ffi:setProperty name="tmp_url" value="${URL5}?TransactionSearch=${TransactionSearch}&AccountGroupID=5&setaccount=true&LastGoodTSZBADisplay=" URLEncrypt="true" />
											<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value='<ffi:getProperty name="tmp_url" />'><s:text name="jsp.account_155"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="equals">
											<% emptyGroupIDs += "5"; %>
											<ffi:cinclude value1="${AccountGroupID}" value2="5" operator="equals">
												<%--this group is not entitled for this dataclassification so we should set the account id to that of the default group--%>
												<ffi:setProperty name="AccountGroupID" value="${defaultGroupID}"/>
											</ffi:cinclude>
										</ffi:cinclude>
									</select>

									<ffi:getProperty name="AccountGroupID" assignTo="thisGroupID"/>
									<% if (thisGroupID != null && emptyGroupIDs.length() > 0 && emptyGroupIDs.indexOf(thisGroupID) >= 0)
									{
										if (nonEmptyGroupIDs.length() > 0)
											thisGroupID = nonEmptyGroupIDs.substring(0,1);      // AccountGroupID was pointing to empty list
									} %>
									<ffi:setProperty name="AccountGroupID" value="<%=thisGroupID%>"/>

									<select class="txtbox" name="select2" id="select2ID" onChange="ns.account.reloadAccount('corporate');">
										<% String strURLValue="";%>
										<ffi:getProperty name="URL${AccountGroupID}" assignTo="strURLValue" />
										<%-- StringUtil element to set selected option attribute --%>
										<%--if it is redirected from other module --%>
										<ffi:cinclude value1="${redirection}" value2="true" operator="equals">
									    	<ffi:setProperty name="StringUtil" property="Value1" value="${DisplayAccountID}" />
										</ffi:cinclude>
										<ffi:cinclude value1="${redirection}" value2="" operator="equals">
										<ffi:setProperty name="StringUtil" property="Value1" value="${DisplayAccount.ID}" />
										</ffi:cinclude>
										<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="ACCOUNTGROUP=${AccountGroupID}" />

										<ffi:cinclude value1="${setaccount}" value2="true" operator="equals">
										<ffi:setProperty name="setaccount" value=""/>
										<ffi:list collection="EntitlementFilteredAccounts" items="acct" startIndex="1" endIndex="1">
											<ffi:setProperty name="SetAccount" property="ID" value="${acct.ID}" />
											<ffi:setProperty name="SetAccount" property="RoutingNum" value="${acct.RoutingNum}" />
											<ffi:setProperty name="SetAccount" property="BankID" value="${acct.BankID}" />
											<ffi:process name="SetAccount"/>
										</ffi:list>
										</ffi:cinclude>

										<ffi:list collection="EntitlementFilteredAccounts" items="acct">
												<ffi:setProperty name="StringUtil" property="Value2" value="${acct.ID}" />
												<ffi:setProperty name="tmp_url" value="${strURLValue}?TransactionSearch=${TransactionSearch}&SetAccount.ID=${acct.ID}&SetAccount.BankID=${acct.BankID}&SetAccount.RoutingNum=${acct.RoutingNum}&ProcessAccount=true&GetPagedTransactions.Page=first&ProcessTransactions=true&TransactionSearch=${TransactionSearch}&LastGoodTSZBADisplay=" URLEncrypt="true" />
												<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="<ffi:getProperty name='tmp_url'/>">
													<ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals"><ffi:getProperty name="acct" property="RoutingNum"/>:</ffi:cinclude><ffi:getProperty name="acct" property="DisplayText"/><ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" > - <ffi:getProperty name="acct" property="NickName"/></ffi:cinclude>	- <ffi:getProperty name="acct" property="CurrencyCode"/>
												</option>
										</ffi:list>
									</select>


					<% String zbaFlag = null; %>
					<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
						<ffi:getProperty name="TSZBADisplay" assignTo="zbaFlag"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TSZBADisplay}" value2="" operator="equals">
						<ffi:setProperty name="DisplayTSZBADisplay" value="${Account.ZBAFlag}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
						<ffi:cinclude value1="${GetPagedTransactions}" value2="" operator="notEquals">
							<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ZBA_DISPLAY %>" />
							<ffi:getProperty name="GetPagedTransactions" property="SearchCriteriaValue" assignTo="zbaFlag"/>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="<%= zbaFlag %>" value2="" operator="equals">
						<ffi:getProperty name="Account" property="ZBAFlag" assignTo="zbaFlag"/>
					</ffi:cinclude>
					<ffi:cinclude value1="<%= zbaFlag %>" value2="" operator="equals">
						<%
							zbaFlag = com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH;
						%>
					</ffi:cinclude>
					<ffi:cinclude value1="${Account.ZBAFlag}" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE%>" operator="notEquals">

						<span class="sectionsubhead" style="margin-left: 20px; white-space: nowrap;"><s:text name="jsp.account_229.1"/></span>

										&nbsp;<select class="txtbox" name="TSZBADisplay" id="zbaDisplay">
											<option value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE%>" ><s:text name="jsp.default_296"/></option>
											<ffi:cinclude value1="${Account.ZBAFlag}" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH%>" operator="equals">
												<option value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY %>" <ffi:cinclude value1="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY %>" value2="${DisplayTSZBADisplay}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_471"/></option>
												<option value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY %>" <ffi:cinclude value1="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY %>" value2="${DisplayTSZBADisplay}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_472"/></option>
												<option value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH %>" <ffi:cinclude value1="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH %>" value2="${DisplayTSZBADisplay}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.account_42.1"/></option>
											</ffi:cinclude>
											<ffi:cinclude value1="${Account.ZBAFlag}" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY%>" operator="equals">
												<option value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY %>" <ffi:cinclude value1="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_CREDIT_ONLY %>" value2="${DisplayTSZBADisplay}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_471"/></option>
											</ffi:cinclude>
											<ffi:cinclude value1="${Account.ZBAFlag}" value2="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY%>" operator="equals">
												<option value="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY %>" <ffi:cinclude value1="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_DEBIT_ONLY %>" value2="${DisplayTSZBADisplay}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_472"/></option>
											</ffi:cinclude>
										</select>

					</ffi:cinclude>
					</div>
					<%-- corporate only row ends test --%>
					<br style="height:0;"/>

					<%-- consumer account field starts --%>
					<div id="consumerOnly" style="<ffi:getProperty name="consumerFieldStyle" />">

					<ffi:setProperty name="URL1" value="/cb/pages/jsp/account/accounthistory.jsp" />
					<% String strURLConsumerValue="";%>
					<ffi:getProperty name="URL1" assignTo="strURLConsumerValue" />
						<!--<span class="sectionsubhead" style="margin-left: 25px; white-space: nowrap;"><s:text name="jsp.default_21"/></span>-->
						<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
							<span class="sectionsubhead" style="margin-left: 25px; white-space: nowrap;"><s:text name="jsp.default_21"/></span>
							<select class="txtbox" name="select22" id="consumerAccountsDropDown" onChange="ns.account.reloadAccount('consumer');" >

								<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND"/>
								<ffi:setProperty name="BankingAccounts" property="SortedBy" value="NICKNAME" />
								<ffi:setProperty name="StringUtil" property="Value1" value="${Account.ID}" />

								<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND"/>
								<ffi:list collection="BankingAccounts" items="Account1">
									<ffi:setProperty name="StringUtil" property="Value2" value="${Account1.ID}" />
							      	<ffi:setProperty name="Compare" property="Value2" value="${Account1.ID}"/>
									<ffi:setProperty name="tmp_url" value="${strURLConsumerValue}?TransactionSearch=${TransactionSearch}&SetAccount.ID=${Account1.ID}&SetAccount.BankID=${Account1.BankID}&SetAccount.RoutingNum=${Account1.RoutingNum}&ProcessAccount=true&GetPagedTransactions.Page=first&ProcessTransactions=true&TransactionSearch=${TransactionSearch}&LastGoodTSZBADisplay=" URLEncrypt="true" />
									<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="<ffi:getProperty name='tmp_url'/>">
										<ffi:getProperty name="Account1" property="ConsumerMenuDisplayText"/>
									</option>
								</ffi:list>

									<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,AND"/>
									<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
									<option value="separator"><ffi:getProperty name="separator"/></option>
									<ffi:list collection="BankingAccounts" items="Account1">
										<ffi:setProperty name="StringUtil" property="Value2" value="${Account1.ID}" />
										<ffi:setProperty name="Compare" property="Value2" value="${Account1.ID}"/>
										<ffi:setProperty name="tmp_url" value="${strURLConsumerValue}?TransactionSearch=${TransactionSearch}&SetAccount.ID=${Account1.ID}&SetAccount.BankID=${Account1.BankID}&SetAccount.RoutingNum=${Account1.RoutingNum}&ProcessAccount=true&GetPagedTransactions.Page=first&ProcessTransactions=true&TransactionSearch=${TransactionSearch}&LastGoodTSZBADisplay=" URLEncrypt="true" />
										<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="<ffi:getProperty name='tmp_url'/>">
											<ffi:getProperty name="Account1" property="ConsumerMenuDisplayText"/>
										</option>
									</ffi:list>
									</ffi:cinclude>
								<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,COREACCOUNT=1"/>
							</select>
						</ffi:cinclude>
						<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
							<span class="sectionsubhead" style="vertical-align: 20px; vertical-align: 10px \9;margin-left: 25px; white-space: nowrap;"><s:text name="jsp.default_21"/></span>
							<%-- ns.account.reloadConsumerView() --%>

							<ffi:setProperty name="currSelectedAccounts" value="${DisplayConsumerAccountsMultiSelectDropDown}" />
							<%-- set default account selected --%>
							<ffi:cinclude value1="" value2="${currSelectedAccounts}" operator="equals">
								<ffi:setProperty name="currSelectedAccounts" value="${Account.ID}" />
							</ffi:cinclude>

							<select class="ui-widget-content ui-corner-all" style="width:300px; margin-bottom: 5px;" name="consumerAccountsMultiSelectDropDown" id="consumerAccountsMultiSelectDropDown" size="3" multiple="multiple">

							<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currSelectedAccounts" />
							<ffi:process name="CriterionListChecker" />

							<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND"/>
							<ffi:setProperty name="BankingAccounts" property="SortedBy" value="NICKNAME" />
							<ffi:setProperty name="StringUtil" property="Value1" value="${Account.ID}" />

							<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=1,AND"/>
							<ffi:list collection="BankingAccounts" items="Account1">
								<ffi:setProperty name="StringUtil" property="Value2" value="${Account1.ID}" />
								<ffi:setProperty name="tmp_url" value="${strURLConsumerValue}?TransactionSearch=${TransactionSearch}&SetAccount.ID=${Account1.ID}&SetAccount.BankID=${Account1.BankID}&SetAccount.RoutingNum=${Account1.RoutingNum}&ProcessAccount=true&GetPagedTransactions.Page=first&ProcessTransactions=true&TransactionSearch=${TransactionSearch}&LastGoodTSZBADisplay=" URLEncrypt="false" />
								<%--
								<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="<ffi:getProperty name='tmp_url'/>">
									<ffi:getProperty name="Account1" property="ConsumerMenuDisplayText"/>
								</option>
								--%>
								<option value="<ffi:getProperty name='tmp_url'/>"
								<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>" />
								<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
								>
									<ffi:getProperty name="Account1" property="ConsumerMenuDisplayText"/>
								</option>
							</ffi:list>

								<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,AND"/>
								<ffi:cinclude value1="0" value2="${BankingAccounts.Size}" operator="notEquals">
								<option value="separator"><ffi:getProperty name="separator"/></option>
								<ffi:list collection="BankingAccounts" items="Account1">
									<ffi:setProperty name="StringUtil" property="Value2" value="${Account1.ID}" />
									<ffi:setProperty name="tmp_url" value="${strURLConsumerValue}?TransactionSearch=${TransactionSearch}&SetAccount.ID=${Account1.ID}&SetAccount.BankID=${Account1.BankID}&SetAccount.RoutingNum=${Account1.RoutingNum}&ProcessAccount=true&GetPagedTransactions.Page=first&ProcessTransactions=true&TransactionSearch=${TransactionSearch}&LastGoodTSZBADisplay=" URLEncrypt="false" />
									<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="<ffi:getProperty name='tmp_url'/>">
										<ffi:getProperty name="Account1" property="ConsumerMenuDisplayText"/>
									</option>
								</ffi:list>
								</ffi:cinclude>
								<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,COREACCOUNT=1"/>
							</select>
						</ffi:cinclude>
			<br style="height:0;"/>
			</div>
			<%--consumer account field ends --%>





					<span class="sectionsubhead" style="margin-left: 40px;"><s:text name="jsp.default_143"/></span>

											<%-- StringUtil element to display text in date fields --%>

											<ffi:setProperty name="StringUtil" property="Value2" value="${GetDatesFromDateRange.StartDate}" />
											<ffi:setProperty name="StringUtil" property="Value1" value="${DisplayStartDate}" />

											<sj:datepicker value="%{#session.StringUtil.notEmpty}" id="startDate" name="GetPagedTransactions.StartDate"
											label="%{getText('jsp.default_137')}" maxlength="10" onfocus="clearDropDown();"
											buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
											<script>
												var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=FormName&calTarget=GetPagedTransactions.StartDate"/>';
							                    ns.common.enableAjaxDatepicker("startDate", tmpUrl);
							                    $("#startDate").val("<ffi:getProperty name="StringUtil" property="notEmpty"/>");
											</script>

												<span id="toDateLabelID" class="sectionsubhead" style="margin-left: 15px;"><s:text name="jsp.default_423.1"/></span>

											<ffi:setProperty name="StringUtil" property="Value2" value="${GetDatesFromDateRange.EndDate}" />
											<ffi:setProperty name="StringUtil" property="Value1" value="${DisplayEndDate}" />

											<sj:datepicker value="%{#session.StringUtil.notEmpty}" id="endDate" name="GetPagedTransactions.EndDate"
											label="%{getText('jsp.default_137')}" maxlength="10" onfocus="clearDropDown();"
											buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>

											<script>
												var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=FormName&calTarget=GetPagedTransactions.EndDate"/>';
							                    ns.common.enableAjaxDatepicker("endDate", tmpUrl);
							                    $("#endDate").val("<ffi:getProperty name="StringUtil" property="notEmpty"/>");
											</script>

									<%-- common date dropdown starts --%>

									<%
									String previousDateRangeValue = null;
									%>
									<ffi:getProperty name="GetPagedTransactions.DateRangeValue" assignTo="previousDateRangeValue"/>

									<ffi:cinclude value1="${unsuccessfulSearch}" value2="true" operator="equals">
										<% previousDateRangeValue = ""; %>
									</ffi:cinclude>

									<label style="margin-left: 15px;" for=""><s:text name="jsp.transfers_text_Or"/></label>&nbsp;&nbsp;&nbsp;
													<select id="dateRangeValue" class="txtbox"
													name="GetPagedTransactions.DateRangeValue"
													onChange="clearDateTextFields();">
														<option value=""><s:text name="jsp.transfers_text_select"/></option>
														<option value="<%= GetDatesFromDateRange.TODAY %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.TODAY %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_today"/></option>
														<option value="<%= GetDatesFromDateRange.YESTERDAY %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.YESTERDAY %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_yesterday"/></option>
														<option value="<%= GetDatesFromDateRange.CURRENT_WEEK %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.CURRENT_WEEK %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_current_task"/></option>
														<option value="<%= GetDatesFromDateRange.LAST_WEEK %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_WEEK %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_lastweek"/></option>
														<option value="<%= GetDatesFromDateRange.LAST_7_DAYS %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_7_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_last7days"/></option>
														<option value="<%= GetDatesFromDateRange.LAST_30_DAYS %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_30_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_last30days"/></option>
														<option value="<%= GetDatesFromDateRange.LAST_60_DAYS %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_60_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_last60days"/></option>
														<option value="<%= GetDatesFromDateRange.LAST_90_DAYS %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_90_DAYS %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_last90days"/></option>
														<option value="<%= GetDatesFromDateRange.CURRENT_MONTH %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.THIS_MONTH_TO_DATE %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_current_month"/></option>
														<option value="<%= GetDatesFromDateRange.LAST_MONTH %>" <ffi:cinclude value1="${previousDateRangeValue}" value2="<%= GetDatesFromDateRange.LAST_MONTH %>"> <s:text name="jsp.transfers_text_selected"/> </ffi:cinclude> ><s:text name="jsp.transfers_text_lastmonth"/></option>
													</select>

									<%-- common date dropdown ends --%>


									<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
										<sj:a
											id="viewAccountHistoryID1"
											button="true"
											onclick="ns.account.reloadView()"
											><s:text name="jsp.default_6"/></sj:a>
									</ffi:cinclude>
				<br style="height:0;">
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
					<span class="sectionsubhead"><s:text name="jsp.account_178"/></span>
					<%-- StringUtil element to display text in date fields --%>
					<input class="ui-widget-content ui-corner-all" style="margin: 5px 0 5px 5px; width: 110px \9;" type="text" name="TSReferenceStart" value="<ffi:getProperty name="DisplayTSReferenceStart" />" size="20" maxlength="10">
					<span id="toDateLabelID" class="sectionsubhead" style="margin-left: 32px;"><s:text name="jsp.default_423.1"/></span>
					<input class="ui-widget-content ui-corner-all" style="margin: 5px 0 5px 5px; width: 110px \9;" type="text" name="TSReferenceEnd" value="<ffi:getProperty name="DisplayTSReferenceEnd" />" size="20" maxlength="10">
					<span class="sectionsubhead" style="margin-left: 32px;"><s:text name="jsp.default_45"/></span>
					<%-- StringUtil element to display text in date fields --%>
					<input class="ui-widget-content ui-corner-all" style="margin: 5px 0 5px 5px; width: 110px \9;" type="text" name="TSMinimumAmount" value="<ffi:getProperty name="DisplayTSMinimumAmount" />" size="20" maxlength="10">
					<span id="toDateLabelID" class="sectionsubhead" style="margin-left: 32px;"><s:text name="jsp.default_423.1"/></span>
					<input class="ui-widget-content ui-corner-all" style="margin: 5px 0 5px 5px; width: 110px \9;" type="text" name="TSMaximumAmount" value="<ffi:getProperty name="DisplayTSMaximumAmount" />" size="20" maxlength="10">
					<br style="height:0;">
					<span class="sectionsubhead" style="vertical-align: 20px; vertical-align: 10px \9;"><s:text name="jsp.account_217"/>:</span>

										<ffi:setProperty name="currTransType" value="${DisplayTSTransactionType}" />
										<%-- set default transaction type --%>
										<ffi:cinclude value1="" value2="${currTransType}" operator="equals">
											<ffi:setProperty name="currTransType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>" />
										</ffi:cinclude>
										<select class="ui-widget-content ui-corner-all" style="width:300px; margin-bottom: 5px;" name="TSTransactionType" size="3" MULTIPLE>

											<ffi:setProperty name="CriterionListChecker" property="CriterionListFromSession" value="currTransType" />
											<ffi:process name="CriterionListChecker" />

											<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>"
												<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>" />
												<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
											>
												<s:text name="jsp.default_42"/>
											</option>

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_CREDIT_TRANSACTIONS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_CREDIT_TRANSACTIONS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_40"/>
			</option>

			<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_DEBIT_TRANSACTIONS %>"
				<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_DEBIT_TRANSACTIONS %>" />
				<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
			>
				<s:text name="jsp.default_41"/>
			</option>
											<ffi:setProperty name="TransactionTypes" property="SortField" value="<%= com.ffusion.tasks.util.GetTransactionTypes.SORTFIELD_DESCRIPTION %>" />
											<ffi:list collection="TransactionTypes.TypeList" items="keyValuePair">
												<ffi:cinclude value1="${keyValuePair.Value}" value2 ="Unknown" operator="notEquals">
												<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${keyValuePair.Key}" />
												<option
													value="<ffi:getProperty name='keyValuePair' property='Key'/>"
													<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
												>
													<ffi:getProperty name="keyValuePair" property="Value"/>
												</option>
												</ffi:cinclude>
											</ffi:list>

											<%-- Check the size of the TransactionGroups, if 0, we do not add the dividing line --%>
											<%-- We only try to add the items only if TransactionGroups has items in it --%>

											<% String TransactionGroupsSize=null; %>
											<ffi:getProperty name="CustomTransactionGroups.TransactionGroups" property="Size" assignTo="TransactionGroupsSize" />

											<ffi:cinclude value1="${TransactionGroupsSize}" value2="" operator="notEquals">
												<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DIVIDER %>"><s:text name="jsp.default_9"/></option>
												<ffi:setProperty name="custTransGroupPrefix" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %>"/>
												<ffi:list collection="CustomTransactionGroups.TransactionGroups" items="customGroupName">
													<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${custTransGroupPrefix}${customGroupName}" />
													<option
														value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %><ffi:getProperty name='customGroupName'/>"
														<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
													>
														<ffi:getProperty name="customGroupName"/>
													</option>
												</ffi:list>
												<ffi:removeProperty name="custTransGroupPrefix"/>
											</ffi:cinclude>
											<ffi:removeProperty name="TransactionGroupsSize"/>
										</select>


							<sj:a
								id="viewAccountHistoryID2"
								button="true"
								onclick="ns.account.reloadView()"
								><s:text name="jsp.default_6"/></sj:a>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_GROUPS%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSTransactionType}"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ZBA_DISPLAY%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSZBADisplay}"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MIN%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSReferenceStart}"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MAX%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSReferenceEnd}"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSMinimumAmount}"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${TSMaximumAmount}"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
					<input type="hidden" name="TSReferenceStart" value="">
					<input type="hidden" name="TSReferenceEnd" value="">
					<input type="hidden" name="TSMinimumAmount" value="">
					<input type="hidden" name="TSMaximumAmount" value="">
					<input type="hidden" name="TSTransactionType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>">
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_GROUPS%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MIN%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value=""/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MAX%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value=""/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value=""/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX%>"/>
					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value=""/>
				</ffi:cinclude>

                 </s:form>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
<ffi:setProperty name="EntitlementFilteredAccounts" property="Filter" value="All"/>

<ffi:removeProperty name="EntitlementFilteredAccounts"/>
<ffi:removeProperty name="CheckPerAccountReportingEntitlements"/>
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
<ffi:removeProperty name="ResetTSZBADisplay"/>
<ffi:removeProperty name="redirection"/>
<ffi:removeProperty name="DisplayAccount"/>