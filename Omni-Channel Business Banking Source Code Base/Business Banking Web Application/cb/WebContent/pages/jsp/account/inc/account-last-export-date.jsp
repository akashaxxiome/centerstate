<%@ page import="java.util.*"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<%
	String accountText = null;
	String acctId=null; 
	accountText=request.getParameter("acctText");
	if(accountText != null){
		StringTokenizer st = new StringTokenizer(accountText,":");
			if(st.hasMoreTokens()) {
	         	acctId= st.nextToken();
	     	}
	}
%>

<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
<ffi:setProperty name="SetAccount" property="ID" value="<%=acctId%>"/>
<ffi:process name="SetAccount"/>

<ffi:object name="com.ffusion.tasks.banking.GetLastExportedDate" id="GetLastExportedDate" scope="session" />
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<ffi:setProperty name="GetLastExportedDate" property="ReportType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY %>"/>
</ffi:cinclude>		
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:setProperty name="GetLastExportedDate" property="ReportType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_ACCT_HISTORY %>"/>
</ffi:cinclude>		
<ffi:setProperty name="GetLastExportedDate" property="DateFormat" value="${UserLocale.DateTimeFormat}"/>
<ffi:process name="GetLastExportedDate"/>

<ffi:cinclude value1="" value2="${GetLastExportedDate.LastExporteddate}" operator="equals">
		<s:text name="jsp.default.label.not.previously.exported"/>
</ffi:cinclude>
<ffi:cinclude value1="" value2="${GetLastExportedDate.LastExporteddate}" operator="notEquals">
		<ffi:getProperty name="GetLastExportedDate" property="LastExportedDate"/>
</ffi:cinclude>
