<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<s:set name="appType" value="%{#session.SecureUser.AppType}"/>
<s:set name="accHistryDataClassiFication" value="accountHistorySearchCriteria.DataClassification"/>
<s:set name="AccountGroupID" value="accountHistorySearchCriteria.Account.AccountGroup"/>
<s:set name="accHistryZBAFlag" value="@com.sap.banking.accountconfig.constants.AccountConstants@ZBA_BOTH"/>
<s:set name="accHistryDateRange" value="accountHistorySearchCriteria.TransactionSearchCriteria.DateRangeValue"/>


  
<div class="acntDashboard_masterItemHolder marginTop10 quickSearchAreaCls marginleft10" data-banking-container-type="search" >	   
		<s:if test="%{#session.SecureUser.AppType=='Business'}">
			<div id="corporateOnly" style="<ffi:getProperty name="corporateFieldStyle" />" class="floatleft marginRight5">
				<table border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td>
					<%-- ACCOUNT --%>
					<div class="acntDashboard_itemHolder">
						<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default_20" /></span>
						<select class="ui-widget-content ui-corner-all textbox" name="accountGroup" id="accountGroupID" onChange="ns.account.reloadAccountType();">
							<option value=<s:property value='depositAccounts'/>	<s:if test="%{#AccountGroupID == #depositAccounts}">selected</s:if>><s:text name="jsp.account_76" /></option>
							<option value=<s:property value='assetAccounts'/> <s:if test="%{#AccountGroupID == #assetAccounts}">selected</s:if>><s:text name="jsp.account_30" /></option>
							<!--<option value=<s:property value='creditCards'/>	<s:if test="%{#AccountGroupID == #creditCards}">selected</s:if>><s:text name="jsp.account_58" /></option>-->
							<option value=<s:property value='loanAccounts'/> <s:if test="%{#AccountGroupID == #loanAccounts}">selected</s:if>><s:text name="jsp.account_116" /></option>
							<!--<option value=<s:property value='otherAccounts'/> <s:if test="%{#AccountGroupID == #otherAccounts}">selected</s:if>><s:text name="jsp.account_155" /></option>-->
						</select>
						
					</div>
				</td>
				<td>	
					<%-- ACCOUNT TYPE --%>
					<div class="acntDashboard_itemHolder accountSelectMenu">
						<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default_21" /></span>
						<s:if test="%{#session.TransactionSearch !='true'}">				
							<select class="ui-widget-content ui-corner-all" name="accountHistorySearchCriteria.selectedAccountOption" id="corporateAccountID"
							onChange="ns.common.populateAccountDetails();">
								<ffi:list collection="accountsList" items="AccountList" startIndex="1" endIndex="1">
									<ffi:setProperty name="AccountDisplayText" value="${AccountList.AccountDisplayText}" />
									<ffi:setProperty name="selectAccountValue" value="${AccountList.ID},${AccountList.BankID},${AccountList.RoutingNum},${AccountList.AccountGroup},${TransactionSearch}" />
								</ffi:list>
								<option  value="<ffi:getProperty name='selectAccountValue'/>" ><ffi:getProperty name="AccountDisplayText"/> </option>
							</select> 
							
							
						</s:if>
					</div>
				</td>
				<td>	
					<%--DATE RANGE COMPONENT --%>
					<div class="acntDashboard_itemHolder">
						<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default.label.date.range"/></span>
						<input type="text" id="accountsDateRangeBox" />
			
						<input value="<ffi:getProperty name='accountHistorySearchCriteria.transactionSearchCriteria' property='startDate' />" id="startDate" name="accountHistorySearchCriteria.transactionSearchCriteria.startDate"
						type="hidden" />
						<input value="<ffi:getProperty name='accountHistorySearchCriteria.transactionSearchCriteria' property='endDate' />" id="endDate" name="accountHistorySearchCriteria.transactionSearchCriteria.endDate"
						type="hidden"/>
						
						
					</div>	  
				</td>
				<td>	
					<%-- REPORT ON --%>
					<div class="acntDashboard_itemHolder">
						<span id="reportOnSpan" class="sectionsubhead dashboardLabelMargin marginRight10"><s:text name="jsp.account_181" /></span>
						<select class="ui-widget-content ui-corner-all"	name="accountHistorySearchCriteria.dataClassification" id="reportOn">
							<option	value=<s:property value="dataIntraDay"/>
								<s:if test="%{#accHistryDataClassiFication == #dataIntraDay}">selected</s:if>>
								<s:text name="jsp.default_252" />
							</option>
							<option	value=<s:property value="dataPrevDay"/>
								<s:if test="%{#accHistryDataClassiFication == #dataPrevDay}">selected</s:if>>
								<s:text name="jsp.default_330" />
							</option>
						</select>
						
						
					</div>
				</td>
				<td>	 
					<%--ZBA FLAG --%>
					<s:if test="%{#accHistryZBAFlag != #zbaNoneConst}">
						<div class="acntDashboard_itemHolder">
							<span class="sectionsubhead dashboardLabelMargin marginRight10"><s:text name="jsp.account_229.1" /></span>
							<select class="ui-widget-content ui-corner-all" name="accountHistorySearchCriteria.zbaFlag"	id="zbaDisplay">
								<option value=<s:property value='zbaNoneConst'/>>
									<s:text name="jsp.default_296" />
								</option>
								<s:if test="%{#accHistryZBAFlag == #zbaBothConst}">
									<option	value=<s:property value="zbaCreditConst"/>
										<s:if test="%{#accHistryZBAFlag == #zbaCreditConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
										<s:text name="jsp.default_471" />
									</option>
									<option value=<s:property value="zbaDebitConst"/>
										<s:if test="%{#accHistryZBAFlag == #zbaDebitConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
										<s:text name="jsp.default_472" />
									</option>
									<option value=<s:property value="zbaBothConst"/>
										<s:if test="%{#accHistryZBAFlag == #zbaBothConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
										<s:text name="jsp.account_42.1" />
									</option>
								</s:if>
								<s:if test="%{#accHistryZBAFlag == #zbaCreditConst}">
									<option value=<s:property value="zbaCreditConst"/>
										<s:if test="%{#accHistryZBAFlag == #zbaCreditConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
										<s:text name="jsp.default_471" />
									</option>
								</s:if>
								<s:if test="%{#accHistryZBAFlag == #zbaDebitConst}">
									<option value=<s:property value="zbaDebitConst"/>
										<s:if test="%{#accHistryZBAFlag == #zbaDebitConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
										<s:text name="jsp.default_472" />
									</option>
								</s:if>
							</select>
							
						</div>
					</s:if>
				</td>
				<td>
					<%-- Right side components --%>
					<s:if test="%{#session.SecureUser.AppType =='Business'}">
						<div class="acntDashboard_itemHolder floatLeft">
							<span class="sectionsubhead dashboardLabelMargin" style="height:15px;">&nbsp;</span>
							<sj:a id="viewAccountHistoryID1" formIds="accountCriteriaForm" button="true"
								targets="resultmessage" validate="true" validateFunction="customValidation"
								onclick="ns.common.populateAccountDetails(),ns.account.resetOperationFlag();"
								onBeforeTopics="beforeAccountHistorySearch"
								onCompleteTopics="confirmAccountHistorySearch"
								onErrorTopics="errorAccountHistorySearch"
								onSuccessTopics="successAccountHistorySearch">
								<s:text name="jsp.default_6" />
							</sj:a>
							<sj:a
								id="exportHistoryButton1" formIds="accountCriteriaForm"
								button="true"
								targets="resultmessage" validate="true" validateFunction="customValidation"
								onclick="ns.common.populateAccountDetails(),ns.account.setOperationFlag('EXPORT');"
								onBeforeTopics="beforeExportHistory"
								onSuccessTopics="successExportHistory"> 
								<s:text name="jsp.default_195"/>
							</sj:a>
						</div>
					</s:if>
				</td>
			</tr>
			<tr>
				<td><span id="accountSearchCriteria.account.accountGroupError"  class="errorLabel imgSrcErrorMsg">&nbsp;</span></td>
				<td><span id="selectedAccountOptionError" class="errorLabel imgSrcErrorMsg">&nbsp;</span></td>
				<td>
					<span id="transactionSearchCriteria.dateRangeValueError" class="errorLabel imgSrcErrorMsg"></span>
					<!-- Adding below spans to display TransactionSearchCriteria's field level @size validations.  -->
					<span id="transactionSearchCriteria.endDateError"  class="errorLabel imgSrcErrorMsg"/>
					<span id="transactionSearchCriteria.dateRangeNodeNameError" class="errorLabel imgSrcErrorMsg"></span>
					<span id="transactionSearchCriteria.startDtNodeNameError" class="errorLabel imgSrcErrorMsg"></span>
					<span id="transactionSearchCriteria.startDateError" class="errorLabel imgSrcErrorMsg"></span>
					<span id="transactionSearchCriteria.endDtNodeNameError" class="errorLabel imgSrcErrorMsg"></span> 
				</td>
				<td><span id="dataClassificationError" class="errorLabel imgSrcErrorMsg">&nbsp;</span></td>
				<td><span id="zbaFlagError" class="errorLabel imgSrcErrorMsg">&nbsp;</span>	</td>
				<td>&nbsp;</td>
			</tr>
		</table>		
	</div>
</s:if>
		<%-- corporate only row ends test --%>


					
					
		<%-- consumer account field starts --%>
		<s:if test="%{#session.SecureUser.AppType !='Business'}">
			<div id="consumerOnly" style="<ffi:getProperty name="consumerFieldStyle" />" class="floatleft">
				<table border="0" cellspacing="0" cellpadding="3">
					<tr>
						<s:if test="%{#session.TransactionSearch !='true'}">	
							<% String strURLConsumerValue = "";%>
							<ffi:getProperty name="URL1" assignTo="strURLConsumerValue" />
							<td>
								<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default_21" /></span>
								<select class="ui-widget-content ui-corner-all" name="accountHistorySearchCriteria.selectedAccountOption"
									id="consumerAccountsDropDown"
									onChange="ns.account.reloadAccount(),ns.common.adjustSelectMenuWidth('consumerAccountsDropDown'),ns.common.populateAccountDetails();">
									<ffi:list collection="accountsList" items="AccountList1">
										<ffi:cinclude value1="1" value2="${AccountList1.CoreAccount}" operator="equals">
											<ffi:setProperty name="AccountDisplayText1" 
												value="${AccountList1.AccountDisplayText}" />
											<ffi:setProperty name="selectAccountValue" value="${AccountList1.ID},${AccountList1.BankID},${AccountList1.RoutingNum},${AccountList1.AccountGroup},${TransactionSearch}" />
											<option  value="<ffi:getProperty name='selectAccountValue' />" ><ffi:getProperty name="AccountList1" property="accountDisplayText" /> </option>
										</ffi:cinclude>
									</ffi:list>
									<%-- <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,AND" /> --%>
									<ffi:cinclude value1="0" value2="${accountsList.Size}" operator="notEquals">
										<% int count = 0; %>
										<ffi:list collection="accountsList" items="AccountList1">
											<ffi:cinclude value1="0" value2="${AccountList1.CoreAccount}" operator="equals">
											<% if (count < 1){ %>
													<option disabled="disabled" value="separator"><ffi:getProperty name="separator" /></option>
												<% }
												count++;
												%>
												<ffi:setProperty name="selectAccountValue" value="${AccountList1.ID},${AccountList1.BankID},${AccountList1.RoutingNum},${AccountList1.AccountGroup},${TransactionSearch}" />
												<option  value="<ffi:getProperty name='selectAccountValue' />" ><ffi:getProperty name="AccountList1" property="accountDisplayText" /> </option>
											</ffi:cinclude>
										</ffi:list>
										<% count = 0; %>
									</ffi:cinclude>
									<%-- <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,COREACCOUNT=1" /> --%>
								</select>
							</td>
						</s:if>
						 <td class="marginleft5">
							<!-- DATE RANGE COMPONENT IN CONSUMER -->
							<!-- <div class="acntDashboard_itemHolder"> -->
								<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default.label.date.range"/></span>
								<input type="text" id="accountsDateRangeBox" />
					
								<input value="<ffi:getProperty name='accountHistorySearchCriteria.transactionSearchCriteria' property='startDate' />" id="startDate" name="accountHistorySearchCriteria.transactionSearchCriteria.startDate"
								type="hidden" />
								<input value="<ffi:getProperty name='accountHistorySearchCriteria.transactionSearchCriteria' property='endDate' />" id="endDate" name="accountHistorySearchCriteria.transactionSearchCriteria.endDate"
								type="hidden"/>
								
								<span id="transactionSearchCriteria.dateRangeValueError" class="errorLabel imgSrcErrorMsg" style="display:block;"></span>
								<!-- Adding below spans to display TransactionSearchCriteria's field level @size validations.  -->
								<span id="transactionSearchCriteria.endDateError" class="errorLabel imgSrcErrorMsg" style="display:block;"/>
								<span id="transactionSearchCriteria.dateRangeNodeNameError" class="errorLabel imgSrcErrorMsg" style="display:block;"></span>
								<span id="transactionSearchCriteria.startDtNodeNameError" class="errorLabel imgSrcErrorMsg" style="display:block;"></span>
								<span id="transactionSearchCriteria.startDateError" class="errorLabel imgSrcErrorMsg" style="display:block;"></span>
								<span id="transactionSearchCriteria.endDtNodeNameError" class="errorLabel imgSrcErrorMsg" style="display:block;"></span> 
							<!-- </div>	 -->

						</td> 
						<td class="marginleft5">
							<s:if test="%{#session.SecureUser.AppType !='Business'}">
							<!-- <div class="acntDashboard_itemHolder marginRight10" style="float:left !important;margin-top:5px;"> -->
								<span class="sectionsubhead dashboardLabelMargin">&nbsp;</span>
								<sj:a id="viewAccountHistoryID1" formIds="accountCriteriaForm" button="true"
									targets="resultmessage" validate="true" validateFunction="customValidation"
									onclick="ns.common.populateAccountDetails(),ns.account.resetOperationFlag();"
									onBeforeTopics="beforeAccountHistorySearch"
									onCompleteTopics="confirmAccountHistorySearch"
									onErrorTopics="errorAccountHistorySearch"
									onSuccessTopics="successAccountHistorySearch">
									<s:text name="jsp.default_6" />
								</sj:a>
				
								<sj:a
									id="exportHistoryButton1" formIds="accountCriteriaForm"
									button="true"
									targets="resultmessage" validate="true" validateFunction="customValidation"
									onclick="ns.common.populateAccountDetails(),ns.account.setOperationFlag('EXPORT');"
									onBeforeTopics="beforeExportHistory"
									onSuccessTopics="successExportHistory"> 
									<s:text name="jsp.default_195"/>
								</sj:a>
							<!-- </div> -->
						</s:if>
						</td>
					</tr>
					<span id="selectedAccountOptionError"></span>
				</table>
			</div> 
		</s:if>
		<%--consumer account field ends --%>
		
		<s:url id="calendarURL" value="/pages/jsp/account/loadCalendar_showAccHistory.action" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="moduleName" value="%{'accHistoryCalendar'}"></s:param>
		</s:url>
	       <sj:a
			id="openCalendar"
			href="%{calendarURL}"
			targets="summary"
			onSuccessTopics="calendarLoadedSuccess"
			cssClass="titleBarBtn hidden"
			>
			<s:text name="jsp.billpay_calender"/><span class="ui-icon ui-icon-calendar floatleft"></span>
		</sj:a>
		
</div>
<script>
	


	$(document).ready(function() {
		var mergedRanges= {};
		var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=FormName&calTarget=accountHistorySearchCriteria.transactionSearchCriteria.startDate"/>';
		mergedRanges = ns.account.overrideDateRanges();                    
        var aConfig = {
            url:tmpUrl,
            startDateBox:$("#startDate"),
            endDateBox:$("#endDate"),
            presetRanges:mergedRanges   
        }
    	$("#accountsDateRangeBox").extdaterangepicker(aConfig);
	})
</script>
