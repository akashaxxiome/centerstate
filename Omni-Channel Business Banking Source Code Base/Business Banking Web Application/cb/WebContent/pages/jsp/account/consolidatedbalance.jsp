<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<% session.setAttribute("AccountDisplayCurrencyCode",request.getParameter("AccountDisplayCurrencyCode")); %>
<% session.setAttribute("ConsolidatedBalanceDataClassification",request.getParameter("DataClassificationVariable")); %>
 
<%-- ADDED BY TCG FOR CHECKIMAGING --%>

<%-- Flag to check whether to retrieve image or not --%>
<ffi:setProperty name="get-image-flag" value="true"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="false"/>
</ffi:cinclude>

<script type="text/javascript"><!--
function CSClickReturn () {
	var bAgent = window.navigator.userAgent;
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
}
CSStopExecution=false;
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
	return CSAction3(fct, array, null);
}
function CSAction3(fct, array, param) {
	var result;
	for (var i=0;i<array.length;i++) {
		if(CSStopExecution) return false;
		var aa = fct[array[i]];
		if (aa == null) return false;
		var ta = new Array;
		for(var j=1;j<aa.length;j++) {
			if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
				if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
				else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
				else ta[j]=aa[j];}
			} else ta[j]=aa[j];
		}
		result=aa[0](ta, param);
	}
	return result;
}
CSAct = new Object;
number = -1;
function CSOpenWindow(action, param) {
	var wf = "";
	wf = wf + "width=" + action[3];
	wf = wf + ",height=" + action[4];
	wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
	wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
	wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
	wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
	wf = wf + ",directories=" + (action[9] ? "yes" : "no");
	wf = wf + ",location=" + (action[10] ? "yes" : "no");
	wf = wf + ",status=" + (action[11] ? "yes" : "no");
	if (param == null) {
		window.open(action[1], action[2], wf);
	} else {
		window.open(action[1] + "?" + param, action[2], wf);
	}
}


<%-- added by TCG for checkimaging --%>
function CSOpenImageWindow(url) {

	window.open(url, "CheckImage", "width=600, height=550, location=no, menubar=no, status=no, toolbar=no, scrollbars=no, resizable=no");
}

function setTranNumber( num )
{
	CSAct[/*CMP*/ 'B9D1DAD41'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}account/transactiondetail.jsp?tranIndex="/>' + num,'',518,455,false,false,false,false,false,false,false);
}


CSAct[/*CMP*/ 'B9D1DAD40'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}account/inquirehistory.jsp"/>','',300,350,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E611E8165'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=back&calForm=FormName&calTarget=GetPagedTransactions.StartDate"/>','',350,300,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E611E8166'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=back&calForm=FormName&calTarget=GetPagedTransactions.EndDate"/>','',350,300,false,false,false,false,false,false,false);

		function popNav()
		{
			var optionValue;
			optionValue = document.navForm.popNav.options[document.navForm.popNav.selectedIndex].value;
			document.location = optionValue;
		}

// --></script>

<script type="text/javascript">

     $(function(){
   	 	$("#AccountDisplayCurrencyCode").selectmenu({width: 200}); 
   		$("#reportOn").selectmenu({width: 140});
     });
    //The js for show account in select form
	function changeAccountDisplayCurrency()
	{   
		var AccountDisplayCurrencyCode = document.ChangAccountDisplayCurrencyForm.AccountDisplayCurrencyCode.options[document.ChangAccountDisplayCurrencyForm.AccountDisplayCurrencyCode.selectedIndex].value;
		var changeAccountDisplayCurrencyString="true";
		var ConsolidatedBalanceDataClassificationString="<ffi:getProperty name="ConsolidatedBalanceDataClassification"/>";
		var urlString = "/cb/pages/jsp/account/ConsilidatedBalanceAction.action"; 
		$.ajax({
			url: urlString,	
			type:"POST",
			data: {changeAccountDisplayCurrency: changeAccountDisplayCurrencyString,DataClassificationVariable: ConsolidatedBalanceDataClassificationString,AccountDisplayCurrencyCode: AccountDisplayCurrencyCode, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
			success: function(data){
				$('#querybord').html(data);
				$('#consolidatedBalanceSummary').html('');
				
				$.publish("common.topics.tabifyDesktop");					
				if(accessibility){
					accessibility.setFocusOnDashboard();
				}
		}
		});
	}
    
	function reloadView(){
			var flag ="<ffi:getProperty name="flag"/>";
			var AccountDisplayCurrencyCode = document.ChangAccountDisplayCurrencyForm.AccountDisplayCurrencyCode.options[document.ChangAccountDisplayCurrencyForm.AccountDisplayCurrencyCode.selectedIndex].value;
			var urlString ="/cb/pages/jsp/account/ConsilidatedBalanceAction.action?AccountDisplayCurrencyCode="+$('#AccountDisplayCurrencyCode').val();
			$.ajax({
				url: urlString,	
				type: "POST",
				data: $("#CriteriaFormID").serialize(),
				success: function(data){
					$('#querybord').html(data);
					$('#consolidatedBalanceSummary').html('');
					$.publish("common.topics.tabifyDesktop");					
					$("#reloadViewButton").focus();
					changeConsolidatedBalancePortletView(ns.account.consolidatedBalanceDisplayType);
				}
			});
	}

</script>


<ffi:setProperty name="subMenuSelected" value="consolidated balance"/>
<%-- include page header --%>
<ffi:setProperty name="SubmitURL" value="/pages/jsp/account/consolidatedbalance.jsp?AccountDisplayCurrencyCode=${AccountDisplayCurrencyCode}" URLEncrypt="false"/>
<ffi:setProperty name="flag" value="consolidate"/>
<ffi:setProperty name="DataClassificationVariable" value="${ConsolidatedBalanceDataClassification}"/>

<ffi:setProperty name="DataType" value="Summary"/>
<ffi:setProperty name="AccountsCollectionName" value="BankingAccounts"/>

<%-- check if there are any accounts to be displayed. if not - don't show anything other than the error message --%>
<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>

<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="notEquals">
	<ffi:setProperty name="EntitledToSummaryAnyDataClassificationForAction" value="false"/>
	<script language="JavaScript" type="text/javascript">
		ns.account.querybordflag=true;
		$(function(){
			 var urlString = "<ffi:urlEncrypt url='/cb/pages/jsp/account/consolidatedbalance_account_summaries_grid.jsp?dataClassfication=${ConsolidatedBalanceDataClassification}&currencyCode=${AccountDisplayCurrencyCode}'/>";
			 ns.account.refreshConsolidatedSummaryForm(urlString);
		});
	</script>   
</ffi:cinclude>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="equals">
	<script language="JavaScript" type="text/javascript">
		ns.account.querybordflag=false;
	</script> 
	<%-- we must determine the default data classification based on whether the user can see intra or previous day for any accounts--%>
	<ffi:cinclude value1="${ConsolidatedBalanceDataClassification}" value2="" operator="equals">
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
			<ffi:setProperty name="ConsolidatedBalanceDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="false" operator="equals">
			<ffi:setProperty name="ConsolidatedBalanceDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${ConsolidatedBalanceDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" operator="equals">
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="notEquals">
			<ffi:setProperty name="ConsolidatedBalanceDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${ConsolidatedBalanceDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" operator="equals">
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="notEquals">
			<ffi:setProperty name="ConsolidatedBalanceDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>


<ffi:setProperty name="section" value="accountmanagement"/>
<ffi:setProperty name="PortalFlag" value="false"/>
   
	<style>
		#querybord { margin-left:10px; margin-bottom:10px; }
	</style>

<ffi:cinclude value1="${AccountDisplayCurrencyCode}" value2="" operator="equals">
<ffi:setProperty name="AccountDisplayCurrencyCode"  value="${SecureUser.BaseCurrency}"/>  
</ffi:cinclude> 


<div class="searchDashboardRenderCls quickSearchAreaCls" style="float:none !important;margin-top:0 !important;">   		
<div id="quicksearchcriteria" >	
	<div id="showAccountIn" class="marginLeft10">
	<ul id="formerrors"></ul>
	<ffi:cinclude value1="true" value2="${showselectcurrency}" operator="equals" >
		<s:form name="ChangAccountDisplayCurrencyForm"  method="post" theme="simple">
				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<div class="acntDashboard_masterItemHolder">
					<!-- Show account in holder -->
					<div class="acntDashboard_itemHolder marginRight5">
						<span id="reportOnSpan" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.account_189"/></span>
						<select id="AccountDisplayCurrencyCode" name="consolidatedBalanceSearcCriteria.currency" class="txtbox">
							<ffi:list collection="FXCurrencies" items="currency">
							    <option <ffi:cinclude value1='${currency.CurrencyCode}' value2='${consolidatedBalanceSearcCriteria.Currency}' operator='equals'> selected </ffi:cinclude>  value='<ffi:getProperty name="currency" property="CurrencyCode"/>' >
							<ffi:getProperty name="currency" property="CurrencyCode"/>-<ffi:getProperty name="currency" property="Description"/>
							</option>
						    </ffi:list>
						</select>
						<br><span id="currencyError"></span>
					</div>
				</div>
		</s:form>
	</ffi:cinclude>	
	</div>
 <!-- INCLUDE SPECIFYCRITERIANODATE.JSP  -->
 <% String DataClassification = "";  String DateString= ""; String AccountDisplayCurrencyCodeString= "";%>

<%
session.setAttribute("DataClassification", session.getAttribute("DataClassificationVariable"));
%>

<ffi:getProperty name="AccountDisplayCurrencyCode" assignTo="AccountDisplayCurrencyCodeString"/>
<%-- If this is the first visit to the page, --%>
<ffi:cinclude value1="${DataClassification}" value2="" operator="equals">
	<%-- we set DataClassification to its default value - Previous Day --%>
	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
	<%-- UNLESS we are doing this for Controlled Disubursement, where we would like default Intra Day --%>
	<ffi:cinclude value1="${DataClassificationVariable}" value2="DisbursementDataClassification" operator="equals">
		<%-- but first we'll check if we are even entitled to Intra day! --%>
		<ffi:setProperty name="intraEntitled" value="false"/>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
			<ffi:setProperty name="intraEntitled" value="true"/>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
			<ffi:setProperty name="intraEntitled" value="true"/>
		</ffi:cinclude>
		<%-- and if we are, we will set the default to Intra Day --%>
		<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
	        	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:setProperty name="${DataClassificationVariable}" value="${DataClassification}"/>
</ffi:cinclude>

<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="notEquals">
	<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
	<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
	<ffi:cinclude value1="" value2="${AccountGroupID}" operator="notEquals">
		<ffi:setProperty name="BankingAccounts" property="Filter" value="ACCOUNTGROUP=${AccountGroupID}" />
	</ffi:cinclude>
	<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="${AccountsCollectionName}"/>
	<ffi:process name="CheckPerAccountReportingEntitlements"/>
</ffi:cinclude>
    <s:form method="post" id="CriteriaFormID" name="CriteriaForm" theme="simple">
    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
    <input type="hidden" name="AccountDisplayCurrencyCode" value="<%= AccountDisplayCurrencyCodeString==null?"":AccountDisplayCurrencyCodeString %>"/>
    <!-- Right side items (Report on combo & View buton) -->
    <div id="reportOnDivID" class="acntDashboard_masterItemHolder" >
    	<!-- Report on label and component -->
    	<div class="acntDashboard_itemHolder" style="margin-right: 0px;">
    		<span id="reportOnSpan" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.default_354"/></span>
			<select class="txtbox" id="reportOn" name="consolidatedBalanceSearcCriteria.dataClassification">
					<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="equals">								
				     	<ffi:removeProperty name="previousEntitled"/>
				     	<ffi:removeProperty name="intraEntitled"/>
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
						<ffi:setProperty name="previousEntitled" value="true"/>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
						<ffi:setProperty name="previousEntitled" value="true"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${previousEntitled}" value2="true" operator="equals">
						<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${consolidatedBalanceSearcCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
					</ffi:cinclude>
	
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
						<ffi:setProperty name="intraEntitled" value="true"/>
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
						<ffi:setProperty name="intraEntitled" value="true"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
						<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${consolidatedBalanceSearcCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
					</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude  value1="${AccountsCollectionName}" value2="" operator="notEquals">
			 		<%-- we must determine which data classifications to place in this drop down, based on the entiltment checks on the accounts collection AND the data type of this information --%>
					<ffi:cinclude value1="${DataType}" value2="Summary" operator="equals">
						<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
							<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${consolidatedBalanceSearcCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
						</ffi:cinclude>
						<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
							<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${consolidatedBalanceSearcCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${DataType}" value2="Detail" operator="equals">
						<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
							<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${consolidatedBalanceSearcCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
						</ffi:cinclude>
						<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
							<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${consolidatedBalanceSearcCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${DataType}" value2="" operator="equals">
						<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryPrev}" value2="true" operator="equals">
							<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${consolidatedBalanceSearcCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
						</ffi:cinclude>
						<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryIntra}" value2="true" operator="equals">
							<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${consolidatedBalanceSearcCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
						</ffi:cinclude>
					</ffi:cinclude>
					</ffi:cinclude>  
			</select>
			<span id="dataClassificationError"></span>
    	</div>
    	
    	<!-- View Button -->
    	<div class="acntDashboard_itemHolder">
    		<span id="reportOnSpan" class="sectionsubhead dashboardLabelMargin" style="display:block">&nbsp;</span>
			<sj:a id="reloadViewButton"								
				button="true"
				onclick="reloadView()"
				cssStyle="margin-left: 3px;">
				<s:text name="jsp.default_459"/>
			</sj:a>
    	</div>
    </div>
    </s:form>
</div>   
</div> 
<ffi:cinclude value1="${ConsolidatedBalanceDataClassification}" value2="" operator="equals">
	<ffi:setProperty name="tempURL" value="/cb/pages/jsp/inc/portal/portal-consolidated-balance.jsp?ConsolidatedBalanceDataClassification=<%=com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>&AccountDisplayCurrencyCode=${AccountDisplayCurrencyCode}" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${ConsolidatedBalanceDataClassification}" value2="" operator="notEquals">
	<ffi:setProperty name="tempURL" value="/cb/pages/jsp/inc/portal/portal-consolidated-balance.jsp?ConsolidatedBalanceDataClassification=${ConsolidatedBalanceDataClassification}&AccountDisplayCurrencyCode=${AccountDisplayCurrencyCode}" URLEncrypt="true"/>
</ffi:cinclude>


<script language="JavaScript" type="text/javascript">
		$(function(){
		    var urlString ="<ffi:getProperty name='tempURL'/>";
			$.ajax({
				url: urlString,	
				success: function(data){
					$('#consolidatedBalanceSummary').html(data);
			}
			});
		   });
</script>
<ffi:removeProperty name="PortalFlag"/>
</ffi:cinclude>

<%-- housekeeping --%>
<ffi:removeProperty name="ConsolidatedBalanceDataClassification"/>
<ffi:removeProperty name="DataClassificationVariable"/>
<ffi:removeProperty name="AccountDisplayCurrencyCode"/>
