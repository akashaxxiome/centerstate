<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>


<div class="acntDashboard_masterItemHolder quickSearchAreaCls" style="margin-left:10px;" >	    
	 <s:if test="%{#session.SecureUser.AppType=='Business'}">
		<div id="corporateOnly" style="<ffi:getProperty name="corporateFieldStyle" />" class="calcHolderDivCls">
			<!-- setting report on properties -->
			<s:set name="appType" value="%{#session.SecureUser.AppType}"/>
			<s:set name="transDataClassiFication" value="transactionHistorySearchCriteria.DataClassification"/>
			<s:set name="AccountGroupID" value="transactionHistorySearchCriteria.Account.AccountGroup" scope="session"/>
			<s:set name="transZBAFlag" value="transactionHistorySearchCriteria.ZbaFlag"/>
			<s:set name="transDateRange" value="transactionHistorySearchCriteria.TransactionSearchCriteria.DateRangeValue"/>
			
			<div class="acntDashboard_itemHolder" style="float:left">
				<table cellpadding="0" cellspacing="5" border="0">
					<tr>
						<%-- <td width="25%" valign="top">
							<div class="templatePane leftPane">
								<div class="header"><s:text name="jsp.account.accountHistory.gridHeader.loadSavedSearches" /></div>
								<div id="loadSavedSearch" class="leftPaneInnerBox">
									<!-- Saved searches element -->
									<div class="acntDashboard_itemHolder" style="float:left">
										<input type="text" name="savedSearches" value="Load saved searches..." class="ui-widget-content ui-corner-all" readonly />
									</div>
								</div>
							</div>
						</td>
						<td>&nbsp;</td> --%>
						<td>										
							<!-- Account -->
							<div class="acntDashboard_itemHolder marginTop10" >
								<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default_20" /></span>
								<div class="selectBoxHolder marginRight5" style="float:none !important;">
									<select class="textbox ui-corner-all" name="accountGroup" id="accountGroupID"
										onChange="ns.account.reloadAccountType();">
										<option value=<s:property value='depositAccounts'/>	<s:if test="%{#AccountGroupID == #depositAccounts}">selected</s:if>><s:text name="jsp.account_76" /></option>
										<option value=<s:property value='assetAccounts'/> <s:if test="%{#AccountGroupID == #assetAccounts}">selected</s:if>><s:text name="jsp.account_30" /></option>
										<!--<option value=<s:property value='creditCards'/>	<s:if test="%{#AccountGroupID == #creditCards}">selected</s:if>><s:text name="jsp.account_58" /></option>-->
										<option value=<s:property value='loanAccounts'/> <s:if test="%{#AccountGroupID == #loanAccounts}">selected</s:if>><s:text name="jsp.account_116" /></option>
										<!--<option value=<s:property value='otherAccounts'/> <s:if test="%{#AccountGroupID == #otherAccounts}">selected</s:if>><s:text name="jsp.account_155" /></option>-->
									</select>
									<br><span id="accountSearchCriteria.account.accountGroupError"></span>
								</div>
							</div>
							<div class="acntDashboard_itemHolder marginTop10" >	
								<!-- Account List Items -->
								<s:if test="%{#session.TransactionSearch=='true'}">		
									<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default_21" /></span>
									<select class="" name="transactionHistorySearchCriteria.selectedAccountOption" id="corporateAccountMultiSelectDropDown"  size="4"
													multiple="multiple" style="width:370px;">
										<ffi:list collection="accountsList" items="acct"
											startIndex="1" endIndex="1">
											<ffi:setProperty name="StringUtil" property="Value1" value="${acct.ID}" />
											<s:set name="acctId1" value="%{#acct.ID}"/>
										</ffi:list>
											
										<ffi:list collection="accountsList" items="acct">
											<s:set name="acctId2" value="acct.RoutingNum"/>
											<ffi:setProperty name="StringUtil" property="Value2" value="${acct.ID}" /> 
											<ffi:setProperty name="selectAccountValue" value="${acct.ID},${acct.BankID},${acct.RoutingNum},${acct.AccountGroup},${TransactionSearch}" />
											<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value="<ffi:getProperty name='selectAccountValue'/>"
											>
											<ffi:getProperty name="acct" property="accountDisplayText"/>
										</ffi:list>
									</select> 
								</s:if>
								<br><span id="selectedAccountOptionError"></span>
							</div>
						
							<!-- Date Range element -->
							<div class="acntDashboard_itemHolder marginTop10">
								<span class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.default.label.date.range" /></span>
								<input type="text" id="transactionDateRangeBox" />
								
								<input value="<ffi:getProperty name="DisplayStartDate" />" id="startDate" name="transactionHistorySearchCriteria.transactionSearchCriteria.startDate"
								type="hidden"/> 
								<input value="<ffi:getProperty name="DisplayEndDate" />" id="endDate" name="transactionHistorySearchCriteria.transactionSearchCriteria.endDate"
								type="hidden"/>
								<span id="transactionSearchCriteria.dateRangeValueError" style="display:block;"></span>
								<%--Adding below spans to display TransactionSearchCriteria's field level @size validations.  --%>
								<span id="transactionSearchCriteria.endDateError" style="display:block;"></span>
								<span id="transactionSearchCriteria.dateRangeNodeNameError" style="display:block;"></span>
								<span id="transactionSearchCriteria.startDtNodeNameError" style="display:block;"></span>
								<span id="transactionSearchCriteria.startDateError" style="display:block;"></span>
								<span id="transactionSearchCriteria.endDtNodeNameError" style="display:block;"></span>				
							</div>		
							
							<!-- Report On Element -->
							<div class="acntDashboard_itemHolder marginTop10">
								<span id="reportOnSpan" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.account_181" /></span>
								<select class="ui-corner-all" name="transactionHistorySearchCriteria.dataClassification" id="reportOn"">
									<option	value=<s:property value="dataIntraDay"/>
										<s:if test="%{#transDataClassiFication == #dataIntraDay}">
											selected
										</s:if>>
										<s:text name="jsp.default_252" />
									</option>
									<option	value=<s:property value="dataPrevDay"/>
										<s:if test="%{#transDataClassiFication == #dataPrevDay}">
											selected
										</s:if>>
										<s:text name="jsp.default_330" />
									</option>
								</select>
								<br><span id="dataClassificationError"></span>
							</div>
							
							
							<!-- ZBA Flag -->
							<s:if test="%{#transZBAFlag != #zbaNoneConst}">
								<div class="acntDashboard_itemHolder marginTop10">
									<span class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.account_229.1"/></span>
									<select class="ui-corner-all" name="transactionHistorySearchCriteria.zbaFlag" id="zbaDisplay"> 
										<option value=<s:property value='zbaNoneConst'/>>
											<s:text name="jsp.default_296" />
										</option>
										<s:if test="%{#transZBAFlag == #zbaBothConst}">
											<option	value=<s:property value="zbaCreditConst"/>
												<s:if test="%{#transZBAFlag == #zbaCreditConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
												<s:text name="jsp.default_471" />
											</option>
											<option value=<s:property value="zbaDebitConst"/>
												<s:if test="%{#transZBAFlag == #zbaDebitConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
												<s:text name="jsp.default_472" />
											</option>
											<option value=<s:property value="zbaBothConst"/>
												<s:if test="%{#transZBAFlag == #zbaBothConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
												<s:text name="jsp.account_42.1" />
											</option>
										</s:if>
										<s:if test="%{#transZBAFlag == #zbaCreditConst}">
											<option value=<s:property value="zbaCreditConst"/>
												<s:if test="%{#transZBAFlag == #zbaCreditConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
												<s:text name="jsp.default_471" />
											</option>
										</s:if>
										<s:if test="%{#transZBAFlag == #zbaDebitConst}">
											<option value=<s:property value="zbaDebitConst"/>
												<s:if test="%{#transZBAFlag == #zbaDebitConst}"><s:text name="jsp.transfers_text_selected"/></s:if>>
												<s:text name="jsp.default_472" />
											</option>
										</s:if>
									</select>
									<br><span id="zbaFlagError"></span>		
								</div>
							</s:if>
							
							<!-- <div id="transactionSearchMoreRow1"> -->
							<!-- Transactions list element -->
								<div class="acntDashboard_itemHolder marginRight15 marginTop10">
									<s:set var="remainingColumnWidth" value="%{#remainingColumnWidth-9}"/>
									<span id="transactionTypeLabelID" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.account_217" /></span>
									<s:set var="remainingColumnWidth" value="%{#remainingColumnWidth-20}"/>
									<s:if test="%{#appType == #businessAppConst}">
										<select class="ui-corner-all" id="transactionTypesID" name="transactionHistorySearchCriteria.transactionType"
											size="3" MULTIPLE style="width:370px;">
												 <option value=<s:property value="defaultTransType"/>
													<s:if test="%{#allTrans == #defaultTransType}">
														selected
													</s:if>>
													<s:text name="jsp.default_42" />
												</option>
												<option value=<s:property value="allCreditTrans"/>
													<s:if test="%{#allCreditTrans == #defaultTransType}">
														selected
													</s:if>>
													<s:text name="jsp.default_40" />
												</option>
												<option value=<s:property value="allDebitsTrans"/>
													<s:if test="%{#allDebitsTrans == #defaultTransType}">
														selected
													</s:if>>
													<s:text name="jsp.default_41" />
												</option>
											<ffi:setProperty name="TransactionTypes" property="SortField"
												value="<%= com.ffusion.tasks.util.GetTransactionTypes.SORTFIELD_DESCRIPTION %>" />
											<ffi:list collection="transactionHistorySearchCriteria.TransactionTypes"
												items="keyValuePair">
												<ffi:cinclude value1="${keyValuePair.Value}" value2="Unknown"
													operator="notEquals">
													<s:set name="key" value="keyValuePair.Key"/>
													<option value="<ffi:getProperty name='keyValuePair' property='Key'/>"
														<s:if test="%{#key == #defaultTransType}">
															selected
														</s:if>>
														<ffi:getProperty name="keyValuePair" property="Value" />
													</option>
												</ffi:cinclude>
											</ffi:list>
							
											<!-- Check the size of the TransactionGroups, if 0, we do not add the dividing line
												We only try to add the items only if TransactionGroups has items in it -->
							
											<% String TransactionGroupsSize = null;%>
											<ffi:getProperty name="transactionHistorySearchCriteria.TransactionGroups" property="Size" assignTo="TransactionGroupsSize" />
											<ffi:cinclude value1="${TransactionGroupsSize}" value2="" operator="notEquals">
												<option	value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DIVIDER%>"><s:text name="jsp.default_9" /></option>
												<ffi:setProperty name="custTransGroupPrefix" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %>" />
												<ffi:list collection="transactionHistorySearchCriteria.TransactionGroups" items="customGroupName">
													<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${custTransGroupPrefix}${customGroupName}" />
													<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %><ffi:getProperty name='customGroupName'/>"
														<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="customGroupName" /></option>
												</ffi:list>
												<ffi:removeProperty name="custTransGroupPrefix" />
											</ffi:cinclude>
											<ffi:removeProperty name="TransactionGroupsSize" />
										</select>
										<br><span id="transactionTypeError"/>
									</s:if>
									<s:if test="%{#appType != #businessAppConst}">
										<select class="ui-widget-content ui-corner-all" name="transactionHistorySearchCriteria.transactionType" id="transactionTypeID" size="3" MULTIPLE style="width:370px;">
											<option value=<s:property value="defaultTransType"/>
												<s:if test="%{#allTrans == #defaultTransType}">
													selected
												</s:if>>
												<s:text name="jsp.default_42" />
											</option>
											
											<ffi:list collection="transactionHistorySearchCriteria.TransactionTypes"
												items="keyValuePair">
													<s:set name="key" value="keyValuePair.Key"/>
													<option value="<ffi:getProperty name='keyValuePair' property='Key'/>"
														<s:if test="%{#key == #defaultTransType}">
															selected
														</s:if>>
														<ffi:getProperty name="keyValuePair" property="Value" />
													</option>
											</ffi:list>
										</select>
										<span id="transactionTypeError"/>
									</s:if>
								</div>
								<!-- Reference -->
								<div class="acntDashboard_itemHolder marginRight15 marginTop10">
									<span id="referenceStartLabelID" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.account_178" /></span>
									
									<s:if test="%{#appType == #businessAppConst}">
										<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_from' />"
										name="transactionHistorySearchCriteria.referenceStart" value="<ffi:getProperty name="DisplayTSReferenceStart" />"
										size="15" maxlength="40" />
									</s:if>
									
									<s:if test="%{#appType != #businessAppConst}">
										<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_from' />"
										name="transactionHistorySearchCriteria.referenceStart" value="<ffi:getProperty name="DisplayTSReferenceStart" />"
										size="15" maxlength="40" />	
									</s:if>
									<s:if test="%{#appType == #businessAppConst}">
										<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_423.1' />"
										name="transactionHistorySearchCriteria.referenceEnd" value="<ffi:getProperty name="DisplayTSReferenceEnd" />"
										size="15" maxlength="40" />
									</s:if>
									<s:if test="%{#appType != #businessAppConst}">	
										<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_423.1' />"
										name="transactionHistorySearchCriteria.referenceEnd" value="<ffi:getProperty name="DisplayTSReferenceEnd" />"
										size="15" maxlength="40" />
									</s:if>
								</div>
								
								<!-- Amount -->
								<div class="acntDashboard_itemHolder marginRight15 marginTop10">
									<span id="amountLabelID" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.default_45" /></span>
									<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_from' />"
									name="transactionHistorySearchCriteria.amountMinimum" value="<ffi:getProperty name="DisplayTSMinimumAmount" />"
									size="15" maxlength="10" />
									<span id="amountMinimumError"></span>
									
									<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_423.1' />"
									name="transactionHistorySearchCriteria.amountMaximum" value="<ffi:getProperty name="DisplayTSMaximumAmount" />"
									size="15" maxlength="10" />
									<span id="amountMaximumError"></span>
									
								</div>
								<!-- Description -->
								<div class="acntDashboard_itemHolder marginRight15 marginTop10">
									<span id="descriptionLabelID" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.account_description" /></span>
									<input class="ui-widget-content ui-corner-all" type="text" name="transactionHistorySearchCriteria.description" 
									value="<ffi:getProperty name="DisplayTSDescription" />"	size="40" maxlength="50" />
								</div>
						<!-- </div>	 -->
					</td>		
					</tr>
					<tr>	
						<!-- <td>&nbsp;</td>
						<td>&nbsp;</td> -->
						<td valign="top">
						 <div class="acntDashboard_itemHolder marginRight15 marginTop10" style="display: block !important;float: left;position: relative;">
							<!-- View button #Condition 1 -->
							<s:if test="%{#appType == #businessAppConst}">
								<div class="acntDashboard_itemHolder">
									<s:set var="remainingColumnWidth" value="%{#remainingColumnWidth-10}"/>
									<sj:a id="viewAccountHistoryID2" formIds="accountCriteriaForm" button="true"
										targets="resultmessage" validate="true" validateFunction="customValidation"
										onclick="ns.account.resetOperationFlag();"
										onBeforeTopics="beforeTransactionSearch"
										onCompleteTopics="confirmTransactionSearch"
										onErrorTopics="errorTransactionSearch"
										onSuccessTopics="successTransactionSearch">
										<s:text name="jsp.default_6" />
									</sj:a>	
								</div>
							</s:if>
						
							<!-- Export history button -->
							<div class="acntDashboard_itemHolder">
								<sj:a id="exportHistoryButton2" formIds="accountCriteriaForm"
									button="true" targets="resultmessage" validate="true" validateFunction="customValidation"
									onclick="ns.account.setOperationFlag('EXPORT');" onBeforeTopics="beforeExportHistory" onSuccessTopics="successExportHistory">
										<s:text name="jsp.default_195"/>
								</sj:a>
							</div>
						</div>
						</td>							
					</tr>
				</table>
			</div>	
		</div>
	 </s:if> 
	<!-- corporate only row ends -->
	
	
	
	<!-- consumer account field starts -->
<s:if test="%{#session.SecureUser.AppType!='Business'}">
	<div id="consumerOnly" style="<ffi:getProperty name="consumerFieldStyle" />" >
	<table cellpadding="0" cellspacing="5" border="0">
		<tr>
		 <td width="25%" valign="top">
			<div class="templateAccPane leftAccountPane leftPaneInnerWrapper">
				<div class="header"><s:text name="jsp.account.accountHistory.gridHeader.loadSavedSearches" /></div>
				<div id="loadSavedSearch" class="leftPaneInnerBox" style="height: 40px; overflow:visible;">
					<!-- Saved searches element -->
					<div class="acntDashboard_itemHolder" style="float:left">
						<%-- <span class="sectionsubhead dashboardLabelMargin" style="display:block">&nbsp;</span> --%>
						<input type="text" name="savedSearches" 
						placeholder="Load saved searches..." 
						class="ui-widget-content ui-corner-all" 
						id="savedSearchGridId" class="txtbox" onclick="loadSavedSearch()" size="40"/>
						<div id="savedSearchGridIdHolderDiv"></div>
					</div>
				</div>
			</div>
		</td>
		<td>&nbsp;</td>
		<td valign="top">
		<div class="formPanel formLeftMargin" >
		<!-- Date Range element -->
			<div class="acntDashboard_itemHolder marginTop10">
				<span class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.default.label.date.range" /></span>
				<input type="text" id="transactionDateRangeBox" />
				
				<input value="<ffi:getProperty name="DisplayStartDate" />" id="startDate" name="transactionHistorySearchCriteria.transactionSearchCriteria.startDate"
				type="hidden"/> 
				<input value="<ffi:getProperty name="DisplayEndDate" />" id="endDate" name="transactionHistorySearchCriteria.transactionSearchCriteria.endDate"
				type="hidden"/>
				<span id="transactionSearchCriteria.dateRangeValueError" style="display:block;"></span>
				<%--Adding below spans to display TransactionSearchCriteria's field level @size validations.  --%>
				<span id="transactionSearchCriteria.endDateError" style="display:block;"></span>
				<span id="transactionSearchCriteria.dateRangeNodeNameError" style="display:block;"></span>
				<span id="transactionSearchCriteria.startDtNodeNameError" style="display:block;"></span>
				<span id="transactionSearchCriteria.startDateError" style="display:block;"></span>
				<span id="transactionSearchCriteria.endDtNodeNameError" style="display:block;"></span>				
			</div>
			
			<!-- Account list element -->
			<s:if test="%{#session.TransactionSearch=='true'}">
				<%
					String strURLConsumerValue = "";
				%>
				<ffi:getProperty name="URL1" assignTo="strURLConsumerValue" />
				<%-- <span class="sectionsubhead"><s:text name="jsp.default_21" /></span> --%>
				<ffi:setProperty name="currSelectedAccounts"
					value="${DisplayConsumerAccountsMultiSelectDropDown}" />
				<!-- set default account selected -->
				<ffi:cinclude value1="" value2="${currSelectedAccounts}"
					operator="equals">
					<ffi:setProperty name="currSelectedAccounts"
						value="${Account.ID}" />
				</ffi:cinclude>
				<div class="acntDashboard_itemHolder marginTop10">
					<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default_21" /></span>
					<select class="ui-corner-all" name="transactionHistorySearchCriteria.selectedAccountOption"
						id="consumerAccountsMultiSelectDropDown" size="4" multiple="multiple" style="width:370px;">
		
						<%-- <ffi:setProperty name="BankingAccounts" property="SortedBy"	value="NICKNAME" />
						<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,AND" /> --%>
						<ffi:setProperty name="StringUtil" property="Value1" value="${Account.ID}" />
						<ffi:list collection="accountsList" items="Account1">
							<ffi:cinclude value1="1" value2="${Account1.CoreAccount}" operator="equals">
								<ffi:setProperty name="StringUtil" property="Value2" value="${Account1.ID}" />
								<ffi:setProperty name="accountID" value="${Account1.ID}" />
								<ffi:setProperty name="selectAccountValue"	value="${Account1.ID},${Account1.BankID},${Account1.RoutingNum},${Account1.AccountGroup},${TransactionSearch}" />
								<option value="<ffi:getProperty name='selectAccountValue'/>"
								<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>" />
								<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>
									<% if(session.getAttribute("SearchAccount_" + session.getAttribute("accountID")) != null) {
										session.removeAttribute("SearchAccount_" + session.getAttribute("accountID"));
									%>
									selected
								<%}%>><ffi:getProperty name="Account1" property="accountDisplayText"/></option>
							<ffi:removeProperty name="accountID" />
							</ffi:cinclude>
						</ffi:list>
						
						<%-- <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,AND" /> --%>
						<ffi:cinclude value1="0" value2="${accountsList.Size}" operator="notEquals">
							<% int count = 0; %>
							<ffi:list collection="accountsList" items="Account1">
								<ffi:cinclude value1="0" value2="${Account1.CoreAccount}" operator="equals">
								<% if (count < 1){ %>
									<option disabled="disabled" value="separator"><ffi:getProperty name="separator" /></option>
								<% }
									count++;
								%>
								<ffi:setProperty name="StringUtil" property="Value2" value="${Account1.ID}" />
								<ffi:setProperty name="selectAccountValue" value="${Account1.ID},${Account1.BankID},${Account1.RoutingNum},${Account1.AccountGroup},${TransactionSearch}" />
								<s:set var="sessionSelectedAccount" value="%{'SearchAccount_'+#attr.Account1.ID}" scope="request"/>
								<option	<s:if test="null!= #session[#request.sessionSelectedAccount]">
								      <%session.removeAttribute(request.getAttribute("sessionSelectedAccount").toString());%>selected </s:if>		
									   value="<ffi:getProperty name='selectAccountValue'/>"><ffi:getProperty name="Account1" property="accountDisplayText" /></option>
								</ffi:cinclude>
							</ffi:list>
							<% count = 0; %>
						</ffi:cinclude>
						<%-- <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,COREACCOUNT=1" /> --%>
					</select>
					<br><span id="selectedAccountOptionError"></span>
				</div>
				
				<s:if test="%{#session.TransactionSearch=='true'}">
					<%-- <s:url id="saved_reports_url" namespace="/pages/jsp/account" value="inc/account_saveSearch.jsp">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
					</s:url>
					<div id="savedReportsDiv" class="savedReportsPane marginBottom10">
						<s:include value="%{saved_reports_url}" />
					</div> --%>
				</s:if>
				
			</s:if>
		
		
		<!-- Transactions list element -->
		<div class="acntDashboard_itemHolder marginRight15 marginTop10">
			<s:set var="remainingColumnWidth" value="%{#remainingColumnWidth-9}"/>
			<span id="transactionTypeLabelID" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.account_217" /></span>
			<s:set var="remainingColumnWidth" value="%{#remainingColumnWidth-20}"/>
			<s:if test="%{#appType == #businessAppConst}">
				<select class="ui-widget-content ui-corner-all" name="transactionHistorySearchCriteria.transactionType"
					size="3" MULTIPLE style="width:370px;">
						 <option value=<s:property value="defaultTransType"/>
							<s:if test="%{#allTrans == #defaultTransType}">
								selected
							</s:if>>
							<s:text name="jsp.default_42" />
						</option>
						<option value=<s:property value="allCreditTrans"/>
							<s:if test="%{#allCreditTrans == #defaultTransType}">
								selected
							</s:if>>
							<s:text name="jsp.default_40" />
						</option>
						<option value=<s:property value="allDebitsTrans"/>
							<s:if test="%{#allDebitsTrans == #defaultTransType}">
								selected
							</s:if>>
							<s:text name="jsp.default_41" />
						</option>
					<ffi:setProperty name="TransactionTypes" property="SortField"
						value="<%= com.ffusion.tasks.util.GetTransactionTypes.SORTFIELD_DESCRIPTION %>" />
					<ffi:list collection="transactionHistorySearchCriteria.TransactionTypes"
						items="keyValuePair">
						<ffi:cinclude value1="${keyValuePair.Value}" value2="Unknown"
							operator="notEquals">
							<s:set name="key" value="keyValuePair.Key"/>
							<option value="<ffi:getProperty name='keyValuePair' property='Key'/>"
								<s:if test="%{#key == #defaultTransType}">
									selected
								</s:if>>
								<ffi:getProperty name="keyValuePair" property="Value" />
							</option>
						</ffi:cinclude>
					</ffi:list>
	
					<!-- Check the size of the TransactionGroups, if 0, we do not add the dividing line
						We only try to add the items only if TransactionGroups has items in it -->
	
					<% String TransactionGroupsSize = null;%>
					<ffi:getProperty name="transactionHistorySearchCriteria.TransactionGroups" property="Size" assignTo="TransactionGroupsSize" />
					<ffi:cinclude value1="${TransactionGroupsSize}" value2="" operator="notEquals">
						<option	value="<%=com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DIVIDER%>"><s:text name="jsp.default_9" /></option>
						<ffi:setProperty name="custTransGroupPrefix" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %>" />
						<ffi:list collection="transactionHistorySearchCriteria.TransactionGroups" items="customGroupName">
							<ffi:setProperty name="CriterionListChecker" property="ValueToCheck" value="${custTransGroupPrefix}${customGroupName}" />
							<option value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUSTOM_TRANS_GROUP_PREFIX %><ffi:getProperty name='customGroupName'/>"
								<ffi:cinclude value1="${CriterionListChecker.FoundInList}" value2="true" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="customGroupName" /></option>
						</ffi:list>
						<ffi:removeProperty name="custTransGroupPrefix" />
					</ffi:cinclude>
					<ffi:removeProperty name="TransactionGroupsSize" />
				</select>
				<br><span id="transactionTypeError"/>
			</s:if>
			<s:if test="%{#appType != #businessAppConst}">
			
			<%
					String selectedTypes= null;
					String selectedTrxn= null;
					String trxnSel = null;
					
			%>
					<ffi:cinclude value1="${loadSavedSearch}" value2="true"	operator="equals">
					<ffi:getProperty name="DisplayTSTransactionType" assignTo="selectedTypes" />
										
			<% 
					if(selectedTypes != null){
					 selectedTypes = selectedTypes.replace("AllTransactionTypes","'AllTransactionTypes'");
					 session.setAttribute("SavedSearchesTrxn", selectedTypes);
					}
			%>
					<ffi:setProperty name="DisplayTSTransactionType" value="${SavedSearchesTrxn}"/>
					</ffi:cinclude>
			
				<select class="ui-widget-content ui-corner-all" name="transactionHistorySearchCriteria.transactionType" id="transactionTypeID" size="3" MULTIPLE style="width:370px;">
					 <option value=<s:property value="defaultTransType"/>
					 <ffi:cinclude value1="${loadSavedSearch}" value2="true"	operator="equals">
					 <ffi:list collection="{'AllTransactionTypes'}"	items="keyValuePair">
							 <ffi:list collection="{${DisplayTSTransactionType}}" items="id">
					<ffi:cinclude value1="${keyValuePair.Value}" value2="${id.Value}" operator="equals"> 
 		 				selected
					</ffi:cinclude>
					</ffi:list>
					</ffi:list>
					</ffi:cinclude>
					<ffi:cinclude value1="${loadSavedSearch}" value2="true"	operator="notEquals">
						<s:if test="%{#allTrans == #defaultTransType}">
							selected
						</s:if>
					</ffi:cinclude>>
						<s:text name="jsp.default_42" />
					</option>
					<ffi:list collection="transactionHistorySearchCriteria.TransactionTypes"
						items="keyValuePair">
							<option value="<ffi:getProperty name='keyValuePair' property='Key'/> "
						<s:set var="key">${keyValuePair.Key}</s:set>
						<ffi:cinclude value1="${loadSavedSearch}" value2="true"	operator="equals">
							 <ffi:list collection="{${DisplayTSTransactionType}}" items="id">
                     	      <ffi:cinclude value1="${keyValuePair.Key}" value2="${id.Value}" operator="equals"> 
									selected
    							</ffi:cinclude>
							</ffi:list>
						</ffi:cinclude>
						<ffi:cinclude value1="${loadSavedSearch}" value2="true"	operator="notEquals">
								<s:if test="%{#key == #defaultTransType}">
									selected
								</s:if>
							</ffi:cinclude>>
								<ffi:getProperty name="keyValuePair" property="Value" />
							</option>
							</ffi:list>
				</select>
				<span id="transactionTypeError"/>
			</s:if>
		</div>
		
		<!-- Reference -->
		<div class="acntDashboard_itemHolder marginRight15 marginTop10">
			<span id="referenceStartLabelID" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.account_178" /></span>
			
			<s:if test="%{#appType == #businessAppConst}">
				<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_from' />"
				name="transactionHistorySearchCriteria.referenceStart" value="<ffi:getProperty name="DisplayTSReferenceStart" />"
				size="15" maxlength="40" />
			</s:if>
			
			<s:if test="%{#appType != #businessAppConst}">
				<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_from' />"
				name="transactionHistorySearchCriteria.referenceStart" value="<ffi:getProperty name="DisplayTSReferenceStart" />"
				size="15" maxlength="40" />	
			</s:if>
			<s:if test="%{#appType == #businessAppConst}">
				<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_423.1' />"
				name="transactionHistorySearchCriteria.referenceEnd" value="<ffi:getProperty name="DisplayTSReferenceEnd" />"
				size="15" maxlength="40" />
			</s:if>
			<s:if test="%{#appType != #businessAppConst}">	
				<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_423.1' />"
				name="transactionHistorySearchCriteria.referenceEnd" value="<ffi:getProperty name="DisplayTSReferenceEnd" />"
				size="15" maxlength="40" />
			</s:if>
		</div>
		
		<!-- Amount -->
		<div class="acntDashboard_itemHolder marginRight15 marginTop10">
			<span id="amountLabelID" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.default_45" /></span>
			<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_from' />"
			name="transactionHistorySearchCriteria.amountMinimum" value="<ffi:getProperty name="DisplayTSMinimumAmount" />"
			size="15" maxlength="10" />
			<span id="amountMinimumError"></span>
			
			<input class="ui-widget-content ui-corner-all" type="text" placeholder="<s:text name='jsp.default_423.1' />"
			name="transactionHistorySearchCriteria.amountMaximum" value="<ffi:getProperty name="DisplayTSMaximumAmount" />"
			size="15" maxlength="10" />
			<span id="amountMaximumError"></span>
			
		</div>
			
		
		
		<!-- Description -->
		<div class="acntDashboard_itemHolder marginRight15 marginTop10">
			<span id="descriptionLabelID" class="sectionsubhead dashboardLabelMargin" style="display:block"><s:text name="jsp.account_description" />:</span>
			<input class="ui-widget-content ui-corner-all" type="text" name="transactionHistorySearchCriteria.description" 
			value="<ffi:getProperty name="DisplayTSDescription" />"	size="40" maxlength="50" />
		</div>
		<br><br>
	
			<div class="acntDashboard_itemHolder ">
		
				<!-- View & other buttons #Condition 2 -->
				<s:if test="%{#appType != #businessAppConst}">
					<div class="acntDashboard_itemHolder">
						<s:set var="remainingColumnWidth" value="%{#remainingColumnWidth-10}"/>
						<sj:a id="viewAccountHistoryID2" formIds="accountCriteriaForm" button="true"
							targets="resultmessage" validate="true" validateFunction="customValidation"
							onclick="ns.account.resetOperationFlag();"
							onBeforeTopics="beforeTransactionSearch"
							onCompleteTopics="confirmTransactionSearch"
							onErrorTopics="errorTransactionSearch"
							onSuccessTopics="successTransactionSearch">
							<s:text name="jsp.default_6" />
						</sj:a>
					</div>
					
					<s:if test="%{#session.TransactionSearch=='true'}">
						<ffi:cinclude value1="${editSavedSearch}" value2="true" operator="notEquals">
							<div class="acntDashboard_itemHolder">
								<s:set var="remainingColumnWidth" value="%{#remainingColumnWidth-15}"/>
								<sj:a id="saveSearchID" formIds="accountCriteriaForm" button="true" 
									targets="resultmessage" validate="true" validateFunction="customValidation"
									onclick="ns.account.setOperationFlag('SAVE_SEARCH');"
									onBeforeTopics="beforeSaveSearch"
									onSuccessTopics="successSaveSearch"
									onCompleteTopics="tabifyDesktop,setFocusOnDashboardTopic">
									<ffi:cinclude value1="${loadSavedSearch}" value2="true" operator="equals">
										Manage Search
									</ffi:cinclude>
									<ffi:cinclude value1="${loadSavedSearch}" value2="true" operator="notEquals">
										<s:text name="jsp.save_button" />
									</ffi:cinclude>
								</sj:a>
							</div>
						</ffi:cinclude>
						
						<!-- modify is kept out irrespective of edit/load but this is hidden-->
						<div class="acntDashboard_itemHolder">
								<s:set var="remainingColumnWidth" value="%{#remainingColumnWidth-15}"/>
								<sj:a id="modifySearchID" formIds="accountCriteriaForm" button="true"
									cssStyle="display:none"
									targets="resultmessage" validate="true" validateFunction="customValidation"
									onclick="ns.account.setOperationFlag('MODIFY_SEARCH');"
									onBeforeTopics="beforeModifySearch"
									onSuccessTopics="successModifySearch">
									<s:text name="jsp.modify_button" />
								</sj:a>
						</div>
							
						<ffi:cinclude value1="${loadSavedSearch}" value2="true" operator="equals">
							<s:url id="cancelModifySearchUrl" value="/pages/jsp/account/accounthistory_dashboard.jsp" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								<s:param name="editSavedSearch" value="false"></s:param>
								<s:param name="loadSavedSearch" value="false"></s:param>
								<s:param name="cancelModifySearch" value="true"></s:param>
								<s:param name="TransactionSearch" value="true"></s:param>
							</s:url>
							<div class="acntDashboard_itemHolder">
								<s:set var="remainingColumnWidth" value="%{#remainingColumnWidth-15}"/>
								<sj:a id="cancelSearchID" targets="appdashboard"
									href="%{cancelModifySearchUrl}" button="true"
									onCompleteTopics="tabifyDesktop,setFocusOnDashboardTopic"
									>
									<s:text name="jsp.default_82" />
								</sj:a>
							</div>
						</ffi:cinclude>
					</s:if>
				</s:if>
				
				<!-- Export history button -->
				<div class="acntDashboard_itemHolder">
					<sj:a id="exportHistoryButton2" formIds="accountCriteriaForm"
						button="true" targets="resultmessage" validate="true" validateFunction="customValidation"
						onclick="ns.account.setOperationFlag('EXPORT');" onBeforeTopics="beforeExportHistory" onSuccessTopics="successExportHistory">
							<s:text name="jsp.default_195"/>
					</sj:a>
				</div>
			</div>
		</div>
		</td>
		</tr>
		<tr>
			 <td width="25%" valign="top"> </td>
			 <td>&nbsp;</td>
			 <td></td>
		</tr>
</table>	
		
		
</div>	
</s:if>
	
	<!-- consumer account field ends -->
						
	<s:url id="calendarURL" value="/pages/jsp/account/loadCalendar_showAccHistory.action" escapeAmp="false">
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	<s:param name="moduleName" value="%{'accHistoryCalendar'}"></s:param>
	</s:url>
       <sj:a
		id="openCalendar"
		href="%{calendarURL}"
		targets="summary"
		onSuccessTopics="calendarLoadedSuccess"
		cssClass="titleBarBtn hidden"
		>
		<s:text name="jsp.billpay_calender"/><span class="ui-icon ui-icon-calendar floatleft"></span>
	</sj:a>
	
</div>	
	
<script>
	$(document).ready(function() {
		var mergedRanges= {};
		var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=FormName&calTarget=transactionHistorySearchCriteria.transactionSearchCriteria.startDate"/>';
		mergedRanges = ns.account.overrideDateRanges();                    
    	var aConfig = {
    		url:tmpUrl,
    		startDateBox:$("#startDate"),
    		endDateBox:$("#endDate"),
    		presetRanges:mergedRanges
    	}
		$("#transactionDateRangeBox").extdaterangepicker(aConfig);
    	
    	$("#corporateAccountMultiSelectDropDown").extmultiselect().multiselectfilter();
    	$("#transactionTypesID").extmultiselect().multiselectfilter();
    	if($("#consumerAccountsMultiSelectDropDown")) {
    		if (!$("#consumerAccountsMultiSelectDropDown option:selected").length) {
    			$($('#consumerAccountsMultiSelectDropDown  option').get(0))
    					.attr('selected', 'selected');
    		}
    		$("#consumerAccountsMultiSelectDropDown").extmultiselect().multiselectfilter();
    	}
    	
    	$("#transactionTypeID").extmultiselect().multiselectfilter();
    	
    	loadSavedSearch = function(){
    		$("#savedSearchGridId").autocomplete("search", "");
    	};
    	
    	shortcutSearchFunction = function(request, response){
			$.ajax({
				url: '/cb/pages/jsp/account/GetSavedSearchAction.action',
				data: {name: request.term,_search:true},
				global:false,
				success: function(data){
					 response($.map( data.gridModel, function(item){
							var text = $.trim(item.reportName);	
						 	var textToReplace = text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "i"), "<strong>$1</strong>");
						 	return {
						 			label: textToReplace,
							 		reportID:item.reportID,
							 		reportName:item.reportName,
							 		reportDesc:item.description
							};
			 	        }));
				}
			});
		};
		
    	$("#savedSearchGridIdHolderDiv").on("click", "li", function(event) {
    		var reportID = $(this).data("uiAutocompleteItem").reportID;
    		var reportName = $(this).data("uiAutocompleteItem").reportName;
    		var description = $(this).data("uiAutocompleteItem").reportDesc;
       		$("#savedSearchGridId").val("");
       		var urlString  ="/cb/pages/jsp/account/accounthistory_dashboard.jsp?loadSavedSearch=true&TransactionSearch=true&reportID="+reportID+"&reportName="+reportName+"&reportDesc="+description;
            ns.account.loadSearch(urlString);
            
           /*  var urlString  ="/cb/pages/jsp/account/savedSearchConfirmDelete.jsp?reportID="+reportID+"&reportName="+reportName+"&reportDesc="+description;
     		ns.account.deleteSearch(urlString); */
    	});
    	
    	$("#savedSearchGridId").autocomplete({
    	    source: shortcutSearchFunction,
    	    appendTo:"#savedSearchGridIdHolderDiv",
    	    minLength : "0",
    	    create: function () {
    	        $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
    	        	var li = '<li class="bookmarkItemListCls">';
    	        	return $(li).append('<a class="shortcutHolderAnchor">' + '<span style="float:left"></span>'+ item.label +' </a>')
    	            .appendTo(ul);
    	        };
    	    },
    	    select: function(event, ui){
    	    	$("#savedSearchGridId").val("");
    	     	return false;
    	    },
    	    focus: function( event, ui ) {
    	    	return false;
    	    },
    	    open: function() { $('#savedSearchGridIdHolderDiv .ui-menu').width(284); 
    	    	$('#savedSearchGridIdHolderDiv .ui-menu').height(100);
    	    	$('#savedSearchGridIdHolderDiv .ui-menu .ui-menu-item').css("overflow-x", "hidden");
    	    } 
    	    
    	});
    	
	});
	
	/* function showMoreOptionsOnclick()
	{
		$('#transactionSearchMoreRow1').slideToggle("fast");
		$('.dashboardSubmenuLbl').toggle();
		$('#showMoreDiv').toggleClass('collapseArrow');

		$('#icon-navigation-arrow').toggleClass('icon-navigation-down-arrow icon-navigation-up-arrow');
	} */
	
</script>	
