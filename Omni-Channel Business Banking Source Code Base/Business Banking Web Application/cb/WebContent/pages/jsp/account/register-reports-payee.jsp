<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<DIV align="center">

<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
  <TBODY>
  
  <TR>
    <TD class="ui-widget ui-widget-content ui-corner-all">
      <TABLE class="ui-widget ui-widget-content ui-corner-all" cellSpacing="0" cellPadding="5" width="100%" border="0">
        <TBODY>
        <TR>
          <TD class="sectionsubhead" colSpan="2" align="left"><s:text name="jsp.default_315"/></TD>
          <TD class="sectionsubhead" align="right"><ffi:getProperty name="Business" property="BusinessName"/></TD>
        </TR>
        <TR>
          <TD class="sectionsubhead" colSpan=3 height=15></TD>
        </TR>
        <TR>
		  <ffi:cinclude value1="${GetRegisterTransactions.FromDate}" value2="" operator="equals">
		  	<s:set var="tmpI18nStr" value="%{getText('jsp.account_149')}" scope="request" /><ffi:setProperty name="FromDate" value="${tmpI18nStr}"/>
		  </ffi:cinclude>
		  <ffi:cinclude value1="${GetRegisterTransactions.FromDate}" value2="" operator="notEquals">
		  	<ffi:setProperty name="FromDate" value="${GetRegisterTransactions.FromDate}"/>
		  </ffi:cinclude>
		  <ffi:cinclude value1="${GetRegisterTransactions.ToDate}" value2="" operator="equals">
		  	<s:set var="tmpI18nStr" value="%{getText('jsp.account_147')}" scope="request" /><ffi:setProperty name="ToDate" value="${tmpI18nStr}"/>
		  </ffi:cinclude>
		  <ffi:cinclude value1="${GetRegisterTransactions.ToDate}" value2="" operator="notEquals">
		  	<ffi:setProperty name="ToDate" value="${GetRegisterTransactions.ToDate}"/>
		  </ffi:cinclude>
		  <TD class="sectionsubhead" colSpan="3" align="left"><s:text name="jsp.account_180"/>&nbsp;&nbsp;
		    <ffi:getProperty name="Account" property="DisplayText"/>
		    <ffi:cinclude value1="${Account.NickName}" value2="" operator="notEquals" >
		         - <ffi:getProperty name="Account" property="NickName"/>
		    </ffi:cinclude>
		     - <ffi:getProperty name="Account" property="CurrencyCode"/>&nbsp;&nbsp;(<ffi:getProperty name="FromDate"/> - <ffi:getProperty name="ToDate"/>)
		  </TD>
		  <ffi:removeProperty name="FromDate"/>
		  <ffi:removeProperty name="ToDate"/>
        </TR>
        <TR>
          <TD class="tbrd_b" colSpan="3" align="left"><P class="sectionsubhead"><s:text name="jsp.account_182"/> &nbsp;</P></TD>
        </TR>
        <TR>
          <TD class="tbrd_b" colSpan="3">
            <TABLE cellSpacing="0" cellPadding="3" border="0" width="100%">
             <TBODY>
              <TR vAlign="center" align="left">
                <TD class="sectionsubhead" vAlign="top"  width="200" nowrap><s:text name="jsp.account_161"/><br><img src="/cb/web/multilang/grafx/spacer.gif" height="1" width="20"><s:text name="jsp.account_52"/></TD>
                <TD class="sectionsubhead" vAlign="top"  width="100" nowrap><s:text name="jsp.account_67"/></TD>
                <TD class="sectionsubhead" vAlign="top"  width="100" nowrap><s:text name="jsp.account_65"/></TD>
								<TD class="sectionsubhead" vAlign="top"  width="80" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.default_444"/></TD>
								<TD class="sectionsubhead" vAlign="top"  width="80" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.account_177"/></TD>
                <TD class="sectionsubhead" vAlign="top"  width="80" align=center nowrap><s:text name="jsp.account_197"/></TD>
                <TD class="sectionsubhead" vAlign="top"  width="85" align="right" nowrap><s:text name="jsp.default_43"/></TD>
			  			</TR>

<%-- These get set up to display Yes or No for the tax-related property --%>
<ffi:setProperty name="trueText" value="Yes"/>
<ffi:setProperty name="falseText" value="No"/>

<%-- These get set up to include ':' if parent category exists --%>
<ffi:setProperty name="Parentfalse" value=""/>
<ffi:setProperty name="Parenttrue" value=":"/>

<ffi:object name="com.ffusion.beans.util.FloatMath" id="adder1"/>
<ffi:setProperty name="adder1" property="Value1" value="0.00"/>
<ffi:setProperty name="adder1" property="Value2" value="0.00"/>

<ffi:object name="com.ffusion.beans.common.Currency" id="temp"/>

<ffi:setProperty name="RegisterReport" property="SortedBy" value="PAYEE_NAME"/>
<ffi:list collection="RegisterReport" items="Cat">
							<TR vAlign="center" align="left">
                <TD class="plainsubhead" vAlign="top" bgColor="#ffffff" colspan="6"><ffi:getProperty name="Cat" property="FullName"/></TD>
              </TR>
<ffi:list collection="Cat" items="Item">
<ffi:setProperty name="Item" property="DateFormat" value="${UserLocale.DateFormat}"/>
<ffi:setProperty name="RegisterCategories" property="Current" value="${Item.RegisterCategoryId}"/>
              <TR vAlign="center" align="left">
                <TD class="columndata"><img src="/cb/web/multilang/grafx/spacer.gif" height="1" width="20"><ffi:getProperty name="RegisterCategories" property="ParentName"/><ffi:getProperty name="Parent${RegisterCategories.HasParent}"/><ffi:getProperty name="RegisterCategories" property="Name"/></TD>
								<TD class="columndata" align="left"><ffi:getProperty name="Item" property="DateIssued"/></TD>
								<TD class="columndata" align="left"><ffi:getProperty name="Item" property="Date"/></TD>
								<TD class="columndata" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="Item" property="RegisterTypeName"/></TD>
								<TD class="columndata" align="left">&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="Item" property="ReferenceNumber"/></TD>
								<TD class="columndata" align="center"><ffi:getProperty name="${RegisterCategories.TaxRelated}Text"/></TD>
								<TD class="columndata" align="right"><ffi:getProperty name="Item" property="AmountValue.CurrencyStringNoSymbol"/></TD>
              </TR>
</ffi:list>
							 <tr>
								<td class="columndata" colspan="6" align="right"><ffi:getProperty name="Cat" property="FullName"/> <s:text name="jsp.default_431"/></td>
					 	    <td class="columndata" align="right"><ffi:getProperty name="Cat" property="AmountValue"/></td>
							</tr>

<%-- <!--We set the item format here, we are dropping the dollar sign and commas, so we can do math on the balances	-->--%>
<ffi:setProperty name="temp" property="Format" value="CURRENCY"/>
<ffi:setProperty name="temp" property="Amount" value="${Cat.AmountValue.AmountValue}"/>
<ffi:setProperty name="temp" property="CurrencyCode" value="${Account.CurrencyCode}"/>
<ffi:setProperty name="temp" property="Format" value="#.00"/>

<%-- <!--Here we are getting the Account balances and adding them to each other to get the adder1 total-->--%>
<ffi:setProperty name="adder1" property="Value1" value="${temp.AmountValue.AmountValue}"/>
<ffi:setProperty name="adder1" property="Value2" value="${adder1.Add}"/>

</ffi:list>
			<tr>
				<td class="sectionsubhead" colspan="6" align="right"><u><s:text name="jsp.default_431"/> (<ffi:getProperty name="Account" property="CurrencyCode"/>)</u></td>
				<ffi:object name="com.ffusion.beans.common.Currency" id="result"/>
				<ffi:setProperty name="result" property="Amount" value="${adder1.Value2}"/>
				<ffi:setProperty name="result" property="CurrencyCode" value="${Account.CurrencyCode}"/>
				<td class="sectionsubhead" align="right"><ffi:getProperty name="result" property="String"/></td>
			</tr>
			<TR vAlign="center" align="left">
                <TD vAlign="top" align="right" colSpan="6" height="20"></TD>
			</TR>
          </TBODY>
        </TABLE>
      </TD></TR>
	  <tr>
		<td colspan="3" align="center">
			<script>
				ns.account.backReportButtonURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/register-reports.jsp'/>";
			</script>
		    <sj:a 
			    id="cancelpayeeReportdown"
				button="true" 
				onclick="ns.account.backReportButton(ns.account.backReportButtonURL)"
				><s:text name="jsp.default_57"/></sj:a>
		&nbsp;&nbsp;
		</td>
	  </tr>
	  </TBODY></TABLE>
     </TD>
  </TR>
  <TR>
   <TD align="left" class="ui-widget-header ui-corner-all" width="100%" height="10"></TD>
  </TR>
  </TBODY>
</TABLE>
</DIV>
