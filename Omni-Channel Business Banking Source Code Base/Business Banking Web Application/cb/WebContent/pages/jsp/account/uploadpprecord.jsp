<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%
     String fileType = request.getParameter("fileType");
%>
<ffi:setProperty name="fileType" value="<%=fileType %>"/>

<ffi:cinclude value1="${fileType}" value2="Positive Pay Check Record">
	<ffi:object id="CheckPPayImport" name="com.ffusion.tasks.fileImport.CheckPPayImportTask" scope="session"/>
	<%
		session.setAttribute("FFICheckPPayImport", session.getAttribute("CheckPPayImport"));
	%>
	
	<%-- 
	<ffi:setProperty name="FileUpload" property="SuccessURL" value="${SecureServletPath}CheckPPayImport"/>
	<ffi:setProperty name="FileUpload" property="AuthenticateURL" value="${SecurePath}account/uploadpprecord.jsp?Authenticate=true" URLEncrypt="false"/>
	<ffi:setProperty name="CheckPPayImport" property="SuccessURL" value="${SecurePath}account/fileuploadsuccess.jsp"/>
	--%>
	<ffi:setProperty name="ProcessImportTask" value="CheckPPayImport"/>
	<ffi:setProperty name="CheckPPayImport" property="ImportErrorsURL" value="${SecurePath}account/uploadfailure.jsp"/>

</ffi:cinclude>


<ffi:cinclude value1="${fileType}" value2="ACH">
	<ffi:object id="ProcessACHFileUpload" name="com.ffusion.tasks.fileImport.ProcessACHFileUploadTask" scope="session"/>
   	<ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>
   	<%
		session.setAttribute("FFINewReportBase", session.getAttribute("NewReportBase"));
	%>
   	<ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
    <%
		session.setAttribute("FFIGenerateReportBase", session.getAttribute("GenerateReportBase"));
	%>
   	<ffi:object id="GetReportsByCategoryACH" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
   	<ffi:setProperty name="GetReportsByCategoryACH" property="ReportCategory" value="<%= com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_FILE_UPLOAD %>" />
   	<ffi:setProperty name="GetReportsByCategoryACH" property="ReportsName" value="ReportsACH" />
   	<ffi:process name="GetReportsByCategoryACH" />

	<ffi:setProperty name="ReportName" value="${GetReportsByCategoryACH.ReportCategory}" />
   	<ffi:setProperty name="ProcessImportTask" value="ProcessACHFileUpload"/>
</ffi:cinclude>

<%--this is used for public --%>
<% request.setAttribute("actionName","/pages/fileupload/AccountFileUploadAction.action"); %>
<s:include value="/pages/jsp/common/publicuploader.jsp"/>
    
<script>
	ns.common.popupAfterUpload = true;
</script>