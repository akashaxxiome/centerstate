<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%
   String id = request.getParameter("SetAccount.ID");
   session.setAttribute("id",id);
   String report = request.getParameter("report");
   session.setAttribute("report",report);
   String fromDate = request.getParameter("GetRegisterTransactions.FromDate1");
   request.setAttribute("fromDate",fromDate);
   String endDate = request.getParameter("GetRegisterTransactions.ToDate1");
%>

<ffi:setProperty name="GetRegisterTransactions" property="FromDate" value="<%=fromDate %>"/>
<ffi:setProperty name="GetRegisterTransactions" property="ToDate" value="<%=endDate %>"/>
<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
<ffi:setProperty name="SetAccount" property="ID" value="<%=id %>"/>
<ffi:process name="SetAccount"/>

<ffi:process name="GetRegisterTransactions"/>

<ffi:cinclude value1="${report}" value2="cash" operator="equals">
	<ffi:object id="SetReportCategory" name="com.ffusion.tasks.register.SetReportCategory" scope="session"/>
	<ffi:process name="SetReportCategory"/>
</ffi:cinclude>
<ffi:cinclude value1="${report}" value2="category" operator="equals">
	<ffi:object id="SetReportCategory" name="com.ffusion.tasks.register.SetReportCategory" scope="session"/>
	<ffi:process name="SetReportCategory"/>
</ffi:cinclude>
<ffi:cinclude value1="${report}" value2="payee" operator="equals">
	<ffi:object id="SetReportPayee" name="com.ffusion.tasks.register.SetReportPayee" scope="session"/>
	<ffi:process name="SetReportPayee"/>
</ffi:cinclude>
<ffi:cinclude value1="${report}" value2="reconcile" operator="equals">
	<ffi:object id="SetReportReconciliation" name="com.ffusion.tasks.register.SetReportReconciliation" scope="session"/>
	<ffi:process name="SetReportReconciliation"/>
</ffi:cinclude>
<ffi:cinclude value1="${report}" value2="tax" operator="equals">
	<ffi:object id="SetReportCategory" name="com.ffusion.tasks.register.SetReportCategory" scope="session"/>
	<ffi:process name="SetReportCategory"/>
</ffi:cinclude>
<ffi:cinclude value1="${report}" value2="type" operator="equals">
	<ffi:object id="SetReportType" name="com.ffusion.tasks.register.SetReportType" scope="session"/>
		<ffi:setProperty name="SetReportType" property="UseTypeName" value="true"/>
	<ffi:process name="SetReportType"/>
</ffi:cinclude>

<div id="register_reports_reportID">
<ffi:help id="account_register-reports-report" />
<s:include value="/pages/jsp/account/register-reports-%{#session.report}.jsp"/>
</div>

<script>	
					
		$('#register_reports_reportID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#register_reports_reportID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});
			$('#register_reports_reportID').portlet('title', js_register_report_portlet_title);
</script>