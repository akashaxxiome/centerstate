<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<script type="text/javascript" src="<s:url value='/web/js/account/account%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/account/account_grid%{#session.minVersion}.js'/>"></script>

<div id="desktop" align="center">
	<ffi:help id="accountsbygroup" />
   <div id="appdashboard">
		<s:include value="/pages/jsp/account/accountbalance_dashboard.jsp"/>
	</div>
   <div id="accountBalanceDashbord"><%-- search criteria --%>
      <s:action namespace="/pages/jsp/account" name="InitAccountBalanceAction_init"/>      
   </div>
  
   <div id="accountBalanceSummary">
     <s:include value="/pages/jsp/account/accountbalance.jsp"/>
   </div>
   <script language="JavaScript" type="text/javascript">
   	$(document).ready(function(){
   		if(ns.account.accountBalanceDashbordflag){
			$('#accountBalanceSummary').html('<span class="acct_mgmt_font">'+js_no_account_summary_display_message+'</span>');
			$('#accountBalanceSummary').portlet({
				generateDOM: true,
				title: js_no_account_summary_display_message,
				helpCallback: function(){
					
					var helpFile = $('#desktop').find('.moduleHelpClass').html();
					callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
				}
		
			});
			$('#accountBalanceDashbord').html('');
		}   		
   	});	
		
	</script> 
</div>
