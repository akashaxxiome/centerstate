<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@page import="com.ffusion.util.FilteredList"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.ffusion.beans.register.RegisterCategory"%>

<%
    session.setAttribute("SetEditRegisterCategory",request.getParameter("SetEditRegisterCategory"));
%>
<ffi:object id="SetRegisterCategory" name="com.ffusion.tasks.register.SetRegisterCategory" scope="session"/>
    <ffi:cinclude value1="${SetEditRegisterCategory}" value2="">
            <ffi:setProperty name="SetRegisterCategory" property="Id" value="" />
    </ffi:cinclude>
    <ffi:cinclude value1="${SetEditRegisterCategory}" value2="" operator="notEquals">
            <ffi:setProperty name="SetRegisterCategory" property="Id" value="${SetEditRegisterCategory}" />
    </ffi:cinclude>
        <ffi:cinclude value1="${SetEditRegisterSubCategory}" value2="" operator="notEquals">
            <ffi:setProperty name="SetRegisterCategory" property="Id" value="${SetEditRegisterSubCategory}" />
    </ffi:cinclude>

<ffi:process name="SetRegisterCategory"/>

<ffi:setProperty name="ListFilter" value="${RegisterCategories.Filter}"/>
<ffi:setProperty name="ListSort" value="${RegisterCategories.SortedBy}"/>

<ffi:setProperty name="RegisterCategories" property="Current" value="${RegisterCategory.Id}"/>
<ffi:setProperty name="ParentControlStatus" value=""/>
<ffi:cinclude value1="${RegisterCategories.HasSubcategories}" value2="true">
    <ffi:setProperty name="ParentControlStatus" value="disabled"/>
    <ffi:setProperty name="RegisterParentCategory" value="-1"/>
</ffi:cinclude>

<script language="JavaScript">
    $(function(){
    	 $("#categorytype").selectmenu({width: 120});
    	 $("#RegisterCategoryTaxRelated").selectmenu({width: 60});
    	 $("#RegisterParentCategory").selectmenu({width: 250});
        });
  
<!--
function checkCategoryType(categoryID)
{   
    var catForm = document.forms[0];
    if (categoryID == "-1")
    {
        catForm.categorytype.disabled = false;
    }
    else
    {
        var catStartIndex = catForm.categorytypes.value.indexOf("cat" + categoryID);
        var catEndIndex = catForm.categorytypes.value.indexOf("-", catStartIndex);
        var catType = catForm.categorytypes.value.substring(catEndIndex+1, catEndIndex+2);

        if (catType == "3")
            catForm.categorytype.options[0].selected = true;
        else
            catForm.categorytype.options[1].selected = true;

        catForm.categorytype.disabled = true;
    }
}
   function setCategoryType(){   
	    var catForm = document.forms[0];
	    catForm.RegisterCategoryType.value = catForm.categorytype.value;
   }

-->
</script>

<div id="register_cateID" align=center>
<table cellSpacing=0 cellPadding=0 width="100%" border="0">
  <TR>
    <TD align=center>
<ffi:cinclude value1="${SetEditRegisterCategory}${SetEditRegisterSubCategory}" value2="">
	<ffi:help id="account_register-category-add-save-wait" />
            <s:form id="addcategory" action="AddRegisterCategoryAction" name="addcategory" method="post" theme="simple" namespace="/pages/jsp/account">
            <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
            <table cellSpacing=3 cellPadding=0 width="80%" border="0">

        <tr><td height="14"></td></tr>

        <TR>
          <TD class=sectionsubhead colSpan=2 align="left"><s:text name="jsp.account_47"/><span class="required">*</span></TD>
          <TD class=sectionsubhead><s:text name="jsp.account_48"/></TD>
          <TD class=sectionsubhead colSpan=2><s:text name="jsp.default_170"/></TD>
          <TD class=sectionsubhead><s:text name="jsp.account_196"/></TD>
         </TR>
        <TR>
          <TD class=columndata colSpan=2 align="left"><INPUT class="ui-widget-content ui-corner-all" size=27 maxlength="40" border=0 name="RegisterCategoryName" value="<ffi:getProperty name='RegisterCategory' property='Name'/>"><span id="RegisterCategoryNameError"></span></TD>
          <TD class=columndata>
                <SELECT id="categorytype" class=txtbox name="categorytype" <ffi:cinclude value1="${RegisterCategory.ParentCategory}" value2="-1" operator="notEquals">disabled</ffi:cinclude>>
                    <OPTION value="3" <ffi:cinclude value1="${RegisterCategory.Type}" value2="3">selected</ffi:cinclude> ><s:text name="jsp.account_84"/></OPTION>
                    <OPTION value="2" <ffi:cinclude value1="${RegisterCategory.Type}" value2="2">selected</ffi:cinclude> ><s:text name="jsp.account_106"/></OPTION>
                </SELECT>
                <input type="hidden" name="RegisterCategoryType" value="<ffi:getProperty name='RegisterCategory' property='Type'/>">
          </TD>
          <TD class=columndata colSpan=2><INPUT class="ui-widget-content ui-corner-all" size=29 maxlength="50" border=0 name="RegisterCategoryDescription" value="<ffi:getProperty name='RegisterCategory' property='Description'/>"></TD>
          <TD class=columndata>
                <SELECT id="RegisterCategoryTaxRelated" class=txtbox name="RegisterCategoryTaxRelated">
                    <OPTION value="true" <ffi:cinclude value1="${RegisterCategory.TaxRelated}" value2="true">selected</ffi:cinclude> ><s:text name="jsp.default_467"/></OPTION>
                    <OPTION value="false"<ffi:cinclude value1="${RegisterCategory.TaxRelated}" value2="false">selected</ffi:cinclude> ><s:text name="jsp.default_295"/></OPTION>
                </SELECT>
          </TD>
        </TR>
        <TR>
          <TD class="sectionsubhead" colSpan=6 align="left"><s:text name="jsp.account_193"/></TD>
    </TR>
    <TR>
        <TD class="columndata" colSpan=6 align="left">
            <SELECT id="RegisterParentCategory" class="txtbox" name="RegisterParentCategory" onchange="checkCategoryType(this.options[this.selectedIndex].value);" <ffi:getProperty name="ParentControlStatus"/>>
                <OPTION value="-1" <ffi:cinclude value1="${RegisterCategory.ParentCategory}" value2="-1">selected</ffi:cinclude> ><s:text name="jsp.default_296"/></OPTION>
                <ffi:setProperty name="RegisterCategories" property="Filter" value="ID!0,ID!${RegisterCategory.Id},and"/>
                <ffi:list collection="RegisterCategories" items="Category">
                    <ffi:cinclude value1="${Category.ParentCategory}" value2="-1">
                        <option value="<ffi:getProperty name='Category' property='Id'/>" <ffi:cinclude value1="${RegisterCategory.ParentCategory}" value2="${Category.Id}">selected</ffi:cinclude> ><ffi:getProperty name="Category" property="Name"/></option>
                    </ffi:cinclude>
                </ffi:list>
            </SELECT>
            <input type="hidden" name="categorytypes" value="<ffi:list collection='RegisterCategories' items='Category'><ffi:cinclude value1='${Category.ParentCategory}' value2='-1'>cat<ffi:getProperty name='Category' property='Id'/>-<ffi:getProperty name='Category' property='Type'/></ffi:cinclude></ffi:list>"/>
        </TD>
    </TR>
    <tr>
	<td colspan="6"><br></td>
    </tr>
    <tr>
	<td align="center" colspan="6"><span class="required">* <s:text name="jsp.default_240"/></span></td>
    </tr>
    <tr>
	<td colspan="6"><br></td>
    </tr>
        <TR>
          <TD class=columndata colSpan=6 align=center>
          
			<script>
				ns.account.cancleButtonRedirectRegisteURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/register-wait.jsp'/>";
			</script>
                   <sj:a
                       button="true"
                       onclick="ns.account.cancleButtonRedirectRegiste(ns.account.cancleButtonRedirectRegisteURL)"><s:text name="jsp.default_82"/></sj:a>&nbsp;&nbsp;
                        <sj:a 
                       formIds="addcategory"
                       button="true"
                       buttonIcon="ui-icon-check"
                       targets="accountRegisterDashbord"
                       validateFunction="customValidation"
                       validate="true"
                       onclick="setCategoryType()"
                       ><s:text name="jsp.default_366"/></sj:a>
         </TR>
        </TABLE>
        </s:form>

</ffi:cinclude>
<ffi:cinclude value1="${SetEditRegisterCategory}${SetEditRegisterSubCategory}" value2="" operator="notEquals">
	<ffi:help id="account_register-category-edit-wait" />
            <ffi:setProperty name="SetEditRegisterPayeeId" value="" />
            <s:form id="editcategory" action="EditeRegisterCategoryAction" name="editcategory" method="post" theme="simple" namespace="/pages/jsp/account">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<table cellSpacing=3 cellPadding=0 width="96%" border=0>
                
        <tr><td height="14"></td></tr>

        <TR>
          <TD class=sectionsubhead colSpan=2><s:text name="jsp.account_47"/><span class="required">*</span></TD>
          <TD class=sectionsubhead><s:text name="jsp.account_48"/></TD>
          <TD class=sectionsubhead colSpan=2><s:text name="jsp.default_170"/></TD>
          <TD class=sectionsubhead><s:text name="jsp.account_196"/></TD>
         </TR>
        <TR>
          <TD class=columndata colSpan=2><INPUT class="ui-widget-content ui-corner-all" size=27 maxlength="40" border=0 name="RegisterCategoryName" value="<ffi:getProperty name='RegisterCategory' property='Name'/>"><span id="RegisterCategoryNameError"></span></TD>
          <TD class=columndata>
                <SELECT id="categorytype" class=txtbox name="categorytype" <ffi:cinclude value1="${RegisterCategory.ParentCategory}" value2="-1" operator="notEquals">disabled</ffi:cinclude>>
                    <OPTION value="3" <ffi:cinclude value1="${RegisterCategory.Type}" value2="3">selected</ffi:cinclude> ><s:text name="jsp.account_84"/></OPTION>
                    <OPTION value="2" <ffi:cinclude value1="${RegisterCategory.Type}" value2="2">selected</ffi:cinclude> ><s:text name="jsp.account_106"/></OPTION>
                </SELECT>
                <input type="hidden" name="RegisterCategoryType" value="<ffi:getProperty name='RegisterCategory' property='Type'/>">
          </TD>
          <TD class=columndata colSpan=2><INPUT class="ui-widget-content ui-corner-all" size=29 maxlength="50" border=0 name="RegisterCategoryDescription" value="<ffi:getProperty name='RegisterCategory' property='Description'/>"><span id="RegisterCategoryDescriptionError"></span></TD>
          <TD class=columndata>
                <SELECT id="RegisterCategoryTaxRelated" class=txtbox name="RegisterCategoryTaxRelated">
                    <OPTION value="true" <ffi:cinclude value1="${RegisterCategory.TaxRelated}" value2="true">selected</ffi:cinclude> ><s:text name="jsp.default_467"/></OPTION>
                    <OPTION value="false"<ffi:cinclude value1="${RegisterCategory.TaxRelated}" value2="false">selected</ffi:cinclude> ><s:text name="jsp.default_295"/></OPTION>
                </SELECT>
          </TD>
        </TR>
        <TR>
          <TD class="sectionsubhead" colSpan=6><s:text name="jsp.account_193"/></TD>
    </TR>
    <TR>
        <TD class="columndata" colSpan=6>
            <SELECT id="RegisterParentCategory" class="txtbox" name="RegisterParentCategory" onchange="checkCategoryType(this.options[this.selectedIndex].value);" <ffi:getProperty name="ParentControlStatus"/>>
                <OPTION value="-1" <ffi:cinclude value1="${RegisterCategory.ParentCategory}" value2="-1">selected</ffi:cinclude> ><s:text name="jsp.default_296"/></OPTION>
                <ffi:setProperty name="RegisterCategories" property="Filter" value="ID!0,ID!${RegisterCategory.Id},and"/>
                <ffi:list collection="RegisterCategories" items="Category">
                    <ffi:cinclude value1="${Category.ParentCategory}" value2="-1">
                        <option value="<ffi:getProperty name='Category' property='Id'/>" <ffi:cinclude value1="${RegisterCategory.ParentCategory}" value2="${Category.Id}">selected</ffi:cinclude> ><ffi:getProperty name="Category" property="Name"/></option>
                    </ffi:cinclude>
                </ffi:list>
            </SELECT>
            <input type="hidden" name="categorytypes" value="<ffi:list collection='RegisterCategories' items='Category'><ffi:cinclude value1='${Category.ParentCategory}' value2='-1'>cat<ffi:getProperty name='Category' property='Id'/>-<ffi:getProperty name='Category' property='Type'/></ffi:cinclude></ffi:list>"/>
        </TD>
    </TR>
    <tr>
	<td colspan="6"><br></td>
    </tr>
    <tr>
	<td align="center" colspan="6"><span class="required">* <s:text name="jsp.default_240"/></span></td>
    </tr>
    <tr>
	<td colspan="6"><br></td>
    </tr>
        <TR>
          <TD class=columndata colSpan=6 align=center>
			<script>
				ns.account.cancleButtonRedirectRegisteURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/register-wait.jsp'/>";
			</script>
                    <sj:a
                       button="true"
                       onclick="ns.account.cancleButtonRedirectRegiste(ns.account.cancleButtonRedirectRegisteURL)"><s:text name="jsp.default_82"/></sj:a>&nbsp;&nbsp;
                        <sj:a 
                       formIds="editcategory"
                       button="true"
                       buttonIcon="ui-icon-check"
                       targets="accountRegisterDashbord"
                       validateFunction="customValidation"
                       validate="true"
                       onclick="setCategoryType()"
                       ><s:text name="jsp.default_366"/></sj:a>
        </TD>
         </TR>
        </TABLE>
        </s:form>
</ffi:cinclude>
  </TD>

  </TR>
</table>

</div>
<div id="deleteCategoryresultmessage">
</div>
<div id="filterLinkCategory">
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>a,AND,names<< b')"><span class=sectionsubhead><s:text name="jsp.account_5"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>b,AND,names<< c')"><span class=sectionsubhead><s:text name="jsp.account_34"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>c,AND,names<< d')"><span class=sectionsubhead><s:text name="jsp.account_43"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>d,AND,names<< e')"><span class=sectionsubhead><s:text name="jsp.account_63"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>e,AND,names<< f')"><span class=sectionsubhead><s:text name="jsp.default_177"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>f,AND,names<< g')"><span class=sectionsubhead><s:text name="jsp.account_88"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>g,AND,names<< h')"><span class=sectionsubhead><s:text name="jsp.account_91"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>h,AND,names<< i')"><span class=sectionsubhead><s:text name="jsp.account_92"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>i,AND,names<< j')"><span class=sectionsubhead><s:text name="jsp.account_96"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>j,AND,names<< k')"><span class=sectionsubhead><s:text name="jsp.account_112"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>k,AND,names<< l')"><span class=sectionsubhead><s:text name="jsp.account_113"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>l,AND,names<< m')"><span class=sectionsubhead><s:text name="jsp.account_114"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>m,AND,names<< n')"><span class=sectionsubhead><s:text name="jsp.account_118"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>n,AND,names<< o')"><span class=sectionsubhead><s:text name="jsp.account_143"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>o,AND,names<< p')"><span class=sectionsubhead><s:text name="jsp.account_151"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>p,AND,names<< q')"><span class=sectionsubhead><s:text name="jsp.account_157"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>q,AND,names<< r')"><span class=sectionsubhead><s:text name="jsp.account_169"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>r,AND,names<< s')"><span class=sectionsubhead><s:text name="jsp.account_173"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>s,AND,names<< t')"><span class=sectionsubhead><s:text name="jsp.account_184"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>t,AND,names<< u')"><span class=sectionsubhead><s:text name="jsp.account_195"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>u,AND,names<< v')"><span class=sectionsubhead><s:text name="jsp.account_219"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>v,AND,names<< w')"><span class=sectionsubhead><s:text name="jsp.account_222"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>w,AND,names<< x')"><span class=sectionsubhead><s:text name="jsp.account_224"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>x,AND,names<< y')"><span class=sectionsubhead><s:text name="jsp.account_225"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>y,AND,names<< z')"><span class=sectionsubhead><s:text name="jsp.account_226"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('names>>z')"><span class=sectionsubhead><s:text name="jsp.account_228"/></span></a>
<span class=sectionsubhead>&nbsp;&nbsp;&nbsp;&nbsp;</span>
<a href="#" onclick="ns.account.redirectCategoryFilter('ALL')"><span class=sectionsubhead><s:text name="jsp.account_17"/></span></a>
</div>
<div id="categoriesSummaryDiv">
     <s:include value="/pages/jsp/account/currentcategories_grid.jsp"/>
</div>
<br/>
<br/>
<br/>
<br/>
<ffi:object id="AddRegisterCategory" name="com.ffusion.tasks.register.AddRegisterCategory" scope="session"/>
	<%
		session.setAttribute("FFIAddRegisterCategory", session.getAttribute("AddRegisterCategory"));
	%>

<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
	<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
	<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
    <%
		session.setAttribute("FFICopyCollection", session.getAttribute("CopyCollection"));
	%>
	<ffi:object id="EditRegisterCategory" name="com.ffusion.tasks.register.EditRegisterCategory" scope="session"/>
	<%
		session.setAttribute("FFIEditRegisterCategory", session.getAttribute("EditRegisterCategory"));
	%>
	
<script>	
					
		$('#register_cateID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#register_cateID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});
		<ffi:cinclude value1="${SetEditRegisterCategory}${SetEditRegisterSubCategory}" value2="">
			$('#register_cateID').portlet('title', js_acc_addcategory_portlet_title);
			<ffi:setProperty name="SetEditRegisterCategory" value=""/>
			<ffi:setProperty name="SetEditRegisterSubCategory" value=""/>
		</ffi:cinclude>	
		<ffi:cinclude value1="${SetEditRegisterCategory}${SetEditRegisterSubCategory}" value2="" operator="notEquals">
			$('#register_cateID').portlet('title', js_acc_editcategory_portlet_title);
			<ffi:setProperty name="SetEditRegisterCategory" value=""/>
			<ffi:setProperty name="SetEditRegisterSubCategory" value=""/>
		</ffi:cinclude>
</script>