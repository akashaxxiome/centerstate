<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.util.FilteredList"%>
<%@ page import="com.ffusion.beans.banking.Transaction"%>

<div class='calendarData'>
<s:include value="/pages/jsp/account/inc/accounthistory-search-init.jsp"/>

<ffi:help id="account_transactionDetail" />
<%
String isCalendarView = (String)request.getParameter("isCalendar");
if(isCalendarView != null && isCalendarView.equals("true"))  {
	session.setAttribute("tranIndex", request.getParameter("tranIndex"));
} else {
	String tranIndex = (String)request.getParameter("tranIndex");
	FilteredList transactions = (FilteredList)session.getAttribute("Transactions"); 
	if (transactions != null && transactions.size() > 0) {
		java.util.Iterator transactionIterator = transactions.iterator();
		int index = 0;
		while ( transactionIterator.hasNext() )
		{
			Transaction transaction = (Transaction)transactionIterator.next();
			index = index + 1;
			if(transaction.containsKey("accIndex")) {
				String trnIndex = (String)transaction.get("accIndex");
				if(trnIndex != null && trnIndex.equals(tranIndex)) {
					session.setAttribute("tranIndex", String.valueOf(index));
					break;
				}
			}
			
		}
		
		if(session.getAttribute("tranIndex") == null){
 			session.setAttribute("tranIndex", String.valueOf(index));
		 
		}
	}
} 
%>
<s:set var="isCalendar" value="#parameters.isCalendar"/>
<ffi:list collection="Transactions" items="Transaction" startIndex="${tranIndex}" endIndex="${tranIndex}">
<%-- <div class="confirmationDetails">
	<span>Reference Number</span>
	<span><ffi:getProperty name="Transaction" property="ReferenceNumber"/></span>
</div> --%>
<div class="transactionDetailHolderCls">
<div class="blockWrapper" >

	
	<div class="blockContent label130">
		<div class="blockRow">
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel "><s:text name="jsp.account_244"/>:</span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="ReferenceNumber"/></span>
			</div>
			<div class="inlineBlock">		
				<span class="sectionLabel"><s:text name="jsp.default_143"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="Date"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock">		
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_62"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="CustomerReferenceNumber"/></span>
			</div>
			<div class="inlineBlock">		
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_216"/></span>
				<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />				
				<ffi:setProperty name="TransactionTypes" property="TypeCode" value="${Transaction.TypeValue}" />
				<ffi:process name="TransactionTypes" />
				<span class="sectionsubhead valueCls">
				<ffi:getProperty name="TransactionTypes" property="TypeCode" />

					  <ffi:getProperty name="TransactionTypes" property="Description" /></span>
					 <ffi:removeProperty name="TransactionTypes" />
			</div>
		</div>
		
		<div class="blockRow">
			<img src="http://172.21.21.24/imgreq/imgreqis.dll?IC&CO=000718&item=<ffi:getProperty name="Transaction" property="BankReferenceNumber"/>&date=<ffi:getProperty name="Transaction" property="Date"/>&type=H&side=F" width="70%" height="70%"/>
		</div>	
		
		<div class="blockRow">
			<img src="http://172.21.21.24/imgreq/imgreqis.dll?IC&CO=000718&item=<ffi:getProperty name="Transaction" property="BankReferenceNumber"/>&date=<ffi:getProperty name="Transaction" property="Date"/>&type=H&side=B" width="70%" height="70%"/>
		</div>			
		
	</div>
	
	

	<s:if test="%{!#isCalendar}">
	<div class="marginTop30 ffivisible">&nbsp;</div>
	<div align="center" class="ui-widget-header customDialogFooter" style="background-color:#fff !important;">
		<form method="post">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<sj:a id="printTransactionDetail"
					  button="true" 
					  onClick="ns.common.printPopup();" cssClass="hiddenWhilePrinting"><s:text name="jsp.default_331"/></sj:a>
					  
		<sj:a id="doneFormButton"
		               button="true"
					onClickTopics="closeDialog"  cssClass="hiddenWhilePrinting" 
				><s:text name="jsp.default_538"/></sj:a>
		</form>
	</div>
	</s:if>
	<s:else>
		<input type="hidden" id="isRendered" value='<s:property value="#parameters.isCalendar"/>'/>
	</s:else>
</div>
</div>
</ffi:list>



</div>