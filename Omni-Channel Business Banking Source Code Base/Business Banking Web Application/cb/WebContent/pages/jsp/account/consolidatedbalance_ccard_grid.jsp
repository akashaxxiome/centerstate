<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<% 
   String dataClassification = request.getParameter("classfication");
   String displayCurrencyCode = request.getParameter("displayCurrencyCode");
   request.setAttribute("dataClassification",dataClassification);
   request.setAttribute("displayCurrencyCode",displayCurrencyCode);
%>

<div id="consolidatedBalance_ccard_girdID" >
<ffi:help id="accountsbygroup" />
<ffi:setGridURL grid="GRID_consolidatedvalanceCcard" name="LinkURL" url="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=4&AccountID={0}&AccountBankID={1}&AccountRoutingNum={2}&DataClassification={3}&redirection=true&ProcessAccount=true&TransactionSearch=false" parm0="Account.ID" parm1="Account.BankID" parm2="Account.RoutingNum" parm3="DataClassification"/>
<ffi:setProperty name="consolidatedBalanceCcardSummaryTempURL" value="/pages/jsp/account/GetConsolidatedBalanceCcardAction.action?dataClassification=${dataClassification}&displayCurrencyCode=${displayCurrencyCode}&GridURLs=GRID_consolidatedvalanceCcard" URLEncrypt="true"/>
<s:url id="ccardSummaryURL" value="%{#session.consolidatedBalanceCcardSummaryTempURL}" escapeAmp="false"/>

<sjg:grid
     id="consolidatedBalanceCcardSummaryID"
	 sortable="true" 
     dataType="json" 
     href="%{ccardSummaryURL}"
     pager="true"
     gridModel="gridModel"
	 rowList="%{#session.StdGridRowList}" 
	 rowNum="%{#session.StdGridRowNum}" 
	 rownumbers="false"
	 navigator="true"
	 navigatorAdd="false"
	 navigatorDelete="false"
	 navigatorEdit="false"
	 navigatorRefresh="false"
	 navigatorSearch="false"
	 navigatorView="false"
	 footerrow="true"
	 userDataOnFooter="true"
	 shrinkToFit="true"
	 scroll="false"
	 scrollrows="true"
	 viewrecords="true"
     onGridCompleteTopics="addGridControlsEvents,consolidatedBalanceCcardSummaryEvent"
     >
     <sjg:gridColumn name="account.accountDisplayText" index="NUMBER" title="%{getText('jsp.default_15')}" sortable="true" width="150" cssClass="datagrid_actionColumn" formatter="ns.account.formatAccountColumn"/>
     <sjg:gridColumn name="nameNickNameccard" index="ACCNICKNAME" title="%{getText('jsp.default_293')}" sortable="true" width="280" formatter="ns.account.customToAccountNickNameColumn" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.bankName" index="BANKNAME" title="%{getText('jsp.default_61')}" sortable="true" width="95"  formatter="ns.account.nameFormatter" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="summaryDateDisplay" index="SummaryDateDisplay" title="%{getText('jsp.default_51')}" sortable="true" width="60" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="availableCredit" index="AVAIL_CREDIT" title="%{getText('jsp.default_56')}" sortable="true" width="150" cssClass="datagrid_amountColumn" formatter="ns.account.currentLedgerFormatter"/>
     <sjg:gridColumn name="rate" index="INTEREST_RATE" title="%{getText('jsp.default_336')}" sortable="true" width="80" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="amountDue" index="AMT_DUE" title="%{getText('jsp.default_44')}" sortable="true" width="80" cssClass="datagrid_dateColumn" formatter="ns.account.currentLedgerFormatter"/>
     <sjg:gridColumn name="dateDue" index="DUE_DATE" title="%{getText('jsp.default_138')}" sortable="true" width="80"  formatter="ns.account.dateTimeFormatter" cssClass="datagrid_dateColumn"/>
     <sjg:gridColumn name="balance" index="CLOSINGBALANCE" title="%{getText('jsp.default_60')}" sortable="true" width="80" cssClass="datagrid_amountColumn" formatter="ns.account.currentLedgerFormatter"/>
     <sjg:gridColumn name="account.displayText" index="displayText" title="%{getText('jsp.account_77')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.nickName" index="NICKNAME" title="%{getText('jsp.account_146')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.currencyCode" index="CURRENCY_CODE" title="%{getText('jsp.account_60')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.ID" index="ID" title="%{getText('jsp.account_12')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.bankID" index="BANKID" title="%{getText('jsp.account_39')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.routingNum" index="ROUTINGNUM" title="%{getText('jsp.account_183')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>
     <sjg:gridColumn name="displayCurrencyCode" index="map.displayCurrencyCode" title="%{getText('jsp.default_173')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="dataClassification" index="map.dataClassification" title="%{getText('jsp.account_64')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="loanAvailCreditTotal" index="map.loanAvailCreditTotal" title="%{getText('jsp.account_117')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="amountDueTotal" index="map.amountDueTotal" title="%{getText('jsp.account_27')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="balanceTotal" index="map.balanceTotal" title="%{getText('jsp.account_37')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="map.LinkURL" index="LinkURL" title="%{getText('jsp.default_262')}" sortable="true" width="60" hidden="true" hidedlg="true"/>
</sjg:grid>

</div>
<script>	
					
		/* $('#consolidatedBalance_ccard_girdID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#consolidatedBalance_ccard_girdID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});

		$('#consolidatedBalance_ccard_girdID').portlet('title', js_ccard_protlet_title); */

</script>
<style>
	#consolidatedBalance_ccard_girdID .ui-jqgrid-ftable td{
		border:none;
	}
</style>
