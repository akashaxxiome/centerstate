<%@page import="com.ffusion.util.HTMLUtil"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<s:include value="/pages/jsp/account/inc/accounthistory_common.jsp"/>

<script type="text/javascript"><!--
function CSClickReturn () {
	var bAgent = window.navigator.userAgent;
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
}
CSStopExecution=false;
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
	var result;
	for (var i=0;i<array.length;i++) {
		if(CSStopExecution) return false;
		var aa = fct[array[i]];
		if (aa == null) return false;
		var ta = new Array;
		for(var j=1;j<aa.length;j++) {
			if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
				if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
				else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
				else ta[j]=aa[j];}
			} else ta[j]=aa[j];
		}
		result=aa[0](ta);
	}
	return result;
}
CSAct = new Object;
function CSOpenWindow(action) {
	var wf = "";
	wf = wf + "width=" + action[3];
	wf = wf + ",height=" + action[4];
	wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
	wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
	wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
	wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
	wf = wf + ",directories=" + (action[9] ? "yes" : "no");
	wf = wf + ",location=" + (action[10] ? "yes" : "no");
	wf = wf + ",status=" + (action[11] ? "yes" : "no");
	window.open(action[1],action[2],wf);
}

function setTranNumber( num )
{
	CSAct[/*CMP*/ 'B9D1DAD41'] = new Array(CSOpenWindow,/*URL*/ '<ffi:getProperty name="SecurePath"/>account/transactiondetail.jsp?tranIndex=' + num,'',518,455,false,false,false,false,false,false,false);
}

CSAct[/*CMP*/ 'B9D1DAD40'] = new Array(CSOpenWindow,/*URL*/ '<ffi:getProperty name="SecurePath"/>account/inquirehistory.jsp','',300,350,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E611E8165'] = new Array(CSOpenWindow,/*URL*/ '<ffi:getProperty name='SecurePath'/>calendar.jsp?calDirection=back&calForm=FormName&calTarget=GetPagedTransactions.StartDate','',350,300,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E611E8166'] = new Array(CSOpenWindow,/*URL*/ '<ffi:getProperty name='SecurePath'/>calendar.jsp?calDirection=back&calForm=FormName&calTarget=GetPagedTransactions.EndDate','',350,300,false,false,false,false,false,false,false);

		function popNav()
		{
			var optionValue;
			optionValue = document.navForm.popNav.options[document.navForm.popNav.selectedIndex].value;
			document.location = optionValue;
		}

// --></script>

<%-- include javascripts for top navigation menu bar --%>

		<div align="left">

			<%-- include page header --%>
			
			<%-- set up to fetch the Transactions collection --%>
			<% String zbaFlag=null; %>
			<ffi:getProperty name="TSZBADisplay" assignTo="zbaFlag"/>
			<ffi:cinclude value1="<%= zbaFlag %>" value2="" operator="equals">
				<ffi:getProperty name="Account" property="ZBAFlag" assignTo="zbaFlag"/>
			</ffi:cinclude>
			<ffi:cinclude value1="<%= zbaFlag %>" value2="" operator="equals">
				<%
					zbaFlag = com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH;
				%>
			</ffi:cinclude>
			<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ZBA_DISPLAY %>" />
			<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="<%= zbaFlag %>" />

			<%-- fetch Transactions collection --%>
			<%
				session.setAttribute("FFIGetPagedTransactions", session.getAttribute("GetPagedTransactions"));
			%>

			<ffi:setProperty name="BackURL" value="${SecurePath}account/accounthistory_loan.jsp"/>
			<%
				com.ffusion.beans.accounts.Account currAccount = (com.ffusion.beans.accounts.Account)session.getAttribute("Account");
				String accountDisplayText = null;
			%>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
				<% if(currAccount != null) { 
					accountDisplayText = HTMLUtil.encode(currAccount.getDisplayTextRoutingNumNickNameCurrency()); 
				} %>
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
					<script>
					    ns.account.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accounthistory_loan_grid.jsp' />";
						$(function(){
							ns.account.refreshSummaryForm(ns.account.refreshSummaryFormURL);
						});
					</script>
				</ffi:cinclude>
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
					<script>
						ns.account.gridContainerID = "<ffi:getProperty name='accountHistoryGridID${accountIndex}'/>";
					    ns.account.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accounthistory_loan_grid.jsp?accountIndex=${accountIndex}&setAccountID=${setAccountID}'/>";
						$(function(){
							var gridID = "<ffi:getProperty name="accountHistoryGridID${accountIndex}"/>";
							ns.account.refreshSummaryForm(ns.account.refreshSummaryFormURL, gridID);
						});
					</script>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
				<% if(currAccount != null) {
						accountDisplayText = currAccount.getConsumerAccountDisplayTextWithType(); 
				} %>
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
					<script>
					    ns.account.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accounthistory_loan_grid.jsp' />";
						$(function(){
							ns.account.refreshSummaryForm(ns.account.refreshSummaryFormURL);
						});
					</script>
				</ffi:cinclude>
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
					<script>
						ns.account.gridContainerID = "<ffi:getProperty name='accountHistoryGridID${accountIndex}'/>";
					    ns.account.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accounthistory_loan_grid.jsp?accountIndex=${accountIndex}&setAccountID=${setAccountID}'/>";
						$(function(){
							var gridID = "<ffi:getProperty name="accountHistoryGridID${accountIndex}"/>";
							ns.account.refreshSummaryForm(ns.account.refreshSummaryFormURL, gridID);
						});
					</script>
				</ffi:cinclude>
			</ffi:cinclude>
		</div>

<%-- Since this search is OK, store those values here. --%>
<ffi:setProperty name="unsuccessfulSearch" value="false"/>

<ffi:setProperty name="LastGoodTSTransactionType" value="${TSTransactionType}"/>
<ffi:setProperty name="LastGoodTSZBADisplay" value="${TSZBADisplay}"/>
<ffi:setProperty name="LastGoodTSReferenceStart" value="${TSReferenceStart}"/>
<ffi:setProperty name="LastGoodTSReferenceEnd" value="${TSReferenceEnd}"/>
<ffi:setProperty name="LastGoodTSMinimumAmount" value="${TSMinimumAmount}"/>
<ffi:setProperty name="LastGoodTSMaximumAmount" value="${TSMaximumAmount}"/>
<ffi:setProperty name="LastGoodStartDate" value="${GetPagedTransactions.StartDate}"/>
<ffi:setProperty name="LastGoodEndDate" value="${GetPagedTransactions.EndDate}"/>
<ffi:setProperty name="LastGoodDataClassification" value="${GetPagedTransactions.DataClassification}"/>
<%
	session.setAttribute( "LastGoodAccount", session.getAttribute( "Account" ) );
%>

<ffi:removeProperty name="TSView"/>
<ffi:removeProperty name="DateRangeValue"/>
