<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<ffi:setProperty name="subMenuSelected" value="account register"/>

<ffi:cinclude value1="${register_init_touched}" value2="true" operator="notEquals" >
	<s:include value="/pages/jsp/inc/init/register-init.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${Prev_RegisterCategories_Locale}" value2="${UserLocale.Locale}" operator="notEquals">
	<ffi:list collection="RegisterCategories" items="Category">
		<ffi:cinclude value1="${Category.Id}" value2="0" operator="equals">
			<ffi:setProperty name="Category" property="Locale" value="${UserLocale.Locale}" />
		</ffi:cinclude>
	</ffi:list>
</ffi:cinclude>
<ffi:setProperty name="Prev_RegisterCategories_Locale" value="${UserLocale.Locale}"/>
<%
    String selectAccount = request.getParameter("SetDefaultRegisterAccountID");
%>
<ffi:setProperty name="SetDefaultRegisterAccount" property="NoRegisterEnabledAccounts" value="false"/>
<ffi:setProperty name="SetDefaultRegisterAccount" property="ID" value="<%=selectAccount %>"/>
<ffi:process name="SetDefaultRegisterAccount"/>


<ffi:cinclude value1="${SetDefaultRegisterAccount.NoRegisterEnabledAccounts}" value2="true">
    <s:include value="/pages/jsp/account/register-no-accounts.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${SetDefaultRegisterAccount.NoRegisterEnabledAccounts}" value2="true" operator="notEquals">
    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS %>">
        <ffi:cinclude value1="${TransfersUpdated}" value2="true">
            <ffi:removeProperty name="TransfersUpdated"/>
            <ffi:setProperty name="GetTransfersRegister" property="Reload" value="true"/>
            <ffi:process name="GetTransfersRegister"/>
            <ffi:process name="ProcessTransferTransactions"/>
        </ffi:cinclude>
    </ffi:cinclude>

    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAYMENTS %>">
        <ffi:cinclude value1="${PaymentsUpdated}" value2="true">
            <ffi:removeProperty name="PaymentsUpdated"/>
            <ffi:setProperty name="GetPaymentsRegister" property="Reload" value="true"/>
            <ffi:process name="GetPaymentsRegister"/>
            <ffi:process name="ProcessPaymentTransactions"/>
        </ffi:cinclude>
    </ffi:cinclude>

    <ffi:setProperty name="GetTransactionsRegister" property="StartDate" value="${Account.REG_RETRIEVAL_DATE}"/>
    <ffi:setProperty name="GetCurrentDate" property="DateFormat" value="MM/dd/yyyy" />
    <ffi:setProperty name="GetTransactionsRegister" property="EndDate" value="${GetCurrentDate.Date}"/>
    <ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateFormat}" />
    <ffi:process name="GetTransactionsRegister"/>

	<ffi:setProperty name="AddBankTransactions" property="Reload" value="true" />
    <ffi:process name="AddBankTransactions"/>

    <ffi:setProperty name="GetRegisterTransactions" property="FromDate" value="${GetCurrentDate.DateWithAdditionalDays}" />
    <ffi:setProperty name="GetRegisterTransactions" property="ToDate" value="${GetCurrentDate.Date}" />
    <ffi:setProperty name="GetRegisterTransactions" property="IncludeUnreconciled" value="true" />
    <ffi:setProperty name="GetRegisterTransactions" property="ReferenceNumber" value="" />
        <%
		 session.setAttribute("FFIGetRegisterTransactions", session.getAttribute("GetRegisterTransactions"));
	    %>
    <ffi:process name="GetRegisterTransactions"/>

    <ffi:setProperty name="RegFilter" value="Status!1"/>
    <ffi:setProperty name="RegisterTransactions" property="SortedBy" value="" />
    <ffi:setProperty name="FutureRegisterTransactions" property="SortedBy" value="DATE_ISSUED" />

    <%-- Prepare Sorting Functionality --%>
    <ffi:object name="com.ffusion.tasks.util.GetSortImage" id="RegisterTransactionsSortImage" scope="session"/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="RegisterTransactionsSortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/account/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="RegisterTransactionsSortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/account/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="RegisterTransactionsSortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/account/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <ffi:setProperty name="RegisterTransactionsSortImage" property="Collection" value="RegisterTransactions"/>
    <ffi:setProperty name="RegisterTransactionsSortImage" property="Compare" value="DATE_ISSUED"/>
    <ffi:process name="RegisterTransactionsSortImage"/>

    <s:include value="/pages/jsp/account/register.jsp"/>
</ffi:cinclude>
