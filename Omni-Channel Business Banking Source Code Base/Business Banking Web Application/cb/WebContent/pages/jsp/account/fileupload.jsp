<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:object id="FileImportType" name="com.ffusion.tasks.fileImport.ListUploadFileDisplayNamesTask" scope="session"/>
<ffi:setProperty name="FileImportType" property="CollectionName" value="UploadFileTypes" />
<ffi:process name="FileImportType"/>
<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies" scope="session"/>
	<ffi:setProperty name="GetACHCompanies" property="CustID" value="${SecureUser.BusinessID}" />
	<ffi:setProperty name="GetACHCompanies" property="Reload" value="true" />
	<ffi:setProperty name="GetACHCompanies" property="FIID" value="${SecureUser.BankID}" />
	<ffi:setProperty name="GetACHCompanies" property="LoadCompanyEntitlements" value="true"/>
<ffi:process name="GetACHCompanies"/>
<ffi:setProperty name="ACHCOMPANIES" property="SortedBy" value="CONAME" />


<script type="text/javascript"><!--

	$(function(){
			$("#fileType").selectmenu({width:'22em'});
	});
	function popNav()
	{
		var optionValue;
		optionValue = document.navForm.popNav.options[document.navForm.popNav.selectedIndex].value;
		document.location = optionValue;
	}
   $(function(){
	   $("#FileUpload\\.FileType").selectmenu({width: 220});
	   });
   function checkFileUpload()
   {
   	window.name='FileUploadOpener'+ '<%= java.lang.Math.random() %>';
   }
   $.subscribe('openAcctFileUploadTopics', function(event,data){
	 	//load import dialog if not loaded
		var config = ns.common.dialogs["fileImport"];
		config.autoOpen = "true";ns.common.openDialog("fileImport");
	});
	$( document ).ready(function() {
	var selectedValue=$("#fileType").value
	if(selectedValue=='ACH')
	{
		$("#achCutoffMessage").show();
	}

					$("#fileType").change(function () {
						var selectedValue=this.value
						if(selectedValue=='ACH')
						{
							$("#achCutoffMessage").show();
						}
						else
						{
							$("#achCutoffMessage").hide();
						}
				});
	});
// --></script>

<div align="center" id="summary">

<ffi:setProperty name="subMenuSelected" value="file upload"/>
            <%--action="<ffi:getProperty name='SecurePath'/>account/uploadpprecord.jsp" --%>
			<form id="actManagementFileType" name="FileType" action="/cb/pages/fileupload/AccountFileUploadAction_openAcctImportWidget.action" method="post" target="FileUpload">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

			<%-- <!-- File upload label -->
			<div class="acntDashboard_masterItemHolder textAlignLeft" style="width:100%">
				<span class="sectionhead">&gt; <s:text name="jsp.default_211"/>
				<hr />
			</div> --%>

			<!-- File upload master holder -->
			<div class="acntDashboard_masterItemHolder textAlignLeft" id="fileUploadTab"  class="portlet floatLeft">
				<ffi:help id="acctMgmt_fileupload" />
				<%-- Check if there are no upload types available --%>
				<ffi:cinclude value1="${UploadFileTypes}" value2="" operator="equals">
					<ffi:setProperty name="tmp_noFileTypes" value="true"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${UploadFileTypes.Size}" value2="0" operator="equals">
					<ffi:setProperty name="tmp_noFileTypes" value="true"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${tmp_noFileTypes}" value2="true" operator="equals">
					<div class="acntDashboard_itemHolder">
						<s:text name="jsp.account_200"/>
					</div>

				</ffi:cinclude><%-- End if, UploadFileTypes is empty --%>

				<ffi:cinclude value1="${tmp_noFileTypes}" value2="true" operator="notEquals">
    				<div class="portlet-header">
							<!-- <div class=""> -->
								<%-- <span class="searchPanelToggleAreaCls" onclick="$('#uploadGridContainer').slideToggle();"> --%>
								<%-- <span class="searchPanelToggleAreaCls">
									<span class="gridSearchIconHolder sapUiIconCls"></span>
								</span> --%>
								<span  class="portlet-title">
									<%-- <span id="selectedGridTab"> --%>
										<s:text name="jsp.default_453"/>
									<%-- </span> --%>
								</span>
							<!-- </div> -->
					</div>
					<div class="portlet-content">
						<div id="uploadGridContainer" class="summaryGridHolderDivCls marginBottom10" style="margin-left:10px;">
							<!-- Upload Records holder -->
							<div class="acntDashboard_itemHolder sectionsubhead">
								<%-- <u><s:text name="jsp.default_453"/></u>
								<br> --%>
								<p class="sectionsubhead marginRight30">
									<s:text name="jsp.account_90"/><br><br>
									<s:text name="jsp.account_99"/>
								</p>
								<br>
							</div>

							<!-- Accepted File Types -->
							<div class="acntDashboard_itemHolder sectionsubhead marginBottom20">
								<s:text name="jsp.default_14"/>
								<select id="fileType" name="fileType" class="txtbox">
									<ffi:list collection="UploadFileTypes" items="FileType">
										<option value="<ffi:getProperty name='FileType' property='Name'/>"><ffi:getProperty name="FileType" property="DisplayName" /></option>
									</ffi:list>
								</select>
							</div>
							<div class="acntDashboard_itemHolder">
								 <sj:a
									id="acctFileUploadID"
									formIds="actManagementFileType"
									button="true"
									targets="openFileImportDialogID"
									onclick="checkFileUpload()"
									onErrorTopics="errorOpenFileUploadTopics"
									onSuccessTopics="openAcctFileUploadTopics">
									<s:text name="jsp.default_452"/></sj:a>
							</div>
						</div>
					</div>
					<div id="fileUploadDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
				       <ul class="portletHeaderMenu" style="background:#fff">
							  <li><a href='#' onclick="ns.common.showDetailsHelp('fileUploadTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
				       </ul>
					</div>
					<div>
						<s:include value="/pages/jsp/account/achSameDayMsg.jsp"/>
					</div>
					</ffi:cinclude> <%-- end if, collection "UploadTypes" is not empty --%>

				</div>

	</form>
</div>

<ffi:object id="FFIUpdateReportCriteria" name="com.ffusion.tasks.reporting.UpdateReportCriteria"/>
<ffi:setProperty name="FFIUpdateReportCriteria" property="ReportName" value="ReportData"/>

<ffi:removeProperty name="saveBackURL"/>
<ffi:removeProperty name="UploadFileTypes"/>
<ffi:removeProperty name="tmp_noFileTypes"/>
<ffi:setProperty name="actionNameNewReportURL" value="NewReportBaseAction.action?NewReportBase.ReportName=${ReportName}&flag=generateReport" URLEncrypt="true" />
<ffi:setProperty name="actionNameGenerateReportURL" value="GenerateReportBaseAction_display.action?doneCompleteDirection=${doneCompleteDirection}" URLEncrypt="true" />

<script type="text/javascript">
$(document).ready(function(){
	ns.common.initializePortlet("fileUploadTab");
});


</script>
