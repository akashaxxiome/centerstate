<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<script type="text/javascript" src="<s:url value='/web/js/account/account%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/account/account_grid%{#session.minVersion}.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/accounts/accounts%{#session.minVersion}.css'/>"></link>

<%
String accountID = request.getParameter("SetAccount.ID");
String accountBankID= request.getParameter("SetAccount.BankID");
String accountRoutingNum = request.getParameter("SetAccount.RoutingNum");
if (accountID == null || accountID.isEmpty()) {
	// from Balance
	accountID = request.getParameter("AccountID"); 
	accountBankID= request.getParameter("AccountBankID");
	accountRoutingNum = request.getParameter("AccountRoutingNum");
}

	session.setAttribute("AcctId", accountID);
	session.setAttribute("BankId", accountBankID);  
	session.setAttribute("RoutingNum",accountRoutingNum );
%>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_335')}" scope="request" /><ffi:setProperty name="PageHeading" value="${tmpI18nStr}"/>

		<div align="center">


		</div>
		<ffi:flush/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="ACCOUNTGROUP!!0"/>
<ffi:setProperty name="BankingAccounts" property="FilterSortedBy" value="ACCOUNTGROUP,ID"/>

<ffi:setProperty name="RedirectURL" value="${SecurePath}account/accounthistory.jsp"/>

<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>

<%-- If the user is not entitled to view detail information for any accounts with either intra day or previous day classification then we will redirect them to a page that indicates there are no accounts to report on --%>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}" value2="false" operator="equals">
	<ffi:setProperty name="EntitledToDetailAnyDataClassificationFlagForAction" value="false"/>
	<script>
		ns.account.appdashboardflag=true;
	</script>
</ffi:cinclude>

<%-- if the user is entitled to view detail information for one or more of the dataclassifications we should determine the redirect URL from the account list that they are entitled to --%>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailAnyDataClassification}" value2="true" operator="equals">
	<script>
		ns.account.appdashboardflag=false;
	</script>
<%-- we must determine the default data classification based on whether the user can see intra or previous day for any accounts--%>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
	<ffi:setProperty name="dataClass" value="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
</ffi:cinclude>
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="false" operator="equals">
	<ffi:setProperty name="dataClass" value="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY%>"/>
</ffi:cinclude>

<ffi:object id="AccountEntitlementFilterTask" name="com.ffusion.tasks.accounts.AccountEntitlementFilterTask" scope="request"/>
<%-- This is a detail page so we will filter accounts on the detail view entitlement --%>
<ffi:setProperty name="AccountEntitlementFilterTask" property="AccountsName" value="BankingAccounts"/>
<ffi:setProperty name="AccountEntitlementFilterTask" property="FilteredAccountsName" value="EntitlementFilteredAccounts"/>
<%-- The initial data classification should be used to filter the account list --%>
<ffi:cinclude value1="${dataClass}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${dataClass}" value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY%>" operator="equals">
	<ffi:setProperty name="AccountEntitlementFilterTask" property="EntitlementFilter" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>"/>
</ffi:cinclude>
<ffi:process name="AccountEntitlementFilterTask"/>
<ffi:cinclude value1="${AcctId}" value2="" operator="notEquals">
<ffi:setProperty name="SetAccount" property="ID" value="${AcctId}"/>
</ffi:cinclude>

<ffi:cinclude value1="${BankId}" value2="" operator="notEquals">
<ffi:setProperty name="SetAccount" property="BankId" value="${BankId}"/>
</ffi:cinclude>

<ffi:cinclude value1="${RoutingNum}" value2="" operator="notEquals">
<ffi:setProperty name="SetAccount" property="RoutingNum" value="${RoutingNum}"/>
</ffi:cinclude>

<ffi:cinclude value1="${AcctId}" value2="">
	<ffi:setProperty name="SetAccount" property="IsDefault" value="true"/>
</ffi:cinclude>

<ffi:setProperty name="SetAccount" property="AccountsName" value="EntitlementFilteredAccounts"/>
<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
<ffi:process name="SetAccount"/>

<%-- remove Transaction search specific properties --%>
<ffi:removeProperty name="TSTransactionType"/>
<ffi:removeProperty name="TSReferenceStart"/>
<ffi:removeProperty name="TSReferenceEnd"/>
<ffi:removeProperty name="TSMinimumAmount"/>
<ffi:removeProperty name="TSMaximumAmount"/>
<ffi:removeProperty name="TSDescription"/>
<ffi:removeProperty name="TSZBADisplay"/>

<ffi:removeProperty name="GetPagedTransactions"/>

<% session.setAttribute("TransactionSearch", request.getParameter("TransactionSearch")); %>

<%-- Clean up search criteria session variables for Transaction Search --%>
			<ffi:removeProperty name="DisplayTSTransactionType" />
			<ffi:removeProperty name="DisplayTSReferenceStart"/>
			<ffi:removeProperty name="DisplayTSReferenceEnd"/>
			<ffi:removeProperty name="DisplayTSMinimumAmount"/>
			<ffi:removeProperty name="DisplayTSMaximumAmount"/>
			<ffi:removeProperty name="DisplayTSDescription" />
			<ffi:removeProperty name="DisplayAccount"/>
			<ffi:removeProperty name="DisplayStartDate"/>
			<ffi:removeProperty name="DisplayEndDate"/>
			
			<ffi:removeProperty name="LastGoodTSReferenceStart"/>
			<ffi:removeProperty name="LastGoodTSReferenceEnd"/>
			<ffi:removeProperty name="LastGoodTSMinimumAmount"/>
			<ffi:removeProperty name="LastGoodTSMaximumAmount"/>
			<ffi:removeProperty name="LastGoodTSDescription"/>
			<ffi:removeProperty name="LastGoodAccount"/>
			<ffi:removeProperty name="LastGoodStartDate"/>
			<ffi:removeProperty name="LastGoodEndDate"/>

<ffi:setProperty name="unsuccessfulSearch" value="true"/>

<%-- Remove the display values if we have switched from transaction search to account history (or vice versa) --%>
<ffi:cinclude value1="${OldTransactionSearch}" value2="${TransactionSearch}" operator="notEquals">

	<ffi:setProperty name="DisplayDataClassification" value="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
	<ffi:setProperty name="DisplayTSTransactionType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>"/>
	<ffi:setProperty name="DisplayTSZBADisplay" value="<%=com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE%>"/>
	<ffi:removeProperty name="DisplayTSReferenceStart"/>
	<ffi:removeProperty name="DisplayTSReferenceEnd"/>
	<ffi:removeProperty name="DisplayTSMinimumAmount"/>
	<ffi:removeProperty name="DisplayTSMaximumAmount"/>
	<ffi:removeProperty name="DisplayTSDescription" />
	<ffi:removeProperty name="DisplayAccount"/>
	<ffi:removeProperty name="DisplayStartDate"/>
	<ffi:removeProperty name="DisplayEndDate"/>
	
	<ffi:setProperty name="LastGoodDataClassification" value="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
	<ffi:setProperty name="LastGoodTSTransactionType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>"/>
	<ffi:setProperty name="LastGoodTSZBADisplay" value=""/>
	<%--
	<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_NONE %>"/>
	--%>
	<ffi:removeProperty name="LastGoodTSReferenceStart"/>
	<ffi:removeProperty name="LastGoodTSReferenceEnd"/>
	<ffi:removeProperty name="LastGoodTSMinimumAmount"/>
	<ffi:removeProperty name="LastGoodTSMaximumAmount"/>
	<ffi:removeProperty name="LastGoodTSDescription"/>
	<ffi:removeProperty name="LastGoodAccount"/>
	<ffi:removeProperty name="LastGoodStartDate"/>
	<ffi:removeProperty name="LastGoodEndDate"/>
</ffi:cinclude>
<ffi:setProperty name="OldTransactionSearch" value="${TransactionSearch}"/>


</ffi:cinclude> <%-- End is user entitled to view at least one detail account for at least one of the data classes --%>
<ffi:removeProperty name="dataClass"/>
<ffi:setProperty name="BackURL" value="${SecurePath}account/index.jsp?TransactionSearch=false" URLEncrypt="true"/>
<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}account/index.jsp?TransactionSearch=true" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>

<%-- Gets default date range values --%>
<ffi:object id="GetDatesFromDateRange" name="com.ffusion.tasks.util.GetDatesFromDateRange" scope="session"/>
<ffi:setProperty name="DateRangeValue" value="<%= com.ffusion.tasks.util.GetDatesFromDateRange.LAST_30_DAYS %>"/>
<ffi:setProperty name="GetDatesFromDateRange" property="DateRangeValue" value="${DateRangeValue}"/>
<ffi:process name="GetDatesFromDateRange"/>
<%-- Sets the Default date range values in session variables for reference --%>
<ffi:setProperty name="DefaultStartDate" value="${GetDatesFromDateRange.StartDate}" />
<ffi:setProperty name="DefaultEndDate" value="${GetDatesFromDateRange.EndDate}" />
<%-- Sets the Default date range values in session variables for Display in search criteria --%>
<ffi:setProperty name="DisplayStartDate" value="${GetDatesFromDateRange.StartDate}"/>
<ffi:setProperty name="DisplayEndDate" value="${GetDatesFromDateRange.EndDate}"/>

	<div id="desktop" align="center">
		<div id="operationresult">
			<div id="resultmessage">Operation Result</div>
		</div><!-- result -->
		<div class="dashboardUiCls">
	    	<div class="moduleSubmenuItemCls">
	    		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
	    			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-tranSearch"></span>
	    		</ffi:cinclude>
	    		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
	    			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-accountHistory"></span>
	    		</ffi:cinclude>
	    		
	    		<span class="moduleSubmenuLbl">
	    		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
	    			<s:text name="jsp.home_221" />
	    		</ffi:cinclude>
	    		<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
	    			<s:text name="jsp.home_20" />
	    		</ffi:cinclude>
	    		</span>
	    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
				
				<!-- dropdown menu include -->    		
	    		<s:include value="/pages/jsp/home/inc/accountSubmenuDropdown.jsp" />
	    	</div>
	    	
	    	    	
	    	<!-- dashboard items listing -->
        	<div id="dbAccountHistorySummary" class="dashboardSummaryHolderDiv">
		        <ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
			         <sj:a id="goBackSummaryLink" button="true"
						cssClass="summaryLabelCls" 
						title="%{getText('jsp.default_400')}"
						onClickTopics="transactionSearchSummaryClick">
				  		<s:text name="jsp.default_400" />
					</sj:a>
		        </ffi:cinclude>
		        <ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
			       <sj:a id="goBackSummaryLink"  button="true"
						cssClass="summaryLabelCls"
						title="%{getText('jsp.default_400')}"
						onClickTopics="accountHistorySummaryClick">
				  		<s:text name="jsp.default_400" />
					</sj:a>
		        </ffi:cinclude>
			</div>
	    	
		</div>
		
		<% 
			String dashboardElementId = request.getParameter("dashboardElementId");
			if ("openCalendar".equals(dashboardElementId)) {
		%>
			<s:url id="calendarURL" value="/pages/jsp/account/loadCalendar_showAccHistory.action" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="moduleName" value="%{'accHistoryCalendar'}"></s:param>
			</s:url>
	       <sj:a
				id="openCalendar"
				href="%{calendarURL}"
				targets="summary"
				onSuccessTopics="calendarLoadedSuccess"
				cssClass="titleBarBtn hidden">
		   </sj:a>
		<% } %>
		
		<div id="summary">
		
			<% 	if (dashboardElementId==null || dashboardElementId.isEmpty()) { %>
			
			  <ffi:cinclude value1="${accountsList.size}" value2="0" operator="notEquals">
					<s:include value="/pages/jsp/account/accounthistory_summary.jsp"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${accountsList.size}" value2="0" operator="equals">
					<span id="noActiveAccount" class="txt_normal_bold"><s:text name="jsp.account_history.noActiveAccounts.message"></s:text></span>
				</ffi:cinclude>
								
			<% } %>
		</div>
	</div>

<ffi:removeProperty name="ReportData" />

<%-- This task will not be referenced in pages, so we can add prefix "FFI" directly. --%>
<ffi:object id="FFINewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>

<%-- This task will not be referenced in pages, so we can add prefix "FFI" directly. --%>
<ffi:object id="FFIUpdateReportCriteria" name="com.ffusion.tasks.reporting.UpdateReportCriteria"/>
<ffi:setProperty name="FFIUpdateReportCriteria" property="ReportName" value="ReportData"/>

<ffi:object id="FFIGenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
<%-- Adding the followed statement is because that the first print report function won't work well without it. --%>
<% session.setAttribute("GenerateReportBase", session.getAttribute("FFIGenerateReportBase")); %>

<ffi:object id="FFIAddReport" name="com.ffusion.tasks.reporting.AddReport"/>
<ffi:setProperty name="FFIAddReport" property="ReportName" value="ReportData" />

<ffi:object id="FFIModifyReport" name="com.ffusion.tasks.reporting.ModifyReport"/>

<ffi:object id="FFIDeleteReport" name="com.ffusion.tasks.reporting.DeleteReport"/>

<ffi:object id="FFIGetReportFromName" name="com.ffusion.tasks.reporting.GetReportFromName"/>
<ffi:setProperty name="FFIGetReportFromName" property="IDInSessionName" value="reportID" />
<ffi:setProperty name="FFIGetReportFromName" property="ReportName" value="ReportData"/>

<ffi:object id="FFIGetReportsIDsByCategory" name="com.ffusion.tasks.reporting.GetReportsIDsByCategory"/>
<ffi:setProperty name="FFIGetReportsIDsByCategory" property="reportCategory" value="all"/>

<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
	<sj:dialog id="saveSearchDialog" cssClass="acctMgmt_searchDialog" title="%{getText('jsp.save_search')}" overlayColor="#000" overlayOpacity="0.7"
		modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="450" formIds="addTransactionSearchForm">
	</sj:dialog>
	<sj:dialog id="deleteSaveSearchDialog" cssClass="acctMgmt_searchDialog" title="%{getText('jsp.delete_saved_search')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570" formIds="deleteSavedSearchForm">
	</sj:dialog>
	<sj:dialog id="accountsHistoryExportDialogID" cssClass="acctMgmt_searchDialog" title="%{getText('jsp.default.label.account.summary.export')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" height="350" width="700">
	</sj:dialog>
	<sj:dialog id="viewTransactionDetailDialogID" cssClass="acctMgmt_searchDialog" title="%{getText('jsp.account_214')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
	<sj:dialog id="viewInquiryDialogID" cssClass="acctMgmt_searchDialog" title="%{getText('jsp.default_249')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" cssStyle="overflow:hidden;" >
	</sj:dialog>
</ffi:cinclude>
<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
	<sj:dialog id="saveSearchDialog" cssClass="acctMgmt_historyDialog" title="%{getText('jsp.save_search')}" overlayColor="#000" overlayOpacity="0.7"
		modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="450" formIds="addTransactionSearchForm">
	</sj:dialog>
	<sj:dialog id="deleteSaveSearchDialog" cssClass="acctMgmt_historyDialog" title="%{getText('jsp.delete_saved_search')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570" formIds="deleteSavedSearchForm">
	</sj:dialog>
	<sj:dialog id="accountsHistoryExportDialogID" cssClass="acctMgmt_historyDialog" title="%{getText('jsp.default.label.account.summary.export')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" height="350" width="700">
	</sj:dialog>
	<sj:dialog id="viewTransactionDetailDialogID" cssClass="acctMgmt_historyDialog" title="%{getText('jsp.account_214')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="650" cssStyle="overflow:hidden">
	</sj:dialog>
	<sj:dialog id="viewInquiryDialogID" cssClass="acctMgmt_historyDialog" title="%{getText('jsp.default_249')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip"  width="800" cssStyle="overflow:hidden;">
	</sj:dialog>
</ffi:cinclude>

<script type="text/javascript">
<!--
$(document).ready(function(){
	 
	ns.account.showAccountDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	
	$('#saveSearchDialog').addHelp(function(){
		var helpFile = $('#saveSearchDialog').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});

	$('#deleteSaveSearchDialog').addHelp(function(){
		var helpFile = $('#deleteSaveSearchDialog').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});

	$('#accountsHistoryExportDialogID').addHelp(function(){
		var helpFile = $('#accountsHistoryExportDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	
	$('#viewTransactionDetailDialogID').addHelp(function(){
		var helpFile = $('#viewTransactionDetailDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});

	$('#viewInquiryDialogID').addHelp(function(){
		var helpFile = $('#viewInquiryDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});


	
	/* $('#appdashboard').pane({
		title: js_dashboard,
		minmax: true,
		close: false
	}); */
});
//-->
</script>

<ffi:removeProperty name="RedirectURL"/>
<ffi:removeProperty name="CheckPerAccountReportingEntitlements"/>
<ffi:removeProperty name="AccountEntitlementFilterTask"/>

<script>
	$("#goBackSummaryLink").find("span").addClass("dashboardSelectedItem");
</script>

<ffi:cinclude value1="${accountsList.size}" value2="0" operator="equals">
	<ffi:removeProperty name="TransactionSearch"/>
</ffi:cinclude>