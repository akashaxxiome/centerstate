<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.util.FilteredList"%>
<%@ page import="com.ffusion.beans.banking.Transaction"%>

<div class='calendarData'>
<s:include value="/pages/jsp/account/inc/accounthistory-search-init.jsp"/>

<ffi:help id="account_transactionDetail" />
<%
String isCalendarView = (String)request.getParameter("isCalendar");
if(isCalendarView != null && isCalendarView.equals("true"))  {
	session.setAttribute("tranIndex", request.getParameter("tranIndex"));
} else {
	String tranIndex = (String)request.getParameter("tranIndex");
	FilteredList transactions = (FilteredList)session.getAttribute("Transactions"); 
	if (transactions != null && transactions.size() > 0) {
		java.util.Iterator transactionIterator = transactions.iterator();
		int index = 0;
		while ( transactionIterator.hasNext() )
		{
			Transaction transaction = (Transaction)transactionIterator.next();
			index = index + 1;
			if(transaction.containsKey("accIndex")) {
				String trnIndex = (String)transaction.get("accIndex");
				if(trnIndex != null && trnIndex.equals(tranIndex)) {
					session.setAttribute("tranIndex", String.valueOf(index));
					break;
				}
			}
			
		}
		
		if(session.getAttribute("tranIndex") == null){
 			session.setAttribute("tranIndex", String.valueOf(index));
		 
		}
	}
} 
%>
<s:set var="isCalendar" value="#parameters.isCalendar"/>
<ffi:list collection="Transactions" items="Transaction" startIndex="${tranIndex}" endIndex="${tranIndex}">
<%-- <div class="confirmationDetails">
	<span>Reference Number</span>
	<span><ffi:getProperty name="Transaction" property="ReferenceNumber"/></span>
</div> --%>
<div class="transactionDetailHolderCls">
<div class="blockWrapper" >
	<div class="blockHead"><s:text name="jsp.account_241" /><!-- Account Summary --></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width:100%">
				<%--Transfer From Account Section --%>
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_21"/>:</span>
				<span  class="sectionsubhead valueCls">
					<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
					<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" 
						value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY %>"/>
					<s:set var="AccountDisplayTextTaskBean" value="#session.Account" scope="request" />
					<ffi:process name="AccountDisplayTextTask"/>
					<ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
				</span>
			</div>
		</div>
	</div>
	
	<div class="blockHead"><s:text name="jsp.account_242" /><!-- Transaction Summary --></div>
	<div class="blockContent label130">
		<div class="blockRow">
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel "><s:text name="jsp.account_244"/>:</span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="ReferenceNumber"/></span>
			</div>
			<div class="inlineBlock">		
				<span class="sectionLabel"><s:text name="jsp.default_143"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="Date"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock">		
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_62"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="CustomerReferenceNumber"/></span>
			</div>
			<div class="inlineBlock">		
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_216"/></span>
				<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />				
				<ffi:setProperty name="TransactionTypes" property="TypeCode" value="${Transaction.TypeValue}" />
				<ffi:process name="TransactionTypes" />
				<span class="sectionsubhead valueCls">
				<ffi:getProperty name="TransactionTypes" property="TypeCode" />

					  <ffi:getProperty name="TransactionTypes" property="Description" /></span>
					 <ffi:removeProperty name="TransactionTypes" />
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock">		
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_38"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="BankReferenceNumber"/></span>	
			</div>
			<div class="inlineBlock">
				<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
					<ffi:object name="com.ffusion.tasks.banking.GetBAITypeCodeInfo" id="GetBAITypeCodeInfo" scope="request"/>
					<ffi:setProperty name="GetBAITypeCodeInfo" property="TypeCode" value="${Transaction.SubType}"/>
					<ffi:process name="GetBAITypeCodeInfo"/>
				</ffi:cinclude>		
				<span class="sectionLabel"><s:text name="jsp.account_194"/></span>
				
				<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
				<span class="sectionsubhead valueCls">
					<ffi:getProperty name="Transaction" property="SubType"/> - <ffi:getProperty name="GetBAITypeCodeInfo" property="Description"/>
				</span>
				<ffi:removeProperty name="GetBAITypeCodeInfo"/>
				</ffi:cinclude>
			</div>
		</div>		
		<div class="blockRow">
			<div class="inlineBlock">		
				<span class="sectionLabel"><s:text name="jsp.account_212"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="TrackingID"/></span>
			</div>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
			<div class="inlineBlock">		
				<span class="sectionLabel"><s:text name="jsp.account_valueDate"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="ValueDate"/></span>
			</div>
			</ffi:cinclude>
		</div>
	</div>
	
	
	<div class="blockHead"><s:text name="jsp.account_243" /><!-- Miscellaneous --></div>
	<div class="blockContent label130">
		<div class="blockRow">
			<div class="inlineBlock" style="width:100%;">		
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_170"/>:</span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="Description"/></span>
			</div>
		</div>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<div class="blockRow">
				<div class="inlineBlock" style="width:100%">
					<span class="sectionsubhead sectionLabel" style="width:180px !important;"><s:text name="jsp.account_168"/></span>
					<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="PONum"/></span>
				</div>
			</div>
		</ffi:cinclude>
		<div class="blockRow">
			<div class="inlineBlock">		
				<span class="sectionsubhead sectionLabel">
					<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.DEPOSIT_ACCT ) %>" operator="equals">
					<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
						<s:text name="jsp.account_23"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
						<s:text name="jsp.account_22"/>
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.ASSET_ACCT ) %>" operator="equals">
					<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
						<s:text name="jsp.account_23"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
						<s:text name="jsp.account_22"/>
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.CREDIT_CARD_ACCT ) %>" operator="equals">
					<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
						<s:text name="jsp.account_21"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
						<s:text name="jsp.account_25"/>
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.LOAN_ACCT ) %>" operator="equals">
					<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
						<s:text name="jsp.account_20"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
						<s:text name="jsp.account_25"/>
					</ffi:cinclude>
				</ffi:cinclude>
				</span>
				<span class="sectionsubhead valueCls">
				<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
					<ffi:getProperty name="Transaction" property="DebitValue"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
					<ffi:getProperty name="Transaction" property="CreditValue"/>
				</ffi:cinclude>
				</span>
			</div>
			<div class="inlineBlock">		
				<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.DEPOSIT_ACCT ) %>" operator="equals">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_35"/></span>
				</ffi:cinclude>
				<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.ASSET_ACCT ) %>" operator="equals">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_223"/></span>
				</ffi:cinclude>
				<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.CREDIT_CARD_ACCT ) %>" operator="equals">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_35"/></span>
				</ffi:cinclude>
				<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.LOAN_ACCT ) %>" operator="equals">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_156"/></span>
				</ffi:cinclude>
				
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="RunningBalance"/></span>
			</div>
		</div>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<div class="blockRow">
				<div class="inlineBlock">		
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_167"/></span>
					<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="ProcessingDate"/></span>
				</div>
				<div class="inlineBlock">		
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_165"/></span>
					<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="PostingDate"/></span>
				</div>
			</div>
		</ffi:cinclude>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<div class="blockRow">
				<div class="inlineBlock">		
					<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_316"/></span>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_164"/></span>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
						<span  class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="PayeePayor"/></span>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
						<span  class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="PayeePayor"/></span>
					</ffi:cinclude>
				</div>
				<div class="inlineBlock">		
					<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_158"/></span>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_162"/></span>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
						<span  class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="PayorNum"/></span>
					</ffi:cinclude>
					<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
						<span  class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="PayorNum"/></span>
					</ffi:cinclude>
				</div>
			</div>
		</ffi:cinclude>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
			<div class="blockRow">
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_79"/></span>
					<span  class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="DueDate"/></span>
				</div>
				<div class="inlineBlock">				
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_154"/></span>	
					<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="OrigUser"/></span>		
				</div>
			</div>
		</ffi:cinclude>
		<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
		<div class="blockRow">
			<div class="inlineBlock" style="width:100%;">		
				<span class="sectionsubhead sectionLabel" style="width:200px !important;"><s:text name="jsp.account_24"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="ImmediateAvailAmount"/></span>
			</div>
		</div>	
		<div class="blockRow">
			<div class="inlineBlock" style="width:100%;">		
				<span class="sectionsubhead sectionLabel" style="width:180px !important;"><s:text name="jsp.account_19"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="OneDayAvailAmount"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width:100%;">		
				<span class="sectionsubhead sectionLabel" style="width:245px !important;"><s:text name="jsp.account_18"/></span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="MoreThanOneDayAvailAmount"/></span>
			</div>
		</div>
		</ffi:cinclude>	
		<div class="blockRow">	
			<div class="inlineBlock" style="width:100%;">		
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/>:</span>
				<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="Memo"/></span>
			</div>
		</div>
	</div>
	<s:if test="%{!#isCalendar}">
	<div class="marginTop30 ffivisible">&nbsp;</div>
	<div align="center" class="ui-widget-header customDialogFooter" style="background-color:#fff !important;">
		<form method="post">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<sj:a id="printTransactionDetail"
					  button="true" 
					  onClick="ns.common.printPopup();" cssClass="hiddenWhilePrinting"><s:text name="jsp.default_331"/></sj:a>
					  
		<sj:a id="doneFormButton"
		               button="true"
					onClickTopics="closeDialog"  cssClass="hiddenWhilePrinting" 
				><s:text name="jsp.default_538"/></sj:a>
		</form>
	</div>
	</s:if>
	<s:else>
		<input type="hidden" id="isRendered" value='<s:property value="#parameters.isCalendar"/>'/>
	</s:else>
</div>
</div>
</ffi:list>


<%-- <div align="left">
	<ffi:list collection="Transactions" items="Transaction" startIndex="${tranIndex}" endIndex="${tranIndex}">
			<table class="lightBackground" width=100% border="0" cellspacing="0" cellpadding="0">
				<tr><br></tr>

			<tr>
				<td>
					<table cellspacing="0">
						<tr valign="baseline"  class="blockRow">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_21"/></span>
							</td>
							<td>
								<span class="columndata">
									<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
									<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat" 
										value="<%=com.ffusion.beans.accounts.Account.TRANSACTION_DETAIL_DISPLAY %>"/>
									<s:set var="AccountDisplayTextTaskBean" value="#session.Account" scope="request" />
									<ffi:process name="AccountDisplayTextTask"/>
									<ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
								</span>
							</td>
						</tr>
						<tr valign="baseline">
							<td>
								<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
								<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />
								<ffi:setProperty name="TransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CORPORATE %>" />
								<ffi:process name="TransactionTypes" />
								</ffi:cinclude>
								
								<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
								<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="TransactionTypes" scope="request" />
								<ffi:setProperty name="TransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CONSUMER %>" />
								<ffi:process name="TransactionTypes" />
								</ffi:cinclude>

								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_216"/></span>
							</td>
							<td>
								<span class="columndata"><ffi:setProperty name="TransactionTypes" property="TypeCode" value="${Transaction.TypeValue}" />
								<ffi:getProperty name="TransactionTypes" property="Description" /></span>
								<ffi:removeProperty name="TransactionTypes" />
							</td>
						</tr>
						
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_38"/></span>
							</td>
							<td>
								<span class="columndata"><ffi:getProperty name="Transaction" property="BankReferenceNumber"/></span>
							</td>
						</tr>
						
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_143"/></span>	
							</td>
							<td>
								<span class="columndata"><ffi:getProperty name="Transaction" property="Date"/></span>
							</td>
						</tr>
						
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_212"/></span>
							</td>
							<td>
								<span class="columndata"><ffi:getProperty name="Transaction" property="TrackingID"/></span>
							</td>
						</tr>
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_165"/></span>
							</td>
							<td>
								<span class="columndata"><ffi:getProperty name="Transaction" property="PostingDate"/></span>
							</td>
						</tr>
						</ffi:cinclude>
						
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_79"/></span>
							</td>
							<td>
								<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
									<span class="columndata"><ffi:getProperty name="Transaction" property="DueDate"/></span>
								</ffi:cinclude>
							</td>
						</tr>
						
						<tr valign="baseline">
							<td>
								<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_316"/></span>
								</ffi:cinclude>
								<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_164"/></span>
								</ffi:cinclude>
							</td>
							<td>
								<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
									<span class="columndata"><ffi:getProperty name="Transaction" property="PayeePayor"/></span>
								</ffi:cinclude>
								<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
									<span class="columndata"><ffi:getProperty name="Transaction" property="PayeePayor"/></span>
								</ffi:cinclude>
							</td>
						</tr>
					</table>
				</td>
				<td>
					<table cellspacing="0" >
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_176"/></span>
							</td>
							<td>
								<span class="columndata"><ffi:getProperty name="Transaction" property="ReferenceNumber"/></span>
							</td>
						</tr>
						<tr valign="baseline">
							<td>
								<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
									<ffi:object name="com.ffusion.tasks.banking.GetBAITypeCodeInfo" id="GetBAITypeCodeInfo" scope="request"/>
									<ffi:setProperty name="GetBAITypeCodeInfo" property="TypeCode" value="${Transaction.SubType}"/>
									<ffi:process name="GetBAITypeCodeInfo"/>
								</ffi:cinclude>
								
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_194"/></span>
							</td>
							<td>
								<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
								<span class="columndata">
									<ffi:getProperty name="Transaction" property="SubType"/> - <ffi:getProperty name="GetBAITypeCodeInfo" property="Description"/>
								</span>
								<ffi:removeProperty name="GetBAITypeCodeInfo"/>
								</ffi:cinclude>
							</td>
						</tr>
						
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_62"/></span>
							</td>
							<td>
								<span class="columndata"><ffi:getProperty name="Transaction" property="CustomerReferenceNumber"/></span>
							</td>
						</tr>
						
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel">
									<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.DEPOSIT_ACCT ) %>" operator="equals">
										<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
											<s:text name="jsp.account_23"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
											<s:text name="jsp.account_22"/>
										</ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.ASSET_ACCT ) %>" operator="equals">
										<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
											<s:text name="jsp.account_23"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
											<s:text name="jsp.account_22"/>
										</ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.CREDIT_CARD_ACCT ) %>" operator="equals">
										<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
											<s:text name="jsp.account_21"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
											<s:text name="jsp.account_25"/>
										</ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.LOAN_ACCT ) %>" operator="equals">
										<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
											<s:text name="jsp.account_20"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
											<s:text name="jsp.account_25"/>
										</ffi:cinclude>
									</ffi:cinclude>
								</span>
							</td>
							<td>
								<span class="columndata">
									<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
										<ffi:getProperty name="Transaction" property="DebitValue"/>
									</ffi:cinclude>
									<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
										<ffi:getProperty name="Transaction" property="CreditValue"/>
									</ffi:cinclude>
								</span>
							</td>
						</tr>
						
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_168"/></span>
							</td>
							<td>
								<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
									<span class="columndata"><ffi:getProperty name="Transaction" property="PONum"/></span>
								</ffi:cinclude>
							</td>
						</tr>
						
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
						<tr valign="baseline">
							<td>
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_167"/></span>
							</td>
							<td>
								<span class="columndata"><ffi:getProperty name="Transaction" property="ProcessingDate"/></span>
							</td>
						</tr>
						</ffi:cinclude>
						
						<tr valign="baseline">
							<td>
								<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.DEPOSIT_ACCT ) %>" operator="equals">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_35"/></span>
								</ffi:cinclude>
								<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.ASSET_ACCT ) %>" operator="equals">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_223"/></span>
								</ffi:cinclude>
								<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.CREDIT_CARD_ACCT ) %>" operator="equals">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_35"/></span>
								</ffi:cinclude>
								<ffi:cinclude value1="${Account.AccountGroup}" value2="<%= String.valueOf( com.ffusion.beans.accounts.AccountGroups.LOAN_ACCT ) %>" operator="equals">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_156"/></span>
								</ffi:cinclude>
							</td>
							<td>
								<span class="columndata"><ffi:getProperty name="Transaction" property="RunningBalance"/></span>
							</td>
						</tr>
						
						<tr valign="baseline">
							<td>
								<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
									<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
										<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_158"/></span>
									</ffi:cinclude>
								</ffi:cinclude>
								
								<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
									<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
										<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_162"/></span>
									</ffi:cinclude>
								</ffi:cinclude>
							</td>
							<td>
								<ffi:cinclude value1="${Transaction.DebitNoSymbol}" value2="" operator="notEquals">
									<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
										<span class="columndata"><ffi:getProperty name="Transaction" property="PayorNum"/></span>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${Transaction.CreditNoSymbol}" value2="" operator="notEquals">
									<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
										<span class="columndata"><ffi:getProperty name="Transaction" property="PayorNum"/></span>
									</ffi:cinclude>
								</ffi:cinclude>
							</td>
						</tr>
						
					</table>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<table cellspacing="0">
						<tr valign="baseline">
							<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
								<td>
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_154"/></span>
								</td>
								<td>
									<span class="columndata"><ffi:getProperty name="Transaction" property="OrigUser"/></span>
								</td>							
							</ffi:cinclude>
						</tr>
						
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
						<tr valign="baseline">
								<td>
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_24"/></span>
								</td>
								<td>
									<span class="columndata"><ffi:getProperty name="Transaction" property="ImmediateAvailAmount"/></span>
								</td>							
						</tr>
						
						<tr valign="baseline">
								<td>
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_19"/></span>
								</td>
								<td>
									<span class="columndata"><ffi:getProperty name="Transaction" property="OneDayAvailAmount"/></span>
								</td>							
						</tr>
						
						<tr valign="baseline">
								<td>
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_18"/></span>
								</td>
								<td>
									<span class="columndata"><ffi:getProperty name="Transaction" property="MoreThanOneDayAvailAmount"/></span>
								</td>							
						</tr>
						</ffi:cinclude>
						
						<tr></tr>
						
						<tr valign="baseline">
								<td colspan="2">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_279"/></span>
								</td>
						</tr>
						<tr valign="baseline">
								<td colspan="2" height="25">
									<ffi:getProperty name="Transaction" property="Memo"/>
								</td>							
						</tr>
						<tr valign="baseline">
								<td colspan="2">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_170"/></span>
								</td>
						</tr>
						<tr valign="baseline">
								<td colspan="2" height="75">
									<ffi:getProperty name="Transaction" property="Description"/>
								</td>							
						</tr>
					</table>
				</td>
			</tr>
			<tr>
					<td colspan="2" align="center">
						<form method="post">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<sj:a id="printTransactionDetail"
					  button="true" 
					  onClick="ns.common.printPopup();"
					><s:text name="jsp.default_331"/></sj:a>&nbsp;&nbsp;
							<sj:a id="doneFormButton"
						                button="true"
						                buttonIcon="ui-icon-refresh"
										onClickTopics="closeDialog"
									><s:text name="jsp.default_538"/></sj:a>
						</form>
					</td>
				</tr>

			</table>
		</ffi:list>
	</div> --%>
</div>