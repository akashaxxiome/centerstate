<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<s:include value="/pages/jsp/account/inc/accounthistory-search-init.jsp"/>

<ffi:help id="account_inquiryHistory" />
<%
	String tranIndexSTR = request.getParameter("tranIndex");
	session.setAttribute("tranIndex", request.getParameter("tranIndex"));
%>

<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="session"/>
<ffi:process name="GetCurrentDate"/>

<div align="center" class="approvalDialogHt2">

<%-- Build up the comment string for the inquiry message we are sending --%>
<%	StringBuffer sb = new StringBuffer("Transaction Info: "); %>
			<ffi:removeProperty name="GetQueue" />
			<ffi:removeProperty name="Queue" />

			 <div class="blockWrapper">
				<div class="blockHead"><s:text name="jsp.account_241" /><!-- Account Summary --></div>
				<div class="blockContent label130">
					<div class="blockRow">
						<div class="inlineBlock" style="width:49%">
							<!-- Transfer From Account Section -->
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_21"/>:</span>
							<span class="sectionsubhead valueCls">
								<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="request"/>
								<s:set var="AccountDisplayTextTaskBean" value="#session.Account" scope="request" />
								<ffi:setProperty name="AccountDisplayTextTask" property="ConsumerAccountDisplayFormat"
									value="<%=com.ffusion.beans.accounts.Account.INQUIRY_SCREEN_DISPLAY%>"/>
								<ffi:process name="AccountDisplayTextTask"/>
								<ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
							</span>
						</div>
						<div class="inlineBlock">		
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_143"/></span>
							<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="Date" /></span>
						</div>
					</div>
					<div class="blockRow">
						<ffi:setProperty name="StringUtil" property="Value2" value="---" />
						<ffi:setProperty name="FloatMath" property="Value2" value="0" />
						<div class="inlineBlock" style="width:49%">
							<ffi:setProperty name="StringUtil" property="Value1" value="${Account.AvailableBalance}" />
							<ffi:setProperty name="FloatMath" property="Value1" value="${Account.AvailableBalance.AmountValue.AmountValue}" />
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_32"/></span>
							<span class="sectionsubhead valueCls"><ffi:getProperty name="StringUtil" property="NotEmpty"/></span>
						</div>
						<div class="inlineBlock">		
							<ffi:setProperty name="StringUtil" property="Value1" value="${Account.CurrentBalance}" />
							<ffi:setProperty name="FloatMath" property="Value1" value="${Account.CurrentBalance.AmountValue.AmountValue}" />
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_61"/></span>
							<span class="sectionsubhead valueCls"><ffi:getProperty name="StringUtil" property="NotEmpty"/></span>
						</div>
					</div>
					<div class="blockRow">
						<div class="inlineBlock" style="width:49%">
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_142"/></span>
							<span class="sectionsubhead valueCls">-</span>
						</div>
						<div class="inlineBlock">		
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_45"/>:</span>
							<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="Amount" /></span>
						</div>
					</div>
					<div class="blockRow">
						<div class="inlineBlock" style="width:49%">
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_description"/>:</span>
							<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="Description" /></span>
						</div>
						<div class="inlineBlock">		
							<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_178"/>:</span>
							<span class="sectionsubhead valueCls"><ffi:getProperty name="Transaction" property="ReferenceNumber" /></span>
						</div>
					</div>
				</div>
				<div class="marginTop20"><s:text name="jsp.default.inquiry.message.RTE.label" /><span class="required">*</span></div>
			</div>	 
			<table width="600" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata_grey" colspan="5">
						 <s:form id="sendMessageForm" method="post" action="/pages/jsp/account/sendAccountHistoryMessageAction_sendAccountHistoryInquiry.action" theme="simple" style="text-align: left;">
							<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<input type="hidden" name="accountHistoryMessage.subject" value="<ffi:getProperty name='Subject'/>"/>	
							<input type="hidden" name="accountHistoryMessage.memo" value="<ffi:getProperty name='Memo'/>"/>	
							<s:hidden name="accountHistoryMessage.from" value="%{#session.User.id}"></s:hidden>
							<div align="left" style="width:760px;">
								<p>
									<textarea class="txtbox ui-widget-content ui-corner-all" name="Memo" id="Memo" rows="10" cols="75" wrap="virtual" style="overflow-y: auto;"></textarea>
								</p>
							</div>
							<br/><br/>
							<div align="center" class="ui-widget-header customDialogFooter">
								<sj:a
									id="cancelFormButton1"
									button="true"
									onClickTopics="closeDialog"
								><s:text name="jsp.default_83"/></sj:a>
								<sj:a
									id="sendInquiry"
									targets="resultmessage"
									formIds="sendMessageForm"
									validate="true"
									button="true"
									validateFunction="customValidation"
									onSuccessTopics="sendMessageFormSuccess"
								><s:text name="jsp.account_188"/></sj:a>
							</div>
														
						</s:form> 
						 
					</td>
				</tr>

			</table>
		</div>
		<div class="marginBottom20">&nbsp;</div>
<ffi:removeProperty name="Transaction" />

<ffi:setProperty name="BackURL" value="inquirehistory.jsp?tranIndex=${tranIndex}" URLEncrypt="true"/>
<script>
		$(document).ready(function(){
			setTimeout(function(){ns.common.renderRichTextEditor('Memo','600',true);}, 600);
		});
	</script>
