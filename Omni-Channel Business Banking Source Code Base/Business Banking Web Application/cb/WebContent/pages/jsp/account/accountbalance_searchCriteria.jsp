<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.accountconfig.beans.AccountBalanceSearchCriteria" %>
<% 
	session.setAttribute("AccountDisplayCurrencyCode",request.getParameter("AccountDisplayCurrencyCode"));
	AccountBalanceSearchCriteria accountBalanceSearchCriteria = (AccountBalanceSearchCriteria) session.getAttribute("accountBalanceSearchCriteria");
	session.setAttribute("AccountBalancesDataClassification",accountBalanceSearchCriteria.getDataClassification());
%>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="false"/>
</ffi:cinclude>

<%-- ADDED BY TCG FOR CHECKIMAGING --%>

<%-- Flag to check whether to retrieve image or not --%>
<ffi:setProperty name="get-image-flag" value="true"/>

<%-- INITIALIZING TRANSACTION SEARCH IMAGE TASK --%>
<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="SearchImage" scope="session"/>
<ffi:setProperty name="SearchImage" property="Init" value="true"/>
<ffi:process name="SearchImage"/>
<ffi:setProperty name="SearchImage" property="SuccessURL" value="${SecurePath}account/checkimage.jsp" />

<ffi:cinclude value1="${GetDisplaySummariesForAccount}" value2="" operator="equals">
<ffi:object name="com.ffusion.tasks.banking.GetDisplaySummariesForAccount" id="GetDisplaySummariesForAccount" scope="session"/>
</ffi:cinclude>
<ffi:setProperty name="GetDisplaySummariesForAccount" property="DisplayCurrency"  value="${AccountDisplayCurrencyCode}"/>

<script type="text/javascript">

$(function(){
	 $("#AccountDisplayCurrencyCode").selectmenu({width: 200});
	 $("#reportOn").selectmenu({width: 140});
	 
	 $("#reloadViewButton").button();
   });

	function changeAccountDisplayCurrency() {
		// prepare json with div id & corresponding grid ID
		var gridIdsArray ={"depositAccounts":"consolidatedBalanceDepositSummaryID",
							"assetAccounts":"consolidatedBalanceAssetSummaryID",
		                   "creditCardsAccounts":"consolidatedBalanceCcardSummaryID",
		                   "loanAccounts":"consolidatedBalanceLoanSummaryID",
		                   "otherAccounts":"consolidatedBalanceOtherSummaryID"};
		var selectedAccountId = "";
		// text of selected account
		var activeAccount = $.trim($('.selectedTabLbl').html());
		$('.gridTabDropdownItem').each(function(i){
			 var html = $.trim($(this).html());
			if(html==activeAccount){
				selectedAccountId = $(this).attr('id');
			}
		});
		var activeGridId = gridIdsArray[selectedAccountId];
		var AccountDisplayCurrencyCode = $('#AccountDisplayCurrencyCode').val();
		var changeAccountDisplayCurrencyString="true";
		var ConsolidatedBalanceDataClassificationString = $('#reportOn').val();
		for (var key in gridIdsArray) {
			if(gridIdsArray.hasOwnProperty(key)){
				$('#'+gridIdsArray[key]).jqGrid('setGridParam', {postData: {
					changeAccountDisplayCurrency: changeAccountDisplayCurrencyString,
					"dataClassification": ConsolidatedBalanceDataClassificationString,
					"displayCurrencyCode": AccountDisplayCurrencyCode, 
				}});
			}
		}
		if(ns.account.displayType == "donut"){
			changeAccountSummaryPortletView('donut',selectedAccountId);
		}else if(ns.account.displayType == "bar"){
			changeAccountSummaryPortletView('bar',selectedAccountId);
		}else{
			var urlString = $('#'+activeGridId).jqGrid('getGridParam', 'url');
			$('#'+activeGridId).jqGrid('setGridParam', {url: urlString}).trigger("reloadGrid");
		}
	}
	 
	function reloadView(){
		var accountbalanceURL = "/cb/pages/jsp/account/AccountBalanceAction.action";
		$.ajax({
			url: accountbalanceURL,	
			type: "POST",
			data: $("#CriteriaFormID").serialize(),
			success: function(data){
				$('#accountBalanceSummary').html(data);
				$.publish("common.topics.tabifyDesktop");					
				$("#reloadViewButton").focus();
			}
		});
	}

</script>


<ffi:setProperty name="subMenuSelected" value="account balance"/>

<%-- The specify criteria page sets variables and removes the variables set (and also the properties --%>
<%-- specified by those variables --%>
<ffi:setProperty name="SubmitURL" value="/pages/jsp/account/accountbalance.jsp"/>
<ffi:setProperty name="flag" value="accountbalance"/>
<ffi:setProperty name="DataClassificationVariable" value="${AccountBalancesDataClassification}"/>

<ffi:setProperty name="DataType" value="Summary"/>
<ffi:setProperty name="AccountsCollectionName" value="BankingAccounts"/>
 
<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>
 
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="notEquals">
	<script language="JavaScript" type="text/javascript">
		ns.account.accountBalanceDashbordflag=true;
		
	</script> 
</ffi:cinclude>

<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="equals">
<div id="accBalRealDashBordDivID" class="acntDashboard_itemHolderContent">
	<script language="JavaScript" type="text/javascript">
		ns.account.accountBalanceDashbordflag=false;
		
	</script> 
	<%-- we must determine the default data classification based on whether the user can see intra or previous day for any accounts--%>
	<ffi:cinclude value1="${AccountBalancesDataClassification}" value2="" operator="equals">
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
			<ffi:setProperty name="AccountBalancesDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="false" operator="equals">
			<ffi:setProperty name="AccountBalancesDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${AccountBalancesDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" operator="equals">
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="notEquals">
			<ffi:setProperty name="AccountBalancesDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${AccountBalancesDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" operator="equals">
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="notEquals">
			<ffi:setProperty name="AccountBalancesDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>

	<ffi:setProperty name="PortalFlag" value="false"/>

	<ffi:cinclude value1="true" value2="${showselectcurrency}" operator="equals" >
	<s:form name="ChangeAccountDisplayCurrencyForm"  method="post" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<div class="acntDashboard_masterItemHolder">
		
		<!-- Show Accounts in holder -->
		<div class="acntDashboard_itemHolder">
			<span class="sectionsubhead dashboardLabelMargin" id="showAccountInLbl"><s:text name="jsp.account_189"/>&nbsp;</span>
			<select id="AccountDisplayCurrencyCode" name="accountBalanceSearchCriteria.currency" class="txtbox">
               <option <ffi:cinclude value1='' value2='${accountBalanceSearchCriteria.Currency}' operator='equals'> selected </ffi:cinclude>  value='' >
			<s:text name="jsp.default_127"/>
				</option>
				<ffi:list collection="FXCurrencies" items="currency">
				    <option <ffi:cinclude value1='${currency.CurrencyCode}' value2='${AccountDisplayCurrencyCode}' operator='equals'> selected </ffi:cinclude>  value='<ffi:getProperty name="currency" property="CurrencyCode"/>' >
				    <ffi:getProperty name="currency" property="CurrencyCode"/>-<ffi:getProperty name="currency" property="Description"/>
				    
				</option>
			    </ffi:list>
			</select>
			<br><span id="currencyError"></span>
		</div>
	</div>
	</s:form>
	</ffi:cinclude>
	<%-- specifycriterianodate.jsp contents Begins. --%>
	<% 
	   String DataClassification = "";  String DateString= ""; String AccountDisplayCurrencyCodeString= "";
	   session.setAttribute("DataClassification", session.getAttribute("DataClassificationVariable"));
	%>
	<ffi:getProperty name="AccountDisplayCurrencyCode" assignTo="AccountDisplayCurrencyCodeString" encodeAssign="true"/>
	<%-- If this is the first visit to the page, --%>
	<ffi:cinclude value1="${DataClassification}" value2="" operator="equals">
		<%-- we set DataClassification to its default value - Previous Day --%>
		<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
		<%-- UNLESS we are doing this for Controlled Disubursement, where we would like default Intra Day --%>
		<ffi:cinclude value1="${DataClassificationVariable}" value2="DisbursementDataClassification" operator="equals">
			<%-- but first we'll check if we are even entitled to Intra day! --%>
			<ffi:setProperty name="intraEntitled" value="false"/>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
				<ffi:setProperty name="intraEntitled" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
				<ffi:setProperty name="intraEntitled" value="true"/>
			</ffi:cinclude>
			<%-- and if we are, we will set the default to Intra Day --%>
			<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
		        	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:setProperty name="${DataClassificationVariable}" value="${DataClassification}"/>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="notEquals">
		<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
		<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
		<ffi:cinclude value1="" value2="${AccountGroupID}" operator="notEquals">
			<ffi:setProperty name="BankingAccounts" property="Filter" value="ACCOUNTGROUP=${AccountGroupID}" />
		</ffi:cinclude>
		<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="${AccountsCollectionName}"/>
		<ffi:process name="CheckPerAccountReportingEntitlements"/>
	</ffi:cinclude>
	    <s:form method="post" id="CriteriaFormID" name="CriteriaForm" theme="simple">
	    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	    <input type="hidden" name="AccountDisplayCurrencyCode" value="<%= AccountDisplayCurrencyCodeString==null?"":AccountDisplayCurrencyCodeString %>"/>
		<div id="reportOnDivID">
		
			<!-- Right side elements holder -->
			<div class="acntDashboard_masterItemHolder marginleft5">
				
				<!-- Report On item holder -->
				<div class="acntDashboard_itemHolder">
					<span class="sectionsubhead dashboardLabelMargin" id="reportOnLbl"><s:text name="jsp.default_354"/></span>
					<select class="txtbox" id="reportOn" name="accountBalanceSearchCriteria.dataClassification">
						<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="equals">								
					<%-- we must determine which data classification the user is entitled to. AccountsCollectionName will be blank for
				       	     Lockbox and Disbursement. --%>
					     	<ffi:removeProperty name="previousEntitled"/>
					     	<ffi:removeProperty name="intraEntitled"/>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
							<ffi:setProperty name="previousEntitled" value="true"/>
						</ffi:cinclude>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
							<ffi:setProperty name="previousEntitled" value="true"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${previousEntitled}" value2="true" operator="equals">
							<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${accountBalanceSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
						</ffi:cinclude>

						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
							<ffi:setProperty name="intraEntitled" value="true"/>
						</ffi:cinclude>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
							<ffi:setProperty name="intraEntitled" value="true"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
							<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${accountBalanceSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
						</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude  value1="${AccountsCollectionName}" value2="" operator="notEquals">
				 		<%-- we must determine which data classifications to place in this drop down, based on the entiltment checks on the accounts collection AND the data type of this information --%>
						<ffi:cinclude value1="${DataType}" value2="Summary" operator="equals">
							<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
								<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${accountBalanceSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
							</ffi:cinclude>
							<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
								<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${accountBalanceSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${DataType}" value2="Detail" operator="equals">
							<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
								<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${accountBalanceSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
							</ffi:cinclude>
							<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
								<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${accountBalanceSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
							</ffi:cinclude>
						</ffi:cinclude>
						<ffi:cinclude value1="${DataType}" value2="" operator="equals">
							<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryPrev}" value2="true" operator="equals">
								<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${accountBalanceSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
							</ffi:cinclude>
							<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryIntra}" value2="true" operator="equals">
								<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${accountBalanceSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
							</ffi:cinclude>
						</ffi:cinclude>
						</ffi:cinclude> <%-- End if accounts collection name is empty --%>
					</select>
					<br><span id="dataClassificationError"></span>
				</div>
				
				<!-- View button holder -->
				<div class="acntDashboard_itemHolder">
					
					<sj:a id="reloadViewButton"								
						button="true"
						onclick="changeAccountDisplayCurrency()"
						cssStyle="">
						<s:text name="jsp.default_459"/>
					</sj:a>
				</div>
			</div>
	        </s:form>
		</div>
		<ul id="formerrors"></ul>
</ffi:cinclude>



<script type="text/javascript">

$(function(){
	 $("#AccountDisplayCurrencyCode").selectmenu({width: 200});
	 $("#reportOn").selectmenu({width: 140});
	 
	 $("#reloadViewButton").button();
   });
   </script>
