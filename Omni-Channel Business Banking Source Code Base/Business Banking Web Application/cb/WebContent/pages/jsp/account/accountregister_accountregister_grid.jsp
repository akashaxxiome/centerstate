<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<div id="accountregister_gridID">
<ffi:help id="account_register-wait" />

<ffi:setGridURL grid="GRID_accountRegister" name="EditURL" url="/cb/pages/jsp/account/register-rec-edit-wait.jsp?SetRegisterTransactionId={0}&CollectionSessionName={1}" parm0="RegisterId" parm1="collectionType"/>
<ffi:setGridURL grid="GRID_accountRegister" name="DeleteURL" url="/cb/pages/jsp/account/register-rec-delete.jsp?SetRegisterTransactionId={0}&CollectionSessionName={1}" parm0="RegisterId" parm1="collectionType"/>
													
	
<ffi:setProperty name="accountRegisterSummaryTempURL" value="/pages/jsp/account/GetRegisterTransactionsAction.action?refrenceNum=${refrenceNum}&fromDate=${fromDate}&toDate=${toDate}&showMemoOrNot=${showMemoOrNot}&GridURLs=GRID_accountRegister" URLEncrypt="true"/>
<s:url id="accountRegisterSummaryURL" value="%{#session.accountRegisterSummaryTempURL}" escapeAmp="false"/>
<sjg:grid
     id="accountRegisterSummaryID"
     sortable="true" 
     dataType="json" 
     href="%{accountRegisterSummaryURL}"
     pager="true"
     gridModel="gridModel"
	 rowList="%{#session.StdGridRowList}" 
	 rowNum="%{#session.StdGridRowNum}" 
	 rownumbers="false"
	 navigator="true"
	 navigatorAdd="false"
	 navigatorDelete="false"
	 navigatorEdit="false"
	 navigatorRefresh="false"
	 navigatorSearch="false"
	 navigatorView="false"
	 shrinkToFit="true"
	 footerrow="true"
	 scroll="false"
	 scrollrows="true"
     onGridCompleteTopics="addGridControlsEvents,accountRegisterSummaryEvent"
     >
     
     <sjg:gridColumn name="registerTypeName" index="registerTypeName" title="%{getText('jsp.default_444')}" sortable="true" width="80" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="referenceNumber" index="referenceNumber" title="%{getText('jsp.account_177')}" sortable="true" width="90"  cssClass="datagrid_numberColumn"/>
     <sjg:gridColumn name="dateIssued" index="dateIssued" title="%{getText('jsp.account_66')}" sortable="true" formatter="ns.account.dateIssuedFormatter" width="60" cssClass="datagrid_dateColumn"/>
     <sjg:gridColumn name="payeeName" index="payeeName" title="%{getText('jsp.account_50')}" sortable="true" formatter="ns.account.payeeNameCategoriesFormatter" width="150" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="typeValue" index="typeValue" title="%{getText('jsp.account_101')}" formatter="ns.account.imageFormatter" width="60"  sortable="false" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="status" index="status" title="%{getText('jsp.account_174')}" width="180" formatter="ns.account.reconciledFormatter"  sortable="false" cssClass="datagrid_textColumn"/> 
     <sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValue.currencyStringNoSymbol" title="%{getText('jsp.default_43')}" sortable="true" formatter="ns.account.amountsFormatter" width="150" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="registerId" index="registerId" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.account.accountRegisterActionFormatter" width="50" cssClass="datagrid_textColumn"/>   
	 <sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.default_233')}" hidden="true" cssClass="datagrid_textColumn"/>	 
     <sjg:gridColumn name="map.registerStatusType" index="map.registerStatusType" title="%{getText('jsp.account_133')}"  hidden="true" hidedlg="true"/>    
     <sjg:gridColumn name="map.collectionType" index="map.collectionType" title="%{getText('jsp.account_123')}"  hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.currencyCode" index="map.currencyCode" title="%{getText('jsp.account_125')}"  hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.accountBalance" index="map.accountBalance" title="%{getText('jsp.account_120')}"  hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.sessRegBalance" index="map.sessRegBalance" title="%{getText('jsp.account_134')}"  hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.dateClearedAlia" index="map.dateClearedAlia" title="%{getText('jsp.account_126')}"  hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.categoryName" index="map.categoryName" title="%{getText('jsp.account_122')}"  hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.showMemoOrNot" index="map.showMemoOrNot" title="%{getText('jsp.account_135')}"  hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="memo" index="memo" title="%{getText('jsp.account_141')}"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="statusValue" index="statusValue" title="%{getText('jsp.account_192')}"  hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="amountValue.amountValue" index="amountValue.amountValue" title="%{getText('jsp.account_28')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="map.currencyAmountValue" index="map.currencyAmountValue" title="%{getText('jsp.account_124')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
	 <sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
</sjg:grid>

</div>
<script>					
		$('#accountregister_gridID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#accountregister_gridID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});

		$('#accountregister_gridID').portlet('title', js_acc_register_portlet_title);

</script>
<style>
	#accountregister_gridID .ui-jqgrid-ftable td{
		border:none;
	}
</style>
<script>
	$("#accountRegisterSummaryID").jqGrid('setColProp','registerId',{title:false});
</script>
