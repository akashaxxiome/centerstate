<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript" src="<s:url value='/web/js/account/accountRegister_grid%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/account/account%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/account/account_grid%{#session.minVersion}.js'/>"></script>
<div id="desktop" align="center">
   <div id="navigatorButton">
      <s:include value="/pages/jsp/account/accountregister_navigaterButton.jsp"/>
   </div>
   <div id="operationresult">
		<div id="resultmessage"><s:text name="jsp.default_498"/></div>
   </div><!-- result -->
   <br/>
   <div id="accountRegisterDashbord">
      <s:include value="/pages/jsp/account/register-wait.jsp"/>
   </div>
</div>
<sj:dialog id="deletePayeeDialogID" cssClass="acctMgmtDialog" title="%{getText('jsp.account_74')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="750">
</sj:dialog>
<sj:dialog id="deleteCategoryDialogID" cssClass="acctMgmtDialog" title="%{getText('jsp.account_73')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="750">
</sj:dialog>
<sj:dialog id="deleteRecordDialogID" cssClass="acctMgmtDialog" title="%{getText('jsp.account_75')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="750">
</sj:dialog>