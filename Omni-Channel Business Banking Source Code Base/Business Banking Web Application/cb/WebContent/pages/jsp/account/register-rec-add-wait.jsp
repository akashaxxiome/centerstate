<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:cinclude value1="${register_init_touched}" value2="true" operator="notEquals" >
	<s:include value="/pages/jsp/inc/init/register-init.jsp" />
</ffi:cinclude>
<ffi:object name="com.ffusion.tasks.register.SetRegisterTransaction" id="SetRegisterTransaction" scope="session"/>
<ffi:object name="com.ffusion.tasks.register.AddRegisterTransactionCB" id="AddRegisterTransaction" scope="session"/>


<ffi:setProperty name="AddCatBackURL" value="/pages/jsp/account/register-rec-add-wait.jsp"/>

<ffi:setProperty name="RegisterCategories" property="Filter" value="All" />
<ffi:setProperty name="SetRegisterTransaction" property="Id" value="" />
<ffi:setProperty name="AddRegisterTransaction" property="TotalAmount" value="" />

<ffi:object id="SetRegisterPayee" name="com.ffusion.tasks.register.SetRegisterPayee" scope="session"/>
	<ffi:setProperty name="SetRegisterPayee" property="Id" value="" />
<ffi:process name="SetRegisterPayee"/>

<ffi:process name="SetRegisterTransaction"/>

<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
	<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
	<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
<ffi:process name="CopyCollection"/>

<ffi:setProperty name="NewPayee" value=""/>
<ffi:setProperty name="ExistingPayee" value=""/>
<ffi:setProperty name="NewChecked" value="false"/>
<ffi:setProperty name="whichCat" value=""/>

<s:include value="/pages/jsp/account/register-rec-add.jsp"/>
