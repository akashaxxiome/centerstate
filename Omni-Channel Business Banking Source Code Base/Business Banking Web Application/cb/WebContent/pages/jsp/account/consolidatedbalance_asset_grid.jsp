<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<% 
   String dataClassification = request.getParameter("classfication");
   String displayCurrencyCode = request.getParameter("displayCurrencyCode");
   request.setAttribute("dataClassification",dataClassification);
   request.setAttribute("displayCurrencyCode",displayCurrencyCode);
%>
<div id="consolidatedBalance_asset_gridID" >
<ffi:help id="accountsbygroup" />
<%-- <ffi:setGridURL grid="GRID_consolidatedvalanceAsset" name="LinkURL" url="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=2&SetAccount.ID={0}&SetAccount.BankID={1}&SetAccount.RoutingNum={2}&DataClassification={3}&redirection=true&ProcessAccount=true&TransactionSearch=false" parm0="Account.ID" parm1="Account.BankID" parm2="Account.RoutingNum" parm3="DataClassification"/> --%> 
<ffi:setGridURL grid="GRID_consolidatedvalanceAsset" name="LinkURL" url="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=2&AccountID={0}&AccountBankID={1}&AccountRoutingNum={2}&DataClassification={3}&redirection=true&ProcessAccount=true&TransactionSearch=false" parm0="Account.ID" parm1="Account.BankID" parm2="Account.RoutingNum" parm3="DataClassification"/>
<ffi:setProperty name="consolidatedBalanceAssetSummaryTempURL" value="/pages/jsp/account/GetConsolidateBalanceAssetsAccountAction.action?dataClassification=${dataClassification}&displayCurrencyCode=${displayCurrencyCode}&GridURLs=GRID_consolidatedvalanceAsset" URLEncrypt="true"/>
<s:url id="assetSummaryURL" value="%{#session.consolidatedBalanceAssetSummaryTempURL}" escapeAmp="false"/>

<sjg:grid
     id="consolidatedBalanceAssetSummaryID"
	 sortable="true" 
     dataType="json" 
     href="%{assetSummaryURL}"
     pager="true"
     gridModel="gridModel"
	 rowList="%{#session.StdGridRowList}" 
	 rowNum="%{#session.StdGridRowNum}" 
	 rownumbers="false"
	 navigator="true"
	 navigatorAdd="false"
	 navigatorDelete="false"
	 navigatorEdit="false"
	 navigatorRefresh="false"
	 navigatorSearch="false"
	 navigatorView="false"
	 shrinkToFit="true"
	 footerrow="true"
	 userDataOnFooter="true"
	 scroll="false"
	 scrollrows="true"
	 viewrecords="true"
     onGridCompleteTopics="addGridControlsEvents,consolidatedBalanceAssetSummaryEvent"
     >
     <sjg:gridColumn name="account.accountDisplayText" index="NUMBER" title="%{getText('jsp.default_15')}" sortable="true" width="150" cssClass="datagrid_actionColumn" formatter="ns.account.formatAccountColumn"/>
     <sjg:gridColumn name="nameNickNameasset" index="ACCNICKNAME" title="%{getText('jsp.default_293')}" sortable="true" width="280" formatter="ns.account.customToAccountNickNameColumn" /> 
     <sjg:gridColumn name="account.bankName" index="BANKNAME" title="%{getText('jsp.default_61')}" sortable="true" width="95" align="left" formatter="ns.account.nameFormatter"/>
     <sjg:gridColumn name="account.type" index="TYPESTRING" title="%{getText('jsp.default_444')}" sortable="true" width="65" align="left"/>
     <sjg:gridColumn name="summaryDateDisplay" index="SummaryDateDisplay" title="%{getText('jsp.default_51')}" sortable="true"  width="60" align="right" />
     <sjg:gridColumn name="bookValue" index="bookValueCurrency" title="%{getText('jsp.default_76')}" sortable="true" width="150" align="right" formatter="ns.account.currentLedgerFormatter"/>
     <sjg:gridColumn name="marketValue" index="marketValueCurrency" title="%{getText('jsp.default_271')}" sortable="true" width="150" align="right" formatter="ns.account.currentLedgerFormatter"/>
     <sjg:gridColumn name="account.displayText" index="displayText" title="%{getText('jsp.account_77')}" sortable="true" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="account.nickName" index="NICKNAME" title="%{getText('jsp.account_146')}" sortable="true" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="account.currencyCode" index="CURRENCYCODE" title="%{getText('jsp.account_60')}" sortable="true" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="account.ID" index="ID" title="%{getText('jsp.account_12')}" sortable="false" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="account.bankID" index="BANKID" title="%{getText('jsp.account_39')}" sortable="false" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="account.routingNum" index="ROUTINGNUM" title="%{getText('jsp.account_183')}" sortable="false" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="displayCurrencyCode" index="map.displayCurrencyCode" title="%{getText('jsp.default_173')}" sortable="true" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="dataClassification" index="map.dataClassification" title="%{getText('jsp.account_64')}" sortable="false" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="bookValueTotal" index="map.bookValueTotal" title="%{getText('jsp.account_41')}" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="marketValueTotal" index="map.marketValueTotal" title="%{getText('jsp.account_136')}" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.LinkURL" index="LinkURL" title="%{getText('jsp.default_262')}" sortable="true" width="60" hidden="true" hidedlg="true"/>
</sjg:grid>

</div>
<script>	
					
		/* $('#consolidatedBalance_asset_gridID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#consolidatedBalance_asset_gridID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});

		$('#consolidatedBalance_asset_gridID').portlet('title', js_acc_assets_portlet_title); */

</script>
<style>
	#consolidatedBalance_asset_gridID .ui-jqgrid-ftable td{
		border:none;
	}
</style>
