<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript">

ns.account.redirectReportingURL="<ffi:urlEncrypt url='/cb/pages/jsp/account/register-reports.jsp'/>";
ns.account.rederectAddEditPayeeURL="<ffi:urlEncrypt url='/cb/pages/jsp/account/register-payees.jsp'/>";
ns.account.rederectAddEditCategoriesURL="<ffi:urlEncrypt url='/cb/pages/jsp/account/register-categories.jsp'/>";
ns.account.rederectAddRecordURL="<ffi:urlEncrypt url='/cb/pages/jsp/account/register-rec-add-wait.jsp'/>";

   ns.account.redirectReporting = function(urlString){
	   //var urlString = "/cb/pages/jsp/account/register-reports.jsp";
		   $.ajax({
				url: urlString,	
				success: function(data){
				$('#accountRegisterDashbord').html(data);
			}
			});
	   }
   ns.account.rederectAddEditPayee = function(urlString){
	  //var urlString = "/cb/pages/jsp/account/register-payees.jsp";
		   $.ajax({
				url: urlString,	
				success: function(data){
				$('#accountRegisterDashbord').html(data);
			}
			});
	   }
   ns.account.rederectAddEditCategories = function(urlString){
	   //var urlString = "/cb/pages/jsp/account/register-categories.jsp";
		   $.ajax({
				url: urlString,	
				success: function(data){
				$('#accountRegisterDashbord').html(data);
			}
			});
	   }
   ns.account.rederectAddRecord = function(urlString){
	   //var urlString = "/cb/pages/jsp/account/register-rec-add-wait.jsp";
		   $.ajax({
				url: urlString,	
				success: function(data){
				$('#accountRegisterDashbord').html(data);
			}
			});
	   }
   
</script>
<sj:a id="report" button="true" onclick="ns.account.redirectReporting(ns.account.redirectReportingURL)"><s:text name="jsp.default_355"/></sj:a>
<sj:a id="addeditpayee" button="true" onclick="ns.account.rederectAddEditPayee(ns.account.rederectAddEditPayeeURL)"><s:text name="jsp.account_16"/></sj:a>
<sj:a id="addeditcategories" button="true" onclick="ns.account.rederectAddEditCategories(ns.account.rederectAddEditCategoriesURL)"><s:text name="jsp.account_15"/></sj:a>
<sj:a id="addrecord" button="true" onclick="ns.account.rederectAddRecord(ns.account.rederectAddRecordURL)"><s:text name="jsp.account_13"/></sj:a>