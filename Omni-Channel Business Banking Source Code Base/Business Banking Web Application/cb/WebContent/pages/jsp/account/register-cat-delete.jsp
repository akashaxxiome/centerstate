<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%
   session.setAttribute("SetRegisterCategoryId", request.getParameter("SetRegisterCategoryId"));
%>
<ffi:object id="SetRegisterCategory" name="com.ffusion.tasks.register.SetRegisterCategory" scope="session"/>
	<ffi:setProperty name="SetRegisterCategory" property="Id" value="${SetRegisterCategoryId}" />
<ffi:process name="SetRegisterCategory"/>
<script type="text/javascript">
   $(function(){
	   $("#NewCategoryId").selectmenu({width: 240});
	   });
</script>
<div align=center>
<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
  <TR>
    <TD></TD>
    <TD></TD>
    <TD></TD>
  </TR>
  <TR>
    <TD></TD>
    <TD align=center>
    	<s:form id="deleteCategoryForm" namespace="/pages/jsp/account" action="DeleteRegisterCategoryAction" method="post" name="deleteform" theme="simple">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
        <TABLE cellSpacing=3 cellPadding=0 width=96% border=0>
				<tr>
					<td class="sectionhead tbrd_b" colspan="6" height="20">&gt; <s:text name="jsp.account_72"/></td>
				</tr>
				<tr>
					<td class="columndata"><s:text name="jsp.account_40"/><br></td>
				</tr>
				<tr>
					<td class="columndata"><ffi:getL10NString rsrcFile='cb' msgKey="jsp/account/register-cat-delete-1" parm0="${RegisterCategory.Name}" /></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td><img src="/cb/web/multilang/grafx/spacer.gif" width="50" height="1" border="0">
						<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
					    	<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
							<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
						<ffi:process name="CopyCollection"/>
						<% String originalFilter = ""; %>
						<ffi:getProperty name="RegisterCategories" property="Filter" assignTo="originalFilter"/>
						<select id="NewCategoryId" class="txtbox" name="NewCategoryId">
							<option value="0"><s:text name="jsp.default_445"/></option>
							<ffi:setProperty name="RegisterCategories" property="Filter" value="ParentCategory=-1,AND,Id!${RegisterCategory.Id},AND,ID!0" />
							<ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />
							<ffi:list collection="RegisterCategories" items="Category">
								<option value="<ffi:getProperty name="Category" property="Id"/>"><ffi:getProperty name="Category" property="Name"/></option>
								<ffi:setProperty name="RegisterCategoriesCopy" property="Filter" value="ParentCategory=${Category.Id},AND,Id!${RegisterCategory.Id},AND,ID!0" />
								<ffi:setProperty name="RegisterCategoriesCopy" property="SortedBy" value="NAME" />
								<ffi:list collection="RegisterCategoriesCopy" items="Category1">
									<option value="<ffi:getProperty name="Category1" property="Id"/>">&nbsp;&nbsp;<ffi:getProperty name="Category" property="Name"/>: <ffi:getProperty name="Category1" property="Name"/></option>
								</ffi:list>
							</ffi:list>
						</select>
						<ffi:setProperty name="RegisterCategories" property="Filter" value="<%=originalFilter%>" />
					</td>
			</tr>
			<tr>
				<td></td>
			</tr>
		  <tr>
		    <TD class=columndata colSpan=6 align=center>
		                                       <sj:a id="cancelDeleteCategoryFormButton" 
											    button="true" 
												onClickTopics="closeDialog"
											  ><s:text name="jsp.default_82"/></sj:a>
							                 &nbsp;&nbsp;
							                  <sj:a id="deleteCategoryButton" 
											        formIds="deleteCategoryForm" 
											        targets="deleteCategoryresultmessage" 
											        button="true"   
													onSuccessTopics="deleteCategorySuccessTopics" 
													effectDuration="1500"
													><s:text name="jsp.default_162"/></sj:a>
		     </TD>
		   </tr>
			</table>
			</s:form>
  	</TD>
    <TD><BR><BR></TD>
  </TR>
  <TR>
    <TD></TD>
    <TD></TD>
    <TD></TD>
  </TR>
</TABLE>
</div>
<%--Initial the delete Action for following task --%>
<ffi:object id="ReassignTransactionsCategory" name="com.ffusion.tasks.register.ReassignTransactionsCategory" scope="session"/>
	<ffi:setProperty name="ReassignTransactionsCategory" property="NewCategoryId" value="${NewCategoryId}" />
	<ffi:setProperty name="ReassignTransactionsCategory" property="OldCategoryId" value="${RegisterCategory.Id}" />
    <%
		session.setAttribute("FFIReassignTransactionsCategory", session.getAttribute("ReassignTransactionsCategory"));
	%>
<ffi:object id="DeleteRegisterCategory" name="com.ffusion.tasks.register.DeleteRegisterCategory" scope="session"/>
	<ffi:setProperty name="DeleteRegisterCategory" property="Id" value="${RegisterCategory.Id}" />
    <%
		session.setAttribute("FFIDeleteRegisterCategory", session.getAttribute("DeleteRegisterCategory"));
	%>
<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
	<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
	<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
	 <%
		session.setAttribute("FFICopyCollection", session.getAttribute("CopyCollection"));
	 %>