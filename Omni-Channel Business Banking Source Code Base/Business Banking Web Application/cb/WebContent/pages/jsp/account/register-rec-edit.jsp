<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%
    session.setAttribute("NewChecked",request.getParameter("checkbox1"));
    String whichCat = request.getParameter("whichCat");
    if(whichCat!=null){
    	 session.setAttribute("whichCat",whichCat);
    }
    String RegisterTransactionType = request.getParameter("RegisterTransaction.Type");
    if(RegisterTransactionType!=null){
    %>
    <ffi:setProperty name="RegisterTransaction" property="RegisterType" value="<%=RegisterTransactionType%>"/>
    <ffi:setProperty name="RegisterTransaction" property="Type" value="<%=RegisterTransactionType %>"/>
<%}
    String RegisterTransactionDateIssued = request.getParameter("RegisterTransaction.DateIssued");
    if(RegisterTransactionDateIssued!=null){
    	%>
    	<ffi:setProperty name="RegisterTransaction" property="DateIssued" value="<%=RegisterTransactionDateIssued%>"/>
   <%
    }
    String TotalAmount = request.getParameter("EditRegisterTransaction.TotalAmount");
    if(TotalAmount!=null){
    	%>
    	<ffi:setProperty name="RegisterTransaction" property="AmountValue.CurrencyStringNoSymbolNoComma" value="<%=TotalAmount%>"/>
   <%
    }
    String refrenceNum = request.getParameter("RegisterTransaction.ReferenceNumber");
    if(refrenceNum!=null){
    	%>
    	<ffi:setProperty name="RegisterTransaction" property="ReferenceNumber" value="<%=refrenceNum%>"/>
    	<%
    }String memo = request.getParameter("RegisterTransaction.Memo");
    if(memo!=null){
    	%>
    	<ffi:setProperty name="RegisterTransaction" property="Memo" value="<%=memo%>"/>
    	<%
    }
    String NewPayee = request.getParameter("NewPayee");
    if(NewPayee!=null){
    	%>
    	<ffi:setProperty name="RegisterTransaction" property="PayeeName" value="<%=NewPayee%>"/>
   <%
   }
    String exitPayee = request.getParameter("ExistingPayee");
    if(exitPayee!=null){
   %>
   <ffi:setProperty name="RegisterTransaction" property="PayeeName" value="<%=exitPayee%>"/>
   <%} %>
<script type="text/javascript" language="javascript">
<ffi:setProperty name="SubCatElementNumbertrue" value="14"/>
<ffi:setProperty name="SubCatElementNumberfalse" value="10"/>

<ffi:object name="com.ffusion.beans.util.IntegerMath" id="TranStatus"/>
	<ffi:setProperty name="TranStatus" property="Value1" value="${RegisterTransaction.Status}"/>
	<ffi:setProperty name="TranStatus" property="Value2" value="1"/>

elementNum=<ffi:getProperty name="SubCatElementNumber${TranStatus.Less}"/> // the element number of the subcategory select box

function preselect(selectedIdx) {
<%-- <!--
// This function will automatically select the Register Payee's default Category:Subcategory when the payee is selected.
-->--%>
	selectedPayee=document.EditTransaction.elements[elementNum-3].options[selectedIdx].value;
	payees=new Array();
<ffi:setProperty name="RegisterPayees" property="Filter" value="All"/>
<ffi:list collection="RegisterPayees" items="RegisterPayee1">
	payees[payees.length]=new Array(["<ffi:getProperty name='RegisterPayee1' property='Name'/>"],["<ffi:getProperty name='RegisterPayee1' property='DefaultCategory'/>"]);
</ffi:list>
	catToSelect=0;
	for (i=0; i<payees.length; i++) {
		if (selectedPayee==payees[i][0]) {
			catToSelect=payees[i][1];
		}
	}
	for (i=0; i<document.EditTransaction.elements[elementNum].length; i++) {
		if (document.EditTransaction.elements[elementNum].options[i].value==catToSelect) {
			document.EditTransaction.elements[elementNum].selectedIndex=i;
		}
	}
}

function setAction(action) {
<%-- <!--
// This function changes the action of the form, depending on if we are adding a new category / subcategory, switching to
// multiple category mode, or actually saving the form changes.  (Which would run the EditRegisterTransaction task.)
-->--%>
	document.EditTransaction['RegisterPayee.DefaultCategory'].value=document.EditTransaction.elements[elementNum].options[document.EditTransaction.elements[elementNum].selectedIndex].value;
	var urlString = action;
	   $.ajax({
	  		  type: "post",
	  		  url: urlString,
	  		  data: $('#EditTransaction').serialize(),
	  		  success: function(data) {
	  			$('#accountRegisterDashbord').html(data);
	  		  }
	  	});
}

function setPayeeName() {
	if (document.EditTransaction.checkbox1.checked == true) {
		document.EditTransaction["RegisterPayee.Name"].value = document.EditTransaction["RegisterTransaction.PayeeName"].value;
	}
}

function clearPayee(which) {
	// This function clears either the existing Payee or New Payee field, depending on which one is being changed.
	// It ALSO copies the value of whichever box we are using into the hidden form value for the name we are
	// submitting.
	if (which == "NewPayee") {
		document.EditTransaction.checkbox1.checked=false;
		document.EditTransaction["RegisterTransaction.PayeeName"].value = document.EditTransaction.ExistingPayee.options[document.EditTransaction.ExistingPayee.selectedIndex].value;
		document.EditTransaction.NewPayee.value = "";
	} else {
		document.EditTransaction["RegisterTransaction.PayeeName"].value = document.EditTransaction.NewPayee.value;
	//	document.EditTransaction.ExistingPayee.selectedIndex = 0;
		$("#ExistingPayee").selectmenu("value","Select Payee or Payor");
	}
	setPayeeName();
	flipCheck();
}

function flipCheck() {
	if (document.EditTransaction.checkbox1.checked == true) {
		document.EditTransaction.NewChecked.value = "true";
	} else {
		document.EditTransaction.NewChecked.value = "false";
	}
}

function setPayee() {
	if (document.EditTransaction.ExistingPayee.selectedIndex != 0) {
		document.EditTransaction.NewPayee.value = "";
	}
}

window.onload=setPayee


CSStopExecution=false;
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
	var result;
	for (var i=0;i<array.length;i++) {
		if(CSStopExecution) return false;
		var aa = fct[array[i]];
		if (aa == null) return false;
		var ta = new Array;
		for(var j=1;j<aa.length;j++) {
			if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
				if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
				else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
				else ta[j]=aa[j];}
			} else ta[j]=aa[j];
		}
		result=aa[0](ta);
	}
	return result;
}
CSAct = new Object;
function CSClickReturn () {
	var bAgent = window.navigator.userAgent;
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
}


CSAct = new Object;
function CSOpenWindow(action, param) {
	var wf = "";
	wf = wf + "width=" + action[3];
	wf = wf + ",height=" + action[4];
	wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
	wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
	wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
	wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
	wf = wf + ",directories=" + (action[9] ? "yes" : "no");
	wf = wf + ",location=" + (action[10] ? "yes" : "no");
	wf = wf + ",status=" + (action[11] ? "yes" : "no");
	if (param == null) {
		window.open(action[1], action[2], wf);
	} else {
		window.open(action[1] + "?" + param, action[2], wf);
	}
}

CSAct[/*CMP*/ 'B9E611E8165'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=back&calForm=EditTransaction&calTarget=RegisterTransaction.DateIssued"/>','',350,300,false,false,false,false,false,false,false);
	$(function(){
		$("#RegisterTransactionType").selectmenu({width: 130});
	 	$("#ExistingPayee").selectmenu({width: 180});
	 	setPayee();
	    });
	ns.account.editmuiltiButtionSubmit = function(whichCat){
    	document.EditTransaction.action="/cb/pages/jsp/account/register-rec-edit.jsp?whichCat="+whichCat;
        }
	ns.account.TypefieldSelectOnchange = function(){
        var urlString = "/cb/pages/jsp/account/register-rec-edit.jsp";
        $.ajax({
  		  type: "post",
  		  url: urlString,
  		  data: $('#EditTransaction').serialize(),
  		  success: function(data) {
  			$('#accountRegisterDashbord').html(data);
  		  }
  	});
        }
    // whent the input text is disabled , it can not be submitted
    ns.account.fillEditTotalAmount = function(){
        var a = $("#EditRegisterTransactionTotalAmount").val();
        $("#editmultiTotalAmount").val(a);
        }

</script>
<div id="register_rec_editID" align=center>
<ffi:help id="account_register-rec-edit-wait" />

<ffi:object name="com.ffusion.beans.util.FloatMath" id="TranDebit"/>
	<ffi:setProperty name="TranDebit" property="Value1" value="${RegisterTransaction.DecimalAmount}"/>
 	<ffi:setProperty name="TranDebit" property="Value2" value="0"/>

	<%--We will set the respect debit type flag only value to false, that way we must explicitly set the
	value to true if we are dealing with a reconciled transaction whose amount type (debit/credit) should
	NOT be determined by the type of the transaction.  --%>
	<ffi:setProperty name="EditRegisterTransactions" property="RespectDebitTypeFlagOnly" value="false"/>
<%-- <!-- If EditRegisterTransaction.TotalAmount is 0, set TotalAmount equal to RegisterTransaction.DecimalAbsoluteAmount -->--%>
<ffi:cinclude value1="0" value2="${EditRegisterTransaction.TotalAmount}">
	<ffi:setProperty name="EditRegisterTransaction" property="TotalAmount" value="${RegisterTransaction.DecimalAbsoluteAmount}"/>
	<ffi:setProperty name="EditRegisterTransaction" property="TotalAmountCurrency" value="${RegisterTransaction.AmountValue.CurrencyCode}"/>
<%-- <!-- For reconciled transactions (mainly transfers), set debit flag -->--%>
	<ffi:cinclude value1="${TranStatus.GreaterEquals}" value2="true">
		<ffi:setProperty name="EditRegisterTransactions" property="RespectDebitTypeFlagOnly" value="true"/>
	 	<ffi:setProperty name="Compare" property="Value1" value="${TranDebit.Less}"/>
	 	<ffi:setProperty name="Compare" property="Value2" value="true"/>
	 	<ffi:cinclude value1="${Compare.Equals}" value2="true">
	 		<ffi:setProperty name="ValueType" value="Debit"/>
	 		<ffi:setProperty name="EditRegisterTransaction" property="DebitType" value="true"/>
	 	</ffi:cinclude>
	 	<ffi:cinclude value1="${Compare.Equals}" value2="false">
	 		<ffi:setProperty name="ValueType" value="Credit"/>
	 		<ffi:setProperty name="EditRegisterTransaction" property="DebitType" value="false"/>
	 	</ffi:cinclude>
	 </ffi:cinclude>
</ffi:cinclude>

<%-- <!--
We set this property so that when we add a category or subcategory from this page, it shows that we are
adding or editing a record on the top of the page.
-->--%>
<ffi:setProperty name="RecordType" value="Edit"/>
<ffi:setProperty name="RecButtonType" value="edit"/>

<s:form id="EditTransaction" name="EditTransaction" method="post" action="EditRegisterTransactionAction"  namespace="/pages/jsp/account" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="obsolete"/>
<input type="hidden" name="RegisterTransaction.PayeeName" value="<ffi:getProperty name="RegisterTransaction" property="PayeeName"/>">
<input type="hidden" name="RegisterPayee.Name" value="<ffi:getProperty name="RegisterPayee" property="Name"/>">
<input type="hidden" name="RegisterPayee.DefaultCategory" value="<ffi:getProperty name="RegisterPayee" property="DefaultCategory"/>">
<input type="hidden" name="NewChecked" value="<ffi:getProperty name="NewChecked"/>">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center" class="sectionhead_grey">
		<table border="0" width="96%">
			<tr>

			</tr>
			<tr>
				<td colspan="4" class="sectionhead" height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="sectionhead" colspan="4" height="20" align="left"><s:text name="jsp.account_7"/><span class="mainfont"><ffi:getProperty name="Account" property="Number"/>
					<ffi:cinclude value1="${Account.NickName}" value2="" operator="notEquals" >
						 - <ffi:getProperty name="Account" property="NickName"/>
			 		</ffi:cinclude>
					 - <ffi:getProperty name="Account" property="CurrencyCode"/></span>
				</td>
			</tr>
			<tr>
				<td class="sectionhead" align="left"><ffi:cinclude value1="${TranStatus.Less}" value2="true"></ffi:cinclude><s:text name="jsp.default_444"/><span class="required">*</span></td>
				<td class="sectionhead"><s:text name="jsp.default_347"/></td>
				<td class="sectionhead"><ffi:cinclude value1="${TranStatus.Less}" value2="true"></ffi:cinclude><s:text name="jsp.account_110"/><span class="required">*</span></td>
				<td class="sectionhead"><s:text name="jsp.account_70"/></td>
				<td class="sectionhead"><ffi:cinclude value1="${TranStatus.Less}" value2="true"></ffi:cinclude><s:text name="jsp.default_43"/><span class="required">*</span></td>
			</tr>
			<tr>
				<td align="left">
					<%-- <!-- Unreconciled transactions (status=0) can edit type -->--%>
					<ffi:cinclude value1="${TranStatus.Less}" value2="true">
						<ffi:setProperty name="RegisterTransactionTypes" property="SortedBy" value="TYPE_NAME"/>
						<select id="RegisterTransactionType" class="txtbox" name="RegisterTransaction.Type" onChange="ns.account.TypefieldSelectOnchange()">
				    	    <ffi:setProperty name="Compare" property="Value1" value="${RegisterTransaction.RegisterType}"/>
				            <ffi:list collection="RegisterTransactionTypes" items="Type">
						        <ffi:setProperty name="Compare" property="Value2" value="${Type.TYPE_ID}"/>
						 		<option value="<ffi:getProperty name='Type' property='TYPE_ID'/>" <ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="Type" property="TYPE_NAME"/> (<ffi:getProperty name="Type" property="TYPE_ABBR"/>)</option>
				  	        </ffi:list>
					    </select>
					</ffi:cinclude>

					<%-- <!-- Reconciled transactions (status>=1) can't edit type -->--%>
					<ffi:cinclude value1="${TranStatus.GreaterEquals}" value2="true">
						<span class="mainfont"><ffi:getProperty name="RegisterTransaction" property="RegisterTypeName"/> (<ffi:getProperty name="RegisterTransaction" property="RegisterTypeAbbr"/>)</span>
					</ffi:cinclude>
				</td>
				<td>
					<%-- <!-- Unreconciled transactions (status=0) can edit reference number -->--%>
					<ffi:cinclude value1="${TranStatus.Less}" value2="true">
						<input class="ui-widget ui-widget-content ui-corner-all" type="text" size="10" maxlength="20" name="RegisterTransaction.ReferenceNumber" value="<ffi:getProperty name="RegisterTransaction" property="ReferenceNumber"/>"><span id="RegisterTransaction.ReferenceNumberError"></span>
					</ffi:cinclude>

					<%-- <!-- Reconciled transactions (status>=1) can't edit reference number -->--%>
					<ffi:cinclude value1="${TranStatus.GreaterEquals}" value2="true">
						<span class="mainfont"><ffi:getProperty name="RegisterTransaction" property="ReferenceNumber"/></span>
					</ffi:cinclude>
				</td>
				<td class="columndata">
					<ffi:setProperty name="RegisterTransaction" property="DateFormat" value="${UserLocale.DateFormat}"/>

					<%-- <!-- Unreconciled transactions (status=0) can edit issue date -->--%>
					<ffi:cinclude value1="${TranStatus.Less}" value2="true">
						<sj:datepicker value="%{#session.RegisterTransaction.DateIssued}" name="RegisterTransaction.DateIssued" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/><span id="RegisterTransaction.DateIssuedError"></span>
					</ffi:cinclude>

					<%-- <!-- Reconciled transactions (status>=1) can't edit issue date -->--%>
					<ffi:cinclude value1="${TranStatus.GreaterEquals}" value2="true">
						<span class="mainfont"><ffi:getProperty name="RegisterTransaction" property="DateIssued"/></span>
					</ffi:cinclude>
				</td>
				<td class="mainfont">
					<% com.ffusion.beans.register.RegisterTransaction regTran = (com.ffusion.beans.register.RegisterTransaction)session.getAttribute( "RegisterTransaction" ); %>
					<% if ( com.ffusion.beans.util.RegisterUtil.isTransactionCreditType( regTran.getRegisterTypeValue() ) ) { %>
						<s:text name="jsp.default_120"/>
					<% } else {%>
						<s:text name="jsp.default_146"/>
					<% } %>
 				</td>
				<td class="columndata">
					<%-- <!-- Unreconciled transactions (status=0) can edit amount -->--%>
					<ffi:cinclude value1="${TranStatus.Less}" value2="true">
						<% String totalAmt=null; %>
						<ffi:getProperty name="RegisterTransaction" property="AmountValue.CurrencyStringNoSymbolNoComma" assignTo="totalAmt"/>
						<%
						if( totalAmt==null || totalAmt.length()<=0 ) {
						%>
							<ffi:getProperty name="EditRegisterTransaction" property="TotalAmount" assignTo="totalAmt"/>
						<%
						}
						%>
						<input class="ui-widget ui-widget-content ui-corner-all" id="EditRegisterTransactionTotalAmount" type="text" maxlength="53" name="EditRegisterTransaction.TotalAmount" value="<%= totalAmt %>" <ffi:cinclude value1="${whichCat}" value2="multi"> disabled </ffi:cinclude>>
						<span id="EditRegisterTransaction.TotalAmountError"></span>
						<%--when it is disabled , it can not be submitted to the action  --%>
					    <input type="hidden" id="editmultiTotalAmount" name="editmultiTotalAmount"/>
					</ffi:cinclude>

					<%-- <!-- Reconciled transactions (status>=1) can't edit amount -->--%>
					<ffi:cinclude value1="${TranStatus.GreaterEquals}" value2="true">
						<ffi:object name="com.ffusion.beans.common.Currency" id="formattedAmount" scope="session"/>
						<ffi:setProperty name="formattedAmount" property="Amount" value="${EditRegisterTransaction.TotalAmount}"/>
						<span class="mainfont"><ffi:getProperty name="formattedAmount" property="String"/> (<ffi:getProperty name="ValueType" />)</span>
					</ffi:cinclude>
				</td>
			</tr>
			<tr>
				<td colspan="4" class="sectionhead" align="left"><s:text name="jsp.default_279"/></td>
			</tr>
			<tr>
				<td colspan="4" class="columndata" align="left"><input class="ui-widget ui-widget-content ui-corner-all" type="text" size="75" maxlength="255" name="RegisterTransaction.Memo" value="<ffi:getProperty name="RegisterTransaction" property="Memo"/>"><span id="RegisterTransaction.MemoError"></span></td>
			</tr>
			<tr>
				<td class="sectionhead" align="left"><s:text name="jsp.account_83"/></td>
				<td class="sectionhead" colspan="3" align="left"><s:text name="jsp.account_145"/></td>
			</tr>
			<tr>
				<td class="columndata" align="left">
				<ffi:setProperty name="Compare" property="Value1" value="${RegisterTransaction.PayeeName}"/>
					<select id="ExistingPayee" class="txtbox" name="ExistingPayee" onChange="clearPayee('NewPayee');preselect(this.selectedIndex);">
						<option value=""><s:text name="jsp.account_187"/></option>
						<ffi:setProperty name="RegisterPayees" property="Filter" value="All"/>
						<ffi:setProperty name="RegisterPayees" property="SortedBy" value="NAME"/>
						<ffi:list collection="RegisterPayees" items="RegisterPayee1">
						<ffi:setProperty name="Compare" property="Value2" value="${RegisterPayee1.Name}"/>
						<option value="<ffi:getProperty name="RegisterPayee1" property="Name"/>" <ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="RegisterPayee1" property="Name"/></option>
						</ffi:list>
					</select>
				</td>
				<td class="columndata" colspan="3" align="left">
					<input type="text" class="ui-widget ui-widget-content ui-corner-all" size="30" maxlength="40" name="NewPayee" onChange="clearPayee('ExistingPayee');" value="<ffi:getProperty name="RegisterTransaction" property="PayeeName"/>"><span id="NewPayeeError"></span>
				<ffi:setProperty name="checkedtrue" value="checked"/>
				<ffi:setProperty name="checkedfalse" value=""/>
				<ffi:setProperty name="Compare" property="Value1" value="${NewChecked}"/>
				<ffi:setProperty name="Compare" property="Value2" value="checked"/>
				<input type="checkbox" name="checkbox1" onClick="setPayeeName();flipCheck()" value="true" <ffi:getProperty name="checked${NewChecked}"/>> <s:text name="jsp.account_14"/></td>
			</tr>

			<%-- <!--
			Here, we're including a file depending on whether they are using a single category or multiple
			categories.  On the wait page, we need to set whichCat to single so it will be includede when they
			are adding a new record.  If, for some reason whichCat doesn't get set, I have it defaulting here to
			"single".
			-->--%>
			<tr>
				<td colspan="4"><s:include value="/pages/jsp/account/inc/register-rec-edit-%{#session.whichCat}.jsp" /></td>
			</tr>
			<tr>
				<td class="columndata" valign="top" colspan="4" height="20" align="left">

				<ffi:cinclude value1="${whichCat}" value2="multi">
				      <sj:a
                       formIds="EditTransaction"
                       button="true"
                       targets="accountRegisterDashbord"
                       onclick="ns.account.editmuiltiButtionSubmit('single#cat')"
                       ><s:text name="jsp.account_221"/></sj:a>
				</ffi:cinclude>
				<ffi:cinclude value1="${whichCat}" value2="single">
				    <sj:a
                       formIds="EditTransaction"
                       button="true"
                       targets="accountRegisterDashbord"
                       onclick="ns.account.editmuiltiButtionSubmit('multi#cat')"
                       ><s:text name="jsp.account_220"/></sj:a>
				</ffi:cinclude>
				</td>
			</tr>
						<tr>
				<td class="sectionhead" colspan="4" align="center" height="20">&nbsp;</td>
			</tr>

			<tr>
				<td colspan="4"><br></td>
			</tr>
			<tr>
				<td align="center" colspan="4"><span class="required">* <s:text name="jsp.default_240"/></span></td>
			</tr>
			<tr>
				<td colspan="4"><br></td>
			</tr>
			<tr>
				<td class="sectionsubhead" colspan="4" align="center">
					<script>
						ns.account.cancleButtonRedirectRegisteURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/register-wait.jsp'/>";
					</script>
			    	<sj:a
                       button="true"
                       onclick="ns.account.cancleButtonRedirectRegiste(ns.account.cancleButtonRedirectRegisteURL)"
                       ><s:text name="jsp.default_82"/></sj:a>
				&nbsp;&nbsp;
				<sj:a
                       formIds="EditTransaction"
                       button="true"
                       targets="accountRegisterDashbord"
                       onclick="ns.account.fillEditTotalAmount()"
                       validateFunction="customValidation"
                       validate="true"
                       ><s:text name="jsp.default_175"/></sj:a>
				</td>
			</tr>
		</table>

	</tr>
</table>
</s:form>
<ffi:setProperty name="RegisterCategories" property="Filter" value="All"/>

<%-- ================= MAIN CONTENT END ================= --%>
    <%
		session.setAttribute("FFIEditRegisterTransaction", session.getAttribute("EditRegisterTransaction"));
	%>
	<%
		session.setAttribute("FFIAddRegisterPayee", session.getAttribute("AddRegisterPayee"));
	%>
</div>

<script>

		$('#register_rec_editID').portlet({
			generateDOM: true,
			helpCallback: function(){

				var helpFile = $('#register_rec_editID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}

		});
			$('#register_rec_editID').portlet('title', js_acc_editrecord_portlet_title);
</script>
