<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript" src="<s:url value='/web/js/account/account%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/account/account_grid%{#session.minVersion}.js'/>"></script>
<div id="desktop" align="center">

	<div id="appdashboard">
		<s:include value="/pages/jsp/account/consolidatedbalance_dashboard.jsp"/>
	</div>
	<div id="summary">   
		<div id="conBalHolder"  class="portlet floatLeft" >
			 <div class="portlet-header">
			 <h1 class="portlet-title hidden"><s:text name="jsp.home_60"/></h1>
				<div class="searchHeaderCls">
					<span class="searchPanelToggleAreaCls" onclick="$('.quickSearchAreaCls').slideToggle();">
						<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
					</span>
					<div class="summaryGridTitleHolder resetHandCursor">
						<span id="selectedGridTab" class="selectedTabLbl marginRight10">
							<s:text name="jsp.default.searchResult"/>
						</span>
					</div>
				</div>
			 </div>   
			<div class="portlet-content">
				<div id="querybord">
	     			<s:include value="/pages/jsp/account/consolidatedbalance.jsp"/>
				</div> 
			   <div id="consolidatedBalanceSummary">
			   
			   </div>
			   <div id="consolidatedBalanceSummaryBarChart" align="center">
			   		
			   </div>
			   <div id="consolidatedBalanceSummaryDonutChart">
			   		
			   </div>
			</div>
			   
			<div id="consolidatedBalanceSummary-menu" class="portletHeaderMenuContainer" style="display:none;">
				<ul class="portletHeaderMenu">
					<li><a href='#' id="viewAsDonut" onclick="changeConsolidatedBalancePortletView('donut')"><span class="sapUiIconCls icon-pie-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDonut' /></a></li>
					<li><a href='#' id="viewAsBar" onclick="changeConsolidatedBalancePortletView('bar')"><span class="sapUiIconCls icon-bar-chart" style="float:left;"></span><s:text name='jsp.home.viewAsBar' /></a></li>
					<li><a href='#' id="viewAsDataTable" onclick="changeConsolidatedBalancePortletView('dataTable')"><span class="sapUiIconCls icon-table-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDataTable' /></a></li>
					<li><a href='#' onclick="ns.common.showDetailsHelp('conBalHolder')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
				</ul>
			</div>
			
		</div>
	</div>
   <br/>
   <table style="margin-top:15px; margin-bottom: 15px; width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="center"><span class="submain"><s:text name="jsp.account_203"/>&nbsp;</span><br/></td>
		</tr>
		<tr><td><br></td></tr>
	</table>
</div>

 <script type="text/javascript">
 
 
 	// $('.portlet-title').html('Consolidated Balance - Data Table');
 	ns.common.initializePortlet('conBalHolder');

 	if(ns.account.querybordflag){
 		$('#querybord').html('');
 	}else{
		/* $('#querybord').pane({
			title: js_dashboard,
			minmax: true,
			close: false
		}); */
 	}
 	
 	changeConsolidatedBalancePortletView = function(dataType) {
 		ns.account.consolidatedBalanceDisplayType = dataType;
 		var tabTypes = ["consolidatedBalanceSummary","consolidatedBalanceSummaryBarChart","consolidatedBalanceSummaryDonutChart"];
 		if(dataType =='donut'){
 			$('#conBalHolder .portlet-title').html('Consolidated Balance - Donut Chart');
 			console.log('donut')
 			renderConsolidateBalanceDonutChart();
 			displayChart("consolidatedBalanceSummaryDonutChart",tabTypes);;
 		}else if(dataType =='bar'){
 			$('#conBalHolder .portlet-title').html('Consolidated Balance - Bar Chart');
 			console.log('bar')
 			renderConsolidateBalanceBarChart();
 			displayChart("consolidatedBalanceSummaryBarChart",tabTypes);
 		}else if(dataType =='dataTable'){
 			$('#conBalHolder .portlet-title').html('Consolidated Balance - Data Table');
 			displayChart("consolidatedBalanceSummary",tabTypes);
 			$('#consolidatedBalanceSummaryID').jqGrid('setGridParam',{datatype:'json'}).trigger("reloadGrid");
 		}
 	}

 	function renderConsolidateBalanceDonutChart() {
 		//var routers = sap.banking.ui.routing.Routers;
		//sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.CHARTS).navTo("_consolidatedBalanceDonutChartPortal");
		$.publish("sap-ui-renderChartsTopic",{chartRoute: "_consolidatedBalanceDonutChartPortal"});
 	}

 	function renderConsolidateBalanceBarChart() {
 		//var routers = sap.banking.ui.routing.Routers;
		//sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.CHARTS).navTo("_consolidatedBalanceBarChartPortal");
		$.publish("sap-ui-renderChartsTopic",{chartRoute: "_consolidatedBalanceBarChartPortal"});
 	}

 	function displayChart(chartType,tabTypes){
 		for(var i=0;i<tabTypes.length;i++){
 			if(tabTypes[i]== chartType){
 				$("#"+tabTypes[i]).show();
 			}else{
 				$("#"+tabTypes[i]).hide();
 			}
 		}
 	}
 	
 	$(document).ready(function(){
 		ns.account.showAccountDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
 	});
 	
</script>
