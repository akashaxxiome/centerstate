<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%
    session.setAttribute("SetRegisterTransactionId",request.getParameter("SetRegisterTransactionId"));
    session.setAttribute("CollectionSessionName",request.getParameter("CollectionSessionName"));
    String collectionName=request.getParameter("CollectionSessionName");
%>
<ffi:setProperty name="SetRegisterTransaction" property="CollectionSessionName" value="<%=collectionName %>"/>
<ffi:setProperty name="RegisterTransaction" property="Current" value=""/>
<ffi:object name="com.ffusion.tasks.register.SetRegisterTransaction" id="SetRegisterTransaction" scope="session"/>
	<ffi:setProperty name="SetRegisterTransaction" property="Id" value="${SetRegisterTransactionId}" />
	<ffi:setProperty name="SetRegisterTransaction" property="CollectionSessionName" value="${CollectionSessionName}" />
<ffi:process name="SetRegisterTransaction"/>
<ffi:object name="com.ffusion.tasks.register.DeleteRegisterTransaction" id="DeleteRegisterTransaction"/>

<div align=center>
<% String v152_4a = ""; String v152_2b = ""; %>

<ffi:setProperty name="negativetrue" value="<span style='color:#FF0000'>"/>
<ffi:setProperty name="negativefalse" value="<span>"/>
<ffi:object id="Currency" name="com.ffusion.beans.common.Currency" scope="request"/>

<s:form id="deleteRecordForm" action="DeleteRegisterTransactionAction" namespace="/pages/jsp/account" method="post" name="deleteform" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="Hidden" name="DeleteRegisterTransaction.Id" value="<ffi:getProperty name="RegisterTransaction" property="RegisterId"/>">
<input type="Hidden" name="DeleteRegisterTransaction.CollectionSessionName" value="<ffi:getProperty name="SetRegisterTransaction" property="CollectionSessionName"/>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="11"></td>
		<td class="sectionhead_grey"></td>
		<td width="11"></td>
	</tr>
	<tr>
		<td width="11"><br>
			<br>
		</td>
		<td align="center" class="sectionhead_grey">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td class="sectionhead tbrd_b" colspan="2">&gt; <s:text name="jsp.account_71"/></td>
				</tr>
				<tr>
					<td class="columndata" colspan="2" height="40"><s:text name="jsp.account_227"/><br></td>
				</tr>
				<tr>
					<td class="sectionhead" height="20" nowrap><s:text name="jsp.account_216"/></td>
					<td class="columndata" width="571">
						<ffi:getProperty name="RegisterTransaction" property="RegisterTypeName"/>
					</td>
				</tr>
				<tr>
					<td class="sectionhead" valign="top" height="20"><s:text name="jsp.account_55"/></td>
					<td class="columndata">
						<ffi:getProperty name="RegisterTransaction" property="ReferenceNumber"/>
					</td>
				</tr>
				<tr>
					<td class="sectionhead" valign="top" height="20"><s:text name="jsp.account_111"/></td>
					<td class="columndata">
						<ffi:setProperty name="RegisterTransaction" property="DateFormat" value="${UserLocale.DateFormat}" />
						<ffi:getProperty name="RegisterTransaction" property="DateIssued"/>
					</td>
				</tr>
				<tr>
					<td class="sectionhead" valign="top" height="20"><s:text name="jsp.account_160"/></td>
					<td class="columndata">
						<ffi:getProperty name="RegisterTransaction" property="PayeeName"/>
					</td>
				</tr>
				<tr>
					<td class="sectionhead" valign="top" height="20"><s:text name="jsp.account_49"/></td>
					<td class="columndata">
							<ffi:cinclude value1="${RegisterTransaction.MultipleCategories}" value2="true">
								Multiple
							</ffi:cinclude>
							<ffi:cinclude value1="${RegisterTransaction.MultipleCategories}" value2="false">
								<ffi:setProperty name="RegisterCategories" property="Current" value="${RegisterTransaction.RegisterCategoryId}" />
								<ffi:setProperty name="Parentfalse" value=""/>
								<ffi:setProperty name="Parenttrue" value=": "/>
								<ffi:getProperty name="RegisterCategories" property="ParentName"/><ffi:getProperty name="Parent${RegisterCategories.HasParent}"/><ffi:getProperty name="RegisterCategories" property="Name"/>
							</ffi:cinclude>
					</td>
				</tr>
				<tr>
					<td class="sectionhead" valign="top" height="20"><s:text name="jsp.default_45"/></td>
					<td class="columndata">
						<ffi:setProperty name="Currency" property="Format" value="CURRENCY" />
						<ffi:setProperty name="Currency" property="Amount" value="${RegisterTransaction.Amount}" />
						<ffi:setProperty name="Currency" property="Format" value="DECIMAL" />
						<ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}" />
						<ffi:setProperty name="FloatMath" property="Value2" value="0" />
						<ffi:getProperty name="negative${FloatMath.Less}" encode="false"/><ffi:getProperty name="RegisterTransaction" property="AmountValue.CurrencyStringNoSymbol"/></span>
					</td>
				</tr>
				<tr>
					<td colspan="2"><img src="/cb/web/multilang/grafx/spacer.gif" width="1" height="15" border="0" alt=""></td>
				</tr>
			</table>
			<br>
			<center>
			     <sj:a  
				    button="true" 
					onClickTopics="closeDialog"
				    ><s:text name="jsp.default_82"/></sj:a>
			  &nbsp;&nbsp;
			  <ffi:cinclude value1="${SetRegisterTransaction.CollectionSessionName}" value2="RegisterTransactions">
			    <sj:a 
                     formIds="deleteRecordForm"
                     button="true"
                     targets="accoungRegisterDeleteSuccess"
                     onSuccessTopics="deleteRecordSuccessTopicss" 
					 effectDuration="1500"
                     ><s:text name="jsp.default_111"/></sj:a>
               </ffi:cinclude>
               <ffi:cinclude value1="${SetRegisterTransaction.CollectionSessionName}" value2="FutureRegisterTransactions">
			    <sj:a 
                     formIds="deleteRecordForm"
                     button="true"
                     targets="accoungPendingRegisterDeleteSuccess"
                     onSuccessTopics="deleteRecordSuccessTopicss" 
					 effectDuration="1500"
                     ><s:text name="jsp.default_111"/></sj:a>
               </ffi:cinclude>
			&nbsp;&nbsp;
			</center>
		</td>
		<td align="right" width="11"><br>
			<br>
		</td>
	</tr>
	<tr>
		<td width="11"></td>
		<td class="sectionhead_grey"></td>
		<td width="11"></td>
	</tr>
</table>
</s:form>
<ffi:setProperty name="RegisterCategories" property="Filter" value="All" />
<%-- ================= MAIN CONTENT END ================= --%>
</div>
<% 	session.setAttribute("FFIDeleteRegisterTransaction", session.getAttribute("DeleteRegisterTransaction")); %>
