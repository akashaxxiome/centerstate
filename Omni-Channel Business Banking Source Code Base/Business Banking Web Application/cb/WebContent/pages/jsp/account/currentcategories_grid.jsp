<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%
   String filter = request.getParameter("filter");
   request.setAttribute("filter",filter);
%>

<div id="account_categories_gridID">
<ffi:help id="account_register-categories" />
<ffi:setGridURL grid="GRID_accountCurrentCategories" name="EditURL" url="/cb/pages/jsp/account/register-categories.jsp?SetEditRegisterCategory={0}" parm0="Id"/>
<ffi:setGridURL grid="GRID_accountCurrentCategories" name="DeleteURL" url="/cb/pages/jsp/account/register-cat-delete.jsp?SetRegisterCategoryId={0}" parm0="Id"/>

<ffi:setProperty name="tempURL" value="/pages/jsp/account/GetRegisterCategoriesAction.action?filter=${filter}&GridURLs=GRID_accountCurrentCategories" URLEncrypt="true"/>
<s:url id="currentCategoriesSummaryURL" value="%{#session.tempURL}" escapeAmp="false"/>
<sjg:grid
     id="currentCategoriesSummaryID"
	 sortable="true" 
     dataType="json" 
     href="%{currentCategoriesSummaryURL}"
     pager="true"
     gridModel="gridModel"
	 rowList="%{#session.StdGridRowList}" 
	 rowNum="%{#session.StdGridRowNum}" 
	 rownumbers="false"
	 shrinkToFit="true"
	 scroll="false"
	 scrollrows="true"
     onGridCompleteTopics="addGridControlsEvents,currentCategoriesSummaryEvent"
     >
     
     <sjg:gridColumn name="map.names" index="map.names" title="%{getText('jsp.account_51')}" sortable="true" width="230" formatter="ns.account.categoryNameFormatter" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="type" index="type" title="%{getText('jsp.default_444')}" sortable="true" width="100"   formatter="ns.account.categoriesTypeFormatter" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="description" index="description" title="%{getText('jsp.default_170')}" sortable="false" width="100"  cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="taxRelated" index="taxRelated" title="%{getText('jsp.account_196')}" sortable="false" width="100"   formatter="ns.account.taxRelatedFormatter" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="id" index="id" title="%{getText('jsp.default_27')}" sortable="false" width="100"  formatter="ns.account.categoryActionFormatter" cssClass="datagrid_actionIcons"/>
     
     <sjg:gridColumn name="map.catType" index="map.catType" title="%{getText('jsp.account_53')}" width="100" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.hassub" index="map.hassub" title="%{getText('jsp.account_93')}" width="100" hidden="true" hidedlg="true"/>
     <sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
	 <sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>     
</sjg:grid>


</div>
<script>	
					
		$('#account_categories_gridID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#account_categories_gridID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});

		$('#account_categories_gridID').portlet('title', js_acc_categories_portlet_title);

		$("#currentCategoriesSummaryID").jqGrid('setColProp','id',{title:false});

</script>