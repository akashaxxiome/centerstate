<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>


<%
   String dataClassification = request.getParameter("classfication");
   String displayCurrencyCode = request.getParameter("displayCurrencyCode");
   request.setAttribute("dataClassification",dataClassification);
   request.setAttribute("displayCurrencyCode",displayCurrencyCode);
%>

<div id="consolidatedBalance_other_gridID">
<ffi:help id="accountsbygroup" />
<ffi:setGridURL grid="GRID_consolidatedvalanceOther" name="LinkURL" url="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=5&AccountID={0}&AccountBankID={1}&AccountRoutingNum={2}&DataClassification={3}&redirection=true&ProcessAccount=true&TransactionSearch=false" parm0="Account.ID" parm1="Account.BankID" parm2="Account.RoutingNum" parm3="DataClassification"/>
<ffi:setProperty name="consolidatedBalanceOtherSummaryTempURL" value="/pages/jsp/account/GetConsolidatedBalanceOtherAction.action?dataClassification=${dataClassification}&displayCurrencyCode=${displayCurrencyCode}&GridURLs=GRID_consolidatedvalanceOther" URLEncrypt="true"/>
<s:url id="otherSummaryURL" value="%{#session.consolidatedBalanceOtherSummaryTempURL}" escapeAmp="false" />

<sjg:grid
     id="consolidatedBalanceOtherSummaryID"
	 sortable="true" 
     dataType="json" 
     href="%{otherSummaryURL}"
     pager="true"
     gridModel="gridModel"
	 rowList="%{#session.StdGridRowList}" 
	 rowNum="%{#session.StdGridRowNum}" 
	 rownumbers="false"
	 navigator="true"
	 navigatorAdd="false"
	 navigatorDelete="false"
	 navigatorEdit="false"
	 navigatorRefresh="false"
	 navigatorSearch="false"
	 navigatorView="false"
	 footerrow="true"
	 userDataOnFooter="true"
	 shrinkToFit="true"
	 scroll="false"
	 scrollrows="true"
	 viewrecords="true"
     onGridCompleteTopics="addGridControlsEvents,consolidatedBalanceOtherSummaryEvent">
     <sjg:gridColumn name="account.accountDisplayText" index="NUMBER" title="%{getText('jsp.default_15')}" sortable="true" width="150" cssClass="datagrid_actionColumn" formatter="ns.account.formatAccountColumn"/>
     <sjg:gridColumn name="nameNickNameother" index="ACCNICKNAME" title="%{getText('jsp.default_293')}" sortable="true" width="280" formatter="ns.account.customToAccountNickNameColumn" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.bankName" index="BANKNAME" title="%{getText('jsp.default_61')}" sortable="true" width="95" formatter="ns.account.nameFormatter" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.type" index="TYPESTRING" title="%{getText('jsp.default_444')}" sortable="true" width="65" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="summaryDateDisplay" index="SummaryDateDisplay" title="%{getText('jsp.default_51')}" sortable="true" width="60" cssClass="datagrid_dateColumn"/>
     <sjg:gridColumn name="displayOpeningLedger" index="OPENING_LEDGER" title="%{getText('jsp.default_305')}" sortable="true" formatter="ns.account.openingLedgerFormatter" width="150" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="totalDebits" index="totalDebits" title="%{getText('jsp.default_434')}" sortable="true" formatter="ns.account.totalDebitsFormatter" width="90" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="totalCredits" index="totalCredits" title="%{getText('jsp.default_433')}" sortable="true" formatter="ns.account.totalCreditsFormatter" width="90" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="account.displayClosingBalance" index="CLOSINGBALANCE" title="%{getText('jsp.default_104')}" sortable="true" width="100" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="account.displayClosingBalance.amountValue.currencyStringNoSymbol" index="displayClosingBalance.amountValue.currencyStringNoSymbol" title="%{getText('jsp.default_104')}" sortable="true" width="100" formatter="ns.account.closingLedgerFormatter" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="account.displayIntradayCurrentBalance.amountValue.currencyStringNoSymbol" index="displayIntradayCurrentBalance.amountValue.currencyStringNoSymbol" title="%{getText('jsp.default_129')}" sortable="true" width="100" formatter="ns.account.currentLedgerFormatter" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="account.displayText" index="displayText" title="%{getText('jsp.account_77')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.nickName" index="NICKNAME" title="%{getText('jsp.account_146')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.currencyCode" index="CURRENCYCODE" title="%{getText('jsp.account_60')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.ID" index="ID" title="%{getText('jsp.account_12')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.bankID" index="BANKID" title="%{getText('jsp.account_39')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.routingNum" index="ROUTINGNUM" title="%{getText('jsp.account_183')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>
     <sjg:gridColumn name="displayCurrencyCode" index="map.displayCurrencyCode" title="%{getText('jsp.default_173')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="dataClassification" index="map.dataClassification" title="%{getText('jsp.account_64')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="totalOpeningLedgerTotal" index="map.totalOpeningLedgerTotal" title="%{getText('jsp.account_210')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="totalDebitsTotal" index="map.totalDebitsTotal" title="%{getText('jsp.account_209')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="totalCreditsTotal" index="map.totalCreditsTotal" title="%{getText('jsp.account_208')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="accountsClosingBalanceTotal" index="map.accountsClosingBalanceTotal" title="%{getText('jsp.account_10')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="accountsIntradayCurrentBalanceTotal" index="map.accountsIntradayCurrentBalanceTotal" title="%{getText('jsp.account_11')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="map.LinkURL" index="LinkURL" title="%{getText('jsp.default_262')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
</sjg:grid>

</div>
<script>	
	$(document).ready(function(){
		/* $('#consolidatedBalance_other_gridID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#consolidatedBalance_other_gridID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});

		$('#consolidatedBalance_other_gridID').portlet('title', js_acc_other_portlet_title); */

	})		
		
</script>
<style>
	#consolidatedBalance_other_gridID .ui-jqgrid-ftable td{
		border:none;
	}
</style>
