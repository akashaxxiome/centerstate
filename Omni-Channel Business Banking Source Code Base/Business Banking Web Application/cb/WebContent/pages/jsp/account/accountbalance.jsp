<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.accountconfig.beans.AccountBalanceSearchCriteria" %>
<% 
	session.setAttribute("AccountDisplayCurrencyCode",request.getParameter("AccountDisplayCurrencyCode"));
	AccountBalanceSearchCriteria accountBalanceSearchCriteria = (AccountBalanceSearchCriteria) session.getAttribute("accountBalanceSearchCriteria");
	session.setAttribute("AccountBalancesDataClassification",accountBalanceSearchCriteria.getDataClassification());
%>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="false"/>
</ffi:cinclude>


<%-- ADDED BY TCG FOR CHECKIMAGING --%>

<%-- Flag to check whether to retrieve image or not --%>
<ffi:setProperty name="get-image-flag" value="true"/>

<%-- INITIALIZING TRANSACTION SEARCH IMAGE TASK --%>
<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="SearchImage" scope="session"/>
<ffi:setProperty name="SearchImage" property="Init" value="true"/>
<ffi:process name="SearchImage"/>
<ffi:setProperty name="SearchImage" property="SuccessURL" value="${SecurePath}account/checkimage.jsp" />

<ffi:cinclude value1="${GetDisplaySummariesForAccount}" value2="" operator="equals">
<ffi:object name="com.ffusion.tasks.banking.GetDisplaySummariesForAccount" id="GetDisplaySummariesForAccount" scope="session"/>
</ffi:cinclude>
<ffi:setProperty name="GetDisplaySummariesForAccount" property="DisplayCurrency"  value="${AccountDisplayCurrencyCode}"/>

<ffi:setProperty name="subMenuSelected" value="account balance"/>

<%-- The specify criteria page sets variables and removes the variables set (and also the properties --%>
<%-- specified by those variables --%>
<ffi:setProperty name="SubmitURL" value="/pages/jsp/account/accountbalance.jsp"/>
<ffi:setProperty name="flag" value="accountbalance"/>
<ffi:setProperty name="DataClassificationVariable" value="${AccountBalancesDataClassification}"/>

<ffi:setProperty name="DataType" value="Summary"/>
<ffi:setProperty name="AccountsCollectionName" value="BankingAccounts"/>
 
<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>
 
<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="notEquals">
	<script language="JavaScript" type="text/javascript">
		ns.account.accountBalanceDashbordflag=true;
	</script> 
</ffi:cinclude>

<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="equals">

	<ffi:removeProperty name="DataClassification"/>
	<ffi:removeProperty name="DateString"/>


<div id="accountBalanceGridTabs" class="portlet floatLeft gridPannelSupportCls">
     	<div class="portlet-header" >
		    <div class="searchHeaderCls">
				<span class="searchPanelToggleAreaCls" onclick="$('#accountBalanceQuicksearchcriteria').slideToggle();">
					<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
				</span>
				<div class="summaryGridTitleHolder">
					<s:if test="%{#parameters.visibleGrid[0] == 'assetAccounts'}">
						<span id="selectedGridTab" class="selectedTabLbl">
							<s:text name="jsp.account_231"/>
						</span>
					</s:if>
					<s:elseif test="%{#parameters.visibleGrid[0] == 'creditCardsAccounts'}">
						<span id="selectedGridTab" class="selectedTabLbl">
							<s:text name="jsp.account_58"/>
						</span>
					</s:elseif>
					<s:elseif test="%{#parameters.visibleGrid[0] == 'loanAccounts'}">
						<span id="selectedGridTab" class="selectedTabLbl">
							<s:text name="jsp.account_116"/>
						</span>
					</s:elseif>
					<s:else>
						<span id="selectedGridTab" class="selectedTabLbl">
							<s:text name="jsp.account_76"/>
						</span>
					</s:else>
				
					
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight marginRight10"></span>
					<div class="gridTabDropdownHolder">
						<span class="gridTabDropdownItem" id='depositAccounts' onclick="ns.account.showGridSummary('consolidatedBalanceDepositSummaryID','/cb/pages/jsp/account/inc/accountsdeposit.jsp','depositAccounts')" >
							<s:text name="jsp.account_76"/>
						</span>
						<span class="gridTabDropdownItem" id='assetAccounts' onclick="ns.account.showGridSummary('consolidatedBalanceAssetSummaryID','/cb/pages/jsp/account/inc/accountsasset.jsp','assetAccounts')">
							<s:text name="jsp.account_231"/>
						</span>
						<!--<span class="gridTabDropdownItem" id='creditCardsAccounts' onclick="ns.account.showGridSummary('consolidatedBalanceCcardSummaryID','/cb/pages/jsp/account/inc/accountsccard.jsp','creditCardsAccounts')">
							<s:text name="jsp.account_58"/>
						</span>-->
						<span class="gridTabDropdownItem" id='loanAccounts' onclick="ns.account.showGridSummary('consolidatedBalanceLoanSummaryID','/cb/pages/jsp/account/inc/accountsloan.jsp','loanAccounts')">
							<s:text name="jsp.account_116"/>
						</span>
						<!--<span class="gridTabDropdownItem" id='otherAccounts' onclick="ns.account.showGridSummary('consolidatedBalanceOtherSummaryID','/cb/pages/jsp/account/inc/accountsother.jsp','otherAccounts')">
							<s:text name="jsp.account_155"/>
						</span>-->
					</div>
					
				</div>
			</div>
	    </div>
	    <div class="portlet-content">
    		<div class="searchDashboardRenderCls">
    			<div id="accountBalanceQuicksearchcriteria" class="quickSearchAreaCls">
    				<s:include value="/pages/jsp/account/accountbalance_searchCriteria.jsp"/>
    			</div>
    		</div>
    		<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="depositAccountsSummaryGrid" gridId="consolidatedBalanceDepositSummaryID" class="gridSummaryContainerCls" >
					<div id="depositAccountSummary">
							<ffi:cinclude value1="${visibleGrid}" value2="depositAccounts" operator="equals">
								<s:include value="/pages/jsp/account/inc/accountsdeposit.jsp" />
							</ffi:cinclude>
							<ffi:cinclude value1="${visibleGrid}" value2="" operator="equals">
								<s:include value="/pages/jsp/account/inc/accountsdeposit.jsp" />
							</ffi:cinclude>
					
					</div>
					<div id="depositAccHolderBarChart">
				
				   	</div>
				   	<div id="depositAccHolderDonutChart">
				
				   	</div>
				</div>
				<div id="assetAccountsSummaryGrid" gridId="consolidatedBalanceAssetSummaryID" class="gridSummaryContainerCls" >
					<div id="assetAccountSummary">
						<ffi:cinclude value1="${visibleGrid}" value2="assetAccounts" operator="equals">
							<s:include value="/pages/jsp/account/inc/accountsasset.jsp" />
						</ffi:cinclude>
					</div>
					<div id="assetAccHolderBarChart">
				
				   	</div>
				   	<div id="assetAccHolderDonutChart">
				
				   	</div>
				</div>
				<div id="creditCardsAccountsSummaryGrid" gridId="consolidatedBalanceCcardSummaryID" class="gridSummaryContainerCls">
					<div id="creditCardsAccountSummary">
						<ffi:cinclude value1="${visibleGrid}" value2="creditCardsAccounts" operator="equals">
							<s:include value="/pages/jsp/account/inc/accountsccard.jsp" />
						</ffi:cinclude>
					</div>
					<div id="creditCardsAccHolderBarChart">
				
				   	</div>
				   	<div id="creditCardsAccHolderDonutChart">
				
				   	</div>
				</div>
				<div id="loanAccountsSummaryGrid" gridId="consolidatedBalanceLoanSummaryID" class="gridSummaryContainerCls" >
					<div id="loanAccountSummary">
						<ffi:cinclude value1="${visibleGrid}" value2="loanAccounts" operator="equals">
							<s:include value="/pages/jsp/account/inc/accountsloan.jsp" />
						</ffi:cinclude>
					</div>
					<div id="loanAccHolderBarChart">
				
				   	</div>
				   	<div id="loanAccHolderDonutChart">
				
				   	</div>
				</div>
				<div id="otherAccountsSummaryGrid" gridId="consolidatedBalanceOtherSummaryID" class="gridSummaryContainerCls" >
					<div id="otherAccountSummary">
						
					</div>
					<div id="otherAccHolderBarChart">
				
				   	</div>
				   	<div id="otherAccHolderDonutChart">
				
				   </div>
				</div>
			</div>
	   	</div>
	   	<div id="accountBalanceDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="changeAccountSummaryPortletView('donut')"><span class="sapUiIconCls icon-pie-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDonut' /></a></li>
				  <li><a href='#' onclick="changeAccountSummaryPortletView('bar')"><span class="sapUiIconCls icon-bar-chart" style="float:left;"></span><s:text name='jsp.home.viewAsBar' /></a></li>
				  <li><a href='#' onclick="changeAccountSummaryPortletView('dataTable')"><span class="sapUiIconCls icon-table-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDataTable' /></a></li>
				  <li><a href='#' onclick="ns.common.showDetailsHelp('accountBalanceGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
  </div>	
	
	
 </ffi:cinclude>
<ffi:removeProperty name="AccountsCollectionName"/>
<ffi:removeProperty name="DataType"/>
<ffi:removeProperty name="AccountBalancesDataClassification"/>
<ffi:removeProperty name="AccountDisplayCurrencyCode"/>
<ffi:removeProperty name="DataClassificationVariable"/>
<ffi:removeProperty name="CheckPerAccountReportingEntitlements"/>
<script type="text/javascript">
$(document).ready(function(){
	ns.common.initializePortlet("accountBalanceGridTabs");
});


changeAccountSummaryPortletView = function(dataType,selectedAccountType) {
	ns.account.displayType =  dataType;
	var accountTypeJson ={
			"depositAccounts":"Deposit",
			"assetAccounts":"Asset",
			"creditCardsAccounts":"CreditCards",
			"loanAccounts":"Loan",
			"otherAccounts":"Other",
	};
	//get id of the class which is visible
	var gridId = $(".gridSummaryContainerCls:visible").attr('id');
	var accountType = "";
	if(selectedAccountType == undefined){
		accountType = gridId.substring(0,gridId.length - 19);
		accountType = accountType.substr(0,1).toUpperCase()+accountType.substr(1);
	}
	else{
		accountType = accountTypeJson[selectedAccountType];
	}

	var tabTypeJson =[];
	tabTypeJson.Deposit = ["depositAccountSummary","depositAccHolderBarChart","depositAccHolderDonutChart"];
	tabTypeJson.Asset = ["assetAccountSummary","assetAccHolderBarChart","assetAccHolderDonutChart"];
	tabTypeJson.CreditCards = ["creditCardsAccountSummary","creditCardsAccHolderBarChart","creditCardsAccHolderDonutChart"];
	tabTypeJson.Loan = ["loanAccountSummary","loanAccHolderBarChart","loanAccHolderDonutChart"];
	tabTypeJson.Other = ["otherAccountSummary","otherAccHolderBarChart","otherAccHolderDonutChart"];
	
	var gridTypeJson =[];
	gridTypeJson.Deposit = "consolidatedBalanceDepositSummaryID";
	gridTypeJson.Asset = "consolidatedBalanceAssetSummaryID";
	gridTypeJson.CreditCards ="consolidatedBalanceCcardSummaryID";
	gridTypeJson.Loan ="consolidatedBalanceLoanSummaryID";
	gridTypeJson.Other ="consolidatedBalanceOtherSummaryID";
	
	
	if(dataType =='donut'){	
		//$('#accountBalanceGridTabs').portlet('title',accountType+' Account - Donut Chart');
		renderDepositAccountDonutChart(accountType);
		displayChart(accountType+"AccHolderDonutChart",tabTypeJson[accountType]);;
	}else if(dataType =='bar'){
		//$('#accountBalanceGridTabs').portlet('title',accountType+' Account - Bar Chart');
		renderDepositAccountBarChart(accountType);
		displayChart(accountType+"AccHolderBarChart",tabTypeJson[accountType]);
	}else if(dataType =='dataTable'){
		//$('#accountBalanceGridTabs').portlet('title',accountType+' Account - Data Table');
		displayChart(accountType+"AccountSummary",tabTypeJson[accountType]);
		$('#'+gridTypeJson[accountType]).trigger("reloadGrid");
	} 

	//var routers = sap.banking.ui.routing.Routers;
	//sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.CHARTS).navOut();
	window.location.hash = "";
 }

	function renderDepositAccountDonutChart(accountType) {
		//var routers = sap.banking.ui.routing.Routers;
		//sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.CHARTS).navTo("_"+accountType+"AccountDonutChartPortal");
		$.publish("sap-ui-renderChartsTopic",{chartRoute: "_"+accountType+"AccountDonutChartPortal"});

		
	}
	
	function renderDepositAccountBarChart(accountType) {
		//var routers = sap.banking.ui.routing.Routers;
		//sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.CHARTS).navTo("_"+accountType+"AccountBarChartPortal");
		$.publish("sap-ui-renderChartsTopic",{chartRoute: "_"+accountType+"AccountBarChartPortal"});
	}
	
	function displayChart(chartType,tabTypes){
		for(var i=0;i<tabTypes.length;i++){
			if(tabTypes[i].toUpperCase()== chartType.toUpperCase()){
				$("#"+tabTypes[i]).show();
			}else{
				$("#"+tabTypes[i]).hide();
			}
		}
	}


</script>
<ffi:removeProperty name="DataClassification"/>
<ffi:removeProperty name="DateString"/>
<ffi:removeProperty name="AccountsCollectionName"/>
<ffi:removeProperty name="DataType"/>
<ffi:removeProperty name="AccountBalancesDataClassification"/>
<ffi:removeProperty name="AccountDisplayCurrencyCode"/>
<ffi:removeProperty name="DataClassificationVariable"/>
<ffi:removeProperty name="CheckPerAccountReportingEntitlements"/>