<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="approvals_corpadminapprovalgroupdelete-confirm" />

<div align="center">
	<s:form id="GroupDelete" namespace="/pages/jsp/approvals" validate="false"
		action="deleteApprovalGroup" method="post" name="GroupDelete" theme="simple">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
	</s:form>
	<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		<TR>
			<TD>&nbsp;</TD>
		</TR>
		<TR>
			<TD class="columndata" align="center"><s:text name="jsp.approvals_13"/> <s:property value="%{#session.deleteApprovalsGroup.GroupName}" />.</TD>
		</TR>
		<TR>
			<TD class="ui-widget-header customDialogFooter">
				<sj:a id="cancelDeleteGroupLink1"
						button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_82')}"><s:text name="jsp.default_82"/></sj:a>
				<sj:a
					id="deleteGroupLink1" targets="resultmessage"
					onclick="deleteApprovalGroupFunc()"
					button="true" title="%{getText('jsp.default_162')}"  
					><s:text name="jsp.default_162"/></sj:a>
			</TD>
		</TR>
	</TABLE>
</div>

<script type="text/javascript">
function deleteApprovalGroupFunc(){
	$.ajax({
		url: "/cb/pages/jsp/approvals/deleteApprovalGroup.action",
		data:{CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
		error : function(oError) {
			console.log(oError);
		},
		success : function(data){
			 ns.common.showStatus(data)
			 ns.common.showSummaryOnSuccess("summary","done");
		}
	});
	ns.common.closeDialog('deleteApprovalGroupDialogID');
}
</script>
