<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
session.setAttribute("idx", request.getParameter("idx"));
session.setAttribute("CollectionName", request.getParameter("CollectionName"));
%>

<ffi:setProperty name='PageHeading' value='${ApprovalsHeading}'/>

<%-- Include approval include-approval-view-details-init.jsp page --%>
<ffi:include page="${PagesPath}/common/include-approval-view-details-init.jsp"/>
<ffi:removeProperty name="ItemID"/>
<ffi:include page="${PagesPath}/inc/include-cb-approval-constants.jsp"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<ffi:help id="approvals_approvepayments-viewtax" />
<div align="center">
    <span id="PageHeading" style="display:none"><s:text name="jsp.approvals_18"/></span>
		<ffi:include page="${PagesPath}/common/include-approval-view-details-header.jsp"/>
		<div>
			<ffi:include page="${PagesPath}/common/include-view-taxpayment-details.jsp"/>
		</div>
</div>