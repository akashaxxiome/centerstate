<%--
<p>This is the confirmation page for pending wire beneficiaries and ACH participants.
 This page will show changes made by users in wire beneficiaries and ACH participants.
 If user clicks the confirm button then it save those changes into the back end.
 </p>

Pages that request this page
----------------------------
da-include-approvepayees.jsp
	
Submit button

Pages this page requests
------------------------
--

Pages included in this page
---------------------------
--
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.beans.dualapproval.DAItemCategories"%>

<%-- set the properties required by this page --%>
<s:set var="tmpI18nStr" value="%{getText('jsp.da_Verify_Pending_Approvals_Actions')}" scope="request" />
<ffi:setProperty name='PageHeading' value="${tmpI18nStr}"/>
<ffi:setProperty name='PageText' value=''/>
<s:set var="tmpI18nStr" value="%{getText('jsp.da_approvals')}" scope="request" />
<ffi:setProperty name="subMenuSelected" value="${tmpI18nStr}"/>
<ffi:setProperty name="BackURL" value="${SecurePath}approvals/approvepayments.jsp"/>
<div id="confirmInfo" style="padding:10px">

            <%-- Payee Info --%>
					<%
						// Retrieve the approval actions and reasons for wire beficiaries to be approved/reject
						String[] approvalDecisionsDA 	= request.getParameterValues( "approvalActionsDA" );
						String[] daReason 				= request.getParameterValues( "daReason" );
						String[] ID 					= request.getParameterValues( "ID" );
						String[] userActions 			= request.getParameterValues( "UserAction" );
						String[] itemTypes	 			= request.getParameterValues( "ItemType" );
						String[] submittedByNames	 	= request.getParameterValues( "SubmittedBy" );
                        for (int i=0; i<submittedByNames.length; i++) {
                            submittedByNames[i] = com.ffusion.util.HTMLUtil.encode(submittedByNames[i]);
                        }

						// put current values of approval , reject reasons in session
						session.setAttribute("ApprovalDecisionsDA",approvalDecisionsDA);
						session.setAttribute("DAReason",daReason);					
						session.setAttribute("DAItemIds",ID);
						session.setAttribute("UserActions",userActions);
						session.setAttribute("ItemTypes",itemTypes);
						session.setAttribute("SubmittedByNames",submittedByNames);
						int approvedPayees = 0;										
					%>
					
					<% 	boolean isWiresPresent = false;
						boolean isACHPresent = false;
						boolean isReasonPresent = true;
						for (int i=0; i<ID.length; i++) { 

							if (approvalDecisionsDA[i].equals(IDualApprovalConstants.STATUS_TYPE_REJECTED)) {
								if (daReason[i] == null || daReason[i].equals("")) {
									isReasonPresent = false;
									break;
								}
							}

							if (approvalDecisionsDA[i].equals(IDualApprovalConstants.STATUS_TYPE_APPROVED) || approvalDecisionsDA[i].equals(IDualApprovalConstants.STATUS_TYPE_REJECTED)) { %>
							<ffi:cinclude value1="<%=itemTypes[i]%>" value2="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>">
							  <% isWiresPresent = true; %>
							</ffi:cinclude>
							<ffi:cinclude value1="<%=itemTypes[i]%>" value2="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>">
							  <% isACHPresent = true; %>
							</ffi:cinclude>
					<% 		}
						}
					 %>

					<% if(isReasonPresent) { %>
					 <%-- <div>
						<% if(isWiresPresent && isACHPresent) { %>
							<s:text name="jsp.da_approvals_pending_payees_changes"/>
						<% } else if(isWiresPresent) { %>						    
							<s:text name="jsp.da_approvals_pending_beneficiary_changes"/>
						<% } else if(isACHPresent) { %>
							<s:text name="jsp.da_approvals_pending_participant_changes"/>
						<% } %>
					</div> --%>
								<%-- For Wire beneficiaries --%>
									 <% for (int i=0; i<ID.length; i++) { %>
									 <% if (approvalDecisionsDA[i].equals(IDualApprovalConstants.STATUS_TYPE_APPROVED) || approvalDecisionsDA[i].equals(IDualApprovalConstants.STATUS_TYPE_REJECTED)) { %>
									 <ffi:cinclude value1="<%=itemTypes[i]%>" value2="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>">
									 	<ffi:list collection="WireTransferPayeesDA" items="WireTransferPayeeDA">
											 <ffi:cinclude value1="${WireTransferPayeeDA.DaStatus}" value2="<%=IDualApprovalConstants.STATUS_TYPE_REJECTED%>" operator="notEquals">
											
											 <ffi:cinclude value1="${WireTransferPayeeDA.IsLastModifiedUser}" value2="<%=IDualApprovalConstants.TRUE%>" operator="notEquals">
											
											 	<ffi:cinclude value1="${WireTransferPayeeDA.ID}" value2="<%=ID[i]%>">

														<%-- Start: Clean up DA information --%>
														<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
														
														<%-- End: Clean up DA information --%>
																						
														<%-- Retrieve category payeeDetails information --%>
														<ffi:object id="GetWireDACategorypayeeDetails" name="com.ffusion.tasks.dualapproval.wiretransfers.GetWireDACategoryDetails"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemId" value="${WireTransferPayeeDA.ID}"/>

															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_WIRE_PAYEE%>"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="businessId" value="${SecureUser.BusinessID}"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="daItemId" value="${WireTransferPayeeDA.daItemId}"/>
														<ffi:process name="GetWireDACategorypayeeDetails"/>
														<ffi:removeProperty name="GetWireDACategorypayeeDetails"/>																							
<div class="marginTop20"></div>
<div class="blockWrapper">
	<div  class="blockHead" align="center">
		<% if(isWiresPresent && isACHPresent) { %>
		    <s:text name="jsp.da_approvals_beneficiary_ach_information"/>
		<% } else if(isWiresPresent) { %>						    
			<s:text name="jsp.da_approvals_beneficiary_information"/>
		<% } else if(isACHPresent) { %>
			<s:text name="jsp.da_approvals_ach_information"/>
		<% } %>		
	</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_Name"/></span><span> <ffi:getProperty name="WireTransferPayeeDA" property="PayeeName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_SubmittedDate"/></span><span> <ffi:getProperty name="WireTransferPayeeDA" property="SubmittedDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_SubmittedBy"/></span><span> <%= com.ffusion.util.HTMLUtil.encode(submittedByNames[i]) %></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_ApprovalAction"/></span><span> <%= com.ffusion.util.HTMLUtil.encode(approvalDecisionsDA[i]) %></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_UserAction"/></span><span> <%= com.ffusion.util.HTMLUtil.encode(userActions[i]) %>  </span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_Reason"/> </span><span> <%= com.ffusion.util.HTMLUtil.encode(daReason[i]) %></span>
			</div>
		</div>
	</div>
</div>
<h3 class="nameSubTitle" ><s:text name="jsp.da_approvals_beneficiary_information"/></h3>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding5">	
																	<tr class="header">
																		<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_FieldName"/></td>
																		<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_OldValue"/></td>
																		<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_NewValue"/></td>
																	</tr>
																<ffi:list collection="CATEGORY_BEAN.daItems" items="payeeDetails">
																	<tr>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="accountNum">
																			<td width="200"><s:text name="da.field.accountNum"/></td>
																		</ffi:cinclude>
																		
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="accountType">
																			<td width="200"><s:text name="da.field.accountType"/></td>
																		</ffi:cinclude>

																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="payeeName">
																			<td width="200"><s:text name="da.field.payeeName"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="nickName">
																			<td width="200"><s:text name="da.field.nickName"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="street">
																			<td width="200"><s:text name="da.field.street"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="street2">
																			<td width="200"><s:text name="da.field.street2"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="city">
																			<td width="200"><s:text name="da.field.city"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="state">
																			<td width="200"><s:text name="da.field.state"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="zipCode">
																			<td width="200"><s:text name="da.field.zipCode"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="country">
																			<td width="200"><s:text name="da.field.country"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="contactName">
																			<td width="200"><s:text name="da.field.contactName"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="payeeDestination">
																			<td width="200"><s:text name="da.field.payeeDestination"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="payeeScope">
																			<td width="200"><s:text name="da.field.payeeScope"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${payeeDetails.fieldName}" value2="memo">
																			<td width="200"><s:text name="da.field.memo"/></td>
																		</ffi:cinclude>
																		<td width="250" class="columndata">
																			<ffi:getProperty name="payeeDetails" property="oldValue"/>			
																		</td>
																		<td width="250" class="columndata">
																			<ffi:getProperty name="payeeDetails" property="newValue"/>
																		</td>
																	</tr>
																	</ffi:list>
														
														<ffi:cinclude value1="${CATEGORY_BEAN.daItems.Size}" value2="">
															<tr>
																<td></td>
																<td width="700" colspan="3">------</td>
															</tr>
														</ffi:cinclude>	</table></div></div>
<h3 class="nameSubTitle"><s:text name="jsp.da_approvals_pending_beneficiary_bank_information"/></h3>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding5">	
							<tr class="header">
								<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_FieldName"/></td>
								<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_OldValue"/></td>
								<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_NewValue"/></td>
							</tr>														
														<%-- Start: Clean up DA information --%>
														<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
														<%-- End: Clean up DA information --%>
					
														<%-- Retrieve category payeeDetails information --%>
														<ffi:object id="GetWireDACategorypayeeDetails" name="com.ffusion.tasks.dualapproval.wiretransfers.GetWireDACategoryDetails"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemId" value="${WireTransferPayeeDA.ID}"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="categorySessionName" value="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_WIRE_BENEFICIARY_BANK%>"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="businessId" value="${SecureUser.BusinessID}"/>
															<ffi:setProperty name="GetWireDACategorypayeeDetails" property="daItemId" value="${WireTransferPayeeDA.daItemId}"/>
														<ffi:process name="GetWireDACategorypayeeDetails"/>
														<ffi:removeProperty name="GetWireDACategorypayeeDetails"/>
														
																<ffi:list collection="CATEGORY_BEAN.daItems" items="beneficiaryDetail">
																	<tr>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="bankName">
																			<td width="200"><s:text name="da.field.bankName"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="street">
																			<td width="200"><s:text name="da.field.street"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="street2">
																			<td width="200"><s:text name="da.field.street2"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="city">
																			<td width="200"><s:text name="da.field.city"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="state">
																			<td width="200"><s:text name="da.field.state"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="zipCode">
																			<td width="200"><s:text name="da.field.zipCode"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="country">
																			<td width="200"><s:text name="da.field.country"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="routingFedWire">
																			<td width="200"><s:text name="da.field.routingFedWire"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="routingSwift">
																			<td width="200"><s:text name="da.field.routingSwift"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="routingChips">
																			<td width="200"><s:text name="da.field.routingChips"/></td>
																		</ffi:cinclude>
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="routingOther">
																			<td width="200"><s:text name="da.field.routingOther"/></td>
																		</ffi:cinclude>	
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="IBAN">
																			<td width="200"><s:text name="da.field.IBAN"/></td>
																		</ffi:cinclude>	
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="correspondentBankAccountNumber">
																			<td width="200"><s:text name="da.field.correspondentBankAccountNumber"/></td>
																		</ffi:cinclude>	
																		<ffi:cinclude value1="${beneficiaryDetail.fieldName}" value2="ID" operator="notEquals">
																			<td width="250" class="columndata">
																				<ffi:getProperty name="beneficiaryDetail" property="oldValue"/>			
																			</td>
																			<td width="250" class="columndata">
																				<ffi:getProperty name="beneficiaryDetail" property="newValue"/>
																			</td>
																		</ffi:cinclude>	
																	</tr>
																	</ffi:list>
																												
														<ffi:cinclude value1="${CATEGORY_BEAN.daItems.Size}" value2="">
															<tr>
																<td></td>
																<td width="700" colspan="3">------</td>
															</tr>
														</ffi:cinclude>	
														</table></div></div>
<h3 class="nameSubTitle"><s:text name="jsp.da_approvals_pending_beneficiary_intermediaries_information"/></h3>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding5">	
							<tr class="header">
								<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_FieldName"/></td>
								<td  class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_OldValue"/></td>
								<td  class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_NewValue"/></td>
							</tr>	
														
														<% int approvalItems = 0; %>
															<%-- Start: Clean up DA information --%>
															<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
															<%-- End: Clean up DA information --%>
															
															<%-- Retrieve category payeeDetails information --%>
															<ffi:object id="GetWireDACategorypayeeDetails" name="com.ffusion.tasks.dualapproval.wiretransfers.GetWireDACategoryDetails"/>
																<ffi:setProperty name="GetWireDACategorypayeeDetails" property="categorySessionName" value="IntermediaryCategories"/>
																<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemId" value="${WireTransferPayeeDA.ID}"/>
																<ffi:setProperty name="GetWireDACategorypayeeDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_WIRE_INTERMEDIARY_BANK%>"/>
																<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
																<ffi:setProperty name="GetWireDACategorypayeeDetails" property="businessId" value="${SecureUser.BusinessID}"/>
																<ffi:setProperty name="GetWireDACategorypayeeDetails" property="daItemId" value="${WireTransferPayeeDA.daItemId}"/>
															<ffi:process name="GetWireDACategorypayeeDetails"/>
															<ffi:removeProperty name="GetWireDACategorypayeeDetails"/>
														
																
																<%
																DAItemCategories categories = (DAItemCategories)session.getAttribute("IntermediaryCategories");
																if (categories != null && categories.size() > 0) {
																for(int counter = 0; counter < categories.size(); counter++){
																	session.setAttribute("CATEGORY_BEAN", categories.get(counter) );
																%>
																<tr>	
																	<td class="transactionHeading" colspan="3"><%=counter+1%> <s:text name="jsp.da_approvals_pending_beneficiary_intermediary_information"/> (<ffi:getProperty name="CATEGORY_BEAN" property="userAction"/>)</td>
																</tr>
																		<ffi:list collection="CATEGORY_BEAN.daItems" items="intermediaryDetails">
																			<tr>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="bankName">
																					<td width="200"><s:text name="da.field.bankName"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="street">
																					<td width="200"><s:text name="da.field.street"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="street2">
																					<td width="200"><s:text name="da.field.street2"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="city">
																					<td width="200"><s:text name="da.field.city"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="state">
																					<td width="200"><s:text name="da.field.state"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="zipCode">
																					<td width="200"><s:text name="da.field.zipCode"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="country">
																					<td width="200"><s:text name="da.field.country"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="routingFedWire">
																					<td width="200"><s:text name="da.field.routingFedWire"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="routingSwift">
																					<td width="200"><s:text name="da.field.routingSwift"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="routingChips">
																					<td width="200"><s:text name="da.field.routingChips"/></td>
																				</ffi:cinclude>
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="routingOther">
																					<td width="200"><s:text name="da.field.routingOther"/></td>
																				</ffi:cinclude>	
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="IBAN">
																					<td width="200"><s:text name="da.field.IBAN"/></td>
																				</ffi:cinclude>	
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="correspondentBankAccountNumber">
																					<td width="200"><s:text name="da.field.correspondentBankAccountNumber"/></td>
																				</ffi:cinclude>
																				
																				<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="ID" operator="notEquals">
																					<td width="250" class="columndata">
																						<ffi:getProperty name="intermediaryDetails" property="oldValue"/>			
																					</td>
																					<td width="250" class="columndata">
																						<ffi:getProperty name="intermediaryDetails" property="newValue"/>
																					</td>
																				</ffi:cinclude>	
																			</tr>
																			<% approvalItems++; %>
																			<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
																		</ffi:list>																		
														<% }
														} %>
																
														<ffi:setProperty name="ApprovalItems" value="<%=String.valueOf(approvalItems)%>"/>
														<ffi:cinclude value1="${ApprovalItems}" value2="0">
															<tr>
																<td></td>
																<td width="700" colspan="3">------ </td>
															</tr>
														</ffi:cinclude>	
														</table></div></div>
														<ffi:removeProperty name="ApprovalItems"/>
														<% approvedPayees++; %>													
														</ffi:cinclude>
													</ffi:cinclude>	
												</ffi:cinclude>
									</ffi:list>
									</ffi:cinclude>

									<%-- For ACH participants --%>
									<ffi:cinclude value1="<%=itemTypes[i]%>" value2="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>">
									<ffi:list collection="ACHPayeesDA" items="achPayeeDA">
										 <ffi:cinclude value1="${achPayeeDA.DaStatus}" value2="<%=IDualApprovalConstants.STATUS_TYPE_REJECTED%>" operator="notEquals">
											
										 <ffi:cinclude value1="${achPayeeDA.IsLastModifiedUser}" value2="<%=IDualApprovalConstants.TRUE%>" operator="notEquals">
										
										 	<ffi:cinclude value1="${achPayeeDA.ID}" value2="<%=ID[i]%>">
										 			<%-- Start: Clean up DA information --%>
													<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
													<%-- End: Clean up DA information --%>
																					
													<%-- Retrieve category details information in session --%>
													<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
														<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${achPayeeDA.ID}"/>
														<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>"/>
														<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
														<ffi:setProperty name="GetDACategoryDetails" property="daItemId" value="${achPayeeDA.daItemId}"/>
													<ffi:process name="GetDACategoryDetails" />
													<ffi:removeProperty name="GetDACategoryDetails"/>
											
<div class="blockWrapper">
	<div  class="blockHead" align="center"><% if(isWiresPresent && isACHPresent) { %>
		    <s:text name="jsp.da_approvals_beneficiary_ach_information"/>
		<% } else if(isWiresPresent) { %>						    
			<s:text name="jsp.da_approvals_beneficiary_information"/>
		<% } else if(isACHPresent) { %>
			<s:text name="jsp.da_approvals_ach_information"/>
		<% } %>		</div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_Name"/></span><span> <ffi:getProperty name="achPayeeDA" property="Name"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_SubmittedDate"/></span><span> <ffi:getProperty name="achPayeeDA" property="SubmittedDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_SubmittedBy"/></span><span> <%= com.ffusion.util.HTMLUtil.encode(submittedByNames[i]) %></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_ApprovalAction"/></span><span> <%= com.ffusion.util.HTMLUtil.encode(approvalDecisionsDA[ i ]) %></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_UserAction"/></span><span> <%= com.ffusion.util.HTMLUtil.encode(userActions[ i ]) %></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_Reason"/></span><span><%= com.ffusion.util.HTMLUtil.encode(daReason[i]) %></span>
			</div>
		</div>
	</div>
</div>
<h3 class="nameSubTitle"><s:text name="jsp.da_approvals_ach_information"/></h3>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding5">	
												<tr class="header">
														<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_FieldName"/></td>
														<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_OldValue"/></td>
														<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_NewValue"/></td>
													</tr>
															<ffi:list collection="CATEGORY_BEAN.daItems" items="payeeDetails">
																<% String achOldValue = ""; String achNewValue = ""; %>
																<tr>
																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="nickName">
																		<td width="200"><s:text name="da.field.ach.nickName"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="name">
																		<td width="200"><s:text name="da.field.ach.name"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="userAccountNumber">
																		<td width="200"><s:text name="da.field.ach.userAccountNumber"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="prenoteStatus">
																		<td width="200"><s:text name="da.field.ach.prenoteStatus"/></td>
                                                                        <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                                                        <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPrenoteStatus${payeeDetails.oldValue}"/>
                                                                        <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                                                        <ffi:process name="ACHResource" />
                                                                        <ffi:getProperty name="ACHResource" property="Resource" assignTo="achOldValue"/>
                                                                        <ffi:removeProperty name="ACHResource"/>

                                                                        <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                                                        <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPrenoteStatus${payeeDetails.newValue}"/>
                                                                        <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                                                        <ffi:process name="ACHResource" />
                                                                        <ffi:getProperty name="ACHResource" property="Resource" assignTo="achNewValue"/>
                                                                        <ffi:removeProperty name="ACHResource"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="payeeGroup">
																		<td width="200"><s:text name="da.field.ach.payeeGroup"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="securePayee">
																		<td width="200"><s:text name="da.field.ach.securePayee"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="destinationCountry">
																		<td width="200"><s:text name="da.field.ach.destinationCountry"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="bankCountryCode">
																		<td width="200"><s:text name="da.field.ach.bankCountryCode"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="currencyCode">
																		<td width="200"><s:text name="da.field.ach.currencyCode"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="foreignExchangeMethod">
																		<td width="200"><s:text name="da.field.ach.foreignExchangeMethod"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="bankName">
																		<td width="200"><s:text name="da.field.ach.bankName"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="routingNumber">
																		<td width="200"><s:text name="da.field.ach.routingNumber"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="bankIdentifierType">
																		<td width="200"><s:text name="da.field.ach.bankIdentifierType"/></td>
																		
																		<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
																		<ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier${payeeDetails.oldValue}"/>
																		<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
																		<ffi:process name="ACHResource" />
																		<ffi:getProperty name="ACHResource" property="Resource" assignTo="achOldValue"/>
																		<ffi:removeProperty name="ACHResource"/>
				
																		<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
																		<ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier${payeeDetails.newValue}"/>
																		<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
																		<ffi:process name="ACHResource" />
																		<ffi:getProperty name="ACHResource" property="Resource" assignTo="achNewValue"/>
																		<ffi:removeProperty name="ACHResource"/>																		
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="accountNumber">
																		<td width="200"><s:text name="da.field.ach.accountNumber"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/></td>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>
																	
																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="accountType">
																		<td width="200"><s:text name="da.field.ach.accountType"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="street">
																		<td width="200"><s:text name="da.field.ach.street"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="city">
																		<td width="200"><s:text name="da.field.ach.city"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="state">
																		<td width="200"><s:text name="da.field.ach.state"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="country">
																		<td width="200"><s:text name="da.field.ach.country"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
																	</ffi:cinclude>

																	<ffi:cinclude value1="${payeeDetails.fieldName}" value2="zipCode">
																		<td width="200"><s:text name="da.field.ach.zipCode"/></td>
																		<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
																		<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
																	</ffi:cinclude>

																	<td width="250" class="columndata">
																		<ffi:cinclude value1="<%= userActions[ i ] %>" value2="Added" operator="notEquals">
																			<ffi:cinclude value1="<%=achOldValue%>" value2="" operator="notEquals">
																				<%=achOldValue%>
																			</ffi:cinclude>
																		</ffi:cinclude>		
																	</td>
																	<td width="250" class="columndata">
																		<ffi:cinclude value1="<%=achNewValue%>" value2="" operator="notEquals">
																			<%=achNewValue%>
																		</ffi:cinclude>	
																	</td>
																</tr>
																</ffi:list>
													
													<ffi:cinclude value1="${CATEGORY_BEAN.daItems.Size}" value2="">
														<tr>
															<td width="700" colspan="3"> ------</td>
														</tr>
													</ffi:cinclude>	</table></div></div>
													<% approvedPayees++; %>													
												</ffi:cinclude>
												</ffi:cinclude>	
											</ffi:cinclude>											
										</ffi:list>
									</ffi:cinclude>	
									<% } %>
									<% } %>
									
									<ffi:cinclude value1="<%=String.valueOf(approvedPayees)%>" value2="0">
										<div>
										<% 	isWiresPresent = false;
											isACHPresent = false;
											for (int i=0; i<ID.length; i++) { 
										%>
									 		<ffi:cinclude value1="<%=itemTypes[i]%>" value2="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>">
									 		  <% isWiresPresent = true; %>
									 		</ffi:cinclude>
									 		<ffi:cinclude value1="<%=itemTypes[i]%>" value2="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>">
									 		  <% isACHPresent = true; %>
									 		</ffi:cinclude>
										<%
											}
									 	%>
										<% if(isWiresPresent && isACHPresent) { %>
										    <s:text name="jsp.da_approvals_pending_payees_please_select_both"/>
										<% } else if(isWiresPresent) { %>						    
											<s:text name="jsp.da_approvals_pending_payees_please_select_beneficiary"/>
										<% } else if(isACHPresent) { %>
											<s:text name="jsp.da_approvals_pending_payees_please_select_ach"/>
										<% } %>
											
										</div>
									</ffi:cinclude>
                   <% } else { %>

						 <div align="center" class="columndata"><s:text name="jsp.da_approvals_pending_payees_add_reason"/></div>
					<% } %>

							<s:form action="dualApprovalAction-confirmPayeesApproval"
									method="post" name="daApprovePayeesConfirmForm" id="daApprovePayeesConfirmForm">
								
								<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<div class="btn-row">
								
								<sj:a id="cancelDAFormButtonOnConfirm" 
									button="true" 
									title="Cancel" 
									onClickTopics="cancelApprovePayeesFormComplete"
									effectDuration="1500"
									><s:text name="jsp.default_82"/>
								</sj:a>&nbsp;
								<ffi:cinclude value1="<%=String.valueOf(approvedPayees)%>" value2="0" operator="notEquals">
									<sj:a id="confirmApprovePayeesDA" 
										formIds="daApprovePayeesConfirmForm" 
										title="Confirm Payee"  
										targets="targetPending"
										button="true"
										onBeforeTopics="submitApprovePayeesVerifyFormBefore"
										onCompleteTopics="confirmApprovePayeesFormComplete"
									  ><s:text name="jsp.default_107"/>
									</sj:a>
								</ffi:cinclude>
								</div>
							</s:form>
</div>