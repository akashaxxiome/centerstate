<%--
<p>This page is used to send the dual approval wire beneficiary information
to system table. It forwards the control to pending approval summary page.
 </p>

Pages that request this page
----------------------------
--

Pages this page requests
------------------------
approvals/approvepayments.jsp

Pages included in this page
---------------------------
--
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>

<ffi:setProperty name="BackURL" value="${SecurePath}approvals/approvepayments.jsp"/>

<%-- For Wire Beneficiaries --%>
<%-- Set search bean to get DA category details --%>
			
<%-- Retrieve multiple category details information in session --%>
<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="<%=IDualApprovalConstants.TRUE%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetDACategoryDetails"/>

<%-- create the AddEditDABeneficiaryToSystem instance and put it in session --%>
<ffi:object id="AddEditDABeneficiaryToSystem" name="com.ffusion.tasks.dualapproval.wiretransfers.AddEditDABeneficiaryToSystem" scope="session"/>
<ffi:process name="AddEditDABeneficiaryToSystem"/>	

<%-- For ACH Participants --%>

<%
	// Retrieve the approval actions and reasons for ACH participants to be approved/reject
	String [] approvalDecisionsDA = (String[]) session.getAttribute("ApprovalDecisionsDA");
	String [] daReasons = (String[]) session.getAttribute("DAReason");
	String [] daItemIds = (String[]) session.getAttribute("DAItemIds");
	String [] userActions = (String[]) session.getAttribute("UserActions");
	String [] itemTypes = (String[]) session.getAttribute("ItemTypes");
%>

<% 
	if (daItemIds != null) {
		for(int i=0; i<daItemIds.length; i++) {
            session.setAttribute("PayeeID", daItemIds[i]);
%>
		<ffi:cinclude value1="<%=itemTypes[i]%>" value2="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>">
			<ffi:cinclude value1="<%=approvalDecisionsDA[i]%>" value2="<%=IDualApprovalConstants.STATUS_TYPE_HOLD%>" operator="notEquals">
				<ffi:cinclude value1="<%=approvalDecisionsDA[i]%>" value2="<%=IDualApprovalConstants.STATUS_TYPE_REJECTED%>" operator="notEquals">

					<ffi:cinclude value1="<%=userActions[i]%>" value2="<%=IDualApprovalConstants.USER_ACTION_ADDED%>">
                        <s:action namespace="/pages/jsp/ach" name="addACHPayeeAction">
                            <s:param name="PayeeID" value="%{#session.PayeeID}"/>
                            <s:param name="ProcessDA" value="true"/>
                        </s:action>
					</ffi:cinclude>
					<ffi:cinclude value1="<%=userActions[i]%>" value2="<%=IDualApprovalConstants.USER_ACTION_MODIFIED%>">
                        <s:action namespace="/pages/jsp/ach" name="editACHPayeeAction">
                            <s:param name="PayeeID" value="%{#session.PayeeID}"/>
                            <s:param name="ProcessDA" value="true"/>
                        </s:action>
					</ffi:cinclude>
					<ffi:cinclude value1="<%=userActions[i]%>" value2="<%=IDualApprovalConstants.USER_ACTION_DELETED%>">
                        <s:action namespace="/pages/jsp/ach" name="deleteACHPayeeAction">
                            <s:param name="PayeeID" value="%{#session.PayeeID}"/>
                            <s:param name="ProcessDA" value="true"/>
                        </s:action>
					</ffi:cinclude>
					
					<ffi:cinclude value1="<%=approvalDecisionsDA[i]%>" value2="<%=IDualApprovalConstants.STATUS_TYPE_APPROVED%>">
						<ffi:object name="com.ffusion.tasks.dualapproval.ApprovePendingChanges" id="ApprovePendingChangesTask" />
						<ffi:setProperty name="ApprovePendingChangesTask" property="businessId" value="${SecureUser.BusinessID}" />
						<ffi:setProperty name="ApprovePendingChangesTask" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>" />
						<ffi:setProperty name="ApprovePendingChangesTask" property="itemId" value="<%=daItemIds[i]%>" />
						<ffi:setProperty name="ApprovePendingChangesTask" property="approveDAItem" value="<%=IDualApprovalConstants.TRUE%>" />
						<ffi:process name="ApprovePendingChangesTask"/>
						<ffi:removeProperty name="ApprovePendingChangesTask"/>
					</ffi:cinclude>
				</ffi:cinclude>

				<ffi:cinclude value1="<%=approvalDecisionsDA[i]%>" value2="<%=IDualApprovalConstants.STATUS_TYPE_REJECTED%>">
					<ffi:object id="RejectPendingChange" name="com.ffusion.tasks.dualapproval.RejectPendingChange"  />
					<ffi:setProperty name="RejectPendingChange" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>" />
					<ffi:setProperty name="REJECTREASON" value="<%=daReasons[i]%>" />
					<ffi:setProperty name="RejectPendingChange" property="itemId" value="<%=daItemIds[i]%>" />
					<ffi:setProperty name="RejectPendingChange" property="validate" value="REJECTREASON" />
					<ffi:process name="RejectPendingChange"/>
					<ffi:removeProperty name="RejectPendingChange"/>
				</ffi:cinclude>
			</ffi:cinclude>
		</ffi:cinclude>
<% 
		}
	}
%>

<%-- Clean up --%>
<ffi:removeProperty name="IsPendingApproval"/>
<ffi:removeProperty name="MultipleCategories"/>
<ffi:removeProperty name="ACHMultipleCategories"/>
<ffi:removeProperty name="<%=SessionNames.APPROVAL_ITEMS_DA%>"/>

<%-- Forward request to pending approval list page --%>
<ffi:include page="${PagesPath}approvals/approvepayments.jsp"/>