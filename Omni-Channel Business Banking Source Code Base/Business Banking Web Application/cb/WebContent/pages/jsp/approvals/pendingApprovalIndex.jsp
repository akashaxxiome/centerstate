<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/approvals/approvals%{#session.minVersion}.css'/>"></link>
<script type="text/javascript" src="<s:url value='/web/js/approvals/approvals%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/approvals/approvals_grid%{#session.minVersion}.js'/>"></script>

<s:include value="/pages/jsp/home/inc/reloadAccounts.jsp"/>

				<div id="desktop" align="center">
				
				   
					<div id="appdashboard">
						<s:include value="pendingApprovalDashboard.jsp"/>
					</div>
					
					<div id="details" style="display:none;">
						<s:include value="apprs_details.jsp"/>
					</div>
	

					<div id="summary" class="marginTop10">
						<%
							String subpage = request.getParameter("subpage");
							if( subpage != null && subpage.equals("pending")) {
						%>
							<div id="approvalGroupSummary" class="portlet gridPannelSupportCls">
									<div class="portlet-header">
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
										<span id="selectedGridTab" class="portlet-title">Pending Transactions</span>
									</ffi:cinclude>
									<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
									    <div class="searchHeaderCls">
											<div class="summaryGridTitleHolder">
												<s:if test="%{#parameters.visibleGrid[0] == 'pendingPayees'}">
													<span id="selectedGridTab" class="selectedTabLbl">Pending Payees</span>
												</s:if>
												<s:else>
													<span id="selectedGridTab" class="selectedTabLbl">Pending Transactions</span>
												</s:else>
											
												<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
												
												<div class="gridTabDropdownHolder">
													<span class="gridTabDropdownItem" id='pendingTransactions' onclick="ns.common.showGridSummary('pendingTransactions')" >
														Pending Transactions
													</span>
													<span class="gridTabDropdownItem" id='pendingPayess' onclick="ns.common.showGridSummary('pendingPayess')">
														Pending Payees
													</span>
												</div>
											</div>
										</div>
									</ffi:cinclude>
									</div>
								<div class="portlet-content">
									<s:include value="approvepayments.jsp?InitPage=true" /> 
								</div> 
						    	<div id="approvalGroupDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
							       <ul class="portletHeaderMenu" style="background:#fff">
							            <li><a href='#' onclick="ns.common.showDetailsHelp('approvalGroupSummary')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
							       </ul>
								</div>	
							</div>
						<% } %>
						
						<div id="targetApprovePaymentsVerifyContainer" class="portlet">
							<%-- <div class="portlet-header">
									<span class="portlet-title" title="jsp.default_395"></span>
								</div> --%>
							<div class="portlet-header">
										<span id="selectedGridTab" class="portlet-title">
											<s:text name="jsp.common_165"/>
										</span>
							</div>
								    
								    
								    
							<div class="portlet-content">
								<div id="targetApprovePaymentsVerify" >
								</div>
							</div>
						</div>
						
					</div>
					
					
					
				</div>
	    							
	    							
	    							
	    							
	


	<script type="text/javascript">
	ns.common.initializePortlet("approvalGroupSummary",false); 
	ns.common.initializePortlet("targetApprovePaymentsVerifyContainer",false); 
	
	$(document).ready(function(){
		$('#targetApprovePaymentsVerifyContainer').hide();
	});
 	</script>
 	


    <sj:dialog id="enableCascadingDialogID" cssClass="approvalsDialog" title="%{getText('jsp.default_185')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>

    <s:url id="displayStallConditionUrl" value="%{#session.PagesPath}/%{#session.approval_display_stall_condition_jsp}" escapeAmp="false">
	   		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	</s:url>

	<sj:dialog id="stallConditionDialogID" cssClass="approvalsDialog" title="%{getText('jsp.default_385')}" href="%{displayStallConditionUrl}"
			modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="650">
	</sj:dialog>

	<sj:dialog id="viewPermissionDialogID" cssClass="approvalsDialog" title="%{getText('jsp.default_463')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>

	<sj:dialog id="deleteApprovalLevelDialogID" cssClass="approvalsDialog" title="%{getText('jsp.approvals_5')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>
