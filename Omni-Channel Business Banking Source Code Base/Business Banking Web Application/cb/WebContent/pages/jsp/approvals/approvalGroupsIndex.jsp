<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/approvals/approvals%{#session.minVersion}.css'/>"></link>
<script type="text/javascript" src="<s:url value='/web/js/approvals/approvals%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/approvals/approvals_grid%{#session.minVersion}.js'/>"></script>

				<div id="desktop" align="center">
				
				    <div id="operationresult">
				        <div id="resultmessage"><s:text name="jsp.default_498"/></div>
				    </div>
				
					<div id="appdashboard">
						<s:include value="approvalGroupsDashboard.jsp"/>
					</div>
	

					<div id="summary" class="marginTop10">
							<div id="approvalGroupSummary" class="portlet">
								<div class="portlet-header">
									<span class="portlet-title" title="%{getText('jsp.approvals_23')}"><s:text name='jsp.approvals_23' /></span>
								</div>
								<div class="portlet-content">
									<s:include value="corpadminapprovalgroup.jsp?InitPage=true" />
								</div>
						    	<div id="approvalGroupDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
							       <ul class="portletHeaderMenu" style="background:#fff">
							            <li><a href='#' onclick="ns.common.showDetailsHelp('approvalGroupSummary')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
							       </ul>
								</div>	
							</div>
					</div>
				</div>

    <sj:dialog id="enableCascadingDialogID" cssClass="approvalsDialog" title="%{getText('jsp.default_185')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>

    <s:url id="displayStallConditionUrl" value="%{#session.PagesPath}/%{#session.approval_display_stall_condition_jsp}" escapeAmp="false">
	   		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	</s:url>

	<sj:dialog id="stallConditionDialogID" cssClass="approvalsDialog" title="%{getText('jsp.default_385')}" href="%{displayStallConditionUrl}"
			modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="650">
	</sj:dialog>

	<sj:dialog id="viewPermissionDialogID" cssClass="approvalsDialog" title="%{getText('jsp.default_463')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>

	<sj:dialog id="deleteApprovalLevelDialogID" cssClass="approvalsDialog" title="%{getText('jsp.approvals_5')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>
	
<script type="text/javascript">
$(document).ready(function(){
	ns.approval.showApprovalsDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	ns.common.initializePortlet("approvalGroupSummary",false); 
});
</script>	
