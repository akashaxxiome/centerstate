<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>




<s:set var="tmpI18nStr" value="%{getText('jsp.approvals_10')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}' />



<ffi:help id="approvals_corpadminapprovalgroup" />
<span class="shortcutPathClass" style="display:none;">approvals_group</span>
<span class="shortcutEntitlementClass" style="display:none;">Approvals Admin</span>

<div align="center" >


	    
	<ffi:setGridURL grid="GRID_corpadminApprovalGroup" name="EditURL" url="/cb/pages/jsp/approvals/editApprovalGroup_input.action?approvalGroupID={0}" parm0="ApprovalsGroupID"/>
	<ffi:setGridURL grid="GRID_corpadminApprovalGroup" name="DeleteURL" url="/cb/pages/jsp/approvals/deleteApprovalGroup_input.action?approvalGroupID={0}" parm0="ApprovalsGroupID"/>	
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/approvals/getApprovalGroups.action?GridURLs=GRID_corpadminApprovalGroup" URLEncrypt="true"/>
	<s:url id="getApprovalGroupsUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	
	<sjg:grid  
		id="approvalGroupsGrid"  
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{getApprovalGroupsUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="approvalGroupsGridCompleteEvents"
		> 
		
		<sjg:gridColumn name="groupName" index="APPROVALGROUPNAME" title="%{getText('jsp.default_226')}" sortable="true" width="500" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="approvalsGroupID" index="approvalsGroupID" title="%{getText('jsp.default_27')}" sortable="false" width="80" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/> 
		<sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>	
	</sjg:grid>
</div>

<sj:dialog id="approvalGroupDialogID" onCloseTopics="closeAddGroupDialog" cssClass="approvalsDialog" title="%{getText('jsp.approvals_2')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="700">
</sj:dialog>
<sj:dialog id="editApprovalGroupDialogID" cssClass="approvalsDialog" title="%{getText('jsp.approvals_25')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="700">
</sj:dialog>
<sj:dialog id="deleteApprovalGroupDialogID" cssClass="approvalsDialog" title="%{getText('jsp.approvals_26')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="700">
</sj:dialog>

<script>
	$('#approvalGroupDialogID').addHelp(function(){
		var helpFile = $('#approvalGroupDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	$('#editApprovalGroupDialogID').addHelp(function(){
		var helpFile = $('#editApprovalGroupDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	
</script>