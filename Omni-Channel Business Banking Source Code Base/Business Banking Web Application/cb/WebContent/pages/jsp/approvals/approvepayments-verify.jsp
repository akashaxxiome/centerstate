<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- set the properties required by this page --%>
<ffi:setProperty name='PageHeading' value="Verify Pending Approvals Actions"/>
<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name="subMenuSelected" value="approvals"/>

<%-- include the approvals constants page --%>
<ffi:include page="${PagesPath}inc/include-cb-approval-constants.jsp"/>
<ffi:help id="approvals_approvepayments-verify" />
<br/>
<ffi:include page="${PagesPath}common/include-approvepayments-verify.jsp" />
