<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>

<div id="include-daApprovepayments">

<ffi:setProperty name="BackURL" value="${SecurePath}approvals/approvepayments.jsp"/>

<style type="text/css">
.approvals-left-cell
{
	border-left-color: inherit;
	border-left-style: solid;
	border-left-width: 1px;
	border-right: none;
}

.disable-input
{
	background: #dddddd;
}
</style>

<%-- Include DA approval Grid js file --%>
<script src="<s:url value='/web'/>/js/approvals/da_pending_approval_grid.js" type="text/javascript"></script>

<script language="JavaScript">
<!--
$(function(){
	// Update the ApprovalSmallPanel in leftbar of the Page
	$.publish('refreshApprovalSmallPanel');
	
	$("#selectOptionDA").selectmenu({width: 160});
});

// --></script>

<%-- Start : Session clean up --%>
<ffi:removeProperty name="ApprovalDecisionsDA"/>
<ffi:removeProperty name="DAReason"/>
<ffi:removeProperty name="DAItemIds"/>
<ffi:removeProperty name="counter"/>
<%-- End : Session clean up --%>


<%-- For pending wire payee list --%>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BENEFICIARY_MANAGEMENT %>">
				
	<%-- Retrieve multiple category details information in session --%>
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="<%=IDualApprovalConstants.TRUE%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetDACategoryDetails"/>
	<ffi:removeProperty name="GetDACategoryDetails"/>
														
	<%-- Clean up session --%>
	<ffi:removeProperty name="<%=SessionNames.WIRE_TRANSFER_PAYEES_DA%>"/>	
	<ffi:removeProperty name="<%=SessionNames.WIRE_TRANSFER_PAYEES%>"/>	

	<%-- create the GetWireTransferPayees instance and put it in session --%>
	<ffi:object id="GetWireTransferPayees" name="com.ffusion.tasks.wiretransfers.GetWireTransferPayees" scope="session"/>
	<ffi:setProperty name="GetWireTransferPayees" property="PayeeDestination" value=""/>
	<ffi:process name="GetWireTransferPayees"/>
	<ffi:removeProperty name="GetWireTransferPayees"/>
	
	<%-- create the GetWireTransferPayeesDA instance and put it in session --%>
	<ffi:object id="GetWireTransferPayeesDA" name="com.ffusion.tasks.dualapproval.wiretransfers.GetWireTransferPayeesDA" scope="session"/>
	<ffi:setProperty name="GetWireTransferPayeesDA" property="PayeeDestination" value=""/>
	<ffi:process name="GetWireTransferPayeesDA"/>
	<ffi:removeProperty name="GetWireTransferPayeesDA"/>	

</ffi:cinclude>

<%-- For pending ACH payee list --%>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">

    <s:action namespace="/pages/jsp/ach" name="viewACHPayeeAction_getPendingDAPayees" />

    <% if (request.getAttribute("PendingDAPayees") != null) {
        session.setAttribute(SessionNames.ACHPAYEES_DA, request.getAttribute("PendingDAPayees"));
    }%>

</ffi:cinclude>

<%-- Retrieve all pending payees for Wires and ACH as well and put it in session --%>
<ffi:object id="GetPendingApprovalPayees" name="com.ffusion.tasks.dualapproval.GetPendingApprovalPayees" scope="session" />
<ffi:setProperty name="GetPendingApprovalPayees" property="achPayeesDA"	value="<%=SessionNames.ACHPAYEES_DA%>" />
<ffi:setProperty name="GetPendingApprovalPayees" property="pendingPayeesDA" value="<%=SessionNames.APPROVAL_ITEMS_DA%>" />
<ffi:setProperty name="GetPendingApprovalPayees" property="wireTransferPayeesDA" value="<%=SessionNames.WIRE_TRANSFER_PAYEES_DA%>" />
<ffi:process name="GetPendingApprovalPayees" />	
<ffi:removeProperty name="GetPendingApprovalPayees"/>	

<ffi:cinclude value1="${ApprovalItemsDA.Size}" value2="0" operator="equals">
	<div class="noPortletMsgCls"  align="center" ><s:text name="jsp.approval_no_pending_payees" /></div>
</ffi:cinclude>
<ffi:cinclude value1="${ApprovalItemsDA.Size}" value2="0" operator="notEquals">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pendingPayeesFormAction">
		<tr>
			<td align="left" width="100%">
			<ffi:setProperty name="PayeeListSize" value="${ApprovalItemsDA.Size}"/>
				<table border="0" cellspacing="3" cellpadding="10">
					<tr>
						 <td class="sectionsubhead" align="left">&nbsp;&nbsp;&nbsp; <s:text name="jsp.common_27"/> &nbsp;
							<select id="selectOptionDA" class="txtbox" name="ApproveOptionsDA">
								<option value="Hold"><s:text name="jsp.default_228"/></option>
								<option value="Approved"><s:text name="jsp.common_28"/></option>
								<option value="Rejected"><s:text name="jsp.common_139"/></option>
							</select>
						</td>
					<td class="sectionsubhead" nowrap>
						<%-- No need to display button if there is no pending payee for approval/rejection --%>
						<%--
						<ffi:cinclude value1="${ApprovalItemsDA.Size}" value2="" operator="equals">
							<sj:a id="applyDAButtonID" button="true" title="%{getText('jsp.common_20')}"><s:text name="jsp.common_20"/></sj:a>
						</ffi:cinclude>
						--%>

							<sj:a id="applyDAButtonID" button="true" onClickTopics="onApplyDAButtonClick" title="%{getText('jsp.common_20')}"><s:text name="jsp.common_20"/></sj:a>

							<script type="text/javascript">
								$.subscribe('onApplyDAButtonClick', function(event,data) {
									ns.approval.applyToAllComboBoxesDA('selectOptionDA');
								});
							</script>

					</td>
					</tr>
				</table>

			</td>
		</tr>
	</table>

	  <s:form
		action="dualApprovalAction-verifyBeneficiaryApproval"
		method="post" name="daPendingApprovalForm" id="daPendingApprovalForm" namespace="/pages/dualapproval" theme="simple">
		<input type="hidden"
		name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />

	<ffi:setGridURL grid="GRID_approvePayees" name="ACHPayeeViewURL" url="/cb/pages/jsp/ach/viewACHPayeeAction_daPendingApprovalView.action?daItemId={0}&ID={1}&Action={2}&SubmittedDate={3}&SubmittedBy={4}" parm0="DaItemId" parm1="ID" parm2="Action" parm3="SubmittedDate" parm4="SubmittedByName"/>
	<ffi:setGridURL grid="GRID_approvePayees" name="WirePayeeViewURL" url="/cb/pages/jsp/wires/initiateWirePayee_daPendingApprovalView.action?daItemId={0}&ID={1}&Action={2}&SubmittedDate={3}&SubmittedBy={4}&BeneficiaryName={5}" parm0="DaItemId" parm1="ID" parm2="Action" parm3="SubmittedDate" parm4="SubmittedByName" parm5="PayeeName"/>

	<ffi:setProperty name="approveDAPayeesTempURL" value="/pages/jsp/approvals/includePendingApprovalGridDA.action?GridURLs=GRID_approvePayees" URLEncrypt="true"/>
	<s:url id="getApproveDAPayeesUrl" value="%{#session.approveDAPayeesTempURL}" escapeAmp="false"/>
	
	<%--Skip call to load grid data when this grid is not visible --%>
	<s:if test="%{#parameters.visibleGrid[0] == 'pendingPayees'}">
		<ffi:setProperty name="gridDataType" value="json"/>
	</s:if>
	<s:else>
		<ffi:setProperty name="gridDataType" value="local"/>
	</s:else>
	
	<sjg:grid
		id="daApprovePaymentsGrid" caption="" 
		dataType="%{#session.gridDataType}" 
		sortable="true" sortname="SUBMISSIONDATE"  
		href="%{getApproveDAPayeesUrl}" pager="true" gridModel="gridModel"
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false" shrinkToFit="true"
		scroll="false" scrollrows="true" viewrecords="true" 
		onGridCompleteTopics="daPendingApprovalGridCompleteEvents">
		
		<sjg:gridColumn name="submittedDate" index="SUBMISSIONDATE" title="%{getText('jsp.approval_daDate_1')}" sortable="true" width="90" cssClass="datagrid_dateColumn"/>
		<sjg:gridColumn name="itemType" index="ITEMTYPE" title="%{getText('jsp.approval_daActivityType_2')}" sortable="true" width="90" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="payeeName" index="PAYEENAME" title="%{getText('jsp.approval_daPayeeName_3')}" sortable="true" width="100" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="submittedByName" index="SUBMITTINGUSERNAME" title="%{getText('jsp.approval_daSubmittedBy_4')}" sortable="true" width="100" cssClass="datagrid_textColumn"/>

		<sjg:gridColumn name="daStatus" index="daStatus" title="%{getText('jsp.approval_daStatus_8')}" sortable="true" width="80" hidden="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="isLastModifiedUser" index="isLastModifiedUser" title="%{getText('jsp.approval_daIsLastModifiedUser_6')}" sortable="true" width="100" hidden="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="submittedBy" index="submittedBy" title="%{getText('jsp.approval_daSubmittedBy_4')}" sortable="true" width="120" hidden="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="action" index="action" title="%{getText('jsp.approval_daAction_8')}" sortable="true" width="80" hidden="true" cssClass="datagrid_actionIcons"/>

		<sjg:gridColumn name="ID" index="itemId" title="%{getText('jsp.approval_daAction_7')}" sortable="false" width="40" hidden="true" cssClass="datagrid_textColumn"/>

		<sjg:gridColumn name="links" index="links" title="%{getText('jsp.approval_daAction_7')}" sortable="false" width="150" formatter="ns.approval.formatDAPendingApprovalActionLinks" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="map.SanitizedReasonDA" index="sanitizedReasonDA" title="%{getText('jsp.common_144')}" hidden="true" sortable="false" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="daItemId" index="daItemId" title="%{getText('jsp.approval_daItemId_9')}" hidden="true" sortable="false"  cssClass="datagrid_textColumn"/> 
		<sjg:gridColumn name="map.DaViewURL" index="daViewURL" title="%{getText('jsp.approval_daViewURL_10')}" sortable="true" width="100" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
	</sjg:grid><br/>
	<script>
		ns.common.disableSortableRows("#daApprovePaymentsGrid");
	</script>
	
	<ffi:cinclude value1="${ApprovalItemsDA.Size}" value2="" operator="notEquals">
		<ffi:cinclude value1="${ApprovalItemsDA.Size}" value2="0" operator="notEquals">
		<div align="center">
		<sj:a id="submitDAPendingApprovalFormLink"
			formIds="daPendingApprovalForm" 
			targets="targetApprovePaymentsVerify" 
			button="true"
			title="%{getText('jsp.default_395.1')}" 
			validate="true" 
			validateFunction="customValidation"
			onclick="removeValidationErrors();" 
			onCompleteTopics="daSubmitPendingApprovalFormComplete"
			onSuccessTopics="daSubmitPendingApprovalFormSuccessTopics"
			><s:text name="jsp.default_395"/></sj:a>

			<br/><br/>
			</div>
		
		<script>
			/** View pending information dialog script. */
			$('#viewDAPendingApprovalDialogID').addHelp(function(){
				var helpFile = $('#viewDAPendingApprovalDialogID').find('.moduleHelpClass').html();
				callHelp('/cb/pages/help/', '/cb/pages/help/' + helpFile);
			});	
		</script>
		</ffi:cinclude>
	</ffi:cinclude>

	</s:form>

	<sj:dialog 
		id="viewDAPendingApprovalDialogID" 
		cssClass="includeApprovalDialog"
		autoOpen="false" 
		modal="true" 
		title="%{getText('jsp.common_167')}"
		height="550"
		width="800"
		resizable="false"
		showEffect="fold" 
		hideEffect="clip" 
		buttons="{ 
			'%{getText(\"jsp.default_175\")}':function() { ns.common.closeDialog(); } 
		}"						    		
	>
	</sj:dialog>
</ffi:cinclude>
</div>
<ffi:removeProperty name="gridDataType"/>