<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.HashMap,
				 com.ffusion.csil.core.common.PaymentEntitlementsDefines" %>
<%@ page import="com.ffusion.beans.approvals.*" %>
<%@ page import="com.ffusion.tasks.approvals.IApprovalsTask" %>

<%

	String operationName = "";
	String objectType = "";
	String objectID = "";
	String locationLimit = "";
	String periodOfLimit = "";
	String groupName = "";
	String exceedLimitsComment = "";

	ApprovalsDecision decision = (ApprovalsDecision)session.getAttribute( "CURRENT_DECISION" );
	ApprovalsItemError itemError = (ApprovalsItemError)session.getAttribute( "CURRENT_ERROR" );
	HashMap limitsMap =
	    ( HashMap )session.getAttribute( IApprovalsTask.APPROVALS_EXCEEDED_LIMITS_MAP );

	if( limitsMap != null && limitsMap.containsKey( decision ) ) {
		com.ffusion.csil.beans.entitlements.Limits limits =
			(com.ffusion.csil.beans.entitlements.Limits)limitsMap.get( decision );
			
		int size = limits.size();
	    for( int i = 0; i < size; i++ ) {
	    	com.ffusion.csil.beans.entitlements.Limit limit =
			(com.ffusion.csil.beans.entitlements.Limit )limits.get(i);
		    com.ffusion.csil.beans.entitlements.Entitlement ent = limit.getEntitlement();

	    	//populate variables with relavent information
	    	operationName 		= ent.getOperationName();
	    	objectType 		= ent.getObjectType();
	    	objectID		= ent.getObjectId();

	    	String memberType = limit.getMemberType();
	    	exceedLimitsComment = null;
	    	locationLimit = null;

	    	int period = limit.getPeriod();

	    	//checking the type of period
	    	if ( period == com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION ) {
	    		periodOfLimit = "per-transaction";
				if (operationName != null && operationName.indexOf(PaymentEntitlementsDefines.ACH_PAYMENT_ENTRY) >=0 )
				{
		    		periodOfLimit = "per-batch";
				}
	    	} else if ( period == com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY ) {
	    		periodOfLimit = "daily";
	    	} else if ( period == com.ffusion.csil.beans.entitlements.Limit.PERIOD_WEEK ) {
	    		periodOfLimit = "weekly";
	    	} else if ( period == com.ffusion.csil.beans.entitlements.Limit.PERIOD_MONTH ) {
	    		periodOfLimit = "monthly";
	    	}

	    	if ( memberType != null )
	    	{
	    		locationLimit = "user";
	    	} else {

        	%>

	    	<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
			<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="<%= Integer.toString( limit.getGroupId() )%>"/>
	    	<ffi:process name="GetEntitlementGroup"/>

        	<%
	    		com.ffusion.csil.beans.entitlements.EntitlementGroup entGroup =
	    			( com.ffusion.csil.beans.entitlements.EntitlementGroup )session.getAttribute( com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP );
	    		if ( entGroup.getEntGroupType().equals( com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_DIVISION ) ) {
	    			groupName = entGroup.getGroupName();
	    			locationLimit = "division " + groupName + " ";
	    		} else if ( entGroup.getEntGroupType().equals( com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_GROUP ) ) {
	    			groupName = entGroup.getGroupName();
	    			locationLimit = "group " + groupName + " ";
	    		} else if ( entGroup.getEntGroupType().equals( com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS ) ) {
		    		locationLimit = "business";
		    	} else {
	    			locationLimit = "bank";
	    		}

	    	}

	    	if ( locationLimit.equalsIgnoreCase("bank") ) {
	    		exceedLimitsComment = "A bank-set " + periodOfLimit + " ";
	    		if ( operationName != null && !operationName.equals( "" ) ) {
	    			exceedLimitsComment += operationName + " ";
	    		}
	    		exceedLimitsComment += "limit";
	    		} else {
	    			exceedLimitsComment = "A " + periodOfLimit + " ";
	    		if ( operationName != null && !operationName.equals( "" ) ) {
	    			exceedLimitsComment += operationName + " ";
	    		}
	    		exceedLimitsComment += "limit ";
	    		if ( objectType != null && !objectType.equals( "" ) ) {
	    			exceedLimitsComment += "on " + objectType + " " + objectID + " ";
	    		}
	    		exceedLimitsComment += "for the " + locationLimit;
	    	}

	    	itemError.set("ExceedLimitsComment", exceedLimitsComment);
				
	    }
	}
	session.removeAttribute( "CURRENT_DECISION" );
	session.removeAttribute( "CURRENT_ERROR" );
	%>
	
