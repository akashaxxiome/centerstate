<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name='PageHeading' value="${ApprovalsHeading}"/>
<ffi:setProperty name='PageText' value=''/>
<ffi:help id="approvals_approvepayments-viewbillpay"/>

<%-- Include approval include-approval-view-details-init.jsp page --%>
<ffi:include page="${PagesPath}/common/include-approval-view-details-init.jsp"/>
<ffi:removeProperty name="ItemID"/>

<div class="approvalDialogHt">
		<ffi:include page="${PagesPath}/common/include-approval-view-details-header.jsp"/>
		
		<div>
			<ffi:include page="${PagesPath}/common/include-view-payment-details.jsp"/>
		</div>						
</div>
		    

