<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Include approval include-approval-view-details-init.jsp page --%>
<ffi:include page="${PagesPath}/common/include-approval-view-details-init.jsp"/>
<ffi:removeProperty name="ItemID"/>

<ffi:cinclude value1="${wireInBatch}" value2="true" operator="equals">
    <ffi:object id="SetWireTransfer" name="com.ffusion.tasks.wiretransfers.SetWireTransfer" scope="session" />
    <ffi:setProperty name="SetWireTransfer" property="CollectionSessionName" value="${collectionName}"/>
    <ffi:cinclude value1="${ID}" value2="" operator="notEquals">
        <ffi:setProperty name="SetWireTransfer" property="ID" value="${ID}"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${ID}" value2="" operator="equals">
        <ffi:setProperty name="SetWireTransfer" property="ID" value=""/>
        <ffi:setProperty name="SetWireTransfer" property="TrackingID" value="${trackingID}"/>
    </ffi:cinclude>
    <ffi:process name="SetWireTransfer"/>
</ffi:cinclude>

<ffi:cinclude value1="${WireTransferFrequencies}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.ResourceList" id="WireTransferFrequencies" scope="session"/>
    <ffi:setProperty name="WireTransferFrequencies" property="ResourceFilename" value="com.ffusion.beansresources.wiretransfers.resources"/>
    <ffi:setProperty name="WireTransferFrequencies" property="ResourceID" value="WireFrequencies"/>
    <ffi:process name="WireTransferFrequencies" />
</ffi:cinclude>

<ffi:cinclude value1="${WireTransferFrequenciesNames}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.Resource" id="WireTransferFrequenciesNames" scope="session"/>
    <ffi:setProperty name="WireTransferFrequenciesNames" property="ResourceFilename" value="com.ffusion.beansresources.wiretransfers.resources"/>
    <ffi:process name="WireTransferFrequenciesNames" />
</ffi:cinclude>

<ffi:removeProperty name="wireInBatch"/> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<ffi:help id="approvals_approvepayments-viewwire"/>
<div>
	<s:set var="tmpI18nStr" value="%{getText('jsp.approvals_20')}" scope="request" />
	<ffi:setProperty name='PageHeading' value="${tmpI18nStr}"/>
<span id="PageHeading" style="display:none"><ffi:getProperty name="PageHeading"/></span>
<ffi:include page="${PagesPath}/common/include-approval-view-wire-details.jsp"/> 
</div>
	