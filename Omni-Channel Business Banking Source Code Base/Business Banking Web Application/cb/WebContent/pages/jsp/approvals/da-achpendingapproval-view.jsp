<%--
<p>This is the view page for pending ACh participants. This page will show
changes made by users in ACH participants. If user clicks the confirm button then it
save those changes into the back end.
 </p>

Pages that request this page
----------------------------
	approvepayments.jsp
	Submit button

Pages this page requests
------------------------
--

Pages included in this page
---------------------------
inc/timeout.jsp
inc/nav_menu_top_js.jsp
inc/nav_header.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<%-- set the properties required by this page --%>
<s:set var="tmpI18nStr" value="%{getText('jsp.da_approvals_ach_information')}" scope="request" />
<ffi:setProperty name='PageHeading' value="${tmpI18nStr}"/>

<span id="PageHeading" style="display:none"><ffi:getProperty name="PageHeading"/></span>
<%-- <div><s:text name="jsp.da_approvals_pending_participant_changes"/></div> --%>
<%-- Start: Clean up DA information --%>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
<%-- End: Clean up DA information --%>

<%
int approvedWires = 0;										
%>

<%-- Retrieve category details information in session --%>
<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${ID}"/>
<ffi:setProperty name="GetDACategoryDetails" property="daItemId" value="${daItemId}"/>
<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>"/>
<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetDACategoryDetails" />
<ffi:removeProperty name="GetDACategoryDetails"/>
<div class="blockWrapper label150 tableData">
	<div  class="blockHead"><s:text name="jsp.da_approvals_pending_participant_changes"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_Name"/></span><span> <ffi:getProperty name="PayeeName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_SubmittedDate"/></span><span> <ffi:getProperty name="SubmittedDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_SubmittedBy"/></span><span> <ffi:getProperty name="SubmittedBy"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_ApprovalAction"/></span><span> <ffi:getProperty name="Action"/></span>
			</div>
		</div>
	</div>
</div>
<h3 class="nameSubTitle"><s:text name="jsp.da_approvals_ach_information"/></h3>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">	
												<tr class="header">
													<td width="10"></td>
													<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_FieldName"/></td>
													<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_OldValue"/></td>
													<td colspan="2" class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_NewValue"/></td>
												</tr>
									<ffi:list collection="CATEGORY_BEAN.daItems" items="payeeDetails">
										<% String achOldValue = ""; String achNewValue = ""; %>
										<tr>
											<td align="left"></td>
											<td width="100" class="columndata">
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="nickName">
													<s:text name="da.field.ach.nickName"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="name">
													<s:text name="da.field.ach.payeeName"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="userAccountNumber">
													<s:text name="da.field.ach.userAccountNumber"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="prenoteStatus">
													<s:text name="da.field.ach.prenoteStatus"/>

                                                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPrenoteStatus${payeeDetails.oldValue}"/>
                                                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                                    <ffi:process name="ACHResource" />
                                                    <ffi:getProperty name="ACHResource" property="Resource" assignTo="achOldValue"/>
                                                    <ffi:removeProperty name="ACHResource"/>

                                                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHPrenoteStatus${payeeDetails.newValue}"/>
                                                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                                    <ffi:process name="ACHResource" />
                                                    <ffi:getProperty name="ACHResource" property="Resource" assignTo="achNewValue"/>
                                                    <ffi:removeProperty name="ACHResource"/>

												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="payeeGroup">
													<s:text name="da.field.ach.payeeGroup"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="securePayee">
													<s:text name="da.field.ach.securePayee"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="destinationCountry">
													<s:text name="da.field.ach.destinationCountry"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="bankCountryCode">
													<s:text name="da.field.ach.bankCountryCode"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="currencyCode">
													<s:text name="da.field.ach.currencyCode"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="foreignExchangeMethod">
													<s:text name="da.field.ach.foreignExchangeMethod"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="bankName">
													<s:text name="da.field.ach.bankName"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="routingNumber">
													<s:text name="da.field.ach.routingNumber"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="bankIdentifierType">
													<s:text name="da.field.ach.bankIdentifierType"/>
													<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
													<ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier${payeeDetails.oldValue}"/>
													<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
													<ffi:process name="ACHResource" />
													<ffi:getProperty name="ACHResource" property="Resource" assignTo="achOldValue"/>
													<ffi:removeProperty name="ACHResource"/>

													<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
													<ffi:setProperty name="ACHResource" property="ResourceID" value="DFINumberQualifier${payeeDetails.newValue}"/>
													<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
													<ffi:process name="ACHResource" />
													<ffi:getProperty name="ACHResource" property="Resource" assignTo="achNewValue"/>
													<ffi:removeProperty name="ACHResource"/>
													
												</ffi:cinclude>	
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="accountNumber">
													<s:text name="da.field.ach.accountNumber"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>	
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="accountType">
													<s:text name="da.field.ach.accountType"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue"/>	
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue"/>
												</ffi:cinclude>	
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="street">
													<s:text name="da.field.ach.street"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
												</ffi:cinclude>	
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="city">
													<s:text name="da.field.ach.city"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="state">
													<s:text name="da.field.ach.state"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="country">
													<s:text name="da.field.ach.country"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${payeeDetails.fieldName}" value2="zipCode">
													<s:text name="da.field.ach.zipCode"/>
													<ffi:getProperty name="payeeDetails" property="oldValue" assignTo="achOldValue" encodeAssign="true"/>
													<ffi:getProperty name="payeeDetails" property="newValue" assignTo="achNewValue" encodeAssign="true"/>
												</ffi:cinclude>
											</td>
											<td width="200" class="columndata">
												<ffi:cinclude value1="<%=achOldValue%>" value2="" operator="notEquals">
													<%=achOldValue%>
												</ffi:cinclude>		
											</td>
											<td width="250" class="columndata">
												<ffi:cinclude value1="<%=achNewValue%>" value2="" operator="notEquals">
													<%=achNewValue%>
												</ffi:cinclude>	
											</td>
										</tr>
									</ffi:list>
</table></div></div>

