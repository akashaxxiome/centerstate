<%--
<p>This is the view page for pending wire beneficiaries. This page will show
changes made by users in wire beneficiaries. If user clicks the confirm button then it
save those changes into the back end.
 </p>

Pages that request this page
----------------------------
	approvepayments.jsp
	Submit button

Pages this page requests
------------------------
--

Pages included in this page
---------------------------
inc/timeout.jsp
inc/nav_menu_top_js.jsp
inc/nav_header.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.beans.dualapproval.DAItemCategories"%>

<%-- set the properties required by this page --%>
<s:set var="tmpI18nStr" value="%{getText('jsp.da_approvals_beneficiary_information')}" scope="request" />
<ffi:setProperty name='PageHeading' value="${tmpI18nStr}"/>

<ffi:setProperty name='Pending Wire Beneficiary changes'/>

	<span id="PageHeading" style="display:none"><ffi:getProperty name="PageHeading"/></span>
	<%-- <div><s:text name="jsp.da_approvals_pending_beneficiary_changes"/></div> --%>
	<%-- Start: Clean up DA information --%>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
	<%-- End: Clean up DA information --%>
	
	<%
		int approvedWires = 0;										
	%>
	<%-- Retrieve category payeeDetails information --%>
	<ffi:object id="GetWireDACategorypayeeDetails" name="com.ffusion.tasks.dualapproval.wiretransfers.GetWireDACategoryDetails"/>
		<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemId" value="${ID}"/>
		<ffi:setProperty name="GetWireDACategorypayeeDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_WIRE_PAYEE%>"/>
		<ffi:setProperty name="GetWireDACategorypayeeDetails" property="daItemId" value="${daItemId}"/>
		<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
		<ffi:setProperty name="GetWireDACategorypayeeDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetWireDACategorypayeeDetails"/>
	<div class="tableData blockWrapper label150">
	<div  class="blockHead"><s:text name="jsp.da_approvals_pending_beneficiary_changes"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_Name"/></span><span> <ffi:getProperty name="BeneficiaryName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_SubmittedDate"/></span><span> <ffi:getProperty name="SubmittedDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_SubmittedBy"/></span><span> <ffi:getProperty name="SubmittedBy"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionLabel"><s:text name="jsp.da_approvals_pending_payees_ApprovalAction"/> </span><span><ffi:getProperty name="Action"/></span>
			</div>
		</div>
	</div>
</div>
<h3 class="nameSubTitle"><s:text name="jsp.da_approvals_beneficiary_information"/></h3>
<div class="paneWrapper">
  	<div class="paneInnerWrapper">
		    <table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">	
				<tr class="header">											
					<td >&nbsp;</td>
					<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_FieldName"/></td>
					<td colspan="1" class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_OldValue"/></td>
					<td colspan="4" class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_NewValue"/></td>
				</tr>
				<ffi:list collection="CATEGORY_BEAN.daItems" items="payeeDetails">
					<tr>
						<td></td>
						<td width="200" class="columndata">
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="accountNum">
								<s:text name="da.field.accountNum"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="accountType">
								<s:text name="da.field.accountType"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="payeeName">
								<s:text name="da.field.payeeName"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="nickName">
								<s:text name="da.field.nickName"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="street">
								<s:text name="da.field.street"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="street2">
								<s:text name="da.field.street2"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="city">
								<s:text name="da.field.city"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="state">
								<s:text name="da.field.state"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="zipCode">
								<s:text name="da.field.zipCode"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="country">
								<s:text name="da.field.country"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="contactName">
								<s:text name="da.field.contactName"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="payeeDestination">
								<s:text name="da.field.payeeDestination"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="payeeScope">
								<s:text name="da.field.payeeScope"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${payeeDetails.fieldName}" value2="memo">
								<s:text name="da.field.memo"/>
							</ffi:cinclude>												
						</td>
						<td width="250" class="columndata">
							<ffi:getProperty name="payeeDetails" property="oldValue"/>			
						</td>
						<td width="250" class="columndata">
							<ffi:getProperty name="payeeDetails" property="newValue"/>
						</td>
					</tr>
				</ffi:list>
				<ffi:cinclude value1="${CATEGORY_BEAN.daItems.Size}" value2="">
					<tr>
						<td ></td>
						<td width="700" colspan="3"> ------</td>
					</tr>
				</ffi:cinclude>	</table></div></div>
<h3 class="nameSubTitle"><s:text name="jsp.da_approvals_pending_beneficiary_bank_information"/></h3>
				<div class="paneWrapper">
  					<div class="paneInnerWrapper">
		    			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">	
							<tr class="header">											
								<td >&nbsp;</td>
								<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_FieldName"/></td>
								<td colspan="1" class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_OldValue"/></td>
								<td colspan="4" class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_NewValue"/></td>
							</tr>
									<%-- Start: Clean up DA information --%>
									<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
									<%-- End: Clean up DA information --%>

									<%-- Retrieve category payeeDetails information --%>
									<ffi:object id="GetWireDACategorypayeeDetails" name="com.ffusion.tasks.dualapproval.wiretransfers.GetWireDACategoryDetails"/>
										<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemId" value="${ID}"/>
										<ffi:setProperty name="GetWireDACategorypayeeDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_WIRE_BENEFICIARY_BANK%>"/>
										<ffi:setProperty name="GetWireDACategorypayeeDetails" property="daItemId" value="${daItemId}"/>
										<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
										<ffi:setProperty name="GetWireDACategorypayeeDetails" property="businessId" value="${SecureUser.BusinessID}"/>
									<ffi:process name="GetWireDACategorypayeeDetails"/>

									<ffi:list collection="CATEGORY_BEAN.daItems" items="beneficiaryDetails">
										<tr>
											<td ></td>
											<td width="200" class="columndata">
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="bankName">
													<s:text name="da.field.bankName"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="street">
													<s:text name="da.field.street"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="street2">
													<s:text name="da.field.street2"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="city">
													<s:text name="da.field.city"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="state">
													<s:text name="da.field.state"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="zipCode">
													<s:text name="da.field.zipCode"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="country">
													<s:text name="da.field.country"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="routingFedWire">
													<s:text name="da.field.routingFedWire"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="routingSwift">
													<s:text name="da.field.routingSwift"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="routingChips">
													<s:text name="da.field.routingChips"/>
												</ffi:cinclude>
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="routingOther">
													<s:text name="da.field.routingOther"/>
												</ffi:cinclude>	
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="IBAN">
													<s:text name="da.field.IBAN"/>
												</ffi:cinclude>	
												<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="correspondentBankAccountNumber">
													<s:text name="da.field.correspondentBankAccountNumber"/>
												</ffi:cinclude>	
											</td>
											<ffi:cinclude value1="${beneficiaryDetails.fieldName}" value2="ID" operator="notEquals">
												<td width="250" class="columndata">
													<ffi:getProperty name="beneficiaryDetails" property="oldValue"/>			
												</td>
												<td width="250" class="columndata">
													<ffi:getProperty name="beneficiaryDetails" property="newValue"/>
												</td>
											</ffi:cinclude>	
										</tr>
									</ffi:list>
									
									<ffi:cinclude value1="${CATEGORY_BEAN.daItems.Size}" value2="">
										<tr>
											<td ></td>
											<td width="700" colspan="3"> ------</td>
										</tr>
									</ffi:cinclude>	</table></div></div>
		<h3 class="nameSubTitle"><s:text name="jsp.da_approvals_pending_beneficiary_intermediaries_information"/></h3>
				<div class="paneWrapper">
  					<div class="paneInnerWrapper">
		    			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableData">	
							<tr class="header">											
								<td >&nbsp;</td>
								<td class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_FieldName"/></td>
								<td colspan="1" class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_OldValue"/></td>
								<td colspan="4" class="sectionsubhead adminBackground"><s:text name="jsp.da_approvals_pending_payees_NewValue"/></td>
							</tr>
									<% 
									   int innerCount = 0;
									   int approvalItems = 0; %>
										<%-- Start: Clean up DA information --%>
										<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
										<%-- End: Clean up DA information --%>

										<%-- Retrieve category payeeDetails information --%>
										<ffi:object id="GetWireDACategorypayeeDetails" name="com.ffusion.tasks.dualapproval.wiretransfers.GetWireDACategoryDetails"/>
											<ffi:setProperty name="GetWireDACategorypayeeDetails" property="categorySessionName" value="IntermediaryCategories"/>
											<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemId" value="${ID}"/>
											<ffi:setProperty name="GetWireDACategorypayeeDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_WIRE_INTERMEDIARY_BANK%>"/>
											<ffi:setProperty name="GetWireDACategorypayeeDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
											<ffi:setProperty name="GetWireDACategorypayeeDetails" property="businessId" value="${SecureUser.BusinessID}"/>
											<ffi:setProperty name="GetWireDACategorypayeeDetails" property="daItemId" value="${daItemId}"/>
										<ffi:process name="GetWireDACategorypayeeDetails"/>

											
											<%
											DAItemCategories categories = (DAItemCategories)session.getAttribute("IntermediaryCategories");
											for(int counter = 0; counter < categories.size(); counter++){
												session.setAttribute("CATEGORY_BEAN", categories.get(counter) );
											%>
											<tr>	
												<td class="transactionHeading" height="10"colspan="4"><%=counter+1%> <s:text name="jsp.da_approvals_pending_beneficiary_intermediary_information"/> (<ffi:getProperty name="CATEGORY_BEAN" property="userAction"/>)</td>
											</tr>
															
											<ffi:list collection="CATEGORY_BEAN.daItems" items="intermediaryDetails">
												<tr>
													<td></td>
													<td width="200" class="columndata">
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="bankName">
															<s:text name="da.field.bankName"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="street">
															<s:text name="da.field.street"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="street2">
															<s:text name="da.field.street2"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="city">
															<s:text name="da.field.city"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="state">
															<s:text name="da.field.state"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="zipCode">
															<s:text name="da.field.zipCode"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="country">
															<s:text name="da.field.country"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="routingFedWire">
															<s:text name="da.field.routingFedWire"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="routingSwift">
															<s:text name="da.field.routingSwift"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="routingChips">
															<s:text name="da.field.routingChips"/>
														</ffi:cinclude>
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="routingOther">
															<s:text name="da.field.routingOther"/>
														</ffi:cinclude>	
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="IBAN">
															<s:text name="da.field.IBAN"/>
														</ffi:cinclude>	
														<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="correspondentBankAccountNumber">
															<s:text name="da.field.correspondentBankAccountNumber"/>
														</ffi:cinclude>								
													</td>
													<ffi:cinclude value1="${intermediaryDetails.fieldName}" value2="ID" operator="notEquals">
														<td width="250" class="columndata">
															<ffi:getProperty name="intermediaryDetails" property="oldValue"/>			
														</td>
														<td width="250" class="columndata">
															<ffi:getProperty name="intermediaryDetails" property="newValue"/>
														</td>
													</ffi:cinclude>	
												</tr>
											<% approvalItems ++; %>
											</ffi:list>
										<% } %>										
									<ffi:setProperty name="ApprovalItems" value="<%=String.valueOf(approvalItems)%>"/>
									<ffi:cinclude value1="${ApprovalItems}" value2="0">
										<tr>
											<td></td>
											<td width="700" colspan="3"> ------</td>
										</tr>
									</ffi:cinclude>	
									<ffi:removeProperty name="ApprovalItems"/>
								</table></div></div>

