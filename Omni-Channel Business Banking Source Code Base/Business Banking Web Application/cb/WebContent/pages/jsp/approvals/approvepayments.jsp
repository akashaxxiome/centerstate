<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%-- set the properties required by this page --%>
<s:set var="tmpI18nStr" value="%{getText('jsp.approvals_12')}" scope="request" /><ffi:setProperty name='PageHeading' value="${tmpI18nStr}"/>
<ffi:setProperty name='PageText' value=''/>
<s:set var="tmpI18nStr" value="%{getText('jsp.approvals_12')}" scope="request" /><ffi:setProperty name="ApprovalsHeading" value="${tmpI18nStr}"/>
<ffi:setProperty name="subMenuSelected" value="approvals"/>

<span class="shortcutPathClass" style="display:none;">approvals_pending</span>
<span class="shortcutEntitlementClass" style="display:none;">Approvals Admin</span>

<%-- include the approvals constants page --%>
<ffi:include page="${PagesPath}/inc/include-cb-approval-constants.jsp"/> 
<ffi:include page="${PagesPath}/approvals/inc/include-view-transaction-details-constants.jsp" />

<% if ( session.getAttribute( com.ffusion.efs.tasks.SessionNames.TRANSFERACCOUNTS ) == null ) { %>
	<ffi:object id="GetTransferAccounts" name="com.ffusion.tasks.banking.GetTransferAccounts" scope="session"/>
	<ffi:setProperty name="GetTransferAccounts" property="AccountsName" value="TransferAccounts"/>		
	<ffi:process name="GetTransferAccounts"/>	

<% } %>	
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS %>" >

    <% if ( session.getAttribute( com.ffusion.efs.tasks.SessionNames.PAYMENTACCOUNTS ) == null ) { %>
    

	<%-- Gets the payment accounts and places the collection into session with name "BillPayAccounts" --%>
	<ffi:object id="GetBillPayAccounts" name="com.ffusion.tasks.billpay.GetAccounts" scope="session"/>
		<ffi:setProperty name="GetBillPayAccounts" property="UseAccounts" value="BankingAccounts" />
	<ffi:process name="GetBillPayAccounts" />

	<%-- Retrieves the payment entitled accounts from "BillPayAccounts" and places the collection
	     into session with name "PaymentAccounts" --%>
	<ffi:object id="GetPaymentAccounts" name="com.ffusion.tasks.billpay.GetPaymentAccounts" scope="session"/>
		<ffi:setProperty name="GetPaymentAccounts" property="AccountsName" value="BillPayAccounts"/>
	<ffi:process name="GetPaymentAccounts"/>
    <% } %>	
</ffi:cinclude>



<% 
	String transactionSummaryCssClass ="visible"; 
	String payeeSummaryCssClass ="hidden";
%>

<s:if test="%{#parameters.visibleGrid[0] == 'pendingPayees'}">
	<%
		transactionSummaryCssClass = "hidden";
		payeeSummaryCssClass = "visible";
	%>
</s:if>


<div id="gridContainer" class="summaryGridHolderDivCls marginTop10">
	<div id="pendingTransactionsSummaryGrid" gridId="approvePaymentsGrid" class="gridSummaryContainerCls <%=transactionSummaryCssClass%>" >
		<ffi:help id="approvals_approvepayments"  className="moduleHelpClass"/>
		<div id="include-approvepaymentsId" class="approvepaymentsclass">
			<s:include value="/pages/jsp/common/include-approvepayments.jsp"/>
		</div>
	</div>
	<div id="pendingPayessSummaryGrid" gridId="daApprovePaymentsGrid" class="gridSummaryContainerCls <%=payeeSummaryCssClass%>">
		<ffi:help id="approvals_approvepayee"  className="moduleHelpClass"/>
		<%-- Start: Dual approval processing --%>
		<s:set name="PayeeCount" value="0" scope="request"/>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<div id="daApprovePayees" class="approvepaymentsclass">
				<s:include value="/pages/jsp/approvals/inc/da-include-approvepayees.jsp"/>
					<s:if test="%{#session.ApprovalItemsDA.Size != 0}">
						<s:set name="PayeeCount" value="1" scope="request"/>
					</s:if>
			</div> 
		</ffi:cinclude>
	</div>
</div>

<s:if test="%{#session.PendingApprovalsCount == 0 && #request.PayeeCount == 0}">
	<div align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left" width="100%">
					<script type="text/javascript">
						//ns.common.showStatus(js_approval_status);
					</script>
				</td>
			</tr>
		</table>
	</div>
</s:if>
<%-- End: Dual approval processing --%>

