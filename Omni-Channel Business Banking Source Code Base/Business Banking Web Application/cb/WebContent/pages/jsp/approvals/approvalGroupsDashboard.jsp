<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>


	<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-approvalGroups"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.approvals_23" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/approvalsSubmenuDropdown.jsp" />
    	</div>
    	
    	<div id="dbApprovalGroupSummary" class="dashboardSummaryHolderDiv">
    	
	       		<sj:a
					id="approvalGroupSummaryBtn"
					indicator="indicator"
					cssClass="summaryLabelCls"
					button="true"
					onclick="ns.shortcut.goToMenu('approvals_group');"
					title="%{getText('jsp.pending_approval_lbl_summary')}"
				><s:text name="jsp.pending_approval_lbl_summary"/></sj:a>
		
	        <ffi:cinclude ifEntitled="Approvals Workflow Admin">
		
		   		<s:url id="addGroupUrl" value="/pages/jsp/approvals/corpadminapprovalgroupadd.jsp">
			    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
				</s:url>
				
				 
			    <sj:a id="addGroupLink"
			    	href="%{addGroupUrl}" 
			    	targets="approvalGroupDialogID" 
			    	button="true" 
			    	title="%{getText('jsp.user_19')}"
			    	onCompleteTopics="loadAddGroupFormComplete" 
			    	onErrorTopics="errorLoadAddGroupForm">
				  	<s:text name="jsp.approvals_21"/>
			    </sj:a>

		</ffi:cinclude>
		</div>
		
      
    </div>
<script>
	$("#approvalGroupSummaryBtn").find("span").addClass("dashboardSelectedItem");
</script>    