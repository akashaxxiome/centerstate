<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="approvals_corpadmincoapwfaddedit" />

<div id='PageHeading' style="display:none;"><s:text name="jsp.approvals_11"/></div>

<%-- set the approvals constants --%>
<s:include value="%{#session.PagesPath}/inc/include-cb-approval-constants.jsp" />

<div align="center">
	<s:include value="%{#session.PagesPath}/common/include-approval-add-edit-level.jsp" />
</div>


	
    
    