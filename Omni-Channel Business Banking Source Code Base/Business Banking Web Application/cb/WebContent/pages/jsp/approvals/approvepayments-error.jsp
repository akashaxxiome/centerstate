<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- set the properties required by this page --%>
<ffi:setProperty name='PageHeading' value="Approvals Errors"/>
<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name="subMenuSelected" value="approvals"/>

<%-- include the approvals constants page --%>
<ffi:include page="${PagesPath}/inc/include-cb-approval-constants.jsp"/>


<ffi:help id="approvals_approvepayments-error" />

<div align="center">
    <div class="plainsubhead marginTop10">
	    <s:text name="jsp.default_497"/>
	</div>
	    <%-- include errors page here --%>     
	    <ffi:include page="${PagesPath}/common/include-approvepayments-error.jsp" />
</div>
