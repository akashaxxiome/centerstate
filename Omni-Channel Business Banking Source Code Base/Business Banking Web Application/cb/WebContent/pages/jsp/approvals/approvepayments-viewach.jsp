<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Include approval include-approval-view-details-init.jsp page --%>
<ffi:include page="${PagesPath}/common/include-approval-view-details-init.jsp"/>
<ffi:removeProperty name="ItemID"/>

<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies" scope="session"/>
	<ffi:setProperty name="GetACHCompanies" property="CustID" value="${SecureUser.BusinessID}" />
	<ffi:setProperty name="GetACHCompanies" property="FIID" value="${SecureUser.BankID}" />
	<ffi:setProperty name="GetACHCompanies" property="LoadCompanyEntitlements" value="true"/>
<ffi:process name="GetACHCompanies"/>


<ffi:help id="approvals_approvepayments-viewach" />
<div>
<span id="PageHeading" style="display:none"><ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/approve-payments-view.jsp" parm0="${ApprovalsItem.Type}" /></span>
		<ffi:include page="${PagesPath}common/include-approval-view-details-header.jsp"/>
		<div>
		<ffi:include page="${PagesPath}common/include-view-achbatch-details.jsp"/>
		</div>
</div>
