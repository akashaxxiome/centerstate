<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="lockboxDepositIcon">
<ffi:help id="cash_cashlockboxdeposits" />
	<ffi:setGridURL grid="GRID_lockboxDeposit" name="ViewURL" url="/cb/pages/jsp/cash/cashlockboxdetail.jsp?AccountID={0}&LockboxNumber={1}&lockboxcredititems-reload=true&DataClassification={2}" parm0="AccountID" parm1="LockboxNumber" parm2="dataClassification"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/cash/GetLockboxDepositAction.action?collectionName=LOCKBOX_TRANSACTIONS&GridURLs=GRID_lockboxDeposit" URLEncrypt="true"/>
    <s:url id="cashlockboxdepositURL" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="cashlockboxdepositID"  
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{cashlockboxdepositURL}"  
		pager="true"  
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,cashlockboxdepositEvent"
		> 
		
		<sjg:gridColumn name="lockboxNumber" index="lockboxNumber" title="%{getText('jsp.default_268')}" sortable="true" width="100" cssClass="datagrid_actionColumn"/>
	    <sjg:gridColumn name="amount.currencyStringNoSymbol" index="amount.currencyStringNoSymbol" title="%{getText('jsp.default_43')}" sortable="true" width="80" hidden="" cssClass="datagrid_textColumn"/>
	    <sjg:gridColumn name="immediateFloat.currencyStringNoSymbol" index="immediateFloat.currencyStringNoSymbol" title="%{getText('jsp.default_234')}" sortable="true" width="80" hidden="" cssClass="datagrid_textColumn"/>
	    <sjg:gridColumn name="oneDayFloat.currencyStringNoSymbol" index="oneDayFloat.currencyStringNoSymbol" title="%{getText('jsp.default_10')}" sortable="true" width="90" hidden="" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="twoDayFloat.currencyStringNoSymbol" index="twoDayFloat.currencyStringNoSymbol" title="%{getText('jsp.default_12')}" sortable="true" width="90" hidden="" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="numRejectedChecks" index="numRejectedChecks" title="%{getText('jsp.cash_92')}" sortable="true" width="90" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="rejectedAmount.currencyStringNoSymbol" index="rejectedAmount.currencyStringNoSymbol" title="%{getText('jsp.cash_91')}" sortable="true" width="50" cssClass="datagrid_amountColumn"/>
		
		<sjg:gridColumn name="accountID" index="accountID" title="%{getText('jsp.cash_10')}" sortable="true" width="50" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.dataClassification" index="map.dataClassification" title="%{getText('jsp.cash_47')}" sortable="true" width="50" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.date" index="map.date" title="%{getText('jsp.default_137')}" sortable="true" width="50" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.cash_121')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		
	</sjg:grid>
</div>
	<br>
	<div align="center">
	<sj:a id="backFormButtonForLockDeposit" 
			button="true" 
			onClickTopics="GoBackToCashLockbox"
	><s:text name="jsp.default_57"/></sj:a> 
	</div>
