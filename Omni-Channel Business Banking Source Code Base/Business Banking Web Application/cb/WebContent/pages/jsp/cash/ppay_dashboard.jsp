<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

    
    <div id="appdashboardRight" >
    	<div style="float:right; margin-top: 5px; margin-bottom: 5px;">	
			<sj:a id="PPayReportingButton" 
				  button="true"					   
				  onclick="ns.shortcut.goToFavorites('reporting_cashmgt');"
				  ><s:text name="jsp.default_355"/></sj:a>
			
			<%-- Find out if the user can upload files --%>
			<% boolean fileUploadEntitled = false; %>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_ENTITLEMENT_FILE_UPLOAD %>" >
			<% fileUploadEntitled = true; %>
			</ffi:cinclude>
			<% if( !fileUploadEntitled ) { %>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
			<% fileUploadEntitled = true; %>
			</ffi:cinclude>
			<% } %>
			<% if( fileUploadEntitled ) { %>
			    <s:url id="ppayuploadUrl" value="/pages/jsp/cash/cashppupload.jsp">
				    <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
				</s:url>
				<sj:a 
					id="ppayuploadLink"
					href="%{ppayuploadUrl}" 
					targets="firstDiv" 
					indicator="indicator" 
					button="true"
					onClickTopics="onClickUploadCheckingRecord"
					onSuccessTopics="uploadCheckingRecordSuccess"
				><s:text name="jsp.cash_118"/></sj:a>
			<% } %>
			
		    <s:url id="ppaybuildUrl" value="/pages/jsp/cash/cashppaybuild.jsp">
		    	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
			</s:url>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAY_ENTITLEMENT_ISSUE_MAINTENANCE %>" >
			<sj:a 
				id="ppaybuildLink"
				href="%{ppaybuildUrl}" 
				targets="firstDiv" 
				indicator="indicator" 
				button="true"
				onSuccessTopics="buildManualFileSuccess"
			><s:text name="jsp.cash_21"/></sj:a>
			</ffi:cinclude>
		</div>
		<span class="ui-helper-clearfix">&nbsp;</span>	
    </div>
	<sj:dialog id="InquiryTransferDialogID" cssClass="cashMgmtDialog" title="%{getText('jsp.default_249')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
	</sj:dialog>