<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%
	String fileType = request.getParameter("fileType");
%>
<ffi:setProperty name="FileUpload" property="FileType" value="<%=fileType %>"/>
<ffi:setProperty name="fileType" value="<%=fileType %>"/>
<ffi:setProperty name="ProcessImportTask" value="CheckPPayImport"/>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
<ffi:process name="SetMappingDefinition"/>
</ffi:cinclude>
   <%
   	request.setAttribute("actionName", "/pages/fileupload/cashFileUploadAction.action");
   %>
   <s:include value="/pages/jsp/common/publicuploader.jsp"/>
   
<script>
	ns.common.popupAfterUpload = true;
</script>