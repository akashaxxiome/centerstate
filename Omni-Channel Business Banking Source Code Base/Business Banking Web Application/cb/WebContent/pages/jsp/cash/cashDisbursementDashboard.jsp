<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>


	<div class="dashboardUiCls">
	    <div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-controlDisbursement"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_63" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/cashflowSubmenuDropdown.jsp" />
	    </div>
	    
	      <div id="dbCashDibursementSummary" class="dashboardSummaryHolderDiv">
       		<sj:a
				id="cashDisbursementSummaryBtn"
				indicator="indicator"
				cssClass="summaryLabelCls"
				button="true"
				onclick="ns.shortcut.goToMenu('cashMgmt_disburs');"
				title="%{getText('jsp.transfers.transfer_summary')}"
			><s:text name="jsp.transfers.transfer_summary"/></sj:a>
		</div>
	</div>
<script>
	$("#cashDisbursementSummaryBtn").find("span").addClass("dashboardSelectedItem");
</script>