<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<s:set var="tmpI18nStr" value="%{getText('jsp.cash_32')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>


<%

	session.setAttribute("BackToURL", request.getParameter("BackToURL"));
	session.setAttribute("Presentment", request.getParameter("Presentment"));
	if(request.getParameter("AccountID") != null) session.setAttribute("AccountID", request.getParameter("AccountID"));
	session.setAttribute("DisbursementCriteriaDate", request.getParameter("DisbursementCriteriaDate"));
	if(request.getParameter("DisbursementDataClassification") !=null ) session.setAttribute("DisbursementDataClassification", request.getParameter("DisbursementDataClassification"));
	session.setAttribute("DisbursementTransactionIndex", request.getParameter("DisbursementTransactionIndex"));
	session.setAttribute("Date", request.getParameter("Date"));
	session.setAttribute("DataClassification", request.getParameter("DataClassification"));
%>


<% 
	String currIndex = (String) session.getAttribute( "DisbursementTransactionIndex" ); 
	String minPageIndex = ( (com.ffusion.tasks.disbursement.GetPagedTransactions) session.getAttribute("GetDisbursementTransactions") ).getMinimumPageTransactionIndex(); 
	String maxPageIndex = ( (com.ffusion.tasks.disbursement.GetPagedTransactions) session.getAttribute("GetDisbursementTransactions") ).getMaximumPageTransactionIndex(); 
	if( Long.parseLong(currIndex) < Long.parseLong(minPageIndex) ) { 
%>
	
<% } else if( Long.parseLong(currIndex) > Long.parseLong(maxPageIndex) ) { %>
	
<% } %>
<ffi:setProperty name="DisbursementItems" property="Filter" value="TransactionIndex=${DisbursementTransactionIndex}"/>
		
		
		<div id="cashdisburseitemID">
		<ffi:help id="cash_cashdisburseitem" />

		<div class="blockWrapper">
			
				<!-- <div class="blockHead">Disbursement Summary</div> -->
				
				<div class="blockContent">
				<ffi:list collection="DisbursementItems" items="DItem" startIndex="1" endIndex="1">
				
				
					<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.cash_76"/></span>
					<span><ffi:getProperty name="DItem" property="Payee"/></span>
					</div>
					
					<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.cash_90"/></span>
					<span><ffi:getProperty name="DItem" property="CheckReferenceNumber"/></span>
					</div>
					
					</div>
					
					
					<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.cash_25"/></span>
					<% String negativeAmount=null; %>
					<ffi:getProperty name="DItem" property="CheckAmount.AmountValue"  assignTo="negativeAmount"/>
					<% if( negativeAmount!= null && negativeAmount.indexOf("-") >= 0 ) { %>
						<span class="negativenumbercolor"><ffi:getProperty name="DItem" property="CheckAmount.CurrencyStringNoSymbol"/></span>
					<% } else { %>
						<span><ffi:getProperty name="DItem" property="CheckAmount.CurrencyStringNoSymbol"/></span>
					<% } %>
					</div>
					
					<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.cash_27"/></span>
					<span><ffi:getProperty name="DItem" property="CheckDate"/></span>
					</div>
					</div>
					
					
					
					<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.cash_58"/></span>
					<span><ffi:getProperty name="DItem" property="IssuedBy"/></span>
					</div>
					<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.cash_17"/></span>
					<span><ffi:getProperty name="DItem" property="ApprovedBy"/></span>
					</div>
					</div>
					</div>
					
					
					<div class="blockHead"><s:text name="jsp.miscellaneous.label" /></div>
					<div class="blockContent">
					<div class="blockRow">
					<div style="width: 50%" class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.cash_72"/></span>
					<span><ffi:getProperty name="DItem" property="Memo"/></span>
					</div>
					
					</div>
					
					
					</div>
					
					<div class="btn-row">
									<%  
										String prevIndex = Long.toString( Long.parseLong( currIndex ) - 1 );										
										String nextIndex = Long.toString( Long.parseLong( currIndex ) + 1 );
									%>
									<ffi:setProperty name="prevIndex" value="<%= prevIndex %>" />
									<ffi:setProperty name="nextIndex" value="<%= nextIndex %>" />
									
									<ffi:cinclude value1="" value2="${BackToURL}" operator="notEquals">
										<ffi:setProperty name="BackToURL" value="${BackToURL}?Presentment=${Presentment}&Date=${Date}&DataClassification=${DataClassification}"/>
									</ffi:cinclude>
									
									<ffi:cinclude value1="" value2="${BackToURL}" operator="equals">
									</ffi:cinclude>
								
									
									<br>
									<s:url id="BackToDetailUrl" value="/pages/jsp/cash/cashdisbursedetail.jsp" escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
										<s:param name="GetDisbursementTransactions.Page" value="%{'current'}"/>
										<s:param name="DataClassification" value="%{#session.DisbursementDataClassification}"/>
										<s:param name="Date" value="%{#session.DisbursementCriteriaDate}"/>
									</s:url>
								
									<sj:a id="backFormButtonForPayee" 
										  href="%{BackToDetailUrl}"
										  targets="cashDisbursedetail"
										  button="true" 
										  title="%{getText('jsp.cash_125')}"
										  onClickTopics=""
										><s:text name="jsp.default_57" /></sj:a>
							      			
							
									<ffi:removeProperty name="BackToURL"/>
										<ffi:cinclude value1="0" value2="<%= currIndex %>" operator="notEquals">
										<s:url id="previousDetailUrl" value="/pages/jsp/cash/cashdisburseitem.jsp" escapeAmp="false">
											<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
											<s:param name="DisbursementTransactionIndex" value="%{#session.prevIndex}"/>
											<s:param name="DataClassification" value="%{#session.DisbursementDataClassification}"/>
											<s:param name="Date" value="%{#session.DisbursementCriteriaDate}"/>
										</s:url>
														
														<sj:a id="previousFormButtonForPayee" 
															   href="%{previousDetailUrl}"
															   targets="cashDisbursedetail"
															   button="true"
															   onClickTopics="GoToPreviousPayee"
															><s:text name="jsp.cash_86"/></sj:a>		
											
										</ffi:cinclude>
									
									
		
									<ffi:cinclude value1="${CurrentSelectedTotalRows}" value2="<%= currIndex %>" operator="notEquals">
													
									<s:url id="nextDetailUrl" value="/pages/jsp/cash/cashdisburseitem.jsp" escapeAmp="false">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
										<s:param name="DisbursementTransactionIndex" value="%{#session.nextIndex}"/>
										<s:param name="DataClassification" value="%{#session.DisbursementDataClassification}"/>
										<s:param name="Date" value="%{#session.DisbursementCriteriaDate}"/>
									</s:url>
										
													<sj:a id="nextFormButtonForPayee" 
														   href="%{nextDetailUrl}"
														   targets="cashDisbursedetail"
														   button="true" 
														   onClickTopics="GoToNextPayee"
														><s:text name="jsp.default_291"/></sj:a>
										
									</ffi:cinclude> 
			
									<ffi:removeProperty name="prevIndex"/>
									<ffi:removeProperty name="nextIndex"/>
							
						</ffi:list>
						<ffi:setProperty name="DisbursementItems" property="Filter" value="All"/>
					</div>
				
		</div>
		</div>
			
			
			

<script>
$('#cashdisburseitemID').portlet({
		generateDOM: true,
		helpCallback: function(){
				
				var helpFile = $('#cashdisburseitemID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
	});
	$("#cashdisburseitemID").portlet('title', js_disbursement_item_detail);
</script>