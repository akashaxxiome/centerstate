<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="cash_index" />

<ffi:setGridURL grid="GRID_cashflow" name="ViewURL" url="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=1&AccountID={0}&AccountBankID={1}&AccountRoutingNum={2}&ProcessAccount=true&GetPagedTransactions.Page=first&StartDate={3}&EndDate={4}&ProcessTransactions=true&DataClassification={5}&redirection=true&isCashFlow=true" parm0="Account.ID" parm1="Account.BankID" parm2="Account.RoutingNum" parm3="DateString" parm4="DateString" parm5="DataClassification"/>

<ffi:setProperty name="tempURL" value="/pages/jsp/cash/GetSummariesForAccountDateAction.action?collectionName=EntitlementFilteredAccounts&GridURLs=GRID_cashflow" URLEncrypt="true"/>
    <s:url id="cashflowSummaryURL" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="cashflowSummaryID" 
		sortable="true"  
		dataType="json"  
		href="%{cashflowSummaryURL}"  
		pager="true" 
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"		
		gridModel="gridModel" 
		rownumbers="false"
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		footerrow="true"
		userDataOnFooter="true"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onBeforeTopics="prepareJsonData"
		onGridCompleteTopics="addGridControlsEvents,cashflowSummaryEvent"> 
		<sjg:gridColumn name="account.accountDisplayText" index="account" title="%{getText('jsp.default_15')}" sortable="true" width="250" />
		<sjg:gridColumn name="accountNickName" index="accountNickName" title="%{getText('jsp.default_294')}" sortable="true" width="150" formatter="ns.cash.formatAccountNickNameColumn" /> 
	    <sjg:gridColumn name="displayOpeningLedger" index="displayOpeningLedger" title="%{getText('jsp.default_304')}" sortable="true"  width="100" formatter="ns.cash.formatOpeningLedgerColumn" cssClass="datagrid_amountColumn"/> 
	    <sjg:gridColumn name="numTransactions" index="numTransactions" title="%{getText('jsp.default_1')}" sortable="true" width="70" formatter="ns.cash.formatOfItemsColumn" cssClass="datagrid_numberColumn"/> 
	    <sjg:gridColumn name="displayTotalDebits" index="displayTotalDebits" title="%{getText('jsp.default_434')}" sortable="true"  width="90" formatter="ns.cash.formatTotalDebitsColumn" cssClass="datagrid_amountColumn"/> 
	    <sjg:gridColumn name="displayTotalCredits" index="displayTotalCredits" title="%{getText('jsp.default_433')}" sortable="true"  width="90" formatter="ns.cash.formatTotalCreditsColumn" cssClass="datagrid_amountColumn"/> 
	    <sjg:gridColumn name="displayClosingLedger" index="displayClosingLedger" title="%{getText('jsp.default_103')}" sortable="true"  width="100" formatter="ns.cash.formatClosingLedgerColumn" cssClass="datagrid_amountColumn"/> 
		<sjg:gridColumn name="account.routingNum" index="routingNumber" title="%{getText('jsp.default_365')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>
        <sjg:gridColumn name="account.nickName" index="nickName" title="%{getText('jsp.default_294')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="account.currencyCode" index="currencyCode" title="%{getText('jsp.default_126')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="account.ID" index="ID" title="%{getText('jsp.cash_7')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="account.bankID" index="bankID" title="%{getText('jsp.cash_18')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="openingLedgerTotal" index="openingLedgerTotal" title="%{getText('jsp.cash_75')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
		<sjg:gridColumn name="debitsTotal" index="debitsTotal" title="%{getText('jsp.cash_49')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
        <sjg:gridColumn name="creditsTotal" index="creditsTotal" title="%{getText('jsp.cash_42')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
        <sjg:gridColumn name="closingLedgerTotal" index="closingLedgerTotal" title="%{getText('jsp.cash_31')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
        <sjg:gridColumn name="runningTotal" index="runningTotal" title="%{getText('jsp.cash_97')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
        <sjg:gridColumn name="displayCurrencyCode" index="map.displayCurrencyCode" title="%{getText('jsp.default_173')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="dataClassification" index="map.dataClassification" title="%{getText('jsp.cash_45')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>		
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/> 
	</sjg:grid>
	
	<div class="sectionsubhead ltrow2_color">
		<br/><s:text name="jsp.cash_total_from_all_pages" />
	</div>
