<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>


	<div id="lockboxSummary" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
	    <ul id="lockboxSummaryTitle" class="portlet-header ui-widget-header ui-corner-all">
	    </ul>
		<div id="lockboxSummaryGrid" class="portlet">
	    </div>		
	</div>