<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%@page import="com.ffusion.beans.disbursement.DisbursementSummaries" %>
<%@page import="com.ffusion.beans.disbursement.DisbursementSummary" %>

<%
	String DisbursementCriteriaDate = request.getParameter("DisbursementCriteriaDate");
	String DisbursementDataClassification = request.getParameter("DisbursementDataClassification");
	
	session.setAttribute("DisbursementCriteriaDate", request.getParameter("DisbursementCriteriaDate"));
	session.setAttribute("DisbursementDataClassification", request.getParameter("DisbursementDataClassification"));
%>	
				
<ffi:cinclude value1="" value2="${DisbursementDataClassification}" operator="equals">
	<ffi:setProperty name="DisbursementDataClassification" value="I"/>
</ffi:cinclude>

<ffi:setProperty name="DateFormat" value="${UserLocale.DateFormat}"/>

<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
	<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="DisbursementDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${fromPortalPage}" value2="true" operator="equals">
	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY %>"/>
	<ffi:setProperty name="DisbursementDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY %>"/>
</ffi:cinclude>

<ffi:cinclude value1="${Reload}" value2="false" operator="notEquals">
	<ffi:object id="GetPresentmentSummaries" name="com.ffusion.tasks.disbursement.GetPresentmentSummaries" scope="session"/>
	<ffi:setProperty name="GetPresentmentSummaries" property="DateFormat" value="${DateFormat}"/>
	<ffi:setProperty name="GetPresentmentSummaries" property="StartDate" value="${DisbursementCriteriaDate}"/>
	<ffi:setProperty name="GetPresentmentSummaries" property="EndDate" value="${DisbursementCriteriaDate}"/>
	<ffi:setProperty name="GetPresentmentSummaries" property="DataClassification" value="${DisbursementDataClassification}"/>
	<ffi:process name="GetPresentmentSummaries"/>
</ffi:cinclude>

				<%
                  session.setAttribute("FFIGetPresentmentSummaries", session.getAttribute("GetPresentmentSummaries"));
                 %>

<%-- need to set the start date for display.  if it was not set during task processing, set it to the current date. --%>
<ffi:cinclude value1="${GetPresentmentSummaries.StartDate}" value2="" operator="equals">
	<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="session"/>
	<ffi:process name="GetCurrentDate"/>
				<%
                  session.setAttribute("FFIGetCurrentDate", session.getAttribute("GetCurrentDate"));
                 %>
	<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${DateFormat}" />
	<ffi:setProperty name="GetPresentmentSummaries" property="StartDate" value="${GetCurrentDate.Date}"/>
</ffi:cinclude>

<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="DisbursementPresentmentSummaries"/>
<ffi:process name="SortImage"/>
<ffi:setProperty name="SortImage" property="Compare" value="PRESENTMENT"/>

<s:include value="/pages/jsp/cash/cashpresentment_grid.jsp"/> 
<script>
	//ns.common.initializePortlet("disbursementPresentmentIcon",true); 
	$(function(){
		var title = js_disbursement_presentment_portlet_title;
		var startDate = '<ffi:getProperty name="GetPresentmentSummaries" property="StartDate"/>';				
		//$("#disbursementPresentmentIcon").portlet('title', title +" "+ startDate);
	});
</script>
   

<ffi:removeProperty name="Reload"/>
<ffi:removeProperty name="DateFormat"/>
<ffi:removeProperty name="GetPresentmentSummaries"/> 
<ffi:removeProperty name="SortImage"/>
