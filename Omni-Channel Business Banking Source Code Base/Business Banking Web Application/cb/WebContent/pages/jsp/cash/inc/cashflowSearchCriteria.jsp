<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%@ page import="com.ffusion.beans.user.UserLocale" %>


<%
	String cashFlowCriteriaDate = request.getParameter("CashFlowCriteriaDate");
	String cashFlowDataClassification = request.getParameter("CashFlowDataClassification");
	session.setAttribute("CashFlowCriteriaDate", request.getParameter("CashFlowCriteriaDate"));
	session.setAttribute("CashFlowDataClassification", request.getParameter("CashFlowDataClassification"));
	session.setAttribute("CashDisplayCurrencyCode", request.getParameter("CashDisplayCurrencyCode"));
	session.setAttribute("changeCashDisplayCurrency", request.getParameter("changeCashDisplayCurrency"));
		
%>

<ffi:setProperty name="DateVariable" value="<%= cashFlowCriteriaDate %>"/>
<ffi:setProperty name="DataClassificationVariable" value="<%= cashFlowDataClassification %>"/>

<script>
	$(function(){
		$("#CashDisplayCurrencyCodeID").combobox();
		$("#CashFlowDataClassification").selectmenu({width:150});
	   });
	   
	ns.cash.changeCashDisplayCurrency = function() { 
		$('#changeCashDisplayCurrency').val('true');
		$('#viewCashFlowForm').find("#displayCurrencyID")
		$('#displayCurrencyID').val($('#CashDisplayCurrencyCodeID').val());
		
		$('#CashDisplayCurrencyCode').val($('#CashDisplayCurrencyCodeID').val());
		$('cashFlowSearchCriteria.currency').val($('#CashDisplayCurrencyCodeID').val());
		$('#viewCashFlowButton').click();
	};
	
	function ViewCashFlow(){ 
			//var finalString = "/cb/pages/jsp/cash/CashFlowAction.action";
			var finalString = "/cb/pages/jsp/cash/validateCashFlow.action";
			$('cashFlowSearchCriteria.currency').val($('#CashDisplayCurrencyCodeID').val());
			$.ajax({
				url: finalString,
				type: "POST",
				data: $("#viewCashFlowForm").serialize(),
				success: function(data){
					$('#appdashboard').html(data);
					
					ns.cash.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/cash/cashflow_grid.jsp'/>";
						ns.cash.refreshSummaryForm(ns.cash.refreshSummaryFormURL);
						var title = js_cashflow_summary_portlet_title;
						var Date = '<ffi:getProperty name="GetSummariesForAccountDate" property="Date"/>';						
						ns.common.updatePortletTitle("cashflowSummary",title + " "+Date);
					
					
					
					
				}
			});
	}


	function isDateString(sDate)
		{
			var iaMonthDays = [31,28,31,30,31,30,31,31,30,31,30,31];
			var iaDate = new Array(3);
			var year, month, day;

			if (arguments.length != 1) return false;
			iaDate = sDate.toString().split("/");
			if (iaDate.length != 3) {
				alert("Please input date as the format MM/dd/yyyy!");
				return false;
			}
			if (iaDate[0].length > 2 || iaDate[1].length > 2) {
				alert("Please input date as the format MM/dd/yyyy!");
				return false;
			}
			month = parseFloat(iaDate[0]);
			day = parseFloat(iaDate[1]);
			year=parseFloat(iaDate[2]);

			if (year < 1900 || year > 2100)  {
				alert("Year too large!");
				return false;
			}
			if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) iaMonthDays[1]=29;
			if (month < 1 || month > 12)  {
				alert("Invalide month!");
				return false;
			}
			if (day < 1 || day > iaMonthDays[month - 1])  {
				alert("Invalide day!");
				return false;
			}
			return true;
		}
	
</script>
<%-- Cash flow shows only deposit accounts, so entitlement filtering should be done based only on the deposit accounts --%>
<ffi:setProperty name="BankingAccounts" property="Filter" value="ACCOUNTGROUP=1" />
 <ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="BankingAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/> 

<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryAnyDataClassification}" value2="true" operator="equals"> 
	<%-- we must determine the default data classification based on whether the user can see intra or previous day for any accounts--%>
	<ffi:cinclude value1="${CashFlowDataClassification}" value2="" operator="equals">
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
			<ffi:setProperty name="CashFlowDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
		</ffi:cinclude>
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="false" operator="equals">
			<ffi:setProperty name="CashFlowDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${CashFlowDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" operator="equals">
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="notEquals">
			<ffi:setProperty name="CashFlowDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>			
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${CashFlowDataClassification}" value2="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" operator="equals">
		<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="notEquals">
			<ffi:setProperty name="CashFlowDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>			
		</ffi:cinclude>
	</ffi:cinclude>
	<%-- <s:include value="/pages/jsp/cash/inc/specifycriteria.jsp"/> --%>
	
	
	<%-- specifycriteria.jsp BEGINS --%>
	<%
		session.setAttribute("DateString", request.getParameter("CashFlowCriteriaDate"));
		session.setAttribute("DataClassification", request.getParameter("CashFlowDataClassification"));
	%>
	
	<%-- If this is the first visit to the page, --%>
	<ffi:cinclude value1="${DataClassification}" value2="" operator="equals">
		<%-- we set DataClassification to its default value - Previous Day --%>
		<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
		<%-- UNLESS we are doing this for Controlled Disubursement, where we would like default Intra Day --%>
		<ffi:cinclude value1="${DataClassificationVariable}" value2="DisbursementDataClassification" operator="equals">
			<%-- but first we'll check if we are even entitled to Intra day! --%>
			<ffi:setProperty name="intraEntitled" value="false"/>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
				<ffi:setProperty name="intraEntitled" value="true"/>
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
				<ffi:setProperty name="intraEntitled" value="true"/>
			</ffi:cinclude>
			<%-- and if we are, we will set the default to Intra Day --%>
			<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
		        	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:setProperty name="${DataClassificationVariable}" value="${DataClassification}"/>
	</ffi:cinclude>
	
	
	 <ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="notEquals">
		<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
		<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
		<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="${AccountsCollectionName}"/>
		<ffi:process name="CheckPerAccountReportingEntitlements"/>
	</ffi:cinclude> 
	<ul id="formerrors" class="hidden"></ul> 
	<div class="floatleft marginBottom10 paddingLeft10">
	<s:form action="/pages/jsp/cash/validateCashFlow_verify.action" id="viewCashFlowForm" method="post" name="CriteriaForm" theme="simple">
     	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
     	<input type="hidden" id="changeCashDisplayCurrency" name="changeCashDisplayCurrency" value="" />
     	<input type="hidden" id="displayCurrencyID" name="CashDisplayCurrencyCode" value="" />
     	
     	
     	
			<div class="acntDashboard_masterItemHolders marginTop10" style="display:none;" data-banking-container-type="search">
				<div class="acntDashboard_itemHolder marginRight10 floatleft">
						<span class="sectionsubhead dashboardLabelMargin" style="display:block;"><s:text name="jsp.default_354"/>&nbsp;</span>
							<!-- <div class="selectBoxHolder"> -->
								<select class="txtbox" id="CashFlowDataClassification" name="cashFlowSearchCriteria.dataClassification"> <!-- CashFlowDataClassification -->
									<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="equals">
									<%-- we must determine which data classification the user is entitled to. AccountsCollectionName will be blank for
								       	     Lockbox and Disbursement. --%>
									     	<ffi:removeProperty name="previousEntitled"/>
									     	<ffi:removeProperty name="intraEntitled"/>
										<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
											<ffi:setProperty name="previousEntitled" value="true"/>
										</ffi:cinclude>
										<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
											<ffi:setProperty name="previousEntitled" value="true"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${previousEntitled}" value2="true" operator="equals">
											<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${cashFlowSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
										</ffi:cinclude>
	
										<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
											<ffi:setProperty name="intraEntitled" value="true"/>
										</ffi:cinclude>
										<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
											<ffi:setProperty name="intraEntitled" value="true"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
											<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${cashFlowSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
										</ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude  value1="${AccountsCollectionName}" value2="" operator="notEquals">
							 		<%-- we must determine which data classifications to place in this drop down, based on the entiltment checks on the accounts collection AND the data type of this information --%>
									<ffi:cinclude value1="${DataType}" value2="Summary" operator="equals">
										<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
											<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${cashFlowSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
											<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${cashFlowSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
										</ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude value1="${DataType}" value2="Detail" operator="equals">
										<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
											<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${cashFlowSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
											<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${cashFlowSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
										</ffi:cinclude>
									</ffi:cinclude>
									<ffi:cinclude value1="${DataType}" value2="" operator="equals">
										<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryPrev}" value2="true" operator="equals">
											<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${cashFlowSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
										</ffi:cinclude>
										<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryIntra}" value2="true" operator="equals">
											<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${cashFlowSearchCriteria.DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
										</ffi:cinclude>
									</ffi:cinclude>
									</ffi:cinclude> <%-- End if accounts collection name is empty --%>
								</select>
							<br />
							<span id="dataClassificationError"></span>
						</div>
							<%-- <span id="cashFlowSearchCriteria.dataClassificationError"></span>
							<span id="CashFlowDataClassificationError"></span> --%>
						<%-- StringUtil element to display text in date fields --%>
						<ffi:setProperty name="StringUtil" property="Value2" value="${UserLocale.DateFormat}" />
						<ffi:setProperty name="StringUtil" property="Value1" value="${DateString}" />
						<div class="acntDashboard_itemHolder marginRight10 floatleft">
						
								<span class="sectionsubhead" style="display:block;"><s:text name="jsp.default_137"/>&nbsp;</span>							
								<sj:datepicker value="%{#session.cashFlowSearchCriteria.cashFlowCriteriaDate}" isfirstelement="true" accesskey="d" id="CashFlowCriteriaDate" name="cashFlowSearchCriteria.cashFlowCriteriaDate" label="%{getText('jsp.default_137')}" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"  style="position: relative;top: 3px;" /><!-- CashFlowCriteriaDate -->
								<%-- <span id="cashFlowCriteriaDate.dateNodeError"></span> --%>
								<script>
								var date = new Date();
								var today = date/(1000*60*60*24);
								var lastDayOfManth = new Date(date.getFullYear(), date.getMonth() + 1, 0)/(1000*60*60*24);
								var days = Math.ceil(lastDayOfManth-today);
								var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=back&calOneDirectionOnly=true"/>';
			                    ns.common.enableAjaxDatepicker("cashFlowSearchCriteria.cashFlowCriteriaDate", tmpUrl);
			                    $("#CashFlowCriteriaDate").datepicker('option', 'maxDate' , days);
			                    $("#CashFlowCriteriaDate").val("<ffi:getProperty name="cashFlowSearchCriteria" property="CashFlowCriteriaDate"/>");
								</script>
							<br />
							<span id="cashFlowCriteriaDate.dateNodeError"></span> 	
						</div>
						<%-- <span id="cashFlowCriteriaDateError"></span> 
						<span id="dateError"></span> --%>
						
						<%-- td align="center" nowrap><a onclick="CSAction(new Array(/*CMP*/'B9E611E8165'));return CSClickReturn();" href="#" csclick="B9E611E8165"><img src="/cb/web/multilang/grafx/i_calendar.gif" alt="" width="19" height="16" border="0"></a></td --%>
					
					<div class="acntDashboard_itemHolder marginRight10 floatleft">
						<div id="selectCurrency">
							<ffi:cinclude value1="true" value2="${showselectcurrency}" operator="equals" > 
								<%-- <td align="left"><span class="sectionsubhead"><s:text name="jsp.default_382"/>&nbsp;</span></td> --%>
								<!-- <td align="left"> -->
									<span class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.default_382"/>&nbsp;</span> 
									<!-- <div class="selectBoxHolder"> -->
										<select id="CashDisplayCurrencyCodeID" name="cashFlowSearchCriteria.currency" class="txtbox"  onchange="ns.cash.changeCashDisplayCurrency();">
											<option <ffi:cinclude value1='' value2='${cashFlowSearchCriteria.currency}' operator='equals'> selected </ffi:cinclude>  value='' >
												<s:text name="jsp.default_127"/>
											</option>
											<ffi:list collection="CashFlowFXCurrencies" items="currency">
											    <option <ffi:cinclude value1='${currency.CurrencyCode}' value2='${cashFlowSearchCriteria.currency}' operator='equals'> selected </ffi:cinclude>  value='<ffi:getProperty name="currency" property="CurrencyCode"/>' ><!-- CashDisplayCurrencyCode -->
											<ffi:getProperty name="currency" property="CurrencyCode"/>-<ffi:getProperty name="currency" property="Description"/>
											</option>
										    </ffi:list>
										</select>
									<!-- </div> -->
								<!-- </td> -->
					    	</ffi:cinclude>
						</div>
						<span id="currencyError"></span>
					</div>
                    <div class="acntDashboard_itemHolder marginRight10 floatleft">
                         	<span class="sectionsubhead dashboardLabelMargin">&nbsp;</span>
							<sj:a targets="view"
								id="viewCashFlowButton"
								formIds="viewCashFlowForm"
								button="true" 
                                validate="true"
                                validateFunction="customValidation"
								onclick="removeValidationErrors()"
								onCompleteTopics="verifyCashFlowCompleteTopic"><s:text name="jsp.default_6" /></sj:a>
					</div>
					<%-- <div class="acntDashboard_itemHolder marginRight10 floatleft">
							<span class="sectionsubhead dashboardLabelMargin" style="display:block;">&nbsp;</span>
							<sj:a id="cashflowReportingButton"
						   		  button="true"
						   		  buttonIcon="ui-icon-document-b"
								  onclick="ns.shortcut.goToFavorites('reporting_cashmgt');"
								  ><s:text name="jsp.default_355"/></sj:a>
					</div>  --%>
			</div>
        </s:form>
     </div>
	<ffi:removeProperty name="DataClassification"/>
	<ffi:removeProperty name="DateString"/>
	<ffi:removeProperty name="CheckPerAccountReportingEntitlements"/>
	<%-- specifycriteria.jsp ENDS --%>
</ffi:cinclude>

<%--There are no accounts so we should display a message to the user --%>
<ffi:cinclude value1="${EntitlementFilteredAccounts.Size}" value2="0" operator="equals">
	<script>
		//set no records display flag for dashboard
		ns.cash.appdashboardflag = true;
	</script>
</ffi:cinclude>
<script>
	 ns.cash.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/cash/cashflow_grid.jsp'/>";
	$(function(){  
		ns.cash.refreshSummaryForm(ns.cash.refreshSummaryFormURL);		
	}); 
</script>

<ffi:removeProperty name="DepositAcctCashFlowSummaries"/>
<ffi:removeProperty name="displayCurrencyCode"/>
<ffi:removeProperty name="List2Sort"/>
<ffi:removeProperty name="SortCriteria"/>
<ffi:removeProperty name="GetNumTransactionsCashFlow"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
<ffi:removeProperty name="AccountEntitlementFilterTask"/>
<ffi:removeProperty name="GetEntitlementObjectID"/>
<ffi:removeProperty name="GetAccountsSortImage"/> 
<ffi:removeProperty name="CashFlowAccountSummary"/>
<ffi:removeProperty name="Account"/>
