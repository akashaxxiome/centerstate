<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%@ page import="com.ffusion.beans.user.UserLocale" %>

<%
	session.setAttribute("DateString", request.getParameter("CashFlowCriteriaDate"));
	session.setAttribute("DataClassification", request.getParameter("CashFlowDataClassification"));
%>

<%-- If this is the first visit to the page, --%>
<ffi:cinclude value1="${DataClassification}" value2="" operator="equals">
	<%-- we set DataClassification to its default value - Previous Day --%>
	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
	<%-- UNLESS we are doing this for Controlled Disubursement, where we would like default Intra Day --%>
	<ffi:cinclude value1="${DataClassificationVariable}" value2="DisbursementDataClassification" operator="equals">
		<%-- but first we'll check if we are even entitled to Intra day! --%>
		<ffi:setProperty name="intraEntitled" value="false"/>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
			<ffi:setProperty name="intraEntitled" value="true"/>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
			<ffi:setProperty name="intraEntitled" value="true"/>
		</ffi:cinclude>
		<%-- and if we are, we will set the default to Intra Day --%>
		<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
	        	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:setProperty name="${DataClassificationVariable}" value="${DataClassification}"/>
</ffi:cinclude>


<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="notEquals">
	<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
	<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
	<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="${AccountsCollectionName}"/>
	<ffi:process name="CheckPerAccountReportingEntitlements"/>
</ffi:cinclude>

<SCRIPT type=text/javascript>

$(function(){
	$("#CashFlowDataClassification").selectmenu({width:150});
   });

CSAct = new Object;
number = -1;
CSStopExecution=false;
function CSClickReturn () {
	var bAgent = window.navigator.userAgent;
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
}
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
	return CSAction3(fct, array, null);
}
function CSAction3(fct, array, param) {
	var result;
	for (var i=0;i<array.length;i++) {
		if(CSStopExecution) return false;
		var aa = fct[array[i]];
		if (aa == null) return false;
		var ta = new Array;
		for(var j=1;j<aa.length;j++) {
			if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
				if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
				else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
				else ta[j]=aa[j];}
			} else ta[j]=aa[j];
		}
		result=aa[0](ta, param);
	}
	return result;
}
function CSOpenWindow(action, param) {
	var wf = "";
	wf = wf + "width=" + action[3];
	wf = wf + ",height=" + action[4];
	wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
	wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
	wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
	wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
	wf = wf + ",directories=" + (action[9] ? "yes" : "no");
	wf = wf + ",location=" + (action[10] ? "yes" : "no");
	wf = wf + ",status=" + (action[11] ? "yes" : "no");
	if (param == null) {
		window.open(action[1], action[2], wf);
	} else {
		window.open(action[1] + "?" + param, action[2], wf);
	}
}
CSAct[/*CMP*/ 'B9E611E8165'] = new Array(CSOpenWindow,/*URL*/ '<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calOneDirectionOnly=true&calDirection=back&calForm=CriteriaForm&calTarget=${DateVariable}"/>','',350,300,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E9A080137'] = new Array(CSOpenWindow,/*URL*/ 'checkimage.jsp','',550,600,false,false,false,false,false,false,false);


function ViewCashFlow(){

		var finalString = "/cb/pages/jsp/cash/inc/cashflow.jsp";

		$.ajax({
			url: finalString,
			type: "POST",
			data: $("#viewCashFlowForm").serialize(),
			success: function(data){
				$('#appdashboard').html(data);
			}
		});
}


function isDateString(sDate)
	{
		var iaMonthDays = [31,28,31,30,31,30,31,31,30,31,30,31];
		var iaDate = new Array(3);
		var year, month, day;

		if (arguments.length != 1) return false;
		iaDate = sDate.toString().split("/");
		if (iaDate.length != 3) {
			alert("Please input date as the format MM/dd/yyyy!");
			return false;
		}
		if (iaDate[0].length > 2 || iaDate[1].length > 2) {
			alert("Please input date as the format MM/dd/yyyy!");
			return false;
		}
		month = parseFloat(iaDate[0]);
		day = parseFloat(iaDate[1]);
		year=parseFloat(iaDate[2]);

		if (year < 1900 || year > 2100)  {
			alert("Year too large!");
			return false;
		}
		if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) iaMonthDays[1]=29;
		if (month < 1 || month > 12)  {
			alert("Invalide month!");
			return false;
		}
		if (day < 1 || day > iaMonthDays[month - 1])  {
			alert("Invalide day!");
			return false;
		}
		return true;
	}


</SCRIPT>

                    	<s:form action="/pages/jsp/cash/GetSummariesForAccountDateAction_verify.action" id="viewCashFlowForm" method="post" name="CriteriaForm" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<input type="hidden" id="changeCashDisplayCurrency" name="changeCashDisplayCurrency" value="" />
                    	<input type="hidden" id="displayCurrencyID" name="CashDisplayCurrencyCode" value="" />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>

			<td class="columndata ltrow2_color" colspan="6" align="left">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.default_354"/>&nbsp;</span></td>
					<td nowrap>
						<table>
							<tr>
								<td>
									<select class="txtbox" id="CashFlowDataClassification" name="CashFlowDataClassification">
										<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="equals">
										<%-- we must determine which data classification the user is entitled to. AccountsCollectionName will be blank for
									       	     Lockbox and Disbursement. --%>
										     	<ffi:removeProperty name="previousEntitled"/>
										     	<ffi:removeProperty name="intraEntitled"/>
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
												<ffi:setProperty name="previousEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
												<ffi:setProperty name="previousEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${previousEntitled}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
											</ffi:cinclude>

											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
												<ffi:setProperty name="intraEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
												<ffi:setProperty name="intraEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
											</ffi:cinclude>
										</ffi:cinclude>
										<ffi:cinclude  value1="${AccountsCollectionName}" value2="" operator="notEquals">
								 		<%-- we must determine which data classifications to place in this drop down, based on the entiltment checks on the accounts collection AND the data type of this information --%>
										<ffi:cinclude value1="${DataType}" value2="Summary" operator="equals">
											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
											</ffi:cinclude>
											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
											</ffi:cinclude>
										</ffi:cinclude>
										<ffi:cinclude value1="${DataType}" value2="Detail" operator="equals">
											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
											</ffi:cinclude>
											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
											</ffi:cinclude>
										</ffi:cinclude>
										<ffi:cinclude value1="${DataType}" value2="" operator="equals">
											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryPrev}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
											</ffi:cinclude>
											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryIntra}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
											</ffi:cinclude>
										</ffi:cinclude>
										</ffi:cinclude> <%-- End if accounts collection name is empty --%>
									</select>
								</td>
							</tr>
						</table>

					</td>
					
					<td width="3%" nowrap>&nbsp;</td>
					<td nowrap><span class="sectionsubhead"><s:text name="jsp.default_137"/>&nbsp;</span></td>
					<td nowrap>
						<table>
							<tr>
								<%-- StringUtil element to display text in date fields --%>
								<ffi:setProperty name="StringUtil" property="Value2" value="${UserLocale.DateFormat}" />

								<ffi:setProperty name="StringUtil" property="Value1" value="${DateString}" />
								<td align="left" nowrap>
								<sj:datepicker value="%{#session.StringUtil.notEmpty}" isfirstelement="true" accesskey="d" id="CashFlowCriteriaDate" name="CashFlowCriteriaDate" label="%{getText('jsp.default_137')}" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
								<br><span id="cashFlowCriteriaDateError"></span>
								<script>
									var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=back&calOneDirectionOnly=true"/>';
				                    ns.common.enableAjaxDatepicker("CashFlowCriteriaDate", tmpUrl);
				                    $("#CashFlowCriteriaDate").val("<ffi:getProperty name="StringUtil" property="notEmpty"/>");
								</script>
								</td>

								<%-- td align="center" nowrap><a onclick="CSAction(new Array(/*CMP*/'B9E611E8165'));return CSClickReturn();" href="#" csclick="B9E611E8165"><img src="/cb/web/multilang/grafx/i_calendar.gif" alt="" width="19" height="16" border="0"></a></td --%>
							</tr>
						</table>
					</td>
					
					<td width="3%" nowrap>&nbsp;</td>

                         <td align="left">
							<sj:a targets="view"
								id="viewCashFlowButton"
								formIds="viewCashFlowForm"
								button="true" 
                                validate="true"
                                validateFunction="customValidation"
								onclick="removeValidationErrors();"
								onCompleteTopics="verifyCashFlowCompleteTopic"><s:text name="jsp.default_6" /></sj:a>
							
						</td>
				</tr>
			</table>
			</td>
			<td align="center">
				<sj:a id="cashflowReportingButton"
			   		  button="true"
					  onclick="ns.shortcut.goToFavorites('reporting_cashmgt');"
					  ><s:text name="jsp.default_355"/></sj:a>
			</td>
		</tr>
	</table>
        </s:form>
<ffi:removeProperty name="DataClassification"/>
<ffi:removeProperty name="DateString"/>
<ffi:removeProperty name="CheckPerAccountReportingEntitlements"/>









