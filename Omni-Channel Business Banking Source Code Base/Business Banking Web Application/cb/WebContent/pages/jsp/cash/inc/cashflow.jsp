<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-cashFlow"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_49" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/cashflowSubmenuDropdown.jsp" />
    	</div>
    	
    	 <!-- dashboard items listing for cashflow -->
        <div id="dbCashflowSummary" class="dashboardSummaryHolderDiv">
	       		<sj:a
					id="cashFlowSummaryBtn"
					indicator="indicator"
					cssClass="summaryLabelCls"
					button="true"
					onclick="ns.shortcut.goToMenu('cashMgmt_cashflow');"
					title="%{getText('jsp.transfers.transfer_summary')}"
				><s:text name="jsp.transfers.transfer_summary"/></sj:a>
		</div>
</div>
<script>
	$("#cashFlowSummaryBtn").find("span").addClass("dashboardSelectedItem");
</script>


       
