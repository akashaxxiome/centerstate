<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%
	session.setAttribute("Presentment", request.getParameter("Presentment"));
	if( request.getParameter("AccountID") != null ) session.setAttribute("AccountID", request.getParameter("AccountID"));
	session.setAttribute("DataClassification", request.getParameter("DataClassification"));
	session.setAttribute("BackToURL", request.getParameter("BackToURL"));
	session.setAttribute("disbursementtransactions-reload", request.getParameter("disbursementtransactions-reload"));
	session.setAttribute("GetPage", request.getParameter("GetDisbursementTransactions.Page"));
	session.setAttribute("DisbursementCriteriaDate", request.getParameter("DisbursementCriteriaDate"));
%>

	<%-- complete the back to url with properties --%>
	<ffi:cinclude value1="" value2="${BackToURL}" operator="notEquals">
		<% 
			session.setAttribute("BackToURLNotNull", request.getParameter("BackToURL")); 
		%>

	<ffi:setProperty name="BackToURL" value="true"/> 
	</ffi:cinclude>
	
	<ffi:cinclude value1="" value2="${BackToURL}" operator="equals">
		<% 
			session.setAttribute("BackToURLNull", request.getParameter("BackToURL")); 
		%>
	 <ffi:removeProperty name="BackToURL"/> 
	</ffi:cinclude>
	
<ffi:setProperty name="DateFormat" value="${UserLocale.DateFormat}"/>
	
<ffi:cinclude value1="${GetPage}" value2="current" operator="notEquals">
    <ffi:object id="GetDisbursementTransactions" name="com.ffusion.tasks.disbursement.GetPagedTransactions" scope="session"/>
    <ffi:setProperty name="GetDisbursementTransactions" property="DateFormat" value="${DateFormat}"/>
    <ffi:setProperty name="GetDisbursementTransactions" property="DateFormatParam" value="${DateFormat}"/>
    <ffi:setProperty name="GetDisbursementTransactions" property="StartDate" value=""/>
    <ffi:setProperty name="GetDisbursementTransactions" property="EndDate" value=""/>
    <ffi:setProperty name="GetDisbursementTransactions" property="Page" value="first"/>
    <ffi:setProperty name="GetDisbursementTransactions" property="DataClassification" value="${DataClassification}"/>
    <ffi:setProperty name="GetDisbursementTransactions" property="AccountID" value="${AccountID}"/>
	
	<%-- Used for testng purpose
    <ffi:setProperty name="GetDisbursementTransactions" property="PageSize" value="4"/>
    --%>
    <ffi:cinclude value1="${Presentment}" value2="" operator="notEquals">
    	<ffi:setProperty name="GetDisbursementTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.dataconsolidator.constants.DCConstants.PAGING_CONTEXT_SEARCH_CRITERIA_PRESENTMENT %>"/>
    	<ffi:setProperty name="GetDisbursementTransactions" property="SearchCriteriaValue" value="${Presentment}"/>
    </ffi:cinclude>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetDisbursementTransactions" property="NoSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetDisbursementTransactions" property="AscendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetDisbursementTransactions" property="DescendingSortImage" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
    <ffi:setProperty name="GetDisbursementTransactions" property="SortCriteriaOrdinal" value="1"/>
    <ffi:setProperty name="GetDisbursementTransactions" property="SortCriteriaName" value="<%= com.ffusion.dataconsolidator.constants.DCConstants.PAGING_SORT_CRITERIA_CHECK_DATE %>"/>
    <ffi:setProperty name="GetDisbursementTransactions" property="SortCriteriaAsc" value="True"/>
    <ffi:setProperty name="disbursementtransactions-reload" value="false"/>
	<ffi:process name="GetDisbursementTransactions"/>
												
	<%
         session.setAttribute("FFIGetDisbursementTransactions", session.getAttribute("GetDisbursementTransactions"));
    %>
</ffi:cinclude>
	
<ffi:setProperty name="BankingAccounts" property="Filter" value="ID=${GetDisbursementTransactions.AccountID}"/>
<ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="notEquals">
	<ffi:list collection="BankingAccounts" items="acct" startIndex="1" endIndex="1">
	</ffi:list>
</ffi:cinclude>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
	
	<br>
	
	<s:include value="/pages/jsp/cash/cashdisbursedetail_grid.jsp"/> 

<ffi:removeProperty name="DataClassification"/>
<ffi:removeProperty name="DateFormat"/>
<ffi:removeProperty name="SortImage"/>
<ffi:removeProperty name="AccountID"/>

<script>

$('#disbursementDetailIcon').portlet({
	generateDOM: true,
	helpCallback: function(){
		var helpFile = $('#disbursementDetailIcon').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	}
});


$(function(){
	var title = js_account_disbursement_portlet_title;
	var routingNum = '<ffi:getProperty name="acct" property="RoutingNum"/>';
	var displayText = '<ffi:getProperty name="acct" property="DisplayText"/>';
	var nickName = '<ffi:getProperty name="acct" property="NickName"/>';
	var currencyCode = '<ffi:getProperty name="acct" property="CurrencyCode"/>';
	var startDate = '<ffi:getProperty name="DisburseDate"/>';				
	$("#disbursementDetailIcon").portlet('title', title +" "+ routingNum + ":" + displayText + "-" + nickName + "-" + currencyCode + ' <s:text name="jsp.cash_130" /> ' + startDate);
});
</script>