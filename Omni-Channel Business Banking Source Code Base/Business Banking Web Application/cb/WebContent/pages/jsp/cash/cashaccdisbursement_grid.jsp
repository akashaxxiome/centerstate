<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


	<div class="innerPortletsContainer">
	<div id="AccountDisbursementIcon">
	<ffi:help id="cash_cashdisbursement_Account" />
	
	<ffi:setGridURL grid="GRID_cashAccDisburse" name="ViewURL" url="/cb/pages/jsp/cash/cashdisbursedetail.jsp?AccountID={0}&Date={1}&DataClassification={2}&disbursementtransactions-reload=true" parm0="Account.AccountID" parm1="date" parm2="dataClassification"/>
	<ffi:setProperty name="cashAccDisbursementTempURL" value="/pages/jsp/cash/GetAccDisbursementAction.action?collectionName=DisbursementSummaries&GridURLs=GRID_cashAccDisburse" URLEncrypt="true"/>
    <s:url id="cashaccdisbursementURL" value="%{#session.cashAccDisbursementTempURL}" escapeAmp="false"/>

<s:if test="%{#parameters.dashboardElementId[0] == 'disbursementAccount'}">
	<ffi:setProperty name="gridDataType" value="json"/>
</s:if>
<s:else>
	<ffi:setProperty name="gridDataType" value="local"/>
</s:else>

	<sjg:grid  
		id="cashaccdisbursementID"  
		caption=""  
		sortable="true"  
		dataType="%{#session.gridDataType}"  
		href="%{cashaccdisbursementURL}"  
		pager="true"  
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		gridModel="gridModel" 
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		sortname="ACCOUNTID" 
		onGridCompleteTopics="addGridControlsEvents,cashaccdisbursementEvent"
		> 
		
		<sjg:gridColumn name="account" index="ACCOUNTID" title="%{getText('jsp.default_15')}" sortable="true" width="250" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="accountNickName" index="NICKNAME" title="%{getText('jsp.cash_13')}" sortable="true" width="250" cssClass="datagrid_textColumn"/>
	    <sjg:gridColumn name="currentBalance.currencyStringNoSymbol" index="CURRENTBALANCE" title="%{getText('jsp.default_60')}" sortable="true" width="100" cssClass="datagrid_textColumn"/>
	    <sjg:gridColumn name="numItemsPending" index="NUMITEMSPENDING" title="%{getText('jsp.cash_60')}" sortable="true" width="120" cssClass="datagrid_actionColumn"/>
	     <sjg:gridColumn name="totalDebits.currencyStringNoSymbol" index="TOTALDEBITS" title="%{getText('jsp.default_43')}" sortable="true" width="90" cssClass="datagrid_textColumn" formatter="ns.cash.totalDebitsFormatter" />
		<sjg:gridColumn name="immediateFundsNeeded.currencyStringNoSymbol" index="IMMEDIATEFUNDSNEEDED" title="%{getText('jsp.cash_FundsToday')}" sortable="true" width="120" cssClass="datagrid_textColumn" formatter="ns.cash.immediateFundsFormatter" />
		<sjg:gridColumn name="oneDayFundsNeeded.currencyStringNoSymbol" index="ONEDAYFUNDSNEEDED" title="%{getText('jsp.cash_FundsOneDay')}" sortable="true" width="140" cssClass="datagrid_textColumn" formatter="ns.cash.oneDayFundsFormatter" />
		<sjg:gridColumn name="twoDayFundsNeeded.currencyStringNoSymbol" index="TWODAYFUNDSNEEDED" title="%{getText('jsp.cash_FundsTwoDay')}" sortable="true" width="140" cssClass="datagrid_textColumn" formatter="ns.cash.twoDaysFundsFormatter" />
		
		<sjg:gridColumn name="map.rountingNumber" index="rountingNumber" title="%{getText('jsp.cash_95')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>
		<sjg:gridColumn name="map.accountDisplayText" index="accountDisplayText" title="%{getText('jsp.cash_8')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="map.accountName" index="accountName" title="%{getText('jsp.cash_12')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="map.currencyType" index="currencyType" title="%{getText('jsp.cash_43')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.date" index="date" title="%{getText('jsp.default_137')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_dateColumn"/>
        <sjg:gridColumn name="map.dataClassification" index="dateClassification" title="%{getText('jsp.cash_48')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="account.accountID" index="accountID" title="%{getText('jsp.cash_10')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="presentment" index="presentment" title="%{getText('jsp.default_328')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="showLink" index="showLink" title="%{getText('jsp.cash_99')}" sortable="" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		
		<%--for double sorting --%>
		<sjg:gridColumn name="name" index="name" title="%{getText('jsp.default_15')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
	</sjg:grid>
	<s:text name="jsp.cash_DisbursmentComment" />
</div>
	</div>
<ffi:removeProperty name="gridDataType"/>	