<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%-- 
	In case of file upload this file will be loaded in Iframe used to upload file.
	In that case it will show javascript error as JQuery won't be available.
	Added following functionality to make sure error won't be shown.
 --%>
<s:include value="%{#session.PagesPath}/common/commonUploadIframeCheck_js.jsp" />
	
		
		
		<span class="sectionhead"><s:text name="jsp.cash_54"/><br/><br/></span>
		
		<ffi:cinclude value1="${ImportErrors}" value2="" operator="notEquals">
			<ffi:cinclude value1="${ImportErrors.size}" value2="0" operator="notEquals">
				<s:include value="%{#session.PagesPath}/common/fileImportErrorsGrid.jsp"/>
			</ffi:cinclude>
		</ffi:cinclude>
	
<span class="sectionhead"><s:text name="jsp.cash_80"/><br/><br/></span>
	<div valign="bottom">
		<div align="center">
			<sj:a id="uploadFailureCancel"
			button="true" 
			onClickTopics="closeDialog"
			><s:text name="jsp.default_111"/></sj:a>
		</div>
	</div>
	<br/>
	
		
		 
