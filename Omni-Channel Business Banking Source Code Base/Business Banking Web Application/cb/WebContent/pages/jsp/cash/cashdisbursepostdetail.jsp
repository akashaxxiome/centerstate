<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%@page import="com.ffusion.beans.disbursement.DisbursementSummaries" %>
<%@page import="com.ffusion.beans.disbursement.DisbursementSummary" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>



<%
	session.setAttribute("Presentment", request.getParameter("Presentment"));
	session.setAttribute("Date", request.getParameter("Date"));
	session.setAttribute("DataClassification", request.getParameter("DataClassification"));
	session.setAttribute("DisbursementCriteriaDate", request.getParameter("DisbursementCriteriaDate"));

%>

<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
	<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY %>"/>
	</ffi:cinclude>
</ffi:cinclude>   

<ffi:setProperty name="DateFormat" value="${UserLocale.DateFormat}"/>

<ffi:object id="GetSummariesForPresentment" name="com.ffusion.tasks.disbursement.GetSummariesForPresentment" scope="session"/>
<ffi:setProperty name="GetSummariesForPresentment" property="PresentmentName" value="${Presentment}"/>
<ffi:setProperty name="GetSummariesForPresentment" property="DateFormat" value="${DateFormat}"/>
<ffi:setProperty name="GetSummariesForPresentment" property="StartDate" value="${Date}"/>
<ffi:setProperty name="GetSummariesForPresentment" property="EndDate" value="${Date}"/>
<ffi:setProperty name="GetSummariesForPresentment" property="DataClassification" value="${DataClassification}"/>
<ffi:process name="GetSummariesForPresentment"/>

				<%
                  session.setAttribute("FFIGetSummariesForPresentment", session.getAttribute("GetSummariesForPresentment"));
                 %>

<ffi:setProperty name="DisbursementSummariesForPresentment" property="SortedBy" value="${SortCriteria}"/>

<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashdisbursement.jsp" URLEncrypt="true"/>
<ffi:setProperty name="TransferHomeURL" value="${SecurePath}cash/cashdisbursepostdetail.jsp" URLEncrypt="true"/>

	<div align="center">

		<br>
		<s:include value="/pages/jsp/cash/cashdisbursepostdetail_grid.jsp"/> 
	
		<br>
	<ffi:setProperty name="BackToURL" value="${SecurePath}/cash/cashdisbursement.jsp?DisbursementDataClassification=${DataClassification}&DisbursementCriteriaDate=${Date}" URLEncrypt="true"/>
	
  <%--  <input class="submitbutton" align="center" type="button" value="BACK" onclick="document.location='<ffi:urlEncrypt url="${BackToURL}"/>';">  --%>
	</div>

<ffi:removeProperty name="GetSummariesForPresentment"/>
<ffi:removeProperty name="DateFormat"/>
<ffi:removeProperty name="DataClassification"/>
<ffi:removeProperty name="SortCriteria"/>
<ffi:removeProperty name="BackToURL"/>
<script>
	$('#disbursementPostDetailIcon').portlet({
		generateDOM: true,
		helpCallback: function(){
			var helpFile = $('#disbursementPostDetailIcon').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
	});
	
	$(function(){
		var title = js_disbuserment_postdetail_portlet_title;
		var startDate = '<ffi:getProperty name="Date" />';
		var presentment = '<ffi:getProperty name="Presentment"/>';
		$("#disbursementPostDetailIcon").portlet('title', title + ' "' + presentment + '" <s:text name="jsp.cash_130" /> ' + startDate);
	});
</script>
