<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:setProperty name='PageHeading' value='Lockbox'/>
<ffi:setProperty name="PortalFlag" value="false"/>

<ffi:setProperty name='PageText' value=''/>
<ffi:cinclude value1="${CashReportsDenied}" value2="false" operator="equals">
<ffi:setProperty name='PageText' value='<a href="${SecurePath}reports/cash_list.jsp"><img src="/cb/web/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0"></a>'/>
</ffi:cinclude>

	
	
<div id="lockboxDesktop" align="center">
		<script type="text/javascript" src="<s:url value='/web/js/cash/cash_grid%{#session.minVersion}.js'/>"></script>
		<ffi:setProperty name="subMenuSelected" value="lockbox"/>

		<%--Set up variables for the specifydatecriteria.jsp page, and then remove them--%>
		<ffi:setProperty name="SubmitURL" value="${SecurePath}cash/cashlockbox.jsp"/>
		<ffi:setProperty name="DateVariable" value="LockboxCriteriaDate"/>
		<ffi:setProperty name="DataClassificationVariable" value="LockboxDataClassification"/>

		<%-- Determine if entitled to see anything at all - check if any of the 4 IR permissions is set. --%>
		<ffi:removeProperty name="IRentitled"/>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
			<ffi:setProperty name="IRentitled" value="true"/>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL%>">
			<ffi:setProperty name="IRentitled" value="true"/>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
			<ffi:setProperty name="IRentitled" value="true"/>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
			<ffi:setProperty name="IRentitled" value="true"/>
		</ffi:cinclude>
		<%-- Display graceful message if not entitled to any of the 4 information reporting entitlements --%>

		<ffi:cinclude value1="${IRentitled}" value2="true" operator="notEquals">
			<table width="750" border="0" cellspacing="0" cellpadding="0"style="display:none">
				<tbody>
					<tr height="13">
						<td colspan="6" align="left" width="750" height="14" background="/cb/web/multilang/grafx/cash/sechdr_blank.gif"><span class="sectiontitle sectionsubhead">&nbsp; > <s:text name="jsp.cash_66"/></span></td>
					</tr>
					<tr>
						<td width="1" class="tbrd_ltb">&nbsp;</td>
						<td align="center" class="tbrd_tb" colspan="4" nowrap>
							<span align="center" class="columndata">
								<s:text name="jsp.cash_105"/>
							</span>
						</td>
						<td width="1" class="tbrd_trb"> &nbsp; </td>
					</tr>
					<tr>
						<td colspan="6"><img src="/cb/web/multilang/grafx/cash/sechdr_cash_btm.gif" alt="" width="750" height="11" border="0"></td>
					</tr>
				</tbody>
			</table>
		</ffi:cinclude>

			

				
				<div id="appdashboardForLockbox" >
					<s:include value="/pages/jsp/cash/cashlockboxDisbursementDashboard.jsp"/>
				</div>
				
				<div id="operationresult">
					<div id="resultmessage"><s:text name="jsp.default_498"/></div>
				</div><!-- result -->
				
			
			
				<ffi:cinclude value1="${IRentitled}" value2="true" operator="equals">
				<div id="cashLockboxList" class="portlet">
					<div class="portlet-header">
							<div class="searchHeaderCls">
								<span class="searchPanelToggleAreaCls" onclick="$('#search').slideToggle();">
									<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
								</span>
								<div class="summaryGridTitleHolder">
									<span id="selectedGridTab" class="selectedTabLbl">
										<s:text name="jsp.default_531"/>
									</span>
								</div>
							</div>
						</div>
						<div class="portlet-content">
							<div id="search" style="display:none;">
								<s:include value="/pages/jsp/cash/inc/specifycriteriaforlockbox.jsp"/>
							</div>
							<div id="summary" class="clearBoth marginTop10">
								<s:include value="/pages/jsp/cash/inc/lockbox.jsp"/>
							</div>
						</div>
						<div id="cashLockBoxDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
						     <ul class="portletHeaderMenu" style="background:#fff">
						           <li><a href='#' onclick="ns.common.showDetailsHelp('cashLockboxList')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
						      </ul>
						</div>		
				</div>
				</ffi:cinclude>
				
				<div id="LockboxListForDeposit">
				</div>

					
				<div id="LockboxListForAccount" class="marginTop10">
				</div>

		<ffi:removeProperty name="LockboxCriteriaDate"/>
		<ffi:removeProperty name="LockboxDataClassification"/>
		<ffi:removeProperty name="DateVariable"/>
		<ffi:removeProperty name="DataClassificationVariable"/>
		<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashlockbox.jsp" URLEncrypt="true"/>
		<ffi:removeProperty name="PortalFlag"/>

</div>

<script>
ns.common.initializePortlet("cashLockboxList"); 
</script>
