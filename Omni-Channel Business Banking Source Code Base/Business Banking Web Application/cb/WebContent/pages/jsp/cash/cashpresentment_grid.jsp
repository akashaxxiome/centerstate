<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div class="innerPortletsContainer clearBoth marginTop10">
<div id="disbursementPresentmentIcon">
<ffi:help id="cash_cashdisbursement_Presentment" />

<ffi:setGridURL grid="GRID_cashPresetment" name="ViewURL" url="/cb/pages/jsp/cash/cashdisbursepostdetail.jsp?Presentment={0}&Date={1}&DataClassification={2}" parm0="Presentment" parm1="date" parm2="dataClassification"/>
	
<ffi:setProperty name="cashPresentSummaryTempURL" value="/pages/jsp/cash/GetPresentmentSummaryAction.action?collectionName=DisbursementPresentmentSummaries&GridURLs=GRID_cashPresetment" URLEncrypt="true"/>
<s:if test="%{#parameters.dashboardElementId[0] != 'disbursementPresentment'}">
	<ffi:setProperty name="gridDataType" value="local"/>
</s:if>
<s:else>
	<ffi:setProperty name="gridDataType" value="json"/>
</s:else>

    <s:url id="cashpresentSummaryURL" value="%{#session.cashPresentSummaryTempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="cashpresentSummaryID"  
		caption=""  
		sortable="true"  
		dataType="%{#session.gridDataType}"  
		href="%{cashpresentSummaryURL}"  
		pager="true"  
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		gridModel="gridModel" 
		viewrecords="true"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		onGridCompleteTopics="addGridControlsEvents,cashpresentSummaryEvent"
		> 
		
		<sjg:gridColumn name="presentment" index="presentment" title="%{getText('jsp.default_328')}" sortable="true" width="200" cssClass="datagrid_textColumn"/>
	    <sjg:gridColumn name="numItemsPending" index="numItemsPending" title="%{getText('jsp.cash_60')}" sortable="true" width="100"  cssClass="datagrid_actionColumn"/>
	    <sjg:gridColumn name="totalDebits.currencyCode" index="currencyCode" title="%{getText('jsp.default_125')}" sortable="true" width="100" cssClass="datagrid_amountColumn"/>
	    <sjg:gridColumn name="totalDebits.currencyStringNoSymbol" index="totalDebits.currencyStringNoSymbol" title="%{getText('jsp.default_43')}" sortable="true" width="90" cssClass="datagrid_amountColumn"/>

		<sjg:gridColumn name="map.date" index="date" title="%{getText('jsp.default_137')}" sortable="true" width="90" hidden="true" hidedlg="true" cssClass="datagrid_dateColumn"/>
	    <sjg:gridColumn name="map.dataClassification" index="dataClassification" title="%{getText('jsp.cash_47')}" sortable="true" width="90" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
	
	</sjg:grid>
</div>
</div>
<ffi:removeProperty name="gridDataType"/>