<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<%
	
	String paramZoom = request.getParameter("paramZoom");
	String paramRotationBy = request.getParameter("paramRotationBy");
	
%>

<ffi:setProperty name="paramZoom" value="<%= paramZoom %>" />
<ffi:setProperty name="paramRotationBy" value="<%= paramRotationBy %>" />

<%-- Flag to check whether to retrieve image or not --%>
<ffi:setProperty name="getImageFlag" value="true"/>

<ffi:cinclude value1="true" value2="${getImageFlag}" operator="equals" >

	<ffi:object name="com.ffusion.tasks.checkimaging.GetImageIndex" id="GetImageIndex"/>
		<ffi:setProperty name="GetImageIndex" property="Collection" value="ImageResults" />
	<ffi:process name="GetImageIndex"/>

	<ffi:object name="com.ffusion.tasks.checkimaging.GetCheckedImages" id="GetCheckedImages"/>

	<ffi:list collection="ImageResults" items="ImageResult1">
		<ffi:setProperty name="GetCheckedImages" property="ID" value="${ImageResult1.ImageHandle}" />
	</ffi:list>

	<ffi:process name="GetCheckedImages"/>

	<ffi:object name="com.ffusion.tasks.checkimaging.GetImages" id="GetImages"/>
	<ffi:process name="GetImages"/>

	<ffi:object name="com.ffusion.tasks.checkimaging.DisplayImage" id="DisplayFrontImage" scope="session"/>
	<ffi:object name="com.ffusion.tasks.checkimaging.DisplayImage" id="DisplayBackImage" scope="session"/>
		<ffi:setProperty name="DisplayFrontImage" property="ImageSide" value="FRONT" />
		<ffi:setProperty name="DisplayBackImage" property="ImageSide" value="BACK" />

	<%-- Task for sending email with images --%>
	<ffi:object name="com.ffusion.tasks.checkimaging.EmailImages" id="EmailImages" scope="session"/>

		<%
			session.setAttribute("FFIEmailImages", session.getAttribute("EmailImages"));
        %>
        
	<ffi:setProperty name="getImageFlag" value="false"/>

</ffi:cinclude>

<ffi:setProperty name="availableImageFlag" value="true"/>

<%-- Set defaults for rotation and zoom values if not already set --%>

<ffi:cinclude value1="${paramZoom}" value2="" operator="equals">
	<ffi:setProperty name="zoom" value="0"/>
</ffi:cinclude>
<ffi:cinclude value1="${paramZoom}" value2="" operator="notEquals">
	<ffi:setProperty name="zoom" value="${paramZoom}"/>
</ffi:cinclude>
<ffi:cinclude value1="${paramRotationBy}" value2="" operator="equals">

	<ffi:setProperty name="rotationBy" value="0"/>
	<%-- Initialize the rotation (in case we have left-over values from a previous popup) --%>
	<ffi:setProperty name="DisplayFrontImage" property="Rotation" value="0" />
	<ffi:setProperty name="DisplayBackImage" property="Rotation" value="0" />	
</ffi:cinclude>
<ffi:cinclude value1="${paramRotationBy}" value2="" operator="notEquals">
	<%
		String rotationBy = (String)session.getAttribute("rotationBy");
		Integer paramRotationByInt = Integer.valueOf(paramRotationBy);
		int finalRotationByInt = Integer.valueOf(rotationBy).intValue() + paramRotationByInt.intValue(); 
		String finalRotation = Integer.toString(finalRotationByInt);
	%>
	<ffi:setProperty name="rotationBy" value="<%= finalRotation %>"/>
</ffi:cinclude>
<ffi:removeProperty name="paramZoom"/>
<ffi:removeProperty name="paramRotationBy"/>

<%-- Set the rotation --%>
<ffi:setProperty name="DisplayFrontImage" property="RotateBy" value="${rotationBy}" />
<ffi:setProperty name="DisplayBackImage" property="RotateBy" value="${rotationBy}" />	
<%-- Set the image type --%>
<ffi:setProperty name="DisplayFrontImage" property="RotatedImageFormat" value="1" />
<ffi:setProperty name="DisplayBackImage" property="RotatedImageFormat" value="1" />	


<%!
	// Stores the check image size in the session (in accordance with the zoom amount)
	private void calculateImageSize( HttpSession session ) {
		int baseWidth = 420;
		int baseHeight = 195;
		int zoomWidth = 40;
		int zoomHeight = 15;
		int zoomAmount = Integer.parseInt( session.getAttribute("zoom").toString() );
		
		// normalize zoom amount (only a problem if the user enters the value in the url)
		if ( zoomAmount > 3 ) {
			zoomAmount = 3;
			session.setAttribute("zoom", zoomAmount + "");
		}
		else if ( zoomAmount < 0 ) {
			zoomAmount = 0;
			session.setAttribute("zoom", zoomAmount + "");		
		}
		
		session.setAttribute( "imageWidth", ( baseWidth + ( zoomWidth *  zoomAmount ) ) + "" );
		session.setAttribute( "imageHeight", ( baseHeight + ( zoomHeight *  zoomAmount ) ) + "" );
	}
	
	// Stores the layout of the checks, based on rotation, in the session
	//	the check images can either be organized horizontally or vertically
	private void calculateLayout( HttpSession session ) {
		int rotation = ((com.ffusion.tasks.checkimaging.DisplayImage)session.getAttribute("DisplayFrontImage")).getRotationValue();
		System.out.println("###########"+rotation);
		if ( rotation == 0 || rotation == 180 ) {
			System.out.println("vertical");
			session.setAttribute( "imageLayout", "vertical" );
		}
		else {
			System.out.println("horizontal");
			session.setAttribute( "imageLayout", "horizontal" );
		}
	}
%>
<%-- Calculate new image size (for zooming) --%>
<% calculateImageSize(session); %>
<%-- Calculate the image layout (based on desired rotation) --%>
<% calculateLayout(session); %>



<script>
	// zooms in/out on the images through a page reload
	//	direction can be either "in" or "out"
	function zoom(direction) {
		<%
			int varZoomInt;
			String varZoomString = "";
		%>
		if ( direction == "in" ) {
		
			<% 
				varZoomInt = Integer.parseInt( session.getAttribute("zoom").toString() );
				varZoomInt+=1;
				varZoomString = Integer.toString(varZoomInt);
				String inUrl = "/cb/pages/jsp/cash/checkimage.jsp?paramRotationBy=0&paramZoom=" + varZoomString;
			%>
			ns.cash.zoomImageURL = "<ffi:urlEncrypt url='<%= inUrl %>'/>";
		}
	   	else if (direction == "out") {
			<% 
				varZoomInt = Integer.parseInt( session.getAttribute("zoom").toString() );
				varZoomInt-=1;
				varZoomString = Integer.toString(varZoomInt);
				String outUrl = "/cb/pages/jsp/cash/checkimage.jsp?paramRotationBy=0&paramZoom=" + varZoomString;
			%>
			ns.cash.zoomImageURL = "<ffi:urlEncrypt url='<%= outUrl %>'/>";
	   	}
    	
    	ns.cash.zoomImage(ns.cash.zoomImageURL);
    	
	}
	
	// rotates the images through a page reload
	//	rotation can be any multiple of 90 (anything else rotates the images back to default position)
	function rotate(rotation) {
    	
    	if(rotation == "90"){
			ns.cash.rotationFunctionURL = "<ffi:urlEncrypt url='/cb/pages/jsp/cash/checkimage.jsp?paramRotationBy=90&paramZoom=${zoom}' />";
    	}else if(rotation == "-90"){
			ns.cash.rotationFunctionURL = "<ffi:urlEncrypt url='/cb/pages/jsp/cash/checkimage.jsp?paramRotationBy=-90&paramZoom=${zoom}' />";
    	}
    	ns.cash.rotationFunction(ns.cash.rotationFunctionURL);
	}

	function closeCheckImage(){		
		ns.common.closeDialog();
		$.publish("common.topics.tabifyNotes");
	}
</script>


<ffi:list collection="AvailableImages" items="image">

	<ffi:cinclude value1="true" value2="${availableImageFlag}" operator="equals" >


	<div align="center">
    <div  id="CheckImageDiv">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<br>
				<tr>
					<td colspan="2">
						<center>
							<ffi:cinclude value1="${image.Status}" value2="PENDING" operator="equals" >
								<s:text name="jsp.default_527"/>
							</ffi:cinclude>
	
							<ffi:cinclude value1="${image.Status}" value2="AVAILABLE" operator="equals" >
	
								<ffi:cinclude value1="true" value2="${getImageFlag}" operator="equals" >
									<%-- ADD FEE DETAILS IN AUDIT LOG --%>
									<ffi:object name="com.ffusion.tasks.checkimaging.LogFee" id="LogFee"/>
										<ffi:setProperty name="LogFee" property="ChargeFlag" value="0" />
										<ffi:setProperty name="LogFee" property="ImageID" value="${image.ImageHandle}" />
										<ffi:setProperty name="LogFee" property="ChargeAccount" value="${image.ChargeAccount}" />
	
									<ffi:process name="LogFee"/>
								</ffi:cinclude>
	
								<%-- Common image properties --%>
								<ffi:setProperty name="DisplayFrontImage" property="ViewerID" value="${image.ImageHandle}" />
								<ffi:setProperty name="DisplayBackImage" property="ViewerID" value="${image.ImageHandle}" />
	
	
								<%-- Vertical Layout (if images are on top of each other) --%>
								<ffi:cinclude value1="${imageLayout}" value2="vertical" operator="equals">							
									<!-- FRONT IMAGE -->
									<img src="/cb/pages/jsp/cash/DisplayFrontImageAction.action?time=<%= System.currentTimeMillis() %>" alt="<s:text name="jsp.default_461"/>" width="<ffi:getProperty name="imageWidth"/>" height="<ffi:getProperty name="imageHeight"/>" border="1">
									<br/>		
									<!-- BACK IMAGE -->
									<img src="/cb/pages/jsp/cash/DisplayBackImageAction.action?time=<%= System.currentTimeMillis() %>" alt="<s:text name="jsp.default_461"/>" width="<ffi:getProperty name="imageWidth"/>" height="<ffi:getProperty name="imageHeight"/>" border="1">
									<br/>
								</ffi:cinclude>
	
								<%-- Horizontal Layout (if images are beside each other) --%>
								<ffi:cinclude value1="${imageLayout}" value2="horizontal" operator="equals">
									<!-- FRONT IMAGE -->
									<img src="/cb/pages/jsp/cash/DisplayFrontImageAction.action?time=<%= System.currentTimeMillis() %>" alt="<s:text name="jsp.default_461"/>" height="<ffi:getProperty name="imageWidth"/>" width="<ffi:getProperty name="imageHeight"/>" border="1">
									<!-- BACK IMAGE -->
									<img src="/cb/pages/jsp/cash/DisplayBackImageAction.action?time=<%= System.currentTimeMillis() %>" alt="<s:text name="jsp.default_461"/>" height="<ffi:getProperty name="imageWidth"/>" width="<ffi:getProperty name="imageHeight"/>" border="1">
									<br/>
								</ffi:cinclude>

							</ffi:cinclude>
						</center>
					</td>
				</tr>
				
				<tr>
					<td colspan="2"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="500" height="8" border="0"></td>
				</tr>
				
				<tr>
					<td class="sectionsubhead" align="right" width="50%"><s:text name="jsp.default_528"/></td>
					<td class="columndata" width="50%">
						<ffi:setProperty name="image" property="DateFormat" value="${UserLocale.DateFormat}"/>
						<ffi:getProperty name="image" property="PostingDate" />
					</td>
				</tr>
				
				<tr>
					<td class="sectionsubhead" align="right" width="50%"><s:text name="jsp.default_523"/></td>
					<td class="columndata" width="50%"><ffi:getProperty name="PPayIssue" property="CheckRecord.Amount" /></td>
				</tr>
				
				<tr>
					<td class="sectionsubhead" align="right" width="50%"><s:text name="jsp.default_21"/></td>
					<td class="columndata" width="50%">
					
					<ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="session"/>
					<ffi:setProperty name="AccountDisplayTextTask" property="AccountID" value="${PPayIssue.CheckRecord.AccountID}"/>
					<ffi:process name="AccountDisplayTextTask"/>
					<ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
					<ffi:removeProperty name="AccountDisplayTextTask"/>
				</tr>
				
				<tr>
					<td class="sectionsubhead" align="right" width="50%"><s:text name="jsp.default_522"/></td>
					<td class="columndata" width="50%"><ffi:getProperty name="PPayIssue" property="CheckRecord.CheckNumber" /></td>
				</tr>
				
				<tr>
					<td colspan="2"><img src="/cb/web/multilang/grafx/cash/spacer.gif" alt="" width="500" height="8" border="0"></td>
				</tr>
                </table>
            </div>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">

				<tr>
					<td colspan="2" align="center">
						<s:form action="" method="post" name="checkImageForm" id="checkImageID" theme="simple">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<sj:a 
									id="printID"
									button="true" 
									onClick="$('#CheckImageDiv').print();"
									><s:text name="jsp.default_331"/></sj:a>
							
							<script>
								function emailImage() {
									var urlString = "<ffi:urlEncrypt url='/cb/pages/jsp/cash/EmailImagesAction.action?Init=true&ImageId=${image.ImageHandle}'/>";
									
									$.ajax({
										url: urlString,
										success: function(data){
											ns.common.closeDialog();
											$('#searchImageDialog').html(data).dialog('open');
										}
									});
								}
							</script>

								<sj:a 
									id="emailID"
									button="true"
									onClick="emailImage()"
									><s:text name="jsp.default_183"/></sj:a>
								
							<ffi:cinclude value1="${zoom}" value2="0" operator="notEquals">
								
								<sj:a 
									id="zoomOutID"
									button="true" 
									onClick="zoom('out')"
									><s:text name="jsp.default_475"/></sj:a>
								
							</ffi:cinclude>
							<ffi:cinclude value1="${zoom}" value2="3" operator="notEquals">
								
								<sj:a 
									id="zoomInID"
									button="true" 
									onClick="zoom('in')"
									><s:text name="jsp.default_474"/></sj:a>
							
							</ffi:cinclude>
							<sj:a 
								id="rotateRightID"
								button="true" 
								onClick="rotate('90')"
								><s:text name="jsp.default_363"/></sj:a>
							<sj:a 
								id="rotateLeftID"
								button="true" 
								onClick="rotate('-90')"
								><s:text name="jsp.default_364"/></sj:a>
							<sj:a 
								id="closeCheckImageID"
								button="true" 
								onClick="closeCheckImage()"
								><s:text name="jsp.default_102"/></sj:a>
		
		

						</s:form>
					</td>
				</tr>
				
				<%-- ERROR DISPLAY --%>
				<tr>
					<td colspan="2" align="center">
						<ffi:cinclude value1="${DisplayFrontImage.DisplayImageError}" value2="0" operator="notEquals">
							<!-- Start: Error Info (FrontImage) -->
						   	<table cellpadding="1" cellspacing="2" border="0">
							  	<tr>
							      	<td align="right" class="homesub"><b>(<s:text name="jsp.cash_112"/>) <s:text name="jsp.default_521"/></b>&nbsp;</td>
							      	<td class="errorBackground">
							      		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${DisplayFrontImage.DisplayImageError}_alert" />
										<ffi:getProperty name="ErrorsResource" property="Resource"/>
							      	</td>
							   	</tr>
							   	<tr>
							      	<td align="right" class="homesub"><b>(<s:text name="jsp.cash_112"/>) <s:text name="jsp.default_171"/></b>&nbsp;</td>
							      	<td class="errorBackground">
							      		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${DisplayFrontImage.DisplayImageError}_descr" />
										<ffi:getProperty name="ErrorsResource" property="Resource"/>
							      	</td>
							   	</tr>								
							</table>
							<!-- End: Error Info (FrontImage) -->
						</ffi:cinclude>
		
						<ffi:cinclude value1="${DisplayBackImage.DisplayImageError}" value2="0" operator="notEquals">
							<!-- Start: Error Info (BackImage) -->
						   	<table cellpadding="1" cellspacing="2" border="0">
							  	<tr>
							      	<td align="right" class="homesub"><b>(<s:text name="jsp.cash_20"/>) <s:text name="jsp.default_521"/></b>&nbsp;</td>
							      	<td class="errorBackground">
							      		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${DisplayBackImage.DisplayImageError}_alert" />
										<ffi:getProperty name="ErrorsResource" property="Resource"/>
							      	</td>
							   	</tr>
							   	<tr>
							      	<td align="right" class="homesub"><b>(<s:text name="jsp.cash_20"/>) <s:text name="jsp.default_171"/></b>&nbsp;</td>
							      	<td class="errorBackground">
							      		<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${DisplayBackImage.DisplayImageError}_descr" />
										<ffi:getProperty name="ErrorsResource" property="Resource"/>
							      	</td>
							   	</tr>								
							</table>
							<!-- End: Error Info (BackImage) -->
						</ffi:cinclude>
					</td>
				</tr>
				
			</table>
		</div>

	</ffi:cinclude>

	<ffi:setProperty name="availableImageFlag" value="false"/>

</ffi:list>