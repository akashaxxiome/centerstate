<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="cash_cashppaybuildconfirm" />

<ffi:cinclude value1="${CashReportsDenied}" value2="false" operator="equals">
	<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/cash_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0"></a>'/>
</ffi:cinclude>
<ffi:setProperty name='Temp1' value='${SecurePath}cash/cashppaybuild.jsp?positivepay-buildFromBlank=true' URLEncrypt="true"/>
<ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}cash/cashppupload.jsp"><img src="/cb/pages/${ImgExt}grafx/cash/button_uploadrecord.gif" alt="" width="150" height="16" border="0" hspace="6"></a><a href="${Temp1}"><img src="/cb/pages/${ImgExt}grafx/cash/button_buildmanualfile_dim.gif" alt="" width="120" height="16" border="0"></a>'/>

<%-- ffi:setProperty name="CreateCheckRecords" property="SuccessURL" value=""/>
<ffi:setProperty name="CreateCheckRecords" property="ValidateOnly" value="true"/>	
<ffi:process name="CreateCheckRecords"/ --%>

<ffi:cinclude value1="${ppayBuildTouched}" value2="true" operator="notEquals" >
    <ffi:process name="CreateCheckRecords"/>
    <ffi:setProperty name="ppayBuildTouched" value="true"/>



</ffi:cinclude>
<ffi:object id="GetCheckRecordsAmountTotal" name="com.ffusion.tasks.positivepay.GetCheckRecordsAmountTotal" scope="request"/>
<ffi:process name="GetCheckRecordsAmountTotal"/>

<ffi:setProperty name="BackURL" value="${SecurePath}cash/cashppaybuildconfirm.jsp" URLEncrypt="true"/>
<ffi:setProperty name="positivepay-buildFromBlank" value="false"/>

        <div align="center">


            <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
                    <td align="left">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                            <tr>

                        <td align="center" class="ltrow2_color">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="2">
                                       <%--  <tr valign="middle">
                                            <td class="tbrd_b" colspan="7" align="left" nowrap><span><s:text name="jsp.cash_79"/></span></td>
                                        </tr> --%>
                                        <tr valign="middle">
                                            <td class="sectionsubhead" nowrap><s:text name="jsp.default_137"/></td>
                                            <td class="sectionsubhead" nowrap><s:text name="jsp.default_15"/></td>
                                            <td class="sectionsubhead" nowrap><s:text name="jsp.default_92"/></td>
                                            <td class="sectionsubhead" style="text-align:right" nowrap><s:text name="jsp.default_43"/></td>
                                            <td class="sectionsubhead" nowrap>&nbsp;&nbsp;</td>
                                            <td class="sectionsubhead" nowrap><s:text name="jsp.cash_14"/></td>
                                            <td class="sectionsubhead" nowrap>
                                                <div align="center">
                                                    <s:text name="jsp.cash_122"/></div>
                                            </td>
                                        </tr>

	<% boolean toggle = false; String cls = ""; %>
                                        <ffi:list collection="PPayCheckRecords" items="CheckRecord2">

                                            <%--Only output non-empty records--%>
                                            <ffi:cinclude value1="${CheckRecord2.CheckDate}" value2="" operator="notEquals">
	<%	toggle = !toggle; cls = toggle ? "columndata_grey" : "columndata_white"; %>
                                                <tr valign="middle">
                                                    <ffi:setProperty name="CheckRecord2" property="CheckDate.Format" value="MM'/'dd'/'yyyy"/>
                                                    <td class="<%= cls %>" nowrap><ffi:getProperty name="CheckRecord2" property="CheckDate"/></td>
	                                                    <ffi:setProperty name="GetAccountData" property="BankID" value="${CheckRecord2.BankID}"/>
                                                        <ffi:setProperty name="GetAccountData" property="AccountID" value="${CheckRecord2.AccountID}"/>
                                                        <ffi:process name="GetAccountData"/>
                                                        <td class="<%= cls %>" nowrap><ffi:getProperty name="GetAccountData" property="RoutingNumber"/> : 
                                                        
                                                        <ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="session"/>
							<ffi:setProperty name="AccountDisplayTextTask" property="AccountID" value="${CheckRecord2.AccountID}"/>
							<ffi:process name="AccountDisplayTextTask"/>
							<ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
							<ffi:removeProperty name="AccountDisplayTextTask"/>
															<ffi:cinclude value1="${GetAccountData.NickName}" value2="" operator="notEquals" >
																 - <ffi:getProperty name="GetAccountData" property="NickName"/>
													 		</ffi:cinclude>	
															 - <ffi:getProperty name="GetAccountData" property="CurrencyType"/>
                                                        </td></td>
                                                        <td class="<%= cls %>" nowrap><ffi:getProperty name="CheckRecord2" property="CheckNumber"/></td>
                                                        <td class="<%= cls %>" style="text-align:right" nowrap><ffi:getProperty name="CheckRecord2" property="Amount.CurrencyStringNoSymbol"/></td>
                                                        <td class="<%= cls %>" nowrap>&nbsp;&nbsp;</td>
                                                        <td class="<%= cls %>" nowrap>
                                                            <ffi:cinclude value1="${CheckRecord2.AdditionalData}" value2="" operator="equals"> - </ffi:cinclude>
                                                            <ffi:cinclude value1="${CheckRecord2.AdditionalData}" value2="" operator="notEquals">
                                                                <ffi:getProperty name="CheckRecord2" property="AdditionalData"/>
                                                            </ffi:cinclude>
                                                                </td>
                                                        <td nowrap class="<%= cls %>">
                                                            <div align="center">
                                                                <input type="checkbox" name="checkbox" value="checkbox"
                                                                    <ffi:cinclude value1="${CheckRecord2.VoidCheck}" value2="true" operator="equals">
                                                                        checked
                                                                    </ffi:cinclude>
                                                                disabled>
                                                            </div>
                                                        </td>
                                                </tr>
                                            </ffi:cinclude>
                                        </ffi:list>

                                        </tr>

										<tr valign="middle">
											<td nowrap class="sectionsubhead">&nbsp;</td>
											<td nowrap class="sectionsubhead">&nbsp;</td>
											<td nowrap class="sectionsubhead"><p><s:text name="jsp.default_432"/></p></td>
											<td align="right" nowrap class="sectionsubhead"><ffi:getProperty name="GetCheckRecordsAmountTotal" property="TotalAmount.CurrencyString" /></td>
											<td nowrap class="sectionsubhead">&nbsp;</td>
											<td nowrap class="sectionsubhead">&nbsp;</td>
										</tr>


                                        <tr>
                                            <td colspan="7">
												<ffi:setProperty name="tmp_url" value="cashppay.jsp" URLEncrypt="true"/>
                                                <s:form name="form1" method="post" action="<ffi:getProperty name='tmp_url'/>" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                                    <div align="center">
                                                        <%-- <input class="submitbutton" onclick="CSAction(new Array(/*CMP*/'B9E8049C7'));" type="button" name="Button2" value="EDIT" csclick="B9E8049C7">&nbsp;  input class="submitbutton" type="submit" name="Button" value="UPLOAD" --%>
                                                        
                                                        
                                            <sj:a 
												id="editBuildFormID"
												button="true"
												onclick="ns.cash.gotoStep(1);"
												><s:text name="jsp.default_178"/></sj:a>
											
											
											
                                            <s:url id="buildUploadUrl" value="/pages/jsp/cash/cashppay.jsp" escapeAmp="false">
													<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
													<s:param name="positivepayReload" value="true"/>
													<s:param name="cashppaybuildupload" value="true"/>
											</s:url>
											<sj:a 
												id="uploadBuildFormID"
												href="%{buildUploadUrl}" 
												targets="notes" 
												indicator="indicator" 
												button="true"
												><s:text name="jsp.default_452"/></sj:a>
                                                        
                                                        
                                                        </div>				
											
                                                </s:form>
												<ffi:removeProperty name="tmp_url"/>
												
												
											
											
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
            </table>
            <br>
        </div>
