<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="${MBSSOURL}" value2="" operator="notEquals">
	<s:url var="continueUrl" value="invalidate-local-session.jsp" escapeAmp="false">
		<s:param name="request_locale" value="#parameters.request_locale"/>
	</s:url>

	<html>
	<head>
		<script type="text/javascript" src="" id="loadScript"></script>
		<script type="text/javascript">
			function invalidateMBankingSession()
			{
				var obj = document.getElementById("loadScript");
				if (obj)
					obj.src = "<ffi:getProperty name='MBSSOURL'/>&invalidate=true";
				window.location.href = "<s:property value="%{continueUrl}" escape="false"/>";
			}
		</script>
	</head>
	<body onload="invalidateMBankingSession()">
	</body>
	</html>
</ffi:cinclude>
<ffi:cinclude value1="${MBSSOURL}" value2="" operator="equals">
	<s:url var="continueUrl" value="invalidate-local-session.jsp" escapeAmp="false">
		<s:param name="TimedOut" value="#parameters.TimedOut"/>
		<s:param name="request_locale" value="#parameters.request_locale"/>
	</s:url>
	<%-- Used Java script let to redirect to "Invalidate-local-session.jsp" as this will be called through AJAX 
		 for RCL functionality. In that case, having javascript won't work.--%>
	<s:set var="logOutUrl" value="%{continueUrl}" scope="page"/>
	<%  
		response.sendRedirect((String)pageContext.getAttribute("logOutUrl") );
	%>
	
</ffi:cinclude>
