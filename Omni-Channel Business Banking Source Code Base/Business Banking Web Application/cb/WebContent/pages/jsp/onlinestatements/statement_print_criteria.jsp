<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/pages/jsp/onlinestatements/inc/statement_common_criteria.jsp" /> 

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DESTINATION %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_OBJECT %>"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_FMT_HTML %>"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_SHOW_ORIENTATION %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" value="<%= com.ffusion.beans.reporting.ReportCriteria.VAL_FALSE %>"/>