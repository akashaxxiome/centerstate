<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<script type="text/javascript" src="<s:url value='/web/js/account/account_grid%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/onlinestatements/onlineStatements%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/onlinestatements/onlineStatements_grid%{#session.minVersion}.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/accounts/accounts%{#session.minVersion}.css'/>"></link>

<%--Clean up all values related to Online statements from session --%>
<s:include value="/pages/jsp/onlinestatements/statement_cleanup.jsp"/>



<div id="desktop" align="center">
	<div id="appdashboard" class="dashboardUiCls">
		<s:action namespace="/pages/jsp/onlinestatement" name="onlineStatementDashBoardAction" executeResult="true"/>
	</div>
	<div id="onlineStmtSummary"> 
		<div id="onlineStmtHolder" class="floatLeft" >
			<div class="portlet-header">
				 <div class="searchHeaderCls">
					<span class="searchPanelToggleAreaCls" onclick="$('.quickSearchAreaCls').slideToggle();">
						<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
					</span>
					<div class="summaryGridTitleHolder">
						<span id="selectedGridTab" class="selectedTabLbl marginRight10">
							<s:text name="jsp.onlineStatements.dashboard.caption"/>
						</span>
					</div>
				</div> 
			 </div>
			 <div class="portlet-content">
					<%-- This section should come only after Statement is accepted by user --%>
					<s:set var="statementAccepted"><s:property value="#session.statementAcceptedFlag"/></s:set>
					<s:if test="%{#statementAccepted=='true'}">
						<div id="searchContents">
							<s:action namespace="/pages/jsp/onlinestatement" name="onlineStatementSearchAction_init" executeResult="true"/>
						</div>
					</s:if>
					
					<s:elseif test="%{#statementAccepted !='true'}">
						<%-- Show message in Dash board to let user know about accepting Online Statement. --%>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center" class="acceptTermsCls" style="cursor: pointer;" onclick="ns.onlineStatements.showStatementAgreement()">
									<s:text name="jsp.onlineStatements.agreementAccept.message"></s:text>
								</td>
							</tr>
							<tr>
								<td align="left" class="txt_normal" colspan="2" >&nbsp;</td>
							</tr>
						</table>
						
						<%-- Java script to show Agreement dialog --%>
						<script type="text/javascript">
							$(document).ready(function() {
								ns.onlineStatements.showStatementAgreement();
							});
						</script>
					</s:elseif>
	     		
	     		<div id="statementDetails" class="ffiVisible" style="float:left !important; width:100%;"></div>
	     		
			</div> 
			<div id="onlineStmt-menu" class="portletHeaderMenuContainer" style="display:none;">
				<ul class="portletHeaderMenu">
					<li><a href='#' onclick="ns.common.showDetailsHelp('onlineStmtSummary')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
				</ul>
			</div> 
		</div>
	</div>		
</div>	


<sj:dialog id="viewOnlineStatementsTransactionDetailDialogID" cssClass="acctMgmtDialog" title="%{getText('jsp.account_214')}" modal="true" resizable="false" autoOpen="false" 
	closeOnEscape="true" showEffect="fold" hideEffect="clip" width="700" height="auto">
</sj:dialog>

<sj:dialog id="viewStatementAgreementDialogID" cssClass="acctMgmtDialog" title="%{getText('jsp.onlineStatements.agreementDialogTitle')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="630" height="630">
</sj:dialog>
<sj:dialog id="viewPrintReadyVersionDialogID" cssClass="acctMgmtDialog" title="%{getText('jsp.onlineStatements.printerReadyDialogTitle')}" modal="true" 
	resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="710" height="700" 
	buttons="{
			 '%{getText(\"jsp.default_82\")}':function() {
    			ns.common.closeDialog('viewPrintReadyVersionDialogID');
    		 },
			 '%{getText(\"jsp.default_331\")}':function() {
				$('#viewPrintReadyVersionDialogID').print();
    		 }
    		}">
</sj:dialog>
<sj:dialog id="viewExportDialogID" cssClass="acctMgmtDialog" title="%{getText('jsp.default_198')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" height="180" width="450" overlayColor="#000" overlayOpacity="0.7">
</sj:dialog>
<sj:dialog id="viewBankNoticeDialogID" cssClass="acctMgmtDialog" title="%{getText('jsp.onlineStatements.bankNoticeDialogTitle')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="630" height="500" cssStyle="overflow:hidden">
</sj:dialog>
	
<script type="text/javascript">
$(document).ready(function() {
	// Show dashboard section
	ns.common.initializePortlet('onlineStmtHolder');
	
	 $('#viewExportDialogID').addHelp(function(){
			var helpFile = $('#viewExportDialogID').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
});
</script>