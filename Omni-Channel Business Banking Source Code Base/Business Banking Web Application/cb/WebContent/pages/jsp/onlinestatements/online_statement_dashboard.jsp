<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<s:set name="statementAcceptedFlag" value="%{statementAccepted}" scope="session" /> 

 <div class="moduleSubmenuItemCls">
	<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-onlineStatement"></span>
	<span class="moduleSubmenuLbl">
		<s:text name="jsp.home.onlineStatements.menu"/>
	</span>
	<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
	
	<!-- dropdown menu include -->    		
	<s:include value="/pages/jsp/home/inc/accountSubmenuDropdown.jsp" />
</div>
