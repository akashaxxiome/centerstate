<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div align="center" class="approvalDialogHt">
   <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="98%" valign="top" align="left">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="header" colspan="7">&nbsp;</td>
						<td class="header" width="1%">
							<div align="right"><img src="/cb/web/multilang/grafx/header_corner.gif" width="21" height="19" align="absmiddle"></div>
						</td>
					</tr>
				</table>
				<table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <br>
                    <tr>
                        <td class="" valign="top" align="center">
                           <s:text name="jsp.onlineStatements.statementNotice.header"/>
                        </td>
					</tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt_normal">
                            <s:text name="jsp.onlineStatements.statementNotice.point1"/>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                           	<table border="0" cellspacing="0" cellpadding="0" width="95%">
                                <tr>
                                    <td class="txt_normal">
                                        <s:text name="jsp.onlineStatements.statementNotice.point1.subPt1"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="txt_normal">
                                        <s:text name="jsp.onlineStatements.statementNotice.point1.subPt2"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="txt_normal">
                                        <s:text name="jsp.onlineStatements.statementNotice.point1.subPt3"/>
                                    </td>
                                </tr>
                                <tr>    
                                    <td class="txt_normal">
                                        <s:text name="jsp.onlineStatements.statementNotice.point1.subPt4"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt_normal">
                            <s:text name="jsp.onlineStatements.statementNotice.point2"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>                    
                    <tr>
                        <td class="txt_normal">
                            <s:text name="jsp.onlineStatements.statementNotice.point3"/>
                        </td>                    
					</tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>                              
                    <tr>
                        <td class="" valign="top">
                           <s:text name="jsp.onlineStatements.statementNotice.billRights.header"/>
                        </td>
					</tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>
					<tr>
                        <td class="txt_normal">
                            <s:text name="jsp.onlineStatements.statementNotice.billRights.question"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt_normal">
                            <s:text name="jsp.onlineStatements.statementNotice.billRights.wrongBillHeader"/>
                        </td>                        
                    </tr>
                    <tr>
                        <td align="left">
                           	<table border="0" cellspacing="0" cellpadding="0" width="95%">
                                <tr>
                                    <td width="20">&nbsp;</td>
                                    <td class="txt_normal">
                                        <li><s:text name="jsp.onlineStatements.statementNotice.billRights.wrongBillPt1"/></li>
                                    </td>
                                </tr>                    
                                <tr>
                                	<td>&nbsp;</td>
                                    <td class="txt_normal">
                                        <li><s:text name="jsp.onlineStatements.statementNotice.billRights.wrongBillPt2"/></li>
                                    </td>
                                </tr>                    
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="txt_normal">
                                        <li><s:text name="jsp.onlineStatements.statementNotice.billRights.wrongBillPt3"/></li>
                                    </td>
                                </tr>                    
                                <tr>
                                    <td>&nbsp;</td>
                                    <td class="txt_normal">
                                        <li><s:text name="jsp.onlineStatements.statementNotice.billRights.wrongBillPt4"/></li>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt_normal">
                            <s:text name="jsp.onlineStatements.statementNotice.billRights.investigation"/>
                        </td>                    
					</tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td class="txt_normal">                       
                            <s:text name="jsp.onlineStatements.statementNotice.billRights.contactDetails"/> <ffi:link url="mailto:general_info@saponlinebank.com">general_info@saponlinebank.com</ffi:link>.
                        </td>                    
					</tr>  
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="ui-widget-header customDialogFooter">
                           <!-- <div align="center" class="ui-widget-header customDialogFooter"> -->
                            <sj:a id="declineAgreementBtn" onclick="ns.common.closeDialog();"
								button="true" ><s:text name="jsp.default_175"/></sj:a>
							<!-- </div>	 -->
                        </td>
                    </tr>                    
				</table>
			</td>
		</tr>
		<tr>
			<td height="10"><img src="/cb/web/multilang/grafx/spacer.gif" width="1" height="10" border="0" alt=""></td>
		</tr>
	</table>
</div>