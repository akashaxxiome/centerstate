<%@page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<%@ page import="com.ffusion.beans.reporting.ExportFormats"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<ffi:help id="account_onlinestatement_export" />

<script type="text/javascript" src="<s:url value='/web/js/reports/reports%{#session.minVersion}.js'/>"/>
<script type="text/javascript">
        function formExport() {
				var selectValues = document.getElementById('exportTypeDropDown');
				var selectIndex = selectValues.selectedIndex;
				var selectVal=selectValues[selectIndex].value;

				if (selectVal == "PDFImages") {
                 document.exportReportForm["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.istatements.IStatementsRptConsts.OPT_CRITERIA_WITH_IMAGES %>"].value = "true";
				 selectValues[selectIndex].value="<%= ExportFormats.FORMAT_PDF %>";
                 }
				else if (selectVal == "PDFNoImages") {
                 document.exportReportForm["<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%= com.ffusion.beans.istatements.IStatementsRptConsts.OPT_CRITERIA_WITH_IMAGES %>"].value = "false";
				 selectValues[selectIndex].value="<%= ExportFormats.FORMAT_PDF %>";
                 }

				 ns.report.exportReport('/cb/pages/jsp/reports/UpdateReportCriteriaAction_generateReport.action', $('#exportReportForm')[0]);
				 ns.common.closeDialog();
	    }



</script>
<s:form id="exportReportForm" theme="simple">
	<%-- <input type="hidden" name="GenerateReportBase.Destination"
		value="<%=com.ffusion.beans.reporting.ReportCriteria.VAL_DEST_USRFS%>"> --%>
	<input type="hidden" name="CSRF_TOKEN"
		value="<ffi:getProperty name='CSRF_TOKEN'/>" />
	<input type="hidden" name="<%= com.ffusion.tasks.reporting.UpdateReportCriteria.REPORT_OPTION_PREFIX %><%=com.ffusion.beans.istatements.IStatementsRptConsts.OPT_CRITERIA_WITH_IMAGES %>" value="false">
	<input type="hidden" name="returnjsp" value="false" />
	<s:hidden key="reportID" />
	<s:set var="GenerateReportBase" value="%{#session.FFIGenerateReportBase}"/>
	
		<p id="exportDialogMessage" align="center">
			<s:text name="jsp.reports_696" />
		</p> 
	<table ellpadding="0" cellspacing="0" width="100%" align="center">
		<tr>
			<td width="100%" align="center"><select class="txtbox"
				name="GenerateReportBase.Format" id="exportTypeDropDown">

					<ffi:setProperty name="ReportData"
						property="ReportCriteria.CurrentReportOption"
						value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED %>" />
					<ffi:cinclude
						value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
						value2="" operator="equals">
						<option value="<%=com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>"><s:text name="jsp.default_105" /></option>
					</ffi:cinclude>
					<ffi:setProperty name="ReportData"
						property="ReportCriteria.CurrentReportOption"
						value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED %>" />
					<ffi:cinclude
						value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
						value2="" operator="equals">
						<option value="<%=com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>"><s:text name="jsp.default_402" /></option>
					</ffi:cinclude>

						<option value="PDFImages"><s:text name="jsp.default_586" /></option>
						<option value="PDFNoImages"><s:text name="jsp.default_587" /></option>

					<ffi:setProperty name="ReportData"
						property="ReportCriteria.CurrentReportOption"
						value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT %>" />
					<ffi:cinclude
						value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
						value2="" operator="equals">
						<option value="<%=com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>"><s:text name="jsp.default_325" /></option>
					</ffi:cinclude>
					<ffi:setProperty name="ReportData"
						property="ReportCriteria.CurrentReportOption"
						value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_FORMAT + com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV %>" />
					<ffi:cinclude
						value1="${ReportData.ReportCriteria.CurrentReportOptionValue}"
						value2="true" operator="equals">
						<option value="<%=com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV%>"><s:text name="jsp.default_124" /></option>
					</ffi:cinclude>
			</select></td>
		</tr>
		<tr>
			<td height="20"></td>
		</tr>
		<tr>
			<td width="100%" align="center">
				<div align="center" class="ui-widget-header customDialogFooter">
					<sj:a id="exportDialogCancelBtn"
						button="true" onclick="{ns.common.closeDialog();}" theme="simple">
						<s:text name="jsp.default_82" />
					</sj:a> <sj:a id="exportDialogSubmitBtn"
						onclick="formExport()"
						effect="pulsate" value="Export" indicator="indicator" button="true"
						theme="simple">
						<s:text name="jsp.default_195" />
					</sj:a>
				</div>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
		$('#exportTypeDropDown').selectmenu({width: 150});
	</script>
</s:form>