<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<% String opt_pagewidth; %>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PRINTER_READY_PAGEWIDTH %>"/>
<ffi:getProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" assignTo="opt_pagewidth"/>
<%
	String real_pagewidth = String.valueOf( Integer.parseInt( opt_pagewidth ) + 2 );

	session.setAttribute( "real_pagewidth", real_pagewidth );
%>
<table width='<ffi:getProperty name="real_pagewidth"/>' border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="tbrd_ltr dkback" colspan="3">
			<ffi:object id="exportHeader" name="com.ffusion.tasks.reporting.ExportHeaderOptions" scope="request"/>
			<ffi:setProperty name="exportHeader" property="ReportName" value="ReportData"/>
			<ffi:setProperty name="exportHeader" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
			<ffi:process name="exportHeader"/>
			<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_HEADER_NAME %>" encode="false"/>
		</td>
	</tr>
	<tr>
		<td class="tbrd_ltr" colspan="3"><br>
			<ffi:object id="exportForDisplay" name="com.ffusion.tasks.reporting.ExportReport" scope="request"/>
			<ffi:setProperty name="exportForDisplay" property="ReportName" value="ReportData"/>
			<ffi:setProperty name="exportForDisplay" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
			<ffi:process name="exportForDisplay"/>
			<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_NAME %>" encode="false"/>
		</td>
	</tr>
	<tr>
		<td class="tbrd_full dkback" colspan="3">
			<ffi:object id="exportFooter" name="com.ffusion.tasks.reporting.ExportFooterOptions" scope="request"/>
			<ffi:setProperty name="exportFooter" property="ReportName" value="ReportData"/>
			<ffi:setProperty name="exportFooter" property="ExportFormat" value="<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML %>"/>
			<ffi:process name="exportFooter"/>
			<ffi:getProperty name="<%= com.ffusion.tasks.reporting.ReportingConsts.DEFAULT_EXPORT_FOOTER_NAME %>" encode="false"/>
		</td>
	</tr>
</table>
<%-- Nullify report result to save memory --%>
<%
com.ffusion.beans.reporting.Report report = (com.ffusion.beans.reporting.Report)session.getAttribute( "ReportData" );
report.setReportResult(null);
%>

<ffi:removeProperty name="FFIGenerateReportBase"/>
<ffi:removeProperty name="ReportData"/>

