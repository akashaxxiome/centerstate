<%--
This is the delete regular wire beneficiary page.

Pages that request this page
----------------------------
wirepayees.jsp

Pages this page requests
------------------------
CANCEL requests wirepayees.jsp
DELETE BENEFICIARY requests wirepayeedeletesend.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ include file="../common/wire_labels.jsp"%>

<ffi:help id="payments_wirepayeedelete" className="moduleHelpClass"/>

<ffi:setProperty name="DimButton" value="beneficiary"/>
<%--<ffi:include page="${PathExt}payments/inc/wire_buttons.jsp"/>--%>



<%-- Start: Dual approval processing --%>

<%-- End: Dual approval processing --%>
<div align="center" class="approvalDialogHt2">
<s:form action="deleteWireBeneficiary_delete" method="post" name="FormName" id="deleteWireBeneficiaryFormId" namespace="/pages/jsp/wires">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="payeeId" value="<ffi:getProperty name="WireTransferPayee" property="ID"/>"/>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Beneficiary Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_NUMBER %>: </span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountNum"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_TYPE %>: </span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountTypeDisplayName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  width="115" class="sectionsubhead sectionLabel"><!--L10NStart-->Beneficiary Name<!--L10NEnd-->: </span>
				<span  width="228" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Name"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Nickname<!--L10NEnd-->: </span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="NickName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Address 1: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Address 2: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street2"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->City: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="City"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->State/Province: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="State"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->ZIP/Postal Code: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="ZipCode"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Country: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="CountryDisplayName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Contact Person: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Contact"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Beneficiary Type: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeDestination"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Beneficiary Scope: <!--L10NEnd--></span>
			<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeScope"/></span>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Beneficiary Bank Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Bank Name: <!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Address 1: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Address 2: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->City: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->State/Province: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.State"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->ZIP/Postal Code: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->Country: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CountryDisplayName"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->FED ABA: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->SWIFT: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingSwift"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->CHIPS: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingChips"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel"><!--L10NStart-->National: <!--L10NEnd--></span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingOther"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel"><%= LABEL_IBAN %>: </span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></span>
			</div>
		</div>
		<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
			<div class="blockRow">
				<span  class="sectionsubhead sectionLabel"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></span>
			</div>
		</ffi:cinclude>
	</div>
</div>
<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks}" value2="" operator="notEquals" >
<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
<div class="blockWrapper">
<% int counter = 0; %>
	<ffi:list collection="WireTransferPayee.IntermediaryBanks" items="Bank1">
		<div  class="blockHead"><!--L10NStart-->Intermediary Bank Info<!--L10NEnd--></div>
		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span  class="sectionsubhead sectionLabel">Intermediary Bank Name: </span>
					<span  class="columndata"><ffi:getProperty name="Bank1" property="BankName"/></span>
				</div>
				<div class="inlineBlock">
					<span  class="sectionsubhead sectionLabel">FED ABA: </span>
					<span  class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span  class="sectionsubhead sectionLabel">SWIFT: </span>
					<span  class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/></span>
				</div>
				<div class="inlineBlock">
					<span  class="sectionsubhead sectionLabel">CHIPS: </span>
					<span  class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span  class="sectionsubhead sectionLabel">National: </span>
					<span  class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/></span>
				</div>
				<div class="inlineBlock">
					<span  class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
					<span  class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/></span>
				</div>
			</div>
			<div class="blockRow">
					<span  class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
					<span  class="columndata">
						<% if(counter == 0) { %>
							<% } else { %>
							<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
							<% } counter++; %>
					</span>
			</div>
		</div>
	</ffi:list>
</div>
</ffi:cinclude>
</ffi:cinclude>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Comments<!--L10NEnd--></div>
<div class="blockContent">
		<div class="blockRow">
			<ffi:getProperty name="WireTransferPayee" property="Memo" />
		</div>
	</div>
</div>
<div align="center" class="marginTop10">
	Are you sure you want to delete this beneficiary?
</div>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
	<div align="center" class="ui-widget-header customDialogFooter">
	<sj:a 
		id="cancelWireBeneficiaryLink" 
		button="true" 
		onClickTopics="closeDialog" 
		title="Cancel"
		><s:text name="jsp.default_82"/>
	</sj:a>
	
	<%-- Start: Dual approval processing --%>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<sj:a 
		id="deleteWireBeneficiaryLink" 
		formIds="deleteWireBeneficiaryFormId" 
		targets="resultmessage" 
		button="true"   
		title="Delete Beneficiary" 
		onSuccessTopics="daDeleteWireBeneficiaryCompleteTopics" 
		onCompleteTopics="refreshWireBeneficiariesGridSuccessTopics" 
		effectDuration="1500" 
		><s:text name="jsp.default_DELETE_BENEFICIARY" /><!-- DELETE BENEFICIARY -->
	</sj:a>									
	</ffi:cinclude>
	<%-- End: Dual approval processing --%>
	
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<sj:a 
		id="deleteWireBeneficiaryLink" 
		formIds="deleteWireBeneficiaryFormId" 
		targets="resultmessage" 
		button="true"   
		title="Delete Beneficiary" 
		onSuccessTopics="daDeleteWireBeneficiaryCompleteTopics" 
		onCompleteTopics="refreshWireBeneficiariesGridSuccessTopics" 
		effectDuration="1500" 
		><s:text name="jsp.default_DELETE_BENEFICIARY" /><!-- DELETE BENEFICIARY -->
	</sj:a>									
	</ffi:cinclude>
</div>
			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td class="columndata ltrow2_color">
					<!-- <table width="720" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td align="left" class="tbrd_b sectionhead">&gt; L10NStartDelete BeneficiaryL10NEnd</td>
						</tr>
						<tr>
							<td align="left" class="sectionsubhead">&nbsp;</td>
						</tr>
						<tr>
							<td class="sectionsubhead" align="center">L10NStartAre you sure you want to delete this beneficiary?L10NEnd</td>
						</tr>
						<tr>
							<td align="left" class="tbrd_b sectionsubhead">&nbsp;</td>
						</tr>
					</table> -->
					<%-- <table width="715" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="357" valign="top">
								<table width="355" cellpadding="3" cellspacing="0" border="0">
									<tr>
										<td align="left" class="sectionhead" colspan="2">&gt; <!--L10NStart-->Beneficiary Info<!--L10NEnd--></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><%= LABEL_ACCOUNT_NUMBER %></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountNum"/></td>
									</tr>
									
									<tr>
										<td align="left" class="sectionsubhead"><%= LABEL_ACCOUNT_TYPE %></td>
										<td align="left" class="columndata">
											<ffi:getProperty name="WireTransferPayee" property="AccountTypeDisplayName"/>
										</td>
									</tr>
									<tr>
										<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Beneficiary Name<!--L10NEnd--></td>
										<td align="left" width="228" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Name"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Nickname<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="NickName"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Address 1<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Address 2<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street2"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->City<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="City"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->State/Province<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="State"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->ZIP/Postal Code<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="ZipCode"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Country<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="CountryDisplayName"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Contact Person<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Contact"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Beneficiary Type<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeDestination"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Beneficiary Scope<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeScope"/></td>
									</tr>
								</table>
							</td> 
							<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>--%>
							<%-- <td width="348" valign="top">
								<table width="347" cellpadding="3" cellspacing="0" border="0">
									<tr>
										<td align="left" class="sectionhead" colspan="2">&gt; <!--L10NStart-->Beneficiary Bank Info<!--L10NEnd--></td>
									</tr>
									<tr>
										<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Bank Name<!--L10NEnd--></td>
										<td align="left" width="220" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Address 1<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Address 2<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->City<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->State/Province<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.State"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->ZIP/Postal Code<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Country<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CountryDisplayName"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->FED ABA<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->SWIFT<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingSwift"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->CHIPS<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingChips"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->National<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingOther"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><%= LABEL_IBAN %></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></td>
									</tr>
									<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
									<tr>
										<td align="left" class="sectionsubhead"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
									</tr>
									</ffi:cinclude>
								</table>
							</td>
						</tr>
					</table> --%>

<%-- <ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks}" value2="" operator="notEquals" >
<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
					<table width="715" cellpadding="3" cellspacing="0" border="0">
						<tr>
						    <td align="left" class="tbrd_b" colspan="5"><span class="sectionhead">&gt; <!--L10NStart-->Intermediary Bank Info<!--L10NEnd--></span></td>
						</tr>
						<tr>
						    <td align="left" width="5%" class="sectionsubhead"><!--L10NStart-->Intermediary Bank Name<!--L10NEnd--></td>
						    <td align="left" width="35%" class="sectionsubhead"><!--L10NStart-->FED ABA<!--L10NEnd--></td>
						    <td align="left" width="12%" class="sectionsubhead"><!--L10NStart-->SWIFT<!--L10NEnd--></td>
						    <td align="left" width="12%" class="sectionsubhead"><!--L10NStart-->CHIPS<!--L10NEnd--></td>
						    <td align="left" width="12%" class="sectionsubhead"><!--L10NStart-->National<!--L10NEnd--></td>
							<td align="left" width="12%" class="sectionsubhead"><%= COLUMN_IBAN %></td>
                            <td align="left" width="12%" class="sectionsubhead"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
						</tr>
						<% int counter = 0; %>
						<ffi:list collection="WireTransferPayee.IntermediaryBanks" items="Bank1">
						<tr>
							<td align="left" width="5%" class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</td>
							<td align="left" width="35%" class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata">
							<% if(counter == 0) { %>
							&nbsp;
							<% } else { %>
							<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
							<% } counter++; %>&nbsp;</td>
						</tr>
						</ffi:list>
					</table>
</ffi:cinclude>
</ffi:cinclude> --%>
					<%-- <table width="715" cellpadding="3" cellspacing="0" border="0">
					    <tr>
							<td align="left" class="sectionsubhead">&nbsp;</td>
					    </tr>
						<tr>
						    <td align="left" class="tbrd_b"><span class="sectionhead">&gt; <!--L10NStart-->Comments<!--L10NEnd--></span></td>
						</tr>
						<tr>
						    <td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Memo" /></td>
						</tr>
					    <tr>
							<td align="left" class="sectionsubhead">&nbsp;</td>
					    </tr>

					</table> --%>
										

						
					</td>
				</tr>
			</table>
</s:form>
</div>
