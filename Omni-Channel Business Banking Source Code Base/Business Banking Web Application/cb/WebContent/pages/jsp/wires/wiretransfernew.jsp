<%--
This page is used for the following operations:
    Add and Edit domestic and international wire transfer
    Add and Edit domestic and international wire transfer to batch
    Add and Edit domestic and international wire transfer template

Pages that request this page
----------------------------
wiretransfers.jsp (wire transfer summary)
    NEW WIRE button (when domestic or international is selected in Wire Type)
wire_transfers_list.jsp (included in wiretransfers.jsp)
    Edit button next to a domestic or international wire transfer
wiretransferconfirm.jsp (wire transfer confirm)
    BACK button
wirebatchff.jsp (add freeform wire batch)
    ADD WIRE button
    Edit button next to a wire transfer
wirebatcheditff.jsp (edit freeform wire batch)
    ADD WIRE button
    Edit button next to a wire transfer
wirebatchtemp.jsp (add template batch)
    Edit button next to a wire transfer
wirebatchedittemplate.jsp (edit template batch)
    Edit button next to a wire transfer
wiretemplates.jsp (wire template summary)
    NEW TEMPLATE button
wire_templates_list.jsp (included in wiretemplates.jsp)
    Load button next to a domestic or international wire template
    Edit button next to a domestic or international wire template
wire_type_select.jsp (included in wire_common_init.jsp, which is in turn included
    in wiretransfernew.jsp, wirebook.jsp, wiredrawdown.jsp, and wirefed.jsp)
    Changing the Wire Type to triggers a javascript function called setDest() in
    wire_scripts_js.jsp
wireaddpayee.jsp
    CANCEL button

Pages this page requests
------------------------
LOAD TEMPLATE requests wiretransfernew.jsp (itself)
MANAGE BENEFICIARY requests wireaddpayee.jsp
SEARCH requests one of the following:
    wirebanklist.jsp
    wirebanklistselect.jsp
ADD INTERMEDIARY/RECEIVING BANK requests wirebanklist.jsp
CANCEL requests one of the following:
    wiretransfers.jsp
    wirebatchff.jsp
    wirebatchtemp.jsp
    wirebatcheditff.jsp
    wirebatchtemplateedit.jsp
    wiretemplates.jsp
SUBMIT WIRE/SUBMIT TEMPLATE requests wiretransferconfirm.jsp
Choose Wire Type requests one of the following:
    wiretransfernew.jsp (itself, when changing between Domestic and International)
    wirebook.jsp
    wiredrawdown.jsp
    wirehost.jsp
    wirefed.jsp
Select Beneficiary requests wiretransfernew.jsp (itself)
Calendar button requests calendar.jsp

Pages included in this page
---------------------------
payments/inc/wire_common_init.jsp
    Common task initialization for all wire types
inc/timeout.jsp
    The meta tag that will redirect to invalidate-session.jsp after a
    predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
    Javascript functions used by the top menu
payments/inc/wire_scripts_js.jsp
    Common javascript functions used for all wire types
payments/inc/nav_header.jsp
    The top menu shared by all of Payments & Transfers
payments/inc/wire_template_picker.jsp
    Template drop-down list
payments/inc/wire_template_fields.jsp
    Template fields, used when creating wire templates
payments/inc/wire_addedit_template_info.jsp
    Read-only template info, used when loading a template
payments/inc/wire_addedit_payee_picker.jsp
    Select Beneficiary drop-down list and MANAGE BENEFICIARY button
payments/inc/wire_addedit_payee_fields.jsp
    Beneficiary name & address fields
payments/inc/wire_addedit_bank_fields.jsp
    Beneficiary destination bank name & address fields
payments/inc/wire_addedit_intermediary_banks.jsp
    Beneficiary intermediary bank list and ADD BENEFICIARY BANK button
payments/inc/wire_addedit_payment_fields.jsp
    Wire amount, date, and recurring fields
payments/inc/wire_addedit_comment_fields.jsp
    Wire transfer comment and By Order Of feilds
common/checkAmount_js.jsp
    A common javascript that validates an amount is formatted properly
--%>
<%@page import="com.ffusion.beans.wiretransfers.WireTransfer"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.tasks.wiretransfers.ModifyWireTransfer,
                 com.ffusion.beans.accounts.Accounts" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ include file="../common/wire_labels.jsp"%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>


<%-- ----------------------------------------------------------------------------------------------- --
NOTE:  This page is for ADD and MODIFY wire transfers, ADD and MODIFY wire transfer templates, ADD and
    MODIFY wire transfers in a new batch, and ADD and MODIFY wire transfers in an existing batch.
The object it's utilizing is in the session called "wireTask":
    "AddWireTransfer" for add single wire.
    "ModifyWireTransfer" for edit single wire.
    "WireTransfer" for add and edit wires part of a batch.
If a string is in the session called "templateTask", and it's one of the following values, it will
treat the wire transfer as a wire transfer template:
    "add" for add wire template.
    "edit" for edit wire template.
If a string is in the session called "wireBatch", and it's one of the following values, it will
treat the wire transfer as a part of a batch:
    "AddWireBatch" for wires included in a new batch.
    "ModifyWireBatch" for wires included in an existing batch.
	If a string in the session called "withinTemplate" and its value is "true", it will treat the wire transfer as a part of a templated batch.
---------------------------------------------------------------------------------------------------- --%>

<ffi:help id="payments_wiretransfernew" className="moduleHelpClass"/>
<ffi:removeProperty name="DontInitialize"/>
<%

if( request.getParameter("notTemplate") != null ) { session.removeAttribute("templateTask"); }
if( request.getParameter("wireTask") != null ){ session.setAttribute("wireTask", request.getParameter("wireTask")); }
if( request.getParameter("wireDestination") != null ){ session.setAttribute("wireDestination", request.getParameter("wireDestination")); }
if( request.getParameter("DA_WIRE_PRESENT") != null ){ session.setAttribute("DA_WIRE_PRESENT", request.getParameter("DA_WIRE_PRESENT")); }
if( request.getParameter("DontInitialize") != null ){ session.setAttribute("DontInitialize", request.getParameter("DontInitialize")); }
if( request.getParameter("setPayee") != null ){ session.setAttribute("setPayee", request.getParameter("setPayee")); }
if( request.getParameter("ID") != null ){ session.setAttribute("ID", request.getParameter("ID")); }
if( request.getParameter("recID") != null) { session.setAttribute("recID", request.getParameter("recID")); }
if( request.getParameter("templateTask") != null ){ session.setAttribute("templateTask", request.getParameter("templateTask")); }
//***Fix QTS:661548 & 662132.Start***
//CR Descriptions:Wire template use button does not work correct, works as clone.
//The session should be removed when it not used.
else if(request.getParameter("userTemplate") != null){ session.removeAttribute("templateTask");}
//***Fix QTS:661548 & 662132.End***
if( request.getParameter("collectionName") != null ){ session.setAttribute("collectionName", request.getParameter("collectionName")); }
if( request.getParameter("wireInBatch") != null ){ session.setAttribute("wireInBatch", request.getParameter("wireInBatch")); }
if( request.getParameter("LoadFromTransferTemplate") != null ){ session.setAttribute("LoadFromTransferTemplate", request.getParameter("LoadFromTransferTemplate")); }
if( request.getParameter("delIntBank") != null){ session.setAttribute("delIntBank", request.getParameter("delIntBank")); }
if( request.getParameter("pendApproval") != null){ session.setAttribute("pendApproval", request.getParameter("dpendApproval")); }
if( request.getParameter("wireBatch") != null ){ session.setAttribute("wireBatch", request.getParameter("wireBatch")); }
if( request.getParameter("wireIndex") != null ){ session.setAttribute("wireIndex", request.getParameter("wireIndex")); }
if( request.getParameter("DontInitializeBatch") != null ){ session.setAttribute("DontInitializeBatch", request.getParameter("DontInitializeBatch")); }
if( request.getParameter("editWireTransfer") != null ){ session.setAttribute("editWireTransfer", request.getParameter("editWireTransfer")); }
if( request.getParameter("loadFromTemplate") != null ){ session.setAttribute("loadFromTemplate", request.getParameter("loadFromTemplate")); }
if( request.getParameter("withinTemplate") != null){ session.setAttribute("withinTemplate", request.getParameter("withinTemplate")); }
if( request.getParameter("ValidateAction") != null){ session.setAttribute("ValidateAction", request.getParameter("ValidateAction")); }
%>

<%-- hints for bookmark --%>
<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
	<span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTransferLink,newSingleWiresTransferID</span>
	<span class="shortcutEntitlementClass" style="display:none;">Wires</span>
</ffi:cinclude>
<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
	<span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTemplateLink,newSingleWiresTemplateID</span>
	<span class="shortcutEntitlementClass" style="display:none;">Wires</span>
</ffi:cinclude>


<ffi:setProperty name="CallerForm" value="WireNew"/>
<ffi:setProperty name="CallerURL" value="wiretransfernew.jsp?DontInitialize=true" URLEncrypt="true"/>

<%-- Initialize the Wire Transfer, common strings, & other objects.  Plus other common code (like the set payee code) --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_common_init.jsp"/>

<ffi:removeProperty name="LoadFromTransferTemplate" />
<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="PAYEE_DESTINATION_DA" value="<%= WireDefines.PAYEE_TYPE_REGULAR %>"/>
</ffi:cinclude>
<%-- End: Dual approval processing --%>


		<s:include value="%{#session.PagesPath}/wires/inc/nav_menu_top_js.jsp" />
		<s:include value="%{#session.PagesPath}/wires/inc/wire_scripts_js.jsp" />

        <div align="center">
            <%-- include page header --%>
            <%-- <ffi:include page="${PathExt}payments/inc/nav_header.jsp" /> --%><ul id="formerrors"></ul>
            <div align="center">
<div class="leftPaneWrapper">
<%-- ------ LOAD TEMPLATE PICK LIST ------ --%>
<!-- below if condition added to hide load template page when coming from load/clone template or edit wire -->
<s:if test='%{(#session.wireTransfer.ID=="" && #session.wireTransfer.wireType!="TEMPLATE") || (#parameters.LoadFromTransferTemplate != null || #parameters.loadFromWireTemplateSummaryGrid != null)}'>
	<div  id="wireTemplateDiv">
		<s:include value="%{#session.PagesPath}/wires/inc/wire_template_picker.jsp" />
	</div>
</s:if>
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
<script>
	var wireDestination = "<ffi:getProperty name="${wireTask}" property="WireDestination"/>";
	var PAYEE_DESTINATION_DA = "<ffi:getProperty name="PAYEE_DESTINATION_DA" />";
	var isSimplified = $('#beneficiaryType').val();
	var value = "";	
	
	/* if (wireDestination == "DRAWDOWN") {
		value = {value:"Create New Debit Account", label:js_create_new_debit_account};		
	} else if(isSimplified){
		// do nothings
	}
	else {
	value = {value:"Create New Beneficiary", label:js_create_new_beneficiary};		
	} */
	
	$("#selectWirePayeeID").lookupbox({
		"source":"/cb/pages/jsp/wires/wireTransferPayeeLookupBoxAction.action?wireDestination="+wireDestination,
		"controlType":"server",
		"size":"35",
		"module":"wireLoadPayee"
	});
</script>
<div class="leftPaneInnerWrapper" role="form" aria-labelledby="wireBeneficiaryHeader">
<div class="header"><h2 id="wireBeneficiaryHeader">Add Wire Beneficiary*</h2></div>
<div class="leftPaneInnerBox">
<%-- ------ BENEFICIARY PICK LIST: WirePayeeID ------ --%>
	<% String curPayee = ""; String thisPayee = ""; String payeeName=""; String nickName="";String accountNum="";String wirePayeeID="";%>
	<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="curPayee"/>
	<ffi:getProperty name="WireTransferPayee" property="PayeeName" assignTo="payeeName" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="NickName" assignTo="nickName" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="AccountNum" assignTo="accountNum" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="ID" assignTo="wirePayeeID"/>

	<% if (curPayee == null) curPayee = ""; %>

	<%-- Set current payee Id in session --%>
	<ffi:setProperty name="currentPayeeId" value="<%=curPayee%>"/>
	<ffi:cinclude value1="" value2="${currentPayeeId}" operator="equals">
		<ffi:setProperty name="currentPayeeId" value="<%= WireDefines.CREATE_NEW_BENEFICIARY%>"/>			
	</ffi:cinclude>
	<div class="selectBoxHolder beneficiaryComboHolder">
	<s:if test="%{beneficiaryType=='managed' || beneficiaryType==null}">
		<select class="txtbox" id="selectWirePayeeID" name="" size="1" onChange="setPayee(this.options[this.selectedIndex].value)" <ffi:getProperty name="disableSemi"/>>
				<%
				if(curPayee!=null && !curPayee.isEmpty() && !curPayee.equals(WireDefines.WIRE_LOOKUPBOX_DEFAULT_VALUE) 
					&& !curPayee.equals(WireDefines.CREATE_NEW_BENEFICIARY) && !curPayee.equals(WireDefines.CREATE_NEW_DEBIT_ACCOUNT)){
					String label = payeeName;
					if(nickName!=null && !nickName.equalsIgnoreCase("")){
						label = label +" (" + nickName + " - "+ accountNum + ")";
					}else{
						label = label +" (" + accountNum + ")";
					}
					
				%>
				<option value="<%=curPayee%>" selected="selected"><%=label %></option>
				<%
				} else { 
				String label = WireDefines.CREATE_NEW_BENEFICIARY;
				%>
				<option value="<%=label%>" selected="selected"></option>
				<% } %>
	</select>
	</s:if>
	<%-- <s:else>
		<select class="txtbox" id="selectWirePayeeID" name="<ffi:getProperty name="wireTask"/>.WirePayeeID" size="1" onChange="setPayee(this.options[this.selectedIndex].value)" <ffi:getProperty name="disableSemi"/>>
				<%
				if(curPayee!=null && !curPayee.isEmpty() && !curPayee.equals(WireDefines.WIRE_LOOKUPBOX_DEFAULT_VALUE) 
					&& !curPayee.equals(WireDefines.CREATE_NEW_BENEFICIARY) && !curPayee.equals(WireDefines.CREATE_NEW_DEBIT_ACCOUNT)){
					String label = payeeName;
					if(nickName!=null && !nickName.equalsIgnoreCase("")){
						label = label +" (" + nickName + " - "+ accountNum + ")";
					}else{
						label = label +" (" + accountNum + ")";
					}
					
				%>
				<option value="<%=curPayee%>" selected="selected"><%=label %></option>
				<%
				} else { 
				String label = wireDest.equals(WireDefines.WIRE_DRAWDOWN) ? WireDefines.CREATE_NEW_DEBIT_ACCOUNT : WireDefines.CREATE_NEW_BENEFICIARY;
				%>
				<option value="<%=label%>" selected="selected"><%=label%></option>
				<% } %>
	</select>
	</s:else> --%>
	<% String wireType = null; 
		String isRecModel = null;
	%>
	<ffi:getProperty name="${wireTask}" property="WireType" assignTo="wireType"/>
	<ffi:getProperty name="isRecModel" assignTo="isRecModel"/>
	</div>
	
		<div align="center">
			<div style="margin: 0 0 5px;">OR</div>
			<sj:a name="payeeDetails" id="freeForm" onclick="loadBeneficiaryForm()" button="true" cssStyle="margin-left:10px">
				<s:text name="jsp.wire.freeForm"/>
			</sj:a>
			
		</div>
		
		</div>
</div>

<%if (WireDefines.WIRE_TYPE_RECURRING.equals(wireType) && !"true".equals(isRecModel)) { %>
 		<ffi:cinclude value1="${IsModify}" value2="true">
		<div class="leftPaneInnerWrapper" style="border:none !important;"><div class="marginTop20 floatleft fxActionBtnCls">	<sj:a id="loadRecModelFormButton"
								button="true"
								onclick="selectRecModel();" 
							><s:text name="jsp.default.edit_orig_trans"/></sj:a>
		</div>	</div>
		</ffi:cinclude>
		
			<% } %></div>
	</ffi:cinclude>


<input type="hidden" id="TEMP_CSRF_TOKEN" name="TEMP_CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>


<div class="rightPaneWrapper" id="wireFormHolderDiv">
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" class="tableData">
                <tr>
                    <td class="ltrow2_color" align="left">
						<%--
						<form action="wiretransferconfirm.jsp" method="post" name="WireNew">
						--%>
                        <s:form id="newWiresTransferFormID" namespace="/pages/jsp/wires" validate="false" action="%{#attr.ValidateAction}" method="post" name="WireNew" theme="simple">
                        <input id="isRecModel" name="isRecModel" value="<ffi:getProperty name="isRecModel"/>" type="hidden"/>
                        <input type="hidden" id="isManagePayeeModified" name="isManagePayeeModified" />
						<input id="recurringId" name="recurringId" value="<ffi:getProperty name="recurringId"/>" type="hidden"/>
						<input id="ID" name="ID" value="<ffi:getProperty name="ID"/>" type="hidden"/>
						<input id="transType" name="transType" value="<ffi:getProperty name="transType"/>" type="hidden"/>
                        <%
					    String wireSource = "";
					    %>
					    <ffi:getProperty name="${wireTask}" property="wireSource" assignTo="wireSource"/>
					    <ffi:cinclude value1="${wireSource}" value2="TEMPLATE" operator="equals">
					        <ffi:cinclude value1="${templateTask}" value2="" operator="equals">
					            <%-- ------ TEMPLATE (READ-ONLY) WireName, NickName, TemplateID, DisplayWireLimit, & WireScope ------ --%>
					            <%-- <ffi:include page="${PathExt}payments/inc/wire_addedit_template_info.jsp"/> --%>
								<div class="paneWrapper" role="form" aria-labelledby="templateDetailsHeader">
									<div  class="header"><h2 id="templateDetailsHeader">Template Details</h2></div>
		                        	<div class="paneInnerWrapper">
		                        		<div class="paneContentWrapper">
		                        		 	<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_template_info.jsp"/>
		                        		</div>
		                        	</div>
		                        </div>
		                        <div class="marginTop10"></div>
					        </ffi:cinclude>
					    </ffi:cinclude>
                        <div class="paneWrapper" role="form" aria-labelledby="beneficiaryInfoHeader">
                        	<div class="paneInnerWrapper">
                        		<div class="header"><h2 id="beneficiaryInfoHeader"><s:text name="jsp.default_70" /></h2></div>
                        		<div class="paneContentWrapper">
                        			<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_picker.jsp"/>
                        			<table cellpadding="0" cellspacing="0" border="0" width="100%"  id="wireAddeditPayeeFields" class="hidden">
                                    <tr>
                                        <td valign="top">
										    <%-- ------ PAYEE FIELDS (WireTransferPayee): PayeeName, Street, Street2, City, State, ZipCode, Country, ContactName, PayeeScope, AccountNum, AccountType ------ --%>
										    <%-- <ffi:setProperty name="WireTransferPayee" property="PayeeDestination" value="<%= WireDefines.PAYEE_TYPE_REGULAR %>"/> --%>
										    <s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_fields.jsp"/>
									    </td>
									</tr>
									<tr><td valign="top"><hr class="hidden thingrayline lineBreak" /></td></tr>
									<tr>
										<td valign="top">
										    <%-- ------ BENEFICIARY BANK FIELDS (WireTransferPayee.DestinationBank):  BankName, Street, Street2, City, State, ZipCode, Country, RoutingFedWire, RoutingSwift, RoutingChips, RoutingOther, IBAN, Correspondent Bank Account Number ------ --%>
										    <div id="wireAddeditBankFields" class="hidden">
										    <s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_bank_fields.jsp"/>
										    </div>
										</td>
                                  </tr>
                              </table>
                                <div id="intermediaryBanksDivID" class="hidden">
							  	  <s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_intermediary_banks.jsp"/>
							    </div>
                       			</div>
                       		</div>
                        </div>
                        <div class="clear" style="clear:both"></div>
                        
                        <input type="hidden" name="<ffi:getProperty name="wireTask"/>.WirePayeeID" id="payeeID">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="DontInitialize" value="true"/>
						<input type="hidden" name="BatchEntry" value="<ffi:getProperty name='isBatchEntry'/>"/>
						<input type="hidden" id="returnPage" value="<ffi:getProperty name='returnPage'/>"/>
						<input type="hidden" name ="bankLookupRaturnPage" id="bankLookupRaturnPage" value="<ffi:getProperty name='returnPage'/>"/>
						<input type="hidden" id="setPayeeAction" value="<ffi:getProperty name='setPayeeAction'/>"/>
						<input type="hidden" id="deleteIntermediaryBankAction" value="<ffi:getProperty name='deleteIntermediaryBankAction'/>"/>
						<input type="hidden" name="PerformWireTaskName" value="<ffi:getProperty name="wireTask"/>"/>
             			<%--<input type="hidden" name="delIntBank" value=""/>--%>
             			<input type="hidden" name="selectedCountry" value=""/>
                         <input type="hidden" name="<ffi:getProperty name="wireTask"/>.Type" value="<ffi:getProperty name="${wireTask}" property="Type"/>">
                         <input type="hidden" name="<ffi:getProperty name="wireTask"/>.WireType" value="<ffi:getProperty name="${wireTask}" property="WireType"/>">
                         <input type="hidden" name="<ffi:getProperty name="wireTask"/>.WireDestination" value="<ffi:getProperty name="${wireTask}" property="WireDestination"/>">
						<input type="hidden" name="disableRec" value="<ffi:getProperty name="disableRec" />"/>
						<input type="hidden" name="disableBat" value="<ffi:getProperty name="disableBat" />"/>
						<input type="hidden" name="disableDates" value="<ffi:getProperty name="disableDates" />"/>
						<input type="hidden" name="isWireBatch" value="<ffi:getProperty name="isWireBatch" />"/>
						<input type="hidden" name="wireBatchUrl" value="<ffi:getProperty name="wireBatchUrl" />"/>
						<input type="hidden" name="WireTransferPayee.PayeeDestination" value="<%= WireDefines.PAYEE_TYPE_REGULAR %>">
						

    <%-- Display task error if one exists --%>
    <ffi:cinclude value1="${wireTask}" value2="AddWireTransfer">
        <% request.setAttribute("ErrorTask", session.getAttribute("AddWireTransfer")); %>
        <%-- <ffi:include page="${PathExt}inc/page-error.jsp"/> --%>
    </ffi:cinclude>
    <ffi:cinclude value1="${wireTask}" value2="ModifyWireTransfer">
        <% request.setAttribute("ErrorTask", session.getAttribute("ModifyWireTransfer")); %>
        <%-- <ffi:include page="${PathExt}inc/page-error.jsp"/> --%>
    </ffi:cinclude>
    <ffi:cinclude value1="${wireTask}" value2="WireTransfer">
        <% request.setAttribute("ErrorTask", session.getAttribute("WireTransferTask")); %>
        <%-- <ffi:include page="${PathExt}inc/page-error.jsp"/> --%>
    </ffi:cinclude>
    <ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
        <%-- ------ TEMPLATE FIELDS: WireName, NickName, WireCategory, & WireScope ------ --%>
        <%--<ffi:include page="${PathExt}payments/inc/wire_template_fields.jsp"/>--%>
		<s:include value="%{#session.PagesPath}/wires/inc/wire_template_fields.jsp"/>
    </ffi:cinclude>
    
    <input type="hidden" id="wireDestination" value="<s:property value="%{wireDestination}"/>" name="wireDestination"/>
    
                                
                             <%--    <span class="columndata">&nbsp;<br></span> --%>

	<ffi:cinclude value1="${wireDestination}" value2="" operator="notEquals">
		<ffi:setProperty name="wireDest" value="${wireDestination}"/>
		<ffi:setProperty name="wireDestinationType" value="${wireDestination}"/>
	</ffi:cinclude>
    <%-- ------ INTERMEDIARY BANKS LIST ------ --%>
    <div id="intermediaryBanksDivID" class="hidden">
    	<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_intermediary_banks.jsp"/>
    </div>
    <ffi:cinclude value1="${wireDestinationType}" value2="<%= WireDefines.WIRE_DOMESTIC %>" operator="equals">
    	
    	<div class="paneWrapper marginTop10" role="form" aria-labelledby="paymentDetailHeader">
          	<div class="paneInnerWrapper">
            	<div class="header"><h2 id="paymentDetailHeader">Payment Details</h2></div>
            	<div class="paneContentWrapper">
	    			<%-- ------ DOMESTIC PAYMENT & REPEATING FIELDS:  FromAccountID, Amount/OrigAmount, DueDate, Frequency, NumberTransfers ------ --%>
	    			<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payment_fields.jsp"/>
	    		</div>
	    	</div>
	    </div>
    </ffi:cinclude>
    <ffi:cinclude value1="${wireDestinationType}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
	<%-- ------ INTERNATIONAL Start ------ --%>
	<script>
		$(function(){
			//$("#selectDebitFromAccountIDID").combobox({'size':'32'});
			$("#selectDebitFromAccountIDID").lookupbox({
				"controlType":"server",
				"collectionKey":"1",
				"module":"Wires",
				"size":"37",
				"source":"/cb/pages/jsp/wires/WireAccountsLookupBoxAction.action"	
			});
			$("#selectPaymentFrequencyID").selectmenu({'width':'110px'});
			$("#selectPaymentFrequencyID1").selectmenu({'width':'110px'});
			$("#selectWireCurrencyFieldID").selectmenu({'width':'14.99em'});
			$("#selectMathRuleID").selectmenu({'width':'8.2em'});
			$("#selectPaymentPayeeCurrencyTypeID").selectmenu({'width':'14.99em'});
		});
	</script>
	
		<div class="paneWrapper" role="form" aria-labelledby="debitPaymentInfoHeader">
          	<div class="paneInnerWrapper">
                        <table cellpadding="0" cellspacing="0" border="0" class="tableData wireForm" width="100%">
                            <tr>
                                <td colspan="5" class="header"><h2 id="debitPaymentInfoHeader">Debit and Payment Information</h2><!--L10NEnd--></td>
                            </tr>
                            <tr>
                            	<td colspan="5">&nbsp;</td>
                            </tr>
                            <%-- ------ INTERNATIONAL PAYMENT AND REPEATING INFO ------ --%>
                            <tr>
                            	<td width="50%" colspan="2"><label for="selectDebitFromAccountIDID_autoComplete"><%= LABEL_FROM_ACCOUNT %></label></span><span class="required" title="required">*</span></td>
                            	<td width="25%" ><label for="AMOUNT"><%= LABEL_AMOUNT %></label></span><span class="required" title="required">*</span></td>
                            	<td width="25%"  nowrap><label for="selectWireCurrencyFieldID"><%= LABEL_DEBIT_CURRENCY %></label><span class="required" title="required">*</span></td>
                            </tr>
                            <ffi:setProperty name="intFieldTask" value="${wireTask}"/>
	    <ffi:cinclude value1="${batchTask}" value2="" operator="notEquals">
	        <ffi:setProperty name="intFieldTask" value="${batchTask}"/>
	    </ffi:cinclude>
                            <%-- ------ INTERNATIONAL CURRENCY INFO ------ --%>
                            <tr>
                            	<td colspan="2">
                            		<select id="selectDebitFromAccountIDID" class="txtbox" name="<ffi:getProperty name="wireTask"/>.FromAccountID" <ffi:getProperty name="disableSemi"/>>
                                        <% String curAcct = ""; String thisAcct = ""; %>
                                        <ffi:getProperty name="${wireTask}" property="FromAccountID" assignTo="curAcct"/>
				<% if (curAcct == null) curAcct = ""; %>
				<ffi:setProperty name="selectedAcct"  value="<%=curAcct%>"/>
				
				<ffi:cinclude value1="${selectedAcct}" value2="" operator="notEquals">
				<ffi:cinclude value1="${selectedAcct}" value2="-1" operator="notEquals">
					<option value="<ffi:getProperty name="selectedAcct" />" selected><ffi:getProperty name="${wireTask}" property="FromAccountNumberDisplayText" /></option>
				</ffi:cinclude>
				</ffi:cinclude>
                                    </select>
                            	</td>
                            	<td>
                            		 <% String wireAmount = ""; %>
                                     <ffi:getProperty name="${wireTask}" property="${wireAmountField}" assignTo="wireAmount"/>
                                     <ffi:object id="Currency" name="com.ffusion.beans.common.Currency" scope="request" />
                                     <ffi:setProperty name="Currency" property="Amount" value="<%= wireAmount %>"/>
                                     <ffi:setProperty name="Currency" property="Format" value="0.00"/>
                            		<input id="AMOUNT" name="<ffi:getProperty name="wireTask"/>.<ffi:getProperty name="wireAmountField"/>Value" value="<ffi:getProperty name="Currency" property="CurrencyStringNoSymbol"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="15"></td>
                            	<td>
                            	
                            		<div class="selectBoxHolder">
                                             	<select id="selectWireCurrencyFieldID" class="txtbox" name="<ffi:getProperty name="intFieldTask"/>.<ffi:getProperty name="wireCurrencyField"/>" <ffi:getProperty name="disableBat"/>>
                                                  <% String curType = ""; String thisType = ""; %>
                                                  <ffi:getProperty name="${intFieldTask}" property="${wireCurrencyField}" assignTo="curType"/>
                                                  <% if (curType == null) curType = ""; %>
                                                  <ffi:list collection="CurrencyList" items="currencyType">
                                                      <ffi:getProperty name="currencyType" property="CurrencyCode" assignTo="thisType"/>
                                                      <option value="<ffi:getProperty name="currencyType" property="CurrencyCode"/>" <%= curType.equals(thisType) ? "selected" : "" %>>
                                                      <ffi:getProperty name="currencyType" property="CurrencyCode"/>-<ffi:getProperty name="currencyType" property="Description"/></option>
                                                  </ffi:list>
                                                 </select>
                                     </div>
                                </td>
                            </tr>
                            
                             <tr>
                            	<td colspan="2"><span id="<ffi:getProperty name="wireTask"/>.FromAccountIDError"></span></td>
                            	<td><span id="<ffi:getProperty name="wireTask"/>.<ffi:getProperty name="wireAmountField"/>Error"></span></td>
                            </tr>
                            <tr>
                            	<ffi:cinclude value1="${templateTask}" value2="" operator="equals">	<td id="wireLabelProcessingDate"><ffi:cinclude value1="${templateTask}" value2="" operator="equals"><label for="WireTransferNewDateId"><%= LABEL_PROCESSING_DATE %></label><span class="required" title="required">*</span></ffi:cinclude></td>
                            	<td id="wireSettlementDateLabel"><ffi:cinclude value1="${templateTask}" value2="" operator="equals"><label for="WireTransferNewDateId2"><%= LABEL_SETTLEMENT_DATE %></label></ffi:cinclude></td>
                            	</ffi:cinclude>
                            	<td id="debitExchangeRateLabel" class="sectionsubhead"><label for="wireExchangeRateValue"><%= LABEL_EXCHANGE_RATE %></label></td>
                            	<td id="debitMathRuleLabel" class="sectionsubhead"><label for="selectMathRuleID"><%= LABEL_MATH_RULE %></label></td>
                            </tr>
                            
                            <tr>
                            	<ffi:cinclude value1="${templateTask}" value2="" operator="equals">		
                             	<td>
                             														
				<ffi:cinclude value1="${disableDates}" value2="disabled" operator="notEquals">
				<% 	//This is for datepicker
					String dueDateForDP=((WireTransfer)session.getAttribute("wireTransfer")).getDueDate();
					session.setAttribute("dueDateForDP",dueDateForDP);
				%>

				<sj:datepicker
					id="WireTransferNewDateId"
					value="%{#session.dueDateForDP}"
					name="%{#request.wireTask}.DueDateValue"
					maxlength="10"
					buttonImageOnly="true"
					cssClass="ui-widget-content ui-corner-all"
					cssStyle="width:100px"
				/>
				<script>
                                   var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=${wireTask}.DueDate&wireDest=${wireDest}"/>';
                                   ns.common.enableAjaxDatepicker("WireTransferNewDateId", tmpUrl);
                                   $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>'); 
                                  </script>
				</ffi:cinclude>
				<ffi:cinclude value1="${disableDates}" value2="disabled" operator="equals">
					<input name="<ffi:getProperty name="wireTask"/>.DueDate" value="<ffi:getProperty name="${wireTask}" property="DueDate"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="15" <ffi:getProperty name="disableDates"/>>
                                      		</ffi:cinclude>
                                      </td>
                             	<td>
				<ffi:cinclude value1="${disableDates}" value2="disabled" operator="notEquals">
				<% 	//This is for datepicker
					String settlementDateForDP=((WireTransfer)session.getAttribute("wireTransfer")).getSettlementDate();
					session.setAttribute("settlementDateForDP",settlementDateForDP);
				%>

				<sj:datepicker
					id="WireTransferNewDateId2"
					value="%{#session.settlementDateForDP}"
					name="%{#request.wireTask}.SettlementDateValue"
					maxlength="10"
					buttonImageOnly="true"
					cssClass="ui-widget-content ui-corner-all"
					cssStyle="width:100px"
					/>
				<script>
                                   var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=${wireTask}.SettlementDate&wireDest=${wireDest}"/>';
                                   ns.common.enableAjaxDatepicker("WireTransferNewDateId2", tmpUrl);
                                   $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>'); 
                                  </script>
				</ffi:cinclude>
				<ffi:cinclude value1="${disableDates}" value2="disabled" operator="equals">
					<input name="<ffi:getProperty name="wireTask"/>.SettlementDate" value="<ffi:getProperty name="${wireTask}" property="SettlementDate"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="15" <ffi:getProperty name="disableDates"/>>
                                      		</ffi:cinclude>
                                      	
                                      </td>
                            	</ffi:cinclude>
                            	<td><input id="wireExchangeRateValue" name="<ffi:getProperty name="intFieldTask"/>.ExchangeRate" value="<ffi:getProperty name="${intFieldTask}" property="ExchangeRate"/>" type="text" class="ui-widget-content ui-corner-all txtbox" onblur="javascript:roundExchangeRate();" size="11" <ffi:getProperty name="disableBat"/>></td>
                            	<% String mr = ""; %>
                                <ffi:getProperty name="${intFieldTask}" property="MathRule" assignTo="mr"/>
                                <% if (mr == null) mr = "MULT"; %>
                                <td>
                                	<select id="selectMathRuleID" class="txtbox" name="<ffi:getProperty name="intFieldTask"/>.MathRule" <ffi:getProperty name="disableBat"/>>
                                     <option <ffi:cinclude value1="<%= mr %>" value2="MULT">selected</ffi:cinclude>><!--L10NStart-->MULT<!--L10NEnd--></option>
                                     <option <ffi:cinclude value1="<%= mr %>" value2="DIV">selected</ffi:cinclude>><!--L10NStart-->DIV<!--L10NEnd--></option>
                                    </select>
                                </td>
                            </tr>
                            
                            <tr>
                            	<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
                             	<td><span id="<ffi:getProperty name="wireTask"/>.DueDateError"></span></td>
                             	<td><span id="<ffi:getProperty name="wireTask"/>.SettlementDateError"></span></td>
                             </ffi:cinclude>
                            	<td colspan=""></td>
                            </tr>
                            <tr>
                            	<td colspan="2" id="wireFrequencyLabel" class="sectionsubhead"><label for="selectPaymentFrequencyID1"><%= LABEL_FREQUENCY %></label></td>
                            	<td id="wireContractNumberLabel" class="sectionsubhead"><label for="wireContractNumberValue"><%= LABEL_CONTRACT_NUMBER %></label></td>
                           	 	<td id="WirePaymentCurrencyLabel" nowrap><%= LABEL_PAYMENT_CURRENCY %><label for="selectPaymentPayeeCurrencyTypeID"><span class="required">*</span></label></td>
                            </tr>
                            
                            <tr>
                            	<td colspan="2">
                            		<div class="selectBoxHolder">                                                    
                                      	<select id="selectPaymentFrequencyID1" name="<ffi:getProperty name="wireTask"/>.FrequencyValue" class="txtbox" onchange="enableRepeating(this.options[this.selectedIndex].value);" <ffi:getProperty name="disableRec"/>>
                                           <ffi:getProperty name="freqNone" encode="false"/>
                                           <% String curFreq = ""; String thisFreq = ""; %>
                                           <ffi:getProperty name="${wireTask}" property="FrequencyValue" assignTo="curFreq"/>
                                           <% if (curFreq == null) curFreq = ""; %>
                                           <ffi:list collection="wireFrequencyList" items="wireFrequency">						
					<ffi:getProperty name="wireFrequency"  property ="key" assignTo="thisFreq"/>							
						<option value="<ffi:getProperty name="wireFrequency" property="key"/>" <%= curFreq.equals(thisFreq) ? "selected" : "" %>><ffi:getProperty name="wireFrequency" property="value"/></option>						
					</ffi:list>                                                        
                                          </select>
                                    </div>
                                    <div id="wireUnlimited" style="display:inline-block;"><input type="radio" name="openEnded" value="true" onclick="neverending(this.value)" disabled><span class="columndata"> <%= LABEL_UNLIMITED %></span></div>
                                    <% String numXfers = ""; Integer freqNoneInt = new Integer(com.ffusion.beans.Frequencies.FREQ_NONE); %>
                                    <ffi:getProperty name="${wireTask}" property="NumberTransfers" assignTo="numXfers"/>
                                    <% if (numXfers == null) numXfers = ""; %>
                                     <div id="noOfWireTransfer" style="display:inline-block;">
                                     	<input type="radio" name="openEnded" value="false" onclick="neverending(this.value)" checked disabled>
                                     	<span class="columndata"> <%= LABEL_NUMBER_OF_PAYMENTS %> </span>
                                     	<input type="text" name="<ffi:getProperty name="wireTask"/>.NumberTransfers" size="2" class="ui-widget-content ui-corner-all txtbox" value="<%= numXfers.equals(freqNoneInt.toString()) ? "" : numXfers %>" <ffi:getProperty name="disableRec"/>>
                                     </div>
                            	</td>
                            	<td><input id="wireContractNumberValue" name="<ffi:getProperty name="intFieldTask"/>.ContractNumber" value="<ffi:getProperty name="${intFieldTask}" property="ContractNumber"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="11" <ffi:getProperty name="disableBat"/>></td>
                            	<td><select id="selectPaymentPayeeCurrencyTypeID" class="txtbox" name="<ffi:getProperty name="intFieldTask"/>.PayeeCurrencyType" <ffi:getProperty name="disableBat"/>>
                                    <ffi:getProperty name="${intFieldTask}" property="PayeeCurrencyType" assignTo="curType"/>
                                    <% if (curType == null) curType = ""; %>
                                    <ffi:list collection="CurrencyList" items="currencyType">
                                        <ffi:getProperty name="currencyType" property="CurrencyCode" assignTo="thisType"/>
                                        <option value="<ffi:getProperty name="currencyType" property="CurrencyCode"/>" <%= curType.equals(thisType) ? "selected" : "" %>>
                                        <ffi:getProperty name="currencyType" property="CurrencyCode"/>-<ffi:getProperty name="currencyType" property="Description"/></option>
                                    </ffi:list>
                                    </select>
                                </td>
                            </tr>
                            
                            <tr>
                            	<td colspan="3">
                            		 <span id="<ffi:getProperty name="wireTask"/>.FrequencyValueError"></span>
                                     <span id="<ffi:getProperty name="wireTask"/>.NumberTransfersError">
                            	</td>
                            	<td></td>
                            </tr>
                            <!-- <tr>
                                <td width="50%" valign="top">

                                    <table cellpadding="3" cellspacing="0" border="0" width="100%"> -->

                                        <%-- 
                                        <ffi:cinclude value1="${templateTask}" value2="" operator="equals"><tr>
                                            <td id="wireDebitAccountLabel"><span class="sectionsubhead"><%= LABEL_FROM_ACCOUNT %></span><span class="required">*</span></td>
                                            <td width="70%">
                                            
                                            
                                            <div class="selectBoxHolder">                               	
                                            	<select id="selectDebitFromAccountIDID" class="txtbox" name="<ffi:getProperty name="wireTask"/>.FromAccountID" <ffi:getProperty name="disableSemi"/>>
							
                                                    <% String curAcct = ""; String thisAcct = ""; %>
                                                    <ffi:getProperty name="${wireTask}" property="FromAccountID" assignTo="curAcct"/>
							<% if (curAcct == null) curAcct = ""; %>
							<ffi:setProperty name="selectedAcct"  value="<%=curAcct%>"/>
							
							<ffi:cinclude value1="${selectedAcct}" value2="" operator="notEquals">
							<ffi:cinclude value1="${selectedAcct}" value2="-1" operator="notEquals">
								<option value="<ffi:getProperty name="selectedAcct" />" selected><ffi:getProperty name="${wireTask}" property="FromAccountNumberDisplayText" /></option>
							</ffi:cinclude>
							</ffi:cinclude>
                                                </select><span id="<ffi:getProperty name="wireTask"/>.FromAccountIDError"></span></td>
                                            </div>
                                        </tr>
                                        <tr>
                                            <td id="wireAmountLabel"><span class="sectionsubhead"><%= LABEL_AMOUNT %></span><span class="required">*</span></td>
                                            <% String wireAmount = ""; %>
                                            <ffi:getProperty name="${wireTask}" property="${wireAmountField}" assignTo="wireAmount"/>
                                            <ffi:object id="Currency" name="com.ffusion.beans.common.Currency" scope="request" />
                                            <ffi:setProperty name="Currency" property="Amount" value="<%= wireAmount %>"/>
                                            <ffi:setProperty name="Currency" property="Format" value="0.00"/>
                                            <td ><input id="AMOUNT" name="<ffi:getProperty name="wireTask"/>.<ffi:getProperty name="wireAmountField"/>Value" value="<ffi:getProperty name="Currency" property="CurrencyStringNoSymbol"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="15"><span id="<ffi:getProperty name="wireTask"/>.<ffi:getProperty name="wireAmountField"/>Error"></span></td>
                                        </tr> --%>
                                        <%--<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
                                         <tr>
                                            <td id="wireLabelProcessingDate"><span class="sectionsubhead"><%= LABEL_PROCESSING_DATE %></span><span class="required">*</span></td>
                                            <td>														
						<ffi:cinclude value1="${disableDates}" value2="disabled" operator="notEquals">
						<% 	//This is for datepicker
							String dueDateForDP=((WireTransfer)session.getAttribute("wireTransfer")).getDueDate();
							session.setAttribute("dueDateForDP",dueDateForDP);
						%>

						<sj:datepicker
							id="WireTransferNewDateId"
							value="%{#session.dueDateForDP}"
							name="%{#request.wireTask}.DueDateValue"
							maxlength="10"
							buttonImageOnly="true"
							cssClass="ui-widget-content ui-corner-all"
						/>
						<script>
                                     var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=${wireTask}.DueDate&wireDest=${wireDest}"/>';
                                     ns.common.enableAjaxDatepicker("WireTransferNewDateId", tmpUrl);
                                    </script>
						</ffi:cinclude>
						<ffi:cinclude value1="${disableDates}" value2="disabled" operator="equals">
							<input name="<ffi:getProperty name="wireTask"/>.DueDate" value="<ffi:getProperty name="${wireTask}" property="DueDate"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="15" <ffi:getProperty name="disableDates"/>>
                                        		</ffi:cinclude>
                                        		<span id="<ffi:getProperty name="wireTask"/>.DueDateError"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="wireSettlementDateLabel"><span class="sectionsubhead"><%= LABEL_SETTLEMENT_DATE %></span></td>													
					<td>
						<ffi:cinclude value1="${disableDates}" value2="disabled" operator="notEquals">
						<% 	//This is for datepicker
							String settlementDateForDP=((WireTransfer)session.getAttribute("wireTransfer")).getSettlementDate();
							session.setAttribute("settlementDateForDP",settlementDateForDP);
						%>

						<sj:datepicker
							id="WireTransferNewDateId2"
							value="%{#session.settlementDateForDP}"
							name="%{#request.wireTask}.SettlementDateValue"
							maxlength="10"
							buttonImageOnly="true"
							cssClass="ui-widget-content ui-corner-all"
							/>
						<script>
                                     var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=${wireTask}.SettlementDate&wireDest=${wireDest}"/>';
                                     ns.common.enableAjaxDatepicker("WireTransferNewDateId2", tmpUrl);
                                    </script>
						</ffi:cinclude>
						<ffi:cinclude value1="${disableDates}" value2="disabled" operator="equals">
							<input name="<ffi:getProperty name="wireTask"/>.SettlementDate" value="<ffi:getProperty name="${wireTask}" property="SettlementDate"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="15" <ffi:getProperty name="disableDates"/>>
                                        		</ffi:cinclude>
                                        		<span id="<ffi:getProperty name="wireTask"/>.SettlementDateError"></span>
                                        	</td>
                                        </tr> 
                                        </ffi:cinclude>--%>
                                        <%-- <tr>
                                            <td id="wireFrequencyLabel" class="sectionsubhead"><%= LABEL_FREQUENCY %></td>
                                            <td>
						<div class="selectBoxHolder">                                                    
                                             	<select id="selectPaymentFrequencyID" name="<ffi:getProperty name="wireTask"/>.FrequencyValue" class="txtbox" onchange="enableRepeating(this.options[this.selectedIndex].value);" <ffi:getProperty name="disableRec"/>>
                                                  <ffi:getProperty name="freqNone" encode="false"/>
                                                  <% String curFreq = ""; String thisFreq = ""; %>
                                                  <ffi:getProperty name="${wireTask}" property="FrequencyValue" assignTo="curFreq"/>
                                                  <% if (curFreq == null) curFreq = ""; %>
                                                  <ffi:list collection="wireFrequencyList" items="wireFrequency">						
									<ffi:getProperty name="wireFrequency"  property ="key" assignTo="thisFreq"/>							
									<option value="<ffi:getProperty name="wireFrequency" property="key"/>" <%= curFreq.equals(thisFreq) ? "selected" : "" %>><ffi:getProperty name="wireFrequency" property="value"/></option>						
								</ffi:list>                                                        
                                                 </select>
                                             </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="sectionsubhead" style="padding-top:0px; padding-bottom:0px;">&nbsp;</td>
                                            <td id="wireUnlimited" style="padding-top:0px; padding-bottom:0px;"><input type="radio" name="openEnded" value="true" onclick="neverending(this.value)" disabled><span class="columndata"> <%= LABEL_UNLIMITED %></span></td>
                                        </tr>
                                        <tr>
                                            <td class="sectionsubhead" style="padding-top:0px; padding-bottom:0px;">&nbsp;</td>
                                            <% String numXfers = ""; Integer freqNoneInt = new Integer(com.ffusion.beans.Frequencies.FREQ_NONE); %>
                                            <ffi:getProperty name="${wireTask}" property="NumberTransfers" assignTo="numXfers"/>
                                            <% if (numXfers == null) numXfers = ""; %>
                                            <td id="noOfWireTransfer" style="padding-top:0px; padding-bottom:0px;" nowrap><input type="radio" name="openEnded" value="false" onclick="neverending(this.value)" checked disabled><span class="columndata"> <%= LABEL_NUMBER_OF_PAYMENTS %> </span><input type="text" name="<ffi:getProperty name="wireTask"/>.NumberTransfers" size="2" class="ui-widget-content ui-corner-all txtbox" value="<%= numXfers.equals(freqNoneInt.toString()) ? "" : numXfers %>" disabled></td>
                                        </tr><span id="<ffi:getProperty name="wireTask"/>.FrequencyValueError"></span>
                                        <span id="<ffi:getProperty name="wireTask"/>.NumberTransfersError"> --%>
                                   <!--  </table>
                                </td>
                                <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                                <td width="50%" valign="top">
	    
                                    <table width="347" cellpadding="3" cellspacing="0" border="0"> -->
                                       <%-- <tr>
                                            <td id="debitNumberOfCurrencyLabel" width="165"><span class="sectionsubhead"><%= LABEL_DEBIT_CURRENCY %></span><span class="required">*</span></td>
                                            <td width="176">
                                            	<div class="selectBoxHolder">
                                             	<select id="selectWireCurrencyFieldID" class="txtbox" name="<ffi:getProperty name="intFieldTask"/>.<ffi:getProperty name="wireCurrencyField"/>" <ffi:getProperty name="disableBat"/>>
                                                  <% String curType = ""; String thisType = ""; %>
                                                  <ffi:getProperty name="${intFieldTask}" property="${wireCurrencyField}" assignTo="curType"/>
                                                  <% if (curType == null) curType = ""; %>
                                                  <ffi:list collection="CurrencyList" items="currencyType">
                                                      <ffi:getProperty name="currencyType" property="CurrencyCode" assignTo="thisType"/>
                                                      <option value="<ffi:getProperty name="currencyType" property="CurrencyCode"/>" <%= curType.equals(thisType) ? "selected" : "" %>><ffi:getProperty name="currencyType" property="CurrencyCode"/></option>
                                                  </ffi:list>
                                                 </select>
                                             </div>
                                            </td>
                                        </tr> 
                                        <tr>
                                            <td id="debitExchangeRateLabel" class="sectionsubhead"><%= LABEL_EXCHANGE_RATE %></td>
                                            <td ><input id="wireExchangeRateValue" name="<ffi:getProperty name="intFieldTask"/>.ExchangeRate" value="<ffi:getProperty name="${intFieldTask}" property="ExchangeRate"/>" type="text" class="ui-widget-content ui-corner-all txtbox" onblur="javascript:roundExchangeRate();" size="11" <ffi:getProperty name="disableBat"/>></td>
                                        </tr>
                                        <tr>
                                            <td id="debitMathRuleLabel" class="sectionsubhead"><%= LABEL_MATH_RULE %></td>
                                            <% String mr = ""; %>
                                            <ffi:getProperty name="${intFieldTask}" property="MathRule" assignTo="mr"/>
                                            <% if (mr == null) mr = "MULT"; %>
                                            <td><select id="selectMathRuleID" class="txtbox" name="<ffi:getProperty name="intFieldTask"/>.MathRule" <ffi:getProperty name="disableBat"/>>
                                                <option <ffi:cinclude value1="<%= mr %>" value2="MULT">selected</ffi:cinclude>><!--L10NStart-->MULT<!--L10NEnd--></option>
                                                <option <ffi:cinclude value1="<%= mr %>" value2="DIV">selected</ffi:cinclude>><!--L10NStart-->DIV<!--L10NEnd--></option>
                                                </select>
                                            </td>
                                        </tr> --%>
                                        <%-- <tr>
                                            <td id="wireContractNumberLabel" class="sectionsubhead"><%= LABEL_CONTRACT_NUMBER %></td>
                                            <td ><input id="wireContractNumberValue" name="<ffi:getProperty name="intFieldTask"/>.ContractNumber" value="<ffi:getProperty name="${intFieldTask}" property="ContractNumber"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="11" <ffi:getProperty name="disableBat"/>></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr> --%>
                        </table></div></div>
                                <%-- <span class="columndata">&nbsp;<br></span>
                                <table width="715" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td class="sectionhead tbrd_b">&gt; <!--L10NStart-->Payment Information<!--L10NEnd--></td>
                                    </tr>
                                    <tr>
                                        <td><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="5" alt=""></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="347" cellpadding="3" cellspacing="0" border="0">
                                               <tr>
                                                    <td id="WirePaymentCurrencyLabel" nowrap><span class="sectionsubhead"><%= LABEL_PAYMENT_CURRENCY %></span><span class="required">*</span></td>
                                                    <td><select id="selectPaymentPayeeCurrencyTypeID" class="txtbox" name="<ffi:getProperty name="intFieldTask"/>.PayeeCurrencyType" <ffi:getProperty name="disableBat"/>>
                                                        <ffi:getProperty name="${intFieldTask}" property="PayeeCurrencyType" assignTo="curType"/>
                                                        <% if (curType == null) curType = ""; %>
                                                        <ffi:list collection="CurrencyList" items="currencyType">
                                                            <ffi:getProperty name="currencyType" property="CurrencyCode" assignTo="thisType"/>
                                                            <option value="<ffi:getProperty name="currencyType" property="CurrencyCode"/>" <%= curType.equals(thisType) ? "selected" : "" %>><ffi:getProperty name="currencyType" property="CurrencyCode"/></option>
                                                        </ffi:list>
                                                        </select>
                                                    </td>
                                                </tr> 
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <span class="columndata">&nbsp;<br></span> --%>
    <%-- ------ INTERNATIONAL End ------ --%>
	</ffi:cinclude>
    <%-- ------ COMMENT FIELDS:  Comment, OrigToBeneficiaryInfo1 - OrigToBeneficiaryInfo4, BankToBankInfo1 - BankToBankInfo6 ------ --%>
	<script language="javascript">
	$(function(){
		var $category=$("#controlContent");
		$category.hide();
		var $toggleBtn=$("#controlPanel > span");
		$toggleBtn.click(function(){
			if($(this).parent("#controlPanel").next().is(":visible")){
				$(this).parent("#controlPanel").next().slideUp("slow");
				$(this).text("> Open to enter comments");
			}else{
				$(this).parent("#controlPanel").next().slideDown("slow");
				$(this).text("> Close comments");
			}
			return false;
		});
	})
	</script>
	<div id="controlContent1" style="margin: 20px 0 10px;"><s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_comment_fields.jsp"/></div>
	<div align="center">
		<span class="required">
		* <!--L10NStart-->indicates a required field<!--L10NEnd-->
		</span>
	</div>
    <ffi:setProperty name="checkAmount_acctCollectionName" value="WiresAccounts"/>
    <%-- include javascript for checking the amount field. Note: Make sure you include this file after the collection has been populated.--%>
    <s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />
								<%--
                                <input type="button" class="submitbutton" value="CANCEL" onclick="document.location='<ffi:getProperty name="wireBackURL"/>'">
                                
                             
								<ffi:cinclude value1="${wireTask}" value2="WireTransfer" operator="notEquals">--%>
		<div class="btn-row">
								<s:if test="%{!isWireBatch}">
									<ffi:cinclude value1="${wireSource}" value2="TEMPLATE">
										<sj:a id="cancelFormButtonOnInput" 
											button="true" summaryDivId="summary" buttonType="cancel" 
											onClickTopics="showSummary,cancelWireTransferForm"
										>
											<s:text name="jsp.default_82"/>
										</sj:a>
									</ffi:cinclude>
									<ffi:cinclude value1="${wireSource}" value2="TEMPLATE" operator="notEquals">
									<sj:a id="cancelFormButtonOnInput"
										button="true" summaryDivId="summary" buttonType="cancel" 
										onClickTopics="showSummary,cancelWireTransferForm"										
									>
										<s:text name="jsp.default_82"/>
									</sj:a>
									</ffi:cinclude>
									</s:if>										
								<%-- </ffi:cinclude>--%>
								
								<script>
									ns.wire.cancelWireURLFromWireBatch = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/${wireBackURL}"/>' ;
								</script>
								<s:if test="%{isWireBatch}">
									<s:hidden id="backToBatchURLForJS" value="%{#session.wireBackURL}"></s:hidden>
	   								<sj:a id="cancelAddWireOnBatchFormID"
										button="true"
										onClickTopics="cancelAddWireOnBatchFormClickTopics"
									>
										<s:text name="jsp.default_82"/>
									</sj:a>
								</s:if>

								<s:if test="%{#request.templateTask ==''}">
									<s:set name="WireButtonLabel" value="%{getText('jsp.default_395')}"/>
								</s:if>
								<s:else>
									<s:set name="WireButtonLabel" value="%{getText('jsp.default_366')}"/>
								</s:else>

                                <%-- Start: Dual approval processing --%>

								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">									
									<s:if test="%{#request.PayeePendingInDA}"> 
										<input id="verifyDAWiresTransferNoSubmit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-disabled" type="Button"  value='<s:property value="%{#WireButtonLabel}" />' >
									</s:if>	
									<s:else>
										<sj:a
												id="verifyDAWiresTransferSubmit"
												formIds="newWiresTransferFormID"
				                                targets="verifyDiv"
				                                button="true"
				                                validate="true"
				                                validateFunction="customValidation"
				                                onBeforeTopics="beforeVerifyWireTransferForm"
				                                onCompleteTopics="verifyWireTransferFormComplete"
												onErrorTopics="errorVerifyWireTransferForm"
				                                onSuccessTopics="successVerifyWireTransferForm"
												onClick="SelectAmount()"
						                        ><s:property value="%{#WireButtonLabel}" />
						            	</sj:a>
									</s:else>										
								</ffi:cinclude>

								<%-- End: Dual approval processing--%>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">

									 <%--
									 <input type="button" class="submitbutton" value="<ffi:getProperty name="wireSaveButton"/>" onclick="if (SelectAmount()) document.WireNew.submit();">
									 --%>

									 <sj:a
												id="verifyWiresTransferSubmit"
												formIds="newWiresTransferFormID"
				                                targets="verifyDiv"
				                                button="true"
				                                validate="true"
				                                validateFunction="customValidation"
				                                onBeforeTopics="beforeVerifyWireTransferForm"
				                                onCompleteTopics="verifyWireTransferFormComplete"
												onErrorTopics="errorVerifyWireTransferForm"
				                                onSuccessTopics="successVerifyWireTransferForm"
												onClick="SelectAmount()"
						                        >
						                        <s:property value="%{#WireButtonLabel}" />
						            </sj:a>
								</ffi:cinclude>
			</div>
                            </s:form>
                    </td>
                </tr>
            </table>
</ffi:cinclude>
</div>
            </div>
        </div>

<ffi:removeProperty name="dueDateForDP"/>
<ffi:removeProperty name="settlementDateForDP"/>
<ffi:removeProperty name="isAddTransfer"/>
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">

<script>
	$(document).ready(function(){
		$("input[name='isBeneficiary']").each( function(){$(this).remove();} );
		var isRecModel = '<ffi:getProperty name="isRecModel" />';
		if(isRecModel == 'true') {
			enableRepeating(<ffi:getProperty name="${wireTask}" property="FrequencyValue"/>);
		}
		//alert($('#selectWirePayeeID').val())
		if($("#selectWirePayeeID").val()!=null && $("#selectWirePayeeID").val()!="Create New Beneficiary"){
			$(".selecteBeneficiaryMessage").toggle();
			$('#expand').toggle();
			
		}
		
		$( "#disbaledExpandBtn" ).button({ disabled: true });
	});

</script>
</ffi:cinclude>

<script>
function loadBeneficiaryForm(){
	debugger;
	//$('#selectWirePayeeID').val('Create New Beneficiary')
	var optionsStr = $("<option value='Create New Beneficiary'></option>");
	var firstOption = $('#selectWirePayeeID').find("option:first");
	$(optionsStr).insertBefore(firstOption);
	$('#selectWirePayeeID').find("option[value='Create New Beneficiary']").attr("selected","selected");
	setPayee('Create New Beneficiary');
	
}
/* used to toggle view when beneficiary selected from drop down or 
when add new is called to show details */
function openBeneficiaryDetail(){
	$('#wireAddeditPayeeFields').slideToggle();
	$('#wireAddeditBankFields').slideToggle();
	$('.lineBreak').slideToggle();
	$('#intermediaryBanksDivID').slideToggle();
	$('#collapse').toggle();
	$('#expand').toggle();
}


</script>
