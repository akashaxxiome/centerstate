<%--
This page is for adding a host wire batch

Pages that request this page
----------------------------
wiretransfers.jsp
	NEW BATCH button (when Host is selected as the Wire Type)
wirebatchhostconfirm.jsp (host wire batch confirm)
	BACK button
wirebatchff.jsp
	Choose Wire Type drop-down list (when changed to Host)
wirebatchtemp.jsp
	Choose Wire Type drop-down list (when changed to Host)

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
SUBMIT WIRE BATCH requests wirebatchhostconfirm.jsp
NEXT PAGE requests wirebatchhost.jsp (itself)
PREVIOUS PAGE requests wirebatchhost.jsp (itself)
Calendar button requests calendar.jsp

Pages included in this page
---------------------------
payments/inc/wire_batch_select.jsp
	Creates the Choose Wire Batch Type drop-down
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_batch_fields.jsp
	edit fields for a non-international wire
--%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Map.Entry"%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<ffi:help id="payments_wirebatchhost" className="moduleHelpClass"/>
<%
//if( request.getParameter("DontInitializeBatch") != null ){ session.setAttribute("DontInitializeBatch", request.getParameter("DontInitializeBatch")); }
//if( request.getParameter("wireDestination") != null ){ session.setAttribute("wireDestination", request.getParameter("wireDestination")); }
//if( request.getParameter("nextPage") != null ){ session.setAttribute("nextPage", request.getParameter("nextPage")); }
//if( request.getParameter("prevPage") != null ){ session.setAttribute("prevPage", request.getParameter("prevPage")); }

Map map = request.getParameterMap(); 
Iterator it = (Iterator) map.entrySet().iterator();
while (it.hasNext()) {
	Map.Entry entry =(Entry) it.next();
	String key =  (String)entry.getKey();
	String[] value = (String[])entry.getValue();
	session.setAttribute(key, value[0]);
}

%>
<ffi:setL10NProperty name='PageHeading' value='New Host Wire Transfer Batch'/>
<span id="PageHeading" style="display:none;"><ffi:getProperty name='PageHeading'/></span>
<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name="BackURL" value="${SecurePath}payments/wirebatchhost.jsp?DontInitializeBatch=true" URLEncrypt="true"  />
<ffi:setProperty name="wireBackURL" value="${SecurePath}payments/wirebatchhost.jsp?DontInitializeBatch=true" URLEncrypt="true" />
<ffi:setProperty name="wireBatchItemsPerPage" value="10"/>

<% String task = "AddWireBatch"; String itemsPerPageStr = ""; String pageStr = ""; String pagesStr = "";
   String nxtPage = ""; String prePage = ""; %>
<ffi:getProperty name="wireBatchItemsPerPage" assignTo="itemsPerPageStr"/>
<ffi:getProperty name="hostPage" assignTo="pageStr"/>
<ffi:getProperty name="hostPages" assignTo="pagesStr"/>
<ffi:getProperty name="nextPage" assignTo="nxtPage"/>
<ffi:getProperty name="prevPage" assignTo="prePage"/>
<%
if (nxtPage == null) nxtPage = "";
if (prePage == null) prePage = "";
int itemsPerPage = Integer.parseInt(itemsPerPageStr);

// get current page number
if (pageStr == null) pageStr = "0";
int pageNum = Integer.parseInt(pageStr);

// get total pages
if (pagesStr == null) pagesStr = "0";
int pagesNum = Integer.parseInt(pagesStr);

// calculate starting and ending wire for this current page
int startNum = pageNum * itemsPerPage;
int endNum = startNum + itemsPerPage;
int pageDelta = 0;
int count;

// if next or previous was clicked, adjust the page number
if (nxtPage.equals("true")) pageDelta = 1;
if (prePage.equals("true")) pageDelta = -1;

// if next or previous was clicked, and remember the checkboxes and adjust the page number
// Put checked checkboxes in the session with a value of "true", unchecked ones "false"
if (pageDelta != 0) {
	for (count = startNum; count < endNum; count++) {
		String checkbox = "hostcheckbox_" + String.valueOf(count);
		if (request.getParameter(checkbox) != null) { %>
			<ffi:setProperty name="<%= checkbox %>" value="true"/>
<%		} else { %>
			<ffi:setProperty name="<%= checkbox %>" value="false"/>
<%		}
	}
	pageNum += pageDelta;
	if (pageNum > pagesNum) pagesNum = pageNum;
	startNum = pageNum * itemsPerPage;
	endNum = startNum + itemsPerPage;
%>
	<ffi:setProperty name="hostPage" value="<%= String.valueOf(pageNum) %>"/>
	<ffi:setProperty name="hostPages" value="<%= String.valueOf(pagesNum) %>"/>
<%
}
%>
<ffi:removeProperty name="nextPage"/>
<ffi:removeProperty name="prevPage"/>

<ffi:setProperty name="wirebatch_confirm_form_url" value="addHostWireBatchAction"/>
 
<ffi:cinclude value1="${DontInitializeBatch}" value2="" operator="equals">
	<%--<ffi:object id="AddWireBatch" name="com.ffusion.tasks.wiretransfers.AddWireBatch" scope="session"/>
		<ffi:setProperty name="AddWireBatch" property="Initialize" value="true"/>
		<ffi:setProperty name="AddWireBatch" property="BatchType" value="<%= WireDefines.WIRE_BATCH_TYPE_HOST %>"/>
	<ffi:process name="AddWireBatch"/>
	<ffi:setProperty name="AddWireBatch" property="DateFormat" value="${UserLocale.DateFormat}"/>
	<ffi:setProperty name="AddWireBatch" property="BatchDestination" value="<%= WireDefines.WIRE_HOST %>"/>
	<ffi:setProperty name="AddWireBatch" property="SettlementDate" value="${AddWireBatch.DueDate}"/>
	--%>
	<%
	// delete any abandoned session variables we'll need to be using
	count = 0;
	String cx = (String)session.getAttribute("hosttemplateID_" + String.valueOf(count));
	while (cx != null) {
		session.removeAttribute("hostcheckbox_" + String.valueOf(count));
		session.removeAttribute("hostamount_" + String.valueOf(count));
		session.removeAttribute("hosttemplateID_" + String.valueOf(count));
		cx = (String)session.getAttribute("hosttemplateID_" + String.valueOf(++count));
	}
	%>
	<ffi:setProperty name="hostPage" value="0"/>
	<ffi:setProperty name="hostPages" value="0"/>
</ffi:cinclude>
<%--
<ffi:setProperty name="AddWireBatch" property="ClearWiresInBatch" value="true"/>
--%>
<%--<ffi:include page="${PathExt}payments/inc/wire_batch_select.jsp"/> --%>

<ffi:removeProperty name="DontInitializeBatch"/>
<ffi:removeProperty name="DontInitialize"/>
<ffi:removeProperty name="editWireTransfer"/>
<ffi:removeProperty name="WireTransfer"/>
<ffi:removeProperty name="WireTransferPayee"/>

<script>


function setDest(dest) {
	if (dest == "<%= WireDefines.WIRE_HOST %>") page = "wirebatchhost.jsp";
	else page = "wirebatch.jsp";
	document.frmBatch.DontInitializeBatch.value = "";
	document.frmBatch.wireDestination.value = dest;
	document.frmBatch.action = page;
	document.frmBatch.submit();
}

function check(state) {
	numBoxes = <%= itemsPerPage %>;     //Number of checkboxes per page
	frm = document.frmBatch;
	for (count = <%= startNum %>; count < <%= endNum %>; count++) {
		frm.elements["hostcheckbox_" + count].checked = (state == "true" ? true : false);
	}
}

function addPage() {
	$.ajax({ 
	  type: "POST",   
	  data: $("#frmBatchID").serialize(),     
	  url: "/cb/pages/jsp/wires/addHostWireBatchAction_addPage.action",     
	  success: function(data) {  	  
		 $("#inputDiv").html(data);
	  }   
	});
}

function backPage() {
	$.ajax({ 
		  type: "POST",   
		  data: $("#frmBatchID").serialize(),     
		  url: "/cb/pages/jsp/wires/addHostWireBatchAction_backPage.action",     
		  success: function(data) {  	  
			 $("#inputDiv").html(data);
		  }   
		});
}
</script>
<span id="formerrors"></span>
<s:form  name="frmBatch"  id="frmBatchID" action="addHostWireBatchAction_verify" namespace="/pages/jsp/wires"  method="post" theme="simple">
<%-- <form name="frmBatch" action="wirebatchhostconfirm.jsp" method="post">--%>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="wireDestination" value=""/>
<input type="hidden" name="DontInitializeBatch" value="true">
<div class="leftPaneWrapper" role="form" aria-labelledby="wireBatchEntryHeader">
	<div class="leftPaneInnerWrapper">
		<div class="header" id=""><h2 id="wireBatchEntryHeader">Wire Batch Entry Details</h2></div>
		<div class="leftPaneInnerBox">
			<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_fields.jsp"/>
		</div>
	</div>
</div>
<div class="confirmPageDetails" style="padding:5px !important; width:70%">
<%-- CONTENT START --%>
			<div class="paneWrapper">
				<div class="paneInnerWrapper">
					<table width="100%" cellpadding="3" cellspacing="0" border="0">
						<tr class="header">
							<td class="sectionsubhead" width="49%" align="center">
								<input type="Checkbox" name="check_all" onclick="check('true');" id="check_all">
								<input type="Checkbox" name="check_none" onclick="check('false');" id="check_none" class="hidden">
							</td>
							<td class="sectionsubhead" width="1%"><!--L10NStart--><h2>Host ID</h2><!--L10NEnd--></td>
							<td class="sectionsubhead" width="1%"><!--L10NStart--><h2>Amount</h2><!--L10NEnd--></td>
							<td class="sectionsubhead" width="49%">&nbsp;</td>
						</tr>
						<%
						for (int i = startNum; i < endNum; i++) {
							String checkbox = "hostcheckbox_" + String.valueOf(i);
							String amount = "hostamount_" + String.valueOf(i);
							String template = "hosttemplateID_" + String.valueOf(i);
							String amt = "";
							String chk = ""; %>
						<tr>
							<ffi:getProperty name="<%= checkbox %>" assignTo="chk"/>
							<% if (chk == null) chk = "false"; %>
							<td align="center" style="padding:10px 3px"><input type="Checkbox" name="hostcheckbox_<%= i %>" <%= chk.equals("true") ? "checked" : "" %>></td>
							<td style="padding:10px 3px"><input type="Text" name="hosttemplateID_<%= i %>" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= template %>"/>" size="40" maxlength="32"></td>
							<td style="padding:10px 3px"><input type="Text" name="hostamount_<%= i %>" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= amount %>"/>" size="20"></td>
							<td style="padding:10px 3px" class="columndata"><span id="hostamount_<%= i %>Error"></span>&nbsp;</td>
						</tr>
						<% } %>
						<%-- <tr>
							<td>&nbsp;</td>
							<td colspan="3">
							<sj:a button="true" onclick="check('true');"><s:text name="jsp.default_93" /></sj:a>
							<sj:a button="true" onclick="check('false');"><s:text name="jsp.default_96" /></sj:a>
							</td>
						</tr> --%>
			</table>
				</div>
			</div>
            <div class="required" align="center">* <!--L10NStart-->indicates a required field<!--L10NEnd--></div>
            <div class="btn-row">
	            <sj:a id="cancelFormButtonOnVerify" 
					button="true" summaryDivId="summary" buttonType="cancel"
					onClickTopics="showSummary,cancelWireTransferForm"
				 ><s:text name="jsp.default_82" />
				 </sj:a>
				
				 <sj:a id="verifyWireBatchID"
					formIds="frmBatchID"  
				 	targets="verifyDiv" 
				 	validate="true" 
					validateFunction="customValidation"
					button="true" 
					onBeforeTopics="verifyWireBatchBeforeTopics"
					onSuccessTopics="verifyWireBatchSuccessTopics"><s:text name="jsp.default_Submit_Wire_Batch" /></sj:a>
					
				<% if (pageNum > 0) { %>
					<sj:a button="true" onclick="backPage('true');"><s:text name="jsp.default_PREVIOUS_PAGE" /></sj:a>
				<% } %>
				<sj:a button="true" onclick="addPage('true');"><s:text name="jsp.default_NEXT_PAGE" /></sj:a>
			</div>
<%-- CONTENT END --%>
</div>
</s:form>
<script type="text/javascript">
	$(document).ready(function(){
		$( "#check_all" ).click(function(){ 
				$(this).hide();
				$("#check_none").show();
				$("#check_none").prop('checked', true);
		});
		$( "#check_none" ).click(function(){ 
				$(this).hide();
				$("#check_all").show();
				$("#check_all").prop('checked', false);
		});
	});

</script>