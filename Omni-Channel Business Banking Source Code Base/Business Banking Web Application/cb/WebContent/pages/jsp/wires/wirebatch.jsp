<%--
This file is essentially a wrapper that will include the freeform or template batch page
depending on the user's entitlements.  If they're entitled to both, it will default to
the freeform batch page.  If they're not included to either, it'll go back to the wire
summary page.

Pages that request this page
----------------------------
wiretransfers.jsp (wire summary page)
	NEW BATCH button
wirebatchff.jsp (add freeform wire batch (non-international))
	Changing Wire Type pick-list
wirebatchhost.jsp (add host wire batch)
	Changing Wire Type pick-list
wirebatchtemp.jsp (add template wire batch (non-international))
	Changing Wire Type pick-list

Pages this page requests
------------------------
See the included files

Pages included in this page
---------------------------
payments/wirebatchff.jsp
	The add free form wire batch page (non-international)
payments/wirebatchtemp.jsp
	The add template wire batch page (non-international)
payments/wiretransfers.jsp
	The wire transfer summary page
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatch" className="moduleHelpClass"/>
<span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTransferLink,newBatchWiresTransferID</span>
<span class="shortcutEntitlementClass" style="display:none;">Wires</span>
<%
if( request.getParameter("DontInitializeBatch") != null ){ session.setAttribute("DontInitializeBatch", request.getParameter("DontInitializeBatch")); }
if( request.getParameter("wireDestination") != null ){ session.setAttribute("wireDestination", request.getParameter("wireDestination")); }
if( request.getParameter("DA_WIRE_PRESENT") != null ){ session.setAttribute("DA_WIRE_PRESENT", request.getParameter("DA_WIRE_PRESENT")); }
%>
<ffi:cinclude value1="${isBatchEntitled}" value2="true"/>
<s:if test="%{isBatchEntitled}">



	<%--  If entitled to create wire batches, determine whether to show the freeform or template batch page --%>



	<ffi:removeProperty name="templatePage,templatePages" />

	<s:if test="%{isFreeFormEntitled}">
		<%-- if entitled to freeform, show the freeform batch by default. --%>
		<s:include value="%{#session.PagesPath}/wires/wirebatchff.jsp" />
	</s:if>
	<s:elseif test="%{isTemplateEntitled}">
		<s:include value="%{#session.PagesPath}/wires/wirebatchtemp.jsp" />
	</s:elseif>
	<s:else>
		<%-- show the wire summary page if not entitled to freeform or templated. (Just in case) --%>
		<s:include value="%{#session.PagesPath}/wires/wiretransfers.jsp" />
	</s:else>

</s:if>

<s:else>
	<%--  show the wire summary page if not entitled to create batches. (Just in case) %> --%>
	<s:include value="%{#session.PagesPath}/wires/wiretransfers.jsp" />
</s:else>

