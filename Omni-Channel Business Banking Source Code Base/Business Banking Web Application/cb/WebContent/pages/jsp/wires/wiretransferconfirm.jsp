<%--
This is a common wire transfer confirm page, used for all wire types except
host wires, which uses wirehostconfirm.jsp

It is used to confirm wires, wire templates, and wires added to a batch.

Pages that request this page
----------------------------
wiretransfernew.jsp
wirebook.jsp
wiredrawdown.jsp
wirefed.jsp
wiresaveastemplate.jsp
wiresaveastemplatesave.jsp

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
BACK requests one of the following:
	wiretransfernew.jsp
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp
CONFIRM/SUBMIT WIRE / CONFIRM/SAVE TEMPLATE / CONTINUE requests one of the following:
	autoentitle-confirm.jsp
	wiretransfersend.jsp
SAVE AS TEMPLATE requests wiresaveastemplate.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireTransfer" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/wire_labels.jsp"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="payments_wiretransferconfirm" className="moduleHelpClass"/>
<ffi:author page="payments/wiretransferconfirm.jsp"/>
<script type="text/javascript">
	$(document).ready(function(){		
		
		$("#viewUSDAmountLink_addwire").click(function(event){
			event.preventDefault();			
			ns.wire.viewDisplayAmount("add");
		});
		
		$("#viewUSDAmountLink_editwire").click(function(event){
			event.preventDefault();			
			ns.wire.viewDisplayAmount("mod");
		});
		
		$("#viewPaymentAmountLink_addwire").click(function(event){
			event.preventDefault();			
			ns.wire.viewDisplayPaymentAmount("add");
		});	
		$("#viewPaymentAmountLink_editwire").click(function(event){
			event.preventDefault();			
			ns.wire.viewDisplayPaymentAmount("mod");
		});	
		
     });
</script>

<%
String task = "wireTransfer";

com.ffusion.beans.wiretransfers.WireTransfer wire = (com.ffusion.beans.wiretransfers.WireTransfer)session.getAttribute(task);

String batchTask = (String)session.getAttribute("wireBatch");
if (task.equals("AddWireTransfer") || task.equals("ModifyWireTransfer") || batchTask == null) batchTask = "";

String intFieldTask = task;
String wireType = "";

boolean wireIsInBatch = false;
if((session.getAttribute("ADD_WIRE_BATCH") != null && session.getAttribute("ADD_WIRE_BATCH").toString().equalsIgnoreCase("true"))
|| (session.getAttribute("EDIT_WIRE_BATCH") != null && session.getAttribute("EDIT_WIRE_BATCH").toString().equalsIgnoreCase("true"))){
	wireIsInBatch = true;
}

String templateTask = "";
%>

<ffi:getProperty name="TemplateTask" assignTo="templateTask"/>
<%
boolean wireIsTemplate = (!templateTask.equals(""));
%>

<ffi:cinclude value1="<%= wire.getWireDestination() %>" value2="<%= WireDefines.WIRE_DOMESTIC %>" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}payments/wiretransfernew.jsp?DontInitialize=true" URLEncrypt="true"/>
	<% wireType = DOMESTIC; %>
</ffi:cinclude>
<ffi:cinclude value1="<%= wire.getWireDestination() %>" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}payments/wiretransfernew.jsp?DontInitialize=true" URLEncrypt="true"/>
	<% wireType = INTERNATIONAL; %>
	<% if (!batchTask.equals("")) {
		intFieldTask = batchTask;
                String debCurr = ""; String xRate = ""; String mRule = ""; String conNum = ""; String payCurr = "";     %>
	<ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}" assignTo="debCurr"/>
	<ffi:getProperty name="<%= intFieldTask %>" property="ExchangeRate" assignTo="xRate"/>
	<ffi:getProperty name="<%= intFieldTask %>" property="MathRule" assignTo="mRule"/>
	<ffi:getProperty name="<%= intFieldTask %>" property="ContractNumber" assignTo="conNum"/>
	<ffi:getProperty name="<%= intFieldTask %>" property="PayeeCurrencyType" assignTo="payCurr"/>
	<ffi:setProperty name="<%= task %>" property="${wireCurrencyField}" value="<%= debCurr %>"/>
	<ffi:setProperty name="<%= task %>" property="ExchangeRate" value="<%= xRate %>"/>
	<ffi:setProperty name="<%= task %>" property="MathRule" value="<%= mRule %>"/>
	<ffi:setProperty name="<%= task %>" property="ContractNumber" value="<%= conNum %>"/>
	<ffi:setProperty name="<%= task %>" property="PayeeCurrencyType" value="<%= payCurr %>"/>
	<% } %>
</ffi:cinclude>

<ffi:cinclude value1="<%= wire.getWireDestination() %>" value2="<%= WireDefines.WIRE_BOOK %>" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}payments/wirebook.jsp?DontInitialize=true" URLEncrypt="true"/>
	<% wireType = BOOK; %>
</ffi:cinclude>
<ffi:cinclude value1="<%= wire.getWireDestination() %>" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}payments/wiredrawdown.jsp?DontInitialize=true" URLEncrypt="true"/>
	<% wireType = DRAWDOWN; %>
</ffi:cinclude>
<ffi:cinclude value1="<%= wire.getWireDestination() %>" value2="<%= WireDefines.WIRE_FED %>" operator="equals">
	<ffi:setProperty name="BackURL" value="${SecurePath}payments/wirefed.jsp?DontInitialize=true" URLEncrypt="true"/>
	<% wireType = FED; %>
</ffi:cinclude>

<ffi:setProperty name='PageHeading' value='<%= (wireType + " <!--L10NStart-->Wire Transfer<!--L10NEnd--> " + (wireIsTemplate ? "<!--L10NStart-->Template<!--L10NEnd--> " : "") + "<!--L10NStart-->Confirm<!--L10NEnd-->") %>'/>
<ffi:setProperty name="PageText" value=""/>
<ffi:setProperty name="wireConfirmBackURL" value="${SecurePath}payments/wiretransferconfirm.jsp" URLEncrypt="true"/>

<div align="center" id="wiresTransferVerificationForID">
<div class="leftPaneWrapper"><div class="leftPaneInnerWrapper">
	<div class="header">Wire Summary</div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyWireBeneficiaryLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME %>: </span>
				<span class="inlineSection floatleft" id="verifyWireBeneficiaryValue"><ffi:getProperty name="WireTransferPayee" property="Name"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<% if (!wireType.equals(DRAWDOWN)) { %>
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyWireAccountLabel"><%= LABEL_FROM_ACCOUNT %>: </span>
					<span class="inlineSection floatleft"  id="verifyWireAccountValue"><ffi:getProperty name="<%= task %>" property="FromAccountNumberDisplayText"/></span>
				<% } %>
				<% if (wireType.equals(DRAWDOWN))  { %>
					<span class="sectionsubhead sectionLabel floatleft inlineSection"><%= LABEL_CREDIT_ACCOUNT %>: </span>
					<span class="inlineSection floatleft"><ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/></span>
				<% } %>
			</div>
	</div>
</div></div>
<div class="confirmPageDetails">
     <%-- Show them warnings or notes --%>
     <% String dateLabel = wireType.equals(INTERNATIONAL) ? LABEL_PROCESSING_DATE : LABEL_DUE_DATE; %>

     <% String dateChanged = "";  %>
     <ffi:getProperty name="DateChanged" assignTo="dateChanged"/>

     <% String nonBizDay = ""; %>
     <ffi:getProperty name="NonBusinessDay" assignTo="nonBizDay"/>

     <% String processingWindowClosed = ""; %>
     <% String processingWindowNotOpened = ""; %>
     <ffi:getProperty name="ProcessingWindowClosed" assignTo="processingWindowClosed"/>
     <ffi:getProperty name="ProcessingWindowNotOpened" assignTo="processingWindowNotOpened"/>

     <% if ("true".equals(dateChanged)) { %>
              <%-- Both proc window is closed and tomorrow is non-biz day --%>
              <div class="sectionsubhead" style="color:red; margin:0 0 10px" align="center">
              	NOTE: The Expected Value Date has been adjusted to the next process date.
              </div>
          <% } else if ("true".equals(processingWindowClosed)) { %>
              <%-- DueDate changed because processing window is closed --%>
              <div class="sectionsubhead" style="color:red; margin:0 0 10px" align="center">
             	 NOTE: The <%= wireType %> wire processing window for today has closed. The Expected <%= dateLabel %> has been adjusted to the next process date.
              </div>
          <% } else if ("true".equals(processingWindowNotOpened)) { %>
              <%-- DueDate did not change.  Processing window not opened yet --%>
              <div class="sectionsubhead" style="color:red; margin:0 0 10px" align="center">
              	NOTE: The <%= wireType %> wire processing window is not yet opened.  The wire will not process immediately, but later today.
              </div>
          <% } else if ("true".equals(nonBizDay)) { %>
              <%-- DueDate changed because its not a business day --%>
              <div class="sectionsubhead" style="color:red; margin:0 0 10px" align="center">
              	NOTE: The Requested <%= dateLabel %> is not a business day. The Expected <%= dateLabel %> has been adjusted to the next process date.
              </div>
          <% } %>

           <%-- Duplicate Wire warning --%>
           <% String duplicateWire = ""; %>
           <ffi:getProperty name="DuplicateWire" assignTo="duplicateWire"/>
           <ffi:cinclude value1="<%= duplicateWire %>" value2="true" operator="equals">
               <div class="sectionsubhead" style="color:red; margin:0 0 10px" align="center">
               	WARNING: A duplicate of this <%= wireType %> wire exists for this business with this Amount, <%= dateLabel %>, and Beneficiary.
               </div>
           </ffi:cinclude>
            <%-- Edit recurring wire warning --%>
            <% if ((!wireIsTemplate) && (task.equals("ModifyWireTransfer") && (wire.getType() == com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER))) { %>
				<div class="sectionsubhead" style="color:red; margin:0 0 10px" align="center">
				    <!--L10NStart-->WARNING: Any changes to the recurring wire instance will also update all other pending recurring instances of the model<!--L10NEnd-->
				</div>
			<% } %>
			<%-- Manual Entry warning message --%>
			<ffi:cinclude value1="<ffi:getProperty name='ManualEntryWarning'/>" value2="" operator="notEquals">
				<div class="sectionsubhead" style="color:red; margin:0 0 10px">
					<!--L10NStart--><ffi:getProperty name='ManualEntryWarning'/><!--L10NEnd-->
				</div>
			<ffi:removeProperty name='ManualEntryWarning'/>
			</ffi:cinclude>
                        
<% if (wireIsTemplate) { %>			
				
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Template Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_NAME %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_NICKNAME %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="NickName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_CATEGORY %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCategory"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_SCOPE %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireScope"/></span>
			</div>
		</div>
	</div>
</div>
<s:hidden name="url_action" value="%{#attr.SubmitAction}" id="url_action"></s:hidden>														
							
							<%-- END OF GET cumulative settings --%>

							<%-- <table border="0" cellspacing="0" cellpadding="3" width="715">
								<tr>
									<td class="tbrd_b sectionhead" colspan="4">Template Info</td>
								</tr>
								<tr>
									<td width="135" class="sectionsubhead"><%= LABEL_TEMPLATE_NAME %></td>
									<td width="221" class="columndata"><ffi:getProperty name="<%= task %>" property="WireName"/></td>
									<td width="115" class="sectionsubhead"><%= LABEL_TEMPLATE_CATEGORY %></td>
									<td width="220" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCategory"/></td>
								</tr>
								<tr>
									<td class="sectionsubhead"><%= LABEL_TEMPLATE_NICKNAME %></td>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="NickName"/></td>
									<td class="sectionsubhead"><%= LABEL_TEMPLATE_SCOPE %></td>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireScope"/></td>
								</tr>
							</table>
							<br> --%>
<% } %>
<s:hidden name="initSaveAsTemplateAction" value="%{#attr.InitSaveAsTemplateAction}" id="initSaveAsTemplateAction"></s:hidden>
<% String wireSource = ""; %>
<ffi:getProperty name="<%= task %>" property="WireSource" assignTo="wireSource"/>
<% if (wireSource == null) wireSource = "";
   if (wireSource.equals("TEMPLATE") && !wireIsTemplate) { %>
<%-- ------ TEMPLATE (READ-ONLY) NAME, NICKNAME, ID, AND WIRE LIMIT ------ --%>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Template Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_NAME %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_NICKNAME %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="NickName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_ID %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="TemplateID"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_LIMIT %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="DisplayWireLimit"/></span>
			</div>
		</div>
	</div>
</div>
							<%-- <table border="0" cellspacing="0" cellpadding="3" width="715">
								<tr>
									<td class="tbrd_b sectionhead" colspan="4"><!--L10NStart-->Template Info<!--L10NEnd--></td>
								</tr>
								<tr>
									<td width="135" class="sectionsubhead"><%= LABEL_TEMPLATE_NAME %></td>
									<td width="221" class="columndata"><ffi:getProperty name="<%= task %>" property="WireName"/></td>
									<td width="115" class="sectionsubhead"><%= LABEL_TEMPLATE_NICKNAME %></td>
									<td width="220" class="columndata"><ffi:getProperty name="<%= task %>" property="NickName"/></td>
								</tr>
								<tr>
									<td class="sectionsubhead"><%= LABEL_TEMPLATE_ID %></td>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="TemplateID"/></td>
									<td class="sectionsubhead"><%= LABEL_TEMPLATE_LIMIT %></td>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="DisplayWireLimit"/></td>
								</tr>
							</table> --%>
<% } %>

<% if (wireType.equals(DRAWDOWN))  { %>
<div  class="blockHead toggleClick"><ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/> <span class="sapUiIconCls icon-positive"></span></div>
<div class="toggleBlock hidden">
	<div class="blockWrapper">
			<div  class="blockHead"><!--L10NStart-->Beneficiary Info<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BENEFICIARY_NAME %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.PayeeName"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_1 %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Street"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_2 %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Street2"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_CITY %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.City"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_STATE %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.StateDisplayName"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_ZIP_CODE %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.ZipCode"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_CONTACT_PERSON %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Contact"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_CREDIT_ACCOUNT %>: </span>
							<span class="columndata"><ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/></span>
					</div>
				</div>
			</div>
			<div  class="blockHead"><!--L10NStart-->Beneficiary Bank Info<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_NAME %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.BankName"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_ADDRESS_1 %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.Street"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_ADDRESS_2 %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.Street2"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_CITY %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.City"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_STATE %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.State"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_ZIP_CODE %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.ZipCode"/></span>
					</div>
				</div>
				<div class="blockRow">
					<span class="sectionsubhead sectionLabel"><%= LABEL_FED_ABA %></span>
					<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.RoutingFedWire"/></span>
				</div>
			</div>
</div></div>
<%-- DEBIT ACCOUNT INFO --%>
		<!-- <div  class="blockHead">L10NStartDebit Bank InformationL10NEnd</div> -->
	<% } %>
<div  class="blockHead toggleClick"><ffi:getProperty name="WireTransferPayee" property="Name"/> (<s:if test='%{WireTransferPayee.NickName!=""}'><ffi:getProperty name="WireTransferPayee" property="NickName"/> - </s:if><ffi:getProperty name="WireTransferPayee" property="AccountNum"/>) <span class="sapUiIconCls icon-positive"></span></div>
<div class="toggleBlock hidden">
<!-- Beneficiary Block -->
<div class="blockWrapper">
			<div  class="blockHead"><%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> Info</div>
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span id="verifyWireAccountNoLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NUMBER : LABEL_ACCOUNT_NUMBER %>: </span>
							<span id="verifyWireAccountNoValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountNum"/></span>
					</div>
					<div class="inlineBlock">
							<span id="verifyWireAccountTypeLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_TYPE : LABEL_ACCOUNT_TYPE %>: </span>
							<span id="verifyWireAccountTypeValuel" class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountTypeDisplayName"/></span>
					</div>
				</div>
				
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span id="verifyWireBeneficiaryLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME %>: </span>
							<span id="verifyWireBeneficiaryValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Name"/></span>
					</div>
					<div class="inlineBlock">
							<span id="verifyWireBeneficiaryNickNameLabel" class="sectionsubhead sectionLabel"><%= LABEL_NICKNAME %>: </span>
							<span id="verifyWireBeneficiaryNickNameValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="NickName"/></span>
					</div>
				</div>
				
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span id="verifyWireAddress1Label" class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_1 %>: </span>
							<span id="verifyWireAddress1Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street"/></span>
					</div>
					<div class="inlineBlock">
							<span id="verifyWireAddress2Label" class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_2 %>: </span>
							<span id="verifyWireAddress2Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street2"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel" id="verifyWireCityLabel"><%= LABEL_CITY %>: </span>
							<span id="verifyWireCityValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="City"/></span>
					</div>
					<ffi:cinclude value1="${StatesExistsForPayeeCountry}" value2="true">
						<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel" id="verifyWireStateLabel"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_STATE_PROVINCE : LABEL_STATE %> :</span>
							<span class="columndata" id="verifyWireStateValue"><ffi:getProperty name="WireTransferPayee" property="StateDisplayName"/></span>
						</div>
					</ffi:cinclude>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel" id="verifyWireZipCodeLabel"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE %> :</span>
							<span class="columndata" id="verifyWireZipCodeValuel"><ffi:getProperty name="WireTransferPayee" property="ZipCode"/></span>
					</div>
					<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
						<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel" id="verifyWireCountryLabel"><%= LABEL_COUNTRY %>: </span>
								<span class="columndata" id="verifyWireCountryValue"><ffi:getProperty name="WireTransferPayee" property="CountryDisplayName"/></span>
						</div>
					<% } %>
				</div>
				
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span id="verifyWireContactLabel" class="sectionsubhead sectionLabel"><%= LABEL_CONTACT_PERSON %>: </span>
							<span id="verifyWireContactValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Contact"/></span>
					</div>
					<div class="inlineBlock">
							<span id="verifyWireScopeLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_SCOPE : LABEL_BENEFICIARY_SCOPE %>: </span>
							<span id="verifyWireScopeValue" class="columndata"><ffi:setProperty name="WirePayeeScopes" property="Key" value="${WireTransferPayee.PayeeScope}"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></span>
					</div>
				</div>
			</div>
			<div  class="blockHead"><%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Debit<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> <!--L10NStart-->Bank Info<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel" id="verifyWireBankNameLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME %>: </span>
							<span id="verifyWireBankNameValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/></span>
					</div>
					<div class="inlineBlock">
							<span id="verifyWireBankAddress1Label" class="sectionsubhead sectionLabel"><%= LABEL_BANK_ADDRESS_1 %>: </span>
							<span class="columndata" id="verifyWireBankAddress1Value"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel" id="verifyWireBankAddress2Label"><%= LABEL_BANK_ADDRESS_2 %>: </span>
							<span class="columndata" id="verifyWireBankAddress2Value"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel" id="verifyWireBankCityLabel"><%= LABEL_BANK_CITY %>: </span>
							<span class="columndata" id="verifyWireBankCityValue"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/></span>
					</div>
				</div>
				<div class="blockRow">
				<ffi:cinclude value1="${StatesExistsForDestinationBankCountry}" value2="true">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel" id="verifyWireBankStateLabel"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_BANK_STATE_PROVINCE : LABEL_BANK_STATE %>: </span>
							<span class="columndata" id="verifyWireBankStateValue"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.StateDisplayName"/></span>
					</div>
				</ffi:cinclude>
					
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel" id="verifyWireBankZipCodeLabel"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_BANK_ZIP_POSTAL_CODE : LABEL_BANK_ZIP_CODE %></span>
							<span class="columndata" id="verifyWireBankZipCodeValue"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/></span>
					</div>
				</div>
				
				<div class="blockRow">
					<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
					<div class="inlineBlock" style="width: 50%">
							<span id="verifyWireBankCountryLabel" class="sectionsubhead sectionLabel"><%= LABEL_BANK_COUNTRY %>: </span>
							<span class="columndata" id="verifyWireBankCountryValue"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CountryDisplayName"/></span>
					</div>
					<% } %>
					
					<div class="inlineBlock">
							<span id="verifyWireFEDLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_ABA : LABEL_FED_ABA %>: </span>
							<span class="columndata" id="verifyWireFEDValue"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/></span>
					</div>
				</div>
				<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
					<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel" id="verifyWireSWIFTLabel"><%= LABEL_SWIFT %>: </span>
							<span class="columndata" id="verifyWireSWIFTValue"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingSwift"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel" id="verifyWireCHIPSLabel"><%= LABEL_CHIPS %>: </span>
							<span class="columndata" id="verifyWireCHIPSValue"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingChips"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel" id="verifyWireNATIONALLabel"><%= LABEL_NATIONAL %>: </span>
							<span class="columndata" id="verifyWireNATIONALValue"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingOther"/></</span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel" id="verifyWireIBANLabel"><%= LABEL_IBAN %>: </span>
							<span class="columndata" id="verifyWireIBANValue"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></span>
					</div>
				</div>
				<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
				<div class="blockRow">
					<span class="sectionsubhead sectionLabel"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></span>
					<span class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></span>
				</div>
				</ffi:cinclude>
				<% } else if(wireType.equals(FED)) {%>
					<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_IBAN %>: </span>
							<span class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></span>
					</div>
					<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
						<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></span>
								<span class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></span>
						</div>
					</ffi:cinclude>
				</div>
				
				<% } %>
				
			</div>
</div>
	<%--<tr>
		<td width="357" valign="top">
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				 <tr>
					<td class="sectionhead" colspan="2"><%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> Info</td>
				</tr>
				<tr>
					<td id="verifyWireAccountNoLabel" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NUMBER : LABEL_ACCOUNT_NUMBER %></td>
					<td id="verifyWireAccountNoValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountNum"/></td>
				</tr>
				<tr>
					<td id="verifyWireAccountTypeLabel" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_TYPE : LABEL_ACCOUNT_TYPE %></td>
					<td id="verifyWireAccountTypeValuel" class="columndata">
						<ffi:getProperty name="WireTransferPayee" property="AccountTypeDisplayName"/>
					</td>
				</tr> 
				<tr>
					<td id="verifyWireBeneficiaryLabel" width="135" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME %></td>
					<td id="verifyWireBeneficiaryValue" width="208" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Name"/></td>
				</tr>
				<tr>
					<td id="verifyWireBeneficiaryNickNameLabel" class="sectionsubhead"><%= LABEL_NICKNAME %></td>
					<td id="verifyWireBeneficiaryNickNameValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="NickName"/></td>
				</tr>
				<tr>
					<td id="verifyWireAddress1Label"  class="sectionsubhead"><%= LABEL_ADDRESS_1 %></td>
					<td id="verifyWireAddress1Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street"/></td>
				</tr>
				<tr>
					<td id="verifyWireAddress2Label" class="sectionsubhead"><%= LABEL_ADDRESS_2 %></td>
					<td id="verifyWireAddress2Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street2"/></td>
				</tr>
				<tr>
					<td id="verifyWireCityLabel" class="sectionsubhead"><%= LABEL_CITY %></td>
					<td id="verifyWireCityValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="City"/></td>
				</tr>
				<ffi:cinclude value1="${StatesExistsForPayeeCountry}" value2="true">
				<tr>
					<td id="verifyWireStateLabel" class="sectionsubhead"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_STATE_PROVINCE : LABEL_STATE %></td>
					<td id="verifyWireStateValue" class="columndata">
						<ffi:getProperty name="WireTransferPayee" property="StateDisplayName"/>
						
					</td>
				</tr>
				</ffi:cinclude>
				<tr>
					<td id="verifyWireZipCodeLabel" class="sectionsubhead"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE %></td>
					<td id="verifyWireZipCodeValuel" class="columndata"><ffi:getProperty name="WireTransferPayee" property="ZipCode"/></td>
				</tr>
<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
				<tr>
					<td id="verifyWireCountryLabel" class="sectionsubhead"><%= LABEL_COUNTRY %></td>
				    <td id="verifyWireCountryValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="CountryDisplayName"/></td>
				</tr>
<% } %>
				<tr>
					<td id="verifyWireContactLabel" class="sectionsubhead"><%= LABEL_CONTACT_PERSON %></td>
					<td id="verifyWireContactValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Contact"/></td>
				</tr>
				<tr>
					<td id="verifyWireScopeLabel" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_SCOPE : LABEL_BENEFICIARY_SCOPE %></td>
					<td id="verifyWireScopeValue" class="columndata"><ffi:setProperty name="WirePayeeScopes" property="Key" value="${WireTransferPayee.PayeeScope}"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></td>
				</tr>				
			</table>
		</td>
		<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td class="sectionhead" colspan="2">&gt; <%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Debit<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> <!--L10NStart-->Bank Info<!--L10NEnd--></td>
				</tr>
				<tr>
					<td id="verifyWireBankNameLabel" width="115" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME %></td>
					<td id="verifyWireBankNameValue" width="220" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/></td>
				</tr>
				<tr>
					<td id="verifyWireBankAddress1Label" class="sectionsubhead"><%= LABEL_BANK_ADDRESS_1 %></td>
					<td id="verifyWireBankAddress1Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/></td>
				</tr>
				<tr>
					<td id="verifyWireBankAddress2Label" class="sectionsubhead"><%= LABEL_BANK_ADDRESS_2 %></td>
					<td id="verifyWireBankAddress2Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/></td>
				</tr>
				<tr>
					<td id="verifyWireBankCityLabel" class="sectionsubhead"><%= LABEL_BANK_CITY %></td>
					<td id="verifyWireBankCityValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/></td>
				</tr>
				<ffi:cinclude value1="${StatesExistsForDestinationBankCountry}" value2="true">
				<tr>
					<td id="verifyWireBankStateLabel" class="sectionsubhead"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_BANK_STATE_PROVINCE : LABEL_BANK_STATE %></td>
					<td id="verifyWireBankStateValue" class="columndata">						
						<ffi:getProperty name="WireTransferPayee" property="DestinationBank.StateDisplayName"/>
					</td>
				</tr>
				</ffi:cinclude>
				<tr>
					<td id="verifyWireBankZipCodeLabel" class="sectionsubhead"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_BANK_ZIP_POSTAL_CODE : LABEL_BANK_ZIP_CODE %></td>
					<td id="verifyWireBankZipCodeValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/></td>
				</tr>
				<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
				<tr>
					<td id="verifyWireBankCountryLabel" class="sectionsubhead"><%= LABEL_BANK_COUNTRY %></td>					
					<td id="verifyWireBankCountryValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CountryDisplayName"/></td>					
				</tr>
				<% } %>
				<tr>
					<td id="verifyWireFEDLabel" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_ABA : LABEL_FED_ABA %></td>
					<td id="verifyWireFEDValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/></td>
				</tr>
				<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
				<tr>
					<td id="verifyWireSWIFTLabel" class="sectionsubhead"><%= LABEL_SWIFT %></td>
					<td id="verifyWireSWIFTValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingSwift"/></td>
				</tr>
				<tr>
					<td id="verifyWireCHIPSLabel" class="sectionsubhead"><%= LABEL_CHIPS %></td>
					<td id="verifyWireCHIPSValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingChips"/></td>
				</tr>
				<tr>
					<td id="verifyWireNATIONALLabel" class="sectionsubhead"><%= LABEL_NATIONAL %></td>
					<td id="verifyWireNATIONALValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingOther"/></td>
				</tr>
				<tr>
					<td id="verifyWireIBANLabel" class="sectionsubhead"><%= LABEL_IBAN %></td>
					<td id="verifyWireIBANValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></td>
				</tr>
				<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
				<tr>
					<td class="sectionsubhead"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
					<td class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
				</tr>
				</ffi:cinclude>
				<% } else if(wireType.equals(FED)) {%>
				<tr>
					<td class="sectionsubhead"><%= LABEL_IBAN %></td>
					<td class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></td>
				</tr>
				<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
				<tr>
					<td class="sectionsubhead"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
					<td class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
				</tr>
				</ffi:cinclude>
				<% } %>
			</table>
		</td>
	</tr>--%>
</table>
<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL) || wireType.equals(FED)) { %>
<ffi:setProperty name="WireTransferPayee" property="IntermediaryBanks.Filter" value="ACTION!!del"/>
<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks}" value2="" operator="notEquals" >
<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >


<div class="blockWrapper">
<% int counter = 0; %>
	<ffi:list collection="WireTransferPayee.IntermediaryBanks" items="Bank1">
	<div  class="blockHead"><!--L10NStart-->Intermediary Banks<!--L10NEnd--></div>

	<div class="blockContent">
		<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%-- <%= COLUMN_INTERMEDIARY_BANK %> --%> Name: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="BankName"/></span>
		</div>
		<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
			</div>
		</div>
		
	<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><%= COLUMN_SWIFT %>: </span>
					<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/></span>
			</div>
			<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><%= COLUMN_CHIPS %>: </span>
					<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><%= COLUMN_NATIONAL %>: </span>
					<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/></span>
			</div>
			<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
					<span class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/></span>
			</div>
		</div>
		<% if(counter == 0) { %>
		<% } else { %>
		<div class="blockRow">
					<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
					<span class="columndata"><ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/></span>
		</div>
		<% } %>
	<% } else if (wireType.equals(FED)) { %>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
					<span class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/></span>
			</div>
			<% if(counter == 0) { %>
			<% } else { %>
			<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
					<span class="columndata"><ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/></span>
			</div>
			<% } %>
		</div>
	<% } else { %>
	<% } %>
	</div>
<% counter++; %>
</ffi:list>
</div>
</ffi:cinclude>
</ffi:cinclude>
<% } %>
</div>
<%-- <table width="720" border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="1" border="0"></td>
		<td class="sectionhead" colspan="5">&gt; <!--L10NStart-->Intermediary Banks<!--L10NEnd--></td>
	</tr>
	<tr>
		<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
		<td width="5%" class="sectionsubhead">&nbsp;</td>
		<td width="30%" class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></td>
		<td width="12%" class="sectionsubhead"><%= COLUMN_FED_ABA %></td>
		<td width="12%" class="sectionsubhead"><%= COLUMN_SWIFT %></td>
		<td width="11%" class="sectionsubhead"><%= COLUMN_CHIPS %></td>
		<td width="10%" class="sectionsubhead"><%= COLUMN_NATIONAL %></td>
		<td width="10%" class="sectionsubhead"><%= COLUMN_IBAN %></td>
		<td width="10%" class="sectionsubhead"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
		<% } else if (wireType.equals(FED)) { %>
		<td width="5%" class="sectionsubhead">&nbsp;</td>
		<td width="35%" class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></td>
		<td width="20%" class="sectionsubhead"><%= COLUMN_FED_ABA %></td>
		<td width="20%" class="sectionsubhead"><%= COLUMN_IBAN %></td>
		<td width="20%" class="sectionsubhead"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
		<% } else { %>
		<td width="5%" class="sectionsubhead">&nbsp;</td>
		<td width="45%" class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></td>
		<td width="50%" class="sectionsubhead"><%= COLUMN_FED_ABA %></td>
		<% } %>
	</tr>
	<% int counter = 0; %>
	<ffi:list collection="WireTransferPayee.IntermediaryBanks" items="Bank1">
	<tr>
		<td class="columndata">&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</td>
		<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
		<td class="columndata">
			<% if(counter == 0) { %>
			&nbsp;
			<% } else { %>
			<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
			<% } %></td>				
		<% } else if (wireType.equals(FED)) { %>
		<td class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
		<td class="columndata">
			<% if(counter == 0) { %>
			&nbsp;
			<% } else { %>
			<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
			<% }  %>&nbsp;</td>	
		<% } counter++; %>
	</tr>
	</ffi:list>
</table> --%>
<%-- DOMESTIC --%>
<div class="blockWrapper">
	<div  class="blockHead"><%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Credit<!--L10NEnd-->" : "<!--L10NStart-->Debit<!--L10NEnd-->" %> <!--L10NStart-->Info<!--L10NEnd--></div>
	<div class="blockContent">
	<% // International wires need a wider first column than domestic.
	int c1 = 135;
	if (wireType.equals(INTERNATIONAL)) c1 = 160;
	int c2 = 343 - c1;
	if (!wireType.equals(DRAWDOWN)) { %>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel" id="verifyWireAccountLabel"><%= LABEL_FROM_ACCOUNT %>: </span>
			<span class="columndata" id="verifyWireAccountValue"><ffi:getProperty name="<%= task %>" property="FromAccountNumberDisplayText"/></span>
		</div>
	<% } %>
	<% if (!wireType.equals(INTERNATIONAL)) { %>
	<div class="blockRow">
		<span class="sectionsubhead sectionLabel" id="verifyWireAmountLabel"><%= wireIsTemplate ? LABEL_TEMPLATE_LIMIT : LABEL_AMOUNT %>: </span>
		<span class="columndata" id="verifyWireAmountValue">
			<ffi:cinclude value1="${wireAmountField}" value2="Amount" operator="equals">
				<ffi:getProperty name="<%= task %>" property="DisplayAmount"/>
			</ffi:cinclude>
				<ffi:cinclude value1="${wireAmountField}" value2="OrigAmount" operator="equals">
				<ffi:getProperty name="<%= task %>" property="DisplayOrigAmountDomestic"/>
			</ffi:cinclude>
		</span>
	</div>
	<% } else { %>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= wireIsTemplate ? LABEL_TEMPLATE_LIMIT : LABEL_ORIG_AMOUNT %>: </span>
				<span class="columndata">
					<ffi:cinclude value1="${wireAmountField}" value2="Amount" operator="equals"><ffi:getProperty name="<%= task %>" property="DisplayAmountForBPW"/></ffi:cinclude>
					<ffi:cinclude value1="${wireAmountField}" value2="OrigAmount" operator="equals"><ffi:getProperty name="<%= task %>" property="DisplayOrigAmount"/></ffi:cinclude>
					(<ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}"/>)
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_USD_AMOUNT %>: </span>		
				<s:if test="%{SubmitAction == 'modifyWireTransferAction' || SubmitAction == 'modifyWireTemplateAction.action'}">
					<span class="columndata"><span id="viewUSDAmountSpan"></span><a href="#" id="viewUSDAmountLink_editwire"><%= BUTTON_VIEW_AMOUNTS %></a></span>
				</s:if>
				<s:else>
					<span class="columndata"><span id="viewUSDAmountSpan"></span><a href="#" id="viewUSDAmountLink_addwire"><%= BUTTON_VIEW_AMOUNTS %></a></span>
				</s:else>
			</div>
		</div>		 
		<div class="blockRow">
					<span class="sectionsubhead sectionLabel"><%= LABEL_PAYMENT_AMOUNT %>: </span>
					<s:if test="%{SubmitAction == 'modifyWireTransferAction'  || SubmitAction == 'modifyWireTemplateAction.action'}">
					<span class="columndata"><span id="viewPaymentAmountSpan"></span><a href="#" id="viewPaymentAmountLink_editwire"><%= BUTTON_VIEW_AMOUNTS %></a>
					</s:if>
					<s:else>
					 <span class="columndata"><span id="viewPaymentAmountSpan"></span><a href="#" id="viewPaymentAmountLink_addwire"><%= BUTTON_VIEW_AMOUNTS %></a>
					 </s:else>
					
					<span id="viewPaymentAmountCurrencySpan" style="display: none">(<ffi:getProperty name="<%= intFieldTask %>" property="PayeeCurrencyType"/>)</span></span>
		</div>
	<%   } %>
	<ffi:removeProperty name="GetAmounts"/>
	<% if (!wireIsTemplate) { %>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel" id="verifyWireDueDateLabel"><%= wireType.equals(INTERNATIONAL) ? LABEL_REQ_PROCESSING_DATE : LABEL_REQ_DUE_DATE %>: </span>
					<span class="columndata" id="verifyWireDueDateValue"><ffi:getProperty name="<%= task %>" property="DueDate"/></span>
			</div>
			<% if (wireType.equals(INTERNATIONAL)) { %>
			<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel" id="verifyWireSettledDateLabel"><%= LABEL_SETTLEMENT_DATE %>: </span>
					<span class="columndata" id="verifyWireSettledDateValue"><ffi:getProperty name="<%= task %>" property="SettlementDate"/></span>
			</div>
			<% } %>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel" id="verifyWireExpectedDateLabel"><%= wireType.equals(INTERNATIONAL) ? LABEL_ACTUAL_PROCESSING_DATE : LABEL_ACTUAL_DUE_DATE %>: </span>
			<span class="columndata" id="verifyWireExpectedDateValue"><ffi:getProperty name="<%= task %>" property="DateToPost"/></span>
		</div>
	<% } %>
	<% if (wire.getType() == com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) { %>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><%= LABEL_FREQUENCY %>: </span>
					<span class="columndata"><ffi:getProperty name="<%= task %>" property="FrequencyDisplayName"/></span>
			</div>
			<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><%= LABEL_NUMBER_OF_PAYMENTS %>: </span>
					<span class="columndata">
						<% if (wire.getNumberTransfersValue() == 999) { %>
						<%= LABEL_UNLIMITED %>
						<% } else { %>
							<ffi:getProperty name="<%= task %>" property="NumberTransfers"/>
						<% } %>
					</span>
			</div>
		</div>
	<% } %>
	</div>
</div>
<% if (!wireType.equals(INTERNATIONAL)) { %>
<% } else {     %>
		<div class="blockWrapper">
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_DEBIT_CURRENCY %>: </span>
							<span class="columndata">
								<ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}"/>
								<% String debitCurrency = ""; %>
								<ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}" assignTo="debitCurrency"/>
								<ffi:setProperty name="DebitCurrencyType" value="<%=debitCurrency%>"/>
							</span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_EXCHANGE_RATE %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ExchangeRate"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_MATH_RULE %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="MathRule"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_CONTRACT_NUMBER %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ContractNumber"/></span>
					</div>
				</div>	
			</div>
		</div>
		<div class="blockWrapper">
			<div  class="blockHead"><!--L10NStart-->Payment Info<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
							<span class="sectionsubhead sectionLabel"><%= LABEL_PAYMENT_CURRENCY %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="PayeeCurrencyType"/> </span>
				</div>
			</div>
		</div>
<% } %>

<%-- <table width="715" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="tbrd_b sectionhead" colspan="3">&gt; <%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Credit<!--L10NEnd-->" : "<!--L10NStart-->Debit<!--L10NEnd-->" %> <!--L10NStart-->Info<!--L10NEnd--></td>
	</tr>
	<tr>
		<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="357" valign="top">
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<% // International wires need a wider first column than domestic.
				int c1 = 135;
				if (wireType.equals(INTERNATIONAL)) c1 = 160;
				int c2 = 343 - c1;
				
				if (!wireType.equals(DRAWDOWN)) { %>
				<tr>
					<td id="verifyWireAccountLabel" width="<%= c1 %>" class="sectionsubhead"><%= LABEL_FROM_ACCOUNT %></td>
					<td id="verifyWireAccountValue" width="<%= c2 %>" class="columndata">
						<ffi:getProperty name="<%= task %>" property="FromAccountNumberDisplayText"/>
					</td>
				</tr>
				<% } %>
				<% if (!wireType.equals(INTERNATIONAL)) { %>
				<tr>
					<td id="verifyWireAmountLabel" width="<%= c1 %>" class="sectionsubhead"><%= wireIsTemplate ? LABEL_TEMPLATE_LIMIT : LABEL_AMOUNT %></td>
					<td id="verifyWireAmountValue" width="<%= c2 %>" class="columndata">
					<ffi:cinclude value1="${wireAmountField}" value2="Amount" operator="equals">
						<ffi:getProperty name="<%= task %>" property="DisplayAmount"/>
					</ffi:cinclude>
						<ffi:cinclude value1="${wireAmountField}" value2="OrigAmount" operator="equals">
						<ffi:getProperty name="<%= task %>" property="DisplayOrigAmountDomestic"/>
					</ffi:cinclude>
					</td>
				</tr>
				<% } else { %>
				 
				
						
				<tr>
					<td width="<%= c1 %>" class="sectionsubhead"><%= wireIsTemplate ? LABEL_TEMPLATE_LIMIT : LABEL_ORIG_AMOUNT %></td>
					<td width="<%= c2 %>" class="columndata">
					<ffi:cinclude value1="${wireAmountField}" value2="Amount" operator="equals"><ffi:getProperty name="<%= task %>" property="DisplayAmountForBPW"/></ffi:cinclude>
					<ffi:cinclude value1="${wireAmountField}" value2="OrigAmount" operator="equals"><ffi:getProperty name="<%= task %>" property="DisplayOrigAmount"/></ffi:cinclude>
					(<ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}"/>)</td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_USD_AMOUNT %></td>					
					<td class="columndata"><span id="viewUSDAmountSpan"></span><a href="#" id="viewUSDAmountLink"><%= BUTTON_VIEW_AMOUNTS %></a></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_PAYMENT_AMOUNT %></td>
					<td class="columndata"><span id="viewPaymentAmountSpan"></span><a href="#" id="viewPaymentAmountLink"><%= BUTTON_VIEW_AMOUNTS %></a>
					<span id="viewPaymentAmountCurrencySpan" style="display: none">(<ffi:getProperty name="<%= intFieldTask %>" property="PayeeCurrencyType"/>)</span></td>
				</tr>
               
				<%   } %>
				<ffi:removeProperty name="GetAmounts"/>
				<% if (!wireIsTemplate) { %>
				<tr>
					<td id="verifyWireDueDateLabel" class="sectionsubhead"><%= wireType.equals(INTERNATIONAL) ? LABEL_REQ_PROCESSING_DATE : LABEL_REQ_DUE_DATE %></td>
					<td id="verifyWireDueDateValue" class="columndata"><ffi:getProperty name="<%= task %>" property="DueDate"/></td>
				</tr>
					<% if (wireType.equals(INTERNATIONAL)) { %>
					<tr>
						<td id="verifyWireSettledDateLabel" class="sectionsubhead"><%= LABEL_SETTLEMENT_DATE %></td>
						<td id="verifyWireSettledDateValue" class="columndata"><ffi:getProperty name="<%= task %>" property="SettlementDate"/></td>
					</tr>
					<% } %>
				<tr>
					<td id="verifyWireExpectedDateLabel" class="sectionsubhead"><%= wireType.equals(INTERNATIONAL) ? LABEL_ACTUAL_PROCESSING_DATE : LABEL_ACTUAL_DUE_DATE %></td>
					<td id="verifyWireExpectedDateValue" class="columndata"><ffi:getProperty name="<%= task %>" property="DateToPost"/></td>
				</tr>
				<% } %>
<% if (wire.getType() == com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) { %>
				<tr>
					<td class="sectionsubhead"><%= LABEL_FREQUENCY %></td>
					<td class="columndata">
					<ffi:getProperty name="<%= task %>" property="FrequencyDisplayName"/>
					</td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_NUMBER_OF_PAYMENTS %></td>
					<td class="columndata">
					<% if (wire.getNumberTransfersValue() == 999) { %>
						<%= LABEL_UNLIMITED %>
					<% } else { %>
						<ffi:getProperty name="<%= task %>" property="NumberTransfers"/>
					<% } %>
					</td>
				</tr>
<% } %>
			</table>
		</td>
<% if (!wireType.equals(INTERNATIONAL)) { %>
		<td colspan="2">&nbsp;</td>
<% } else {     %>
		<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td width="115" class="sectionsubhead"><%= LABEL_DEBIT_CURRENCY %></td>
					<td width="220" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}"/>
						<% String debitCurrency = ""; %>
						<ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}" assignTo="debitCurrency"/>
						<ffi:setProperty name="DebitCurrencyType" value="<%=debitCurrency%>"/>
					</td>
				</tr>
				<tr>
					<td class="sectionsubhead" nowrap><%= LABEL_EXCHANGE_RATE %></td>
					<td class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ExchangeRate"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_MATH_RULE %></td>
					<td class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="MathRule"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_CONTRACT_NUMBER %></td>
					<td class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ContractNumber"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table width="715" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="tbrd_b sectionhead">&gt; <!--L10NStart-->Payment Info<!--L10NEnd--></td>
	</tr>
	<tr>
		<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td>
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td width="115" class="sectionsubhead"><%= LABEL_PAYMENT_CURRENCY %></td>
					<td width="228" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="PayeeCurrencyType"/></td>
				</tr>
			</table>
		</td>
<% } %>
	</tr>
</table> --%>
<div class="blockWrapper">
			<div  class="blockHead">
				<%= wireType.equals(INTERNATIONAL) ? "<!--L10NStart-->Sender's Reference<!--L10NEnd-->" : "<!--L10NStart-->Reference for Beneficiary<!--L10NEnd-->" %>: 
				<ffi:getProperty name="<%= task %>" property="Comment"/>
			</div>
			<div  class="blockHead"><!--L10NStart-->Originator to Beneficiary Information<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
					<% String info = ""; %>
					<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo1"/>
					<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2" assignTo="info"/>
					<% if (info != null && info.length() > 0) { %>
						<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2"/>
					<% } %>
					<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3" assignTo="info"/>
					<% if (info != null && info.length() > 0) { %>
						<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3"/>
					<% } %>
					<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4" assignTo="info"/>
					<% if (info != null && info.length() > 0) { %>
						<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4"/></td>
					<% } %>
				</div>
			</div>
			<% if (!wireType.equals(BOOK)) { %>
				<div  class="blockHead"><!--L10NStart-->Bank to Bank Information<!--L10NEnd--></div>
				<div class="blockContent">
					<div class="blockRow">
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo1"/>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo2" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo2"/>
						<% } %>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo3" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo3"/>
						<% } %>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo4" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo4"/>
						<% } %>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo5" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo5"/>
						<% } %>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo6" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo6"/></td>
						<% } %>
					</div>
				</div>
			<% } %>
</div>
							<%-- <table width="715" border="0" cellspacing="0" cellpadding="3">
								<tr>
									<td class="tbrd_b"><span class="sectionhead">&gt; <%= wireType.equals(INTERNATIONAL) ? "<!--L10NStart-->Sender's Reference<!--L10NEnd-->" : "<!--L10NStart-->Reference for Beneficiary<!--L10NEnd-->" %></span></td>
								</tr>
								<tr>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="Comment"/></td>
								</tr> 
								<tr>
									<td class="tbrd_b"><span class="sectionhead">&gt; <!--L10NStart-->Originator to Beneficiary Information<!--L10NEnd--></span></td>
								</tr>
								<tr>
									<td class="columndata">
									<% String info = ""; %>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo1"/>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4"/></td>
									<% } %>
								</tr>
								<% if (!wireType.equals(BOOK)) { %>
								<tr>
									<td class="tbrd_b"><span class="sectionhead">&gt; <!--L10NStart-->Bank to Bank Information<!--L10NEnd--></span></td>
								</tr>
								<tr>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="BankToBankInfo1"/>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo2" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo2"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo3" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo3"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo4" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo4"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo5" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo5"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo6" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo6"/></td>
									<% } %>
								</tr>
								<% } %>
							</table>--%>

<ffi:cinclude value1="<%= wire.getWireDestination() %>" value2="<%= WireDefines.WIRE_FED %>" operator="equals">
	<ffi:cinclude value1="<%= wire.getAddendaType() %>" value2="" operator="notEquals">
		<div class="blockWrapper">
			<div  class="blockHead"><!--L10NStart-->Addenda<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
							<span class="sectionsubhead sectionLabel">Addenda Type: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="AddendaType"/> - <ffi:getProperty name="<%= task %>" property="AddendaTypeDisplayText"/></span>
				</div>
				<div class="blockRow">
					<span class="sectionsubhead sectionLabel">Addenda: </span>
					<% String addendaString = ""; %>
                     <ffi:getProperty name="<%= task %>" property="AddendaForDisplay" assignTo="addendaString"/>
                     <%
                         if( addendaString != null ) {
                             addendaString = com.ffusion.util.HTMLUtil.encode( addendaString );

                                 // convert all line separator characters to html newline i.e <br>
                             addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "\n", "<br>");
                             // convert all MULTIPLE text spaces to HTML non-breaking spaces i.e &nbsp;
                             addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "  ", "&nbsp;&nbsp;");
                             // in case there was an odd amount of multiple spaces, convert to double space
                             addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "&nbsp; ", "&nbsp;&nbsp;");
                         }
                     %>
					<span class="columndata"><%= addendaString %></span>
				</div>				
			</div>
		</div>
 	</ffi:cinclude>
</ffi:cinclude>
<div class="blockWrapper">
			<div  class="blockHead"><!--L10NStart-->By Order Of Information<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BOO_NAME %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfName"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BOO_ADDRESS_1 %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress1"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BOO_ADDRESS_2 %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress2"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BOO_ADDRESS_3 %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress3"/></span>
					</div>
				</div>
				<div class="blockRow">
							<span class="sectionsubhead sectionLabel"><%= LABEL_BOO_ACCOUNT_NUMBER %>: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAccount"/></span>
				</div>
			</div>
</div>
<ffi:cinclude value1="${templateTask}" value2="edit" operator="equals">
<div class="blockWrapper">
			<div  class="blockHead"><!--L10NStart-->Template Lifecycle<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
							<span class="sectionsubhead sectionLabel">Current Status: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="StatusName"/></span>
					</div>
					<div class="inlineBlock">
							<span class="sectionsubhead sectionLabel">Added Comment: </span>
							<span class="columndata"><ffi:getProperty name="<%= task %>" property="TemplateComment"/></span>
					</div>
				</div>
			</div>
</div>
</ffi:cinclude>
<div class="btn-row">
	<% if (wireIsTemplate) { %>
		<%--
		<form action="<ffi:urlEncrypt url="${wire_confirm_form_url}" />" method="post" name="TransferNew">
            	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		--%>
		<ffi:setProperty name="wire_confirm_form_url" value="addWireTransferAction_autoentitle"/>
		<s:hidden name="wire_confirm_form_url" value="%{#session.wire_confirm_form_url}" id="wire_confirm_form_url"/>
		<% } else { %>
		
		<%--
		<form action="wiretransfersend.jsp" method="post" name="TransferNew">
		--%>
		<ffi:setProperty name="wire_confirm_form_url" value="${operation_action}"/>
		<% } %>
		<s:form id="WireTransferNewConfirmFormID" namespace="/pages/jsp/wires" action="%{#attr.SubmitAction}" method="post" name="TransferNew" theme="simple">
				
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" id="isManagePayeeModified" name="isManagePayeeModified" value="<ffi:getProperty name="isManagePayeeModified"/>"/>
<input type="hidden" name="isRecModel" value="<ffi:getProperty name='isRecModel'/>"/>
<input type="hidden" name="recurringId" value="<ffi:getProperty name='recurringId'/>"/>
<input type="hidden" name="ID" value="<ffi:getProperty name='ID'/>"/>
			<%--
			<input type="Button" value="CANCEL" class="submitbutton" onclick="document.location='<ffi:getProperty name="wireBackURL"/>'">
			<input type="Button" value="BACK" class="submitbutton" onClick="javascript:document.location='<ffi:getProperty name="BackURL"/>'">
			--%>
			<script>
				ns.wire.cancelWireURLFromWireBatch = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/${wireBackURL}"/>' ;
			</script>
			<s:if test="%{isWireBatch}">
				<s:hidden id="backToBatchURLForJS" value="%{#session.wireBackURL}"></s:hidden>
							<sj:a 
					button="true" 
					onClickTopics="cancelAddWireOnBatchFormClickTopics"
				>
					<s:text name="jsp.default_82"/>
				</sj:a>
				</s:if>
				<s:else>
			   <ffi:cinclude value1="${wireSource}" value2="TEMPLATE">
					<sj:a id="cancelFormButtonOnVerify" 
						button="true" summaryDivId="summary" buttonType="cancel"
						onClickTopics="showSummary,cancelWireTransferForm"
						>
						<s:text name="jsp.default_82"/>
					</sj:a>		
				</ffi:cinclude>
				<ffi:cinclude value1="${wireSource}" value2="TEMPLATE" operator="notEquals">
					<sj:a id="cancelFormButtonOnVerify" 
						button="true"  summaryDivId="summary" buttonType="cancel"
						onClickTopics="showSummary,cancelWireTransferForm"
						>
						<s:text name="jsp.default_82"/>
					</sj:a>									
				</ffi:cinclude>
			
			</s:else>
			<sj:a id="backFormButton" 
				  button="true" 
				  onClickTopics="backToInput"
			>
				<s:text name="jsp.default_57"/>
			</sj:a>
			<% if (wireIsTemplate) { %>
			<%--<input type="submit" class="submitbutton" name="submitButtonName" value="<ffi:getProperty name="wire_confirm_button_label"/>">--%>
			<sj:a 											
				id="sendNewWireTransferSubmit"
				button="true" 
				onClickTopics="sendWireTemplateFormClick"
                      ><s:text name="jsp.default_111"/>
			</sj:a>
			<% } else { %>
			<%--
			<input type="submit" class="submitbutton" name="submitButtonName" value="CONFIRM/SUBMIT WIRE">
			--%>
			<s:if test="%{isWireBatch}">
			<s:if test="%{wireBatchUrl == 'modifyWireTransferAction.action'}">
			<script>ns.wire.sendNewWireTransferOnBatchClickUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/modifyWireTransferAction_batch.action?ignoreMfa=true'/>";</script>
			</s:if>
			<s:if test="%{wireBatchUrl == 'addWireTransferAction.action'}">
			<script>ns.wire.sendNewWireTransferOnBatchClickUrl = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/addWireTransferAction_batch.action?ignoreMfa=true'/>";</script>
			</s:if>
			
			<sj:a 											
				id="sendNewWireTransferOnBatchSubmit"
				button="true" 
				onClick="sendNewWireTransferOnBatchClick(ns.wire.sendNewWireTransferOnBatchClickUrl);"
                      ><s:text name="jsp.default_107" />
			</sj:a>
			</s:if>
			<s:else>
			
			<sj:a 											
				id="sendNewWireTransferSubmit"
				formIds="WireTransferNewConfirmFormID"
				targets="confirmDiv" 
				button="true" 
				onBeforeTopics="beforeSendWireTransferForm"
				onSuccessTopics="sendWireTransferFormSuccess"
				onErrorTopics="errorSendWireTransferForm"				
                      ><s:text name="jsp.default_395" />
			</sj:a>
			</s:else>
			<% } %>
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE %>">
			<s:if test="%{!isWireBatch}">
			<% if (!wireIsTemplate) { %>
				<ffi:setProperty name="saveTemplateButtonDisabled" value=""/>
				<%-- Start: Dual approval processing --%>
				<%--
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<ffi:cinclude value1="${WireTransferPayee.PayeeScope}" value2="<%=WireDefines.PAYEE_SCOPE_UNMANAGED%>">
						<ffi:setProperty name="saveTemplateButtonDisabled" value="disabled"/>
					</ffi:cinclude>
				</ffi:cinclude>
				--%>
				<%-- End: Dual approval processing --%>


				<%--<input type="Button" value="SAVE AS TEMPLATE" class="submitbutton" onclick="document.location='wiresaveastemplate.jsp';" <ffi:getProperty name="saveTemplateButtonDisabled"/>>--%>
				<%-- <sj:a 											
					id="saveAsTemplateSubmit"
					button="true" 
					onClickTopics="saveAsTemplateSubmitOnClickTopics"
					>Save As Template
				</sj:a> --%>

				<ffi:removeProperty name="saveTemplateButtonDisabled"/>												
			<% } %>
			</s:if>
			</ffi:cinclude>
	</s:form>
	</div></div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-positive").addClass("icon-negative")
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-positive").removeClass("icon-negative")
			}
		});
	});

</script>
