<%--
This page is used to add or edit a REGULAR wire transfer beneficiary.  BOOK
beneficiaries use wireaddpayeebook.jsp and DRAWDOWN beneficiaries use
wireaddpayeedrawdown.jsp

Pages that request this page
----------------------------
wirepayee.jsp (wire beneficiary summary page)
	ADD BENEFICIARY button, or by clicking the edit icon next to a REGULAR
	beneficiary.
wiretransfernew.jsp (add/edit domestic and international wire)
	MANAGE BENEFICIARY button
wirefed.jsp (add/edit fed wire)
	MANAGE BENEFICIARY button

Pages this page requests
------------------------
CANCEL requests one of the following:
	wirepayee.jsp
	wiretransfernew.jsp
SAVE BENEFICIARY requests wirepayeeaddconfirm.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
payments/inc/wire_common_payee_init.jsp
	Initialization common to all add/edit wire beneficiary pages
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/wire_scripts_payee_js.jsp
	Javascript functions common to all add/edit wire beneficiary pages
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_addedit_payee_fields.jsp
	The beneficiary name and address fields - shared by all add/edit wire
	transfer and all add/edit wire beneficiary pages.
payments/inc/wire_addedit_bank_fields.jsp
	The beneficiary bank name and address fields and SEARCH button - shared by
	all add/edit wire transfer and all add/edit wire beneficiary pages.
payments/inc/wire_addedit_intermediary_banks.jsp
	The intermediary bank list and ADD INTERMEDIARY BANK button - shared by all
	add/edit wire transfer and all add/edit wire beneficiary pages.
payments/inc/wire_addedit_payee_comment.jsp
	The beneficiary comment field - shared by all add/edit wire beneficiary
	pages. (Not used by add/edit wire transfers.)
--%>

<%@ page import="com.ffusion.beans.wiretransfers.WireDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="/pages/jsp/common/wire_labels.jsp"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wireaddpayee" className="moduleHelpClass"/>
<ffi:removeProperty name="isBeneficiary"/>
<ffi:removeProperty name="IsPending"/>

<span class="shortcutPathClass" style="display:none;">pmtTran_wire,beneficiaryLink,createBeneficiaryLink</span>
<% if (request.getParameter("Initialize") != null) { %>
<ffi:removeProperty name="ID"/>
<ffi:removeProperty name="returnPage"/>
<ffi:removeProperty name="benficiaryType"/>
<%}%>
<%
String validation_action ="";
%>

 
 <ffi:getProperty name="validateAction" assignTo="validation_action"/>

<ffi:setProperty name="PageHeading" value="Add Wire Beneficiary"/>
<ffi:cinclude value1="${validateAction}" value2="addWireBeneficiaryAction_verify" operator="notEquals">
	<ffi:setProperty name="PageHeading" value="Edit Wire Beneficiary"/>
</ffi:cinclude>
<span id="PageHeading" style="display:none;"><ffi:getProperty name='PageHeading'/></span>
<ffi:setProperty name="DimButton" value="beneficiary"/>
<%--<ffi:include page="${PathExt}payments/inc/wire_buttons.jsp"/>

<ffi:setProperty name="BackURL" value="${SecurePath}wires/wireaddpayee.jsp" />
<ffi:setProperty name="CallerForm" value="WireNew"/>
<ffi:setProperty name="CallerURL" value="wireaddpayee.jsp"/>

The following processing retrieves a list of valid States based
on the Country chosen. This was done for localiztion.
--%>
<%
	String selectedDBBLCountry = "";
	String selectedDBISOCountry = "";
%>



<s:include value="%{#session.PagesPath}/wires/inc/wire_scripts_payee_js.jsp"/>
<%--<ffi:include page="${PathExt}payments/inc/nav_menu_top_js.jsp" />--%>

		<div align="center" id="wireBeneficiesNewFormID">
			<%-- include page header --%>
			<%--<ffi:include page="${PathExt}payments/inc/nav_header.jsp" />--%><ul id="formerrors"></ul>
		
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="ltrow2_color" align="center">
						<s:form name="WireNew" id="newBeneficiaryFormID" namespace="/pages/jsp/wires"  validate="false"  
								action="%{#attr.validation_action}" method="post" theme="simple">
						<%--<form action="wirepayeeaddconfirm.jsp" method="post" name="WireNew"> --%>
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<input type="hidden" name="delIntBank" value=""/>
                    	<input type="hidden" name="selectedCountry" value=""/>
                    	<input type="hidden" name="validateAction" value="<ffi:getProperty name='validateAction'/>"/>
                    	<input type="hidden" id="manageBeneficiary" name="manageBeneficiary" value="<ffi:getProperty name='manageBeneficiary'/>"/>
                    	<input type="hidden" name ="returnPage" id="returnPage" value="<ffi:getProperty name='returnPage'/>"/>
                    	
                    	<s:if test="%{validateAction == 'addWireBeneficiaryAction_verify'}">
                    		<input type="hidden" name ="bankLookupRaturnPage" id="bankLookupRaturnPage" value="addWireBeneficiaryAction_preProcess.action"/>
                    	</s:if>
                    	<s:else>
                    		<input type="hidden" name ="bankLookupRaturnPage" id="bankLookupRaturnPage" value="modifyWirebeneficiary_preProcess.action"/>
                    	</s:else>
                    	
						<s:hidden name="IsPending" value="%{#session.IsPending}" id="IsPending"/>
						<input type="hidden" name="<ffi:getProperty name="payeeTask"/>.ID" value="<ffi:getProperty name="${payeeTask}" property="ID"/>"/>
						
<table cellpadding="0" cellspacing="0" border="0" align="left" width="100%">
<tr><td class='sectionhead nameTitle' style='cursor:auto'><h2>Beneficiary Info</h2></td></tr>
									<tr>
										<td valign="top">
<%-- ------ PAYEE FIELDS: PayeeName, Street, Street2, City, State, ZipCode, Country, ContactName, PayeeScope, AccountNum, AccountType ------ --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_fields.jsp"/>
										</td>
									</tr>
									<tr><td valign="top"><hr class="thingrayline lineBreak" /></td></tr>
									<tr>
										<td width="348" valign="top">

<%-- Start: Dual approval processing --%>

<%-- End: Dual approval processing --%>
	
<%-- ------ BENEFICIARY BANK FIELDS:  BankName, Street, Street2, City, State, ZipCode, Country, RoutingFedWire, RoutingSwift, RoutingChips, RoutingOther, IBAN, Correspondent Bank Account Number ------ --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_bank_fields.jsp"/>
													</td>
												</tr>
								</table>
								<span class="columndata">&nbsp;<br></span>
<%-- ------ INTERMEDIARY BANKS LIST ------ --%>
<div id="intermediaryBanksDivID">
<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_intermediary_banks.jsp"/>
<span class="columndata">&nbsp;<br></span>
</div>
<%-- ------ PAYEE COMMENTS ------ --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_comment.jsp"/>


								<span class="columndata">&nbsp;<br>&nbsp;<br></span>
								<input type="hidden" id="returnPageIDForJS" value="<ffi:getProperty name='returnPage'/>"/>
                    			<s:hidden name="isBeneficiary" value="true" id="isBeneficiary"/>
								<s:hidden name="IsPending" value="%{#session.IsPending}" id="IsPending"/>
                    			<%--if isHaveReturnPageParamIDForJS is not null, it means this page from click manage beneficiary button from wire or template --%>
                    			
								<div align="center">
								<sj:a id="cancelFormButtonOnInput" 
									button="true" summaryDivId="summary" buttonType="cancel" 
									onClickTopics ="showSummary"
									onclick="ns.wire.cancelWireBeneficiaryFormOnClick()"
								><s:text name="jsp.default_82" /><!-- CANCEL -->
								</sj:a>
								<sj:a 
												id="verifyBeneficiarySubmit"
												formIds="newBeneficiaryFormID"
				                                targets="verifyDiv" 
				                                button="true" 
				                                validate="true" 
				                                validateFunction="customValidation"
				                                onBeforeTopics="beforeVerifyBeneficiaryForm"
				                                onCompleteTopics="verifyBeneficiaryFormComplete"
												onErrorTopics="errorVerifyBeneficiaryForm"
				                                onSuccessTopics="successVerifyBeneficiaryForm"
						                        ><s:text name="jsp.default_395" /><!-- SAVE BENEFICIARY -->
						            </sj:a>
						            </div>
								</s:form>
							<%--</form> --%>
					</td>
					<td>&nbsp;</td>
				</tr>
			</table>
				<br>
			
		</div>


