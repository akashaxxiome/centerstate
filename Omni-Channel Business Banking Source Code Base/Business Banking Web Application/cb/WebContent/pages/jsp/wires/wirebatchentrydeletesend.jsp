<%@ page import="com.ffusion.beans.wiretransfers.WireTransfer,
                 com.ffusion.beans.wiretransfers.WireTransfers,
                 com.ffusion.beans.wiretransfers.WireBatch,
   				       com.ffusion.beans.wiretransfers.WireDefines"%>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="${wireBatch}" value2="ModifyWireBatch" operator="notEquals">
	<ffi:setProperty name="AddWireBatch" property="DeleteWireFromBatch" value="${wireIndex}"/>
	<%-- Go back to wire batch add page --%>
	<s:include value="/pages/jsp/wires/wirebatchff.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireBatch}" value2="ModifyWireBatch" operator="equals">
	<ffi:setProperty name="ModifyWireBatch" property="DeleteWireFromBatch" value="${wireIndex}"/>
	<%-- Go back to wire batch edit page --%>
	<s:include value="/pages/jsp/wires/wirebatcheditff.jsp"/>
</ffi:cinclude>

<%--<script>location.replace("<ffi:getProperty name="wireBackURL"/>");</script>--%>
