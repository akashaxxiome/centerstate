<%--
This is a host wire transfer confirm page. All other wire types use
wiretransferconfirm.jsp

Pages that request this page
----------------------------
wirehost.jsp

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
BACK requests wirehost.jsp
SUBMIT WIRE requests wirehostsend.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_wirehostconfirm" className="moduleHelpClass"/>
<ffi:setProperty name='PageText' value=''/>

<%
String task = "wireTransfer";
%>

<div align="center" id="hostWiresVerificationTableID">
<span id="wireTransferResultMessage" style="display:none">Your host wire has been saved.</span>
 <%-- Show them warnings or notes --%>
    <% String dateChanged = ""; %>
    <ffi:getProperty name="DateChanged" assignTo="dateChanged"/>

    <% String nonBizDay = ""; %>
    <ffi:getProperty name="NonBusinessDay" assignTo="nonBizDay"/>

    <% if ("true".equals(dateChanged) && "true".equals(nonBizDay)) { %>
        <%-- Both proc window is closed and tomorrow is non-biz day --%>
        <div class="sectionsubhead" style="color:red" align="center">
            <!--L10NStart-->NOTE: The Host wire processing window for today has closed,
            so the Requested Processing Date has been changed to tomorrow's date.
            Since tomorrow is not a business day, the Expected Processing Date has been changed
            to the next business day.<!--L10NEnd--><br><br>
       </div>
     <% } else if ("true".equals(dateChanged)) { %>
         <%-- DueDate changed because processing window is closed --%>
         <div class="sectionsubhead" style="color:red" align="center">
             <!--L10NStart-->NOTE: The Host wire processing window for today has closed.
             The Requested Processing Date has been changed to tomorrow's date.<!--L10NEnd--><br><br>
         </div>
     <% } else if ("true".equals(nonBizDay)) { %>
         <%-- DueDate changed because its not a business day --%>
         <div class="sectionsubhead" style="color:red" align="center">
             <!--L10NStart-->NOTE: The Requested Processing Date is not a business day.
             The Expected Processing Date has been changed to the next business day.<!--L10NEnd--><br><br>
         </div>
<% } %>

<div class="blockWrapper hostWireTransfer">
	<div  class="blockHead"><!--L10NStart-->Wire Transfer Summary<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Host ID<!--L10NEnd-->: </span>
                <span class="columndata"><ffi:getProperty name="<%= task %>" property="HostID"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Requested Processing Date<!--L10NEnd-->: </span>
                <span class="columndata"><ffi:getProperty name="<%= task %>" property="DueDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Amount<!--L10NEnd-->: </span>
                    <span class="columndata"><ffi:getProperty name="<%= task %>" property="Amount"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Settlement Date<!--L10NEnd-->: </span>
                <span class="columndata"><ffi:getProperty name="<%= task %>" property="SettlementDate"/></span>
			</div>
		</div>
		<div class="blockRow">
                 <span class="sectionsubhead sectionLabel"><!--L10NStart-->Expected Processing Date<!--L10NEnd-->: </span>
                 <span class="columndata"><ffi:getProperty name="<%= task %>" property="DateToPost"/></span>
		</div>
	</div>
</div>

<div class="btn-row"/>
<s:form id="WireTransferNewConfirmFormID" namespace="/pages/jsp/wires" action="%{#attr.SubmitAction}" method="post" name="TransferNew" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden name="initSaveAsTemplateAction" value="addWireTransferAction_initSaveAsTemplate.action" id="initSaveAsTemplateAction"></s:hidden>
	<sj:a id="cancelFormButtonOnVerify" 
		button="true" summaryDivId="summary" buttonType="cancel"
		onClickTopics="showSummary,cancelWireTransferForm"
	><s:text name="jsp.default_82" />
	</sj:a>
	<sj:a id="backFormButton" 
		button="true" 
		onClickTopics="backToInput"
	><s:text name="jsp.default_57" />
	</sj:a>
	<sj:a id="sendNewWireTransferSubmit"
		formIds="WireTransferNewConfirmFormID"
		targets="confirmDiv" 
		button="true" 
		onBeforeTopics="beforeSendWireTransferForm"
		onSuccessTopics="sendWireTransferFormSuccess"
		onErrorTopics="errorSendWireTransferForm"
		onCompleteTopics="sendWireTransferFormComplete"
	><s:text name="jsp.default_395" />
		
	</sj:a>
</s:form>
</div>
            
<table width="750" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="lightBackground" align="center">
<%-- CONTENT START --%>
            
<%-- CONTENT END --%>
		</td>
	</tr>
</table>

