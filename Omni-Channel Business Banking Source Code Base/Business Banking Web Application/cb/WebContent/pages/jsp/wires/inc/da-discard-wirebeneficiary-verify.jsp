<%--
This page is a dialog page for discarding wire beneficiary pending changes.

Pages that request this page
----------------------------
Discard and Cancel button

Pages this page requests
------------------------
DONE button requests wire_beneficiary_summary.jsp

Pages included in this page
---------------------------

--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<ffi:setL10NProperty name='PageHeading' value='Verify Discard Wire Beneficiary'/>
<ffi:help id="payments_wirebeneficiarydiscard"/>
<div class="sectionsubhead marginTop10" align="center"><s:text name="da.message.wirebeneficiary.discard"><s:param><s:property value="#request.nickname"/></s:param></s:text></div>
<div class="btn-row">
	<form method="post">
		<input type="hidden" name="hidden_CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		
		
	<s:url id="discardURL" escapeAmp="false" value="/pages/dualapproval/dualApprovalAction-discardChanges.action?daAction=Discarded&module=WireBeneficiary" >
		<s:param name="CSRF_TOKEN_param" value="%{#session.CSRF_TOKEN}"></s:param>
		<s:param name="itemType" value="%{#request.itemType}"></s:param>
		<s:param name="itemId" value="%{#request.itemId}"></s:param>															
	</s:url>
		<sj:a id="discard"
			button="true"
			title="Discard Beneficiary" 
			targets="resultmessage" 
			href="%{discardURL}"
			onClickTopics="discardWireBeneficiaryCompleteTopics" 
			onCompleteTopics="refreshWireBeneficiariesGridSuccessTopics" 
			onSuccessTopics="discardWireBeneficiarySuccessTopics"
			effectDuration="1500" 
			><s:text name="jsp.default_467"/>
		</sj:a>
		<sj:a id="noDiscard" button="true" title="No" onClickTopics="closeDialog"><s:text name="jsp.default_295"/></sj:a>
	</form>
</div>

