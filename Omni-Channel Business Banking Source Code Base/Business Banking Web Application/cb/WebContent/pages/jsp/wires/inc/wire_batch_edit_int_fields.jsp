<%--
This file contains the batch-specific fields for an international wire batch

It is included on all edit wire batch pages
	wirebatcheditff.jsp
	wirebatchedithost.jsp
	wirebatchedittemplate.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<script>
	$(function(){
		$("#OrigCurrencySelectID").selectmenu({width:'12em'});
		$("#PayeeCurrencyTypeSelectID").selectmenu({width:'12em'});
		$("#MathRuleSeletID").selectmenu({width:'12em'});
	});
</script>
<%
String typeHost = com.ffusion.beans.wiretransfers.WireDefines.WIRE_BATCH_TYPE_HOST;
String wireBatchPage = ""; String disableInt = ""; %>
<ffi:getProperty name="wireBatchObject" property="BatchType" assignTo="wireBatchPage"/>
<%
if (wireBatchPage == null) wireBatchPage = "";
if (wireBatchPage.equals(typeHost)) disableInt = "disabled";
%>

<div class="marginTop5 sectionsubhead"><!--L10NStart--><label for="batchName">Batch Name</label><!--L10NEnd--><span class="required" title="required">*</span></div>
<div class="marginTop5"><input type="Text" id="batchName" name="wireBatchObject.BatchName" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="wireBatchObject" property="BatchName"/>" size="30"></div>
<div class="marginTop5 sectionsubhead"><!--L10NStart-->Application Typess<!--L10NEnd--><span class="required" title="required">*</span></div>
<div class="marginTop5 columndata"><ffi:getProperty name="wireBatchObject" property="BatchType"/></div>
<div class="marginTop5 sectionsubhead"><!--L10NStart--><label for="Date">Value Date</label><!--L10NEnd--><span class="required" title="required">*</span></div>
<div class="marginTop5" ><%-- <input type="Text" name="wireBatchObject.DueDate" class="txtbox" value="<ffi:getProperty name="wireBatchObject" property="DueDate"/>" size="11">
	<a onclick="popCalendar('wireBatchObject.DueDate');return false;" href="#"><img src="/cb/web/multilang/grafx/i_calendar.gif" alt="" width="19" height="16" align="absmiddle" border="0"></a>--%>
	<ffi:setProperty name="dueDateForDP" value="${wireBatchObject.DueDate}"/>
	<sj:datepicker value="%{#session.dueDateForDP}" id="Date" name="wireBatchObject.DueDateString" label="Date" maxlength="10" buttonImageOnly="true" minDate="0" cssStyle="width:110px" cssClass="ui-widget-content ui-corner-all"/>
	
</div>
<div class="marginTop5"><span id="DueDateError"></span></div>
<div class="marginTop5 sectionsubhead"><!--L10NStart--><label for="SettlementDate">Settlement Date</label><!--L10NEnd--></div>
<div class="marginTop5" ><%-- <input type="Text" name="wireBatchObject.SettlementDate" class="txtbox" value="<ffi:getProperty name="wireBatchObject" property="SettlementDate"/>" size="11">
	<a onclick="popCalendar('wireBatchObject.SettlementDate');return false;" href="#"><img src="/cb/web/multilang/grafx/i_calendar.gif" alt="" width="19" height="16" align="absmiddle" border="0"></a>--%>
	<ffi:setProperty name="settlementDateForDP" value="${wireBatchObject.SettlementDate}"/>
	<sj:datepicker value="%{#session.settlementDateForDP}" id="SettlementDate" name="wireBatchObject.SettlementDateString" label="Date"
							maxlength="10" buttonImageOnly="true" minDate="0" cssClass="ui-widget-content ui-corner-all" cssStyle="width:110px"/>
	<script>
	  $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>'); 
	</script>
</div>
<div class="marginTop5"><span id="SettlementDateError"></span></div>
<% String curType = ""; String thisType = ""; %>
<div class="marginTop5 sectionsubhead"><!--L10NStart--><label for="OrigCurrencySelectID">Debit Currency</label><!--L10NEnd--></div>
<div class="marginTop5">
	<select id="OrigCurrencySelectID" class="txtbox" onchange="refreshCurrencyMismatches();" name="wireBatchObject.OrigCurrency" <%= disableInt %>>
		<s:iterator value="currencies" var="currencyType"> 
			<option value="<s:property  value="CurrencyCode"/>"   <s:if test="%{WireBatchObject.OrigCurrency == CurrencyCode}">selected</s:if>  ><s:property  value="CurrencyCode"/></option>
		</s:iterator>
	</select>
</div>
<div class="marginTop5 sectionsubhead"><!--L10NStart--><label for="PayeeCurrencyTypeSelectID">Payment Currency</label><!--L10NEnd--></div>
<div class="marginTop5" >
	<select id="PayeeCurrencyTypeSelectID" class="txtbox"  onchange="refreshCurrencyMismatches();" name="wireBatchObject.PayeeCurrencyType" <%= disableInt %>>
		<s:iterator value="currencies" var="currencyType"> 
			<option value="<s:property  value="CurrencyCode"/>"   <s:if test="%{WireBatchObject.PayeeCurrencyType == CurrencyCode}">selected</s:if>  ><s:property  value="CurrencyCode"/></option>
		</s:iterator>
	</select>
</div>
<div class="marginTop5 sectionsubhead"><!--L10NStart--><label for="exchangeRate">Exchange Rate</label><!--L10NEnd--></div>
<div class="marginTop5" ><input type="Text" id="exchangeRate" name="wireBatchObject.ExchangeRate" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="wireBatchObject" property="ExchangeRate"/>" size="7" <%= disableInt %>></div>
<div class="marginTop5 sectionsubhead"><!--L10NStart--><label for="MathRuleSeletID">Math Rule</label><!--L10NEnd--></div>
<div class="marginTop5" ><select id="MathRuleSeletID" name="wireBatchObject.MathRule" class="txtbox" <%= disableInt %>>
<ffi:getProperty name="wireBatchObject" property="MathRule" assignTo="curType"/>
<% if (curType == null) curType = ""; %>
	<option <%= curType.equals("MULT") ? "selected" : "" %>><!--L10NStart-->MULT<!--L10NEnd--></option>
	<option <%= curType.equals("DIV") ? "selected" : "" %>><!--L10NStart-->DIV<!--L10NEnd--></option>
</select></div>
<div class="marginTop5 sectionsubhead"><!--L10NStart--><label for="contractNumber">Contract Number</label><!--L10NEnd--></div>
<div class="marginTop5"><input type="Text" id="contractNumber" name="wireBatchObject.ContractNumber" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="wireBatchObject" property="ContractNumber"/>" size="30" <%= disableInt %>></div>
