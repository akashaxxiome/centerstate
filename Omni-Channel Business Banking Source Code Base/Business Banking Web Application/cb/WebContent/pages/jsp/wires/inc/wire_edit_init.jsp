<%--
This file contains initialization code used on all wire transfer pages specific
to editing a wire transfer (not in a batch)

It is included in wire_common_init.jsp, which is included on all add/edit wire
transfer pages

It includes the following files:
common/wire_labels.jsp
	The labels for fields and buttons
--%>
<%@ page import="com.ffusion.tasks.wiretransfers.ModifyWireTransfer"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../common/wire_labels.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%
String templateTask = (String)session.getAttribute("templateTask");
if (templateTask == null) templateTask = "";
if (!(templateTask.equals("add") || templateTask.equals("edit"))) templateTask = "";
%>

<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
	<ffi:setProperty name="wireBackURL" value="wiretransfers.jsp?Refresh=true" URLEncrypt="true"/>
	<ffi:setProperty name="wireSaveButton" value="<%= BUTTON_SUBMIT_WIRE %>"/>
</ffi:cinclude>

<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
	<ffi:setProperty name="wireBackURL" value="wiretemplates.jsp"/>
	<ffi:setProperty name="wireSaveButton" value="<%= BUTTON_SUBMIT_TEMPLATE %>"/>
</ffi:cinclude>

<ffi:setProperty name="GetWireTransferPayees" property="Reload" value="true"/>
<ffi:process name="GetWireTransferPayees"/>

<%-- Set the wire into the session --%>
<%--
<ffi:include page="${PathExt}payments/inc/wire_set_wire.jsp"/>
--%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_set_wire.jsp"/>
<ffi:removeProperty name="pendApproval"/>

<ffi:object id="ModifyWireTransfer" name="com.ffusion.tasks.wiretransfers.ModifyWireTransfer" scope="session"/>
	<ffi:setProperty name="ModifyWireTransfer" property="Initialize" value="true"/>
<ffi:process name="ModifyWireTransfer"/>
<ffi:setProperty name="ModifyWireTransfer" property="DateFormat" value="${UserLocale.DateFormat}"/>
<%
	session.setAttribute("FFIModifyWireTransfer", session.getAttribute("ModifyWireTransfer"));
%>

<ffi:object id="GetPayeeFromWireTransfer" name="com.ffusion.tasks.wiretransfers.GetPayeeFromWireTransfer" scope="session"/>
	<ffi:setProperty name="GetPayeeFromWireTransfer" property="WireSessionName" value="ModifyWireTransfer"/>
	<ffi:setProperty name="GetPayeeFromWireTransfer" property="PayeeSessionName" value="WireTransferPayee"/>
<ffi:process name="GetPayeeFromWireTransfer"/>

<ffi:setProperty name="disableRec" value="disabled"/>
<ffi:setProperty name='freqNone' value='<option value="0"><!--L10NStart-->None<!--L10NEnd--></option>'/>
<ffi:setProperty name="wireAmountField" value="OrigAmount"/>
<ffi:setProperty name="wireCurrencyField" value="OrigCurrency"/>

<%
Integer fundsType = new Integer(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER);
String ft = fundsType.toString();
%>
<ffi:cinclude value1="${ModifyWireTransfer.Type}" value2="<%= ft %>" operator="equals">
	<ffi:setProperty name="disableRec" value=""/>
	<ffi:setProperty name='freqNone' value=''/>
</ffi:cinclude>

<ffi:object id="RemoveIntermediaryBank" name="com.ffusion.tasks.wiretransfers.RemoveIntermediaryBank" scope="session" />
	<ffi:setProperty name="RemoveIntermediaryBank" property="BankCollectionSessionName" value="WireTransferPayee.IntermediaryBanks"/>
	

