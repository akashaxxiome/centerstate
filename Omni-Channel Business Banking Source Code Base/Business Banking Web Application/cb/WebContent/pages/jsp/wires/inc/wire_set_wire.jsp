<%--
This is a common file used by many pages in wires to set a wire transfer or
wire batch into the session given it's ID.  If a string called wireInBatch is
in the session and set to "true", or if a string called collectionName is
in the session and set to "WireRelease", then it will use the SetWireTransfer
task.  Otherwise, it will use SetWireHistory.

It is called from the following files:
	wirebatchdelete.jsp
	wirebatchedit.jsp
	wirebatcheditff.jsp
	wirebatchedithost.jsp
	wirebatchintdelete.jsp
	wirebatchintview.jsp
	wirebatchview.jsp
	wirebookview.jsp
	wiredrawdownview.jsp
	wirefedview.jsp
	wirehost.jsp
	wirehostdelete.jsp
	wirehostview.jsp
	wiretransferview.jsp
	payments/inc/wire_delete_common.jsp
	payments/inc/wire_edit_init.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- If this is NOT a wire in a batch (it's either a single wire or a batch)... --%>
<ffi:setProperty name="useSetWireHistory" value="true"/>

<%-- For deleting wire template transfer if there is no approval workflow then do this. --%>
<ffi:cinclude value1="${collectionName}" value2="WireTemplatesBusiness">
	<ffi:setProperty name="useSetWireHistory" value="false"/>
</ffi:cinclude>

<%-- If this IS a wire in a batch... --%>
<ffi:cinclude value1="${wireInBatch}" value2="true" operator="equals">
	<ffi:setProperty name="useSetWireHistory" value="false"/>
</ffi:cinclude>

<%-- If this is a wire in the WiresRelease collection (which is WireTransfers, not WireHistories) ... --%>
<ffi:cinclude value1="${collectionName}" value2="WiresRelease" operator="equals">
	<ffi:setProperty name="useSetWireHistory" value="false"/>
</ffi:cinclude>

<%-- Execute this block if useSetWireHistory is true ... --%>
<ffi:cinclude value1="${useSetWireHistory}" value2="true" operator="equals">
    <ffi:object id="SetWireWistory" name="com.ffusion.tasks.wiretransfers.SetWireHistory" scope="session" />
    <ffi:setProperty name="SetWireWistory" property="CollectionSessionName" value="${collectionName}"/>
    <ffi:cinclude value1="${ID}" value2="" operator="notEquals">
        <ffi:setProperty name="SetWireWistory" property="ID" value="${ID}"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${ID}" value2="" operator="equals">
        <ffi:setProperty name="SetWireWistory" property="ID" value=""/>
        <ffi:setProperty name="SetWireWistory" property="TrackingID" value="${trackingID}"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${Date}" value2="" operator="notEquals">
        <ffi:setProperty name="SetWireWistory" property="Date" value="${Date}"/>
    </ffi:cinclude>
    <ffi:process name="SetWireWistory"/>
    <ffi:removeProperty name="SetWireWistory"/>

    <ffi:object id="GetWireHistoryDetails" name="com.ffusion.tasks.wiretransfers.GetWireHistoryDetails" scope="session" />
    <ffi:setProperty name="GetWireHistoryDetails" property="ViewHistory" value="${InViewHistory}"/>
    <ffi:process name="GetWireHistoryDetails"/>
    <ffi:removeProperty name="GetWireHistoryDetails"/>
</ffi:cinclude>

<%-- Execute this block if useSetWireHistory is false ... --%>
<ffi:cinclude value1="${useSetWireHistory}" value2="false" operator="equals">
    <ffi:object id="SetWireTransfer" name="com.ffusion.tasks.wiretransfers.SetWireTransfer" scope="session" />
    <ffi:setProperty name="SetWireTransfer" property="CollectionSessionName" value="${collectionName}"/>
    <ffi:cinclude value1="${ID}" value2="" operator="notEquals">
        <ffi:setProperty name="SetWireTransfer" property="ID" value="${ID}"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${ID}" value2="" operator="equals">
        <ffi:setProperty name="SetWireTransfer" property="ID" value=""/>
        <ffi:setProperty name="SetWireTransfer" property="TrackingID" value="${trackingID}"/>
    </ffi:cinclude>
    <ffi:process name="SetWireTransfer"/>
    <ffi:removeProperty name="SetWireTransfer"/>
</ffi:cinclude>

<ffi:removeProperty name="wireInBatch"/>
<ffi:removeProperty name="Date"/>
<ffi:removeProperty name="useSetWireHistory"/>