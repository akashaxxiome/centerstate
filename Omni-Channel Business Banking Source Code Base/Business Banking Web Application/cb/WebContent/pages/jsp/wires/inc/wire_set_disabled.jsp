<%--
This file sets the various disabled flags for the add/edit wire transfer pages,
based on whether your adding a new, editing an existing, adding a template,
adding from a template, etc.

It is included in wire_common_init.jsp, which in turn is included in all the
add/edit wire transfer pages.
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%-- <ffi:setProperty name="disableEdit" value=""/> --%>
<ffi:setProperty name="disableSemi" value=""/>
<ffi:setProperty name="disableRep" value=""/>
<ffi:setProperty name="disableBat" value=""/>
<ffi:setProperty name="disableDates" value=""/>

<% String wpid = ""; String cat = ""; String wpscope = ""; %>

<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="wpid"/>
<ffi:getProperty name="WireTransferPayee" property="PayeeScope" assignTo="wpscope"/>
<ffi:getProperty name="${wireTask}" property="WireCategory" assignTo="cat"/>

<%
if (wpid == null) wpid = "";
if (wpscope == null) wpscope = "";
if (cat == null) cat = "";
%>

<%-- Disable payee fields if a payee ID has been selected and the payee is not unmanaged --%>
<ffi:cinclude value1="<%= wpid %>" value2="" operator="notEquals">
	<ffi:cinclude value1="<%= wpid %>" value2="<%= WireDefines.WIRE_LOOKUPBOX_DEFAULT_VALUE%>" operator="notEquals">
		<ffi:cinclude value1="<%= wpid %>" value2="<%= WireDefines.CREATE_NEW_BENEFICIARY%>" operator="notEquals">
			<ffi:cinclude value1="<%= wpscope %>" value2="<%= WireDefines.PAYEE_SCOPE_UNMANAGED %>" operator="notEquals">
				<%-- <ffi:setProperty name="disableEdit" value="disabled"/> --%>
				<s:set var="disableEdit" value="'disabled'" />
			</ffi:cinclude>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<%-- If this wire has been created from a template (but isn't a template itself), disable the appropriate fields based on the wire's category --%>
<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
	<ffi:cinclude value1="<%= cat %>" value2="<%= WireDefines.WIRE_CATEGORY_REPETITIVE %>" operator="equals">
		<s:set var="disableEdit" value="'disabled'" />
		<ffi:setProperty name="disableSemi" value="disabled"/>
		<ffi:setProperty name="disableRep" value="disabled"/>
		<ffi:setProperty name="disableBat" value="disabled"/>
		<ffi:setProperty name="disableRec" value="disabled"/>
	</ffi:cinclude>
	<ffi:cinclude value1="<%= cat %>" value2="<%= WireDefines.WIRE_CATEGORY_SEMIREPETITIVE %>" operator="equals">
		<s:set var="disableEdit" value="'disabled'" />
		<ffi:setProperty name="disableSemi" value="disabled"/>
		<ffi:setProperty name="disableBat" value="disabled"/>
		<ffi:setProperty name="disableRec" value="disabled"/>
	</ffi:cinclude>
</ffi:cinclude>
