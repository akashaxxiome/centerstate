<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.wiretransfers.*" %>
<%@ page import="com.ffusion.util.HTMLUtil" %>
<%@ include file="../../common/wire_labels.jsp"%>

<ffi:help id="payments_wiretransfersend" className="moduleHelpClass"/>

<% String task = "wireTransfer"; String templateTask = ""; String batchTask = ""; %>
<%-- <ffi:getProperty name="wireTask" assignTo="task"/>
<ffi:getProperty name="templateTask" assignTo="templateTask"/>
<ffi:getProperty name="wireBatch" assignTo="batchTask"/> --%>
<%
//if (task == null) //task = "AddWireTransfer";
//if (templateTask == null) templateTask = "";
//if (!(templateTask.equals("add") || templateTask.equals("edit"))) templateTask = "";
//if (batchTask == null) batchTask = "AddWireBatch";
%>

<%--=================== PAGE SECURITY CODE - Part 1 Start ====================--%>
<%	session.setAttribute( "Credential_Op_Type", ""+ com.ffusion.beans.authentication.AuthConsts.OPERATION_TYPE_TRANSACTION) ; %>
<ffi:setProperty name="LoadURL" value="payments/inc/print_wiretransfersend.jsp"/>
<ffi:setProperty name="securityMessageKey" value="jsp/payments/security-check.jsp-6"/>
<ffi:setProperty name="securitySubMenu" value="${subMenuSelected}"/>
<ffi:setProperty name='securityPageHeading' value='${PageHeading}'/>

<% if (!(templateTask.trim().equals("")) || (batchTask.equals("AddWireBatch"))|| (batchTask.equals("ModifyWireBatch"))) { // no security check for templates & batches.... %>
<ffi:setProperty name="verified" value="${UserSecurityKey.ImmutableString}" />
<% } %>

<% if( !( session.getAttribute( "UserSecurityKey" ) instanceof com.ffusion.tasks.util.ImmutableString) )  { %>
	<script>
		//window.location = "<ffi:getProperty name="SecurePath"/>payments/security-check-wait.jsp";
	</script>
<%} %>
<ffi:cinclude value1="${UserSecurityKey.ImmutableString}" value2="${verified}" operator="notEquals">
	<script>
		//window.location = "<ffi:getProperty name="SecurePath"/>payments/security-check-wait.jsp";
	</script>
</ffi:cinclude>

<%--=================== PAGE SECURITY CODE - Part 1 End ======================--%>
	<ffi:removeProperty name="verified"/>

<%-- If this is a wire Template, set autoentitle on the task --%>
<%
if (templateTask.equals("add") || templateTask.equals("edit") ) {
%>
	<ffi:setProperty name="<%= task %>" property="AutoEntitleWireTransfer" value="${doAutoEntitle}"/>
<%
}
%>

<ffi:object id="LocalizeCountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="LocalizeCountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="LocalizeCountryResource" />
<ffi:object id="GetCodeForBankLookupCountryName" name="com.ffusion.tasks.util.GetCodeForBankLookupCountryName" scope="session"/>

<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>

<%--
###############################################################
#     If this is wire *is not* part of a batch, do this...    #
###############################################################
--%>
<ffi:cinclude value1="${wireTask}" value2="WireTransfer" operator="notEquals">

<%
com.ffusion.beans.wiretransfers.WireTransfer wire = (com.ffusion.beans.wiretransfers.WireTransfer)session.getAttribute(task);

String intFieldTask = task;
String wireType = "";

boolean wireIsInBatch = (task.equals("WireTransfer"));
boolean wireIsTemplate = (!templateTask.equals(""));
/* 
if (wire.getWireDestination().equals(WireDefines.WIRE_DOMESTIC)) wireType = DOMESTIC;
if (wire.getWireDestination().equals(WireDefines.WIRE_INTERNATIONAL)) wireType = INTERNATIONAL;
if (wire.getWireDestination().equals(WireDefines.WIRE_BOOK)) wireType = BOOK;
if (wire.getWireDestination().equals(WireDefines.WIRE_DRAWDOWN)) wireType = DRAWDOWN;
if (wire.getWireDestination().equals(WireDefines.WIRE_FED)) wireType = FED; */

%>

<ffi:getProperty name="<%= task %>" property="wireDestination" assignTo="wireType"/>
<ffi:setProperty name="PageText" value=""/>

		<div align="center">
			<table width="670" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tbrd_b">&nbsp;</td>
				</tr>
                <tr>
                    <td class="tbrd_lrb" width="670">
                        <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td align="left" colspan="4" height="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                        </tr>
                        <tr>
                            <td align="left" width="5" class="sectionsubhead">&nbsp;</td>
                            <td align="left" width="325" class="sectionsubhead"><%= wireType %> Wire Transfer Confirmation</td>
                            <td align="left" colspan="2" class="sectionsubhead">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="sectionsubhead">&nbsp;</td>
                            <td align="left" class="sectionsubhead"><ffi:getProperty name="SecureUser" property="BusinessName"/></td>
                            <td align="left" width="90" class="sectionsubhead"><%= LABEL_TRACKING_ID %>:</td>
                            <td align="left" width="215" class="sectionsubhead"><ffi:getProperty name="<%= task %>" property="TrackingID"/></td>
                        </tr>
                        <tr>
                            <td align="left" class="sectionsubhead">&nbsp;</td>
                            <td align="left" class="sectionsubhead">
                                <ffi:process name="GetCurrentDate"/>
                                <ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateTimeFormat}"/>
                                <ffi:getProperty name="GetCurrentDate" property="Date"/>
                            </td>
                            <td align="left"  class="sectionsubhead"><%= LABEL_APPLICATION_ID %>:</td>
                            <td align="left" class="sectionsubhead"><ffi:getProperty name="<%= task %>" property="ID"/></td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4" height="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                        </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="tbrd_lr">&nbsp;</td>
                </tr>
                <tr>
					<td class="tbrd_lr">
						<div align="center">
<% String wireSource = ""; %>
<ffi:getProperty name="<%= task %>" property="WireSource" assignTo="wireSource"/>
<% if (wireSource == null) wireSource = "";
   if (wireSource.equals("TEMPLATE") && !wireIsTemplate) { %>
<%-- ------ TEMPLATE (READ-ONLY) NAME, NICKNAME, ID, AND WIRE LIMIT ------ --%>
							<table border="0" cellspacing="0" cellpadding="2" width="635">
								<tr>
									<td align="left" class="tbrd_b sectionsubhead" colspan="4">&gt; <!--L10NStart-->Template Info<!--L10NEnd--></td>
								</tr>
								<tr>
									<td align="left" width="120" class="sectionsubhead"><%= LABEL_TEMPLATE_NAME %></td>
									<td align="left" width="210" class="columndata"><ffi:getProperty name="<%= task %>" property="WireName"/></td>
                                    <td align="left" width="120" class="sectionsubhead"><%= LABEL_TEMPLATE_NICKNAME %></td>
									<td align="left" width="185" class="columndata"><ffi:getProperty name="<%= task %>" property="NickName"/></td>
								</tr>
								<tr>
									<td align="left" class="sectionsubhead"><%= LABEL_TEMPLATE_ID %></td>
									<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="TemplateID"/></td>
                                    <td align="left" class="sectionsubhead"><%= LABEL_TEMPLATE_LIMIT %></td>
									<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="DisplayWireLimit"/></td>
								</tr>
							</table>
							<span class="columndata">&nbsp;<br></span>
<% } %>
<table width="635" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="left" class="tbrd_b sectionsubhead" colspan="3">&gt; <!--L10NStart-->Send Wire To<!--L10NEnd--></td>
	</tr>
	<tr>
		<td align="left" colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<% if (wireType.equals(WireDefines.WIRE_DRAWDOWN))  { %>
	<tr>
		<td width="320" valign="top">
			<table width="320" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td align="left" class="sectionsubhead" colspan="2">&gt; <!--L10NStart-->Beneficiary Info<!--L10NEnd--></td>
				</tr>
				<tr>
					<td align="left" width="120" class="sectionsubhead"><%= LABEL_BENEFICIARY_NAME %></td>
					<td align="left"  width="200" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.PayeeName"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_ADDRESS_1 %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Street"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_ADDRESS_2 %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Street2"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_CITY %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.City"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_STATE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.State"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_ZIP_CODE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.ZipCode"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_CONTACT_PERSON %></td>
					<td align="left"  class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Contact"/></td>
				</tr>
                <tr>
                    <td align="left" class="sectionsubhead"><%= LABEL_CREDIT_ACCOUNT %></td>
                    <td align="left" class="columndata"><ffi:getProperty name="Account" property="DisplayText"/>
						<ffi:cinclude value1="${Account.NickName}" value2="" operator="notEquals" >
							 - <ffi:getProperty name="Account" property="NickName"/>
				 		</ffi:cinclude>
						 - <ffi:getProperty name="Account" property="CurrencyCode"/>
					</td>
                </tr>
			</table>
		</td>
		<td align="left" class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="305" valign="top">
			<table width="305" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td align="left" class="sectionsubhead" colspan="2">&gt; <!--L10NStart-->Beneficiary Bank Info<!--L10NEnd--></td>
				</tr>
				<tr>
					<td align="left" width="120" class="sectionsubhead"><%= LABEL_BANK_NAME %></td>
					<td align="left" width="185" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.BankName"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_BANK_ADDRESS_1 %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.Street"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_BANK_ADDRESS_2 %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.Street2"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_BANK_CITY %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.City"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_BANK_STATE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.State"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_BANK_ZIP_CODE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.ZipCode"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_FED_ABA %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.RoutingFedWire"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%-- DEBIT ACCOUNT INFO --%>
<br>
<table width="635" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="left" class="tbrd_b sectionsubhead" colspan="3">&gt; <!--L10NStart-->Debit Bank Information<!--L10NEnd--></td>
	</tr>
	<tr>
		<td align="left" colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<% } %>
	<tr>
		<td width="320" valign="top">
			<table width="320" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td align="left" class="sectionsubhead" colspan="2">&gt; <%= wireType.equals(WireDefines.WIRE_DRAWDOWN) ? "<!--L10NStart-->Debit Account Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Info<!--L10NEnd-->" %> </td>
				</tr>
                <% int firstColWidth = 120, secondColWidth = 200;%>
                <% if (wireType.equals(WireDefines.WIRE_DRAWDOWN)) 
                   {
                       firstColWidth = 140;
                       secondColWidth = 180;
                   }
                %>
                <tr>
					<td align="left" width="<%= firstColWidth %>" class="sectionsubhead"><%= wireType.equals(WireDefines.WIRE_DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME %></td>
					<td align="left"  width="<%= secondColWidth%>" class="columndata"><ffi:getProperty name="wireTransferPayee" property="Name"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_NICKNAME %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="NickName"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_ADDRESS_1 %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="Street"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_ADDRESS_2 %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="Street2"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_CITY %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="City"/></td>
				</tr>
				<ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="true">
				<tr>
					<td align="left" class="sectionsubhead"><%= (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL)) ? LABEL_STATE_PROVINCE : LABEL_STATE %></td>
					<td align="left" class="columndata">
						<ffi:setProperty name="GetCodeForBankLookupCountryName" property="CountryName" value="${wireTransferPayee.Country}" />
						<ffi:process name="GetCodeForBankLookupCountryName" />
						<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${GetCodeForBankLookupCountryName.CountryCode}" />
						<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${wireTransferPayee.State}" />
						<ffi:process name="GetStateProvinceDefnForState"/>
						<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
							<ffi:getProperty name="wireTransferPayee" property="State"/>
						</ffi:cinclude>
						<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
							<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
						</ffi:cinclude>
					</td>
				</tr>
				</ffi:cinclude>
				<tr>
					<td align="left" class="sectionsubhead"><%= (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL)) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="ZipCode"/></td>
				</tr>
<% if (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL)) { %>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_COUNTRY %></td>
					<ffi:setProperty name="GetCodeForBankLookupCountryName" property="CountryName" value="${wireTransferPayee.Country}" />
					<ffi:process name="GetCodeForBankLookupCountryName" />
					<ffi:setProperty name="LocalizeCountryResource" property="ResourceID" value="Country${GetCodeForBankLookupCountryName.CountryCode}" />
					<td align="left" class="columndata"><ffi:getProperty name="LocalizeCountryResource" property="Resource"/></td>
				</tr>
<% } %>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_CONTACT_PERSON %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="Contact"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= wireType.equals(WireDefines.WIRE_DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_SCOPE : LABEL_BENEFICIARY_SCOPE %></td>
					<td align="left" class="columndata"><ffi:setProperty name="WirePayeeScopes" property="Key" value="${wireTransferPayee.PayeeScope}"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= wireType.equals(WireDefines.WIRE_DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NUMBER : LABEL_ACCOUNT_NUMBER %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="AccountNum"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= wireType.equals(WireDefines.WIRE_DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_TYPE : LABEL_ACCOUNT_TYPE %></td>
					<td align="left" class="columndata">
						<ffi:setProperty name="WireAccountType" property="ResourceID" value="AccountType_${wireTransferPayee.AccountType}"/>
						<ffi:getProperty name="WireAccountType" property="Resource"/>
					</td>
				</tr>
			</table>
		</td>
		<td align="left" class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="305" valign="top">
			<table width="305" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td align="left" class="sectionsubhead" colspan="2">&gt; <%= wireType.equals(WireDefines.WIRE_DRAWDOWN) ? "<!--L10NStart-->Debit Bank Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Bank Info<!--L10NEnd-->" %> </td>
				</tr>
				<tr>
					<td align="left" width="120" class="sectionsubhead"><%= wireType.equals(WireDefines.WIRE_DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME %></td>
					<td align="left" width="185" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.BankName"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_BANK_ADDRESS_1 %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.Street"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_BANK_ADDRESS_2 %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.Street2"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_BANK_CITY %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.City"/></td>
				</tr>
				<ffi:cinclude value1="${GetStatesForCountry2.IfStatesExists}" value2="true">
				<tr>
					<td align="left" class="sectionsubhead"><%= (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL)) ? LABEL_BANK_STATE_PROVINCE : LABEL_BANK_STATE %></td>
					<td align="left" class="columndata">
						<ffi:setProperty name="GetCodeForBankLookupCountryName" property="CountryName" value="${wireTransferPayee.DestinationBank.Country}" />
							<ffi:process name="GetCodeForBankLookupCountryName" />
							<ffi:setProperty name="GetStateProvinceDefnForState" property="CountryCode" value="${GetCodeForBankLookupCountryName.CountryCode}" />
							<ffi:setProperty name="GetStateProvinceDefnForState" property="StateCode" value="${wireTransferPayee.DestinationBank.State}" />
							<ffi:process name="GetStateProvinceDefnForState"/>
							<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="equals">
								<ffi:getProperty name="wireTransferPayee" property="DestinationBank.State"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${GetStateProvinceDefnForState.StateProvinceDefn.Name}" value2="" operator="notEquals">
								<ffi:getProperty name="GetStateProvinceDefnForState" property="StateProvinceDefn.Name"/>
							</ffi:cinclude>
					</td>
				</tr>
				</ffi:cinclude>
				<tr>
					<td align="left" class="sectionsubhead"><%= (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL)) ? LABEL_BANK_ZIP_POSTAL_CODE : LABEL_BANK_ZIP_CODE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.ZipCode"/></td>
				</tr>
				<% if (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL)) { %>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_BANK_COUNTRY %></td>
					<ffi:setProperty name="GetCodeForBankLookupCountryName" property="CountryName" value="${wireTransferPayee.DestinationBank.Country}" />
					<ffi:process name="GetCodeForBankLookupCountryName" />
					<ffi:setProperty name="LocalizeCountryResource" property="ResourceID" value="Country${GetCodeForBankLookupCountryName.CountryCode}" />
					<td align="left" class="columndata"><ffi:getProperty name="LocalizeCountryResource" property="Resource"/></td>
				</tr>
				<% } %>
				<tr>
					<td align="left" class="sectionsubhead"><%= wireType.equals(WireDefines.WIRE_DRAWDOWN) ? LABEL_DRAWDOWN_BANK_ABA : LABEL_FED_ABA %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.RoutingFedWire"/></td>
				</tr>
				<% if (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL)) { %>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_SWIFT %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.RoutingSwift"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_CHIPS %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.RoutingChips"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_NATIONAL %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.RoutingOther"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_IBAN %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.IBAN"/></td>
				</tr>
				<ffi:cinclude value1="${wireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
				</tr>
				</ffi:cinclude>
				<% } %>

				<% if (wireType.equals(WireDefines.WIRE_FED)) { %>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_IBAN %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.IBAN"/></td>
				</tr>
				<ffi:cinclude value1="${wireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
					<td align="left" class="columndata"><ffi:getProperty name="wireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
				</tr>
				</ffi:cinclude>
				<% } %>
			</table>
		</td>
	</tr>
</table>
<% if (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL) || wireType.equals(WireDefines.WIRE_FED)) { %>
<ffi:setProperty name="wireTransferPayee" property="IntermediaryBanks.Filter" value="ACTION!!del"/>
<ffi:cinclude value1="${wireTransferPayee.IntermediaryBanks}" value2="" operator="notEquals" >
<ffi:cinclude value1="${wireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
<table width="635" border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td align="left" ><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="1" border="0"></td>
		<td align="left" class="sectionsubhead" colspan="5">&gt; <!--L10NStart-->Intermediary Banks<!--L10NEnd--></td>
	</tr>
	<tr>
		<% if (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL)) { %>
		<td align="left" width="5%" class="sectionsubhead">&nbsp;</td>
		<td align="left"  width="30%" class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></td>
		<td align="left" width="12%" class="sectionsubhead"><%= COLUMN_FED_ABA %></td>
		<td align="left" width="12%" class="sectionsubhead"><%= COLUMN_SWIFT %></td>
		<td align="left" width="11%" class="sectionsubhead"><%= COLUMN_CHIPS %></td>
		<td align="left" width="10%" class="sectionsubhead"><%= COLUMN_NATIONAL %></td>
		<td align="left" width="10%" class="sectionsubhead"><%= COLUMN_IBAN %></td>
		<td align="left" width="10%" class="sectionsubhead"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
		<% } else if(wireType.equals(WireDefines.WIRE_FED)) { %>
		<td align="left" width="5%" class="sectionsubhead">&nbsp;</td>
		<td align="left" width="35%" class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></td>
		<td align="left" width="20%" class="sectionsubhead"><%= COLUMN_FED_ABA %></td>
		<td align="left" width="20%" class="sectionsubhead"><%= COLUMN_IBAN %></td>
		<td align="left" width="20%" class="sectionsubhead"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
		<% } else { %>
		<td align="left" width="5%" class="sectionsubhead">&nbsp;</td>
		<td align="left" width="45%" class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></td>
		<td align="left" width="50%" class="sectionsubhead"><%= COLUMN_FED_ABA %></td>
		<% } %>
	</tr>
	<% int counter = 0; %>
	<ffi:list collection="wireTransferPayee.IntermediaryBanks" items="Bank1">
	<tr>
		<td align="left" class="columndata">&nbsp;</td>
		<td align="left" class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</td>
		<td align="left" class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</td>
		<% if (wireType.equals(WireDefines.WIRE_DOMESTIC) || wireType.equals(WireDefines.WIRE_INTERNATIONAL)) { %>
		<td align="left" class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</td>
		<td align="left" class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</td>
		<td align="left" class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</td>
		<td align="left" class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
		<td align="left" class="columndata">
			<% if(counter == 0) { %>
			&nbsp;
			<% } else { %>
			<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
			<% } %>&nbsp;</td>
		<% } %>
		<% if (wireType.equals(WireDefines.WIRE_FED)) { %>
		<td align="left" class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
		<td align="left" class="columndata">
			<% if(counter == 0) { %>
			&nbsp;
			<% } else { %>
			<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
			<% }  %>&nbsp;</td>
		<% } counter++; %>
	</tr>
	</ffi:list>
</table>
</ffi:cinclude>
</ffi:cinclude>
<% } %>
<br>
<%-- DOMESTIC --%>
<table width="635" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="left" class="tbrd_b sectionsubhead" colspan="3">&gt; <%= wireType.equals(WireDefines.WIRE_DRAWDOWN) ? "<!--L10NStart-->Credit Info<!--L10NEnd-->" : "<!--L10NStart-->Debit Info<!--L10NEnd-->" %></td>
	</tr>
	<tr>
		<td align="left" colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="320" valign="top">
			<table width="320" cellpadding="2" cellspacing="0" border="0">
				<% // International wires need a wider first column than domestic.
				int c1 = 130;
				if (wireType.equals(WireDefines.WIRE_INTERNATIONAL)) c1 = 150;
				int c2 = 320 - c1;

				if (!wireType.equals(WireDefines.WIRE_DRAWDOWN)) { %>
				<tr>
					<td align="left" width="<%= c1 %>" class="sectionsubhead" nowrap><%= LABEL_FROM_ACCOUNT %></td>
					<td align="left" width="<%= c2 %>" class="columndata">
						<%-- check to see if the account is in the Accounts collection, so we can show its NickName --%>
						<% String fromAcctFilter = ""; %>
						<ffi:getProperty name="<%= task %>" property="FromAccountID" assignTo="fromAcctFilter"/>
						<% fromAcctFilter = "ID=" + fromAcctFilter; %>
						<ffi:setProperty name="BankingAccounts" property="Filter" value="<%= fromAcctFilter %>"/>
						<ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="notEquals">
							<ffi:list collection="BankingAccounts" items="acct" startIndex="1" endIndex="1">
								<ffi:getProperty name="acct" property="DisplayText"/> -
								<ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
									<ffi:getProperty name="acct" property="NickName"/> -
						 		</ffi:cinclude>
								<ffi:getProperty name="acct" property="CurrencyCode"/>
							</ffi:list>
						</ffi:cinclude>
						<%-- if the account is not in the Accounts collection, it's not entitled.  Show the account type instead. --%>
						<ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="equals">
							<ffi:getProperty name="<%= task %>" property="FromAccountID"/>
						</ffi:cinclude>
						<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
					</td>
				</tr>
				<% } %>
				<tr>
					<td align="left" width="<%= c1 %>" class="sectionsubhead" nowrap><%= wireIsTemplate ? LABEL_TEMPLATE_LIMIT : LABEL_AMOUNT %></td>
					<td align="left" width="<%= c2 %>" class="columndata">
					
						<ffi:getProperty name="<%= task %>" property="DisplayOrigAmountDomestic"/>
					
</td>
				</tr>
				<ffi:removeProperty name="GetAmounts"/>
				<% if (!wireIsTemplate) { %>
				<tr>
					<td align="left" class="sectionsubhead" nowrap><%= wireType.equals(WireDefines.WIRE_INTERNATIONAL) ? LABEL_REQ_PROCESSING_DATE : LABEL_REQ_DUE_DATE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="DueDate"/></td>
				</tr>
					<% if (wireType.equals(WireDefines.WIRE_INTERNATIONAL)) { %>
					<tr>
						<td align="left" class="sectionsubhead" nowrap><%= LABEL_SETTLEMENT_DATE %></td>
						<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="SettlementDate"/></td>
					</tr>
					<% } %>
				<tr>
					<td align="left" class="sectionsubhead" nowrap><%= wireType.equals(WireDefines.WIRE_INTERNATIONAL) ? LABEL_ACTUAL_PROCESSING_DATE : LABEL_ACTUAL_DUE_DATE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="DateToPost"/></td>
				</tr>
				<% } %>
				<%String transferType=""; %>
				<ffi:getProperty name="<%= task %>" property="type" assignTo="transferType"/>
<% if (transferType != null && Integer.parseInt(transferType) == com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) { %>
				<tr>
					<td align="left" class="sectionsubhead" nowrap><%= LABEL_FREQUENCY %></td>
					<td align="left" class="columndata"><% String temp = "WireFrequencies" + wire.getFrequencyValue(); %>
						<ffi:setProperty name="WireTransferFrequenciesNames" property="ResourceID" value="<%= temp %>"/>
						<ffi:getProperty name="WireTransferFrequenciesNames" property="Resource"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead" nowrap><%= LABEL_NUMBER_OF_PAYMENTS %></td>
					<td align="left" class="columndata">
					<% if (wire.getNumberTransfersValue() == 999) { %>
						<%= LABEL_UNLIMITED %>
					<% } else { %>
						<ffi:getProperty name="<%= task %>" property="NumberTransfers"/>
					<% } %>
					</td>
				</tr>
<% } %>
			</table>
		</td>
<% if (!wireType.equals(WireDefines.WIRE_INTERNATIONAL)) { %>
		<td align="left" colspan="2">&nbsp;</td>
<% } else {     %>
		<td align="left" class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="305" valign="top">
			<table width="305" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td align="left" width="120" class="sectionsubhead" nowrap><%= LABEL_DEBIT_CURRENCY %></td>
					<td align="left" width="185" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead" nowrap><%= LABEL_EXCHANGE_RATE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ExchangeRate"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead" nowrap><%= LABEL_MATH_RULE %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="MathRule"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead" nowrap><%= LABEL_CONTRACT_NUMBER %></td>
					<td align="left" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ContractNumber"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table width="635" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="left" class="tbrd_b sectionsubhead">&gt; <!--L10NStart-->Payment Info<!--L10NEnd--></td>
	</tr>
	<tr>
		<td align="left"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td>
			<table width="305" cellpadding="2" cellspacing="0" border="0">
				<tr>
					<td align="left" width="120" class="sectionsubhead"><%= LABEL_PAYMENT_CURRENCY %></td>
					<td align="left" width="185" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="PayeeCurrencyType"/></td>
				</tr>
			</table>
		</td>
<% } %>
	</tr>
</table>
							<br>
							<table width="635" border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td align="left" class="tbrd_b"><span class="sectionsubhead">&gt; <%= wireType.equals(WireDefines.WIRE_INTERNATIONAL) ? "<!--L10NStart-->Sender's Reference<!--L10NEnd-->" : "<!--L10NStart-->Reference for Beneficiary<!--L10NEnd-->" %></span></td>
								</tr>
								<tr>
									<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="Comment"/></td>
								</tr>
								<tr>
									<td align="left" class="tbrd_b"><span class="sectionsubhead">&gt; <!--L10NStart-->Originator to Beneficiary Information<!--L10NEnd--></span></td>
								</tr>
								<tr>
									<td align="left" class="columndata">
									<% String info = ""; %>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo1"/>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4"/></td>
									<% } %>
								</tr>
								<% if (!wireType.equals(WireDefines.WIRE_BOOK)) { %>
								<tr>
									<td align="left" class="tbrd_b"><span class="sectionsubhead">&gt; <!--L10NStart-->Bank to Bank Information<!--L10NEnd--></span></td>
								</tr>
								<tr>
									<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="BankToBankInfo1"/>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo2" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo2"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo3" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo3"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo4" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo4"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo5" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo5"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo6" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo6"/></td>
									<% } %>
								</tr>
								<% } %>
							</table>
<% if (wireType.equals(WireDefines.WIRE_FED)) { %>

<table width="635" border="0" cellspacing="0" cellpadding="2">
    <tr>
        <td align="left" class="tbrd_b" colspan="2"><span class="sectionsubhead">&gt; <!--L10NStart-->Addenda<!--L10NEnd--></span></td>
    </tr>
    <tr>
        <td align="left" width="120" class="sectionsubhead"><!--L10NStart-->Addenda Type<!--L10NEnd--></td>
        <td align="left" width="515" class="columndata"><ffi:getProperty name="<%= task %>" property="AddendaType"/> - <ffi:getProperty name="<%= task %>" property="AddendaTypeDisplayText"/></td>
    </tr>
    <tr>
        <td align="left" class="sectionsubhead"><!--L10NStart-->Addenda<!--L10NEnd--></td>
        <% String addendaString = ""; %>
        <ffi:getProperty name="<%= task %>" property="AddendaForDisplay" assignTo="addendaString"/>
        <%
            if( addendaString != null ) {
                addendaString = HTMLUtil.encode( addendaString );

                    // convert all line separator characters to html newline i.e <br>
                addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "\n", "<br>");
                // convert all MULTIPLE text spaces to HTML non-breaking spaces i.e &nbsp;
                addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "  ", "&nbsp;&nbsp;");
                // in case there was an odd amount of multiple spaces, convert to double space
                addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "&nbsp; ", "&nbsp;&nbsp;");
            }
        %>
        <td align="left" class="columndata"><%= addendaString %>
        </td>

    </tr>
</table>
<% } %>
							<table width="635" border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td align="left" class="tbrd_b" colspan="2"><span class="sectionsubhead">&gt; <!--L10NStart-->By Order Of Information<!--L10NEnd--></span></td>
								</tr>
								<tr>
									<td align="left" width="120" class="sectionsubhead"><%= LABEL_BOO_NAME %></td>
									<td align="left" width="515" class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfName"/></td>
								</tr>
								<tr>
									<td align="left" class="sectionsubhead"><%= LABEL_BOO_ADDRESS_1 %></td>
									<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress1"/></td>
								</tr>
								<tr>
									<td align="left" class="sectionsubhead"><%= LABEL_BOO_ADDRESS_2 %></td>
									<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress2"/></td>
								</tr>
								<tr>
									<td align="left" class="sectionsubhead"><%= LABEL_BOO_ADDRESS_3 %></td>
									<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress3"/></td>
								</tr>
								<tr>
									<td align="left" class="sectionsubhead"><%= LABEL_BOO_ACCOUNT_NUMBER %></td>
									<td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAccount"/></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
                <tr>
                    <td class="tbrd_lr">&nbsp;</td>
                </tr>
                <tr>
                    <td class="tbrd_ltr sectionsubhead" height="20" width="670">
                        <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td align="left" width="5" class="sectionsubhead">&nbsp;</td>
                            <td align="left" width="665" class="sectionsubhead"><!--L10NStart-->Confidential<!--L10NEnd--></td>
                        </tr>
                        </table>
                    </td>
                </tr>
                <tr>
					<td align="left" class="tbrd_t">&nbsp;</td>
				</tr>
            </table>

		</div>
<%--	</body>

</html> --%>
</ffi:cinclude>

<ffi:removeProperty name="LocalizeCountryResource" />
<ffi:removeProperty name="GetCodeForBankLookupCountryName" />
<ffi:removeProperty name="GetStateProvinceDefnForState" />
<%--=================== PAGE SECURITY CODE - Part 2 Start ====================--%>

<%--=================== PAGE SECURITY CODE - Part 2 End =====================--%>

