<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:help id="payments_autoentitle-confirm" className="moduleHelpClass"/>
<ffi:setL10NProperty name='PageHeading' value='Auto-Entitle Confirmation'/>

	<div align="center">
		<ffi:setProperty name="subMenuSelected" value="wires"/>

		<table width="750" border="0" cellspacing="0" cellpadding="0">
	
	    <tr>
		<td>&nbsp;</td>
		<td align="center" class="ltrow2_color">
			
			<s:form id="autoEntitleFormFormID" validate="false" action="%{#attr.SubmitAction}"  namespace="/pages/jsp/wires" method="post" name="frmFeatures" theme="simple">
	                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	                    	<input id="isSaveAsWireTemplateFlagID" type="hidden" name="isSaveAsWireTemplate" value="<ffi:getProperty name='isSaveAsWireTemplate'/>"/>
	                    	<s:hidden name="url_action" value="%{#attr.SubmitAction}" id="url_action"></s:hidden>
	                    	
		    <table width="98%" border="0" cellspacing="0" cellpadding="3" class="ltrow2_color">
			<tr>			
				<td colspan="2" style="text-align:center" nowrap><span class="columndata"><s:text name="jsp.wire.template.auto.entitle.confirm"/></span><br>
				</td>
			</tr>
			<s:if test="wireTemplateExceedsLimits">
				<tr>
					<td colspan="2" style="text-align:center" nowrap><span class="columndata">*<s:text name="jsp.wire.template.exceed.limits"/></span><br>
					</td>
				</tr>
			</s:if>
			
			<ffi:removeProperty name="autoEntitleInfoMsg"/>
			<tr>
				<td style="text-align:center"  clospan="2"><input type="radio" name="doAutoEntitle" value="true" checked="checked"/>
				<s:text name="jsp.default_467"/>&nbsp;&nbsp;
				<input type="radio" name="doAutoEntitle" value="false" />		
				<s:text name="jsp.default_295"/></td>
			</tr>
			<tr>
				<td colspan="2" height="55">&nbsp;</td>
			</tr> 
	    		<tr>
				<td colspan="2" class="ui-widget-header customDialogFooter">
	
				<sj:a id="cancelFormButtonOnAutoEntitle" 
					button="true" 
					onClickTopics="cancelFormOnAutoEntitle"
				><s:text name="jsp.default_82"/>
				</sj:a>
				
				<s:if test='%{#attr.SubmitAction=="addWireTransferAction_saveAsTemplate.action" || 
				#attr.SubmitAction=="modifyWireTransferAction_saveAsTemplate.action"}'>
					<sj:a 											
					id="sendAutoEntitleFormFormSubmit"
					formIds="autoEntitleFormFormID"
					targets="resultMessageDiv" 
					button="true" 
					onBeforeTopics="beforeSaveWireTemplateFormWithAutoEntitle"
					onSuccessTopics="sendSaveWireTemplateFormWithAutoEntitleSuccess"
					onErrorTopics="sendSaveWireTemplateFormWithAutoEntitleError"
					onCompleteTopics="sendSaveWireTemplateFormWithAutoEntitleComplete"
					><s:text name="jsp.default_366"/>
				</sj:a>
				</s:if>
				<s:else>
					<sj:a 											
					id="sendAutoEntitleFormFormSubmit"
					formIds="autoEntitleFormFormID"
					targets="confirmDiv" 
					button="true" 
					onBeforeTopics="beforeSaveWireTemplateFormWithAutoEntitle"
					onSuccessTopics="sendSaveWireTemplateFormWithAutoEntitleSuccess"
					onErrorTopics="sendSaveWireTemplateFormWithAutoEntitleError"
					onCompleteTopics="sendSaveWireTemplateFormWithAutoEntitleComplete"
					><s:text name="jsp.default_366"/>
				</sj:a>
				</s:else>
				
				
				</td>
			</tr>
		    </table>
			</s:form>
		</td>
		<td>&nbsp;</td>
	    </tr>
	</table>

</div>
