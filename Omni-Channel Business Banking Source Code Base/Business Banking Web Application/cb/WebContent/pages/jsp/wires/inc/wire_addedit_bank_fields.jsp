<%--
This file contains the wire beneficiary bank name and address fields, plus the
SEARCH button.

It is included on all add/edit wire transfer and add/edit wire beneficiary
pages
    wireaddpayee.jsp
    wireaddpayeebook.jsp
    wireaddpayeedrawdown.jsp
    wirebook.jsp
    wiredrawdown.jsp
    wirefed.jsp
    wiretransfernew.jsp

It includes the following file:
common/wire_labels.jsp
    The labels for fields and buttons
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ include file="../../common/wire_labels.jsp"%>
<style>
/* input#bankName, input#bankAddress1, input#bankAddress2, input#bankCity, input#bankState{width:23.5em} */
</style>
<script>
$.subscribe('selectDestBankTopics', function(event,data) {
	
	
	if ($("#selectWirePayeeID").length > 0 && $("#selectWirePayeeID").val() != js_wirelookup_default_value 
		&& $("#selectWirePayeeID").val() != 'Create New Beneficiary' && $("#selectWirePayeeID").val() != 'Create New Debit Account') {			
			$("#searchDestBank").button("option", "disabled", true);			
	} else {			
			$("#searchDestBank").button("option", "disabled", false);
		selectDestBank();
	}	
});
</script>
<ffi:cinclude value1="${disableEdit}" value2="disabled" operator="notEquals">
<script>
	$(function(){
		$("#selectBankCountryID").selectmenu({width:'15em'});
		//$("#selectBankCountryID_autoComplete").css("width", "22em");

		$("#selectAffiliateBankID").selectmenu({width:'24em'});
	});

</script>
</ffi:cinclude>
<ffi:cinclude value1="${disableEdit}" value2="disabled">
<script>
	$(function(){
		$("#selectBankCountryID").selectmenu({width:'15em'});
		//$("#selectBankCountryID_autoComplete").css("width", "13.6em");
		
		$("#selectAffiliateBankID").selectmenu({width:'21.5em'});
	});
	
	//$("#searchDestBank").fadeTo('slow', 0.5);
</script>
</ffi:cinclude>

<%-- Load from wire template flag --%>
<% session.setAttribute("LoadFromWireTemplate", (String)request.getParameter("LoadFromWireTemplate"));%>

<%-- Start: Dual approval processing --%>

<%-- End: Dual approval processing --%>

<ffi:setProperty name="disableBank" value="${disableEdit}"/>

<% String payeeDest = "";
   boolean showInternational = false;
   boolean showFed = false;
   String bankSize = "";
   String intBankSize = ""; %>




<ffi:getProperty name="destination"  assignTo="payeeDest"/>
<ffi:setProperty name="${payeeTask}" property="IntermediaryBanks.Filter" value="ACTION!!del"/>
<ffi:getProperty name="${payeeTask}" property="IntermediaryBanks.Size" assignTo="bankSize"/>
<ffi:cinclude value1="${bankSize}" value2="" operator="equals">
	<% bankSize = "0"; %>
</ffi:cinclude>

<% int noOfIntBanks = Integer.parseInt(bankSize); %>
<%--
Show the international fields if:
    A) This is included in add/edit wire transfer, and the current wire destination is Domestic or International, or
    B) This is included in add/edit wire payee, and the payee destination is Regular.
    NOTE:  payeeTask == WireTransferPayee if this is being included in add/edit wire transfer.
--%>

    <% if (payeeDest.equals(WireDefines.WIRE_DOMESTIC) || payeeDest.equals(WireDefines.WIRE_INTERNATIONAL)) showInternational = true; %>
    <% if (payeeDest.equals(WireDefines.WIRE_FED)) {
    		showFed = true;
    	} %>
	<% if (payeeDest.equals(WireDefines.PAYEE_TYPE_REGULAR)) showInternational = true; %>
<%--
The following processing retrieves a list of valid States based
on the Country chosen. This was done for localiztion.
--%>
<% String selectedDBBLCountry2 = ""; String selectedDBISOCountry2 = ""; %>

<ffi:getProperty name="${payeeTask}" property="DestinationBank.Country" assignTo="selectedDBBLCountry2"/>

<table cellpadding="3" cellspacing="0" border="0" width="100%">
    <tr>
                
         <td class="sectionhead nameTitle" style="cursor:auto; padding-top:10px;padding-bottom:10px;" colspan="5"><h2><%= payeeDest.equals(WireDefines.PAYEE_TYPE_DRAWDOWN) ? "Debit" : "Beneficiary" %> Bank Info</h2></td>
<%-- <% if (!payeeDest.equals(WireDefines.PAYEE_TYPE_BOOK)) { %>
	<td align="right">
		
		<ffi:cinclude value1="true" value2="${LoadFromWireTemplate}">		
			<script> 
			if(ns.common.isInitialized($("#searchDestBank"),"ui-button")){
				$("#searchDestBank").button("option", "disabled", true);
			}
			</script>			
		</ffi:cinclude>
		<ffi:cinclude value1="true" value2="${LoadFromWireTemplate}" operator="notEquals">		
			<script>
			if(ns.common.isInitialized($("#searchDestBank"),"ui-button")){
				$("#searchDestBank").button("option", "disabled", false);
			}
			</script>		
		</ffi:cinclude>
		<sj:a button="true" title="SEARCH" id="searchDestBank" onClickTopics="selectDestBankTopics"><s:text name="jsp.default_373"/> </sj:a>		
	</td>
<% } %> --%>
    </tr>
    
 <% if (payeeDest.equals(WireDefines.PAYEE_TYPE_BOOK)) { %>
    <ffi:setProperty name="disableBank" value="disabled"/>
    <tr>
   	 	<td colspan="4" id="affiliateBankLabel" class="sectionsubhead"><%= LABEL_AFFILIATE_BANKS %></td>
    </tr>
    <tr>
        
        <td colspan="4"><select id="selectAffiliateBankID" name="affBank" class="txtbox" onchange="selectAffBank(this.value);" <ffi:getProperty name="disableEdit"/>>
            <% String curAffBank = ""; String thisAffBank = ""; %>
            <ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingFedWire" assignTo="curAffBank"/>
            <% if (curAffBank == null) curAffBank = ""; %>
            <option value=""><%= DEFAULT_AFFILIATE_BANK %></option>
            <ffi:list collection="AffiliateBanks" items="bank">
                <ffi:getProperty name="bank" property="AffiliateRoutingNum" assignTo="thisAffBank"/>
                <option value="<ffi:getProperty name="bank" property="AffiliateBankID"/>" <%= curAffBank.equals(thisAffBank) ? "selected" : "" %>><ffi:getProperty name="bank" property="AffiliateBankName"/></option>
            </ffi:list>
            </select></td>
    </tr>
    <tr>
   	 	<td colspan="4"></td>
   	 </tr>
<% } %> 


<tr>
   	 <td colspan="2" width="50%" id="bankNameLabel" width="115"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.DestinationBank.BankName}" 
    	 oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.BankName}" daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
    	 	<%= payeeDest.equals(WireDefines.PAYEE_TYPE_DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME %></span><span class="required">*</span>
    	 </td>
        <td width="25%" id="routingFedWireLabel"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.DestinationBank.RoutingFedWire}" 
        	oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.RoutingFedWire}"  daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
        		<%= payeeDest.equals(WireDefines.PAYEE_TYPE_DRAWDOWN) ? LABEL_DRAWDOWN_BANK_ABA : LABEL_FED_ABA %><% if (!payeeDest.equals(WireDefines.PAYEE_TYPE_REGULAR)) { %></span><span class="required">*</span><% } %></td>
        <td width="25%" id="routingSwiftLabel" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.DestinationBank.RoutingSwift}" oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.RoutingSwift}" 
        	daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><% if (showInternational) { %><%= LABEL_SWIFT %><% } %></td>
    </tr>
    <tr>
   	 <td colspan="2">
       	<input type="hidden" name="isBankSearch" />
       	<input type="hidden" name="Inter" />
       	<input type="hidden" name="isWireTransfer" value="<ffi:getProperty name="IsWireTransfer"/>" />
		<% if (payeeDest.equals(WireDefines.PAYEE_TYPE_BOOK)) { %>
		<%-- For a book payee, the DestinationBank BankName is a disabled field for display only, but that means that it won't be submitted with the form.
		     That field is used by the validation xml file, so if it isn't submitted, it can't be properly validated.
		     Use a disabled field for display and a hidden field with the correct name for validation. --%>
			<input id="bankName" name="BankName.DisplayOnly" value="<ffi:getProperty name="${payeeTask}" property="DestinationBank.BankName"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="25" disabled>
			<input type="hidden" name="WireTransferPayee.DestinationBank.BankName" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/>">
		<% } else { %>
			<input id="bankName" name="WireTransferPayee.DestinationBank.BankName" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="128" size="30" <ffi:getProperty name="disableBank"/>>
		<% } %>
		<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
		<ffi:cinclude value1="${WireTransferPayee.DestinationBank.BankName}" value2="${WireTransferPayee.MasterPayee.DestinationBank.BankName}" operator="notEquals">			
       		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.BankName" /></span>
       	</ffi:cinclude>
		</ffi:cinclude>
		<% if (!payeeDest.equals(WireDefines.PAYEE_TYPE_BOOK)) { %>
			
			<ffi:cinclude value1="true" value2="${LoadFromWireTemplate}">		
				<script> 
				if(ns.common.isInitialized($("#searchDestBank"),"ui-button")){
					$("#searchDestBank").button("option", "disabled", true);
				}
				</script>			
			</ffi:cinclude>
			<ffi:cinclude value1="true" value2="${LoadFromWireTemplate}" operator="notEquals">		
				<script>
				if(ns.common.isInitialized($("#searchDestBank"),"ui-button")){
					$("#searchDestBank").button("option", "disabled", false);
				}
				</script>		
			</ffi:cinclude>
			<sj:a button="true" title="SEARCH" id="searchDestBank" onClickTopics="selectDestBankTopics"><s:text name="jsp.default_373"/> </sj:a>		
	<% } %>
       </td>
	    	<td>
	        	<input id="routingFedWire" name="WireTransferPayee.DestinationBank.RoutingFedWire" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="32" size="20" <ffi:getProperty name="disableBank"/>>
	        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${WireTransferPayee.DestinationBank.RoutingFedWire}" value2="${WireTransferPayee.MasterPayee.DestinationBank.RoutingFedWire}" operator="notEquals">
	        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.RoutingFedWire" /></span>
	        	</ffi:cinclude>
				</ffi:cinclude>
        	</td>
			<td>
				<% if (showInternational) { %>
		        	<input id="routingSwift" name="WireTransferPayee.DestinationBank.RoutingSwift" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingSwift"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="32" size="20" <ffi:getProperty name="disableEdit"/>>
		        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
					<ffi:cinclude value1="${WireTransferPayee.DestinationBank.RoutingSwift}" value2="${WireTransferPayee.MasterPayee.DestinationBank.RoutingSwift}" operator="notEquals">
		        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.RoutingSwift" /></span>
		        	</ffi:cinclude>
					</ffi:cinclude>
				<% } %>
  	 		</td>	    	
    </tr>
    
    <tr>
    <td colspan="2"><span id="WireTransferPayee.DestinationBank.BankNameError"></span><span id="destinationBank.bankNameError"></span></td>
    	<td><span id="WireTransferPayee.DestinationBank.RoutingFedWireError"></span><span id="destinationBank.routingFedWireError"></span>
    		<span id="WireTransferBank.invalidRoutingFedWireError"></span>
    	</td>		
    	<td><% if (showInternational) { %><span id="WireTransferPayee.DestinationBank.RoutingSwiftError"></span> <span id="DestinationBankRoutingSwiftError"></span><span id="WireTransferBank.invalidSwiftBicError"></span><% } %></td>
    	
    </tr>
    <tr>
    	
        <td id="routingChipsLabel" class='<ffi:daPendingStyle 
        newFieldValue="${WireTransferPayee.DestinationBank.RoutingChips}" 
        oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.RoutingChips}" 
        daUserAction="${WireTransferPayeeDA.Action}" 
        dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><% if (showInternational) { %><%= LABEL_CHIPS %><% } %></td>
        
        
        <td id="routingOtherLabel" class='<ffi:daPendingStyle 
        newFieldValue="${WireTransferPayee.DestinationBank.RoutingOther}" 
        oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.RoutingOther}" 
        daUserAction="${WireTransferPayeeDA.Action}" 
        dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><% if (showInternational) { %><%= LABEL_NATIONAL %><% } %></td>
        

<%  if (showInternational || showFed) { %>
      <td id="IBANLabel" class='<ffi:daPendingStyle 
        newFieldValue="${WireTransferPayee.DestinationBank.IBAN}" 
        oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.IBAN}" 
        daUserAction="${WireTransferPayeeDA.Action}" 
        dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%= LABEL_IBAN %></td>
        
        <% } %>
	
        <td class='<ffi:daPendingStyle 
        newFieldValue="${WireTransferPayee.DestinationBank.CorrespondentBankAccountNumber}" 
        oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.CorrespondentBankAccountNumber}" 
        daUserAction="${WireTransferPayeeDA.Action}" 
        dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><% if (noOfIntBanks >= 1) { %><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %><% } %></td>
	
</tr>
<tr>
	<% if (showInternational) { %>
		<td>
        	<input id="routingChips" name="WireTransferPayee.DestinationBank.RoutingChips" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingChips"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="32" size="20" <ffi:getProperty name="disableEdit"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.DestinationBank.RoutingChips}" value2="${WireTransferPayee.MasterPayee.DestinationBank.RoutingChips}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.RoutingChips" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
        	
        </td>
        <td>
        	<input id="routingOther" name="WireTransferPayee.DestinationBank.RoutingOther" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingOther"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="32" size="20" <ffi:getProperty name="disableEdit"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.DestinationBank.RoutingOther}" value2="${WireTransferPayee.MasterPayee.DestinationBank.RoutingOther}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.RoutingOther" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
        	
        </td>
	<% } %>
	<%  if (showInternational || showFed) { %>
		<td><input id="IBAN" name="WireTransferPayee.DestinationBank.IBAN" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="48" size="20" <ffi:getProperty name="disableEdit"/>>
        <ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
		<ffi:cinclude value1="${WireTransferPayee.DestinationBank.IBAN}" value2="${WireTransferPayee.MasterPayee.DestinationBank.IBAN}" operator="notEquals">
			<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.IBAN" /></span>
		</ffi:cinclude>
		</ffi:cinclude>
			
		</td>
		<% } %>
	<% if (noOfIntBanks >= 1) { %>
		<td>
        	<input name="WireTransferPayee.DestinationBank.CorrespondentBankAccountNumber" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="48" size="20" <ffi:getProperty name="disableEdit"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.DestinationBank.CorrespondentBankAccountNumber}" value2="${WireTransferPayee.MasterPayee.DestinationBank.CorrespondentBankAccountNumber}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.CorrespondentBankAccountNumber" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>       	
        	      	
        </td>
	<% } %>
</tr>
	<tr>
		<% if (showInternational) { %>
			<td><span id="WireTransferPayee.DestinationBank.RoutingChipsError"></span></td>
			<td><span id="WireTransferPayee.DestinationBank.RoutingOtherError"></span></td>
		<% } %>
		<%  if (showInternational || showFed) { %>
			<td><span id="DestinationBankIBANError"></span> <span id="WireTransferPayee.DestinationBank.IBANError"></span></td>
		<% } %>
		<% if (noOfIntBanks >= 1) { %>
			<td><span id="WireTransferPayee.DestinationBank.CorrespondentBankAccountNumberError"></span> </td>
		<% } %>
	</tr>
	
	<% if (noOfIntBanks == 0) { %>
       	<ffi:setProperty name="${payeeTask}" property="DestinationBank.CorrespondentBankAccountNumber" value=""/>
	<% } %>
    <tr>
        <td id="bankAddress1Label" colspan="2" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.DestinationBank.Street}" oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.Street}" 
        	daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%= LABEL_BANK_ADDRESS_1 %>
        </td>
        <td id="bankAddress2Label" colspan="2" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.DestinationBank.Street2}" 
         oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.Street2}" daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
         	<%= LABEL_BANK_ADDRESS_2 %>
        </td>
     </tr>
     <tr>
        <td colspan="2">
        	<input id="bankAddress1" name="WireTransferPayee.DestinationBank.Street" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="128" size="40" <ffi:getProperty name="disableBank"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.DestinationBank.Street}" value2="${WireTransferPayee.MasterPayee.DestinationBank.Street}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.Street" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
        	
        </td>
        <td colspan="2">
        	<input id="bankAddress2" name="WireTransferPayee.DestinationBank.Street2" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="128" size="40" <ffi:getProperty name="disableBank"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.DestinationBank.Street2}" value2="${WireTransferPayee.MasterPayee.DestinationBank.Street2}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.Street2" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
        </td>
        
    </tr>
    <tr>
        <td colspan="2"><span id="WireTransferPayee.DestinationBank.StreetError"></span></td>
        <td colspan="2"><span id="WireTransferPayee.DestinationBank.Street2Error"></span></td>
    </tr>
    <tr>
        <td id="bankCityLabel" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.DestinationBank.City}" oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.City}" 
        	daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%= LABEL_BANK_CITY %></td>
      	<ffi:cinclude value1="${stateList.size}" value2="0" operator="notEquals" >
        <td id="bankStateLabel" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.DestinationBank.State}" oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.State}" 
        	daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%= showInternational ? LABEL_BANK_STATE_PROVINCE : LABEL_BANK_STATE %></td>
    	</ffi:cinclude>
    	<ffi:cinclude value1="${GetStatesForCountry2.IfStatesExists}" value2="false">
        	<input type="hidden" name="<ffi:getProperty name="payeeTask"/>.DestinationBank.State" value="">
    	</ffi:cinclude>
    	<td id="bankZipCodeLabel" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.DestinationBank.ZipCode}" 
          oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.ZipCode}" daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
          	<%= showInternational ? LABEL_BANK_ZIP_POSTAL_CODE : LABEL_BANK_ZIP_CODE %>
         </td>
         <% if (showInternational) { %>
        	<td id="bankCountryLabel"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.DestinationBank.Country}" 
       		 oldFieldValue="${WireTransferPayee.MasterPayee.DestinationBank.Country}"  daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'>
       		 	<%= LABEL_BANK_COUNTRY %></span><span class="required">*</span>
       		 </td>
         <% } %>
    </tr>
    <tr>
        <td>
        	<input id="bankCity" name="WireTransferPayee.DestinationBank.City" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="32" size="20" <ffi:getProperty name="disableBank"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.DestinationBank.City}" value2="${WireTransferPayee.MasterPayee.DestinationBank.City}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.City" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
        	
        </td>
        <ffi:cinclude value1="${stateList.size}" value2="0" operator="notEquals" >
        	<td>
        	<input id="bankState" name="WireTransferPayee.DestinationBank.State" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.State"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="32" size="20" <ffi:getProperty name="disableBank"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.DestinationBank.State}" value2="${WireTransferPayee.MasterPayee.DestinationBank.State}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.State" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
        </td>
        </ffi:cinclude>
        <td>
        	<input id="bankZipCode" name="WireTransferPayee.DestinationBank.ZipCode" value="<ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="32" size="20" <ffi:getProperty name="disableBank"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.DestinationBank.ZipCode}" value2="${WireTransferPayee.MasterPayee.DestinationBank.ZipCode}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.ZipCode" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
        </td>
        <td>
        <% if (showInternational) { %>
        
            <% String tempDefaultValue = ""; String tempCountry = ""; %>
            <ffi:getProperty name="WireTransferPayee" property="DestinationBank.Country" assignTo="tempDefaultValue"/>
            <% if (tempDefaultValue == null) tempDefaultValue = ""; %>
            <% tempDefaultValue = ( tempDefaultValue == "" ? com.ffusion.banklookup.beans.FinancialInstitution.PREFERRED_COUNTRY : tempDefaultValue ); %>
            <div class="selectBoxHolder">
	            <select id="selectBankCountryID" name="WireTransferPayee.DestinationBank.Country" class="txtbox" <ffi:getProperty name="disableEdit"/> onChange="ns.common.adjustSelectMenuWidth('selectBankCountryID')">
				<ffi:list collection="WireCountryList" items="country">
	            <ffi:getProperty name="country" property="BankLookupCountry" assignTo="tempCountry"/>
	            <option value="<ffi:getProperty name='tempCountry'/>" <%= ( tempDefaultValue.equals( tempCountry ) ? "selected" : "" ) %>><ffi:getProperty name="country" property='Name'/></option>
	            </ffi:list>
	            </select>
            </div>
            <ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.DestinationBank.Country}" value2="${WireTransferPayee.MasterPayee.DestinationBank.Country}" operator="notEquals">
            	<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.DestinationBank.Country" /></span>
            </ffi:cinclude>
			</ffi:cinclude>
            <ffi:removeProperty name="BeneficiaryBankCountry"/>
			<span id="WireTransferPayee.DestinationBank.CountryError"></span>
            
         <% } %>
         </td>
    </tr>
    <tr>
    	<td><span id="<ffi:getProperty name="payeeTask"/>.DestinationBank.CityError"></span></td>
    	<ffi:cinclude value1="${stateList.size}" value2="0" operator="notEquals" >
    		<td><span id="WireTransferPayee.DestinationBank.StateError"></span></td>
    	</ffi:cinclude>
    	<td></td>
    	<td></td>
    	<td><span id="WireTransferPayee.DestinationBank.ZipCodeError"></span></td>
    	<% if (showInternational) { %>
         <% } %>
    </tr>

</table>


<script>
$(function(){
	ns.common.adjustSelectMenuWidth('selectBankCountryID');
});
</script>
