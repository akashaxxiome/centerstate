<%--
This file contains the Javascript functions common to all add/edit wire
beneficiary pages.

It is included in the following files:
    wireaddpayee.jsp
    wireaddpayeebook.jsp
    wireaddpayeedrawdown.jsp

--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<script type="text/javascript">
<!--
payeeDest = "<ffi:getProperty name='${payeeTask}' property='PayeeDestination'/>";
PAYEE_REGULAR = "<%= WireDefines.PAYEE_TYPE_REGULAR %>";
PAYEE_BOOK = "<%= WireDefines.PAYEE_TYPE_BOOK %>";
PAYEE_DRAWDOWN = "<%= WireDefines.PAYEE_TYPE_DRAWDOWN %>";

function selectDestBank() {
    frm = document.WireNew;
    rtSwift = false;
    rtOther = false;
    rtChips = false;
    iban = false;

    bankName = frm["<ffi:getProperty name="payeeTask"/>.DestinationBank.BankName"].value == "" ? false : true;
    bankCity = frm["<ffi:getProperty name="payeeTask"/>.DestinationBank.City"].value == "" ? false : true;
    bankState = frm["<ffi:getProperty name="payeeTask"/>.DestinationBank.State"].value == "" ? false : true;
    bankCountry = frm["<ffi:getProperty name="payeeTask"/>.DestinationBank.Country"].value == "" ? false : true;
    rtFed = frm["<ffi:getProperty name="payeeTask"/>.DestinationBank.RoutingFedWire"].value == "" ? false : true;

    if (frm["<ffi:getProperty name="payeeTask"/>.PayeeDestination"].value != "") {
		payeeDest = frm["<ffi:getProperty name="payeeTask"/>.PayeeDestination"].value;
	}
    if (payeeDest == PAYEE_REGULAR) {
        rtSwift = frm["<ffi:getProperty name="payeeTask"/>.DestinationBank.RoutingSwift"].value == "" ? false : true;
        rtOther = frm["<ffi:getProperty name="payeeTask"/>.DestinationBank.RoutingOther"].value == "" ? false : true;
        rtChips = frm["<ffi:getProperty name="payeeTask"/>.DestinationBank.RoutingChips"].value == "" ? false : true;
        iban = frm["<ffi:getProperty name="payeeTask"/>.DestinationBank.IBAN"].value == "" ? false : true;
    }
    if (bankName || rtSwift || rtOther || rtFed || rtChips || bankCity || bankState || bankCountry || iban) {
    	frm["isBankSearch"].value="false";
    	frm["Inter"].value="no";
    	destURL = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/wirebanklistselect.jsp?Inter=no&Edit=false&currentTask=${payeeTask}'/>";
    }
    else {
    	frm["isBankSearch"].value="true";
    	frm["Inter"].value="no";
    	destURL = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/wirebanklist.jsp?Inter=no&Edit=false&currentTask=${payeeTask}'/>";
    }
    	actionURL = "/cb/pages/jsp/wires/getWireTransferBanksAction_init.action";
    ns.wire.searchBeneficiaryDestinationBankForm(destURL, actionURL);
}

function selectInterBank() {
    frm = document.WireNew;
   	actionURL = "/cb/pages/jsp/wires/getWireTransferBanksAction_init.action";
    targetURL = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/getWireTransferBanksAction_init.action?Inter=yes&isBankSearch=true&manageBeneficiary=${manageBeneficiary}&returnPage=${returnPage}'/>";	
 	ns.wire.searchDestinationBankForm(targetURL, actionURL);
}

function deleteInterBank(page,idx) {
	frm = document.WireNew;
	var action = frm["validateAction"].value;
	var targetUrl="";
	if(action=="addWireBeneficiaryAction_verify"){
		targetUrl = "/cb/pages/jsp/wires/addWireBeneficiaryAction_deleteIntermediary.action?delIntBank=" + idx
	} else {
		targetUrl = "/cb/pages/jsp/wires/modifyWirebeneficiary_deleteIntermediary.action?delIntBank=" + idx
	}
	$.ajax({   
		type: "POST",   
		//679869
		//url: "/cb/pages/jsp/wires/inc/wire_addedit_intermediary_banks.jsp",
		url: targetUrl,
		data: $("#newBeneficiaryFormID").serialize(),
		//data: {DontInitialize:"true", delIntBank:idx, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
		success: function(data) {
			$('#intermediaryBanksDivID').html(data);
		}   
	});
}

function changeType(t) {
    frm = document.WireNew;
  	var validAction = frm.validateAction.value;
	var url ;
	
	if(validAction.indexOf("add") != -1){
		url="addWireBeneficiaryAction_selectAffBank.action";
		 if (t == "<%= WireDefines.PAYEE_TYPE_REGULAR %>") url='addWireBeneficiaryAction_regular.action';
		    if (t == "<%= WireDefines.PAYEE_TYPE_DRAWDOWN %>") url='addWireBeneficiaryAction_drawdown.action';
		    if (t == "<%= WireDefines.PAYEE_TYPE_BOOK %>") url='addWireBeneficiaryAction_book.action';
	}
	if(validAction.indexOf("modify")  != -1){
		
		 if (t == "<%= WireDefines.PAYEE_TYPE_REGULAR %>") url='modifyWirebeneficiary_regular.action';
		    if (t == "<%= WireDefines.PAYEE_TYPE_DRAWDOWN %>") url='modifyWirebeneficiary_drawdown.action';
		    if (t == "<%= WireDefines.PAYEE_TYPE_BOOK %>") url='modifyWirebeneficiary_book.action';
	}
   
    ns.wire.submmitBeneficiaryForm("/cb/pages/jsp/wires/" + url);
}

var theChild = null;
var theTarget = null;
var theAction = null;
function closeDestBank() {
    frm = document.WireNew;
	if (theChild != null)
	{
		try
		{
			if (!theChild.closed)
				theChild.close();
		} catch(e) {}
		theChild = null;
		frm.target = theTarget;
		frm.action = theAction;
	}
}

/** Funcion to change the country. 
	console.error('wire_scripts_payee_js.jsp updateDBStateInformation');
	console.error('action=>'+document.WireNew.action);
}
*/

function updateDBStateInformation(obj) {
	//var urlString = '/cb/pages/jsp/wires/inc/wire_country_state.jsp';
	var urlString = '/cb/pages/jsp/wires/addWireBeneficiaryAction_getStateForCountry.action';
	
	$.ajax({
		   type: "POST",
		   url: urlString,
		   data: {countryName: obj.value, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
		   success: function(data){
				$("#wireStateSelectedId").html('<span></span>');
				$("#wireStateSelectedId").html(data);		   	
		   }
	});
}

// --></script>
