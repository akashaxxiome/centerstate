<%--
<p>This is the pending wire beneficiary page. This page is used to display pending
wire beneficiaries. It displays beneficiaries that are pending approval or have
 been rejected will be displayed in this new island.
 
 If a user clicks the cancel icon from the pending island, pending changes will be
 discarded and the entity will be moved to the wire beneficiaries island. Also the 
 entity will be removed from Pending Approvals page. Delete icon is not provided in
 the pending island because delete operation also requires approval. So a user has to
 cancel his pending changes first to delete the beneficiary which can then be submitted
 for approval. As soon as the user adds a beneficiary, its status would be marked as
 "Pending Approval". Clicking the edit icon would enable to user to change the information
 but still the beneficiary would have the Pending Approval status and would be available
 in the approvals page.
 </p>

Pages that request this page
----------------------------
wireaddpayee.jsp	
wireaddpayeebook.jsp
wireaddpayeedrawdown.jsp

It will display Edit icon, Discard icon, Approve icon

Pages this page requests
------------------------
wire_beneficiaries_summary.jsp

Pages included in this page
---------------------------

--%>

<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Start: Clean up DA information --%>
<ffi:removeProperty name="isBeneficiary"/>
<ffi:removeProperty name="ID"/>
<ffi:removeProperty name="Initialize"/>
<ffi:removeProperty name="DA_WIRE_PRESENT"/>
<ffi:removeProperty name="daItemId"/>
<ffi:removeProperty name="payeeTask"/>


<ffi:removeProperty name="disablePayeeType"/>
<ffi:removeProperty name="MultipleCategories"/>
<ffi:removeProperty name="IS_COUNTRY_CHANGED"/>
<ffi:removeProperty name="IsPending"/>
<%-- End: Clean up DA information --%>

<%-- Retrieve multiple category details information in session --%>
<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
	<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="<%=IDualApprovalConstants.TRUE%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
	<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
<ffi:process name="GetDACategoryDetails"/>

<ffi:removeProperty name="Entitlement_BeneficiaryManagement"/>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BENEFICIARY_MANAGEMENT %>">
	<ffi:setProperty name="Entitlement_BeneficiaryManagement" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BENEFICIARY_MANAGEMENT %>">
	<ffi:setProperty name="Entitlement_BeneficiaryManagement" value="false"/>
</ffi:cinclude>


<ffi:help id="payments_daWireBeneficiarySummary" className="moduleHelpClass"/>
	<ffi:setGridURL grid="GRID_daWireBeneficiary" name="daWirePayeeEditPage" url="/cb/pages/jsp/wires/modifyWirebeneficiary_init.action?IsPending=true&payeeId={0}&payeeDestination={1}" parm0="ID" parm1="wirePayeeDestination"/>	
	<ffi:setGridURL grid="GRID_daWireBeneficiary" name="daWirePayeeViewPage" url="/cb/pages/jsp/wires/viewWireBeneficiary.action?IsPending=true&payeeId={0}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_daWireBeneficiary" name="daWirePayeeDiscardPage" url="/cb/pages/jsp/wires/discardWireBeneficiary_init.action?itemType=WireBeneficiary&itemId={0}&userAction={1}" parm0="ID" parm1="Action"/>
	<ffi:setGridURL grid="GRID_daWireBeneficiary" name="daWirePayeeModifyPage" url="/cb/pages/jsp/user/da-modify-changes.jsp?itemType=WireBeneficiary&itemId={0}&DA_WIRE_PRESENT=true" parm0="ID"/>
	
	<ffi:setProperty name="wireBeneficiariesGridTempDAURL" value="/pages/jsp/wires/getPendingApprovalWireBeneficiaries.action?GridURLs=GRID_daWireBeneficiary" URLEncrypt="true" />

    <s:url id="daBeneficiariesWiresUrl" value="%{#session.wireBeneficiariesGridTempDAURL}" escapeAmp="false" includeParams="all"/>	
	<sjg:grid  
		id="daWireBeneficiariesGridID"  
		caption=""   
		dataType="json"   
		href="%{daBeneficiariesWiresUrl}"   
		pager="true"   
		gridModel="gridModel"  
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		sortable="true" 
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,daWireBeneficiariesGridCompleteEvents" 
		> 
		<sjg:gridColumn name="beneficiaryName" index="beneficiaryName" title="Name" sortable="true" width="135" formatter="ns.wire.customNameColumnForBeneficiaries"/>
		<sjg:gridColumn name="address" index="address" title="Address" sortable="true" width="85" formatter="ns.wire.customAddressColumnForBeneficiaries"/>
		<sjg:gridColumn name="accountNum" index="accountNum" title="Account Number" sortable="true" width="135"/>
		<sjg:gridColumn name="payeeDestination" index="payeeDestination" title="Type" sortable="true" width="70"/>
		<sjg:gridColumn name="payeeScope" index="payeeScope" title="Scope" sortable="true" width="70"/>
		<sjg:gridColumn name="destinationBank.bankName" index="destinationBank.bankName" title="Bank" sortable="true" width="140"/>
		<sjg:gridColumn name="daStatus" index="daStatus" title="Status" sortable="true" width="100"/>
		<sjg:gridColumn name="action" index="action" title="User Action" sortable="true" width="70"/>
		
		<sjg:gridColumn name="payeeName" index="payeeName" title="Payee Name" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="nickName" index="nickName" title="Nick Name" sortable="true"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="city" index="city" title="city" sortable="true" width="85" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="state" index="state" title="state" sortable="true" width="85" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="submittedBy" index="submittedBy" title="Submitted By" sortable="true" width="85" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="daItemId" index="daItemId" title="DA Item Id" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="edit" index="edit" title="Edit" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="discard" index="discard" title="Discard" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="modifyChange" index="modifyChange" title="Modify" sortable="true"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="view" index="view" title="View" sortable="true"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="reject" index="reject" title="Reject" sortable="true"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="rejectReason" index="rejectReason" title="Reject reason" sortable="true"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="rejectedBy" index="rejectedBy" title="Rejected By" sortable="true" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="map.daItemId" index="map.daItemId" title="DA Item Id" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.daWirePayeeViewPage" index="map.daWirePayeeViewPage" title="View" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.daWirePayeeEditPage" index="map.daWirePayeeEditPage" title="Edit" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.daWirePayeeDiscardPage" index="map.daWirePayeeDiscardPage" title="Discard" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.daWirePayeeModifyPage" index="map.daWirePayeeModifyPage" title="Modify Change" sortable="true" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="map.wirePayeeViewPage" index="map.wirePayeeViewPage" title="View" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.wirePayeeDeletePage" index="map.wirePayeeDeletePage" title="Delete" sortable="" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.wirePayeeEditPage" index="map.wirePayeeEditPage" title="Edit" sortable="true" hidden="true" hidedlg="true"/>

		<sjg:gridColumn name="ID" index="ID" title="Action" align="left" sortable="false" width="100" formatter="ns.wire.formatDAWireBeneficiariesActionLinks"  hidden="true" hidedlg="true" cssClass="__gridActionColumn"
		formatoptions="{ canManageBeneficiary : '%{#session.Entitlement_BeneficiaryManagement}'	}" 
		search="false" 
		/>		
	</sjg:grid>
	<script>
		$("#daWireBeneficiariesGridID").jqGrid('setColProp','ID',{title:false});
	</script>

