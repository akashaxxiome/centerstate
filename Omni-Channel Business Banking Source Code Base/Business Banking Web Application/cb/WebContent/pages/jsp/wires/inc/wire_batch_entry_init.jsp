<%--
This file contains initialization code used on all wire transfer pages specific
to adding/editing a wire to a batch

It is included in wire_common_init.jsp, which is included on all add/edit wire
transfer pages
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<% String batchTask = ""; String editWireTransfer = ""; %>
<ffi:getProperty name="wireBatch" assignTo="batchTask"/>
<ffi:getProperty name="editWireTransfer" assignTo="editWireTransfer"/>
<%
if (batchTask == null) batchTask = "AddWireBatch";
if (editWireTransfer == null) editWireTransfer = "false";
%>

<ffi:setProperty name="disableRec" value="disabled"/>
<ffi:setProperty name='freqNone' value='<option value="0"><!--L10NStart-->None<!--L10NEnd--></option>'/>
<ffi:setProperty name="wireAmountField" value="Amount"/>
<ffi:setProperty name="wireCurrencyField" value="AmtCurrencyType"/>

<ffi:removeProperty name="WireTransferPayee"/>
<ffi:removeProperty name="WireTransferBank"/>
<ffi:removeProperty name="WireTransfer"/>
<ffi:removeProperty name="ModifyWireTransfer"/>
<ffi:removeProperty name="AddWireTransfer"/>
<ffi:removeProperty name="FFIModifyWireTransfer"/>
<ffi:removeProperty name="FFIWireTransferPayee"/>
	
<ffi:setProperty name="GetWireTransferPayees" property="Reload" value="true"/>
<ffi:process name="GetWireTransferPayees"/>

<ffi:cinclude value1="${loadFromTemplate}" value2="" operator="equals">
	<ffi:setProperty name="<%= batchTask %>" property="WireFreeFormFlag" value="true"/>
	<ffi:setProperty name="<%= batchTask %>" property="WireParameter" value="${wireIndex}"/>
</ffi:cinclude>

<ffi:cinclude value1="${loadFromTemplate}" value2="" operator="notEquals">
	<% // remember the checkboxes from the previous page
	String itemsPerPageStr = ""; String pageStr = ""; String pagesStr = ""; %>
	<ffi:getProperty name="wireBatchItemsPerPage" assignTo="itemsPerPageStr"/>
	<ffi:getProperty name="templatePage" assignTo="pageStr"/>
	<ffi:getProperty name="templatePages" assignTo="pagesStr"/>
	<%
	int itemsPerPage = Integer.parseInt(itemsPerPageStr);

	// get current page number
	if (pageStr == null) pageStr = "0";
	int pageNum = Integer.parseInt(pageStr);

	// calculate starting and ending wire for this current page
	int startNum = pageNum * itemsPerPage;
	int endNum = startNum + itemsPerPage;
	int count;
	for (count = startNum; count < endNum; count++) {
		String checkbox = "checkbox_" + String.valueOf(count);
		if (request.getParameter(checkbox) != null) { %>
			<ffi:setProperty name="<%= checkbox %>" value="true"/>
	<%      } else { %>
			<ffi:setProperty name="<%= checkbox %>" value="false"/>
	<%      }
	} %>

	<ffi:setProperty name="<%= batchTask %>" property="WireFromTemplateFlag" value="true"/>
	<ffi:setProperty name="<%= batchTask %>" property="WireParameter" value="${loadFromTemplate}"/>

	<% String wireBak = ""; String parm = ""; %>
	<ffi:getProperty name="wireBackURL" assignTo="wireBak"/>
	<ffi:getProperty name="loadFromTemplate" assignTo="parm"/>
	<% wireBak += "&cancelFromBatch=" + parm.substring(0,parm.indexOf(";")); %>
	<ffi:setProperty name="wireBackURL" value="<%= wireBak %>"/>
</ffi:cinclude>

<% if (batchTask.equals("ModifyWireBatch")) {
		if (editWireTransfer != null && editWireTransfer.equalsIgnoreCase("true")) { %>
			<ffi:object id="SetWireTransferFromBatch" name="com.ffusion.tasks.wiretransfers.SetWireTransferFromBatch"/>
			<ffi:setProperty name="SetWireTransferFromBatch" property="Index" value="${wireIndex}"/>
			<ffi:setProperty name="SetWireTransferFromBatch" property="CollectionSessionName" value="ModifyWireBatch"/>
			<ffi:setProperty name="SetWireTransferFromBatch" property="BeanSessionName" value="WireTransfer"/>
			<ffi:process name="SetWireTransferFromBatch"/>

			<ffi:cinclude value1="${WireTransfer.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
				<ffi:setProperty name="wireAmountField" value="OrigAmount"/>
				<ffi:setProperty name="wireCurrencyField" value="OrigCurrency"/>
			</ffi:cinclude>
			
<%		} else { %>
			<ffi:object id="WireTransfer" name="com.ffusion.tasks.wiretransfers.AddWireTransfer"/>
			<ffi:setProperty name="WireTransfer" property="WireDestination" value="${wireDestination}"/>

<%		}
	} else { %>
		<ffi:setProperty name="<%= batchTask %>" property="WireSessionName" value="WireTransfer"/>
		<ffi:process name="<%= batchTask %>"/>
		<ffi:cinclude value1="${loadFromTemplate}" value2="" operator="equals">
			<ffi:cinclude value1="${editWireTransfer}" value2="true" operator="notEquals">
				<ffi:setProperty name="WireTransfer" property="WireDestination" value="${wireDestination}"/>
				<% Integer fundsType = new Integer(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_WIRE_TRANSFER); %>
				<ffi:setProperty name="WireTransfer" property="Type" value="<%= fundsType.toString() %>"/>
			</ffi:cinclude>
		</ffi:cinclude>
<%      } %>

<ffi:object id="WireTransferPayee" name="com.ffusion.beans.wiretransfers.WireTransferPayee" scope="session"/>
<ffi:object id="WireTransferBank" name="com.ffusion.beans.wiretransfers.WireTransferBank" scope="session"/>

<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.dateFormat}"/>
<ffi:setProperty name="WireTransfer" property="DateFormat" value="${UserLocale.dateFormat}"/>
<% String duedate = ""; String settledate = ""; %>
<ffi:getProperty name="<%= batchTask %>" property="DueDate" assignTo="duedate"/>
<ffi:getProperty name="<%= batchTask %>" property="SettlementDate" assignTo="settledate"/>

<%
if (duedate == null) duedate = "";
if (settledate == null) settledate = "";
%>

<ffi:setProperty name="WireTransfer" property="DueDate" value="<%= duedate %>"/>
<ffi:setProperty name="WireTransfer" property="SettlementDate" value="<%= settledate %>"/>

<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_TEMPLATE %>" operator="equals">
	<ffi:setProperty name="WireTransfer" property="WireType" value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_SINGLE %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="equals">
	<ffi:setProperty name="WireTransfer" property="WireType" value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_RECURRING %>"/>
</ffi:cinclude>

<ffi:cinclude value1="${WireTransfer.WirePayee}" value2="" operator="notEquals">
	<ffi:cinclude value1="${editWireTransfer}" value2="true">
		<ffi:object id="GetPayeeFromWireTransfer" name="com.ffusion.tasks.wiretransfers.GetPayeeFromWireTransfer" scope="session" />
			<ffi:setProperty name="GetPayeeFromWireTransfer" property="WireSessionName" value="WireTransfer"/>
			<ffi:setProperty name="GetPayeeFromWireTransfer" property="PayeeSessionName" value="WireTransferPayee"/>
		<ffi:process name="GetPayeeFromWireTransfer"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${editWireTransfer}" value2="true" operator="notEquals">
		<ffi:setProperty name="${WireTransfer}" property="WirePayee" value=""/>	
		<ffi:setProperty name="${wireTask}" property="WirePayeeID" value=""/>		
	</ffi:cinclude>
</ffi:cinclude>

<ffi:object id="SetWireTransferPayee" name="com.ffusion.tasks.wiretransfers.SetWireTransferPayee" scope="session"/>

<ffi:object id="GetWiresAccounts" name="com.ffusion.tasks.wiretransfers.GetWiresAccounts" scope="session"/>
	<ffi:setProperty name="GetWiresAccounts" property="Reload" value="true"/>
	<ffi:setProperty name="GetWiresAccounts" property="Type" value="${wireDestination}"/>
<ffi:process name="GetWiresAccounts"/>
<ffi:removeProperty name="GetWiresAccounts"/>

<ffi:object id="WireTransferTask" name="com.ffusion.tasks.wiretransfers.AddWireTransfer" scope="session"/>
<ffi:object id="GetWireTransferTask" name="com.ffusion.tasks.wiretransfers.GetWireTransferTask" scope="session"/>
<ffi:object id="RemoveIntermediaryBank" name="com.ffusion.tasks.wiretransfers.RemoveIntermediaryBank" scope="session" />
<% 
	session.setAttribute("FFIWireTransfer", session.getAttribute("WireTransfer"));
	session.setAttribute("FFIWireTransferPayee", session.getAttribute("WireTransferPayee"));
	session.setAttribute("FFIWireTransferBank", session.getAttribute("WireTransferBank"));

%>
