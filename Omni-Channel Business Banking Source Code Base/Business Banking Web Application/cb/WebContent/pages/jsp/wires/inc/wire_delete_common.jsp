<%--
This file contains common initialization code used on all delete wire
confirmation pages (not in batch).

It is included on all delete wire confirmation pages (except Host)
	wirebookdelete.jsp
	wiredrawdowndelete.jsp
	wirefeddelete.jsp
	wiretransferdelete.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="%{#session.PagesPath}/wires/inc/wire_common_init.jsp"/>
<ffi:setProperty name="wireBackURL" value="wiretransfers.jsp"/>
<ffi:setProperty name="wireDeleteURL" value="wiretransferdeletesend.jsp"/>

<ffi:setProperty name="wireDeleteButtonName" value="DELETE TRANSFER"/>

<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="equals" >
    <ffi:setProperty name="wireDeleteButtonName" value="DELETE TEMPLATE"/>
</ffi:cinclude>

<%-- <ffi:cinclude value1="${WireTransferPayees}" value2="" operator="equals">
	<ffi:object id="GetWireTransferPayees" name="com.ffusion.tasks.wiretransfers.GetWireTransferPayees" scope="session"/>
	<ffi:process name="GetWireTransferPayees"/>
</ffi:cinclude> --%>

<ffi:setProperty name="WireTransfer" property="DateFormat" value="${UserLocale.DateFormat}"/>
<ffi:removeProperty name="pendApproval"/>
