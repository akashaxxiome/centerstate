<%--
This is the view domestic and international wire page.

This page is not much more than a wrapper.  The main contents of the view
domestic and international wire page can be found in the common include file
common/include-view-wiretransfer.jsp

Pages that request this page
----------------------------
wire_transfers_list.jsp (included in wiretransfers.jsp, wire summary page)
	View button in a row containing a domestic or international wire
wire_templates_list.jsp (included in wiretransfers.jsp, wire summary page)
	View button in a row containing a domestic or international wire
wiresrelease.jsp
	View button in a row containing a domestic or international wire

Pages included in this page
---------------------------
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
payments/inc/wire_set_wire.jsp
	Sets a wire transfer or wire batch with a given ID into the session
payments/include-view-transaction-details-constants.jsp
	does nothing other than include common/include-view-transaction-details-constants.jsp
	which sets values in the session used in displaying the view page
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
common/include-view-wiretransfer.jsp
	A common view page for domestic and international wires used by many parts of CB and BC
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
session.setAttribute("ID", request.getParameter("ID"));
session.setAttribute("recID", request.getParameter("recID"));
session.setAttribute("collectionName", request.getParameter("collectionName"));
session.setAttribute("wireInBatch", request.getParameter("wireInBatch"));
%>

<ffi:setProperty name="DimButton" value=""/>

<ffi:cinclude value1="${WireTransferFrequencies}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.ResourceList" id="WireTransferFrequencies" scope="session"/>
    <ffi:setProperty name="WireTransferFrequencies" property="ResourceFilename" value="com.ffusion.beansresources.wiretransfers.resources"/>
    <ffi:setProperty name="WireTransferFrequencies" property="ResourceID" value="WireFrequencies"/>
    <ffi:process name="WireTransferFrequencies" />
	<ffi:getProperty name="WireTransferFrequencies" property="Resource"/>
</ffi:cinclude>

<ffi:cinclude value1="${WireTransferFrequenciesNames}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.Resource" id="WireTransferFrequenciesNames" scope="session"/>
    <ffi:setProperty name="WireTransferFrequenciesNames" property="ResourceFilename" value="com.ffusion.beansresources.wiretransfers.resources"/>
    <ffi:process name="WireTransferFrequenciesNames" />
</ffi:cinclude>

<ffi:cinclude value1="${WireAccountTypes}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.ResourceList" id="WireAccountTypes" scope="session"/>
    <ffi:setProperty name="WireAccountTypes" property="ResourceFilename" value="com.ffusion.beansresources.wiretransfers.resources"/>
    <ffi:setProperty name="WireAccountTypes" property="ResourceID" value="AccountTypes"/>
    <ffi:process name="WireAccountTypes" />
	<ffi:getProperty name="WireAccountTypes" property="Resource"/>
</ffi:cinclude>

<ffi:cinclude value1="${WireAccountType}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.Resource" id="WireAccountType" scope="session"/>
    <ffi:setProperty name="WireAccountType" property="ResourceFilename" value="com.ffusion.beansresources.wiretransfers.resources"/>
    <ffi:process name="WireAccountType" />
</ffi:cinclude>

<%-- Set the wire into the session --%>
<ffi:setProperty name="InViewHistory" value="true"/>
<s:include value="%{#session.PagesPath}/wires/inc/wire_set_wire.jsp"/>
<ffi:removeProperty name="InViewHistory"/>

<ffi:object id="GetAuditWireTransferById" name="com.ffusion.tasks.wiretransfers.GetAuditWireTransferById" scope="request" />
    <ffi:setProperty name="GetAuditWireTransferById" property="TrackingID" value="${WireTransfer.TrackingID}"/>
    <ffi:setProperty name="GetAuditWireTransferById" property="ID" value="${WireTransfer.ExternalID}"/>
    <ffi:setProperty name="GetAuditWireTransferById" property="CollectionSessionName" value="${collectionName}"/>
    <ffi:setProperty name="GetAuditWireTransferById" property="BeanSessionName" value="WireTransferHistory"/>
    <ffi:cinclude value1="${WireTransfer.WireType}" value2="TEMPLATE" operator="equals">
        <ffi:setProperty name="GetAuditWireTransferById" property="Template" value="true"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${WireTransfer.WireType}" value2="RECTEMPLATE" operator="equals">
        <ffi:setProperty name="GetAuditWireTransferById" property="Template" value="true"/>
    </ffi:cinclude>
<ffi:process name="GetAuditWireTransferById"/>

<ffi:include page="../wires/inc/include-view-transaction-details-constants.jsp"/>

        <div align="center">
           
            <table width="750" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td><img src="/cb/web/multilang/grafx/payments/corner_al.gif" alt="" width="11" height="7" border="0"></td>
                    <td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="728" height="7" border="0"></td>
                    <td align="right" class="ltrow1_color"><img src="/cb/web/multilang/grafx/payments/corner_ar.gif" alt="" width="11" height="7" border="0"></td>
                </tr>
                <tr>
                    <td></td>
                    <td class="ltrow2_color">
            <s:include value="%{#session.PagesPath}/common/include-view-wiretransfer.jsp"/>
                    </td>
            <td align="right" background="/cb/web/multilang/grafx/payments/corner_cellbg.gif">
            <br><br>
            </td>
        </tr>
        <tr>
            <td><img src="/cb/web/multilang/grafx/payments/corner_bl.gif" alt="" width="11" height="7" border="0"></td>
            <td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="728" height="7" border="0"></td>
            <td><img src="/cb/web/multilang/grafx/payments/corner_br.gif" alt="" width="11" height="7" border="0"></td>
        </tr>
        </table>
        </div>

