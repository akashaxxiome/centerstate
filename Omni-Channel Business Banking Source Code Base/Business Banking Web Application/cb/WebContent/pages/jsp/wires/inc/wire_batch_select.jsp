<%--
This page is an included file that creates the Choose Wire Batch Type drop-down
list.

It is included in the following files:
	wirebatchff.jsp
	wirebatchhost.jsp
	wirebatchtemp.jsp
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<% String curDest = ""; %>
<ffi:getProperty name="AddWireBatch" property="BatchDestination" assignTo="curDest"/>

<ffi:setProperty name="PageText" value="<span class='sectionsubhead'>Choose Wire Batch Type <select class='txtbox' onchange='setDest(this.value);'>"/>

<ffi:object id="GetEntitledWireTypes" name="com.ffusion.tasks.wiretransfers.GetEntitledWireTypes" scope="session"/>
<ffi:process name="GetEntitledWireTypes"/>

<% String temp = ""; %>
<ffi:list collection="GetEntitledWireTypes.EntitledWireTypes" items="wireType">
	<ffi:getProperty name="wireType" assignTo="temp"/>
	<% if (temp.equals(curDest)) { %>
		<ffi:setProperty name="_selected" value="selected"/>
	<% } else { %>
		<ffi:setProperty name="_selected" value=""/>
	<% } %>
	<ffi:setProperty name="WireDestinations" property="Key" value="${wireType}"/>
	<ffi:setProperty name="PageText" value="${PageText}<option value='${wireType}' ${_selected}>${WireDestinations.Value}</option>"/>
</ffi:list>

<ffi:setProperty name="PageText" value="${PageText}</select></span>"/>
<ffi:removeProperty name="_selected"/>
