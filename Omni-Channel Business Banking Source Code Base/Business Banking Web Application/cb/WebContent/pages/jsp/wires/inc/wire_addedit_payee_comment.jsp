<%--
This file contains the beneficiary comment field

It is included on the add/edit wire beneficiary pages:
	wireaddpayee.jsp
	wireaddpayeebook.jsp
	wireaddpayeedrawdown.jsp

It includes the following file:
common/wire_labels.jsp
	The labels for fields and buttons
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="IsPayeeCommentPage" value="true"/>
	<ffi:setProperty name="WireBeneficiaryCategory" value="<%=IDualApprovalConstants.CATEGORY_WIRE_PAYEE%>"/>
	<ffi:cinclude value1="${ID}" value2="" operator="notEquals">
		<s:include value="%{#session.PagesPath}/wires/inc/da_populate_beneficiary_values.jsp" />
	</ffi:cinclude>
	<ffi:cinclude value1="${payeeTask}" value2="AddWireTransferPayee">
		<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
		<ffi:removeProperty name="<%=IDualApprovalConstants.OLD_DA_OBJECT%>"/>
	</ffi:cinclude>
</ffi:cinclude>
<%-- End: Dual approval processing--%>

<% String payeeDest = ""; %>
<ffi:getProperty name="destination" assignTo="payeeDest"/>

<table border="0" cellspacing="0" cellpadding="3" width="100%">
	<tr><td valign="top"><hr class="thingrayline lineBreak"></td></tr>
	<tr>
			<td class='<ffi:getPendingStyle fieldname="memo" defaultcss="sectionhead nameTitle"  dacss="sectionheadDA"/>' style="cursor:auto"><h2><%= payeeDest.equals(WireDefines.PAYEE_TYPE_DRAWDOWN) ? "<!--L10NStart-->Debit Account<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> <!--L10NStart-->Comment<!--L10NEnd--></h2></td>
	</tr>
	<tr>
	<td></td>
	</tr>
	<tr>
		<td>
			<input name="<ffi:getProperty name="payeeTask"/>.Memo" value="<ffi:getProperty name="${payeeTask}" property="Memo"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="240" size="60" style="width:325">
			<span class="sectionhead_greyDA"><ffi:getProperty name="oldDAObject" property="Memo" /></span>
		</td>
	</tr>
</table>
<%-- Start: Clean up DA information --%>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
<ffi:removeProperty name="<%=IDualApprovalConstants.OLD_DA_OBJECT%>"/>
<%-- End: Clean up DA information --%>

