<%--
This file contains the batch-specific fields for a non-international wire batch

It is included on all edit wire batch pages
	wirebatcheditff.jsp
	wirebatchedithost.jsp
	wirebatchedittemplate.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%
String typeHost = com.ffusion.beans.wiretransfers.WireDefines.WIRE_BATCH_TYPE_HOST;
String wireBatchPage = ""; String disableInt = ""; %>
<ffi:getProperty name="wireBatchObject" property="BatchType" assignTo="wireBatchPage"/>
<%
if (wireBatchPage == null) wireBatchPage = "";
if (wireBatchPage.equals(typeHost)) disableInt = "disabled";
%>

					<div class="sectionsubhead marginTop5"><!--L10NStart--><label for="batchName">Batch Name:</label><!--L10NEnd--><span class="required" title="required">*</span></div>
					<div class="marginTop5"><input type="Text" id="batchName" name="wireBatchObject.BatchName" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="wireBatchObject" property="BatchName"/>" size="35" maxlength="128"></div>
					<div class="sectionsubhead marginTop10"><!--L10NStart-->Application Type<!--L10NEnd--><span class="required" title="required">*</span>: <ffi:getProperty name="wireBatchObject" property="BatchType"/></div>
					<div class="sectionsubhead marginTop10">
						<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= typeHost %>" operator="notEquals"><!--L10NStart--><label for="Date">Value Date</label><!--L10NEnd--><span class="required" title="required">*</span> :</ffi:cinclude>
						<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= typeHost %>" operator="equals"><!--L10NStart--><label for="Date">Processing Date</label><!--L10NEnd--><span class="required" title="required">*</span> :</ffi:cinclude>
						<ffi:setProperty name="dueDateForDP" value="${wireBatchObject.DueDate}"/>
						<sj:datepicker value="%{#session.dueDateForDP}" id="Date" name="wireBatchObject.DueDateString" label="Date" cssStyle="width:110px"
												maxlength="10" buttonImageOnly="true" minDate="0" cssClass="ui-widget-content ui-corner-all"/><span id="DueDateError"></span>
					</div>
					<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= typeHost %>" operator="equals">
						<div class="sectionsubhead marginTop5"><!--L10NStart--><label for="SettlementDate">Settlement Date:</label><!--L10NEnd-->
							<ffi:setProperty name="settlementDateForDP" value="${wireBatchObject.SettlementDate}"/>
							<sj:datepicker value="%{#session.settlementDateForDP}" id="SettlementDate" name="wireBatchObject.SettlementDateString" label="Date" cssStyle="width:110px"
													maxlength="10" buttonImageOnly="true" minDate="0" cssClass="ui-widget-content ui-corner-all"/><span id="SettlementDateError"></span>
								 <script>
								 $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>'); 		
								 </script>
								 			
						</div>
					</ffi:cinclude>
