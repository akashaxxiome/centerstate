<%--
This file contains common initialization code used on all add/edit wire transfer
pages (except Host).  This page also includes the logic used when selecting/
unselecting a beneficiary in the Choose Beneficiary drop-down list, and the
logic used when deleting an intermediary bank.

It is included on all add/edit wire transfer pages (except Host)
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp
	wiretransfernew.jsp

It includes the following files:
common/wire_labels.jsp
	The labels for fields and buttons
payments/inc/wire_new_init.jsp
	Initialization code specific to adding a wire transfer
payments/inc/wire_edit_init.jsp
	Initialization code specific to modifying a wire transfer
payments/inc/wire_batch_entry_init.jsp
	Initialization code specific to adding/editing a wire in a batch
payments/inc/wire_set_disabled.jsp
	Sets the various disabled flags used on the add/edit wire transfer pages
payments/inc/wire_type_select.jsp
	Creates the Choose Wire Type drop-down list

A string in the session called wireTask determines which initialization file to
include.  Valid values are "AddWireTransfer", "ModifyWireTransfer", and
"WireTransfer", for add single wire, modify single wire, and add/edit wire in
batch, respectively.
--%>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%@ include file="../../common/wire_labels.jsp"%>

<ffi:setProperty name="wireTask" value="wireTransfer"/>
<ffi:setProperty name="payeeTask" value="wireTransferPayee"/>

<span id="PageHeading" style="display:none;"><ffi:getProperty name='PageHeading'/></span>

<%-- Determine which fields need to be disabled, and set the appropriate flags --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_set_disabled.jsp"/>

<%-- CHOOSE WIRE TYPE PICK LIST (this sets the pick list into the PageText variable) --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_type_select.jsp"/>

<%-- Start: Dual approval processing --%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="NO_BASIC_DUAL_APPROVAL_SUPPORT">
	<ffi:removeProperty name="WireTransferPayeesDA"/>	
</ffi:cinclude>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>">
	<ffi:removeProperty name="IS_DA_WIRE_TRANSFER"/>
	<ffi:setProperty name="IS_DA_WIRE_TRANSFER" value="true"/>
</ffi:cinclude>

<%-- End: Dual approval processing--%>
<ffi:removeProperty name="isBeneficiary"/>
