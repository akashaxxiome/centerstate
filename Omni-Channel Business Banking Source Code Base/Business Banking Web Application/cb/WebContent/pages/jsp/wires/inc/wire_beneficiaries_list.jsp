<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_wireBeneficiarySummary" className="moduleHelpClass"/>
<ffi:setGridURL grid="GRID_wireBeneficiary" name="EditURL" url="/cb/pages/jsp/wires/modifyWirebeneficiary_init.action?payeeId={0}&payeeDestination={1}" parm0="ID" parm1="wirePayeeDestination"/>	
	<ffi:setGridURL grid="GRID_wireBeneficiary" name="DeleteURL" url="/cb/pages/jsp/wires/deleteWireBeneficiary_init.action?payeeId={0}&payeeDestination={1}" parm0="ID" parm1="wirePayeeDestination"/>
	<ffi:setGridURL grid="GRID_wireBeneficiary" name="ViewURL" url="/cb/pages/jsp/wires/viewWireBeneficiary.action?IsPending=&payeeId={0}" parm0="ID"/>

	<ffi:setProperty name="beneficiariesWiresTempURL" value="/pages/jsp/wires/getWireBeneficiaries.action?GridURLs=GRID_wireBeneficiary" URLEncrypt="true"/>
    <s:url id="beneficiariesWiresUrl" value="%{#session.beneficiariesWiresTempURL}" escapeAmp="false" includeParams="all"/>
	<sjg:grid  
		id="wireBeneficiariesGridID"  
		caption=""  
		dataType="json"  
		href="%{beneficiariesWiresUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"		
		sortable="true"
		scroll="false" 
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,wireBeneficiariesGridCompleteEvents"
		> 
		<sjg:gridColumn name="beneficiaryName" index="beneficiaryName" title="%{getText('jsp.wire.Beneficiary_Name')}" sortable="true" width="135" formatter="ns.wire.customNameColumnForBeneficiaries"/>
		<sjg:gridColumn name="address" index="address" title="%{getText('jsp.wire.Address')}" sortable="true" width="85" formatter="ns.wire.customAddressColumnForBeneficiaries"/>
		<sjg:gridColumn name="accountNum" index="accountNum" title="%{getText('jsp.wire.Account_Number')}" sortable="true" width="135"/>
		<sjg:gridColumn name="payeeDestination" index="payeeDestination" title="%{getText('jsp.wire.Type')}" sortable="true" width="55"/>
		<sjg:gridColumn name="payeeScope" index="payeeScope" title="%{getText('jsp.wire.Scope')}" sortable="true" width="50"/>
		<sjg:gridColumn name="destinationBank.bankName" index="destinationBank.bankName" title="%{getText('jsp.wire.BankName')}" sortable="true" width="125"/>
				
		<sjg:gridColumn name="canLoad" index="canLoad" title="Can Load" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="canDelete" index="canDelete" title="Can Delete" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="canEdit" index="canEdit" title="Can Edit" sortable="true" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="payeeName" index="payeeName" title="%{getText('jsp.wire.payeeName')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="nickName" index="nickName" title="%{getText('jsp.wire.nickName')}" sortable="true"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="city" index="city" title="%{getText('jsp.wire.city')}" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="state" index="state" title="%{getText('jsp.wire.state')}" sortable="true" hidden="true" hidedlg="true"/>
		
		<sjg:gridColumn name="map.wirePayeeViewPage" index="map.wirePayeeViewPage" title="View" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.wirePayeeDeletePage" index="map.wirePayeeDeletePage" title="Delete" sortable="" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.wirePayeeEditPage" index="map.wirePayeeEditPage" title="Edit" sortable="true" hidden="true" hidedlg="true"/>

		<sjg:gridColumn name="map.EditURL" index="EditURL" title="EditURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="DeleteURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="ViewURL" search="false" sortable="false" hidden="true" hidedlg="true"/>

		<sjg:gridColumn name="ID" index="ID" align="right" title="Action" sortable="false" width="100" formatter="ns.wire.formatWireBeneficiariesActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
	</sjg:grid>
	<script>
		$("#wireBeneficiariesGridID").jqGrid('setColProp','ID',{title:false});
	</script>
	<ffi:removeProperty name="tempURL"/>
