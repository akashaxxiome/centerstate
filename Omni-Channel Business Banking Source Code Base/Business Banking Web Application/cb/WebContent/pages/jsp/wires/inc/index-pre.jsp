<%@ page import="com.ffusion.beans.wiretransfers.WireDefines,
				 com.ffusion.beans.user.UserLocale" 
%>
				 
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
	String init = request.getParameter("Initialize");
	if( init != null ){
		session.setAttribute("Initialize", init);
	}
%>
<ffi:cinclude value1="${GetTemplatesAccounts}" value2="" operator="equals">
	<ffi:object id="GetTemplatesAccounts" name="com.ffusion.tasks.wiretransfers.GetTemplatesAccounts" scope="session"/>
		<ffi:setProperty name="GetTemplatesAccounts" property="Reload" value="true"/>
	<ffi:process name="GetTemplatesAccounts"/>
	<ffi:setProperty name="templateAccountsSize" value="${WiresAccounts.Size}"/>
	<ffi:removeProperty name="GetTemplatesAccounts"/>
</ffi:cinclude>

<!---This task is used to get all accounts for business-->
    <ffi:object name="com.ffusion.tasks.accounts.GetAccountsById" id="GetAccountsById" scope="session" />
        <ffi:setProperty name="GetAccountsById" property="DirectoryId" value="${Business.Id}"/>
        <ffi:setProperty name="GetAccountsById" property="Reload" value="true"/>
         <ffi:setProperty name="GetAccountsById" property="AccountsName" value="BusinessAccounts"/>
    <ffi:process name="GetAccountsById"/>
    

<ffi:cinclude value1="${GetSupportRelease}" value2="" operator="equals">
	<ffi:object id="GetSupportRelease" name="com.ffusion.tasks.wiretransfers.GetSupportRelease" scope="session"/>
	<ffi:process name="GetSupportRelease"/>
</ffi:cinclude>

<ffi:setL10NProperty name='PageHeading' value='Wire Transfers'/>

<ffi:setProperty name="DimButton" value=""/>
<s:include value="/pages/jsp/wires/inc/wire_buttons.jsp"/>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wiretransfers.jsp?DontInitialize=true" URLEncrypt="true"/>
<ffi:setProperty name="releaseBackURL" value="${SecurePath}payments/wiretransfers.jsp?Refresh=true" URLEncrypt="true"/>

<%--
<% if ("true".equals(request.getParameter("DontInitialize")) ) { %>
    <ffi:removeProperty name="wireStartDate, wireEndDate"/>
<% } %>
--%>
	<ffi:removeProperty name="wireStartDate, wireEndDate"/>

<%
	String startDate = request.getParameter("wireStartDate");
	String endDate = request.getParameter("wireEndDate");
	if(startDate != null && startDate.length() > 0){
		session.setAttribute("wireStartDate", startDate);
	}
	if(endDate != null && endDate.length() > 0){
		session.setAttribute("wireEndDate", endDate);
	}
	String viewAll = request.getParameter("viewAll");
	String showWireType = request.getParameter("showWireType");
	if( viewAll != null &&  viewAll.length() > 0){
		session.setAttribute("viewAll", viewAll);
	}
	if( showWireType != null ){
		session.setAttribute("showWireType", showWireType);
	}
%>

<ffi:removeProperty name="wireTask"/>
<ffi:removeProperty name="templateTask"/>
<ffi:removeProperty name="DontInitialize"/>
<ffi:removeProperty name="DontInitializeBatch"/>
<ffi:removeProperty name="LoadFromTransferTemplate" />

<ffi:cinclude value1="${AccountType}" value2="" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.Resource" id="AccountType" scope="session"/>
	<ffi:setProperty name="AccountType" property="ResourceFilename" value="com.ffusion.beansresources.accounts.resources"/>
	<ffi:process name="AccountType" />
</ffi:cinclude>

<ffi:cinclude value1="${WireDestinations}" value2="" operator="equals">
	<ffi:object id="WireDestinations" name="com.ffusion.beans.util.StringTable" scope="session" />
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_DOMESTIC %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="Domestic"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_INTERNATIONAL %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="International"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_FED %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="FED Wire"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_DRAWDOWN %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="Drawdown"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_BOOK %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="Book"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_HOST %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="Host"/>
</ffi:cinclude>

<ffi:cinclude value1="${WireTransTypes}" value2="" operator="equals">
	<ffi:object id="WireTransTypes" name="com.ffusion.beans.util.StringTable" scope="session" />
	<ffi:setProperty name="WireTransTypes" property="Key" value="<%= WireDefines.WIRE_TYPE_SINGLE %>"/>
	<ffi:setProperty name="WireTransTypes" property="Value" value="Single"/>
	<ffi:setProperty name="WireTransTypes" property="Key" value="<%= WireDefines.WIRE_TYPE_RECURRING %>"/>
	<ffi:setProperty name="WireTransTypes" property="Value" value="Recurring"/>
	<ffi:setProperty name="WireTransTypes" property="Key" value="<%= WireDefines.WIRE_TYPE_BATCH %>"/>
	<ffi:setProperty name="WireTransTypes" property="Value" value="Batch"/>
</ffi:cinclude>

<ffi:cinclude value1="${WirePayeeScopes}" value2="" operator="equals">
	<ffi:object id="WirePayeeScopes" name="com.ffusion.beans.util.StringTable" scope="session" />
	<ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_BUSINESS %>"/>
	<ffi:setProperty name="WirePayeeScopes" property="Value" value="Business"/>
	<ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_USER %>"/>
	<ffi:setProperty name="WirePayeeScopes" property="Value" value="User"/>
	<ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_UNMANAGED %>"/>
	<ffi:setProperty name="WirePayeeScopes" property="Value" value="One-Off"/>
</ffi:cinclude>

<ffi:setProperty name='noSortImage' value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name='ascSortImage' value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="Reverse Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name='descSortImage' value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="Sort" width="24" height="8" border="0">'/>

<%
if (request.getParameter("GetPendingWireHistories.SortedBy") == null &&
	request.getParameter("GetPendingWireHistories.PreviousPage") == null &&
	request.getParameter("GetPendingWireHistories.NextPage") == null &&
	request.getParameter("GetCompletedWireHistories.SortedBy") == null &&
	request.getParameter("GetCompletedWireHistories.PreviousPage") == null &&
	request.getParameter("GetCompletedWireHistories.NextPage") == null &&
	request.getParameter("GetApprovalWireHistories.SortedBy") == null &&
	request.getParameter("GetApprovalWireHistories.PreviousPage") == null &&
	request.getParameter("GetApprovalWireHistories.NextPage") == null) {
	// reset the sort name if Refresh is not on the request (the user did not click Refresh View)
	//boolean resetSort = (request.getParameter("Refresh") == null);
	boolean resetSort = false; // Do not reset the sort criteria, let the user decide
	String currentSort = "";
	String currentSortCriteria = "";
	String viewAllType = null;
	%>
	  <ffi:getProperty name="viewAll" assignTo="viewAllType"/>
	<%
	  // reset the View All flag if viewAll is not on the request (the user did not click Refresh View)
	  if (viewAllType == null) { %>
	   <ffi:setProperty name="viewAll" value="false"/>
        <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
        <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
        <ffi:process name="ACHResource" />
        <ffi:setProperty name="ACHResource" property="ResourceID" value="DISPLAY_PERSONAL_BUSINESS"/>
        <% String bus_or_pers = ""; %>
        <ffi:getProperty name="ACHResource" property="Resource" assignTo="bus_or_pers" />
        <% if (bus_or_pers != null && bus_or_pers.equalsIgnoreCase("Business")) { %>
            <ffi:setProperty name="viewAll" value="true"/>
        <% } %>
	<%  } %>
 

	<ffi:object id="GetDatesFromDateRange" name="com.ffusion.tasks.util.GetDatesFromDateRange" scope="session"/>
		<ffi:setProperty name="GetDatesFromDateRange" property="DateRangeValue" value="Last X Days and Next X Days"/>
		<ffi:setProperty name="GetDatesFromDateRange" property="PreviousDays" value="30"/>
		<ffi:setProperty name="GetDatesFromDateRange" property="FutureDays" value="30"/>
	<ffi:process name="GetDatesFromDateRange"/>

	<ffi:cinclude value1="${wireStartDate}" value2="" operator="equals">
		<ffi:setProperty name="wireStartDate" value="${GetDatesFromDateRange.StartDate}" />
	</ffi:cinclude>
	<ffi:cinclude value1="${wireEndDate}" value2="" operator="equals">
		<ffi:setProperty name="wireEndDate" value="${GetDatesFromDateRange.EndDate}" />
	</ffi:cinclude>

	<ffi:object id="GetWiresAccounts" name="com.ffusion.tasks.wiretransfers.GetWiresAccounts" scope="session"/>
		<ffi:setProperty name="GetWiresAccounts" property="Reload" value="true"/>
	<ffi:process name="GetWiresAccounts"/>
	<ffi:removeProperty name="GetWiresAccounts"/>
	
	<% String initialize=""; %>
	
	<ffi:getProperty name="Initialize" assignTo="initialize"/>
	
	<% if ( initialize == null) initialize = ""; %>
	
	<ffi:getProperty name="GetPendingWireHistories" property="SortCriteriaName" assignTo="currentSort"/>
	<ffi:getProperty name="GetPendingWireHistories" property="SortCriteriaAsc" assignTo="currentSortCriteria"/>
	<% if (currentSort == null || initialize.equals("true")) 
		resetSort = true; 
	   else 
	   	resetSort = false;
	%>
	<ffi:object id="GetPendingWireHistories" name="com.ffusion.tasks.wiretransfers.GetPagedWireHistories" scope="session"/>
		<ffi:setProperty name="GetPendingWireHistories" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetPendingWireHistories" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetPendingWireHistories" property="FirstPage" value="true"/>
		<ffi:setProperty name="GetPendingWireHistories" property="PageSize" value=""/>
		<ffi:setProperty name="GetPendingWireHistories" property="StartDate" value="${wireStartDate}" />
		<ffi:setProperty name="GetPendingWireHistories" property="EndDate" value="${wireEndDate}" />
		<ffi:setProperty name="GetPendingWireHistories" property="CollectionSessionName" value="PendingWireTransfers" />
		<ffi:setProperty name="GetPendingWireHistories" property="Type" value="${showWireType}"/>
		<ffi:setProperty name="GetPendingWireHistories" property="Status" value="<%= WireDefines.WIRE_DATA_PENDING %>"/>
		<ffi:setProperty name="GetPendingWireHistories" property="ViewAll" value="${viewAll}"/>
		<ffi:setProperty name="GetPendingWireHistories" property="ClearSortCriteria" value=""/>
		<ffi:setProperty name="GetPendingWireHistories" property="SortCriteriaOrdinal" value="1"/>
		<ffi:setProperty name="GetPendingWireHistories" property="SortCriteriaName" value="<%= resetSort ? WireDefines.SORT_CRITERIA_DATETOPOST : currentSort %>"/>
		<ffi:setProperty name="GetPendingWireHistories" property="SortCriteriaAsc" value="<%= resetSort ? \"FALSE\" : currentSortCriteria %>"/>
		<ffi:setProperty name="GetPendingWireHistories" property="NoSortImage" value="${noSortImage}"/>
		<ffi:setProperty name="GetPendingWireHistories" property="AscendingSortImage" value="${ascSortImage}"/>
		<ffi:setProperty name="GetPendingWireHistories" property="DescendingSortImage" value="${descSortImage}"/>
	
	<ffi:getProperty name="GetCompletedWireHistories" property="SortCriteriaName" assignTo="currentSort"/>
	<ffi:getProperty name="GetCompletedWireHistories" property="SortCriteriaAsc" assignTo="currentSortCriteria"/>
	<% if (currentSort == null || initialize.equals("true")) 
		resetSort = true; 
	   else 
	   	resetSort = false;
	%>
		<ffi:object id="GetCompletedWireHistories" name="com.ffusion.tasks.wiretransfers.GetPagedWireHistories" scope="session"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="FirstPage" value="true"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="PageSize" value=""/>
		<ffi:setProperty name="GetCompletedWireHistories" property="StartDate" value="${wireStartDate}" />
		<ffi:setProperty name="GetCompletedWireHistories" property="EndDate" value="${wireEndDate}" />
		<ffi:setProperty name="GetCompletedWireHistories" property="CollectionSessionName" value="CompletedWireTransfers" />
		<ffi:setProperty name="GetCompletedWireHistories" property="Type" value="${showWireType}"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="Status" value="<%= WireDefines.WIRE_DATA_COMPLETED %>"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="ViewAll" value="${viewAll}"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="ClearSortCriteria" value=""/>
		<ffi:setProperty name="GetCompletedWireHistories" property="SortCriteriaOrdinal" value="1"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="SortCriteriaName" value="<%= resetSort ? WireDefines.SORT_CRITERIA_DATETOPOST : currentSort %>"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="SortCriteriaAsc" value="<%= resetSort ? \"FALSE\" : currentSortCriteria %>"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="NoSortImage" value="${noSortImage}"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="AscendingSortImage" value="${ascSortImage}"/>
		<ffi:setProperty name="GetCompletedWireHistories" property="DescendingSortImage" value="${descSortImage}"/>

	<ffi:getProperty name="GetApprovalWireHistories" property="SortCriteriaName" assignTo="currentSort"/>
	<ffi:getProperty name="GetApprovalWireHistories" property="SortCriteriaAsc" assignTo="currentSortCriteria"/>
	<% if (currentSort == null || initialize.equals("true")) 
			resetSort = true; 
		   else 
		   	resetSort = false;
	%>
	<ffi:object id="GetApprovalWireHistories" name="com.ffusion.tasks.wiretransfers.GetPagedWireHistories" scope="session"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="FirstPage" value="true"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="PageSize" value=""/>
		<ffi:setProperty name="GetApprovalWireHistories" property="StartDate" value="${wireStartDate}" />
		<ffi:setProperty name="GetApprovalWireHistories" property="EndDate" value="${wireEndDate}" />
		<ffi:setProperty name="GetApprovalWireHistories" property="CollectionSessionName" value="PendingApprovalWireTransfers" />
		<ffi:setProperty name="GetApprovalWireHistories" property="Type" value="${showWireType}"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="Status" value="<%= WireDefines.WIRE_DATA_APPROVAL %>"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="ViewAll" value="${viewAll}"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="ClearSortCriteria" value=""/>
		<ffi:setProperty name="GetApprovalWireHistories" property="SortCriteriaOrdinal" value="1"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="SortCriteriaName" value="<%= resetSort ? WireDefines.SORT_CRITERIA_DATETOPOST : currentSort %>"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="SortCriteriaAsc" value="<%= resetSort ? \"FALSE\" : currentSortCriteria %>"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="NoSortImage" value="${noSortImage}"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="AscendingSortImage" value="${ascSortImage}"/>
		<ffi:setProperty name="GetApprovalWireHistories" property="DescendingSortImage" value="${descSortImage}"/>

<%
	session.setAttribute("FFIGetApprovalWireHistories", session.getAttribute("GetApprovalWireHistories"));
	session.setAttribute("FFIGetCompletedWireHistories", session.getAttribute("GetCompletedWireHistories"));
	session.setAttribute("FFIGetPendingWireHistories", session.getAttribute("GetPendingWireHistories"));
%>

<%
} else if (request.getParameter("GetPendingWireHistories.PreviousPage") != null ||
           request.getParameter("GetPendingWireHistories.NextPage") != null) { %>
	<ffi:process name="GetPendingWireHistories"/>
<%
} else if (request.getParameter("GetCompletedWireHistories.PreviousPage") != null ||
           request.getParameter("GetCompletedWireHistories.NextPage") != null) { %>
	<ffi:process name="GetCompletedWireHistories"/>
<%
} else if (request.getParameter("GetApprovalWireHistories.PreviousPage") != null ||
           request.getParameter("GetApprovalWireHistories.NextPage") != null) { %>
	<ffi:process name="GetApprovalWireHistories"/>
<%
} else if (request.getParameter("GetPendingWireHistories.SortedBy") != null) { %>
	<ffi:setProperty name="GetPendingWireHistories" property="FirstPage" value="true"/>
	<ffi:process name="GetPendingWireHistories"/>
<%
} else if (request.getParameter("GetCompletedWireHistories.SortedBy") != null) { %>
	<ffi:setProperty name="GetCompletedWireHistories" property="FirstPage" value="true"/>
	<ffi:process name="GetCompletedWireHistories"/>
<%
} else if (request.getParameter("GetApprovalWireHistories.SortedBy") != null) { %>
	<ffi:setProperty name="GetApprovalWireHistories" property="FirstPage" value="true"/>
	<ffi:process name="GetApprovalWireHistories"/>
<%
} %>

<ffi:removeProperty name="Initialize" />

<ffi:object id="GetEntitledWireTypes" name="com.ffusion.tasks.wiretransfers.GetEntitledWireTypes" scope="session"/>
<ffi:process name="GetEntitledWireTypes"/>


<ffi:removeProperty name="GetAccountsById"/>
<ffi:removeProperty name="BusinessAccounts"/>