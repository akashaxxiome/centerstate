<%--
This file contains the Choose Wire Type dropdown list, which is actually
defined here in a string called PageText.

It's included in the following:
	wire_common_init.jsp (which is included in all add/edit wire transfer pages,
	except Host)
	wirehost.jsp (Host wire add/edit page)
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
String task = (String)session.getAttribute("wireTask");
if (task == null) task = "AddWireTransfer";
String templateTask = (String)session.getAttribute("templateTask");
if (templateTask == null) templateTask = "";
if (!(templateTask.equals("add") || templateTask.equals("edit"))) templateTask = "";
String batchTask = (String)session.getAttribute("wireBatch");
if (task.equals("AddWireTransfer") || task.equals("ModifyWireTransfer") || task.equals("AddHostWire") || task.equals("ModifyHostWire") || batchTask == null) batchTask = "";

String curDest = "";

if (task.equals("AddHostWire")) {
	curDest = WireDefines.WIRE_HOST;
   } else { %>
	<ffi:getProperty name="<%= task %>" property="WireDestination" assignTo="curDest"/>
<% } %>

<% if (task.equals("WireTransfer") || task.equals("ModifyWireTransfer") || task.equals("ModifyHostWire")) { // Is a wire in a batch, or we're editing a wire %>
	<ffi:setProperty name="PageText" value=""/>
<% } else { // Is a new wire or template %>

	<ffi:setProperty name="PageText" value="<span class='sectionsubhead'><!--L10NStart-->Choose Wire Type<!--L10NEnd--> <select class='txtbox' onchange='setDest(this.value);'>"/>
	<% if (templateTask.equals("")) { // Is not a template (use Get Entitled Wire Types) %>
		<ffi:object id="GetEntitledWireTypes" name="com.ffusion.tasks.wiretransfers.GetEntitledWireTypes" scope="session"/>
		<ffi:process name="GetEntitledWireTypes"/>
		
		<% String temp = ""; %>
		<ffi:list collection="GetEntitledWireTypes.EntitledWireTypes" items="wireType">
			<ffi:getProperty name="wireType" assignTo="temp"/>
			<% if (temp.equals(curDest)) { %>
				<ffi:setProperty name="_selected" value="selected"/>
			<% } else { %>
				<ffi:setProperty name="_selected" value=""/>
			<% } %>
			<ffi:setProperty name="WireDestinations" property="Key" value="${wireType}"/>
			<ffi:setProperty name="PageText" value="${PageText}<option value='${wireType}' ${_selected}>${WireDestinations.Value}</option>"/>
		</ffi:list>
	<% } else { // Is a template (show all wire types) %>
		<ffi:setProperty name="_wireType" value="<%= WireDefines.WIRE_DOMESTIC %>"/>
		<% if (curDest.equals(WireDefines.WIRE_DOMESTIC)) { %><ffi:setProperty name="_selected" value="selected"/><% } else { %><ffi:setProperty name="_selected" value=""/><% } %>
		<ffi:setProperty name="WireDestinations" property="Key" value="${_wireType}"/>
		<ffi:setProperty name="PageText" value="${PageText}<option value='${_wireType}' ${_selected}>${WireDestinations.Value}</option>"/>
		<ffi:setProperty name="_wireType" value="<%= WireDefines.WIRE_INTERNATIONAL %>"/>
		<% if (curDest.equals(WireDefines.WIRE_INTERNATIONAL)) { %><ffi:setProperty name="_selected" value="selected"/><% } else { %><ffi:setProperty name="_selected" value=""/><% } %>
		<ffi:setProperty name="WireDestinations" property="Key" value="${_wireType}"/>
		<ffi:setProperty name="PageText" value="${PageText}<option value='${_wireType}' ${_selected}>${WireDestinations.Value}</option>"/>
		<ffi:setProperty name="_wireType" value="<%= WireDefines.WIRE_BOOK %>"/>
		<% if (curDest.equals(WireDefines.WIRE_BOOK)) { %><ffi:setProperty name="_selected" value="selected"/><% } else { %><ffi:setProperty name="_selected" value=""/><% } %>
		<ffi:setProperty name="WireDestinations" property="Key" value="${_wireType}"/>
		<ffi:setProperty name="PageText" value="${PageText}<option value='${_wireType}' ${_selected}>${WireDestinations.Value}</option>"/>
		<ffi:setProperty name="_wireType" value="<%= WireDefines.WIRE_DRAWDOWN %>"/>
		<% if (curDest.equals(WireDefines.WIRE_DRAWDOWN)) { %><ffi:setProperty name="_selected" value="selected"/><% } else { %><ffi:setProperty name="_selected" value=""/><% } %>
		<ffi:setProperty name="WireDestinations" property="Key" value="${_wireType}"/>
		<ffi:setProperty name="PageText" value="${PageText}<option value='${_wireType}' ${_selected}>${WireDestinations.Value}</option>"/>
		<ffi:setProperty name="_wireType" value="<%= WireDefines.WIRE_FED %>"/>
		<% if (curDest.equals(WireDefines.WIRE_FED)) { %><ffi:setProperty name="_selected" value="selected"/><% } else { %><ffi:setProperty name="_selected" value=""/><% } %>
		<ffi:setProperty name="WireDestinations" property="Key" value="${_wireType}"/>
		<ffi:setProperty name="PageText" value="${PageText}<option value='${_wireType}' ${_selected}>${WireDestinations.Value}</option>"/>
		<ffi:removeProperty name="_wireType"/>
	<% } %>
	<ffi:setProperty name="PageText" value="${PageText}</select></span>"/>
	<ffi:removeProperty name="_selected"/>
<% } %>
