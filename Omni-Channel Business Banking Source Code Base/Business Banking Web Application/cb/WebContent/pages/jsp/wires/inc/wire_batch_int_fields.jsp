<%--
This file contains the batch-specific fields for an international wire batch

It is included on all add wire batch pages (except Host)
	wirebatchff.jsp
	wirebatchtemp.jsp
--%>
<%@page import="com.ffusion.beans.wiretransfers.WireBatch"%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<style>
	input#WireBatchExchangeRateValue, input#WireBatchContractNumberValue{width:255px}
</style>
<script>
	$(function(){
		$("#batchTypeSelectID").selectmenu({width:'260px'});
		$("#AmtCurrencyTypeID").selectmenu({width:'260px'});
		$("#payeeCurrencyTypeSelectID").selectmenu({width:'260px'});
		$("#mathRuleSelectID").selectmenu({width:'260px'});
	});
</script>
<%
String task = "WireBatchObject";
 %>




<script type="text/javascript"><!--
// process input

function roundExchangeRate() {
  frm = document.frmBatch;
  var num = frm['<%= task %>.ExchangeRate'].value
  frm['<%= task %>.ExchangeRate'].value = roundTo7DecimalPlaces(num)
}

// Round to 7 decimal places

function roundTo7DecimalPlaces(n) {

  ans = (Math.round(n * 10000000))/10000000 + ""
  dot = ans.indexOf(".",0)
  if (dot == -1) {ans = "1.0"}
  else if (dot == ans.length - 7) {ans = ans + "0"}
  return ans
}
// --></script>

	<div id="wireBatchNameLabel" ><span class="sectionsubhead"><!--L10NStart--><label for="wireBatchName">Batch Name</label><!--L10NEnd--></span><span class="required" title="required">*</span></div>
	<div class="marginTop5"><input id="wireBatchName" type="Text" name="<%= task %>.BatchName" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="BatchName"/>" size="35"></div>
	<div class="marginTop5"><span id="BatchNameError"></span></div>
	<div class="marginTop5" id="wireBatchApplicationTypeLabel" ><span class="sectionsubhead"><!--L10NStart--><label for="batchTypeSelectID">Application Type</label><!--L10NEnd--></span><span class="required" title="required">*</span></div>
	<div class="marginTop5">
		<select name="<%= task %>.BatchType" id="batchTypeSelectID" class="txtbox" onchange="changeForm(this.options[this.selectedIndex].value);" <s:if test="%{!(isFreeFormEntitled && isTemplateEntitled)}">disabled</s:if> %>>
			<s:if test="%{isFreeFormEntitled}"><option value="<s:property value="@com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_FREEFORM" />"  <s:if test="%{WireBatchObject.BatchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_FREEFORM}">selected</s:if>><!--L10NStart-->Free-form<!--L10NEnd--></option> </s:if>
			<s:if test="%{isTemplateEntitled}"><option value="<s:property value="@com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_TEMPLATE"/>" <s:if test="%{WireBatchObject.BatchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_TEMPLATE}">selected</s:if>><!--L10NStart-->Template<!--L10NEnd--></option></s:if>		
		</select>
	</div>
	<div id="wireBatchProcessingDateLabel" class="marginTop5"><span class="sectionsubhead"><!--L10NStart--><label for="Wire_Batch_Int_FieldsDateId">Processing Date</label><!--L10NEnd--></span><span class="required" title="required">*</span></div>
	<div class="marginTop5">
		<sj:datepicker
			value="%{WireBatchObject.DueDate}"
			id="Wire_Batch_Int_FieldsDateId"
			name="WireBatchObject.DueDateString"
			label="Date"
			maxlength="10"
			cssStyle="width:110px"
			buttonImageOnly="true"
			cssClass="ui-widget-content ui-corner-all" />
			<script>
				var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.DueDate&wireDest=${AddWireBatch.BatchDestination}"/>';
                   ns.common.enableAjaxDatepicker("Wire_Batch_Int_FieldsDateId", tmpUrl);
                  
			</script>
			<div class="marginTop10"><span id="DueDateError"></span></div>
	</div>
	<div id="wireBatchSettlementDateLabel" class="sectionsubhead marginTop5" ><!--L10NStart--><label for="SettlementDate">Settlement Date</label><!--L10NEnd--></div>
	<div class="marginTop5">
		<sj:datepicker
			value="%{WireBatchObject.SettlementDate}"
			id="SettlementDate"
			name="WireBatchObject.SettlementDateString"
			label="Date"
			maxlength="10"
			cssStyle="width:110px"
			buttonImageOnly="true"
			cssClass="ui-widget-content ui-corner-all" />
			<script>
				var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.SettlementDate&wireDest=${AddWireBatch.BatchDestination}"/>';
	                  ns.common.enableAjaxDatepicker("SettlementDate", tmpUrl);
	                  $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>'); 
			</script>
			<div class="marginTop10"><span id="SettlementDateError"></span></div>
</div>
	<div id="WireBatchDebitCurrencyLabel" class="sectionsubhead marginTop5"><!--L10NStart--><label for="AmtCurrencyTypeID">Debit Currency</label><!--L10NEnd--></div>
	<div class="marginTop5">
		<select id="AmtCurrencyTypeID" class="txtbox" name="<%= task %>.AmtCurrencyType"  onchange="refreshCurrencyMismatches();">
			<s:iterator value="currencies" var="currencyType" > 
				<option value="<s:property  value="CurrencyCode"/>"   <s:if test="%{WireBatchObject.AmtCurrencyType == CurrencyCode}">selected</s:if>  ><s:property  value="CurrencyCode"/>-<s:property  value="Description"/></option>
			</s:iterator>					
		</select>
	</div>
	<div id="WireBatchPaymenytCurrencyLabel" class="sectionsubhead marginTop5"><!--L10NStart--><label for="payeeCurrencyTypeSelectID">Payment Currency</label><!--L10NEnd--></div>
	<div class="marginTop5">
		<select id="payeeCurrencyTypeSelectID" class="txtbox" name="<%= task %>.PayeeCurrencyType"  onchange="refreshCurrencyMismatches();">
		<s:iterator value="currencies" var="currencyType"> 
			<option value="<s:property  value="CurrencyCode"/>"   <s:if test="%{WireBatchObject.PayeeCurrencyType == CurrencyCode}">selected</s:if>  ><s:property  value="CurrencyCode"/>-<s:property  value="Description"/></option>
		</s:iterator>
		</select>
	</div>
	<div id="WireBatchExchangeRateLabel" class="sectionsubhead marginTop5"><!--L10NStart--><label for="WireBatchExchangeRateValue">Exchange Rate</label><!--L10NEnd--></div>
	<div class="marginTop5"><input id="WireBatchExchangeRateValue" type="Text" name="<%= task %>.ExchangeRate" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="ExchangeRate"/>" onblur="javascript:roundExchangeRate();" size="18" ></div>
	<div id="WireBatchMathRuleLabel" class="sectionsubhead marginTop5"><!--L10NStart--><label for="mathRuleSelectID">Math Rule</label><!--L10NEnd--></div>
	<div class="marginTop5">
		<select name="<%= task %>.MathRule" class="txtbox"  id="mathRuleSelectID">
		<option <s:if test="%{WireBatchObject.MathRule == 'MULT'}">selected</s:if>><!--L10NStart-->MULT<!--L10NEnd--></option>
		<option   <s:if test="%{WireBatchObject.MathRule == 'DIV'}">selected</s:if>><!--L10NStart-->DIV<!--L10NEnd--></option>
		</select>
	</div>
</tr>
<div class="marginTop5 sectionsubhead" id="WireBatchContractNumberLabel"><!--L10NStart--><label for="WireBatchContractNumberValue">Contract Number</label><!--L10NEnd--></div>
<div class="marginTop5"><input id="WireBatchContractNumberValue" type="Text" name="<%= task %>.ContractNumber" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="ContractNumber"/>" size="18" >
</div>
	<%-- <tr>
		<td valign="top">
			<table width="100%" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td  id="wireBatchNameLabel" width="115" align="right"><span class="sectionsubhead"><!--L10NStart-->Batch Name<!--L10NEnd--></span><span class="required">*</span></td>
					<td width="228" align="left"><input id="wireBatchName" type="Text" name="<%= task %>.BatchName" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="BatchName"/>" size="35"><span id="BatchNameError"></span></td>
				</tr>
				<tr>
					<td id="wireBatchApplicationTypeLabel" align="right"><span class="sectionsubhead"><!--L10NStart-->Application Type<!--L10NEnd--></span><span class="required">*</span></td>
					<td align="left">
					<select name="<%= task %>.BatchType" id="batchTypeSelectID" class="txtbox" onchange="changeForm(this.options[this.selectedIndex].value);" <s:if test="%{!(isFreeFormEntitled && isTemplateEntitled)}">disabled</s:if> %>>
						<s:if test="%{isFreeFormEntitled}"><option value="<s:property value="@com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_FREEFORM" />"  <s:if test="%{WireBatchObject.BatchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_FREEFORM}">selected</s:if>><!--L10NStart-->Free-form<!--L10NEnd--></option> </s:if>
						<s:if test="%{isTemplateEntitled}"><option value="<s:property value="@com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_TEMPLATE"/>" <s:if test="%{WireBatchObject.BatchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_TEMPLATE}">selected</s:if>><!--L10NStart-->Template<!--L10NEnd--></option></s:if>		
					</select>
					</td>
				</tr>
				<tr>
					<td id="wireBatchProcessingDateLabel" align="right"><span class="sectionsubhead"><!--L10NStart-->Processing Date<!--L10NEnd--></span><span class="required">*</span></td>
					<td align="left"><input type="Text" name="<%= task %>.DueDate" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="DueDate"/>" size="11">
					<a onclick="window.open('<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.DueDate&wireDest=${AddWireBatch.BatchDestination}"/>','Calendar','width=350,height=300');" href="#"><img src="/cb/web/multilang/grafx/i_calendar.gif" alt="" width="19" height="16" align="absmiddle" border="0"></a>
					
					<sj:datepicker
						value="%{WireBatchObject.DueDate}"
						id="Wire_Batch_Int_FieldsDateId"
						name="WireBatchObject.DueDateString"
						label="Date"
						maxlength="10"
						buttonImageOnly="true"
						cssClass="ui-widget-content ui-corner-all" /><span id="DueDateError"></span>
						<script>
							var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.DueDate&wireDest=${AddWireBatch.BatchDestination}"/>';
		                    ns.common.enableAjaxDatepicker("Wire_Batch_Int_FieldsDateId", tmpUrl);
						</script>
					</td>
				</tr>
				<tr>
					<td id="wireBatchSettlementDateLabel" class="sectionsubhead" align="right"><!--L10NStart-->Settlement Date<!--L10NEnd--></td>
					<td align="left"><input type="Text" name="<%= task %>.SettlementDate" class="txtbox" value="<ffi:getProperty name="<%= task %>" property="SettlementDate"/>" size="11">
					<a onclick="window.open('<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.SettlementDate&wireDest=${AddWireBatch.BatchDestination}"/>','Calendar','width=350,height=300');" href="#"><img src="/cb/web/multilang/grafx/i_calendar.gif" alt="" width="19" height="16" align="absmiddle" border="0"></a>
					<sj:datepicker
						value="%{WireBatchObject.SettlementDate}"
						id="SettlementDate"
						name="WireBatchObject.SettlementDateString"
						label="Date"
						maxlength="10"
						buttonImageOnly="true"
						cssClass="ui-widget-content ui-corner-all" /><span id="SettlementDateError"></span>
						<script>
							var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.SettlementDate&wireDest=${AddWireBatch.BatchDestination}"/>';
		                    ns.common.enableAjaxDatepicker("SettlementDate", tmpUrl);
						</script>
					</td>
				</tr>
			</table> --%>

		<%-- <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
				<tr>
					
					<td id="WireBatchDebitCurrencyLabel" width="115" class="sectionsubhead" align="right"><!--L10NStart-->Debit Currency<!--L10NEnd--></td>
					<td width="226" align="left"><select id="AmtCurrencyTypeID" class="txtbox" name="<%= task %>.AmtCurrencyType"  onchange="refreshCurrencyMismatches();">
						<s:iterator value="currencies" var="currencyType" > 
							<option value="<s:property  value="CurrencyCode"/>"   <s:if test="%{WireBatchObject.AmtCurrencyType == CurrencyCode}">selected</s:if>  ><s:property  value="CurrencyCode"/></option>
						</s:iterator>					
					</select></td>
				</tr>
				<tr>
					<td id="WireBatchPaymenytCurrencyLabel" class="sectionsubhead" align="right"><!--L10NStart-->Payment Currency<!--L10NEnd--></td>
					<td align="left"><select id="payeeCurrencyTypeSelectID" class="txtbox" name="<%= task %>.PayeeCurrencyType"  onchange="refreshCurrencyMismatches();">
						<s:iterator value="currencies" var="currencyType"> 
							<option value="<s:property  value="CurrencyCode"/>"   <s:if test="%{WireBatchObject.PayeeCurrencyType == CurrencyCode}">selected</s:if>  ><s:property  value="CurrencyCode"/></option>
						</s:iterator>
					</select></td>
				</tr>
				<tr>
					<td id="WireBatchExchangeRateLabel" class="sectionsubhead" align="right"><!--L10NStart-->Exchange Rate<!--L10NEnd--></td>
					<td align="left"><input id="WireBatchExchangeRateValue" type="Text" name="<%= task %>.ExchangeRate" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="ExchangeRate"/>" onblur="javascript:roundExchangeRate();" size="18" ></td>
				</tr>
				<tr>
					<td id="WireBatchMathRuleLabel" class="sectionsubhead" align="right"><!--L10NStart-->Math Rule<!--L10NEnd--></td>
					<td align="left"><select name="<%= task %>.MathRule" class="txtbox"  id="mathRuleSelectID">

						<option <s:if test="%{WireBatchObject.MathRule == 'MULT'}">selected</s:if>><!--L10NStart-->MULT<!--L10NEnd--></option>
						<option   <s:if test="%{WireBatchObject.MathRule == 'DIV'}">selected</s:if>><!--L10NStart-->DIV<!--L10NEnd--></option>
					</select></td>
				</tr>
				<tr>
					<td id="WireBatchContractNumberLabel" class="sectionsubhead" align="right"><!--L10NStart-->Contract Number<!--L10NEnd--></td>
					<td align="left"><input id="WireBatchContractNumberValue" type="Text" name="<%= task %>.ContractNumber" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="ContractNumber"/>" size="18" ></td>
				</tr>
			</table>
		</td>
	</tr> --%>
