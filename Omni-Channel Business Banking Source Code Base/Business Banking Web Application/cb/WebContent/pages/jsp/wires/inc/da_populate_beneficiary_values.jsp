<%--
<p>This page is used to populate master values on wire beneficiary page. This page
first retrieves the DA category information and populate it into the master wire
beneficiary information. It put the category information bean and old wire beneficiary
information into session.
 </p>

Pages that request this page
----------------------------
wire_beneficiaries_summary.jsp
wire_addedit_payee_fields.jsp
wire_addedit_bank_fields.jsp
wire_addedit_intermediary_banks.jsp
wire_addedit_payee_comment.jsp

Pages this page requests
------------------------

Pages included in this page
---------------------------

--%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<%-- Start: Dual approval processing --%>
	<%-- Start: Clean up DA information --%>
	<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
	<ffi:removeProperty name="<%=IDualApprovalConstants.OLD_DA_OBJECT%>"/>
	<%-- End: Clean up DA information --%>
	<%-- If DA Item Id is null then set it from payee instance--%>
	<ffi:cinclude value1="${daItemId}" value2="">
		<% String daItemID = null; %>
		<ffi:getProperty name="WireTransferPayee" property="daItemId" assignTo="daItemID"/>
		<ffi:setProperty name="daItemId" value="<%=daItemID%>"/>
	</ffi:cinclude>	
	
	<%-- Retrieve category details information --%>
	<ffi:cinclude value1="${IsPending}" value2="true">
		<ffi:object id="GetWireDACategoryDetails" name="com.ffusion.tasks.dualapproval.wiretransfers.GetWireDACategoryDetails"/>
			<ffi:setProperty name="GetWireDACategoryDetails" property="itemId" value="${ID}"/>
			<ffi:setProperty name="GetWireDACategoryDetails" property="daItemId" value="${daItemId}"/>
			<ffi:setProperty name="GetWireDACategoryDetails" property="categoryType" value="${WireBeneficiaryCategory}"/>
			<ffi:setProperty name="GetWireDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
			<ffi:setProperty name="GetWireDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetWireDACategoryDetails"/>

		<ffi:cinclude value1="${WireBeneficiaryCategory}" value2="<%=IDualApprovalConstants.CATEGORY_WIRE_PAYEE%>">	
			<%-- Populate master payee information --%>
			<ffi:object id="PopulateObjectWithDAValues"  name="com.ffusion.tasks.dualapproval.PopulateObjectWithDAValues" scope="session"/>
				<ffi:setProperty name="PopulateObjectWithDAValues" property="sessionNameOfEntity" value="${payeeTask}"/>
			<ffi:process name="PopulateObjectWithDAValues"/>
		</ffi:cinclude>
		
	</ffi:cinclude>
<%-- End: Dual approval processing --%>

<%-- Start: Clean up DA information --%>
<ffi:removeProperty name="IsPayeeCommentPage"/>
<ffi:removeProperty name="WireBeneficiaryCategory"/>
<%-- End: Clean up DA information --%>		
