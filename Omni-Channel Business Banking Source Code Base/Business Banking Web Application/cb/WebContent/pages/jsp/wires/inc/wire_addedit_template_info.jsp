<%--
This file contains read-only template info, and is used when adding or editing
wire loaded from a template

It is included on all add/edit wire transfer pages (except Host)
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp
	wiretransfernew.jsp

It includes the following file:
common/wire_labels.jsp
	The labels for fields and buttons
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- ------ TEMPLATE (READ-ONLY) WireName, NickName, TemplateID, DisplayWireLimit, & WireScope ------ --%>
<div class="blockWrapper label130">
<div class="blockContent">
<div class="blockRow">
	<div class="inlineBlock" style="width: 50%">
		<span class="sectionsubhead sectionLabel">Template Name:</span>
		<span><ffi:getProperty name="${wireTask}" property="WireName" /> 
		<%String strNickName= null; %>
		<ffi:getProperty name="${wireTask}" property="NickName" assignTo="strNickName"/>		
		<ffi:cinclude value1="${strNickName}" value2="" operator="notEquals">(<ffi:getProperty name="${wireTask}" property="NickName"/>)</ffi:cinclude></span>
	</div>
	<div class="inlineBlock">
		<span class="sectionsubhead sectionLabel">Template ID:</span>
		<span><ffi:getProperty name="${wireTask}" property="TemplateID"/></span>
	</div>
</div>
<div class="blockRow">
	<div class="inlineBlock" style="width: 50%">
		<span class="sectionsubhead sectionLabel">Template Limit:</span>
		<span><ffi:getProperty name="${wireTask}" property="DisplayWireLimit"/></span>
	</div>
	<div class="inlineBlock">
		<span class="sectionsubhead sectionLabel">Template Scope:</span>
		<span><ffi:getProperty name="${wireTask}" property="WireScope"/></span>
	</div>
</div>
</div>
</div>

<%-- <table border="0" cellspacing="0" cellpadding="3" width="710">
	<tr>
		<td class="sectionsubhead"><!--L10NStart-->Template Name<!--L10NEnd--></td>
		<td class="sectionsubhead"><!--L10NStart-->Template Nickname<!--L10NEnd--></td>
		<td class="sectionsubhead"><!--L10NStart-->Template ID<!--L10NEnd--></td>
		<td class="sectionsubhead"><!--L10NStart-->Template Limit<!--L10NEnd--></td>
		<td class="sectionsubhead"><!--L10NStart-->Template Scope<!--L10NEnd--></td>
	</tr>
	<tr>
		<td class="columndata"><ffi:getProperty name="${wireTask}" property="WireName"/></td>
		<td class="columndata"><ffi:getProperty name="${wireTask}" property="NickName"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="${wireTask}" property="TemplateID"/></td>
		<td class="columndata"><ffi:getProperty name="${wireTask}" property="DisplayWireLimit"/></td>
		<td class="columndata"><ffi:getProperty name="${wireTask}" property="WireScope"/></td>
	</tr>
</table>
<span class="columndata">&nbsp;<br></span> --%>
