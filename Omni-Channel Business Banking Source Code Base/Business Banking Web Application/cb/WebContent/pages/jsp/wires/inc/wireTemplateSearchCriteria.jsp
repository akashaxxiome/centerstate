<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines"%>

<div class="searchDashboardRenderCls">
	<div id="quicksearchTemplatecriteria" class="quickSearchAreaCls" style="display:none;">
		<s:form action="/pages/jsp/wires/getWireTransferTemplatesAction_verify.action" method="post" id="QuickSearchFrmTempID" name="QuickSearchFrmTemp" theme="simple">
			<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
			<table border="0" width="100%" class="tableData">
				<tr>
					<td width="15%"><span class="sectionsubhead"><s:text name="jsp.default.wire_debitAccount"/></span></td>
					<td width="15%"><span class="sectionsubhead"><s:text name="jsp.default.wire_beneficiaryName"/></span></td>
					<td width="15%"><span class="sectionsubhead"><s:text name="jsp.default.wire_beneficiaryAccountNumber"/></span></td>
					<td width="15%"><span class="sectionsubhead"><s:text name="jsp.default.wire_beneficiaryRoutingNumber"/></span></td>
					<td width="15%"></td>
				</tr>
				<tr>
					<td>
						<select id="searchAcctID" class="txtbox" style="width:200px;" name="searchAcct"></select>
					</td>
					<td >					
						<input id="searchBeneficiaryNameID" class="ui-widget-content ui-corner-all" type="text" name="SearchBeneficiaryName" value="<ffi:getProperty name="SearchBeneficiaryName" />" size="25">
					</td>
					<td>
						<input id="searchBeneficiaryAccountNumberID" class="ui-widget-content ui-corner-all" type="text" name="SearchBeneficiaryAccountNumber" value="<ffi:getProperty name="SearchBeneficiaryAccountNumber" />" size="18">
					</td>
					
					<td >					
						<input id="searchBeneficiaryRoutingNumberID" class="ui-widget-content ui-corner-all" type="text" name="SearchBeneficiaryRoutingNumber" value="<ffi:getProperty name="SearchBeneficiaryRoutingNumber" />" size="20">
					</td>
					<td></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td><span class="sectionsubhead"><s:text name="jsp.default.wire_templateType"/></span></td>
					<td><span class="sectionsubhead"><s:text name="jsp.default.wire_templateName"/></span></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					
				</tr>
				<td>
					<% String curTempType = ""; %>
						<ffi:getProperty name="showTemplateType" assignTo="curTempType"/>
						<% if (curTempType == null) curTempType = ""; %>

					<select name="showTemplateType" class="txtbox" id="showTemplateTypeID">
						<option value="-1"><s:text name="jsp.default.wire_allTemplates"/></option>
						<option value="<%= WireDefines.WIRE_DOMESTIC %>" <%= curTempType.equals(WireDefines.WIRE_DOMESTIC) ? "selected" : "" %>><s:text name="jsp.default.wire_domestic_templates"/></option>
						<option value="<%= WireDefines.WIRE_INTERNATIONAL %>" <%= curTempType.equals(WireDefines.WIRE_INTERNATIONAL) ? "selected" : "" %>><s:text name="jsp.default.wire_international_templates"/></option>
						<option value="<%= WireDefines.WIRE_DRAWDOWN %>" <%= curTempType.equals(WireDefines.WIRE_DRAWDOWN) ? "selected" : "" %>><s:text name="jsp.default.wire_drawdown_templates"/></option>
						<option value="<%= WireDefines.WIRE_FED %>" <%= curTempType.equals(WireDefines.WIRE_FED) ? "selected" : "" %>><s:text name="jsp.default.wire_FEDWire_templates"/></option>
						<option value="<%= WireDefines.WIRE_BOOK %>" <%= curTempType.equals(WireDefines.WIRE_BOOK) ? "selected" : "" %>><s:text name="jsp.default.wire_book_templates"/></option>								
					</select>
				</td>
				<td >					
					<input id="searchTemplateNameID" class="ui-widget-content ui-corner-all" type="text" name="SearchTemplateName" value="<ffi:getProperty name="SearchTemplateName" />" size="25">
				</td>
				<td></td>
				<td></td>
				<td>
					<sj:a targets="quick" id="quicksearchTemplatebutton" formIds="QuickSearchFrmTempID" button="true" onCompleteTopics="quickSearchWiresTemplateComplete"><s:text name="jsp.default_6" /></sj:a>
				</td>
			</table>
		</s:form>
	</div>
</div>

<script>
$(function(){
	$("#showTemplateTypeID").selectmenu({width:'16em'});
	
	$("#searchAcctID").lookupbox({
		"controlType":"server",
		"collectionKey":"1",
		"module":"Wires",
		"size":"40",
		"source":"/cb/pages/jsp/wires/WireAccountsLookupBoxAction.action",
		"defaultOption":{ "label":"All Accounts",
						   "value":"All Accounts"
						 }
	});	
});
</script>