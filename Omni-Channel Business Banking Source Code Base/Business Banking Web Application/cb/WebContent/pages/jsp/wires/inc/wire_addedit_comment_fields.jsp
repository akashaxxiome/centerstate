<%--
This file contains the wire beneficiary comment and By Order Of fields.

It is included on all add/edit wire transfer pages (except Host)
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp
	wiretransfernew.jsp

It includes the following file:
common/wire_labels.jsp
	The labels for fields and buttons
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ include file="../../common/wire_labels.jsp"%>

<% String wireDest = ""; %>
<% String WireTranId = ""; %>
<% String WireSrvrtId = ""; %>
<% String WireAddendaType = "";

	String[] addendaTypes = {
                     "" + WireDefines.WIRE_ADDENDA_ANSI,
					 "" + WireDefines.WIRE_ADDENDA_GXML,
					 "" + WireDefines.WIRE_ADDENDA_IXML,
                     "" + WireDefines.WIRE_ADDENDA_NARR,
                     "" + WireDefines.WIRE_ADDENDA_S820,
                     "" + WireDefines.WIRE_ADDENDA_SWIF,
					 "" + WireDefines.WIRE_ADDENDA_UEDI };
    String[] addendaEntitlements = {
                     "" + com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_CTP_US_FED_ANSI,
                     "" + com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_CTP_US_FED_GXML,
                     "" + com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_CTP_US_FED_IXML,
                     "" + com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_CTP_US_FED_NARR,
                     "" + com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_CTP_US_FED_S820,
                     "" + com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_CTP_US_FED_SWIF,
                     "" + com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_CTP_US_FED_UEDI };
%>
<ffi:getProperty name="${wireTask}" property="WireDestination" assignTo="wireDest"/>
<ffi:getProperty name="${wireTask}" property="TrackingID"  assignTo="WireTranId" />
<ffi:getProperty name="${wireTask}" property="ExternalID"  assignTo="WireSrvrtId" />
<ffi:getProperty name="${wireTask}" property="AddendaType"  assignTo="WireAddendaType" />

<% if ( WireTranId != null && !"".equals( WireTranId )  ) {  %>
<ffi:object id="GetAuditWireTransferById" name="com.ffusion.tasks.wiretransfers.GetAuditWireTransferById" scope="session" />
    <ffi:setProperty name="GetAuditWireTransferById" property="TrackingID" value='<%= WireTranId %>'/>
    <ffi:setProperty name="GetAuditWireTransferById" property="ID" value='<%= WireSrvrtId %>'/>
    <ffi:setProperty name="GetAuditWireTransferById" property="CollectionSessionName" value="${collectionName}"/>
    <ffi:setProperty name="GetAuditWireTransferById" property="BeanSessionName" value="WireTransferHistory"/>
    <ffi:cinclude value1="${templateTask}" value2="edit" operator="equals">
        <ffi:setProperty name="GetAuditWireTransferById" property="Template" value="true"/>
    </ffi:cinclude>
<ffi:process name="GetAuditWireTransferById"/>
<% }  %>
<script type="text/javascript">
var textarea_wire_maxlen = {
  isMax : function (){
	
	var textarea = 
		document.getElementById("wireTemplateCommentID");
	var max_length = 985;
	if(textarea.value.length > max_length){
	  textarea.value = 
		textarea.value.substring(0, max_length);
	  document.getElementById("wireTemplateCommentError").innerHTML=js_template_comment_error;
	}
  },
  disabledRightMouse : function (){
	document.oncontextmenu = 
	  function (){ return false; }
  },
  enabledRightMouse : function (){
	document.oncontextmenu = null;
  }
};
</script>

 <script type="text/javascript"><!--
 function tempHistoryDisplay(show)
 {
     if (show)
     {
        document.getElementById("tempHistory").style.display = "inline";
        document.getElementById("viewTempHistory").style.display = "none";
        document.getElementById("hideTempHistory").style.display = "inline";
		$("#hideTempHistory").focus();
     }
     else
     {
        document.getElementById("tempHistory").style.display = "none";
        document.getElementById("viewTempHistory").style.display = "inline";
        document.getElementById("hideTempHistory").style.display = "none";
		 $("#viewTempHistory").focus();
     }
 }

 function addExtraWiresOnclick(){
	 $('#addExtraWiresCommentsID').toggle();
 }
     //--></script>

		<sj:a
			id="addExtraWiresLinkID"
			button="true" 
            onclick="addExtraWiresOnclick()"
		><s:text name="jsp.default_Add_Comments" /></sj:a>
<div id="addExtraWiresCommentsID" style="display:none; margin:10px 0 0">
<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData ">
	<tr>
		<td width="50%" class="sectionhead" valign="top">
			<span style="margin:0 0 10px; display:block"><%= wireDest.equals(WireDefines.WIRE_INTERNATIONAL) ? "<!--L10NStart-->Sender's Reference<!--L10NEnd-->" : "<!--L10NStart-->Reference for Beneficiary<!--L10NEnd-->" %></span>
			<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.Comment" value="<ffi:getProperty name="${wireTask}" property="Comment"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="35" maxlength="16" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.CommentError"></span>
			<span style="margin:0 0 10px; display:block; height:35px; line-height:35px"><!--L10NStart-->Originator to Beneficiary Information<!--L10NEnd--></span>
			<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.OrigToBeneficiaryInfo1" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="OrigToBeneficiaryInfo1"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.OrigToBeneficiaryInfo1Error"></span>
			<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.OrigToBeneficiaryInfo2" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="OrigToBeneficiaryInfo2"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.OrigToBeneficiaryInfo2Error"></span>
			<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.OrigToBeneficiaryInfo3" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="OrigToBeneficiaryInfo3"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.OrigToBeneficiaryInfo3Error"></span>
			<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.OrigToBeneficiaryInfo4" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="OrigToBeneficiaryInfo4"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.OrigToBeneficiaryInfo4Error"></span>
		</td>
		<% if (!wireDest.equals(WireDefines.WIRE_BOOK)) { %>
		<td width="50%" class="sectionhead" valign="top">
			<span style="margin:0 0 10px; display:block"><!--L10NStart-->Bank to Bank Information<!--L10NEnd--></span>
		
		<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.BankToBankInfo1" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="BankToBankInfo1"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.BankToBankInfo1Error"></span>
		<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.BankToBankInfo2" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="BankToBankInfo2"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.BankToBankInfo2Error"></span>
		<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.BankToBankInfo3" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="BankToBankInfo3"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.BankToBankInfo3Error"></span>
		<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.BankToBankInfo4" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="BankToBankInfo4"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.BankToBankInfo4Error"></span>
		<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.BankToBankInfo5" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="BankToBankInfo5"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.BankToBankInfo5Error"></span>
		<input style="margin:0 0 10px; display:block" name="<ffi:getProperty name="wireTask"/>.BankToBankInfo6" size="35" maxlength="35"class="ui-widget-content ui-corner-all txtbox"value="<ffi:getProperty name="${wireTask}" property="BankToBankInfo6"/>" <ffi:getProperty name="disableRep"/>><span id="<ffi:getProperty name="wireTask"/>.BankToBankInfo6Error"></span>
		</td>
		<% } %>
	</tr>
</table>

    <% if (wireDest.equals(WireDefines.WIRE_FED)) {

        %>
    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.PaymentEntitlementsDefines.WIRE_CTP_US_FED %>">

    <table width="100%" cellpadding="3" cellspacing="0" border="0">
        <tr>
            <td class="sectionhead" colspan="2"><!--L10NStart-->Addenda<!--L10NEnd--></td>
        </tr>
        <tr>
            <td colspan="2" class="sectionsubhead"><!--L10NStart-->Customer is responsible for properly formatting Addenda information.  Please be aware that the maximum number of characters allowed in the Addenda is 8192.  Copying, pasting or importing more than 8192 characters will result in truncation.<!--L10NEnd--></td>
        </tr>
        <tr>
            <td class="sectionsubhead"><!--L10NStart-->Addenda Type<!--L10NEnd--></td>
            <td>
                <% String selectedWireAddendaType = ""; %>
                <ffi:getProperty name="${wireTask}" property="AddendaType" assignTo="selectedWireAddendaType"/>


                <select id="selectAddendaType" class="txtbox" name="<ffi:getProperty name="wireTask"/>.AddendaType" onchange="showAddenda(this.value);" class="ui-widget-content ui-corner-all txtbox" <ffi:getProperty name="disableRep"/>>
                    <option value='' ><!--L10NStart-->Please select an Addenda Type<!--L10NEnd--></option>

                    <ffi:object name="com.ffusion.tasks.util.Resource" id="MyWireAddendaType" scope="session"/>
                    <ffi:setProperty name="MyWireAddendaType" property="ResourceFilename" value="com.ffusion.beansresources.wiretransfers.resources"/>
                    <ffi:process name="MyWireAddendaType" />
                    <%
                        for (int j=0; j < addendaTypes.length; j++)
                        {
                            String addType = addendaTypes[ j ];
                            String addEnt = addendaEntitlements [ j ];
                            String addResourceID = "WireAddendaType_"+addendaTypes[ j ];
                        %>
                            <ffi:cinclude ifEntitled="<%= addEnt %>">
                                <option value="<%= addType %>" <ffi:cinclude value1="<%= addType %>" value2="<%= selectedWireAddendaType %>" operator="equals">selected</ffi:cinclude> >
                                    <%= addType %> -
                                    <ffi:setProperty name="MyWireAddendaType" property="ResourceID" value='<%= addResourceID %>'/>
                                    <ffi:getProperty name="MyWireAddendaType" property="Resource"/>
                                </option>
                            </ffi:cinclude>
                        <% } %>

                </select>
                &nbsp;
                &nbsp;
                <% //Due to sj:a button doesn't support disable feature, old button widget has to be used here.
                   //replace the CSS of old search button with the new one, so that it looks like a new button widget.
                      StringBuffer searchButtonClassString = new StringBuffer("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
                      if((String)(session.getAttribute("disableRep")) == "disabled")
                              searchButtonClassString.append(" ui-state-disabled");
                %>
                <%-- <input id="ImportAddenda" class="<%= searchButtonClassString %>" type="button" value="Import Addenda" <ffi:getProperty name="disableRep"/> onclick="ns.wire.WireImportAddenda();"> --%>
                
                <sj:a id="ImportAddenda" 
                		cssClass=""
						button="true" 
						onClick="ns.wire.WireImportAddenda();"
						title="Import Addenda">
						Add New
					</sj:a>
					
                &nbsp;
                &nbsp;
                <a id="AddendaTrash" href="#" title="Delete Addenda" title="Delete Addenda" onclick="clearAddenda();"><span class="sapUiIconCls icon-delete"></span></a>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <textarea class="WireAddenda" id="<ffi:getProperty name="wireTask"/>.Addenda" name="<ffi:getProperty name="wireTask"/>.Addenda" rows="20" cols="100" wrap="physical" <ffi:getProperty name="disableRep"/> border="0"><ffi:getProperty name="${wireTask}" property="Addenda"/></textarea>
                <span id="<ffi:getProperty name="wireTask"/>.AddendaError" style="display:block"></span>
                <span id="ImportAddendaResult" style="font-size: 12px"></span>
            </td>
        </tr>
    </table>
    <br>
    </ffi:cinclude>
    <%    } %>

<table width="100%" cellpadding="3" cellspacing="0" border="0" class="tableData">
	<tr>
		<td class="sectionhead" colspan="2"><!--L10NStart-->By Order Of Information<!--L10NEnd--></td>
	</tr>
	
	<tr><td valign="top" colspan="2"><hr class="thingrayline" /></td></tr>
	
	<tr>
		<td width="50%" class="sectionsubhead"><%= LABEL_BOO_NAME %></td>
		<td width="50%" class="sectionsubhead"><%= LABEL_BOO_ACCOUNT_NUMBER %></td>
	</tr>
	
	<tr>
		<td><input name="<ffi:getProperty name="wireTask"/>.ByOrderOfName" value="<ffi:getProperty name="${wireTask}" property="ByOrderOfName"/>" type="text"class="ui-widget-content ui-corner-all txtbox"size="40" maxlength="35" <ffi:getProperty name="disableRep"/>></td>
		<td><input name="<ffi:getProperty name="wireTask"/>.ByOrderOfAccount" value="<ffi:getProperty name="${wireTask}" property="ByOrderOfAccount"/>" type="text"class="ui-widget-content ui-corner-all txtbox"size="40" maxlength="35" <ffi:getProperty name="disableRep"/>></td>
	</tr>
	<tr>
		<td><span id="<ffi:getProperty name="wireTask"/>.ByOrderOfNameError"></span></td>
		<td><span id="<ffi:getProperty name="wireTask"/>.ByOrderOfAccountError"></span></td>
	</tr>
	<tr>
		<td class="sectionsubhead"><%= LABEL_BOO_ADDRESS_1 %></td>
		<td class="sectionsubhead"><%= LABEL_BOO_ADDRESS_2 %></td>
	</tr>
	
	<tr>
		<td><input name="<ffi:getProperty name="wireTask"/>.ByOrderOfAddress1" value="<ffi:getProperty name="${wireTask}" property="ByOrderOfAddress1"/>" type="text"class="ui-widget-content ui-corner-all txtbox"size="40" maxlength="35" <ffi:getProperty name="disableRep"/>></td>
		<td><input name="<ffi:getProperty name="wireTask"/>.ByOrderOfAddress2" value="<ffi:getProperty name="${wireTask}" property="ByOrderOfAddress2"/>" type="text"class="ui-widget-content ui-corner-all txtbox"size="40" maxlength="35" <ffi:getProperty name="disableRep"/>></td>
	</tr>
	<tr>
		<td><span id="<ffi:getProperty name="wireTask"/>.ByOrderOfAddress1Error"></span></td>
		<td><span id="<ffi:getProperty name="wireTask"/>.ByOrderOfAddress2Error"></span></td>
	</tr>
	<tr>
		<td colspan="2" class="sectionsubhead"><%= LABEL_BOO_ADDRESS_3 %></td>
	</tr>
	<tr>
		<td colspan="2"><input name="<ffi:getProperty name="wireTask"/>.ByOrderOfAddress3" value="<ffi:getProperty name="${wireTask}" property="ByOrderOfAddress3"/>" type="text"class="ui-widget-content ui-corner-all txtbox"size="40" maxlength="35" <ffi:getProperty name="disableRep"/>></td>
	</tr>
	<tr>
		<td colspan="2"><span id="<ffi:getProperty name="wireTask"/>.ByOrderOfAddress3Error"></span></td>
	</tr>
</table>

<ffi:cinclude value1="${templateTask}" value2="edit" operator="equals">
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableData" style="margin:10px 0 0;">
	<tr>
		<td colspan="3" class="sectionhead"><!--L10NStart-->Template Lifecycle<!--L10NEnd--></td>
	</tr>
	<tr><td valign="top"><hr class="thingrayline" /></td></tr>
	<tr>
		<td width="715" valign="top">
<%-- ------ Last info ------ --%>
			<table width="715" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td width="100"><span class="sectionsubhead"><!--L10NStart-->Current Status<!--L10NEnd--></span></td>
					<td width="615" class="columndata"><ffi:getProperty name="${wireTask}" property="StatusName"/></td>
				</tr>
                <tr>
					<td class="sectionsubhead" valign="top" style="padding-top:5px"><!--L10NStart-->Add Comment<!--L10NEnd--></td>
                    <td>
				        <textarea id="wireTemplateCommentID" onkeyup="textarea_wire_maxlen.isMax()" onfocus="textarea_wire_maxlen.disabledRightMouse()" onblur="textarea_wire_maxlen.enabledRightMouse()" class="ui-widget-content ui-corner-all txtbox"name="<ffi:getProperty name="wireTask"/>.TemplateComment" rows="2" cols="40" ><ffi:getProperty name="${wireTask}" property="TemplateComment"/></textarea><span id="wireTemplateCommentError" style="font-weight:bold; color:red"></span>
                    </td>
                </tr>
                <tr>
					<td width="100"><span class="sectionsubhead"><!--L10NStart-->Template History<!--L10NEnd--></span></td>
					<td width="615" class="columndata">
                        <a href="javascript:tempHistoryDisplay(true)" onclick="tempHistoryDisplay(true)" id="viewTempHistory" title="View History">
                           <span class="sapUiIconCls icon-positive"></span>
                        </a>
                        <a href="javascript:tempHistoryDisplay(false)" onclick="tempHistoryDisplay(false)" id="hideTempHistory" style="display:none" title="Hide History">
                           <span class="sapUiIconCls icon-negative"></span>
                        </a>
                    </td>
				</tr>
                <tr>
                    <td colspan="2">
                        <span id="tempHistory" style="display:none">
                            <s:include value="%{#session.PagesPath}/common/include-view-wire-history.jsp"/>
                        </span>
                    </td>
                </tr>
            </table>
		</td>
	</tr>
</table>
<br>
</ffi:cinclude>
</div>

<script type="text/javascript"><!--
function showAddenda(addendaType)
{
    if (document.getElementById("<ffi:getProperty name="wireTask"/>.Addenda") != null)
    {
        if (addendaType != null && addendaType.length > 0)
        {
            document.getElementById("<ffi:getProperty name="wireTask"/>.Addenda").style.display = "inline";
            document.getElementById("ImportAddenda").style.display = "inline-block";
            document.getElementById("AddendaTrash").style.display = "inline";
        } else
        {
            document.getElementById("<ffi:getProperty name="wireTask"/>.Addenda").style.display = "none";
            document.getElementById("ImportAddenda").style.display = "none";
            document.getElementById("AddendaTrash").style.display = "none";
        }
    }
}
function clearAddenda(){
    document.getElementById("<ffi:getProperty name="wireTask"/>.Addenda").value='';
}

showAddenda("<%= WireAddendaType %>");

    //--></script>
<script>
$(function(){
	$("#selectAddendaType").selectmenu({width:'24em'});
});
</script>

<ffi:removeProperty name="MyWireAddendaType"/>


