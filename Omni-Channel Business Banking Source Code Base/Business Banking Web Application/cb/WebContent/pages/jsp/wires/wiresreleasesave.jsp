<%--
This page processes the release wires request.

Pages that request this page
----------------------------
wiresreleaseconfirm.jsp (wire release confirm page)
	CONFIRM/RELEASE WIRES button

Pages this page requests
------------------------
Javascript auto-refreshes to one of the following:
	wiretransfers.jsp (wire summary page)
	index.jsp (CB home page - in cb root directory)
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>



<%--
<html>
	<head>
		<script>location.replace("<ffi:getProperty name="releaseBackURL"/>");</script>
	</head>
	<body></body>
</html>
--%>
<ffi:removeProperty name="releaseWireStartDate"/>
<ffi:removeProperty name="releaseWireEndDate"/>
<s:include value="%{#session.PagesPath}/wires/wires_summary.jsp"/>
