<%--
This is the delete host wire confirmation page.

Pages that request this page
----------------------------
wire_transfer_list.jsp (included in wiretransfers.jsp, wire transfer summary)
	Delete button next to a fed wire transfer.

Pages this page requests
------------------------
CANCEL button requests wiretransfers.jsp
DELETE TRANSFER button requests wirehostdeletesend.jsp

Pages included in this page
---------------------------
payments/inc/wire_set_wire.jsp.jsp
	Sets a wire transfer or wire batch with a given ID into the session
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireTransfer"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirehostdelete" className="moduleHelpClass"/>

<%
if( request.getParameter("ID") !=null ) { session.setAttribute("ID", request.getParameter("ID")); }
if( request.getParameter("collectionName") !=null ) { session.setAttribute("collectionName", request.getParameter("collectionName")); }
if( request.getParameter("pendApproval") !=null ) { session.setAttribute("pendApproval", request.getParameter("pendApproval")); }
boolean isSkip = Boolean.valueOf(request.getParameter("isSkip"));
%>
<s:set var="isSkip" scope="request"><%=isSkip%></s:set>
	<s:set id="deleteAtionURL" value="%{'deleteWireTransferAction'}" scope="request"></s:set>
	<ffi:cinclude value1="${isSkip}" value2="true">
		<s:set id="deleteAtionURL" value="%{'skipWireTransferAction'}" scope="request"></s:set>
	</ffi:cinclude>
<div align="center">
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Host ID<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Host ID<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="HostID"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Amount<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name='WireTransfer' property='Amount'/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><!--L10NStart-->Date<!--L10NEnd--></span>
			<span class="columndata"><ffi:getProperty name="WireTransfer" property="DueDate"/></span>
		</div>
	</div>
</div>
	<%-- <table width="750" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="ltrow2_color">
				<div align="center">
					<table width="99%" border="0" cellspacing="0" cellpadding="3">
						<tr>
						   <td nowrap align="left"  class="tbrd_b sectionhead" colspan="4">&gt; 
								<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="equals"><!--L10NStart-->Are you sure you want to delete this host wire transfer template?<!--L10NEnd--></ffi:cinclude>
								<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="notEquals"><!--L10NStart-->Are you sure you want to delete this host wire transfer?<!--L10NEnd--></ffi:cinclude>
						   </td>
						</tr>
						<tr>
							<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10"></td>
						</tr>
						<tr>
							<td align="left" width="33%" style="text-align: right;" nowrap class="sectionsubhead"><!--L10NStart-->Host ID<!--L10NEnd--></td>
							<td width="33%"></td>
							<td align="left" width="33%" style="text-align: left;" nowrap class="columndata"><ffi:getProperty name="WireTransfer" property="HostID"/></td>
						</tr>
						<tr>
							<td align="left" width="33%" style="text-align: right;"nowrap class="sectionsubhead"><!--L10NStart-->Amount<!--L10NEnd--></td>
							<td width="33%"></td>
							<td align="left" width="33%" style="text-align: left;" nowrap class="columndata"><ffi:getProperty name='WireTransfer' property='Amount'/></td>
						</tr>
						<tr>
							<td align="left" width="33%" style="text-align: right;"nowrap class="sectionsubhead"><!--L10NStart-->Date<!--L10NEnd--></td>
							<td width="33%"></td>
							<td align="left" width="33%" style="text-align: left;" nowrap class="columndata"><ffi:getProperty name="WireTransfer" property="DueDate"/></td>
						</tr>
					</table>
					<table width="99%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td align="center">
								<s:form action="/pages/jsp/wires/deleteWireTransferAction.action" method="post" name="TransferNew" id="deleteHostWireFormId">
								<input type="hidden" name="ID" value="<ffi:getProperty name="WireTransfer" property="ID"/>"/>
								<input type="hidden" name="wireType" value="<ffi:getProperty name="WireTransfer" property="WireType"/>"/>
									<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<s:hidden name="collectionName" value="%{#session.collectionName}" id="collectionName"/>
									
									<input type="Button" value="CANCEL" class="submitbutton" onClick="javascript:document.location='wiretransfers.jsp'">
									<input class="submitbutton" type="submit" name="submitButtonName" value="DELETE TRANSFER" border="0">
									
									
								</s:form>
								
								<sj:a id="cancelDeleteHostWireFormID" 
										button="true" 
										onClickTopics="closeDialog"
								><s:text name="jsp.default_82" />
								</sj:a>
								<sj:a 
										id="deleteHostWireFormID" 
										formIds="deleteHostWireFormId" 
										targets="resultmessage" 
										button="true"   
										title="DELETE TRANSFER" 
										onCompleteTopics="deleteHostWireCompleteTopics" 
										onSuccessTopics="deleteWiresTransfersSuccessTopics" 
										effectDuration="1500" 
								><s:text name="jsp.default_DELETE_TRANSFER" />
								</sj:a>
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table> --%>
</div>
<div align="center" class="sectionhead marginTop20" style="color:red"> 
<% if (isSkip) { %><!--L10NStart-->Are you sure you want to skip this host wire transfer?<!--L10NEnd--><% } else { %>
	<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="equals"><!--L10NStart-->Are you sure you want to delete this host wire transfer template?<!--L10NEnd--></ffi:cinclude>
	<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="notEquals"><!--L10NStart-->Are you sure you want to delete this host wire transfer?<!--L10NEnd--></ffi:cinclude>
	<% } %>
</div>
<s:form action="%{#request.deleteAtionURL}" method="post" name="TransferNew" id="deleteHostWireFormId">
<input type="hidden" name="ID" value="<ffi:getProperty name="WireTransfer" property="ID"/>"/>
<input type="hidden" name="wireType" value="<ffi:getProperty name="WireTransfer" property="WireType"/>"/>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden name="collectionName" value="%{#session.collectionName}" id="collectionName"/>
</s:form>
<div class="btn-row">							
	<sj:a id="cancelDeleteHostWireFormID" 
			button="true" 
			onClickTopics="closeDialog"
	><s:text name="jsp.default_82" />
	</sj:a>
	<% if (!isSkip) { %>
		<sj:a 
				id="deleteHostWireFormID" 
				formIds="deleteHostWireFormId" 
				targets="resultmessage" 
				button="true"   
				title="DELETE TRANSFER" 
				onCompleteTopics="deleteHostWireCompleteTopics" 
				onSuccessTopics="deleteWiresTransfersSuccessTopics" 
				effectDuration="1500" 
		><s:text name="jsp.default_DELETE_TRANSFER" />
		</sj:a>
	<% } else { %>
		<sj:a 
			id="skipWireLink" 
			formIds="deleteHostWireFormId" 
			targets="resultmessage" 
			button="true"   
			title="Skip Wire" 
			onCompleteTopics="skipWiresTransfersCompleteTopics" 
			onSuccessTopics="deleteWiresTransfersSuccessTopics" 
			onErrorTopics="deleteWiresTransfersErrorTopics" 
			effectDuration="1500"
			><s:text name="jsp.default.Skip_Wire" />
		</sj:a>
	<% } %>
</div>	
