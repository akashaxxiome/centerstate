<%--
This is the host wire batch confirmation page.

Pages that request this page
----------------------------
wirebatchhost.jsp
	SUBMIT WIRE BATCH button
wirebatchedithost.jsp
	SUBMIT WIRE BATCH button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
BACK requests one of the following
	wirebatchhost.jsp
	wirebatchedithost.jsp
CONFIRM/SEND BATCH requests wirebatchsend.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script>

$.subscribe('sendWireBacthInitial', function(event,data) {
	var url = $("#targetURLID").val();
	 $.ajax({
         type: "POST",
         url: url,
         success: function(data) {
         }
     });
});

</script>

<ffi:help id="payments_wirebatchhostconfirm" className="moduleHelpClass"/>
<% String itemsPerPageStr = ""; String pageStr = ""; String pagesStr = ""; %>
<ffi:getProperty name="wireBatchItemsPerPage" assignTo="itemsPerPageStr"/>
<ffi:getProperty name="hostPage" assignTo="pageStr"/>
<ffi:getProperty name="hostPages" assignTo="pagesStr"/>

<%-- 
<ffi:setProperty name="${AddEditTask}" property="DateFormat" value="${UserLocale.DateFormat}"/>
--%>
<%-- Copy DateToPost to SettlementDate if SettlementDate is empty --%>
<% String settleDate = ""; %>
<ffi:getProperty name="wireBatchObject" property="SettlementDate" assignTo="settleDate"/>
<%
if (settleDate == null) settleDate = "";
if (settleDate.equals("")) {
    String postDate = "";
%>
    <ffi:getProperty name="wireBatchObject" property="DateToPost" assignTo="postDate"/>
    <ffi:setProperty name="wireBatchObject" property="SettlementDate" value="<%= postDate %>"/>
<%
}
%>
<div class="leftPaneWrapper" role="form" aria-labelledby="wireBatchSummaryHeader"><div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="wireBatchSummaryHeader">Wire Batch Summary</h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyWireBeneficiaryLabel">Batch Name: </span>
				<span class="inlineSection floatleft labelValue" id="verifyWireBeneficiaryValue"><ffi:getProperty name="wireBatchObject" property="BatchName"/></span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection">Batch Type: </span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="batchType"/></span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection">Application Type: </span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="wireBatchObject" property="BatchType"/></span>
		</div>
	</div>
</div></div>
<div class="confirmPageDetails">
<span id="wireBatchResultMessage" style="display:none">Your wire batch has been saved.</span>
<div class="marginTop20">
	  <s:if test="%{isDueDateChanged && nonBusinessDay}">
        <%-- Both proc window is closed and tomorrow is not a biz day --%>
        <div class="sectionsubhead" style="color:red" align="center">
            <!--L10NStart-->NOTE: The Host wire processing window for today has closed,
            so the Requested Value Date has been changed to tomorrow's date.
            Since tomorrow is not a business day, the Expected Value Date has been changed
            to the next business day.<!--L10NEnd--><br><br>
        </div>
    </s:if>
     <s:elseif test="%{isDueDateChanged}">
        <%-- DueDate changed because processing window is closed --%>
        <div class="sectionsubhead" style="color:red" align="center">
            <!--L10NStart-->NOTE: The Host wire processing window for today has closed.
            The Requested Value Date has been changed to tomorrow's date.<!--L10NEnd--><br><br>
        </div>
    </s:elseif>
    <s:elseif test="%{nonBusinessDay}">
        <%-- DueDate changed because its not a business day --%>
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
            <!--L10NStart-->NOTE: The Requested Value Date is not a business day.
            The Expected Value Date has been changed to the next business day.<!--L10NEnd--><br><br>
        </div>
    </s:elseif>
</div>
<div class="blockWrapper" role="form" aria-labelledby="batchInfoHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="batchInfoHeader">Batch Information</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Requested Processing Date:<!--L10NEnd--></span>
                <span class="columndata"><ffi:getProperty name="wireBatchObject" property="DueDate"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Settlement Date:<!--L10NEnd--></span>
                <span class="columndata"><ffi:getProperty name="wireBatchObject" property="SettlementDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Expected Processing Date:<!--L10NEnd--></span>
                <span class="columndata"><ffi:getProperty name="wireBatchObject" property="DateToPost"/></span>
			</div>
			<div class="inlineBlock">
			</div>
		</div>
	</div>
</div>

<div class="blockWrapper marginTop20" role="form" aria-labelledby="includedBatchesHeader">
	<div  class="blockHead"><h2 id="includedBatchesHeader">Included Batches</h2></div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
                 <tr class="header">
                     <td align="center" class="sectionsubhead"><!--L10NStart-->Host ID<!--L10NEnd--></td>
                     <td align="" class="sectionsubhead" align="left"><!--L10NStart-->Status<!--L10NEnd--></td>
                     <td align="left" class="sectionsubhead"><!--L10NStart-->Amount<!--L10NEnd--></td>
                 </tr>
                 
                 <ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="ACTION!!del,Status!!3,AND" />
                 
                 <ffi:list collection="wireBatchObject.Wires" items="wire">
                 <tr>
                     <td align="center" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
                     
                     <td class="columndata" align="" nowrap>
                         <ffi:getProperty name="wire" property="StatusName"/>
                     </td>

                     <td align="left" class="columndata" nowrap>
                     <s:if test="%{!isModifyAction}">
                         <ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/>
                     </s:if>
                     <s:else>
                         <ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="equals">
                             <ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/>
                         </ffi:cinclude>
                         <ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
                             <ffi:object id="Currency" name="com.ffusion.beans.common.Currency"/>
                             <ffi:setProperty name="Currency" property="Amount" value="${wire.OrigAmount}"/>
                             <ffi:setProperty name="Currency" property="CurrencyCode" value="${wire.OrigCurrency}"/>
                             <ffi:getProperty name="Currency" property="CurrencyStringNoSymbol"/>
                         </ffi:cinclude>
                     </s:else>
                     </td>
                 </tr>
                 </ffi:list>
                 
                 <ffi:setProperty name="${AddEditTask}" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
                 
                 <tr>
                     <td align="center" width="60%" class="columndata">&nbsp;</td>
                     <td class="columndata" width="20%" align="left" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                     <td align="left" class="tbrd_t sectionsubhead" width="20%" valign="bottom" nowrap>
                    <s:if test="%{!isModifyAction}">
                         <ffi:getProperty name="wireBatchObject" property="Amount"/>
                    </s:if>
                     <s:else>
                         <ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>
                     </s:else>
                     </td>
                 </tr>
       </table>
	</div>
</div>
<s:form id="sendWireBacthFormID" method="post" name="FormName" namespace="/pages/jsp/wires" action="%{#session.wirebatch_confirm_form_url}" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
   <div class="btn-row">             	
    <sj:a id="cancelFormButtonOnBatchVerify"
		button="true" 
		onClickTopics="cancelWireTransferForm"
	><s:text name="jsp.default_82" />
	</sj:a>
	<s:if test="%{isModifyAction}">
		<s:hidden id="targetURLID"  name="targetURLID" value="/cb/pages/jsp/wires/modifyHostWireBatchAction_hostProcess.action"/>
	</s:if>
	<s:else>
		<s:hidden id="targetURLID"  name="targetURLID" value="/cb/pages/jsp/wires/addHostWireBatchAction_hostProcess.action"/>
	</s:else>
	<sj:a id="backFormButton" 
		  button="true" 
		  onClickTopics="sendWireBacthInitial,backToInput"
	>
		<s:text name="jsp.default_57" />
	</sj:a>
	
    <sj:a id="sendWireBacthSubmit"
		formIds="sendWireBacthFormID" 
		targets="confirmDiv" 
		button="true" 
		onBeforeTopics="sendWireBacthFormBeforeTopics"
		onSuccessTopics="sendWireBacthFormSuccessTopic"
		onErrorTopics="errorSendWireBatchForm"
		onCompleteTopics="sendWireBacthFormCompleteTopic"><s:text name="jsp.default_395" /></sj:a>
	</div>    
</s:form>
</div></div>
           <%--  <table width="750" cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td class="columndata ltrow2_color" align="center">
                     <table width="98%" border="0" cellspacing="0" cellpadding="3">
   Show alert if date was changed --%>


  <%--   <s:if test="%{isDueDateChanged && nonBusinessDay}">
    <tr>
        Both proc window is closed and tomorrow is not a biz day
        <td class="sectionsubhead" style="color:red" align="center">
            <!--L10NStart-->NOTE: The Host wire processing window for today has closed,
            so the Requested Value Date has been changed to tomorrow's date.
            Since tomorrow is not a business day, the Expected Value Date has been changed
            to the next business day.<!--L10NEnd--><br><br>
        </td>
    </tr>
    </s:if>
     <s:elseif test="%{isDueDateChanged}">
    <tr>
        DueDate changed because processing window is closed
        <td class="sectionsubhead" style="color:red" align="center">
            <!--L10NStart-->NOTE: The Host wire processing window for today has closed.
            The Requested Value Date has been changed to tomorrow's date.<!--L10NEnd--><br><br>
        </td>
    </tr>
    </s:elseif>
    <s:elseif test="%{nonBusinessDay}">
    <tr>
        DueDate changed because its not a business day
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
            <!--L10NStart-->NOTE: The Requested Value Date is not a business day.
            The Expected Value Date has been changed to the next business day.<!--L10NEnd--><br><br>
        </td>
    </tr>
    </s:elseif> --%>

                      <!--   <tr>
                            <td align="left" class="tbrd_b sectionhead">&gt; L10NStartBatch InformationL10NEnd</td>
                        </tr>
                    </table>
                    <br> -->
                    <%-- <table width="715" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="357" valign="top">
                                <table width="355" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Batch Name<!--L10NEnd--></td>
                                        <td align="left" width="228" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchName"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead"><!--L10NStart-->Batch Type<!--L10NEnd--></td>
                                        <td align="left" class="columndata"> <ffi:getProperty name="batchType"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead"><!--L10NStart-->Application Type<!--L10NEnd--></td>
                                        <td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchType"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                            <td width="348" valign="top">
                                <table width="347" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" width="160" class="sectionsubhead"><!--L10NStart-->Requested Processing Date<!--L10NEnd--></td>
                                        <td align="left" width="185" class="columndata"><ffi:getProperty name="wireBatchObject" property="DueDate"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead"><!--L10NStart-->Settlement Date<!--L10NEnd--></td>
                                        <td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="SettlementDate"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead"><!--L10NStart-->Expected Processing Date<!--L10NEnd--></td>
                                        <td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="DateToPost"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="98%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="tbrd_b sectionhead">&gt; <!--L10NStart-->Included Batches<!--L10NEnd--></td>
                        </tr>
                    </table> --%>
                    <%-- <table width="375" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="sectionsubhead"><!--L10NStart-->Host ID<!--L10NEnd--></td>
                            <td align="left" class="sectionsubhead" align="left"><!--L10NStart-->Status<!--L10NEnd--></td>
                            <td align="left" class="sectionsubhead"><!--L10NStart-->Amount<!--L10NEnd--></td>
                        </tr>
                        
                        <ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="ACTION!!del,Status!!3,AND" />
                        
                        <ffi:list collection="wireBatchObject.Wires" items="wire">
                        <tr>
                            <td align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
                            
                            <td class="columndata" align="left" nowrap>
                                <ffi:getProperty name="wire" property="StatusName"/>
                            </td>

                            <td align="left" class="columndata" nowrap>
                            <s:if test="%{!isModifyAction}">
                                <ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/>
                            </s:if>
                            <s:else>
                                <ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="equals">
                                    <ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/>
                                </ffi:cinclude>
                                <ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
                                    <ffi:object id="Currency" name="com.ffusion.beans.common.Currency"/>
                                    <ffi:setProperty name="Currency" property="Amount" value="${wire.OrigAmount}"/>
                                    <ffi:setProperty name="Currency" property="CurrencyCode" value="${wire.OrigCurrency}"/>
                                    <ffi:getProperty name="Currency" property="CurrencyStringNoSymbol"/>
                                </ffi:cinclude>
                            </s:else>
                            </td>
                        </tr>
                        </ffi:list>
                        
                        <ffi:setProperty name="${AddEditTask}" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
                        
                        <tr>
                            <td align="left" class="columndata">&nbsp;</td>
                            <td align="left" class="columndata" align="right" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                            <td align="left" class="tbrd_t sectionsubhead" width="1%" valign="bottom" nowrap>
                           <s:if test="%{!isModifyAction}">
                                <ffi:getProperty name="wireBatchObject" property="Amount"/>
                           </s:if>
                            <s:else>
                                <ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>
                            </s:else>
                            </td>
                        </tr>
                    </table> --%>
                    <%-- <s:form id="sendWireBacthFormID" method="post" name="FormName" namespace="/pages/jsp/wires" action="%{#session.wirebatch_confirm_form_url}" theme="simple">
                    <form action="wirebatchsend.jsp" method="post" name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	
                    <sj:a id="cancelFormButtonOnBatchVerify"
						button="true" 
						onClickTopics="cancelWireTransferForm"
					>
						CANCEL
					</sj:a>
					<s:if test="%{isModifyAction}">
						<s:hidden id="targetURLID"  name="targetURLID" value="/cb/pages/jsp/wires/modifyHostWireBatchAction_hostProcess.action"/>
					</s:if>
					<s:else>
						<s:hidden id="targetURLID"  name="targetURLID" value="/cb/pages/jsp/wires/addHostWireBatchAction_hostProcess.action"/>
					</s:else>
					<sj:a id="backFormButton" 
						  button="true" 
						  onClickTopics="sendWireBacthInitial,backToInput"
					>
						BACK
					</sj:a>
					
                     <sj:a id="sendWireBacthSubmit"
						formIds="sendWireBacthFormID" 
						targets="confirmDiv" 
						button="true" 
						onBeforeTopics="sendWireBacthFormBeforeTopics"
						onSuccessTopics="sendWireBacthFormSuccessTopic"
						onErrorTopics="errorSendWireBatchForm"
						onCompleteTopics="sendWireBacthFormCompleteTopic">CONFIRM/SEND BATCH</sj:a>
                    </s:form> 
                    </td>
                </tr>
            </table>--%>
