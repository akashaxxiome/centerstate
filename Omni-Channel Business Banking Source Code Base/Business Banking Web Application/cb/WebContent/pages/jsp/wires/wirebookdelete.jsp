<%--
This is the delete book wire confirmation page.

Pages that request this page
----------------------------
wire_transfer_list.jsp (included in wiretransfers.jsp, wire transfer summary)
	Delete button next to a book transfer.
wire_templates_list.jsp (included in wiretemplates.jsp, wire template summary)
	Delete button next to a book transfer.
wirebatchff.jsp (add freeform wire batch)
	Delete button next to a book transfer.
wirebatcheditff.jsp (edit freeform wire batch)
	Delete button next to a book transfer.
wirebatchedittemplate.jsp (edit template batch)
	Delete button next to a book transfer.

Pages this page requests
------------------------
CANCEL button requests one of the following:
	wiretransfers.jsp (wire summary page)
	wiretemplates.jsp (wire template summary page)
	wirebatchff.jsp (add freeform wire batch)
	wirebatcheditff.jsp (edit freeform wire batch)
	wirebatchedittemplate.jsp (edit template wire batch)
DELETE TRANSFER button requests one of the following:
	wiretransferdeletesend.jsp (for single transfers and templates)
	wirebatchentrydeletesend.jsp (for wires in a batch)

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
payments/inc/wire_delete_common.jsp
	Common initialization for all delete wire pages (non-batch)
payments/inc/wire_batch_entry_delete_common.jsp
	Common initialization for all delete wire pages (in batch)
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireTransfer"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/wire_labels.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebookdelete" className="moduleHelpClass"/>
<%
if( request.getParameter("ID") != null ){session.setAttribute("ID", request.getParameter("ID"));}
if( request.getParameter("recID") != null ){session.setAttribute("recID", request.getParameter("recID"));}
if( request.getParameter("deleteTemplate") != null ){session.setAttribute("deleteTemplate", request.getParameter("deleteTemplate"));}
if( request.getParameter("collectionName") != null ){session.setAttribute("collectionName", request.getParameter("collectionName"));}
if( request.getParameter("wireInBatch") != null ){session.setAttribute("wireInBatch", request.getParameter("wireInBatch"));}
if( request.getParameter("wireIndex") != null ){session.setAttribute("wireIndex", request.getParameter("wireIndex"));}
if( request.getParameter("SetActionFlag") != null ){session.setAttribute("SetActionFlag", request.getParameter("SetActionFlag"));}
if( request.getParameter("wireBatch") != null ){session.setAttribute("wireBatch", request.getParameter("wireBatch"));}
boolean isSkip = Boolean.valueOf(request.getParameter("isSkip"));
boolean isRecModel = Boolean.valueOf(request.getParameter("isRecModel"));
%>
<s:set var="isSkip" scope="request"><%=isSkip%></s:set>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<% if (request.getParameter("wireIndex") == null) { %>
	<%-- This wire is NOT in a batch --%>
	<%--<ffi:include page="${PathExt}payments/inc/wire_delete_common.jsp"/>--%>
	<s:include value="%{#session.PagesPath}/wires/inc/wire_delete_common.jsp"/>
	<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="notEquals">
	<s:set id="deleteAtionURL" value="%{'deleteWireTransferAction'}" scope="request"></s:set>
	</ffi:cinclude>
	<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true">
	<s:set id="deleteAtionURL" value="%{'deleteWireTemplateAction'}" scope="request"></s:set>
	</ffi:cinclude>
	<ffi:cinclude value1="${isSkip}" value2="true">
		<s:set id="deleteAtionURL" value="%{'skipWireTransferAction'}" scope="request"></s:set>
	</ffi:cinclude>
<% }%>

<div align="center" class="approvalDialogHt">
<% String wireType = null; %>
<ffi:getProperty name="${wireTask}" property="WireType" assignTo="wireType"/>

<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true">
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Template Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span width="115" class="sectionsubhead sectionLabel"><!--L10NStart-->Template Name:<!--L10NEnd--></span>
				<span width="238" class="columndata"><ffi:getProperty name="WireTransfer" property="WireName"/></span>
			</div>
			<div class="inlineBlock">
				<span width="115" class="sectionsubhead sectionLabel"><!--L10NStart-->Template Category:<!--L10NEnd--></span>
				<span width="223" class="columndata"><ffi:getProperty name="WireTransfer" property="WireCategory"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Template Nickname:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="NickName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Template Scope:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WireScope"/></span>
			</div>
		</div>
	</div>
</div>
</ffi:cinclude>
<div  class="blockHead toggleClick">Beneficiary details <span class="sapUiIconCls icon-positive"></span></div>
<div class="toggleBlock hidden">
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Beneficiary Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_NUMBER %>:</span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.AccountNum"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_TYPE %>:</span>
				<span class="columndata">
					<ffi:getProperty name="WireTransfer" property="WirePayee.AccountTypeDisplayName"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Beneficiary Name:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Name"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Nickname:<!--L10NEnd--></span>
				    <span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.NickName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Address 1:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Address 2:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street2"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->City:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.City"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->State/Province:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.StateDisplayName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->ZIP/Postal Code:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.ZipCode"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Contact Person:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Contact"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><!--L10NStart-->Beneficiary Scope:<!--L10NEnd--></span>
			<span class="columndata"><ffi:setProperty name="WirePayeeScopes" property="Key" value="${WireTransfer.WirePayee.PayeeScope}"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></span>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Beneficiary Bank Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Bank Name:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.BankName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Address 1:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Address 2:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->City:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->State/Province:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.State"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->ZIP/Postal Code:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.ZipCode"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><!--L10NStart-->FED ABA:<!--L10NEnd--></span>
			<span class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingFedWire"/></span>
		</div>
	</div>
</div>					
</div>	
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Debit Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Debit Account<!--L10NEnd-->:</span>
				<span class="columndata">
					<ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/>
				</span>
			</div>
			<ffi:setProperty name="temp_amount" value="Amount"/>
			<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_TEMPLATE %>" operator="equals">
				<ffi:setProperty name="temp_amount" value="Template Limit"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="equals">
				<ffi:setProperty name="temp_amount" value="Template Limit"/>
			</ffi:cinclude>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><ffi:getProperty name="temp_amount"/><ffi:removeProperty name="temp_amount"/>:</span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="Amount"/></span>
			</div>
		</div>
		<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="notEquals">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Value Date<!--L10NEnd-->:</span>
					<span class="columndata"><ffi:getProperty name="WireTransfer" property="DueDate"/></span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Date To Post<!--L10NEnd-->:</span>
					<span class="columndata"><ffi:getProperty name="WireTransfer" property="DateToPost"/>:</span>
				</div>
			</div>
		</ffi:cinclude>
		<ffi:cinclude value1="${WireTransfer.Type}" value2="<%= String.valueOf(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) %>" operator="equals">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Frequency<!--L10NEnd-->:</span>
					<span class="columndata"><ffi:getProperty name="WireTransfer" property="Frequency"/>:</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><!--L10NStart-->Payments<!--L10NEnd-->:</span>
					<span class="columndata">
						<ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="equals">
							<!--L10NStart-->Unlimited<!--L10NEnd-->
						</ffi:cinclude>
						<ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="notEquals">
							<ffi:getProperty name="WireTransfer" property="NumberTransfers"/>
						</ffi:cinclude>
					</span>
				</div>
			</div>
		</ffi:cinclude>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead">Reference for Beneficiary<!--L10NEnd--></span>: <ffi:getProperty name="WireTransfer" property="Comment"/></div>
	<div  class="blockHead"><!--L10NStart-->Originator to Beneficiary Information<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<% String info = ""; %>
			<ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo1"/>
			<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo2}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo2"/></ffi:cinclude>
			<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo3}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo3"/></ffi:cinclude>
			<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo4}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo4"/></ffi:cinclude>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->By Order Of Information<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Name: <!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Address 1:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress1"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Address 2:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Address 3:<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress3"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><!--L10NStart-->Account Number:<!--L10NEnd--></span>
			<span class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAccount"/></span>
		</div>
	</div>
</div>
<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="notEquals">
	<%if (!isSkip && WireDefines.WIRE_TYPE_RECURRING.equals(wireType)) { %>
		<div class="marginTop10 columndata_error" align="left"><s:text name="jsp.wire.delete.warning"/></div>
	<% } %>
</ffi:cinclude>
<div align="center" class="sectionhead marginTop20" style="color:red">
	<% if (isSkip) { %><!--L10NStart-->Are you sure you want to skip this book wire transfer?<!--L10NEnd--><% } else { %>
	 <ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="equals"><!--L10NStart-->Are you sure you want to delete this book wire transfer template?<!--L10NEnd--></ffi:cinclude>
	 <% } %>
</div>
<s:form action="%{#request.deleteAtionURL}" namespace="/pages/jsp/wires" method="post" name="TransferNew" id="deleteWireFormId">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden name="collectionName" value="%{#session.collectionName}" id="collectionName"/>
<s:hidden name="wireBackURLForJS" value="%{#session.wireBackURL}" id="wireBackURLForJS"/>
<input type="hidden" name="wireIndex" value="<ffi:getProperty name="wireIndex"/>"/>
<input type="hidden" name="ID" value="<ffi:getProperty name="ID"/>"/>
<input type="hidden" name="wireType" value="<ffi:getProperty name="WireTransfer" property="WireType"/>"/>
<input type="hidden" name="isRecModel1" id="isRecModel1" value="false"/>
</s:form>
<div class="ffivisible" style="height:160px;">&nbsp;</div>
<div class="ui-widget-header customDialogFooter">
	<sj:a 
		id="cancelWireTemplateLink" 
		button="true" 
		onClickTopics="closeDialog" 
		title="Cancel"
		><s:text name="jsp.default_82" />
	</sj:a>

	<%if (!isSkip && WireDefines.WIRE_TYPE_RECURRING.equals(wireType)) { %>
		<sj:a id="deleteRecWireModel"
			  button="true"
			formIds="deleteWireFormId" 
		targets="resultmessage" 
		title="Delete Wire" 
		onClickTopics="setRecModel"
		onCompleteTopics="deleteWiresTransfersCompleteTopics" 
		onSuccessTopics="deleteWiresTransfersSuccessTopics" 
		onErrorTopics="deleteWiresTransfers"
					><s:text name="jsp.default.delete_orig_trans"/></sj:a>
	<% } %>
	<script>
			ns.wire.deleteTransferFromBatchURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/${wireBackURL}"/>' ;
		</script>
	<% if (!isSkip) { %>
	<% if (request.getParameter("wireIndex") != null) { %>
		<sj:a 
			id="deleteWireLink" 
			formIds="deleteWireFormId" 
			targets="inputDiv" 
			button="true"   
			title="DELETE TRANSFER FROM BATCH" 
			onCompleteTopics="deleteWireInBatchCompleteTopics" 
			onSuccessTopics="deleteWireInBatchSuccessTopics" 
			effectDuration="1500" 
			><s:text name="jsp.default_162" />
		</sj:a>
	<% }else{ %>
	<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="equals">
		<sj:a 
			id="deleteWireTemplateLink" 
			formIds="deleteWireFormId" 
			targets="resultmessage" 
			button="true"   
			title="Delete Template" 
			onCompleteTopics="deleteWireTemplateCompleteTopics" 
			onSuccessTopics="deleteWireTemplateSuccessTopics"
			onErrorTopics="deleteWireTemplateErrorTopics"
			effectDuration="1500" 
			><s:text name="jsp.default_162" />
		</sj:a>
	</ffi:cinclude>
	<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="notEquals">
		<% if (!isRecModel) { %>
			<sj:a 
				id="deleteWireTransferFormID" 
				formIds="deleteWireFormId" 
				targets="resultmessage" 
				button="true"   
				title="DELETE TRANSFER" 
				onCompleteTopics="deleteWiresTransfersCompleteTopics" 
				onSuccessTopics="deleteWiresTransfersSuccessTopics"
				onErrorTopics="deleteWiresTransfersErrorTopics"
				effectDuration="1500" 
				><s:text name="jsp.default.delete_this_trans" />
			</sj:a>
		<% } %>
	</ffi:cinclude>
	<% } 
	} else { %>
	<sj:a 
		id="skipWireLink" 
		formIds="deleteWireFormId" 
		targets="resultmessage" 
		button="true"   
		title="Skip Wire" 
		onCompleteTopics="skipWiresTransfersCompleteTopics" 
		onSuccessTopics="deleteWiresTransfersSuccessTopics" 
		onErrorTopics="deleteWiresTransfersErrorTopics" 
		effectDuration="1500"
		><s:text name="jsp.default.Skip_Wire" />
	</sj:a>	
	<% } %>							
</div>
<ffi:removeProperty name="deleteTemplate"/>
<ffi:removeProperty name="wireDeleteURL"/>
<ffi:removeProperty name="wireDeleteButtonName"/>
</div>
			<table width="750" border="0" cellspacing="0" cellpadding="0">

				<tr>
					<td class="ltrow2_color">
						<div align="center">
							<%-- <table border="0" cellspacing="0" cellpadding="3" width="711">
							  <ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="notEquals">
								<ffi:cinclude value1="${WireTransfer.Type}" value2="<%= String.valueOf(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) %>" operator="equals">
								<% if (!isSkip) { %>
									<tr>
									    <td class="sectionsubhead" colspan="3" style="color:red" align="center">
									    <!--L10NStart-->WARNING: Deleting recurring wire instance will also delete all other pending recurring instances of the model<!--L10NEnd--><br><br>
									    </td>
									</tr>
								<% } %>
								</ffi:cinclude>
								</ffi:cinclude>
								<tr>
                                    <td align="left" class="tbrd_b sectionhead">&gt;&nbsp;
									<% if (isSkip) { %><!--L10NStart-->Are you sure you want to skip this book wire transfer<!--L10NEnd--><% } else { %>
										<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="equals"><!--L10NStart-->Are you sure you want to delete this book wire transfer template?<!--L10NEnd--></ffi:cinclude>
										<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="notEquals"><!--L10NStart-->Are you sure you want to delete this book wire transfer?<!--L10NEnd--></ffi:cinclude>
									<% } %>
                                    </td>
								</tr>
							</table> --%>
							<br>
						<%-- <ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true">
							<table border="0" cellspacing="0" cellpadding="3" width="715">
								<tr>
									<td align="left" class="tbrd_b sectionhead" colspan="4">&gt; <!--L10NStart-->Template Info<!--L10NEnd--></td>
								</tr>
								<tr>
									<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Template Name<!--L10NEnd--></td>
									<td align="left" width="238" class="columndata"><ffi:getProperty name="WireTransfer" property="WireName"/></td>
									<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Template Category<!--L10NEnd--></td>
									<td align="left" width="223" class="columndata"><ffi:getProperty name="WireTransfer" property="WireCategory"/></td>
								</tr>
								<tr>
									<td align="left" class="sectionsubhead"><!--L10NStart-->Template Nickname<!--L10NEnd--></td>
									<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="NickName"/></td>
									<td align="left" class="sectionsubhead"><!--L10NStart-->Template Scope<!--L10NEnd--></td>
									<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WireScope"/></td>
								</tr>
							</table>
							<br>
						</ffi:cinclude> --%>
	<!--<table width="715" cellpadding="0" cellspacing="0" border="0">
 <tr>
		<td align="left" class="tbrd_b sectionhead" colspan="3">&gt; L10NStartSend Wire ToL10NEnd</td>
	</tr>
	<tr>
		<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr> -->
	<%--<tr>
		<td width="357" valign="top">
			 <table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td align="left" class="sectionhead" colspan="2">&gt; <!--L10NStart-->Beneficiary Info<!--L10NEnd--></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_ACCOUNT_NUMBER %></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.AccountNum"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><%= LABEL_ACCOUNT_TYPE %></td>
					<td align="left" class="columndata">
						<ffi:getProperty name="WireTransfer" property="WirePayee.AccountTypeDisplayName"/>
					</td>
				</tr>
				<tr>
					<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Beneficiary Name<!--L10NEnd--></td>
					<td align="left" width="228" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Name"/></td>
				</tr>
				<tr>
				    <td align="left" class="sectionsubhead"><!--L10NStart-->Nickname<!--L10NEnd--></td>
				    <td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.NickName"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Address 1<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Address 2<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street2"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->City<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.City"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->State/Province<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.StateDisplayName"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->ZIP/Postal Code<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.ZipCode"/></td>
				</tr>				
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Contact Person<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Contact"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Beneficiary Scope<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:setProperty name="WirePayeeScopes" property="Key" value="${WireTransfer.WirePayee.PayeeScope}"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></td>
				</tr>				
			</table>
		</td>
		<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td align="left" class="sectionhead" colspan="2">&gt; <!--L10NStart-->Beneficiary Bank Info<!--L10NEnd--></td>
				</tr>
				<tr>
					<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Bank Name<!--L10NEnd--></td>
					<td align="left" width="220" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.BankName"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Address 1<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Address 2<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street2"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->City<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.City"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->State/Province<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.State"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->ZIP/Postal Code<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.ZipCode"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->FED ABA<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingFedWire"/></td>
				</tr>
			</table> 
		</td>
	</tr>
</table>	--%>					
<%-- <table width="715" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="left" class="tbrd_b sectionhead" colspan="3">&gt; <!--L10NStart-->Debit Info<!--L10NEnd--></td>
	</tr>
	<tr>
		<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="357" valign="top">
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Debit Account<!--L10NEnd--></td>
					<td align="left" width="228" class="columndata">
						<ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/>
					</td>
				</tr>
<ffi:setProperty name="temp_amount" value="Amount"/>
<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_TEMPLATE %>" operator="equals">
	<ffi:setProperty name="temp_amount" value="Template Limit"/>
</ffi:cinclude>
<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="equals">
	<ffi:setProperty name="temp_amount" value="Template Limit"/>
</ffi:cinclude>
				<tr>
					<td align="left" class="sectionsubhead"><ffi:getProperty name="temp_amount"/><ffi:removeProperty name="temp_amount"/></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="Amount"/></td>
				</tr>
				<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="notEquals">
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Value Date<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="DueDate"/></td>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Date To Post<!--L10NEnd--></td>
					<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="DateToPost"/></td>
				</tr>
				</ffi:cinclude>
				<ffi:cinclude value1="${WireTransfer.Type}" value2="<%= String.valueOf(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) %>" operator="equals">
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Frequency<!--L10NEnd--></td>
					<td align="left" class="columndata">
						<ffi:getProperty name="WireTransfer" property="Frequency"/>
				</tr>
				<tr>
					<td align="left" class="sectionsubhead"><!--L10NStart-->Payments<!--L10NEnd--></td>
					<td align="left" class="columndata">
					<ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="equals">
						<!--L10NStart-->Unlimited<!--L10NEnd-->
					</ffi:cinclude>
					<ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="notEquals">
						<ffi:getProperty name="WireTransfer" property="NumberTransfers"/>
					</ffi:cinclude>
					</td>
				</tr>
				</ffi:cinclude>
			</table>
		</td>
		<td colspan="2">&nbsp;</td>
	</tr>
</table> --%>
<%-- <table width="715" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td align="left" class="tbrd_b"><span class="sectionhead">&gt; <!--L10NStart-->Reference for Beneficiary<!--L10NEnd--></span></td>
			</tr>
			<tr>
			 	<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="Comment"/></td>
			</tr>
			<tr>
				<td align="left" class="tbrd_b"><span class="sectionhead">&gt; <!--L10NStart-->Originator to Beneficiary Information<!--L10NEnd--></span></td>
			</tr>
			<tr>
			 	<td align="left" class="columndata">
				<% String info = ""; %>
				<ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo1"/>
				<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo2}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo2"/></ffi:cinclude>
				<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo3}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo3"/></ffi:cinclude>
				<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo4}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo4"/></ffi:cinclude>
				</td>
			</tr>
		</table>
		<table width="715" border="0" cellspacing="0" cellpadding="3">
			<tr>
				<td align="left" class="tbrd_b" colspan="2"><span class="sectionhead">&gt; <!--L10NStart-->By Order Of Information<!--L10NEnd--></span></td>
			</tr>
			<tr>
				<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Name<!--L10NEnd--></td>
				<td align="left" width="600" class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfName"/></td>
			</tr>
			<tr>
				<td align="left" class="sectionsubhead"><!--L10NStart-->Address 1<!--L10NEnd--></td>
				<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress1"/></td>
			</tr>
			<tr>
				<td align="left" class="sectionsubhead"><!--L10NStart-->Address 2<!--L10NEnd--></td>
				<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress2"/></td>
			</tr>
			<tr>
				<td align="left" class="sectionsubhead"><!--L10NStart-->Address 3<!--L10NEnd--></td>
				<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAddress3"/></td>
			</tr>
			<tr>
				<td align="left" class="sectionsubhead"><!--L10NStart-->Account Number<!--L10NEnd--></td>
				<td align="left" class="columndata"><ffi:getProperty name="WireTransfer" property="ByOrderOfAccount"/></td>
			</tr>
		</table> --%>
				<%-- <table width="98%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td align="center">
						<s:form action="%{#request.deleteAtionURL}" namespace="/pages/jsp/wires" method="post" name="TransferNew" id="deleteWireFormId">
						<form action="<ffi:urlEncrypt url="${wireDeleteURL}" />" method="post" name="TransferNew">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<s:hidden name="collectionName" value="%{#session.collectionName}" id="collectionName"/>
						<s:hidden name="wireBackURLForJS" value="%{#session.wireBackURL}" id="wireBackURLForJS"/>
						<input type="hidden" name="wireIndex" value="<ffi:getProperty name="wireIndex"/>"/>
						<input type="hidden" name="ID" value="<ffi:getProperty name="WireTransfer" property="ID"/>"/>
						<input type="hidden" name="wireType" value="<ffi:getProperty name="WireTransfer" property="WireType"/>"/>
							</s:form>
							<div align="center">
								<sj:a 
									id="cancelWireTemplateLink" 
									button="true" 
									onClickTopics="closeDialog" 
									title="Cancel"
									><s:text name="jsp.default_82" />
								</sj:a>
								<script>
 									ns.wire.deleteTransferFromBatchURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/${wireBackURL}"/>' ;
 								</script>
								<% if (request.getParameter("wireIndex") != null) { %>
									<sj:a 
										id="deleteWireLink" 
										formIds="deleteWireFormId" 
										targets="inputDiv" 
										button="true"   
										title="DELETE TRANSFER FROM BATCH" 
										onCompleteTopics="deleteWireInBatchCompleteTopics" 
										onSuccessTopics="deleteWireInBatchSuccessTopics" 
										effectDuration="1500" 
										><s:text name="jsp.default_DELETE_TRANSFER_FROM_BATCH" />
									</sj:a>
								<% }else{ %>
								<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="equals">
									<sj:a 
										id="deleteWireTemplateLink" 
										formIds="deleteWireFormId" 
										targets="resultmessage" 
										button="true"   
										title="Delete Template" 
										onCompleteTopics="deleteWireTemplateCompleteTopics" 
										onSuccessTopics="deleteWireTemplateSuccessTopics"
										onErrorTopics="deleteWireTemplateErrorTopics"
										effectDuration="1500" 
										><s:text name="jsp.default_DELETE_TEMPLATE" />
									</sj:a>
								</ffi:cinclude>
								<ffi:cinclude value1="${IsDeleteWireTemplate}" value2="true" operator="notEquals">
									<sj:a 
										id="deleteWireTransferFormID" 
										formIds="deleteWireFormId" 
										targets="resultmessage" 
										button="true"   
										title="DELETE TRANSFER" 
										onCompleteTopics="deleteWiresTransfersCompleteTopics" 
										onSuccessTopics="deleteWiresTransfersSuccessTopics"
										onErrorTopics="deleteWiresTransfersErrorTopics"
										effectDuration="1500" 
										><s:text name="jsp.default_DELETE_TRANSFER" />
									</sj:a>
								</ffi:cinclude>	
								<% } %>							
							</div>
							<ffi:removeProperty name="deleteTemplate"/>
							<ffi:removeProperty name="wireDeleteURL"/>
							<ffi:removeProperty name="wireDeleteButtonName"/>
						</td>
					</tr>
				</table> --%>
			</div>
			</td>
		</tr>
	</table>
		</div>
<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-positive").addClass("icon-negative")
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-positive").removeClass("icon-negative")
			}
		});
		$.subscribe('setRecModel', function(event,data) {
			document.getElementById('isRecModel1').value = 'true';
		});
	});

</script>
<ffi:removeProperty name="isRecModel" />