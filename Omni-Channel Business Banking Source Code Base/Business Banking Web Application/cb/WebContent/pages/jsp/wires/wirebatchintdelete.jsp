<%--
This is the delete international wire batch page.  It is used only for
International wire batches. All other types use wirebatchdelete.jsp.

Pages that request this page
----------------------------
wire_transfers_list.jsp (included in wiretransfers.jsp, wire summary page)
	Delete button next to a wire batch.

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
DELETE BATCH requests wirebatchdeletesend.jsp

Pages included in this page
---------------------------
payments/inc/wire_set_wire.jsp
	Sets a wire transfer or wire batch with a given ID into the session
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatchintdelete" className="moduleHelpClass"/>
<ffi:setProperty name='PageHeading' value='Delete Wire Batch'/>
<ffi:setProperty name='PageText' value=''/>
<% boolean isSkip = Boolean.valueOf(request.getParameter("isSkip"));%>
<s:set var="isSkip" scope="request"><%=isSkip%></s:set>
<ffi:cinclude value1="${isSkip}" value2="true">
	<s:set id="deleteActionURL" value="%{'skipWireBatch'}" scope="request"></s:set>
</ffi:cinclude>
<ffi:cinclude value1="${isSkip}" value2="true" operator="notEquals">
	<s:set id="deleteActionURL" value="%{'deleteWireBatch'}" scope="request"></s:set>
</ffi:cinclude>
<%-- Set the wire batch into the session 
<s:include value="%{#session.PagesPath}/wires/inc/wire_set_wire.jsp"/>

<ffi:setProperty name="collectionToShow" value="WireBatch.Wires"/>
<ffi:cinclude value1="${collectionName}" value2="PendingApprovalWireBatches" operator="notEquals">
    <ffi:object id="GetWiresByBatchId" name="com.ffusion.tasks.wiretransfers.GetWiresByBatchId" scope="session" />
        <ffi:setProperty name="GetWiresByBatchId" property="BatchId" value="${WireBatch.BatchID}"/>
        <ffi:setProperty name="GetWiresByBatchId" property="CollectionSessionName" value="WireTransfersByBatchId"/>
    <ffi:process name="GetWiresByBatchId"/>
    <ffi:setProperty name="collectionToShow" value="WireTransfersByBatchId"/>
</ffi:cinclude>
--%>
<ffi:setProperty name="WireBatch" property="DateFormat" value="${UserLocale.DateFormat}"/>
<div align="center">
<div class="blockWrapper" role="form" aria-labelledby="batchInfoHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="batchInfoHeader">Batch Information</h2><!--L10NEnd--></div>
	<div class="blockContent label120">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">Batch Name:</span>
                <span class="columndata"><ffi:getProperty name="WireBatch" property="BatchName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">Application Type:</span>
                 <span class="columndata"><ffi:getProperty name="WireBatch" property="BatchType"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">Value Date:</span>
                <span class="columndata"><ffi:getProperty name="WireBatch" property="DueDate"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">Settlement Date:</span>
                <span class="columndata"><ffi:getProperty name="WireBatch" property="SettlementDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">Debit Currency:</span>
                <span class="columndata"><ffi:getProperty name="WireBatch" property="OrigCurrency"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">Payment Currency:</span>
                <span class="columndata"><ffi:getProperty name="WireBatch" property="PayeeCurrencyType"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">Exchange Rate:</span>
                <span class="columndata"><ffi:getProperty name="WireBatch" property="ExchangeRate"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">Math Rule:</span>
                <span class="columndata"><ffi:getProperty name="WireBatch" property="MathRule"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel">Contract Number:</span>
            <span class="columndata"><ffi:getProperty name="WireBatch" property="ContractNumber"/></span>
       </div>
	</div>
</div>

<div class="blockWrapper marginTop20" role="form" aria-labelledby="includedBatchesHeader">
	<div  class="blockHead"><h2 id="includedBatchesHeader"><s:text name="jsp.default_239"/></h2></div>
</div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr class="header">
			<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
              <td align="center" class="sectionsubhead">Host ID</td>
			</ffi:cinclude>
			<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
              <td align="center" class="sectionsubhead">Beneficiary</td>
			</ffi:cinclude>
              <td class="sectionsubhead" align="left">Status</td>
              <td class="sectionsubhead" align="right">Amount</td>
          </tr>
          <ffi:list collection="WireBatch.Wires" items="wire">
          <tr>
			<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
                            <td align="center" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
			</ffi:cinclude>
			<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
                            <td align="center" class="columndata" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
                                <ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
			</ffi:cinclude>
            <td class="columndata" align="left" nowrap>
                <ffi:getProperty name="wire" property="StatusName"/>
            </td>
            <td class="columndata" align="right" nowrap><ffi:getProperty name="wire" property="AmountForBPW"/> (USD)</td>
        </tr>
        </ffi:list>
        <tr>
            <td class="columndata">&nbsp;</td>
            <td class="columndata" align="right" valign="bottom" nowrap>Total:</td>
            <td class="tbrd_t sectionsubhead" width="1%" align="right" valign="bottom" nowrap><ffi:getProperty name="WireBatch" property="TotalDebitForBPW"/> (USD)</td>
        </tr>
    </table>
	</div>
</div>                                        
        <!--     <table width="750" cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td class="columndata ltrow2_color" align="center">
                    <table width="715" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="tbrd_b sectionhead">&gt; Delete Batch</td>
                        </tr>
                        <tr>
                            <td class="sectionsubhead">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="sectionsubhead" align="center">Are you sure you want to delete this batch?</td>
                        </tr>
                        <tr>
                            <td class="sectionsubhead">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" class="tbrd_b sectionhead">&gt; Batch Information</td>
                        </tr>
                    </table> -->
                    <%-- <table width="715" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="357" valign="top">
                                <table width="355" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" width="115" class="sectionsubhead">Batch Name</td>
                                        <td align="left" width="228" class="columndata"><ffi:getProperty name="WireBatch" property="BatchName"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Application Type</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="WireBatch" property="BatchType"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Value Date</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="WireBatch" property="DueDate"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Settlement Date</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="WireBatch" property="SettlementDate"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                            <td width="348" valign="top">
                                <table width="347" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" width="115" class="sectionsubhead">Debit Currency</td>
                                        <td align="left" width="220" class="columndata"><ffi:getProperty name="WireBatch" property="OrigCurrency"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Payment Currency</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="WireBatch" property="PayeeCurrencyType"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Exchange Rate</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="WireBatch" property="ExchangeRate"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Math Rule</td>
                                        <td class="columndata"><ffi:getProperty name="WireBatch" property="MathRule"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Contract Number</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="WireBatch" property="ContractNumber"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table> --%>

                   <!--  <br>
                    <table width="98%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="tbrd_b sectionhead">&gt; Included Batches</td>
                        </tr>
                    </table> -->
                    <%-- <table width="375" border="0" cellspacing="0" cellpadding="3">
                        <tr>
<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
                            <td align="left" class="sectionsubhead">Host ID</td>
</ffi:cinclude>
<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
                            <td align="left" class="sectionsubhead">Beneficiary</td>
</ffi:cinclude>
                            <td class="sectionsubhead" align="left">Status</td>
                            <td class="sectionsubhead" align="right">Amount</td>
                        </tr>
                        <ffi:list collection="WireBatch.Wires" items="wire">
                        <tr>
<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
                            <td align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
</ffi:cinclude>
<ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
                            <td align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
                                <ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
</ffi:cinclude>
                            
                            <td class="columndata" align="left" nowrap>
                                <ffi:getProperty name="wire" property="StatusName"/>
                            </td>
                            
                            <td class="columndata" align="right" nowrap><ffi:getProperty name="wire" property="AmountForBPW"/> (USD)</td>
                        </tr>
                        </ffi:list>
                        <tr>
                            <td class="columndata">&nbsp;</td>
                            <td class="columndata" align="right" valign="bottom" nowrap>Total:</td>
                            <td class="tbrd_t sectionsubhead" width="1%" align="right" valign="bottom" nowrap><ffi:getProperty name="WireBatch" property="TotalDebitForBPW"/> (USD)</td>
                        </tr>
                    </table> --%>
                    <%-- <s:form action="/pages/jsp/wires/deleteWireBatch.action" method="post" name="FormName" id="deleteWireBatchFormId" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<s:hidden name="batchId" value="%{WireBatch.ID}" id="collectionName"/>
                    <sj:a 
						button="true" 
						onClickTopics="closeDialog" 
						title="Cancel"
						>CANCEL
					</sj:a>
					<sj:a 
					        id="deleteWireBatch"
						formIds="deleteWireBatchFormId" 
						targets="resultmessage" 
						button="true"   
						title="Delete BATCH" 
						onCompleteTopics="deleteWireCompleteTopics" 
						onSuccessTopics="deleteWireSuccessTopics" 
						effectDuration="1500" 
						>DELETE BATCH
					</sj:a>
                    </form>
                    </s:form> 
                    </td>
                    <td align="right" background="/cb/web/multilang/grafx/payments/corner_cellbg.gif"><br>
                        <br>
                    </td>
                </tr>
            </table>--%>
<div align="center" class="sectionhead marginTop20" style="color:red">
<% if (isSkip) { %>Are you sure you want to skip this batch ?<% } else { %>
Are you sure you want to delete this batch?
<% } %>
</div>
<s:form action="%{#request.deleteActionURL}" namespace="/pages/jsp/wires" method="post" name="FormName" id="deleteWireBatchFormId" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden name="batchId" value="%{WireBatch.ID}" id="collectionName"/>
<div class="btn-row">
	<sj:a 
		button="true" 
		onClickTopics="closeDialog" 
		title="Cancel"
		>CANCEL
	</sj:a>
	<% if (!isSkip) { %>
	<sj:a 
	    id="deleteWireBatch"
		formIds="deleteWireBatchFormId" 
		targets="resultmessage" 
		button="true"   
		title="Delete BATCH" 
		onCompleteTopics="deleteWireCompleteTopics" 
		onSuccessTopics="deleteWireSuccessTopics" 
		effectDuration="1500" 
		>DELETE BATCH
	</sj:a>
	<% } else { %>
	<sj:a 
		id="skipWireLink" 
		formIds="deleteWireBatchFormId" 
		targets="resultmessage" 
		button="true"   
		title="Skip Wire Batch" 
		onCompleteTopics="skipWiresTransfersCompleteTopics" 
		onSuccessTopics="deleteWiresTransfersSuccessTopics" 
		onErrorTopics="deleteWiresTransfersErrorTopics" 
		effectDuration="1500"
		>SKIP WIRE BATCH
	</sj:a>
	<% } %>
</div>
</s:form>            
</div>

