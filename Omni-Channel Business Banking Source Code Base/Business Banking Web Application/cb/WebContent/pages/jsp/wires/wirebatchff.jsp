<%--
This page is for adding a freeform wire batch

Pages that request this page
----------------------------
wirebatch.jsp (add wire batch)
	As an included file
wirebatchffconfirm.jsp (freeform wire batch confirm (non-international))
wirebatchintffconfirm.jsp (freeform international wire batch confirm)
	BACK button
wiretransfernew.jsp (add/edit domestic and international wire)
wirebook.jsp (add/edit book wire)
wiredrawdown.jsp (add/edit drawdown wire)
wirefed.jsp (add/edit fed wire)
wiretransferconfirm.jsp (add/edit wire transfer confirm)
	CANCEL button
wiretransfersend.jsp (add/edit wire transfer final send/save page)
	Javascript auto-refresh
wiretransferdelete.jsp (domestic and international wire)
wirefeddelete.jsp (fed wire)
wiredrawdowndelete.jsp (drawdown wire)
wirebookdelete.jsp (book wire)
	CANCEL button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
SUBMIT WIRE BATCH requests one of the following:
	wirebatchffconfirm.jsp (for freeform batches (non-international))
	wirebatchintffconfirm.jsp (for freeform international batches)
ADD WIRE requests one of the following:
	wiretransfernew.jsp (domestic and international wire)
	wirefed.jsp (fed wire)
	wiredrawdown.jsp (drawdown wire)
	wirebook.jsp (book wire)
Edit button next to wires requests one of the following:
	wiretransfernew.jsp (domestic and international wire)
	wirefed.jsp (fed wire)
	wiredrawdown.jsp (drawdown wire)
	wirebook.jsp (book wire)
Delete button next to wires requests one of the following:
	wiretransferdelete.jsp (domestic and international wire)
	wirefeddelete.jsp (fed wire)
	wiredrawdowndelete.jsp (drawdown wire)
	wirebookdelete.jsp (book wire)
Calendar button requests calendar.jsp

Pages included in this page
---------------------------
payments/inc/wire_batch_select.jsp
	Creates the Choose Wire Batch Type drop-down
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_batch_fields.jsp
	edit fields for a non-international wire
payments/inc/wire_batch_int_fields.jsp
	edit fields for an international wire
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatchff" className="moduleHelpClass"/>
<%
if( request.getParameter("DontInitializeBatch") != null ){ session.setAttribute("DontInitializeBatch", request.getParameter("DontInitializeBatch")); }
if( request.getParameter("wireDestination") != null ){ session.setAttribute("wireDestination", request.getParameter("wireDestination")); }
%>
<ffi:setProperty name="wireBackURL" value="addWireBatchAction_reload.action"/>
<ffi:setProperty name="wireSaveButton" value="ADD TO BATCH"/>
<ffi:setProperty name="wirebatch_confirm_form_url" value="addWireBatchAction"/>
<ffi:removeProperty name="loadFromTemplate"/>
<%-- 
<ffi:cinclude value1="${FXCurrencies}" value2="" operator="equals">
	<ffi:object id="GetCurrencies" name="com.ffusion.tasks.fx.GetCurrencies" scope="session"/>
	<ffi:setProperty name="GetCurrencies" property="FXSessionName" value="FXCurrencies"/>
	<ffi:process name="GetCurrencies"/>
	<ffi:removeProperty name="GetCurrencies"/>
</ffi:cinclude>

<ffi:cinclude value1="${SetWireTransferPayee}" value2="" operator="equals">
	<ffi:object id="SetWireTransferPayee" name="com.ffusion.tasks.wiretransfers.SetWireTransferPayee" scope="session"/>
</ffi:cinclude>
 --%>
<% String task = "WireBatchObject"; %>
<ffi:setProperty name="nextWireIndex" value="0"/>
<%-- 
 <ffi:cinclude value1="${DontInitializeBatch}" value2="" operator="equals">
	<ffi:cinclude value1="${wireDestination}" value2="" operator="equals">
		<ffi:setProperty name="wireDestination" value="<%= WireDefines.WIRE_DOMESTIC %>"/>
	</ffi:cinclude>
	<ffi:object id="AddWireBatch" name="com.ffusion.tasks.wiretransfers.AddWireBatch" scope="session"/>
		<ffi:setProperty name="AddWireBatch" property="Initialize" value="true"/>
		<ffi:setProperty name="AddWireBatch" property="BatchType" value="<%= WireDefines.WIRE_BATCH_TYPE_FREEFORM %>"/>
	<ffi:process name="AddWireBatch"/>
	<ffi:setProperty name="AddWireBatch" property="DateFormat" value="${UserLocale.DateFormat}"/>
	<ffi:setProperty name="AddWireBatch" property="BatchDestination" value="${wireDestination}"/>
	<ffi:setProperty name="AddWireBatch" property="SettlementDate" value="${AddWireBatch.DueDate}"/>

	<ffi:setProperty name="nextWireIndex" value="0"/>
</ffi:cinclude>
--%>


<ffi:setProperty name="wireBatchConfirmURL" value="wirebatchffconfirm.jsp"/>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DOMESTIC}">
	<ffi:setL10NProperty name="PageHeading" value="New Domestic Wire Transfer Batch"/>
</s:if>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
	<ffi:setL10NProperty name="PageHeading" value="New International Wire Transfer Batch"/>
</s:if>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_FED}">
	<ffi:setL10NProperty name="PageHeading" value="New FED Wire Transfer Batch"/>
</s:if>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DRAWDOWN}">
	<ffi:setL10NProperty name="PageHeading" value="New Drawdown Wire Transfer Batch"/>
</s:if>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_BOOK}">
	<ffi:setL10NProperty name="PageHeading" value="New Book Wire Transfer Batch"/>
</s:if>

 <%-- 
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_DOMESTIC %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wiretransfernew.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wiretransferdelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New Domestic Wire Transfer Batch"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wiretransfernew.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wiretransferdelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New International Wire Transfer Batch"/>
	<ffi:setProperty name="wireBatchConfirmURL" value="wirebatchintffconfirm.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_FED %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wirefed.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wirefeddelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New FED Wire Transfer Batch"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wiredrawdown.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wiredrawdowndelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New Drawdown Wire Transfer Batch"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_BOOK %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wirebook.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wirebookdelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New Book Wire Transfer Batch"/>
</ffi:cinclude>
--%>
<span id="PageHeading" style="display:none;"><ffi:getProperty name='PageHeading'/></span>

 


<ffi:removeProperty name="DontInitializeBatch"/>
<ffi:removeProperty name="DontInitialize"/>
<ffi:removeProperty name="editWireTransfer"/>
<ffi:removeProperty name="WireTransfer"/>
<ffi:removeProperty name="WireTransferPayee"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<script>

function popCalendar(calTarget) {
		window.open("<ffi:getProperty name='SecurePath'/>calendar.jsp?calForm=frmBatch&calTarget=" + calTarget,"","width=350,height=300,resizable=false,scrollbars=false,menubar=false,toolbar=false,directories=false,location=false,status=false");
}

function changeForm(dest) {
	var _batchDestination ="<%= ((String)request.getAttribute("WireDestination"))%> "
	if (dest == "<%= WireDefines.WIRE_BATCH_TYPE_TEMPLATE %>") page = "addWireBatchTemplateAction_initTemplate.action?wireDestination=" + _batchDestination;
	if (dest == "<%= WireDefines.WIRE_BATCH_TYPE_FREEFORM %>") page = "addWireBatchAction_init.action?wireDestination=" + _batchDestination;
	document.frmBatch.DontInitializeBatch.value = "";
	document.frmBatch.wireDestination.value = dest;
	$.ajax({  
		  type: "POST",   
		  data: $("#frmBatchID").serialize(),    
		  url: "/cb/pages/jsp/wires/" + page,     
		  success: function(data) {  	  
			 $("#inputDiv").html(data);
			 
			 $.publish("common.topics.tabifyNotes");
			 if(accessibility){
				$("#wireBatchTypeSelectID-menu").focus();
			 }
			 
			 var heading = $('#PageHeading').html();
			var $title = $('#details .portlet-title');
			$title.html(heading);
		  }   
		});
}

function addWire(idx) {
	frm = document.frmBatch;
	frm.wireIndex.value=idx;
	var targetAction="/cb/pages/jsp/wires/addWireBatchAction_addWireTransfer.action";
	$("#frmBatchID").attr("action",targetAction); 
	
}

function setDest(dest) {
	if (dest == "<%= WireDefines.WIRE_HOST %>") page = "wirebatchhost.jsp";
	else page = "wirebatch.jsp";
	document.frmBatch.DontInitializeBatch.value = "";
	document.frmBatch.wireDestination.value = dest;
	document.frmBatch.action = page;
	document.frmBatch.submit();
}

function eidtWireTransferFromBatch(wireIndex){
	frm = document.frmBatch;
	frm.wireIndex.value=wireIndex;
	$.ajax({  
	  type: "POST",   
	  data: $("#frmBatchID").serialize(),    
	  url: "/cb/pages/jsp/wires/addWireTransferAction_editWireTransferFromBatch.action",     
	  success: function(data) {  	  
		 $("#inputDiv").html(data);
	  }   
	});
}

function deleteWireTransferFromBatch(wireIndex){
	frm = document.frmBatch;
	frm.wireIndex.value=wireIndex;
	$.ajax({  
	  type: "POST",   
	  data: $("#frmBatchID").serialize(),    
	  url: "/cb/pages/jsp/wires/addWireBatchAction_deleteWireTransfer.action",     
	  success: function(data) {  	  
		 $("#deleteWireInBatchyDialogID").html(data).dialog('open');
	  }   
	});
}

</script>
<span id="formerrors"></span>
<s:form  name="frmBatch"  id="frmBatchID" action="addWireBatchAction_verify" namespace="/pages/jsp/wires"  method="post" theme="simple" validate="false">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="DontInitializeBatch" value="true">
<input type="hidden" name="wireIndex" value=""/>
<input type="hidden" name="wireDestination" value=""/>
<input type="hidden" name="isModifyAction" value=""/>
<div class="leftPaneWrapper" role="form" aria-labelledby="batchEntryHeader">
	<div class="leftPaneInnerWrapper">
		<div class="header" id=""><h2 id="batchEntryHeader">Wire Batch Entry Details</h2></div>
		<div class="leftPaneInnerBox">
			<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
				<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_int_fields.jsp"/>
			</s:if>
			<s:else>
				<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_fields.jsp"/>
			</s:else>
		</div>
	</div>
</div>
<div class="confirmPageDetails" id="wireTransferBatchDiv" style="padding:0 !important;">
<%-- CONTENT START --%>
			<div class="paneWrapper">
				<div class="paneInnerWrapper">
					<table width="100%" cellpadding="3" cellspacing="0" border="0">
					<tr class="header">
						<td class="sectionsubhead" width="33%">
							<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DRAWDOWN}"><!--L10NStart--><h2>Drawdown Account Name</h2><!--L10NEnd--></s:if>
							<s:else><!--L10NStart--><h2>Beneficiary Name</h2><!--L10NEnd--></s:else>					
						</td>
						<td class="sectionsubhead" width="33%"><!--L10NStart--><h2>Amount</h2><!--L10NEnd--></td>
						<td class="sectionsubhead" width="33%"><h2>Action</h2></td>
					</tr>
					<% int count = 0; %>
					<ffi:list collection="WireBatchObject.Wires" items="wire">
					<tr style="padding:10px 0">
						<td class="columndata"><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
							<ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude>
						</td>
						<td class="columndata"><ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/></td>
						<td class="columndata"><%-- <ffi:link url="${wireBatchAddTransferURL}?wireIndex=${wire.WireIndex}&wireTask=WireTransfer&templateTask=&editWireTransfer=true"><img src="/cb/web/multilang/grafx/payments/i_edit.gif" width="19" height="14" alt="Edit" border="0"></ffi:link>
						<ffi:link url="${wireBatchDeleteTransferURL}?wireIndex=${wire.WireIndex}"><img src="/cb/web/multilang/grafx/payments/i_trash.gif" width="14" height="14" alt="Delete" border="0"></ffi:link>--%>
						<a class='ui-button' title='Edit' href='#' onClick='eidtWireTransferFromBatch(<%=count%>)'>
						<span class='sapUiIconCls icon-wrench'></span></a>
						<a class='ui-button' title='Delete' href='#' onClick='deleteWireTransferFromBatch(<%=count%>)'><span class='sapUiIconCls icon-delete'></span></a>
						</td>
					</tr>
					<% count++; %>
					</ffi:list>
				</table>
				</div>
			</div>
            <div class="required marginTop20" align="center">* <!--L10NStart-->indicates a required field<!--L10NEnd--></div>
           	
		<%-- 	<input type="Button" value="CANCEL" class="submitbutton" onclick="document.location='wiretransfers.jsp';">
			<input type="Button" value="SUBMIT WIRE BATCH" class="submitbutton" onclick="document.frmBatch.submit();">
			<input type="Button" value="ADD WIRE" class="submitbutton" onclick="addWire(<ffi:getProperty name="nextWireIndex"/>);"> --%>
			<div class="btn-row">
				 <sj:a id="cancelFormButtonOnVerify" 
					button="true"  summaryDivId="summary" buttonType="cancel"
					onClickTopics="showSummary,cancelWireTransferForm"
				 ><s:text name="jsp.default_82" /><!-- CANCEL -->
				 </sj:a>
				
				 <sj:a id="verifyWireBatchID"
					formIds="frmBatchID"  
				 	targets="verifyDiv" 
				 	validate="true" 
					validateFunction="customValidation"
					button="true" 
					onBeforeTopics="verifyWireBatchBeforeTopics"
					onSuccessTopics="verifyWireBatchSuccessTopics">
						<s:text name="jsp.default_395" /><!-- SUBMIT WIRE BATCH -->
					</sj:a>
					
				<sj:a targets="inputDiv" 
					id="addWireOnNewBatchID" 
					formIds="frmBatchID"
					nextwireindex='<%= session.getAttribute("nextWireIndex") %>' 
					onClickTopics="addWireClickTopic"
					button="true" 
					onCompleteTopics="addWireOnBatchCompleteTopic">
						<s:text name="jsp.default_29" /><!-- ADD WIRE -->
					</sj:a> 
			</div>
				 <%--   --%>
			<%-- </form>--%>
		
<%-- CONTENT END --%>
</div>
</s:form>