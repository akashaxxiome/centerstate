<%--
This page processes the add/edit wire benenficiary request, and displays the
reference numbers

Pages that request this page
----------------------------
wireaddpayeeconfirm.jsp
	CONFIRM/SAVE BENEFICIARY button

Pages this page requests
------------------------
DONE requests one of the following:
	wirepayees.jsp
	wiretransfernew.jsp
	wirebook.jsp 
	wiredrawdown.jsp
	wirefed.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ include file="../common/wire_labels.jsp"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<ffi:help id="payments_wirepayeeaddsave" className="moduleHelpClass"/>
<ffi:setL10NProperty name='PageHeading' value='Wire Beneficiary'/>
<ffi:setProperty name="PageText" value=""/>

<% String task = ""; %>
<ffi:getProperty name="payeeTask" assignTo="task"/>
<% if (task == null) task = "AddWireTransferPayee"; %>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>"  operator="notEquals">
	<%-- move this in to Action --%>
	<%-- 
	<ffi:setProperty name="<%= task %>" property="Process" value="true" />
	<ffi:process name="<%= task %>"/>
	--%>
</ffi:cinclude>
<%-- End: Dual approval processing --%>


<%
String type = "";
String REGULAR = WireDefines.PAYEE_TYPE_REGULAR;
%>
<% String intBankSize = ""; %>
<ffi:getProperty name="${payeeTask}" property="IntermediaryBanks.Size" assignTo="intBankSize"/>
<% if (intBankSize == null) {
	intBankSize = "0";
}
%>
<ffi:getProperty name="${payeeTask}" property="PayeeDestination" assignTo="type"/>
<%
if (type == null) type = REGULAR;
if( session.getAttribute("returnPage") != null){ 
	String retPage = session.getAttribute("returnPage").toString() + "?isBeneficiaryModified=true";
	session.setAttribute("returnPage", retPage); 	
} 

%>

<ffi:object id="LocalizeCountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="LocalizeCountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="LocalizeCountryResource" />
<ffi:object id="GetCodeForBankLookupCountryName" name="com.ffusion.tasks.util.GetCodeForBankLookupCountryName" scope="session"/>
<div id="wireBeneficiesConfirmFormID">

<s:if test="%{isPendingApprovalMsg}">
<div class="blockWrapper">
	<div  class="blockHead" style="color:red">
		 <!--L10NStart-->The following wires that were approval pending or rejected were reprocessed because their beneficiary was modified. As a result, the status of those wires changed as follows:<!--L10NEnd-->
	</div>
<% boolean toggle = false; String band; %>
    <ffi:list collection="wireHistories" items="wire">
    <% toggle = !toggle; band = (toggle) ? "class='ltrow1_color'" : "";%>
    <%-- <tr <%= band %>> </tr> --%>
	<div <%= band %> class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Item Type<!--L10NEnd-->:</span>
				<span class="columndata"><ffi:setProperty name="errorWireTransTypes" property="Key" value="${wire.TransType}"/><ffi:getProperty name="errorWireTransTypes" property="Value"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->TrackingID<!--L10NEnd-->:</span>
				<span class="columndata"><ffi:getProperty name="wire" property="TrackingID"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->SubmittedBy<!--L10NEnd-->:</span>
				<span class="columndata"><ffi:getProperty name="wire" property="SubmittedBy"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Old&nbsp;Status<!--L10NEnd-->:</span>
				<span class="columndata"><ffi:getProperty name="wire" property="<%= WireDefines.OLD_STATUS_KEY%>"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Current&nbsp;Status<!--L10NEnd-->:</span>
				<span class="columndata"><ffi:getProperty name="wire" property="StatusName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Amount<!--L10NEnd-->:</span>
				<span class="columndata"><ffi:getProperty name="wire" property="Amount"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Date<!--L10NEnd-->:</span>
				<span class="columndata"><ffi:getProperty name="wire" property="Date"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Message<!--L10NEnd-->:</span>
				<span class="columndata"><ffi:getProperty name="wire" property="Comment"/></span>
			</div>
		</div>
	</div>
</ffi:list>
</div>
</s:if>
<div class="leftPaneWrapper"><div class="leftPaneInnerWrapper">
	<div class="header">Beneficiary Summary</div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyWireBeneficiaryLabel"><%=type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME%>:</span>
				<span class="inlineSection floatleft labelValue" id="verifyWireBeneficiaryValue"><ffi:getProperty name="${payeeTask}" property="Name"/></span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><%=LABEL_BENEFICIARY_TYPE%>:</span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="${payeeTask}" property="PayeeDestination"/></span>
		</div>
	</div>
</div></div>
<div class="confirmPageDetails">
	<div class="blockWrapper">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">
			<div class="blockRow pending marginTop10"><span id="wireBeneficiaryResultMessage"><span class="sapUiIconCls icon-future"></span> <s:text name="jsp.wire.payeeadd.da.success"/></span></div>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="notEquals">
			<div class="blockRow completed marginTop10"><span id="wireBeneficiaryResultMessage"><span class="sapUiIconCls icon-accept"></span> <s:text name="jsp.wire.payeeadd.success"/></span></div>
		</ffi:cinclude>	
		<div  class="blockHead"><%= type.equals(DRAWDOWN) ? "Debit Account" : "Beneficiary" %> Info</div>
		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmPayeeAccountNoLabel" class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_NUMBER  %>: </span>
					<span id="confirmPayeeAccountNoValue"class="columndata"><ffi:getProperty name="${payeeTask}" property="AccountNum"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmPayeeAccountTypeLabel" class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_TYPE  %>: </span>
					<span id="confirmPayeeAccountTypeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="AccountType"/></span>
				</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmPayeeNameLabel" class="sectionsubhead sectionLabel"><%= type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME  %>: </span>
					<span id="confirmPayeeNameValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="Name"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmPayeeNickNameLabel" class="sectionsubhead sectionLabel"><%= LABEL_NICKNAME  %>: </span>
					<span id="confirmPayeeNickNameValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="NickName"/></span>
				</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmPayeeAddress1Label" class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_1  %>: </span>
					<span id="confirmPayeeAddress1Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="Street"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmPayeeAddress2Label" class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_2  %>: </span>
					<span id="confirmPayeeAddress2Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="Street2"/></span>
				</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmPayeeCityLabel" class="sectionsubhead sectionLabel"><%= LABEL_CITY  %>: </span>
					<span id="confirmPayeeCityValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="City"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmPayeeStateLabel" class="sectionsubhead sectionLabel"><%= type.equals(REGULAR) ? LABEL_STATE_PROVINCE : LABEL_STATE  %>: </span>
					<span id="confirmPayeeStateValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="State"/></span>
				</div>
			</div>
			<div class="blockRow">
				<span id="confirmPayeeZipCodeLabel" class="sectionsubhead sectionLabel"><%= type.equals(REGULAR) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE  %>: </span>
				<span id="confirmPayeeZipCodeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="ZipCode"/></span>
			</div>
			<% if (type.equals(REGULAR)) { %>
				<div class="blockRow">
					<span id="confirmPayeeCountryLabel" class="sectionsubhead sectionLabel"><%= LABEL_COUNTRY  %>: </span>
					<% String bankLookupCountryName = null; %>
					<ffi:getProperty name="${payeeTask}" property="Country" assignTo="bankLookupCountryName"/>
					<ffi:setProperty name="GetCodeForBankLookupCountryName" property="CountryName" value="<%= bankLookupCountryName %>" />
					<ffi:process name="GetCodeForBankLookupCountryName" />
					<ffi:setProperty name="LocalizeCountryResource" property="ResourceID" value="Country${GetCodeForBankLookupCountryName.CountryCode}" />
					<span id="confirmPayeeCountryValue" class="columndata"><ffi:getProperty name="LocalizeCountryResource" property="Resource"/></span>
				</div>
			<% } %>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmPayeeContactLabel" class="sectionsubhead sectionLabel"><%= LABEL_CONTACT_PERSON  %>: </span>
					<span id="confirmPayeeContactValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="Contact"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmPayeeTypeLabel" class="sectionsubhead sectionLabel"><%= LABEL_BENEFICIARY_TYPE  %>: </span>
					<span id="confirmPayeeTypeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="PayeeDestination"/></span>
				</div>
			</div>
			<div class="blockRow">
				<span id="confirmPayeeScopeLabel" class="sectionsubhead sectionLabel"><%= LABEL_BENEFICIARY_SCOPE  %>: </span>
				<span id="confirmPayeeScopeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="PayeeScope"/></span>
			</div>
		</div>
	</div>
	<div class="blockWrapper">
		<div  class="blockHead"><%= type.equals(DRAWDOWN) ? "Debit" : "Beneficiary" %> Bank Info</div>
		<div class="blockContent">
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmBankNameLabel" class="sectionsubhead sectionLabel"><%= type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME %>: </span>
					<span id="confirmBankNameValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.BankName"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmBankAddress1Label" class="sectionsubhead sectionLabel"><%= LABEL_BANK_ADDRESS_1 %>: </span>
					<span id="confirmBankAddress1Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.Street"/></span>
				</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmBankAddress2Label" class="sectionsubhead sectionLabel"><%= LABEL_BANK_ADDRESS_2 %>: </span>
					<span id="confirmBankAddress2Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.Street2"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmBankCityLabel" class="sectionsubhead sectionLabel"><%= LABEL_BANK_CITY %>: </span>
					<span id="confirmBankCityValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.City"/></span>
				</div>
			</div>
			
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmBankStateLabel" class="sectionsubhead sectionLabel"><%= type.equals(REGULAR) ? LABEL_STATE_PROVINCE : LABEL_STATE %>: </span>
					<span id="confirmBankStateValue" class="columndata">
							<s:if test="%{WireTransferPayee.DestinationBank.StateDisplayName != null && WireTransferPayee.DestinationBank.StateDisplayName != ''}">
								<ffi:getProperty name="${payeeTask}" property="DestinationBank.StateDisplayName" />
							</s:if>
							<s:else>
								<ffi:getProperty name="${payeeTask}" property="DestinationBank.State" />
							</s:else>
					</span>
				</div>
				<div class="inlineBlock">
					<span id="confirmBankZipCodeLabel" class="sectionsubhead sectionLabel"><%= type.equals(REGULAR) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE %>: </span>
					<span id="confirmBankZipCodeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.ZipCode"/></span>
				</div>
			</div>
			<% if (type.equals(REGULAR)) { %>
	           <div class="blockRow">
					<span id="confirmBankCountryLabel" class="sectionsubhead sectionLabel"><%= LABEL_COUNTRY %>: </span>
					<% String bankLookupCountryName = null; %>
					<ffi:getProperty name="${payeeTask}" property="DestinationBank.Country" assignTo="bankLookupCountryName"/>
					<ffi:setProperty name="GetCodeForBankLookupCountryName" property="CountryName" value="<%= bankLookupCountryName %>" />
					<ffi:process name="GetCodeForBankLookupCountryName" />
					<ffi:setProperty name="LocalizeCountryResource" property="ResourceID" value="Country${GetCodeForBankLookupCountryName.CountryCode}" />
					<span id="confirmBankCountryValue" class="columndata"><ffi:getProperty name="LocalizeCountryResource" property="Resource"/></span>
	             </div>
	       <% } %>
	       <div class="blockRow">
	       		<span id="confirmFEDLabel" class="sectionsubhead sectionLabel"><%= LABEL_FED_ABA %>: </span>
				<span id="confirmFEDValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingFedWire"/></span>
			</div>
			<% if (type.equals(REGULAR)) { %>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span id="confirmSWIFTLabel" class="sectionsubhead sectionLabel"><%= LABEL_SWIFT %>: </span>
						<span id="confirmSWIFTValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingSwift"/></span>
					</div>
					<div class="inlineBlock">
						<span id="confirmCHIPSLabel" class="sectionsubhead sectionLabel"><%= LABEL_CHIPS %>: </span>
						<span id="confirmCHIPSValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingChips"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span id="confirmNationalLabel" class="sectionsubhead sectionLabel"><%= LABEL_NATIONAL %>: </span>
						<span id="confirmNationalValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingOther"/></span>
					</div>
					<div class="inlineBlock">
						<span id="confirmIBANLabel" class="sectionsubhead sectionLabel"><%= LABEL_IBAN %>: </span>
						<span id="confirmIBANValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.IBAN"/></span>
					</div>
				</div>
				<ffi:cinclude value1="${intBankSize}"  value2="0" operator="notEquals" >
	                 <div class="blockRow">
						<span id="confirmCorrespondantBankAccountNoLabel" class="sectionsubhead sectionLabel"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
						<span id="confirmCorrespondantBankAccountNoValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.CorrespondentBankAccountNumber"/></span>
	                 </div>
	            </ffi:cinclude>
			 <% } %>
		</div>
	</div>
             <%--<table width="750" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="columndata ltrow2_color">
                    <div align="center">
                   <table width="715" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="357" valign="top">
                                 <table width="355" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td class="sectionhead" colspan="2">&gt; <%= type.equals(DRAWDOWN) ? "Debit Account" : "Beneficiary" %> Info</td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeAccountNoLabel" class="sectionsubhead"><%= LABEL_ACCOUNT_NUMBER %></td>
										<td id="confirmPayeeAccountNoValue"class="columndata"><ffi:getProperty name="${payeeTask}" property="AccountNum"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeAccountTypeLabel" class="sectionsubhead"><%= LABEL_ACCOUNT_TYPE %></td>
										<td id="confirmPayeeAccountTypeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="AccountType"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeNameLabel" width="115" class="sectionsubhead"><%= type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME %></td>
										<td id="confirmPayeeNameValue" width="228" class="columndata"><ffi:getProperty name="${payeeTask}" property="Name"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeNickNameLabel" class="sectionsubhead"><%= LABEL_NICKNAME %></td>
										<td id="confirmPayeeNickNameValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="NickName"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeAddress1Label" class="sectionsubhead"><%= LABEL_ADDRESS_1 %></td>
										<td id="confirmPayeeAddress1Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="Street"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeAddress2Label" class="sectionsubhead"><%= LABEL_ADDRESS_2 %></td>
										<td id="confirmPayeeAddress2Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="Street2"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeCityLabel" class="sectionsubhead"><%= LABEL_CITY %></td>
										<td id="confirmPayeeCityValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="City"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeStateLabel" class="sectionsubhead"><%= type.equals(REGULAR) ? LABEL_STATE_PROVINCE : LABEL_STATE %></td>
										<td id="confirmPayeeStateValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="State"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeZipCodeLabel" class="sectionsubhead"><%= type.equals(REGULAR) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE %></td>
										<td id="confirmPayeeZipCodeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="ZipCode"/></td>
                                    </tr>
                                    <% if (type.equals(REGULAR)) { %>
                                    <tr>
										<td id="confirmPayeeCountryLabel" class="sectionsubhead"><%= LABEL_COUNTRY %></td>
										<% String bankLookupCountryName = null; %>
										<ffi:getProperty name="${payeeTask}" property="Country" assignTo="bankLookupCountryName"/>
										<ffi:setProperty name="GetCodeForBankLookupCountryName" property="CountryName" value="<%= bankLookupCountryName %>" />
										<ffi:process name="GetCodeForBankLookupCountryName" />
										<ffi:setProperty name="LocalizeCountryResource" property="ResourceID" value="Country${GetCodeForBankLookupCountryName.CountryCode}" />
										<td id="confirmPayeeCountryValue" class="columndata"><ffi:getProperty name="LocalizeCountryResource" property="Resource"/></td>
                                    </tr>
                                    <% } %>
                                    <tr>
										<td id="confirmPayeeContactLabel" class="sectionsubhead"><%= LABEL_CONTACT_PERSON %></td>
										<td id="confirmPayeeContactValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="Contact"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmPayeeTypeLabel" class="sectionsubhead"><%= LABEL_BENEFICIARY_TYPE %></td>
										<td id="confirmPayeeTypeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="PayeeDestination"/></td>
                                    </tr>

                                    <tr>
										<td id="confirmPayeeScopeLabel" class="sectionsubhead"><%= LABEL_BENEFICIARY_SCOPE %></td>
										<td id="confirmPayeeScopeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="PayeeScope"/></td>
                                    </tr>                                   
                                </table> 
                            </td>
                            <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>--%>
                            <%-- <td width="348" valign="top">
                                <table width="347" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td class="sectionhead" colspan="2">&gt; <%= type.equals(DRAWDOWN) ? "Debit" : "Beneficiary" %> Bank Info</td>
                                    </tr>
                                    <tr>
										<td id="confirmBankNameLabel" width="115" class="sectionsubhead"><%= type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME %></td>
										<td id="confirmBankNameValue" width="220" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.BankName"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmBankAddress1Label" class="sectionsubhead"><%= LABEL_BANK_ADDRESS_1 %></td>
										<td id="confirmBankAddress1Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.Street"/></td>
									</tr>
									<tr>
										<td id="confirmBankAddress2Label" class="sectionsubhead"><%= LABEL_BANK_ADDRESS_2 %></td>
										<td id="confirmBankAddress2Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.Street2"/></td>
									</tr>
									<tr>
										<td id="confirmBankCityLabel" class="sectionsubhead"><%= LABEL_BANK_CITY %></td>
										<td id="confirmBankCityValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.City"/></td>
									</tr>
									<tr>
										<td id="confirmBankStateLabel" class="sectionsubhead"><%= type.equals(REGULAR) ? LABEL_STATE_PROVINCE : LABEL_STATE %></td>
										<td id="confirmBankStateValue" class="columndata">
												<s:if test="%{WireTransferPayee.DestinationBank.StateDisplayName != null && WireTransferPayee.DestinationBank.StateDisplayName != ''}">
													<ffi:getProperty name="${payeeTask}" property="DestinationBank.StateDisplayName" />
												</s:if>
												<s:else>
													<ffi:getProperty name="${payeeTask}" property="DestinationBank.State" />
												</s:else>
										
										</td>
									</tr>
									<tr>
										<td id="confirmBankZipCodeLabel" class="sectionsubhead"><%= type.equals(REGULAR) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE %></td>
										<td id="confirmBankZipCodeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.ZipCode"/></td>
                                    </tr>
                                    <% if (type.equals(REGULAR)) { %>
                                    <tr>
										<td id="confirmBankCountryLabel" class="sectionsubhead"><%= LABEL_COUNTRY %></td>
										<% String bankLookupCountryName = null; %>
										<ffi:getProperty name="${payeeTask}" property="DestinationBank.Country" assignTo="bankLookupCountryName"/>
										<ffi:setProperty name="GetCodeForBankLookupCountryName" property="CountryName" value="<%= bankLookupCountryName %>" />
										<ffi:process name="GetCodeForBankLookupCountryName" />
										<ffi:setProperty name="LocalizeCountryResource" property="ResourceID" value="Country${GetCodeForBankLookupCountryName.CountryCode}" />
										<td id="confirmBankCountryValue" class="columndata"><ffi:getProperty name="LocalizeCountryResource" property="Resource"/></td>
                                    </tr>
                                    <% } %>
                                    <tr>
										<td id="confirmFEDLabel" class="sectionsubhead"><%= LABEL_FED_ABA %></td>
										<td id="confirmFEDValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingFedWire"/></td>
                                    </tr>
                                    <% if (type.equals(REGULAR)) { %>
                                    <tr>
										<td id="confirmSWIFTLabel" class="sectionsubhead"><%= LABEL_SWIFT %></td>
										<td id="confirmSWIFTValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingSwift"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmCHIPSLabel" class="sectionsubhead"><%= LABEL_CHIPS %></td>
										<td id="confirmCHIPSValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingChips"/></td>
                                    </tr>
                                    <tr>
										<td id="confirmNationalLabel" class="sectionsubhead"><%= LABEL_NATIONAL %></td>
										<td id="confirmNationalValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingOther"/></td>
                                    </tr>
                                     <tr>
										<td id="confirmIBANLabel" class="sectionsubhead"><%= LABEL_IBAN %></td>
										<td id="confirmIBANValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.IBAN"/></td>
                                    </tr>
                                    <ffi:cinclude value1="${intBankSize}"  value2="0" operator="notEquals" >
                                    <tr>
										<td id="confirmCorrespondantBankAccountNoLabel" class="sectionsubhead"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
										<td id="confirmCorrespondantBankAccountNoValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
                                    </tr>
                                    </ffi:cinclude>
                                    <% } %>
                                </table>
                            </td> 
                        </tr>
                    </table>--%>

<% if (type.equals(REGULAR)) { %>
<% String filter = ""; int counter = 0; %>
<ffi:getProperty name="${payeeTask}" property="IntermediaryBanks.Filter" assignTo="filter"/>
<ffi:setProperty name="${payeeTask}" property="IntermediaryBanks.Filter" value="ACTION!!del"/>
<% if (!intBankSize.equals("0")) { %>

<ffi:list collection="${payeeTask}.IntermediaryBanks" items="Bank1">
<div class="blockWrapper">
<div  class="blockHead">Intermediary Bank Info</div>
<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_SWIFT %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_CHIPS %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_NATIONAL %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</span>
			</div>
		</div>
		
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
			<span class="columndata">
				<% if(counter == 0) { %>
				&nbsp;
				<% } else { %>
				<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
				<% } counter++; %>&nbsp;
			</span>
		</div>
</div>
</div>   
</ffi:list>
<ffi:setProperty name="${payeeTask}" property="IntermediaryBanks.Filter" value="<%= filter %>"/>
                 
<% } %>
<% } %>
<div class="blockWrapper">
	<div  class="blockHead">Comments</div>
		<div class="blockContent">
		<div class="blockRow">
			<ffi:getProperty name="${payeeTask}" property="Memo" />
		</div>
	</div>
</div>
<form action="<ffi:urlEncrypt url="${returnPage}" />" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" id="returnPageIDForJS" value="<ffi:getProperty name='returnPage'/>"/>
<s:hidden name="isBeneficiary" value="%{#session.isBeneficiary}" id="isBeneficiary"/>
<s:hidden name="IsPending" value="%{#session.IsPending}" id="IsPending"/>
<input type="hidden" id="isHaveReturnPageParamIDForJS" value="<ffi:getProperty name='returnPage'/>"/>
<%-- <input type="submit" class="submitbutton" name="submitButtonName" value="DONE">--%>
	<div class="btn-row">
		<sj:a id="sendFormButtonOnConfirm" 
		button="true" summaryDivId="summary" buttonType="done"
		onclick="ns.wire.payeeSaveOnClick();"
		><s:text name="jsp.default_175" /><!-- DONE -->
		</sj:a>							
	</div>
</form>
</div>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<%-- Retrieve multiple category details information in session --%>
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
		<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="<%=IDualApprovalConstants.TRUE%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetDACategoryDetails"/>
</ffi:cinclude>
<ffi:removeProperty name="LocalizeCountryResource" />
<ffi:removeProperty name="GetCodeForBankLookupCountryName" />
<ffi:removeProperty name="WireTransferIntermediaryBanks"/>
<ffi:removeProperty name="WireTransferDestinationBanks"/>

