<%--
This page is used to Add and Edit host wire transfer.  Note, unlike the other
wire types, you cannot make host wire templates.

Pages that request this page
----------------------------
wiretransfers.jsp (wire transfer summary)
	NEW WIRE button (when host is selected in Wire Type)
wire_transfers_list.jsp (included in wiretransfers.jsp)
	Edit button next to a host wire transfer
wirehostconfirm.jsp (host wire transfer confirm)
	BACK button
wire_type_select.jsp (included in wire_common_init.jsp, which is in turn included
	in wiretransfernew.jsp, wirebook.jsp, wiredrawdown.jsp,	and wirefed.jsp)
	Changing the Wire Type to triggers a javascript function called setDest() in
	wire_scripts_js.jsp

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
SUBMIT WIRE requests wirehostconfirm.jsp
Choose Wire Type requests one of the following:
	wiretransfernew.jsp
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp
Calendar button requests calendar.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
payments/inc/wire_set_wire.jsp
	Sets a wire transfer or wire batch with a given ID into the session
payments/inc/wire_type_select.jsp
	Choose Wire Type drop-down list
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/wire_scripts_js.jsp
	Common javascript functions used for all wire types
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
common/checkAmount_js.jsp
	A common javascript that validates an amount is formatted properly
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/wire_labels.jsp"%>
<%@page import="com.ffusion.beans.wiretransfers.WireTransfer"%>

<ffi:help id="payments_wirehost" className="moduleHelpClass"/>

<%
if( request.getParameter("notTemplate") != null ) { session.removeAttribute("templateTask"); }
if( request.getParameter("DA_WIRE_PRESENT") != null ){ session.setAttribute("DA_WIRE_PRESENT", request.getParameter("DA_WIRE_PRESENT")); }
if( request.getParameter("ID") != null ){ session.setAttribute("ID", request.getParameter("ID")); }
if( request.getParameter("collectionName") != null ){ session.setAttribute("collectionName", request.getParameter("collectionName")); }
if( request.getParameter("pendApproval") != null ){ session.setAttribute("pendApproval", request.getParameter("dpendApproval")); }

%>
<%-- hints for bookmark --%>
<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
	<span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTransferLink,newSingleWiresTransferID</span>
	<span class="shortcutEntitlementClass" style="display:none;">Wires</span>
</ffi:cinclude>
<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
	<span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTemplateLink,newSingleWiresTemplateID</span>
	<span class="shortcutEntitlementClass" style="display:none;">Wires</span>
</ffi:cinclude>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wirehost.jsp?DontInitialize=true" URLEncrypt="true"/>
		
<%	String pgTxt = "<ffi:link url=\"wiretransfernew.jsp?wireTask=AddWireTransfer&DontInitialize=true&AddWireTransfer.WireDestination=" + com.ffusion.beans.wiretransfers.WireDefines.WIRE_DOMESTIC + "\">" +
	   "<img src=\"/cb/pages/${ImgExt}grafx/payments/button_domestic.gif\" alt=\"\" width=\"58\" height=\"16\" border=\"0\" hspace=\"3\"></ffi:link>" +
	   "<ffi:link url=\"wiretransfernew.jsp?wireTask=AddWireTransfer&DontInitialize=true&AddWireTransfer.WireDestination=" + com.ffusion.beans.wiretransfers.WireDefines.WIRE_INTERNATIONAL + "\">" +
	   "<img src=\"/cb/pages/${ImgExt}grafx/payments/button_international.gif\" alt=\"\" width=\"81\" height=\"16\" border=\"0\" hspace=\"3\"></ffi:link>" +
	   "<img src=\"/cb/pages/${ImgExt}grafx/payments/button_host_dim.gif\" alt=\"\" width=\"36\" height=\"16\" border=\"0\" hspace=\"3\">";
	session.setAttribute("PageText", pgTxt);
%>
	
	
<ffi:setProperty name='PageText' value='<img src="/cb/pages/${ImgExt}grafx/payments/button_domestic.gif" alt="" width="58" height="16" border="0" hspace="3"><img src="/cb/pages/${ImgExt}grafx/payments/button_international.gif" alt="" width="81" height="16" border="0" hspace="3"><img src="/cb/pages/${ImgExt}grafx/payments/button_host_dim.gif" alt="" width="36" height="16" border="0" hspace="3">'/>  

<span id="PageHeading" style="display:none;"><ffi:getProperty name='PageHeading'/></span>
<s:include value="%{#session.PagesPath}/wires/inc/wire_type_select.jsp"/>

<ffi:removeProperty name="pendApproval"/>

<s:include value="%{#session.PagesPath}/wires/inc/nav_menu_top_js.jsp" />
<s:include value="%{#session.PagesPath}/wires/inc/wire_scripts_js.jsp" />

<div align="center">
<table width="80%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="lightBackground" align="center">
<%-- CONTENT START --%>
			<input type="hidden" id="TEMP_CSRF_TOKEN" name="TEMP_CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			
			<s:form id="newWiresTransferFormID" namespace="/pages/jsp/wires" validate="false" name="WireNew" action="%{#attr.ValidateAction}" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="DontInitialize" value="true"/>
			<input type="hidden" name="delIntBank" value=""/>
			<input type="hidden" name="selectedCountry" value=""/>			
           

            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="tdTopBottomPadding">
				<!-- <tr>
					<td class="sectionhead" colspan="8">L10NStartHost Wire TransferL10NEnd</td>
				</tr> -->
				<tr>
					<td colspan="5"><span id="formerrors"></span></td>
				</tr>
				<tr>
					<td width="25%"><label for="hostID"><%= LABEL_HOST_ID %></label><span class="required">*</span></td>
                   	<td width="25%"><label for="amount"><%= LABEL_AMOUNT %></label><span class="required">*</span></td>
                   	<td width="25%"><label for="hostWireDueDateID"><%= LABEL_PROCESSING_DATE %></label><span class="required">*</span></td>
                   	<td width="25%"><label for="SettlementDateValue"><%= LABEL_SETTLEMENT_DATE %></label></td>
				</tr>
				
				<tr>
					<ffi:cinclude value1="${IsAdd}" value2="true">
    					<td><input tabindex="1" id="hostID" type="text" name="<ffi:getProperty name="wireTask"/>.HostID" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name='${wireTask}' property="HostID"/>" size="20" maxlength="32"></td>
                    </ffi:cinclude>
                    <ffi:cinclude value1="${IsModify}" value2="true">
    					<td class="columndata"><ffi:getProperty name="${wireTask}" property="HostID"/></td>
                    </ffi:cinclude>
					<td><input tabindex="2" type="text" id="amount" maxlength="16" name="<ffi:getProperty name='wireTask'/>.Amount" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name='${wireTask}' property="AmountValue.CurrencyStringNoSymbol"/>" size="20"></td>
					<td>
						<% 	//This is for datepicker
						String dueDateForDP=((WireTransfer)session.getAttribute("wireTransfer")).getDueDate();
						session.setAttribute("dueDateForDP",dueDateForDP);
						%>
						<sj:datepicker
							title="Choose date"
							id="hostWireDueDateID"
							name="%{#request.wireTask}.DueDateValue"
							value="%{#session.dueDateForDP}"
							maxlength="10"
							buttonImageOnly="true"
							cssClass="ui-widget-content ui-corner-all"
							cssStyle="vertical-align:middle"
							size="12"
							readonly="true" />
							<script>
								var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=AddHostWire.DueDate&wireDest=${WireDefines.HOST_WIRE}"/>';
					    			ns.common.enableAjaxDatepicker("hostWireDueDateID", tmpUrl);
							</script>
					</td>
					<td><% 
						String settlementDateForDP=((WireTransfer)session.getAttribute("wireTransfer")).getSettlementDate();
						session.setAttribute("settlementDateForDP",settlementDateForDP);	
						%>
						<sj:datepicker
							title="Choose date"
							id="hostWireSettleDateID"
							name="%{#request.wireTask}.SettlementDateValue"
							value="%{#session.settlementDateForDP}"
							maxlength="10"
							buttonImageOnly="true"
							cssClass="ui-widget-content ui-corner-all"
							cssStyle="vertical-align:middle"
							size="12"
							readonly="true" />
							<script>
								var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=AddHostWire.SettlementDate&wireDest=${WireDefines.HOST_WIRE}"/>';
			                    ns.common.enableAjaxDatepicker("hostWireSettleDateID", tmpUrl);
			                    $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>'); 
							</script>
					</td>
				</tr>
				
				<tr>
					<td><span id="<ffi:getProperty name='wireTask'/>.HostIDError"></span></td>
					<td><span id="<ffi:getProperty name='wireTask'/>.AmountError"></span></td>
					<td><span id="<ffi:getProperty name='wireTask'/>.DueDateError"></span></td>
					<td></td>
				</tr>
				
				<%-- <tr>
					<td><span class="sectionsubhead"><%= LABEL_HOST_ID %></span><span class="required">*</span></td>
                    <ffi:cinclude value1="${IsAdd}" value2="true">
    					<td><input tabindex="1" type="text" name="<ffi:getProperty name="wireTask"/>.HostID" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name='${wireTask}' property="HostID"/>" size="20" maxlength="32"><span id="<ffi:getProperty name='wireTask'/>.HostIDError"></span></td>
                    </ffi:cinclude>
                    <ffi:cinclude value1="${IsModify}" value2="true">
    					<td class="columndata"><ffi:getProperty name="${wireTask}" property="HostID"/></td>
                    </ffi:cinclude>
					
					<td><span class="sectionsubhead"><%= LABEL_AMOUNT %></span><span class="required">*</span></td>
					<td><input tabindex="2" type="text" maxlength="16" name="<ffi:getProperty name='wireTask'/>.Amount" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name='${wireTask}' property="AmountValue.CurrencyStringNoSymbol"/>" size="20"><span id="<ffi:getProperty name='wireTask'/>.AmountError"></span></td>
				
					<td><span class="sectionsubhead"><%= LABEL_PROCESSING_DATE %></span><span class="required">*</span></td>
						<td>
						<% 	//This is for datepicker
						String dueDateForDP=((WireTransfer)session.getAttribute("wireTransfer")).getDueDate();
						session.setAttribute("dueDateForDP",dueDateForDP);
						%>
						<sj:datepicker
							title="Choose date"
							id="hostWireDueDateID"
							name="%{#request.wireTask}.DueDateValue"
							value="%{#session.dueDateForDP}"
							maxlength="10"
							buttonImageOnly="true"
							cssClass="ui-widget-content ui-corner-all"
							cssStyle="vertical-align:middle"
							size="12"
							readonly="true" /><span id="<ffi:getProperty name='wireTask'/>.DueDateError"></span>
							<script>
								var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=AddHostWire.DueDate&wireDest=${WireDefines.HOST_WIRE}"/>';
					    			ns.common.enableAjaxDatepicker("hostWireDueDateID", tmpUrl);
							</script>
					</td>
					<td class="sectionsubhead"><%= LABEL_SETTLEMENT_DATE %></td>
					<td>
						
						<% 
						String settlementDateForDP=((WireTransfer)session.getAttribute("wireTransfer")).getSettlementDate();
						session.setAttribute("settlementDateForDP",settlementDateForDP);	
						%>
						<sj:datepicker
							title="Choose date"
							id="hostWireSettleDateID"
							name="%{#request.wireTask}.SettlementDateValue"
							value="%{#session.settlementDateForDP}"
							maxlength="10"
							buttonImageOnly="true"
							cssClass="ui-widget-content ui-corner-all"
							cssStyle="vertical-align:middle"
							size="12"
							readonly="true" />
						<script>
							var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=AddHostWire.SettlementDate&wireDest=${WireDefines.HOST_WIRE}"/>';
		                    ns.common.enableAjaxDatepicker("hostWireSettleDateID", tmpUrl);
						</script>
					</td>
				</tr> --%>
			</table>
			&nbsp;<br>
			<span class="required">* <!--L10NStart-->indicates a required field<!--L10NEnd--></span><br>

<%-- include javascript for checking the amount field. Note: Make sure you include this file after the collection has been populated.--%>
<s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />
<div class="btn-row">
				<sj:a id="cancelFormButtonOnInput"
					button="true" summaryDivId="summary" buttonType="cancel" 
					onClickTopics="showSummary,cancelWireTransferForm"
					>
					Cancel
				</sj:a>
				<sj:a
					id="verifyWiresTransferSubmit"
					formIds="newWiresTransferFormID"
				    targets="verifyDiv"
				    button="true"
				    validate="true"
				    validateFunction="customValidation"
				    onBeforeTopics="beforeVerifyWireTransferForm"
				    onCompleteTopics="verifyWireTransferFormComplete"
					onErrorTopics="errorVerifyWireTransferForm"
				    onSuccessTopics="successVerifyWireTransferForm"
					onclick="SelectAmount()"
					>Submit
				</sj:a>
</div>			
			</s:form>
<%-- CONTENT END --%>
		</td>
	</tr>
</table>
&nbsp;
</div>
