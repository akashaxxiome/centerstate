<%--
<p>This jsp page is used for displaying approved wire beneficiaries. It will display its
 data in Struts JQuery grid.</p>
 
 Pages that request this page
----------------------------
wireaddpayee.jsp
wireaddpayeebook.jsp
wireaddpayeedrawdown.jsp
wirepayeeview.jsp
wirepayeedelete.jsp

Pages this page requests
------------------------

Pages included in this page
---------------------------

--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<%
	if( request.getParameter("DA_WIRE_PRESENT") != null && request.getParameter("DA_WIRE_PRESENT").length() > 0 ){ session.setAttribute("DA_WIRE_PRESENT", request.getParameter("DA_WIRE_PRESENT")); }
%>

<%-- Clean wire properties fom session --%>
<ffi:removeProperty name="isBeneficiary"/>
<ffi:removeProperty name="returnPage"/>

<ffi:cinclude value1="${WirePayeeScopes}" value2="" operator="equals">
	<ffi:object id="WirePayeeScopes" name="com.ffusion.beans.util.StringTable" scope="session" />
	<ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_USER %>"/>
	<ffi:setProperty name="WirePayeeScopes" property="Value" value="User"/>
	<ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_BUSINESS %>"/>
	<ffi:setProperty name="WirePayeeScopes" property="Value" value="Business"/>
</ffi:cinclude>

<%-- Pending Approval Users changes island start --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	  <div id="wires_beneficiaryPendingApprovalSummary" class="portlet">
		<div class="portlet-header">
			<h1 class="portlet-title"><s:text name="jsp.default.label.pending.approval.changes"/></h1>
		</div>
	
		<div id="achPayeesPendingApprovalChangeDiv" class="portlet-content">
			<s:include value="%{#session.PagesPath}/wires/inc/da_wire_beneficiaries_list.jsp"/>
		</div>	
		
		<div id="wirePendingBeneficiarySummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	 </div>
</ffi:cinclude>
<div class="marginTop10"></div>
<%-- <div id="wireBeneficiariesGridTabs">
	<div class="portlet-header">
	    <div class="searchHeaderCls">
			<div class="summaryGridTitleHolder">
				<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_text_Wire_Beneficiaries"/>
				</span>
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
					
					<div class="gridTabDropdownHolder">
						<span class="gridTabDropdownItem" id='daPendingBeneficiaries' onclick="ns.common.showGridSummary('daPendingBeneficiaries')" >
							<s:text name="jsp.default_text_Pending_Approval_Changes"/>
						</span>
						<span class="gridTabDropdownItem" id='beneficiaries' onclick="ns.common.showGridSummary('beneficiaries')">
							<s:text name="jsp.default_text_Wire_Beneficiaries"/>
						</span>
					</div>
				</ffi:cinclude>
			</div>
		</div>
	</div>

	<div class="portlet-content">
		<div id="gridContainer" class="summaryGridHolderDivCls">
			<div id="beneficiariesSummaryGrid" gridId="wireBeneficiariesGridID" class="gridSummaryContainerCls" >
				<s:include value="%{#session.PagesPath}/wires/inc/wire_beneficiaries_list.jsp"/>
			</div>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				<div id="daPendingBeneficiariesSummaryGrid" gridId="daWireBeneficiariesGridID" class="gridSummaryContainerCls hidden" >
					wire beneficiaries pending island will be displayed here	
					<s:include value="%{#session.PagesPath}/wires/inc/da_wire_beneficiaries_list.jsp"/>
				</div>
			</ffi:cinclude>
		</div>
	</div>
	
	<div id="wireBeneficiariesSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>		
</div> --%>

<div id="wireBeneficiariesGridTabs" class="portlet">
	<div class="portlet-header">
				<h1 id="" class="portlet-title">
					<s:text name="jsp.default_text_Wire_Beneficiaries"/>
				</h1>
	</div>

	<div class="portlet-content">
		<div id="gridContainer" class="summaryGridHolderDivCls">
			<div id="beneficiariesSummaryGrid" gridId="wireBeneficiariesGridID" >
				<s:include value="%{#session.PagesPath}/wires/inc/wire_beneficiaries_list.jsp"/>
			</div>
		</div>
	</div>
	
	<div id="wireBeneficiariesSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('wireBeneficiariesGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>		
</div>

<script>

//Initialize portlet with settings icon
ns.common.initializePortlet("wires_beneficiaryPendingApprovalSummary");


//Initialize portlet with settings icon
ns.common.initializePortlet("wireBeneficiariesGridTabs");
</script>