<%--
This page does the final processing of the add wire from import request. At this
point, wires are saved to the back-end.

Pages that request this page
----------------------------
wiretransferimportview.jsp (import wire confirm page)
	SUBMIT button

Pages this page requests
------------------------
Javascript auto-refreshes to wiretransferimportresults.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:removeProperty name="verified"/>

<ffi:setProperty name="AddWireTransfersFromImport" property="Process" value="true"/>
<ffi:process name="AddWireTransfersFromImport"/>

<ffi:removeProperty name="AddWireTransfersFromImport"/>

<s:include value="%{#session.PagesPath}/wires/wiretransferimportresults.jsp"/>
<ffi:removeProperty name="tmp_url"/>
