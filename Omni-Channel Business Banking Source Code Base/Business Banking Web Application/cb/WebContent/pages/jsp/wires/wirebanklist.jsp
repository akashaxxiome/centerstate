<%--
This page is the wire bank search pop-up window that's used to lookup a wire
beneficiary bank or intermediary bank.

Pages that request this page
----------------------------
wire_addedit_bank_fields.jsp (Included on the add/edit wire transfer pages and the add/edit wire beneficiary pages.)
    SEARCH button (when no criteria has been entered on the page)
wirebanklistselect.jsp (bank lookup search results)
    SEARCH AGAIN button

Pages this page requests
------------------------
SEARCH requests wirebanklistselect.jsp
CLOSE does not request any page.  It just closes the pop-up window.

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
inc/timeout.jsp
    The meta tag that will redirect to invalidate-session.jsp after a
    predetermined amount of time.
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/wire_labels.jsp"%>
<%@ page import="com.ffusion.beans.wiretransfers.*" %>

<ffi:help id="payments_wirebanklist" className="moduleHelpClass"/>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web'/>/css/wires/wires.css" />

<% if( request.getParameter("searchAgain") != null && request.getParameter("searchAgain").length() > 0 ) session.setAttribute("searchAgain", request.getParameter("searchAgain")); %>
<% if( request.getParameter("Inter") != null && request.getParameter("Inter").length() > 0 ) { session.setAttribute("Inter", request.getParameter("Inter")); } %>
<% if( request.getParameter("Edit") != null && request.getParameter("Edit").length() > 0 )session.setAttribute("Edit", request.getParameter("Edit")); %>
<% if( request.getParameter("currentTask") != null && request.getParameter("currentTask").length() > 0 )session.setAttribute("currentTask", request.getParameter("currentTask")); %>
<script>
	$(function(){
		$("#getWireTransferBanksBankCountryID").selectmenu({width:'22.7em'});
		$("#getWireTransferBanksBankStateID").selectmenu({width:'22.7em'});
	});
</script>
<% String str = "";%>
<ffi:getProperty name="Inter" assignTo="str"/>
<SCRIPT LANGUAGE="JavaScript">
<!--
function doManualEntry(obj) {
	if(obj.name == "manualEntry") {
		document.BankForm.action = '<ffi:urlEncrypt url="wirebanklist.jsp?searchAgain=true" />';
	}
}
//-->
</SCRIPT>

<ffi:setProperty name="wirePayeeBeanName" value="WireTransferPayee"/>
<ffi:cinclude value1="${currentTask}" value2="AddWireTransferPayee" operator="equals">
	<ffi:setProperty name="wirePayeeBeanName" value="AddWireTransferPayee"/>
</ffi:cinclude>
<ffi:cinclude value1="${currentTask}" value2="EditWireTransferPayee" operator="equals">
	<ffi:setProperty name="wirePayeeBeanName" value="EditWireTransferPayee"/>
</ffi:cinclude>

<%--ffi:setProperty name="BackURL" value="${SecurePath}payments/wirebanklist.jsp" URLEncrypt="true"/--%>

        <script type="text/javascript"><!--
        	
        	$(document).ready(function() {
				/* $("#wireBankStateSelectedId a").addClass("width98prcnt"); */
        	})
                function updateBankStates(obj) {
        	
					$.ajax({   
						type: "POST",   
						url: "/cb/pages/jsp/wires/getWireTransferBanksAction_getStateForCountry.action",
						data: {countryName: obj.value, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
						success: function(data) {
							$("#wireBankStateSelectedId").html('<span></span>');
							$("#wireBankStateSelectedId").html(data);	
						}, 
						complete: function(){
							if($("#getWireTransferBanksBankStateID").length > 0){
								$("#wireBankStateLabelId").html('<span>State</span>');
							} else {
								$("#wireBankStateLabelId").html('<span></span>');
							}
						}
					});
                }

        //--></script>

        <div align="center" class="approvalDialogHt">
            <table width="80%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
                <tr>
                    <td class="columndata" align="center" class="ltrow2_color">
                        <table border="0" cellspacing="0" cellpadding="3" class="ltrow2_color">
                            <tr>
                                <td>
									<%--
                                    <form action="wirebanklistselect.jsp" method="post" name="BankForm">
									--%>
									<s:form action="/pages/jsp/wires/getWireTransferBanksAction.action" method="post" name="BankForm" id="BankFormID">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                    <input type="hidden" name="wireBankFormUsed" value="true">
                                    <input type="hidden" name="Inter" value="<ffi:getProperty name='Inter'/>"/>
                                    <input type="hidden" name="validateAction"/>
                                    <input type="hidden" name="bankLookupRaturnPage" id="bankLookupRaturnPage" value="<ffi:getProperty name='bankLookupRaturnPage'/>"/>
									<input type="hidden" id="manageBeneficiary" name="manageBeneficiary" value="<ffi:getProperty name='manageBeneficiary'/>"/>
                    				<input type="hidden" name ="returnPage" id="returnPage" value="<ffi:getProperty name='returnPage'/>"/>
                                    
                                    <table border="0" cellspacing="0" cellpadding="3">
                                        <tr>
                                            <td width="247" align="right" class="sectionhead"><!--L10NStart-->Bank<!--L10NEnd-->&nbsp;</td>
                                            <td><input class="ui-widget-content ui-corner-all txtbox" type="text" name="wireTransferBank.BankName" size="39" ></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="sectionhead">City&nbsp;</td>
                                            <td><input class="ui-widget-content ui-corner-all txtbox" type="text" name="wireTransferBank.City" size="39" maxlength="32" value="<ffi:getProperty name="WireTransferBank" property="BankCity"/>"></td>
                                        </tr>
                                       
                                       
                                        <tr>
                                            <td align="right" class="sectionhead">
                                            <div id="wireBankStateLabelId">
                                            	<!--L10NStart-->State<!--L10NEnd-->&nbsp;
                                            	</div>
                                            </td>
                                            <td>
                                             <div id="wireBankStateSelectedId">
                                                <select name="wireTransferBank.State" id="getWireTransferBanksBankStateID" onChange="ns.common.adjustSelectMenuWidth('getWireTransferBanksBankStateID')">
													<option <ffi:cinclude value1="${wireTransferBank.State}" value2="">selected</ffi:cinclude> value=""><s:text name="jsp.default_376"/></option>
                                                    <ffi:list collection="StateList" items="SPDefn">
                                                        <option <ffi:cinclude value1="${wireTransferBank.BankState}" value2="${SPDefn.StateKey}">selected</ffi:cinclude> value="<ffi:getProperty name="SPDefn" property="StateKey"/>"><ffi:getProperty name="SPDefn" property="Name"/></option>
                                                    </ffi:list>
                                                </select>
                                                </div>
                                            </td>
                                        </tr>
                                       
                                        
                                        <ffi:cinclude value1="${GetStatesForCountry.IfStatesExists}" value2="false">
                                           <input type="hidden" name="wireTransferBank.State" value="">
                                        </ffi:cinclude>
                                        
                                        <tr>
                                            <td align="right" class="sectionhead"><!--L10NStart-->Country<!--L10NEnd-->&nbsp;</td>
                                            <td>
                                                <% String tempDefaultValue = ""; String tempCountry = ""; %>
                                                <ffi:getProperty name="wireTransferBank" property="Country" assignTo="tempDefaultValue"/>
                                                <% if (tempDefaultValue == null) tempDefaultValue = ""; %>
                                                <% tempDefaultValue = ( tempDefaultValue == "" ? com.ffusion.banklookup.beans.FinancialInstitution.PREFERRED_COUNTRY : tempDefaultValue ); %>
                                                <select name="wireTransferBank.Country" style="width: 162;"  onChange="updateBankStates(this),ns.common.adjustSelectMenuWidth('getWireTransferBanksBankCountryID')" id="getWireTransferBanksBankCountryID">
                                                <ffi:list collection="WireCountryList" items="country">
                                                    <ffi:getProperty name="country" property="BankLookupCountry" assignTo="tempCountry"/>
                                                    <option value="<ffi:getProperty name='tempCountry'/>" <%= ( tempDefaultValue.equals( tempCountry ) ? "selected" : "" ) %>><ffi:getProperty name="country" property="Name"/></option>
                                                </ffi:list>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="sectionhead"><!--L10NStart-->FED ABA<!--L10NEnd-->&nbsp;</td>
                                            <td><input class="ui-widget-content ui-corner-all txtbox" type="text" name="wireTransferBank.RoutingFedWire" size="39" ></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="sectionhead"><%= LABEL_SWIFT %>&nbsp;</td>
                                            <td><input class="ui-widget-content ui-corner-all txtbox" type="text" name="wireTransferBank.RoutingSwift" size="39" ></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="sectionhead"><%= LABEL_CHIPS %>&nbsp;</td>
                                            <td><input class="ui-widget-content ui-corner-all txtbox" type="text" name="wireTransferBank.RoutingChips" size="39" ></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="sectionhead"><%= LABEL_NATIONAL %>&nbsp;</td>
                                            <td><input class="ui-widget-content ui-corner-all txtbox" type="text" name="wireTransferBank.RoutingOther" size="39" ></td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="sectionhead"><%= LABEL_IBAN %>&nbsp;</td>
                                            <td><input class="ui-widget-content ui-corner-all txtbox" type="text" name="wireTransferBank.IBAN" size="39" ></td>
                                        </tr>
                                        <tr>
                                        	<td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
											<div class="ui-widget-header customDialogFooter">
											<sj:a 											
												id="searchWiresBankButtonID"
												formIds="BankFormID"
												targets="searchDestinationBankDialogID" 
												button="true" 
						                        ><s:text name="jsp.default_373" /><!-- SEARCH -->
											</sj:a>
											
											<sj:a id="closeReSerachBankListID" 
													button="true" 
													onClickTopics="closeSerachBankListOnClickTopics"
											><s:text name="jsp.default_102" /><!-- CLOSE -->
											</sj:a>
											<sj:a id="mannualEntryWireBankListID" 
													button="true" 
													onClickTopics="manualEntryOfWireBankOnClickTopics"
												><s:text name="jsp.default_MANUAL_ENTRY" /><!-- MANUAL ENTRY -->
											</sj:a>
											</div>
											</td>
                                        </tr>
                                        <!-- BEGIN: PAC5 Wires Manual Entry button -->
                                        <tr>
                                            <td align="center" colspan="2">
												
										</tr>
                                        <!-- END: Manual Entry button -->
                                    </table>
                                    &nbsp;
                                    </s:form>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>