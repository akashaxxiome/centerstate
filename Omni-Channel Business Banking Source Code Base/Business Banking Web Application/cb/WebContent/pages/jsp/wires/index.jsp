<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<%-- 
<s:include value="inc/index-pre.jsp"/>--%>
	<script type="text/javascript" src="<s:url value='/web/js/wires/wires%{#session.minVersion}.js'/>"/>
	<script type="text/javascript" src="<s:url value='/web/js/wires/wires_grid%{#session.minVersion}.js'/>"/>
	<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/wires/wires%{#session.minVersion}.css'/>"></link>

        <div id="desktop" align="center">
			
            <div id="operationresult">
                <div id="resultmessage">Operation Result</div>
            </div><!-- result -->

            <div id="appdashboardNew">
		        <s:include value="wires_dashboard.jsp" />
            </div>

			<div id="details" style="display:none;">
				<s:include value="wires_details.jsp"/>
			</div>
				
			<div id="fileupload" style="display:none;">
				<s:include value="wiretransferimportconsole.jsp"/>
			</div>
			
			<div id="summary">
				<ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
					<ffi:cinclude value1="${summaryToLoad}" value2="Template"	operator="notEquals">
						<ffi:cinclude value1="${summaryToLoad}" value2="Payee"	operator="notEquals">
							<ffi:cinclude value1="${summaryToLoad}" value2="ReleaseWires"	operator="notEquals">
								<s:include value="wires_summary.jsp"/>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude value1="${summaryToLoad}" value2="Template"	operator="equals">
						<s:action namespace="/pages/jsp/wires" name="initWireTemplatesSummaryAction_init" executeResult="true"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${summaryToLoad}" value2="Payee"	operator="equals">
						<s:action namespace="/pages/jsp/wires" name="wireBeneficiaryDashBoard_init" executeResult="true"/>
					</ffi:cinclude>
				</ffi:cinclude>
			</div>	
			
			<div id="customMappingDetails" style="display:none;">
				<s:include value="/pages/jsp/custommapping_workflow.jsp"/>
			</div>	
			<div id="test"></div>
					
			<div id="mappingDiv">
			
 	    	</div>	
			<div id="wiresReleaseDiv">
				<%-- <ffi:cinclude value1="${summaryToLoad}" value2="ReleaseWires"	operator="equals">
					<s:action namespace="/pages/jsp/transfers" name="initReleaseWireDashBoard_init" executeResult="true"/>
				</ffi:cinclude> --%>
			</div>
				
			<div id="wiresReleaseConfirmDiv">
					
			</div>
    </div>
	
	<sj:dialog id="searchDestinationBankDialogID" cssClass="pmtTran_wireDialog" title="Bank Look Results" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570"  height="460">
	</sj:dialog>
	
	<sj:dialog id="wireTypeChangeConfirmationDialogID" cssClass="pmtTran_wireDialog" title="%{getText('jsp.wire.Change_Wire_Type')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500" height="150">
	</sj:dialog>
	
	<sj:dialog id="InquiryWiresDialogID" cssClass="pmtTran_wireDialog" title="Inquiry Wire Transfer" resizable="false" modal="true" autoOpen="false" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
		
	<sj:dialog id="viewWiresDetailsDialogID" cssClass="pmtTran_wireDialog" title="Wire Transfer Detail" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800" height="500"
	
	>
	</sj:dialog>
	
	<sj:dialog id="viewRecurringWireModelDetailsDialogID" cssClass="pmtTran_wireDialog" title="Wire Transfer Detail" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800" height="500"
		buttons="{
			'Print':function() { ns.common.printPopup(); },
			'Done':function() { ns.common.closeDialog(); }}"	
		>
	</sj:dialog>
	
	<sj:dialog id="viewWiresDetailsFromBatchDialogID" cssClass="pmtTran_wireDialog" title="Wire Transfer Detail" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800" height="500"
	buttons="{
		'Print':function() { ns.common.printPopup(); },
		'Done':function() { ns.wire.closeviewWireTransferDialogInBacth(); }}"	
	>
	</sj:dialog>
	 
	<sj:dialog id="deleteWiresDialogID" cssClass="pmtTran_wireDialog" title="Delete Wire Transfer Confirm" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800" height="500" cssStyle="overflow-y:hidden;">
	</sj:dialog>
	
	<sj:dialog id="skipWiresDialogID" cssClass="pmtTran_wireDialog" title="Skip Wire Transfer Confirm" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800" height="500" cssStyle="overflow-y:hidden;">
	</sj:dialog>
	
	<%-- Div  wiresAutoEntitleConfirmDiv will be emptied after Cancel or Save operation on AutoEntitle dialog.--%>
	<sj:dialog id="autoEntitleConfirmDialogID" cssClass="pmtTran_wireDialog staticContentDialog" title="Auto-Entitle Confirmation" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="800">
	<div id="wiresAutoEntitleConfirmDiv"></div>
	</sj:dialog>
	
	<sj:dialog id="wireSaveAsTemplateDialogID" cssClass="pmtTran_wireDialog" title="Save Wire Transfer As Template" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="auto">
	</sj:dialog>
	
	<sj:dialog id="deleteWireInBatchyDialogID" cssClass="pmtTran_wireDialog" title="Delete Wire Transfer" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="810" height="500"  cssStyle="overflow-y:hidden;">
	</sj:dialog>	
	
	<sj:dialog id="viewWireTempateDialogID" cssClass="pmtTran_wireDialog" title="View Wire Template" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="810" height="500" buttons="{ 
				'Done':function() { ns.common.closeDialog(); } }"	>
	</sj:dialog>
	
	<sj:dialog id="deleteWireTemplateDialogID" cssClass="pmtTran_wireDialog" title="Delete Wire Template" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="810" height="500">
	</sj:dialog>

	<sj:dialog id="wireBatchEditAddTemplateDialogID" cssClass="pmtTran_wireDialog" title="Edit Wire Transfer Batch" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="810" height="auto">
	</sj:dialog>
	
	<sj:dialog id="viewWireBeneficiariesDialogID" cssClass="pmtTran_wireDialog" title="View Wire Beneficiary" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="810" height="auto" cssStyle="overflow:hidden;">
	</sj:dialog>
	
    <sj:dialog id="importFED_CTPAddendaDialogID" cssClass="pmtTran_wireDialog" title="Import Addenda" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
    </sj:dialog>

	<sj:dialog id="discardWireBeneficiaryDialogID" cssClass="pmtTran_wireDialog" title="Discard Pending Beneficiary" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500" height="150" position="center">
	</sj:dialog>
	
	<sj:dialog id="modifyWireBeneficiaryDialogID" cssClass="pmtTran_wireDialog" title="Modify Pending Beneficiary" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="350" height="150" >
	</sj:dialog>
	
	<sj:dialog id="deleteWireBeneficiariesDialogID" cssClass="pmtTran_wireDialog" title="Delete Wire Beneficiary" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="810" height="500">
	</sj:dialog>
	
	<sj:dialog id="deleteWireBeneficiariesDADialogID" cssClass="pmtTran_wireDialog" title="Delete Wire Beneficiaries" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="810"  height="auto" cssStyle="overflow:hidden;">
	</sj:dialog>
	<s:set name="CancelButtonLabel" value="%{getText('jsp.default.Cancel')}"/>
	<s:set name="PrintButtonLabel" value="%{getText('jsp.default.Print')}"/>
	<sj:dialog id="printerReadyWireDialog" autoOpen="false" modal="true" cssClass="pmtTran_wireDialog" title="Print" height="600" width="700" overlayColor="#000" overlayOpacity="0.7"
			   buttons="{'%{CancelButtonLabel}':function() { ns.common.closeDialog(); }, '%{PrintButtonLabel}': function(){ ns.wire.printWire();}}">
   	</sj:dialog>
   	
	<script> ns.wire.callOnCloseTopic = true; </script>
	<sj:dialog id="changeBeneScopeDialog" autoOpen="false" modal="true" cssClass="pmtTran_wireDialog" title="Change Beneficiary Scope" onCloseTopics="onCloseBeneficiaryScopeTopics" height="auto" width="400" overlayColor="#000" overlayOpacity="0.7" 
					   buttons="{'%{CancelButtonLabel}':function() {ns.wire.isChangeBeneScope = false;
					   				  ns.wire.callOnCloseTopic = false;
					                                  ns.wire.showScopeWarning = true; 
					                                  ns.wire.changeBeneScope(ns.wire.origSelect);
									  ns.common.closeDialog();}, 
						     '%{getText('jsp.default_303')}': function(){ ns.wire.isChangeBeneScope = true;    
								   ns.wire.showScopeWarning = false;
								   ns.wire.callOnCloseTopic = false;
								   ns.wire.changeBeneScope(ns.wire.currentSelect);
								   ns.common.closeDialog();}}">
   	</sj:dialog>

<script type="text/javascript">
$(document).ready(function(){
	ns.wire.showWireDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
});	


</script>   	