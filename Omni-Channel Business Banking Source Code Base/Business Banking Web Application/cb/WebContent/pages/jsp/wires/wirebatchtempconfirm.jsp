<%--
This is the wire batch confirmation page.  This is for non-international wires
only.  International wires use wirebatchinttempconfirm.jsp

Pages that request this page
----------------------------
wirebatchtemp.jsp
	SUBMIT WIRE BATCH button
wirebatchedittemplate.jsp
	SUBMIT WIRE BATCH button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
BACK requests one of the following
	wirebatchtemp.jsp
	wirebatchedittemplate.jsp
CONFIRM/SEND BATCH requests wirebatchsend.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<ffi:help id="payments_wirebatchtempconfirm" className="moduleHelpClass"/>

<% String itemsPerPageStr = ""; String pageStr = ""; String pagesStr = ""; %>
<ffi:getProperty name="wireBatchItemsPerPage" assignTo="itemsPerPageStr"/>
<ffi:getProperty name="templatePage" assignTo="pageStr"/>
<ffi:getProperty name="templatePages" assignTo="pagesStr"/>
<div align="center">
<span id="wireBatchResultMessage" style="display:none">Your wire batch has been saved.</span>
<div class="marginTop20">
 <% String batchDest, batchDestDisplay = ""; %>
    <%-- <ffi:getProperty name="${WireBatchObject}" property="BatchDestination" assignTo="batchDest"/>
    <ffi:setProperty name="WireDestinations" property="Key" value="<%= batchDest %>"/>
    --%>
      <ffi:getProperty name="batchType"  assignTo="batchDestDisplay"/>

    <%-- Show alert if date was changed --%>
    
    <s:if test="%{isDueDateChanged && nonBusinessDay}">
        <%-- Both proc window is closed and tomorrow is not a biz day --%>
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
					<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchtempconfirm.jsp-1" parm0="<%= batchDestDisplay %>" />
					<br><br>
        </div>
    </s:if> <s:elseif test="%{isDueDateChanged}">
        <%-- DueDate changed because processing window is closed --%>
        <div class="sectionsubhead" style="color:red" align="center">
					<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchtempconfirm.jsp-2" parm0="<%= batchDestDisplay %>" />
        </div>
    </s:elseif>
    <s:elseif test="%{nonBusinessDay}">
        <%-- DueDate changed because its not a business day --%>
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
            NOTE: The Requested Value Date is not a business day.
            The Expected Value Date has been changed to the next business day.<br><br>
        </div>
    </s:elseif>
    
    <%-- Duplicate Wire warning --%>
   <s:if test="%{duplicateWire}">
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
					<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchtempconfirm.jsp-3" parm0="<%= batchDestDisplay %>" />
					<br><br>
        </div>
  </s:if>
</div>
<div class="leftPaneWrapper" role="form" aria-labelledby="wireSummaryHeader"><div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="wireSummaryHeader">Wire Summary</h2></div>
	
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="">Batch Name:</span>
				<span class="inlineSection floatleft labelValue" id=""><ffi:getProperty name="wireBatchObject" property="BatchName"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="">Batch Type:</span>
					<span class="inlineSection floatleft labelValue"  id=""><ffi:getProperty name="wireBatchObject" property="BatchName"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="">Application Type:</span>
					<span class="inlineSection floatleft labelValue"  id=""><ffi:getProperty name="wireBatchObject" property="BatchType"/></span>
			</div>
	</div>
</div></div>
<div class="confirmPageDetails label130">
<div class="blockWrapper" role="form" aria-labelledby="batchInfoHeader">
	<div  class="blockHead"><h2 id="batchInfoHeader">Batch Information</h2></div>
	<div class="blockContent">
		<%-- <div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">Batch Name:</span>
                <span class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">Batch Type:</span>
                <span class="columndata"> <ffi:getProperty name="batchType"/></span>
			</div>
		</div> --%>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<%-- <span class="sectionsubhead sectionLabel">Application Type:</span>
                <span class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchType"/></span> --%>
                <span class="sectionsubhead sectionLabel">Requested Value Date:</span>
                <span class=""><ffi:getProperty name="wireBatchObject" property="DueDate"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">Expected Value Date:</span>
           	 <span class=""><ffi:getProperty name="wireBatchObject" property="DateToPost"/></span>
			</div>
		</div>
	</div>
</div>

<div class="blockWrapper marginTop20" role="form" aria-labelledby="includedBatchHeader">
	<div  class="blockHead"><h2 id="includedBatchHeader">Included Batches</h2></div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
                   <tr class="header">
                       <td align="center" class="sectionsubhead" colspan="2"> <s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DRAWDOWN}">Drawdown Account</s:if> <s:else>Beneficiary</s:else></td>
                       <td align="left" class="sectionsubhead">Amount</td>
                   </tr>
		<ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="ACTION!!del,Status!!3,AND" />
                    <s:iterator value="%{wireBatchObject.Wires}" var="wire" > 
                   <tr>
                       <td align="center" class="columndata" colspan="2" nowrap><s:property  value="WirePayee.PayeeName"/>
                           	<s:if test="%{WirePayee.NickName != null && WirePayee.NickName != ''}"> (<s:property  value="WirePayee.NickName"/>)</s:if> 
                       </td>
                       <td align="left" class="columndata" nowrap>
                       
                       <s:if test="%{!isModifyAction}">
                           <ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/>
                       </s:if>
                       <s:else>
                          <s:if test="%{Action ==  @com.ffusion.beans.wiretransfers.WireDefines@WIRE_ACTION_ADD}">
                               <ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/>
                           </s:if>
                           <s:else>
                               <ffi:getProperty name="wire" property="OrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
                           </s:else>
                       </s:else>
                       </td>
                   </tr>
                 </s:iterator>

	<ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />

                   <tr>
                       <td align="center" class="columndata" width="60%">&nbsp;</td>
                       <td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                       <td align="left" class="tbrd_t sectionsubhead" width="20%" valign="bottom" nowrap>
                            <s:if test="%{!isModifyAction}">
                               <ffi:getProperty name="wireBatchObject" property="Amount"/>
                           </s:if>
                           <s:else>
                                <ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>
                           </s:else>
                       </td>
                   </tr>
               </table>
	</div>
</div>
</div>
<div class="btn-row">
<s:form id="sendWireBacthFormID" method="post" name="FormName" namespace="/pages/jsp/wires" action="%{#session.wirebatch_confirm_form_url}" theme="simple">
               	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
               <sj:a id="cancelFormButtonOnBatchVerify"
	button="true" summaryDivId="summary" buttonType="cancel"
	onClickTopics="showSummary,cancelWireBatchForm"
><s:text name="jsp.default_82" />
</sj:a>
<sj:a id="backFormButton" 
	  button="true" 
	  onClickTopics="backToInput"
><s:text name="jsp.default_57" />
</sj:a>
                <sj:a id="sendWireBacthSubmit"
	formIds="sendWireBacthFormID" 
	targets="confirmDiv" 
	button="true" 
	onBeforeTopics="sendWireBacthFormBeforeTopics"
	onSuccessTopics="sendWireBacthFormSuccessTopic"
	onErrorTopics="errorSendWireBatchForm"
	onCompleteTopics="sendWireBacthFormCompleteTopic"><s:text name="jsp.default_378" /></sj:a>
</s:form>
</div>
</div></div>
<!--<table width="100%" cellpadding="0" cellspacing="0" border="0">
				<tr>
				<td class="columndata ltrow2_color" align="center">
					 <table width="715 border="0" cellspacing="0" cellpadding="3">
   
                        <tr>
                            <td align="left" class="tbrd_b sectionhead">Batch Information</td>
                        </tr>
                    </table> -->
                    <%-- <br>
                    <table width="715" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="357" valign="top">
                                <table width="355" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" width="115" class="sectionsubhead">Batch Name</td>
                                        <td align="left" width="228" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchName"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Batch Type</td>
                                        <td align="left" class="columndata"> <ffi:getProperty name="batchType"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Application Type</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchType"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                            <td width="348" valign="top">
                                <table width="347" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" width="135" class="sectionsubhead">Requested Value Date</td>
                                        <td align="left" width="200" class="columndata"><ffi:getProperty name="wireBatchObject" property="DueDate"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Expected Value Date</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="DateToPost"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br> 	
                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="sectionhead">Included Batches</td>
                        </tr>
                    </table>--%>
                    <%-- <table width="375" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="sectionsubhead" colspan="2"> <s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DRAWDOWN}">Drawdown Account</s:if> <s:else>Beneficiary</s:else></td>
                            <td align="left" class="sectionsubhead">Amount</td>
                        </tr>

						<ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="ACTION!!del,Status!!3,AND" />

                         <s:iterator value="%{wireBatchObject.Wires}" var="wire" > 
                        <tr>
                            <td align="left" class="columndata" colspan="2" nowrap><s:property  value="WirePayee.PayeeName"/>
                                	<s:if test="%{WirePayee.NickName != ''}"> (<s:property  value="WirePayee.NickName"/>)</s:if> 
                            </td>
                            <td align="left" class="columndata" nowrap>
                            
                            <s:if test="%{!isModifyAction}">
                                <ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/>
                            </s:if>
                            <s:else>
                               <s:if test="%{Action ==  @com.ffusion.beans.wiretransfers.WireDefines@WIRE_ACTION_ADD}">
                                    <ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/>
                                </s:if>
                                <s:else>
                                    <ffi:getProperty name="wire" property="OrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
                                </s:else>
                            </s:else>
                            </td>
                        </tr>
                      </s:iterator>

						<ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />

                        <tr>
                            <td align="left" class="columndata" width="98%">&nbsp;</td>
                            <td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                            <td align="left" class="tbrd_t sectionsubhead" width="1%" valign="bottom" nowrap>
                                 <s:if test="%{!isModifyAction}">
                                    <ffi:getProperty name="wireBatchObject" property="Amount"/>
                                </s:if>
                                <s:else>
                                     <ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>
                                </s:else>
                            </td>
                        </tr>
                    </table> 
                    </td>
                </tr>
            </table>--%>


