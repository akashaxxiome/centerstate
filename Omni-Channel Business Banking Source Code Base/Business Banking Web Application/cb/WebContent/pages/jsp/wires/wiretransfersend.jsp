<%--
This is a common final send/save page for all wire types, except host.  Host
wires use wirehostsend.jsp.

For single wires and templates, it displays the wire and its reference numbers.
If the wire is being added/edited to a batch, it auto-refreshes to the batch.

Pages that request this page
----------------------------
wiretransferconfirm.jsp
	CONFIRM/SUBMIT WIRE / CONFIRM/SAVE TEMPLATE button
autoentitle-confirm.jsp
	SAVE button

Pages this page requests
------------------------
DONE requests one of the following:
	wiretransfers.jsp
	wiretemplates.jsp
Javascript auto-refreshes to one of the following: (when working in a batch)
	wirebatchff.jsp
	wirebatcheditff.jsp
	wirebatchtemp.jsp
	wirebatchedittemplate.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
common/wire_labels.jsp
    The labels for fields and buttons
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.wiretransfers.*" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page import="java.lang.Integer" %>
<%@ include file="../common/wire_labels.jsp"%>

<div id="wirePayeeStatusOnSendDivId" class="sectionsubhead" style="color:red; text-align:center;"></div>

<script>
    $(document).ready(function() {
		ns.wire.payeeStatus = '<ffi:getProperty name="wirePayeeStatusMsg"/>';
		if(ns.wire.payeeStatus!=null&&ns.wire.payeeStatus!=""){
			$('#wirePayeeStatusOnSendDivId').html(js_wire_duplicatedPayee);
			ns.wire.payeeStatus = null;
		}
     });
</script>

<%@page import="com.ffusion.csil.core.common.PaymentEntitlementsDefines"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="payments_wiretransfersend" className="moduleHelpClass"/>

<%--=================== PAGE SECURITY CODE - Part 1 Start ====================--%>
<%	session.setAttribute( "Credential_Op_Type", ""+ com.ffusion.beans.authentication.AuthConsts.OPERATION_TYPE_TRANSACTION) ; %>
<ffi:setProperty name="LoadURL" value="payments/wiretransfersend.jsp"/>
<ffi:setProperty name="securityMessageKey" value="jsp/payments/security-check.jsp-6"/>
<ffi:setProperty name="securitySubMenu" value="${subMenuSelected}"/>
<ffi:setProperty name='securityPageHeading' value='${PageHeading}'/>
 


<% if( !( session.getAttribute( "UserSecurityKey" ) instanceof com.ffusion.tasks.util.ImmutableString) )  { %>
	<script>
		//window.location = "<ffi:getProperty name="SecurePath"/>payments/security-check-wait.jsp";
	</script>
<%} %>
<ffi:cinclude value1="${UserSecurityKey.ImmutableString}" value2="${verified}" operator="notEquals">
	<script>
		//window.location = "<ffi:getProperty name="SecurePath"/>payments/security-check-wait.jsp";
	</script>
</ffi:cinclude>
<% if( session.getAttribute( "UserSecurityKey" ) instanceof com.ffusion.tasks.util.ImmutableString )  { %>

<%--=================== PAGE SECURITY CODE - Part 1 End ======================--%>

<ffi:removeProperty name="verified"/>

<%
String task = "wireTransfer";
String templateTask = "";
com.ffusion.beans.wiretransfers.WireTransfer wire = (com.ffusion.beans.wiretransfers.WireTransfer)session.getAttribute(task);

String intFieldTask = task;
String wireType = "";
String debCurr = (String)session.getAttribute("DebitCurrencyType");
String status;
status = new java.lang.Integer(wire.getStatus()).toString();
session.setAttribute("wireStatus", status);

if (wire.getWireDestination().equals(WireDefines.WIRE_DOMESTIC)) wireType = DOMESTIC;
if (wire.getWireDestination().equals(WireDefines.WIRE_INTERNATIONAL)) wireType = INTERNATIONAL;
if (wire.getWireDestination().equals(WireDefines.WIRE_BOOK)) wireType = BOOK;
if (wire.getWireDestination().equals(WireDefines.WIRE_DRAWDOWN)) wireType = DRAWDOWN;
if (wire.getWireDestination().equals(WireDefines.WIRE_FED)) wireType = FED;

%>
<!-- this parameter is used to pass type for save template as and when applicable -->
<input type="hidden" id="amtCurrencyType" value="<ffi:getProperty name="<%= task %>" property="AmtCurrencyType"/>">
<div id="resultMessageDiv" class="hidden">
</div>
<ffi:getProperty name="TemplateTask" assignTo="templateTask"/>
<%
boolean wireIsTemplate = (!templateTask.equals(""));
%>
<ffi:setProperty name="<%= task %>" property="DateFormat" value="${UserLocale.DateFormat}"/>
<ffi:setProperty name="<%= intFieldTask %>" property="${wireCurrencyField}" value="<%= debCurr %>"/>

<span id='wireTypeTextID' style='display:none'><%= wireType %> Wire Transfer Print</span>

<ffi:setProperty name="PageText" value=""/>
<ffi:setProperty name="wireConfirmBackURL" value="${SecurePath}payments/wiretransferconfirm.jsp" URLEncrypt="true"/>
<div id="wiresTransferConfirmationForID">
<ffi:setProperty name="wireType" value="<%= wireType %>" /> 
<div class="leftPaneWrapper">
<div class="leftPaneInnerWrapper">
	<div class="header">Wire Summary</div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyWireBeneficiaryLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME %>: </span>
				<span class="inlineSection floatleft labelValue" id="verifyWireBeneficiaryValue"><ffi:getProperty name="WireTransferPayee" property="Name"/></span>
			</div>
			<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<% if (!wireType.equals(DRAWDOWN)) { %>
					<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyWireAccountLabel"><%= LABEL_FROM_ACCOUNT %>: </span>
					<span class="inlineSection floatleft labelValue"  id="verifyWireAccountValue"><ffi:getProperty name="<%= task %>" property="FromAccountNumberDisplayText"/></span>
				<% } %>
				<% if (wireType.equals(DRAWDOWN))  { %>
					<span class="sectionsubhead sectionLabel floatleft inlineSection"><%= LABEL_CREDIT_ACCOUNT %>: </span>
					<span class="inlineSection floatleft labelValue"><ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/></span>
				<% } %>
			</div>
	</div>
</div>
<div class="leftPaneInnerWrapper" id="saveAsTemplateHolder"  role="form" aria-labelledby="saveTemplateHeader">
	<div class="header"><h2 id="saveTemplateHeader"><s:text name="jsp.default_371" /></h2></div>
	<div class="leftPaneInnerBox">
	   <div id="templateSaveAsID">
	   		<%-- <s:include value="%{#session.PagesPath}/wires/wiresaveastemplate.jsp"/> --%>
       </div>
    </div>
  </div>
</div>
<div class="confirmPageDetails">
<%-- <div class="transferErrorMessges">
<jsp:include page="wiretransfersendpart.jsp" />
</div> --%>
<div class="confirmationDetails" style="width:50%; float:left">
	<span><%= LABEL_TRACKING_ID %>: </span>
	<span><ffi:getProperty name="<%= task %>" property="TrackingID"/></span>
</div>
<div class="confirmationDetails" style="width:49%; float:left">
	<span><%= LABEL_APPLICATION_ID %>: </span>
	<span><ffi:getProperty name="<%= task %>" property="ID"/></span>
</div>
<div class="clear" style="clear:both"></div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
	<div class="blockContent">
		<div class="blockRow" id="statusBlock">
			<span id="confirmWireStatusLabel" class="sectionsubhead sectionLabel">Status:</span>
			<span id="confirmWireStatusValue" class="columndata" style="width:85%">
				<span class="sapUiIconCls" id="wireTransferStatusIcon"></span>
				<span><ffi:getProperty name="<%= task %>" property="StatusName"/></span>
				<span class="transferErrorMessges" style="float:right">
					 <jsp:include page="wiretransfersendpart.jsp" />
				</span>
			</span>
		</div>
	</div>
</div>
<% if (wireIsTemplate) { %>
<%-- ------ TEMPLATE (READ-ONLY) NAME, NICKNAME, ID, AND WIRE LIMIT ------ --%>
<div class="blockWrapper"  role="form" aria-labelledby="saveTemplateHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="saveTemplateHeader">Template Info</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" id="confirmWireTemplateNameLabel"><%= LABEL_TEMPLATE_NAME %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" id="confirmWireTemplateCategoryLabel"><%= LABEL_TEMPLATE_CATEGORY %>: </span>
				<span class="columndata" id="confirmWireTemplateCategory"><ffi:getProperty name="<%= task %>" property="WireCategory"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" id="confirmWireTemplateNickNameLabel"><%= LABEL_TEMPLATE_NICKNAME %>: </span>
				<span class="columndata" id="confirmWireTemplateNickName"><ffi:getProperty name="<%= task %>" property="NickName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" id="confirmWireTemplateScopeLabel"><%= LABEL_TEMPLATE_SCOPE %>: </span>
				<span class="columndata" id="confirmWireTemplateScope"><ffi:getProperty name="<%= task %>" property="WireScope"/></span>
			</div>
		</div>
	</div>
</div>	
<% } %>	

<% String wireSource = ""; %>
<ffi:getProperty name="<%= task %>" property="WireSource" assignTo="wireSource"/>
<% if (wireSource == null) wireSource = "";
   if (wireSource.equals("TEMPLATE") && !wireIsTemplate) { %>
<%-- ------ TEMPLATE (READ-ONLY) NAME, NICKNAME, ID, AND WIRE LIMIT ------ --%>
<div class="blockWrapper" role="form" aria-labelledby="templateInfoHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="templateInfoHeader">Template Info</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireTemplateNameLabel" class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_NAME %>: </span>
				<span id="confirmWireTemplateNameValue"class="columndata"><ffi:getProperty name="<%= task %>" property="WireName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireTemplateNickNameLabel" class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_NICKNAME %>: </span>
				<span id="confirmWireTemplateNickNameValue" class="columndata"><ffi:getProperty name="<%= task %>" property="NickName"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireTemplateIDLabel" class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_ID %>: </span>
				<span id="confirmWireTemplateId" class="columndata"><ffi:getProperty name="<%= task %>" property="TemplateID"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireTemplateLimitLabel" class="sectionsubhead sectionLabel"><%= LABEL_TEMPLATE_LIMIT %>: </span>
				<span id="confirmWireTemplateLimit" class="columndata"><ffi:getProperty name="<%= task %>" property="DisplayWireLimit"/></span>
			</div>
		</div>
		
	</div>
</div>
<% } %>
<% if (wireType.equals(DRAWDOWN))  { %>
<div  class="blockHead toggleClick"><ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/> <span class="sapUiIconCls icon-positive"></span></div>
<div class="toggleBlock hidden">
<div class="blockWrapper" role="form" aria-labelledby="beneficiaryInfoHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="beneficiaryInfoHeader">Beneficiary Info</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_BENEFICIARY_NAME %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.PayeeName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_1 %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_2 %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_CITY %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_STATE %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.State"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_ZIP_CODE %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.ZipCode"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_CONTACT_PERSON %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Contact"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_CREDIT_ACCOUNT %>: </span>
                <span class="columndata"><ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/></span>
			</div>
		</div>
	</div>
</div>
<div class="blockWrapper" role="form" aria-labelledby="beneficiaryBankInfoHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="beneficiaryBankInfoHeader">Beneficiary Bank Info</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_NAME %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.BankName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_ADDRESS_1 %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.Street"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_ADDRESS_2 %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_BANK_CITY %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.City"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead"><%= LABEL_BANK_STATE %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.State"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead"><%= LABEL_BANK_ZIP_CODE %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.ZipCode"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead"><%= LABEL_FED_ABA %>: </span>
			<span class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.RoutingFedWire"/></span>
		</div>
	</div>
</div></div>
<%-- DEBIT ACCOUNT INFO --%>
<!-- <div  class="blockHead">L10NStartDebit Bank InformationL10NEnd</div> -->
<% } %>	
<div  class="blockHead toggleClick"><ffi:getProperty name="WireTransferPayee" property="Name"/> (<s:if test='%{WireTransferPayee.NickName!=""}'><ffi:getProperty name="WireTransferPayee" property="NickName"/> - </s:if><ffi:getProperty name="WireTransferPayee" property="AccountNum"/>) <span class="sapUiIconCls icon-positive"></span></div>
<div class="toggleBlock hidden">
<div class="blockWrapper" role="form" aria-labelledby="beneficiaryBankInfoHeader">
	<div  class="blockHead" id="confirmWireBeneficiaryInfoLabel"><h2 id="beneficiaryBankInfoHeader"><%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Info<!--L10NEnd-->" %> </h2></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryAccountNumberLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NUMBER : LABEL_ACCOUNT_NUMBER %>: </span>
				<span id="confirmWireBeneficiaryAccountNumberValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountNum"/></span>
			</div>
			<div class="inlineBlock">
			<span id="confirmWireBeneficiaryAccountTypeLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_TYPE : LABEL_ACCOUNT_TYPE %>: </span>
			<span id="confirmWireBeneficiaryAccountTypeValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountTypeDisplayName"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
			<span id="confirmWireBeneficiaryNameLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME %>: </span>
			<span id="confirmWireBeneficiaryNameValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Name"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBeneficiaryNickNameLabel" class="sectionsubhead sectionLabel"><%= LABEL_NICKNAME %>: </span>
				<span id="confirmWireBeneficiaryNickNameValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="NickName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryAddress1Label" class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_1 %>: </span>
				<span id="confirmWireBeneficiaryAddress1Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBeneficiaryAddress2Label" class="sectionsubhead sectionLabel"><%= LABEL_ADDRESS_2 %>: </span>
				<span id="confirmWireBeneficiaryAddress2Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street2"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryCityLabel" class="sectionsubhead sectionLabel"><%= LABEL_CITY %>: </span>
				<span id="confirmWireBeneficiaryCityValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="City"/></span>
			</div>
			<ffi:cinclude value1="${StatesExistsForPayeeCountry}" value2="true">
				<div class="inlineBlock">
					<span id="confirmWireBeneficiaryStateLabel" class="sectionsubhead sectionLabel"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_STATE_PROVINCE : LABEL_STATE %>: </span>
					<span id="confirmWireBeneficiaryStateValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="StateDisplayName"/></span>
				</div>
			</ffi:cinclude>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryZipCodeLabel" class="sectionsubhead sectionLabel"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE %>: </span>
				<span id="confirmWireBeneficiaryZipCodeValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="ZipCode"/></span>
			</div>
			<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
				<div class="inlineBlock">
					<span id="confirmWireBeneficiaryCountryLabel" class="sectionsubhead sectionLabel"><%= LABEL_COUNTRY %>: </span>
					<span id="confirmWireBeneficiaryCountryValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="CountryDisplayName"/></span>
				</div>
			<% } %>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryContactPersonLabel"  class="sectionsubhead sectionLabel"><%= LABEL_CONTACT_PERSON %>: </span>
				<span id="confirmWireBeneficiaryContactPersonValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Contact"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBeneficiaryScopeLabel"  class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_SCOPE : LABEL_BENEFICIARY_SCOPE %>: </span>
				<span id="confirmWireBeneficiaryScopeValue" class="columndata"><ffi:setProperty name="WirePayeeScopes" property="Key" value="${WireTransferPayee.PayeeScope}"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></span>
			</div>
		</div>
	</div>
</div>
<div class="blockWrapper"  role="form" aria-labelledby="confirmWireBeneficiaryBankInforLabel">
	<div  class="blockHead" id="confirmWireBeneficiaryBankInfoLabel"><h2 id="confirmWireBeneficiaryBankInforLabel"><%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Bank Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Bank Info<!--L10NEnd-->" %></h2> </div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryBankNameLabel" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME %>: </span>
				<span id="confirmWireBeneficiaryBankNameValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBeneficiaryBankAddress1Label" class="sectionsubhead sectionLabel"><%= LABEL_BANK_ADDRESS_1 %>: </span>
				<span id="confirmWireBeneficiaryBankAddress1Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryBankAddress2Label"  class="sectionsubhead sectionLabel"><%= LABEL_BANK_ADDRESS_2 %>: </span>
				<span id="confirmWireBeneficiaryBankAddress2Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/></span>
			</div>
			<div class="inlineBlock">
			<span id="confirmWireBeneficiaryBankCityLabel" class="sectionsubhead sectionLabel"><%= LABEL_BANK_CITY %>: </span>
				<span id="confirmWireBeneficiaryBankCityValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/></span>
			</div>
		</div>
		<ffi:cinclude value1="${StatesExistsForDestinationBankCountry}" value2="true">
			<div class="blockRow">
				<span id="confirmWireBeneficiaryBankStateLabel" class="sectionsubhead sectionLabel"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_BANK_STATE_PROVINCE : LABEL_BANK_STATE %>: </span>
				<span id="confirmWireBeneficiaryBankStateValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.StateDisplayName"/></span>
			</div>
		</ffi:cinclude>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryBankZipCodeLabel" class="sectionsubhead sectionLabel"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_BANK_ZIP_POSTAL_CODE : LABEL_BANK_ZIP_CODE %>: </span>
				<span id="confirmWireBeneficiaryBankZipCodeValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/></span>
			</div>
			<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
			<div class="inlineBlock">
				<span id="confirmWireBeneficiaryBankCountryLabel" class="sectionsubhead sectionLabel"><%= LABEL_BANK_COUNTRY %>: </span>			
				<span id="confirmWireBeneficiaryBankCountryValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CountryDisplayName"/></span>
			</div>
			<% } %>
		</div>
		
		<div class="blockRow">
				<span id="confirmWireBeneficiaryBankFEDABALabel" class="sectionsubhead sectionLabel"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_ABA : LABEL_FED_ABA %>: </span>
				<span id="confirmWireBeneficiaryBankFEDABAValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/></span>
		</div>
		<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryBankSwiftLabel" class="sectionsubhead sectionLabel"><%= LABEL_SWIFT %>: </span>
				<span id="confirmWireBeneficiaryBankSwiftValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingSwift"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBeneficiaryBankChipsLabel" class="sectionsubhead sectionLabel"><%= LABEL_CHIPS %>: </span>
				<span id="confirmWireBeneficiaryBankChipsValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingChips"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBeneficiaryBankNationalLabel" class="sectionsubhead sectionLabel"><%= LABEL_NATIONAL %>: </span>
				<span id="confirmWireBeneficiaryBankNationalValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingOther"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBeneficiaryBankIBANLabel" class="sectionsubhead sectionLabel"><%= LABEL_IBAN %>: </span>
				<span id="confirmWireBeneficiaryBankIBANValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></span>
			</div>
		</div>
		<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
			<div class="blockRow">
				<span class="sectionsubhead sectionLabel"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
				<span class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></span>
			</div>
		</ffi:cinclude>
		<% } %>
		<% if (wireType.equals(FED)) { %>
		<div class="blockRow">
			<span id="confirmWireBeneficiaryBankIBANFEDLabel" class="sectionsubhead sectionLabel"><%= LABEL_IBAN %>: </span>
			<span id="confirmWireBeneficiaryBankIBANFEDValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></span>
		</div>
		<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
			<span> class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></span>
		</div>
		</ffi:cinclude>
		<% } %>
	</div>
</div>
<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL) || wireType.equals(FED)) { %>
<ffi:setProperty name="WireTransferPayee" property="IntermediaryBanks.Filter" value="ACTION!!del"/>
<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks}" value2="" operator="notEquals" >
<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
<div class="blockWrapper" role="form" aria-labelledby="intermediaryBanksHeader">
<% int counter = 0; %>
	<ffi:list collection="WireTransferPayee.IntermediaryBanks" items="Bank1">
	<div  class="blockHead"><!--L10NStart--><h2 id="intermediaryBanksHeader">Intermediary Banks</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="BankName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
			</div>
		</div>
		<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_SWIFT %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_CHIPS %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_NATIONAL %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
			<span class="columndata">
				<% if(counter == 0) { %>
				<% } else { %>
					<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
				<% } %>
			</span>
		</div>
		<% } else if(wireType.equals(FED)) { %> 
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
					<span class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
					<span class="columndata">
					<% if(counter == 0) { %>
					<% } else { %>
						<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
					<% }  %></span>
				</div>
			</div>
		<% } else { %>
		<% } %>
</div>
<%  counter++; %>
</ffi:list>
</div>
</ffi:cinclude>
</ffi:cinclude>
<% } %>
</div>
	<%-- <% int counter = 0; %>
	<ffi:list collection="WireTransferPayee.IntermediaryBanks" items="Bank1">
	<tr>
		<td class="columndata">&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</td>
		<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
		<td class="columndata">
			<% if(counter == 0) { %>
			&nbsp;
			<% } else { %>
			<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
			<% } %>&nbsp;</td>
		<% } %>
		<% if (wireType.equals(FED)) { %>
		<td class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
		<td class="columndata">
			<% if(counter == 0) { %>
			&nbsp;
			<% } else { %>
			<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
			<% }  %>&nbsp;</td>
		<% } counter++; %>
	</tr>
	</ffi:list>
</table> --%>

	
			<%--<table width="750" border="0" cellspacing="0" cellpadding="0" style="padding-top: 20px; padding-bottom: 20px;">
				<tr>
					<td class="ltrow2_color">
						<div align="center">
						 <table border="0" cellspacing="0" cellpadding="3" width="715">
							<tr style="display:none;">
								<td colspan="4" class="sectionsubhead" align="center">

								<jsp:include page="wiretransfersendpart.jsp" />

								</td>
							</tr>
							<tr>
								<td width="135" class="sectionsubhead"><%= LABEL_TRACKING_ID %></td>
								<td width="221" class="columndata"><ffi:getProperty name="<%= task %>" property="TrackingID"/></td>
								<td width="115" class="sectionsubhead"><%= LABEL_APPLICATION_ID %></td>
								<td width="220" class="columndata"><ffi:getProperty name="<%= task %>" property="ID"/></td>
							</tr>
							<tr>
								<td id="confirmWireStatusLabel" width="135" class="sectionsubhead">Status</td>
								<td id="confirmWireStatusValue" width="221" class="columndata" colspan="3"><ffi:getProperty name="<%= task %>" property="StatusName"/></td>
							</tr>
						</table> --%>
					<%-- <% if (wireIsTemplate) { %>
							<table border="0" cellspacing="0" cellpadding="3" width="715">
								<tr>
									<td class="tbrd_b sectionhead" colspan="4">&gt; <!--L10NStart-->Template Info<!--L10NEnd--></td>
								</tr>
								<tr>
									<td id="confirmWireTemplateNameLabel" width="135" class="sectionsubhead"><%= LABEL_TEMPLATE_NAME %></td>
									<td id="confirmWireTemplateName" width="221" class="columndata"><ffi:getProperty name="<%= task %>" property="WireName"/></td>
									<td id="confirmWireTemplateCategoryLabel" width="115" class="sectionsubhead"><%= LABEL_TEMPLATE_CATEGORY %></td>
									<td id="confirmWireTemplateCategory" width="220" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCategory"/></td>
								</tr>
								<tr>
									<td id="confirmWireTemplateNickNameLabel" class="sectionsubhead"><%= LABEL_TEMPLATE_NICKNAME %></td>
									<td id="confirmWireTemplateNickName" class="columndata"><ffi:getProperty name="<%= task %>" property="NickName"/></td>
									<td id="confirmWireTemplateScopeLabel" class="sectionsubhead"><%= LABEL_TEMPLATE_SCOPE %></td>
									<td id="confirmWireTemplateScope" class="columndata"><ffi:getProperty name="<%= task %>" property="WireScope"/></td>
								</tr>
							</table>
							<br>
<% } %> --%>

<!-- <table width="715" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="tbrd_b sectionhead" colspan="3">&gt; L10NStartSend Wire ToL10NEnd</td>
	</tr>
	<tr>
		<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr> -->
	<%-- <% if (wireType.equals(DRAWDOWN))  { %>
	<tr>
		<td width="357" valign="top">
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td class="sectionhead" colspan="2">&gt; <!--L10NStart-->Beneficiary Info<!--L10NEnd--></td>
				</tr>
				<tr>
					<td width="135" class="sectionsubhead"><%= LABEL_BENEFICIARY_NAME %></td>
					<td width="208" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.PayeeName"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_ADDRESS_1 %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Street"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_ADDRESS_2 %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Street2"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_CITY %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.City"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_STATE %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.State"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_ZIP_CODE %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.ZipCode"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_CONTACT_PERSON %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.Contact"/></td>
				</tr>
                <tr>
                    <td class="sectionsubhead"><%= LABEL_CREDIT_ACCOUNT %></td>
                    <td class="columndata">
                    <ffi:getProperty name="WireTransfer" property="FromAccountNumberDisplayText"/>
					</td>
                </tr>
			</table>
		</td>
		<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td class="sectionhead" colspan="2">&gt; <!--L10NStart-->Beneficiary Bank Info<!--L10NEnd--></td>
				</tr>
				<tr>
					<td width="115" class="sectionsubhead"><%= LABEL_BANK_NAME %></td>
					<td width="220" class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.BankName"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_BANK_ADDRESS_1 %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.Street"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_BANK_ADDRESS_2 %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.Street2"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_BANK_CITY %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.City"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_BANK_STATE %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.State"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_BANK_ZIP_CODE %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.ZipCode"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= LABEL_FED_ABA %></td>
					<td class="columndata"><ffi:getProperty name="<%= task %>" property="WireCreditInfo.DestinationBank.RoutingFedWire"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
DEBIT ACCOUNT INFO
<br>
<table width="715" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="tbrd_b sectionhead" colspan="3">&gt; <!--L10NStart-->Debit Bank Information<!--L10NEnd--></td>
	</tr>
	<tr>
		<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<% } %> --%>
	<%-- <tr>
		<td width="357" valign="top">
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td id="confirmWireBeneficiaryInfoLabel" class="sectionhead" colspan="2">111&gt; <%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Info<!--L10NEnd-->" %> </td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryAccountNumberLabel" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NUMBER : LABEL_ACCOUNT_NUMBER %></td>
					<td id="confirmWireBeneficiaryAccountNumberValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountNum"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryAccountTypeLabel" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_TYPE : LABEL_ACCOUNT_TYPE %></td>
					<td id="confirmWireBeneficiaryAccountTypeValue" class="columndata">
						<ffi:getProperty name="WireTransferPayee" property="AccountTypeDisplayName"/>
					</td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryNameLabel" width="135" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME %></td>
					<td id="confirmWireBeneficiaryNameValue" width="208" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Name"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryNickNameLabel" class="sectionsubhead"><%= LABEL_NICKNAME %></td>
					<td id="confirmWireBeneficiaryNickNameValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="NickName"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryAddress1Label" class="sectionsubhead"><%= LABEL_ADDRESS_1 %></td>
					<td id="confirmWireBeneficiaryAddress1Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryAddress2Label" class="sectionsubhead"><%= LABEL_ADDRESS_2 %></td>
					<td id="confirmWireBeneficiaryAddress2Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street2"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryCityLabel" class="sectionsubhead"><%= LABEL_CITY %></td>
					<td id="confirmWireBeneficiaryCityValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="City"/></td>
				</tr>
				<ffi:cinclude value1="${StatesExistsForPayeeCountry}" value2="true">
				<tr>
					<td id="confirmWireBeneficiaryStateLabel" class="sectionsubhead"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_STATE_PROVINCE : LABEL_STATE %></td>
					<td id="confirmWireBeneficiaryStateValue" class="columndata">
						<ffi:getProperty name="WireTransferPayee" property="StateDisplayName"/>
					</td>
				</tr>
				</ffi:cinclude>
				<tr>
					<td id="confirmWireBeneficiaryZipCodeLabel" class="sectionsubhead"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE %></td>
					<td id="confirmWireBeneficiaryZipCodeValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="ZipCode"/></td>
				</tr>
				<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
				<tr>
					<td id="confirmWireBeneficiaryCountryLabel" class="sectionsubhead"><%= LABEL_COUNTRY %></td>
					<td id="confirmWireBeneficiaryCountryValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="CountryDisplayName"/></td>
				</tr>
				<% } %>
				<tr>
					<td id="confirmWireBeneficiaryContactPersonLabel"  class="sectionsubhead"><%= LABEL_CONTACT_PERSON %></td>
					<td id="confirmWireBeneficiaryContactPersonValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Contact"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryScopeLabel"  class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_SCOPE : LABEL_BENEFICIARY_SCOPE %></td>
					<td id="confirmWireBeneficiaryScopeValue" class="columndata"><ffi:setProperty name="WirePayeeScopes" property="Key" value="${WireTransferPayee.PayeeScope}"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></td>
				</tr>
			</table>
		</td>
		<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td  id="confirmWireBeneficiaryBankInfoLabel" class="sectionhead" colspan="2">&gt; <%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Bank Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Bank Info<!--L10NEnd-->" %> </td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryBankNameLabel" width="115" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME %></td>
					<td id="confirmWireBeneficiaryBankNameValue" width="220" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryBankAddress1Label" class="sectionsubhead"><%= LABEL_BANK_ADDRESS_1 %></td>
					<td id="confirmWireBeneficiaryBankAddress1Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryBankAddress2Label"  class="sectionsubhead"><%= LABEL_BANK_ADDRESS_2 %></td>
					<td id="confirmWireBeneficiaryBankAddress2Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryBankCityLabel" class="sectionsubhead"><%= LABEL_BANK_CITY %></td>
					<td id="confirmWireBeneficiaryBankCityValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/></td>
				</tr>
				<ffi:cinclude value1="${StatesExistsForDestinationBankCountry}" value2="true">
				<tr>
					<td id="confirmWireBeneficiaryBankStateLabel" class="sectionsubhead"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_BANK_STATE_PROVINCE : LABEL_BANK_STATE %></td>
					<td id="confirmWireBeneficiaryBankStateValue" class="columndata">
						<ffi:getProperty name="WireTransferPayee" property="DestinationBank.StateDisplayName"/>
					</td>
				</tr>
				</ffi:cinclude>
				<tr>
					<td id="confirmWireBeneficiaryBankZipCodeLabel" class="sectionsubhead"><%= (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) ? LABEL_BANK_ZIP_POSTAL_CODE : LABEL_BANK_ZIP_CODE %></td>
					<td id="confirmWireBeneficiaryBankZipCodeValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/></td>
				</tr>
				<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
				<tr>		
					<td id="confirmWireBeneficiaryBankCountryLabel" class="sectionsubhead"><%= LABEL_BANK_COUNTRY %></td>			
					<td id="confirmWireBeneficiaryBankCountryValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CountryDisplayName"/></td>
				</tr>
				<% } %>
				<tr>
					<td id="confirmWireBeneficiaryBankFEDABALabel" class="sectionsubhead"><%= wireType.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_ABA : LABEL_FED_ABA %></td>
					<td id="confirmWireBeneficiaryBankFEDABAValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/></td>
				</tr>
				<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
				<tr>
					<td id="confirmWireBeneficiaryBankSwiftLabel" class="sectionsubhead"><%= LABEL_SWIFT %></td>
					<td id="confirmWireBeneficiaryBankSwiftValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingSwift"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryBankChipsLabel" class="sectionsubhead"><%= LABEL_CHIPS %></td>
					<td id="confirmWireBeneficiaryBankChipsValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingChips"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryBankNationalLabel" class="sectionsubhead"><%= LABEL_NATIONAL %></td>
					<td id="confirmWireBeneficiaryBankNationalValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingOther"/></td>
				</tr>
				<tr>
					<td id="confirmWireBeneficiaryBankIBANLabel" class="sectionsubhead"><%= LABEL_IBAN %></td>
					<td id="confirmWireBeneficiaryBankIBANValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></td>
				</tr>
				<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
				<tr>
					<td class="sectionsubhead"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
					<td class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
				</tr>
				</ffi:cinclude>
				<% } %>
				
				<% if (wireType.equals(FED)) { %>
				<tr>
					<td id="confirmWireBeneficiaryBankIBANFEDLabel" class="sectionsubhead"><%= LABEL_IBAN %></td>
					<td id="confirmWireBeneficiaryBankIBANFEDValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></td>
				</tr>
				<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
				<tr>
					<td class="sectionsubhead"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
					<td class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
				</tr>
				</ffi:cinclude>
				<% } %>
			</table>
		</td>
	</tr> 
</table>--%>
<%-- <% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL) || wireType.equals(FED)) { %>
<ffi:setProperty name="WireTransferPayee" property="IntermediaryBanks.Filter" value="ACTION!!del"/>
<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks}" value2="" operator="notEquals" >
<ffi:cinclude value1="${WireTransferPayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
<table width="720" border="0" cellspacing="0" cellpadding="3">
	<tr>
		<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="1" border="0"></td>
		<td class="sectionhead" colspan="5">&gt; <!--L10NStart-->Intermediary Banks<!--L10NEnd--></td>
	</tr>
	<tr>
		<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
		<td width="5%" class="sectionsubhead">&nbsp;</td>
		<td width="30%" class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></td>
		<td width="12%" class="sectionsubhead"><%= COLUMN_FED_ABA %></td>
		<td width="12%" class="sectionsubhead"><%= COLUMN_SWIFT %></td>
		<td width="11%" class="sectionsubhead"><%= COLUMN_CHIPS %></td>
		<td width="10%" class="sectionsubhead"><%= COLUMN_NATIONAL %></td>
		<td width="10%" class="sectionsubhead"><%= COLUMN_IBAN %></td>
		<td width="10%" class="sectionsubhead"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
		<% } else if(wireType.equals(FED)) { %> 
		<td width="5%" class="sectionsubhead">&nbsp;</td>
		<td width="35%" class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></td>
		<td width="20%" class="sectionsubhead"><%= COLUMN_FED_ABA %></td>
		<td width="20%" class="sectionsubhead"><%= COLUMN_IBAN %></td>
		<td width="20%" class="sectionsubhead"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
		<% } else { %>
		<td width="5%" class="sectionsubhead">&nbsp;</td>
		<td width="45%" class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></td>
		<td width="50%" class="sectionsubhead"><%= COLUMN_FED_ABA %></td>
		<% } %>
	</tr>
	<% int counter = 0; %>
	<ffi:list collection="WireTransferPayee.IntermediaryBanks" items="Bank1">
	<tr>
		<td class="columndata">&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</td>
		<% if (wireType.equals(DOMESTIC) || wireType.equals(INTERNATIONAL)) { %>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</td>
		<td class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
		<td class="columndata">
			<% if(counter == 0) { %>
			&nbsp;
			<% } else { %>
			<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
			<% } %>&nbsp;</td>
		<% } %>
		<% if (wireType.equals(FED)) { %>
		<td class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
		<td class="columndata">
			<% if(counter == 0) { %>
			&nbsp;
			<% } else { %>
			<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
			<% }  %>&nbsp;</td>
		<% } counter++; %>
	</tr>
	</ffi:list>
</table>
</ffi:cinclude>
</ffi:cinclude>
<% } %> --%>
<%-- DOMESTIC --%>


<div class="blockWrapper" role="form" aria-labelledby="drawDownInfoHeader">
	<div class="blockHead" id="confirmWireCreditInfoLabel"><h2 id="drawDownInfoHeader"><%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Credit Info<!--L10NEnd-->" : "<!--L10NStart-->Debit Info<!--L10NEnd-->" %></h2></div>
	<div class="blockContent">
		<% // International wires need a wider first column than domestic.
		int c1 = 135;
		if (wireType.equals(INTERNATIONAL)) c1 = 160;
		int c2 = 343 - c1;
		
		if (!wireType.equals(DRAWDOWN)) { %>
		<div class="blockRow">
			<span id="confirmWireDebitAccountLabel" width="<%= c1 %>" class="sectionsubhead sectionLabel"><%= LABEL_FROM_ACCOUNT %>: </span>
			<span id="confirmWireDebitAccountValue" width="<%= c2 %>" class="columndata"><ffi:getProperty name="<%= task %>" property="FromAccountNumberDisplayText"/></span>
		</div>
		<% } %>
		<div class="blockRow">
			<span id="confirmWireDebitAmountLabel" width="<%= c1 %>" class="sectionsubhead sectionLabel"><%= wireIsTemplate ? LABEL_TEMPLATE_LIMIT : LABEL_AMOUNT %>: </span>
			<span id="confirmWireDebitAmountValue" width="<%= c2 %>" class="columndata">
				<ffi:cinclude value1="${wireAmountField}" value2="Amount" operator="equals">
				<ffi:getProperty name="<%= task %>" property="DisplayAmount"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${wireAmountField}" value2="OrigAmount" operator="equals">
				<ffi:getProperty name="<%= task %>" property="DisplayOrigAmountDomestic"/>
				</ffi:cinclude>
			</span>
		</div>
		<ffi:removeProperty name="GetAmounts"/>
		<% if (!wireIsTemplate) { %>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireDebitReqDateLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(INTERNATIONAL) ? LABEL_REQ_PROCESSING_DATE : LABEL_REQ_DUE_DATE %>: </span>
				<span id="confirmWireDebitReqDateValue" class="columndata"><ffi:getProperty name="<%= task %>" property="DueDate"/></span>
			</div>
			<% if (wireType.equals(INTERNATIONAL)) { %>
			<div class="inlineBlock">
				<span id="confirmWireDebitSettlementDateLabel" class="sectionsubhead sectionLabel"><%= LABEL_SETTLEMENT_DATE %>: </span>
				<span id="confirmWireDebitSettlementDateValue" class="columndata"><ffi:getProperty name="<%= task %>" property="SettlementDate"/></span>
			</div>
			<% } %>
		</div>
		<div class="blockRow">	
			<span id="confirmWireExpectedDateLabel" class="sectionsubhead sectionLabel"><%= wireType.equals(INTERNATIONAL) ? LABEL_ACTUAL_PROCESSING_DATE : LABEL_ACTUAL_DUE_DATE %>: </span>
			<span id="confirmWireExpectedDateValue" class="columndata"><ffi:getProperty name="<%= task %>" property="DateToPost"/></span>
		</div>
		<% } %>
		<% if (wire.getType() == com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) { %>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireFreqLabel" class="sectionsubhead sectionLabel"><%= LABEL_FREQUENCY %>: </span>
				<span id="confirmWireFreqValue" class="columndata"><ffi:getProperty name="<%= task %>" property="FrequencyDisplayName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireOfPaymentLabel" class="sectionsubhead sectionLabel"><%= LABEL_NUMBER_OF_PAYMENTS %>: </span>
				<span  id="confirmWireOfPaymentValue" class="columndata">
					<% if (wire.getNumberTransfersValue() == 999) { %>
					<%= LABEL_UNLIMITED %>
					<% } else { %>
					<ffi:getProperty name="<%= task %>" property="NumberTransfers"/>
					<% } %>
				</span>
			</div>
		</div>
		<% } %>
	</div>
</div>
<% if (!wireType.equals(INTERNATIONAL)) { %>
<% } else {     %>
	<div class="blockContent">	
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmWireDebitCurrencyLabel" class="sectionsubhead sectionLabel"><%= LABEL_DEBIT_CURRENCY %>: </span>
					<span id="confirmWireDebitCurrencyValue" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmWireExchangeRateLabel"  class="sectionsubhead sectionLabel" nowrap><%= LABEL_EXCHANGE_RATE %>: </span>
					<span id="confirmWireExchangeRateValue" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ExchangeRate"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="confirmWireMathRuleLabel" class="sectionsubhead sectionLabel"><%= LABEL_MATH_RULE %>: </span>
					<span id="confirmWireMathRuleValue" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="MathRule"/></span>
				</div>
				<div class="inlineBlock">
					<span id="confirmContractNumberLabel" class="sectionsubhead sectionLabel"><%= LABEL_CONTRACT_NUMBER %>: </span>
					<span id="confirmContractNumberValue" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ContractNumber"/></span>
				</div>
			</div>
	</div>
	<div class="blockWrapper" role="form" aria-labelledby="payInfoHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="payInfoHeader">Payment Info</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<span id="ConfirmWirepaymentCurrencyLabel" class="sectionsubhead sectionLabel"><%= LABEL_PAYMENT_CURRENCY %>: </span>
			<span id="ConfirmWirepaymentCurrencyValue" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="PayeeCurrencyType"/></span>
		</div>
	</div>
</div>
<% } %>
<%--<table width="715" cellpadding="0" cellspacing="0" border="0">
	 <tr>
		<td id="confirmWireCreditInfoLabel" class="tbrd_b sectionhead" colspan="3">&gt; <%= wireType.equals(DRAWDOWN) ? "<!--L10NStart-->Credit Info<!--L10NEnd-->" : "<!--L10NStart-->Debit Info<!--L10NEnd-->" %></td>
	</tr>
	<tr>
		<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr> --%>
	<%--<tr>
		<td width="357" valign="top">
			 <table width="355" cellpadding="3" cellspacing="0" border="0">
				<% // International wires need a wider first column than domestic.
				int c1 = 135;
				if (wireType.equals(INTERNATIONAL)) c1 = 160;
				int c2 = 343 - c1;
				
				if (!wireType.equals(DRAWDOWN)) { %>
				<tr>
					<td id="confirmWireDebitAccountLabel" width="<%= c1 %>" class="sectionsubhead"><%= LABEL_FROM_ACCOUNT %></td>
					<td id="confirmWireDebitAccountValue" width="<%= c2 %>" class="columndata">
						<ffi:getProperty name="<%= task %>" property="FromAccountNumberDisplayText"/>
					</td>
				</tr>
				<% } %>
				<tr>
					<td id="confirmWireDebitAmountLabel" width="<%= c1 %>" class="sectionsubhead"><%= wireIsTemplate ? LABEL_TEMPLATE_LIMIT : LABEL_AMOUNT %></td>
					<td id="confirmWireDebitAmountValue" width="<%= c2 %>" class="columndata">
					<ffi:cinclude value1="${wireAmountField}" value2="Amount" operator="equals">
						<ffi:getProperty name="<%= task %>" property="DisplayAmount"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${wireAmountField}" value2="OrigAmount" operator="equals">
						<ffi:getProperty name="<%= task %>" property="DisplayOrigAmountDomestic"/>
					</ffi:cinclude>
					</td>
				</tr>
				<ffi:removeProperty name="GetAmounts"/>
				<% if (!wireIsTemplate) { %>
				<tr>
					<td id="confirmWireDebitReqDateLabel" class="sectionsubhead"><%= wireType.equals(INTERNATIONAL) ? LABEL_REQ_PROCESSING_DATE : LABEL_REQ_DUE_DATE %></td>
					<td id="confirmWireDebitReqDateValue" class="columndata"><ffi:getProperty name="<%= task %>" property="DueDate"/></td>
				</tr>
					<% if (wireType.equals(INTERNATIONAL)) { %>
					<tr>
						<td id="confirmWireDebitSettlementDateLabel" class="sectionsubhead"><%= LABEL_SETTLEMENT_DATE %></td>
						<td id="confirmWireDebitSettlementDateValue" class="columndata"><ffi:getProperty name="<%= task %>" property="SettlementDate"/></td>
					</tr>
					<% } %>
				<tr>
					<td id="confirmWireExpectedDateLabel" class="sectionsubhead"><%= wireType.equals(INTERNATIONAL) ? LABEL_ACTUAL_PROCESSING_DATE : LABEL_ACTUAL_DUE_DATE %></td>
					<td id="confirmWireExpectedDateValue" class="columndata"><ffi:getProperty name="<%= task %>" property="DateToPost"/></td>
				</tr>
				<% } %>
<% if (wire.getType() == com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) { %>
				<tr>
					<td id="confirmWireFreqLabel" class="sectionsubhead"><%= LABEL_FREQUENCY %></td>
					<td id="confirmWireFreqValue" class="columndata">
					<ffi:getProperty name="<%= task %>" property="FrequencyDisplayName"/>
					</td>
				</tr>
				<tr>
					<td id="confirmWireOfPaymentLabel" class="sectionsubhead"><%= LABEL_NUMBER_OF_PAYMENTS %></td>
					<td  id="confirmWireOfPaymentValue" class="columndata">
					<% if (wire.getNumberTransfersValue() == 999) { %>
						<%= LABEL_UNLIMITED %>
					<% } else { %>
						<ffi:getProperty name="<%= task %>" property="NumberTransfers"/>
					<% } %>
					</td>
				</tr>
<% } %>
			</table>
		</td> 
<% if (!wireType.equals(INTERNATIONAL)) { %>
		<td colspan="2">&nbsp;</td>
<% } else {     %>
		<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td id="confirmWireDebitCurrencyLabel" width="115" class="sectionsubhead"><%= LABEL_DEBIT_CURRENCY %></td>
					<td id="confirmWireDebitCurrencyValue" width="220" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="${wireCurrencyField}"/></td>
				</tr>
				<tr>
					<td id="confirmWireExchangeRateLabel"  class="sectionsubhead" nowrap><%= LABEL_EXCHANGE_RATE %></td>
					<td id="confirmWireExchangeRateValue" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ExchangeRate"/></td>
				</tr>
				<tr>
					<td id="confirmWireMathRuleLabel" class="sectionsubhead"><%= LABEL_MATH_RULE %></td>
					<td id="confirmWireMathRuleValue" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="MathRule"/></td>
				</tr>
				<tr>
					<td id="confirmContractNumberLabel" class="sectionsubhead"><%= LABEL_CONTRACT_NUMBER %></td>
					<td id="confirmContractNumberValue" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="ContractNumber"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>--%>
<%-- <table width="715" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="tbrd_b sectionhead">&gt; <!--L10NStart-->Payment Info<!--L10NEnd--></td>
	</tr>
	<tr>
		<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td>
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td id="ConfirmWirepaymentCurrencyLabel" width="115" class="sectionsubhead"><%= LABEL_PAYMENT_CURRENCY %></td>
					<td id="ConfirmWirepaymentCurrencyValue" width="228" class="columndata"><ffi:getProperty name="<%= intFieldTask %>" property="PayeeCurrencyType"/></td>
				</tr>
			</table>
		</td>
<% } %>
	</tr>
</table> --%>
<div class="blockWrapper" role="form" aria-labelledby="internationalInfoHeader">
	<div class="blockHead">
		<h2 id="internationalInfoHeader"><%= wireType.equals(INTERNATIONAL) ? "<!--L10NStart-->Sender's Reference<!--L10NEnd-->" : "<!--L10NStart-->Reference for Beneficiary<!--L10NEnd-->" %>:</h2>
		<ffi:getProperty name="<%= task %>" property="Comment"/>
	</div>
	<div class="blockHead"><!--L10NStart-->Originator to Beneficiary Information<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<% String info = ""; %>
			<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo1"/>
			<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2" assignTo="info"/>
			<% if (info != null && info.length() > 0) { %>
				<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2"/>
			<% } %>
			<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3" assignTo="info"/>
			<% if (info != null && info.length() > 0) { %>
				<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3"/>
			<% } %>
			<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4" assignTo="info"/>
			<% if (info != null && info.length() > 0) { %>
				<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4"/>
			<% } %>
		</div>
	</div>
		<% if (!wireType.equals(BOOK)) { %>
		<div  class="blockHead"><!--L10NStart--><h2 id="internationalInfoHeader">Bank to Bank Information</h2><!--L10NEnd--></div>
				<div class="blockContent">
					<div class="blockRow">
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo1"/>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo2" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo2"/>
						<% } %>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo3" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo3"/>
						<% } %>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo4" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo4"/>
						<% } %>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo5" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo5"/>
						<% } %>
						<ffi:getProperty name="<%= task %>" property="BankToBankInfo6" assignTo="info"/>
						<% if (info != null && info.length() > 0) { %>
							<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo6"/>
						<% } %>
					</div>
				</div>
		<% } %>
</div>
<ffi:cinclude value1="<%= wire.getWireDestination() %>" value2="<%= WireDefines.WIRE_FED %>" operator="equals">
    <ffi:cinclude value1="<%= wire.getAddendaType() %>" value2="" operator="notEquals">
     	<div class="blockWrapper"  role="form" aria-labelledby="addendaHeader">
			<div  class="blockHead"><!--L10NStart--><h2 id="addendaHeader">Addenda</h2><!--L10NEnd--></span></div>
             <div class="blockContent">
				<div class="blockRow">
                   <span class="sectionsubhead sectionLabel"><!--L10NStart-->Addenda Type<!--L10NEnd-->: </span>
                   <span class="columndata"><ffi:getProperty name="<%= task %>" property="AddendaType"/> - <ffi:getProperty name="<%= task %>" property="AddendaTypeDisplayText"/></span>
                </div>
                <div class="blockRow">
                    <span class="sectionsubhead sectionLabel"><!--L10NStart-->Addenda<!--L10NEnd-->: </span>
	                    <% String addendaString = ""; %>
	                    <ffi:getProperty name="<%= task %>" property="AddendaForDisplay" assignTo="addendaString"/>
	                    <%
	                        if( addendaString != null ) {
	                            addendaString = com.ffusion.util.HTMLUtil.encode( addendaString );
	
	                                // convert all line separator characters to html newline i.e <br>
	                            addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "\n", "<br>");
	                            // convert all MULTIPLE text spaces to HTML non-breaking spaces i.e &nbsp;
	                            addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "  ", "&nbsp;&nbsp;");
	                            // in case there was an odd amount of multiple spaces, convert to double space
	                            addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "&nbsp; ", "&nbsp;&nbsp;");
	                        }
	                    %>
                    <span class="columndata"><%= addendaString %> </span>
               </div>
              </div>
           </div>    
    </ffi:cinclude>
</ffi:cinclude>
<div class="blockWrapper" role="form" aria-labelledby="byOrderInfoHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="byOrderInfoHeader">By Order Of Information</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span width="115" class="sectionsubhead sectionLabel"><%= LABEL_BOO_NAME %>: </span>
				<span width="600" class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_BOO_ADDRESS_1 %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress1"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%= LABEL_BOO_ADDRESS_2 %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress2"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= LABEL_BOO_ADDRESS_3 %>: </span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress3"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><%= LABEL_BOO_ACCOUNT_NUMBER %>: </span>
			<span class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAccount"/></span>
		</div>
	</div>
</div>
<ffi:cinclude value1="${templateTask}" value2="edit" operator="equals">
   <div class="blockWrapper" role="form" aria-labelledby="templateCycleHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="templateCycleHeader">Template Lifecycle</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span width="115" class="sectionsubhead sectionLabel"><!--L10NStart-->Current Status: <!--L10NEnd--></span>
				<span width="600" class="columndata"><ffi:getProperty name="<%= task %>" property="StatusName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Added Comment: <!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="<%= task %>" property="TemplateComment"/></span>
			</div>
		</div>
	</div>
</div>
</ffi:cinclude>						
<div align="center">
	<s:form namespace="/pages/jsp/wires" action="addWireTransferAction_print" method="post" name="TransferNew" id="TransferNewSendID">
	    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	</s:form>
</div>
<div class="btn-row">
	<ffi:cinclude value1="${wireSource}" value2="TEMPLATE">
		<sj:a id="sendFormButtonOnConfirm" 
			button="true" summaryDivId="summary" buttonType="done"
			onClickTopics="showSummary,cancelWireTransferForm"
		><s:text name="jsp.default_175" /><!-- DONE -->
		</sj:a>													
	</ffi:cinclude>
	<ffi:cinclude value1="${wireSource}" value2="TEMPLATE" operator="notEquals">
		<sj:a id="sendFormButtonOnConfirm" 
			button="true"  summaryDivId="summary" buttonType="done"
			onClickTopics="showSummary,cancelWireTransferForm"
		><s:text name="jsp.default_175" /><!-- DONE -->
		</sj:a>									
	</ffi:cinclude>	
    <% if (!wireIsTemplate) { %>
	   	<script>
			ns.wire.printReadyUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/inc/print_wiretransfersend.jsp"/>';
			var isHostWire = $("#isHostWireID").val();
			if( isHostWire == "true" ){
			ns.wire.printReadyUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/inc/print_wirehostsend.jsp"/>';	
			}
		</script>
	   	<sj:a id="sendFormButtonOnPrint" 
			  formIds="TransferNewSendID"
			  name="printerReady"
			  button="true" 
			  targets="printDiv"
			  onclick="ns.wire.printerReadyButtonOnClick(ns.wire.printReadyUrl);"
			  cssClass="rpt_det_ctrl_notcancel"
		>
			<s:text name="jsp.default_332" />
		</sj:a>
	<% } %>
</div>
									
                     <%-- <table width="715" border="0" cellspacing="0" cellpadding="3">
								<tr>
									<td class="tbrd_b"><span class="sectionhead">&gt; <%= wireType.equals(INTERNATIONAL) ? "<!--L10NStart-->Sender's Reference<!--L10NEnd-->" : "<!--L10NStart-->Reference for Beneficiary<!--L10NEnd-->" %></span></td>
								</tr>
								<tr>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="Comment"/></td>
								</tr>
								<tr>
									<td class="tbrd_b"><span class="sectionhead">&gt; <!--L10NStart-->Originator to Beneficiary Information<!--L10NEnd--></span></td>
								</tr>
								<tr>
									<td class="columndata">
									<% String info = ""; %>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo1"/>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo2"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo3"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="OrigToBeneficiaryInfo4"/></td>
									<% } %>
								</tr>
								<% if (!wireType.equals(BOOK)) { %>
								<tr>
									<td class="tbrd_b"><span class="sectionhead">&gt; <!--L10NStart-->Bank to Bank Information<!--L10NEnd--></span></td>
								</tr>
								<tr>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="BankToBankInfo1"/>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo2" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo2"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo3" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo3"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo4" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo4"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo5" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo5"/>
									<% } %>
									<ffi:getProperty name="<%= task %>" property="BankToBankInfo6" assignTo="info"/>
									<% if (info != null && info.length() > 0) { %>
										<br><ffi:getProperty name="<%= task %>" property="BankToBankInfo6"/></td>
									<% } %>
								</tr>
								<% } %>
							</table> --%>
                        <%-- <ffi:cinclude value1="<%= wire.getWireDestination() %>" value2="<%= WireDefines.WIRE_FED %>" operator="equals">
                            <ffi:cinclude value1="<%= wire.getAddendaType() %>" value2="" operator="notEquals">
                                                    <table width="715" border="0" cellspacing="0" cellpadding="3">
                                                        <tr>
                                                            <td class="tbrd_b" colspan="2"><span class="sectionhead">&gt; <!--L10NStart-->Addenda<!--L10NEnd--></span></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="115" class="sectionsubhead"><!--L10NStart-->Addenda Type<!--L10NEnd--></td>
                                                            <td width="600" class="columndata"><ffi:getProperty name="<%= task %>" property="AddendaType"/> - <ffi:getProperty name="<%= task %>" property="AddendaTypeDisplayText"/></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="sectionsubhead"><!--L10NStart-->Addenda<!--L10NEnd--></td>
                                                            <% String addendaString = ""; %>
                                                            <ffi:getProperty name="<%= task %>" property="AddendaForDisplay" assignTo="addendaString"/>
                                                            <%
                                                                if( addendaString != null ) {
                                                                    addendaString = com.ffusion.util.HTMLUtil.encode( addendaString );

                                                                        // convert all line separator characters to html newline i.e <br>
                                                                    addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "\n", "<br>");
                                                                    // convert all MULTIPLE text spaces to HTML non-breaking spaces i.e &nbsp;
                                                                    addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "  ", "&nbsp;&nbsp;");
                                                                    // in case there was an odd amount of multiple spaces, convert to double space
                                                                    addendaString = com.ffusion.util.Strings.replaceStr(addendaString, "&nbsp; ", "&nbsp;&nbsp;");
                                                                }
                                                            %>
                                                            <td class="columndata"><%= addendaString %>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br>
                            </ffi:cinclude>
                        </ffi:cinclude> --%>
							<%-- <table width="715" border="0" cellspacing="0" cellpadding="3">
								<tr>
									<td class="tbrd_b" colspan="2"><span class="sectionhead">&gt; <!--L10NStart-->By Order Of Information<!--L10NEnd--></span></td>
								</tr>
								<tr>
									<td width="115" class="sectionsubhead"><%= LABEL_BOO_NAME %></td>
									<td width="600" class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfName"/></td>
								</tr>
								<tr>
									<td class="sectionsubhead"><%= LABEL_BOO_ADDRESS_1 %></td>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress1"/></td>
								</tr>
								<tr>
									<td class="sectionsubhead"><%= LABEL_BOO_ADDRESS_2 %></td>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress2"/></td>
								</tr>
								<tr>
									<td class="sectionsubhead"><%= LABEL_BOO_ADDRESS_3 %></td>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAddress3"/></td>
								</tr>
								<tr>
									<td class="sectionsubhead"><%= LABEL_BOO_ACCOUNT_NUMBER %></td>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="ByOrderOfAccount"/></td>
								</tr>
							</table> --%>
                            <%-- <ffi:cinclude value1="${templateTask}" value2="edit" operator="equals">
                            <table width="715" border="0" cellspacing="0" cellpadding="3">
								<tr>
									<td class="tbrd_b" colspan="2"><span class="sectionhead">&gt; <!--L10NStart-->Template Lifecycle<!--L10NEnd--></span></td>
								</tr>
								<tr>
									<td width="115" class="sectionsubhead"><!--L10NStart-->Current Status<!--L10NEnd--></td>
									<td width="600" class="columndata"><ffi:getProperty name="<%= task %>" property="StatusName"/></td>
								</tr>
								<tr>
									<td class="sectionsubhead"><!--L10NStart-->Added Comment<!--L10NEnd--></td>
									<td class="columndata"><ffi:getProperty name="<%= task %>" property="TemplateComment"/></td>
								</tr>							
							</table>
							<br>
                            </ffi:cinclude> --%>
	</div>
</div>


<ffi:removeProperty name="operation_action"/>
<ffi:removeProperty name="new_wireBackURL"/>
<ffi:removeProperty name="wirePayeeStatusMsg"/>
<ffi:removeProperty name="WireTransferIntermediaryBanks"/>
<ffi:removeProperty name="WireTransferDestinationBanks"/>

<%--=================== PAGE SECURITY CODE - Part 2 Start ====================--%>
<%--</ffi:cinclude>--%>
<%}%>
<%--=================== PAGE SECURITY CODE - Part 2 End =====================--%>


<script type="text/javascript">
	$(document).ready(function(){
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-positive").addClass("icon-negative")
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-positive").removeClass("icon-negative")
			}
		});
		var colorCode = $('#colorCode').val();
		if(colorCode=="red"){
			$('#statusBlock').addClass("failed");
			$('#wireTransferStatusIcon').addClass('icon-failed');
		}else if(colorCode=="green"){
			$('#statusBlock').addClass("completed");
			$('#wireTransferStatusIcon').addClass('icon-accept');
		}else{
			$('#statusBlock').addClass("pending");
			$('#wireTransferStatusIcon').addClass('icon-pending');
		}
	});

</script>