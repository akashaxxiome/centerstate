<%--
This page is used for the following operations:
	Add and Edit drawdown wire transfer
	Add and Edit drawdown wire transfer to batch
	Add and Edit drawdown wire transfer template

Pages that request this page
----------------------------
wiretransfers.jsp (wire transfer summary)
	NEW WIRE button (when drawdown is selected in Wire Type)
wire_transfers_list.jsp (included in wiretransfers.jsp)
	Edit button next to a drawdown wire transfer
wiretransferconfirm.jsp (wire transfer confirm)
	BACK button
wirebatchff.jsp (add freeform wire batch)
	ADD WIRE button
	Edit button next to a wire transfer
wirebatcheditff.jsp (edit freeform wire batch)
	ADD WIRE button
	Edit button next to a wire transfer
wirebatchtemp.jsp (add template batch)
	Edit button next to a wire transfer
wirebatchedittemplate.jsp (edit template batch)
	Edit button next to a wire transfer
wire_templates_list.jsp (included in wiretemplates.jsp)
	Load button next to a drawdown wire template
	Edit button next to a drawdown wire template
wire_type_select.jsp (included in wire_common_init.jsp, which is in turn included
	in wiretransfernew.jsp, wirebook.jsp, wiredrawdown.jsp,	and wirefed.jsp)
	Changing the Wire Type to triggers a javascript function called setDest() in
	wire_scripts_js.jsp
wireaddpayee.jsp
	CANCEL button

Pages this page requests
------------------------
LOAD TEMPLATE requests wiredrawdown.jsp (itself)
MANAGE BENEFICIARY requests wireaddpayeedrawdown.jsp
SEARCH requests one of the following:
	wirebanklist.jsp
	wirebanklistselect.jsp
CANCEL requests one of the following:
	wiretransfers.jsp
	wirebatchff.jsp
	wirebatchtemp.jsp
	wirebatcheditff.jsp
	wirebatchtemplateedit.jsp
	wiretemplates.jsp
SUBMIT WIRE/SUBMIT TEMPLATE requests wiretransferconfirm.jsp
Choose Wire Type requests one of the following:
	wiretransfernew.jsp (itself, when changing between Domestic and International)
	wirebook.jsp
	wiredrawdown.jsp
	wirehost.jsp
	wirefed.jsp
Credit Account selects wiredrawdown.jsp (itself)
Select Beneficiary requests wiretransfernew.jsp (itself)
Calendar button requests calendar.jsp

Pages included in this page
---------------------------
payments/inc/wire_common_init.jsp
	Common task initialization for all wire types
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/wire_scripts_js.jsp
	Common javascript functions used for all wire types
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_template_picker.jsp
	Template drop-down list
payments/inc/wire_template_fields.jsp
	Template fields, used when creating wire templates
payments/inc/wire_addedit_template_info.jsp
	Read-only template info, used when loading a template
payments/inc/wire_addedit_payee_picker.jsp
	Select Beneficiary drop-down list and MANAGE BENEFICIARY button
payments/inc/wire_addedit_payee_fields.jsp
	Beneficiary name & address fields
payments/inc/wire_addedit_bank_fields.jsp
	Beneficiary destination bank name & address fields
payments/inc/wire_addedit_payment_fields.jsp
	Wire amount, date, and recurring fields
payments/inc/wire_addedit_comment_fields.jsp
	Wire transfer comment and By Order Of feilds
common/checkAmount_js.jsp
	A common javascript that validates an amount is formatted properly
--%>
<%@ page import="com.ffusion.tasks.wiretransfers.ModifyWireTransfer,
                 com.ffusion.beans.accounts.Accounts" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>

<%@ include file="../common/wire_labels.jsp"%>

<ffi:help id="payments_wiredrawdown" className="moduleHelpClass"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%-- ----------------------------------------------------------------------------------------------- --
NOTE:  This page is for ADD and MODIFY wire transfers, ADD and MODIFY wire transfer templates, ADD and
MODIFY wire transfers in a new batch, and ADD and MODIFY wire transfers in an existing batch.
The object it's utilizing is in the session called "wireTask":
	"AddWireTransfer" for add single wire.
	"ModifyWireTransfer" for edit single wire.
	"WireTransfer" for add and edit wires part of a batch.
If a string is in the session called "templateTask", and it's one of the following values, it will
treat the wire transfer as a wire transfer template:
	"add" for add wire template.
	"edit" for edit wire template.
If a string is in the session called "wireBatch", and it's one of the following values, it will
treat the wire transfer as a part of a batch:
	"AddWireBatch" for wires included in a new batch.
	"ModifyWireBatch" for wires included in an existing batch.
---------------------------------------------------------------------------------------------------- --%>

<%
if( request.getParameter("notTemplate") != null ) { session.removeAttribute("templateTask"); }
if( request.getParameter("wireTask") != null ){ session.setAttribute("wireTask", request.getParameter("wireTask")); }
if( request.getParameter("wireDestination") != null ){ session.setAttribute("wireDestination", request.getParameter("wireDestination")); }
if( request.getParameter("DA_WIRE_PRESENT") != null ){ session.setAttribute("DA_WIRE_PRESENT", request.getParameter("DA_WIRE_PRESENT")); }
if( request.getParameter("DontInitialize") != null ){ session.setAttribute("DontInitialize", request.getParameter("DontInitialize")); }
if( request.getParameter("setPayee") != null ){ session.setAttribute("setPayee", request.getParameter("setPayee")); }
if( request.getParameter("delIntBank") != null ){ session.setAttribute("delIntBank", request.getParameter("delIntBank")); }
if( request.getParameter("selectedCountry") != null ){ session.setAttribute("selectedCountry", request.getParameter("selectedCountry")); }
if( request.getParameter("templateTask") != null ){ session.setAttribute("templateTask", request.getParameter("templateTask")); }
//***Fix QTS:661548 & 662132.Start***
//CR Descriptions:Wire template use button does not work correct, works as clone.
//The session should be removed when it not used.
else if(request.getParameter("userTemplate") != null){ session.removeAttribute("templateTask");}
//***Fix QTS:661548 & 662132.End***
if( request.getParameter("ID") != null){ session.setAttribute("ID", request.getParameter("ID")); }
if( request.getParameter("recID") != null ){ session.setAttribute("recID", request.getParameter("recID")); }
if( request.getParameter("collectionName") != null ){ session.setAttribute("collectionName", request.getParameter("collectionName")); }
if( request.getParameter("wireInBatch") != null){ session.setAttribute("wireInBatch", request.getParameter("wireInBatch")); }
if( request.getParameter("LoadFromTransferTemplate") != null && request.getParameter("LoadFromTransferTemplate").length() > 0 ){ session.setAttribute("LoadFromTransferTemplate", request.getParameter("LoadFromTransferTemplate")); }
if( request.getParameter("wireBatch") != null ){ session.setAttribute("wireBatch", request.getParameter("wireBatch")); }
if( request.getParameter("wireIndex") != null ){ session.setAttribute("wireIndex", request.getParameter("wireIndex")); }
if( request.getParameter("DontInitializeBatch") != null ){ session.setAttribute("DontInitializeBatch", request.getParameter("DontInitializeBatch")); }
if( request.getParameter("editWireTransfer") != null ){ session.setAttribute("editWireTransfer", request.getParameter("editWireTransfer")); }
%>

<script>
	var wireDestination = "<ffi:getProperty name="${wireTask}" property="WireDestination"/>";
	var PAYEE_DESTINATION_DA = "<ffi:getProperty name="PAYEE_DESTINATION_DA" />";
	var isSimplified = $('#beneficiaryType').val();
	var value = "";	
	
	/* if (wireDestination == "DRAWDOWN") {
		value = {value:"Create New Debit Account", label:js_create_new_debit_account};		
	} else if(isSimplified){
		// do nothings
	}
	else {
	value = {value:"Create New Beneficiary", label:js_create_new_beneficiary};		
	} */
	
	$("#selectWirePayeeID").lookupbox({
		"source":"/cb/pages/jsp/wires/wireTransferPayeeLookupBoxAction.action?wireDestination="+wireDestination,
		"controlType":"server",
		"size":"30",
		"module":"wireLoadPayee"
	});
</script>

<%-- hints for bookmark --%>
<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
	<span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTransferLink,newSingleWiresTransferID</span>
	<span class="shortcutEntitlementClass" style="display:none;">Wires</span>
</ffi:cinclude>
<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
	<span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTemplateLink,newSingleWiresTemplateID</span>
	<span class="shortcutEntitlementClass" style="display:none;">Wires</span>
</ffi:cinclude>

<script>
$(function(){
		/* var value = {value:"Select Account", label: js_select_account}; */
		$("#selectPayeeOfDrawDownWireID").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"module":"Wires",
			"size":"40",
			"source":"/cb/pages/jsp/wires/WireAccountsLookupBoxAction.action"	
		});
});
</script>

<ffi:setProperty name="CallerForm" value="WireNew"/>
<ffi:setProperty name="CallerURL" value="wiredrawdown.jsp?DontInitialize=true" URLEncrypt="true"/>
<ffi:setProperty name="disableWireBeneficiaryEdit" value=""/>

<%-- Initialize the Wire Transfer, common strings, & other objects.  Plus other common code (like the set payee code) --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_common_init.jsp"/>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wiredrawdown.jsp?DontInitialize=true" URLEncrypt="true" />


<%-- Start: Dual approval processing --%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="PAYEE_DESTINATION_DA" value="<%= WireDefines.PAYEE_TYPE_DRAWDOWN %>"/>
</ffi:cinclude>

<%-- End: Dual approval processing --%>


<s:include value="%{#session.PagesPath}/wires/inc/nav_menu_top_js.jsp" />
<s:include value="%{#session.PagesPath}/wires/inc/wire_scripts_js.jsp" />
<script>
function setAccount(a) {
	$('#accountID').val(a);
	var payeeID = $('#selectWirePayeeID').val(); 
	$('#payeeID').val(payeeID);
	var accountAction = "";
	frm = document.WireNew;
	accountAction = $("#accountAction").val();
	// check if manage beneficiary button is there
	var isManagedVisible = $('#manageBeneficiaryButtonID').size()==0;
	$.ajax({   
		type: "POST",   
		url: "/cb/pages/jsp/wires/"+accountAction,
		data: $("#newWiresTransferFormID").serialize(),
		success: function(data) {
			$('#inputDiv').html(data);
			$('#accountID').val(a);
			// if managed is not there remove if it comes for any reason
			if(isManagedVisible){
				$('#manageBeneficiaryButtonIDHolder').remove();
			}
			if($(".selecteBeneficiaryMessage div#freeFormBeneLoaded").html()!=' '){
        		$(".selecteBeneficiaryMessage").html('');
        		$(".selecteBeneficiaryMessage").html("<div class='sectionhead nameTitle' id='freeFormBeneLoaded' style='cursor:auto'>Beneficiary Info</div> ");
        		$('#expand').hide();
        		$('#collapse').hide();
        		$('#drawdownHiddenJsp').show();
        	}
		}   
	});
}
</script>

<div align="center"><ul id="formerrors"></ul>
<div class="leftPaneWrapper">
<%-- ------ LOAD TEMPLATE PICK LIST ------ --%>
<!-- below if condition added to hide load template page when coming from load/clone template or edit wire -->
<s:if test='%{#session.wireTransfer.ID=="" && #session.wireTransfer.wireType!="TEMPLATE"}'>
	<div id="wireTemplateDiv">
	<s:include value="%{#session.PagesPath}/wires/inc/wire_template_picker.jsp" />
	</div>
</s:if>
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
<div class="leftPaneInnerWrapper" role="form" aria-labelledby="addCreditAccountHeader">
	<div class="header" id=""><h2 id="addCreditAccountHeader">Add Credit Account</h2></div>
	<div class="leftPaneInnerBox">
		<div id="wireCreditAccountLbel" class="sectionsubhead" style="margin: 0 0 10px !important"><label for="selectPayeeOfDrawDownWireID"><%= LABEL_CREDIT_ACCOUNT %></label><span class="required" title="required">*</span></div>
		<%-- <% String curAcct = ""; String thisAcct = ""; %>
		<ffi:getProperty name="${wireTask}" property="FromAccountID" assignTo="curAcct"/>
		<% if (curAcct == null) curAcct = ""; %>
		<ffi:setProperty name="selectedAcct"  value="<%=curAcct%>"/> --%>
		<% String curAcct = ""; String thisAcct = ""; %>
		<ffi:getProperty name="${wireTask}" property="FromAccountID" assignTo="curAcct"/>
		<% if (curAcct == null) curAcct = ""; %>
		<ffi:setProperty name="selectedAcct"  value="<%=curAcct%>"/>
		<% if(curAcct != null && curAcct.trim().length()!=0) {%>
			<ffi:setProperty name="disableWireBeneficiaryEdit" value="disabled"/>
		<%}%>
		<input type="hidden" id="retainedAccountID" value="<ffi:getProperty name="curAcct"/>"/>
		<div class="selectBoxHolder creditAccountPayeeSelecor">									
			<select id="selectPayeeOfDrawDownWireID" class="txtbox" name="" onchange="setAccount(this.value);" <ffi:getProperty name="disableSemi"/>>
							
				<ffi:cinclude value1="${selectedAcct}" value2="" operator="notEquals">
				<ffi:cinclude value1="${selectedAcct}" value2="-1" operator="notEquals">
					<option value="<ffi:getProperty name="selectedAcct" />" selected><ffi:getProperty name="${wireTask}" property="FromAccountNumberDisplayText" /> </option>
				</ffi:cinclude>
				</ffi:cinclude>
				<%-- <ffi:cinclude value1="${selectedAcct}" value2="">
					<option value="<%= WireDefines.SELECT_ACCOUNT%>" selected><%= WireDefines.SELECT_ACCOUNT%></option>
				</ffi:cinclude> --%>
				</select>
			</div>
			<div><span id="<ffi:getProperty name="wireTask"/>.FromAccountIDError"></span></div>
			<%-- <div align="center" style="margin: 5px 0 5px; float:left">OR</div>
			<div align="center">		<!-- <input type="button" name="creditAccountDetails" value="Add New" onclick="setAccount('freeForm')" id="creditAccountDetails" style="margin-bottom:20px"/> -->
					<sj:a id="creditAccountDetails" 
						button="true" 
						onClick="setAccount('freeForm')"
						title="Add New">
						Add New
					</sj:a>
			</div> --%>
	</div>
</div>

<div class="leftPaneInnerWrapper" role="form" aria-labelledby="addDebitAccountHeader">
	<div class="header" id=""><h2 id="addDebitAccountHeader">Add Debit Account*</h2></div>
	<div class="leftPaneInnerBox">
	<% String curPayee = ""; String thisPayee = ""; String payeeName=""; String nickName="";String accountNum="";String wirePayeeID="";%>
	<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="curPayee"/>
	<ffi:getProperty name="WireTransferPayee" property="PayeeName" assignTo="payeeName" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="NickName" assignTo="nickName" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="AccountNum" assignTo="accountNum" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="ID" assignTo="wirePayeeID"/>
	<%-- <ffi:getProperty name="curPayee"/> --%>
	<% if (curPayee == null) curPayee = ""; %>

	<%-- Set current payee Id in session --%>
	<ffi:setProperty name="currentPayeeId" value="<%=curPayee%>"/>
	<ffi:cinclude value1="" value2="${currentPayeeId}" operator="equals">
		<ffi:setProperty name="currentPayeeId" value="<%= WireDefines.CREATE_NEW_BENEFICIARY%>"/>			
	</ffi:cinclude>
	<input type="hidden" id="retainedWirePayeeID" value="<ffi:getProperty name="curPayee"/>"/>
<select class="txtbox" id="selectWirePayeeID" name="" size="1" onChange="setPayee(this.options[this.selectedIndex].value)" <ffi:getProperty name="disableSemi"/>>
				<%
				if(curPayee!=null && !curPayee.isEmpty() && !curPayee.equals(WireDefines.WIRE_LOOKUPBOX_DEFAULT_VALUE) 
					&& !curPayee.equals(WireDefines.CREATE_NEW_BENEFICIARY) && !curPayee.equals(WireDefines.CREATE_NEW_DEBIT_ACCOUNT)){
					String label = payeeName;
					if(nickName!=null && !nickName.equalsIgnoreCase("")){
						label = label +" (" + nickName + " - "+ accountNum + ")";
					}else{
						label = label +" (" + accountNum + ")";
					}
					
				%>
				<option value="<%=curPayee%>" selected="selected"><%=label %></option>
				<%
				} else { 
				String label =  WireDefines.CREATE_NEW_BENEFICIARY;
				%>
				<option value="<%=label%>" selected="selected"><%=label%></option>
				<% } %>
	</select>
	<% String wireType = null; 
		String isRecModel = null;
	%>
	<ffi:getProperty name="${wireTask}" property="WireType" assignTo="wireType"/>
	<ffi:getProperty name="isRecModel" assignTo="isRecModel"/>
	<div align="center">
		<div style="margin: 5px 0 5px; float:left">OR</div>
			<sj:a name="payeeDetails" id="freeForm" onclick="loadBeneficiaryForm()" button="true">
					<s:text name="jsp.wire.freeForm"/>
			</sj:a>
			
	</div>
	</div>
</div>
	<%if (WireDefines.WIRE_TYPE_RECURRING.equals(wireType) && !"true".equals(isRecModel)) { %>
		<ffi:cinclude value1="${IsModify}" value2="true">
				<div class="leftPaneInnerWrapper" style="border:none !important;"><div class="marginTop20 floatleft fxActionBtnCls"><sj:a id="loadRecModelFormButton"
				                button="true"
				                onclick="selectRecModel();" 
				            ><s:text name="jsp.default.edit_orig_trans"/></sj:a>
		</div></div>
		</ffi:cinclude>
			<% } %>
	</ffi:cinclude></div>



<input type="hidden" id="TEMP_CSRF_TOKEN" name="TEMP_CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" id="isManagePayeeModified" name="isManagePayeeModified" />
<div class="rightPaneWrapper">
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="ltrow2_color" align="center">
						<s:form id="newWiresTransferFormID" namespace="/pages/jsp/wires" validate="false" action="%{#attr.ValidateAction}" method="post" name="WireNew" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<input id="isRecModel" name="isRecModel" value="<ffi:getProperty name="isRecModel"/>" type="hidden"/>
						<input id="recurringId" name="recurringId" value="<ffi:getProperty name="recurringId"/>" type="hidden"/>
						<input id="ID" name="ID" value="<ffi:getProperty name="ID"/>" type="hidden"/>
						<input id="transType" name="transType" value="<ffi:getProperty name="transType"/>" type="hidden"/>
						<input type="hidden" name="isDRAWDOWNKWIRE" value="DRAWDOWNKWIRE" id="isDRAWDOWNKWIRE"/>
						<input type="hidden" name="DontInitialize" value="true"/>
						<input type="hidden" name="BatchEntry" value="<ffi:getProperty name='isBatchEntry'/>"/>
						<input type="hidden" id="returnPage" value="<ffi:getProperty name='returnPage'/>"/>
						<input type="hidden" name ="bankLookupRaturnPage" id="bankLookupRaturnPage" value="<ffi:getProperty name='returnPage'/>"/>
						<input type="hidden" id="setPayeeAction" value="<ffi:getProperty name='setPayeeAction'/>"/>
						 <input type="hidden" name="<ffi:getProperty name="wireTask"/>.WirePayeeID" id="payeeID">
						 <input type="hidden" name="<ffi:getProperty name="wireTask"/>.FromAccountID" id="accountID">
                    			<input type="hidden" name="delIntBank" value=""/>
                    			<input type="hidden" name="selectedCountry" value=""/>
								<input type="hidden" name="<ffi:getProperty name="wireTask"/>.Type" value="<ffi:getProperty name="${wireTask}" property="Type"/>">
								<input type="hidden" name="<ffi:getProperty name="wireTask"/>.WireType" value="<ffi:getProperty name="${wireTask}" property="WireType"/>">
								<input type="hidden" name="<ffi:getProperty name="wireTask"/>.WireDestination" value="<ffi:getProperty name="${wireTask}" property="WireDestination"/>">
								<input type="hidden" name="WireTransferPayee.PayeeDestination" value="<%= WireDefines.PAYEE_TYPE_DRAWDOWN %>">
								<input type="hidden" name="WireTransferPayee.Country" value="UNITED STATES">
								<input type="hidden" name="WireTransferPayee.DestinationBank.Country" value="UNITED STATES">
								<input type="hidden" name="<ffi:getProperty name="wireTask"/>.WireCreditInfo.Country" value="<ffi:getProperty name="${wireTask}" property="WireCreditInfo.Country"/>">
								<input type="hidden" name="disableRec" value="<ffi:getProperty name="disableRec" />"/>
								<input type="hidden" name="disableBat" value="<ffi:getProperty name="disableBat" />"/>
								<input type="hidden" name="disableDates" value="<ffi:getProperty name="disableDates" />"/>
								<input type="hidden" name="isWireBatch" value="<ffi:getProperty name="isWireBatch" />"/>
								<input type="hidden" name="wireBatchUrl" value="<ffi:getProperty name="wireBatchUrl" />"/>
								<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals"> 
									<s:if test="%{!isTemplateModify}">
										<s:hidden id="accountAction"  value="addWireTemplateAction_setWireCreditInfo.action"></s:hidden>
									</s:if>
									<s:else>
										<s:hidden id="accountAction"  value="modifyWireTemplateAction_setWireCreditInfo.action"></s:hidden>
									</s:else>
								</ffi:cinclude>
								<ffi:cinclude value1="${templateTask}" value2="" operator="equals"> 
									<s:if test="%{!isModify}">
										<s:hidden id="accountAction"  value="addWireTransferAction_setWireCreditInfo.action"></s:hidden>
									</s:if>
									<s:else>
										<s:hidden id="accountAction"  value="modifyWireTransferAction_setWireCreditInfo.action"></s:hidden>
									</s:else>
								</ffi:cinclude>
    <%-- Display task error if one exists --%>
    <ffi:cinclude value1="${SetWireTransferFromAccount}" value2="" operator="notEquals">
        <% request.setAttribute("ErrorTask", session.getAttribute("SetWireTransferFromAccount")); %>
        <%-- <ffi:include page="${PathExt}inc/page-error.jsp"/> --%>
    </ffi:cinclude>
    <ffi:cinclude value1="${SetWireTransferFromAccount.Error}" value2="0">
        <ffi:cinclude value1="${wireTask}" value2="AddWireTransfer">
            <% request.setAttribute("ErrorTask", session.getAttribute("AddWireTransfer")); %>
            <%-- <ffi:include page="${PathExt}inc/page-error.jsp"/> --%>
        </ffi:cinclude>
        <ffi:cinclude value1="${wireTask}" value2="ModifyWireTransfer">
            <% request.setAttribute("ErrorTask", session.getAttribute("ModifyWireTransfer")); %>
            <%-- <ffi:include page="${PathExt}inc/page-error.jsp"/> --%>
        </ffi:cinclude>
        <ffi:cinclude value1="${wireTask}" value2="WireTransfer">
		 <% request.setAttribute("ErrorTask", session.getAttribute("WireTransferTask")); %>
		 <%-- <ffi:include page="${PathExt}inc/page-error.jsp"/> --%>
    	</ffi:cinclude>
    </ffi:cinclude>    

    
	 <%    String wireSource = "";    %>
	<ffi:getProperty name="${wireTask}" property="wireSource" assignTo="wireSource"/>
	<ffi:cinclude value1="${wireSource}" value2="TEMPLATE" operator="equals">
		<div class="paneWrapper"  role="form" aria-labelledby="templateDetailsHeader">
			<div  class="header"><h2 id="templateDetailsHeader">Template Details</h2></div>
          	<div class="paneInnerWrapper">
           		<div class="paneContentWrapper">
	                <%-- ------ TEMPLATE (READ-ONLY) WireName, NickName, TemplateID, DisplayWireLimit, & WireScope ------ --%>
					<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_template_info.jsp"/>
				</div>
	        </div>
	    </div>
		<div class="marginTop10"></div>
	</ffi:cinclude>
	
	
	<div class="paneWrapper" role="form" aria-labelledby="creditAccountHeader">
          	<div class="paneInnerWrapper">
          		<div class="header"><h2 id="creditAccountHeader"><%= LABEL_CREDIT_ACCOUNT %></h2></div>
          		<div class="paneContentWrapper">	
          			<ffi:getProperty name="${wireTask}" property="fromAccountNumberDisplayText"/>
					<sj:a id="expandCreditAccount" 
						onClick="openAccountDetail()"
						title="Show Account Details"
						cssStyle="display:none"
						cssClass="nameSubTitle">
						<span class="sapUiIconCls icon-positive"></span>
					</sj:a>
					<%-- <span class="sapUiIconCls icon-drill-down hidden" id="expandCreditAccount" onclick="openAccountDetail();" title="Show Account Details"></span> --%>
					<%-- <span class="sapUiIconCls icon-drill-up hidden" id="collapseCreditAccount" onclick="openAccountDetail();" title="Hide Account Details"></span> --%>
					<sj:a id="collapseCreditAccount" 
						onClick="openAccountDetail()"
						title="Hide Account Details"
						cssStyle="display:none"
						cssClass="nameSubTitle">
						<span class="sapUiIconCls icon-negative"></span>
					</sj:a>
					<%-- <% String curAcct = ""; String thisAcct = ""; %>
					<ffi:getProperty name="${wireTask}" property="FromAccountID" assignTo="curAcct"/>
					<% if (curAcct == null) curAcct = ""; %>
					<ffi:setProperty name="selectedAcct"  value="<%=curAcct%>"/>
					<% if(curAcct != null && curAcct.trim().length()!=0) {%>
						<ffi:setProperty name="disableWireBeneficiaryEdit" value="disabled"/>
					<%}%> --%>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="hidden tableData" id="managedCreditAccount">
									<tr>
										<td valign="top">
	<%-- ------ BENEFICIARY CONTACT INFO ------ --%>
											<table width="100%" cellpadding="3" cellspacing="0" border="0" class="tableData">
												<tr>
													<td class="sectionhead nameTitle" style="cursor:auto; padding:10px 1px 10px" colspan="4"><!--L10NStart--><h2>Beneficiary Info</h2><!--L10NEnd--></td>
												</tr>
												<tr>
													<td colspan="2" width="50%"><label for="payeeName"><%= LABEL_BENEFICIARY_NAME %></label></td>
													<td width="50%" colspan="2"><label for="address1"><%= LABEL_ADDRESS_1 %></label></td>
												</tr>
												<tr>
													<td colspan="2"><input id="payeeName" name="<ffi:getProperty  name="wireTask"/>.WireCreditInfo.PayeeName" value="<ffi:getProperty name="${wireTask}" property="WireCreditInfo.PayeeName"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="40" <ffi:getProperty name="disableSemi"/>  <ffi:getProperty name="disableWireBeneficiaryEdit"/>></td>
													<td colspan="2"><input id="address1" name="<ffi:getProperty  name="wireTask"/>.WireCreditInfo.Street" value="<ffi:getProperty name="${wireTask}" property="WireCreditInfo.Street"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="40" <ffi:getProperty name="disableSemi"/>  <ffi:getProperty name="disableWireBeneficiaryEdit"/>></td>
												</tr>
												
												<tr>
													<td colspan="2"><label for="address2"><%= LABEL_ADDRESS_2 %></label></td>
													<td><label for=""><label for="city"><%= LABEL_CITY %></label></td>
													<td><label for=""><label for="state"><%= LABEL_STATE %></label></td>
												</tr>
												
												<tr>
													<td colspan="2"><input  id="address2" name="<ffi:getProperty name="wireTask"/>.WireCreditInfo.Street2" value="<ffi:getProperty name="${wireTask}" property="WireCreditInfo.Street2"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="40"  <ffi:getProperty name="disableSemi"/>  <ffi:getProperty name="disableWireBeneficiaryEdit"/>></td>
													<td><input  id="city" name="<ffi:getProperty name="wireTask"/>.WireCreditInfo.City" value="<ffi:getProperty name="${wireTask}" property="WireCreditInfo.City"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="20"   <ffi:getProperty name="disableSemi"/> <ffi:getProperty name="disableWireBeneficiaryEdit"/>></td>
													<td><input  id="state" name="<ffi:getProperty name="wireTask"/>.WireCreditInfo.State" value="<ffi:getProperty name="${wireTask}" property="WireCreditInfo.State"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="20"   <ffi:getProperty name="disableSemi"/> <ffi:getProperty name="disableWireBeneficiaryEdit"/>></td>
												</tr>
												
												<tr>
													<td><label for="zipCode"><%= LABEL_ZIP_CODE %></label></td>
													<td><label for="contactPerson"><%= LABEL_CONTACT_PERSON %></label></td>
													<td colspan="2"></td>
												</tr>
												
												<tr>
													<td><input  id="zipCode" name="<ffi:getProperty name="wireTask"/>.WireCreditInfo.ZipCode" value="<ffi:getProperty name="${wireTask}" property="WireCreditInfo.ZipCode"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="11" maxlength="10" <ffi:getProperty name="disableSemi"/>  <ffi:getProperty name="disableWireBeneficiaryEdit"/>></td>
													<td><input  id="contactPerson" name="<ffi:getProperty name="wireTask"/>.WireCreditInfo.ContactName" value="<ffi:getProperty name="${wireTask}" property="WireCreditInfo.ContactName"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="20" style="width: 200;" <ffi:getProperty name="disableSemi"/> <ffi:getProperty name="disableWireBeneficiaryEdit"/>></td>
													<td colspan="2"></td>
												</tr>
											</table>
											<% String wciid = ""; %>
											<ffi:getProperty name="${wireTask}" property="WireCreditInfo.ID" assignTo="wciid"/>
											<% if (wciid != null && wciid.length() > 0) { %>
												<input type="Hidden" name="<ffi:getProperty name="wireTask"/>.WireCreditInfo.Action" value="mod">
											<% } %>
										</td>
										</tr>
										<tr><td valign="top"><hr class="thingrayline lineBreak hidden" /></td></tr>
										<tr>
										<td width="100%" valign="top">
	<%-- ------ BENEFICIARY BANK INFO ------ --%>
											<table width="100%" cellpadding="3" cellspacing="0" border="0" class="tableData">
												<tr>
													<td colspan="2" class="sectionhead nameTitle" style="cursor:auto; padding:10px 1px 10px"><!--L10NStart--><h2>Beneficiary Bank Info</h2><!--L10NEnd--></td>
												</tr>
													
												<tr>
													<td width="50%"><%= LABEL_BANK_NAME %>: <ffi:getProperty name="${wireTask}" property="WireCreditInfo.DestinationBank.BankName"/></td>
													<td width="50%"><%= LABEL_BANK_ADDRESS_1 %>: <ffi:getProperty name="${wireTask}" property="WireCreditInfo.DestinationBank.Street"/></td>
												</tr>
												<tr>
													<td><%= LABEL_BANK_ADDRESS_2 %>: <ffi:getProperty name="${wireTask}" property="WireCreditInfo.DestinationBank.Street2"/></td>
													<td><%= LABEL_BANK_CITY %>: <ffi:getProperty name="${wireTask}" property="WireCreditInfo.DestinationBank.City"/> </td>
												</tr>
												
												<tr>
													<td><%= LABEL_BANK_STATE %>: <ffi:getProperty name="${wireTask}" property="WireCreditInfo.DestinationBank.State"/></td>
													<td><%= LABEL_BANK_ZIP_CODE %>: <ffi:getProperty name="${wireTask}" property="WireCreditInfo.DestinationBank.ZipCode"/></td>
												</tr>
												<tr>
													<td><%= LABEL_FED_ABA %>: <ffi:getProperty name="${wireTask}" property="WireCreditInfo.DestinationBank.RoutingFedWire"/></td>
													<td></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
				</div>
			</div>
	</div>

<div class="paneWrapper marginTop20" role="form" aria-labelledby="debitAccountInformationHeader">
  	<div class="paneInnerWrapper">
  		<div class="header"><h2 id="debitAccountInformationHeader"><s:text name="jsp.default_148" /></h2></div>
  		<div class="paneContentWrapper">
			<%-- ------ BENEFICIARY PICK LIST: WirePayeeID ------ --%>
			<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_picker.jsp"/>
	<table width="100%" cellpadding="0" cellspacing="0" border="0" class="hidden" id= "drawdownHiddenJsp">
	<tr>
		<td valign="top">
			<%-- ------ PAYEE FIELDS: PayeeName, Street, Street2, City, State, ZipCode, Country, ContactName, PayeeScope, AccountNum, AccountType ------ --%>
			<ffi:setProperty name="WireTransferPayee" property="PayeeDestination" value="<%= WireDefines.PAYEE_TYPE_DRAWDOWN %>"/>
			<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_fields.jsp"/>
		</td>
		</tr>
		<tr><td valign="top"><hr class="hidden thingrayline lineBreakPayee" /></td></tr>
		<tr>
					<td valign="top">
			<%-- ------ BENEFICIARY BANK FIELDS (WireTransferPayee.DestinationBank):  BankName, Street, Street2, City, State, ZipCode, Country, RoutingFedWire, RoutingSwift, RoutingChips, RoutingOther ------ --%>
			<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_bank_fields.jsp"/>
		</td>
	</tr>
</table>
		</div>
	</div>
</div>
<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
	<%-- ------ TEMPLATE FIELDS: WireName, NickName, WireCategory, & WireScope ------ --%>
	<s:include value="%{#session.PagesPath}/wires/inc/wire_template_fields.jsp"/>
</ffi:cinclude>
	
								<%--
								<input type="hidden" name="<ffi:getProperty name="wireTask"/>.<ffi:getProperty name="wireCurrencyField"/>" value="USD">
								--%>
								

	<div class="paneWrapper marginTop10" role="form" aria-labelledby="paymentDetailsHeader">
	  	<div class="paneInnerWrapper">
	  		<div class="header"><h2 id="paymentDetailsHeader">Payment Details</h2></div>
	  		<div class="paneContentWrapper">
				<%-- ------ DOMESTIC PAYMENT & REPEATING FIELDS:  Amount/OrigAmount, DueDate, Frequency, NumberTransfers ------ --%>
				<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payment_fields.jsp"/>
			</div>
		</div>
	</div>
	<div class="marginTop10"></div>	
	<%-- ------ COMMENT FIELDS:  Comment, OrigToBeneficiaryInfo1 - OrigToBeneficiaryInfo4, BankToBankInfo1 - BankToBankInfo6 ------ --%>
	<div align="left"><s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_comment_fields.jsp"/></div>
	<div class="required">* <!--L10NStart-->indicates a required field<!--L10NEnd--></div>
	<ffi:setProperty name="checkAmount_acctCollectionName" value="WiresAccounts"/>
	<%-- include javascript for checking the amount field. Note: Make sure you include this file after the collection has been populated.--%>
	<s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />
	<div class="btn-row">					<%--
								<input type="button" class="submitbutton" value="CANCEL" onclick="document.location='<ffi:getProperty name="wireBackURL"/>'">
								--%>
								<s:if test="%{!isWireBatch}">
									<ffi:cinclude value1="${wireSource}" value2="TEMPLATE">
										<sj:a id="cancelFormButtonOnInput" 
											button="true" summaryDivId="summary" buttonType="cancel" 
											onClickTopics="showSummary,cancelWireTransferForm"
											>
											<s:text name="jsp.default_82"/>
										</sj:a>		
									</ffi:cinclude>
									<ffi:cinclude value1="${wireSource}" value2="TEMPLATE" operator="notEquals">
										<sj:a id="cancelFormButtonOnInput" 
											button="true" summaryDivId="summary" buttonType="cancel" 
											onClickTopics="showSummary,cancelWireTransferForm"
											>
											<s:text name="jsp.default_82"/>
										</sj:a>									
									</ffi:cinclude>										
								</s:if>
								
								<script>
									ns.wire.cancelWireURLFromWireBatch = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/${wireBackURL}"/>' ;
								</script>
								<s:if test="%{isWireBatch}">
	   								<s:hidden id="backToBatchURLForJS" value="%{#session.wireBackURL}"></s:hidden>
	   								<sj:a id="cancelAddWireOnBatchFormID" 
										button="true" 
										onClickTopics="cancelAddWireOnBatchFormClickTopics"
									>
										<s:text name="jsp.default_82"/>
									</sj:a>
								</s:if>
								<s:if test="%{#request.templateTask ==''}">
									<s:set name="WireButtonLabel" value="%{getText('jsp.default_395')}"/>
								</s:if>	
								<s:else>
									<s:set name="WireButtonLabel" value="%{getText('jsp.default_366')}"/>
								</s:else>
								<%-- Start: Dual approval processing --%>

								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">	
									<s:if test="%{#request.PayeePendingInDA}"> 
										<input id="verifyDAWiresTransferNoSubmit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-disabled" type="Button"  value='<s:property value="%{#WireButtonLabel}" />' >
									</s:if>	
									<s:else>
										<sj:a
												id="verifyDAWiresTransferSubmit"
												formIds="newWiresTransferFormID"
				                                targets="verifyDiv"
				                                button="true"
				                                validate="true"
				                                validateFunction="customValidation"
				                                onBeforeTopics="beforeVerifyWireTransferForm"
				                                onCompleteTopics="verifyWireTransferFormComplete"
												onErrorTopics="errorVerifyWireTransferForm"
				                                onSuccessTopics="successVerifyWireTransferForm"
												onClick="SelectAmount()"
						                        ><s:property value="%{#WireButtonLabel}" />
						            	</sj:a>
									</s:else>																	
								</ffi:cinclude>
								
								<%-- End: Dual approval processing--%>
								
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">								
	
									 <sj:a 
												id="verifyWiresTransferSubmit"
												formIds="newWiresTransferFormID"
				                                targets="verifyDiv" 
				                                button="true" 
				                                validate="true" 
				                                validateFunction="customValidation"
				                                onBeforeTopics="beforeVerifyWireTransferForm"
				                                onCompleteTopics="verifyWireTransferFormComplete"
												onErrorTopics="errorVerifyWireTransferForm"
				                                onSuccessTopics="successVerifyWireTransferForm"
												onclick="SelectAmount()"
						                        ><s:property value="%{#WireButtonLabel}" />
						            </sj:a>
								</ffi:cinclude>
			</div>		
					</s:form>		
					</td>
				</tr>
			</table>
</ffi:cinclude>
</div>
		</div>

<ffi:removeProperty name="DontInitialize"/>
<ffi:removeProperty name="disableWireBeneficiaryEdit"/>

<%-- Start: Dual approval processing --%>
<ffi:removeProperty name="IS_DA_TRANSFER"/>
<%-- End: Dual approval processing --%>
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
<script>
	$(document).ready(function(){
		$("input[name='isBeneficiary']").each( function(){$(this).remove();} );
		var isRecModel = '<ffi:getProperty name="isRecModel" />';
		if(isRecModel == 'true') {
			enableRepeating(<ffi:getProperty name="${wireTask}" property="FrequencyValue"/>);
		}
		if( ($("#selectWirePayeeID").val()!=null && $("#selectWirePayeeID").val()!="Create New Beneficiary" )
				||
				/* new check is on the payee id that is held for worst case scenarion in a different hidden input box */
				($("#retainedWirePayeeID").val()!='' &&
						$("#retainedWirePayeeID").val()!=null && $("#retainedWirePayeeID").val()!="Create New Beneficiary" )
				){
			$('#payeeID').val($("#retainedWirePayeeID").val());
			$('#expand').toggle();
			$(".selecteBeneficiaryMessage").toggle();
		}
		
		var creditAccount = $('#selectPayeeOfDrawDownWireID').val();
		if($.trim(creditAccount)!='' && $.trim(creditAccount)!=null){
			$('#expandCreditAccount').attr('style','display:inline-block');
		}
		
		// check if retained account is available.if yes show the expand button
		if($("#retainedAccountID").val()!='' && $.trim($('#accountID').val())==''){
			$('#accountID').val($("#retainedAccountID").val());
			$('#expandCreditAccount').attr('style','display:inline-block');
		}
	});
	
	$( "#disbaledExpandBtn" ).button({ disabled: true });
	//$("#wireDebitAccountLabel").css({"visibility":"hidden", "display":"none"});
	//$("#wireDebitAccountLabel").next().css({"visibility":"hidden", "display":"none"});
	//$("#wireDebitAccountLabel").parent().next().find("td:eq(0)").css({"visibility":"hidden", "display":"none"});
	//$("#wireDebitAccountLabel").parent().find("td:eq(1)").css({"visibility":"hidden", "display":"none"});
	
	function openBeneficiaryDetail(){
		$('#drawdownHiddenJsp').slideToggle();
		$('.lineBreakPayee').slideToggle();
		$('#collapse').toggle();
		$('#expand').toggle();
	}
	
	function loadBeneficiaryForm(){
		var optionsStr = $("<option value='Create New Debit Account'></option>");
		var firstOption = $('#selectWirePayeeID').find("option:first");
		$(optionsStr).insertBefore(firstOption);
		$('#selectWirePayeeID').find("option[value='Create New Debit Account']").attr("selected","selected");
		setPayee('Create New Debit Account');
	}

	function openAccountDetail(){
		$('#expandCreditAccount').toggle();
		$('#collapseCreditAccount').toggle();
		$('#managedCreditAccount').slideToggle();
		
	}
	
	
</script>
</ffi:cinclude>
