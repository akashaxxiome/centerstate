<%--
This page allows you to save a wire transfer as a template.

Pages that request this page
----------------------------
wiretransferconfirm.jsp
	SAVE AS TEMPLATE button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
BACK requests wiretransferconfirm.jsp
SAVE TEMPLATE requests one of the following:
	autoentitle-confirm.jsp
	wiresaveastemplatesave.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_template_fields.jsp
	Template fields, used when creating wire templates
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireTransfer"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@page import="com.ffusion.tasks.wiretransfers.ModifyWireTransfer"%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/wire_labels.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<span id="wireTransferResultMessage" class="hidden">
<ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-3" parm0="${wireType}"/></span>
<ffi:help id="payments_wiresaveastemplate" className="moduleHelpClass"/>

               <div align="center">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                        <td class="ltrow2_color" align="center">
						
							<s:form id="saveAsTemplateFormID" namespace="/pages/jsp/wires" validate="false" action="%{#attr.SubmitAction}" method="post" name="saveAsTemplateFormID" theme="simple">
								<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<s:hidden name="url_action" value="%{#attr.SubmitAction}" id="url_template_action"></s:hidden>
								<%-- Code ported from wire_template_fields.jsp starts --%>
								<%
									String task = "WireTransfer";
									%>
									
									<%-- ------ TEMPLATE INFO ------ --%>
									<input type="hidden" name="<%= task %>.WireLimitValue" value="<ffi:getProperty name="<%= task %>" property="WireLimit"/>">
									<input type="hidden" name="<%= task %>.AmtCurrencyType" id="amtCurrencyTypeForTemplate">
									<%-- <input type="hidden" name="<%= task %>.AmtCurrencyType" value="<ffi:getProperty name="<%= task %>" property="AmtCurrencyType"/>"> --%>
									<%
									String curCat = "";
									String curScope = "";
									%>
									<ffi:getProperty name="<%= task %>" property="WireCategory" assignTo="curCat"/>
									<ffi:getProperty name="<%= task %>" property="WireScope" assignTo="curScope"/>
									<%
									if (curCat == null) curCat = "";
									if (curScope == null) curScope = "";
									%>
									<table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableData">
										<!-- <tr>
											<td colspan="3" class="sectionhead tbrd_b">L10NStartTemplate InformationL10NEnd</td>
										</tr>
										<tr>
											<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="5" alt=""></td>
										</tr>
										<tr> -->
									<%-- ------ TEMPLATE NAMES ------ --%>
											<tr>
												<td id="wireTemplateNameLabel"><span class="sectionsubhead"><%= LABEL_TEMPLATE_NAME %></span><span class="required">*</span></td>
											</tr>
											<tr>
												<td><input id="wireTemplateName" type="text" name="<%= task %>.WireName" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="WireName"/>" maxlength="32" style="width:90%"></td>
											</tr>
											<tr><td><span id="<ffi:getProperty name="wireTask"/>.WireNameError"></span></td></tr>
											<tr>
												<td id="wireTemplateNickNameLabel" class="sectionsubhead"><%= LABEL_TEMPLATE_NICKNAME %></td>
											</tr>
											<tr>
												<td ><input id="wireTemplateNickName" type="text" name="<%= task %>.NickName" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="NickName"/>" maxlength="32" style="width:90%"></td>
											</tr>
											<tr><td></td></tr>
									<%-- ------ TEMPLATE CATEGORY AND SCOPE ------ --%>
											<tr>
												<td id="wireTemplateCategoryLabel"><span class="sectionsubhead"><%= LABEL_TEMPLATE_CATEGORY %></span><span class="required">*</span></td>
											</tr>
											<tr>
												<td><select id="selectWireCategoryID" name="<%= task %>.WireCategory" class="txtbox">
													<option value="<%= WireDefines.WIRE_CATEGORY_FREEFORM %>" <%= curCat.equals(WireDefines.WIRE_CATEGORY_FREEFORM) ? "selected" : "" %>><!--L10NStart-->Free-form<!--L10NEnd--></option>
													<option value="<%= WireDefines.WIRE_CATEGORY_REPETITIVE %>" <%= curCat.equals(WireDefines.WIRE_CATEGORY_REPETITIVE) ? "selected" : "" %>><!--L10NStart-->Repetitive<!--L10NEnd--></option>
													<option value="<%= WireDefines.WIRE_CATEGORY_SEMIREPETITIVE %>" <%= curCat.equals(WireDefines.WIRE_CATEGORY_SEMIREPETITIVE) ? "selected" : "" %>><!--L10NStart-->Semi-Repetitive<!--L10NEnd--></option>
													</select>
												</td>
											</tr>
											<tr><td></td></tr>
											<s:if test="%{!IsConsumerUser}">
											<tr>
												<td id="wireTemplateScopeLabel" ><span class="sectionsubhead"><%= LABEL_TEMPLATE_SCOPE %></span><span class="required">*</span></td>
											</tr>
											<tr>
												<td>
													<select id="selectWireScopeID" name="<%= task %>.WireScope" class="txtbox">
														<option value="<%= WireDefines.WIRE_SCOPE_BUSINESS %>" <%= curScope.equals(WireDefines.WIRE_SCOPE_BUSINESS) ? "selected" : "" %>><!--L10NStart-->Business (General Use)<!--L10NEnd--></option>
														<option value="<%= WireDefines.WIRE_SCOPE_USER %>" <%= curScope.equals(WireDefines.WIRE_SCOPE_USER) ? "selected" : "" %>><!--L10NStart-->User (My Use)<!--L10NEnd--></option>
													</select>
												</td>
											</tr>
											</s:if>
											<s:else>
													<input id="selectWireScopeID" name="<%= task %>.WireScope" value="<%= WireDefines.WIRE_SCOPE_USER %>" type="hidden"  maxlength="128" size="20">													
											</s:else>
									</table>
									<span class="columndata">&nbsp;<br></span>
									<%-- Code ported from wire_template_fields.jsp ends --%>
								<%-- <sj:a 
									id="cancelSaveAsTemplateLink" 
									button="true" 
									onClickTopics="closeDialog" 
									title="Cancel"
									><s:text name="jsp.default_82"/>
								</sj:a> --%>
								<div class="btn-row">
									<sj:a
										id="saveAsTemplateLink" 
										formIds="saveAsTemplateFormID"
										targets="wiresAutoEntitleConfirmDiv"
										button="true"	
										validate="true"		
										validateFunction="customValidation"
										onCompleteTopics="saveAsTemplateComplete"
																																			
										title="Save Template"													
										><s:text name="jsp.default_366" />
									</sj:a>				
								</div>
							</s:form>
						
						
                                        </td>
                                </tr>
                        </table>

                </div>
<ffi:setProperty name="wireTask" value="${oldWireTask}"/>
<ffi:removeProperty name="oldWireTask"/>
<ffi:removeProperty name="GetCumulativeSettings"/>
<script>
$(function(){
	$("#selectWireCategoryID").selectmenu({width:'92%'});;
	<s:if test="%{!IsConsumerUser}">
		$("#selectWireScopeID").selectmenu({width:'92%'});;
	</s:if>
});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var amtCurrencyType = $('#amtCurrencyType').val();
		$('#amtCurrencyTypeForTemplate').val(amtCurrencyType);
	});

</script>