<%--
This is a common add/edit wire beneficiary confirm page used by all beneficiary
types

Pages that request this page
----------------------------
wireaddpayee.jsp
	SAVE BENEFICIARY button
wireaddpayeebook.jsp
	SAVE BENEFICIARY button
wireaddpayeedrawdown.jsp
	SAVE BENEFICIARY button

Pages this page requests
------------------------
CANCEL requests one of the following:
	wirepayees.jsp
	wiretransfernew.jsp
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp
BACK requests one of the following:
	wireaddpayee.jsp
	wireaddpayeebook.jsp
	wireaddpayeedrawdown.jsp
CONFIRM/SAVE BENEFICIARY requests wirepayeeaddsave.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ include file="../common/wire_labels.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirepayeeaddconfirm" className="moduleHelpClass"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">


<%
	String task = "";
	String confirm_action = "";
%>
<ffi:getProperty name="payeeTask" assignTo="task"/>
<%
	if (task == null)
		task = "AddWireTransferPayee";
%>
<ffi:getProperty name="confirmAction" assignTo="confirm_action"/>
<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:cinclude value1="<%= task %>" value2="AddWireTransferPayee">
		<ffi:object id="AddWireTransferPayeeDA" name="com.ffusion.tasks.dualapproval.wiretransfers.AddWireTransferPayeeDA" />
		<ffi:setProperty name="AddWireTransferPayeeDA" property="CurrentPayeeSessionName" value="<%=task%>"/>
		<%--
		<ffi:process name="AddWireTransferPayeeDA" />
		<ffi:removeProperty name="AddWireTransferPayeeDA"/>
		<ffi:setProperty name="FFIAddWireTransferPayeeDA" value="${AddWireTransferPayeeDA}"/>
		--%>		
	</ffi:cinclude>
	<ffi:cinclude value1="<%= task %>" value2="EditWireTransferPayee">
		<ffi:object id="EditWireTransferPayeeDA" name="com.ffusion.tasks.dualapproval.wiretransfers.EditWireTransferPayeeDA" />
		<ffi:setProperty name="EditWireTransferPayeeDA" property="CurrentPayeeSessionName" value="<%=task%>"/>
		<%--
		<ffi:process name="EditWireTransferPayeeDA" />
		<ffi:removeProperty name="EditWireTransferPayeeDA"/>
		<ffi:setProperty name="FFIEditWireTransferPayeeDA" value="${EditWireTransferPayeeDA}"/>
		--%>		
	</ffi:cinclude>	
</ffi:cinclude>

<%
	String type = "";
	String REGULAR = WireDefines.PAYEE_TYPE_REGULAR;
%>
<%
	String intBankSize = "";
%>
<ffi:getProperty name="${payeeTask}" property="IntermediaryBanks.Size" assignTo="intBankSize"/>
<%
	if (intBankSize == null) {
		intBankSize = "0";
	}
%>
<ffi:getProperty name="${payeeTask}" property="PayeeDestination" assignTo="type"/>
<%
	if (type == null)
		type = REGULAR;
	if (type.equals(DRAWDOWN)) {
%>
	<ffi:setL10NProperty name='PageHeading' value='Drawdown Wire Account Confirm'/>
<%
	}
	if (type.equals(BOOK)) {
%>
	<ffi:setL10NProperty name='PageHeading' value='Book Wire Beneficiary Confirm'/>
<%
	}
%>
<ffi:setProperty name="confirm_action" value="confirmAction"/>

<ffi:object id="LocalizeCountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="LocalizeCountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="LocalizeCountryResource" />
<ffi:object id="GetCodeForBankLookupCountryName" name="com.ffusion.tasks.util.GetCodeForBankLookupCountryName" scope="session"/>
<div align="center" id="wireBeneficiesVerificationFormID">
<div align="center">
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="equals">
		<span id="wireBeneficiaryResultMessage" style="display:none"><s:text name="jsp.wire.payeeadd.da.success"/></span>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL%>" operator="notEquals">
		<span id="wireBeneficiaryResultMessage" style="display:none"><s:text name="jsp.wire.payeeadd.success"/></span>
	</ffi:cinclude>			
</div>
<div align="center" class="warningMessage" style="margin:5px 0;">
	<%-- Manual Entry warning message --%>
	<ffi:cinclude value1="<ffi:getProperty name='ManualEntryWarning'/>" value2="" operator="notEquals">
			<span class="sectionsubhead" colspan="3" style="color:red" align="left">
			<!--L10NStart--><ffi:getProperty name='ManualEntryWarning'/><!--L10NEnd-->
			</span>
	<ffi:removeProperty name='ManualEntryWarning'/>
	</ffi:cinclude>
</div>
<div class="leftPaneWrapper"><div class="leftPaneInnerWrapper">
	<div class="header">Beneficiary Summary</div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyWireBeneficiaryLabel"><%=type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME%>:</span>
				<span class="inlineSection floatleft labelValue" id="verifyWireBeneficiaryValue"><ffi:getProperty name="${payeeTask}" property="Name"/></span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><%=LABEL_BENEFICIARY_TYPE%>:</span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="${payeeTask}" property="PayeeDestination"/></span>
		</div>
	</div>
</div></div>
<div class="confirmPageDetails">
<div class="blockWrapper">
	<div  class="blockHead"><%=type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Info<!--L10NEnd-->"%></div>
<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeAccountNoLabel" class="sectionsubhead sectionLabel"><%=LABEL_ACCOUNT_NUMBER%>: </span>
				<span id="verifyPayeeAccountNoValue"class="columndata"><ffi:getProperty name="${payeeTask}" property="AccountNum"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeAccountTypeLabel" class="sectionsubhead sectionLabel"><%=LABEL_ACCOUNT_TYPE%>: </span>
				<span id="verifyPayeeAccountTypeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="AccountType"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeNameLabel" class="sectionsubhead sectionLabel"><%=type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME%>:</span>
				<span id="verifyPayeeNameValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="Name"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeNickNameLabel" class="sectionsubhead sectionLabel"><%=LABEL_NICKNAME%>: </span>
				<span id="verifyPayeeNickNameValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="NickName"/></span>
			</div>
		</div>
		
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeAddress1Label" class="sectionsubhead sectionLabel"><%=LABEL_ADDRESS_1%>: </span>
				<span id="verifyPayeeAddress1Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="Street"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeAddress2Label" class="sectionsubhead sectionLabel"><%=LABEL_ADDRESS_2%>: </span>
				<span id="verifyPayeeAddress2Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="Street2"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeCityLabel" class="sectionsubhead sectionLabel"><%=LABEL_CITY%>: </span>
				<span id="verifyPayeeCityValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="City"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeStateLabel" class="sectionsubhead sectionLabel"><%=type.equals(REGULAR) ? LABEL_STATE_PROVINCE : LABEL_STATE%>: </span>
				<span id="verifyPayeeStateValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="State"/></span>
			</div>
		</div>
		
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeZipCodeLabel" class="sectionsubhead sectionLabel"><%=type.equals(REGULAR) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE%>: </span>
				<span id="verifyPayeeZipCodeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="ZipCode"/></span>
			</div>
				<% if (type.equals(REGULAR)) { %>
			<div class="inlineBlock">
				<span id="verifyPayeeCountryLabel" class="sectionsubhead sectionLabel"><%=LABEL_COUNTRY%>: </span>
				<span id="verifyPayeeCountryValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="CountryDisplayName"/></span>
			</div>
			<% } %>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyPayeeContactLabel" class="sectionsubhead sectionLabel"><%=LABEL_CONTACT_PERSON%>: </span>
				<span id="verifyPayeeContactValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="Contact"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyPayeeTypeLabel" class="sectionsubhead sectionLabel"><%=LABEL_BENEFICIARY_TYPE%>: </span>
				<span id="verifyPayeeTypeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="PayeeDestination"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span id="verifyPayeeScopeLabel" class="sectionsubhead sectionLabel"><%=LABEL_BENEFICIARY_SCOPE%>: </span>
			<span id="verifyPayeeScopeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="PayeeScope"/></span>
		</div>
	</div>
</div>	
<div class="blockWrapper">
	<div  class="blockHead"><%=type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Bank Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Bank Info<!--L10NEnd-->"%> </div>
	<div class="blockContent">
		<div class="blockRow">
			<span id="verifyBankNameLabel" class="sectionsubhead sectionLabel"><%=type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME%></span>
			<span id="verifyBankNameValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.BankName"/></span>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBankAddress1Label" class="sectionsubhead sectionLabel"><%=LABEL_BANK_ADDRESS_1%>: </span>
				<span id="verifyBankAddress1Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.Street"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyBankAddress2Label" class="sectionsubhead sectionLabel"><%=LABEL_BANK_ADDRESS_2%>: </span>
				<span id="verifyBankAddress2Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.Street2"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBankCityLabel" class="sectionsubhead sectionLabel"><%=LABEL_BANK_CITY%>: </span>
				<span id="verifyBankCityValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.City"/></span>
			</div>
			<div class="inlineBlock">
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyBankStateLabel" class="sectionsubhead sectionLabel"><%=type.equals(REGULAR) ? LABEL_STATE_PROVINCE : LABEL_STATE%>: </span>
				<span id="verifyBankStateValue" class="columndata">
					<s:if test="%{WireTransferPayee.DestinationBank.StateDisplayName != null && WireTransferPayee.DestinationBank.StateDisplayName != ''}">
						<ffi:getProperty name="${payeeTask}" property="DestinationBank.StateDisplayName" />
					</s:if>
					<s:else>
						<ffi:getProperty name="${payeeTask}" property="DestinationBank.State" />
					</s:else>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="verifyBankZipCodeLabel" class="sectionsubhead sectionLabel"><%=type.equals(REGULAR) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE%>: </span>
				<span id="verifyBankZipCodeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.ZipCode"/></span>
			</div>
		</div>
		<% if (type.equals(REGULAR)) { %>
			<div class="blockRow">
				<span id="verifyBankCountryLabel" class="sectionsubhead sectionLabel"><%=LABEL_COUNTRY%>: </span>
				<span id="verifyBankCountryValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.CountryDisplayName"/></span>
			</div>
		<% } %>
		<div class="blockRow">
			<span id="verifyFEDLabel" class="sectionsubhead sectionLabel"><%=LABEL_FED_ABA%>: </span>
			<span id="verifyFEDValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingFedWire"/></span>
		</div>
		<% if (type.equals(REGULAR)) { %>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="verifySWIFTLabel" class="sectionsubhead sectionLabel"><%=LABEL_SWIFT%>: </span>
					<span id="verifySWIFTValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingSwift"/></span>
				</div>
				<div class="inlineBlock">
					<span id="verifyCHIPSLabel" class="sectionsubhead sectionLabel"><%=LABEL_CHIPS%>: </span>
					<span id="verifyCHIPSValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingChips"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="verifyNationalLabel" class="sectionsubhead sectionLabel"><%=LABEL_NATIONAL%>: </span>
					<span id="verifyNationalValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingOther"/></span>
				</div>
				<div class="inlineBlock">
					<span id="verifyIBANLabel" class="sectionsubhead sectionLabel"><%=LABEL_IBAN%>: </span>
					<span id="verifyIBANValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.IBAN"/></span>
				</div>
			</div>
			<ffi:cinclude value1="${intBankSize}"  value2="0" operator="notEquals" >
			<div class="blockRow">
				<span id="verifyCorrespondantBankAccountNoLabel" class="sectionsubhead sectionLabel"><%=LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER%>: </span>
				<span id="verifyCorrespondantBankAccountNoValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.CorrespondentBankAccountNumber"/></span>
			</div>
		</ffi:cinclude>
		<% } %>
	</div>
</div>
<%
	int counter = 0;
%>
<%
	if (type.equals(REGULAR)) {
		if (!intBankSize.equals("0")) {
%>
					
<div class="blockWrapper">
<ffi:list collection="${payeeTask}.IntermediaryBanks" items="Bank1">
	<div  class="blockHead"><!--L10NStart-->Intermediary Bank Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%=COLUMN_INTERMEDIARY_BANK%>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%=COLUMN_FED_ABA%>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%=COLUMN_SWIFT%>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%=COLUMN_CHIPS%>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</span>
			</div>
		</div>
		
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><%=COLUMN_NATIONAL%>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%=COLUMN_IBAN%>: </span>
				<span class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</span>
			</div>
		</div>
		
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><%=COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER%>: </span>
			<span class="columndata">
				<%
					if (counter == 0) {
				%>
				<%
					} else {
				%>
				<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
				<%
					}
								counter++;
				%>
			</span>
		</div>
		
	</div>
</ffi:list>
</div>
<%
	}
%>
<%
	}
%>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Comments<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<ffi:getProperty name="${payeeTask}" property="Memo" />
		</div>
	</div>
</div>
<div class="btn-row">
	<s:form id="BeneficiaryNewConfirmFormID" namespace="/pages/jsp/wires" action="%{#attr.confirmAction}"  method="post" theme="simple">
		<%-- <form action="wirepayeeaddsave.jsp" method="post">--%>
      	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<ffi:cinclude value1="${returnPage}" value2="wirepayees.jsp" operator="notEquals">
		<input type="Hidden" name="returnPage" value="<ffi:getProperty name="returnPage"/>">
		</ffi:cinclude>
		<input type="hidden" id="returnPageIDForJS" value="<ffi:getProperty name='returnPage'/>"/>
              			<s:hidden name="isBeneficiary" value="%{#session.isBeneficiary}" id="isBeneficiary"/>
		<s:hidden name="IsPending" value="%{#session.IsPending}" id="IsPending"/>
		<input type="hidden" id="isHaveReturnPageParamIDForJS" value="<ffi:getProperty name='returnPage'/>"/>
		<input type="hidden" id="manageBeneficiary" value="<ffi:getProperty name='manageBeneficiary'/>"/>
		<sj:a id="cancelFormButtonOnVerify" 
			button="true" summaryDivId="summary" buttonType="cancel"
			onClickTopics="showSummary"
			onclick="ns.wire.cancelWireBeneficiaryFormOnClick(),ns.wire.showSummaryWhileManagingPayee('%{manageBeneficiary}')"
			><s:text name="jsp.default_82" />
		</sj:a>
		<sj:a id="backFormButton" 
			button="true" 
			onClickTopics="backToInput"
			><s:text name="jsp.default_57" />
		</sj:a>
		<sj:a id="sendNewBeneficiarySubmit"
			formIds="BeneficiaryNewConfirmFormID"
			targets="confirmDiv" 
			button="true" 
			onBeforeTopics="beforeSendBeneficiaryForm"
			onSuccessTopics="sendBeneficiaryFormSuccess"
			onErrorTopics="errorSendWireTransferForm"			
          ><s:text name="jsp.default_111" />
		</sj:a>
	</s:form>
</div>
</div>
</div>
<ffi:removeProperty name="LocalizeCountryResource" />
<ffi:removeProperty name="GetCodeForBankLookupCountryName" />
			<%--<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata ltrow2_color">
							
					 <table width="715" cellpadding="0" cellspacing="0" border="0">
						
						
						<tr>
							<td width="357" valign="top">
								<table width="355" cellpadding="3" cellspacing="0" border="0" class="tableData">
									<tr>
										<td class="sectionhead" colspan="2">&gt; <%=type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account Info<!--L10NEnd-->"
					: "<!--L10NStart-->Beneficiary Info<!--L10NEnd-->"%></td>
									</tr>
									<tr>
										<td id="verifyPayeeAccountNoLabel" class="sectionsubhead"><%=LABEL_ACCOUNT_NUMBER%></td>
										<td id="verifyPayeeAccountNoValue"class="columndata"><ffi:getProperty name="${payeeTask}" property="AccountNum"/></td>
									</tr>
									<tr>
										<td id="verifyPayeeAccountTypeLabel" class="sectionsubhead"><%=LABEL_ACCOUNT_TYPE%></td>
										<td id="verifyPayeeAccountTypeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="AccountType"/></td>
									</tr>
									<tr>
										<td id="verifyPayeeNameLabel" width="115" class="sectionsubhead"><%=type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_ACCOUNT_NAME : LABEL_BENEFICIARY_NAME%></td>
										<td id="verifyPayeeNameValue" width="228" class="columndata"><ffi:getProperty name="${payeeTask}" property="Name"/></td>
									</tr>
									<tr>
										<td id="verifyPayeeNickNameLabel" class="sectionsubhead"><%=LABEL_NICKNAME%></td>
										<td id="verifyPayeeNickNameValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="NickName"/></td>
									</tr>
									<tr>
										<td id="verifyPayeeAddress1Label" class="sectionsubhead"><%=LABEL_ADDRESS_1%></td>
										<td id="verifyPayeeAddress1Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="Street"/></td>
									</tr>
									<tr>
										<td id="verifyPayeeAddress2Label" class="sectionsubhead"><%=LABEL_ADDRESS_2%></td>
										<td id="verifyPayeeAddress2Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="Street2"/></td>
									</tr>
									<tr>
										<td id="verifyPayeeCityLabel" class="sectionsubhead"><%=LABEL_CITY%></td>
										<td id="verifyPayeeCityValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="City"/></td>
									</tr>
									<tr>
										<td id="verifyPayeeStateLabel" class="sectionsubhead"><%=type.equals(REGULAR) ? LABEL_STATE_PROVINCE : LABEL_STATE%></td>
										<td id="verifyPayeeStateValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="State"/></td>
									</tr>
									<tr>
										<td id="verifyPayeeZipCodeLabel" class="sectionsubhead"><%=type.equals(REGULAR) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE%></td>
										<td id="verifyPayeeZipCodeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="ZipCode"/></td>
									</tr>
									<%
										if (type.equals(REGULAR)) {
									%>
									<tr>
										<td id="verifyPayeeCountryLabel" class="sectionsubhead"><%=LABEL_COUNTRY%></td>
										<td id="verifyPayeeCountryValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="CountryDisplayName"/></td>
									</tr>
									<%
										}
									%>
									<tr>
										<td id="verifyPayeeContactLabel" class="sectionsubhead"><%=LABEL_CONTACT_PERSON%></td>
										<td id="verifyPayeeContactValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="Contact"/></td>
									</tr>
									<tr>
										<td id="verifyPayeeTypeLabel" class="sectionsubhead"><%=LABEL_BENEFICIARY_TYPE%></td>
										<td id="verifyPayeeTypeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="PayeeDestination"/></td>
									</tr>

									<tr>
										<td id="verifyPayeeScopeLabel" class="sectionsubhead"><%=LABEL_BENEFICIARY_SCOPE%></td>
										<td id="verifyPayeeScopeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="PayeeScope"/></td>
									</tr>
								</table>
							</td>
							<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td> --%>
							<%-- <td width="348" valign="top">
								<table width="347" cellpadding="3" cellspacing="0" border="0">
									<tr>
										<td class="sectionhead" colspan="2">&gt; <%=type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Bank Info<!--L10NEnd-->"
					: "<!--L10NStart-->Beneficiary Bank Info<!--L10NEnd-->"%> </td>
									</tr>
									<tr>
										<td id="verifyBankNameLabel" width="115" class="sectionsubhead"><%=type.equals(DRAWDOWN) ? LABEL_DRAWDOWN_BANK_NAME : LABEL_BANK_NAME%></td>
										<td id="verifyBankNameValue" width="220" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.BankName"/></td>
									</tr>
									<tr>
										<td id="verifyBankAddress1Label" class="sectionsubhead"><%=LABEL_BANK_ADDRESS_1%></td>
										<td id="verifyBankAddress1Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.Street"/></td>
									</tr>
									<tr>
										<td id="verifyBankAddress2Label" class="sectionsubhead"><%=LABEL_BANK_ADDRESS_2%></td>
										<td id="verifyBankAddress2Value" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.Street2"/></td>
									</tr>
									<tr>
										<td id="verifyBankCityLabel" class="sectionsubhead"><%=LABEL_BANK_CITY%></td>
										<td id="verifyBankCityValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.City"/></td>
									</tr>
									<tr>
												<td id="verifyBankStateLabel" class="sectionsubhead"><%=type.equals(REGULAR) ? LABEL_STATE_PROVINCE : LABEL_STATE%></td>
												<td id="verifyBankStateValue" class="columndata">
												<s:if test="%{WireTransferPayee.DestinationBank.StateDisplayName != null && WireTransferPayee.DestinationBank.StateDisplayName != ''}">
													<ffi:getProperty name="${payeeTask}" property="DestinationBank.StateDisplayName" />
												</s:if>
												<s:else>
													<ffi:getProperty name="${payeeTask}" property="DestinationBank.State" />
												</s:else>
												</td>
									</tr>
									<tr>
										<td id="verifyBankZipCodeLabel" class="sectionsubhead"><%=type.equals(REGULAR) ? LABEL_ZIP_POSTAL_CODE : LABEL_ZIP_CODE%></td>
										<td id="verifyBankZipCodeValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.ZipCode"/></td>
									</tr>
									<%
										if (type.equals(REGULAR)) {
									%>
									<tr>
										<td id="verifyBankCountryLabel" class="sectionsubhead"><%=LABEL_COUNTRY%></td>
										<td id="verifyBankCountryValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.CountryDisplayName"/></td>
									</tr>
									<%
										}
									%>
									<tr>
										<td id="verifyFEDLabel" class="sectionsubhead"><%=LABEL_FED_ABA%></td>
										<td id="verifyFEDValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingFedWire"/></td>
									</tr>
									<%
										if (type.equals(REGULAR)) {
									%>
									<tr>
										<td id="verifySWIFTLabel" class="sectionsubhead"><%=LABEL_SWIFT%></td>
										<td id="verifySWIFTValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingSwift"/></td>
									</tr>
									<tr>
										<td id="verifyCHIPSLabel" class="sectionsubhead"><%=LABEL_CHIPS%></td>
										<td id="verifyCHIPSValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingChips"/></td>
									</tr>
									<tr>
										<td id="verifyNationalLabel" class="sectionsubhead"><%=LABEL_NATIONAL%></td>
										<td id="verifyNationalValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.RoutingOther"/></td>
									</tr>
									<tr>
										<td id="verifyIBANLabel" class="sectionsubhead"><%=LABEL_IBAN%></td>
										<td id="verifyIBANValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.IBAN"/></td>
									</tr>
									<ffi:cinclude value1="${intBankSize}"  value2="0" operator="notEquals" >
									<tr>
										<td id="verifyCorrespondantBankAccountNoLabel" class="sectionsubhead"><%=LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER%></td>
										<td id="verifyCorrespondantBankAccountNoValue" class="columndata"><ffi:getProperty name="${payeeTask}" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
									</tr>
									</ffi:cinclude>
									<%
										}
									%>
								</table>
							</td> 
						</tr>
					</table>--%>


