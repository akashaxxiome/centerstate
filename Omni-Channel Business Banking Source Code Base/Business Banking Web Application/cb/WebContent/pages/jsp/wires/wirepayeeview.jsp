<%--
This is a common view wire beneficiary page.

Pages that request this page
----------------------------
wirepayees.jsp
	View button next to a wire beneficiary

Pages this page requests
------------------------
DONE button requests wirepayees.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ include file="../common/wire_labels.jsp"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<ffi:help id="payments_wirepayeeview" className="moduleHelpClass"/>

<ffi:setProperty name="DimButton" value="beneficiary"/>
<%--<ffi:include page="${PathExt}payments/inc/wire_buttons.jsp"/>--%>


<%
String type = "";
String REGULAR = WireDefines.PAYEE_TYPE_REGULAR;
%>
<% String intBankSize = ""; %>
<ffi:getProperty name="WireTransferPayee" property="IntermediaryBanks.Size" assignTo="intBankSize"/>
<%
if (intBankSize == null) {
	intBankSize = "0";
}
%>

<ffi:getProperty name="WireTransferPayee" property="PayeeDestination" assignTo="type"/>
<%
if (type == null) type = REGULAR;
if (type.equals(DRAWDOWN)) { %>
	<ffi:setL10NProperty name='PageHeading' value='Drawdown Wire Account Detail'/>
<% }
if (type.equals(BOOK)) { %>
	<ffi:setL10NProperty name='PageHeading' value='Book Wire Beneficiary Detail'/>
<% } %>
<div align="center" class="approvalDialogHt2">
<div class="confirmationDetails">
	<span>Tracking Number: </span>
	<span><ffi:getProperty name="WireTransferPayee" property="TrackingID" /></span>
</div>
<div class="blockWrapper label130">
	<div  class="blockHead"><%= type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> <!--L10NStart-->Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBeneficiaryAccountNoLabel"  class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_NUMBER %>:</span>
				<span id="viewBeneficiaryAccountNoValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountNum"/></span>
			</div>
			<div class="inlineBlock">
				<span  id="viewBeneficiaryAccountTypeLabel"  class="sectionsubhead sectionLabel"><%= LABEL_ACCOUNT_TYPE %>:</span>
				<span id="viewBeneficiaryAccountTypeValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountTypeDisplayName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBeneficiaryNameLabel"  width="115" class="sectionsubhead sectionLabel"><%= type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> <!--L10NStart-->Name<!--L10NEnd-->:</span>
				<span id="viewBeneficiaryNameValue"  width="228" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Name"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBeneficiaryNickNameLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->Nickname<!--L10NEnd-->:</span>
				<span id="viewBeneficiaryNickNameValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="NickName"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBeneficiaryAddress1Label"  class="sectionsubhead sectionLabel"><!--L10NStart-->Address 1:<!--L10NEnd--></span>
				<span id="viewBeneficiaryAddress1Value"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBeneficiaryAddress2Label"  class="sectionsubhead sectionLabel"><!--L10NStart-->Address 2:<!--L10NEnd--></span>
				<span id="viewBeneficiaryAddress2Value"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street2"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBeneficiaryCityLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->City:<!--L10NEnd--></span>
				<span id="viewBeneficiaryCityValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="City"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBeneficiaryStateLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->State<!--L10NEnd--><% if (type.equals(REGULAR)) { %>/<!--L10NStart-->Province<!--L10NEnd--><% } %>:</span>
				<span id="viewBeneficiaryStateValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="State"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBeneficiaryZipLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->ZIP<!--L10NEnd--><% if (type.equals(REGULAR)) { %>/Postal Code<% } %>:</span>
				<span id="viewBeneficiaryZipValue"   class="columndata"><ffi:getProperty name="WireTransferPayee" property="ZipCode"/></span>
			</div>
			<% if (type.equals(REGULAR)) { %>
				<div class="inlineBlock">
					<span id="viewBeneficiaryCountryLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->Country<!--L10NEnd-->:</span>
									    <!-- retrieve localized name based on banklookup name -->
					<span id="viewBeneficiaryCountryValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="CountryDisplayName" /></span>
				</div>
			<% } %>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBeneficiaryContactLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->Contact Person<!--L10NEnd-->:</span>
				<span id="viewBeneficiaryContactValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Contact"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBeneficiaryTypeLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->Beneficiary Type<!--L10NEnd-->:</span>
				<span id="viewBeneficiaryTypeValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeDestination"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span id="viewBeneficiaryScopeLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->Beneficiary Scope<!--L10NEnd-->:</span>
			<span id="viewBeneficiaryScopeValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeScope"/></span>
		</div>
	</div>
</div>
<div class="blockWrapper label130">
	<div  class="blockHead"><%= type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Bank Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Bank Info<!--L10NEnd-->" %></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBankNameLabel" class="sectionsubhead sectionLabel"><% if (type.equals(DRAWDOWN)) { %><!--L10NStart-->Debit<!--L10NEnd--> <% } %><!--L10NStart-->Bank Name<!--L10NEnd-->: </span>
				<span id="viewBankNameValue" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBankAddress1Label" class="sectionsubhead sectionLabel"><!--L10NStart-->Address 1<!--L10NEnd-->: </span>
				<span id="viewBankAddress1Value" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBankAddress2Label"  class="sectionsubhead sectionLabel"><!--L10NStart-->Address 2<!--L10NEnd-->: </span>
				<span id="viewBeneficiaryAddress2Value"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBankCityLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->City<!--L10NEnd-->: </span>
				<span id="viewBankCityValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewBankStateLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->State<!--L10NEnd--><% if (type.equals(REGULAR)) { %>/Province<% } %>: </span>
				<span id="viewBeneficiaryStateValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.State"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewBankZipLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->ZIP<!--L10NEnd--><% if (type.equals(REGULAR)) { %>/Postal Code<% } %>: </span>
				<span id="viewBankZipValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/></span>
			</div>
		</div>
		<% if (type.equals(REGULAR)) { %>
		<div class="blockRow">
			<span id="viewBankCountryLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->Country<!--L10NEnd-->: </span>
		    <span id="viewBankCountryValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CountryDisplayName" /></span>
		</div>
		<% } %>
		<div class="blockRow">
			<span id="viewBankFEDLabel"   class="sectionsubhead sectionLabel"><!--L10NStart-->FED ABA<!--L10NEnd-->: </span>
			<span id="viewBankFEDValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/></span>
		</div>
		<% if (type.equals(REGULAR)) { %>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="verifySWIFTLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->SWIFT<!--L10NEnd-->: </span>
					<span id="verifySWIFTValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingSwift"/></span>
				</div>
				<div class="inlineBlock">
					<span id="viewBankCHIPSLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->CHIPS<!--L10NEnd-->: </span>
				<span id="viewBankCHIPSValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingChips"/></span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span id="viewBankNationalLabel"  class="sectionsubhead sectionLabel"><!--L10NStart-->National<!--L10NEnd-->: </span>
					<span id="viewBankNationalValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingOther"/></span>
				</div>
				<div class="inlineBlock">
					<span id="viewBankIBANLabel"  class="sectionsubhead sectionLabel"><%= LABEL_IBAN %>: </span>
				<span id="viewBankIBANValue"  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></span>
				</div>
			</div>
			<ffi:cinclude value1="${intBankSize}"  value2="0" operator="notEquals" >
			<div class="blockRow">
				<span  class="sectionsubhead sectionLabel"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></span>
			</div>
			</ffi:cinclude>
		<% } %>
	</div>
</div>
<% if (type.equals(REGULAR)) { %>
	<% if (!intBankSize.equals("0")) { %>
	<div class="blockWrapper">
	<% int counter = 0; %>
		<ffi:list collection="WireTransferPayee.IntermediaryBanks" items="Bank1">
			<div  class="blockHead"><!--L10NStart-->Intermediary Bank Info<!--L10NEnd--></div>
			<div class="blockContent">
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel">Intermediary Bank Name: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="BankName"/></span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel">FED ABA: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel">SWIFT: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/></span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel">CHIPS: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/></span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel">National: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/></span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
		    			<span class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/></span>
					</div>
				</div>
				<div class="blockRow">
						<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
		    			<span class="columndata">
		    				<% if(counter == 0) { %>
							<% } else { %>
							<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
							<% } counter++; %>
		    			</span>
				</div>
			</div>
		</ffi:list>
</div>
	<% } %>
<% } %>		
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Comments<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<ffi:getProperty name="WireTransferPayee" property="Memo" />
		</div>
	</div>
</div>
						
		<%--	<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata ltrow2_color">
					<div align="center">
					<table width="715" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="357" valign="top">
								<table width="355" cellpadding="3" cellspacing="0" border="0">
									 <tr>
										<td align="left" class="sectionhead" colspan="2">&gt; <%= type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> <!--L10NStart-->Info<!--L10NEnd--></td>
									</tr> --%>
									<%-- <tr>
										<td id="viewBeneficiaryAccountNoLabel" align="left" class="sectionsubhead"><%= LABEL_ACCOUNT_NUMBER %></td>
										<td id="viewBeneficiaryAccountNoValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountNum"/></td>
									</tr>
									<tr>
										<td  id="viewBeneficiaryAccountTypeLabel" align="left" class="sectionsubhead"><%= LABEL_ACCOUNT_TYPE %></td>
										<td id="viewBeneficiaryAccountTypeValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountTypeDisplayName"/></td>
									</tr>
									<tr>
										<td id="viewBeneficiaryNameLabel" align="left" width="115" class="sectionsubhead"><%= type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Account<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> <!--L10NStart-->Name<!--L10NEnd--></td>
										<td id="viewBeneficiaryNameValue" align="left" width="228" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Name"/></td>
									</tr>
									<tr>
										<td id="viewBeneficiaryNickNameLabel" align="left" class="sectionsubhead"><!--L10NStart-->Nickname<!--L10NEnd--></td>
										<td id="viewBeneficiaryNickNameValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="NickName"/></td>
									</tr>
									<tr>
										<td id="viewBeneficiaryAddress1Label" align="left" class="sectionsubhead"><!--L10NStart-->Address 1<!--L10NEnd--></td>
										<td id="viewBeneficiaryAddress1Value" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street"/></td>
									</tr>
									<tr>
										<td id="viewBeneficiaryAddress2Label" align="left" class="sectionsubhead"><!--L10NStart-->Address 2<!--L10NEnd--></td>
										<td id="viewBeneficiaryAddress2Value" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street2"/></td>
									</tr>
									<tr>
										<td id="viewBeneficiaryCityLabel" align="left" class="sectionsubhead"><!--L10NStart-->City<!--L10NEnd--></td>
										<td id="viewBeneficiaryCityValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="City"/></td>
									</tr>
									<tr>
										<td id="viewBeneficiaryStateLabel" align="left" class="sectionsubhead"><!--L10NStart-->State<!--L10NEnd--><% if (type.equals(REGULAR)) { %>/<!--L10NStart-->Province<!--L10NEnd--><% } %></td>
										<td id="viewBeneficiaryStateValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="State"/></td>
									</tr>
									<tr>
										<td id="viewBeneficiaryZipLabel" align="left" class="sectionsubhead"><!--L10NStart-->ZIP<!--L10NEnd--><% if (type.equals(REGULAR)) { %>/Postal Code<% } %></td>
										<td id="viewBeneficiaryZipValue"  align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="ZipCode"/></td>
									</tr>
									<% if (type.equals(REGULAR)) { %>
									<tr>
										<td id="viewBeneficiaryCountryLabel" align="left" class="sectionsubhead"><!--L10NStart-->Country<!--L10NEnd--></td>
									    <!-- retrieve localized name based on banklookup name -->
										
										<td id="viewBeneficiaryCountryValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="CountryDisplayName" /></td>
									</tr>
									<% } %>
									<tr>
										<td id="viewBeneficiaryContactLabel" align="left" class="sectionsubhead"><!--L10NStart-->Contact Person<!--L10NEnd--></td>
										<td id="viewBeneficiaryContactValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Contact"/></td>
									</tr>
									<tr>
										<td id="viewBeneficiaryTypeLabel" align="left" class="sectionsubhead"><!--L10NStart-->Beneficiary Type<!--L10NEnd--></td>
										<td id="viewBeneficiaryTypeValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeDestination"/></td>
									</tr>

									<tr>
										<td id="viewBeneficiaryScopeLabel" align="left" class="sectionsubhead"><!--L10NStart-->Beneficiary Scope<!--L10NEnd--></td>
										<td id="viewBeneficiaryScopeValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeScope"/></td>
									</tr> 
								</table>
							</td>
							<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
							<td width="348" valign="top">
								<table width="347" cellpadding="3" cellspacing="0" border="0">--%>
									<%-- <tr>
										<td align="left" class="sectionhead" colspan="2">&gt; <%= type.equals(DRAWDOWN) ? "<!--L10NStart-->Debit Bank Info<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary Bank Info<!--L10NEnd-->" %></td>
									</tr>
									<tr>
										<td id="viewBankNameLabel" align="left" width="115" class="sectionsubhead"><% if (type.equals(DRAWDOWN)) { %><!--L10NStart-->Debit<!--L10NEnd--> <% } %><!--L10NStart-->Bank Name<!--L10NEnd--></td>
										<td id="viewBankNameValue" align="left" width="220" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/></td>
									</tr>
									<tr>
										<td id="viewBankAddress1Label" align="left" class="sectionsubhead"><!--L10NStart-->Address 1<!--L10NEnd--></td>
										<td id="viewBankAddress1Value" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/></td>
									</tr>
									<tr>
										<td id="viewBankAddress2Label" align="left" class="sectionsubhead"><!--L10NStart-->Address 2<!--L10NEnd--></td>
										<td id="viewBeneficiaryAddress2Value" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/></td>
									</tr>
									<tr>
										<td id="viewBankCityLabel" align="left" class="sectionsubhead"><!--L10NStart-->City<!--L10NEnd--></td>
										<td id="viewBankCityValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/></td>
									</tr>
									<tr>
										<td id="viewBankStateLabel" align="left" class="sectionsubhead"><!--L10NStart-->State<!--L10NEnd--><% if (type.equals(REGULAR)) { %>/Province<% } %></td>
										<td id="viewBeneficiaryStateValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.State"/></td>
									</tr>
									<tr>
										<td id="viewBankZipLabel" align="left" class="sectionsubhead"><!--L10NStart-->ZIP<!--L10NEnd--><% if (type.equals(REGULAR)) { %>/Postal Code<% } %></td>
										<td id="viewBankZipValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/></td>
									</tr>
									<% if (type.equals(REGULAR)) { %>
									<tr>
										<td id="viewBankCountryLabel" align="left" class="sectionsubhead"><!--L10NStart-->Country<!--L10NEnd--></td>
									    <td id="viewBankCountryValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CountryDisplayName" /></td>
									</tr>
									<% } %>
									<tr>
										<td id="viewBankFEDLabel"  align="left" class="sectionsubhead"><!--L10NStart-->FED ABA<!--L10NEnd--></td>
										<td id="viewBankFEDValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/></td>
									</tr>
									<% if (type.equals(REGULAR)) { %>
									<tr>
										<td id="verifySWIFTLabel" align="left" class="sectionsubhead"><!--L10NStart-->SWIFT<!--L10NEnd--></td>
										<td id="verifySWIFTValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingSwift"/></td>
									</tr>
									<tr>
										<td id="viewBankCHIPSLabel" align="left" class="sectionsubhead"><!--L10NStart-->CHIPS<!--L10NEnd--></td>
										<td id="viewBankCHIPSValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingChips"/></td>
									</tr>
									<tr>
										<td id="viewBankNationalLabel" align="left" class="sectionsubhead"><!--L10NStart-->National<!--L10NEnd--></td>
										<td id="viewBankNationalValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingOther"/></td>
									</tr>
									<tr>
										<td id="viewBankIBANLabel" align="left" class="sectionsubhead"><%= LABEL_IBAN %></td>
										<td id="viewBankIBANValue" align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.IBAN"/></td>
									</tr>
									<ffi:cinclude value1="${intBankSize}"  value2="0" operator="notEquals" >
									<tr>
										<td align="left" class="sectionsubhead"><%= LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
										<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.CorrespondentBankAccountNumber"/></td>
									</tr>
									</ffi:cinclude>
									<% } %> 
								</table>
							</td>
						</tr>
					</table>--%>
<%-- <% if (type.equals(REGULAR)) { %>
	<% 
	if (!intBankSize.equals("0")) { %>
					<table width="715" cellpadding="3" cellspacing="0" border="0">
						<tr>
						    <td align="left" class="tbrd_b" colspan="5"><span class="sectionhead">&gt; <!--L10NStart-->Intermediary Bank Info<!--L10NEnd--></span></td>
						</tr>
						<tr>
							<td align="left" width="5%" class="sectionsubhead"><!--L10NStart-->Intermediary Bank Name<!--L10NEnd--></td>
							<td align="left" width="30%" class="sectionsubhead"><!--L10NStart-->FED ABA<!--L10NEnd--></td>
							<td align="left" width="12%" class="sectionsubhead"><!--L10NStart-->SWIFT<!--L10NEnd--></td>
							<td align="left" width="12%" class="sectionsubhead"><!--L10NStart-->CHIPS<!--L10NEnd--></td>
							<td align="left" width="12%" class="sectionsubhead"><!--L10NStart-->National<!--L10NEnd--></td>
							<td align="left" width="12%" class="sectionsubhead"><%= COLUMN_IBAN %></td>
							<td align="left" width="12%" class="sectionsubhead"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></td>
						</tr>
						<% int counter = 0; %>
						<ffi:list collection="WireTransferPayee.IntermediaryBanks" items="Bank1">
						<tr>
							<td align="left" width="5%" class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</td>
							<td align="left" width="30%" class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>&nbsp;</td>
							<td align="left" width="12%" class="columndata">
							<% if(counter == 0) { %>
							&nbsp;
							<% } else { %>
							<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
							<% } counter++; %>&nbsp;</td>
						</tr>
						</ffi:list>
					</table>
	<% } %>
<% } %> --%>
					<%-- <table width="715" cellpadding="3" cellspacing="0" border="0">
						<tr>
							<td align="left" class="sectionsubhead">&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class="tbrd_b"><span class="sectionhead">&gt; <!--L10NStart-->Comments<!--L10NEnd--></span></td>
						</tr>
						<tr>
							<td align="left" class="columndata"><ffi:getProperty name="WireTransferPayee" property="Memo" /></td>
						</tr>
					</table> --%>
					
					<%-- <table width="715" cellpadding="3" cellspacing="0" border="0">
						<tr>
							<td align="left" class="sectionsubhead">&nbsp;</td>
						</tr>
						<tr>
							<td align="left" class="tbrd_b" colspan="2"><span class="sectionhead">&gt; <!--L10NStart-->Confirmation<!--L10NEnd--></span></td>
						</tr>
						<tr>
							<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Tracking Number<!--L10NEnd--></td>
							<td align="left" width="600" class="columndata"><ffi:getProperty name="WireTransferPayee" property="TrackingID" /></td>
						</tr>
						<tr>
							<td align="left" class="sectionsubhead">&nbsp;</td>
						</tr>
					</table> --%>
					<%-- <table width="715" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td class="columndata" colspan="6" nowrap align="center">
							<form method="post">                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/><sj:a button="true" title="Close" onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a></form>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table> --%>
<form method="post"> 
	<div class="ffivisible" style="height:100px;">&nbsp;</div>
	<div align="center" class="ui-widget-header customDialogFooter">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/><sj:a button="true" title="Close" onClickTopics="closeDialog"><s:text name="jsp.default_175" /></sj:a>
	</div>
</form>
<s:if test="%{isManageAllowed}">

	<sj:a id="manageBeneficiaryButtonID" 
		button="true"
		name="wireBankSearch"
		onClickTopics="manageBeneficiaryClickTopic,closeBeneficiary"
	><s:text name="Manage beneficiary" />
	</sj:a>

</s:if>
</div>