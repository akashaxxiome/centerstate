<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.wiretransfers.*" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page import="java.lang.Integer" %>
<%@ include file="../common/wire_labels.jsp"%>

<ffi:cinclude value1="${templateTask}" value2="" operator="equals">	
								
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_FAILED_BACKEND).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-6" parm0="${wireType}" />"/>
  </span>
  <s:hidden id="colorCode" value="red" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_FAILED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-5" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="red" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_REJECTED_BACKEND).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-7" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="red" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_NO_FUNDS).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-8" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="red" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_LIMIT_CHECK_FAILED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-9" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="red" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_FUNDS_FAILED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-10" parm0="${wireType}"/>
  </span>
	<s:hidden id="colorCode" value="red" />
</ffi:cinclude>								

<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_CREATED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="green" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_SCHEDULED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="pink" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_IN_FUNDS_ALLOC).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="pink" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_PROCESSED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="pink" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_COMPLETED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="green" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_ACCEPTED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
 <s:hidden id="colorCode" value="green" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_CONFIRMED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="green" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_IN_PROCESS).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
 <s:hidden id="colorCode" value="pink" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_PENDING_FUNDS).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="pink" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_PENDING_BACKEND).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="pink" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_ACKNOWLEDGED).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="green" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_POSTEDON).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="pink" />
</ffi:cinclude>
<ffi:cinclude value1="${wireStatus}" value2="<%=new Integer(WireDefines.WIRE_STATUS_PENDING_APPROVAL).toString()%>">
  <span id="wireTransferResultMessage">
  <ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="${wireType}"/>
  </span>
  <s:hidden id="colorCode" value="pink" />
</ffi:cinclude>
<ffi:removeProperty name="wireStatus"/>												
</ffi:cinclude>

<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
  <span id="wireTransferResultMessage"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-3" parm0="${wireType}"/></span>
</ffi:cinclude>
