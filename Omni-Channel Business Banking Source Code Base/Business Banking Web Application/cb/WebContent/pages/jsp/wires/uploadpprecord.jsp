<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<% 	request.setAttribute("actionName","/pages/fileupload/WireFileUploadAction.action");
	String fileType = request.getParameter("fileType");
%>

<ffi:setProperty name="fileType" value="<%=fileType %>"/>

<ffi:setProperty name="ProcessImportTask" value="ProcessWiresImport"/>


<s:include value="/pages/jsp/common/publicuploader.jsp"/>

<script>
	ns.common.popupAfterUpload = true;
</script>
