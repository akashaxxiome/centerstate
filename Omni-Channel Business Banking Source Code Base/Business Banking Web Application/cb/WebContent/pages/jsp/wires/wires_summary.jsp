<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:removeProperty name="wireStartDate, wireEndDate,wireTask,DontInitialize,DontInitializeBatch,templateTask,LoadFromTransferTemplate"/>

<%-- Start: Clean up Dual approval data --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
<ffi:removeProperty name="daItemId"/>
<ffi:removeProperty name="ID"/>
</ffi:cinclude>
<%-- End: Clean up Dual approval data --%>

<div id="wiresGridTabs" class="portlet gridPannelSupportCls" style="float: left; width: 100%;" role="section" aria-labelledby="summaryHeader">
	<div class="portlet-header">
	<h1 id="summaryHeader" class="portlet-title"><s:text name="jsp.wire.wire_summary"/></h1>
	    <div class="searchHeaderCls">
			<span class="searchPanelToggleAreaCls" onclick="$('#quicksearchcriteria').slideToggle();">
				<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
			</span>
			<div class="summaryGridTitleHolder">
				<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.default_531"/>
				</span>
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
				
				<div class="gridTabDropdownHolder">
					<span class="gridTabDropdownItem" id='approving' onclick="ns.common.showGridSummary('approving')" >
						<s:text name="jsp.default_531"/>
					</span>
					<span class="gridTabDropdownItem" id='pending' onclick="ns.common.showGridSummary('pending')">
						<s:text name="jsp.default_530"/>
					</span>
					<span class="gridTabDropdownItem" id='completed' onclick="ns.common.showGridSummary('completed')">
						<s:text name="jsp.default_518"/>
					</span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="portlet-content">
    	<div class="searchDashboardRenderCls">
			<s:include value="/pages/jsp/wires/inc/wireSearchCriteria.jsp"/>
		</div>
		<div id="gridContainer" class="summaryGridHolderDivCls">
			<div id="approvingSummaryGrid" gridId="pendingApprovalWiresGridID" class="gridSummaryContainerCls" >
				<s:action namespace="/pages/jsp/wires" name="initWireTransfersSummaryAction" executeResult="true">
					<s:param name="collectionName">PendingApprovalWireTransfers</s:param>
				</s:action>
			</div>
			<div id="pendingSummaryGrid" gridId="pendingWiresGridID" class="gridSummaryContainerCls hidden" >
				<s:action namespace="/pages/jsp/wires" name="initWireTransfersSummaryAction" executeResult="true">
					<s:param name="collectionName">PendingWireTransfers</s:param>
				</s:action>
			</div>
			<div id="completedSummaryGrid" gridId="completedWiresGridID" class="gridSummaryContainerCls hidden" >
				<s:action namespace="/pages/jsp/wires" name="initWireTransfersSummaryAction" executeResult="true">
					<s:param name="collectionName">CompletedWireTransfers</s:param>
				</s:action>
			</div>
		</div>
	</div>
		
	<div id="wireSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('wiresGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>

<s:url id="calendarURL" value="/pages/jsp/wires/loadCalender_showPayments.action" escapeAmp="false">
      <%-- <s:url id="calendarURL" value="/pages/jsp/billpay/addBillpayAction_init.action"> --%>
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	<s:param name="moduleName" value="%{'WiresCalendar'}"></s:param>
</s:url>
      <sj:a
	id="openCalendar"
	href="%{calendarURL}"
	targets="summary"
	onClickTopics="" 
	onSuccessTopics="calendarLoadedSuccess"
	cssStyle="display:none;"
	cssClass="titleBarBtn"
	>
	<s:text name="jsp.billpay_calender"/><span class="ui-icon ui-icon-calendar floatleft"></span>
</sj:a>
			
<input type="hidden" id="getTransID" value="<ffi:getProperty name='wiretabs' property='TransactionID'/>" />

<ffi:removeProperty name="templateTask"/>

<script>
$.ajax({
    type: "POST",
   	url: "/cb/pages/jsp/wires/getWireTransfersAction_verify.action",
	data: $("#QuickSearchWiresFormID").serialize(),
    success: function(data) {
       
    }
});


//Initialize portlet with settings & calendar icon
ns.common.initializePortlet("wiresGridTabs", false, true, function( ) {
    $("#openCalendar").click();
});
</script>
