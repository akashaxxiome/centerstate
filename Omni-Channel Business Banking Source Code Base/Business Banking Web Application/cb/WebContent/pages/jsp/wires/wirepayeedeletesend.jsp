<%--
This page processes the delete wire beneficiary request

Pages that request this page
----------------------------
wirepayeedelete.jsp
	DELETE BENEFICIARY button
wirepayeebookdelete.jsp
	DELETE BENEFICIARY button
wirepayeedrawdowndelete.jsp
	DELETE BENEFICIARY button

Pages included in this page
---------------------------
payments/wirepayees.jsp
	The wire beneficiary summary page
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="DeleteWireTransferPayee" property="Process" value="false" />
	<ffi:object id="DeleteWireTransferPayeeDA" name="com.ffusion.tasks.dualapproval.wiretransfers.DeleteWireTransferPayeeDA" />
	<ffi:process name="DeleteWireTransferPayeeDA" />
	<ffi:removeProperty name="DeleteWireTransferPayeeDA"/>
</ffi:cinclude>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>"  operator="notEquals">
	<ffi:setProperty name="DeleteWireTransferPayee" property="Process" value="true"/>
	<ffi:process name="DeleteWireTransferPayee"/>
	<ffi:removeProperty name="DeleteWireTransferPayee"/>
</ffi:cinclude>
<ffi:removeProperty name="WireTransferPayee"/>
<%-- End: Dual approval processing --%>

<s:include value="%{#session.PagesPath}/wires/wire_beneficiaries_summary.jsp"/>
