<%@ page import="com.ffusion.beans.wiretransfers.WireTransfer,
                 com.ffusion.beans.wiretransfers.WireTransfers,
                 com.ffusion.beans.wiretransfers.WireBatch"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_wirebatchentrydelete" className="moduleHelpClass"/>

<ffi:cinclude value1="${wireBatch}" value2="ModifyWireBatch" operator="notEquals">
	<ffi:setProperty name="AddWireBatch" property="WireFreeFormFlag" value="true"/>
	<ffi:setProperty name="AddWireBatch" property="WireParameter" value="${wireIndex}"/>
	<ffi:setProperty name="AddWireBatch" property="WireSessionName" value="WireTransfer"/>
	<ffi:process name="AddWireBatch"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireBatch}" value2="ModifyWireBatch" operator="equals">
		<ffi:object id="SetWireTransferFromBatch" name="com.ffusion.tasks.wiretransfers.SetWireTransferFromBatch"/>
		<ffi:setProperty name="SetWireTransferFromBatch" property="Index" value="${wireIndex}"/>
		<ffi:setProperty name="SetWireTransferFromBatch" property="CollectionSessionName" value="ModifyWireBatch"/>
		<ffi:setProperty name="SetWireTransferFromBatch" property="BeanSessionName" value="WireTransfer"/>
		<ffi:process name="SetWireTransferFromBatch"/>
</ffi:cinclude>

<%
String wireDest = "";
String wireDom = com.ffusion.beans.wiretransfers.WireDefines.WIRE_DOMESTIC;
String wireInt = com.ffusion.beans.wiretransfers.WireDefines.WIRE_INTERNATIONAL;
 %>
<ffi:getProperty name="WireTransfer" property="WireDestination" assignTo="wireDest"/>

		<div align="center">

			<table width="750" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="ltrow2_color">
						<div align="center">
							<table border="0" cellspacing="0" cellpadding="3" width="711">
								<tr>
									<td class="tbrd_b sectionhead">&gt; 
									<% if (isSkip) { %><!--L10NStart-->Are you sure you want to skip this wire transfer<!--L10NEnd--><% } else { %>
										<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="equals">Are you sure you want to delete this wire transfer template?</ffi:cinclude>
										<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="notEquals">Are you sure you want to delete this wire transfer?</ffi:cinclude>
									<% } %>
									</td>
								</tr>
							</table>
							<br>
						<ffi:cinclude value1="${deleteTemplate}" value2="true">
							<table border="0" cellspacing="0" cellpadding="3" width="715">
								<tr>
									<td class="tbrd_b sectionhead" colspan="4">&gt; Template Info</td>
								</tr>
								<tr>
									<td width="115" class="sectionsubhead">Template Name</td>
									<td width="238" class="columndata"><ffi:getProperty name="WireTransfer" property="WireName"/></td>
									<td width="115" class="sectionsubhead">Template Category</td>
									<td width="223" class="columndata"><ffi:getProperty name="WireTransfer" property="WireCategory"/></td>
								</tr>
								<tr>
									<td class="sectionsubhead">Template Nickname</td>
									<td class="columndata"><ffi:getProperty name="WireTransfer" property="NickName"/></td>
									<td class="sectionsubhead">Template Scope</td>
									<td class="columndata"><ffi:getProperty name="WireTransfer" property="WireScope"/></td>
								</tr>
							</table>
							<br>
						</ffi:cinclude>
<table width="715" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="tbrd_b sectionhead" colspan="3">&gt; Send Wire To</td>
	</tr>
	<tr>
		<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="357" valign="top">
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td class="sectionhead" colspan="2">&gt; Beneficiary Info</td>
				</tr>
				<tr>
					<td width="115" class="sectionsubhead">Beneficiary Name</td>
					<td width="228" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Name"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Address 1</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Address 2</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Street2"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">City</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.City"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">State/Province</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.State"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">ZIP/Postal Code</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.ZipCode"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Country</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Country"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Contact Person</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.Contact"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Beneficiary Scope</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.PayeeScope"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Account Number</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.AccountNum"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Account Type</td>
					<td class="columndata">
						<ffi:setProperty name="WireAccountType" property="ResourceID" value="AccountType_${WireTransfer.WirePayee.AccountType}"/>
						<ffi:getProperty name="WireAccountType" property="Resource"/>
					</td>
				</tr>
			</table>
		</td>
		<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td class="sectionhead" colspan="2">&gt; Beneficiary Bank Info</td>
				</tr>
				<tr>
					<td width="115" class="sectionsubhead">Bank Name</td>
					<td width="220" class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.BankName"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Address 1</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Address 2</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Street2"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">City</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.City"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">State/Province</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.State"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">ZIP/Postal Code</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.ZipCode"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Country</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.Country"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">FED ABA</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingFedWire"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">SWIFT</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingSwift"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">CHIPS</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingChips"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">National</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="WirePayee.DestinationBank.RoutingOther"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>						
<ffi:cinclude value1="${WireTransfer.WirePayee.IntermediaryBanks}" value2="" operator="notEquals" >
<ffi:cinclude value1="${WireTransfer.WirePayee.IntermediaryBanks.Size}" value2="0" operator="notEquals" >
			    <table width="720" border="0" cellspacing="0" cellpadding="3">
				<tr>
				    <td class="columndata ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="53" height="1" border="0"></td>
				    <td class="columndata ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="229" height="1" border="0"></td>
				    <td class="columndata ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="90" height="1" border="0"></td>
				    <td class="columndata ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="90" height="1" border="0"></td>
				    <td class="columndata ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="90" height="1" border="0"></td>
				    <td class="columndata ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="90" height="1" border="0"></td>
				</tr>
				<tr>
				    <td class="sectionsubhead">&nbsp;</td>
				    <td class="sectionsubhead">Intermediary Bank Name</td>
				    <td class="sectionsubhead">FED ABA</td>
				    <td class="sectionsubhead">SWIFT</td>
				    <td class="sectionsubhead">CHIPS</td>
				    <td class="sectionsubhead">National</td>
				</tr>
				<ffi:list collection="WireTransfer.WirePayee.IntermediaryBanks" items="Bank1">
				    <tr>
					<td class="columndata">&nbsp;</td>
					<td class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</td>
					<td class="columndata" colspan="1"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</td>
					<td class="columndata" colspan="1"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</td>
					<td class="columndata" colspan="1"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</td>
					<td class="columndata" colspan="1"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</td>
				    </tr>
				</ffi:list>
			    </table>
</ffi:cinclude>
</ffi:cinclude>
				<br>
<table width="715" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="tbrd_b sectionhead" colspan="3">&gt; Debit Info</td>
	</tr>
	<tr>
		<td colspan="3"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td width="357" valign="top">
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td width="115" class="sectionsubhead">Debit Account</td>
					<td width="228" class="columndata"><ffi:getProperty name="WireTransfer" property="FromAccountNum"/></td>
				</tr>
				<tr>
<ffi:setProperty name="temp_amount" value="Amount"/>
<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_TEMPLATE %>" operator="equals">
	<ffi:setProperty name="temp_amount" value="Template Limit"/>
</ffi:cinclude>
<ffi:cinclude value1="${WireTransfer.WireType}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="equals">
	<ffi:setProperty name="temp_amount" value="Template Limit"/>
</ffi:cinclude>
					<td class="sectionsubhead"><ffi:getProperty name="temp_amount"/><ffi:removeProperty name="temp_amount"/></td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="AmountForBPW"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead"><%= wireDest.equals(wireDom) ? "Value Date" : "Processing Date" %></td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="DueDate"/></td>
				</tr>
				<% if (wireDest.equals(wireInt)) { %>
				<tr>
					<td class="sectionsubhead">Settlement Date</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="SettlementDate"/></td>
				</tr>
				<% } %>
				<tr>
					<td class="sectionsubhead">Date To Post</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="DateToPost"/></td>
				</tr>
<ffi:cinclude value1="${WireTransfer.Type}" value2="<%= String.valueOf(com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER) %>" operator="equals">
				<tr>
					<td class="sectionsubhead">Frequency</td>
					<td class="columndata">
						<ffi:setProperty name="WireTransferFrequenciesNames" property="ResourceID" value="WireFrequencies${WireTransfer.FrequencyValue}"/>
						<ffi:getProperty name="WireTransferFrequenciesNames" property="Resource"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Payments</td>
					<td class="columndata">
					<ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="equals">
						Unlimited
					</ffi:cinclude>
					<ffi:cinclude value1="${WireTransfer.NumberTransfers}" value2="999" operator="notEquals">
						<ffi:getProperty name="WireTransfer" property="NumberTransfers"/>
					</ffi:cinclude>
					</td>
				</tr>
				</ffi:cinclude>
			</table>
		</td>
<% if (wireDest.equals(wireDom)) { %>
		<td colspan="2">&nbsp;</td>
<% } else { %>
		<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
			<table width="347" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td width="115" class="sectionsubhead">Debit Currency</td>
					<td width="220" class="columndata"><ffi:getProperty name="WireTransfer" property="AmtCurrencyType"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Exchange Rate</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="ExchangeRate"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Math Rule</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="MathRule"/></td>
				</tr>
				<tr>
					<td class="sectionsubhead">Contract Number</td>
					<td class="columndata"><ffi:getProperty name="WireTransfer" property="ContractNumber"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>
<table width="715" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="tbrd_b sectionhead">&gt; Payment Info</td>
	</tr>
	<tr>
		<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="10" alt=""></td>
	</tr>
	<tr>
		<td>
			<table width="355" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td width="115" class="sectionsubhead">Payment Currency</td>
					<td width="228" class="columndata"><ffi:getProperty name="WireTransfer" property="PayeeCurrencyType"/></td>
				</tr>
			</table>		
		</td>
<% } %>
	</tr>
</table>
<br>
<table width="715" border="0" cellspacing="0" cellpadding="3">
								<tr>
									<td class="tbrd_b"><span class="sectionhead">&gt; <%= wireDest.equals(wireDom) ? "Reference for Beneficiary" : "Sender's Reference" %></span></td>
								</tr>
								<tr>
								 	<td class="columndata"><ffi:getProperty name="WireTransfer" property="Comment"/></td>
								</tr>
								<tr>
									<td class="tbrd_b"><span class="sectionhead">&gt; Originator to Beneficiary Information</span></td>
								</tr>
								<tr>
								 	<td class="columndata">
									<% String info = ""; %>
									<ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo1"/>
									<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo2}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo2"/></ffi:cinclude>
									<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo3}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo3"/></ffi:cinclude>
									<ffi:cinclude value1="${WireTransfer.OrigToBeneficiaryInfo4}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="OrigToBeneficiaryInfo4"/></ffi:cinclude>
									</td>
								</tr>
								<tr>
									<td class="tbrd_b"><span class="sectionhead">&gt; Bank to Bank Information</span></td>
								</tr>
								<tr>
								 	<td class="columndata"><ffi:getProperty name="WireTransfer" property="BankToBankInfo1"/>
									<ffi:cinclude value1="${WireTransfer.BankToBankInfo2}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo2"/></ffi:cinclude>
									<ffi:cinclude value1="${WireTransfer.BankToBankInfo3}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo3"/></ffi:cinclude>
									<ffi:cinclude value1="${WireTransfer.BankToBankInfo4}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo4"/></ffi:cinclude>
									<ffi:cinclude value1="${WireTransfer.BankToBankInfo5}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo5"/></ffi:cinclude>
									<ffi:cinclude value1="${WireTransfer.BankToBankInfo6}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransfer" property="BankToBankInfo6"/></ffi:cinclude>
									</td>
								</tr>
							</table>

				<br>
				<table width="98%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td align="center">
							<form action="wirebatchentrydeletesend.jsp" method="post" name="TransferNew">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="notEquals">
								<input type="Button" value="Cancel" class="submitbutton" onClick="javascript:document.location='<ffi:getProperty name="wireBackURL"/>'">
							</ffi:cinclude>
							<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="equals">
								<input type="Button" value="Cancel" class="submitbutton" onClick="javascript:document.location='<ffi:getProperty name="wireBackURL"/>'">
								<input type="hidden" name="deleteTemplate" value="true">
							</ffi:cinclude>
								<input class="submitbutton" type="submit" name="submitButtonName" value="Delete Transfer From Batch" border="0">
							</form>
							<ffi:removeProperty name="deleteTemplate"/>
						</td>
					</tr>
				</table>
			</span>
			</div>
			</td>
		</tr>
	</table>
		</div>
