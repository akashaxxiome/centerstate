<%--
This is the freeform international wire batch confirmation page. Note it is only
for international wire batches.  All other wire types use wirebatchffconfirm.jsp

Pages that request this page
----------------------------
wirebatchff.jsp
	SUBMIT WIRE BATCH button
wirebatcheditff.jsp
	SUBMIT WIRE BATCH button
wirebatchedittemplate.jsp
	SUBMIT WIRE BATCH button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
BACK requests one of the following
	wirebatchff.jsp
	wirebatcheditff.jsp
	wirebatchedittemplate.jsp
CONFIRM/SEND BATCH requests wirebatchsend.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_wirebatchintffconfirm" className="moduleHelpClass"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<% if (request.getParameter("GetAmounts") == null) { %>
	<% if(session.getAttribute("AddWireBatch")!=null) {%>
        <ffi:setProperty name="AddEditTask" value="AddWireBatch" />
        <ffi:removeProperty name="ModifyWireBatch"/>
    <%} %>
    <%if(session.getAttribute("ModifyWireBatch")!=null) {%>
        <ffi:setProperty name="AddEditTask" value="ModifyWireBatch" />
	<%} %>
	
    <%-- Copy DateToPost to SettlementDate if SettlementDate is empty --%>
    <% String settleDate = ""; %>
    <ffi:getProperty name="${AddEditTask}" property="SettlementDate" assignTo="settleDate"/>
    <%
    if (settleDate == null) settleDate = "";
    if (settleDate.equals("")) {
        String postDate = "";
    %>
        <ffi:getProperty name="${AddEditTask}" property="DateToPost" assignTo="postDate"/>
        <ffi:setProperty name="${AddEditTask}" property="SettlementDate" value="<%= postDate %>"/>
    <%
    }
    %>

<% } else { %>
    <ffi:object id="GetBatchAmounts" name="com.ffusion.tasks.wiretransfers.GetBatchAmounts" scope="session" />
    <ffi:setProperty name="GetBatchAmounts" property="BeanSessionName" value="${AddEditTask}"/>
    <ffi:cinclude value1="${AddEditTask}" value2="AddWireBatch" operator="equals" >
        <ffi:setProperty name="GetBatchAmounts" property="Add" value="true"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${AddEditTask}" value2="AddWireBatch" operator="notEquals" >
        <ffi:setProperty name="GetBatchAmounts" property="Add" value="false"/>
    </ffi:cinclude>
    <ffi:process name="GetBatchAmounts"/>
<% } %>

        <div align="center">
            <div align="center">
            <table width="750" cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td class="columndata ltrow2_color" align="center">
                    <table width="98%" border="0" cellspacing="0" cellpadding="3">
    <%-- Show alert if date was changed --%>
    <% String dateChanged = ""; %>
    <ffi:getProperty name="${AddEditTask}" property="DateChanged" assignTo="dateChanged"/>

    <% String nonBizDay = ""; %>
    <ffi:getProperty name="${AddEditTask}" property="NonBusinessDay" assignTo="nonBizDay"/>

    <% if ("true".equals(dateChanged) && "true".equals(nonBizDay) ) { %>
    <tr>
        <%-- Both proc window is closed and tomorrow is not a biz day --%>
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
            NOTE: The International wire processing window for today has closed,
            so the Requested Processing Date has been changed to tomorrow's date.
            Since tomorrow is not a business day, the Expected Processing Date has been changed
            to the next business day.<br><br>
        </td>
    </tr>
    <% } else if ("true".equals(dateChanged)) { %>
    <tr>
        <%-- DueDate changed because processing window is closed --%>
        <td class="sectionsubhead" style="color:red" align="center">
            NOTE: The International wire processing window for today has closed.
            The Requested Processing Date has been changed to tomorrow's date.<br><br>
        </td>
    </tr>
    <% } else if ("true".equals(nonBizDay)) { %>
    <tr>
        <%-- DueDate changed because its not a business day --%>
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
            NOTE: The Requested Processing Date is not a business day.
            The Expected Processing Date has been changed to the next business day.<br><br>
        </td>
    </tr>
    <% } %>

    <%-- Duplicate Wire warning --%>
    <% String dupeWire = ""; %>
    <ffi:getProperty name="${AddEditTask}" property="DuplicateWire" assignTo="dupeWire"/>
    <ffi:cinclude value1="<%= dupeWire %>" value2="true" operator="equals">
    <tr>
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
            Warning: One or more duplicate International wires exist
            for this business with the same Amount, Processing Date, and Beneficiary.<br><br>
        </td>
    </tr>
    </ffi:cinclude>
                        <tr>
                            <td align="left" class="tbrd_b sectionhead"><h2>Batch Information</h2></td>
                        </tr>
                    </table>
                    <br>
                    <table width="715" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="357" valign="top">
                                <table width="355" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" width="160" class="sectionsubhead">Batch Name</td>
                                        <td align="left" width="183" class="columndata"><ffi:getProperty name="${AddEditTask}" property="BatchName"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Application Type</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="BatchType"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Requested Processing Date</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="DueDate"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Settlement Date</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="SettlementDate"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Expected Processing Date</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="DateToPost"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                            <td width="348" valign="top">
                                <table width="347" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" width="115" class="sectionsubhead">Debit Currency</td>
                                        <td align="left" width="220" class="columndata">
                                        <ffi:cinclude value1="${AddEditTask}" value2="ModifyWireBatch" operator="notEquals">
                                            <ffi:getProperty name="${AddEditTask}" property="AmtCurrencyType"/>
                                        </ffi:cinclude>
                                        <ffi:cinclude value1="${AddEditTask}" value2="ModifyWireBatch" operator="equals">
                                            <ffi:getProperty name="${AddEditTask}" property="OrigCurrency"/>
                                        </ffi:cinclude>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Payment Currency</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="PayeeCurrencyType"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Exchange Rate</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="ExchangeRate"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Math Rule</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="MathRule"/></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="sectionsubhead">Contract Number</td>
                                        <td align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="ContractNumber"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table width="98%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="tbrd_b sectionhead">Included Batches</td>
                        </tr>
                    </table>
                    <table width="375" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="sectionsubhead" colspan="2">Beneficiary</td>
                            <td align="left" class="sectionsubhead">Amount</td>
                        </tr>
                        
                        <ffi:setProperty name="${AddEditTask}" property="Wires.Filter" value="ACTION!!del,Status!!3,AND" />
                        
                        <ffi:list collection="${AddEditTask}.Wires" items="wire">
                        <tr>
                            <td align="left" class="columndata" colspan="2" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
								<ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
                            <td align="left" class="columndata" nowrap>
                            <ffi:cinclude value1="${AddEditTask}" value2="AddWireBatch" operator="equals">
                                <ffi:getProperty name="wire" property="AmountForBPW"/> (<ffi:getProperty name="${AddEditTask}" property="AmtCurrencyType"/>)
                            </ffi:cinclude>
                            <ffi:cinclude value1="${AddEditTask}" value2="ModifyWireBatch" operator="equals">
                                <ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="equals">
                                    <ffi:getProperty name="wire" property="AmountForBPW"/> (<ffi:getProperty name="${AddEditTask}" property="OrigCurrency"/>)
                                </ffi:cinclude>
                                <ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
                                    <ffi:getProperty name="wire" property="OrigAmount"/> (<ffi:getProperty name="${AddEditTask}" property="OrigCurrency"/>)
                                </ffi:cinclude>
                            </ffi:cinclude>
                            </td>
                        </tr>
                        </ffi:list>
                        
                        <ffi:setProperty name="${AddEditTask}" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
                        
                        <% if (request.getParameter("GetAmounts") == null) { %>
                        <tr>
                            <td align="left" class="columndata" width="98%">&nbsp;</td>
                            <td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap>Original Total:</td>
                            <td align="left" class="tbrd_t sectionsubhead" width="1%" valign="bottom" nowrap>
                            <ffi:cinclude value1="${AddEditTask}" value2="ModifyWireBatch" operator="notEquals">
                                <ffi:getProperty name="${AddEditTask}" property="TotalAmountForBPW"/> (<ffi:getProperty name="${AddEditTask}" property="AmtCurrencyType"/>)
                            </ffi:cinclude>
                            <ffi:cinclude value1="${AddEditTask}" value2="ModifyWireBatch" operator="equals">
                                <ffi:getProperty name="${AddEditTask}" property="TotalOrigAmount"/> (<ffi:getProperty name="${AddEditTask}" property="OrigCurrency"/>)
                            </ffi:cinclude>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="columndata" width="98%">&nbsp;</td>
                            <td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap>Total in USD:</td>
                            <td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap><%-- <ffi:link url="wirebatchintffconfirm.jsp?GetAmounts=true">View Totals</ffi:link>--%>
                            	<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm('wirebatchintffconfirm.jsp?GetAmounts=true')">View Totals</a>
                            </td>
                        </tr>
								<script>
									//QTS680696
 									ns.wire.viewTotalsInBatchConfirmURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/wirebatchintffconfirm.jsp?GetAmounts=true"/>' ;
 								</script>
                        <tr>
                            <td align="left" class="columndata" width="98%">&nbsp;</td>
                            <td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap>Total Payment:</td>
                            <td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap><%-- <ffi:link url="wirebatchintffconfirm.jsp?GetAmounts=true">View Totals</ffi:link>--%>
                            	<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm()">View Totals</a>
                            </td>
                        </tr>
                        <% } else { %>
                        <tr>
                            <td align="left" class="columndata" width="98%">&nbsp;</td>
                            <td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap>Original Total:</td>
                            <td align="left" class="tbrd_t sectionsubhead" width="1%" valign="bottom" nowrap>
                                <ffi:getProperty name="GetBatchAmounts" property="TotalOrigAmount"/>
                                <ffi:cinclude value1="${AddEditTask}" value2="ModifyWireBatch" operator="notEquals">
                                    (<ffi:getProperty name="${AddEditTask}" property="AmtCurrencyType"/>)
                                </ffi:cinclude>
                                <ffi:cinclude value1="${AddEditTask}" value2="ModifyWireBatch" operator="equals">
                                    (<ffi:getProperty name="${AddEditTask}" property="OrigCurrency"/>)
                                </ffi:cinclude>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="columndata" width="98%">&nbsp;</td>
                            <td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap>Total in USD:</td>
                            <td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap>
                                <ffi:getProperty name="GetBatchAmounts" property="TotalDebit"/>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="columndata" width="98%">&nbsp;</td>
                            <td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap>Total Payment:</td>
                            <td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap>
                                <ffi:getProperty name="GetBatchAmounts" property="TotalPaymentAmount"/> (<ffi:getProperty name="${AddEditTask}" property="PayeeCurrencyType"/>)
                            </td>
                        </tr>
                        <% } %>
                        <ffi:removeProperty name="GetAmounts"/>
                    </table>
                    <br>
                       <s:form id="sendWireBacthFormID" method="post" name="FormName" namespace="/pages/jsp/wires" action="%{#session.wirebatch_confirm_form_url}" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    <sj:a id="cancelFormButtonOnBatchVerify"
						button="true" summaryDivId="summary" buttonType="cancel"
						onClickTopics="showSummary,cancelWireBatchForm"
					><s:text name="jsp.default_82" />
					</sj:a>
					<sj:a id="backFormButton" 
						  button="true" 
						  onClickTopics="backToInput"
					><s:text name="jsp.default_57" />
					</sj:a>
                     <sj:a id="sendWireBacthSubmit"
						formIds="sendWireBacthFormID" 
						targets="confirmDiv" 
						button="true" 
						onBeforeTopics="sendWireBacthFormBeforeTopics"
						onSuccessTopics="sendWireBacthFormSuccessTopic"
						onErrorTopics="errorSendWireBatchForm"
						onCompleteTopics="sendWireBacthFormCompleteTopic"><s:text name="jsp.default_CONFIRM_SEND_BATCH" /></sj:a>
                     <%-- </form> --%>
                    </s:form>
					<%--
                    <form action="wirebatchsend.jsp" method="post" name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    <input type="button" class="submitbutton" value="CANCEL" onclick="document.location='wiretransfers.jsp'">&nbsp;
                    <input type="button" class="submitbutton" value="BACK" onclick="document.location='<ffi:getProperty name="wireBackURL" />'">&nbsp;
                    <input type="submit" class="submitbutton" value="CONFIRM/SEND BATCH">
                    </form>
					--%>
                    </td>
                </tr>
            </table>
            </div>
        </div>

