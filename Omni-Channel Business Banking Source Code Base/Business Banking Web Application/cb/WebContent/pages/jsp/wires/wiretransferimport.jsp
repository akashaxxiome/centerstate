<%--
This is the wire transfer import page.

Pages that request this page
----------------------------
wire_buttons.jsp (included in many pages)
	FILE IMPORT top button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
IMPORT requests uploadpprecord.jsp
	Note:  uploadpprecord.jsp requests FileUpload (task), which in turn requests
	wiretransferimportwait.jsp
CUSTOM MAPPINGS requests fileuploadcustom.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_wiretransferimport" className="moduleHelpClass"/>

<ffi:object id="ProcessWiresImport" name="com.ffusion.tasks.fileImport.ProcessWiresImport" scope="session"/>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	<ffi:object id="GetMappingDefinitions" name="com.ffusion.tasks.fileImport.GetMappingDefinitionsTask" scope="session"/>
	<ffi:process name="GetMappingDefinitions"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
	<ffi:object id="MappingDefinitions" name="com.ffusion.beans.fileimporter.MappingDefinitions" scope="session" />
</ffi:cinclude>

<ffi:object id="SetMappingDefinition" name="com.ffusion.tasks.fileImport.SetMappingDefinitionTask" scope="session"/>
<% session.setAttribute("FFISetMappingDefinition", session.getAttribute("SetMappingDefinition")); %>

<ffi:object id="GetOutputFormat" name="com.ffusion.tasks.fileImport.GetOutputFormatTask" scope="session"/>

		<%-- Set up the flow of control for  fileimport --%>
<%--
		<ffi:setProperty name="FileUpload" property="SuccessURL" value="${SecureServletPath}ProcessWiresImport"/>
		<ffi:setProperty name="ProcessWiresImport" property="SuccessURL" value="${SecurePath}payments/wiretransferimportwait.jsp"/>

--%>
			<script type="text/javascript"><!--

function updateWireCustomMapping(fileType, customFileType)
{
	if (fileType.value == 'Custom')
	{
		customFileType.disabled = false;
		$("#SetMappingDefinitionIDID").selectmenu('enable'); 
	}
	else
	{
		customFileType.selectedIndex = 0;
		customFileType.disabled = true;
		$("#SetMappingDefinitionIDID").val('0');
		$("#SetMappingDefinitionIDID").selectmenu('destroy').selectmenu({escapeHtml:'true', width:'20em'});
		$("#SetMappingDefinitionIDID").selectmenu('disable');
		
	}
}


$("#FileUploadFileTypeID").selectmenu({width:'20em'});
$("#SetMappingDefinitionIDID").selectmenu({escapeHtml:'true', width:'20em'});

$.subscribe('beforeLoadingWireUploadForm', function(event,data){
	removeValidationErrors();
});

$.subscribe('successLoadingWireUploadForm', function(event,data){
	$("#openFileImportDialogID").dialog('open');
});
$(document).ready(function(){
	updateWireCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID']);
});
// --></script>

<ffi:removeProperty name="ImportWireTransfers"/>

<%--
<body onLoad="updateCustomMapping(document.FileType['FileUpload.FileType'], document.FileType['SetMappingDefinition.ID'])">
--%>
		<div>

						<%--<form name="FileType" action="<ffi:getProperty name='SecurePath'/>payments/uploadpprecord.jsp" method="post" target="FileUpload">--%>
						<form name="FileType" action="/cb/pages/fileupload/WireFileUploadAction_verify.action" method="post" id="WireUploadFileTypeFormID">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<table border="0" cellspacing="0" cellpadding="0" class="tableData" width="100%">
								<tr>
									<td class="columndata"><span class="sectionsubhead"></span></td>
									<%-- <td width="30"></td>
									<td><span class="sectionsubhead"><s:text name="jsp.default_14" /></span></td> --%>
								</tr>
								<tr>
									<td class="columndata"><!--L10NStart-->Fusion Bank accepts electronic records from a variety of applications you may be using in your business. Importing these records is easy and can save your business a significant amount of time and expense. Accepted file types are displayed in the column on the right.<!--L10NEnd--><br>
										<br><!--L10NStart-->If you have questions about file import, see the Help section, or call us at 1-800-000-0000.<!--L10NEnd-->
									</td>
									<!-- <td class="columndata" nowrap width="30"></td> -->

									<%-- <td class="columndata" valign="top">
									<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
										<select id="FileUploadFileTypeID" name="fileType" class="txtbox" onChange="updateWireCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID'])">
											<option value="Wire Import"><s:text name="wire.file.swift_format" /></option>
											<option value="Custom"><s:text name="wire.file.custom_format" /></option>
										</select>
									</ffi:cinclude>
									<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
										<input type="hidden" name="fileType" value="Wire Import">
										<input type="hidden" name="SetMappingDefinition.ID" value="0">
										<s:text name="wire.file.swift_format" />
									</ffi:cinclude>
									</td> --%>
								</tr>
								<tr>
									<td class="columndata" valign="top">
										<div class="marginTop10">&nbsp;</div>
										<span class="sectionsubhead"><s:text name="jsp.default_14" /></span>
										<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
											<select id="FileUploadFileTypeID" name="fileType" class="txtbox" onChange="updateWireCustomMapping(document.FileType['fileType'], document.FileType['SetMappingDefinition.ID'])">
												<option value="Wire Import"><s:text name="wire.file.swift_format" /></option>
												<option value="Custom"><s:text name="wire.file.custom_format" /></option>
											</select>
										</ffi:cinclude>
										<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
											<input type="hidden" name="fileType" value="Wire Import">
											<input type="hidden" name="SetMappingDefinition.ID" value="0">
											<s:text name="wire.file.swift_format" />
										</ffi:cinclude>
										
										<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
											<span class="sectionsubhead marginleft10"><!--L10NStart-->Custom Mappings<!--L10NEnd--></span>
											 <select name="SetMappingDefinition.ID" class="txtbox" id="SetMappingDefinitionIDID">
											     <option value="0"><!--L10NStart-->- Select Custom Mapping -<!--L10NEnd--></option>
											     <ffi:list collection="MappingDefinitions" items="MappingDefinition">
											         <ffi:cinclude value1="${MappingDefinition.OutputFormatName}" value2="" operator="notEquals">
											             <ffi:setProperty name="GetOutputFormat" property="Name" value="${MappingDefinition.OutputFormatName}"/>
											             <ffi:process name="GetOutputFormat"/>
											             <ffi:setProperty name="OutputFormat" property="CurrentCategory" value="WIRE"/>
											
											             <ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
											                 <option value="<ffi:getProperty name='MappingDefinition' property='MappingID'/>"><ffi:getProperty name="MappingDefinition" property="Name"/></option>
											             </ffi:cinclude>
											         </ffi:cinclude>
											     </ffi:list>
											 </select><br/>
											 <span id="mappingTypeError"></span>
										</ffi:cinclude>
									</td>
								</tr>
								<%-- <tr>
									<td valign="bottom">
										<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
										<span class="sectionsubhead"><!--L10NStart-->Custom Mappings<!--L10NEnd--></span>
										</ffi:cinclude>
									</td>
								</tr> --%>
								<%-- <tr>
									<td valign="top" class="columndata" nowrap>
										<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
											<span class="sectionsubhead"><!--L10NStart-->Custom Mappings<!--L10NEnd--></span>
											 <select name="SetMappingDefinition.ID" class="txtbox" id="SetMappingDefinitionIDID">
											     <option value="0"><!--L10NStart-->- Select Custom Mapping -<!--L10NEnd--></option>
											     <ffi:list collection="MappingDefinitions" items="MappingDefinition">
											         <ffi:cinclude value1="${MappingDefinition.OutputFormatName}" value2="" operator="notEquals">
											             <ffi:setProperty name="GetOutputFormat" property="Name" value="${MappingDefinition.OutputFormatName}"/>
											             <ffi:process name="GetOutputFormat"/>
											             <ffi:setProperty name="OutputFormat" property="CurrentCategory" value="WIRE"/>
											
											             <ffi:cinclude value1="${OutputFormat.ContainsCategory}" value2="true">
											                 <option value="<ffi:getProperty name='MappingDefinition' property='MappingID'/>"><ffi:getProperty name="MappingDefinition" property="Name"/></option>
											             </ffi:cinclude>
											         </ffi:cinclude>
											     </ffi:list>
											 </select><br/>
											 <span id="mappingTypeError"></span>
										</ffi:cinclude>
									</td>
								</tr> --%>
								<tr>
									<td colspan="3" align="center" nowrap>
										<div class="marginTop10">&nbsp;</div>
										<sj:a 
											id="cancelFileImportTypeID"
			                                button="true" 
			                                onclick="ns.wire.cancelFileImport()"
		    	                            cssClass="cancelImportFormButton"
					                        ><s:text name="jsp.default_82" /><!-- CANCEL -->
					            		</sj:a>
										<sj:a 
										    id="wireFileUploadID"
											formIds="WireUploadFileTypeFormID"
			                                targets="openFileImportDialogID" 
			                                button="true" 
			                                validate="true" 
			                                validateFunction="customValidation"
											onclick="removeValidationErrors();"
			                                onBeforeTopics="beforeLoadingWireUploadForm"
			                                onErrorTopics="errorOpenFileUploadTopics"
											onSuccessTopics="successLoadingWireUploadForm"
					                        ><s:text name="jsp.default_235" /><!-- IMPORT FILE -->
					            		</sj:a>
									</td>
								</tr>
										
							</table>
			</form>
		</div>

<ffi:removeProperty name="GetOutputFormat"/>
<ffi:setProperty name="actionNameNewReportURL" value="NewReportBaseAction.action?NewReportBase.ReportName=${ReportName}&flag=generateReport" URLEncrypt="true" />
<ffi:setProperty name="actionNameGenerateReportURL" value="GenerateReportBaseAction_display.action?doneCompleteDirection=${doneCompleteDirection}" URLEncrypt="true" />