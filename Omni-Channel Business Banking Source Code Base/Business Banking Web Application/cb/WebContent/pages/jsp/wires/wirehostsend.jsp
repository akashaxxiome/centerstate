<%--
This page processes the Add/Edit host wire

Pages that request this page
----------------------------
wirehostconfirm.jsp
	SUBMIT WIRE button

Pages this page requests
------------------------
wiretansfers.jsp
    DONE button
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%
String task = "wireTransfer";
%>
<ffi:setProperty name="<%= task %>" property="DateFormat" value="${UserLocale.DateFormat}"/>
<ffi:help id="payments_wirehostconfirm" className="moduleHelpClass"/>
<div align="center" id="hostWiresTransferConfirmationFormID">
<div id="hostWiresConfirmationTableID">
	<div style="display:none;">
	  <span id="wireTransferResultMessage"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/wiretransfersend.jsp-4" parm0="Host"/></span>
	</div>
	<div class="blockWrapper hostWireTransfer">
	<div  class="blockHead"><!--L10NStart-->Wire Transfer Summary<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Host ID<!--L10NEnd-->: </span>
                <span class="columndata"><ffi:getProperty name="<%= task %>" property="HostID"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Requested Processing Date<!--L10NEnd-->: </span>
                <span class="columndata"><ffi:getProperty name="<%= task %>" property="DueDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Amount<!--L10NEnd-->: </span>
                    <span class="columndata"><ffi:getProperty name="<%= task %>" property="Amount"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Settlement Date<!--L10NEnd-->: </span>
                <span class="columndata"><ffi:getProperty name="<%= task %>" property="SettlementDate"/></span>	
			</div>
		</div>
		<div class="blockRow">
                 <span class="sectionsubhead sectionLabel"><!--L10NStart-->Expected Processing Date<!--L10NEnd-->: </span>
                 <span class="columndata"><ffi:getProperty name="<%= task %>" property="DateToPost"/></span>
		</div>
	</div>
</div>
</div>
<div class="btn-row">
<form name="frmNew" method="post" id="HostWireTransferNewSendID">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="isHostWireFlag" value="true" id="isHostWireID"/>
	<sj:a id="sendFormButtonOnConfirm" 
		button="true" summaryDivId="summary" buttonType="done"
		onClickTopics="showSummary,cancelWireTransferForm"
	><s:text name="jsp.default_175" /><!-- DONE -->
	</sj:a>
	<script>
		ns.wire.printReadyUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/inc/print_wiretransfersend.jsp"/>';
		var isHostWire = $("#isHostWireID").val();
		if( isHostWire == "true" ){
		ns.wire.printReadyUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/inc/print_wirehostsend.jsp?"/>';	
		}					
	</script>
	<sj:a id="sendFormButtonOnPrint"
		  name="printerReady"
		  button="true" 
		  onclick="ns.wire.printerReadyButtonOnClick(ns.wire.printReadyUrl);"
		  cssClass="rpt_det_ctrl_notcancel"
	><s:text name="jsp.default_332" />
	</sj:a>
</form>
</div>
</div>
<ffi:removeProperty name="operation_action"/>