<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
	<script>
	//onchange="chooseWireType();" 
		$(function(){
			$("#wiresTypesID").combobox({'size':'18'});
		});	
		$.subscribe('setDestinationTopic', function(event,data) {
				frm = document.WireTemplate;
				dest = frm.dests.options[frm.dests.selectedIndex].value;
				var targetAction = "/cb/pages/jsp/wires/wiretransfernew.jsp";
				if (dest == "DOMESTIC")	{
					targetAction = "/cb/pages/jsp/wires/wiretransfernew.jsp?wireTask=AddWireTransfer&templateTask=add";
				} else if ( dest == "INTERNATIONAL" ){
					targetAction = "/cb/pages/jsp/wires/wiretransfernew.jsp?wireTask=AddWireTransfer&wireDestination=INTERNATIONAL&templateTask=add";
				} else if ( dest == "DRAWDOWN" ){
					targetAction = "/cb/pages/jsp/wires/wiredrawdown.jsp?wireTask=AddWireTransfer&wireDestination=DRAWDOWN&templateTask=add";
				} else if ( dest == "FEDWIRE" ){
					targetAction = "/cb/pages/jsp/wires/wirefed.jsp?wireTask=AddWireTransfer&wireDestination=FEDWIRE&templateTask=add";
				} else if ( dest == "BOOKTRANSFER" ){
					targetAction = "/cb/pages/jsp/wires/wirebook.jsp?wireTask=AddWireTransfer&wireDestination=BOOKTRANSFER&templateTask=add";
				}
				$("#SelectWiresTypeFormID").attr("action",targetAction); 
		});
		
		//$.subscribe('setBatchDestinationTopic', function(event,data) {
		//});
	</script>
	<span id="PageHeading" style="display:none;">Add Wire Template</span>
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>">
		<table width="750" border="0" cellspacing="0" cellpadding="1">
			<tr>
				<td nowrap>
					<s:form name="WireTemplate" action="/pages/jsp/wires/wiretransfernew.jsp" id="SelectWiresTypeFormID" theme="simple">
               			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>		
						<span class="sectionsubhead"><!--L10NStart-->Wire Type<!--L10NEnd--> </span>
						<select name="dests" id="wiresTypesID" class="txtbox" <ffi:getProperty name="disableNewOptions"/>>
							<ffi:list collection="GetEntitledWireTypes.EntitledWireTypes" items="wireType">
								<option value="<ffi:getProperty name="wireType"/>"><ffi:setProperty name="WireDestinations" property="Key" value="${wireType}"/><ffi:getProperty name="WireDestinations" property="Value"/></option>
							</ffi:list>
							<ffi:cinclude value1="${disableNewOptions}" value2="disabled" operator="equals">
								<option value=""><!--L10NStart-->None Available<!--L10NEnd--></option>
							</ffi:cinclude>
						</select>
						<sj:a targets="inputDiv" id="newSingleWiresTransferID" formIds="SelectWiresTypeFormID" button="true" onClickTopics="setDestinationTopic" onCompleteTopics="selectWiresTypeCompleteTopic">NEW TEMPLATE</sj:a>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_BATCH_CREATE %>">
							<%--
							&nbsp;<input type="button" value="NEW BATCH" class="submitbutton" onclick="setBatchDest()" <ffi:getProperty name="disableNewOptions"/>>
							
							<sj:a targets="inputDiv" id="newBatchWiresTransferID" formIds="SelectWiresTypeFormID" button="true" onClickTopics="setBatchDestinationTopic" onCompleteTopics="selectWiresTypeCompleteTopic">NEW BATCH</sj:a>
							--%>
						</ffi:cinclude>		
					</s:form>
				</td>
			</tr>
		</table>
		<br>
	</ffi:cinclude>
