<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

	<div id="detailsPortlet" class="portlet wizardSupportCls ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
        <div class="portlet-header ui-widget-header ui-corner-all">
        	<h1 class="portlet-title">Add Wires Transfer</h1>
		</div>
        <div class="portlet-content">

		    <sj:tabbedpanel id="TransactionWizardTabs" >
		    	
				
		        <sj:tab id="inputTab" target="inputDiv" label="%{getText('jsp.enter.details.label')}"/>
		        <sj:tab id="verifyTab" target="verifyDiv" label="%{getText('jsp.verify.details.label')}"/>
		        <sj:tab id="confirmTab" target="confirmDiv" label="%{getText('jsp.confirm.details.label')}"/>

		        <div id="inputDiv" style="border:1px;overflow:auto;">
					Enter Wires Information
		            <br><br>
		        </div>
		        <div id="verifyDiv" style="border:1px">
		            Verify Wires Information
		            <br><br>
		        </div>
		        <div id="confirmDiv" style="border:1px">
		            Confirm Wires Information
		            <br><br>
		        </div>
				
		    </sj:tabbedpanel>
			
		</div>
		<div id="wireDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	       		  <li><a href='#' onclick="ns.common.bookmarkThis('detailsPortlet')"><span class="sapUiIconCls icon-create-session" style="float:left;"></span>Bookmark</a></li>
	              <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
    </div>
<script>    
//Initialize portlet with settingsicon
ns.common.initializePortlet("detailsPortlet");
</script>
    