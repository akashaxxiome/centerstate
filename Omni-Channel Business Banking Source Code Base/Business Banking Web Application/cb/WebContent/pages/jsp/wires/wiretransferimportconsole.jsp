<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<div id="fileuploadPortlet">
	<div class="portlet-header" role="section" aria-labelledby="summaryHeader">
		<h1 id="summaryHeader" class="portlet-title"><s:text name="jsp.wire.file_import_summary"/>
		</h1>
	</div>
	<div class="portlet-content">
	    <div id="fileUploaderPanelID">
	    	<div id="selectImportFormatOfFileUploadID">
			</div>
	        <div id="selectFileFormAndStatusID" class="selectFilePane">
				<div id="selectFileOFFileUploadFormID"></div>
	        </div>
	    </div>
	</div>
	<div id="wirefileUploadDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('fileuploadPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
	</div>	
</div>

<script>
//Initialize portlet with settings & calendar icon
ns.common.initializePortlet("fileuploadPortlet");
</script>