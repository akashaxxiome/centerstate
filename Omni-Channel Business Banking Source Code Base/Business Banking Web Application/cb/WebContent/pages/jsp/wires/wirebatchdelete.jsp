<%--
This is the delete wire batch page.  It is used for all wire batch types,
except for International wire batches, which use wirebatchintdelete.jsp.

Pages that request this page
----------------------------
wire_transfers_list.jsp (Included in wiretransfers.jsp, the wire transfer summary page)
	Delete button next to a wire batch.

Pages this page requests
------------------------
DELETE BATCH requests wirebatchdeletesend.jsp
CANCEL requests wiretransfers.jsp

Pages included in this page
---------------------------
payments/inc/wire_set_wire.jsp
	Sets a wire transfer or wire batch with a given ID into the session
payments/include-view-transaction-details-constants.jsp
	A wrapper that includes common/include-view-transaction-details-constants.jsp,
	which contains common values used on the various View pages.
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<% boolean isSkip = Boolean.valueOf(request.getParameter("isSkip"));%>
<s:set var="isSkip" scope="request"><%=isSkip%></s:set>
<ffi:cinclude value1="${isSkip}" value2="true">
	<s:set id="deleteActionURL" value="%{'skipWireBatch'}" scope="request"></s:set>
</ffi:cinclude>
<ffi:cinclude value1="${isSkip}" value2="true" operator="notEquals">
	<s:set id="deleteActionURL" value="%{'deleteWireBatch'}" scope="request"></s:set>
</ffi:cinclude>
<ffi:help id="payments_wirebatchdelete" className="moduleHelpClass"/>
<ffi:setL10NProperty name='PageHeading' value='Delete Wire Batch'/>
<ffi:setProperty name='PageText' value=''/>

<%-- Set the wire batch into the session --%>
<%-- <s:include value="%{#session.PagesPath}/wires/inc/wire_set_wire.jsp"/>

<ffi:setProperty name="collectionToShow" value="WireBatch.Wires"/>
<ffi:cinclude value1="${collectionName}" value2="PendingApprovalWireTransfers" operator="notEquals">
    <ffi:object id="GetWiresByBatchId" name="com.ffusion.tasks.wiretransfers.GetWiresByBatchId" scope="session" />
        <ffi:setProperty name="GetWiresByBatchId" property="BatchId" value="${WireBatch.BatchID}"/>
        <ffi:setProperty name="GetWiresByBatchId" property="CollectionSessionName" value="WireTransfersByBatchId"/>
    <ffi:process name="GetWiresByBatchId"/>
    <ffi:setProperty name="collectionToShow" value="WireTransfersByBatchId"/>
</ffi:cinclude>
--%>
<s:include value="%{#session.PagesPath}/common/include-view-transaction-details-constants.jsp"/>
<ffi:setProperty name="WireBatch" property="DateFormat" value="${UserLocale.DateFormat}"/>
<div align="center" class="approvalDialogHt">
<div class="blockWrapper" role="form" aria-labelledby="batchInfoHeader">
	<div  class="blockHead"><h2 id="batchInfoHeader"><s:text name="jsp.default_65" /></h2></div>
	<div class="blockContent label130">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				 <span class="sectionsubhead sectionLabel"><s:text name="ach.grid.batchName" />:</span>
                 <span width="228" class="columndata"><ffi:getProperty name="WireBatch" property="BatchName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_67" />:</span>
                <% String batchDest = ""; %>
                <ffi:getProperty name="WireBatch" property="BatchDestination" assignTo="batchDest"/>
                <span class="columndata"><%-- <ffi:setProperty name="WireDestinations" property="Key" value="<%= batchDest %>"/> --%>
                <ffi:getProperty name="WireBatch" property="BatchDestination"/></span>
         
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				 <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_47" />:</span>
                 <span class="columndata"><ffi:getProperty name="WireBatch" property="BatchType"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><%= batchDest.equals(WireDefines.WIRE_HOST) ? "<!--L10NStart-->Processing Date<!--L10NEnd-->" : "<!--L10NStart-->Value Date<!--L10NEnd-->" %>:</span>
                <span class="columndata"><ffi:getProperty name="WireBatch" property="DueDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_141"/>:</span>
                <span class="columndata"><ffi:getProperty name="WireBatch" property="DateToPost"/></span>
			</div>
			<% if (batchDest.equals(WireDefines.WIRE_HOST)) { %>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_380"/>:</span>
                    <span class="columndata"><ffi:getProperty name="WireBatch" property="SettlementDate"/></span>
				</div>
			<% } %>
		</div>
	</div>
</div>

<div class="blockWrapper marginTop20" role="form" aria-labelledby="includedBatchesHeader">
	<div  class="blockHead"><h2 id="includedBatchesHeader"><s:text name="jsp.default_239"/></h2></div>
</div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr class="header">
                <td align="center" class="sectionsubhead">
                <% if (batchDest.equals(WireDefines.WIRE_HOST)) { %><s:text name="jsp.default_230"/>
                <% } else if (batchDest.equals(WireDefines.WIRE_DRAWDOWN)) { %><s:text name="jsp.default_486"/>
                <% } else { %><s:text name="jsp.default_68"/>
                <% } %>
                </td>
                <td class="sectionsubhead" align="left"><s:text name="jsp.default_388"/></td>
                <td class="sectionsubhead paddingRight10" align="right" width="5%"><s:text name="jsp.default_43"/></td>
            </tr>
            <ffi:list collection="WireBatch.Wires" items="wire">
            <tr>
            <ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
                <td align="center" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
            </ffi:cinclude>
            <ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
                <td align="center" class="columndata" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
                    <ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
            </ffi:cinclude>

            <td class="columndata" align="left" nowrap>
                <ffi:getProperty name="wire" property="StatusName"/>
            </td>

            <td class="columndata paddingRight10" align="right" nowrap><ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/></td>
            </tr>
            </ffi:list>
            
            <tr>
                <td class="columndata">&nbsp;</td>
                <td class="columndata" align="right" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                <td class="tbrd_t sectionsubhead" width="10%" align="right" valign="bottom" nowrap><ffi:getProperty name="WireBatch" property="Amount"/></td>
            </tr>
        </table>
	</div>
</div>
           <!--  <table width="750" cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td class="columndata ltrow2_color" align="center">
                    <table width="715" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="tbrd_b sectionhead">&gt; L10NStartDelete BatchL10NEnd</td>
                        </tr>
                        <tr>
                            <td class="sectionsubhead">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="sectionsubhead" align="center">L10NStartAre you sure you want to delete this batch?L10NEnd</td>
                        </tr>
                        <tr>
                            <td class="sectionsubhead">&nbsp;</td>
                        </tr>
                    </table>
                    <br> -->
                   <!-- <table width="750" cellpadding="0" cellspacing="0" border="0"><tr>
    <td class='<ffi:getProperty name="wirebatch_details_background_color"/>' align="center">
         <table width="98%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="left" class="tbrd_b sectionhead">&gt; L10NStartBatch InformationL10NEnd</td>
            </tr>
        </table>
        <br> -->
        <%-- <table width="715" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="357" valign="top">
                    <table width="355" cellpadding="3" cellspacing="0" border="0">
                        <tr>
                            <td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Batch Name<!--L10NEnd--></td>
                            <td align="left" width="228" class="columndata"><ffi:getProperty name="WireBatch" property="BatchName"/></td>
                        </tr>
                        <tr>
                            <td align="left" class="sectionsubhead"><!--L10NStart-->Batch Type<!--L10NEnd--></td>
                            <% String batchDest = ""; %>
                            <ffi:getProperty name="WireBatch" property="BatchDestination" assignTo="batchDest"/>
                            <td align="left" class="columndata"><ffi:setProperty name="WireDestinations" property="Key" value="<%= batchDest %>"/><ffi:getProperty name="WireDestinations" property="Value"/></td>
                        </tr>
                        <tr>
                            <td align="left" class="sectionsubhead"><!--L10NStart-->Application Type<!--L10NEnd--></td>
                            <td align="left" class="columndata"><ffi:getProperty name="WireBatch" property="BatchType"/></td>
                        </tr>
                    </table>
                </td>
                <td class="tbrd_l" width="10"><img src='<ffi:getProperty name="spacer_gif"/>' width="1" height="1" alt=""></td>
                <td width="348" valign="top">
                    <table width="347" cellpadding="3" cellspacing="0" border="0">
                        <tr>
                            <td align="left" width="115" class="sectionsubhead"><%= batchDest.equals(WireDefines.WIRE_HOST) ? "<!--L10NStart-->Processing Date<!--L10NEnd-->" : "<!--L10NStart-->Value Date<!--L10NEnd-->" %></td>
                            <td align="left" width="220" class="columndata"><ffi:getProperty name="WireBatch" property="DueDate"/></td>
                        </tr>
                        <% if (batchDest.equals(WireDefines.WIRE_HOST)) { %>
						<tr>
                            <td align="left" class="sectionsubhead"><!--L10NStart-->Settlement Date<!--L10NEnd--></td>
                            <td align="left" class="columndata"><ffi:getProperty name="WireBatch" property="SettlementDate"/></td>
                        </tr>
						<% } %>
                        <tr>
                            <td align="left" class="sectionsubhead"><!--L10NStart-->Date To Post<!--L10NEnd--></td>
                            <td align="left" class="columndata"><ffi:getProperty name="WireBatch" property="DateToPost"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table> 
        <table width="98%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="left" class="tbrd_b sectionhead">&gt; Included Batches</td>
            </tr>
        </table>--%>
        <%-- <table width="375" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td align="left" class="sectionsubhead">
                <% if (batchDest.equals(WireDefines.WIRE_HOST)) { %><!--L10NStart-->Host ID<!--L10NEnd-->
                <% } else if (batchDest.equals(WireDefines.WIRE_DRAWDOWN)) { %><!--L10NStart-->Drawdown Account<!--L10NEnd-->
                <% } else { %><!--L10NStart-->Beneficiary<!--L10NEnd-->
                <% } %>
                </td>
                <td align="left" class="sectionsubhead" align="left"><!--L10NStart-->Status<!--L10NEnd--></td>
                <td align="left" class="sectionsubhead" align="right" width="1%"><!--L10NStart-->Amount<!--L10NEnd--></td>
            </tr>
            <ffi:list collection="WireBatch.Wires" items="wire">
            <tr>
            <ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
                <td align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
            </ffi:cinclude>
            <ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
                <td align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
                    <ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
            </ffi:cinclude>

            <td class="columndata" align="left" nowrap>
                <ffi:getProperty name="wire" property="StatusName"/>
            </td>

            <td class="columndata" align="right" nowrap><ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/></td>
            </tr>
            </ffi:list>
            
            <tr>
                <td class="columndata">&nbsp;</td>
                <td class="columndata" align="right" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                <td class="tbrd_t sectionsubhead" width="1%" align="right" valign="bottom" nowrap><ffi:getProperty name="WireBatch" property="Amount"/></td>
            </tr>
        </table> 
        </td>
    </tr>
</table>--%>
                    <%-- <s:form action="/pages/jsp/wires/deleteWireBatch.action" method="post" name="FormName" id="deleteWireBatchFormId" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<s:hidden name="batchId" value="%{WireBatch.ID}" id="collectionName"/>
                    <sj:a 
						button="true" 
						onClickTopics="closeDialog" 
						title="Cancel"
						><s:text name="jsp.default_82" />
					</sj:a>
					<sj:a 
					        id="deleteWireBatch"
						formIds="deleteWireBatchFormId" 
						targets="resultmessage" 
						button="true"   
						title="Delete BATCH" 
						onCompleteTopics="deleteWireCompleteTopics" 
						onSuccessTopics="deleteWireSuccessTopics"
						onErrorTopics="deleteWireErrorTopics"
						effectDuration="1500" 
						><s:text name="jsp.default_165" />
					</sj:a>
                    </s:form> 
                    </td>
                </tr>
            </table>--%>
<div align="center" class="sectionhead marginTop20 marginBottom10" style="color:red">
<% if (isSkip) { %><!--L10NStart-->Are you sure you want to skip this batch?<!--L10NEnd--><% } else { %>
<!--L10NStart-->Are you sure you want to delete this batch?<!--L10NEnd-->
<% } %>
</div>
<s:form action="%{#request.deleteActionURL}" namespace="/pages/jsp/wires"  method="post" name="FormName" id="deleteWireBatchFormId" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden name="batchId" value="%{WireBatch.ID}" id="collectionName"/>
<div  class="ui-widget-header customDialogFooter">
<sj:a 
	button="true" 
	onClickTopics="closeDialog" 
	title="Cancel"
	><s:text name="jsp.default_82" />
</sj:a>
<% if (!isSkip) { %>
<sj:a 
    id="deleteWireBatch"
	formIds="deleteWireBatchFormId" 
	targets="resultmessage" 
	button="true"   
	title="Delete" 
	onCompleteTopics="deleteWireCompleteTopics" 
	onSuccessTopics="deleteWireSuccessTopics"
	onErrorTopics="deleteWireErrorTopics"
	effectDuration="1500" 
	><s:text name="jsp.default_162" />
</sj:a>
<% } else { %>
	<sj:a 
		id="skipWireLink" 
		formIds="deleteWireBatchFormId" 
		targets="resultmessage" 
		button="true"   
		title="Skip Wire Batch" 
		onCompleteTopics="skipWiresTransfersCompleteTopics" 
		onSuccessTopics="deleteWiresTransfersSuccessTopics" 
		onErrorTopics="deleteWiresTransfersErrorTopics" 
		effectDuration="1500"
		><s:text name="jsp.default.Skip_Wire_Batch" />
	</sj:a>
	<% } %>
</div>
</s:form>
</div>

