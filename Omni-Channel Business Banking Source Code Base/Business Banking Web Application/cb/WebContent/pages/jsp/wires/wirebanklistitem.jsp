<%--
This page is the wire bank search page which shows you the details of the bank
selected from the search results found on the previous page
(wirebanklistselect.jsp), and allows you to insert the values into the add/edit
wire transfer or add/edit wire beneficiary page that the SEARCH button was
clicked.

Pages that request this page
----------------------------
wirebanklistselect.jsp (bank lookup search results)
	Clicking on a bank name in the list

Pages this page requests
------------------------
INSERT BANK INFO requests the page that popped-up the banklookup window
	wireaddpayee.jsp
	wireaddpayeebook.jsp
	wireaddpayeedrawdown.jsp
	wiretransfernew.jsp
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/wire_labels.jsp"%>
<%@ page import="com.ffusion.beans.wiretransfers.*" %>

<ffi:help id="payments_wirebanklistitem" className="moduleHelpClass"/>
<%-- <ffi:removeProperty name="WireTransferBank"/> --%>
<%
if( request.getParameter("SetWireTransferBank.Id") != null){ session.setAttribute("wireTransferBankID", request.getParameter("SetWireTransferBank.Id"));}
if( request.getParameter("Inter") != null){ session.setAttribute("Inter", request.getParameter("Inter"));}
%>
<%-- <ffi:setProperty name="SetWireTransferBank" property="Id" value="${wireTransferBankID}" />
<ffi:process name="SetWireTransferBank"/>
--%>
<% String str = ""; String currentTask = ""; %>
<ffi:getProperty name="Inter" assignTo="str"/>
<ffi:getProperty name="currentTask" assignTo="currentTask"/>
	<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata ltrow2_color" align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<%-- <tr>
								<td><span class="sectionhead">&gt; <!--L10NStart-->Bank Lookup Results<!--L10NEnd--></span></td>
							</tr> --%>
							<tr>
								<td>
									<s:form name="wirebanklistitem.jsp" method="post" id="instertSelectedDestinationBankFormID">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
										<table width="100%" border="0" cellspacing="0" cellpadding="3">
											<tr>
												<td class="sectionsubhead" colspan="3" align="center">
												<span class="headerTitle">
													<ffi:getProperty name="WireTransferBank" property="BankName"/>
												</span>
												</td>
											</tr>
											<tr>
										    	<td colspan="3"><p class="transactionHeading"><s:text name="jsp.user_31"/></p></td>
										    </tr>
											<tr>
										    	<td colspan="3">
										    		<table width="100%" border="0" cellspacing="0" cellpadding="5">
											    		<tr>
													    	<td width="40%" class="labelCls">Street</td>
													    	<td width="20%" class="labelCls"><s:text name="jsp.default_101"/></td>
													    	<td width="20%" class="labelCls"><s:text name="jsp.default_386"/></td>
													    	<td width="20%" class="labelCls"><s:text name="jsp.default_115"/></td>
													    </tr>
												    </table>	
										    	</td>
									    	</tr>
									    	<tr>
									    		<td colspan="3">
										    		<table width="100%" border="0" cellspacing="0" cellpadding="5">
											    		<tr>
												    		<td width="40%">
																<span class="columndata valueCls">
																	<ffi:cinclude value1="${WireTransferBank.Street}" value2="" operator="notEquals"><ffi:getProperty name="WireTransferBank" property="Street"/></ffi:cinclude>
																	<ffi:cinclude value1="${WireTransferBank.ZipCode}" value2="" operator="notEquals">,&nbsp;<ffi:getProperty name="WireTransferBank" property="ZipCode"/></ffi:cinclude>
																</span>
															</td>
													    	<td width="20%" valign="top">
																<span class="columndata valueCls">
																	<ffi:cinclude value1="${WireTransferBank.City}" value2="" operator="notEquals"><ffi:getProperty name="WireTransferBank" property="City"/></ffi:cinclude>
																</span>
															</td>
													    	<td width="20%" valign="top">
																<span class="columndata valueCls">
																	<ffi:cinclude value1="${WireTransferBank.City}" value2="" operator="notEquals"><ffi:getProperty name="WireTransferBank" property="City"/></ffi:cinclude>
																	<ffi:cinclude value1="${WireTransferBank.State}" value2="" operator="notEquals">,&nbsp;<ffi:getProperty name="WireTransferBank" property="State"/></ffi:cinclude>
																</span>
															</td>
													    	<td width="20%" valign="top">
																<span class="columndata">
																	<ffi:cinclude value1="${WireTransferBank.Country}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransferBank" property="Country"/></ffi:cinclude>
																</span>
															</td>
														</tr>
												    </table>	
										    	</td>	
										    </tr>
									    	<tr>
										    	<td colspan="3" height="20">&nbsp;</td>
										    </tr>
										    <tr>
										    	<td colspan="3"><p class="transactionHeading"><s:text name="admin.banklookup.identification.codes"/></p></td>
										    </tr>
										    <tr>
										    	<td colspan="3">
											    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
												    	<tr>
													    	<td width="20%"><s:text name="jsp.default_201"/></td>
													    	<td width="20%">SWIFT</td>
													    	<td width="20%"><s:text name="jsp.default_99"/></td>
													    	<td width="20%"><s:text name="jsp.default_289"/></td>
													    	<td width="20%">IBAN</td>
													    </tr>
												    </table>
											    </td>	
										    </tr>
										    <tr>
										    	<td colspan="4">
											    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
											    		<tr>
															<td class="columndata" width="20%"><ffi:getProperty name="WireTransferBank" property="RoutingFedWire"/></td>
															<td class="columndata" width="20%"><ffi:getProperty name="WireTransferBank" property="RoutingSwift"/></td>
															<td class="columndata" width="20%"><ffi:getProperty name="WireTransferBank" property="RoutingChips"/></td>
															<td class="columndata" width="20%"><ffi:getProperty name="WireTransferBank" property="RoutingOther"/></td>
															<td class="columndata" width="20%"><ffi:getProperty name="WireTransferBank" property="IBAN"/></td>
														</tr>
												    </table>
											    </td>	
										    </tr>
									    	
											<%-- <tr>
												<td class="columndata" width="49%" valign="top">
													<ffi:cinclude value1="${WireTransferBank.Street}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransferBank" property="Street"/></ffi:cinclude>
													<ffi:cinclude value1="${WireTransferBank.City}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransferBank" property="City"/></ffi:cinclude>
													<ffi:cinclude value1="${WireTransferBank.State}" value2="" operator="notEquals">,&nbsp;<ffi:getProperty name="WireTransferBank" property="State"/></ffi:cinclude>
													<ffi:cinclude value1="${WireTransferBank.ZipCode}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransferBank" property="ZipCode"/></ffi:cinclude>
													<ffi:cinclude value1="${WireTransferBank.Country}" value2="" operator="notEquals"><br><ffi:getProperty name="WireTransferBank" property="Country"/></ffi:cinclude>
													</td>
												<td class="tbrd_l" nowrap width="1"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="50" border="0"></td>
												<td class="columndata" align="right" width="49%" nowrap><br>
													<table align="right" cellpadding="0" cellspacing="0" border="0">
														<tr>
															<td class="columndata" align="right"><!--L10NStart-->FED ABA:<!--L10NEnd--></td>
															<td class="columndata"><ffi:getProperty name="WireTransferBank" property="RoutingFedWire"/></td>
														</tr>
														<tr>
															<td class="columndata" align="right"><%= LABEL_SWIFT %></td>
															<td class="columndata"><ffi:getProperty name="WireTransferBank" property="RoutingSwift"/></td>
														</tr>
														<tr>
															<td class="columndata" align="right"><%= LABEL_CHIPS %></td>
															<td class="columndata"><ffi:getProperty name="WireTransferBank" property="RoutingChips"/></td>
														</tr>
														<tr>
															<td class="columndata" align="right"><%= LABEL_NATIONAL %></td>
															<td class="columndata"><ffi:getProperty name="WireTransferBank" property="RoutingOther"/></td>
														</tr>
														<tr>
															<td class="columndata" align="right"><%= LABEL_IBAN %></td>
															<td class="columndata"><ffi:getProperty name="WireTransferBank" property="IBAN"/></td>
														</tr>
													</table>
												</td>
											</tr> --%>
											<tr>
												<td colspan="3"><br></td>
											</tr>
											<tr>
												<td colspan="3" align="center" class="ui-widget-header customDialogFooter">
													<input type="hidden" name="insertInfo" value="INSERT BANK INFO"/>
													<input type="hidden" name="SetWireTransferBank.Id" value="<ffi:getProperty name='wireTransferBankID'/>"/>
													<input type="hidden" name="Inter" value="<ffi:getProperty name='Inter'/>"/>
													<input type="hidden" name="isBankSearch" value="<ffi:getProperty name='isBankSearch'/>"/>
													<input type="hidden" name="bankLookupRaturnPage" id="bankLookupRaturnPage" value="<ffi:getProperty name='bankLookupRaturnPage'/>"/>
													<input type="hidden" id="manageBeneficiary" name="manageBeneficiary" value="<ffi:getProperty name='manageBeneficiary'/>"/>
                    								<input type="hidden" name ="returnPage" id="returnPage" value="<ffi:getProperty name='returnPage'/>"/>
													<%--<input class="submitbutton" name="insertInfo" type="submit" value="INSERT BANK INFO" border="0">&nbsp;&nbsp;<input class="submitbutton" type="button" value="CLOSE" onclick="self.close();return false;">--%>
													<sj:a id="insertSelectedBankForm" 
														button="true" 
														onClickTopics="insertSelectedBankFormClikTopics"
													><s:text name="jsp.default_250" /><!-- INSERT BANK INFO -->
													</sj:a>
													&nbsp;&nbsp;
													<sj:a id="cancelSelectedBankForm" 
														button="true" 
														onClickTopics="closeSelectedBankFormClikTopics"
													><s:text name="jsp.default_102" /><!-- CLOSE -->
													</sj:a>
												</td>
											</tr>
										</table>
				    &nbsp;
									</s:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
<ffi:removeProperty name="Inter"/>
<ffi:removeProperty name="wireTransferBankID"/>