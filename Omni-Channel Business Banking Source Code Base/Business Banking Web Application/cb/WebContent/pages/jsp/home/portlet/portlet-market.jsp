<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_switchToEdit() {
		$('#<s:property value="portletId"/>').portlet('edit');
	}
	
	ns.home.renderMarketSettings = function(){
		var aConfig = {
			url : "/cb/pages/jsp/home/MarketPortletAction_loadEditMode.action",
			data: {CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>',portletId:'<s:property value="portletId"/>'},
			success: function(data) {
				$('#marketSettings').html("");
				$('#marketSettings').html(data);
				$("#marketDataContainer").hide();
				$("#marketSettings").show();
			}
		};
		 
		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}
</script>

<ffi:help id="home_portlet-market" />

<div id="marketDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick="ns.home.renderMarketSettings()"><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<div id="marketSettings" class="hidden"></div>  
<div id="marketDataContainer">
	<table border="0" cellspacing="0" cellpadding="2" width="100%" class="marginBottom5">
		<%-- Since StockIndexes does not support the Size property, we use the following to determine if it's empty --%>
		<ffi:setProperty name="hasMarketItems" value="false"/>
		<ffi:setProperty name="hasValidMarketItemData" value="false"/>
		<ffi:list collection="StockIndexes" items="item">
			<ffi:cinclude value1="N.a." value2="${item.IndexName}" operator="notEquals">
				<ffi:setProperty name="hasValidMarketItemData" value="true"/>
				<ffi:setProperty name="hasMarketItems" value="true"/>
			</ffi:cinclude>
		</ffi:list>
	
		<%-- IF StockIndexes COLLECTION IS EMPTY --%>
		<ffi:cinclude value1="false" value2="${hasMarketItems}" operator="equals">
			<ffi:cinclude value1="false" value2="${hasValidMarketItemData}" operator="equals">
				<tr>
					<td align="center" height="20">
						<br><br><br><br><br>
						<s:text name="jsp.home_141"/><br>
						<s:text name="jsp.home_56"/> <a href='javascript:void(0)' class="sapUiLnk" onclick='ns.home.renderMarketSettings();'><s:text name="jsp.default_179"/></a> <s:text name="jsp.home_216"/>
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude value1="false" value2="${hasValidMarketItemData}" operator="notEquals">
				<tr>
					<td align="center" height="20">
						<s:text name="jsp.home_132"/> <br>
						<s:text name="jsp.home_56"/> <a href='javascript:void(0)' onclick='renderMarketSettings();'><s:text name="jsp.default_179"/></a> <s:text name="jsp.home_216"/>
					</td>
				</tr>
			</ffi:cinclude>
		</ffi:cinclude>
	
	
		<%-- IF StockIndexes COLLECTION IS NOT EMPTY --%>
		<ffi:cinclude value1="true" value2="${hasMarketItems}" operator="equals">
			<ffi:list collection="StockIndexes" items="Index1">
				<tr>
					<td style="width: 2"></td>
					<td valign="top"><ffi:getProperty name="Index1" property="IndexName"/>&nbsp;&nbsp;</td>
					<td align="right" valign="top">&nbsp;&nbsp;<ffi:getProperty name="Index1" property="Value"/></td>
					<ffi:setProperty name="StringUtil" property="Find" value="-" />
					<ffi:setProperty name="StringUtil" property="Replace" value="" />
					<ffi:setProperty name="StringUtil" property="Value1" value="${Index1.ChangePrice}" />
					<ffi:setProperty name="Compare" property="Value1" value="${Index1.ChangePrice}" />
					<ffi:setProperty name="Compare" property="Value2" value="${StringUtil.String}" />
					<td align="right" valign="top" nowrap>
						<span style="color:<ffi:getProperty name="positive${Compare.Equals}"/>;">
							&nbsp;&nbsp;<ffi:getProperty name="Index1" property="ChangePrice"/>
						</span>
					</td>
					<td class="columndata" style="width: 2"></td>
				</tr>
			</ffi:list>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td>&nbsp;</td>
				<td class="columndata" colspan="3" height="35" align="center">
					<s:text name="jsp.home_70"/>&nbsp;&nbsp;<br>
					<s:text name="jsp.home_111"/> <ffi:setProperty name="GetCurrentDate" property="DateFormat" value="M/d/yyyy h:mm a (z)" />
				<ffi:getProperty name="GetCurrentDate" property="Date"/></td>
				<td>&nbsp;</td>
			</tr>
		</ffi:cinclude>
	</table>
</div>