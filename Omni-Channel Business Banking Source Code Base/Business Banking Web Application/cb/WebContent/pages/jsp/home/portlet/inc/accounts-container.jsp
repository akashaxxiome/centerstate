<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.accounts.AccountTypes"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummaries"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummary"%>
<%@ page import="com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.accounts.AssetAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.DepositAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.LoanAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.CreditCardAcctSummary"%>
<%@ page import="com.ffusion.beans.common.Currency"%>
<%@ page import="java.util.Collection"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
 <%@ page contentType="text/html; charset=UTF-8" %>
<% session.setAttribute("displayCurrencyCode",request.getParameter("displayCurrencyCode")); %>

<ffi:cinclude value1="${GetTargetCurrencies}" value2="" operator="equals">
<ffi:object name="com.ffusion.tasks.fx.GetTargetCurrencies" id="GetTargetCurrencies" scope="session"/>
</ffi:cinclude>

<ffi:process name="GetTargetCurrencies"/>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<s:include value="accounts-top-container.jsp"/>
</ffi:cinclude>

<div id="accountSummaryAccordion">

	<h3 id="0" onclick="loadAccountSummary($(this));" contentLoaded="true" data-sap-ui-banking-panel= "internal"><s:text name="jsp.account.internalAccount"></s:text></h3>
	<div>
		<div id="internalAccountsData">
		   <s:include value="internal-accounts-grid.jsp" />
		</div>
		<div id="internalAccountsNoData" class="hidden">
			<s:text name="jsp.default.message.no_internal_accounts"/>
		</div>
	</div>
	
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_AGGREGATION%>">
		<h3 id="1" onclick="loadAccountSummary($(this));" data-sap-ui-banking-panel= "external"><s:text name="jsp.account.externalAccount"></s:text></h3>
		<div>
			<div id="externalAccountsData">
			   <s:include value="external-accounts-grid.jsp" />
			</div>
			<div id="externalAccountsNoData" class="hidden">
				<s:text name="jsp.default.message.no_external_accounts"/>
			</div>
		</div>
	</ffi:cinclude>
</div>

<s:include value="accountSummaryTotalsGrid.jsp" />

<script type="text/javascript">
$(document).ready(function(){
	
	$("#accountSummaryAccordion").accordion({
        heightStyle: "content",
        create: function( event, ui ) {
        	ns.common.resizeWidthOfGrids();
        },
        activate: function( event, ui ) {
		var totalGrid =  $('#accountsSummaryTotalUrlID');
		var panelName = ui.newHeader.attr('data-sap-ui-banking-panel');
		if(panelName == "external"){
		totalGrid.jqGrid('hideCol','currencyCodeInternal');
		totalGrid.jqGrid('hideCol','displayCurrentBalanceInternal');
		totalGrid.jqGrid('showCol','displayCurrentBalanceExternal');
		totalGrid.jqGrid('showCol','currencyCodeExternal');
		}else if(panelName == "internal"){
		totalGrid.jqGrid('hideCol','currencyCodeExternal');
		totalGrid.jqGrid('hideCol','displayCurrentBalanceExternal');
		totalGrid.jqGrid('showCol','currencyCodeInternal');
		totalGrid.jqGrid('showCol','displayCurrentBalanceInternal');
		}
		
		
        	ns.common.resizeWidthOfGrids();
        }
	});

});

function loadAccountSummary(header){
	var contentLoaded = $(header).attr('contentLoaded');
	//Bypass the grid reload call if the Grid already has data
	if(contentLoaded != 'true'){
		var gridElement= $(header).next().find(".ui-jqgrid");
		if(gridElement){
			gridId = $(gridElement).attr("id").substr(5);
			ns.common.reloadFirstGridPage('#'+gridId);
			ns.common.resizeWidthOfGrids();
			$(header).attr('contentLoaded','true');//Set the content as loaded, so as to avoid loading next time.
		}
	}
}

</script>
