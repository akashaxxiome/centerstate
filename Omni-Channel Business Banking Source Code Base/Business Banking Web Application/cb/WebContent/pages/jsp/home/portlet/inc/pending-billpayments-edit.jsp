<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/billpay/billpay%{#session.minVersion}.css'/>"></link>
<div id="operationresult">
	<div id="resultmessage"><s:text name="jsp.default_498"/></div>
</div><!-- result -->

<%-- Include JSP to perform initial data setting. This JSP has only processing logic and no HTML--%>
<s:include value="/pages/jsp/billpay/billpayeditinit.jsp"></s:include>

<%-- Setting property in Session to evaluate it in <ffi:cinclude> tag. --%>
<s:set var="fromPendingPayments" value="true" scope="session"></s:set>

<%-- This JSP is responsible for  --%>
<s:include value="/pages/jsp/home/portlet/inc/pendingTransactionNavigation.jsp"></s:include>
