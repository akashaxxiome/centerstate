<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<span><s:text name="jsp.default_101"/></span>
<br/>

<ffi:cinclude value1="${COUNTRY_CITIES_FOUND}" value2="true" operator="notEquals" >
	<span class="txtbox" style="border: 0px" ><s:text name="jsp.home_128"/></span>
</ffi:cinclude>

<ffi:cinclude value1="${COUNTRY_CITIES_FOUND}" value2="true" operator="equals" >
<table>
	<tr>
		<td>
		    <select class="txtbox" id="<s:property value="portletId"/>_forecastCountryCities">
			    <ffi:list collection="ForecastCountryCities" items="City">
				    <ffi:setProperty name="Compare" property="Value1" value="${City.Abbr}" />
			        <ffi:setProperty name="Compare" property="Value2" value="${ForecastCountryCities.Selected}" />
		
				    <option value="<ffi:getProperty name="City" property="Key"/>" <ffi:getProperty name="selected${Compare.Equals}"/> >
				        <ffi:getProperty name="City" property="Name"/>
			        </option>
			    </ffi:list>
		    </select>
		</td>
    	<td class="dvcPlusIconTd">
    		<a href="javascript:void(0)" onclick="<s:property value="portletId"/>_getCountryInit()"><span class="ui-icon ui-icon-circle-plus"/></a>
		</td>
    </tr>
</table>
</ffi:cinclude>

<script type="text/javascript">
function <s:property value="portletId"/>_getCountryInit()
{
	var idSelector = '#<s:property value="portletId"/>';
	var selectedStateValue = $('#<s:property value="portletId"/>_forecastStates').val();
	var selectedCountryValue = $('#<s:property value="portletId"/>_forecastCountries').val();
	var selectedCountryCityValue = $('#<s:property value="portletId"/>_forecastCountryCities').val();
	var selectedCities = <s:property value="portletId"/>_getCheckedItems();
	var selectedScale = <s:property value="portletId"/>_getScale();
	
	//To avoid if user selects 'Type to search' option due to bug in widget
	if(selectedCountryCityValue == '-1'){
		return false;
	}
	
	var aConfig = {
			type: "POST",
			url: '<s:url action="ForecastPortletAction_selectCountryCity"/>',
			data: {portletId: '<s:property value="portletId"/>', selectState: selectedStateValue, selectCountry: selectedCountryValue, selectCountryCity: selectedCountryCityValue, cities: selectedCities, scale:selectedScale, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
			success:function(data){
				$(idSelector).html(data);
			}
		};

	var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
	$.ajax(ajaxSettings);
}
</script>