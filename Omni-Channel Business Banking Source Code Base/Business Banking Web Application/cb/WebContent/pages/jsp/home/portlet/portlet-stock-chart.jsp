<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="<s:property value="portletId"/>_stockSymbol"><s:property value="symbol"/></div>
<script type="text/javascript">
	//$('select').selectmenu({width:'150'}).css('z-index','1');
</script>

<%-- ================ MAIN CONTENT START ================ --%>
<ffi:help id="home_portlet-stock-chart"/>
<ffi:setProperty name="PortalStockChartSymbol" value="${StockLookup.Symbol}"/>
<ffi:cinclude value1="" value2="${PortalStockChartPeriod}" operator="equals" >
	<ffi:setProperty name="PortalStockChartPeriod" value="3mon"/>
</ffi:cinclude>

<table border="0" cellspacing="0" cellpadding="0" style="text-align: center" width="100%">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="sectionhead" align="center">
			<ffi:getProperty name="StockLookup" property="CompanyName"/> (<ffi:getProperty name="StockLookup" property="Symbol"/>)
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="text-align: center">
			<ffi:setProperty name="StringUtil" property="Value1" value="${StockLookup.Symbol}" />
			<ffi:setProperty name="StringUtil" property="Value2" value="1" />
			<IMG SRC="http://www.hfn.newsalert.com/bin/chartsnip?cs=400x150&Symbol=<ffi:getProperty name="PortalStockChartSymbol"/>&ChartType=<ffi:getProperty name="PortalStockChartPeriod"/>&redirect=1" BORDER="0" WIDTH="400" HEIGHT="150">
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="sectionsubhead" style="text-align: center" height="20px"><s:text name="jsp.home_180"/></td>
					</tr>
					<tr>
						<td style="text-align: center">
							<ffi:setProperty name="StringUtil" property="Value1" value="${PortalStockChartPeriod}" />
							<select id="<s:property value='portletId'/>_stock_chart_period_select" class="txtbox" name="PortalStockChartPeriod" style="vertical-align: middle" <%--onChange="location.replace('<ffi:getProperty name="SecurePath"/>inc/portal/portal-stock-chart.jsp?PortalStockChartPeriod='+this.options[this.selectedIndex].value);"--%>>
								<ffi:setProperty name="StringUtil" property="Value2" value="1day" />
								<option value="1day" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_7"/></option>

								<ffi:setProperty name="StringUtil" property="Value2" value="1mon" />
								<option value="1mon" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_8"/></option>

								<ffi:setProperty name="StringUtil" property="Value2" value="3mon" />
								<option value="3mon" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_13"/></option>

								<ffi:setProperty name="StringUtil" property="Value2" value="6mon" />
								<option value="6mon" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_18"/></option>

								<ffi:setProperty name="StringUtil" property="Value2" value="Ytd" />
								<option value="Ytd" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_236"/></option>

								<ffi:setProperty name="StringUtil" property="Value2" value="1year" />
								<option value="1year" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_9"/></option>

								<ffi:setProperty name="StringUtil" property="Value2" value="2year" />
								<option value="2year" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_12"/></option>

								<ffi:setProperty name="StringUtil" property="Value2" value="3year" />
								<option value="3year" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_14"/></option>

								<ffi:setProperty name="StringUtil" property="Value2" value="5year" />
								<option value="5year" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_15"/></option>

								<ffi:setProperty name="StringUtil" property="Value2" value="10year" />
								<option value="10year" <ffi:getProperty name="selected${StringUtil.Equals}"/>><s:text name="jsp.home_10"/></option>
							</select>
						</td>
					</tr>
					<tr>
						<td></td>
					</tr>
				</table>
		</td>
	</tr>
</table>
<%-- ================= MAIN CONTENT END ================= --%>

<div id="stockPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick='<s:property value="portletId"/>_switchToEdit()'><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>