<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_send() {
		var idSelector = '#<s:property value="portletId"/>';
		
		var styleValue = $(idSelector + '_chart-style').val();
		$.get('<s:url action="ReversePositivePayPortletAction_updateChartStyle.action"/>', {portletId: '<s:property value="portletId"/>', chartStyle:styleValue}, function(data){
			$(idSelector).portlet('hideCtrlIcon','.view');
			$(idSelector).portlet('showCtrlIcon','.edit');
			$(idSelector).html(data);
			ns.home.bindPortletHoverToTable($(idSelector));
		});
	}
	$('#<s:property value="portletId"/>_anchor').button({
		icons: {}
	});
</script>
<ffi:help id="home_portlet-reversepositive-pay-edit"/>
<table>
	<tr>
		<td>
			<span style="font-weight:bold;">
				<s:text name="jsp.home_53"/>
			</span>
		</td>
		<td>
			<span>
				
					<select name="chartStyle" id="<s:property value="portletId"/>_chart-style">
						<option value="pie" selected><s:text name="jsp.home_154"/></option>
						<option value="bar"><s:text name="jsp.home_42"/></option>
						<option value="area"><s:text name="jsp.home_38"/></option>
						<option value="line"><s:text name="jsp.default_542"/></option>
					</select>
					&nbsp;
			</span>
		</td>
		<td>
			<a id="<s:property value="portletId"/>_anchor" href="javascript:void(0)" onclick="<s:property value="portletId"/>_send()" ><s:text name="jsp.default_367"/></a>
		</td>
	</tr>
	
</table>