<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="${StockLookupForPortlet}" value2="" operator="equals">
		<ffi:object id="StockLookupForPortlet" name="com.ffusion.tasks.portal.StockLookup" scope="session"/>
</ffi:cinclude>

<%
	session.setAttribute("FFIStockLookupForPortlet", session.getAttribute("StockLookupForPortlet"));
%>


<ffi:setProperty name="populateStockPortletDataURL" value="/cb/pages/jsp/home/StockPortletAction_loadViewDetailsMode.action" URLEncrypt="true"/>


<div id="stockPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick='<s:property value="portletId"/>_switchToEdit()'><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<div id="stockPortletDataDiv">
	<s:action name="StockPortletAction_loadViewDetailsMode" namespace="/pages/jsp/home" executeResult="true">
	 	<s:param name="portletId" value="%{#portletId}" />
	 </s:action>
</div>