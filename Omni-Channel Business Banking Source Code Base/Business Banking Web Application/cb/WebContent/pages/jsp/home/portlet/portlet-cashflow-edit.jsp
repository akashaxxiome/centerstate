<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<ffi:help id="home_portlet-cashflow"/>
<style>
	#<s:property value="portletId"/>_availAccounts, #<s:property value="portletId"/>_chartAccounts { }
	#<s:property value="portletId"/>_availAccounts div, #<s:property value="portletId"/>_chartAccounts div { }
</style>

<script type="text/javascript">
var availableCashflowAccStr = String('<s:property value="chartAccountIDs"/>');
	$('#<s:property value="portletId"/>_combobox_styles').selectmenu({width:'101'});
	$('#<s:property value="portletId"/>_combobox_dimension').selectmenu({width:'120'});
	
	$('#<s:property value="portletId"/>_anchor1').button({
		icons: {}
	});
	
	$('#<s:property value="portletId"/>_anchor2').button({
		icons: {}
	});
	
	function <s:property value="portletId"/>_changeDisplayStyle() {
		var type = $('#<s:property value="portletId"/>_combobox_styles').val();
		if(type) {
			if(type == 'Table') {
				$('#<s:property value="portletId"/>_dimension_option1').hide();
				$('#<s:property value="portletId"/>_dimension_option2').hide();
				$('#<s:property value="portletId"/>_dimension_option3').hide();
				$('#<s:property value="portletId"/>_anchor2').hide();
				$('#<s:property value="portletId"/>_table_save').show();
			} else {
				if(type == 'pie'){
					$('#<s:property value="portletId"/>_dimension_option1').hide();
					$('#<s:property value="portletId"/>_dimension_option3').show();
				}else{
					$('#<s:property value="portletId"/>_dimension_option1').show();
					$('#<s:property value="portletId"/>_dimension_option3').hide();
				}
				$('#<s:property value="portletId"/>_dimension_option2').show();
				$('#<s:property value="portletId"/>_table_save').hide();
				$('#<s:property value="portletId"/>_anchor2').show();
			}
		}
	}
	
	function <s:property value="portletId"/>_save() {
		$('#barError').addClass('hidden');
		// portlet id
		var idSelector = '#<s:property value="portletId"/>';
		// id of display type i.e. bar pie of data table
		var type = $(idSelector + '_combobox_styles').val();
		// get display dimension data
		var dimension="";
		if(type=='bar'){
			$('input:checkbox[name="displayDimension"]:checked').each(function(){
				if(dimension!=""){
					dimension=dimension+","+$(this).val();
				}else{
					dimension=$(this).val();
				}
			});
		}
		
		if(type=='pie'){
			dimension=$(idSelector+'_combobox_dimension').val();
		}
		
		// check if for bar chart selection is made of not
		var isValid = true;
		if(type=="bar" && dimension==""){
			isValid=false;
			$('#barError').removeClass('hidden');
		}
		
		// if condition is valid then only make the call
		if(isValid){
			availableCashflowAccStr = $("#<s:property value="portletId"/>_availAccounts").find(".ui-state-default").map(function() {
	             if($(this).find('.toggle-off').hasClass('active')){
	                    return $(this).attr('id');
	             }
	      }).get().join(",");
		 
		 var aConfig = {
             //URL Encryption: Change "get" method to "post" method
             url:'<s:url action="CashFlowPortletAction_updateSettings.action"/>',
             type:"POST",
             containerId: '<s:property value="portletId"/>',
             data: {portletId: '<s:property value="portletId"/>', 
 				displayType:type, 
				displayDimension: dimension, 
				chartAccountIDs:availableCashflowAccStr, 
				CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'}, 
             success:function(data){
          	   if(type=='pie'){
          		 ns.home.changeCashFlowPortletView('donut');
          	   }else if(type=='bar'){
          		 ns.home.changeCashFlowPortletView('bar');
          	   }else if(type=='Table'){
          		 ns.home.changeCashFlowPortletView('dataTable');
          	   }
             }
	       };
	       
			//Portlet calls should be non blocking local ajax
			var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
			$.ajax(ajaxSettings);
		}
	}
	
</script>

<table style="margin-bottom:10px;" border="0" width="100%" cellspancing="1" cellpadding="0">
	<tr>
		<td align="left" style="font-weight:bold;padding-left:5px;" valign="top" width="150"><span id="DisplayStyle" ><s:text name="jsp.home_80"/></span></td><!-- Display styles: -->
		<td style="padding-left:15px" align="left" valign="top"> <!-- Display available chart styles  -->
			<select id="<s:property value="portletId"/>_combobox_styles" onchange="<s:property value="portletId"/>_changeDisplayStyle()">
				<option value="pie" <ffi:cinclude value1="${CashFlowSummarySettings.displayType}" value2="pie" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_257"/></option>
				<option value="bar" <ffi:cinclude value1="${CashFlowSummarySettings.displayType}" value2="bar" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_43"/></option>
				<option value="Table" <ffi:cinclude value1="${CashFlowSummarySettings.displayType}" value2="Table" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_72"/></option>
			</select>
		</td>
	</tr>
	<tr>
		<td valign="top" colspan="2">
			<div align="left" id="<s:property value="portletId"/>_table_save">
				<span><a id="<s:property value="portletId"/>_anchor1" href="javascript:void(0)" onclick="<s:property value="portletId"/>_save()" ><s:text name="jsp.default_303"/></a></span>
			</div>
			<!--  has data for bar chart -->
			<table id="<s:property value="portletId"/>_dimension_option1">
				<tr>
					<td valign="top" align="left" style="font-weight:bold;" width="135"><s:text name="jsp.home.measure"/> </td> <!-- Display dimension for Bar Chart-->
					<td align="left">
					<%-- 	 <select id="<s:property value="portletId"/>_combobox_styles">
							<option value="openBal" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="openBal" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_146"/></option>
							<option value="totalDebits" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="totalDebits" operator="equals">selected</ffi:cinclude>><s:text name="jsp.default_434"/></option>
							<option value="totalCredits" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="totalCredits" operator="equals">selected</ffi:cinclude>><s:text name="jsp.default_433"/></option>
							<option value="closeBal" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="closeBal" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_57"/></option>
							<option value="items" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="items" operator="equals">selected</ffi:cinclude>><s:text name="jsp.default_1"/></option>
						</select> --%>
							<div style="margin-bottom:10px;" class="inlineDiv"><input name="displayDimension" type="checkbox" value="openBal"
							<s:if test="%{#session.CashFlowSummarySettings.displayDimension.indexOf('openBal')>-1}">
							checked
							</s:if>/><s:text name="jsp.cash.openBal"/>
							</div>
							<div style="margin-bottom:10px;" class="inlineDiv">
							<input name="displayDimension" type="checkbox" value="totalDebits"
							<s:if test="%{#session.CashFlowSummarySettings.displayDimension.indexOf('totalDebits')>-1}">
							checked
							</s:if>/><s:text name="jsp.cash.totDebits"/>
							</div>
							<div style="margin-bottom:10px;" class="inlineDiv">
							<input name="displayDimension" type="checkbox" value="totalCredits"
							<s:if test="%{#session.CashFlowSummarySettings.displayDimension.indexOf('totalCredits')>-1}">
							checked
							</s:if>/><s:text name="jsp.cash.totCredits"/>
							</div>
							<div style="margin-bottom:10px;" class="inlineDiv">
							<input name="displayDimension" type="checkbox" value="closeBal"
							 <s:if test="%{#session.CashFlowSummarySettings.displayDimension.indexOf('closeBal')>-1}">
							checked
							</s:if>/><s:text name="jsp.cash.closeBal"/>
							</div>
							<div style="margin-bottom:10px;" class="inlineDiv">
							<input name="displayDimension" type="checkbox" value="items"
							<s:if test="%{#session.CashFlowSummarySettings.displayDimension.indexOf('items')>-1}">
							checked
							</s:if>/><s:text name="jsp.cash.totTransactions"/>
							</div>
					</td>
				</tr>
			</table>
			<!--  has data for pie chart -->
			<table id="<s:property value="portletId"/>_dimension_option3">
				<tr>
					<td valign="top" align="left" style="font-weight:bold;" width="135"><s:text name="jsp.home_78"/></td> <!-- Display dimension for pie chart-->
					<td>
						<select id="<s:property value="portletId"/>_combobox_dimension">
							<option value="openBal" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="openBal" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_146"/></option>
							<option value="totalDebits" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="totalDebits" operator="equals">selected</ffi:cinclude>><s:text name="jsp.default_434"/></option>
							<option value="totalCredits" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="totalCredits" operator="equals">selected</ffi:cinclude>><s:text name="jsp.default_433"/></option>
							<option value="closeBal" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="closeBal" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_57"/></option>
							<option value="items" <ffi:cinclude value1="${CashFlowSummarySettings.displayDimension}" value2="items" operator="equals">selected</ffi:cinclude>><s:text name="jsp.default_1"/></option>
						</select>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
	<td colspan="3">
		<span class="hidden red" id="barError">
				Please select atleast one type to display on Chart
		</span>
	</td>
	</tr>
</table>
<div id="<s:property value="portletId"/>_dimension_option2">

		<span style="font-weight:bold"><s:text name="jsp.home_181"/></span> <!-- Select accounts to display on chart -->
		<table border="0" width="90%" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td height="5"></td>
                        </tr>
                        <tr>
                           <td>
                              <span><s:text name="jsp.home_40"/></span>
                           </td>
                           <%-- <td>&nbsp;</td>
                           <td>
                               <span style="font-weight:bold"><s:text name="jsp.home_23"/></span>
                           </td>
                           <td>&nbsp;</td> --%>
                        </tr>
                        <tr>
                           <td width="100%" valign="top" align="left">
                                <div id="<s:property value="portletId"/>_availAccounts" class="<s:property value="portletId"/>_connectedSortable ui-widget ui-widget-content ui-corner-all cashflowAccountsWrapper">
                                	<div class="ui-state-disabled" style="height:0px"></div>
                                   <%-- 	<ffi:list collection="CashFlowAccountSummaries" items="Account">
	                                   	<div id="<ffi:getProperty name="Account" property="AccountID"/>" class="ui-state-default">
	                                   		<span class="sapUiIconCls icon-sort"></span>
	                                   		<span>
	                                        	<ffi:getProperty name="Account" property="AccountNum"/>
	                                   		</span>
	                                   		<div class="toggle-dark" style="float: right;">
                                                    <div class="availableCashFlowAcc on"></div>
                                            </div>
	                                   	</div>
                                   	</ffi:list> --%>
                                   	
                                   		<ffi:list collection="shownAccounts" items="Account">
	                                   	<div id="<ffi:getProperty name="Account" property="AccountID"/>" class="ui-state-default">
	                                   		<span class="sapUiIconCls icon-sort floatleft"></span>
	                                   		<span>
	                                        	<ffi:getProperty name="Account" property="AccountNum"/>
	                                   		</span>
	                                   		<div class="toggle-dark" style="float: right;">
                                                    <div class="availableCashFlowAcc on"></div>
                                            </div>
	                                   	</div>
                                   	</ffi:list>
                                   		<ffi:list collection="hiddenAccounts" items="Account">
	                                   	<div id="<ffi:getProperty name="Account" property="AccountID"/>" class="ui-state-default">
	                                   		<span class="sapUiIconCls icon-sort floatleft"></span>
	                                   		<span>
	                                        	<ffi:getProperty name="Account" property="AccountNum"/>
	                                   		</span>
	                                   		<div class="toggle-dark" style="float: right;">
                                                    <div class="availableCashFlowAcc on"></div>
                                            </div>
	                                   	</div>
                                   	</ffi:list>
                                   	
                                </div>
                                
                           </td>
                          <%--  <td width="40" valign="middle" align="center">&nbsp;
                           </td>
                           <td width="250px" valign="top">
                           		<div id="<s:property value="portletId"/>_chartAccounts" class="<s:property value="portletId"/>_connectedSortable ui-widget ui-widget-content ui-corner-all">
                                	<div class="ui-state-disabled" style="height:0px"></div>
                                	
                                </div>
                           </td>
                           <td width="40" valign="middle" align="center">
                           </td> --%>
                        </tr>
                        <tr>
                            <td height="5"></td>
                        </tr> 
                    </table>
          

              <div>&nbsp;</div>
              
<div align="center">
	<span><a id="<s:property value="portletId"/>_anchor2" href="javascript:void(0)" class="saveCashflowAccountsStatus" 
	onclick="<s:property value="portletId"/>_save()" ><s:text name="jsp.default_303"/></a></span>
</div>
</div>
<script type="text/javascript">

              $(document).ready(function(){
            	  
            	  <s:property value="portletId"/>_changeDisplayStyle();
            	  
                     $("#<s:property value="portletId"/>_availAccounts").find(".ui-state-default").click(function(){
                           $(this).removeClass("ui-state-hover");
                     })
                     
                    
                     stopToggleDrag();
                    
                     //addToggleSwitchBtn();
                     
                     if(availableCashflowAccStr==""){
                    	 // var cashFlowChartCount 0
                    }else{
                    	ns.common.initAvailCashflowAcntChartCntr(availableCashflowAccStr.split(',').length);
                    }
                     
                     $("#<s:property value="portletId"/>_availAccounts").find(".ui-state-default").each(function(){
                         if(availableCashflowAccStr.indexOf($(this).attr('id')) > -1){
                              $(this).find('.toggle-dark > div').css('width', '0').toggles({on:false, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22, 'drag': false});
                        }else{
                              $(this).find('.toggle-dark > div').css('width', '0').toggles({on:true, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22, 'drag': false});
                        }                        
                     });
                     
                     if(cashFlowChartCount==8){
                    	 $("#<s:property value="portletId"/>_availAccounts").find(".ui-state-default").each(function(){
                     		  if($(this).find('.toggle-dark > div').find('.toggle-on').hasClass('active')){
   	                    		  $(this).find('.toggle-dark > div').addClass('disableCashFlowToggle');
   	                    		  $('.disableCashFlowToggle').css('width', '0').toggles({on:true, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22, 'drag': false, 'click':false});
   	                    	  }
                     	  })
                     	  
                       }
                     
                     $("#<s:property value="portletId"/>_availAccounts > .ui-state-default").find('.availableCashFlowAcc').on('toggle', function (e, active) {
                         if (active) {
                        	//cashFlowChartCount--;
                        	ns.common.updateAvailCashflowAcntChartCntr(false);
                        	//console.log("after minus " + cashFlowChartCount);
                        	if(cashFlowChartCount<8){
                        		
                        		$("#<s:property value="portletId"/>_availAccounts").find(".ui-state-default").each(function(){
		                    		  if($(this).find('.toggle-dark > div').hasClass('disableCashFlowToggle'))
		                    			  {
		                    			  	$(this).find('.toggle-dark > div').removeClass('disableCashFlowToggle').css('width', '0').toggles({on:true, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22, 'click':true, 'drag': false});
		                    			  	
		                    			  }
		                    	  });
                        		
		                      };
                         } else {
                        	 //cashFlowChartCount++;
                        	 //ns.common.updateAvailCashflowAcntChartCntr(true);
                         }
                         if(cashFlowChartCount==8){
                        	 $("#<s:property value="portletId"/>_availAccounts").find(".ui-state-default").each(function(){
	                    		  if($(this).find('.toggle-on').hasClass('active')){
		                    		  $(this).find('.toggle-dark > div').addClass('disableCashFlowToggle');
		                    		  $('.disableCashFlowToggle').css('width', '0').toggles({on:true, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22, 'drag': false, 'click':false});
		                    	  }
	                    	  });
	                    	  
	                      }
                     });
                     
                     

              });
              
              function addToggleSwitchBtn(){
                     $('.availableCashFlowAcc').toggles({on:true, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22});
              }
              
              function stopToggleDrag(){
               $('.availableCashFlowAcc').droppable({
                  drop: function(event, ui) {
                      ui.draggable.draggable("disable", 1); // *not* ui.draggable("disable", 1);
                  }
              });
              $('.availableCashFlowAcc').draggable({ disabled: false }); 
              $('.availableCashFlowAcc').on('drag', function() {
                  return false;
                });
              }
              
            
</script>