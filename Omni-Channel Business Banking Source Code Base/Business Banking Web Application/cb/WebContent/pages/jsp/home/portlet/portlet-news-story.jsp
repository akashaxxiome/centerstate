<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:set var="StoryID" value="#parameters.storyId[0]" scope="session" />

<ffi:object name="com.ffusion.tasks.portal.ViewNewsStory" id="PortalViewNewsStory" scope="session"/>
	<ffi:setProperty name="PortalViewNewsStory" property="StoryID" value="${StoryID}"/>
<ffi:process name="PortalViewNewsStory"/>
<ffi:help id="home_portlet-news-story"/>
<div class="mainfont">
	<ffi:getProperty name="NewsStory" property="Story" encode="false"/>
</div>