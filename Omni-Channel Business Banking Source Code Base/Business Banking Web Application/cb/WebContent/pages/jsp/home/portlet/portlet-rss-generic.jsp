<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_switchToEdit() {
		$('#<s:property value="portletId"/>').portlet('edit');
	}
	
	$(document).ready(function() {
		var idSelector = '#<s:property value="portletId"/>';
		var channelTitle = '<ffi:getProperty name="RssEntries_${portletId}" property="channelTitle"/>';
		if(channelTitle!=""){
			$(idSelector).portlet('title',channelTitle);
		}
	});
</script>

<% String portletId = (String)request.getAttribute("portletId");
   String rssEntriesString = "RssEntries_" + portletId;
   if(session.getAttribute(rssEntriesString)!= null) {
%>
<div style="height: 300px;">
<ffi:list collection="RssEntries_${portletId}" items="rssEntry">
	<div>
		<p style="font-size: 13px; font-weight: bold;"><ffi:getProperty name="rssEntry" property="title"/></p>
		<p><a href="<ffi:getProperty name="rssEntry" property="link"/>" target="_blank"><ffi:getProperty name="rssEntry" property="link"/></a></p>
		<p><ffi:getProperty name="rssEntry" property="description" encode="false"/></p>
	</div>
</ffi:list>
</div>
<% }  else {
%>
	<div style="text-align: center">
		<p>
		<s:text name="jsp.home_136"/><br>
		<s:text name="jsp.home_56"/> <a href='javascript:void(0)' onclick='<s:property value="portletId"/>_switchToEdit()'><s:text name="jsp.default_179"/></a> <s:text name="jsp.home_210"/>
		</p>
	</div>
	
<% } %>