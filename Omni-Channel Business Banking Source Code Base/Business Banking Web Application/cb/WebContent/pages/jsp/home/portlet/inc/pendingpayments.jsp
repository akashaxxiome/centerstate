<%@ taglib prefix="s" uri="/struts-tags" %>
<%-- <%@ page import="com.ffusion.beans.billpay.PaymentDefines,
				 com.ffusion.beans.billpay.Payments,
				 com.ffusion.beans.billpay.Payment"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<% String toggleSortedBy = ""; String sortedBy = ""; %>
<ffi:object id="AcctNicknameTable" name="com.ffusion.beans.util.StringTable" scope="session"/>
<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
<ffi:list collection="BankingAccounts" items="Account1">
   <ffi:setProperty name="AcctNicknameTable" property="Key" value="${Account1.ID}" />
   <ffi:setProperty name="AcctNicknameTable" property="Value" value="${Account1.ConsumerDisplayText}" />
</ffi:list>

Include index-pre.jsp from Bill-Payments module so that all initial settings taken care of.
<s:set var="fromPortalPage" value="true" scope="session"/>
<s:set var="Initialize" value="true" />
<s:include value="/pages/jsp/billpay/inc/index-pre.jsp"></s:include>

<!-- always get payments to make sure they are up to date -->
 <s:include value="payments-init.jsp"/>

	get all of the bill pay accounts into session, regardless of whether or not the user is entitled to them
	<ffi:object id="GetBillPayAccounts" name="com.ffusion.tasks.billpay.GetConsumerBillPayAccounts" scope="request"/>

	check if this is a secondary user
	<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="notEquals">
		    <ffi:setProperty name="GetBillPayAccounts" property="SourceAccounts" value="PrimaryUserAccounts" />
	</ffi:cinclude>

	only do this if this is a primary user
	<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="equals">
		    <ffi:setProperty name="GetBillPayAccounts" property="SourceAccounts" value="BankingAccounts" />
	</ffi:cinclude>

	<ffi:setProperty name="GetBillPayAccounts" property="DestinationAccounts" value="BillPayAccounts" />
	<ffi:process name="GetBillPayAccounts" />
	<ffi:removeProperty name="GetBillPayAccounts" />

	Retrieve the latest account summary values from banksim and update the accounts in session with those latest values
	<ffi:object id="GetAccountSummaries" name="com.ffusion.tasks.banking.GetSummariesForAccountDate" scope="session"/>
	<ffi:setProperty name="GetAccountSummaries" property="UpdateAccountBalances" value="true"/>
	<ffi:setProperty name="GetAccountSummaries" property="SummariesName" value="LatestSummaries"/>
	<ffi:setProperty name="GetAccountSummaries" property="AccountsName" value="PaymentAccounts"/>
	<ffi:process name="GetAccountSummaries"/>
	<ffi:removeProperty name="GetAccountSummaries"/>
	<ffi:removeProperty name="LatestSummaries"/>
	 <ffi:setProperty name="PaymentCurrency" value=""/>
   	<ffi:setProperty name="MultiplePaymentCurrency" value="false"/>
<ffi:setProperty name="BillPayAccounts" property="Filter" value="CoreAccount=1,HIDE=0,AND"/>
at this point, only the bill pay accounts that the user is entitled to are in session

Get a list of secondary users for this user's primary user
<ffi:object name="com.ffusion.tasks.multiuser.GetSecondaryUsers" id="GetSecondaryUsers" scope="request"/>
    <ffi:setProperty name="GetSecondaryUsers" property="SecondaryUsersName" value="SecondaryUsers"/>
    check if this is a secondary user
    <ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="notEquals">
        <ffi:setProperty name="GetSecondaryUsers" property="PrimaryUserId" value="${PrimaryUser.Id}"/>
    </ffi:cinclude>
    only do this if this is a primary user
    <ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="equals">
        <ffi:setProperty name="GetSecondaryUsers" property="PrimaryUserId" value="${User.Id}"/>
    </ffi:cinclude>
<ffi:process name="GetSecondaryUsers"/>

The following table will keep track of what accounts the user is entitled to.
<ffi:object id="EntitledAccountsTable" name="com.ffusion.beans.util.StringTable" scope="session"/>
iterate through all of the entitled accounts and add each entry in the entitled accounts table for further use
<ffi:list collection="BillPayAccounts"items="BAccount">
    Add the current account to the EntitledAccountsTable
    <ffi:setProperty name="EntitledAccountsTable" property="Key" value="${BAccount.ID}" />
    <ffi:setProperty name="EntitledAccountsTable" property="Value" value="true" />
</ffi:list>

<% String currentSort = ""; %>

<br/>

<ffi:object id="GetPagedPendingPayments" name="com.ffusion.tasks.billpay.GetPagedPayments" scope="session"/>
	<ffi:setProperty name="GetPagedPendingPayments" property="DateFormat" value="MM/dd/yyyy" />
	<ffi:setProperty name="GetPagedPendingPayments" property="DateFormatParam" value="MM/dd/yyyy" />
	<ffi:setProperty name="GetPagedPendingPayments" property="NoSortImage" value='<img src="/efs/efs/multilang/grafx/i_sort_off.gif" alt="Sort" width="24" height="8" border="0">' />
	<ffi:setProperty name="GetPagedPendingPayments" property="AscendingSortImage" value='<img src="/efs/efs/multilang/grafx/i_sort_asc.gif" alt="Reverse Sort" width="24" height="8" border="0">' />
	<ffi:setProperty name="GetPagedPendingPayments" property="DescendingSortImage" value='<img src="/efs/efs/multilang/grafx/i_sort_desc.gif" alt="Sort" width="24" height="8" border="0">' />
	<ffi:setProperty name="GetPagedPendingPayments" property="ClearSortCriteria" value="" />
	<ffi:setProperty name="GetPagedPendingPayments" property="SortCriteriaOrdinal" value="1" />
	<ffi:setProperty name="GetPagedPendingPayments" property="SortCriteriaName" value="<%= com.ffusion.beans.billpay.PaymentDefines.SORT_CRITERIA_PAYDATE %>" />
	<ffi:setProperty name="GetPagedPendingPayments" property="CompareSortCriterion" value="<%= com.ffusion.beans.billpay.PaymentDefines.SORT_CRITERIA_PAYDATE %>" />
	<ffi:setProperty name="GetPagedPendingPayments" property="SortCriteriaAsc" value="True" />
	<ffi:setProperty name="GetPagedPendingPayments" property="ClearSearchCriteria" value="" />
	<ffi:setProperty name="GetPagedPendingPayments" property="PageSize" value="0" />
	<ffi:setProperty name="GetPagedPendingPayments" property="AccountsCollection" value="BillPayAccounts" />
	<ffi:setProperty name="GetPagedPendingPayments" property="DataSetName" value="PendingPayments" />
	<ffi:setProperty name="GetPagedPendingPayments" property="Status" value="<%= com.ffusion.beans.billpay.PaymentDefines.PAYMENT_STATUS_PENDING %>" />
	<ffi:setProperty name="GetPagedPendingPayments" property="Type" value="CURRENT,RECURRING"/>
	<ffi:setProperty name="GetPagedPendingPayments" property="DateFormat" value="${UserLocale.DateFormat}"/>
	<ffi:setProperty name="GetPagedPendingPayments" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
<ffi:process name="GetPagedPendingPayments"/>

<ffi:cinclude value1="${PendingPayments.SortedBy}" value2="" operator="equals">
	<% currentSort = Payment.PAYDATE; %>
	<ffi:setProperty name="PendingPayments" property="SortedBy" value="<%= Payment.PAYDATE %>" />
</ffi:cinclude>

<ffi:cinclude value1="${PaymentSort}" value2="" operator="notEquals">
	<ffi:setProperty name="PendingPayments" property="SortedBy" value="${PaymentSort}"/>
</ffi:cinclude>
<ffi:getProperty name="PendingPayments" property="SortedBy" assignTo="currentSort"/>

<s:text name="jsp.billpay_payments_uppercase"/>
<br/>
<br/>

<s:include value="pendingpayments-grid.jsp"/>

<ffi:object id="EditPayment" name="com.ffusion.tasks.billpay.EditPayment" scope="session"/>
   <ffi:setProperty name="EditPayment" property="Locale" value="${UserLocale.Locale}"/>   
<ffi:setProperty name="Edi<s:include value="pendingpayments-grid.jsp"/>tPayment" property="Initialize" value="TRUE" />
<ffi:removeProperty name="fromPortalPage"/>
 --%>
<%-- <s:text name="jsp.billpay_payments_uppercase"/> --%>
<s:include value="pendingpayments-grid.jsp"/>