<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<style>
       #<s:property value="portletId"/>_availAccounts, #<s:property value="portletId"/>_favoriteAccounts, #<s:property value="portletId"/>_availChartAccounts, #<s:property value="portletId"/>_showChartAccounts 
       { list-style-type: none; margin: 0; padding: 0; float: left; margin-right: 10px; height:200px; overflow-y:scroll; width:100%}
       #<s:property value="portletId"/>_availAccounts > div, #<s:property value="portletId"/>_favoriteAccounts > div, #<s:property value="portletId"/>_availChartAccounts > div, #<s:property value="portletId"/>_showChartAccounts > div 
       {padding: 2px; width: auto; cursor:default; padding:8px 5px; cursor:pointer; background:none; border:none; border-bottom:1px solid #ccc; position:relative; overflow:hidden;}
       #<s:property value="portletId"/>_availAccounts div.ui-state-disabled, #<s:property value="portletId"/>_favoriteAccounts div.ui-state-disabled, #<s:property value="portletId"/>_availChartAccounts div.ui-state-disabled, #<s:property value="portletId"/>_showChartAccounts div.ui-state-disabled
       {padding:0; height:auto; border:none}
       .accountName{}
        #<s:property value="portletId"/>_availAccounts > div > span:nth-child(2), #<s:property value="portletId"/>_favoriteAccounts > div > span:nth-child(2), #<s:property value="portletId"/>_availChartAccounts > div > span:nth-child(2), #<s:property value="portletId"/>_showChartAccounts > div > span:nth-child(2)
        {display: inline-block; float: left; width: 70%;}

.msgFormat{ 
   font-family: Arial,Helvetica,sans-serif;
  	color: #900!important ;
    font-size: 12px;
    font-style: italic;
    vertical-align:middle;
}
</style>
<script src="<s:url value='/web'/>/js/toggles.js" type="text/javascript"></script>
<script>
     function saveSelection() {
              var idSelector = '<s:property value="portletId"/>';
              var type = $('#'+idSelector + '_combobox').val();

               $.ajax({
                     //URL Encryption: Change "get" method to "post" method
                     url:'<s:url action="FavoriteAccountsPortletAction_updateSettings.action"/>',
                     type:"POST",
                     data: {portletId: '<s:property value="portletId"/>', accountIDs: $('#accountIDs').val(), 
                              chartAccountIDs: $('#chartAccountIDs').val(),
                              displayType: type, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
                     success:function(data){
                  	   if(type=='pie'){
                  		 ns.home.changeFavAccountPortletView('donut');
                  	   }else if(type=='bar'){
                  		 ns.home.changeFavAccountPortletView('bar');
                  	   }else if(type=='Table'){
                  		 ns.home.changeFavAccountPortletView('dataTable');
                  	   }
                     }
               });
       }
       
       $('#<s:property value="portletId"/>_combobox').selectmenu({width:'120'});
       
       $('#<s:property value="portletId"/>_anchor').button({
              icons: {}
       });
       
       function <s:property value="portletId"/>_changeDisplayStyle() {
              var type = $('#<s:property value="portletId"/>_combobox').val();
              if(type) {
                     if(type != 'Table') {
                           $('#<s:property value="portletId"/>_chartAccountSettings').show();
                     } else {
                           $('#<s:property value="portletId"/>_chartAccountSettings').hide();
                     }
              } 
       } 
</script>

<ffi:help id="home_portlet-favorite-accounts-edit" />
<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
                                  <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center">
                                       <tr>
                           					<td>
                                  				<div><span style="font-weight:bold"  style="float:left;"><%-- <s:text name="jsp.home_251" /> --%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="jsp.home_258"/></span> <%-- jsp.home_251 :Config display Styles --%>
                                                      
                                                       <select id="<s:property value="portletId"/>_combobox" style="width:200px" onchange="<s:property value="portletId"/>_changeDisplayStyle()">
                                                       		<option value="pie" <ffi:cinclude value1="${FavoriteAccountsSettings.displayType}" value2="pie" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_257"/></option>
                                                       		<option value="bar" <ffi:cinclude value1="${FavoriteAccountsSettings.displayType}" value2="bar" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_43"/></option>
                                                       		<option value="Table" <ffi:cinclude value1="${FavoriteAccountsSettings.displayType}" value2="Table" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_72"/></option>
                                                       </select>
                                               </div>
                           					</td>
                           			
                        			  </tr>
                                  </table>
			</td>
		</tr>
              <tr>
                     <td>        </br>
                                  <span style="font-weight:bold" ><s:text name="jsp.home_250" /></span> <%-- Select favorite accounts to display --%>
                           		 <!-- <span id="eightAcMsgSpanId" hidden="true"><s:text name="First 8 Accounts will be used as favorite accounts for Chart"/></span> <%-- "Available Accounts for Chart" change to "First 8 Accounts will be used as favorite jsp.home_41--%> -->
                                  <table border="0" width="90%" cellpadding="0" cellspacing="0" align="center">
                        <tr>
                            <td colspan="5" height="5"></td>
                        </tr>
                        <tr>
                           <td>
                             <%--  <span><s:text name="jsp.home_41" /></span><span id="jdboard"></span> --%>
                           </td>
                           <%-- <td>&nbsp;</td>
                           <td>
                               <span style="font-weight:bold"><s:text name="Favorite_Accounts" /></span>
                           </td>
                           <td>&nbsp;</td> --%>
                        </tr>
                        <tr>
                           <td width="50%" valign="top">
                           <div id="FavoriteMessageDivId" hidden="true"><s:text id="FavoriteMessageId" name="First 8 Accounts will be selected as favorite accounts"/></div>
                                <div id="<s:property value="portletId"/>_availAccounts" class="<s:property value="portletId"/>_connectedSortable ui-widget ui-widget-content ui-corner-all">
                                  <div class="ui-state-disabled" style="height:0px"></div>
                                 
                                  <%-- Following is the list of favorites accounts  it will be empty initially when accounts from the BankingAccount list is selected then it will get added in to this list--%>
                                  <%-- Following is the list of favorites accounts  from which we are selecting MAX 8 accounts to be displayed On Charts --%>
                                  <ffi:list collection="FavoriteAccounts" items="Account">
                                         <div id="<ffi:getProperty name="Account" property="ID"/>" class="ui-state-default">
                                                
                                                <span class="sapUiIconCls icon-sort floatleft"></span>
                                                <span class="accountName">
                                       				<a href="#"><ffi:getProperty name="Account" property="accountDisplayText"/></a>
                                                </span>
                                                
                                                 <div class="toggle-dark" style="float: right;">
                                                      <div class="favoriteAccountsToggle on"></div>
                                                 </div>
                                   		</div>
                                   </ffi:list>
                                   
                                  <ffi:setProperty name="BankingAccounts" property="Filter" value="${FavoriteAccountsSettings.AccountIdsFilter}"/>

                                   <%-- When the accounts from the Below BankingAccounts list is selected then it will get added in to the above favorite accounts list  --%>
                                   <%-- Following is the list of accounts from which we are selecting the accounts to be added in the FSavorites accounts list above --%>
                                    
                                      <ffi:list collection="BankingAccounts" items="Account">
                                         <div id="<ffi:getProperty name="Account" property="ID"/>" class="ui-state-default">
                                               
                                                <span class="sapUiIconCls icon-sort floatleft"></span>
                                                <span class="accountName"> 
                                        			<a href="#"><ffi:getProperty name="Account" property="accountDisplayText"/></a>
                                                </span>
                                                
                                                <div class="toggle-dark" style="float: right;">
                                                       <div class="favoriteAccountsToggle on"></div>
                                                </div>
                                         </div>
                                      </ffi:list>
                                         
                                         <ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
                                </div>
                           </td>
                          <%--  <td width="40" valign="middle" align="center">&nbsp;
                           </td>
                           <td width="250px" valign="top">
                                  <div id="<s:property value="portletId"/>_favoriteAccounts" class="<s:property value="portletId"/>_connectedSortable ui-widget ui-widget-content ui-corner-all">
                                  <div class="ui-state-disabled" style="height:0px"></div>
                                  <ffi:list collection="FavoriteAccounts" items="Account">
                                         <div id="<ffi:getProperty name="Account" property="ID"/>" class="ui-state-highlight ui-corner-all">
                                                <span class="ui-icon ui-icon-arrow-2-n-s" style="float:left"/>
                                                <span>
                                        <a href="#"><ffi:getProperty name="Account" property="DisplayText"/> - <ffi:getProperty name="Account" property="NickName"/></a>
                                                </span>
                                   </div>
                                   </ffi:list>
                                </div>
                           </td>
                           <td width="40" valign="middle" align="center">
                           </td> --%>
                        </tr>
                        <tr>
                            <td colspan="5" height="5"></td>
                        </tr>
                    </table>
                     </td>
              </tr>
              <tr>
                     <td>&nbsp;</td>
              </tr>       
              <tr>
                     <td>
                      <table border="0" width="90%" cellpadding="0" cellspacing="0" align="center" id="<s:property value="portletId"/>_chartAccountSettings">
                        <tr>
                            <td colspan="5" height="5"></td>
                        </tr>
                        <tr>
                                <div style="display: none; id="<s:property value="portletId"/>_availChartAccounts" class="<s:property value="portletId"/>_connectedSortableChart ui-widget ui-widget-content ui-corner-all">
                                 	 <div class="ui-state-disabled" style="height:0px"></div>
                                </div>
                        </tr>
                        <tr>
                            <td colspan="5" height="5"></td>
                        </tr>
                    </table>
                     </td>
              </tr>
			  <tr>
                     <td align="center">
                    	<span id="eightAcMsgSpanId" class="msgFormat"><s:text name="jsp.home.favAcctsSettingsMsg" /></span>
            		</td>
              </tr>
			 		
              <tr>
                  </br>
                     <td align="center">
                     	  </br>
                           <a id="<s:property value="portletId"/>_anchor" href="javascript:void(0)" class="saveAccountsStatus" ><s:text name="jsp.default_303"/></a>
            		</td>
              </tr>
       </table>
       <input type="hidden" name="accountIDs" id="accountIDs">
       <input type="hidden" name="chartAccountIDs" id="chartAccountIDs">
       
       <script type="text/javascript">
       
              $(document).ready(function(){
               <s:property value="portletId"/>_changeDisplayStyle();

               $(<s:property value="portletId"/>_availAccounts).find(".ui-state-default").click(function(){
                           $(this).removeClass("ui-state-hover");
                     })
                 
                    var setFavAccId = "";
                    var availableAccStr = String('<s:property value="accountIDs"/>'); 
                    
                    $('#<s:property value="portletId"/>_availAccounts').find(".ui-state-default").each(function(){
                    	if(availableAccStr.indexOf($(this).attr('id')) > -1){
                    		$(this).find('.toggle-dark > div').css('width', '0').toggles({on:false, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22, 'drag': false});
                    		var strToAppendParent = $(this);
                            var strToAppend = $(this).find('span.accountName').html();
                            setFavAccId +=($(strToAppendParent).attr('id'))+",";
                        
                           $('#<s:property value="portletId"/>_availChartAccounts').append("<div class='ui-state-default' id='"+ setFavAccId +"'><span class='sapUiIconCls icon-sort floatleft'></span><span> "+strToAppend+"</span><div style='float: right;' class='toggle-dark'><div class='chartAccToggle on '></div></div></div>");
                    	}else{
                    		$(this).find('.toggle-dark > div').css('width', '0').toggles({on:true, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22, 'drag': false});
                    	}
                    })
                 	setFavAccId=removeLastComma(setFavAccId);

            		ns.common.initAvailAcntChartCntr(setFavAccId.split(',').length);
                 
                     $(this).find('.favoriteAccountsToggle').on('toggle', function (e, active) {
                         if (active) {
                            $('#<s:property value="portletId"/>_availAccounts').find(".ui-state-default").removeClass("ui-state-hover");
                            var idToRemove = $(this).parent().parent().attr('id')+ "_chart";
                          	  //chartCount--;
                            	ns.common.updateAvailAcntChartCntr(false);
                            	console.log("IF Count : " +chartCount);
                         }else {
                        		//chartCount++;
  	                    	    ns.common.updateAvailAcntChartCntr(true);
  	                    	  	console.log("Else Count  : " +chartCount);
	                      }
                       });
                     
                     stopToggleDrag();
                    
                     addToggleSwitchBtn();
                     //The following functon is invoked when "OK" button is clicked , the selected accounts are then passed to save in DB via AJAX Call
                     $(".saveAccountsStatus").click(function(){
                           availableAccStr = $('#<s:property value="portletId"/>_availAccounts').find(".ui-state-default").map(function() {
                                  if($(this).find('.toggle-off').hasClass('active')){
                                         return $(this).attr('id');
                                  }
                           }).get().join(",");
                           $('#accountIDs').val(availableAccStr);
     
                           if(availableAccStr.indexOf($(this).attr('id')) > -1){
                        		$(this).find('.toggle-dark > div').css('width', '0').toggles({on:false, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22, 'drag': false});
                        		var strToAppendParent = $(this);
                                var strToAppend = $(this).find('span.accountName').html();

                                setFavAccId +=($(strToAppendParent).attr('id'))+",";
                           }
                           var setFavAccIdsForChart="";
                           $(function(){
                           	    var availableAcc=availableAccStr.split(',');
                        	    console.log(" availableAccStr.length :"+availableAcc.length);
                        	    console.log(" availableAccStr : "+availableAccStr);
                        	    if(availableAcc.length>=8){
                        	    	var match=availableAccStr.split(',').slice(0,8);
                            	    console.log("match is : "+match);
                                	console.log("if more than Eight accounts are shown/selected then only first 8 accounts will be considered for displaying on chart :  "+setFavAccId);
                        	    	setFavAccId=match;
                        	    	console.log("The First 8 FavAccIdsForChart are:  "+setFavAccId)
                        	    }else if(availableAcc.length<8){
                        	    	//for less than 8 accounts selected from the UI
                        	    		  var match=availableAccStr.split(',').slice(0,availableAcc.length);
                        	    		  console.log("match is : "+match);
                                      	  console.log("if less than Eight accounts are shown/selected then all selected accounts will be considered for displaying on chart :  "+setFavAccId);
                            	    	  setFavAccId=match;
                        	    }
                            	$('#chartAccountIDs').val(setFavAccId);
                           });
                           $('#chartAccountIDs').val(setFavAccId);
                           saveSelection();
                     });
              });
              
              function removeLastComma(str){  
                    var n=str.lastIndexOf(",");
                     var a=str.substring(0,n) 
                     return a;
			  }

              function addToggleSwitchBtn(){
                     //$('.favoriteAccountsToggle').toggles({on:true, text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22});
                     //$('.chartAccToggle').toggles({text:{on:'Show',off:'Hide'}, 'type': 'select', 'width': 70, 'height': 22});
              }
              
              function stopToggleDrag(){
               $('.favoriteAccountsToggle, .chartAccToggle').droppable({
                  drop: function(event, ui) {
                      ui.draggable.draggable("disable", 1); // *not* ui.draggable("disable", 1);
                  }
              });
              $('.favoriteAccountsToggle, .chartAccToggle').draggable({ disabled: false }); 
              $('.favoriteAccountsToggle, .chartAccToggle').on('drag', function() {
                  return false;
                });
              }
              
            
</script>
  