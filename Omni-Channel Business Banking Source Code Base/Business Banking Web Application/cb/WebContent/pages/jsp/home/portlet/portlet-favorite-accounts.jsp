<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
    <ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="FavoriteAccounts"/>
<ffi:process name="CheckPerAccountReportingEntitlements"/>

<ffi:setProperty name="BalanceDataClassification" property=""/>
<ffi:setProperty name="BalanceSummaryEntitlement" property=""/>

<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
    <ffi:setProperty name="BalanceDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
    <ffi:setProperty name="BalanceSummaryEntitlement" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>"/>
</ffi:cinclude>

<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="false" operator="equals">
    <ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
        <ffi:setProperty name="BalanceDataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
        <ffi:setProperty name="BalanceSummaryEntitlement" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>"/>
    </ffi:cinclude>
</ffi:cinclude>

<script type="text/javascript">
	$('#<s:property value="portletId"/>').portlet('bindCtrlEvent','.edit','click', function(){
		ns.home.removeColumnChooser('<s:property value="portletId"/>');
	});
	
	function max(arr) {  
		var max = arr[0]-0;  
		var len = arr.length;  
		for (var i = 1; i < len; i++){  
			if (arr[i] > max) {  
				max = arr[i];  
			}  
		}  
		return max;  
	}
	
	function <s:property value="portletId"/>_dummyBarChartData(length, ymax) {
		var tr = $('<tr>').append($('<th>').attr('scope','row'));
		for(var i=0;i < length; i++) {
			tr.append($('<td style="display: none;">').html(ymax));
		}
		return tr;
	}
	
</script>
<ffi:help id="home_portlet-favorite-accounts" />
<div id="favPortletContainer"  class="marginBottom5">

<div id="favPortletContainer-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick="ns.home.changeFavAccountPortletView('donut')"><span class="sapUiIconCls icon-pie-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDonut' /></a></li>
		<li><a href='#' onclick="ns.home.changeFavAccountPortletView('bar')"><span class="sapUiIconCls icon-bar-chart" style="float:left;"></span><s:text name='jsp.home.viewAsBar' /></a></li>
		<li><a href='#' onclick="ns.home.changeFavAccountPortletView('dataTable')"><span class="sapUiIconCls icon-table-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDataTable' /></a></li>
		<li><a href='#' onclick="ns.home.changeFavAccountPortletView('chartSettings')"><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<div id="tableContainer" style="padding:0 2px;overflow:auto; ">
    <!-- this holds which tabs was saved to be opened on default load.
	corresponding tab will be selected i.e. selectedChart class will be applied -->
	<input type="hidden" value="<s:property value="%{#session.FavoriteAccountsSettings.displayType}"/>" id="favAccountSelectedTab">
	<div id="dataGrid">
	<ffi:setGridURL grid="GRID_favAccountLinkURL" name="favAccountURL" url="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=2&AccountID={0}&AccountBankID={1}&AccountRoutingNum={2}&redirection=true&ProcessAccount=true&TransactionSearch=false" parm0="ID" parm1="BankID" parm2="RoutingNum"/>
		<ffi:setProperty name="favoriteaccountsPortletTempURL" value="/pages/jsp/home/FavoriteAccountsPortletAction_loadFavoriteAccountsData.action?GridURLs=GRID_favAccountLinkURL&portletId=${portletId}" URLEncrypt="true"/>
		<s:url id="favoriteaccounts_data_action" value="%{#session.favoriteaccountsPortletTempURL}"/>
		<sjg:grid id="%{portletId}_favoriteaccounts_datagrid"
							sortable="true"  
							dataType="local" 
							href="%{favoriteaccounts_data_action}"
							hiddengrid="true" 
							gridModel="gridModel" 
							hoverrows="true"
							pager="true" 
							rowList="%{#session.StdGridRowList}" 
			 				rowNum="5"
			 				rownumbers="false"
		     				navigator="true"
			 				navigatorAdd="false"
			 				navigatorDelete="false"
							navigatorEdit="false"
							navigatorRefresh="false"
							navigatorSearch="false"
							navigatorView="false"
							shrinkToFit="true"					
							scroll="false"
							scrollrows="true"								
							sortname="ID"
							viewrecords="true"
							onGridCompleteTopics="%{portletId}_grid_load_complete">         
					<sjg:gridColumn name="accountDisplayText" cssClass="datagrid_actionColumn" index="ID" title="%{getText('jsp.default_15')}" width="500" align="left" sortable="true" hidedlg="true" formatter="ns.home.formatFavoAcctColumn" />
					<sjg:gridColumn name="nickName" index="NICKNAME" title="%{getText('jsp.account_144.1')}" width="140" align="left" sortable="true" hidden="true"/>
					<sjg:gridColumn name="type" index="TYPESTRING" title="%{getText('jsp.default_444')}" width="100" align="left" sortable="true" hidden="true"/>
					<sjg:gridColumn name="groupString" index="ACCOUNTGROUPSTRING" title="%{getText('jsp.default_225')}" width="120" align="left" sortable="true" hidden="true"/>
					<sjg:gridColumn name="displayClosingBalance.amountValue.currencyStringNoSymbol" index="CLOSINGBALANCE" title="%{getText('jsp.home_166')}" width="200" align="right" sortable="true" formatter="ns.home.FAPrevBalFormatter"/>
					<sjg:gridColumn name="map.displayClosingBalanceValue" index="CLOSINGBALANCEVALUE" title="%{getText('jsp.home_166')}" hidedlg="true" hidden="true"/>
					<sjg:gridColumn name="bankID" index="BANKID" title="%{getText('jsp.account_39')}" sortable="false" hidden="true" hidedlg="true"/>
					<sjg:gridColumn name="ID" index="IDAcc" title="%{getText('jsp.account_12')}" sortable="false" hidden="true" hidedlg="true"/>
				    <sjg:gridColumn name="bankID" index="BANKID" title="%{getText('jsp.account_39')}" sortable="false" hidden="true" hidedlg="true"/>
				    <sjg:gridColumn name="routingNum" index="ROUTINGNUM" title="%{getText('jsp.account_183')}" sortable="false" hidden="true" hidedlg="true"/>
				    <sjg:gridColumn name="dataClassification" index="map.dataClassification" title="%{getText('jsp.account_64')}" sortable="false" hidden="true" hidedlg="true"/>
				    <sjg:gridColumn name="map.LinkURL" index="LinkURL" title="%{getText('jsp.default_262')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
		</sjg:grid>
	</div>
</div>

<div id="<s:property value="portletId"/>_favoriteaccounts_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('acctMgmt_balance');"><s:text name="jsp.default.viewAllRecord" /></div>


<s:hidden value="%{portletId}" id="portletIdFavAccount"></s:hidden>
<div id="favDonutChart" class="hidden favDonutChart" style="text-align: center;"></div>
<div id="favBarChart" class="hidden favBarChart" style="text-align: center;"></div>
<div id="favBarSettings" class="hidden favBarSettings marginBottom10"></div>  
</div>



<script type="text/javascript">
	ns.home.initFavorAccPortlet('<s:property value="portletId"/>',
						'<s:property value="portletId"/>_favoriteaccounts_datagrid',
						'<ffi:urlEncrypt url="/cb/pages/jsp/home/FavoriteAccountsPortletAction_updateColumnSettings.action?portletId=${portletId}"/>',
						'<s:property value="portletId"/>_grid_load_complete',
						'<s:property value="columnPerm"/>',
						'<s:property value="hiddenColumnNames"/>');

	if($("#<s:property value='portletId'/>_favoriteaccounts_datagrid tbody").data("ui-jqgrid") != undefined){
		$("#<s:property value='portletId'/>_favoriteaccounts_datagrid tbody").sortable("destroy");
	}

$(document).ready(function(){
	//array which holds the divs which will show the charts
	var tabTypes = ["favDonutChart","favBarChart","favBarSettings","tableContainer"];
	var selectedTab=$('#favAccountSelectedTab').val().trim();
	if(selectedTab=='Table'){
		$('#favAccountTable').addClass('selectedChart');
		var portletId = $('#portletIdFavAccount').val();
		setTimeout(function(){ns.common.reloadFirstGridPage('#'+portletId+'_favoriteaccounts_datagrid');}, 500);
		ns.common.resizeWidthOfGrids();
	}else if(selectedTab=='bar'){
		$('#favAccountBar').addClass('selectedChart');
		renderFavBarChart();
		displayFavAccountChart("favBarChart");;
	}else if(selectedTab=='pie'){
		$('#favAccountDonut').addClass('selectedChart');
		renderFavDonutChart();
		displayFavAccountChart("favDonutChart");;
	}
	
	window.location.hash = "";
	
	function displayFavAccountChart(chartType){
		for(var i=0;i<tabTypes.length;i++){
			if(tabTypes[i]== chartType){
				$("#"+tabTypes[i]).show();
			}else{
				$("#"+tabTypes[i]).hide();
			}
		}
	}
	
	ns.home.changeFavAccountPortletView = function(dataType) {
			if(dataType =='donut'){
				renderFavDonutChart();
				displayFavAccountChart("favDonutChart");
			}else if(dataType =='bar'){
				renderFavBarChart();
				displayFavAccountChart("favBarChart");
			}else if(dataType =='dataTable'){
				displayFavAccountChart("tableContainer");
				var portletId = $('#portletIdFavAccount').val();
				ns.common.reloadFirstGridPage('#'+portletId+'_favoriteaccounts_datagrid');
				ns.common.resizeWidthOfGrids();
			}else if(dataType =='chartSettings'){
				console.log('chartSettings=>');	
				displayFavAccountChart("favBarSettings");
				 renderFavAccountSettings();
			}
		};
	});

	function renderFavAccountSettings(){
		// below step is done to remove the last loaded from thru sapui5 so that new view is loaded if anything changes through setting
		window.location.hash='';
		var aConfig = {
			url : "/cb/pages/jsp/home/FavoriteAccountsPortletAction_loadEditMode.action",
			data: {CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>',portletId:'<s:property value="portletId"/>'},
			success: function(data) {
				$('#favBarSettings').html("");
				$('#favBarSettings').html(data);
			}
		};

		var localAjaxSettings = ns.common.applyLocalAjaxSettings(aConfig); 
		$.ajax(localAjaxSettings); 
	}

	function renderFavDonutChart() {
		//console.log('going to call router');
		$.publish("sap-ui-renderChartsTopic",{
			chartRoute: "_favoriteAccountsDonutChart",
			beforeNavigate : function(){
				// Place SAP UI5 HTML element for each chart
				var donutChartLayout = sap.ui.getCore().byId("favAccUI5DonutChartContainer");
				if(!donutChartLayout){
					donutChartLayout = new sap.ui.layout.VerticalLayout("favAccUI5DonutChartContainer", {
						content : [],
					});
				}
				donutChartLayout.placeAt($("#favDonutChart")[0]);
			}});
	}
	
	function renderFavBarChart() {
		$.publish("sap-ui-renderChartsTopic",{
			chartRoute: "_favoriteAccountsBarChart",
			beforeNavigate : function(){
				var columnChartLayout = sap.ui.getCore().byId("favAccUI5ColChartContainer");
				if(!columnChartLayout){
					columnChartLayout = new sap.ui.layout.VerticalLayout("favAccUI5ColChartContainer", {
						content :[]						
					});
				}
				columnChartLayout.placeAt($("#favBarChart")[0]);
			}});
	}

</script>
