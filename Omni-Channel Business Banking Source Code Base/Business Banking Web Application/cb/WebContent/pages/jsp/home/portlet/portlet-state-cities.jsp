<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<span><s:text name="jsp.default_101" /></span>
<br/>

<ffi:cinclude value1="${STATE_CITIES_FOUND}" value2="true" operator="notEquals" >
	<span class="txtbox" style="border: 0px" ><s:text name="jsp.home_128"/></span>
</ffi:cinclude>

<ffi:cinclude value1="${STATE_CITIES_FOUND}" value2="true" operator="equals" >
<table>
	<tr>
		<td>
		    <select style="float:left" class="txtbox" id="<s:property value="portletId"/>_forecastStateCities">
			    <ffi:list collection="ForecastStateCities" items="City">
				    <ffi:setProperty name="Compare" property="Value1" value="${City.Abbr}" />
				    <ffi:setProperty name="Compare" property="Value2" value="${ForecastStateCities.Selected}" />
		
				    <option value="<ffi:getProperty name="City" property="Key"/>" <ffi:getProperty name="selected${Compare.Equals}"/>>
			            <ffi:getProperty name="City" property="Name"/>
		            </option>
		        </ffi:list>
		    </select>
	    </td>
    	<td class="dvcPlusIconTd">
    		<a href="javascript:void(0)" onclick="<s:property value="portletId"/>_getStateInit()"><span class="ui-icon ui-icon-circle-plus"/></a>
    	</td>
    </tr>
</table>
</ffi:cinclude>

<script type="text/javascript">
function <s:property value="portletId"/>_getStateInit()
{
	var idSelector = '#<s:property value="portletId"/>';
	var selectedStateValue = $('#<s:property value="portletId"/>_forecastStates').val();
	var selectedCountryValue = $('#<s:property value="portletId"/>_forecastCountries').val();
	var selectedStateCityValue = $('#<s:property value="portletId"/>_forecastStateCities').val();
	var selectedCities = <s:property value="portletId"/>_getCheckedItems();
	var selectedScale = <s:property value="portletId"/>_getScale();
	
	//To avoid if user selects 'Type to search' option due to bug in widget
	if(selectedStateCityValue == '-1'){
		return false;
	}

	var aConfig = {
			type: "POST",
			url: '<s:url action="ForecastPortletAction_selectStateCity"/>',
			data: {portletId: '<s:property value="portletId"/>', selectState: selectedStateValue, selectCountry: selectedCountryValue, selectStateCity: selectedStateCityValue, cities: selectedCities, scale:selectedScale, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
			success:function(data){
				$(idSelector).html(data);
			}
		};

	var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
	$.ajax(ajaxSettings);
}
</script>
