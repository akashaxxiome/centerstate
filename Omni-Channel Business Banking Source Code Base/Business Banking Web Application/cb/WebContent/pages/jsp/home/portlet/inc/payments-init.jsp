<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="payments-init-touched" value="true"/>
<ffi:setProperty name="touchedvar" value="payments-init-touched"/>
 <!-- 
<ffi:object name="com.ffusion.tasks.billpay.Initialize" id="BillPayInitService" scope="request"/>
	<ffi:setProperty name="BillPayInitService" property="ClassName" value="com.ffusion.services.billpay.BillPayServiceImpl" />
	<ffi:setProperty name="BillPayInitService" property="InitURL" value="billpay.xml" />
<ffi:process name="BillPayInitService" />
 -->
 
<ffi:object name="com.ffusion.tasks.billpay.UpdateService" id="BillPayUpdateService" scope="request"/>
	<ffi:setProperty name="BillPayUpdateService" property="UserName" value="${TransID}" />
	<ffi:setProperty name="BillPayUpdateService" property="Password" value="${TransPassword}" />
<ffi:process name="BillPayUpdateService" />

<ffi:cinclude value1="${PaymentFrequencies}" value2="">
	<ffi:object id="PaymentFrequencies" name="com.ffusion.tasks.util.ResourceList" scope="session"/>
		<ffi:setProperty name="PaymentFrequencies" property="ResourceFilename" value="com.ffusion.beansresources.billpay.resources" />
		<ffi:setProperty name="PaymentFrequencies" property="ResourceID" value="RecPaymentFrequencies" />
	<ffi:process name="PaymentFrequencies" />
</ffi:cinclude>

<ffi:cinclude value1="${PaymentFreqResource}" value2="">
	<ffi:object id="PaymentFreqResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
		<ffi:setProperty name="PaymentFreqResource" property="ResourceFilename" value="com.ffusion.beansresources.billpay.resources" />
	<ffi:process name="PaymentFreqResource" />
</ffi:cinclude>

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />


<ffi:object id="GetPayees" name="com.ffusion.tasks.billpay.GetExtPayees" scope="request"/>
    <ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="notEquals">
        <ffi:setProperty name="GetPayees" property="UserSessionName" value="PrimaryUser"/>
    </ffi:cinclude>
<ffi:process name="GetPayees" />


<ffi:object id="FindPayments" name="com.ffusion.tasks.billpay.FindPayments" scope="session"/>
   <ffi:setProperty name="FindPayments" property="Locale" value="${UserLocale.Locale}" />

<%-- ------------------------ BEGIN REGISTER INTEGRATION ----------------------- --%>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
	<ffi:object id="SetRegisterAccountsData" name="com.ffusion.tasks.register.SetRegisterAccountsData" scope="request"/>
	<ffi:process name="SetRegisterAccountsData"/>
</ffi:cinclude>
<%-- ------------------------ END REGISTER INTEGRATION ----------------------- --%>
