<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
 <%@ page contentType="text/html; charset=UTF-8" %>
 <%@ taglib prefix="s" uri="/struts-tags"%>
 <script type="text/javascript" src="<s:url value='/web/js/account/account_grid%{#session.minVersion}.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/accounts/accounts%{#session.minVersion}.css'/>"></link>
<ffi:author page="inc/portal/container-top-wide.jsp"/>
	<form name="ChangeDisplayCurrencyForm" action='<ffi:getProperty name="SecurePath" />index.jsp?changeDisplayCurrency=true' method="post">

<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
<script type="text/javascript">

	function changeDisplayCurrency()
	{
		var displayCurrencyCode = $("#displayCurrencyCode").val();
		var urlString = '/cb/pages/jsp/home/portlet/inc/accounts-container.jsp?displayCurrencyCode='+displayCurrencyCode;
		ns.home.getHomeSummaryDatas(urlString,'#accountsSummaryGridDiv');
	}

	$("#displayCurrencyCode").selectmenu({width: 200});

</script>
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<td  width="75%"  colspan="3" class="header" align="left" valign="middle" style="border-right: hidden; border-bottom: hidden" >
			<s:text name="jsp.home.label.show.accounts.in"/>&nbsp;
			<select name="displayCurrencyCode" id="displayCurrencyCode" class="textbox" onchange="changeDisplayCurrency();">
				<option <ffi:cinclude value1='' value2='${displayCurrencyCode}' operator='equals'> selected </ffi:cinclude> value=''><s:text name="jsp.default_127"/></option>
				<ffi:list collection="FOREIGN_EXCHANGE_TARGET_CURRENCIES" items="currency">
				    <option <ffi:cinclude value1='${currency.CurrencyCode}' value2='${displayCurrencyCode}' operator='equals'> selected </ffi:cinclude>  value='<ffi:getProperty name="currency" property="CurrencyCode"/>'><ffi:getProperty name="currency" property="Description"/></option>
			    </ffi:list>
			</select>
		</td>
	</tr>
</table>
	</form>
<br/>