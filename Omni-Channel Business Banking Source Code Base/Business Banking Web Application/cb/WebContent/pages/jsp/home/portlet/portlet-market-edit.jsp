<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_save() {
		var idSelector = '#<s:property value="portletId"/>';
		
		var checkedItems = '';
		$('input[name="<s:property value='portletId'/>_checkbox"]').each(function(){
			if($(this).attr("checked")){
				if(checkedItems) {
					checkedItems += ",";
				}
				checkedItems += $(this).val();
			}
		});
		
		var aConfig = {
				type: "POST",
				url: '<s:url action="MarketPortletAction_updateSettings.action"/>',
				data: {portletId: '<s:property value="portletId"/>', indexes: checkedItems, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
				success:function(data){
			$("#marketDataContainer").html(data);
			isPortalModified = true;
			//ns.home.bindPortletHoverToTable($(idSelector));
			$("#marketDataContainer").show();
			$("#marketSettings").hide();
			
			$.publish("common.topics.tabifyNotes");	
			if(accessibility){
				$(idSelector).setFocus();
			}
				}
			};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}
	
	$('#<s:property value="portletId"/>_anchor').button({
		icons: {}
	});

</script>

					<ffi:help id="home_portlet-market-edit" />

					<table border="0" cellspacing="2" cellpadding="2" align="center" class="tableData marginBottom5">
						<tr>
							<td colspan="5" style="font-weight:bold;"><s:text name="jsp.home_182"/><br/></td>
						</tr>
						<tr valign="top">
							<ffi:setProperty name="StockIndexSettings" property="Checked" value="$INDU" />

							<td class="mainfont">
								<input type="checkbox" name="<s:property value="portletId"/>_checkbox" value="$INDU" <ffi:getProperty name="checked${StockIndexSettings.Checked}"/>>
							</td>
							<td><s:text name="jsp.home_84"/></td>
							<ffi:setProperty name="StockIndexSettings" property="Checked" value="SPX" />
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td class="mainfont">
								<input type="checkbox" name="<s:property value="portletId"/>_checkbox" value="SPX" <ffi:getProperty name="checked${StockIndexSettings.Checked}"/>>
							</td>
							<td><s:text name="jsp.home_174"/></td>
						</tr>
						<tr valign="top">
							<ffi:setProperty name="StockIndexSettings" property="Checked" value="COMP" />

							<td class="mainfont">
								<input type="checkbox" name="<s:property value="portletId"/>_checkbox" value="COMP" <ffi:getProperty name="checked${StockIndexSettings.Checked}"/>>
							</td>
							<td><s:text name="jsp.home_123"/></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<ffi:setProperty name="StockIndexSettings" property="Checked" value="RUT" />

							<td class="mainfont">
								<input type="checkbox" name="<s:property value="portletId"/>_checkbox" value="RUT" <ffi:getProperty name="checked${StockIndexSettings.Checked}"/>>
							</td>
							<td><s:text name="jsp.home_173"/></td>
						</tr>
						<tr><td colspan="5">&nbsp;</td></tr>
						<tr align="center">
							<td colspan="5">
								<a id="<s:property value="portletId"/>_anchor" href="javascript:void(0)" onclick="<s:property value="portletId"/>_save()" ><s:text name="jsp.default_303"/></a>
							</td>
						</tr>
					</table>
				
