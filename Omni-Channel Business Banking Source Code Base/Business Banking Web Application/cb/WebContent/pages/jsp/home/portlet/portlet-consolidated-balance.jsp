<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setProperty name="populateConsBalanceURL" value="/cb/pages/jsp/home/ConsolidatedBalancePortletAction_loadViewDetailsMode.action" URLEncrypt="true"/>

<div id="consolidatedBalanceDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick="ns.home.changeConsolidatedBalancePortletView('donut')"><span class="sapUiIconCls icon-pie-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDonut' /></a></li>
		<li><a href='#' onclick="ns.home.changeConsolidatedBalancePortletView('bar')"><span class="sapUiIconCls icon-bar-chart" style="float:left;"></span><s:text name='jsp.home.viewAsBar' /></a></li>
		<li><a href='#' onclick="ns.home.changeConsolidatedBalancePortletView('dataTable')"><span class="sapUiIconCls icon-table-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDataTable' /></a></li>
		<li><a href='#' onclick="ns.home.changeConsolidatedBalancePortletView('chartSettings')"><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<div id="consolidatedBalanceDataDiv">
	<s:action name="ConsolidatedBalancePortletAction_loadViewDetailsMode" namespace="/pages/jsp/home" executeResult="true">
	 	<s:param name="portletId" value="%{#portletId}" />
	 </s:action>
</div>