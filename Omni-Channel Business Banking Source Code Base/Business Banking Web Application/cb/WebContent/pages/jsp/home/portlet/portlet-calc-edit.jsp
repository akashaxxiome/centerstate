<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_save() {
		var idSelector = '#<s:property value="portletId"/>';
		
		var checkedItems = '';
		$('input[name="<s:property value='portletId'/>_checkbox"]').each(function(){
			if($(this).attr("checked")){
				if(checkedItems) {
					checkedItems += ",";
				}
				checkedItems += $(this).val();
			}
		});
	
		var aConfig = {
				type: "POST",
				url: '<s:url action="CalcPortletAction_updateSettings.action"/>',
				data: {portletId: '<s:property value="portletId"/>', calcs: checkedItems, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
				success:function(data){
					ns.home.switchToView($(idSelector));
					$(idSelector).html(data);
					isPortalModified = true;
					ns.home.bindPortletHoverToTable($(idSelector));
					
					$.publish("common.topics.tabifyNotes");	
					if(accessibility){
						$(idSelector).setFocus();
					}
					
				}
			};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);

	}
	
	$('#<s:property value="portletId"/>_anchor').button({
		icons: {}
	});

</script>

<ffi:cinclude value1="${UseLastRequest}" value2="TRUE" operator="notEquals">
    <ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>
<ffi:removeProperty name="UseLastRequest"/>
<ffi:cinclude value1="${LastRequest}" value2="" operator="equals">
	<ffi:object id="LastRequest" name="com.ffusion.beans.util.LastRequest" scope="session"/>
</ffi:cinclude>


<%-- ================ MAIN CONTENT START ================ --%>
<ffi:help id="home_portlet-calc-edit"/>

				<table cellpadding="0" cellspacing="0" border="0" align="center" class="tableData marginBottom5">
					<ffi:list collection="CalculatorsMaster" items="CalculatorsTemp,CalculatorsTemp2">
						<tr>
							<td valign="top">
								<ffi:setProperty name="LastRequest" property="Name" value="CalculatorsEdit.AddCalc"/>
								<table border="0" cellspacing="2" cellpadding="2">
									<tr>
										<td colspan="2" style="font-weight:bold;"><ffi:getProperty name="CalculatorsTemp" property="Category"/></td>
									</tr>
									<ffi:list collection="CalculatorsTemp" items="Calculator">
										<tr valign="top">
											<td>
												<ffi:setProperty name="Calculators" property="ContainsCalc" value="${Calculator.ID}" />
												<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${Calculator.ID}"/>
												<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Calculators.ContainsCalc}"/>
												<input type="checkbox" name="<s:property value="portletId"/>_checkbox" <ffi:getProperty name="LastRequest" property="CheckboxAttribs" encode="false"/>>
											</td>
											<td>
                                                <ffi:link url="${Calculator.URL}"><ffi:getProperty name="Calculator" property="Name"/></ffi:link>
                                            </td>
										</tr>
									</ffi:list>
									<tr>
										<td colspan="2" height="8"></td>
									</tr>
								</table>
							</td>
							<td valign="top">
								<table border="0" cellspacing="2" cellpadding="2">
									<tr>
										<td colspan="2" style="font-weight:bold;"><ffi:getProperty name="CalculatorsTemp2" property="Category"/></td>
									</tr>
									<ffi:list collection="CalculatorsTemp2" items="Calculator">

									<tr valign="top">
										<td>
										<ffi:setProperty name="Calculators" property="ContainsCalc" value="${Calculator.ID}" />
										<ffi:setProperty name="LastRequest" property="CheckboxValue" value="${Calculator.ID}"/>
										<ffi:setProperty name="LastRequest" property="CheckboxChecked" value="${Calculators.ContainsCalc}"/>
										<input type="checkbox" name="<s:property value="portletId"/>_checkbox" <ffi:getProperty name="LastRequest" property="CheckboxAttribs" encode="false"/>></td>
										<td>
                                            <ffi:link url="${Calculator.URL}"><ffi:getProperty name="Calculator" property="Name"/></ffi:link>
                                        </td>
									</tr>
									</ffi:list>

									<tr>
										<td colspan="2" height="8"></td>
									</tr>
								</table>
							</td>
						</tr>
					</ffi:list>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr align="center">
						<td colspan="2">
							<a id="<s:property value="portletId"/>_anchor" href="javascript:void(0)" onclick="<s:property value="portletId"/>_save()" ><s:text name="jsp.default_303"/></a>
						</td>
					</tr>
				</table>

<%-- ================= MAIN CONTENT END ================= --%>

<ffi:removeProperty name="LastRequest"/>
<%-- 
<ffi:setProperty name="BackURL" value="${SecurePath}inc/portal/portal-calc-edit.jsp?UseLastRequest=TRUE" URLEncrypt="true"/>
--%>

<div id="calculatorPortletEditDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div