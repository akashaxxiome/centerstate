<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.accounts.AccountTypes"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummaries"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummary"%>
<%@ page import="com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.accounts.AssetAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.DepositAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.LoanAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.CreditCardAcctSummary"%>
<%@ page import="com.ffusion.beans.common.Currency"%>
<%@ page import="java.util.Collection"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
 <%@ page contentType="text/html; charset=UTF-8" %>
 
 		<%-- render total grid start --%>
		<script type="text/javascript">
			ns.home.initTotalsGrid('accountsSummaryTotalUrlID', 'onTotalsGridLoadComplete');
		</script>

		<ffi:setProperty name="totalsURL" value="/pages/jsp/home/AccountsSummaryPortletAction_getTotals.action" URLEncrypt="true"/>

		<s:url id="accountsSummaryTotalUrl" value="%{#session.totalsURL}"  escapeAmp="false"/>
		<sjg:grid
			id="accountsSummaryTotalUrlID"
			caption=""
			dataType="json"
			href="%{accountsSummaryTotalUrl}"
			pager="false"
			gridModel="gridModel"
			rowList="10,15,20"
			rowNum="10"
			shrinkToFit="true"
			sortable="true"
			scroll="false"
			scrollrows="true"
			viewrecords="true" 
			onGridCompleteTopics="onTotalsGridLoadComplete"
			>


			<sjg:gridColumn name="consumerDisplayText" index="NICKNAME" title="Account" sortable="true" />
			<sjg:gridColumn name="bankName" index="BANKNAME" title="Bank" sortable="true" hidden="true" hidedlg="true" />
			<sjg:gridColumn name="type" index="TYPESTRING" title="Account Type" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="currencyCodeInternal" index="CURRENCY_CODE_INT" title="Currency" width="300"  formatter="ns.home.boldFormatter" sortable="false" />
			<sjg:gridColumn name="displayCurrentBalanceInternal" index="CurrentBalanceFieldInt" title="Current Bal." formatter="ns.home.boldFormatter" sortable="false" />
			<sjg:gridColumn name="currencyCodeExternal" index="CURRENCY_CODE_EXT" title="Currency" width="300"  formatter="ns.home.boldFormatter" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="displayCurrentBalanceExternal" index="CurrentBalanceFieldExt" title="Current Bal." formatter="ns.home.boldFormatter" sortable="false" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="displayAvailableBalance" index="displayAvailableBalance" title="Avail. Bal." formatter="ns.home.boldFormatter" sortable="false" hidden="true" hidedlg="true" />
			<sjg:gridColumn name="" index="" title="Action" hidden="true" hidedlg="true" cssClass="" sortable="false"  />

			</sjg:grid>
		<script>
		if($("#accountsSummaryTotalUrlID tbody").data("ui-jqgrid") != undefined){
			$("#accountsSummaryTotalUrlID tbody").sortable("destroy");
		}
			//to hide column headers for totals Grid
			var grid = $("#accountsSummaryTotalUrlID");
			var gview = grid.parents("div.ui-jqgrid-view");
			gview.children("div.ui-jqgrid-hdiv").hide();
		</script>


		<%-- render total grid end --%>