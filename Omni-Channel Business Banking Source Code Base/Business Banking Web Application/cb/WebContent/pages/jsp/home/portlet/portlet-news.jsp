<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<style>
	.newsLink:link {color: #0000FF}
	.newsLink:visited {color: #800080}
</style>

<script type="text/javascript">
	function <s:property value="portletId"/>_openNewsContent(anchor, storyID) {
		var buttonOptions = {};
		buttonOptions[js_CLOSE] = function() { $(this).dialog('close'); };
		var dialogObj = $("<div/>").dialog({
			width: 500,
			height: 400,
			autoOpen: true,
			buttons:buttonOptions,
			show: 'fold',
			hide: 'clip',
			resizable: false,
			title: js_loading_news_message
		});
		
		dialogObj.addHelp(function(){
			var helpFile = dialogObj.find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		});
		
		var aConfig = {
			type: "POST",
			url: '<s:url value="/pages/jsp/home/portlet/portlet-news-story.jsp" />',
			data: {storyId: storyID, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
			success: function(data){
				if(dialogObj) {
					dialogObj.html(data);
					dialogObj.dialog('option', 'title', anchor.innerHTML);
				}
			}
		};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}
	
	function <s:property value="portletId"/>_switchToEdit() {
		$('#<s:property value="portletId"/>').portlet('edit');
	}
</script>

<ffi:help id="home_portlet-news"/>

<div id="newsPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick='<s:property value="portletId"/>_switchToEdit()'><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<table border="0" cellspacing="0" cellpadding="0" width="100%" class="marginBottom5">
	<ffi:setProperty name="hasNewsItems" value="false"/>
	<%-- Since NewsSports does not support the Size property, we use the following to determine if it's empty --%>
	<s:if test="%{newsType == 1}">
		<ffi:list collection="NewsSports" items="item">
			<ffi:setProperty name="hasNewsItems" value="true"/>
		</ffi:list>
	</s:if>
	<s:if test="%{newsType == 2}">
		<ffi:list collection="NewsBusiness" items="item">
			<ffi:setProperty name="hasNewsItems" value="true"/>
		</ffi:list>
	</s:if>
	<s:if test="%{newsType == 3}">
		<ffi:list collection="NewsFrontpage" items="item">
			<ffi:setProperty name="hasNewsItems" value="true"/>
		</ffi:list>
	</s:if>

	<%-- IF COLLECTION IS EMPTY --%>
	<ffi:cinclude value1="false" value2="${hasNewsItems}" operator="equals">
		<tr align="center">
			<td>
				<br><br><br><br><br>
				<s:text name="jsp.home_144"/><br><s:text name="jsp.home_56"/> <a href='javascript:void(0)' class="sapUiLnk" onclick='<s:property value="portletId"/>_switchToEdit()'><s:text name="jsp.default_179"/></a> <s:text name="jsp.home_218"/>
			</td>
		</tr>
	</ffi:cinclude>


	<%-- IF COLLECTION IS NOT EMPTY --%>
	<ffi:cinclude value1="true" value2="${hasNewsItems}" operator="equals">
		<s:if test="%{newsType == 1}">
			<ffi:list collection="NewsSports" items="Headline">
					<tr>
						<td>
							<span class="ui-icon-carat-1-e ui-icon" style="float:left"></span>
							<a class="newsLink" href="javascript:void(0)" onclick="<s:property value='portletId'/>_openNewsContent(this, '<ffi:getProperty name="Headline" property="StoryID" encode="false"/>')">
								<ffi:getProperty name="Headline" property="Headline"/>
							</a>
						</td>
					</tr>
				</ffi:list>
		</s:if>
		<s:if test="%{newsType == 2}">
			<ffi:list collection="NewsBusiness" items="Headline">
			<tr>
						<td>
							<span class="ui-icon-carat-1-e ui-icon" style="float:left"></span>
							<a class="newsLink" href="javascript:void(0)" onclick="<s:property value='portletId'/>_openNewsContent(this, '<ffi:getProperty name="Headline" property="StoryID" encode="false"/>')">
								<ffi:getProperty name="Headline" property="Headline"/>
							</a>
						</td>
					</tr>
				</ffi:list>
		</s:if>
		<s:if test="%{newsType == 3}">
			<ffi:list collection="NewsFrontpage" items="Headline">
			<tr>
						<td>
							<span class="ui-icon-carat-1-e ui-icon" style="float:left"></span>
							<a class="newsLink" href="javascript:void(0)" onclick="<s:property value='portletId'/>_openNewsContent(this, '<ffi:getProperty name="Headline" property="StoryID" encode="false"/>')">
								<ffi:getProperty name="Headline" property="Headline"/>
							</a>
						</td>
					</tr>
				</ffi:list>
		</s:if>
	</ffi:cinclude>
</table>