<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
    <%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
    <%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/billpay/billpay%{#session.minVersion}.css'/>"></link>

<style>
	#pendingTransactionNavigationPortlet .wizardStepIndicatorHolder {
		width: 99% !important;
	}
</style>

<div id="operationresult">
		<div id="resultmessage"><s:text name="jsp.default_498"/></div>
</div>


<div id="pendingTransactionNavigationPortlet" class="portlet wizardSupportCls ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
     <div class="portlet-header ui-widget-header ui-corner-all">
	 </div>
     <div class="portlet-content">
		<div class="TransactionWizardPannelWrapper">
			<sj:tabbedpanel id="PendingTransactionWizardTabs" >
		       <sj:tab id="inputTab" target="inputDiv" label="%{getText('jsp.default_390')}"/>
		       <sj:tab id="verifyTab" target="verifyDiv" label="%{getText('jsp.default_392')}"/>
		       <sj:tab id="confirmTab" target="confirmDiv" label="%{getText('jsp.default_393')}"/>
		       <div id="inputDiv" align="left">
					<s:if test="fromPendingTransfers=='true'">
						<s:include value="/pages/jsp/transfers/accounttransferedit.jsp"></s:include>
					</s:if>
					<s:else>
					<s:include value="/pages/jsp/billpay/billpayeditform.jsp"></s:include>
					</s:else>
				</div>
				<div id="verifyDiv" style="width: 100%; display: none;">
				</div>
				<div id="confirmDiv" style="width: 100%; display: none;">
				</div>
			</sj:tabbedpanel>
		</div>	
	 </div>
</div>

<ffi:removeProperty name="fromPendingPayments"/>
<ffi:removeProperty name="fromPendingTransfers"/>

<script>    
	//Initialize portlet with settings icon
	ns.common.initializePortlet("pendingTransactionNavigationPortlet"); 
	ns.pendingTransactionsPortal.gotoStep(1); 
</script>