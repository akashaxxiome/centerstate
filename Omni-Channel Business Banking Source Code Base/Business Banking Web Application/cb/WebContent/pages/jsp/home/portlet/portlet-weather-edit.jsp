<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<div id="WeatherEditPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<script type="text/javascript">
	function <s:property value="portletId"/>_save() {
		var idSelector = '#<s:property value="portletId"/>';
		
		var selectedCities = <s:property value="portletId"/>_getCheckedItems();
		var selectedScale = <s:property value="portletId"/>_getScale();

		var aConfig = {
				type: "POST",
				url: '<s:url action="ForecastPortletAction_updateSettings.action"/>',
				data: {portletId: '<s:property value="portletId"/>', cities: selectedCities, scale:selectedScale, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
				success:function(data){
					ns.home.switchToView($(idSelector));
					$(idSelector).html(data);
					isPortalModified = true;
					ns.home.bindPortletHoverToTable($(idSelector));
					
					$.publish("common.topics.tabifyNotes");	
					if(accessibility){
						$(idSelector).setFocus();
					}
					
				}
			};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}
	
	function <s:property value="portletId"/>_getStateCities() {
		var idSelector = '#<s:property value="portletId"/>';
		var selectedStateValue = $('#<s:property value="portletId"/>_forecastStates').val();
		var selectedCountryValue = $('#<s:property value="portletId"/>_forecastCountries').val();
		var selectedCities = <s:property value="portletId"/>_getCheckedItems();
		var selectedScale = <s:property value="portletId"/>_getScale();
		
		var aConfig = {
				type: "POST",
				url: '<s:url action="ForecastPortletAction_loadStateCities.action"/>',
				data: {portletId: '<s:property value="portletId"/>', selectState: selectedStateValue, selectCountry: selectedCountryValue, cities: selectedCities, scale:selectedScale, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
				success:function(data){
					$(idSelector).html(data);
					ns.home.bindPortletHoverToTable($(idSelector));
				}
			};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}
	
	function <s:property value="portletId"/>_getCountryCities() {
		var idSelector = '#<s:property value="portletId"/>';
		var selectedStateValue = $('#<s:property value="portletId"/>_forecastStates').val();
		var selectedCountryValue = $('#<s:property value="portletId"/>_forecastCountries').val();
		var selectedCities = <s:property value="portletId"/>_getCheckedItems();
		var selectedScale = <s:property value="portletId"/>_getScale();
	
		var aConfig = {
				type: "POST",
				url: '<s:url action="ForecastPortletAction_loadCountryCities.action"/>',
				data: {portletId: '<s:property value="portletId"/>', selectState: selectedStateValue, selectCountry: selectedCountryValue, cities: selectedCities, scale:selectedScale, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
				success:function(data){
					$(idSelector).html(data);
					ns.home.bindPortletHoverToTable($(idSelector));
				}
			};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}
	
	function <s:property value="portletId"/>_getCheckedItems() {
		var checkedItems = '';
		$('input[name="<s:property value='portletId'/>_forecastcheck"]').each(function(){
			if($(this).attr("checked")){
				if(checkedItems) {
					checkedItems += ",";
				}
				checkedItems += $(this).val();
			}
		});
		return checkedItems;
	}
	
	function <s:property value="portletId"/>_getScale() {
		return $('input[name="<s:property value='portletId'/>_scale"]:checked').val();
	}
	
	$('#<s:property value="portletId"/>_forecastStates').combobox();
	$('#<s:property value="portletId"/>_forecastCountries').combobox();
	$('#<s:property value="portletId"/>_forecastStateCities').combobox();
	$('#<s:property value="portletId"/>_forecastCountryCities').combobox();
	
	$('#<s:property value="portletId"/>_anchor').button({
		icons: {}
	});
</script>

<ffi:setProperty name="ResetForecasts" value="true" />

<ffi:object id="ForecastsEdit" name="com.ffusion.beans.portal.Forecasts" scope="session"/>

<%-- ================ MAIN CONTENT START ================ --%>

<ffi:help id="home_portlet-weather-edit" />
	<table border="0" cellspacing="0" cellpadding="0" width="100%" class="tableData marginBottom5">
		<tr>
			<td>
					<table border="0" cellspacing="0" cellpadding="0" align="center">
						<tr>
							<td>
								<!-- <fieldset style="padding:10px"> -->
									<span style="font-weight:bold"><s:text name="jsp.home_188"/></span>
									<table border="0" cellspacing="0" cellpadding="0">
									<% 
										java.util.HashMap stateCities = (java.util.HashMap) config.getServletContext().getAttribute(com.ffusion.tasks.portal.Task.STATE_CITY_LIST_CONTEXT); 
										java.util.HashMap countryCities = (java.util.HashMap) config.getServletContext().getAttribute(com.ffusion.tasks.portal.Task.COUNTRY_CITY_LIST_CONTEXT); 
										String cityAbbr = null;
										int index = -1;
										String stateName = null;
										String countryName = null;
										String cityName = null;
										com.ffusion.beans.portal.GeographicUnit city = null;
									%>
									<ffi:list collection="ForecastSettingsTemp" items="Forecast">
										<ffi:getProperty name="Forecast" property="City" assignTo="cityAbbr" />
										
										<% 
											com.ffusion.beans.portal.GeographicUnits cities = null;
											if (cityAbbr.endsWith(" US") ) {
												// this is a US city. find the state
												
												index = cityAbbr.lastIndexOf(" US");
												stateName = cityAbbr.substring(index - 2, index);
												cities = (com.ffusion.beans.portal.GeographicUnits) stateCities.get(stateName);
											} else {
												index = cityAbbr.lastIndexOf(' ');
												countryName = cityAbbr.substring(index + 1);
												cities = (com.ffusion.beans.portal.GeographicUnits) countryCities.get(countryName);
											}
											
											
											for( int i = 0 ; i < cities.size() ; i++ ) {
												city = (com.ffusion.beans.portal.GeographicUnit) cities.get(i);
												if( city.getKey().equals(cityAbbr) )
													break;
												city = null;
											}
										%>
										<tr>
											<td></td>
											<td><input type="checkbox" name="<s:property value="portletId"/>_forecastcheck" value="<ffi:getProperty name="Forecast" property="City"/>" style="vertical-align: middle" checked></td>
											<td><%= city != null ? city.getName() : "" %></td>
										</tr>
									</ffi:list>
									
								</table>
								<!-- </fieldset> -->
								
							</td>
						</tr>
						<tr><td height="15px">&nbsp;</td></tr>
						<tr>
							<td>
								<!-- <fieldset style="padding:10px"> -->
									<span style="font-weight:bold"><s:text name="jsp.home_24"/></span>
									<table>
										<tr>
											<td nowrap>
												<s:text name="jsp.home_225"/><br/>
												<select id="<s:property value="portletId"/>_forecastStates"  style="vertical-align: middle" onChange="<s:property value="portletId"/>_getStateCities()">
													<ffi:list collection="ForecastStates" items="State">
													<ffi:setProperty name="Compare" property="Value1" value="${State.Abbr}" />
													<ffi:setProperty name="Compare" property="Value2" value="${ForecastStates.Selected}" />
				
													<option value="<ffi:getProperty name="State" property="Abbr"/>" <ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="State" property="Name"/></ffi:list>
												</select>
											</td>
											<td>&nbsp;&nbsp;</td>
											<td nowrap>
												<s:text name="jsp.default_541"/><br/>
												<select id="<s:property value="portletId"/>_forecastCountries" class="txtbox" onChange="<s:property value="portletId"/>_getCountryCities()">
													<ffi:list collection="ForecastCountries" items="Country">
													<ffi:setProperty name="Compare" property="Value1" value="${Country.Abbr}" />
													<ffi:setProperty name="Compare" property="Value2" value="${ForecastCountries.Selected}" />
				
													<option value="<ffi:getProperty name="Country" property="Abbr"/>" <ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="Country" property="Name"/></ffi:list>
												</select>
											</td>
											
										</tr>
										<tr>
											<td nowrap>
												<ffi:flush/>
												<ffi:cinclude value1="${ForecastStates.Selected}" value2="UN" operator="notEquals" >
													<s:include value="/pages/jsp/home/portlet/portlet-state-cities.jsp" />
												</ffi:cinclude>
											</td>
											<td>&nbsp;&nbsp;</td>
											<td nowrap>
												<ffi:flush/>
												<ffi:cinclude value1="${ForecastCountries.Selected}" value2="UN" operator="notEquals" >
													<s:include value="/pages/jsp/home/portlet/portlet-country-cities.jsp" />
												</ffi:cinclude>
											</td>
										</tr>
									</table>
								<!-- </fieldset> -->
							</td>
						</tr>
						<!-- <tr>
							<td height="15px">&nbsp;</td>
						</tr> -->
						<tr>
							<td>
								<!-- <fieldset style="padding:10px"> -->
									<span style="font-weight:bold"><s:text name="jsp.home_201"/></span>
									<br/>
									<input type="radio" name="<s:property value="portletId"/>_scale" value="F" <ffi:cinclude value1="${ForecastSettingsTemp.Scale}" value2="C" operator="notEquals" >checked</ffi:cinclude> ><s:text name="jsp.home_88"/>
									&nbsp;&nbsp;
									<input type="radio" name="<s:property value="portletId"/>_scale" value="C" <ffi:cinclude value1="${ForecastSettingsTemp.Scale}" value2="C" operator="equals" >checked</ffi:cinclude> ><s:text name="jsp.home_51"/>
								<!-- </fieldset> -->
							</td>
						</tr>
						<tr>
							<td height="15px">&nbsp;</td>
						</tr>
						<tr style="text-align: center">
							<td>
								<a id="<s:property value="portletId"/>_anchor" href="javascript:void(0)" onclick="<s:property value="portletId"/>_save()" ><s:text name="jsp.default_303"/></a>
							</td>
						</tr>
					</table>
			</td>
		</tr>
		
	</table>

<%-- ================= MAIN CONTENT END ================= --%>

<ffi:setProperty name="ForecastStates" property="Selected" value="UN" />
<ffi:setProperty name="ForecastCountries" property="Selected" value="UN" />
