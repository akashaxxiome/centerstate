<%@page import="com.ffusion.tasks.banking.GetPagedTransactions"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


<ffi:cinclude value1="${GetPresentmentSummariesForDisbursementPortlet}" value2="" operator="equals">
		<ffi:object id="GetPresentmentSummariesForDisbursementPortlet" name="com.ffusion.tasks.disbursement.GetPresentmentSummaries" scope="session"/>
</ffi:cinclude>
<ffi:setProperty name="GetPresentmentSummariesForDisbursementPortlet" property="DataClassification" value="<%=GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY %>"/> 

<%
	session.setAttribute("FFIGetPresentmentSummariesForDisbursementPortlet", session.getAttribute("GetPresentmentSummariesForDisbursementPortlet"));
%>


<ffi:setProperty name="populatePresentMentDisbursementPortletDataURL" value="/cb/pages/jsp/home/PresentmentDisbursementsPortletAction_loadViewDetailsMode.action" URLEncrypt="true"/>

<div id="presentMentDisbursementPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<div id="presentMentDisbursementPortletDataDiv">
	<s:action name="PresentmentDisbursementsPortletAction_loadViewDetailsMode" namespace="/pages/jsp/home" executeResult="true">
	 	<s:param name="portletId" value="%{#portletId}" />
	 </s:action>
</div>
