<%@page import="com.ffusion.tasks.banking.GetPagedTransactions"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:object id="GetDisplaySummariesForAccountForCashFlowPortlet" name="com.ffusion.tasks.banking.GetDisplaySummariesForAccount" scope="session"/>
<ffi:setProperty name="GetDisplaySummariesForAccountForCashFlowPortlet" property="AccountsName" value="CashFlowEntitlementFilteredAccounts"/>
<ffi:setProperty name="GetDisplaySummariesForAccountForCashFlowPortlet" property="SummariesName" value="CashFlowAccountSummaries"/>

<%
  session.setAttribute("FFIGetDisplaySummariesForAccountCashFlowPortlet", session.getAttribute("GetDisplaySummariesForAccountForCashFlowPortlet"));
%>

<ffi:object id="GetSummariesForAccountDateForCashFlowPortlet" name="com.ffusion.tasks.banking.GetSummariesForAccountDateFromDC" scope="session"/>
<ffi:setProperty name="GetSummariesForAccountDateForCashFlowPortlet" property="AccountsName" value="CashFlowEntitlementFilteredAccounts"/>
<ffi:setProperty name="GetSummariesForAccountDateForCashFlowPortlet" property="SummariesName" value="CashFlowAccountSummaries"/>
<ffi:setProperty name="GetSummariesForAccountDateForCashFlowPortlet" property="DataClassification" value="<%=GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY %>"/>
<%-- this informs the task to search for a summaries, if no date is provided, but as soon as one summary is found for --%>
<%-- a particular date, return all summaries for that date (even if some summaries will be empty --%>
<ffi:setProperty name="GetSummariesForAccountDateForCashFlowPortlet" property="AllSummariesOnSameDate" value="true"/>
<ffi:setProperty name="GetSummariesForAccountDateForCashFlowPortlet" property="BatchSize" value="50"/>

<%
 session.setAttribute("FFIGetSummariesForAccountDateCashFlowPortlet", session.getAttribute("GetSummariesForAccountDateForCashFlowPortlet"));
 session.setAttribute("portletId", request.getAttribute("portletId"));
%>

<ffi:setProperty name="populateURL" value="/cb/pages/jsp/home/CashFlowPortletAction_loadViewDetailsMode.action" URLEncrypt="true"/>

<div id="cashFlowDataGridDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick="ns.home.changeCashFlowPortletView('donut')"><span class="sapUiIconCls icon-pie-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDonut' /></a></li>
		<li><a href='#' onclick="ns.home.changeCashFlowPortletView('bar')"><span class="sapUiIconCls icon-bar-chart" style="float:left;"></span><s:text name='jsp.home.viewAsBar' /></a></li>
		<li><a href='#' onclick="ns.home.changeCashFlowPortletView('dataTable')"><span class="sapUiIconCls icon-table-chart" style="float:left;"></span><s:text name='jsp.home.viewAsDataTable' /></a></li>
		<li><a href='#' onclick="ns.home.changeCashFlowPortletView('chartSettings')"><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<div id="cashFlowDataGridDiv" class="marginBottom5">
	<s:action name="CashFlowPortletAction_loadViewDetailsMode" namespace="/pages/jsp/home" executeResult="true">
	 	<s:param name="portletId" value="%{#portletId}" />
	 </s:action>
</div>