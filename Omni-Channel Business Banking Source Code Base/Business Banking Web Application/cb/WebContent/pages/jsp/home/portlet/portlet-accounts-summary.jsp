<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.accounts.AccountTypes"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummaries"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummary"%>
<%@ page import="com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.accounts.AssetAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.DepositAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.LoanAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.CreditCardAcctSummary"%>
<%@ page import="com.ffusion.beans.common.Currency"%>
<%@ page import="java.util.Collection"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
 <%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript" src="<s:url value='/web/js/account/account_grid%{#session.minVersion}.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/accounts/accounts%{#session.minVersion}.css'/>"></link>

<ffi:author page="inc/portal/accounts.jsp"/>
<ffi:help id="home_portlet-account-summary"/>
<ffi:removeProperty name="displayCurrencyCode"/>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_SETUP%>">
	<ffi:setProperty name="editurl" value="account-edit-wait.jsp"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.ACCOUNT_SETUP%>">
	<ffi:setProperty name="editurl" value="none"/>
</ffi:cinclude>
<%-- 
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<s:include value="inc/accounts-top-container.jsp"/>
</ffi:cinclude>
 --%>
<div id="accountsSummaryGridDiv">
<s:include value="inc/accounts-container.jsp"/>
</div>

<div id="accountsSummaryGridDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<sj:dialog id="accountsSummaryExportDialogID" cssClass="homeDialog" 
title="%{getText('jsp.default.label.account.summary.export')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" height="300" showEffect="fold" hideEffect="clip" width="590">
</sj:dialog>

<script type="text/javascript">
<!--
$('#accountsSummaryExportDialogID').addHelp(function(){
	var helpFile = $('#accountsSummaryExportDialogID').find('.moduleHelpClass').html();
	callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
});
//-->
</script>