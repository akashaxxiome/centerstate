<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_switchToEdit() {
		$('#<s:property value="portletId"/>').portlet('edit');
	}
</script>

<% String v38_4a = ""; %>

<ffi:help id="home_portlet-calc"/>
<div id="calculatorPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick='<s:property value="portletId"/>_switchToEdit()'><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<table border="0" cellspacing="0" cellpadding="0" width="100%" class="marginBottom5">

	<ffi:setProperty name="hasCalItems" value="false"/>
	
	<ffi:list collection="Calculators" items="Calc1">
		<ffi:setProperty name="hasCalItems" value="true"/>
		<tr>
			<td>
				<span class="ui-icon-carat-1-e ui-icon" style="float:left"></span>
                <ffi:link url="${Calc1.URL}" target="_blank" ><ffi:getProperty name="Calc1" property="Name"/></ffi:link>
            </td>
		</tr>
	</ffi:list>
	<ffi:cinclude value1="false" value2="${hasCalItems}" operator="equals">
		<tr>
			<td align="center">
				<br><br><br><br><br>
				<s:text name="jsp.home_127"/><br>
				<s:text name="jsp.home_56"/> <a href='javascript:void(0)' class="sapUiLnk" onclick='<s:property value="portletId"/>_switchToEdit()'><s:text name="jsp.default_179"/></a> <s:text name="jsp.home_214"/>
			</td>
		</tr>
	</ffi:cinclude>
</table>
