<%@ page import="com.ffusion.beans.billpay.PaymentDefines,
				 com.ffusion.beans.billpay.Payments,
				 com.ffusion.beans.billpay.RecPayments,
				 com.ffusion.beans.billpay.Payment,
				 com.ffusion.tasks.billpay.Task,
				 java.util.ArrayList,
				 com.ffusion.csil.core.common.EntitlementsDefines"%>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="inc/portal/pendingpayments.jsp"/>
<ffi:help id="home_portlet-pending-transactions"/>
<%-- following action is required to support delete payment action starts --%>
<%
	session.setAttribute(Task.PAYMENTS,new Payments());
	session.setAttribute(Task.RECPAYMENTS, new RecPayments());
	session.setAttribute(Task.PAYHISTORYPAYEES, new ArrayList());
%>

<%-- above action is required to support delete payment action ends --%>
<%-- following action is required to support delete transfer action starts --%>
<ffi:object name="com.ffusion.tasks.banking.SetTransfer" id="SetTransfer" scope="session" />
<ffi:object name="com.ffusion.tasks.banking.SetFundsTransaction" id="SetFundsTran" scope="session" />
<%-- above action is required to support delete transfer action ends --%>

<%-- following action is required to support edit payment action starts --%>
<%--
<ffi:object id="GetPaymentAccounts" name="com.ffusion.tasks.billpay.GetPaymentAccounts" scope="session"/>
	<ffi:setProperty name="GetPaymentAccounts" property="AccountsName" value="BillPayAccounts"/>
<ffi:process name="GetPaymentAccounts"/>
--%>
<%-- above action is required to support edit payment action ends --%>

<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.TRANSFERS %>" >
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS %>" >
		<ffi:setProperty name="style" value="payments"/>
		<ffi:setL10NProperty name="tmp" value="PENDING TRANSACTIONS"/>
		<ffi:setProperty name="itemtitle" value="${tmp}"/>
		<ffi:setProperty name="editurl" value="none"/>
	</ffi:cinclude>
</ffi:cinclude>


<%-- <div id="pendingPaymentsData">
	   <s:include value="inc/pendingpayments.jsp"/>
</div>
 --%>

<!-- <div onload="loadPendingTransactions($(this));"> -->
<div>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS %>" >
				<%-- PENDINGPAYMENTS --%>
				<div id="pendingPaymentsData-menu" class="portletHeaderMenuContainer" style="display:none;">
					<ul class="portletHeaderMenu">
						<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
							<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
						</ffi:cinclude>
						<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
					</ul>
				</div>
	
			<div id="pendingPaymentsData">
	   			<s:include value="inc/pendingpayments.jsp"/>
			</div>
	
			<br><br><br><br><br>
			<div id="pendingPaymentsNoData">
			</div>
			
			</ffi:cinclude>
</div>


<br>
<script type="text/javascript">
/*
function loadPendingTransactions(header){
		var contentLoaded = $(header).attr('contentLoaded');
		//Bypass the grid reload call if the Grid already has data
		if(contentLoaded != 'true'){
			var gridElement= $(header).next().find(".ui-jqgrid");
			if(gridElement){
				gridId = $(gridElement).attr("id").substr(5);
				ns.common.reloadFirstGridPage('#'+gridId);
				ns.common.resizeWidthOfGrids();
				$(header).attr('contentLoaded','true');//Set the content as loaded, so as to avoid loading next time.
			}
		}
	}
*/
</script>
































<%-- <%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

</body>
</html> --%>