<%@ page import="com.ffusion.beans.banking.TransferDefines,
				 com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- <s:text name="jsp.transfers_text_transfers_uppercase"/> --%>
<s:set var="DisplayToAmount" value="false" />
<s:set var="trueFlag" value="true" /> 

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_TRANSFERS %>">
    <s:set var="DisplayToAmount" value="true" />
</ffi:cinclude> 

<s:include value="pendingtransfers-grid.jsp"/>
