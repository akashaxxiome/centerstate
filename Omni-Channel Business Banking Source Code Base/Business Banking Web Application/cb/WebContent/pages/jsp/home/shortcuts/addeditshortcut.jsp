<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
<!--

/**
 * Remove the error message on the page before submitting the form to validate input.
 */
$.subscribe('addShortcutFormBeforeTopics', function(event,data) {
	removeValidationErrors();	
});

$.subscribe('cancelAddEditShortcutFormClickTopics', function(event,data) {
	ns.common.closeDialog("#addShortcutDialogID");	
});

$.subscribe('refreshShortCutsAreaSuccessTopics', function(event,data) {
	//alert("comp");
	ns.common.closeDialog("#addShortcutDialogID");
	$.publish('refreshShortcutsPanelPanel');	
});
//-->
</script>

<ffi:object id="ShortCutSetting" name="com.ffusion.struts.shortcut.bean.ShortCutSetting" scope="session"/>
<% session.setAttribute("FFIShortCutSetting", session.getAttribute("ShortCutSetting")); %>
<%
	String emptyPath = "false";
	String shortcutPath =  request.getParameter("newShortcutPath");
	String existingFlag = "false";
	if( shortcutPath==null || ( shortcutPath != null && shortcutPath.equals("null") ) || ( shortcutPath != null && shortcutPath.trim().length() == 0 ) ){
		emptyPath = "true";
	}
%>
<ffi:cinclude value1="true" value2="<%= emptyPath%>" operator="equals">
	<div align="center" class="dialogueData tableData">
		<fieldset id="notAllowedAddShortInfoID">
			<legend><s:text name="jsp.home_233"/></legend>
			<span><s:text name="jsp.home_238"/></span> <br><br>
			<sj:a id="canNotAddShortcutForEmptyPathID" 
				button="true" 
				onClickTopics="cancelAddEditShortcutFormClickTopics"
				><s:text name="jsp.default_102"/></sj:a>
		</fieldset>
	</div>
</ffi:cinclude>

<ffi:list collection="UserShortcuts" items="shortCut">
	<ffi:cinclude value1="${shortCut.shortCutPath}" value2="<%= shortcutPath%>" operator="equals">
		<% 
			existingFlag = "true"; 
		%>
		<div align="center" class="tableData">
			<fieldset id="notAllowedAddShortInfoID">
				<legend><s:text name="jsp.home_233"/></legend>
				<span><s:text name="jsp.home_239"/></span> <br><br>
				<sj:a id="cancelForNotAllowedAddShortcutID" 
					button="true" 
					onClickTopics="cancelAddEditShortcutFormClickTopics"
					><s:text name="jsp.default_102"/></sj:a>
			</fieldset>
		</div>
	</ffi:cinclude>
</ffi:list>

<ffi:cinclude value1="false" value2="<%= existingFlag%>" operator="equals">
  <ffi:cinclude value1="false" value2="<%= emptyPath%>" operator="equals">
	<div align="center" class="dialogueData">
	<fieldset id="addNewShortcutForFieldSetID">
		<legend><s:text name="jsp.home_158"/></legend>
		<span id="ShortCutSetting.shortCutNameError"></span><ul id="formerrors"></ul>
		<span id="InvalidShortCutPathError"></span>
		<s:form name="addEditShortcutForm" action="manageShortcut" namespace="/pages/shortcut" method="post" validate="false" id="addEditShortcutFormID"
			onsubmit="$('#addEditShortcutFormAddButtonID').trigger('click');return false">
			
           	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
           	<input type="hidden" name="ShortCutSetting.categoryID" value="0"/>
			<table border="0" align="center">
				<tr>
					<td>
						<input name="ShortCutSetting.shortCutName" size="20" class="ui-widget ui-widget-content" maxlength="20"/>
					</td>
				</tr>
				<tr>
					<td><span id="ShortCutSetting.shortCutNameError"/>
					</td>
				</tr>
					
			</table>
			<!--  #parameters.newShortcutPath: To get the path from http request. -->
			<input type="hidden" name="ShortCutSetting.shortCutPath" value='<s:property value="#parameters.newShortcutPath" />'/>
			<input type="hidden" name="ShortCutSetting.entitlementSetting" value='<s:property value="#parameters.entitlementSetting" />'/>
		</s:form>
		<br>
		<div align="center">
			<sj:a id="cancelAddEditShortcutFormID" 
				button="true" 
				onClickTopics="cancelAddEditShortcutFormClickTopics"
				><s:text name="jsp.default_82"/></sj:a>
			<sj:a 
					id="addEditShortcutFormAddButtonID"
					button="true"
					formIds="addEditShortcutFormID"
					validate="true"
					validateFunction="customValidation"
					onBeforeTopics="addShortcutFormBeforeTopics"
					onCompleteTopics="refreshShortCutsAreaSuccessTopics"
					targets="addEditShortcutStatusID"
					onSuccessTopics=""
                    ><s:text name="jsp.default_29"/></sj:a>
		</div>
		<div id="addEditShortcutStatusID"></div>
		<br>
	</fieldset>
	</div>
  </ffi:cinclude>
</ffi:cinclude>
