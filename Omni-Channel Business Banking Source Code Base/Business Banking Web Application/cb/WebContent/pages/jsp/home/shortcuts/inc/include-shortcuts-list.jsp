<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<li class="shortcutListItems"> 
    <span id="<ffi:getProperty name="shortCut" property="shortcutID"/>">
    	<a href="javascript:void(0)" class="ui-button" onclick="ns.shortcut.goToFavorites('<ffi:getProperty name="shortCut" property="shortCutPath"/>');" title="<s:text name="jsp.home_100"/>">
    		<ffi:getProperty name="shortCut" property="shortCutName"/>
    	</a>
    </span>
    <%-- 
    <a href="javascript:void(0)" class="ui-corner-all fg-menu-indicator" onclick="ns.shortcut.goToFavorites('<ffi:getProperty name="shortCut" property="shortCutPath"/>');" title="<s:text name="Go_to_the_page" />">
    	<span class="ui-icon ui-icon-arrowthick-1-e"></span>
    </a>
    --%>
	<%--URL Encryption Edit By Dongning --%>
    <a href="javascript:void(0)" class="ui-button" onclick="ns.shortcut.deleteOneShortcut('<ffi:urlEncrypt url="/cb/pages/shortcut/manageShortcut_deleteOneShortcut.action?shortcutName=${shortCut.shortCutName}"/>');" title="<s:text name="Delete_the_shortcut" />">
    	<span class="ui-icon ui-icon-trash"></span>
    </a>
    <a href="javascript:void(0)" class="ui-button" title="<s:text name="Edit_the_shortcut" />" onclick="ns.shortcut.editShortcut('<ffi:getProperty name="shortCut" property="shortcutID"/>');">
    	<span class="ui-icon ui-icon-pencil"></span>
    </a>
    
</li>
