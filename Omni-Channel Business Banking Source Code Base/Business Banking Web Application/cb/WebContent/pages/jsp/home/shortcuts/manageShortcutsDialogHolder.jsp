<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<sj:dialog
	id="manageShortcutsDialogId"
	autoOpen="false"
	modal="true"
	title="%{getText('jsp.default_Shortcut_Mgmt')}"
	width="500"
	showEffect="fold"
	hideEffect="clip"
	resizable="false"
>
</sj:dialog>