<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>


		<div align="left">
			<div id="frmSendMessageTarget" class="hidden" />
      <!-- <form method="post" name="frmSendMessage" action="contact_my_admin_done.jsp"> -->
      <s:form id="frmContactMyAdmin" namespace="/pages/jsp/message" validate="false" action="contactMyAdminAction" method="post" name="frmContactMyAdmin" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<s:hidden name="contactAdminMessage.from" value="%{#session.User.id}"></s:hidden>			
			<s:hidden name="contactAdminMessage.to" value="%{contactAdminMessage.to}"></s:hidden>
			<table width="100%" border="0" cellspacing="0" cellpadding="3" class="greenClayBackground">
						<tr>
							<td align="right" nowrap class="greenClayBackground" width="10%"><span class="sectionsubhead"><s:text name="jsp.default_text_to"/></span></td>
							<td width="40%" class="columndata">
								<span class="sectionsubhead"><ffi:getProperty name="contactAdminMessage" property="ToName"/></span>
							</td>
							<td width="40%"></td>
							<td width="10%"></td>
						</tr>
						<tr>
							<td align="right" nowrap class="greenClayBackground" valign="top"><span class="sectionsubhead"><s:text name="jsp.default_516"/></span><span class="required">*</span></td>
							<td colspan="3"><input class="txtbox ui-widget-content ui-corner-all" type="text" id="contactMyAdmin_subject" name="contactAdminMessage.subject" size="60" maxlength="100">
								<br/><span id="subjectError"></span>
							</td>
						</tr>
						<tr>
							<td align="right" class="greenClayBackground" valign="top"><span class="sectionsubhead"><s:text name="jsp.message_10"/></span><span class="required">*</span></td>
							<td colspan="3" class="greenClayBackground" style="word-wrap: break-word"><textarea class="txtbox ui-widget-content ui-corner-all" id="contactAdminTxtArea" name="contactAdminMessage.memo" rows="9" cols="40"></textarea>
								<br/><span id="memoError"></span>
							</td>
						</tr>
						<tr>	
							<td align="center" colspan="4">
							<span class="required">* <s:text name="jsp.default_240"/></span>
							</td>
						</tr>
						<tr>
							<td colspan="4" align="center" nowrap class="greenClayBackground">

									<sj:a button="true" 
											onClickTopics="closeDialog"
							        ><s:text name="jsp.default_82"/></sj:a>
							        
						            <sj:a
										id="contactMyAdminSubmit"
										formIds="frmContactMyAdmin"
			                            targets="frmSendMessageTarget" 
			                            button="true" 
			                            validate="true" 
			                            validateFunction="customValidation"
			                            onBeforeTopics="beforeSendingContactMyAdmin"
			                            onCompleteTopics=""
										onErrorTopics=""
			                            onSuccessTopics="successContactMyAdmin"
			                        ><s:text name="jsp.default_378"/></sj:a>
			                        
							</td>
						</tr>
					</table>
			</s:form>
			<br>
		</div>

<script>
	$(document).ready(function() {
		setTimeout(function(){ns.common.renderRichTextEditor('contactAdminTxtArea','535');}, 500);
	});
</script>