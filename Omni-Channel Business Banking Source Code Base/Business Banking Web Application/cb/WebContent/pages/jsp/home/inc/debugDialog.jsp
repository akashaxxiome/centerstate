<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- a dialog used to display debug information - used by jquery.debug.js --%>
<sj:dialog
   	id="debugDiv"
   	autoOpen="false"
   	modal="false"
   	height="500"
   	title="%{getText('jsp.home_76')}"
   	cssClass="debugDialog"
   >
   	<div id="debugObjectInfo"><ol></ol></div>

   </sj:dialog>
