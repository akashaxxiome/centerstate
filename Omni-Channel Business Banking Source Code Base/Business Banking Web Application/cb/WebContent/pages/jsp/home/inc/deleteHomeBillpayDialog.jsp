<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<sj:dialog id="deleteHomeBillpayDialogID" title="%{getText('jsp.billpay_43')}" modal="true"
	resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="750"
	onCloseTopics="clearDialogContent">
</sj:dialog>
