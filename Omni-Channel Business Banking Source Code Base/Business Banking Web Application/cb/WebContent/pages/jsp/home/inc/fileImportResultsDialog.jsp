<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<sj:dialog id="checkFileImportResultsDialogID" title="%{getText('jsp.default_210')}" modal="true" position="['center','center']"  autoOpen="false" resizable="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" minHeight="50" minWidth="300" width="790">
</sj:dialog>
