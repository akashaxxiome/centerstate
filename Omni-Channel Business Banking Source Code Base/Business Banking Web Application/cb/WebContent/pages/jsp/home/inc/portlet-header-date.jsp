<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="request"/>
	<ffi:process name="GetCurrentDate"/>
	<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateFormat}" />
<ffi:getProperty name="GetCurrentDate" property="Date" />