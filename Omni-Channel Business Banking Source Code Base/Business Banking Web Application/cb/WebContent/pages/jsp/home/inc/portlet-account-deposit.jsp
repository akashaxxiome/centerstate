<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<style>
	#<s:property value="portletId"/>_colbal_summary_detail .ui-jqgrid-ftable td{
		border:none;
	}
</style>

<script type="text/javascript">
	//ns.home.initAccountDepositGrid('<s:property value="portletId"/>_conBal_deposit_datagrid', 'onDepositGridLoadComplete');
</script> 
<%-- <s:include value="/pages/jsp/home/inc/portlet-con-bal-options.jsp" /> --%>
<div id="depositAccountsPortletID">
	<ffi:setProperty name="tempURL" value="/pages/jsp/home/ConsolidatedBalancePortletAction_loadDepositData.action?queryDataClass=${queryDataClass}&portletId=${portletId}" URLEncrypt="true"/>
	<s:url id="account_deposit_data_action" value="%{#session.tempURL}"/>
	<sjg:grid id="%{portletId}_conBal_deposit_datagrid"
					dataType="local" 
					href="%{account_deposit_data_action}" 
					pager="true"
     				gridModel="summaries"
					hoverrows="false" 
					footerrow="false"
					userDataOnFooter="false"
					rowList="%{#session.StdGridRowList}" 
	 				rowNum="5"
					sortable="true"					
					rownumbers="false"
     				navigator="true"
	 				navigatorAdd="false"
	 				navigatorDelete="false"	
					navigatorEdit="false"
					navigatorRefresh="false"
					navigatorSearch="false"
					navigatorView="false" 
					shrinkToFit="true"					
					scroll="false"
					scrollrows="true"
					viewrecords="true"
					onGridCompleteTopics="onDepositGridLoadComplete,enableGridAutoWidth">
			 <sjg:gridColumn name="account.ID" index="ID" title="%{getText('jsp.account_12')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
			<sjg:gridColumn name="account.bankID" index="BANKID" title="%{getText('jsp.account_39')}" sortable="false" hidden="true" hidedlg="true" />
			<sjg:gridColumn name="account.routingNum" index="ROUTINGNUM" title="%{getText('jsp.account_183')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>	
			<sjg:gridColumn name="nameNickNamedeposit" index="NUMBER" title="%{getText('jsp.default_15')}" width="150" align="left" sortable="true"  formatter="ns.home.formatAccountColumn" hidden="true"/>
			<sjg:gridColumn name="account.nickName" index="ACCNICKNAME" title="%{getText('jsp.default_293')}" width="320" align="left" sortable="true"  formatter="ns.home.customToAccountNickNameColumn" hidden="true"/>
			<sjg:gridColumn name="account.accountDisplayText" title="Account" width="120" align="center" sortable="false" formatter="ns.home.formatDepositAcctColumn" cssClass="datagrid_actionColumn"  />
			<sjg:gridColumn name="account.bankName" index="BANKNAME" title="%{getText('jsp.default_61')}" width="100" align="left" sortable="true" hidden="true"/>
			<%-- <sjg:gridColumn name="account.type" index="TYPESTRING" title="%{getText('jsp.default_444')}" width="100" align="left" sortable="true"/> --%>
			<sjg:gridColumn name="summaryDateDisplay" index="SummaryDateDisplay" title="%{getText('jsp.default_51')}" width="90" align="center" sortable="true" hidden="true"/>
			<sjg:gridColumn name="displayOpeningLedger" index="OPENING_LEDGER" title="%{getText('jsp.default_305')}" width="90" align="left" sortable="true" formatter="ns.home.openingLedgerDepositFormatter" hidden="false"/>
			<sjg:gridColumn name="totalDebits" index="TOTAL_DEBITS" title="%{getText('jsp.default_434')}" width="120" align="right" sortable="true" formatter="ns.home.totalDebitsFormatter" hidden="true"/>
			<sjg:gridColumn name="totalCredits" index="TOTAL_CREDITS" title="%{getText('jsp.default_433')}" width="120" align="right" sortable="true" formatter="ns.home.totalCreditsFormatter" hidden="true"/>
			<sjg:gridColumn name="closingLedger.currencyStringNoSymbol" index="closingLedger.currencyStringNoSymbol" title="%{getText('jsp.default_104')}" formatter="ns.home.balanceFormatter" width="50" align="left" sortable="true" cssClass="datagrid_amountColumn"/>
			<sjg:gridColumn name="displayClosingAmountValue" index="CLOSINGBALANCE" title="%{getText('jsp.default_104')}" width="70" align="right" sortable="true"  formatter="ns.home.closingLedgerDepositeGridFormatter" hidden="true" hidedlg="true" /> 
			<sjg:gridColumn name="displayOpeningLedgerAmountValue" index="displayOpeningLedgerAmountValue" title="%{getText('jsp.account_131')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="dataClassification" index="dataClassification" title="%{getText('jsp.account_64')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
	</sjg:grid>
	
	<div id="<s:property value="portletId"/>_conBal_deposit_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('acctMgmt_balance,depositAccounts');"><s:text name="jsp.default.viewAllRecord" /></div>
	
<script>
 $(document).ready(function(){
   if(ns.common.isInitialized($("#<s:property value='portletId'/>_conBal_deposit_datagrid",'ui-jqgrid'))){
     $("#<s:property value='portletId'/>_conBal_deposit_datagrid tbody").sortable("destroy");
   }
 });
</script>

</div>
