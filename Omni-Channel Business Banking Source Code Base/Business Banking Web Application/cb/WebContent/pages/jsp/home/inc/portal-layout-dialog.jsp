<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:url id="pageurl" value="/pages/jsp/home/getSystemLayouts.action" escapeAmp="false">
</s:url>

<sj:dialog id="portalPageLayout" title="%{getText('jsp.home_148')}" position="['center','middle']"
		hideEffect="clip" onBeforeTopics="onBeforeLayoutDialogOpen" cssClass="staticContentDialog homeDialog" cssStyle="overflow:visible;"
		showEffect="fold" width="700" height="340" autoOpen="false" href="%{pageurl}"
		modal="true" resizable="false" overlayColor="#000" overlayOpacity="0.7">
</sj:dialog>