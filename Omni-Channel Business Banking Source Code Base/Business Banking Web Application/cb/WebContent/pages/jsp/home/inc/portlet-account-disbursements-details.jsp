<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript">
var prefix = ' ';
<ffi:cinclude value1="en_US" value2="${UserLocale.Locale}" operator="equals" >
	prefix = ' as of ';
</ffi:cinclude>
</script>
<ffi:help id="home_portlet-account-disbursements"/>
<ffi:cinclude value1="" value2="${DisbursementAccountSettings}" operator="equals">
	<div align="center"><s:text name="jsp.default_552"/></div>
</ffi:cinclude>

<ffi:cinclude value1="" value2="${DisbursementAccountSettings}" operator="notEquals">
	<ffi:cinclude value1="${DisbursementAccountSettings.size}" value2="0" operator="equals">
		<div align="center"><s:text name="jsp.default_552"/></div>
	</ffi:cinclude>
	<ffi:cinclude value1="${DisbursementAccountSettings.size}" value2="0" operator="notEquals">
	<%--URL Encryption Edit By Dongning --%>
		<script type="text/javascript">
			ns.home.initAccountDisPortlet('<s:property value="portletId"/>', 
									'<s:property value="portletId"/>_accountDis_datagrid',
									'<ffi:urlEncrypt url="/cb/pages/jsp/home/AccountDisbursementsPortletAction_updateColumnSettings.action?portletId=${portletId}"/>',
									'<s:property value="portletId"/>_grid_load_complete',
									'<s:property value="columnPerm"/>',
									'<s:property value="hiddenColumnNames"/>');
		</script>
		<%-- URL Encryption Edit By Dongning --%>
		<ffi:setProperty name="accountDisPortletTempURL" value="/pages/jsp/home/AccountDisbursementsPortletAction_loadAccountDisData.action?portletId=${portletId}" URLEncrypt="true"/>
		<s:url id="accountDis_data_action" value="%{#session.accountDisPortletTempURL}"/>
		<sjg:grid id="%{portletId}_accountDis_datagrid"
						sortable="true"  
						dataType="json" 
						href="%{accountDis_data_action}" 
						gridModel="gridModel"
						pager="true" 
						rowList="%{#session.StdGridRowList}" 
		 				rowNum="5"
		 				viewrecords="true"
		 				rownumbers="false"
	     				navigator="true"
		 				navigatorAdd="false"
		 				navigatorDelete="false"
						navigatorEdit="false"
						navigatorRefresh="false"
						navigatorSearch="false"
						navigatorView="false"
						shrinkToFit="true"					
						scroll="false"
						scrollrows="true"			
						hoverrows="true"
						sortname="ACCOUNTID"
						onGridCompleteTopics="%{portletId}_grid_load_complete"
						>         
				<sjg:gridColumn name="account.routingNumber" index="ACCOUNTID" title="%{getText('jsp.default_15')}" width="150" align="left" sortable="true" hidedlg="true" formatter="ns.home.accountDisAccountIDFormatter"/>
				<sjg:gridColumn name="account.accountName" index="NICKNAME" title="%{getText('jsp.default_293')}" width="250" align="left" sortable="true" formatter="ns.home.accountDisAccountNickNameFormatter"/>
				<sjg:gridColumn name="currentBalance.currencyStringNoSymbol" index="CURRENTBALANCE" title="%{getText('jsp.default_60')}" width="70" align="right" sortable="true" hidden="true"/>
				<sjg:gridColumn name="numItemsPending" index="NUMITEMSPENDING" title="%{getText('jsp.home_109')}" width="100" align="center" sortable="true" formatter="ns.home.accountDisAccountItemsPendingFormatter" hidden="true"/>
				<sjg:gridColumn name="totalDebits.currencyStringNoSymbol" index="TOTALDEBITS" title="%{getText('jsp.default_43')}" width="100" align="right" sortable="true" hidden="true"/>
				<sjg:gridColumn name="immediateFundsNeeded.currencyStringNoSymbol" index="IMMEDIATEFUNDSNEEDED" title="%{getText('jsp.default_222')}" width="120" align="right" sortable="true"/>
				<sjg:gridColumn name="oneDayFundsNeeded.currencyStringNoSymbol" index="ONEDAYFUNDSNEEDED" title="%{getText('jsp.home_96')}" width="120" align="right" sortable="true" hidden="true"/>
				<sjg:gridColumn name="twoDayFundsNeeded.currencyStringNoSymbol" index="TWODAYFUNDSNEEDED" title="%{getText('jsp.home_97')}" width="120" align="right" sortable="true" hidden="true"/>
		</sjg:grid>
		
		<div id="<s:property value="portletId"/>_accountDis_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('cashMgmt_disburs');"><s:text name="jsp.default.viewAllRecord" /></div>
		
		<!-- below commented for CR :770537 -->
		<%-- <div class="sectionsubhead ltrow2_color"><br/>* <s:text name="jsp.home_202"/></div> --%>
		<script>
		if($("#<s:property value='portletId'/>_accountDis_datagrid tbody").data("ui-jqgrid") != undefined){
			$("#<s:property value='portletId'/>_accountDis_datagrid tbody").sortable("destroy");
		}
		</script>
	</ffi:cinclude>
</ffi:cinclude>