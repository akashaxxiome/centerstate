<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<s:set var="wiresReleaseCount" value="10" scope="action"/>

<ffi:help id="home_leftbar_approvalsmallpanel"/>
<% boolean displayHeading = false; %>
					   <ffi:cinclude value1="${IsApprover}" value2="true" operator="equals">
<% displayHeading = true; %>
	   				    </ffi:cinclude>
					    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>">
<% displayHeading = true; %>
	   				    </ffi:cinclude>
	   				     <ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>">
<% displayHeading = true; %>
	   				    </ffi:cinclude>
					    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES %>">
						    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES_RELEASE %>">
<% displayHeading = true; %>
   						    </ffi:cinclude>
   					    </ffi:cinclude>
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
<% displayHeading = true; %>
					    </ffi:cinclude>
					    
<% if (displayHeading) { %>
<% } %>

<div class="approvalMasterWrapper">
	
	<div id="approvalTopActionBar" class="approvalTopActionBarCls">
		<div class="floatleft topActionItemsCls">
			<span id="unReadApprovals"><s:text name="jsp.approvals_12" /></span>
		</div>
	</div>
	<!-- if no pending task -->
	<s:if test="%{numberOfIssues+wiresReleaseCount+pendingApprovalCount==0 && (daPendingCount==-1 || daPendingCount==0) && (daPendingCompanyCount==-1 || daPendingCompanyCount==0)}">
		 <span class="noMsgSpan"><s:text name="jsp.home_134" /></span>
	</s:if>
	<s:else>
	   <ffi:cinclude value1="${IsApprover}" value2="true" operator="equals">
	   	<s:set var="pendingApprovalCount" value="%{pendingApprovalCount}" />
		   	<ffi:cinclude value1="${pendingApprovalCount}" value2="-1" operator="equals">
				<span><s:text name="jsp.home_223"/></span>
    		</ffi:cinclude>
			<ffi:cinclude value1="${pendingApprovalCount}" value2="-1" operator="notEquals">
				<ffi:cinclude value1="${pendingApprovalCount}" value2="0" operator="notEquals">
					<s:url id="ajax" value="/pages/jsp/approvals/pendingApprovalIndex.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
					   	<s:param name="subpage" value="%{'pending'}"/>
					</s:url>
					<div class="approvalDataHolder">
						<sj:a id="pendingApprovalLink" href="%{ajax}" indicator="indicator" targets="notes" onClick="$('li[menuId=\"approvals_pending\"] a').click();" onCompleteTopics="subMenuOnCompleteTopics" cssClass="approvalDetails" cssStyle="text-decoration:none;">
							<s:property value="%{pendingApprovalCount}"/>&nbsp;<s:text name="jsp.home_124"/>
						</sj:a>
						<div class="approvalActionItemHolder">
							<span class="ffiUiIcoMedium ffiUiIco-icon-check" title="<s:text name='jsp.default_6' />" onclick="$('#pendingApprovalLink').trigger('click')"></span>
						</div>
					</div>
				</ffi:cinclude>
			</ffi:cinclude>
	    </ffi:cinclude>
	
	    <%-- Positive Pay --%>
	    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>">
			<s:set var="numberOfIssues" value="%{numberOfIssues}" />
			<s:if test="#numberOfIssues > 0">
				<div class="approvalDataHolder">
					<ffi:setProperty name="ppayURL" value="/pages/jsp/positivepay/initPositivePayAction.action" URLEncrypt="true"/>
					<s:url id="ppayMenuURL" value="%{#session.ppayURL}" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="positivepayReload" value="true"></s:param>
					</s:url>
					<sj:a  id="ppayActionLink" href="%{ppayMenuURL}" indicator="indicator" targets="notes" onClick="$('li[menuId=\"cashMgmt_ppay\"] a').click();" onCompleteTopics="subMenuOnCompleteTopics" cssClass="approvalDetails" cssStyle="text-decoration:none;">
						<span><s:property value="%{numberOfIssues}"/>&nbsp;<s:text name="jsp.home_133.1" /></span>
					</sj:a>
					<div class="approvalActionItemHolder">
						<span class="ffiUiIcoMedium ffiUiIco-icon-check" title="<s:text name='jsp.default_6' />" onclick="$('#ppayActionLink').trigger('click')"></span>
					</div>
				</div>
			</s:if>
	    </ffi:cinclude>
	    
	   	<%-- Reverse Positive Pay --%>
	    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>">
			<s:set var="numberOfIssues" value="%{numberOfIssues}" />
			<s:if test="#numberOfIssues > 0">
				<div class="approvalDataHolder">
					<ffi:setProperty name="rppayURL" value="/pages/jsp/reversepositivepay/initReversePositivePayAction.action" URLEncrypt="true"/>
					<s:url id="rppayMenuURL" value="%{#session.rppayURL}" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="reversepositivepayReload" value="true"></s:param>
					</s:url>
					<sj:a  id="rppayActionLink" href="%{rppayMenuURL}" indicator="indicator" targets="notes" onClick="$('li[menuId=\"cashMgmt_rppay\"] a').click();" onCompleteTopics="subMenuOnCompleteTopics" cssClass="approvalDetails" cssStyle="text-decoration:none;">
						<span><s:property value="%{numberOfIssues}"/>&nbsp;<s:text name="jsp.home_133.1" /></span>
					</sj:a>
					<div class="approvalActionItemHolder">
						<span class="ffiUiIcoMedium ffiUiIco-icon-check" title="<s:text name='jsp.default_6' />" onclick="$('#rppayActionLink').trigger('click')"></span>
					</div>
				</div>
			</s:if>
	    </ffi:cinclude>
	
		<%-- Wires --%>
	    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES %>">
			<s:if test="%{isSupportRelease}">
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES_RELEASE %>">
					<ffi:setProperty name="releaseBackURL" value="${SecurePath}index.jsp" URLEncrypt="true"/>
					<ffi:cinclude value1="${WiresReleaseCount}" value2="-1" operator="notEquals">
						<ffi:cinclude value1="${WiresReleaseCount}" value2="0" operator="notEquals">
							<div class="approvalDataHolder">
								<span style="float:right;" onclick="ns.shortcut.goToFavorites('pmtTran_wire,releaseWiresLink');"></span>
								<a class="approvalDetails" onclick="ns.shortcut.goToFavorites('pmtTran_wire,releaseWiresLink');" href="javascript:void(0)"><s:property value="%{WiresReleaseCount}"/>&nbsp;<s:text name="jsp.home_125"/></a>
								<div class="approvalActionItemHolder">
									<span class="ffiUiIcoMedium ffiUiIco-icon-check" title="<s:text name='jsp.default_6' />" onclick="$('#ppayActionLink').trigger('click')"></span>
								</div>
							</div>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
			</s:if>
		</ffi:cinclude>
		
		<%--Dual Approval --%>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<s:if test="%{daPendingCount != 0}">
				<div class="approvalDataHolder">
					<a class="approvalDetails" id="pendingApprovalLinkId" onclick="ns.home.openPendingApprovalPayees();" href="javascript:void(0)"><s:property value="%{daPendingCount}"/>&nbsp;<s:text name="jsp.home_151"/></a>
					
					<div class="approvalActionItemHolder">
						<span class="ffiUiIcoMedium ffiUiIco-icon-check" title="<s:text name='jsp.default_6' />" onclick="$('#pendingApprovalLinkId').trigger('click')"></span>
					</div>
				</div>
			</s:if>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.COMPANY_SUMMARY %>">
	 			<s:if test="%{daPendingCompanyCount != 0}">
					<div class="approvalDataHolder">
	   					<span>
							<s:url id="ajax" value="/pages/jsp/user/admin_summaryindex.jsp" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
							</s:url>

							<sj:a cssClass="approvalDetails" id="dualApprovalLink" href="%{ajax}" indicator="indicator" targets="notes" onClick="$('li[menuId=\"admin_summary\"] a').click();" onCompleteTopics="subMenuOnCompleteTopics">
								<s:property value="%{daPendingCompanyCount}"/>&nbsp;<s:text name="jsp.home_28"/>
							</sj:a>
						</span>
						
						<div class="approvalActionItemHolder">
							<span class="ffiUiIcoMedium ffiUiIco-icon-check" title="<s:text name='jsp.default_6' />" onclick="$('#dualApprovalLink').trigger('click')"></span>
						</div>
					</div>
				</s:if>
			</ffi:cinclude>  						
		</ffi:cinclude>
	</s:else>
</div>
