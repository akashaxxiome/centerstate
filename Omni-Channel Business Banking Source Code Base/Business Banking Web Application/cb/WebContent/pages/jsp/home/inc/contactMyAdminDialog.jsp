<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<s:url id="contactMyAdminUrl" action="contactMyAdminAction_init" namespace="/pages/jsp/message">
	</s:url>
	<sj:dialog 
		id="contactMyAdminDialog" 
		autoOpen="false" 
		modal="true" 
		title="%{getText('jsp.default_text_contact_my_admin')}"
		height="510"
		width="700"
		href="%{contactMyAdminUrl}"
		showEffect="fold" 
   		hideEffect="clip" 
		onCloseTopics=""
		closeOnEscape="true"
	>
	</sj:dialog>