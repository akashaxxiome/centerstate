<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%-- second level menu --%>
<%
	Map menuShortcuts = (Map)request.getAttribute("menuShortcuts");
%>
<div class="submenuHolderBgCls">
			<div class="formActionItemPlaceholder">
				<div class="cardHolder">
					<div class="subMenuCard">
						<span class="cardTitle">_______________</span>
						<div class="cardSummary">
							<span>__________________________<br>__________________________<br>________</span>
						</div>
						<div class="cardLinks">
							<span>___________</span>
							<span>___________</span>
						</div> 
					</div>
				</div>	
			</div> 
						
			<ul id="tab12" class="mainSubmenuItemsCls">
				<ffi:cinclude value1="${displayAccountHistory}" value2="true" operator="equals">
						<li id="menu_acctMgmt_history" menuId="acctMgmt_history">
						<ffi:help id="acctMgmt_history" className="moduleHelpClass"/>
						<ffi:setProperty name="accHistoryURL" value="/pages/jsp/account/InitAccountHistoryAction.action" URLEncrypt="true"/>
						<s:url id="accHistoryMenuURL" value="%{#session.accHistoryURL}" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="TransactionSearch" value="false"/>
						</s:url>
						<sj:a
								href="%{accHistoryMenuURL}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="jsp.home_20"/>
						</sj:a>
						<span onclick="ns.common.showMenuHelp('menu_acctMgmt_history')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{accHistoryMenuURL}"/>','acctMgmt_history','AccHistory');"  ><s:text name="jsp.default_6"/> <s:text name="jsp.home_20"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ViewAccountHistory"/> 
										</span>
									</div>
									<div class="cardLinks">
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{accHistoryMenuURL}"/>','acctMgmt_history','AccHistory','openCalendar');"><s:text name="jsp.home_259"/></span>
						</div> 
								</div>
							</div>	
							
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{accHistoryMenuURL}"/>','acctMgmt_history','AccHistory');"  ><s:text name="jsp.default_6"/> <s:text name="jsp.home_20"/></span><br/>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{accHistoryMenuURL}"/>','acctMgmt_history','AccHistory','openCalendar');"><s:text name="jsp.home_259"/></span> --%>
						</div> 
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-acnthistory"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_21')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_20')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "acctMgmt_history");
					%>
				</ffi:cinclude>
				<ffi:cinclude value1="${displayConsolidatedBalance}" value2="true" operator="equals">
					<li id="menu_acctMgmt_consobal" menuId="acctMgmt_consobal">
						<ffi:help id="acctMgmt_consobal" className="moduleHelpClass"/>
						<ffi:setProperty name="conBalanceURL" value="/pages/jsp/account/InitConsilidatedBalanceAction.action" URLEncrypt="true"/>
						<s:url id="conBalanceMenuURL" value="%{#session.conBalanceURL}" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="TransactionSearch" value="false"/>
						</s:url>
						<sj:a
								href="%{conBalanceMenuURL}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="jsp.home_60"/>
						</sj:a>
						<span onclick="ns.common.showMenuHelp('menu_acctMgmt_consobal')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{conBalanceMenuURL}"/>','acctMgmt_consobal','ConBalance');"><s:text name="jsp.default.ConsolidatedBalanceHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ConsolidatedBalance"/> 
										</span>
									</div>
									<div class="cardLinks">
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{conBalanceMenuURL}"/>','acctMgmt_consobal','ConBalance','viewAsBar');"  ><s:text name='jsp.home.barChart' /></span>
							<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{conBalanceMenuURL}"/>','acctMgmt_consobal','ConBalance','viewAsDonut');"><s:text name='jsp.home.donutChart' /></span>
									</div>
								</div>
							</div>
							
							<%-- <span  onclick="ns.common.loadSubmenuSummary('<s:property value="%{conBalanceMenuURL}"/>','acctMgmt_consobal','ConBalance');"><s:text name='jsp.home_261' /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{conBalanceMenuURL}"/>','acctMgmt_consobal','ConBalance','viewAsBar');"  ><s:text name='jsp.home.barChart' /></span>
							<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{conBalanceMenuURL}"/>','acctMgmt_consobal','ConBalance','viewAsDonut');"><s:text name='jsp.home.donutChart' /></span> --%>
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('acctMgmt_consobal,viewAsDataTable');">Data table view</span> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-consolidatedbal"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_21')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_60')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "acctMgmt_consobal");
					%>
				</ffi:cinclude>
				<ffi:cinclude value1="${displayAccountBalance}" value2="true" operator="equals">
					<li id="menu_acctMgmt_balance" menuId="acctMgmt_balance">
						<ffi:help id="acctMgmt_balance" className="moduleHelpClass"/>
						<s:url id="accountBalUrl" value="/pages/jsp/account/accountbalance_index.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a
								href="%{accountBalUrl}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_19"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_acctMgmt_balance')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{accountBalUrl}"/>','acctMgmt_balance','ActBalSummary','depositAccounts');"><s:text name="jsp.account_76"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.DepositAccounts"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{accountBalUrl}"/>','acctMgmt_balance','ActBalSummary','assetAccounts');"  ><s:text name="jsp.account_231"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.AssetsAccounts"/> 
										</span>
									</div>
									<div class="cardLinks borderNone">
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{accountBalUrl}"/>','acctMgmt_balance','ActBalSummary','loanAccounts');"  ><s:text name="jsp.account_116"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.LoanAccounts"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
									</div>
								</div>
							</div>
							
							<%-- <span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{accountBalUrl}"/>','acctMgmt_balance','ActBalSummary','depositAccounts');"><s:text name="jsp.account_76"/></span>
							<span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{accountBalUrl}"/>','acctMgmt_balance','ActBalSummary','assetAccounts');"  ><s:text name="jsp.account_231"/></span>
							<span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{accountBalUrl}"/>','acctMgmt_balance','ActBalSummary','creditCardsAccounts');"><s:text name="jsp.account_58"/></span>
							<span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{accountBalUrl}"/>','acctMgmt_balance','ActBalSummary','loanAccounts');"  ><s:text name="jsp.account_116"/></span> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-acntbal"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_21')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_19')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "acctMgmt_balance");
					%>
				</ffi:cinclude>
				<ffi:cinclude value1="${displayTransactionSearch}" value2="true" operator="equals" >
					<li id="menu_acctMgmt_search" menuId="acctMgmt_search">
						<ffi:help id="acctMgmt_search" className="moduleHelpClass"/>
						<ffi:setProperty name="transHistoryURL" value="/pages/jsp/account/InitTransactionHistoryAction.action" URLEncrypt="true"/>
						<s:url id="transHistoryMenuURL" value="%{#session.transHistoryURL}" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="TransactionSearch" value="true"/>
						</s:url>
						<sj:a
								href="%{transHistoryMenuURL}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="jsp.home_221"/>
						</sj:a>
						<span onclick="ns.common.showMenuHelp('menu_acctMgmt_search')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{transHistoryMenuURL}"/>','acctMgmt_search','TransHistory');" ><s:text name="jsp.default.TransactionSearchHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.TransactionSearch"/> 
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transHistoryMenuURL}"/>','acctMgmt_search','TransHistory','openCalendar');"><s:text name="jsp.home_259"/></span>
									</div> 
								</div>
							</div>	
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transHistoryMenuURL}"/>','acctMgmt_search','TransHistory');" ><s:text name="jsp.home_260"/></span><br/>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transHistoryMenuURL}"/>','acctMgmt_search','TransHistory','openCalendar');"><s:text name="jsp.home_259"/></span> --%>
						</div> 
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-transSearch"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_21')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_221')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "acctMgmt_search");
					%>
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD %>" >
					<li id="menu_acctMgmt_fileupload" menuId="acctMgmt_fileupload">
						<ffi:help id="acctMgmt_fileupload" className="moduleHelpClass"/>
						<s:url id="ajax" value="/pages/jsp/account/file_upload_index.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a
								href="%{ajax}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="jsp.home_92"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_acctMgmt_fileupload')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.shortcut.navigateToSubmenus('acctMgmt_fileupload');"><s:text name="jsp.home_92"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.FileUpload"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
									</div>
								</div>
							</div>	
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('acctMgmt_fileupload');"><s:text name="jsp.home_92"/></span> --%>
						</div> 
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-fileupload"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_21')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_92')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "acctMgmt_fileupload");
					%>
				</ffi:cinclude>
				<ffi:cinclude value1="true" value2="${isImageSearchEntitled}" operator="equals">
						<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CHECK_IMAGING %>" >
						<li id="menu_acctMgmt_imagesearch" menuId="acctMgmt_imagesearch">
							<ffi:help id="acctMgmt_imagesearch" className="moduleHelpClass"/>
							<s:url id="imageSearchMenuURL" value="/pages/jsp/imagesearch/index.jsp" escapeAmp="false"></s:url>
							<sj:a
									href="%{imageSearchMenuURL}"
									indicator="indicator"
									targets="notes"
									onCompleteTopics="subMenuOnCompleteTopics"
								><s:text name="jsp.home_254"/></sj:a>
							<span onclick="ns.common.showMenuHelp('menu_acctMgmt_imagesearch')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
							<div class="formActionItemHolder hidden">
								<div class="cardHolder">
									<div class="subMenuCard">
										<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{imageSearchMenuURL}"/>','acctMgmt_imagesearch','RequestedImageSearch');"><s:text name="jsp.imageSearch.button.reqImages"/></span>
										<div class="cardSummary">
											<span>This area provides brief information about this feature. Please click on help icon to know more. 
											</span>
										</div>
										<div class="cardLinks borderNone">
										</div>
									</div>
								</div>	
								<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{imageSearchMenuURL}"/>','acctMgmt_imagesearch','RequestedImageSearch');"><s:text name="jsp.imageSearch.button.reqImages"/></span> --%>
							</div>
							<span class="ffiUiIco-menuItem ffiUiIco-menuItem-imageSearch"></span>
						</li>
						<s:set var="tempMenuName" value="%{getText('jsp.home_21')}" scope="session" />
						<s:set var="tempSubMenuName" value="%{getText('jsp.home_254')}" scope="session" />
						<% 
							menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "acctMgmt_imagesearch");
						%>
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER %>" >
					<li id="menu_acctMgmt_register" menuId="acctMgmt_register">
						<ffi:help id="acctMgmt_register" className="moduleHelpClass"/>
						<s:url id="ajax" value="/pages/jsp/account/accountregister_index.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a
								href="%{ajax}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_22"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_acctMgmt_register')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder hidden">
							<span>Register link 1</span>
							<span>Register link 2</span>
							<span>Register link 3</span><br>
							<span class="removeComma">Register link 4</span>
							<span>Register link 5</span>
							<span>Register link 6</span>
						</div> 
						<img src="<s:url value="/web/grafx/icons/submenuBgIco.png"/>" class="submenuBgImgCls" />
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_21')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_22')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "acctMgmt_register");
					%>
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ONLINE_STATEMENT %>" >
						<li id="menu_acctMgmt_onlineStatements" menuId="acctMgmt_onlineStatements">
							<ffi:help id="acctMgmt_onlineStatements" className="moduleHelpClass"/>
							<ffi:setProperty name="onlineStatementURL" value="/pages/jsp/onlinestatement/initOnlineStatementAction.action" URLEncrypt="true"/>
							<s:url id="onlineStatementMenuURL" value="%{#session.onlineStatementURL}" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							</s:url>
							<sj:a
									href="%{onlineStatementMenuURL}"
									indicator="indicator"
									targets="notes"
									onCompleteTopics="subMenuOnCompleteTopics"
								><s:text name="jsp.home.onlineStatements.menu"/></sj:a>
							<span onclick="ns.common.showMenuHelp('menu_acctMgmt_onlineStatements')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
							<div class="formActionItemHolder hidden">
								<div class="cardHolder">
									<div class="subMenuCard">
										<span class="cardTitle"><s:text name="jsp.home.onlineStatements.menu"/></span>
										<div class="cardSummary">
											<span>This area provides brief information about this feature. Please click on help icon to know more. 
											</span>
										</div>
										<div class="cardLinks borderNone">
										</div>
									</div>
								</div>	
							</div>	
							<span class="ffiUiIco-menuItem ffiUiIco-menuItem-onlineStatement"></span>
						</li>
						<s:set var="tempMenuName" value="%{getText('jsp.home_21')}" scope="session" />
						<s:set var="tempSubMenuName" value="%{getText('jsp.home.onlineStatements.menu')}" scope="session" />
						<% 
							menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "acctMgmt_onlineStatements");
						%>
				</ffi:cinclude>
			</ul>
</div>

