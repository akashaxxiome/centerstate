<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%
	Map menuShortcuts = (Map)request.getAttribute("menuShortcuts");
%>
	<% String displayPPay = "false"; %>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_DECISION_ENTITLEMENT %>" >
		<ffi:setProperty name="displayPPay" value="true" />
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT_ISSUE_MAINTENANCE %>" >
		<ffi:setProperty name="displayPPay" value="true" />
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT_FILE_UPLOAD %>" >
		<ffi:setProperty name="displayPPay" value="true" />
	</ffi:cinclude>

<div class="submenuHolderBgCls">
			<div class="formActionItemPlaceholder">
				<div class="cardHolder">
					<div class="subMenuCard">
						<span class="cardTitle">_______________</span>
						<div class="cardSummary">
							<span>__________________________<br>__________________________<br>________</span>
						</div>
						<div class="cardLinks">
							<span>___________</span>
							<span>___________</span>
						</div> 
					</div>
				</div>	
			</div> 
			<ul id="tab13" class="mainSubmenuItemsCls">
				<ffi:cinclude value1="${displayCashFlowSummary}" value2="true" operator="equals">
					<li id="menu_cashMgmt_cashflow" menuId="cashMgmt_cashflow">
						<ffi:help id="cashMgmt_cashflow" className="moduleHelpClass"/>
						<ffi:setProperty name="cashFlowURL" value="/pages/jsp/cash/InitCashFlowAction.action" URLEncrypt="true"/>
						<s:url id="cashFlowMenuURL" value="%{#session.cashFlowURL}" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						</s:url>
						<sj:a
								href="%{cashFlowMenuURL}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="jsp.home_49"/>
						</sj:a> 
						<span onclick="ns.common.showMenuHelp('menu_cashMgmt_cashflow')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
						<div class="formActionItemHolder">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{cashFlowMenuURL}"/>','cashMgmt_cashflow');"  ><s:text name="jsp.home_49"/> <s:text name="SUMMARY"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.CashFlowSummary"/>
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</div>	
							
						<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{cashFlowMenuURL}"/>','cashMgmt_cashflow');"  ><s:text name="jsp.home_49"/> <s:text name="SUMMARY"/></span> --%>
						</div>
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-cashcon"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_50')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_49')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "cashMgmt_cashflow");
					%>
				</ffi:cinclude>			
				
				<ffi:cinclude value1="${displayPPay}" value2="true" operator="equals">
					<li id="menu_cashMgmt_ppay" menuId="cashMgmt_ppay">
						<ffi:help id="cashMgmt_ppay" className="moduleHelpClass"/>
						<ffi:setProperty name="ppayURL" value="/pages/jsp/positivepay/initPositivePayAction.action" URLEncrypt="true"/>
						<s:url id="ppayMenuURL" value="%{#session.ppayURL}" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="positivepayReload" value="true"/>
						</s:url>
						<sj:a
								href="%{ppayMenuURL}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="jsp.home_163"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_cashMgmt_ppay')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ppayMenuURL}"/>','cashMgmt_ppay','positivePay','ppaybuildLink');"  ><s:text name="jsp.cash_21" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.BuildManualFile"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ppayMenuURL}"/>','cashMgmt_ppay','positivePayUpload','showdbPPayFileUploadSummary');"  ><s:text name="jsp.cash_118"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.UploadCheckingRecord"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
									</div>
								</div>
							</div>	
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ppayMenuURL}"/>','cashMgmt_ppay','positivePay','ppaybuildLink');"  ><s:text name="jsp.cash_21" /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ppayMenuURL}"/>','cashMgmt_ppay','positivePayUpload','showdbPPayFileUploadSummary');"  ><s:text name="jsp.cash_118"/></span> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-pospay"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_50')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_163')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "cashMgmt_ppay");
					%> 
				</ffi:cinclude>
				
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
					<li id="menu_cashMgmt_rppay" menuId="cashMgmt_rppay">
						<ffi:help id="cashMgmt_rppay" className="moduleHelpClass"/>
						<ffi:setProperty name="rppayURL" value="/pages/jsp/reversepositivepay/initReversePositivePayAction.action" URLEncrypt="true"/>
						<s:url id="rppayMenuURL" value="%{#session.rppayURL}" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="reversepositivepayReload" value="true"/>
						</s:url>
						<sj:a
								href="%{rppayMenuURL}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="jsp.home_263"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_cashMgmt_rppay')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
						<div class="formActionItemHolder hidden"> 
					<div class="cardHolder"> 
								<div class="subMenuCard"> 
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{rppayMenuURL}"/>','cashMgmt_rppay');"  ><s:text name="jsp.home_263" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.CashflowReversePositivePay" />
										</span>
									</div> 
 									<div class="cardLinks borderNone"> 
								</div> 
 								</div> 
								
							</div>	
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{rppayMenuURL}"/>','cashMgmt_ppay','positivePay','ppaybuildLink');"  ><s:text name="jsp.cash_21" /></span>
<%-- 							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{rppayMenuURL}"/>','cashMgmt_ppay','positivePayUpload','showdbPPayFileUploadSummary');"  ><s:text name="jsp.cash_118"/></span> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-revpospay"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_50')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_263')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "cashMgmt_rppay");
					%> 
				</ffi:cinclude>
				<ffi:cinclude value1="${displayDisbursements}" value2="true">
					<li id="menu_cashMgmt_disburs" menuId="cashMgmt_disburs">
						<ffi:help id="cashMgmt_disburs" className="moduleHelpClass"/>
						<s:url id="ajax" value="/pages/jsp/cash/cashdisbursement.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="positivepayReload" value="true"/>
						</s:url>
						<sj:a
							href="%{ajax}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_63"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_cashMgmt_disburs')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','cashMgmt_disburs','disbursement');"><s:text name="jsp.default.DisbursementHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Disbursement"/>
										</span>
									</div>
									<div class="cardLinks">
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','cashMgmt_disburs','disbursement','disbursementPresentment');"  ><s:text name="jsp.cash.by.presentment"/></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','cashMgmt_disburs','disbursement','disbursementAccount');"  ><s:text name="jsp.cash.by.account"/></span>
						</div>  
								</div>
							</div>	
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','cashMgmt_disburs','disbursement','disbursementPresentment');"  ><s:text name="jsp.cash.by.presentment"/></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','cashMgmt_disburs','disbursement','disbursementAccount');"  ><s:text name="jsp.cash.by.account"/></span> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-controlleddisburse"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_50')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_63')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "cashMgmt_disburs");
					%>  
				</ffi:cinclude>
				<ffi:cinclude value1="${displayLockbox}" value2="true">
					<li id="menu_cashMgmt_lockbox" menuId="cashMgmt_lockbox">
						<ffi:help id="cashMgmt_lockbox" className="moduleHelpClass"/>
						<s:url id="ajax" value="/pages/jsp/cash/cashlockbox.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="positivepayReload" value="true"/>
						</s:url>
						<sj:a
							href="%{ajax}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_113"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_cashMgmt_lockbox')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','cashMgmt_lockbox');"  ><s:text name="jsp.cash.lockbox.availibilty.summary"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Lockbox"/>
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</div>	
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','cashMgmt_lockbox');"  ><s:text name="jsp.cash.lockbox.availibilty.summary"/></span> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-lockbox"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_50')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_113')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "cashMgmt_lockbox");
					%>   
				</ffi:cinclude>
			</ul>
</div>

