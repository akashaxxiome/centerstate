<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%-- second level menu --%>
<%
	Map menuShortcuts = (Map)request.getAttribute("menuShortcuts");
%>
<div class="submenuHolderBgCls">
			<ul id="tab19" class="mainSubmenuItemsCls">
				<li id="menu_preferences_userPrefs" menuId="userPreferences" class="preferencesMenuDummyCls">
					<ffi:help id="pref_userperf" className="moduleHelpClass"/>
					<sj:a
							href="#"
							indicator="indicator"
						><s:text name="jsp.home.user.preference.title"/></sj:a>
						
					
					<ffi:setProperty name="ShowAccountTab" value="False"/>
				   	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
						<ffi:setProperty name="ShowAccountTab" value="True"/>
					</ffi:cinclude>	
					<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>" operator="equals">
						<ffi:setProperty name="ShowAccountTab" value="True"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${ShowAccountTab}" value2="True" operator="equals">
						 <ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.ACCOUNT_SETUP%>" >
						 	<ffi:setProperty name="ShowAccountTab" value="False"/>
					    </ffi:cinclude>	
					</ffi:cinclude> 
					<span onclick="ns.common.showMenuHelp('menu_preferences_userPrefs')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>		
					<div class="formActionItemHolder">
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
							<span id="updateProfileID" onclick="ns.common.openDialog('updateProfile');"><s:text name="jsp.default_Update_Profile" /></span>
							<span id="siteNavigationID" onclick="ns.common.openDialog('siteNavigation');"  ><s:text name="jsp.default_Site_Navigation" /></span>
							<span id="otherSettingID" onclick="ns.common.openDialog('otherSetting');" ><s:text name="jsp.default_Other_Settings" /></span>
							<span id="sessionActivityReportSpanID" onclick="ns.common.openDialog('sessionActivityReport');" ><s:text name="jsp.default.session.activity" /></span>
							<%-- <span onclick="ns.common.openDialog('themePicker');" class="removeComma"><s:text name="jsp.home_244" /></span><br> --%>
						</ffi:cinclude>
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
							<ffi:cinclude value1="${ShowAccountTab}" value2="True" operator="equals">
								<span id="updateProfileID" onclick="ns.common.openDialog('updateProfile');"><s:text name="jsp.default_Update_Profile" /></span>
								<span id="accountPreferencesID" onclick="ns.common.openDialog('accountPrefs');"  ><s:text name="jsp.default_Account_Preferences" /></span>
								<span id="siteNavigationID" onclick="ns.common.openDialog('siteNavigation');" ><s:text name="jsp.default_Site_Navigation" /></span>
								<span id="otherSettingID" onclick="ns.common.openDialog('otherSetting');"  ><s:text name="jsp.default_Other_Settings" /></span>
								<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SESSION_RECEIPT %>" >
									<span id="sessionActivityReportSpanID" onclick="ns.common.openDialog('sessionActivityReport');"  ><s:text name="jsp.default.session.activity" /></span>
								</ffi:cinclude>
							</ffi:cinclude>
							<ffi:cinclude value1="${ShowAccountTab}" value2="True" operator="notEquals">
								<span id="updateProfileID" onclick="ns.common.openDialog('updateProfile');" ><s:text name="jsp.default_Update_Profile" /></span>
								<span id="siteNavigationID" onclick="ns.common.openDialog('siteNavigation');"  ><s:text name="jsp.default_Site_Navigation" /></span>
								<span id="otherSettingID" onclick="ns.common.openDialog('otherSetting');" ><s:text name="jsp.default_Other_Settings" /></span>
								<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SESSION_RECEIPT %>" >
									<span id="sessionActivityReportSpanID" onclick="ns.common.openDialog('sessionActivityReport');"  ><s:text name="jsp.default.session.activity" /></span>
								</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</div> 
					<span class="ffiUiIco-menuItem ffiUiIco-menuItem-userPref"></span>
				</li>
				<s:set var="tempMenuName" value="%{getText('jsp.home.user.preference.title')}" scope="session" />
				<s:set var="tempSubMenuName1" value="%{getText('jsp.default_Update_Profile')}" scope="session" />
				<s:set var="tempSubMenuName2" value="%{getText('jsp.default_Account_Preferences')}" scope="session" />
				<s:set var="tempSubMenuName3" value="%{getText('jsp.default.session.activity')}" scope="session" />
				<s:set var="tempSubMenuName4" value="%{getText('jsp.default_Site_Navigation')}" scope="session" />
				<s:set var="tempSubMenuName5" value="%{getText('jsp.default_Other_Settings')}" scope="session" />
				<% 
				menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName1"), "userPreferencesVirtualMenu,updateProfileID");
				menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName3"), "userPreferencesVirtualMenu,sessionActivityReportID");
				menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName4"), "userPreferencesVirtualMenu,siteNavigationID");
				menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName5"), "userPreferencesVirtualMenu,otherSettingID");
				%>   
				
				<ffi:cinclude value1="${ShowAccountTab}" value2="True" operator="equals">
				<%menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName2"), "userPreferencesVirtualMenu,accountPreferencesID"); %>
				</ffi:cinclude>
				
				<li id="menu_preferences_tools" menuId="systemToolPreferences" class="preferencesMenuDummyCls">
					<ffi:help id="pref_tools" className="moduleHelpClass"/>
					<sj:a
							href="#"
							indicator="indicator"
						><s:text name="jsp.home.tools.title"/></sj:a>
						
					<span onclick="ns.common.showMenuHelp('menu_preferences_tools')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
					<div class="formActionItemHolder">
						<span id="bankLookupDivID" onclick="ns.common.openDialog('bankLookup');"  ><s:text name="jsp.default_554" /></span><br/>
						<span id="fxConverterID" onclick="ns.common.openDialog('fx');"  ><s:text name="jsp.home_3" /></span>
					</div> 
					<span class="ffiUiIco-menuItem ffiUiIco-menuItem-tools"></span>
				</li>
				<s:set var="tempMenuName" value="%{getText('jsp.home.tools.title')}" scope="session" />
				<s:set var="tempSubMenuName1" value="%{getText('jsp.default_554')}" scope="session" />
				<s:set var="tempSubMenuName2" value="%{getText('jsp.home_3')}" scope="session" />
				<% 
				menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName1"), "userPreferencesVirtualMenu,bankLookupDivID");
				menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName2"), "userPreferencesVirtualMenu,fxConverterID");
				%>   
				
				<%-- 
				<li id="menu_preferences_layout" menuId="layoutPreferences">
					<sj:a
							href="#"
							indicator="indicator"
						><s:text name="jsp.home.portlets.settings.title"/></sj:a>
						
					<div class="formActionItemHolder">
						<span id="addPortlet" onclick="ns.home.onOpenLayoutPortletsDialog('<s:url  value="/pages/jsp/home/inc/portal-portlets-dialog.jsp"/>', '<ffi:getProperty name="CSRF_TOKEN"/>')" class="removeComma"><s:text name="jsp.home_248" /><br></span>
						<span onclick="ns.common.openDialog('portalLayoutDialog');" class="removeComma"><s:text name="jsp.home.label.choose.layout" /></span><br>
						<span onclick="ns.home.openSaveLayoutPage()"><s:text name="jsp.home.label.save.layout" /></span>
					</div>
					<img src="<s:url value="/web/grafx/icons/submenuBgIco.png"/>" class="submenuBgImgCls" /> 
				</li>
				 --%>
			</ul>
</div>

