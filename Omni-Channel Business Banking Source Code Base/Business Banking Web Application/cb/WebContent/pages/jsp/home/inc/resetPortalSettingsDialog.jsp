<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<sj:dialog id="resetPreviousConfirm" title="%{getText('jsp.home_170')}" position="['center','middle']"
		hideEffect="clip" cssClass="staticContentDialog homeDialog"
		showEffect="fold" width="400" modal="true" resizable="false" overlayColor="#000" overlayOpacity="0.7"
		buttons="{'%{getText(\"jsp.default_82\")}': function() { $(this).dialog('close');}, '%{getText(\"jsp.default_111\")}':function(){ resetPortalPage(); $(this).dialog('close');}}">

	<div id="resetPortalNoticeDiv" style="height: 45px; padding: 0pt 0.7em;text-align:left" class="ui-state-highlight ui-corner-all ui-widget ui-widget-content">
			<table width="100%" align="left">
				<tr valign="top">
					<td><span class="ui-icon ui-icon-info" ></span></td>
					<td><span id="resetPortalNoticeSpan"> <s:text name="jsp.home_31"/></span></td>
				</tr>
			</table>
	</div>

	<div id="resetPortalOptionDiv">
		<label> <s:text name="jsp.home_169"/> </label>
		<br>
		<input type="radio" name="portalPageResetMode" value="1" onclick="resetPortalPageNotice();" style="margin-left: 80px; margin-left: 75px \9;" checked/><s:text name="jsp.home_172"/>
		<br>
		<input type="radio" name="portalPageResetMode" value="2" onclick="resetPortalPageNotice();" style="margin-left: 80px; margin-left: 75px \9;"/><s:text name="jsp.home_171"/>
		<p>
			<s:text name="jsp.home_37"/>
		</p>
	</div>
</sj:dialog>