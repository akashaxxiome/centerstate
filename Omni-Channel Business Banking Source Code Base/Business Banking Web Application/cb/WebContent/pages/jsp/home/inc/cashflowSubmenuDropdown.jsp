<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%-- selected module drop down items holder --%>

	<ul class="moduleSubmenuDropdownHolder">
		<ffi:cinclude value1="${displayCashFlowSummary}" value2="true" operator="equals">
			<li onclick="ns.shortcut.goToMenu('cashMgmt_cashflow');">
				<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-cashFlow"></span>
	    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_49"/></span>
			</li>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>" >
			<li onclick="ns.shortcut.goToMenu('cashMgmt_ppay');">
				<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-positivePay"></span>
	    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_163"/></span>
			</li>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
			<li onclick="ns.shortcut.goToMenu('cashMgmt_rppay');">
				<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-reversePositivePay"></span>
	    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_263"/></span>
			</li>
		</ffi:cinclude>
		<ffi:cinclude value1="${displayDisbursements}" value2="true">
			<li onclick="ns.shortcut.goToMenu('cashMgmt_disburs');">
				<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-controlDisbursement"></span>
	    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_63"/></span>
			</li>
		</ffi:cinclude>
		<ffi:cinclude value1="${displayLockbox}" value2="true">
			<li onclick="ns.shortcut.goToMenu('cashMgmt_lockbox');">
				<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-lockBox"></span>
	    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_113"/></span>
			</li>
		</ffi:cinclude>
		
	</ul>