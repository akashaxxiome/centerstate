<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<sj:dialog id="deleteHomeTransferDialogID" title="%{getText('jsp.transfers_34')}"
modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="750"
	onCloseTopics="clearDialogContent">
</sj:dialog>