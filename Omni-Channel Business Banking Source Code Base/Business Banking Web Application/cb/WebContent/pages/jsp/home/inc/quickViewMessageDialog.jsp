<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<sj:dialog id="quickViewMsgDialogID" title="%{getText('jsp.message_12')}" resizable="false" modal="true" autoOpen="false" 
   	closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" cssClass="staticContentDialog">
	<div id="quickViewMsgDlgBand1"></div>
	<div id="quickViewMsgDlgBand2"></div>
</sj:dialog>
