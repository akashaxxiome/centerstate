<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- selected module drop down items holder --%>
<ul class="moduleSubmenuDropdownHolder">
	<li onclick="ns.shortcut.goToMenu('home');">
		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-home"></span>
   		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_105" /></span>
	</li>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MESSAGES %>" >
		<li onclick="ns.shortcut.goToMenu('home_message');">
			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-message"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_121" /></span>
		</li>
	</ffi:cinclude>
	<!-- alerts menu items -->
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ALERTS%>" >
		<li onclick="ns.shortcut.goToMenu('home_alert');">
			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-alert"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_29" /></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<li onclick="ns.shortcut.goToMenu('home_manageUsers');">
			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-manageUsers"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.user.preferences.manageUsersTabName" /></span>
		</li>
	</ffi:cinclude>
</ul>	
