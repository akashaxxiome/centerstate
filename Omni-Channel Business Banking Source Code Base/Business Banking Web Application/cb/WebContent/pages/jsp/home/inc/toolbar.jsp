<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<div class="toolbarCls">
	<!-- left side elements -->
	<div class="toolbarContainerLeft">
		<ul class="mainMenuItem">
			<li class="resetBorder" id="homeQuickAccessButton">
				<a href='javascript:void(0)' title="<s:text name="jsp.home_143"/>"  style="text-decoration:none; float:left;" >
					<span class="sapUiIconCls icon-home ui-state-active"></span>
				</a>
				<div id="notificationCountIndicator" class="notificationIndicatorCls" ></div>
			</li>
			
		</ul>
		
	</div>

	<!-- right side elements -->
	<div class="toolbarContainerRight">
		<div id='rightSideMenuBar' class="rightSideMenuBarCls">
			<ul class="mainMenuItem">
				<li><a href='#' title="<s:text name="jsp.home.user.preference.title"/>"><span class="sapUiIconCls icon-person-placeholder ui-state-active"></span></a>
					<ul>
						<li><a href='#' onClick="ns.common.openDialog('updateProfile');"><span class="sapUiIconCls icon-employee-pane" style="float:left;"></span><s:text name="jsp.default_Update_Profile" /></a></li>
						<li><a href='#' onClick="ns.common.openDialog('themePicker');"><span class="sapUiIconCls icon-palette" style="float:left;"></span><s:text name="jsp.home_244" /></a></li>
						<li><a href='#' onClick="ns.common.openDialog('sessionActivityReport');"><span class="sapUiIconCls icon-activity-individual" style="float:left;"></span><s:text name="jsp.default.session.activity" /></a></li>
						<li><a href='#' onClick="ns.common.openDialog('siteNavigation');"><span class="sapUiIconCls icon-org-chart" style="float:left;"></span><s:text name="jsp.default_Site_Navigation" /></a></li>
						<li><a href='#' onClick="ns.common.openDialog('otherSetting');"><span class="sapUiIconCls icon-settings" style="float:left;"></span><s:text name="jsp.default_Other_Settings" /></a></li>
					</ul>
				</li>
				<li><a href='#' title="<s:text name="jsp.home.tools.title"/>"><span class="sapUiIconCls icon-wrench ui-state-active"></span></a>
					<ul>
						<li><a href='#' onClick="ns.common.openDialog('bankLookup');"><span class="sapUiIconCls icon-search" style="float:left;"></span><s:text name="jsp.default_554" /></a></li>
						<li><a href='#' onclick="ns.common.openDialog('fx');"><span class="sapUiIconCls icon-simulate" style="float:left;"></span><s:text name="jsp.home_3" /></a></li>
					</ul>
				</li>
				<li id="portletSettings"><a href='#' title="<s:text name="jsp.home.portlets.settings.title"/>"><span class="sapUiIconCls icon-action-settings ui-state-active"></span></a>
					<ul>
						<li><a href='#' data-menu-type="add_portlet" onclick="ns.home.onOpenLayoutPortletsDialog('<s:url  value="/pages/jsp/home/inc/portal-portlets-dialog.jsp"/>', '<ffi:getProperty name="CSRF_TOKEN"/>')"><span class="sapUiIconCls icon-newspaper" style="float:left;"></span><s:text name="jsp.home_248" /></a></li>						
						<li><a href='#' data-menu-type="open_layout" onclick="ns.common.openDialog('portalLayoutDialog');"><span class="sapUiIconCls icon-ppt-attachment" style="float:left;"></span><%--<s:text name="jsp.home_148" />--%>Choose Layout</a></li>
						<li><a href='#' data-menu-type="save_layout" onclick="ns.home.openSaveLayoutPage()"><span class="sapUiIconCls icon-save" style="float:left;"></span><%--<s:text name="jsp.home_247" />--%>Save Layout</a></li>						
					</ul>
				</li>
				<li>
					<a href='javascript:void(0)' title="<s:text name="jsp.common.dashboard.printBtn.title"/>" onclick="javascript:ns.common.printNotesSection();"><span class="sapUiIconCls icon-print ui-state-active"></span></a>
				</li>
				<li>
					<a href='javascript:void(0)' title="<s:text name="jsp.home_103"/>" onclick="javascript:ns.common.callHelpFunction('<ffi:getProperty name="CSRF_TOKEN"/>');">
						<span class="sapUiIconCls icon-sys-help-2 ui-state-active"></span>
					</a>
				</li>
					<li>
                     	  <input name="shortcutSearchInput" onclick="ns.shortcut.showAllShortcuts()" id="shortcutSearchInput"  placeholder="Select shortcut..." class="ui-widget-content ui-corner-all txtbox" style="width: 250px!important;height:11px!important;"  />
                     	  <div id="shortcutOptHolderDiv"></div>
				</li>
			</ul>
		</div>
	</div>
</div>
<script>
       $(document).ready(function() {
       		if(toolBarController){
       			toolBarController.init();
       		}             
       });
</script>
	

