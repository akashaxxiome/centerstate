<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
		<ffi:help id="admin_company" className="moduleHelpClass"/>
						<s:url id="adminCompanyMenuURL" value="/pages/jsp/user/admin_companyindex.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
						</s:url>
						<sj:a
							href="%{adminCompanyMenuURL}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_58"/></sj:a>
						<span id="spanMenuAdmin" onclick="ns.common.showMenuHelp('menu_admin_company')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						
						<ffi:setProperty name="editCompanyCssClass" value="visible"/>
						<ffi:setProperty name="viewCompanyCssClass" value="hidden"/>
						<!--  <ffi:setProperty name="Business" property="dualApprovalMode" value="1"/>-->
						<!-- Dual Approval -->
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
							<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
							<ffi:setProperty name="GetDAItem" property="itemId" value="${SecureUser.BusinessID}"/>
							<ffi:setProperty name="GetDAItem" property="itemType" value="Business"/>
							<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.BusinessID}"/>
							<ffi:process name="GetDAItem" />
							
							<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
								<ffi:cinclude value1="${DAItem.status}" value2="Pending Approval" operator="equals">
									<!-- <ffi:setProperty name="editCompanyCssClass" value="hidden"/>-->
									<!-- <ffi:setProperty name="viewCompanyCssClass" value="visible"/>-->
									<div class="formActionItemHolder" id="viewAdminCompanyLinks">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyProfileSummaryMenuLinkView" class="hidden" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile');"><s:text name="jsp.user_404"/></span> <%-- This hidden link is required since it's click event is triggered when user do any operation on pending items grid and we load menu again to retain the exisiting summary --%>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Profile"/>
										</span>
									</div>
									<div class="cardLinks">
										
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile','viewCompany');"><s:text name="jsp.default_459"/>&nbsp;<s:text name="jsp.user_404"/></span>
										
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyBAISummaryMenuLinkView" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI');"><s:text name="jsp.user_405"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.BAISetting"/>
										</span>
									</div>
									<div class="cardLinks">
									
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI','editCompanyBAI');"  ><s:text name="jsp.default_459"/>&nbsp;<s:text name="jsp.user_405"/></span>
										
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyAdministratorSummaryMenuLinkView" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators');"><s:text name="jsp.user_36"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Administrator"/>
										</span>
									</div>
									<div class="cardLinks">
										
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators','AdminitratorsViewLinkView');"  ><s:text name="jsp.default_459"/>&nbsp;<s:text name="jsp.user_36"/></span>
										
									</div>
								</div>
								<br/>
								<div class="subMenuCard">
								
								<span class="cardTitle" id="dbAdminCompanyAccConfSummaryMenuLinkView" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountConfiguration');"  ><s:text name="jsp.default.AccountConfigurationHeader"/></span>
								
									<div class="cardSummary">
											<span><s:text name="jsp.default.AccountConfiguration"/></span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountGroups');"  ><s:text name="jsp.default_17"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.AccountGroup"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups');"><s:text name="jsp.user_407"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.TransactionGroup"/> 
										</span>
									</div>
									<div class="cardLinks borderNone">
									
										<span></span>
										
									</div>
								</div>
								<br/>
								<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<div class="subMenuCard">
									<span class="cardTitle"  id="dbAdminCompanyPermissionsSummaryMenuLinkView" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','CompanyPermissions');"  ><s:text name="jsp.common_119"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Permissions"/> 
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								</ffi:cinclude>
							</div>	
						</div> 
								</ffi:cinclude>
							</ffi:cinclude>
							
							
									<ffi:cinclude value1="${DAItem.status}" value2="Pending Approval" operator="notEquals">
										
						<div class="formActionItemHolder" id="editAdminCompanyLinks">
							<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
							<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
							<ffi:process name="CanAdminister"/>
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyProfileSummaryMenuLink" class="hidden" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile');"><s:text name="jsp.user_404"/></span> <%-- This hidden link is required since it's click event is triggered when user do any operation on pending items grid and we load menu again to retain the exisiting summary --%>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Profile"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile','editCompany');"><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_404"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyBAISummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI');"><s:text name="jsp.user_405"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.BAISetting"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI','BAIEditLink');"  ><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_405"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle"id="dbAdminCompanyAdministratorSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators');"><s:text name="jsp.user_36"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Administrator"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators','adminitratorsEditLink');"  ><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_36"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<br/>
								<div class="subMenuCard">
										<span class="cardTitle" id="dbAdminCompanyAccConfSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountConfiguration');"  ><s:text name="jsp.default.AccountConfigurationHeader"/></span>
										
									<div class="cardSummary">
										
											<span><s:text name="jsp.default.AccountConfiguration"/></span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountGroups');"  ><s:text name="jsp.default_17"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.AccountGroup"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups');"><s:text name="jsp.user_407"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.TransactionGroup"/> 
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups','addTransactionGroupBtn');"><s:text name="jsp.user_18"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<br/>
								<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<div class="subMenuCard">
									<span class="cardTitle"  id="dbAdminCompanyPermissionsSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','CompanyPermissions');"  ><s:text name="jsp.common_119"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Permissions"/> 
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								</ffi:cinclude>
							</div>	
							<%-- <ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
							<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
							<ffi:process name="CanAdminister"/>
						
							<span id="dbAdminCompanyProfileSummaryMenuLink" class="hidden" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile');"><s:text name="jsp.user_404"/></span> This hidden link is required since it's click event is triggered when user do any operation on pending items grid and we load menu again to retain the exisiting summary
							
							<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile','editCompany');"><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_404"/></span>
							</ffi:cinclude>
							
							<span id="dbAdminCompanyBAISummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI');"><s:text name="jsp.user_405"/></span>
							
							<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI','BAIEditLink');"  ><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_405"/></span>
							</ffi:cinclude>
							
							<span id="dbAdminCompanyAdministratorSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators');"><s:text name="jsp.user_36"/></span>
							
							<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators','adminitratorsEditLink');"  ><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_36"/></span>
							</ffi:cinclude>
							
							<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<span id="dbAdminCompanyAccConfSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountConfiguration');"><s:text name="jsp.user_406"/></span>
							</ffi:cinclude>
							
							<span id="dbAdminCompanyAccGrpSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountGroups');"  ><s:text name="jsp.default_17"/></span>
							<span id="dbAdminCompanyTransactionGrpSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups');"><s:text name="jsp.user_407"/></span>
							<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups','addTransactionGroupBtn');"><s:text name="jsp.user_18"/></span>
								<span id="dbAdminCompanyPermissionsSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','CompanyPermissions');"  ><s:text name="jsp.common_119"/></span>
							</ffi:cinclude> --%>
						</div>
									
									
										</ffi:cinclude>
							
							<ffi:removeProperty name="GetDAItem"/>
						</ffi:cinclude>
					  
						
						<!-- temporary commenting the view submenu for Company-->
						  
						
										
						<!--<ffi:removeProperty name="editCompanyCssClass"/>-->
						<!--<ffi:removeProperty name="viewCompanyCssClass"/>-->
						
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-admincompany"></span>