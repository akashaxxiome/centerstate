<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<sj:dialog id="editHomeBillPaymentDialogID" title="%{getText('jsp.billpay_48')}"
	modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="900"
	minHeight="400" maxHeight="500" onCloseTopics="closeEditPendingBillPaymentDialog,clearDialogContent">
</sj:dialog>


<script type="text/javascript">
$(document).ready(function(){

	$("#editHomeBillPaymentDialogID").dialog({
		   beforeClose: function(event, ui) { 
			   	return ns.pendingTransactionsPortal.checkExceptionBeforeClose(event, ui);
		   	}
	});
	
});
</script>