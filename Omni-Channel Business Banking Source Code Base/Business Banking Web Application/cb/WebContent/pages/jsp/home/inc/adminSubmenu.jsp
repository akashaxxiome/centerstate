<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%
	Map menuShortcuts = (Map)request.getAttribute("menuShortcuts");
%>
<div class="submenuHolderBgCls">
			<div class="formActionItemPlaceholder">
				<div class="cardHolder">
					<div class="subMenuCard">
						<span class="cardTitle">_______________</span>
						<div class="cardSummary">
							<span>__________________________<br>__________________________<br>________</span>
						</div>
						<div class="cardLinks">
							<span>___________</span>
							<span>___________</span>
						</div> 
					</div>
				</div>	
			</div> 
			<ul id="tab15" class="mainSubmenuItemsCls">
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.COMPANY_SUMMARY %>" >
					<li id="menu_admin_summary" menuId="admin_summary">
						<ffi:help id="admin_summary" className="moduleHelpClass"/>
						<s:url id="adminSummaryUrl" value="/pages/jsp/user/admin_summaryindex.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
						</s:url>
						<sj:a
							href="%{adminSummaryUrl}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_197"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_admin_summary')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminSummaryUrl}"/>','admin_summary', 'AdminSummary');" class="removeComma"><s:text name="jsp.default.AdministrableAreasHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.AdministrableAreas"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
									</div>
								</div>
							</div>
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminSummaryUrl}"/>','admin_summary', 'AdminSummary');" class="removeComma">Administrable areas</span> --%>
						</div>
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-adminsummary"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_27')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_197')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "admin_summary");
					%> 
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN %>" >
					<li id="menu_admin_company" menuId="admin_company">

						<ffi:help id="admin_company" className="moduleHelpClass"/>
						<s:url id="adminCompanyMenuURL" value="/pages/jsp/user/admin_companyindex.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
						</s:url>
						<sj:a
							href="%{adminCompanyMenuURL}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_58"/></sj:a>
						<span id="spanMenuAdmin" onclick="ns.common.showMenuHelp('menu_admin_company')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						
						<!--<ffi:setProperty name="editCompanyCssClass" value="visible"/>-->
						<!--<ffi:setProperty name="viewCompanyCssClass" value="hidden"/>-->
						<!-- Dual Approval -->
					
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					
							<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
							<ffi:setProperty name="GetDAItem" property="itemId" value="${SecureUser.BusinessID}"/>
							<ffi:setProperty name="GetDAItem" property="itemType" value="Business"/>
							<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.BusinessID}"/>
							<ffi:process name="GetDAItem" />
							
							<ffi:cinclude value1="${DAItem}" value2="" operator="notEquals">
								<ffi:cinclude value1="${DAItem.status}" value2="Pending Approval" operator="equals">
									
									<!-- <ffi:setProperty name="editCompanyCssClass" value="hidden"/>-->
									<!-- <ffi:setProperty name="viewCompanyCssClass" value="visible"/>-->
									<div class="formActionItemHolder" id="viewAdminCompanyLinks">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyProfileSummaryMenuLinkView" class="hidden" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile');"><s:text name="jsp.user_404"/></span> <%-- This hidden link is required since it's click event is triggered when user do any operation on pending items grid and we load menu again to retain the exisiting summary --%>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Profile"/>
										</span>
									</div>
									<div class="cardLinks">
										
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile','viewCompany');"><s:text name="jsp.default_459"/>&nbsp;<s:text name="jsp.user_404"/></span>
										
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyBAISummaryMenuLinkView" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI');"><s:text name="jsp.user_405"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.BAISetting"/>
										</span>
									</div>
									<div class="cardLinks">
									
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI','editCompanyBAI');"  ><s:text name="jsp.default_459"/>&nbsp;<s:text name="jsp.user_405"/></span>
										
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyAdministratorSummaryMenuLinkView" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators');"><s:text name="jsp.user_36"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Administrator"/>
										</span>
									</div>
									<div class="cardLinks">
										
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators','AdminitratorsViewLinkView');"  ><s:text name="jsp.default_459"/>&nbsp;<s:text name="jsp.user_36"/></span>
										
									</div>
								</div>
								<br/>
								<div class="subMenuCard">
								
								<span class="cardTitle" id="dbAdminCompanyAccConfSummaryMenuLinkView" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountConfiguration');"  ><s:text name="jsp.default.AccountConfigurationHeader"/></span>
								
									<div class="cardSummary">
											<span><s:text name="jsp.default.AccountConfiguration"/></span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountGroups');"  ><s:text name="jsp.default_17"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.AccountGroup"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups');"><s:text name="jsp.user_407"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.TransactionGroup"/> 
										</span>
									</div>
									<div class="cardLinks borderNone">
									<span></span>
									</div>
								</div>
								<br/>
								<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<div class="subMenuCard">
									<span class="cardTitle"  id="dbAdminCompanyPermissionsSummaryMenuLinkView" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','CompanyPermissions');"  ><s:text name="jsp.common_119"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Permissions"/> 
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								</ffi:cinclude>
							</div>	
						</div> 
								</ffi:cinclude>
							</ffi:cinclude>
							
							
									<ffi:cinclude value1="${DAItem.status}" value2="Pending Approval" operator="notEquals">
										
										
						<div class="formActionItemHolder" id="editAdminCompanyLinks">
							<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
							<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
							<ffi:process name="CanAdminister"/>
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyProfileSummaryMenuLink" class="hidden" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile');"><s:text name="jsp.user_404"/></span> <%-- This hidden link is required since it's click event is triggered when user do any operation on pending items grid and we load menu again to retain the exisiting summary --%>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Profile"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile','editCompany');"><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_404"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyBAISummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI');"><s:text name="jsp.user_405"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.BAISetting"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI','BAIEditLink');"  ><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_405"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle"id="dbAdminCompanyAdministratorSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators');"><s:text name="jsp.user_36"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Administrator"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators','adminitratorsEditLink');"  ><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_36"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<br/>
								<div class="subMenuCard">
										<span class="cardTitle" id="dbAdminCompanyAccConfSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountConfiguration');"  ><s:text name="jsp.default.AccountConfigurationHeader"/></span>
										
									<div class="cardSummary">
										
											<span><s:text name="jsp.default.AccountConfiguration"/></span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountGroups');"  ><s:text name="jsp.default_17"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.AccountGroup"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups');"><s:text name="jsp.user_407"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.TransactionGroup"/> 
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups','addTransactionGroupBtn');"><s:text name="jsp.user_18"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<br/>
								<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<div class="subMenuCard">
									<span class="cardTitle"  id="dbAdminCompanyPermissionsSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','CompanyPermissions');"  ><s:text name="jsp.common_119"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Permissions"/> 
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								</ffi:cinclude>
							</div>	
						
						</div>
									
									
										</ffi:cinclude>
							
							<ffi:removeProperty name="GetDAItem"/>
						</ffi:cinclude>
						
						<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
									<div class="formActionItemHolder" id="editAdminCompanyLinks">
							<ffi:object id="CanAdminister" name="com.ffusion.efs.tasks.entitlements.CanAdminister" scope="session" />
							<ffi:setProperty name="CanAdminister" property="CanAdminGroupId" value="${Business.EntitlementGroupId}"/>
							<ffi:process name="CanAdminister"/>
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyProfileSummaryMenuLink" class="hidden" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile');"><s:text name="jsp.user_404"/></span> <%-- This hidden link is required since it's click event is triggered when user do any operation on pending items grid and we load menu again to retain the exisiting summary --%>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Profile"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Profile','editCompany');"><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_404"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" id="dbAdminCompanyBAISummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI');"><s:text name="jsp.user_405"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.BAISetting"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','BAI','BAIEditLink');"  ><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_405"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle"id="dbAdminCompanyAdministratorSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators');"><s:text name="jsp.user_36"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Administrator"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','Administrators','adminitratorsEditLink');"  ><s:text name="jsp.default_178"/>&nbsp;<s:text name="jsp.user_36"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<br/>
								<div class="subMenuCard">
										<span class="cardTitle" id="dbAdminCompanyAccConfSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountConfiguration');"  ><s:text name="jsp.default.AccountConfigurationHeader"/></span>
										
									<div class="cardSummary">
										
											<span><s:text name="jsp.default.AccountConfiguration"/></span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','AccountGroups');"  ><s:text name="jsp.default_17"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.AccountGroup"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups');"><s:text name="jsp.user_407"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.TransactionGroup"/> 
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','TransactionGroups','addTransactionGroupBtn');"><s:text name="jsp.user_18"/></span>
										</ffi:cinclude>
									</div>
								</div>
								<br/>
								<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<div class="subMenuCard">
									<span class="cardTitle"  id="dbAdminCompanyPermissionsSummaryMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminCompanyMenuURL}"/>','admin_company','CompanyPermissions');"  ><s:text name="jsp.common_119"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Permissions"/> 
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								</ffi:cinclude>
							</div>	
		
						</div>
						</ffi:cinclude>
						
					  
						
						<!-- temporary commenting the view submenu for Company-->
						  
						
										
						<!-- <ffi:removeProperty name="editCompanyCssClass"/>-->
						<!-- <ffi:removeProperty name="viewCompanyCssClass"/>-->
						
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-admincompany"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_27')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_58')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "admin_company");
					%> 
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
					<ffi:object id="GetDescendantsByType" name="com.ffusion.efs.tasks.entitlements.GetDescendantsByType" scope="session"/>
					<ffi:setProperty name="GetDescendantsByType" property="GroupID" value="${Business.EntitlementGroupId}"/>
					<ffi:setProperty name="GetDescendantsByType" property="GroupType" value="Division"/>
					<ffi:process name="GetDescendantsByType"/>
					<ffi:removeProperty name="GetDescendantsByType" />
					<li id="menu_admin_division" menuId="admin_division">
						<ffi:help id="admin_division" className="moduleHelpClass"/>
						<s:url id="ajax" value="/pages/jsp/user/admin_divisionsindex.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
						</s:url>
						<sj:a
							href="%{ajax}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_83"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_admin_division')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','admin_division','division');"><s:text name="jsp.default.DivisionsHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Divisions"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
											<span id="addDivisionMenuLink" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','admin_division','division','addDivisionLink');"  ><s:text name="jsp.user_15"/></span>
										</ffi:cinclude>
									</div>
								</div>
								
								<ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','admin_division','division','showdbLocationSummary');"  ><s:text name="jsp.default.LocationHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Location"/>
										</span>
									</div>
									<div class="cardLinks borderNone">
										<span></span>
									</div>
								</div>
								</ffi:cinclude>
								</ffi:cinclude>	
							</div>	
							<%-- <div id="addDivisionMenuLink"><span onclick="ns.shortcut.navigateToSubmenus('admin_division,addDivisionLink');"  ><s:text name="jsp.user_15"/></span></div> --%>
							<%-- <ffi:cinclude value1="${Entitlement_CanAdminister}" value2="TRUE" operator="equals">
								<div id="addDivisionMenuLink"><span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','admin_division','division','addDivisionLink');"  ><s:text name="jsp.user_15"/></span></div>
							
								<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCATION_MANAGEMENT %>">
									<div id="locationSummaryMenuLink"><br/><span onclick="ns.shortcut.navigateToSubmenus('admin_division,showdbLocationSummary');"  ><s:text name="user.locations"/></span></div>
									<div id="locationSummaryMenuLink"><br/><span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','admin_division','division','showdbLocationSummary');"  ><s:text name="user.locations"/></span></div>
								</ffi:cinclude>
							</ffi:cinclude> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-division"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_27')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_83')}" scope="session" />
					<ffi:object id="GetGroupsAdministeredBy" name="com.ffusion.efs.tasks.entitlements.GetGroupsAdministeredBy" scope="session"/>
					<ffi:setProperty name="GetGroupsAdministeredBy" property="UserSessionName" value="BusinessEmployee"/>
					<ffi:process name="GetGroupsAdministeredBy"/>
					<ffi:removeProperty name="GetGroupsAdministeredBy" />
					<ffi:object id="GetDescendantsByTypeDA" name="com.ffusion.tasks.dualapproval.GetDescendantsByTypeDA" scope="session" />
					<ffi:setProperty name="GetDescendantsByTypeDA" property="UserSessionName" value="BusinessEmployees" />
					<ffi:setProperty name="GetDescendantsByTypeDA" property="DecendantsSessionName" value="Descendants" />
					<ffi:setProperty name="GetDescendantsByTypeDA" property="ItemType" value="<%= IDualApprovalConstants.ITEM_TYPE_DIVISION %>" />
					<ffi:setProperty name="GetDescendantsByTypeDA" property="MaxAdminStringLength" value="60" />
					<ffi:process name="GetDescendantsByTypeDA" />
					<ffi:removeProperty name="GetDescendantsByTypeDA" />
					<% 
						
						com.ffusion.csil.beans.entitlements.EntitlementGroups descendants = (com.ffusion.csil.beans.entitlements.EntitlementGroups)session.getAttribute("Descendants");
						
						com.ffusion.csil.beans.entitlements.EntitlementGroups entitlementGroups = (com.ffusion.csil.beans.entitlements.EntitlementGroups)session.getAttribute("Entitlement_EntitlementGroups");
						
						int divisionCount = 0;
						int defaultDivisionId = 0;
						String defaultDivisionName = "";
						
						if(descendants!=null){
							for(int i=0;i<descendants.size();i++){
								if(divisionCount > 0){
									break;
								}else{
									com.ffusion.csil.beans.entitlements.EntitlementGroup descendant = (com.ffusion.csil.beans.entitlements.EntitlementGroup)descendants.get(i);
									int descendantId = descendant.getGroupId();
									
									if(entitlementGroups !=null){
										for(int j=0;j<entitlementGroups.size();j++){
											com.ffusion.csil.beans.entitlements.EntitlementGroup entitlementGroup = (com.ffusion.csil.beans.entitlements.EntitlementGroup)entitlementGroups.get(j);
											int entitlementGroupId = entitlementGroup.getGroupId();
											
											if(descendantId == entitlementGroupId){
												defaultDivisionId = descendantId;
												defaultDivisionName = entitlementGroup.getGroupName();
												divisionCount+=1;
												break;
											}
										}	
									}
								}
							}
						}
																							
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "admin_division");
					%>
					<input type="hidden" id="defaultDivisionId"/>
					<input type="hidden" id="defaultDivisionName"/>
					<script type="text/javascript">	
	
						$(function(){						
							var divisionCountValue = '<%=divisionCount%>';
							var defaultDivisionId = '<%=defaultDivisionId%>';
							var defaultDivisionName = '<%=defaultDivisionName%>';
							if(divisionCountValue == 'undefined' || divisionCountValue == '' || divisionCountValue == 0){
								$('#locationSummaryMenuLink').hide();
								$('#addLocationMenuLink').hide();
							}else {
								$('#defaultDivisionId').val(defaultDivisionId);
								$('#defaultDivisionName').val(defaultDivisionName);
								$('#locationSummaryMenuLink').show();
								$('#addLocationMenuLink').show();
								$("#addDivisionMenuLink span").append(",&nbsp; ");
							}
						});
					</script>
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT %>" >
					<li id="menu_admin_group" menuId="admin_group">
						<ffi:help id="admin_group" className="moduleHelpClass"/>
						<s:url id="adminGroupUrl" value="/pages/jsp/user/admin_groupsindex.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
						</s:url>
						<sj:a
							href="%{adminGroupUrl}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_101"/></sj:a>
						
						<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
						<ffi:setProperty name="CanAdministerAnyGroup" property="GroupType" value="<%= EntitlementsDefines.ENT_GROUP_DIVISION %>"/>
						<ffi:process name="CanAdministerAnyGroup"/>
						<span onclick="ns.common.showMenuHelp('menu_admin_group')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
							<div class="formActionItemHolder hidden">
								<div class="cardHolder">
									<div class="subMenuCard">
										<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminGroupUrl}"/>','admin_group', 'AdminGroupSummary' );"><s:text name="jsp.default.GroupsHeader"/></span>
										<div class="cardSummary">
											<span><s:text name="jsp.default.Groups"/>
											</span>
										</div>
										<div class="cardLinks">
									<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminGroupUrl}"/>','admin_group', 'AdminGroupSummary','addGroupLink' );"  ><s:text name="jsp.user_18"/></span>
							</div>  
									</div>
								</div>
									<%-- <span onclick="ns.shortcut.navigateToSubmenus('admin_group');" class="removeComma"><s:text name="SUMMARY"/></span><br> --%>
									<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminGroupUrl}"/>','admin_group', 'AdminGroupSummary','addGroupLink' );"  ><s:text name="jsp.user_18"/></span> --%>
							</div>  
						</ffi:cinclude>
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-groups"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_27')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_101')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "admin_group");
					%> 
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.USER_ADMIN %>" >
					<li id="menu_admin_user" menuId="admin_user">
						<ffi:help id="admin_user" className="moduleHelpClass"/>
						<s:url id="adminUsersMenuURL" value="/pages/jsp/user/admin_usersindex.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
						</s:url>
						<sj:a
							href="%{adminUsersMenuURL}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_229"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_admin_user')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminUsersMenuURL}"/>','admin_user','UsersSummary');" ><s:text name="jsp.default.UsersHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Users"/>
										</span>
									</div>
									<div class="cardLinks">
							<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
									<ffi:process name="CanAdministerAnyGroup"/>
						            <ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminUsersMenuURL}"/>','admin_user','UsersSummary','addUserLink');"  ><s:text name="jsp.user_28"/></span>
									</ffi:cinclude>
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminUsersMenuURL}"/>','admin_user','ProfilesSummary');"  ><s:text name="jsp.user_269"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Profiles"/>
										</span>
									</div>
									<div class="cardLinks">
										<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
										<ffi:process name="CanAdministerAnyGroup"/>
							            <ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminUsersMenuURL}"/>','admin_user','ProfilesSummary','addProfileLink');"  ><s:text name="jsp.user_26"/></span>
										</ffi:cinclude>
									</div>
								</div>
							</div>
							
							<%-- <ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
							<ffi:process name="CanAdministerAnyGroup"/>
				            <ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
								<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminUsersMenuURL}"/>','admin_user','UsersSummary','addUserLink');"  ><s:text name="jsp.user_28"/></span><br>
							</ffi:cinclude>
							<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminUsersMenuURL}"/>','admin_user','ProfilesSummary');"  ><s:text name="jsp.user_269"/></span><br>
							
							<ffi:object id="CanAdministerAnyGroup" name="com.ffusion.efs.tasks.entitlements.CanAdministerAnyGroup" scope="session" />
							<ffi:process name="CanAdministerAnyGroup"/>
				            <ffi:cinclude value1="${Entitlement_CanAdministerAnyGroup}" value2="TRUE" operator="equals">
								<span class="removeComma"  onclick="ns.common.loadSubmenuSummary('<s:property value="%{adminUsersMenuURL}"/>','admin_user','ProfilesSummary','addProfileLink');"  ><s:text name="jsp.user_26"/></span>
							</ffi:cinclude> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-users"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_27')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_229')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "admin_user");
					%> 
				</ffi:cinclude>
			</ul>
</div>

	