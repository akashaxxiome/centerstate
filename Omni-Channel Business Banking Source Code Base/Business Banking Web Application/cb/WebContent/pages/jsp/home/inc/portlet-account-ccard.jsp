<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<style>
	#<s:property value="portletId"/>_colbal_summary_detail .ui-jqgrid-ftable td{
		border:none;
	}
</style>

<script type="text/javascript">
	//ns.home.initAccountCcardGrid('<s:property value="portletId"/>_conBal_ccard_datagrid', 'onCcardGridLoadComplete');
</script>

<%-- <s:include value="/pages/jsp/home/inc/portlet-con-bal-options.jsp"/> --%>
<div id="cCardAccountsPortletID">
	<%-- URL Encryption Edit By Dongning --%>
	<ffi:setProperty name="tempURL" value="/pages/jsp/home/ConsolidatedBalancePortletAction_loadCcardData.action?queryDataClass=${queryDataClass}&portletId=${portletId}" URLEncrypt="true"/>
	<s:url id="account_ccard_data_action" value="%{#session.tempURL}"/>
		<sjg:grid id="%{portletId}_conBal_ccard_datagrid"
					dataType="local" 
					href="%{account_ccard_data_action}" 
					pager="true" 
					rowList="%{#session.StdGridRowList}"
	 				rowNum="5"
					gridModel="summaries" 
					hoverrows="false" 
					footerrow="false" 
					userDataOnFooter="false"
					sortable="true"
					rownumbers="false"
     				navigator="true"
	 				navigatorAdd="false"
	 				navigatorDelete="false"
					navigatorEdit="false"
					navigatorRefresh="false"
					navigatorSearch="false"
					navigatorView="false"
					shrinkToFit="true"					
					scroll="false"
					scrollrows="true"
					viewrecords="true"
					onGridCompleteTopics="onCcardGridLoadComplete,enableGridAutoWidth">  
   <sjg:gridColumn name="account.ID" index="ID" title="%{getText('jsp.account_12')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
					     <sjg:gridColumn name="account.bankID" index="BANKID" title="%{getText('jsp.account_39')}" sortable="false" hidden="true" hidedlg="true" />
			<sjg:gridColumn name="account.routingNum" index="ROUTINGNUM" title="%{getText('jsp.account_183')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>					
			<sjg:gridColumn name="nameNickNameloan" index="NUMBER" title="%{getText('jsp.default_15')}" width="150" align="left" sortable="true" formatter="ns.home.formatAccountColumn" hidden="true"/>
			<sjg:gridColumn name="account.nickName" index="ACCNICKNAME" title="%{getText('jsp.default_293')}" width="280" align="left" sortable="true" formatter="ns.home.customToAccountNickNameColumn" hidden="true"/>
			<sjg:gridColumn name="account.accountDisplayText" index="" title="Account" width="500" align="center" sortable="false" formatter="ns.home.formatCreditCardAcctColumn" cssClass="datagrid_actionColumn" />
			<sjg:gridColumn name="account.bankName" index="BANKNAME" title="%{getText('jsp.default_61')}" width="100" align="left" sortable="true" hidden="true"/>
			<sjg:gridColumn name="summaryDateDisplay" index="SummaryDateDisplay" title="%{getText('jsp.default_51')}" width="90" align="center" sortable="true"  hidden="true"/>
			<sjg:gridColumn name="availableCredit" index="AVAIL_CREDIT" title="%{getText('jsp.default_56')}" width="140" align="center" sortable="true" formatter="ns.home.totalDebitsFormatter"  hidden="false"/>
			<sjg:gridColumn name="rate" index="INTEREST_RATE" title="%{getText('jsp.default_336')}" width="90" align="center" sortable="true" formatter="ns.home.totalDebitsFormatter"  hidden="true"/>
			<sjg:gridColumn name="amountDue" index="AMT_DUE" title="%{getText('jsp.default_44')}" width="130" align="right" sortable="true" formatter="ns.home.totalDebitsFormatter"  hidden="false"/>
			<sjg:gridColumn name="dateDue" index="DUE_DATE" title="%{getText('jsp.default_138')}" width="90" align="center" sortable="true"  hidden="true"/>
			<sjg:gridColumn name="balance" index="CLOSINGBALANCE" title="%{getText('jsp.default_60')}" width="90" align="right" sortable="true" formatter="ns.home.balanceFormatter"/>  
			<sjg:gridColumn name="dataClassification" index="dataClassification" title="%{getText('jsp.account_64')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		</sjg:grid>		
		
		<div id="<s:property value="portletId"/>_conBal_ccard_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('acctMgmt_balance,creditCardsAccounts');"><s:text name="jsp.default.viewAllRecord" /></div>
		
</div>

<script>
     $(document).ready(function(){
       if(ns.common.isInitialized($("#<s:property value='portletId'/>_conBal_ccard_datagrid",'ui-jqgrid'))){
           $("#<s:property value='portletId'/>_conBal_ccard_datagrid tbody").sortable("destroy");
       }
     });
</script>
