<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- selected module drop down items holder --%>
<ul class="moduleSubmenuDropdownHolder">
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SERVICES %>" >
		<li onclick="ns.shortcut.goToMenu('serviceCenter_services');">
			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-services"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="SERVICES"/></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FAQ %>" >
		<li onclick="ns.shortcut.goToMenu('serviceCenter_faq');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-faq"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="FAQ"/></span>
		</li>
	</ffi:cinclude>
</ul>	