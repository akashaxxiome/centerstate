<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<%-- force billpay accounts to reload --%>
<ffi:setProperty name="payments_init_touched" value="false"/>
<ffi:removeProperty name="BillPayAccounts"/>

<%-- reload banking accounts --%>
<s:action namespace="/pages/jsp/account" name="getBankingAccounts" executeResult="true"/>