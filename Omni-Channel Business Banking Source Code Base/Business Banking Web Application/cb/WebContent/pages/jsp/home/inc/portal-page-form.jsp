<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<script>
	 $(document).ready(function() {
		$('#newPortalPageName').val(ns.home.curPortalPageName);
	});
</script>

<div>
<ffi:help id="home_portal-page-form" />
<%--
	<div id='savePortalPage' style="padding:10px; display: none;">
		<table border="0">
			<tr>
				<td colspan="2" align="left">
					<div id="confirmSavePortalPageError" style="padding: 0pt 0.7em;text-align:left; display: none;" class="ui-state-error ui-corner-all ui-widget ui-widget-content">
						<span class="ui-icon ui-icon-alert" style="float:left"></span>
						<span style="float:left" id="confirmSavePortalPageErrorMsg"></span>
					</div>
				</td>
			</tr>
			<tr>
				<td align="right">Page name:</td>
				<td>
					<input class="ui-widget-content" type="text" id="newPortalPageName" value="" onkeypress="ns.home.hideErrorMsg()"/>
					<input class="ui-widget-content" type="hidden" id="currentPortalPageName" value="" />
				</td>
			</tr>
			<tr>
				<td align="right"></td>
				<td>
					<input type="checkbox" id="newPortalPageSetDefault"/>set as default &nbsp;(This portlet will show on home page)
				</td>
			</tr>
			<tr valign="top">
				<td align="right">Save Mode:</td>
				<td>
					<input type="radio" name="newPortalPageSaveMode" value="1" onclick="ns.home.onClickCreateNew()" checked />create new page
					<br/>
					<input type="radio" name="newPortalPageSaveMode" value="2" onclick="ns.home.onClickUpdateCurrent()"/>update current page					
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</table>
	</div>
--%>

	<div id="confirmSavePortalPage" style="display:none">
		<div id="confirmSavePortalPageUpdateWarn" style="padding: 0pt 0.7em;text-align:left" class="ui-state-highlight ui-corner-all ui-widget ui-widget-content">
			<table width="100%" align="left">
				<tr valign="top">
					<td><span class="ui-icon ui-icon-info" ></span></td>
					<td><span><s:text name="jsp.home_204"/></span></td>
				</tr>
			</table>
		</div>
		<div id="confirmSavePortalPageResetWarn" style="padding: 0pt 0.7em;text-align:left" class="ui-state-highlight ui-corner-all ui-widget ui-widget-content">
			<table width="100%" align="left">
				<tr valign="top">
					<td><span class="ui-icon ui-icon-info" ></span></td>
					<td><span><s:text name="jsp.home_203"/></span></td>
				</tr>
			</table>
		</div>
		<table border="0" align="left">
			<tr>
				<td align="right" nowrap><s:text name="jsp.home_150"/></td>
				<td>
					<span id="confirmSavePortalPageName"></span>
				</td>
			</tr>
			<tr>
				<td align="right" nowrap><s:text name="jsp.home_185"/></td>
				<td>
					<span id="confirmSavePortalPageDefault"></span>
				</td>
			</tr>
			<tr valign="top">
				<td align="right" nowrap><s:text name="jsp.home_175"/></td>
				<td>
					<span id="confirmSavePortalPageMode"></span>
				</td>
			</tr>
		</table>
	</div>
</div>