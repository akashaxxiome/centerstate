<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%
	Map menuShortcuts = (Map)request.getAttribute("menuShortcuts");
%>
					
<div class="submenuHolderBgCls">
		<div class="formActionItemPlaceholder">
				<div class="cardHolder">
					<div class="subMenuCard">
						<span class="cardTitle">_______________</span>
						<div class="cardSummary">
							<span>__________________________<br>__________________________<br>________</span>
						</div>
						<div class="cardLinks">
							<span>___________</span>
							<span>___________</span>
						</div> 
					</div>
				</div>	
			</div> 
		<ul id="tab14" class="mainSubmenuItemsCls">
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS%>" >
				<li id="menu_pmtTran_transfer" menuId="pmtTran_transfer">
					<ffi:help id="pmtTran_transfer" className="moduleHelpClass"/>
					<ffi:setProperty name="transURL" value="/pages/jsp/transfers/initTransferAction.action" URLEncrypt="true"/>
					<s:url id="transferMenuURL" value="%{#session.transURL}" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
					</s:url>
					<sj:a
						href="%{transferMenuURL}"
						indicator="indicator"
						targets="notes"
						onCompleteTopics="subMenuOnCompleteTopics"
					><s:text name="jsp.home_222"/></sj:a>
					<span onclick="ns.common.showMenuHelp('menu_pmtTran_transfer')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
					<div class="formActionItemHolder">
						<div class="cardHolder">
							<div class="subMenuCard">
								<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Transfer');"><s:text name="jsp.default.TransferHeader"/></span>
								<div class="cardSummary">
									<span><s:text name="jsp.default.Transfer"/>
									</span>
								</div>
								<div class="cardLinks">
						<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Transfer','addSingleTransferLink');" ><s:text name="jsp.transfer.single" /></span>
									<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Transfer','addMultipleTransferLink');"><s:text name="jsp.transfer.multiple" /></span>
								</div>
							</div>
							<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ENT_TRANSFERS_TEMPLATE%>" >
							<div class="subMenuCard">
								<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Template');"><s:text name="jsp.default.TemplateHeader"/></span>
								<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_TRANSFERS_TEMPLATE %>">
									<div class="cardSummary">
										<span><s:text name="jsp.default.Template"/>
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Template','addSingleTemplateLink');"><s:text name="jsp.transfer.single" /></span>
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Template','addMultipleTemplateLink');"><s:text name="jsp.transfer.multiple" /></span>						
									</div>
								</ffi:cinclude>
							</div>
							</ffi:cinclude>
						</div>
						<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Transfer','addSingleTransferLink');" ><s:text name="jsp.transfer.single" /></span>
						<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Transfer','addMultipleTransferLink');"><s:text name="jsp.transfer.multiple" /></span><br>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_TRANSFERS_TEMPLATE %>">
							<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Template');"><s:text name="jsp.transfers.template_summary" /></span><br>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Template','addSingleTemplateLink');"><s:text name="jsp.default.single.template" /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{transferMenuURL}"/>','pmtTran_transfer','Template','addMultipleTemplateLink');"><s:text name="jsp.default.multiple.template" /></span>						
						</ffi:cinclude> --%>
					</div> 
					<span class="ffiUiIco-menuItem ffiUiIco-menuItem-transfer"></span>
				</li>
				<s:set var="tempMenuName" value="%{getText('jsp.home.label.menu.payments')}" scope="session" />
				<s:set var="tempSubMenuName" value="%{getText('jsp.home_222')}" scope="session" />
				<% 
					menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "pmtTran_transfer");
				%>  
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES %>" >
				<li id="menu_pmtTran_wire" menuId="pmtTran_wire">
					<ffi:help id="pmtTran_wire" className="moduleHelpClass"/>
					<s:url id="wireMenuURL" value="/pages/jsp/wires/initWireAction.action" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
					</s:url>
					<sj:a
						href="%{wireMenuURL}"
						indicator="indicator"
						targets="notes"
						onCompleteTopics="subMenuOnCompleteTopics"
					><s:text name="jsp.home_235"/></sj:a>
					<span onclick="ns.common.showMenuHelp('menu_pmtTran_wire')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
					<div class="formActionItemHolder hidden">
					   <div class="cardHolder">
							<div class="subMenuCard">
								<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Wires');"><s:text name="jsp.default.WireHeader"/></span>
								<div class="cardSummary">
									<span><s:text name="jsp.default.Wire"/>
									</span>
								</div>
								<div class="cardLinks">
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>">
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Wires','newSingleWiresTransferID');"><s:text name="jsp.wire.single_wire" /></span>
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_BATCH_CREATE %>">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Wires','newBatchWiresTransferID');" class="removeComma"><s:text name="jsp.wire.wire_batch" /></span>
										</ffi:cinclude>
									</ffi:cinclude>
								</div>
							</div>
							
							<div class="subMenuCard">
								<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Template');"><s:text name="jsp.default.wireTemplateHeader" /></span>
								<div class="cardSummary">
									<span><s:text name="jsp.default.wireTemplate" />
									</span>
								</div>
								<div class="cardLinks">
									<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE %>">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Template','newSingleWiresTemplateID');" class="removeComma"><s:text name="jsp.transfers.add_template" /></span>
									</ffi:cinclude>
								</div>
							</div>
							
							<div class="subMenuCard">
								<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Payee');"><s:text name="jsp.default.BeneficiaryHeader" /></span>
								<div class="cardSummary">
									<span><s:text name="jsp.default.Beneficiary" />
									</span>
								</div>
								<div class="cardLinks">
									<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Payee','createBeneficiaryLink');" class="removeComma"><s:text name="jsp.wire.wire.add_beneficiary" /></span>
									<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_RELEASE %>">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','ReleaseWires');"><s:text name="jsp.default_589" /></span>
									</ffi:cinclude>
								</div>
							</div>
						</div>
						
						<%-- <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>">
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Wires','newSingleWiresTransferID');"><s:text name="jsp.wire.single_wire" /></span>
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_BATCH_CREATE %>">
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Wires','newBatchWiresTransferID');" class="removeComma"><s:text name="jsp.wire.wire_batch" /></span><br>
							</ffi:cinclude>
						</ffi:cinclude>
						<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Template');"><s:text name="jsp.transfers.template_summary" /></span>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE %>">
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Template','newSingleWiresTemplateID');" class="removeComma"><s:text name="jsp.transfers.add_template" /></span><br>
						</ffi:cinclude>
						<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Payee');"><s:text name="jsp.wire.wire.beneficiary_summary" /></span>
						<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','Payee','createBeneficiaryLink');" class="removeComma"><s:text name="jsp.wire.wire.add_beneficiary" /></span><br>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_RELEASE %>">
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{wireMenuURL}"/>','pmtTran_wire','ReleaseWires');"><s:text name="jsp.default_589" /></span>
						</ffi:cinclude> --%>
					</div>
					<span class="ffiUiIco-menuItem ffiUiIco-menuItem-wiretransfer"></span>
				</li>
				<s:set var="tempMenuName" value="%{getText('jsp.home.label.menu.payments')}" scope="session" />
				<s:set var="tempSubMenuName" value="%{getText('jsp.home_235')}" scope="session" />
				<% 
					menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "pmtTran_wire");
				%>  
			</ffi:cinclude>
			<ffi:cinclude value1="${displayACH}" value2="true" operator="equals">
                      <li id="menu_pmtTran_ach" menuId="pmtTran_ach">
                      	  <ffi:help id="pmtTran_ach" className="moduleHelpClass"/>
                          <s:url id="achIndexURL" value="/pages/jsp/ach/index.jsp" escapeAmp="false">
                           <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						   <s:param name="Initialize" value="true"/>
                          </s:url>
						  
						  <s:url id="achBatchSummaryURL" value="/pages/jsp/ach/index.jsp" escapeAmp="false">
                           <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						   <s:param name="summaryToLoad" value="%{'ACHBatch'}"/>
                          </s:url>
						  
                          <sj:a
								href="%{achBatchSummaryURL}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="jsp.default_22"/></sj:a>
							<span onclick="ns.common.showMenuHelp('menu_pmtTran_ach')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
							<div class="formActionItemHolder hidden">
								<div class="cardHolder">
									<div class="subMenuCard">
										<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHBatch');"><s:text name="jsp.default.BatchHeader" /></span>
										<div class="cardSummary">
											<span><s:text name="jsp.default.Batch" /> 
											</span>
										</div>
										<div class="cardLinks">
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHBatch','addSingleACHBatchLink');" >
											<s:text name="jsp.transfer.single" /></span>
										</div>
									</div>
									
									<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>" >
									<div class="subMenuCard">
										<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHPayee');"><s:text name="ach.payees_summary" /></span>
										<div class="cardSummary">
											<span><s:text name="jsp.default.PayeesSummary" /> 
											</span>
										</div>
										<div class="cardLinks borderNone">
											<span></span>
										</div>
									</div>
									</ffi:cinclude>
									
									<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_ACH_TEMPLATE %>"> 
									<div class="subMenuCard">
										<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHBatchTemplate');"><s:text name="ach.template_summary" /></span>
										<div class="cardSummary">
											<span><s:text name="jsp.default.TemplateSummary" />  
											</span>
										</div>
										<div class="cardLinks">
											<span  onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHBatchTemplate','addSingleTemplateLink');"> <s:text name="jsp.transfer.single" /></span>
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHBatchTemplate','addMultipleTemplateLink');" ><s:text name="jsp.transfer.multiple" /></span>
										</div>
									</div>
									</ffi:cinclude>
									<br/>
									<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_FILE_UPLOAD %>" >
									<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD %>" >
									<div class="subMenuCard">
										<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHFileUpload','showdbFileUploadSummary');" ><s:text name="ach.fileUpload" /></span>
										<div class="cardSummary">
											<span><s:text name="jsp.default.ACHFileUpload" />  
											</span>
										</div>
										<div class="cardLinks borderNone">
											<span></span>
										</div>
									</div>
									</ffi:cinclude>
									</ffi:cinclude>
								</div>
								
								<%-- <span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHBatch','addSingleACHBatchLink');" >
								<s:text name="ach.new_batch" /></span><br>
								<span class="removeComma" onclick="ns.shortcut.navigateToSubmenus('pmtTran_ach,addMultipleACHBatchLink');"><s:text name="ach.newMultiBatch"/></span>
								<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>" >
									<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHPayee');"><s:text name="ach.payees_summary" /></span>
									<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHPayee','addACHPayeeLink');"  class="removeComma"><s:text name="ach.add_payee" /></span><br>
								</ffi:cinclude>
								<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_ACH_TEMPLATE %>"> 
									<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHBatchTemplate');"><s:text name="ach.template_summary" /></span>
									<span  onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHBatchTemplate','addSingleTemplateLink');" 
									 class="removeComma"> <s:text name="ach.single_template" /></span><br>
									<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHBatchTemplate','addMultipleTemplateLink');" ><s:text name="ach.childsupport.multiple_batch_template" /></span>
								</ffi:cinclude>
								<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_FILE_UPLOAD %>" >
									<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD %>" >
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{achIndexURL}"/>','pmtTran_ach','ACHFileUpload','showdbFileUploadSummary');" ><s:text name="ach.fileUpload" /></span>
									</ffi:cinclude>
								</ffi:cinclude> --%>
							</div> 
							<span class="ffiUiIco-menuItem ffiUiIco-menuItem-ach"></span>
                      </li>
					<s:set var="tempMenuName" value="%{getText('jsp.home.label.menu.payments')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.default_22')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "pmtTran_ach");
					%>  
               </ffi:cinclude>
               <ffi:cinclude ifEntitled="<%= EntitlementsDefines.CCD_DED %>" >
               
               	<%
				    String entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_PAYMENT_ENTRY );
				    session.setAttribute( "tempEntToCheck", entitlementName);
				%>
				<!-- <br> -->
				<li id="menu_pmtTran_childsp" menuId="pmtTran_childsp">
					<ffi:help id="pmtTran_childsp" className="moduleHelpClass"/>
					<s:url id="childSPIndexURL" value="/pages/jsp/ach/childsp/index.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
					</s:url>
					<s:url id="childSPBatchSummaryURL" value="/pages/jsp/ach/childsp/index.jsp" escapeAmp="false">
                           <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						   <s:param name="summaryToLoad" value="%{'ChildSPBatch'}"/>					   
                    </s:url>
						  
                         <sj:a
							href="%{childSPBatchSummaryURL}"
                            indicator="indicator"
                            targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
                         	><s:text name="jsp.home_55"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_pmtTran_childsp')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder  hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle"  onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPBatch');"><s:text name="jsp.default.ChildSupportHeader" /> </span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ChildSupport" /> 
										</span>
									</div>
									<div class="cardLinks">
							<ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPBatch','addSingleChildspBatchLink');" ><s:text name="jsp.transfer.single" /></span>
										</ffi:cinclude>
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle"  onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPBatchTemplate');"><s:text name="ach.childsupport.template_summary" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ChildSupportTemplateSummary" /> 
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPBatchTemplate','addSingleTemplateLink');" ><s:text name="jsp.transfer.single" /></span>
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPBatchTemplate','addMultipleTemplateLink');"><s:text name="jsp.transfer.multiple" /></span>
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPPayee');"><s:text name="ach.childsupport.child_support_types" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ChildSupportTypes" /> 
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPPayee','addChildspPayeeLink');"><s:text name="ach.childsupport.add_child_support_type" /></span>
									</div>
								</div>
							</div>
							<%-- <ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
								<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPBatch','addSingleChildspBatchLink');" ><s:text name="ach.childsupport.new_batch" /></span><br>
							</ffi:cinclude>
							<span class="removeComma" onclick="ns.shortcut.navigateToSubmenus('pmtTran_childsp,addMultipleChildSupportBatchLink');"><s:text name="ach.newMultiBatch"/></span><br>
							<span class="removeComma"  onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPBatchTemplate');"><s:text name="ach.childsupport.template_summary" /></span><br>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPBatchTemplate','addSingleTemplateLink');" ><s:text name="ach.childsupport.single_template" /></span>
							<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPBatchTemplate','addMultipleTemplateLink');"><s:text name="ach.childsupport.multiple_batch_template" /></span><br>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPPayee');"><s:text name="ach.childsupport.child_support_types" /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{childSPIndexURL}"/>','pmtTran_childsp','ChildSPPayee','addChildspPayeeLink');"><s:text name="ach.childsupport.add_child_support_type" /></span> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-childsupport"></span>                       	
				</li>
				<s:set var="tempMenuName" value="%{getText('jsp.home.label.menu.payments')}" scope="session" />
				<s:set var="tempSubMenuName" value="%{getText('jsp.home_55')}" scope="session" />
				<% 
					menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "pmtTran_childsp");
				%>  
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CCD_TXP %>" >
				<%
				    String entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_PAYMENT_ENTRY );
				    session.setAttribute( "tempEntToCheck", entitlementName);
				%>
				<li id="menu_pmtTran_tax" menuId="pmtTran_tax">
					<ffi:help id="pmtTran_tax" className="moduleHelpClass"/>
					<s:url id="taxIndexURL" value="/pages/jsp/ach/tax/index.jsp" escapeAmp="false">
                              <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
                          </s:url>
						  
					<s:url id="taxBatchSummaryURL" value="/pages/jsp/ach/tax/index.jsp" escapeAmp="false">
                           <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						   <s:param name="summaryToLoad" value="%{'TaxBatch'}"/>					   
                    </s:url>
                          <sj:a
                              href="%{taxBatchSummaryURL}"
                              indicator="indicator"
                              targets="notes"
						onCompleteTopics="subMenuOnCompleteTopics"
                          ><s:text name="jsp.home_200"/></sj:a>
                        <span onclick="ns.common.showMenuHelp('menu_pmtTran_tax')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>  
						<div class="formActionItemHolder  hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxBatch');"><s:text name="jsp.default.TaxHeader" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Tax" /> 
										</span>
									</div>
									<div class="cardLinks">
							<ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxBatch','addSingleTaxBatchLink');"><s:text name="jsp.transfer.single" /></span>
										</ffi:cinclude>
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxBatchTemplate');"><s:text name="ach.tax.template_summary" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.TaxTemplateSummary" />  
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxBatchTemplate','addSingleTemplateLink');"><s:text name="jsp.transfer.single" /></span>
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxBatchTemplate','addMultipleTemplateLink');"><s:text name="jsp.transfer.multiple" /></span>
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxType');" ><s:text name="ach.tax.tax_types" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.TaxTypes" />   
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxType','addTaxPayeeLink');"><s:text name="ach.tax.add_tax_type" /></span>
									</div>
								</div>
							</div>
							<%-- <ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
								<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxBatch','addSingleTaxBatchLink');"><s:text name="ach.tax.new_batch" /></span><br>
							</ffi:cinclude>
							<span class="removeComma" onclick="ns.shortcut.navigateToSubmenus('pmtTran_tax,addMultipleTaxBatchLink');"><s:text name="ach.newMultiBatch"/></span><br>
							<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxBatchTemplate');"><s:text name="ach.tax.template_summary" /></span><br>
							<span  onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxBatchTemplate','addSingleTemplateLink');"><s:text name="ach.tax.single_template" /></span>
							<span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxBatchTemplate','addMultipleTemplateLink');"><s:text name="ach.childsupport.multiple_batch_template" /></span><br>
							<span  onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxType');" ><s:text name="ach.tax.tax_types" /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{taxIndexURL}"/>','pmtTran_tax','TaxType','addTaxPayeeLink');"><s:text name="ach.tax.add_tax_type" /></span> --%>
						</div>
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-tax"></span>
				</li>
				<s:set var="tempMenuName" value="%{getText('jsp.home.label.menu.payments')}" scope="session" />
				<s:set var="tempSubMenuName" value="%{getText('jsp.home_200')}" scope="session" />
				<% 
					menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "pmtTran_tax");
				%>  
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS %>" >
				<li id="menu_pmtTran_billpay" menuId="pmtTran_billpay">
					<ffi:help id="pmtTran_billpay" className="moduleHelpClass"/>
					<s:url id="billPayMenuURL" value="/pages/jsp/billpay/initBillpay_init.action" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
					</s:url>
					<sj:a
							href="%{billPayMenuURL}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_45"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_pmtTran_billpay')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>  
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','BillPay');"><s:text name="jsp.default.PaymentHeader" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Payment" /> 
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','BillPay','addSingleBillpayLink');"><s:text name="jsp.transfer.single" /></span>
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','BillPay','addMultipleBillpayLink');" class="removeComma"><s:text name="jsp.transfer.multiple" /></span>
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Template');" ><s:text name="jsp.default.BillpayHeader" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Billpaytemplate" /> 
										</span>								
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Template','addSingleTemplateLink');"><s:text name="jsp.transfer.single" /></span>
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Template','addMultipleTemplateLink');" class="removeComma"><s:text name="jsp.transfer.multiple" /></span>
									</div>
								</div>
								
								<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYEES %>">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Payee');"><s:text name="jsp.default.PayeeHeader" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.PaymentPayeesSummary" /> 
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Payee','addBillpayPayeeLink');"><s:text name="jsp.billpay.add_payee" /></span>
									</div>
								</div>
								</ffi:cinclude>
							</div>	
							
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','BillPay','addSingleBillpayLink');"><s:text name="jsp.billpay.single_payment" /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','BillPay','addMultipleBillpayLink');" class="removeComma"><s:text name="jsp.billpay.multiple_payment" /></span><br>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Template');" class="removeComma"><s:text name="jsp.billpay.template_summary" /></span><br>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Template','addSingleTemplateLink');"><s:text name="ach.tax.single_template" /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Template','addMultipleTemplateLink');" class="removeComma"><s:text name="ach.tax.multiple_template" /></span><br>
							<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYEES %>">
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Payee');"><s:text name="jsp.billpay.payee_summary" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{billPayMenuURL}"/>','pmtTran_billpay','Payee','addBillpayPayeeLink');"><s:text name="jsp.billpay.add_payee" /></span>
							</ffi:cinclude> --%>
						</div>
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-billpay"></span>
				</li>
				<s:set var="tempMenuName" value="%{getText('jsp.home.label.menu.payments')}" scope="session" />
				<s:set var="tempSubMenuName" value="%{getText('jsp.home_45')}" scope="session" />
				<% 
					menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "pmtTran_billpay");
				%>  
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.STOPS %>" >
				<li id="menu_pmtTran_stops" menuId="pmtTran_stops">
					<ffi:help id="pmtTran_stops" className="moduleHelpClass"/>
					<s:url id="stopsUrl" value="/pages/jsp/stops/stopsAction.action" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
					</s:url>
					<sj:a
						href="%{stopsUrl}"
						indicator="indicator"
						targets="notes"
						onCompleteTopics="subMenuOnCompleteTopics"
					><s:text name="jsp.home_196"/></sj:a>
					<span onclick="ns.common.showMenuHelp('menu_pmtTran_stops')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>  
					<div class="formActionItemHolder hidden">
						<div class="cardHolder">
							<div class="subMenuCard">
								<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{stopsUrl}"/>','pmtTran_stops','Stops');" class="removeComma"><s:text name="jsp.default_400" /></span>
								<div class="cardSummary">
									<span><s:text name="jsp.default.StopPayment" />  
									</span>
								</div>
								<div class="cardLinks">
					<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{stopsUrl}"/>','pmtTran_stops','Stops','addStopPaymentLink');"><s:text name="jsp.add.label" /> <s:text name="jsp.stops_17" /></span>
					</div> 
							</div>
						</div>	
							
						<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{stopsUrl}"/>','pmtTran_stops','Stops');" class="removeComma"><s:text name="jsp.default_400" /></span><br />
					    <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{stopsUrl}"/>','pmtTran_stops','Stops','addStopPaymentLink');"><s:text name="jsp.add.label" /> <s:text name="jsp.stops_17" /></span> --%>
					</div> 
					<span class="ffiUiIco-menuItem ffiUiIco-menuItem-stops"></span>
				</li>
				<s:set var="tempMenuName" value="%{getText('jsp.home.label.menu.payments')}" scope="session" />
				<s:set var="tempSubMenuName" value="%{getText('jsp.home_196')}" scope="session" />
				<% 
					menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "pmtTran_stops");
				%> 
			</ffi:cinclude>
			<ffi:cinclude value1="${displayCashConcentation}" value2="true" operator="equals">
				<li id="menu_pmtTran_cashcon" menuId="pmtTran_cashcon">
					<ffi:help id="pmtTran_cashcon" className="moduleHelpClass"/>
					<s:url id="cashConUrl" value="/pages/jsp/cashcon/index.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
					</s:url>
					<sj:a
							href="%{cashConUrl}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_48"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_pmtTran_cashcon')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{cashConUrl}"/>','pmtTran_cashcon','CashCon');" class="removeComma"><s:text name="jsp.default_400" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.cashCon"/> 
										</span>
									</div>
									<div class="cardLinks">
										<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>" >
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{cashConUrl}"/>','pmtTran_cashcon','CashCon','addDepositLink');" class="removeComma"><%-- <s:text name="jsp.add.label" /> --%> <s:text name="jsp.cashcon.deposit" /></span>
										</ffi:cinclude>
										<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST %>" >
											<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{cashConUrl}"/>','pmtTran_cashcon','CashCon','addDisbursementLink');"><%-- <s:text name="jsp.add.label" /> --%> <s:text name="jsp.cashcon.disbursement" /></span>
										</ffi:cinclude>
									</div>
								</div>
							</div>	
							
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{cashConUrl}"/>','pmtTran_cashcon','CashCon');" class="removeComma"><s:text name="jsp.default_400" /></span><br />
							<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DEPOSIT_ENTRY %>" >
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{cashConUrl}"/>','pmtTran_cashcon','CashCon','addDepositLink');" class="removeComma"><s:text name="jsp.add.label" /> <s:text name="jsp.cashcon.deposit" /></span><br>
							</ffi:cinclude>
							<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_CON_DISBURSEMENT_REQUEST %>" >
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{cashConUrl}"/>','pmtTran_cashcon','CashCon','addDisbursementLink');"><s:text name="jsp.add.label" /> <s:text name="jsp.cashcon_16" /></span>
							</ffi:cinclude> --%>
						</div> 
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-cashcon"></span>
				</li>
				<s:set var="tempMenuName" value="%{getText('jsp.home.label.menu.payments')}" scope="session" />
				<s:set var="tempSubMenuName" value="%{getText('jsp.home_48')}" scope="session" />
				<% 
					menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "pmtTran_cashcon");
				%> 
			</ffi:cinclude>
		</ul>
</div>