<%@page import="java.util.List"%>
<%@page import="com.ffusion.beans.positivepay.PPayIssue"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<script >
	var rowElement ='';
</script>


<script type="text/javascript" src="<s:url value='/web/js/reversepositivepay/reversepositivepay_grid%{#session.minVersion}.js'/>"></script>
<link rel='stylesheet' type='text/css' href='/cb/web/css/home-page.css' />
<ffi:cinclude value1="${PositivePayIssues.size}" value2="0" operator="equals">
	<div align="center">
		<s:text name="jsp.default_601"/>
	</div>
</ffi:cinclude>
<ffi:help id="home_portlet-reversepositive-pay"/>
<ffi:cinclude value1="${PositivePayIssues.size}" value2="0" operator="notEquals">
	<style>
		#<s:property value="portletId"/> .ui-jqgrid-ftable td{
			border:none;
		}
	</style>
	
	<%--URL Encryption Edit By Dongning --%>
	<script type="text/javascript">
		$(document).ready(function(){
			ns.home.initRPPayPortlet('<s:property value="portletId"/>',
							'<s:property value="portletId"/>_rppay_datagrid', 
							'<ffi:urlEncrypt url="/cb/pages/jsp/home/ReversePositivePayPortletAction_updateColumnSettings.action?portletId=${portletId}"/>',
							'<s:property value="portletId"/>_grid_load_complete',
							'<s:property value="columnPerm"/>',
							'<s:property value="hiddenColumnNames"/>');
		});		
	</script>
	<%-- URL Encryption Edit By Dongning --%>
	<ffi:setGridURL grid="GRID_rppayGrid" name="ViewURL" url="/cb/pages/jsp/reversepositivepay/SearchImageAction.action?module=REVERSEPOSITIVEPAY&Portlet=TRUE&pPayaccountID={0}&pPaybankID={1}&pPaycheckNumber={2}" parm0="CheckRecord.AccountID" parm1="CheckRecord.BankID" parm2="CheckRecord.CheckNumber" />
	<ffi:setProperty name="rppayPortletTempURL" value="/pages/jsp/home/ReversePositivePayPortletAction_loadPPayData.action?GridURLs=GRID_rppayGrid&portletId=${portletId}" URLEncrypt="true"/>
	<s:url id="rppay_data_action" value="%{#session.rppayPortletTempURL}"/>	
	<sjg:grid id="%{portletId}_rppay_datagrid"
					sortable="true"  
					dataType="json" 
					href="%{rppay_data_action}" 
					gridModel="gridModel" 
					hoverrows="true"
					sortname="issueDate" 
					sortorder="desc"
					pager="true" 
					rowList="%{#session.StdGridRowList}" 
	 				rowNum="5"
	 				rownumbers="false"
     				navigator="true"
	 				navigatorAdd="false"
	 				navigatorDelete="false"
					navigatorEdit="false"
					navigatorRefresh="false"
					navigatorSearch="false"
					navigatorView="false"
					shrinkToFit="true"					
					scroll="false"
					viewrecords="true"
					scrollrows="true"
					onGridCompleteTopics="%{portletId}_grid_load_complete,reversePositivePayPortletGridComplete">
			<sjg:gridColumn name="map.FormattedAccountID" index="account" title="%{getText('jsp.default_15')}" width="260" sortable="true" hidedlg="true" 
			cssClass="datagrid_textColumn" formatter="ns.home.positivePayAccount"/>
			<sjg:gridColumn name="map.FormattedIssueDate" index="issueDate" title="%{getText('jsp.default_137')}" width="80" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_dateColumn"/>
			<sjg:gridColumn name="checkRecord.checkNumber" index="checkNumber" title="%{getText('jsp.default_92')}" width="70" sortable="true" formatter="ns.cash.searchImageFormatter" cssClass="datagrid_actionColumn ppayDetailsOnHoverCls"/>
			<sjg:gridColumn name="checkRecord.amount.currencyStringNoSymbol" index="amount" title="%{getText('jsp.default_43')}" width="80" align="right" sortable="true" cssClass="datagrid_textColumn"/>
			<sjg:gridColumn name="checkRecord.bankID" index="bankIDColumn" title="%{getText('jsp.cash_19')}"  hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="checkRecord.accountID" index="accountIDColumn" title="%{getText('jsp.cash_11')}" hidden="true" hidedlg="true"/>
		
			<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="PPAYImageUrlId"/>
	</sjg:grid>
	
	<div id="<s:property value="portletId"/>_rppay_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('cashMgmt_rppay');"><s:text name="jsp.default.viewAllRecord" /></div>

<div class="ppayHoverDetailsMaster">
	<div class="ppayDetailsWinOverlay"></div>
	<div class="ppayHoverDetailsHolder">
		<span class="winTitle">Check Image</span>

		<hr />
		<div id ="imgHolder" class="chkImgHolder">
			<span class="imagePlaceholderText">Loading check image, please wait...</span>
		</div>
		

		<div class="imageControlsHolder">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<tr>
					<td colspan="2" align="center">
						<s:form action="" method="post" name="checkImageForm" id="checkImageID" theme="simple">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							
						</s:form>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
	
	<script>
		$(document).ready(function(){
			if($("#<s:property value='portletId'/>_rppay_datagrid tbody").data("ui-jqgrid") != undefined){
				$("#<s:property value='portletId'/>_rppay_datagrid tbody").sortable("destroy");
			}
		});	
			
		$(".ppayDetailsOnHoverCls a").hover(
				function(event) {				
					rowElement = event.fromElement.parentElement;
					var urlString = $(rowElement.getElementsByClassName('PPAYImageUrlId')).attr('title');
					$.ajax({
						url: urlString,
						success: function(data){
							$('#imgHolder').html(data);
							}
					});
					$(".ppayHoverDetailsMaster").show();
					
					if(($(this).offset().left) <= ($(window).width()/2)) {
						$(".ppayHoverDetailsHolder").offset({left: ($(this).offset().left) });
					} else {
						$(".ppayHoverDetailsHolder").offset({left: ($(window).width() / 2) });
					}
				}, function() {
					// space for code to run on mouseout...
				}
			);
			
			$(".ppayHoverDetailsHolder").hover(function(event){
				event.stopPropagation();
			});

			$(".ppayDetailsWinOverlay").hover(function(event) {
				$(".ppayHoverDetailsMaster").hide();
			})
			.mouseleave(function() {
				// space for code to run on mouseleave event
			});
	</script>
</ffi:cinclude>
<sj:dialog id="searchImageDialog" cssClass="cashMgmtDialog" title="%{getText('jsp.default_95')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="600" >
</sj:dialog>