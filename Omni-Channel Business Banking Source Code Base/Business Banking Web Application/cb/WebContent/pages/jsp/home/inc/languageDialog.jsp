<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<sj:dialog
	id="changeLanguage"
	onCloseTopics="changeLanguageCloseTopic"
	autoOpen="false"
	modal="true" 
	title="%{getText('language')}"
	showEffect="fold"
	hideEffect="clip"
	cssClass="staticContentDialog"
	width="auto"
>
<div class="tableData">
	<span><s:text name="jsp.home_107"/></span><br/><br/>
	<ffi:cinclude value1="${GetLanguageList}" value2="" operator="equals" >
		<ffi:object id="GetLanguageList" name="com.ffusion.tasks.util.GetLanguageList" scope="session" />
		<ffi:process name="GetLanguageList" />
	</ffi:cinclude>
			
	<div class="langSelectionSeperator">
		<ffi:list collection="GetLanguageList.LanguagesList" items="language">
			<ffi:setProperty name="tmpLan" value="${language.Language}" />
				<a onclick="ns.common.changeLanguage('index.jsp?request_locale=<ffi:getProperty name='language' property='Language' />')" title="<s:i18n name="com.ffusion.struts.i18n.home_%{#session.tmpLan}" ><s:text name="jsp.home_107" /></s:i18n>"
				><img src="/cb/web/multilang/grafx/<ffi:getProperty name="language" property="Language" />.gif"
				style="border-style:none;cursor:pointer;" /></a>
		</ffi:list>
	</div>
</div>
</sj:dialog>