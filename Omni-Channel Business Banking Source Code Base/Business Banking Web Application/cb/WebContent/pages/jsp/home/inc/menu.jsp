<!DOCTYPE html>
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page
	import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%
		Map menuShortcuts = new LinkedHashMap();
		request.setAttribute("menuShortcuts",menuShortcuts);
	%>

<%-- enabling entitlement based on app type --%>
<%-- Search Image Check--%>
<ffi:setProperty name="isImageSearchEntitled" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
	operator="equals">
	<ffi:setProperty name="isImageSearchEntitled" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="isImageSearchEntitled" value="true" />
</ffi:cinclude>

<%-- Service Center Check--%>
<ffi:setProperty name="isServiceCenterEntitled" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
	operator="equals">
	<ffi:setProperty name="isServiceCenterEntitled" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>"
	operator="equals">
	<ffi:setProperty name="isServiceCenterEntitled" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="isServiceCenterEntitled" value="true" />
</ffi:cinclude>

<%-- Admin Entitlement Check--%>
<ffi:setProperty name="isAdminEntitled" value="true" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
	operator="equals">
	<ffi:setProperty name="isAdminEntitled" value="false" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="isAdminEntitled" value="false" />
</ffi:cinclude>


<%-- Reporting Check--%>
<ffi:setProperty name="isReportingEntitled" value="true" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
	operator="equals">
	<ffi:setProperty name="isReportingEntitled" value="false" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="isReportingEntitled" value="false" />
</ffi:cinclude>

<ffi:setProperty name="isUserPreferencesEntitled" value="true" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
	operator="equals">
	<ffi:setProperty name="isUserPreferencesEntitled" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="isUserPreferencesEntitled" value="true" />
</ffi:cinclude>


<%-- first level menu --%>
<ul id="tabbuttons" class="mainMenuBtnBarcls hidden"
	style="margin: auto; width: 100%; background: transparent;">

	<ffi:list collection="DisplayedMenuTabs" items="Tab">
		<ffi:cinclude value1="${Tab.Key}" value2="home">
			<li id="menu_home" class="tab1 tab11"
				link="<s:url value="/pages"/>/jsp/home/home-page.action"
				subList="tab11" menuId="home"><span
				class="mainMenuItemCls ffiUiIcoLarge ffiUiIco-icon-home"></span> <a
				id="ffiHomeID" class="mainMenuCls" href="#" accesskeytype="home"><s:text
						name="jsp.home_105" /></a> <s:set var="tempMenuName"
					value="%{getText('jsp.home_105')}" scope="session" /> <%
					menuShortcuts.put(session.getAttribute("tempMenuName"), "home");
					%> <s:include value="/pages/jsp/home/inc/homeSubmenu.jsp" /></li>
		</ffi:cinclude>
		<ffi:cinclude value1="${Tab.Key}" value2="acctMgmt">
			<li id="menu_acctMgmt" class="tab1 tab12"
				link="<s:url value="/pages"/>/jsp/account/InitConsilidatedBalanceAction.action"
				subList="tab12" menuId="acctMgmt"><span
				class="mainMenuItemCls ffiUiIcoLarge ffiUiIco-icon-accounts"></span>
				<a href="#" class="mainMenuCls"><s:text name="jsp.home_21" /></a> <s:include
					value="/pages/jsp/home/inc/accountSubmenu.jsp" /></li>
		</ffi:cinclude>
		<ffi:cinclude value1="${Tab.Key}" value2="cashMgmt">
			<li id="menu_cashMgmt" class="tab1 tab13"
				link="<s:url value="/"/><ffi:getProperty name="nav_default_cash"/>"
				subList="tab13" menuId="cashMgmt"><span
				class="mainMenuItemCls ffiUiIcoLarge ffiUiIco-icon-cashFlow"></span>
				<a href="#" class="mainMenuCls"><s:text name="jsp.home_50" /></a> <s:include
					value="/pages/jsp/home/inc/cashSubmenu.jsp" /></li>
		</ffi:cinclude>
		<ffi:cinclude value1="${Tab.Key}" value2="pmtTran">
			<li id="menu_pmtTran" class="tab1 tab14"
				link="<s:url value="/"/><ffi:getProperty name="nav_default_payments"/>"
				subList="tab14" menuId="pmtTran"><span
				class="mainMenuItemCls ffiUiIcoLarge ffiUiIco-icon-transfer"></span>
				<a href="#" class="mainMenuCls"><s:text
						name="jsp.home.label.menu.payments" /></a> <s:include
					value="/pages/jsp/home/inc/transferSubmenu.jsp" /></li>
		</ffi:cinclude>
		<ffi:cinclude value1="true" value2="${isAdminEntitled}"
			operator="equals">
			<ffi:cinclude value1="${Tab.Key}" value2="admin">
				<li id="menu_admin" class="tab1 tab15"
					link="<s:url value="/"/><ffi:getProperty name="nav_default_user"/>"
					subList="tab15" menuId="admin"><span
					class="mainMenuItemCls ffiUiIcoLarge ffiUiIco-icon-admin"></span> <a
					href="#" class="mainMenuCls"><s:text name="jsp.home_27" /></a> <s:include
						value="/pages/jsp/home/inc/adminSubmenu.jsp" /></li>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Tab.Key}" value2="approvals">
			<li id="menu_approvals" class="tab1 tab16"
				link="<s:url value="/"/><ffi:getProperty name="nav_default_approvals"/>"
				subList="tab16" menuId="approvals"><span
				class="mainMenuItemCls ffiUiIcoLarge ffiUiIco-icon-approval-icon"></span>
				<a href="#" class="mainMenuCls"><s:text name="jsp.home_34" /></a> <s:include
					value="/pages/jsp/home/inc/approvalSubmenu.jsp" /></li>
		</ffi:cinclude>
		<ffi:cinclude value1="true" value2="${isReportingEntitled}"
			operator="equals">
			<ffi:cinclude value1="${Tab.Key}" value2="reporting">
				<li id="menu_reporting" class="tab1 tab17"
					link="<s:url value="/"/><ffi:getProperty name="nav_default_reports"/>"
					subList="tab17" menuId="reporting"><span
					class="mainMenuItemCls ffiUiIcoLarge ffiUiIco-icon-reporting"></span>
					<a href="#" class="mainMenuCls"><s:text name="jsp.default_355" /></a>
					<s:include value="/pages/jsp/home/inc/reportingSubmenu.jsp" /></li>
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude value1="${Tab.Key}" value2="contactus">
			<li id="menu_contactus" class="tab1 tab18">
					<a href="#" class="mainMenuCls" onclick="ns.common.openDialog('contactUs');">
					<s:text name="contactUs" /></a>
					</li>
		</ffi:cinclude>

		<ffi:cinclude value1="true" value2="${isServiceCenterEntitled}"
			operator="equals">
			<ffi:cinclude value1="${Tab.Key}" value2="serviceCenter">
				<li id="menu_serviceCenter" class="tab1 tab19"
					link="<s:url value="/"/><ffi:getProperty name="nav_default_servicecenter"/>"
					subList="tab19" menuId="serviceCenter"><span
					class="mainMenuItemCls ffiUiIcoLarge ffiUiIco-icon-service-center"></span>
					<a href="#" class="mainMenuCls"><s:text name="SERVICE_CENTER" /></a>
					<s:include value="/pages/jsp/home/inc/serviceCenterSubmenu.jsp" />
				</li>
			</ffi:cinclude>
		</ffi:cinclude>
		<%-- <ffi:cinclude value1="true" value2="${isUserPreferencesEntitled}" operator="equals">
				<ffi:cinclude value1="${Tab.Key}" operator="equals" value2="preferences">
					<li id="menu_preferences" class="tab1 tab19" subList="tab19" menuId="preferences">
						<span class="mainMenuItemCls ffiUiIcoLarge ffiUiIco-icon-prefrences"></span>
						<a  href="#" class="mainMenuCls"><s:text name="jsp.home_165"/></a>
						<s:include value="/pages/jsp/home/inc/preferencesSubmenu.jsp" />
					</li>
				</ffi:cinclude>
			</ffi:cinclude> --%>
	</ffi:list>
</ul>

<%
	session.setAttribute("menuShortcuts",menuShortcuts);	
%>

<script>
	$(document).ready(function() {
		$(".mainMenuBtnBarcls li").removeClass("ui-state-default ui-corner-top");
	});
	// BCP incident #1670498284 - To fix jquery widget pop-over issue.
	$(".tab1").mouseover(function(){
		$('#notes').click(); // Other pop-over controls.
		$('#notes').trigger('mouseup');// Auto-complete.
		$('#notes').trigger('mousedown');// SelectMenu.
	});
</script>
