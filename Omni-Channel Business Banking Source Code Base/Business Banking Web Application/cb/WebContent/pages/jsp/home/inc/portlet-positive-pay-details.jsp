<%@page import="java.util.List"%>
<%@page import="com.ffusion.beans.positivepay.PPayIssue"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<script >
	var rowElement ='';
</script>

<script type="text/javascript" src="<s:url value='/web/js/positivepay/positivepay_grid%{#session.minVersion}.js'/>"></script>
<link rel='stylesheet' type='text/css' href='/cb/web/css/home-page.css' />
<ffi:cinclude value1="${PositivePayIssues.size}" value2="0" operator="equals">
	<div align="center">
		<s:text name="jsp.default_550"/>
	</div>
</ffi:cinclude>
<ffi:help id="home_portlet-positive-pay"/>
<ffi:cinclude value1="${PositivePayIssues.size}" value2="0" operator="notEquals">
	<style>
		#<s:property value="portletId"/> .ui-jqgrid-ftable td{
			border:none;
		}
	</style>
	
	<%--URL Encryption Edit By Dongning --%>
	<script type="text/javascript">
		$(document).ready(function(){
			ns.home.initPPayPortlet('<s:property value="portletId"/>',
							'<s:property value="portletId"/>_ppay_datagrid', 
							'<ffi:urlEncrypt url="/cb/pages/jsp/home/PositivePayPortletAction_updateColumnSettings.action?portletId=${portletId}"/>',
							'<s:property value="portletId"/>_grid_load_complete',
							'<s:property value="columnPerm"/>',
							'<s:property value="hiddenColumnNames"/>');
		});		
	</script>
	<%-- URL Encryption Edit By Dongning --%>
	<ffi:setGridURL grid="GRID_ppayGrid" name="ViewURL" url="/cb/pages/jsp/positivepay/SearchImageAction.action?module=POSITIVEPAY&Portlet=TRUE&pPayaccountID={0}&pPaybankID={1}&pPaycheckNumber={2}" parm0="CheckRecord.AccountID" parm1="CheckRecord.BankID" parm2="CheckRecord.CheckNumber" />
	<ffi:setProperty name="ppayPortletTempURL" value="/pages/jsp/home/PositivePayPortletAction_loadPPayData.action?GridURLs=GRID_ppayGrid&portletId=${portletId}" URLEncrypt="true"/>
	<s:url id="ppay_data_action" value="%{#session.ppayPortletTempURL}"/>	
	<sjg:grid id="%{portletId}_ppay_datagrid"
					sortable="true"  
					dataType="json" 
					href="%{ppay_data_action}" 
					gridModel="gridModel" 
					hoverrows="true"
					sortname="issueDate" 
					sortorder="desc"
					pager="true" 
					rowList="%{#session.StdGridRowList}" 
	 				rowNum="5"
	 				rownumbers="false"
     				navigator="true"
	 				navigatorAdd="false"
	 				navigatorDelete="false"
					navigatorEdit="false"
					navigatorRefresh="false"
					navigatorSearch="false"
					navigatorView="false"
					shrinkToFit="true"					
					scroll="false"
					viewrecords="true"
					scrollrows="true"
					onGridCompleteTopics="%{portletId}_grid_load_complete,positivePayPortletGridComplete">
			<sjg:gridColumn name="map.FormattedAccountID" index="account" title="%{getText('jsp.default_15')}" width="260" sortable="true" hidedlg="true" 
			cssClass="datagrid_textColumn" formatter="ns.home.positivePayAccount"/>
			<sjg:gridColumn name="map.FormattedIssueDate" index="issueDate" title="%{getText('jsp.default_137')}" width="80" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_dateColumn"/>
				<sjg:gridColumn name="checkRecord.checkNumber" index="checkNumber" title="%{getText('jsp.default_92')}" width="70" sortable="true" formatter="ns.cash.searchImageFormatter" cssClass="datagrid_actionColumn ppayDetailsOnHoverCls"/>
			<sjg:gridColumn name="checkRecord.amount.currencyStringNoSymbol" index="amount" title="%{getText('jsp.default_43')}" width="80" align="right" sortable="true" cssClass="datagrid_textColumn"/>
			<sjg:gridColumn name="checkRecord.bankID" index="bankIDColumn" title="%{getText('jsp.cash_19')}"  hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="checkRecord.accountID" index="accountIDColumn" title="%{getText('jsp.cash_11')}" hidden="true" hidedlg="true"/>
			<sjg:gridColumn name="rejectReason" index="rejectReason" title="%{getText('jsp.default_349')}" width="120" sortable="true" cssClass="datagrid_textColumn"/>
			<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true" cssClass = "PPAYImageUrlId"/>
	</sjg:grid>
	
	<div id="<s:property value="portletId"/>_ppay_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('cashMgmt_ppay');"><s:text name="jsp.default.viewAllRecord" /></div>
<div class="ppayHoverDetailsMaster">
	<div class="ppayDetailsWinOverlay"></div>
	<div class="ppayHoverDetailsHolder">
		<span class="winTitle">Check Image</span>
<!-- 		<div class="checkDetailsSection">
			<table border="0" width="100%" class="checkDetailsTable">
				<tr>
					<td class="fieldLbl" width="25%">Check Number:</td>
					<td class="fieldVal" width="25%">1016</td>
					<td class="fieldLbl" width="25%">Posting Date:</td>
					<td class="fieldVal" width="25%">04/30/1998</td>
				</tr>
				<tr>
					<td class="fieldLbl" width="25%">Account:</td>
					<td class="fieldVal" width="25%">50000-Savings</td>
					<td class="fieldLbl" width="25%">Debit Amount:</td>
					<td class="fieldVal" width="25%">$400,000.00</td>
				</tr>
			</table>
		</div> -->
		<hr />
		<div id ="imgHolder" class="chkImgHolder">
			<span class="imagePlaceholderText">Loading check image, please wait...</span>
		</div>
		

		<div class="imageControlsHolder">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<tr>
					<td colspan="2" align="center">
						<s:form action="" method="post" name="checkImageForm" id="checkImageID" theme="simple">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							
						</s:form>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<script>
		$(document).ready(function(){
			if($("#<s:property value='portletId'/>_ppay_datagrid tbody").data("ui-jqgrid") != undefined){
				$("#<s:property value='portletId'/>_ppay_datagrid tbody").sortable("destroy");
			}
			
			
			$(".ppayDetailsOnHoverCls a").hover(
					function(event) {				
						rowElement = event.fromElement.parentElement;
						var urlString = $(rowElement.getElementsByClassName('PPAYImageUrlId')).attr('title');
						$.ajax({
							url: urlString,
							success: function(data){
								$('#imgHolder').html(data);
								}
						});
						$(".ppayHoverDetailsMaster").show();
						
						if(($(this).offset().left) <= ($(window).width()/2)) {
							$(".ppayHoverDetailsHolder").offset({left: ($(this).offset().left) });
						} else {
							$(".ppayHoverDetailsHolder").offset({left: ($(window).width() / 2) });
						}
					}, function() {
						// space for code to run on mouseout...
					}
				);
				
			

				$(".ppayHoverDetailsHolder").hover(function(event){
					event.stopPropagation();
				});

				$(".ppayDetailsWinOverlay").hover(function(event) {
					$(".ppayHoverDetailsMaster").hide();
				})
				.mouseleave(function() {
					// space for code to run on mouseleave event
				});
		});	
	</script>
</ffi:cinclude>
<sj:dialog id="searchImageDialog" cssClass="cashMgmtDialog" title="%{getText('jsp.default_95')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="600" >
</sj:dialog>