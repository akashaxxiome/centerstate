<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<style>
	#<s:property value="portletId"/> .ui-jqgrid-ftable td{
		border:none;
	}
</style>
<script type="text/javascript">
	
	function <s:property value="portletId"/>_switchToEdit() {
		$('#<s:property value="portletId"/>').portlet('edit');
	}
	
	function lookupStockQuote(stockSymbol, stockSymbolType) {
		if($('#stockLookupDialog')) {
			$('#stockLookupDialog').remove();
	    }
		
		var dialogObj = $("<div id='stockLookupDialog'/>").dialog({
			width: 500,
			autoOpen: true,
			show: 'fold',
			hide: 'clip',
			resizable: false,
			modal: true,
			title: js_lookupstock_dialog_title
		});

		var aConfig = {
				type: "POST",
				url: '<s:url action="StockPortletAction_lookupStock" />',
				data: {portletId:'<s:property value="portletId"/>', lookupType: 'quote', symbol: stockSymbol, symbolType: stockSymbolType, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
				success: function(data){
					dialogObj.html(data);
					var symbolDiv = dialogObj.find('#<s:property value="portletId"/>_stockSymbol');
					if(symbolDiv) {
						var newSymbol = dialogObj.find('#<s:property value="portletId"/>_stockSymbol').html();
						if(newSymbol) {
							symbolDiv.hide();
							var buttonOpts = {};
							buttonOpts[js_done_PascalCase] = function () { $(this).dialog("close"); };
							if(newSymbol == 'lookup') {
								dialogObj.dialog('option', 'buttons',buttonOpts);
								dialogObj.dialog('option', 'title', js_symbollookup_dialog_title);
							} else if(newSymbol == 'nomatch') {
								dialogObj.dialog('option', 'buttons',buttonOpts);
								dialogObj.dialog('option', 'title', js_nosymbolfound_message);
							} else {
								var stockSymbol = dialogObj.find('#<s:property value="portletId"/>_stockSymbolHiddenDiv').html();
								buttonOpts[js_button_StockChart] = function(){$(this).dialog('close');lookupStockChart($.trim(newSymbol), 'Symbol');};
								buttonOpts[js_Add_PascalCase] =  function() { $(this).dialog('close'); <s:property value="portletId"/>_addSymbol(stockSymbol)};	
								
								dialogObj.dialog('option', 'buttons', buttonOpts);
								dialogObj.dialog('option', 'title', js_stock_quote_dailog_title);
							}
						}
					}
				}
			};

			var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
			$.ajax(ajaxSettings);
	}
	
	function lookupStockChart(stockSymbol, stockSymbolType) {
		var dialogObj = $("<div/>").dialog({
			width: 500,
			autoOpen: true,
			show: 'fold',
			hide: 'clip',
			resizable: false,
			modal: true,
			title: js_lookupstock_dialog_title
		});

		var aConfig = {
			type: "POST",
			url: '<s:url action="StockPortletAction_lookupStock" />',
			data: {portletId:'<s:property value="portletId"/>', lookupType: 'chart', symbol: stockSymbol, symbolType: stockSymbolType, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
			success:function(data){
				dialogObj.html(data);
				var symbolDiv = dialogObj.find('#<s:property value="portletId"/>_stockSymbol');
				if(symbolDiv) {
					var newSymbol = dialogObj.find('#<s:property value="portletId"/>_stockSymbol').html();
					if(newSymbol) {
						symbolDiv.hide();
						var buttonOpts = {};
						buttonOpts[js_done_PascalCase] = function() { $(this).dialog('close'); };
						if(newSymbol == 'lookup') {
							dialogObj.dialog('option', 'buttons', buttonOpts);
							dialogObj.dialog('option', 'title', js_symbollookup_dialog_title);
						} else if(newSymbol == 'nomatch') {
							dialogObj.dialog('option', 'buttons', buttonOpts);						
							dialogObj.dialog('option', 'title', js_nosymbolfound_message);
						} else {
							buttonOpts[js_stock_quote] = function(){$(this).dialog('close');lookupStockQuote($.trim(newSymbol), 'Symbol');};
							buttonOpts[js_Add_PascalCase] =  function() { $(this).dialog('close'); <s:property value="portletId"/>_addSymbol(newSymbol);};
							dialogObj.dialog('option', 'buttons', buttonOpts);
							dialogObj.dialog('option', 'title', js_stock_chart_dailog_title);
						}
					}
				}
			}
		};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);		
	}
	
	function SymbolAnchorFormatter(cellvalue, options, rowObject) {
		var anchor = '<a href="javascript:void(0)" onclick="lookupStockQuote(\'' + cellvalue + '\', \'Symbol\')">' + cellvalue + '</a>';
		return anchor;
	}
	
	function ChartAnchorFormatter(cellvalue, options, rowObject) {
		var symbol = rowObject.symbol;
		var anchor = '<a href="javascript:void(0)" onclick="lookupStockChart(\'' + symbol + '\', \'Symbol\')"><img src="<s:url value="/web"/>/multilang/grafx/chart.gif" width="12" height="12" alt="<s:text name="jsp.home_230"/>" border="0" style="text-align: center"/></a>';
		return anchor;
	}
	
	function getFormattedContent(formatType, cellvalue) {
		var html = cellvalue;
		var aConfig = {
			async: false,
			url: '<s:url value="/pages" />/jsp/home/portlet/portlet-stock-formatter.jsp',
			type:"POST",
			data: {
				type: formatType, value: cellvalue, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage
			},
			success: function(data){
				html = $.trim(data);
			}
		};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);		
		$.ajax(ajaxSettings);
		return html;
	}
	
	function DayChangeFormatter(cellvalue, options, rowObject) {
		if(rowObject.symbol)
			return getFormattedContent('DayChange', cellvalue);
		return cellvalue;
	}
	
	function GainLossFormatter(cellvalue, options, rowObject) {
		return getFormattedContent('GainLoss', cellvalue);
	}
</script>

<ffi:object id="Currency" name="com.ffusion.beans.common.Currency" scope="request"/>
<ffi:object id="FloatMath" name="com.ffusion.beans.util.FloatMath" scope="session"/>
<ffi:setProperty name="PortalStockChartPeriod" value="3mon"/>

<ffi:help id="home_portlet-stock"/>
<table border="0" cellspacing="0" cellpadding="0" width="100%" class="marginBottom5">
	

	<%-- Since Stocks does not support the Size property, we use the following to determine if it's empty --%>
	<ffi:setProperty name="hasStocksItems" value="false"/>
	<ffi:list collection="Stocks" items="item">
		<ffi:setProperty name="hasStocksItems" value="true"/>
	</ffi:list>

	<ffi:cinclude value1="false" value2="${hasStocksItems}" operator="equals">
	<%-- IF Stocks COLLECTION IS EMPTY --%>
		<tr>
			<td align="center" colspan="6">
				<br><br><br><br><br>
				<s:text name="jsp.home_137"/><br>
				<s:text name="jsp.home_56"/> <a href='javascript:void(0)' class="sapUiLnk" onclick='<s:property value="portletId"/>_switchToEdit()'><s:text name="jsp.default_179"/></a> <s:text name="jsp.home_211"/>
			</td>
		</tr>
	</ffi:cinclude>

	<ffi:cinclude value1="true" value2="${hasStocksItems}" operator="equals">
	<tr>
		<td>
		<%-- IF Stocks COLLECTION IS NOT EMPTY  --%>
		<ffi:setProperty name="tempURL" value="StockPortletAction_loadStockData.action?portletId=%{portletId}" URLEncrypt="true"/>		
		<s:url id="stock_data_action" value="%{#session.tempURL}"/>
		
		<sjg:grid id="%{portletId}_stock_datagrid"
					sortable="true"  
					dataType="json" 
					href="%{stock_data_action}" 
					pager="false" 
					gridModel="gridModel" 
					hoverrows="true" 
					footerrow="true" 
					userDataOnFooter="true" 
					onCompleteTopics="onStocksDataLoadComplete">         
			<sjg:gridColumn name="chart" index="chart" title="%{getText('jsp.home_54')}" width="60" align="center" formatter="ChartAnchorFormatter" sortable="false"/>
			<sjg:gridColumn name="symbol" index="symbol" title="%{getText('jsp.default_517')}" width="60" align="left" formatter="SymbolAnchorFormatter" sortable="false"/>
			<sjg:gridColumn name="lastSalePrice" index="currentPrice" title="%{getText('jsp.home_69')}" width="100" align="right" sortable="false"/>
			<sjg:gridColumn name="changePrice" index="dayChange" title="%{getText('jsp.home_73')}" width="100" align="right" formatter="DayChangeFormatter" sortable="false"/>
			<sjg:gridColumn name="position" index="gainLoss" title="%{getText('jsp.home_98')}" width="120" align="right" formatter="GainLossFormatter" sortable="false"/>
			<sjg:gridColumn name="currentValue" index="totalValue" title="%{getText('jsp.home_220')}" width="120" align="right" sortable="false"/>
		</sjg:grid>
		<script>
		if($("#<s:property value='portletId'/>_stock_datagrid tbody").data("ui-jqgrid") != undefined){
			$("#<s:property value='portletId'/>_stock_datagrid tbody").sortable("destroy");
		}
		</script>
		
		</td>
	</tr>
	</ffi:cinclude>
<%-- 
	<tr>
		<td class="mainfont" colspan="6" align="center" valign="bottom" height="30">
			<form name="StockSearch" method="post" action="<ffi:getProperty name="SecurePath"/>inc/portal/portal-stock-lookup-wait.jsp" onsubmit="newWindow('','NewFormsWindow','width=525,height=425,resizable=no,scrollbars=no,status=no'); return true;" target="NewFormsWindow">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><img src="/cb/web/multilang/grafx/spacer.gif" width="15" height="10"></td>
					</tr>
					<tr>
						<td valign="bottom" nowrap>
							<nobr>
								<select class="txtbox" name="SymbolCompanyFlag" style="vertical-align: middle">
									<option value="company" selected>Business Name&nbsp;</option>
									<option value="symbol">Stock Symbol</option>
								</select>
								<input class="txtbox" type="text" name="SymbolCompany" size="8" style="vertical-align: middle">&nbsp;
								<select class="txtbox" name="StockLookup" style="vertical-align: middle">
									<option value="quote">Quote and News</option>
									<option value="chart">Stock Chart</option>
								</select>
								<input type="Image" value="submit" src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/button_search.gif" width="55" height="16" border="0" alt="Search" style="vertical-align: middle">
							</nobr>
						</td>
					</tr>
					<tr>
						<td class="columndata">
							Data delayed at least 20 minutes&nbsp;&nbsp;	Last update <ffi:setProperty name="GetCurrentDate" property="DateFormat" value="M/d/yyyy h:mm a (z)" />
							<ffi:getProperty name="GetCurrentDate" property="Date"/>
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
--%>
</table>

<div id="stockPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick='<s:property value="portletId"/>_switchToEdit()'><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>