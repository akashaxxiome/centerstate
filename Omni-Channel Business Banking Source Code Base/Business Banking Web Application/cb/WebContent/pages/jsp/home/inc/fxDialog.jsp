<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<s:url id="fxpageurl" value="/pages/jsp/fx/fx.jsp" escapeAmp="false">
	<s:param name="Init" value="true"/>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
		<s:param name="objectID" value="%{#session.Business.entitlementGroupId}"/>
		<s:param name="objectType" value="3"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
		<s:param name="objectID" value="%{#session.SecureUser.ProfileID}"/>
		<s:param name="objectType" value="4"/>
	</ffi:cinclude>
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
</s:url>
<sj:dialog
	id="remotefxdialog"
	autoOpen="false"
	modal="true"
	title="%{getText('jsp.home_94')}"
	height="550"
	width="700"
	href="%{fxpageurl}"
	showEffect="fold"
  		hideEffect="clip"
	onCloseTopics="closeForeignExchangeUitilityDialog"
  		buttons="{
   		'%{getText(\"jsp.default_175\")}':function() { ns.common.closeDialog('#remotefxdialog');}
  		}"
  		resizable="false"
>
</sj:dialog>