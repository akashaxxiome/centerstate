<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<sj:dialog id="contactUs" autoOpen="false" modal="true"
	title="%{getText('jsp.home_62')}" cssClass="staticContentDialog"
	showEffect="fold" hideEffect="clip" resizable="false">
	<s:text name="jsp.default_540" />
	<b><s:text name="jsp.default_533" /></b>
	<s:text name="jsp.default_535" /><b><s:text name="jsp.default_535.1" /></b><br><br>
	<a id="livechat"
		href="https://centerstatesupport.webex.com/sc3200/supportcenter/webacd.do?siteurl=centerstatesupport&WQID=34178a9b0e889f92ab5eba9811057784&siteurl=centerstatesupport&apiname=webacd&servicename=SC&needFilter=false&rnd=5999172211&isurlact=true&entactname=%2Fwebacd.do&entappname=url3200&renewticket=0"
		target="_blanck">
		<s:text name="jsp.default_liveChat" />
	<a>
</sj:dialog>