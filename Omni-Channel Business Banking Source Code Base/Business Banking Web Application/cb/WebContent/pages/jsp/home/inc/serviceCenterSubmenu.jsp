<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%
	Map menuShortcuts = (Map)request.getAttribute("menuShortcuts");
%>
		<div class="submenuHolderBgCls">
				<div class="formActionItemPlaceholder">
					<div class="cardHolder">
						<div class="subMenuCard">
							<span class="cardTitle">_______________</span>
							<div class="cardSummary">
								<span>__________________________<br>__________________________<br>________</span>
							</div>
							<div class="cardLinks">
								<span>___________</span>
								<span>___________</span>
							</div> 
						</div>
					</div>	
				</div> 
				<ul id="tab18" class="mainSubmenuItemsCls">
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SERVICES %>" >
						<li id="menu_serviceCenter_services" menuId="serviceCenter_services">
							<ffi:help id="serviceCenter_services" className="moduleHelpClass"/>
							<s:url id="serviceCenterMenuUrl" value="/pages/jsp/servicecenter/index.jsp" escapeAmp="false">
								<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								<s:param name="Initialize" value="true"/>
								<s:param name="subpage" value="%{'services'}"/>
							</s:url>
							<sj:a
									href="%{serviceCenterMenuUrl}"
									indicator="indicator"
									targets="notes"
									onCompleteTopics="subMenuOnCompleteTopics"
								><s:text name="SERVICES"/></sj:a>
							<span onclick="ns.common.showMenuHelp('menu_serviceCenter_services')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
							<div class="formActionItemHolder">
								<div class="cardHolder">
									<div class="subMenuCard">
										<span class="cardTitle" id="addAccountsLinkId" onclick="ns.common.loadSubmenuSummary('<s:property value="%{serviceCenterMenuUrl}"/>','serviceCenter_services','ServiceCenter');">Services</span>
										<div class="cardSummary">
											<span>This area provides brief information about this feature. Please click on help icon to know more. 
											</span>
										</div>
										<div class="cardLinks borderNone">
										</div>
									</div>
								</div>	
							
								<%-- <span id="addAccountsLinkId" onclick="ns.common.loadSubmenuSummary('<s:property value="%{serviceCenterMenuUrl}"/>','serviceCenter_services','ServiceCenter','addAccountsLink');"><s:text name="jsp.servicecenter_add_bill_pay" /></span>
								<span id="addOnlineAccountsLinkId" onclick="ns.common.loadSubmenuSummary('<s:property value="%{serviceCenterMenuUrl}"/>','serviceCenter_services','ServiceCenter','addOnlineAccountsLink');"><s:text name="jsp.servicecenter_add_online_accounts" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{serviceCenterMenuUrl}"/>','serviceCenter_services','ServiceCenter','orderClearedChecksLink');"><s:text name="jsp.servicecenter_order_copies_of_cleared_checks" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{serviceCenterMenuUrl}"/>','serviceCenter_services','ServiceCenter','orderCopyStatementLink');"><s:text name="jsp.servicecenter_order_statement_copies" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{serviceCenterMenuUrl}"/>','serviceCenter_services','ServiceCenter','orderChecksLink');"><s:text name="jsp.servicecenter_order_checks" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{serviceCenterMenuUrl}"/>','serviceCenter_services','ServiceCenter','orderDepositEnvelopesLink');"><s:text name="jsp.servicecenter_order_deposit_envelopes" /></span> --%>
							</div> 
							<span class="ffiUiIco-menuItem ffiUiIco-menuItem-services"></span>
						</li>
						<s:set var="tempMenuName" value="%{getText('SERVICE_CENTER')}" scope="session" />
						<s:set var="tempSubMenuName" value="%{getText('SERVICES')}" scope="session" />
						<% 
							menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "serviceCenter_services");
						%> 
					</ffi:cinclude>
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FAQ %>" >
						<li id="menu_serviceCenter_faq" menuId="serviceCenter_faq">
						<ffi:help id="serviceCenter_faq" className="moduleHelpClass"/>
						<s:url id="faqMenuUrl" value="/pages/jsp/servicecenter/faq_index.jsp" escapeAmp="false">
							<s:param name="Initialize" value="true"/>
							<s:param name="subpage" value="%{'faq'}"/>
						</s:url>
						<sj:a
								href="%{faqMenuUrl}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="FAQ"/></sj:a>
							<span onclick="ns.common.showMenuHelp('menu_serviceCenter_faq')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>	
							<div class="formActionItemHolder hidden">
								 <div class="cardHolder">
									<div class="subMenuCard">
										<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{faqMenuUrl}"/>','serviceCenter_faq','FAQServiceCenter');"><s:text name="FAQ"/></span>
										<div class="cardSummary">
											<span>This area provides brief information about this feature. Please click on help icon to know more. 
											</span>
										</div>
										<div class="cardLinks borderNone">
										</div>
									</div>
								</div>	
								<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{faqMenuUrl}"/>','serviceCenter_faq','FAQServiceCenter','gettingstarted');"><s:text name="jsp.servicecenter_getting_started" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{faqMenuUrl}"/>','serviceCenter_faq','FAQServiceCenter','accountAccess');"><s:text name="jsp.servicecenter_account_access" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{faqMenuUrl}"/>','serviceCenter_faq','FAQServiceCenter','transferring_funds');"><s:text name="jsp.servicecenter_transferring_funds" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{faqMenuUrl}"/>','serviceCenter_faq','FAQServiceCenter','billPay');"><s:text name="jsp.default_74" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{faqMenuUrl}"/>','serviceCenter_faq','FAQServiceCenter','selfService');"><s:text name="jsp.servicecenter_self_service" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{faqMenuUrl}"/>','serviceCenter_faq','FAQServiceCenter','mMoney');"><s:text name="jsp.servicecenter_quicken_and_microsoft_money" /></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{faqMenuUrl}"/>','serviceCenter_faq','FAQServiceCenter','techSecurity');"><s:text name="jsp.servicecenter_technical_and_security" /></span> --%>
							</div> 
							<span class="ffiUiIco-menuItem ffiUiIco-menuItem-faq"></span>
							
						</li>
						<s:set var="tempMenuName" value="%{getText('SERVICE_CENTER')}" scope="session" />
						<s:set var="tempSubMenuName" value="%{getText('FAQ')}" scope="session" />
						<% 
							menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "serviceCenter_faq");
						%> 
					</ffi:cinclude>
					<%-- <ffi:cinclude ifEntitled="<%= EntitlementsDefines.SESSION_RECEIPT %>" >
						<li id="menu_serviceCenter_session_receipt" menuId="serviceCenter_session_receipt">
						<s:url id="ajax" value="/pages/jsp/servicecenter/session-receipt.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
							<s:param name="subpage" value="%{'audit'}"/>
						</s:url>
						<sj:a
								href="%{ajax}"
								indicator="indicator"
								targets="notes"
								onCompleteTopics="subMenuOnCompleteTopics"
							><s:text name="SESSION_RECEIPT"/></sj:a>
						</li>
						<s:set var="tempMenuName" value="%{getText('SERVICE_CENTER')}" scope="session" />
						<s:set var="tempSubMenuName" value="%{getText('SESSION_RECEIPT')}" scope="session" />
						<% 
							menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "serviceCenter_session_receipt");
						%> 
					</ffi:cinclude> --%>
				</ul>
</div>
