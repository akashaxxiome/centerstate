<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:set name="DataClassificationVariable" value="%{queryDataClass}" scope="session"/>
<% String DataClassification = "";  String DateString= ""; %>
<ffi:getProperty name="${DataClassificationVariable}" assignTo="DataClassification"/>
<ffi:cinclude value1="${AccountDisplayCurrencyCode}" value2="" operator="equals">
	<ffi:setProperty name="AccountDisplayCurrencyCode"  value="${SecureUser.BaseCurrency}"/>  
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.MULTI_CURRENCY_PAYMENTS%>">
	<ffi:setProperty name="showselectcurrency" value="false"/>
</ffi:cinclude>

<script type="text/javascript">
$('#AccountDisplayCurrencyCode').selectmenu({width:'auto'});
$('#AccountDisplayDataClassification').selectmenu({width:'auto'});

function changeAccountDisplayCurrency() {
	var oldURL = $('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('getGridParam', 'url');
	if($('#AccountDisplayCurrencyCode').val()) {
		//var urlString = oldURL + '&currencyType=' + $('#AccountDisplayCurrencyCode').val();
		var urlString = oldURL;
		var currencyTypeVar = $('#AccountDisplayCurrencyCode').val();
		$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {ajaxGridOptions: {type:"POST"}});
		$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {postData: {currencyType:  currencyTypeVar}});		
		$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {url: urlString}).trigger("reloadGrid");
	}
}

function changeAccountDisplayDataClassification() {
	var oldURL = $('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('getGridParam', 'url');
	if($('#AccountDisplayDataClassification').val()) {
		var urlString = '';
		if(oldURL.indexOf('queryDataClass=') == -1 ) {
			if(oldURL.indexOf('?') != -1) {
				urlString = oldURL;
				queryDataClassVar = $('#AccountDisplayDataClassification').val();
				$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {ajaxGridOptions: {type:"POST"}});
				$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {postData: {queryDataClassVar:  queryDataClassVar}});		
				$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {url: urlString}).trigger("reloadGrid");
			} else {
				urlString = oldURL;
				queryDataClassVar = $('#AccountDisplayDataClassification').val();
				$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {ajaxGridOptions: {type:"POST"}});
				$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {postData: {queryDataClassVar:  queryDataClassVar}});		
				$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {url: urlString}).trigger("reloadGrid");
			}
		} else {
			if(oldURL.indexOf('?') != -1) {
				var temp = oldURL.substring(0, oldURL.indexOf('?'));
				urlString = temp + '?';
				var temp1 = oldURL.substring(oldURL.indexOf('?'));
				var params = temp1.split('&');
				if(params && params.length) {
					for(var i=0;i<params.length;i++) {
						var param = params[i];
						
						if(param){
							if(param.indexOf('queryDataClass=') != -1) {
								//urlString += 'queryDataClass=' + $('#AccountDisplayDataClassification').val() + '&';
								var queryDataClassVar = $('#AccountDisplayDataClassification').val() + '&';
								$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {ajaxGridOptions: {type:"POST"}});
								$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {postData: {queryDataClassVar:  queryDataClassVar}});	
								$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {url: urlString}).trigger("reloadGrid");
							} else {
								urlString += param + '&';
								$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {ajaxGridOptions: {type:"POST"}});
								$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {url: urlString}).trigger("reloadGrid");
							}
						}
					}
				}
				urlString = urlString.substring(0, urlString.length - 1);
			} else {
				//urlString = oldURL + '?queryDataClass=' + $('#AccountDisplayDataClassification').val();
				urlString = oldURL;
				var queryDataClassVar = $('#AccountDisplayDataClassification').val();
				$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {ajaxGridOptions: {type:"POST"}});
				$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {postData: {queryDataClassVar:  queryDataClassVar}});		
				$('#<s:property value="portletId"/>_conBal_datagrid').jqGrid('setGridParam', {url: urlString}).trigger("reloadGrid");
			}
		}

	}
}
</script>




<%-- If this is the first visit to the page, --%>
<ffi:cinclude value1="${DataClassification}" value2="" operator="equals">
	<%-- we set DataClassification to its default value - Previous Day --%>
	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>"/>
	<%-- UNLESS we are doing this for Controlled Disubursement, where we would like default Intra Day --%>
	<ffi:cinclude value1="${DataClassificationVariable}" value2="DisbursementDataClassification" operator="equals">
		<%-- but first we'll check if we are even entitled to Intra day! --%>
		<ffi:setProperty name="intraEntitled" value="false"/>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
			<ffi:setProperty name="intraEntitled" value="true"/>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
			<ffi:setProperty name="intraEntitled" value="true"/>
		</ffi:cinclude>
		<%-- and if we are, we will set the default to Intra Day --%>
		<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
	        	<ffi:setProperty name="DataClassification" value="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>"/>
		</ffi:cinclude>
	</ffi:cinclude>
	<ffi:setProperty name="${DataClassificationVariable}" value="${DataClassification}"/>
</ffi:cinclude>

 <ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="notEquals">
	<ffi:object id="CheckPerAccountReportingEntitlements" name="com.ffusion.tasks.accounts.CheckPerAccountReportingEntitlements" scope="request"/>
	<ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>
	<ffi:cinclude value1="" value2="${AccountGroupID}" operator="notEquals">
		<ffi:setProperty name="BankingAccounts" property="Filter" value="ACCOUNTGROUP=${AccountGroupID}" />
	</ffi:cinclude>
	<ffi:setProperty name="CheckPerAccountReportingEntitlements" property="AccountsName" value="${AccountsCollectionName}"/>
	<ffi:process name="CheckPerAccountReportingEntitlements"/>
</ffi:cinclude>
			<table border="0" cellspacing="0" cellpadding="0" class="tableData" style="display:none;">
				<tr>
					<td nowrap><span style="font-weight:bold"><s:text name="jsp.default_354"/></span>&nbsp;&nbsp;</td>
					<td nowrap> 
						<select id="AccountDisplayDataClassification"  onchange="changeAccountDisplayDataClassification()">
 										<ffi:cinclude value1="${AccountsCollectionName}" value2="" operator="equals">								
										<%-- we must determine which data classification the user is entitled to. AccountsCollectionName will be blank for
									       	     Lockbox and Disbursement. --%>
										     	<ffi:removeProperty name="previousEntitled"/>
										     	<ffi:removeProperty name="intraEntitled"/>
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
												<ffi:setProperty name="previousEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
												<ffi:setProperty name="previousEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${previousEntitled}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
											</ffi:cinclude>

											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
												<ffi:setProperty name="intraEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
												<ffi:setProperty name="intraEntitled" value="true"/>
											</ffi:cinclude>
											<ffi:cinclude value1="${intraEntitled}" value2="true" operator="equals">
												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
											</ffi:cinclude>
 										</ffi:cinclude>
 										<ffi:cinclude  value1="${AccountsCollectionName}" value2="" operator="notEquals">
 								 		<%-- we must determine which data classifications to place in this drop down, based on the entiltment checks on the accounts collection AND the data type of this information --%>
 										<ffi:cinclude value1="${DataType}" value2="Summary" operator="equals">
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryPrev}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
 											</ffi:cinclude>
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToSummaryIntra}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
 
 											</ffi:cinclude>
 										</ffi:cinclude>
 										<ffi:cinclude value1="${DataType}" value2="Detail" operator="equals">
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
 											</ffi:cinclude>
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
 
 											</ffi:cinclude>
 										</ffi:cinclude>
 										<ffi:cinclude value1="${DataType}" value2="" operator="equals">
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryPrev}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_PREVIOUSDAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_330"/></option>
 											</ffi:cinclude>
 											<ffi:cinclude value1="${CheckPerAccountReportingEntitlements.EntitledToDetailOrSummaryIntra}" value2="true" operator="equals">
 												<option value='<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>' <ffi:cinclude value1="<%= com.ffusion.tasks.Task.DATA_CLASSIFICATION_INTRADAY%>" value2="${DataClassification}" operator="equals">selected</ffi:cinclude> ><s:text name="jsp.default_252"/></option>
 
 											</ffi:cinclude>
 										</ffi:cinclude>
 										</ffi:cinclude> <%-- End if accounts collection name is empty --%>
									</select>
					</td>
				
					<td>&nbsp;&nbsp;</td>
			
<ffi:cinclude value1="true" value2="${showselectcurrency}" operator="equals" >
		<td align="left"><span style="font-weight:bold"><s:text name="jsp.default_382"/></span>&nbsp;&nbsp;</td>
		<td align="left"> 
			<select id="AccountDisplayCurrencyCode" onchange="changeAccountDisplayCurrency()">
				<option <ffi:cinclude value1='' value2='${AccountDisplayCurrencyCode}' operator='equals'> selected </ffi:cinclude>  value='' >
					<s:text name="jsp.default_127"/>
				</option>
				<ffi:list collection="FOREIGN_EXCHANGE_TARGET_CURRENCIES" items="currency">
				    <option <ffi:cinclude value1='${currency.CurrencyCode}' value2='${AccountDisplayCurrencyCode}' operator='equals'> selected </ffi:cinclude>  value='<ffi:getProperty name="currency" property="CurrencyCode"/>-<ffi:getProperty name="currency" property="Description"/></option>' >
						<ffi:getProperty name="currency" property="Description"/>
					</option>
			    </ffi:list>
			</select>
		</td>
</ffi:cinclude>
</tr>
</table>
<ffi:removeProperty name="AccountDisplayCurrencyCode"/>
<ffi:removeProperty name="DataClassification"/>
<ffi:removeProperty name="DateString"/>
<ffi:removeProperty name="CheckPerAccountReportingEntitlements"/>
