<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<script>
    ns.home.saveTheme = function(){
        var curThemeTitle=null;
        $('#themePickerContainer').find('ul').eq(0).children().each(
        function(){
            if($(this).hasClass('themes_current'))
            {
                curThemeTitle =$(this).find('span').attr('id');
            }
        });

        var urlString = "/cb/pages/jsp/home/manageTheme_saveTheme.action";
        //URL Encryption: Change "get" method to "post" method
        //Edit by Dongning
        $.ajax({
            url: urlString,
            type: "POST",
            async: false,
            global:false,
            data: {themeName: curThemeTitle,CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
            success: function(data){
                //$('body').layout().resizeAll();
				ns.home.userCustomThemeTitle = curThemeTitle;
				$('#themePickerDivID_pane span.ui-icon-disk').parent().hide();
				// Sets the saved Theme ID in a cookie variable 
				ns.common.setCookie("UserTheme", ns.common.getCookie("themeID"));
            }
        });
        $('body').layout().resizeAll();
    };
		
    ns.home.loadTheme = function(){
        var ids = new Array();
        var counter = 0;
        var display;
        var iconUrl;
        var spriteTag;
        var cssUrl;
		var cssPatchUrl;
			
        //URL Encryption Edit by Dongning
        var urlString = '<ffi:urlEncrypt url="/cb/pages/jsp/home/manageTheme_loadTheme.action"/>';
        var userThemeTitle =  null;
        $.ajax({
            url: urlString,
            async: false,
            dataType:'json',
            success: function(data){
                userThemeTitle =  data.themeName;
                ns.home.userCustomThemeTitle = userThemeTitle;
            }
        });
			
        <ffi:list collection="UserThemes" items="theme">
            ids[counter] = '<ffi:getProperty name="theme" property="id"/>';
            display = '<ffi:getProperty name="theme" property="display"/>';
            iconUrl = '<ffi:getProperty name="theme" property="iconUrl"/>';
            spriteTag = iconUrl.substring(0,1);
            if (spriteTag == '#') {
                var spriteNum = iconUrl.substring(1,iconUrl.length);
                iconUrl = spriteNum - 0;
            }
            cssUrl = '<ffi:getProperty name="theme" property="cssUrl"/>';
			cssPatchUrl = '<ffi:getProperty name="theme" property="cssPatchUrl"/>';
            if(userThemeTitle == ids[counter]){
                var cssUrlBase = '/cb/struts/themes/';
                var cssUrlforset = cssUrlBase.concat(cssUrl);
                $.themes._setTheme( ids[counter], display, cssUrlforset, true);
				ns.home.loadThemePatch(cssPatchUrl);
				ns.common.setCookie("UserTheme", ids[counter]);
                return true;
            }
            counter++;
		</ffi:list>
            return false;
        }
		
	ns.home.initThemes = function(){
		//theme chooser
		$.themes.init( { themeBase : '/cb/struts/themes/' } );
		
		var ids = new Array();
		var counter = 0;
		var display;
		var iconUrl;
		var spriteTag;
		var cssUrl;
		var cssPatchUrl;
		<ffi:list collection="UserThemes" items="theme">
			ids[counter] = '<ffi:getProperty name="theme" property="id"/>';
			display = '<ffi:getProperty name="theme" property="display"/>';
			iconUrl = '<ffi:getProperty name="theme" property="iconUrl"/>';
			spriteTag = iconUrl.substring(0,1);
			if (spriteTag == '#') {
				var spriteNum = iconUrl.substring(1,iconUrl.length);
				iconUrl = spriteNum - 0;
			}
			cssUrl = '<ffi:getProperty name="theme" property="cssUrl"/>';
			cssPatchUrl = '<ffi:getProperty name="theme" property="cssPatchUrl"/>';
			//$.themes.addTheme( ids[counter], display, iconUrl, '#', cssUrl );
			$.themes._themes[ids[counter]] = {display: display, icon: iconUrl, preview: spriteTag, url: cssUrl , cssPatchUrl: cssPatchUrl};
			counter++;
		</ffi:list>
				
		$('#themePickerContainer').themes({
			themes : ids
		});
		
		/* We are discontinueing theme picker, so skipping the flow to get user saved theme.
		var loadThemeTag = ns.home.loadTheme();
		if(loadThemeTag == false){
			ns.home.setDefaultTheme();
		}*/
		
		//Load system default theme : 'Dev_Favor'
		ns.home.setDevFavorTheme();
		
	};
	
	ns.home.setDevFavorTheme = function(){
		var defaultThemeName = 'Dev_Favor';
		var ids = new Array();
		var counter = 0;
		var display;
		var iconUrl;
		var spriteTag;
		var cssUrl;
		var cssPatchUrl;
		<ffi:list collection="UserThemes" items="theme">
			ids[counter] = '<ffi:getProperty name="theme" property="id"/>';
			display = '<ffi:getProperty name="theme" property="display"/>';
			iconUrl = '<ffi:getProperty name="theme" property="iconUrl"/>';
			spriteTag = iconUrl.substring(0,1);
			if (spriteTag == '#') {
				var spriteNum = iconUrl.substring(1,iconUrl.length);
				iconUrl = spriteNum - 0;
			}
			cssUrl = '<ffi:getProperty name="theme" property="cssUrl"/>';
			cssPatchUrl = '<ffi:getProperty name="theme" property="cssPatchUrl"/>';
			if(ids[counter] == defaultThemeName){
				var cssUrlBase = '/cb/struts/themes/';
				var cssUrlforset = cssUrlBase.concat(cssUrl);
				$.themes._setTheme( ids[counter], display, cssUrlforset, true);
				ns.home.loadThemePatch(cssPatchUrl);
				return true;
			}
			counter++;
		</ffi:list>
		return false;
	};
	
	
	ns.home.setDefaultTheme = function(){
		var ids = new Array();
		var counter = 0;
		var display;
		var iconUrl;
		var spriteTag;
		var cssUrl;
		var cssPatchUrl;
		<ffi:list collection="UserThemes" items="theme">
			ids[counter] = '<ffi:getProperty name="theme" property="id"/>';
			display = '<ffi:getProperty name="theme" property="display"/>';
			iconUrl = '<ffi:getProperty name="theme" property="iconUrl"/>';
			spriteTag = iconUrl.substring(0,1);
			if (spriteTag == '#') {
				var spriteNum = iconUrl.substring(1,iconUrl.length);
				iconUrl = spriteNum - 0;
			}
			cssUrl = '<ffi:getProperty name="theme" property="cssUrl"/>';
			cssPatchUrl = '<ffi:getProperty name="theme" property="cssPatchUrl"/>';
			if(ids[counter] == '<ffi:getProperty name="themeName"/>'){
				var cssUrlBase = '/cb/struts/themes/';
				var cssUrlforset = cssUrlBase.concat(cssUrl);
				$.themes._setTheme( ids[counter], display, cssUrlforset, true);
				ns.home.loadThemePatch(cssPatchUrl);
				return true;
			}
			counter++;
		</ffi:list>
		return false;
	}
	
	ns.home.userCustomThemeTitle = null; // global variable
	
	ns.home.isThemeChanged = function(){
		var curThemeTitle=null;
		$('#themePickerContainer').find('ul').eq(0).children().each(
			function(){
				if($(this).hasClass('themes_current'))
				{
					curThemeTitle =$(this).find('span').attr('title');
				}
		});
		
		if( (ns.home.userCustomThemeTitle!=null)&&(curThemeTitle!=null)){
			if((ns.home.userCustomThemeTitle!=curThemeTitle)){
				$('#themePickerDivID_pane span.ui-icon-disk').parent().show();
			} else {
				$('#themePickerDivID_pane span.ui-icon-disk').parent().hide();
			}
			ns.home.loadThemePatch($.themes._themes[$.themes.currentTheme].cssPatchUrl);
		}
	};
	
	ns.home.loadThemePatch = function(cssPatch){
		var uipatch = $("link[id=uipatch]");
		var cssUrlBase = '/cb/struts/themes/';
		var cssPatchUrl = cssUrlBase.concat(cssPatch);
		if (!uipatch.attr('href')){
			//create a new link tag
			var styleTag = document.createElement("link");
			styleTag.setAttribute('id', 'uipatch');
			styleTag.setAttribute('type', 'text/css');
			styleTag.setAttribute('rel', 'stylesheet');
			styleTag.setAttribute('href', cssPatchUrl);
			$("head")[0].appendChild(styleTag);
		}else{
			//change the existing tag
			uipatch.attr('href',cssPatchUrl);
			$(window).trigger('resize');
		}
	}
</script>
