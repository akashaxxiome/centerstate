<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<s:url id="ajax" value="%{#session.target}" >
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
</s:url>
<sj:div href="%{ajax}" indicator="indicator"></sj:div>