<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<%
	String sessionID = request.getParameter("SID");
	String locale = request.getParameter("LOC");
	//if (sessionID != null && sessionID.length() > 0) {
		/* session.setAttribute("SID", sessionID); */
		
	//}
	
%>

<%-- <f:view>
<f:loadBundle var="appBundle" basename="templates_#{JSFUtil.applicationName}" /> --%>




<s:i18n name="com.ffusion.struts.i18n.wappush_en_US">
	<html>

<head>
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="viewport" content="width=250, user-scalable=no" />
<title>
	<%-- <h:outputText value="#{appBundle.title_authenticate}"/> --%> <s:property
		value="getText('title_authenticate')" />
</title>
<link rel="stylesheet" type="text/css"
	href="<%= request.getContextPath() %>/css/wap.css" />
<script type="text/javascript"
	src="<%= request.getContextPath() %>/jsp/js/exitFrames.js"></script>
</head>
<body>
	<div align="center">
		<%--  <h:inputHidden id="getAccounts" value="#{UserAction.pushSecretText}"/> --%>

		<s:form id="frmAuth" name="authForm" method="post"
			namespace="/pages/jsp-ns/sms" action="doWapPushAuthenticate"
			autocomplete="OFF">
			<!--  <h:panelGrid columns="1" cellpadding="0" cellspacing="2" style="text-align:center"> -->
			<%--   <h:graphicImage value="#{request.contextPath}/grafx/mBankingLogo.gif" alt="FFI"/> --%>
			<s:property value="getText('msg_outOfBandAuthenticate')" />
			<%-- <h:outputText value="#{appBundle.msg_outOfBandAuthenticate}" styleClass="msg"/> --%>
			<%-- <h:outputText value="#{appBundle.msg_secretText}" styleClass="msg" rendered="#{UserAction.secretText != null}"/> --%>
			<%-- 	<h:outputText value="#{UserAction.secretText}" styleClass="inst" rendered="#{UserAction.secretText != null}"/>
			<f:verbatim><br/></f:verbatim>
			<h:outputText value="#{appBundle.inst_enterBusinessID}" styleClass="inst" rendered="#{UserAction.businessIdRequired == true}"/>
			<t:inputText id="businessCIF" value="#{UserAction.user.groupId}" autocomplete="off" styleClass="input_txt" required="true" size="12" maxlength="32" rendered="#{UserAction.businessIdRequired == true}"/> --%>
			<%-- 	<h:outputText value="#{appBundle.inst_enterPassword}" styleClass="inst"/> --%>
			<s:property value="getText('inst_enterPassword')" />
			<s:password id="password" key="password" required="true" size="20"
				maxlength="20" autocomplete="off"></s:password>
				<div class="errors">
					<s:actionerror />
				</div>
				<input type="hidden" value="<%=sessionID%>" name="sessionId"></input>
				<input type="hidden" value="<%=locale%>" name="locale"></input>

			
			<!-- <h:message for="password" styleClass="error"/> -->
			<%-- <h:commandButton value="#{appBundle.btn_authenticate}" action="#{UserAction.outOfBandAuthenticate}" styleClass="btn"/> --%>
			<!-- 	</h:panelGrid> -->
			<s:submit key="btn_authenticate" name="submit"></s:submit>

		</s:form>


		<%--  <jsp:include page="/ns/inc/footer.jsp"/> --%>
	</div>
</body>
	</html>
</s:i18n>
<!-- </f:view> -->