<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/html"%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">


<s:i18n name="com.ffusion.struts.i18n.wappush_en_US">
<html>

<head>
    <meta name="viewport" content="width=250, user-scalable=no"/>
    <title><s:property value="getText('title_authenticate')" /></title>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/wap.css" />
</head>
<body>
    <div align="center">
       <%--  <h:panelGrid columns="1" cellpadding="0" cellspacing="2" style="text-align:center">
			<h:graphicImage value="#{request.contextPath}/grafx/mBankingLogo.gif" alt="FFI"/> --%>
			<%-- <h:outputText value="#{appBundle.msg_authenticateSuccess}" styleClass="msg"/> --%>
			<s:property value="getText('msg_authenticateSuccess')" />
		<%-- </h:panelGrid> --%>
	   <%-- <jsp:include page="/ns/inc/footer.jsp"/> --%>
	</div>
</body>
</html>
</s:i18n>
