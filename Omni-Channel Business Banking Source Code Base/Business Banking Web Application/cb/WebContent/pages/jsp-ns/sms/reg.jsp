<?xml version="1.0" encoding="UTF-8"?>
<%@ page contentType="text/html"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">

<ffi:setProperty name="BACKURL" value="ns/token.jsp" />

<%
	String sessionID = request.getParameter("SID");
	String locale = request.getParameter("LOC");
	String token = request.getParameter("T");
	String channel= request.getParameter("C");
	if (sessionID != null && sessionID.length() > 0) {
		session.setAttribute("SID", sessionID);
	}
	if (locale != null && locale.length() > 0) {
		com.sybase.mbanking.common.faces.JSFUtil.setLocale(locale);
	}
	if ( token!= null && token.length() > 0) {
		session.setAttribute("TOKEN", token);
	}
	if ( channel != null && channel.length() > 0) {
		session.setAttribute("CHANNEL", channel);
	}
	
	if(sessionID==null || token==null){
		response.sendRedirect("invalid-session.jsp");
	}
%>

<f:view>
<f:loadBundle var="appBundle" basename="templates_#{JSFUtil.applicationName}" />

<html>

<head>
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta name="viewport" content="width=250, user-scalable=no"/>
    <title><h:outputText value="#{appBundle.title_registerDevice}"/></title>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/wap.css" />
</head>
<body>
    <div align="center">
	
       
	        <h:graphicImage value="#{request.contextPath}/grafx/mBankingLogo.gif" alt="FFI"/><br/>
			<h:inputHidden value="#{UserAction.tokenValStatus}"/>
	<h:outputText rendered="#{!UserAction.displayErrorMssg}" value="#{appBundle.msg_deviceRegisterSuccess}"/> <br/><br/>
	<h:outputLink value="#{UserAction.WAPURL}" styleClass="link" rendered="#{!UserAction.displayErrorMssg && UserAction.deviceRegChannel eq 'WAP'}">
					<h:outputText value="#{appBundle.btn_login}" />
			</h:outputLink>
	<h:outputLink value="#{UserAction.smartClientURL}" styleClass="link" rendered="#{!UserAction.displayErrorMssg && UserAction.deviceRegChannel eq 'CSAM'}">
					<h:outputText value="#{appBundle.btn_downloadLink}" />
			</h:outputLink>		
	<h:outputText rendered="#{UserAction.displayErrorMssg}" value="#{appBundle.msg_deviceRegError}"/>
			
	
	
    <jsp:include page="/ns/inc/footer.jsp"/>
	</div>
</body>
</html>
</f:view>