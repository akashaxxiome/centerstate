<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
 <%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="task-redirect.jsp"/>

<%
	String taskNextURL = (String) session.getAttribute(com.ffusion.jtf.JTF.TASK_REDIRECT_URL_SESSION_NAME);
	String servletPathNoNs = (String) session.getAttribute("ServletPathNoNs");
	if( !taskNextURL.startsWith( servletPathNoNs ) ) {
		taskNextURL = servletPathNoNs + taskNextURL;
	}
%>
<ffi:removeProperty name="<%= com.ffusion.jtf.JTF.TASK_REDIRECT_URL_SESSION_NAME %>"/>
<script LANGUAGE="Javascript">
    function setLocation()
    {
		location.replace("<%= taskNextURL %>");
    }
    window.onload=setLocation;
</script>