<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<div style="text-align:left">
<sj:accordion id="accordion" active="false" heightStyle="content">
	<sj:accordionItem title="%{getText('jsp.login_100') + ' ?'}" >
	
<s:text name="jsp.login_59"/>
<hr>
<span class="boldHeading"><s:text name="jsp.login_86"/></span><br>
<s:text name="jsp.login_41"/>
<hr>
<span class="boldHeading"><s:text name="jsp.login_81"/></span><br>
<s:text name="jsp.login_57"/>
<br>
<s:text name="jsp.login_102"/>
 <hr>
<span class="boldHeading"><s:text name="jsp.login_62"/></span><br>
<s:text name="jsp.login_91"/>
 <hr>
<span class="boldHeading"><s:text name="jsp.login_13"/></span><br>
<s:text name="jsp.login_105"/>
 <hr>
<span class="boldHeading"><s:text name="jsp.login_78"/></span><br>
<s:text name="jsp.login_38"/>
 <hr>
<span class="boldHeading"><s:text name="jsp.login_32"/></span><br>
<s:text name="jsp.login_37"/>
 </sj:accordionItem>
 <sj:accordionItem title="%{getText('jsp.login_101') + ' ?'}" cssClass="columndata columndata_grey"> 
<ul>
	 <li>
		<s:text name="jsp.login_7"/>
	</li>
	<li>
		<s:text name="jsp.login_6"/>
	</li>
	<li>
		<s:text name="jsp.login_9"/>
	</li>
	<li>
		<s:text name="jsp.login_53"/>
	</li>
	<li>
		<s:text name="jsp.login_15"/>
	</li>
	<li>
		<s:text name="jsp.login_14"/>
	</li>
</ul>
</sj:accordionItem>
</sj:accordion>
</div>

<style>
	ul li, #accordion * {
		font-size: 13px; line-height: normal; font-family: Arial; font-weight: normal; 
	}
	
	#accordion span[class~="boldHeading"] {
		font-weight: bold !important;
	} 
</style>