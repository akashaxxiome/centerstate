<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<title><s:text name="jsp.login_29" /></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<s:include value="include-theme.jsp" />

<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/FF%{#session.minVersion}.css'/>"/>
<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/login%{#session.minVersion}.css'/>"/>
<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/home/jquery/layout%{#session.minVersion}.css'/>"/>
<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/showLoading%{#session.minVersion}.css'/>" />
<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/desktop%{#session.minVersion}.css'/>" />
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/ui.custom.config%{#session.minVersion}.css'/>"/>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/SAPIconLib%{#session.minVersion}.css'/>"/>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/miscIconSprite%{#session.minVersion}.css'/>"/>

<script type="text/javascript" src="<s:url value='/web/js/jquery.showLoading%{#session.minVersion}.js'/>"></script>
</head>
<body>
	<div id="container" class="desktop">
		<div id="signin_menu">
			<div align="center">
				<div class="ui-state-error ui-corner-all">
					<p>
						<span style="float: left;"></span> 
						<strong><s:text name="jsp.login_5" />
						<ffi:setProperty name="PageHeading" value="ERROR" />
						<% String taskErrorCode; %>
						<ffi:getProperty name="CurrentTask" property="Error"
							assignTo="taskErrorCode" />
						<%
							boolean useAdditionalErrorMessage = com.ffusion.util.CommBankIdentifier.isValidAdditionalServiceCode(taskErrorCode);
						%>
						<ffi:cinclude value1="${CurrentTask.Error}" value2="<%= String.valueOf( com.ffusion.csil.CSILException.ERROR_EXCEED_LIMIT ) %>" operator="equals">
							<script type="text/javascript">
								window.location.href = "<ffi:getProperty name='SecurePath'/> approvals/exceedlimitsnoapproval.jsp";
							</script>
						</ffi:cinclude>
						<%-- ================ MAIN CONTENT START ================ --%>
						<ffi:setProperty name="${touchedvar}" value="false" />
						<%-- ERROR MESSAGE BEGIN --%>
						<%
							if (useAdditionalErrorMessage) {
						%>
						<ffi:setProperty name="ErrorsResource" property="ResourceID" value="ErrorA${CurrentTask.Error}_descr" />
						<%
							} else {
						%>
						<ffi:setProperty name="ErrorsResource" property="ResourceID" value="Error${CurrentTask.Error}_descr" />
						<%
							}
						%>
						<ffi:cinclude value1="${ErrorsResource.Resource}" value2="" operator="equals">
							<s:text name="jsp.default_114" />
						</ffi:cinclude>
						<ffi:cinclude value1="${ErrorsResource.Resource}" value2="" operator="notEquals">
							<ffi:getProperty name="ErrorsResource" property="Resource" />
						</ffi:cinclude>
						<%-- SE_WHY will have a BPW specific error, text gotten from BPW --%>
						<%
							if (session.getAttribute("SE_WHY") != null) {
						%>
						<ffi:getProperty name="SE_WHY" />
						<%
								session.removeAttribute("SE_WHY");
							}
						%>
						</strong>
					</p>
				</div>
				<div align="center" style="padding: 2px 2px 2px 2px;">

					<sj:a id="invalidInputBackButtonID" button="true" onclick="invalidateSession();" tabindex="3">
						<s:text name="jsp.default_57" />
					</sj:a>
					<script type="text/javascript">
						<!--
							function invalidateSession() {
								window.location = "invalidate-session.jsp";
							}
						//-->
					</script>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<s:url value='/web/js/jquery.ui.pane.js'/>"></script>
<script>
		$(document).ready(function() {
			
			$("#signin_menu").pane({
				"title":js_invalid_input_ajax_pane_title,
				"minmax":false,
				"close":false,
			});
			
			jQuery.i18n.properties({
				name: 'LoginJavaScript',
				path:'<s:url value="/web/js/i18n/" />',
				mode:'both',
				language: '<ffi:getProperty name="UserLocale" property="Locale"/>',
				cache:true,
				callback: function(){
				}
			});
		});
	</script>
</html>