<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<div align="center" style="line-height: normal !important;">
	<br/>
	<div class="instructions"><s:text name="jsp.login_110"/></div>
	<br/>
	<div align="center">
		<sj:a id="mustChangePwdConfirmConButtonID" targets="signin_menu" formIds="pswdChangeForm"
			button="true" cssClass="buttonSizeReset"><s:text name="jsp.default_111"/></sj:a>
	</div>
	<s:form id="pswdChangeForm" name="pswdChangeForm" method="post" action="authenticate" theme="simple">
		<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
	</s:form>
</div>
