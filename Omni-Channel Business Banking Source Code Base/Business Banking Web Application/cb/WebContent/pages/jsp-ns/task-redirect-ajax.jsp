<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%-- <ffi:author page="task-redirect.jsp"/>--%>
<%
	String taskNextURL = (String) session.getAttribute(com.ffusion.jtf.JTF.TASK_REDIRECT_URL_SESSION_NAME);
%>

<script type="text/javascript">
$('#login-init-target').html("");
$.post("<%=taskNextURL%>", function(data){
	$('#signin_menu').html(data);
	$('#signin_menu').hideLoading();
});

</script>

<ffi:removeProperty name="<%= com.ffusion.jtf.JTF.TASK_REDIRECT_URL_SESSION_NAME %>"/>