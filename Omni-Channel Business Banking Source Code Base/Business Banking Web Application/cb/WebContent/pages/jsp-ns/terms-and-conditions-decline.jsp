<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	logout = function()
	{
		window.location.href = "/cb/pages/jsp/invalidate-session.jsp";
	}
</script>

<div id="termsAndConditions" class="instructions">
	<form name="frmTermsAndConditionsDeclined" method="post" action="#">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<center>
		<h3><s:text name='termsAndConditions.header'/></h3>
			<h4><s:text name='termsAndConditions.message6'/></h4>
			<p><s:text name='termsAndConditions.message7'/></p>

		<s:url id="ajax" namespace="/pages/jsp-ns" action="termsBack.action">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="request_locale" value="#parameters.request_locale"/>
		</s:url>
		<sj:a id="terms_and_conditions_accept" href="%{ajax}" targets="signin_menu" button="true"><s:text name='jsp.default_57'/></sj:a>
		&nbsp;
		<sj:a id="terms_and_conditions_decline" targets="signin_menu" onclick="logout();" button="true"><s:text name='termsAndConditions.buttonDecline'/></sj:a>
		</center>
	</form>
</div>
