<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="javax.servlet.http.Cookie"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<title><s:text name="jsp.login_29"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<s:include value="include-theme.jsp"/>

	<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/FF.min.css'/>"/>
	<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/login.min.css'/>"/>
	<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/showLoading.min.css'/>"/>
	<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/desktop.min.css'/>"/>
	<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/home/jquery/layout.min.css'/>"/>
	<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/ui.custom.config.min.css'/>"/>
	<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/SAPIconLib%{#session.minVersion}.css'/>"/>
	<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/ubuntu-font-family-0.80/ubuntuFontLib%{#session.minVersion}.css'/>"/>
	<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/miscIconSprite%{#session.minVersion}.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/login_custom.css'/>" />
<style type="text/css">
/*
	Login page background and branding
*/
/*background-image: url("/cb/web/multilang/grafx/loginPageBg.png");
    background-repeat: repeat-x;
    background-size: 100% 100%;*/
.corpLoginPageStyle, .consLoginPageStyle {
    background: #e1e1e1;	
}
.headePane{background: none !important; border:none !important; padding: 0 !important;}
.centerLayoutPane{background: none !important; border:none !important; padding: 0 !important;}
.footerPane{ border:none !important; padding: 0 !important; overflow:hidden !important;}
.instructions{color: rgba(0,156,120,1) !important; font-size:18px !important;} 
.desktop{position: relative; overflow: hidden; width: 80%; height: 100%; margin: 0 auto;}
.passwordEntryPageStyle{background-color: rgba(255, 255, 255, 0.6); border-radius: 5px; height: 80%; margin: 0 auto;  width: 80%; position: relative; display: table;}

  .noAuthTextWrapper {
  	width: 100%;
  	margin:25% auto;
  }
  .noAuthInstructions {
    font-style: normal;
    font-variant: normal;
    font-weight: normal;
    line-height: normal;
    padding: 8px 5px 8px 15px;
    text-transform: none;
    /* line-height: 25em; */
    color: rgba(0,156,120,1) !important;
    font-size: 18px !important;
}
#backRef {
	color: #333A9B;
}

</style>	
	<script type="text/javascript" src="<s:url value='/web/js/jsp-ns/jquery.showLoading.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/web/js/jquery.i18n.properties.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/web/jquery/home/layout.latest.min.js'/>"></script>
	<script type="text/javascript" src="<s:url value='/web/js/jquery.ui.pane.js'/>"></script>
</head>

<%

String userLoginAppType = ""; //request.getParameter("LoginAppType");

String CONST_LOGIN_APP_TYPE = "LoginAppType";
String CONST_DEFAULT_LOGIN_APP_TYPE = "DefaultLoginAppType";

String loginAppTypeFrmCookie = null;

if (session.getAttribute(CONST_LOGIN_APP_TYPE) != null) {
	userLoginAppType = (String) session.getAttribute(CONST_LOGIN_APP_TYPE);
} else if (request.getParameter(CONST_LOGIN_APP_TYPE) != null) {
	userLoginAppType = (String) request.getParameter(CONST_LOGIN_APP_TYPE);
}

Cookie[] cookielist = request.getCookies();
if (cookielist != null) {
	for (int i = 0; i < cookielist.length; i++) {
		if (cookielist[i].getName().equalsIgnoreCase(CONST_DEFAULT_LOGIN_APP_TYPE)){
			loginAppTypeFrmCookie = cookielist[i].getValue();
		}
	}
}

if (StringUtils.isNotBlank(userLoginAppType)) {
	request.setAttribute(CONST_LOGIN_APP_TYPE, userLoginAppType);
} else if (StringUtils.isNotBlank(loginAppTypeFrmCookie)) {
	userLoginAppType = loginAppTypeFrmCookie;
	request.setAttribute(CONST_LOGIN_APP_TYPE, loginAppTypeFrmCookie);
}


%>

<body class="corpLoginPageStyle">
	<%--layout starts --%>
	<div class="ui-layout-north headePane"><s:include value="simple-header.jsp"/></div>
	<div class="ui-layout-center centerLayoutPane">
		<div id="container" class="desktop">
			<div id="signin_menu" class="passwordEntryPageStyle" style="background-color:transparent !important;">
				<div align="center" class="noAuthTextWrapper">
					<div class="noAuthInstructions">
						<!-- <div align="center" style="padding:2px 2px 2px 2px;  line-height: normal !important;"> -->
						 	<s:text name="jsp.login.timedOut"/>
						 	<s:text name="jsp.login_68"/>
						 	<%
						 	if("CB".equalsIgnoreCase(userLoginAppType)){
						 	%>
								<s:url var="loginUrl" value="/pages/jsp-ns/login-corp.jsp" escapeAmp="false">
									<s:param name="request_locale" value="#parameters.request_locale"/>
								</s:url>
								<span onfocus="setFocus()" tabindex="3"><a id="backRef" href="<s:property value='%{loginUrl}'/>" tabindex="3"><s:text name="jsp.login_34"/></a></span>
								<%--<a
									id="backButton"
									button="true"
									href="<s:property value='%{loginUrl}'/>"
									class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
									style="font-size: 13px; padding: 6px;">
										<s:text name="jsp.default_58"/>
										<s:text name="jsp.login_Log_In_Again" /> 
								</a> --%>
							<% }else{  %>
								<s:url var="loginUrl" value="/pages/jsp-ns/login-cons.jsp" escapeAmp="false">
									<s:param name="request_locale" value="#parameters.request_locale"/>
								</s:url>
								<span onfocus="setFocus()" tabindex="3"><a id="backRef" href="<s:property value='%{loginUrl}'/>" tabindex="3"><s:text name="jsp.login_34"/></a></span>
								
								<%-- <a
									id="backButton"
									button="true"
									href="<s:property value='%{loginUrl}'/>"
									class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
									style="font-size: 13px; padding: 6px;">
										<s:text name="jsp.default_58"/>
										<s:text name="jsp.login_Log_In_Again" /> 
								</a> --%>
							<%} %>
							<s:text name="jsp.login_90"/>
						<!-- </div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui-layout-south footerPane">
		<s:include value="footer.jsp" />
	</div> 
	<%--layout starts --%>
</body>
</html>
<script type="text/javascript">
	//This check is added to make sure that this page is shown on the top of all frame structure.
	if (window.parent.location != window.location) {
		if (window.parent.setPageUnloadHandledFlag) {
			window.parent.setPageUnloadHandledFlag();
		}
		window.parent.location = window.location;
	}
	
	$(document).ready(function() {		
		jQuery.i18n.properties({
			name: 'LoginJavaScript',
			path:'<s:url value="/web/js/i18n/" />',
			mode:'both',
			language: '<ffi:getProperty name="UserLocale" property="Locale"/>',
			cache:true,
			callback: function(){
			}
		});
		
		$('body').layout({ 
			applyDefaultStyles: true,
			north:{
				size:100,
				resizable :false,
				closable :false
			},
			south:{
				size:35,
				resizable :false,
				closable :false
			}				
		});
		
	});
</script>