<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>


<ffi:setProperty name="PathExt" value="jsp/" />
<ffi:setProperty name="PathExtNs" value="jsp-ns/" />
<ffi:setProperty name="ImgExt" value="" />
<ffi:setProperty name="PagesPath" value="/pages/jsp/" />

<ffi:cinclude value1="${OBOLocale}" value2="en_US" operator="equals">
	<ffi:setProperty name="PathExt" value="jsp/" />
	<ffi:setProperty name="ImgExt" value="" />
	<ffi:setProperty name="PathExtNs" value="jsp-ns/" />
	<ffi:setProperty name="PagesPath" value="/pages/jsp/" />
	<ffi:setProperty name="ComPathExt" value="jsp/" />
	<ffi:setProperty name="ComImgExt" value="" />
	<ffi:setProperty name="HtmlPath" value="/cb/web/html/"/>
</ffi:cinclude>
<ffi:cinclude value1="${OBOLocale}" value2="en_US" operator="notEquals">
	<ffi:setProperty name="PathExt" value="${OBOLocale}/jsp/" />
	<ffi:setProperty name="ImgExt" value="${OBOLocale}/" />
	<ffi:setProperty name="PathExtNs" value="${OBOLocale}/jsp-ns/" />
	<ffi:setProperty name="PagesPath" value="${OBOLocale}/pages/jsp/" />
	<ffi:setProperty name="ComPathExt" value="${OBOLocale}/jsp/" />
	<ffi:setProperty name="ComImgExt" value="${OBOLocale}/" />	
	<ffi:setProperty name="HtmlPath" value="/cb/web/${OBOLocale}/html/"/>
</ffi:cinclude>

<ffi:setProperty name="ServletPath" value="/pages/${PathExtNs}"/>
<ffi:setProperty name="SecurePath" value="/pages/${PathExt}"/>

<%-- Get the security password generated here, used to secure the password recovery sequence.  --%>
<ffi:object id="GetSecurityKey" name="com.ffusion.tasks.util.GetSecurityKey" scope="session"/>
<ffi:setProperty name="GetSecurityKey" property="KeyLength" value="20"/>
<ffi:process name="GetSecurityKey"/>
<ffi:removeProperty name="GetSecurityKey"/>
	
<ffi:object id="InvalidateSession" name="com.ffusion.tasks.util.InvalidateSession" scope="session"/>

<ffi:setProperty name="InvalidateSession" property="SuccessURL" value="${HtmlPath}logout.html" />

<ffi:object id="Compare" name="com.ffusion.beans.util.StringUtil" scope="session"/>
<ffi:object id="StringUtil" name="com.ffusion.beans.util.StringUtil" scope="session"/>
<ffi:object id="FloatMath" name="com.ffusion.beans.util.FloatMath" scope="session"/>
<ffi:object id="Math" name="com.ffusion.beans.util.IntegerMath" scope="session"/>

<ffi:task errorURL="${PathExtNs}invalid-input.jsp" />
<ffi:task serviceErrorURL="${PathExtNs}process-error.jsp" />
<ffi:task errorRedirectURL="${PathExtNs}error-redirect.jsp" />
<ffi:task taskRedirectURL="${PathExtNs}task-redirect.jsp" />

<ffi:setProperty name="BackURL" value="${ServletPath}invalidate-session.jsp"/>
<ffi:setProperty name="OBONewWindow" value="true"/>

<html>
	<head>
		<title><s:text name="jsp.login_29"/></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<s:include value="include-theme.jsp" />
		<%
			org.apache.struts2.config.DefaultSettings defaultSettings = new org.apache.struts2.config.DefaultSettings(); 
			String devMode = defaultSettings.get("struts.devMode");
			String minVersion = "";
			String minCompressed = "false";
			if("false".equals(devMode)) {
				minVersion = ".min";
				minCompressed = "true";
			}
			session.setAttribute("minCompressed", minCompressed);
			session.setAttribute("minVersion", minVersion);
		%>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/front.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/FF.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/home/jquery/layout.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/desktop.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/showLoading.css'/>"/>

		<script type="text/javascript" src="<s:url value='/web/js/jsp-ns/jquery.showLoading.min.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/web/js/jsp-ns/jquery.pwdstrength.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/web/js/utils.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/web/js/custom.validation.js'/>"></script>

		<style type="text/css">
		.ui-widget-content {
			padding: 0px;
			font-size: 1em
		}
		</style>

		<s:property value="%{request.getHeader['User-Agent']}" />
	</head>
	<body style="font-size: 10px;text-align:center">
		<div id="container">
			<div id="login-init-target" style="width:500;margin:0 auto;margin-top:200px;visibility:hidden"></div>
		</div>

		<div id="hidden"  style="display:none;">
			<s:url id="loginInitUrl" value="/pages/jsp-ns/login-init.jsp">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a id="loginInitLink" href="%{loginInitUrl}" targets="login-init-target" button="true" title="OBO Login"/>
		</div>

		<script type="text/javascript">
			$(document).ready(function() {

				$("#login-init-target").ajaxStart(function(){
					$('#login-init-target').showLoading();
				});
				$("#login-init-target").ajaxStop(function(){
					if(!$('#login-init-target') || !$('#login-init-target').html()) {
						$('#login-init-target').hideLoading();
					}
				});

				$('#loginInitLink').click();
			});
		</script>
	</body>
</html>
