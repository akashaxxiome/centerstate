<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<s:include value="inc/login-pre.jsp" />
<s:set var="LoginAppType" value="%{'EFS'}" scope="session"/>
<s:set var="themeName" value="%{'Dev_Favor'}" scope="session"/>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title><s:text name="jsp.login_29"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<% String timeout = String.valueOf(session.getMaxInactiveInterval()); %>
        <meta http-equiv="refresh" content="<%=timeout%>">
		<s:include value="include-theme.jsp" />

		<%
			org.apache.struts2.config.DefaultSettings defaultSettings = new org.apache.struts2.config.DefaultSettings(); 
			String devMode = defaultSettings.get("struts.devMode");
			String minVersion = "";
			String minCompressed = "false";
			if("false".equals(devMode)) {
				minVersion = ".min";
				minCompressed = "true";
			}
			session.setAttribute("minCompressed", minCompressed);
			session.setAttribute("minVersion", minVersion);
		%>
		
		<%--
			JS and CSS files Inclusion
			NOTE: 
				-To improve page performance the JS and CSS files have been aggreagated during build, please refer to login-page.css and
				login-page.js configuration in pom.xml of war. 
				-Any new css or js file inclusion has to be defined in pom.xml
				-The order of files is important. To resolve dependency files are placed in right order in pom.xml

			Files included in login-page.css
				/web/css/FF.css
				/web/css/login.css
				/web/css/home/jquery/layout.css
				/web/css/showLoading.css
				/web/css/ui.selectmenu.css
				/web/css/ui.custom.config.css
				/web/css/notifications.css
				/web/css/SAPIconLib.css
				/web/css/miscIconSprite.css
				/web/css/ubuntu-font-family-0.80/ubuntuFontLib.css
		--%>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/login-page%{#session.minVersion}.css'/>" />

		<%--			
			Files included in login-page.js
				/web/js/jsp-ns/login.js
				/web/js/jsp-ns/jquery.pwdstrength.js
				/web/js/jquery.showLoading.js
				/web/js/utils.js
				/web/js/custom.validation.js
				/web/js/jquery.ui.selectmenu.js
				/web/js/jquery.ui.pane.js
				/web/js/jquery.browser.js
				/web/jquery/home/layout.latest.js
				/web/js/namespace.js
				/web/js/jquery.i18n.properties.js
				/web/jquery/home/jquery-migrate-1.2.1.js
		--%>
		<script type="text/javascript" src="<s:url value='/web/js/login-page%{#session.minVersion}.js'/>"></script>

		<%-- Added this script explicitly at the top as we want I18N stuff getting loaded first up. --%>
		<script type="text/javascript">
		$(document).ready(function() {
			jQuery.i18n.properties({
				name: 'LoginJavaScript',
				path:'/cb/web/js/i18n/',
				mode:'both',
				language: '<ffi:getProperty name="UserLocale" property="Locale"/>',
				cache:true,
				callback: function(){
				}
			});
		});
		</script>

		<!-- Device specific, do not consider for aggrgation using JAWR for now-->
		<link type="text/css" rel="stylesheet" media="only screen and (min-device-width: 768px) and (max-device-width: 1024px)" href="<s:url value='/web/css/deviceStyle%{#session.minVersion}.css'/>"/>

		
        <style type="text/css">

			<ffi:list collection="GetLanguageList.LanguagesList" items="languageDf">
			body .<ffi:getProperty name="languageDf" property="Language" /> .ui-selectmenu-item-icon {
				height: 20px; width: 30px; margin-top: -13px;
				background: url('/cb/web/multilang/grafx/<ffi:getProperty name="languageDf" property="Language" />_flag.png') 0 0 no-repeat;
			}
			</ffi:list>
			
			/*to remove the gray bar from the top*/
			.ui-layout-resizer {
				position: static !important;
			}
        </style>

        <s:property value="%{request.getHeader['User-Agent']}" />

    </head>
<%
	request.setAttribute("appType", "Consumers");
%>
    <body class="consLoginPageStyle">
		
		
		<div class="ui-layout-north"><s:include value="header.jsp" /></div>
		<div class="ui-layout-center" style="overflow:auto;">
			
			<s:include value="loginDesktop.jsp" />
		</div>	
		<div class="ui-layout-south"><s:include value="footer.jsp" /></div>        
    </body>
	
	<script>
		$(document).ready(function() {
			var config = {
				appType:"<s:property value="%{#session.LoginAppType}"/>",
				themeName:"<s:property value="%{#session.themeName}"/>"
			}
			loginController.init(config);
		});		
	</script>
</html>


<!-- 	
	Sets DefaultLoginAppType name in a cookie variable.
	This theme will be used when the user theme is null or not set.
-->
	
	<script type="text/javascript">
	<!--
	document.cookie='<%="DefaultLoginAppType=" + com.ffusion.util.HTMLUtil.encode("EFS") + ";expires=365 path=/" %>';
	//-->
	</script>		
