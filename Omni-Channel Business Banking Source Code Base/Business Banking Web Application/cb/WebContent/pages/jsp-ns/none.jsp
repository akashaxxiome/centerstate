<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

			<table border="0" cellpadding="0" cellspacing="0" width="750">
				<tr>
					<td colspan="3" class="logoStyle"><img src="/cb/web/multilang/grafx/hdr_fusion_logo.gif" alt=""></td>
				</tr>
				<tr>
					<%-- 1st column --%>
					<%-- 2nd column --%>
					<td valign="bottom" width="10" height="35"><img src="/cb/web/multilang/grafx/hdr_left_end_curve.gif" alt="" width="10" height="35"></td>
					<td valign="bottom" width="730" height="35"><img src="/cb/web/multilang/grafx/hdr_background.gif" alt="" width="730" height="35"></td>
					<td valign="bottom" width="10" height="35"><img src="/cb/web/multilang/grafx/hdr_right_end_curve.gif" alt="" width="10" height="35"></td>
				</tr>
				<tr>
					<td colspan="3" width="750" height="23" valign="bottom">
						<img src="/cb/web/multilang/grafx/hdr_subnav_line.gif" alt="" width="750" height="2">
					</td>
				</tr>
				<tr>
					<%-- 1st column --%>
					<%-- 2nd column --%>
					<td colspan="3" width="750" height="36">
				    <table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
						    <td colspan="3"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="0" height="10"></td>
							</tr>
							<tr>
						    <%-- 1st column --%>
						    <td valign="top" width="25"><img src="/cb/web/multilang/grafx/hdr_title_arrow.gif" alt=""></td>
						    <td valign="top" class="pagehead" align="left"><ffi:getProperty name="navTitle"/></td>
						    <td align="right" valign="middle" class="columndata">
									<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
									<ffi:process name="GetCurrentDate"/>
									<%-- Format and display the current date/time --%>
									<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="M/d/yyyy hh:mm a"/>
									<ffi:getProperty name="GetCurrentDate" property="Date" />

									<ffi:cinclude value1="${showDate}" value2="true" operator="equals">
							    	<span id="date"></span>
									</ffi:cinclude>
									<ffi:cinclude value1="${showDate}" value2="true" operator="notEquals">
							    	&nbsp;
									</ffi:cinclude>
						    </td>
							</tr>
					  </table>
					</td>
				</tr>
			</table>


<ffi:setProperty name="sub2${sub2Menu}" value=""/>