<%@page import="com.ffusion.csil.beans.entitlements.Channel"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<style type="text/css">
#formerrors{font-size:0}
</style>
<%
	String appType = (String)request.getAttribute("appType");
	if (appType == null) {
    appType = (String)session.getAttribute("appType");
    if(appType != null && !appType.equals("CB") && !appType.equals("EFS")){
    	appType = (String)session.getAttribute("appType");	
    }
    session.removeAttribute("appType");
  }
%>
<s:action namespace="/pages/jsp-ns" name="authenticateInit"/>
<s:if test="%{#session.LoginAppType.equals('CB')}">
<%
  request.setAttribute("IsMultiUser", Boolean.toString(com.ffusion.csil.handlers.util.SignonSettings.allowDuplicateUserNames()));
%>
</s:if>
<s:else>
	<%
	  request.setAttribute("IsMultiUser", "false");
	%>
</s:else>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<!-- Adding new css file for customization -->
	<%-- <link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/ui.custom.config%{#session.minVersion}.css'/>"/> --%>
	<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/ui.jqgrid.custom%{#session.minVersion}.css'/>"/>
</head>


<div class="corpLoginPageArea">
	
	
		<div style="height: auto; text-align: center;">

        <s:form
            id="loginFormID"
            name="loginForm" method="post" namespace="/pages/jsp-ns"
            action="getMultifactorInfo" autocomplete="OFF" 
			theme="simple"
            onsubmit="$('#enterSubmitUserName').trigger('click');return false">
            <s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
			<s:hidden name="secureUser.BankID" value="1000"/>
			<input type="hidden" name="secureUser.AppType" value='<%=appType%>'/>
			<s:hidden name="secureUser.locale" value="%{#parameters.request_locale}"/>
			<s:hidden name="AuthType" value="%{@com.ffusion.beans.authentication.AuthConsts@OPERATION_TYPE_LOGIN}"/>
			
			<!--  This is the temporory code to test secureUser.ChannelType -->
			<%--<select name="secureUser.ChannelType">
  				<option value="">Select</option>
  				<option value="<%=Channel.WEB%>">WEB</option>
  				<option value="<%=Channel.MOBILE%>">Mobile</option>
			</select>  --%>

            <div id="loginInputDivID" class="loginPanel">
            	<s:include value="inc/action-error.jsp"/>
                <s:if test="%{#request.IsMultiUser}">
					<div id="customerInputDivID" class="sectionhead" align="center" style="padding:2px; margin-top: 23px;">
                        <%-- <label><span><s:text name="jsp.login_25"/></span><span class="requiredField">*</span></label>
                        <input id="focusObj" class="multiUserInput ui-widget-content ui-corner-all" tabindex="1" name="secureUser.BusinessCustId" size="24" maxlength="32"/> --%>
                        <div class="usernameEntryDivBase">
                       		<div class="customerIconDiv"></div>
                       		<div class="userNameInputFieldDiv">
                       			<!-- <input id="focusObj" type="text" maxlength="32" placeholder="enter your username..." size="24" name="secureUser.UserName" tabindex="1" style="vertical-align:middle;"> -->
                       			<s:textfield id="focusObj" class="multiUserInput eseg" type="text" maxlength="32" placeholder="%{getText('jsp.login_25')}" size="24" name="secureUser.BusinessCustId" tabindex="1" style="vertical-align:middle;" autocorrect="off"  autocapitalize="off" />
                       			<!-- <s:textfield id="focusObj" class="multiUserInput eseg" type="text" maxlength="32" placeholder="User ID" size="24" name="secureUser.BusinessCustId" tabindex="1" style="vertical-align:middle;" autocorrect="off"  autocapitalize="off" />-->
                       		</div>
                       		<div class="ffiUiMiscIco ffiUiIco-misc-submitbtnicon enterButtonDiv" onclick="$('#enterSubmitUserName').trigger('click');" tabindex="2"></div>
                       		<!-- <div class="helpBtnDiv">&nbsp;</div> -->
                       	</div>
                    </div>
                    <div id="userNameInputDivID" class="resetMargin sectionhead">
                        <%-- <label><span><s:text name="jsp.login_92"/></span><span class="requiredField">*</span></label>
                        <s:textfield id="focusObj" type="text" maxlength="32" placeholder="%{getText('jsp.login_92')}" size="24" name="secureUser.UserName" tabindex="1" style="vertical-align:middle;" /> --%>
                        <div class="usernameEntryDivBase">
                       		<div class="ffiUiMiscIco ffiUiIco-misc-usericon userIconDiv"></div>
                       		<div class="userNameInputFieldDiv">
                       			<!-- <input id="focusObj" type="text" maxlength="32" placeholder="enter your username..." size="24" name="secureUser.UserName" tabindex="1" style="vertical-align:middle;"> -->
                       			<s:textfield id="focusObj" type="text" maxlength="32" placeholder="%{getText('jsp.login_92')}" size="24" name="secureUser.UserName" tabindex="1" style="vertical-align:middle;" autocorrect="off"  autocapitalize="off" />
                       		</div>
                       		<div class="enterButtonDiv" onclick="$('#enterSubmitUserName').trigger('click');" tabindex="2">
                       			<span class="ffiUiMiscIco ffiUiIco-misc-submitbtnicon"></span>
                       		</div>
                       		<!-- <div class="helpBtnDiv">&nbsp;</div> -->
                       	</div>
					 </div>
                </s:if>
                <s:else>
                    <s:hidden name="CustomerID" value="uselessCustID" />
                    <div id="userNameInputDivID" class="sectionhead" style="float:left; width: 100%;">
                        <%-- <label><span class=""><s:text name="jsp.login_92"/>&nbsp;&nbsp;</span><span class="requiredField">*</span></label>
                        <input  id="focusObj" type="text" maxlength="32" size="24" name="secureUser.UserName" class="ui-widget-content ui-corner-all"  tabindex="1" style="vertical-align:middle;"> --%>
                       	<div class="usernameEntryDivBase">
                       		<div class="ffiUiMiscIco ffiUiIco-misc-usericon userIconDiv"></div>
                       		<div class="userNameInputFieldDiv">
                       			<!-- <input id="focusObj" type="text" maxlength="32" placeholder="enter your username..." size="24" name="secureUser.UserName" tabindex="1" style="vertical-align:middle;"> -->
                       			<!--USE CASE OCB_000001 -->
                       			<!--<s:textfield id="focusObj" type="text" maxlength="32" placeholder="%{getText('jsp.login_92')}" size="24" name="secureUser.UserName" tabindex="1" style="vertical-align:middle;" autocorrect="off"  autocapitalize="off" />-->
                       			<s:textfield id="focusObj" type="text" maxlength="32" placeholder="%{getText('jsp.login_116_Custom_UserID')}" size="24" name="secureUser.UserName" tabindex="1" style="vertical-align:middle;" autocorrect="off"  autocapitalize="off" />
                       			<!--USE CASE OCB_000001 -->
                       		</div>
                       		
                       		<div class="enterButtonDiv" onclick="$('#enterSubmitUserName').trigger('click');" tabindex="2">
                       			<span class="ffiUiMiscIco ffiUiIco-misc-submitbtnicon"></span>
                       		</div>
                       		<!-- <div class="helpBtnDiv">&nbsp;</div> -->
                       	</div> 
					
					    <!--USE CASE OCB_000001 -->	
					    <!--<div id="loginAlertInfoDivID_Custom" style="text-align:left; clear:left; line-height: normal !important; float:left; width: 100%">-->
					    	<div id="loginAlertInfoDivID_Custom" class="accessRestrictCls" style="align=center; width:92%">
                        		<s:text name="jsp.login_Custom_Not_Case_Sensitive" />
					    	</div>
					    <!--USE CASE OCB_000001 -->
					</div>
						
					
                </s:else>
                
                <div id="customerErrorDivID" style="text-align:center;clear:left;">
                        <span id="secureUser.businessCustIdError"></span>
                    </div>
                <div id="userNameErrorDivID" style="text-align:center; clear:left; line-height: normal !important; float:left; width: 100%">
                    <span id="secureUser.userNameError" class="requiredField">&nbsp;</span>
                </div>
                <div class="loginLinkCls">
                	<sj:a href="javascipt:void(0)" id="" formIds="LookupForm" targets="signin_menu" indicator="indicator" tabindex="2" onclick="$('#security').dialog('open');">
                		<s:text name="jsp.login_Security_Tips"/>
                	</sj:a>
                	
                	<sj:a href="javascipt:void(0)" id="forgotPwdLink" cssClass="hidden" formIds="LookupForm" targets="signin_menu" indicator="indicator" tabindex="2">
                		Forgot Password
                	</sj:a>
                	
                	<%-- <sj:a
						id="contactUsButton"
						onclick="$('#contactUs').dialog('open');"
						indicator="indicator"
						href="#">
						<s:text  name="contactUs"/>
					</sj:a> --%>
                </div>
                <%-- <div class="forgotPassLinkCls" style="float:left; width: 100%">
                	<sj:a href="#" id="ajaxformlink" formIds="LookupForm" targets="signin_menu" indicator="indicator" tabindex="2" onSuccessTopics="forgotpasswordStep1Success">
                		<s:text name="jsp.login_31"/>
                	</sj:a>
                </div> --%>
				 <div align="center" style="padding:7px; display: none;">
                        <sj:submit id="enterSubmitUserName"
                              formIds="loginFormID"
                              button="true" 
                              targets="signin_menu"
                              validate="true"
                              validateFunction="customValidation"
                              onkeydown="submitForm(event)"
                              indicator="indicator"
                              onBeforeTopics="onClickSubmitButton,resetUserName"
							  effect="highlight"
							  effectOptions="{ color : '#E8F4FD' }" 
							  tabIndex="-1"
							  value="%{getText('jsp.login_26')}"
							  cssClass="buttonSizeReset" />
                   </div>
            		
            </div>
        </s:form>
    <%
            request.removeAttribute("IsMultiUser");
    %>
    <s:url id="ajax" value="inc/security_info.jsp" />
    <s:set var="tmpI18nStr" value="%{getText('jsp.default_102')}" />
    <sj:dialog id="security" position="['center',50]" cssStyle="display:none; float:left;"
               hideEffect="fade" title="%{getText('jsp.login_83')}" autoOpen="false"
               showEffect="fade" width="700" modal="true" resizable="false"
               buttons="{'%{tmpI18nStr}': function() { $(this).dialog('close'); } }"
	>
        <s:include value="%{ajax}" />
    </sj:dialog>
			
</div>

</div>



		<%-- <div id="loginAlertInfoDivID" class="accessRestrictCls">
			<span><s:text name="jsp.login_56"/></span>
		</div> --%>
					
                   

					<%-- <div style="text-align: right;">
						<br>
						<span class="requiredField">* <s:text name="jsp.default_240"/></span>
					</div> --%>

<!--USE CASE OCB_000001 -->
<!--<div id="loginAlertInfoDivID" class="accessRestrictCls">
	<span><s:text name="jsp.login_56"/></span>
</div>-->

<div id="loginAlertInfoDivID" class="accessRestrictCls">
	<span><s:text name="jsp.login.117_Custom_Support_Msg"/></span>

</div>



<!--<div id="globalmessage" class="loginGlobalMessage homePageGlobalMessages">
    <ffi:setProperty name='msgType' value='<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.MSG_TYPE_LOGIN ) %>' />
	<ffi:setProperty name='BackGround' value="greenClayBackground" />
	<s:action namespace="/pages/common" name="GlobalMessageAction_getLoginGlobalMessages" executeResult="true">			
		<s:param name="msgType" value="@com.ffusion.beans.messages.GlobalMessageConsts@MSG_TYPE_LOGIN"></s:param>
	</s:action>
</div>-->

<!--USE CASE OCB_000001 -->

<s:form id="LookupForm" name="LookupForm" method="post" action="lookup" namespace="/pages/jsp-ns" cssStyle="display:none">
	<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
</s:form>
	
<script>
	$(document).ready(function(){
		$("body").addClass("loginPageBg");
		$(".pageWrapper").css("width","100%");
		$("#loginPageHolderDiv").removeClass("passwordEntryPageStyle");
		
		$("#loginPageHolderDiv").css('height', '100%');
		totalWindowHeightAvailable = $(document).height();
		var headerHeight = 100;
		var FooterHeight = 35;
		totalAvailablePaneHeight = totalWindowHeightAvailable - headerHeight -  FooterHeight;
		loginPageHolderHeight = totalAvailablePaneHeight - 135 - 30; // 120 is globalmessage and 30 is extra header height
		$("#loginPageHolderDiv").find(".corpLoginPageArea").css('height', loginPageHolderHeight+"px");
		//$(".corpLoginPageArea > div").css('height', loginPageHolderHeight);
		$("#loginPageHolderDiv").find($(".corpLoginPageArea > div")).css('line-height', loginPageHolderHeight+'px');
		//alert(loginPageHolderHeight)
		$("#channel").selectmenu({width:100});
		if(loginController){
			loginController.changeLoginPanelTitle(js_login_step1);
		}
		setTimeout("$('#focusObj').focus();", 500);
		
		/* $("#focusObj").focus(function() {
			$(".helpBtnDiv").fadeIn("slow")
		}).focusout(function() {
			$(".helpBtnDiv").fadeOut("slow")
		}); */
		
		$(".enterButtonDiv").on("keypress", function(event) {
			if(event.keyCode == 13) {
				$("#enterSubmitUserName").trigger("click");
			} else {
				// not enter key					
			}
		});
		
		$("#contactUsLink").click(function(){
			alert("contact us");
		});
		
		// Released under MIT license: http://www.opensource.org/licenses/mit-license.php
		 
		/* $('[placeholder]').focus(function() {
		var input = $(this);
		if (input.val() == input.attr('placeholder')) {
			input.val('');
			input.removeClass('placeholder');
		}
		}).blur(function() {
		var input = $(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
			input.addClass('placeholder');
			input.val(input.attr('placeholder'));
		}
		}).blur().parents('form').submit(function() {
		$(this).find('[placeholder]').each(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
			input.val('');
		}
		})
		}); */
		
		 // Checks browser support for the placeholder attribute
	    jQuery(function() {
	    jQuery.support.placeholder = false;
	    test = document.createElement('input');
	    if('placeholder' in test) jQuery.support.placeholder = true;
	    });

	    // Placeholder for IE
	    $(function () {
	        if(!$.support.placeholder) { 

	            var active = document.activeElement;
	            $(':text, textarea').focus(function () {
	                if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
	                    $(this).val('').removeClass('hasPlaceholder');
	                }
	            }).blur(function () {
	                if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
	                    $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
	                }
	            });
	            $(':text, textarea').blur();
	            $(active).focus();
	            $('form').submit(function () {
	                $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
	            });
	        }  

	    });
		
	    
	});
	
	function submitForm(event){
		// if enter then only submit
		if(event.keyCode=='13'){
			$('#loginFormID').submit();
		}
	}
	
	$( window ).resize(function() {
		totalWindowHeightAvailable = $(document).height();
		var headerHeight = 100;
		var FooterHeight = 35;
		totalAvailablePaneHeight = totalWindowHeightAvailable - headerHeight -  FooterHeight;
		loginPageHolderHeight = totalAvailablePaneHeight - 135 - 45; // 120 is globalmessage and 45 is accessRestrictCls
		$("#loginPageHolderDiv").find(".corpLoginPageArea").css('height', loginPageHolderHeight+"px");
		//$(".corpLoginPageArea > div").css('height', loginPageHolderHeight);
		$("#loginPageHolderDiv").find($(".corpLoginPageArea > div")).css('line-height', loginPageHolderHeight+'px');
	});
</script>