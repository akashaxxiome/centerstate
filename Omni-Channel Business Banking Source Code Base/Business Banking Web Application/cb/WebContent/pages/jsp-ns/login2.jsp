<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<script type="text/javascript">
	<s:if test="%{#session.isMSIE}">document.getElementById("password").focus();
	</s:if>
	$(document).ready(function()
	{
		loginController.changeLoginPanelTitle(js_login_step2);
		$("#password").trigger('focus');
		
		$("#loginPageHolderDiv").removeClass().addClass("passwordEntryPageStyle");
		
		$("#loginBackButtonID").click(function() {
			$("#loginPageHolderDiv").removeClass().addClass("corpLoginPageArea globalMsgShadow");
		})
		
		$(".enterButtonDiv, #password").on("keypress", function(event) {
			if(event.keyCode == 13) {
				$("#loginLoginForm").trigger("click");
			} else {
				// not enter key					
			}
		});
	});
</script>

<!-- Adding new css file for customization -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
	<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/ui.custom.config%{#session.minVersion}.css'/>"/>
	<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/ui.jqgrid.custom%{#session.minVersion}.css'/>"/>
	
	<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/ubuntu-font-family-0.80/ubuntuFontLib%{#session.minVersion}.css'/>"/>
	
</head>
<body>

<s:if test='%{#session.CustomerID.equals("uselessCustID")}'>
	<s:set var="CustomerID" value="" scope="session"/>
</s:if>

<s:include value="inc/login2-pre.jsp"/>

<ffi:setProperty name="BackAction" value="userName"/>

<s:include value="inc/action-error.jsp"/>

<s:set var="secret" value="0" scope="request"/>
<s:iterator value="#session.SecureUser.multifactorInfo" var="next">
	<s:if test="#next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_SECRET_URL">
		<s:if test="#request.secret == 0">
			<div class="informationField">
				<%-- <img class="loginSecureImg" align="left" src="<s:url value='/web'/>/grafx/<ffi:cinclude value1='${UserLocale.Locale}' value2='en_US' operator='notEquals'><ffi:getProperty name='UserLocale' property='Locale'/>/</ffi:cinclude>icons/infoMsgIcon.png"> --%>
				<span class="ffiUiMiscIco ffiUiIco-misc-infomsgicon"></span>
				<!--<span><s:text name="jsp.login_16"/></span>-->
					<span><s:text name="jsp.login.118_Custom_Password_Msg"/></span>
			</div>
			<s:set var="secret" value="1" scope="request"/>
		</s:if>
		<div align="center" style="line-height: normal !important;" class="userSecureImgWrapper">
			<img class="userSecureImg loginSecureImgShadow" src="<s:url value='/'/><s:property value="#next.challenge" escape="false"/>" alt="<s:text name="jsp.login_80"/>"/>
		</div>
	</s:if>
</s:iterator>
<div id="passwordStep">
	<s:form id="loginFormID1" name="loginForm1" method="post" action="authenticate" namespace="/pages/jsp-ns" theme="simple" onsubmit="$('#loginLoginForm').trigger('click');return false"  autocomplete="OFF">
		<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
		 
		<div align="center">
			
						<div class="usernameEntryDivBase">
							<div class="userIconDiv">
								<span class=" ffiUiMiscIco ffiUiIco-misc-lockicon"></span>
							</div>
							<div class="userNameInputFieldDiv">
								<s:password tabindex="1" name="secureUser.Password" id="password" size="20" maxlength="20" value="" placeHolder="Enter your password here..."/>
								<input class="hidden" type="password" />
							</div>
							<div class="enterButtonDiv" onclick="$('#loginLoginForm').trigger('click');" tabindex="2">
								<span class="ffiUiMiscIco ffiUiIco-misc-submitbtnicon"></span>
							</div>
	                       	<div class="helpBtnDiv">&nbsp;</div>
	                    </div>
	                    <span id="secureUser.passwordError"></span>
				<s:iterator value="#session.SecureUser.multifactorInfo" var="next" status="status">
					<s:if test="#next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_QUESTION || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_TOKEN || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_SCRATCH_CARD || #next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_OTP">
						<s:set var="tempType" value="#next.type" scope="session"/>
							<div style="word-wrap:break-word;" class="sectionhead">
								<label for="secureUser.MultifactorInfo[<s:property value='#status.index'/>].response"><s:text name="%{@com.ffusion.beans.authentication.AuthConsts@CREDENTIAL_FIELD_NAME_PREFIX}%{#next.type}"/>:</label><span class="requiredField">*</span>
								<s:property value="#next.challenge" escape="false"/>
							</div>
							<div class="usernameEntryDivBase">
								<div class="usernameEntryDivBase">
									<div class="userIconDiv"></div>
									<div class="userNameInputFieldDiv">
		                       			<input class="credentialUserInput" id="secureUser.MultifactorInfo[<s:property value="#status.index"/>].response" name="secureUser.MultifactorInfo[<s:property value="#status.index"/>].response" size="20" maxlength="50" required="true" tabindex="2" type="password" />
		                       		</div>
		                       		<div class="enterButtonDiv" onclick="" tabindex="2">
		                       			<span class="ffiUiMiscIco ffiUiIco-misc-submitbtnicon"></span>
		                       		</div>
	                       			<div class="helpBtnDiv">&nbsp;</div>	
	                       		</div>
                       		</div>
					</s:if>
					<s:if test="#next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_QUESTION">
						<span id="secureUser.multifactorInfo.challengequestionError"></span>
					</s:if>
					<s:if test="#next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_TOKEN">
						<span id="secureUser.multifactorInfo.tokenError"></span>
					</s:if>
					<s:if test="#next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_SCRATCH_CARD">
						<span id="secureUser.multifactorInfo.scratchcardError"></span>
					</s:if>
					<s:if test="#next.type == @com.sap.banking.authentication.beans.MultifactorInfo@TYPE_OTP">
						<span id="secureUser.multifactorInfo.otpError"></span>
					</s:if>
				</s:iterator>
				<span id="secureUser.multifactorInfoError" style="margin-top: 48px;"></span>
			<script>
				$(".credentialUserInput:last").keypress(function(e)
				{
					var key = e.keyCode ? e.keyCode : e.which;
					// if the key pressed is the enter key
					if (key == 13) {
						$("#loginLoginForm").focus();
						$('#loginLoginForm').trigger('click');
					}
				});

				// Triggers "click" event when the LOGIN button has got focus
				// and "Enter" key is hit on the keyboard
				$("#loginLoginForm").keypress(function(event)
				{
					if (event.which == 13) {
						event.preventDefault();
						$('#loginLoginForm').trigger('click');
					}
				});
			</script>
			<div class="loginLinkCls" style="margin: 0 0 10px;">
				<s:url id="ajax" namespace="/pages/jsp-ns" action="userName.action">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				<sj:a href="javascipt:void(0)" id="" formIds="LookupForm" targets="signin_menu" indicator="indicator" tabindex="2" onclick="$('#security').dialog('open');">
                		<s:text name="jsp.login_Security_Tips"/></sj:a>
				<sj:a href="#" id="ajaxformlink" formIds="LookupForm" targets="signin_menu"
					  indicator="indicator" tabindex="2" onSuccessTopics="forgotpasswordStep1Success"><s:text name="jsp.login_31"/></sj:a>
				<sj:a id="loginBackButtonID" href="%{ajax}" targets="signin_menu" tabindex="3" onSuccessTopics="goBackToLoginStep1"><s:text name="jsp.default_58"/></sj:a>
				<sj:a id="loginLoginForm" cssStyle="display:none;" formIds="loginFormID1" button="true" targets="signin_menu" onBeforeTopics="removeValidationErrors" validate="true" validateFunction="customValidation" tabindex="4" cssClass="buttonSizeReset"><s:text name="jsp.login_44"/></sj:a>
			</div>

			<%-- <div id="login2ButtonDivID">
				<s:url id="ajax" namespace="/pages/jsp-ns" action="userName.action">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				<sj:a id="loginBackButtonID" href="%{ajax}" targets="signin_menu" button="true" tabindex="3" onSuccessTopics="goBackToLoginStep1" cssClass="buttonSizeReset"><s:text name="jsp.default_58"/></sj:a>
				<sj:a id="loginLoginForm" cssStyle="display:none;" formIds="loginFormID1" button="true" targets="signin_menu" onBeforeTopics="removeValidationErrors" validate="true" validateFunction="customValidation" tabindex="4" cssClass="buttonSizeReset"><s:text name="jsp.login_44"/></sj:a>
			</div> --%>

			<%-- <div style="text-align: right;">
				<span class="requiredField">* <s:text name="jsp.default_240"/></span>
			</div> --%>
		</div>
	</s:form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		totalWindowHeightAvailable = $(document).height();
		$(".passwordEntryPageStyle").css('max-height', totalWindowHeightAvailable);
		$(".userSecureImg").css('width', '220px');
		$(".userSecureImg").css('height', 'auto');
		$(".userSecureImg").bind('load', function() {
			var secureImageHeight = $(this).outerHeight(true);
			console.log(secureImageHeight)
			var totalHeight = $(".informationField").outerHeight(true) + secureImageHeight +  $("#passwordStep").outerHeight(true) + 50;
			$("#loginPageHolderDiv.passwordEntryPageStyle").css('height', totalHeight+"px");
		  });
		
		 // Checks browser support for the placeholder attribute
	    jQuery(function() {
	    jQuery.support.placeholder = false;
	    test = document.createElement('input');
	    if('placeholder' in test) jQuery.support.placeholder = true;
	    });

	    // Placeholder for IE
	    $(function () {
	        if(!$.support.placeholder) { 

	            var active = document.activeElement;
	            $(':text, :password').focus(function () {
	                if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
	                    $(this).val('').removeClass('hasPlaceholder');
	                }
	            }).blur(function () {
	                if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
	                    $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
	                }
	            });
	            $(':text, :password').blur();
	            $(active).focus();
	            $('form').submit(function () {
	                $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
	            });
	        }  

	    });
	    
	});
	

	
</script>
<s:form id="LookupForm" name="LookupForm" method="post" action="lookup" namespace="/pages/jsp-ns" cssStyle="display:none">
	<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
</s:form>