<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object id="AccountTypeTable" name="com.ffusion.beans.util.StringTable" scope="session"/>
<ffi:setProperty name="AccountTypeTable" property="Update" value="0,set,Unknown" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="1,set,Checking" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="2,set,Savings" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="3,set,Credit Card" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="4,set,Loan" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="5,set,Mortgage" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="6,set,Home Equity" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="7,set,Line of Credit" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="8,set,CD" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="9,set,IRA" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="10,set,Stock" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="11,set,Brokerage" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="12,set,Money Market" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="13,set,Business Loan" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="14,set,Fixed Deposit" />
<ffi:setProperty name="AccountTypeTable" property="Update" value="15,set,Other" />

<ffi:object id="TransactionTypeTable" name="com.ffusion.beans.util.StringTable" scope="session"/>
<ffi:setProperty name="TransactionTypeTable" property="Update" value="0,set,Unknown" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="1,set,Deposit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="2,set,Withdrawal" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="3,set,Check" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="4,set,Credit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="5,set,Debit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="6,set,ATM Credit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="7,set,ATM Debit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="8,set,POS Credit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="9,set,POS Debit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="10,set,Fee" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="11,set,Interest" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="12,set,Direct Debit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="13,set,Recurring Payment" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="14,set,Unpaid Item" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="15,set,Advice" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="16,set,Transfer" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="17,set,Sup List" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="18,set,Dividend" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="19,set,Bank Giro Credit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="20,set,Remittance" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="21,set,Reversal" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="22,set,Finance Charge" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="23,set,Service Charge" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="24,set,Interest Paid" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="25,set,Bill Payment" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="26,set,Buy" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="27,set,Sell" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="28,set,Cash" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="29,set,Direct Deposit" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="30,set,Other" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="31,set,Wire Transfer" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="32,set,ACH Transfer" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="33,set,Wire Transfer In" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="34,set,ACH Transfer In" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="35,set,NSF( Insufficient Funds )" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="36,set,FX" />
<ffi:setProperty name="TransactionTypeTable" property="Update" value="37,set,TAX" />
