<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object id="GetNameConvention" name="com.ffusion.tasks.util.GetNameConvention"/>
<ffi:process name="GetNameConvention" />
<ffi:removeProperty name="GetNameConvention" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<center>
   <ffi:setProperty name="showDate" value="false"/>
   <s:set var="tmpI18nStr" value="%{getText('jsp.default_335')}" scope="request" /><ffi:setProperty name="navTitle" value="${tmpI18nStr}"/>
   <ffi:include page="${PathExtNs}blank-nav.jsp" />
   <ffi:removeProperty name="navTitle"/>
   <ffi:removeProperty name="showDate"/>
</center>

<center>
<!--table width="741" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td height="53" align="left" valign="bottom"><img src="/cb/web/multilang/grafx/hdr_fusion_logo.gif" /><table cellpadding="0" cellspacing="0" border="0" background="">
				<tr>
					<td colspan="4" align="right">
					</td>
					<td>&nbsp;&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td height="3"><img src="/cb/web/multilang/grafx/spacer.gif" width="7" height="3"></td></tr>
	<tr> 
		<td class="topnav_row1" height="7"><img src="/cb/web/multilang/grafx/spacer.gif" width="7" height="7"></td>
	</tr>
	<tr> 
		<td class="topnav_row2_bg" width="85%">&nbsp;</td>
	</tr>
	<tr valign="middle"> 
		<td class="topnav_row3_act">&nbsp;</td>
	</tr>
</table-->

<%-- ================ MAIN CONTENT START ================ --%>
<br>
<center>
<span class="sectionhead"><s:text name="jsp.login_77"/></span>
</center>

<%-- ================= MAIN CONTENT END ================= --%>

<ffi:include page="${PathExtNs}inc/nav/footer.jsp" />

<SCRIPT LANGUAGE="JavaScript">
    function setLocation()
    {
       location.replace("<ffi:getProperty name="ServletPath"/><ffi:getProperty name="target"/>");
   }
    window.onload=setLocation;
</SCRIPT>
