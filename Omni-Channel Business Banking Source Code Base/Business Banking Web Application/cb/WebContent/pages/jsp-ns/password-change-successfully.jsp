<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
	
<style type="text/css">
/*
	background-image: url("/cb/web/multilang/grafx/loginPageBg.png");
    background-repeat: repeat-x;
    background-size: 100% 100%;
*/
.corpLoginPageStyle, .consLoginPageStyle {
    background: #e1e1e1;
}
.messageWrapper{display: table; height:100%; width:100%}
.messageWrapper > div{display: table-cell; vertical-align: middle;}
</style>	
	
</head>
<body>
<div class="messageWrapper">
	<div align="center" >
						<s:if test='%{"Business".equals(#session.LoginAppType)}'>
							<p class="instructions"><s:text name="jsp.login_110" /></p>
							<s:url var="loginUrl" value="/pages/jsp-ns/login-corp.jsp" escapeAmp="false">
								<s:param name="request_locale" value="#parameters.request_locale"/>
							</s:url>
							<a
								id="backButton"
								button="true"
								href="<s:property value='%{loginUrl}'/>"
								class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
								style="font-size: 13px; padding: 6px;">
									<%-- <s:text name="jsp.default_58"/> --%>
									<s:text name="jsp.default_111" /> 
							</a>
						</s:if>
						<s:else>
							<p class="instructions"><s:text name="jsp.login_110" /></p>
							<s:url var="loginUrl" value="/pages/jsp-ns/login-cons.jsp" escapeAmp="false">
								<s:param name="request_locale" value="#parameters.request_locale"/>
							</s:url>
							<a
								id="backButton"
								button="true"
								href="<s:property value='%{loginUrl}'/>"
								class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"
								style="font-size: 13px; padding: 6px;">
									<%-- <s:text name="jsp.default_58"/> --%>
									<s:text name="jsp.default_111" /> 
							</a>
						</s:else>
	</div>		
</div>	
</body>
</html>

<script type="text/javascript">
	//This check is added to make sure that this page is shown on the top of all frame structure.
	if (window.parent.location != window.location) {
		if (window.parent.setPageUnloadHandledFlag) {
			window.parent.setPageUnloadHandledFlag();
		}
		window.parent.location = window.location;
	}
	
	$(document).ready(function() {		
		jQuery.i18n.properties({
			name: 'LoginJavaScript',
			path:'<s:url value="/web/js/i18n/" />',
			mode:'both',
			language: '<ffi:getProperty name="UserLocale" property="Locale"/>',
			cache:true,
			callback: function(){
			}
		});
		
		/* $("#signin_menu").pane({
			"title":js_bank_title,
			"minmax":false,
			"close":false
		}); */
		
		$('body').layout({ 
			applyDefaultStyles: true,
			north:{
				size:100,
				resizable :false,
				closable :false
			},
			south:{
				size:35,
				resizable :false,
				closable :false
			}				
		});
		
	});
</script>