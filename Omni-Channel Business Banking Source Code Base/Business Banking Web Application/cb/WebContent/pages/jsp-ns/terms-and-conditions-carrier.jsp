<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="terms-and-conditions.jsp"/>

<div id="carrierTermsAndConditions" align="center" class="instructions">
	<form name="frmCarrierTermsAndConditions" method="post">
		<center>		
			<h2><ffi:getProperty name='CarrierTC' property='CarrierName'/></h2>
			<h3><s:text name="carrierTermsAndConditions.header"/></h3>
			<h4><s:text name="carrierTermsAndConditions.message1"/></h4>
			<p><s:text name="carrierTermsAndConditions.message2"/></p>
			<p><s:text name="carrierTermsAndConditions.message3"/></p>
			<p><s:text name="carrierTermsAndConditions.message4"/></p>
			<p><s:text name="carrierTermsAndConditions.message5"/></p>

			<s:url id="ajax" namespace="/pages/jsp-ns" action="carrierTermsAndConditions_decline.action" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="carrierId" value="#request.CarrierTC.CarrierId"></s:param>
				<s:param name="request_locale" value="#parameters.request_locale"/>
			</s:url>
			<sj:a id="carrier_terms_and_conditions_decline" href="%{ajax}" targets="signin_menu" button="true"><s:text name='termsAndConditions.buttonDecline'/></sj:a>
			&nbsp;
			<s:url id="ajax" namespace="/pages/jsp-ns" action="carrierTermsAndConditions_accept.action" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="carrierId" value="#request.CarrierTC.CarrierId"></s:param>
				<s:param name="request_locale" value="#parameters.request_locale"/>
			</s:url>
			<sj:a id="carrier_terms_and_conditions_accept" href="%{ajax}" targets="signin_menu" button="true"><s:text name='termsAndConditions.buttonAccept'/></sj:a>
		</center>
	</form>
</div >
