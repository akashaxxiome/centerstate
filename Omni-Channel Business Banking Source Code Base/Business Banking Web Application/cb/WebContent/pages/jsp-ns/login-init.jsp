<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
				 java.util.Iterator,
				 com.ffusion.beans.accounts.Accounts,
				 com.ffusion.beans.accounts.Account"%>
<%@ page import="java.util.Calendar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<script type="text/javascript">
			//this try catch block is added in order to suppress "$ is not defined" js error
			try {
				$('#login-init-target').append("placeholder");
			} catch (e) {
			    //console.log (e.message);
			}

		</script>
	</head>
	<body>
<s:if test='%{#parameters.Password[0]!=null && !"".equals(#parameters.Password[0].trim())}'>
	<s:set var="Password" value="#parameters.Password[0]" scope="session" />
</s:if>

<%
	String v130_4a = "";
	String v130_2b = "";
	String dateFormat = "";
	java.util.Enumeration enum1 = request.getParameterNames();
	while(enum1.hasMoreElements()) {
		String key = (String)enum1.nextElement();
		if(key != null && key.startsWith(com.ffusion.tasks.authentication.Task.CREDENTIAL_RESPONSE_PREFIX)) {
			session.setAttribute(key, request.getParameter(key));
		}
	}
%>

<ffi:getProperty name="UserLocale" property="DateFormat" assignTo="dateFormat" />

<%-- To get to non secure 404 page, PathExt was changed to point to jsp-ns folder.
     This is now changed back to the jsp folder
--%>

	<ffi:setProperty name="PathExt" value="jsp/" />
    <ffi:setProperty name="PathExtNs" value="jsp-ns/" />
    <ffi:setProperty name="ImgExt" value="" />
    <ffi:setProperty name="PagesPath" value="/pages/jsp/" />

<s:set name="HelpPath" scope="session" ><s:url value="/" />web/help/</s:set>
<s:set name="HtmlPath" scope="session" ><s:url value="/" />pages/html/</s:set>
<s:set name="FullPagesPath" scope="session" ><s:url value="/" />pages/jsp/</s:set>


<ffi:object id="GetNameConvention" name="com.ffusion.tasks.util.GetNameConvention"/>
	<ffi:process name="GetNameConvention" />
<ffi:removeProperty name="GetNameConvention" />

<ffi:setProperty name="SecurePath" value="/pages/${PathExt}"/>
<ffi:setProperty name="SecureServletPath" value="/pages/"/>
<ffi:setProperty name="ServletPath" value="/pages/${PathExtNs}"/>
<ffi:setProperty name="ServletPathNoNs" value="/pages/"/>


<ffi:setProperty name="<%= com.ffusion.tasks.reporting.GenerateReportBase.RPT_STYLESHEET_NAME %>" value="FF.css"/>

<ffi:setProperty name="BankId" value="1000"/>
<ffi:setProperty name="product" value="FusionBank"/>
<ffi:setProperty name="AssistancePhoneNumber" value="1-800-888-8888"/>

<ffi:setProperty name="selectedtrue" value="selected"/>
<ffi:setProperty name="selectedfalse" value=""/>
<ffi:setProperty name="checkedtrue" value="checked"/>
<ffi:setProperty name="checkedfalse" value=""/>
<ffi:setProperty name="disabledtrue" value="disabled"/>
<ffi:setProperty name="disabledfalse" value=""/>
<ffi:setProperty name="account-agg_init_touched" value="false"/>
<ffi:setProperty name="banking_init_touched" value="false"/>
<ffi:setProperty name="billpresentment_init_touched" value="false"/>
<ffi:setProperty name="messaging_init_touched" value="false"/>
<ffi:setProperty name="payments_init_touched" value="false"/>
<ffi:setProperty name="portal_init_touched" value="false"/>
<ffi:setProperty name="stoppayments_init_touched" value="false"/>
<ffi:setProperty name="wires_init_touched" value="false"/>
<ffi:setProperty name="alerts_init_touched" value="false"/>
<ffi:setProperty name="register_init_touched" value="false"/>
<ffi:setProperty name="menu_init_touched" value="false"/>
<ffi:setProperty name="register_init_touched" value="false"/>
<ffi:setProperty name="positivefalse" value="red"/>
<ffi:setProperty name="positivetrue" value="black"/>
<ffi:setProperty name="negativetrue" value="red"/>
<ffi:setProperty name="negativefalse" value="black"/>

<%--ffi:setProperty name="BackURL" value="${ServletPath}InvalidateSession"/
<ffi:setProperty name="BackURL" value="<s:url value='/pages'/>/${PathExtNs}login.jsp?locale=${UserLocale.Locale}" URLEncrypt="true" />
--%>
<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="session"/>
   <ffi:setProperty name="GetCurrentDate" property="Locale" value="${UserLocale.Locale}" />
   <ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateFormat}" />
<ffi:process name="GetCurrentDate"/>

<ffi:object id="EntitlementsSignOff" name="com.ffusion.efs.tasks.entitlements.SignOff" scope="session"/>
<ffi:process name="EntitlementsSignOff"/>

<ffi:object id="PositivePaySignOff" name="com.ffusion.tasks.positivepay.SignOff" scope="session"/>
<ffi:process name="PositivePaySignOff"/>

<ffi:object id="ReversePositivePaySignOff" name="com.ffusion.tasks.reversepositivepay.SignOff" scope="session"/>
<ffi:process name="ReversePositivePaySignOff"/>

<ffi:object id="BillPaySignOff" name="com.ffusion.tasks.billpay.SignOff" scope="session"/>
<ffi:process name="BillPaySignOff"/>

<ffi:object id="BankingSignOff" name="com.ffusion.tasks.banking.SignOff" scope="session"/>
<ffi:process name="BankingSignOff"/>
<%--
<ffi:object id="StateList" name="com.ffusion.tasks.util.ResourceList" scope="session"/>
    <ffi:setProperty name="StateList" property="ResourceFilename" value="com.ffusion.utilresources.states" />
    <ffi:setProperty name="StateList" property="ResourceID" value="StateAbbr" />
<ffi:process name="StateList"/>
--%>
<ffi:object id="ServiceErrorsResource" name="com.ffusion.tasks.util.ServiceErrorsResource" scope="session"/>
<ffi:process name="ServiceErrorsResource"/>

<ffi:object id="ErrorsResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="ErrorsResource" property="ResourceFilename" value="com.ffusion.tasksresources.errors" />
<ffi:process name="ErrorsResource"/>

<ffi:object id="AccountTypes" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="AccountTypes" property="ResourceFilename" value="com.ffusion.beansresources.accounts.resources" />
<ffi:process name="AccountTypes"/>

<ffi:object id="CurrencyDesc" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="CurrencyDesc" property="ResourceFilename" value="com.ffusion.beansresources.fx.resources" />
<ffi:process name="CurrencyDesc"/>

<ffi:object id="UserResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="UserResource" property="ResourceFilename" value="<%= com.ffusion.beans.user.UserLocale.RESOURCEBUNDLE %>"/>
<ffi:process name="UserResource"/>

<ffi:object id="ENTResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="ENTResource" property="ResourceFilename" value="com.ffusion.utilresources.logging.audit.autoentitleadmin.autoentitleadmin" />
<ffi:process name="ENTResource"/>

<s:include value="initStringTables.jsp" />

<ffi:object id="URLEncode" name="com.ffusion.beans.util.URLEncode" scope="session"/>

<ffi:setProperty name="login" value="live"/>


<ffi:setProperty name="SecureUser" property="DateFormat" value="${UserLocale.DateFormat}" />
<ffi:task errorURL="${PathExtNs}invalid-input.jsp" />
<ffi:task serviceErrorURL="${PathExtNs}process-error-ajax.jsp" />
<ffi:task errorRedirectURL="${PathExtNs}error-redirect.jsp" />
<ffi:task taskRedirectURL="${PathExtNs}task-redirect-ajax.jsp" />

<ffi:object name="com.ffusion.beans.DateTime" id="<%= com.ffusion.tasks.Task.SESSION_LOGIN_TIME %>" />
<%
        // QTS 767108: Sometimes not all of the transactions get posted to the Audit Log after this time, so
        // subtract 5 seconds to make sure everything will show up on the Session Activity Receipt
	    com.ffusion.beans.DateTime date = (com.ffusion.beans.DateTime)session.getAttribute(com.ffusion.tasks.Task.SESSION_LOGIN_TIME);
        date.add(java.util.Calendar.SECOND, -5);
%>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
<ffi:setProperty name="ID" value="${User.Id}"/>

<%--<ffi:object id="EntitlementsGetEntitlements" name="com.ffusion.efs.tasks.entitlements.GetEntitlements" scope="session"/>
    <ffi:setProperty name="EntitlementsGetEntitlements" property="UserId" value="${ID}" />
<ffi:process name="EntitlementsGetEntitlements" /> --%>
<ffi:object id="BankingSignOn" name="com.ffusion.tasks.banking.SignOn" scope="session"/>
    <ffi:setProperty name="BankingSignOn" property="InvalidPasswordURL" value="${PathExt}invalid-input.jsp" />
    <ffi:setProperty name="BankingSignOn" property="MustChangePasswordURL" value="${PathExtNs}online/must-changepassword.jsp" />
    <ffi:setProperty name="BankingSignOn" property="UserName" value="${Business.BusinessName}" />
    <ffi:setProperty name="BankingSignOn" property="Password" value="${Business.BusinessCIF}" />
    <ffi:setProperty name="BankingSignOn" property="ServiceName" value="${BankingInitialize.ServiceName}" />
<ffi:process name="BankingSignOn" />
<%-- need to recreate object so that the already processed flag gets cleared, sign on Banking object --%>
<ffi:object id="BankingSignOn" name="com.ffusion.tasks.banking.SignOn" scope="session"/>
    <ffi:setProperty name="BankingSignOn" property="InvalidPasswordURL" value="${PathExt}invalid-input.jsp" />
    <ffi:setProperty name="BankingSignOn" property="MustChangePasswordURL" value="${PathExtNs}online/must-changepassword.jsp" />
    <ffi:setProperty name="BankingSignOn" property="UserName" value="${Business.BusinessName}" />
    <ffi:setProperty name="BankingSignOn" property="Password" value="${Business.BusinessCIF}" />
    <ffi:setProperty name="BankingSignOn" property="ServiceName" value="com.ffusion.services.banking.interfaces.BankingService" />
<ffi:process name="BankingSignOn" />

<ffi:object name="com.ffusion.tasks.accounts.SignOn" id="AccountsSignOn" scope="session"/>
    <ffi:setProperty name="AccountsSignOn" property="UserName" value="${ID}"/>
<ffi:process name="AccountsSignOn"/>

<ffi:object id="GetFixedDepositInstruments" name="com.ffusion.tasks.banking.GetFixedDepositInstruments" scope="session"/>
    <ffi:setProperty name="GetFixedDepositInstruments" property="AccountName" value="Account"/>
    <ffi:setProperty name="GetFixedDepositInstruments" property="InstrumentsName" value="FixedDepositInstruments"/>
<ffi:object id="GetFixedDepositInstrument" name="com.ffusion.tasks.banking.GetFixedDepositInstrument" scope="session"/>
    <ffi:setProperty name="GetFixedDepositInstrument" property="InstrumentsName" value="FixedDepositInstruments"/>
    <ffi:setProperty name="GetFixedDepositInstrument" property="InstrumentName" value="FixedDepositInstrument"/>
<ffi:object id="UpdateFixedDepositInstrument" name="com.ffusion.tasks.banking.UpdateFixedDepositInstrument" scope="session"/>
    <ffi:setProperty name="GetFixedDepositInstrument" property="InstrumentName" value="FixedDepositInstrument"/>

<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" scope="session"/>
    <ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
    <ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
<ffi:object id="SetSettlementAccount" name="com.ffusion.tasks.accounts.SetAccount" scope="session"/>
    <ffi:setProperty name="SetSettlementAccount" property="AccountsName" value="BankingAccounts"/>
    <ffi:setProperty name="SetSettlementAccount" property="AccountName" value="SettlementAccount"/>

<ffi:object id="GetFDInstrumentTransactions" name="com.ffusion.tasks.banking.GetFDInstrumentTransactions" scope="session"/>
    <ffi:setProperty name="GetFDInstrumentTransactions" property="InstrumentName" value="FixedDepositInstrument"/>
    <ffi:setProperty name="GetFDInstrumentTransactions" property="TransactionsName" value="Transactions"/>
    <ffi:setProperty name="GetFDInstrumentTransactions" property="DateFormat" value="${UserLocale.DateFormat}"/>

<ffi:object id="SetTransaction" name="com.ffusion.tasks.banking.SetTransaction" scope="session"/>
    <ffi:setProperty name="SetTransaction" property="TransactionsName" value="Transactions"/>
    <ffi:setProperty name="SetTransaction" property="TransactionName" value="Transaction"/>

<ffi:object id="ExportHistory" name="com.ffusion.tasks.banking.ExportHistory" scope="session"/>
    <ffi:setProperty name="ExportHistory" property="AccountName" value="Account"/>
    <ffi:setProperty name="ExportHistory" property="DateFormat" value="${UserLocale.DateFormat}"/>

<ffi:setProperty name="HomeBackURL" value="${SecurePath}index.jsp"/>
<ffi:object id="PortalSuccessURLs" name="com.ffusion.beans.util.StringList" scope="session"/>
<ffi:setProperty name="PortalSuccessURLs" property="Add" value="${SecurePath}index.jsp" />

<ffi:object id="BuildPropertyLocaleHashmaps" name="com.ffusion.efs.tasks.entitlements.BuildPropertyLocaleHashmaps" scope="session"/>
	<ffi:setProperty name="BuildPropertyLocaleHashmaps" property="PropertyName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
	<ffi:setProperty name="BuildPropertyLocaleHashmaps" property="PropertyName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_COMPONENT %>"/>
<ffi:process name="BuildPropertyLocaleHashmaps"/>

<ffi:object id="IsApproverObj" name="com.ffusion.tasks.approvals.IsApprover" scope="session"/>
<ffi:process name="IsApproverObj"/>

</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:object id="SetAccount" name="com.ffusion.tasks.accounts.SetAccount" scope="session"/>
		<ffi:setProperty name="SetAccount" property="AccountsName" value="BankingAccounts"/>
		<ffi:setProperty name="SetAccount" property="AccountName" value="Account"/>
	<ffi:object id="SetSettlementAccount" name="com.ffusion.tasks.accounts.SetAccount" scope="session"/>
		<ffi:setProperty name="SetSettlementAccount" property="AccountsName" value="BankingAccounts"/>
		<ffi:setProperty name="SetSettlementAccount" property="AccountName" value="SettlementAccount"/>

	<ffi:setProperty name="ID" value="${User.Id}"/>
	<ffi:cinclude value1="${User.CustomerType}" value2="1" operator="equals">
		<ffi:object id="BuildPropertyLocaleHashmaps" name="com.ffusion.efs.tasks.entitlements.BuildPropertyLocaleHashmaps" scope="session"/>
			<ffi:setProperty name="BuildPropertyLocaleHashmaps" property="PropertyName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_DISPLAY_NAME %>"/>
			<ffi:setProperty name="BuildPropertyLocaleHashmaps" property="PropertyName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_PROPERTY_COMPONENT %>"/>
		<ffi:process name="BuildPropertyLocaleHashmaps"/>

		<ffi:object name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroupPropertyValues" id="GetEntitlementGroupPropertyValues" scope="session" />
		<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="PropertyName" value="<%= com.ffusion.csil.core.common.EntitlementsDefines.TYPE_GROUP_PROPERTY_SMALL_BIZ %>"/>
		<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="CheckParents" value="true"/>
		<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="DefaultPropertyValue" value="false"/>
		<ffi:setProperty name="GetEntitlementGroupPropertyValues" property="EntGroupId" value="${SecureUser.EntitlementID}" />
		<ffi:process name="GetEntitlementGroupPropertyValues" />
		<ffi:cinclude value1="${GetEntitlementGroupPropertyValues.PropertyValue}" value2="false">
			<%-- need to log out because corporate user (NOT SMALL BUSINESS USER) has logged in. Change the USERNAME because
			we don't want the lockout to happen if a corporate banking customer tries to log in. --%>
			<ffi:setProperty name="GetUser" property="UserName" value="XXXXzzzzYY" />
			<ffi:setProperty name="GetUser" property="Password" value="YYYYYYYY12" />

			<%-- do not validate the password, as failed login attempts should be logged for security and thus must
				 pass beyond the task level --%>
			<ffi:setProperty name="GetUser" property="Validate" value="USERNAME" />
			<ffi:setProperty name="GetUser" property="Authenticate" value="true" />
			<ffi:process name="GetUser" />
		</ffi:cinclude>

	</ffi:cinclude>
	<ffi:cinclude value1="${User.CustomerType}" value2="1" operator="notEquals">
		<ffi:setProperty name="TransPassword" value="${User.PROCESSOR_PIN}"/>
		<ffi:setProperty name="TransID" value="${User.CustId}"/>
	</ffi:cinclude>

	<%-- Check if this is a primary user --%>
	<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="equals">
		<ffi:object id="BankingSignOn" name="com.ffusion.tasks.banking.SignOn" scope="request"/>
			<ffi:setProperty name="BankingSignOn" property="InvalidPasswordURL" value="${PathExtNs}process-error.jsp" />
			<ffi:setProperty name="BankingSignOn" property="MustChangePasswordURL" value="${PathExtNs}online/must-changepassword.jsp" />
			<ffi:cinclude value1="${User.CustomerType}" value2="1" operator="notEquals">
				<ffi:setProperty name="BankingSignOn" property="UserName" value="${TransID}" />
				<ffi:setProperty name="BankingSignOn" property="Password" value="${TransPassword}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${User.CustomerType}" value2="1" operator="equals">
				<ffi:setProperty name="BankingSignOn" property="UserName" value="${Business.BusinessName}" />
				<ffi:setProperty name="BankingSignOn" property="Password" value="${Business.BusinessCIF}" />
			</ffi:cinclude>
		<ffi:process name="BankingSignOn" />
	</ffi:cinclude>

<%-- Check Terms and Conditions Acceptance ends --%>
	<ffi:cinclude value1="${User.CustomerType}" value2="1" operator="notEquals">
		<ffi:setProperty name="SkipRestOfPage" value="false"/>

		<%-- Check if primary user is active --%>
		<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="notEquals">
			<ffi:object name="com.ffusion.tasks.user.GetUserById" id="GetUserById" scope="request"/>
				<ffi:setProperty name="GetUserById" property="ProfileId" value="${SecureUser.PrimaryUserID}"/>
				<ffi:setProperty name="GetUserById" property="UserSessionName" value="PrimaryUser"/>
			<ffi:process name="GetUserById"/>
			<%-- added User object to session --%>

			<ffi:cinclude value1="${PrimaryUser.AccountStatus}" value2="<%= com.ffusion.beans.user.UserDefines.ACTIVE %>" operator="notEquals">
				<ffi:setProperty name="NextPage" value="${ServletPath}inactive.jsp"/>
				<ffi:setProperty name="SkipRestOfPage" value="true"/>
			</ffi:cinclude>
			<%--<ffi:removeProperty name="PrimaryUser"/>--%>
		</ffi:cinclude>

		<ffi:cinclude value1="${SkipRestOfPage}" value2="true" operator="notEquals">
			<ffi:object name="com.ffusion.tasks.accounts.SignOn" id="AccountsSignOn" scope="request"/>
				<ffi:setProperty name="AccountsSignOn" property="UserName" value="${ID}"/>
			<ffi:process name="AccountsSignOn"/>

			<%-- Check if this is a secondary user --%>
			<ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="notEquals">
				<ffi:object id="BankingSignOn" name="com.ffusion.tasks.banking.SignOn" scope="request"/>
					<ffi:setProperty name="BankingSignOn" property="InvalidPasswordURL" value="${PathExtNs}process-error.jsp" />
					<ffi:setProperty name="BankingSignOn" property="MustChangePasswordURL" value="${PathExtNs}online/must-changepassword.jsp" />
					<ffi:setProperty name="BankingSignOn" property="UserName" value="${SecureUser.PrimaryUserCustID}" />
					<ffi:setProperty name="BankingSignOn" property="Password" value="${SecureUser.primaryUserProcessorPin}" />
				<ffi:process name="BankingSignOn" />
			</ffi:cinclude>

		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>
<s:action namespace="/pages/jsp/account" name="getBankingAccounts" executeResult="true"/>
<%-- Get user's affiliate bank --%>
<ffi:object name="com.ffusion.tasks.affiliatebank.GetAffiliateBankByID" id="GetAffiliateBankByID" scope="request"/>
	<ffi:setProperty name="GetAffiliateBankByID" property="AffiliateBankID" value="${SecureUser.AffiliateID}"/>
<ffi:process name="GetAffiliateBankByID"/>

<%-- Get the user alerts delivery settings --%>
<ffi:object id="GetAlertSettings" name="com.ffusion.tasks.user.GetUserAlertSettings" scope="session"/>
<ffi:process name="GetAlertSettings"/>

<%-- Get the user session settings --%>
<ffi:object id="GetSessionSettings" name="com.ffusion.tasks.user.GetSessionSettings" scope="session"/>
<ffi:process name="GetSessionSettings"/>

<ffi:object id="DisplayImage" name="com.ffusion.tasks.reporting.DisplayImage"/>

<ffi:object id="GetTabsSettings" name="com.ffusion.tasks.util.GetTabsSettings"/>
<ffi:process name="GetTabsSettings"/>

<ffi:object id="GetGridSettings" name="com.ffusion.tasks.util.GetGridSettings"/>
<ffi:process name="GetGridSettings"/>


<s:action namespace="/pages/jsp/ach" name="achVariablesAction" executeResult="true"/>

<%-- initialize enumeration type data --%>
<s:include value="/pages/jsp/inc/init-enumeration.jsp" />

<ffi:setProperty name="band1" value="class=\"ltrow\""/>
<ffi:setProperty name="band2" value="class=\"dkrow\""/>


    <%-- WARNING!!! init-session.jsp invalidates the existing session and creates a new session for session fixation security issue --%>
    <%-- Struts does not like the session being changed out from under it. No Struts stuff that depends on session properties will work below this include --%>
  <%--  <s:include value="/pages/jsp/init-session.jsp"/> --%>
    <%-- lock-session MUST be after init-session because init-session invalidates the session and will remove the lock that lock-session created --%>
    <%-- for proper error display, lock-session MUST use ffi:include --%>
   <%-- <ffi:include page="/pages/jsp/lock-session.jsp"/> --%>
     <s:include value="/pages/jsp/start-page.jsp"/>

</body>
</html>