<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:cinclude value1="${useBCsettings}" value2="true" operator="equals">
	<ffi:setProperty name="numDigits" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getBCMinPasswordDigits() ) %>"/>
	<ffi:setProperty name="numLetters" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getBCMinPasswordLetters() ) %>"/>
	<ffi:setProperty name="numUppercase" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getBCMinPasswordUpperCaseLetters() ) %>"/>
	<ffi:setProperty name="minLength" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getBCMinPasswordLength() ) %>"/>
	<ffi:setProperty name="maxLength" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getBCMaxPasswordLength() ) %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${useBCsettings}" value2="true" operator="notEquals">
	<ffi:setProperty name="numDigits" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getMinPasswordDigits() ) %>"/>
	<ffi:setProperty name="numLetters" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getMinPasswordLetters() ) %>"/>
	<ffi:setProperty name="numUppercase" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getMinPasswordUpperCaseLetters() ) %>"/>
	<ffi:setProperty name="minLength" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getMinPasswordLength() ) %>"/>
	<ffi:setProperty name="maxLength" value="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getMaxPasswordLength() ) %>"/>
</ffi:cinclude>

<ffi:cinclude value1="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getPasswordAllowSpecialChars() ) %>" value2="false" operator="equals">
	<ffi:getL10NString rsrcFile="common" msgKey="PasswordStrengthInstructionNoSpecialChar" parm0="${minLength}" parm1="${maxLength}" parm2="${numDigits}" parm3="${numLetters}"  parm4="${numUppercase}" />
</ffi:cinclude>
<ffi:cinclude value1="<%= String.valueOf( com.ffusion.csil.handlers.util.SignonSettings.getPasswordAllowSpecialChars() ) %>" value2="false" operator="notEquals">
	<ffi:getL10NString rsrcFile="common" msgKey="PasswordStrengthInstructionAllowSpecialChar" parm0="${minLength}" parm1="${maxLength}" parm2="${numDigits}" parm3="${numLetters}"  parm4="${numUppercase}" />
</ffi:cinclude>

<ffi:removeProperty name="numDigits"/>
<ffi:removeProperty name="numLetters"/>
<ffi:removeProperty name="numUppercase"/>
<ffi:removeProperty name="minLength"/>
<ffi:removeProperty name="maxLength"/>
