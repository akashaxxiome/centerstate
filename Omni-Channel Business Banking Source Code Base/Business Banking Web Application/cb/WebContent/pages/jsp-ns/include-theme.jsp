<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@page import="javax.servlet.http.Cookie"%>

<%
	String CONST_THEME_NAME = "themeName";
	String CONST_USER_THEME = "UserTheme";
	String CONST_DEFAULT_THEME = "DefaultTheme";

	String theme = null;
	String defaultTheme = null;

	if (session.getAttribute(CONST_THEME_NAME) != null) {
		theme = (String) session.getAttribute(CONST_THEME_NAME);
	} else if (request.getParameter(CONST_THEME_NAME) != null) {
		theme = (String) request.getParameter(CONST_THEME_NAME);
	}
	Cookie[] cookielist = request.getCookies();
	if (cookielist != null) {
		for (int i = 0; i < cookielist.length; i++) {
			if (cookielist[i].getName().equalsIgnoreCase(
					CONST_USER_THEME)) {
				theme = cookielist[i].getValue();
			} else if (cookielist[i].getName().equalsIgnoreCase(
					CONST_DEFAULT_THEME)) {
				defaultTheme = cookielist[i].getValue();
			}
		}
	}
	if (theme != null) {
		request.setAttribute(CONST_THEME_NAME, theme);
	} else if (defaultTheme != null) {
		request.setAttribute(CONST_THEME_NAME, defaultTheme);
	}
%>
<s:url id="head" value="/web/css" />
<sj:head defaultIndicator="indicator" compressed="true"
	jquerytheme="Dev_Favor" customBasepath="%{head}"
	jqueryui="true" loadAtOnce="true" loadFromGoogle="%{google}"
	ajaxhistory="%{ajaxhistory}" />
<%
	/**
	Sets default theme name in a cookie variable.
	This theme will be used when the user theme is null or not set.
	 */
	if (theme != null && defaultTheme != theme) {
%>
	<script type="text/javascript">
	<!--
	document.cookie='<%=CONST_DEFAULT_THEME + "=" + com.ffusion.util.HTMLUtil.encode(theme) + ";expires=365 path=/" %>';
	//-->
	</script>		
<%	
	}
%>