<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<s:include value="inc/login-pre.jsp" />
<s:set var="LoginAppType" value="%{'OneApp'}" scope="session"/>
<s:set var="themeName" value="%{'Dev_Favor'}" scope="session"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title><s:text name="jsp.login_29"/></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<s:include value="include-theme.jsp" />	

		<%
			org.apache.struts2.config.DefaultSettings defaultSettings = new org.apache.struts2.config.DefaultSettings(); 
			String devMode = defaultSettings.get("struts.devMode");		
			String minVersion = "";
			String minCompressed = "false";
			if("false".equals(devMode)) {
				minVersion = ".min";
				minCompressed = "true";
			}			
			session.setAttribute("minCompressed", minCompressed);
			session.setAttribute("minVersion", "");
		%>

		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/FF%{#session.minVersion}.css'/>" />
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/login%{#session.minVersion}.css'/>" />
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/home/jquery/layout%{#session.minVersion}.css'/>" />
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/ui.selectmenu%{#session.minVersion}.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/ui.custom.config%{#session.minVersion}.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/loginPage%{#session.minVersion}.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/showLoading%{#session.minVersion}.css'/>" />
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/notifications%{#session.minVersion}.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/SAPIconLib%{#session.minVersion}.css'/>"/>
		<link type="text/css" rel="stylesheet" media="all" href="<s:url value='/web/css/miscIconSprite%{#session.minVersion}.css'/>"/>
		
		<script type="text/javascript" src="<s:url value='/web/js/jsp-ns/login%{#session.minVersion}.js'/>" ></script> 
		<script type="text/javascript" src="<s:url value='/struts/js/base/jquery-ui.js'/>"></script>
        <script type="text/javascript" src="<s:url value='/web/js/jquery.showLoading%{#session.minVersion}.js'/>" ></script>
        <script type="text/javascript" src="<s:url value='/web/js/utils%{#session.minVersion}.js'/>" ></script>
        <script type="text/javascript" src="<s:url value='/web/js/custom.validation%{#session.minVersion}.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/web/js/jquery.ui.selectmenu%{#session.minVersion}.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/web/js/jquery.ui.pane%{#session.minVersion}.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/web/js/jquery.browser%{#session.minVersion}.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/web/js/namespace%{#session.minVersion}.js'/>"></script>
		<script type="text/javascript" src="<s:url value='/web/js/jquery.i18n.properties%{#session.minVersion}.js'/> "></script>
		
		<!-- Include Migrate Plugin for jQuery Upgrade -->
		<script type="text/javascript" src="<s:url value='/web/jquery/home/jquery-migrate-1.2.1%{#session.minVersion}.js'/>"></script>
	
		<%-- Added this script explicitly at the top as we want I18N stuff getting loaded first up. --%>
		<script type="text/javascript">
		var hoveredDiv = "";
		
		$(document).ready(function() {
			jQuery.i18n.properties({
				name: 'LoginJavaScript',
				path:'<s:url value="/web/js/i18n/" />',
				mode:'both',
				language: '<ffi:getProperty name="UserLocale" property="Locale"/>',
				cache:true,
				callback: function(){
				}
			});
			
			$(".businessBankingCls span, .consumerBankingCls span").mouseover(function(){
				if($(this).attr("id") == "businessBanking") {
					$("#businessBanking").fadeTo( 200 , 1 ).find("a").addClass("selectedAnchor");
					$("#consumerBanking").fadeTo( 200, .3 ).find("a").removeClass("selectedAnchor");
				} else {
					$("#businessBanking").fadeTo( 200, .3 ).find("a").removeClass("selectedAnchor");
					$("#consumerBanking").fadeTo( 200,  1 ).find("a").addClass("selectedAnchor");
				}
			})
			
			/* $("#languagesDropdown").selectmenu(); */
		});
		</script>
		
        <style type="text/css">
			<ffi:list collection="GetLanguageList.LanguagesList" items="languageDf">
			body .<ffi:getProperty name="languageDf" property="Language" /> .ui-selectmenu-item-icon {
				height: 24px; width: 24px; border:red solid 1px;
				background: url('/cb/web/multilang/grafx/<ffi:getProperty name="languageDf" property="Language" />_flag.png') 0 0 no-repeat;
			}
			</ffi:list>
			
        </style>

        <s:property value="%{request.getHeader['User-Agent']}" />

    </head>

    <body>
		<%-- <div class="ui-layout-north"><s:include value="header.jsp" /></div> --%>
		<div class="masterDiv">
			<div class="headerDiv">
				<img alt="<s:text name="jsp.home_90"/>" src="<s:url value="/web/grafx/banklogos/Online_Banking_Logo_747075.png"/>"/>
			</div>
			<!-- <div class="sapJspNsGoldBar" class="ui-state-hover"></div> -->
		</div>
		
		<div class="dateComboHolder">
			<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
			<ffi:process name="GetCurrentDate"/>			
			<%-- Format and display the current date/time --%>
			<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="EEEEEEEEE, ${UserLocale.DateTimeFormat}"/>
			<ffi:getProperty name="GetCurrentDate" property="Date" />
			
			<ffi:cinclude value1="${showDate}" value2="true" operator="equals">
				<span id="date"></span>
			</ffi:cinclude>
			<ffi:cinclude value1="${showDate}" value2="true" operator="notEquals">
				&nbsp;
			</ffi:cinclude>
			
			<%-- <ffi:cinclude value1="${GetLanguageList.LanguagesList.size}" value2="1" operator="notEquals">
				<select id="languagesDropdown" tabindex="0"  name="localeName">
					<ffi:setProperty name="GetLanguageList" property="LanguagesList.Locale" value="${UserLocale.Locale}" />
					<ffi:list collection="GetLanguageList.LanguagesList" items="languageDf">
						<option
						 class="<ffi:getProperty name="languaglDf" property="Language" />"
						 value="<ffi:getProperty name="languageDf" property="Language" />"
						 <ffi:cinclude value1="${UserLocale.Locale}" value2="${languageDf.Language}" operator="equals"
						  >selected</ffi:cinclude>
						  >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="languageDf" property="DisplayName" />
						</option>
					</ffi:list>
				</select> 
			</ffi:cinclude> --%>			
		</div>
		
		<div class="centerDivCls">
			<div class="businessBankingCls">
				<span id="businessBanking">
					<h1>SAP&reg; Online Business Banking<hr></h1>
					<p class="introTxt">
						SAP Online Business Banking enables financial institutions to offer their corporate clients the most robust, technically advanced, and efficient cash management solutions available.
					</p>
					<br><br><br>
					<a href="login-corp.jsp">Visit Online Business Banking</a>
				</span>
			</div>
			<div class="consumerBankingCls">
				<span id="consumerBanking">
					<h1>SAP&reg; Online Retail Banking<hr></h1>
					<p class="introTxt">SAP Online Retail Banking is designed for leading financial institutions who are committed to meeting and exceeding customer expectations and improving their return on investment.</p>
					<br><br><br>
					<a href="login-cons.jsp">Visit Online Retail Banking</a>
				</span>
			</div>
		</div>
		<div class="copyrightDiv">
			<!-- <div class="sapJspNsGoldBar" class="ui-state-hover"></div> -->
			<span>Copyright &copy; 2014 SAP AG or an SAP affiliate company. All rights reserved.<br>No part of this publication may be reproduced or transmitted in any form or for any purpose without the express permission of SAP AG. The information contained herein may be changed without prior notice.</span>
		</div>
		
	<script>
		$(document).ready(function() {
			var config = {
				appType:"<s:property value="%{#session.LoginAppType}"/>",
				themeName:"<s:property value="%{#session.themeName}"/>"
			}
			//loginController.init(config);
		});		
	</script>
</html>