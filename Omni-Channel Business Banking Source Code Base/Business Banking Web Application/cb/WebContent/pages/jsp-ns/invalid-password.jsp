<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>


<div class="ui-widget">
	<div  class="ui-state-error ui-corner-all">
		<p>
			<span style="float: left;"></span> 
			<strong><s:text name="jsp.login_5"/></strong> 
			<ffi:setProperty name='PageHeading' value='Service Error' /> 
			<ffi:setProperty name="PageTitle" value="ERROR" /> 
			<%-- ================ MAIN CONTENT START ================ --%>
			<ffi:setProperty name="${touchedvar}" value="false" /> 
			<%-- ERROR MESSAGE BEGIN --%>
			<ffi:setProperty name="ErrorCode" value="<%= Integer.toString(com.ffusion.csil.CSILException.ERROR_INVALID_PASSWORD) %>" />
		
			<ffi:setProperty name="ServiceErrorsResource" property="ResourceID" value="Error${ErrorCode}_descr" /> 
			<ffi:cinclude value1="${ServiceErrorsResource.Resource}" value2="" operator="equals">
				<s:text name="jsp.default_114"/>
			</ffi:cinclude> 
			<ffi:cinclude value1="${ServiceErrorsResource.Resource}" value2="" operator="notEquals">
				<ffi:getProperty name="ServiceErrorsResource" property="Resource" />
			</ffi:cinclude>
		</p>
	</div>
</div>
<br/>
<s:url id="ajax" namespace="/pages/jsp-ns" action="userName.action">
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
</s:url> 
<div align="center">
<sj:a id="invalidPwdBackButtonID" href="%{ajax}" targets="signin_menu" button="true" tabindex="3"><s:text name="jsp.default_57"/></sj:a>
</div>		
	
<script type="text/javascript">
	$(document).ready(function() {
		jQuery.i18n.properties({
			name: 'LoginJavaScript',
			path:'<s:url value="/web/js/i18n/" />',
			mode:'both',
			language: '<ffi:getProperty name="UserLocale" property="Locale"/>',
			cache:true,
			callback: function(){
			}
		});
		
		if(loginController){
			loginController.changeLoginPanelTitle(js_invalid_login_pane_title)
		}	
	});
</script>