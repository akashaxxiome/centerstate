<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:author page="413.jsp"/>
<ffi:setProperty name="InvalidateSession" property="SuccessURL" value=""/>
<ffi:process name="InvalidateSession"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">  
<META HTTP-EQUIV="Refresh" CONTENT="5;URL=/cb/web/html/logout.html">
<html>

<head>
	<title><s:text name="jsp.login_28"/></title>
</head>
<body>
<b><s:text name="jsp.login_28"/></b><br>
<s:text name="jsp.login_87"/>
</body>
</html>
