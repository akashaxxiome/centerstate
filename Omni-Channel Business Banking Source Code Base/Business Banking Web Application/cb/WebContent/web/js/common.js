//Whether to show the global indicator
ns.common.showLIndicator = true;
//Whether to display the popup dialog showing file uploading result 
ns.common.popupAfterUpload = true;
ns.common.canCloseFileUploadPortlet = true;

//Iframe loading flag check if loading is shown from the iframe or not.
ns.common.isIframeLoading = true;

//Store a reference to the original html method.
ns.common.originalHtmlMethod = jQuery.fn.html;

//Store urls which are already submitted
ns.common.submittedUrls = {}; 

//Store UI-Settings value from csil.xml file
ns.common.UISettings = {};

ns.common.lastSyncDate = "";

ns.common.isMenuRollOver = false;

ns.common.autoRefreshPanelsInterval = "";

ns.common.moduleType = "";

//This is added to maintain state of Trxn Saved search delete functionality.
ns.common.isTrxnSearchDeleteConfirmed = false;

/* check whether widget is initialized or not */
ns.common.isInitialized = function (identifier,elementName){
	if(identifier.data(elementName) != undefined){
		return true;
	}else{
		return false;
	}
};


/*
 * Overwrite jQuery html to do cleanup before applying new content to avoid
 * leaking memory
 */
$.fn.html= function( value ) {
	var result;
	try{
		result = ns.common.originalHtmlMethod.apply( this, arguments );
	}catch(ex){
		console.error("Error in updating dom: ", ex);
	}
	
	$(this).find('.ui-jqgrid-btable').each(function(i,table){
		
		/*$(table).bind('reloadGrid', function(e,opts){
			alert(e.target.id);
			});*/
		$(table).bind('remove.gridDestroy',function(){
			$(this).unbind('remove.gridDestroy');
			$(this).jqGrid('destroy');
		});
	});
	return result;
};



	
	
	/*if($.browser.msie && ($.browser.version).split('.')[0]<=7){
		(function($){
		$.fn.html= function( value ) {
				var result = ns.common.originalHtmlMethod.apply( this, arguments );
				$(this).find('.ui-jqgrid-btable').each(function(i,table){
					$(table).unbind('mouseover').unbind('mouseout');
					$(table).bind('mouseover',function(e) {
						ptr = $(e.target).closest("tr.jqgrow");
						if($(ptr).attr("class") !== "subgrid") {
							$(ptr).addClass("ui-state-hover");
						}
						return false;
					}).bind('mouseout',function(e) {
						ptr = $(e.target).closest("tr.jqgrow");
						$(ptr).removeClass("ui-state-hover");
						return false;
					});
					
					var wnarr = $(table).data('widgetName');
					if (!wnarr) {
						$(table).data('widgetName',['jqGrid']);
					}else{
						wnarr.push('jqGrid');
						$(table).data('widgetName', wnarr);
					}
				});
				return result;
			};

		//The following fix is got from jquery.ui.1.8.16
		//jQuery 1.4+
		if ( $.cleanData ) {
		    var _cleanData = $.cleanData;
		    $.cleanData = function( elems ) {
		        for ( var i = elems.length-1, elem; i>=0; i-- ) {
		            try {
						elem = elems[i];
						
						var tmp = $(elem).data('widgetName');
						if(tmp){
							for(var p in tmp) { 
								$(elem)[tmp[p]]('destroy');
							}
						}
						if($(elem).hasClass('hasDatepicker')){
							$(elem).datepicker('destroy');
						}
						$(elem).removeData('widgetName');
						$( elem ).triggerHandler( "remove" );						
 					} catch( e ) {}
		        }
		        _cleanData.apply(this, arguments);
		    };
		}

		$.Widget.prototype._createWidget= function( options, element ) {
				// $.widget.bridge stores the plugin instance, but we do it anyway
				// so that it's stored even before the _create function runs
				this.element = $( element ).data( this.widgetName, this );
				this.options = $.extend( true, {},
					this.options,
					$.metadata && $.metadata.get( element )[ this.widgetName ],
					options );

				var wnarr = this.element.data('widgetName');
				if (!wnarr) {
					this.element.data('widgetName',[this.widgetName]);
				}else{
					wnarr.push(this.widgetName);
					this.element.data('widgetName', wnarr);
				}
				this._create();
				this._init();
			};

		})(jQuery);
			}*/


(function( $ ) {
$.fn.datepicker = function(options){
	/* Initialise the date picker. */
	var self = $(this);
	if (!$.datepicker.initialized) {
		$(document).bind('mousedown.datepicker',$.datepicker._checkExternalClick).
			find('body').append($.datepicker.dpDiv);
		$.datepicker.initialized = true;
	}
	
	options.buttonImage = "/cb/web/grafx/icons/calendar.png";
	
	var otherArgs = Array.prototype.slice.call(arguments, 1);
	if (typeof options == 'string' && (options == 'isDisabled' || options == 'getDate' || options == 'widget'))
		return $.datepicker['_' + options + 'Datepicker'].
			apply($.datepicker, [this[0]].concat(otherArgs));
	if (options == 'option' && arguments.length == 2 && typeof arguments[1] == 'string')
		return $.datepicker['_' + options + 'Datepicker'].
			apply($.datepicker, [this[0]].concat(otherArgs));
	return this.each(function() {
		typeof options == 'string' ?
			$.datepicker['_' + options + 'Datepicker'].
				apply($.datepicker, [this].concat(otherArgs)) :
			$.datepicker._attachDatepicker(this, options);
	});
};


$.fn.extend({
	delAttrByRoot: function(root){
		var elem = $(this)[0];
		for(var attrName in elem){
			if (attrName.indexOf(root)<0) continue;
			if (typeof(elem[attrName])=='function'){
				elem[attrName]=null;
			}else{
				delete elem[attrName];
			}
		}
		elem = null;
	}
});
$.delAttrByRoot = function(root){$(window).delAttrByRoot(root);};

var _tabsdestroy = $.ui.tabs.prototype.destroy;
$.ui.tabs.prototype.destroy = function(){
	var events = $(window).data('events');
	if(events){
		var tempHandler = [], count = 0;
		$.each(events.unload,function(i,unloadObj){
			if(unloadObj.namespace==='') {tempHandler[count++]=unloadObj.handler;}
		});
		for (var i=0;i<count;i++){$(window).unbind('unload',tempHandler[i]);}
		delete tempHandler;
	};
	return _tabsdestroy.apply(this,arguments);
};


$(document).ready(function(){
	/*extending widget to display loading text on tabs. as spinner attribute is not
	 * supported in Tabbed panel jQuery-ui1.9*/	
	$.widget( "ui.tabs", $.ui.tabs, {
		options: {
			spinner: js_tab_loadingText
		},
		_create: function() {			
			this._super();
			this._on({
				tabsbeforeload: function( event, ui ) {
					// Don't react to nested tabs or tabs that don't use a spinner
					if ( event.target !== this.element[ 0 ] ||
							!this.options.spinner ) {
						return;
					}
					var tabAnchor  =  ui.tab.find("a");
					var anchorSpan  =  tabAnchor.find("span");
					/*
					 * Put a condition for Span as in TabbedPanel, 
					 * anchor text is in Span tag while in case of 
					 * Query Tabs it is in Text node 
					 * 
					 */
					if(anchorSpan.length){
						//var span = ui.tab.find( "span" );
						var tabTitle = tabAnchor.attr("title");
						anchorSpan.html( this.options.spinner );
						ui.jqXHR.complete(function() {
							anchorSpan.html( tabTitle );
						});
					}else {						
						var originalText = tabAnchor.text();
						tabAnchor.text( this.options.spinner );
						ui.jqXHR.complete(function() {
							tabAnchor.text( originalText );
						});
					}					
				}
			});
		}
	});	
});
//when a tab is selected make it viewable if it's not.
var _tabsselect = $.ui.tabs.prototype.option;
$.ui.tabs.prototype.option = function(key, value){	
	var result = _tabsselect.apply(this,arguments);	
	if(ns.common.scrollTo){
		var currentTab = this.anchors.eq(this.options.active);
		var portlet = currentTab.parents(".portlet:first");
		if( portlet.length > 0 )
			ns.common.scrollTo(portlet);
		else 
			ns.common.scrollTo(currentTab);
	}
	return result;
};


//The following fix is got from jquery.ui.1.8.16
//jQuery 1.4+
/*if ( $.cleanData ) {
    var _cleanData = $.cleanData;
    $.cleanData = function( elems ) {
        for ( var i = elems.length-1, elem; i>=0; i-- ) {
            try {
              elem = elems[i]
			 $( elem ).triggerHandler( "remove" );
              $( elem ).empty();
            } catch( e ) {}
        }
        _cleanData( elems );
    };
} else {
    var _remove = $.fn.remove;
    $.fn.remove = function( selector, keepData ) {
        return this.each(function() {
            if ( !keepData ) {
                if ( !selector || $.filter( selector, [ this ] ).length ) {
                    $( "*", this ).add( [ this ] ).each(function() {
                        try {
                            $( this ).triggerHandler( "remove" );
                            //this.outerHTML = null;
                        // http://bugs.jquery.com/ticket/8235
                        } catch( e ) {}
                    });
                }
            }
            return _remove.call( $(this), selector, keepData );
        });
    };
};*/
//The above fix is got from jquery.ui.1.8.16

//Extend createTopic function from jquery.subscribe.js to record all topics created via an DOM element
//so that we can destroy the topics upon removing the element
var _createTopic = $.fn.createTopic;
$.fn.createTopic = function(topic) {
	var self = this;
	$.struts2_jquery.log('TOPIC TRACKING - create topic: ' + topic + ' by [id:' + self.attr('id')+']-[title:'+self.attr('title')+']' );
	if(topic && !this.isSubscribed(topic)) {
		var topics = this.data('createdTopics');
		if(!topics){
			topics = [];
			this.data('createdTopics', topics);
			this.bind('remove.topics', function(){
				self.unLoadTopics();
			});
		}
		topics.push(topic);
	}
	
	_createTopic.apply(this, arguments);
};
 
$.fn.unLoadTopics = function(){
	var self = this;
	$.struts2_jquery.log('TOPIC TRACKING - unLoadFor: [id:' + self.attr('id')+']-[title:'+self.attr('title')+']');
	
	var createdTopics = self.data('createdTopics');
	$.struts2_jquery.log('TOPIC TRACKING - unLoadTopics: ' + createdTopics);
	if(createdTopics){
		//$.each(createdTopics, function(i, topic){
		for(var i=0;i<createdTopics.length;i++){
			var topic = createdTopics[i];
			self.unsubscribe(topic);
			$.struts2_jquery.log('TOPIC TRACKING - unsubscribed by: [id:' + self.attr('id')+']-[title:'+self.attr('title') + '] topic: ' + topic);
			
			//check if there are any other subscribers, otherwise destroy the topic
			//var hasSubscribers = false;
			/*if( _subscribe_topics[topic]){
				$.each( _subscribe_topics[topic].objects, function(i, object){
					if($.isArray(object)) {		// handle '__noId__' elements
						if(object.length > 0) {
							$.struts2_jquery.log('TOPIC TRACKING - subscribed by document: ' + topic);
							hasSubscribers = true;
							return false;
						}
							
					} else {
						$.struts2_jquery.log('TOPIC TRACKING - subscribed by dom element: ' + topic + ' [id:' + object.attr('id')+']-[title:'+object.attr('title')+']');
						hasSubscribers = true;
						return false;
					}
				});
			}*/
			/*if(!$(document).isSubscribed(topic)){
				$.destroyTopic(topic);
				$.struts2_jquery.log('TOPIC TRACKING - destroyed ' + topic);
			}*/
			
		};
		self.removeData('createdTopics');
		createdTopics=null;
	}

	return this;
};

var debugFlag=false;

$.log = function(msg){
	if(window.console && debugFlag){
		console.log(msg);
	}
};

$.debug = function(isDebug){
	debugFlag=isDebug;
};

//Extend subscribe function from jquery.subscribe.js to use a certain DOM element for each module 
//so that we can destroy the topics upon going to another module.
$.subscribe =  function(topic, handler, data) {
	if(!ns.home.lastMenuId || ns.home.lastMenuId === ''){
		$.struts2_jquery.log('TOPIC TRACKING - subscribe to document: ' + topic);
		return $(document).subscribe(topic, handler, data);
	} else {
		$.struts2_jquery.log('TOPIC TRACKING - subscribe to ' + ns.home.lastMenuId + ': ' + topic);
		return $('#menu_'+ns.home.lastMenuId).subscribe(topic, handler, data);
	}
};

(function($){
    var dialogExtensions ={
        oldClose: $.ui.dialog.prototype.close,
        close: function(event){
        	var isDialogOpen =  (this.element.dialog('isOpen') === true);
        	
        	//Call original functionality
        	this.oldClose(event);

        	//If dialog is closed, then clear dialog content, if dialog does not have static content.        	
        	if(!this.element.hasClass('staticContentDialog')){
    			this.element.empty();
    		}
			
        	if(isDialogOpen){
        		// As soon as dialog is closed, reset the tab index and unbind the key down events for dialog
				$.publish("common.topics.tabifyAll");
				if(accessibility){				
					accessibility.unbindDialogEvents();				
					accessibility.getDialogInvoker().focus();
				}
        	}			
        }
    };
    $.extend($.ui.dialog.prototype, dialogExtensions);
})(jQuery);

//Override dialog create by creating our own widget.
(function ($) {
    $.widget("ffi.dialog", $.ui.dialog, {
        _create: function () {
        	//Check if dialog with same id exist or not, if exist then remove it
        	var divId = this.element[0].id;
        	var counter = 0;
        	
        	//Using this Jquery syntax we can get all elements with same id.
        	var dialogArr = $('[id="'+divId+'"]');
        	if(dialogArr.length > 1){
        		for(counter = 0; counter < dialogArr.length; counter++ ){
        			//Get the dialog to destroy
        			if($(dialogArr[counter]).hasClass('ui-dialog-content')){
        				$(dialogArr[counter]).dialog('destroy');
        				$(dialogArr[counter]).remove();
        			}	
        		}
        	}
        	
        	// continue with creating dialog
        	$.ui.dialog.prototype._create.call(this);
        }
    });
})(jQuery);


/*
 * Overwrite dialog open method to set width and height according to window size 
 */
var _dialogOpen = $.ui.dialog.prototype.open;

$.ui.dialog.prototype.open = function(){	
	var width = $(this.element).dialog("option","width");
	var height = $(this.element).dialog("option","height");
	var widthSet = false; //Is width of dialog adjusted due to small window?
	//Save the original settings
	if(!this.element.oWidth)
		this.element.oWidth = width;
	if(!this.element.oHeight)
		this.element.oHeight = height;
	
	//Get size of current window
	var wWidth = $(window).width();
	var wHeight = $(window).height();

	if (this.element.oWidth > wWidth) {
		width = wWidth * 0.9;
		$(this.element).dialog("option", "width", width);
		widthSet = true;
	} else {
		$(this.element).dialog("option", "width", this.element.oWidth);
	}

	if (this.element.oHeight != "auto" && this.element.oHeight > wHeight) {
		height = wHeight * 0.9;
		$(this.element).dialog("option", "height", height);
	} else {
		$(this.element).dialog("option", "height", this.element.oHeight);
	}
	
	if(accessibility){
		accessibility.setDialogInvoker(document.activeElement);
	}

	//Call original open() to open dialog
	_dialogOpen.apply(this, arguments);

    //if height is set to 'auto' the actual height can only be calculated after rendering
    if (this.element.oHeight == "auto") {
		height = 0;
		$(this.element).children().each(function(index) {
			height += $(this).height();
		});
		if (height > wHeight) {
			height = wHeight * 0.9;
			$(this.element).dialog("option", "height", height);
		} else {
			$(this.element).dialog("option", "height", "auto");
		}
	}
    
    if(!widthSet){ 
	    var maxWidth = 0;
	    $(this.element).find('*').each(function() {
	        if ($(this).width() > maxWidth) 
	            maxWidth = $(this).width();
	    });
	    if(this.element.oWidth < maxWidth * 1.1) {
			// don't allow dialog with to be wider than the window width
			if (maxWidth > wWidth) {
				maxWidth = wWidth * 0.9;
			}
			$(this.element).dialog("option", "width", maxWidth * 1.1);
    }
    }
    
    // Add logic to set few Css properties which will override default ones
    var dialogButtonPane = this.uiDialog.find('.ui-dialog-buttonpane');
    if(dialogButtonPane.length){
    	dialogButtonPane.css({'border-width':'0px 0 0', 'text-align':'center'});
    	dialogButtonPane.addClass('ui-widget-header');
    	dialogButtonPane.find('button').css('float','none');
    }
   	
	//Attach help and help file path to opened dialog if its there.
    //Check if current dialog has help icon already added 
    var helpIcon = $(this.element).dialog('widget').find('.ui-dialog-titlebar').find('.helpCtl');
    if(!helpIcon.length){
	    //Attach help and help file path to opened dialog if its there.\
	    var id =  this.element[0].id;
	    var helpFile = $(this.element).find('.moduleHelpClass').html();
		if("openFileImportDialogID"!=id && helpFile){
			$(this.element).addHelp(function(){
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			});
		}	
    }

	// As soon as dialog opens tabify it and bind keydown event for capturing focus on modal dialog	
	$.publish("common.topics.tabifyDialog");
	if(accessibility){
		accessibility.bindDialogEvents();
	}	
	
	$(".ui-widget-overlay").css('z-index','997');
	$(".ui-dialog").css('z-index','998');
};

$.ui.dialog.prototype._focusTabbable = function(){
	this.uiDialog.eq(0).focus();
};


$.fn.hasScrollBar = function() {
    return this.get(0).scrollHeight > this.height();
};


//extend $.ajax function to prevent double submit
var _ajax = $.ajax;
$.ajax = function(option){
	extendAjaxSettingsForStaticResources(option);	
	var url = option.url || location.href;
	var data = option.data;
	if(data){
		if(ns.common.submittedUrls[url]&&ns.common.objEquals(ns.common.submittedUrls[url],data)){
			return false;
		}else{
			ns.common.submittedUrls[url] = data;
			var processFlag = true;
			var _success = option.success;			
			option.success = function(data, textStatus, qXHR ){					
				processFlag = !isExceptionOccured(data);
				if(processFlag && _success){
					try{
						_success.apply(this,arguments);
					}catch(error) { 
						if(this.url){
							console.error("Error in processing response from->", this.url);	
						}
						console.error(error.message, error.stack);
					}
				}
			};
			var _complete = option.complete;
			option.complete = function(jqXHR, textStatus){
				delete ns.common.submittedUrls[url];
				if(processFlag && _complete){										
					_complete.apply(this,arguments);
				}
				avoidIEAjaxLeak(jqXHR)
			}
			
			//Remove logging of synchronous requests later
			if(option.async === false || option.async === "false" || option.async === "undefined"){
				if(!window.NO_OF_ASYNC_REQUESTS){
					window.NO_OF_ASYNC_REQUESTS = 0;
				}
				window.NO_OF_ASYNC_REQUESTS++;
				console.info("Synchronous Req==>", option.url, window.NO_OF_ASYNC_REQUESTS);
			}
			
			return _ajax.call(this,option);
		}
	}else {
		if(ns.common.submittedUrls[url]==true){
			return false;
		}else{
			ns.common.submittedUrls[url] = true;
			var processFlag = true;
			var _success = option.success;			
			option.success = function(data, textStatus, qXHR ){							
				processFlag = !isExceptionOccured(data);
				if(processFlag && _success){
					try{
					_success.apply(this,arguments);
					}catch(error) {   
						console.error(error.message, error.stack);
					}
				}
			};
			var _complete = option.complete;
			option.complete = function(jqXHR, textStatus){
				delete ns.common.submittedUrls[url];
				if(processFlag && _complete){										
					_complete.apply(this,arguments);
				}
				avoidIEAjaxLeak(jqXHR);
			};
						
			//Remove logging of synchronous requests later
			if(option.async === false || option.async === "false" || option.async === "undefined"){
				if(!window.NO_OF_ASYNC_REQUESTS){
					window.NO_OF_ASYNC_REQUESTS = 0;
				}
				window.NO_OF_ASYNC_REQUESTS++;
				console.info("Synchronous Req==>", option.url, window.NO_OF_ASYNC_REQUESTS);
			}
			
			return _ajax.call(this,option);
		}
	}
};

/* Function to check exxeption code return in success response to prevent success call*/

function isExceptionOccured(data){
	var exceptionOccuredFlag = false;

	if ($.type(data) === "string" && data.startsWith("[[Exception:")) {
		exceptionOccuredFlag = true;
		var i1 = data.indexOf("]]");
		var exception = data.substring(0, i1 + 2);					
		var i2 = exception.indexOf(":");
		var errorCode = exception.substring(i2 + 1, i1);		
		if (errorCode === "500") { //General TASK_PROCESSING_ERROR
			ns.common.handleErrorMessage(data.substring(i1+2));
		} else if (errorCode == "601") { //AUTHENTICATION_REQUIRED
			ns.common.openDialog("authenticationInfo");
		} else if (errorCode == "700") { //INTERVAL_VALIDATION_ERROR
			$('#stallConditionDialogID').dialog('open');
		} else if (errorCode == "600") { //SESSION_INVALIDATION_NEEDED
			location.href='/cb/pages/jsp/invalidate-session.jsp?TimedOut=true';
		} else if (errorCode == "701") { //NO_GLOBAL_MSG_AVAILABLE
			ns.home.hideGlobalMessage();
		}
		
	}
	return exceptionOccuredFlag;
}

/*
* This function overrides ajax settings if requested resource is statis resource
* Apply caching for Script and Css files. We do not want to fetch these files
* every time.
* Do not show spinner in case static resource is requested
*/
function extendAjaxSettingsForStaticResources(option){
	var url = option.url; 
	if(url){
		if(url.match("/web/js") !== null || url.match("/web/css") !== null || url.match("/resources/") !== null){
			option.cache = true;
			option = ns.common.applyLocalAjaxSettings(option);
		}	
	}
}

/*
Function to set jqXHR and its content like onreadystatechange and abort to null depending
on various conditions.
*/
function avoidIEAjaxLeak(jqXHR){
	 // Avoid IE memory leak
	if (typeof(jqXHR) !== 'undefined') {
		if (typeof(jqXHR.onreadystatechange) !== 'unknown' ) {
			jqXHR.onreadystatechange = null;
		} 		
		if (typeof(jqXHR.abort) !== 'unknown' ) {
			jqXHR.abort = null;
		} 
		jqXHR = null;
	}
}


var _showLoading = $.fn.showLoading;
$.fn.showLoading = function(options) {
	
	var id = $(this).attr('id');
	if(!options){
		options = {};
		arguments[0] = options;
		arguments.length = 1;
	}
	if(!options.afterShow) {
		options.afterShow = function() {
			setTimeout( "jQuery('#" + id + "').hideLoading()", 3000 );
		};
	}
	_showLoading.apply(this, arguments);
}

})( jQuery );


$(document).ready(function(){
	//Get values from csil.xml and assign to global JSON object and also initialize Ajax setup.
	initUISetup();
	
	$.subscribe('closeDialog', function(event,data) {
		ns.common.closeDialog();
	});
	
	$.subscribe('wireUploadCancelOnClickTopics', function(event,data) {
		ns.common.canCloseFileUploadPortlet = false;
		ns.common.closeDialog();
		ns.common.canCloseFileUploadPortlet = true;
	});
	
	$.subscribe('wireUploadDoneOnClickTopics', function(event,data) {
		ns.common.canCloseFileUploadPortlet = false;
		ns.common.closeDialog();
		ns.common.canCloseFileUploadPortlet = true;
	});	
	
	$.subscribe('hideLoadingStatus', function(event,data) {
		$('#loadingstatus').hide();
	});
	
	$.subscribe('hideOperationResult', function(event,data) {
		$('#operationresult').hide();
	});

	$.subscribe('hideTaskErrorMessage', function(event, data) {
		$('#taskErrorMessageDiv').hide();
	});
	
	$.subscribe('cancelFileImportForm', function(event, data) {
		$('.cancelImportFormButton').click();
	});

	$(window).resize(function() {
		ns.common.resizeWidthOfGrids();
	});
	
	
	$(document).on("click", "a,input[type=submit]", function(e){
		var $elem = $(e.currentTarget);
		if(ns && ns.common){
			ns.common.trackElement($elem);
		}		
	});	
});


function initUISetup(){
	//Override Jquery AJAX functionality to set our handler to be called on different events.
	registerGlobalAjaxHandler();
	ajaxSetup();
	
	//When UI Settings are ready apply!
	$.getUISettings().done(function(settings){
		//Set ajax request timeout
		$.ajaxSetup({timeout:settings.AjaxRequestTimeout});

	//Set Auto refresh Interval for message panel if not disabled (-1) at CSIL
		if(settings.AutoRefreshPanelsInterval != "-1"){
			ns.common.autoRefreshPanelsInterval = setInterval("ns.common.refreshPanels()",settings.AutoRefreshPanelsInterval);
		}

		//Subscribe rese
		$.subscribe('ns.common.resetRefreshPanels', function(event,data) {
			window.clearInterval(ns.common.autoRefreshPanelsInterval);
			ns.common.autoRefreshPanelsInterval = setInterval("ns.common.refreshPanels()",settings.AutoRefreshPanelsInterval);	
		});

		//Override the lookupbox settings
		$.widget( "ux.lookupbox", $.ux.lookupbox, {
		    options: {
		        maxItems:settings.LookUpBoxMaxItems
	}
		});

		//Set the variable
		ns.common.ajaxSpinningWheelTimeout = settings.AjaxSpinningWheelTimeout;
	});
}

/**
 * Utility
 */
String.prototype.endsWith = function (a) {
    return this.substr(this.length - a.length) === a;
}

String.prototype.startsWith = function (a) {
    return this.substr(0, a.length) === a;
}

String.prototype.trimEnd = function () {
    return this.replace(/\s+$/, "");
}

String.prototype.trimStart = function () {
    return this.replace(/^\s+/, "");
}

String.prototype.trim = function() {
    return this.replace(/^\s*/, '').replace(/\s*$/, '');
}



ns.common.inView = function($elem, viewAll) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    if (viewAll)
		return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	else
		return (elemTop >= docViewTop);
};


ns.common.scrollTo = function($elem){	
    var off = $('#notes').offset().top - $elem.offset().top;
    if(off > 0) //The element is scrolled above the top of notes
    	$("#notes").animate({scrollTop: $("#notes").scrollTop()-off-15}, {duration:0, easing:"linear"});
    if(!ns.common.inView($('#notes')))
    	$("html, body").animate({scrollTop: 0}, {duration:0, easing:"linear"}); 
	
};

ns.common.ajaxSpinningWheelTimeout = 300000;
ns.common.showLoading = function(){
	ns.common.blockLoading = true;
	$('#defaultLoadingIndicator').hide();
	$('#outer-center').showLoading({
		'indicatorID' : getLoadingId(),
		'addClass': 'loading-indicator-dripcircle',
	    'afterShow': function() {
					setTimeout( "jQuery('#outer-center').hideLoading({'indicatorID':'"+getLoadingId()+"'})",
							ns.common.ajaxSpinningWheelTimeout);
				}
		}
	);
};


ns.common.hideLoading = function(loadingId){
	if (ns.common.blockLoading) {
		var indicatorID = loadingId ? loadingId : getLoadingId();
		$('#outer-center').hideLoading({'indicatorID':''+indicatorID});
		ns.common.blockLoading = false;
	} 
	$('#defaultLoadingIndicator').hide();
};

//Function will be called on each menu load, to remove any previous menu loading / spinning gif.
ns.common.hidePrevMenuLoading = function(loadingId){
	if(loadingId){
		$('#outer-center').hideLoading({'indicatorID':''+loadingId});
	}
	// remove other loading indicators and thier overlays
	$(document).hideLoadingIndicators();
};

//Function to decide loading indicator (spinning wheel gif and blocking div) id.
function getLoadingId(){
	//If Id is not available give landing menu Id. 
	if(!ns.home.currentMenuId){
		return ns.common.landingMenuId;
	}
	return ns.home.currentMenuId;//this variable gets value when Main menu or Sub menu clicked.
}

/*$.ajaxSetup({
	xhr: window.XMLHttpRequest && (window.location.protocol !== "file:" || !window.ActiveXObject) ?
		function () {
		// start memory leak fix. 
			if (window.ActiveXObject) {
				try {
					return new window.ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) { }
			}
		// end memory leak fix. 
			return new window.XMLHttpRequest();
		} :
		function () {
			try {
				return new window.ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) { }
		}
});	*/

$.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
	/*if(options.dataType && (options.dataType.toUpperCase()==="SCRIPT" 
			|| options.dataType.toUpperCase()==="CSS")){
		//Turn caching ON
		options.cache = true;
	}*/
});

function ajaxSetup(){
	
	//common ajax options
	$.ajaxSetup({
		cache: false,
		
		timeout: "300000",
		
        beforeSend: function(jqXHR, settings ){
			/*if(settings.dataType && (settings.dataType.toUpperCase() === "SCRIPT" 
				|| settings.dataType.toUpperCase() === "CSS")){
				
				//Remove timestamp, so that Script/css files can be cached.
				var index = -1;
				if(settings.url.search("_=") != -1){
					index = settings.url.lastIndexOf("?_=");
					index = index == -1 ?settings.url.lastIndexOf("&_=") : index;
						
					if(index != -1){
						settings.url = settings.url.replace(settings.url.slice(index),"");
					}
				}
			}*/
        	
        },
        
 /*       dataFilter: function(data, type){
    		//$.log('ajaxSetup: dataFilter: ' + type);        	
        	if ($.type(data) === "string" && data.startsWith("[[Exception:")) {
				var i1 = data.indexOf("]]");
				var exception = data.substring(0, i1 + 2);
				console.info("dataFilter exception="+exception);
				var i2 = exception.indexOf(":");
				var errorCode = exception.substring(i2 + 1, i1);
				console.info("dataFilter errorcode="+errorCode);
				if (errorCode === "500") { //General TASK_PROCESSING_ERROR
					ns.common.showError(data.substring(i1+2));
				} else if (errorCode == "601") { //AUTHENTICATION_REQUIRED
					ns.common.openDialog("authenticationInfo");
				} else if (errorCode == "700") { //INTERVAL_VALIDATION_ERROR
					$('#stallConditionDialogID').dialog('open');
				} else if (errorCode == "600") { //SESSION_INVALIDATION_NEEDED
					location.href='/cb/pages/jsp/invalidate-session.jsp?TimedOut=true';
				} else if (errorCode == "701") { //NO_GLOBAL_MSG_AVAILABLE
					ns.home.hideGlobalMessage();
				}
				console.info("data=="+data);				
				//throw 'exception';
				
			}
			return data;
        },*/
        
        error: function(xmlHttpRequest, textStatus, errorThrown){
    		//$.log('ajaxSetup: error');
    		var errorMsg;
    		if(xmlHttpRequest.getAllResponseHeaders()){
    			if (textStatus=='parsererror'){
    				errorMsg = 'Error.\nParsing JSON Request failed.';
    				return;//don't show parseerror
    			} else if (textStatus=='timeout'){
    				errorMsg = 'Request Time out.<br/>';
    			} else if (xmlHttpRequest.status==404){
    				errorMsg = 'Requested URL not found.<br/>';
    			} else if (xmlHttpRequest.status==500){
    				errorMsg = 'Internal Server Error.<br/>';
    			} else {
    				if(xmlHttpRequest.status==200)
    					return;
    				errorMsg = 'Unknown Error.<br/>';
    			}

//  			errorMsg += "status: " + xmlHttpRequest.status + "<br/>";
//  			errorMsg += "statusText: " + xmlHttpRequest.statusText + "<br/>";
//  			errorMsg += "responseText: " + xmlHttpRequest.responseText + "<br/>";
//  			errorMsg += "textStatus: " + textStatus + "<br/>";
//  			errorMsg += "errorThrown: " + errorThrown + "<br/>";
    			errorMsg += xmlHttpRequest.responseText + "<br/>";
    			
    			var contentType = xmlHttpRequest.getResponseHeader("Content-Type");
    			if(contentType.match("application/json")){     				
    				var  responseText = xmlHttpRequest.responseText;
					if(responseText !="" && responseText !="null"){
						//"{"error":{"code":null,"message":{"lang":"en","value":"java.lang.NullPointerException"}}}"
						var commentedIndex = responseText.indexOf("/*"); 
						var sResponse = "";
						if(commentedIndex && commentedIndex !== -1 ){
							sResponse = responseText.substring(0,commentedIndex);
						}else{
							sResponse = responseText;
						}	

						if(sResponse != ""){
							var res = JSON.parse(sResponse);
							isJsonResponse = true;
							if(res.response){
								var response = res.response;
								errortype = response.type;
								if(response){
									errorMsg = response.data;
								}						
							}else{
								if(res.error){
									errorMsg = res.error.message.value;	
								}
							}
						}						
					}
    			}
    			ns.common.handleErrorMessage(errorMsg);
    		}
        },
        
        success: function(data, textStatus, xmlHttpRequest){
        	/*console.info('success function set up');
        	if ($.type(data) === "string" && data.startsWith("[[Exception:")) {
        	console.info('success function set up=='+data);
        	}*/
        },
        
        complete: function(xmlHttpRequest, textStatus){
    		//$.log('ajaxSetup: complete');
        }
    
    });
	
	//Save a reference to this complete call back for restoring in case of it's changed	
	ns.common.ajaxComplete = $.ajaxSettings.complete;

}
function registerGlobalAjaxHandler(){
	
	//$('#loadingstatus').ajaxSend(function(event, xmlHttpRequest, ajaxOptions){
		$(document).ajaxSend(function(event, xmlHttpRequest, ajaxOptions){
		$.log('GlobalAjaxHandler: ajaxSend');
	});
	
	//$('#loadingstatus').ajaxStart(function(){
		$(document).ajaxStart(function(){
		//Check if iframe is available or not
		if(ns.common.isIframeLoading){
			ns.common.isIframeLoading = false;
			if(window.parent.ifrm 
					&& (window.parent.$('#loading-indicator-ifrm').is(':visible'))){		
				window.parent.$('#ifrm').hideLoading();
			}
		}
		
		//$.log('GlobalAjaxHandler: ajaxStart');
		$('#loadingstatus').hide(); //to display on error only
		ns.common.hideStatus();
		if(ns.common.showLIndicator && ns.home.currentMenuId !== "home") {
			//$('#defaultLoadingIndicator').show();
			ns.common.showLoading();
			//ns.common.LoadingTimer = setTimeout("ns.common.showLoading()",1000);
		}
	});
	
	//$('#loadingstatus').ajaxStop(function(){
		$(document).ajaxStop(function(){
		//$.log('GlobalAjaxHandler: ajaxStop');

		//refresh timeout interval (set in timeout.jsp)
		if(ns.common.sessionIntervalId){
			clearInterval(ns.common.sessionIntervalId);
			ns.common.sessionIntervalId = setInterval("ns.common.timerExpired()",ns.common.timeoutMS);
		}

		//clear timer to show the block loading indicator
		clearTimeout(ns.common.LoadingTimer);
		//the indicator may not be triggered, but it's safe to perform hiding
		ns.common.hideLoading();
		
		//to adjust the minDate of datePickers according to date difference
		//between client and server (dateOffset was computed in timeout.jsp)
		var minDate;
		if ($('.hasDatepicker').length > 0)
			minDate = $('.hasDatepicker').datepicker( "option", "minDate" );
		if(minDate == 0)
			$('.hasDatepicker').datepicker( "option" , 'minDate', dateOffset );
		
		if($('#notes').hasScrollBar())
			ns.common.resizeWidthOfGrids();
		
	});
	
	//$('#loadingstatus').ajaxComplete(function(event, xmlHttpRequest, ajaxOptions){
		$(document).ajaxComplete(function(event, xmlHttpRequest, ajaxOptions){
		//$.log('GlobalAjaxHandler: ajaxComplete');
		//$('#notes').hideLoading();
	});
	
	//$('#loadingstatus').ajaxError(function(event, xmlHttpRequest, ajaxOptions){
		$(document).ajaxError(function(event, xmlHttpRequest, ajaxOptions,exception){
		//$.log('GlobalAjaxHandler: ajaxError');
			//console.info("Error occured=="+exception);
		if(xmlHttpRequest.getAllResponseHeaders()){
		var errorMsg = '';
		
		if (xmlHttpRequest.status == 0) {
			errorMsg = 'You are offline!!\n Please Check Your Network.<br/>';
		} else if (xmlHttpRequest.status == 500){
			errorMsg = 'Internal Server Error.<br/>';
		} else if (xmlHttpRequest.status == 422){//Added for Limit Check Fail Scenario
			errorMsg = '';
		} else if (xmlHttpRequest.status==404){
			errorMsg = 'Requested URL not found.<br/>';
		} else {
			return;
		}
		
//		errorMsg += "status: " + xmlHttpRequest.status + "<br/>";
//		errorMsg += "statusText: " + xmlHttpRequest.statusText + "<br/>";
//		errorMsg += "responseText: " + xmlHttpRequest.responseText + "<br/>";
		
		var contentType = xmlHttpRequest.getResponseHeader("Content-Type");
		var rErr =  xmlHttpRequest.responseText;
		var isJsonResponse = false;
		var errortype = "";
		if(contentType){
			if(contentType.match("application/json")!=null){
				var  responseText = xmlHttpRequest.responseText;
				if(responseText !="" && responseText !="null"){
					//"{"error":{"code":null,"message":{"lang":"en","value":"java.lang.NullPointerException"}}}"
					var commentedIndex = responseText.indexOf("/*"); 
					var sResponse = "";
					if(commentedIndex && commentedIndex !== -1){
						sResponse = responseText.substring(0,commentedIndex);
					}else{
						sResponse = responseText;
					}	
					if(sResponse != ""){
						var res = {};
						try{
							res = JSON.parse(sResponse);
						}catch(e){
							console.error("Error in parsing json response!!!");
							res.error = {
								message:{
									value:errorMsg
								}
							}					
						}
						
						isJsonResponse = true;
						if(res && res.response){
							var response = res.response;
							errortype = response.type;
							if(response){
								rErr = response.data;
							}						
						}else{
							if(res && res.error){
								rErr = res.error.message.value;	
							}
						}
					}					
				}				
			}
						
		}else{
			rErr = xmlHttpRequest.responseText;
		}
		


		
		//'Error 500' is returned by struts dispatcher for any uncaught exceptions
		if(rErr != undefined){
			
			if(rErr.startsWith('Error 500')){
				rErr = 'Please contact the system administrator.';
			}

		if(!isJsonResponse){
			//Check if response has error in format of [[Exception:500]] Some Error Message			
			if(rErr.startsWith("[[Exception:500]]")){
				//Retrieve the specific message from error response
				errorMsg = rErr.replace("[[Exception:500]]","");
			}else{
			errorMsg += rErr + "<br/>";
			}
		}else{
			errorMsg = rErr;
		}
		
		
		if ($('#operationresult').length == 0) {
			ns.common._showLStatus(errorMsg);
		} else {
			ns.common.handleErrorMessage(errorMsg,errortype);
		}

		//clear timer to show the block loading indicator
		clearTimeout(ns.common.LoadingTimer);
		//the indicator may not be triggered, but it's safe to perform hiding
		ns.common.hideLoading();
		}

	}
	});
		
	
	//$('#loadingstatus').ajaxSuccess(function(event, xmlHttpRequest, ajaxOptions){
	$(document).ajaxSuccess(function(event, xmlHttpRequest, ajaxOptions){
		//$.log('GlobalAjaxHandler: ajaxSuccess');		
		var contentType = xmlHttpRequest.getResponseHeader("Content-Type");
		var successMessage = "";
		if(contentType){
			if(contentType.match("application/json")!=null){
				var  responseText = xmlHttpRequest.responseText;
				if(responseText !="" && responseText !="null"){
					var res = JSON.parse(responseText);
					var response = res.response;
					if(response){
						if(response.status === "success"){
							successMessage = response.data;
							if(successMessage != "" && typeof successMessage =="string" ){
								ns.common.showStatus(successMessage);
							}							
						};						
					};
				};				
				
			};			
			
		};
		
		var  responseText = xmlHttpRequest.responseText;
		if ($.type(responseText) === "string" && responseText.startsWith("[[Exception:")) {
			 xmlHttpRequest={responseText:''};
		}				
	});

}



/* =================================================================================
	Common Utility Functions
==================================================================================== */


/**
 * Show Operation Result island with an error message
 */
ns.common.showError = function(htmlMsg){
	var $result = $('#operationresult');
	var $iconSpan = "<span class='sapUiIconCls icon-sys-cancel-2' style='color: #AD455A;'></span>";
	$("#resultmessage").children("span:first").remove();
	$('#resultmessage').removeClass("serverResponseHolderDiv_SuccessMsg").addClass("serverResponseHolderDiv_ErrorMsg").html($iconSpan+htmlMsg);
	$result.show().delay(10000).fadeOut();
};

/**
 * Show Operation Result island with a message
 */
ns.common.showStatus = function(htmlMsg){
	if(!$('#verifyTab').hasClass('ui-state-active') && htmlMsg!= undefined && htmlMsg != ''){
		var $result = $('#operationresult');
		var $iconSpan = "<span class='sapUiIconCls icon-accept' style='color:#3FA83F;'></span>";
		$("#resultmessage").children("span:first").remove();
		$('#resultmessage').removeClass("serverResponseHolderDiv_ErrorMsg").addClass("serverResponseHolderDiv_SuccessMsg").html($iconSpan+htmlMsg);
		$result.show().delay(10000).fadeOut();
	}
};


/**
 * Hide Operation Result island
 */
ns.common.hideStatus = function(){
	$('#operationresult').hide();
	$('#loadingstatus').hide();
}

ns.common.autoHide = function(){	
	$("#operationresult").delay(6000).fadeOut(); 
}

ns.common._showLStatus = function(htmlMsg, nonError){
	var $LStatus = $('#loadingstatus');
	$LStatus.html(htmlMsg);
	$LStatus.show();
	if (nonError)
		$LStatus.addClass('ui-state-highlight').removeClass('ui-state-error');
	else
		$LStatus.addClass('ui-state-error').removeClass('ui-state-highlight');

	if (!ns.common.inView($LStatus))
		$("window").animate( {scrollTop : $LStatus.offset().top}, {duration : 0,	easing : "linear"});
}


/**
 * Close a modal dialog being displayed currently
 * @param dialogId Id of the dialog to be closed
 */
ns.common.closeDialog = function(dialogId){
	if (dialogId) {
		if (!dialogId.startsWith('#')){
			dialogId = '#' + dialogId;
		}	
		if ($(dialogId).dialog("isOpen")===true){
			$(dialogId).dialog("close");
		}	
	} else {
		$(".ui-dialog-content").each(function(){
			var alertDialog = $(this).hasClass("alert");
			if ($(this).dialog("isOpen")===true && !alertDialog) {
				$(this).dialog("close");
			}
		});
	}
};

/**
 * Resize the width of grids when the size of window is changed 
 * or left panel is colosed or opened.
 */
ns.common.resizeWidthOfGrids = function(){
	
	var $grids = $('.ui-jqgrid:visible');
	$grids.each(function(){
		//4 is padding-left + padding-right defined by ui-widget-content in desktop.css
		var grid_width = $(this).parent().width() - 4;
		var $grid = $(this).find(".ui-jqgrid-bdiv .ui-jqgrid-btable");
		$grid.jqGrid('setGridWidth', grid_width, true);
		
	});
}
	

/**
 * When the dialog is bigger than the browser window, 
 * resize it to display completely inner the window. 
 * @param dialogId Id of the dialog
 */
ns.common.resizeDialog = function(dialogId) {
	var $dialog = $('#' + dialogId);
	
	if($dialog.parent().width() > ($(window).width() - 100) )
	{
		$dialog.parent().width($(window).width() - 100);
	}
	
	if($dialog.height() > ($(window).height() - 150) )
	{
		$dialog.height($(window).height() - 150);
	}
}

/**
 * compare two js objects 
 * @param the fisrt object
 * @param the second object
 * @return TRUE if their contents are the same, FALSE if different
 */
ns.common.objEquals = function(src, des){
	if(src == des){
		return true;
	}
	if(typeof(des)=="undefined"||typeof(src)=="undefined"||des==null||src==null||typeof(des)!="object"||typeof(src)!="object"){
		return false;
	}
	var length = 0; 
	var length1=0;
	for(var ele in src) {
		length++;
	}
	for(var ele in des) {
		length1++;
	}
	if(length!=length1){
		return false;
	}
	if(des.constructor==src.constructor){
		for(var ele in src){
			if(typeof(src[ele])=="object") {
				if(!ns.common.objEquals(src[ele],des[ele])){
					return false;
				}
			}
			else if(typeof(src[ele])=="function"){
				if(!ns.common.objEquals(src[ele].toString(),des[ele].toString())){
					return false;
				}
			}	
			else if(src[ele]!=des[ele]){
				return false;
			}
		}
		return true;
	}
	return false;
}

/**
 * HTML encode a string, but leave <br> alone since it is often returned in error messages
 * @param value String to encode
 * @return encoded string
 */
ns.common.HTMLEncode = function(value) {
   	if(value!=undefined && typeof value =='string'){
		return value.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/&lt;br&gt;/g,'<br>');
	}
	return "";
}
/* =================================================================================
	Custom Mapping Functions
==================================================================================== */

/**
 * 
 * @param cellvalue
 * @param options
 * @param rowObject
 * @return The HTML snippet to be filled in grid cell
 */
function formatCustomMappingEditandDelete(cellvalue, options, rowObject) { 

	var edit = "<a title='" + js_edit + "' href='#' onClick='editMapping(\""+ rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del  = "<a title='" + js_delete + "' href='#' onClick='deleteMapping(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";
	
	return ' ' + edit + ' '  + del;
}

/**
 * 
 * @param stepnum
 * @return No return value
 */
function gotoStepInCustomMapping(stepnum){
	
	ns.common.gotoWizardStep('customMappingPortlet',stepnum,2);
	
	var tabindex = stepnum - 1;
	$('#customMappingWizardTabs').tabs( 'enable', tabindex );
	$('#customMappingWizardTabs').tabs( 'option','active', tabindex );
	$('#customMappingWizardTabs').tabs( 'disable', (tabindex+1)%3 );
	$('#customMappingWizardTabs').tabs( 'disable', (tabindex+2)%3 );
	
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#customMappingDetails").setInitialFocus();
	}
}

$.subscribe('addCustomMappingFormCompleteTopics', function(event,data) {
	gotoStepInCustomMapping(2);
});

$.subscribe('customMappingBackTopics', function(event,data) {
	gotoStepInCustomMapping(1);
});

$.subscribe('customMappingGridCompleteEvents', function(event,data) {
	ns.common.resizeWidthOfGrids();
	$.publish("common.topics.tabifyNotes");
});

function editMapping(urlString){

	$.ajax({
		url: urlString,
		success: function(data){
			$('#verifyCustomMappingDiv').html(data);
			$.publish("common.topics.tabifyDesktop");
			
			var detailsTabIndex = ns.common.getSectionTabIndex("details");
			
			$("#customMappingDetails").tabify({baseIndex:detailsTabIndex})
			if(accessibility){
				$("#customMappingDetails").setInitialFocus();
			}
		}
	});
	$('#customMappingDetails').show();
	gotoStepInCustomMapping(2);
	$('#mappingDiv').hide();
}

$.subscribe('common.topics.deleteMappingComplete', function(event,data) {
	ns.common.closeDialog("#deleteMappingDialogID");
	$('#custommappingGridID').trigger("reloadGrid");
	ns.common.showStatus(js_status_custom_mapping_deleted);
});

$.subscribe('addMappingSuccess', function(event,data) {
	gotoStepInCustomMapping(1);
	$('#customMappingDetails').show();
	$('#mappingDiv').hide();
});

$.subscribe('addMappingFormSaveComplete', function(event,data) {
	$('#custommappingGridID').trigger("reloadGrid");
	$('#mappingDiv').show();
	$('#customMappingDetails').hide();
	
	$("#addCustomMappingsLink").find("span").removeClass("dashboardSelectedItem");
	$("#ppaymappingLink").find("span").addClass("dashboardSelectedItem");
	
	ns.common.showStatus(js_status_custom_mapping_added);
});

$.subscribe('editMappingFormSaveComplete', function(event,data) {
	$('#custommappingGridID').trigger("reloadGrid");
	$('#mappingDiv').show();
	$('#customMappingDetails').hide();
	ns.common.showStatus(js_status_custom_mapping_modified);
});

function deleteMapping(urlString){

	$.ajax({
		url: urlString,
		success: function(data){
			
			var config = ns.common.dialogs["deleteMapping"];
			var beforeDialogOpen = function(){			
				$('#deleteMappingDialogID').html(data);
				$('#deleteMappingDialogID').dialog('open');
			}
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("deleteMapping");			
		}
	});
}

$.subscribe('cancelAddMapping', function(event,data) {
	gotoStepInCustomMapping(1);
	$('#mappingDiv').show();
	$('#customMappingDetails').hide();
	
	$("#addCustomMappingsLink").find("span").removeClass("dashboardSelectedItem");
	$("#ppaymappingLink").find("span").addClass("dashboardSelectedItem");
	
});

$.subscribe('addCustomMappingsOnCompleteTopics', function(event,data) {
	gotoStepInCustomMapping(1);
	var urlString = "/cb/pages/jsp/custommapping.jsp";
	var MappingDefinitionIDVar="0";
	$.ajax({
		url: urlString,
		type:"POST",
		data: {MappingDefinitionID: MappingDefinitionIDVar, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
		success: function(data){
			$('#inputCustomMappingDiv').html(data);
			$.publish("common.topics.tabifyDesktop");
			
			var detailsTabIndex = ns.common.getSectionTabIndex("details");
			
			$("#customMappingDetails").tabify({baseIndex:detailsTabIndex})
			if(accessibility){
				$("#customMappingDetails").setInitialFocus();
			}
		}
	});
	$('#customMappingDetails').show();
	$('#mappingDiv').hide();
});

$.subscribe('customMappingsOnCompleteTopics', function(event,data) {
	gotoStepInCustomMapping(1);
	$('#addCustomMappingsLink').removeAttr('style');
	$('#summary').hide();
	$('#fileupload').hide();
    $('#mappingDiv').show();
	$('#customMappingsLink').hide();
	
	$.publish("common.topics.tabifyDesktop");
	
	var detailsTabIndex = ns.common.getSectionTabIndex("details");
	
	$("#customMappingDetails").tabify({baseIndex:detailsTabIndex})
	if(accessibility){
		accessibility.setFocusOnDashboard();
	}
});




/* =================================================================================
	Help Functions
==================================================================================== */

ns.common.callHelpFunction = function(token){
	
	var subMenuNameValue = $('#helpSubMenuName').html().replace(/&amp;/g,'_');
	//URL Encryption: Change "get" method to "post" method
	//Edit by Dongning
	var urlString = "/cb/pages/util/GetHelpNameAction.action";
	//added dataType as text to prevent FF error of parsing XML document.
	$.ajax({
		url: urlString,
		type:"POST",
		data: {helpKey: subMenuNameValue,CSRF_TOKEN: token},
		dataType: "text",
		success: function(data){
			$('#helpFileName').html(data);
			var fileName = $('#helpFileName').html();
			callHelp('/cb/web/help/','/cb/web/help/' + fileName);
		}
	});
}


//method to add a help icon to title bar for dialog/grid
$.fn.extend({
	addHelp: function(helpCallback){
		var $header = $(this).parents(".ui-dialog:first").find('.ui-dialog-titlebar');
		if ($header.length === 0) {
			$header = $(this).parents(".ui-jqgrid:first").find('.ui-jqgrid-titlebar');
		}
		if ($header.length === 0) {
			return;
		}
		
		
		var HELP_CLS = "ui-icon ui-icon-help help";
		var $helpIcon = $('<span/>').addClass(HELP_CLS);
		$('<a href="javascript:void(0)" class="helpCtl"/>').append($helpIcon).prependTo($header)
			.addClass('ui-corner-all')
			.hover(
				function() {
					$(this).addClass('ui-state-hover');
				},
				function() {
					$(this).removeClass('ui-state-hover');
				}
			);
	
	
		$header.find('.help').bind('click', function(event) {
			helpCallback();
		});
	}
});





/* =================================================================================
 	File Uploading Functions
==================================================================================== */


/**
 * Implement a delay to start querying uploading progress. In addition, 
 * by setting different statuses to a uploading request according to server
 * response to tell the querying thread what to do. 
 * When a uploading request gets sent, we set the status to "Initiated". 
 * It means that the request just get sent. When querying thread reads this
 * status it can send querying request.
 * "Finished" indicates that the file has been uploaded successfully. The 
 * querying thread can stop immediately and set the progess to 100 percent.
 * "Failed" indicates that the file has failed to upload. The querying thread
 * should stop.
 * 
 * "Initiated": 
 * "Finished": 
 * "Failed"
 */
ns.common.uploadingStatus = new Object();
ns.common.uploadingStatus.INITIATED = "Initiated";
ns.common.uploadingStatus.FINISHED = "Finished";
ns.common.uploadingStatus.FAILED = "Failed";
ns.common.uploadingID = "TellurideUpload";
ns.common.progressBarID = "uploadprogressbar";

/**
 * The method is used by file uploader, 
 * and is used to check the percentage of file size which has been imported to server.
 * If the percentage is up to "100", the heart-beat will exit.
 * 
 */
ns.common.prepareQuery = function(uploadingID, progressBarID, extra1, extra2){

	///alert('prepareQuery: ' + uploadingID);
	if(!uploadingID)
		uploadingID = ns.common.uploadingID;
	if(!progressBarID)
		progressBarID = ns.common.progressBarID; 
		
	$( '#' + progressBarID ).progressBar(
			{ barImage:{0:'/cb/web/images/progressbg_red.gif',
				30:'/cb/web/images/progressbg_orange.gif',
				70:'/cb/web/images/progressbg_green.gif'},
			  boxImage: '/cb/web/images/progressbar.gif'
			});
	
	$( '#' + progressBarID ).fadeIn();
	
	var func = "ns.common.queryProgress('" + uploadingID + "','" + progressBarID + "')";
	//alert(func);
	setTimeout(func, 500);

};

ns.common.queryProgress = function( uploadingID, progressBarID , extra1, extra2) {

	//alert("queryProgress: " + uploadingID + " progressBarID: " + progressBarID);
	
	var urlString = "/cb/pages/fileuploadstatus/getUploadingStatus.action";
	
	var status = ns.common.uploadingStatus[uploadingID].status;
	//alert("queryProgress: " + status);
	
	//Before starting the query thread, check if uploading has already failed/finished
	if(status === ns.common.uploadingStatus.FINISHED){
		//alert("finished");
		$('#' + progressBarID).progressBar("100");
		return;
	}
	if(status === ns.common.uploadingStatus.FAILED){
		//alert("failed");
		return;
	}
	//Not Initiated?
	if(status !== ns.common.uploadingStatus.INITIATED){
		//alert("Not initiated");
		return;
	}
		
	var i = setInterval(function() { 
		
		//Before making each query check if uploading has already failed/finished
		var status = ns.common.uploadingStatus[uploadingID].status;
		if(status === ns.common.uploadingStatus.FAILED){
			//alert("i: failed");
			return;
		}
		if(status === ns.common.uploadingStatus.FINISHED){
			//alert("i: finished");
			$('#' + progressBarID).progressBar("100");
			return;
		}	
		$.ajax({
			  type: "POST",
			  data: {uploadingID: uploadingID, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
			  url: urlString,
			  dataType: 'json',
			  success: function(data, textStatus, xhr){				  
				if (data == null) {
					clearInterval(i);
					return;
				}
			
				var percentage = data.percentage;				
				if (percentage !== ns.common.uploadingStatus[uploadingID].percentage) {
					ns.common.uploadingStatus[uploadingID].percentage = percentage;
				} else {
					if (ns.common.uploadingStatus[uploadingID].trycount) {
						ns.common.uploadingStatus[uploadingID].trycount++;
					} else {
						ns.common.uploadingStatus[uploadingID].trycount = 1;
					}
					if (ns.common.uploadingStatus[uploadingID].trycount === 3) {
						clearInterval(i);
						return;
					}
				}
				$('#' + progressBarID).progressBar(percentage);
				//$("#uploadprogressbar").progressbar( 'value' , parseInt( percentage ) ); 
				if (percentage == "100") {
					clearInterval(i);
					return;
				}
			
			  },
			  
			  error: function(xhr, textStatus, errorThrown){
				  //alert('query failed');
				  clearInterval(i);
			  },
			  
			  beforeSend: function(xhr){
			  },
			
			  complete: function(xhr, textStatus){
			  }
			
			});
		
	}, 1000); //query per 1 second
}


ns.common.closeUploadFileWindow = function(){
	$.publish('closeDialog');
}

ns.common.showUploadedFileName = function(){
	var importedFile = $("#importFileInputID").val();
	var length = importedFile.length;
	var x = importedFile.lastIndexOf("\\");
	x++; 
	var fileName = importedFile.substring(x,length);
	uploadedFile = fileName;
	$("#showImportFileNameID").show();
	$("#showImportFileNameID").html(js_uploading  + fileName);
}

ns.common.checkFileImportResults = function( urlString ){
	var aConfig = ns.common.dialogs["fileImportResults"];
	aConfig.autoOpen = "true";
	ns.common.openDialog("fileImportResults");
	aConfig.autoOpen = "false";
};


/**
 *  the method is for upload file function of upload buttion onclick method
 * @param formID
 */
ns.common.updateRequestParams = function(formID) {    
    var originalURL= $('#' + formID)[0].getAttribute("action");
    if (originalURL.indexOf("?")<0){
	    var submitURL = originalURL + "?";  
	    var inputValue = $('#' + formID)[0].CSRF_TOKEN.value;
	    submitURL = submitURL + "CSRF_TOKEN=" + inputValue;
	    $('#' + formID).attr('action', submitURL); 
    }
    //alert('form action: ' + $('#' + formID).attr('action'));
};

ns.common.updateFilePath = function(){
	$("#fakeImportFileID").val($("#importFileInputID").val());
};

/**
 * Send wire transfer end.
 */
$.subscribe('beforeUploadingFile', function(event,data) {
	
	//alert('beforeUploadingFile');
	ns.common.showLIndicator = false;
	
	var uploadingID = ns.common.uploadingID;
	
	/**
	 * Just before uploading...
	 * Initiate uploading task 
	 */
	eval("ns.common.uploadingStatus." + uploadingID + " = new Object();");
	ns.common.uploadingStatus[uploadingID].status = ns.common.uploadingStatus.INITIATED;
	$("#uploadprogressbar").show();
	ns.common.prepareQuery(uploadingID);

	//Hide the import form
	$("#importedFileFormDivID").hide();
	ns.common.showUploadedFileName();

});

/**
 * Publishes the File Upload Report Display Done button click event to the event listener
 * The listener can then take appropriate action once the Done button on the Report is clicked.
*/
$.subscribe('common.topics.reportDoneListener', function(event,data) {
	$.publish('ach.topics.closeImportACHEntries');
});

/**
  * File Uploading finished.
  * Previously the result page would be displayed automatically. 
  * We'll perform post-uploading tasks here.
  * Progressbar should be finished around this event (asynchronously).
 */
$.subscribe('completeUploadingFile', function(event,data) {	
	ns.common.showLIndicator = true;

	var uploadingID = ns.common.uploadingID;
	var errorMarker = 'EBANKING_FILE_UPLOADING_ERROR: ';
	var errorMarker2 = 'EBANKING_FILE_UPLOADING_ERROR_FILE_TYPE_OR_SIZE: ';
	var errorNoRecordsMarker = "NO_RECORDS_FOUND:";
	var responseText = event.originalEvent.request.responseText;
	var responseStatus = "";
	if(event.originalEvent.request.responseJSON!=undefined)
	{
	responseStatus=event.originalEvent.request.responseJSON.response.status;
	}
	var res = "";
	var response = "";
	var data = "";
	if(responseText!=''){
	try{
		res = $.parseJSON(responseText);
		response=res.response;
		data=response.data;
		}catch(e){}
	}
	
	if(data.indexOf(errorNoRecordsMarker)!==-1){
		ns.common.showError(data.substr(errorNoRecordsMarker.length));
		ns.common.closeDialog('openFileImportDialogID');
	}
		
	if (responseText.startsWith(errorMarker)){
		ns.common.uploadingStatus[uploadingID].status = ns.common.uploadingStatus.FAILED;
		$("#showImportFileNameID").html("Failed to upload file " + uploadedFile);
		$("#showImportFileNameID").after("<br><a onclick='ns.common.closeUploadFileWindow();' id='closeDialogButton' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' role='button' aria-disabled='false'><span class='ui-button-text'>"+js_CLOSE+"</span></a>");
		ns.common.showStatus(responseText.substr(errorMarker.length));
		/**Close Upload File Window once ERROR message displayed.*/
		ns.common.closeUploadFileWindow();
		//For transfer and billpay the inputDiv will be populated with transaction info upon success,
		//but upon failure the error message will be sent to inputDiv as well. Just to clear it for now.
		if(!ns.common.popupAfterUpload) $('#inputDiv').html('');
		
	} else if (responseText.startsWith(errorMarker2)){
		ns.common.uploadingStatus[uploadingID].status = ns.common.uploadingStatus.FAILED;
		$("#uploadprogressbar").hide();
		
		if($("#importedFileFormDivID").length>0){
			$("#showImportFileNameID").hide();
			$("#importedFileFormDivID").show();
			$('#errorFileType').html(responseText.substr(errorMarker2.length));
		}else{
			var tempContent = responseText.substr(errorMarker2.length);
			ns.common.handleErrorMessage($(tempContent).find("span").html());
		}

		if(!ns.common.popupAfterUpload) $('#inputDiv').html('');

	} else {
		ns.common.uploadingStatus[uploadingID].status = ns.common.uploadingStatus.FINISHED;
		$("#showImportFileNameID").html("Uploaded " + uploadedFile);
		$("#showImportFileNameID").after("<br><a onclick='ns.common.checkFileImportResults()' href='javascript:void(0)'>Check Import Results</a>");
		//TODO: close upload widget (dialog) - now it will be closed when closing the result page
		//popup the result page directly because it may contain information for confirmation
			
		//ns.common.jqueryfyImportReport();
		//ns.common.jqueryfyImportConfirm();
		//ns.common.jqueryWireImportView();
		
		if (responseStatus=='error') {
			$("#checkFileImportResultsDialogID").dialog("close");
			ns.common.closeDialog('openFileImportDialogID');			
		} else {
			if (ns.common.popupAfterUpload) {
				ns.common.closeDialog('openFileImportDialogID');
				ns.common.checkFileImportResults();
			} 
		}
	}
		
});

/**
 * Uploading succeeded
 */
$.subscribe('successUploadingFile', function(event,data) {
	//alert('uploading success');
});

/**
 * Uploading Failed
 */
$.subscribe('errorUploadingFile', function(event,data) {
	//alert('uploading error');

	var uploadingID = ns.common.uploadingID;
	ns.common.uploadingStatus[uploadingID].status === ns.common.uploadingStatus.FAILED;

});

/**
 * As the response page for file uploading is returned to the iframe created for uploading, and 
 * the iframe doesn't recognize jQuery, it will cause javascript errors. So we have to call these
 * functions manually after uploading succeeds.
 */
ns.common.jqueryfyImportReport = function(){	
	if (jQuery('#anchor_displayreport2_done').length > 0) {
		var options_anchor_displayreport2_done = {};
		options_anchor_displayreport2_done.jqueryaction = "anchor";
		options_anchor_displayreport2_done.id = "anchor_displayreport2_done";
		options_anchor_displayreport2_done.href = "#";
		options_anchor_displayreport2_done.onclicktopics = "closeDialog,common.topics.reportDoneListener";
		options_anchor_displayreport2_done.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_displayreport2_done'),options_anchor_displayreport2_done);
	}
	
	if (jQuery('#anchor_displayreport2_export').length > 0) {
		var options_anchor_displayreport2_export = {};
		options_anchor_displayreport2_export.jqueryaction = "anchor";
		options_anchor_displayreport2_export.id = "anchor_displayreport2_export";
		options_anchor_displayreport2_export.href = "#";
		options_anchor_displayreport2_export.onclicktopics = "exportFileImportingReport";
		options_anchor_displayreport2_export.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_displayreport2_export'),options_anchor_displayreport2_export);
	}
	
	if (jQuery('#anchor_displayreport2_printerready').length > 0) {
		var options_anchor_displayreport2_printerready = {};
		options_anchor_displayreport2_printerready.jqueryaction = "anchor";
		options_anchor_displayreport2_printerready.id = "anchor_displayreport2_printerready";
		options_anchor_displayreport2_printerready.href = "#";
		options_anchor_displayreport2_printerready.indicatorid = "indicator";
		options_anchor_displayreport2_printerready.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_displayreport2_printerready'),options_anchor_displayreport2_printerready);
	}
	
	if (jQuery('#anchor_displayreport2_done2').length > 0) {
		var options_anchor_displayreport2_done2 = {};
		options_anchor_displayreport2_done2.jqueryaction = "anchor";
		options_anchor_displayreport2_done2.id = "anchor_displayreport2_done2";
		options_anchor_displayreport2_done2.href = "#";
		options_anchor_displayreport2_done2.onclicktopics = "closeDialog";
		options_anchor_displayreport2_done2.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_displayreport2_done2'),options_anchor_displayreport2_done2);
	}
	
	if (jQuery('#printerReadyReportDialog').length > 0) {
		var options_printerReadyReportDialog = {};
		options_printerReadyReportDialog.height = 600;
		options_printerReadyReportDialog.width = 700;
		options_printerReadyReportDialog.title = "Print Window";
		options_printerReadyReportDialog.buttons = { 
	    		 	'CANCEL':function() { ns.common.closeDialog(); } 
				,	'PRINT': function(){ printReport();}
	   		};
		options_printerReadyReportDialog.autoOpen = false;
		options_printerReadyReportDialog.backgroundColor = "#000";
		options_printerReadyReportDialog.opacity = 0.7;
		options_printerReadyReportDialog.modal = true;
		options_printerReadyReportDialog.jqueryaction = "dialog";
		options_printerReadyReportDialog.id = "printerReadyReportDialog";
		options_printerReadyReportDialog.href = "#";
		jQuery.struts2_jquery.bind(jQuery('#printerReadyReportDialog'),options_printerReadyReportDialog);
	}
	
    $("#GenerateReportBaseFormat").selectmenu({width: 170});

}

/**
 * As the response page for file uploading is returned to the iframe created for uploading, and 
 * the iframe doesn't recognize jQuery, it will cause javascript errors. So we have to call these
 * functions manually after uploading succeeds.
 */
ns.common.jqueryfyImportConfirm = function(){
	
	//ACH confirmation
	if (jQuery('#anchor_importconfirm_cancel').length > 0) {
		var options_anchor_importconfirm_cancel = {};
		options_anchor_importconfirm_cancel.jqueryaction = "anchor";
		options_anchor_importconfirm_cancel.id = "anchor_1984135008";
		options_anchor_importconfirm_cancel.href = "#";
		options_anchor_importconfirm_cancel.onclicktopics = "closeDialog";
		options_anchor_importconfirm_cancel.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_importconfirm_cancel'),options_anchor_importconfirm_cancel);
	}

	
	if (jQuery('#anchor_importconfirm_continue').length > 0) {
		var options_anchor_importconfirm_continue = {};
		options_anchor_importconfirm_continue.jqueryaction = "anchor";
		options_anchor_importconfirm_continue.id = "anchor_importconfirm_continue";
		options_anchor_importconfirm_continue.oncompletetopics = "completeConfirmUploadedAchEntryProcess";
		options_anchor_importconfirm_continue.onsuccesstopics = "successConfirmUploadedAchEntryProcess";
		options_anchor_importconfirm_continue.onerrortopics = "errorConfirmUploadedAchEntryProcess";
		options_anchor_importconfirm_continue.onbeforetopics = "beforeConfirmUploadedAchEntryProcess";
		options_anchor_importconfirm_continue.targets = "checkFileImportResultsDialogID";
		options_anchor_importconfirm_continue.href = "#";
		options_anchor_importconfirm_continue.formids = "achentryuploadconfirmform";
		options_anchor_importconfirm_continue.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_importconfirm_continue'),options_anchor_importconfirm_continue);
	}

	//Transfer confirmation
	if (jQuery('#anchor_xferimport_confirm_cancel').length > 0) {
		var options_anchor_xferconfirm_cancel = {};
		options_anchor_xferconfirm_cancel.jqueryaction = "anchor";
		options_anchor_xferconfirm_cancel.id = "anchor_xferimport_confirm_cancel";
		options_anchor_xferconfirm_cancel.href = "#";
		options_anchor_xferconfirm_cancel.onclicktopics = "cancelForm";
		options_anchor_xferconfirm_cancel.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_xferimport_confirm_cancel'),options_anchor_xferconfirm_cancel);
	}

	
	if (jQuery('#anchor_xferimport_confirm_continue').length > 0) {
		var options_anchor_xferconfirm_continue = {};
		options_anchor_xferconfirm_continue.jqueryaction = "anchor";
		options_anchor_xferconfirm_continue.id = "anchor_xferimport_confirm_continue";
		options_anchor_xferconfirm_continue.oncompletetopics = "completeConfirmUploadedTransfersProcess";
		options_anchor_xferconfirm_continue.onsuccesstopics = "successConfirmUploadedTransfersProcess";
		options_anchor_xferconfirm_continue.onerrortopics = "errorConfirmUploadedTransfersProcess";
		options_anchor_xferconfirm_continue.onbeforetopics = "beforeConfirmUploadedTransfersProcess";
		options_anchor_xferconfirm_continue.targets = "inputDiv";
		options_anchor_xferconfirm_continue.href = "#";
		options_anchor_xferconfirm_continue.formids = "transferuploadconfirmform";
		options_anchor_xferconfirm_continue.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_xferimport_confirm_continue'),options_anchor_xferconfirm_continue);
	}
	
	//Billpay confirmation
	if (jQuery('#anchor_billimport_confirm_cancel').length > 0) {
		var options_anchor_billconfirm_cancel = {};
		options_anchor_billconfirm_cancel.jqueryaction = "anchor";
		options_anchor_billconfirm_cancel.id = "anchor_billimport_confirm_cancel";
		options_anchor_billconfirm_cancel.href = "#";
		options_anchor_billconfirm_cancel.onclicktopics = "cancelBillpayForm";
		options_anchor_billconfirm_cancel.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_billimport_confirm_cancel'),options_anchor_billconfirm_cancel);
	}

	
	if (jQuery('#anchor_billimport_confirm_continue').length > 0) {
		var options_anchor_billconfirm_continue = {};
		options_anchor_billconfirm_continue.jqueryaction = "anchor";
		options_anchor_billconfirm_continue.id = "anchor_billimport_confirm_continue";
		options_anchor_billconfirm_continue.oncompletetopics = "completeConfirmUploadedPaymentsProcess";
		options_anchor_billconfirm_continue.onsuccesstopics = "successConfirmUploadedPaymentsProcess";
		options_anchor_billconfirm_continue.onerrortopics = "errorConfirmUploadedPaymentsProcess";
		options_anchor_billconfirm_continue.onbeforetopics = "beforeConfirmUploadedPaymentsProcess";
		options_anchor_billconfirm_continue.targets = "inputDiv";
		options_anchor_billconfirm_continue.href = "#";
		options_anchor_billconfirm_continue.formids = "paymentuploadconfirmform";
		options_anchor_billconfirm_continue.button = true;
		jQuery.struts2_jquery.bind(jQuery('#anchor_billimport_confirm_continue'),options_anchor_billconfirm_continue);
	}
	
}

/**
 * This method is used for wire file upload,view the import result
 */
ns.common.jqueryWireImportView = function(){
	if (jQuery('#anchor_wireupload_view_cancel').length > 0) {
		var options_anchor_wireupload_view_cancel = {};
		options_anchor_wireupload_view_cancel.jqueryaction = "anchor";
		options_anchor_wireupload_view_cancel.id = "anchor_wireupload_view_cancel";
		options_anchor_wireupload_view_cancel.href = "#";
		options_anchor_wireupload_view_cancel.onclicktopics = "wireUploadCancelOnClickTopics";
		options_anchor_wireupload_view_cancel.button = true;
	    jQuery.struts2_jquery.bind(jQuery('#anchor_wireupload_view_cancel'),options_anchor_wireupload_view_cancel);
	}
	if (jQuery('#submitFileImportFormButtonID').length > 0) {
		var options_submitFileImportFormButtonID = {};
		options_submitFileImportFormButtonID.jqueryaction = "anchor";
		options_submitFileImportFormButtonID.id = "submitFileImportFormButtonID";
		options_submitFileImportFormButtonID.onerrortopics = "errorImportingWiresFileFormTopics";
		options_submitFileImportFormButtonID.targets = "checkFileImportResultsDialogID";
		options_submitFileImportFormButtonID.href = "#";
		options_submitFileImportFormButtonID.formids = "submitFileImportFormID";
		options_submitFileImportFormButtonID.button = true;
	    jQuery.struts2_jquery.bind(jQuery('#submitFileImportFormButtonID'),options_submitFileImportFormButtonID);
	}
}

/**
 * The report will be displayed after user clicks "CONTINUE" on confirm page
 */
$.subscribe('successConfirmUploadedAchEntryProcess', function(event,data) {
	//ns.common.jqueryfyImportReport();
});

$.subscribe('exportFileImportingReport', function(event,data) {
	$("#innerExportReportForm2").submit();
});


$.subscribe('errorOpenFileUploadTopics', function(event,data){
	var resStatus = event.originalEvent.request.status;
	if(resStatus === 700) //700 indicates interval validation error
		ns.common.openDialog("authenticationInfo");
});



/* =================================================================================
	"Smart" Date Picker Functions for extending ui.datepicker Ajax features.
	Use Smart Calendar to disable non-business days
==================================================================================== */

/**
 * Enable ui.datepicker for Ajax functionality. 
 * Register several callbacks to ui.datepicker for firing Ajax requests.
 * @param calId Id of the date picker
 * @param urlStr Encrypted URL for retrieving date information per month
 */
ns.common.enableAjaxDatepicker = function(calId, urlStr){
	$('#' + calId).attr('calUrl', urlStr);
	$('#' + calId).datepicker('option', 'onChangeMonthYear', ns.common.datepicker_onChangeMonthYearHandler);
	$('#' + calId).datepicker('option', 'beforeShow', ns.common.datepicker_beforeShowHandler);
	$('#' + calId).datepicker('option', 'beforeShowDay', ns.common.datepicker_beforeShowDayHandler);
	
	// Make the input keyup event no more upate the calendar data.
	$('#' + calId).unbind('keyup');
	
	if( $('#' + calId).width() == 0 )
		$('#' + calId).width(110) ;
};

/**
 * Send an Ajax request to server to get the date information for a month.
 * @param urlStr Encrypted URL for retrieving date information per month
 */
ns.common.refreshCalendar = function(urlstr){
	$.ajax({
		url: urlstr,
		async: false,
		cache: false,
        timeout: 30000,
		success: function(data){
			if($('#ui-datepicker-div-infos').size() == 0 )
				$("<div id='ui-datepicker-div-infos' style='display:none'/>")
					.insertAfter('#ui-datepicker-div');

			$('#ui-datepicker-div-infos').html(data);
		}
	});
};

/**
 * Get the earliest and latest date in a month from the response from server
 */
ns.common.datepicker_getMinMaxDate = function(){
	
	var data = $('#ui-datepicker-div-infos').html();
	if(data == null)
		return null;

	var year = $(data).children('ul[name="calInfo"]').children('li[name="year"]').html();
	var month = $(data).children('ul[name="calInfo"]').children('li[name="month"]').html();
	var today = $(data).children('ul[name="calInfo"]').children('li[name="today"]').html();
	var serverDate =  $(data).children('ul[name="calInfo"]').children('li[name="serverDate"]').html();
	var minDate = $(data).children('ul[name="calInfo"]').children('li[name="minDate"]').html();
	var maxDate = $(data).children('ul[name="calInfo"]').children('li[name="maxDate"]').html();


	var options = {};
	
	var removeLink = $(data).children('ul[name="calInfo"]').children('li[name="removeLink"]').html();

	if(removeLink == 'previous')
		options.minDate = new Date(parseInt(year), parseInt(month) - 1, 1);

	if(removeLink == 'next')
		options.maxDate = new Date(parseInt(year), parseInt(month), 0);

	var sDate = new Date(serverDate);
	
	if(minDate){
		var tmpMinDate = new Date(sDate);
		options.minDate = ns.common.datepicker_getAdjustedDate(tmpMinDate,minDate,true);
	}
	
	if(maxDate){
		var tmpMaxDate = new Date(sDate);
		options.maxDate =ns.common.datepicker_getAdjustedDate(tmpMaxDate,maxDate,false);
	}	

	return options;
};


ns.common.datepicker_getAdjustedDate = function(aDate,dateSettings,adjustToPriorDate){
	//console.info("ns.common.datepicker_getAdjustedDate");
	var mode,modeVal;
	mode = dateSettings.charAt(dateSettings.length-1);
	mode = mode.toUpperCase();

	modeVal = dateSettings.substring(0,dateSettings.length-1);
	modeVal = Number(modeVal);


	switch(mode){
		case "D":
				if(adjustToPriorDate){
					aDate.setDate(aDate.getDate()-modeVal);
				}else{
					aDate.setDate(aDate.getDate()+ modeVal);
				}
				break;

		case "W":
				if(adjustToPriorDate){
					aDate.setDate(aDate.getDate()-modeVal*7);
				}else{
					aDate.setDate(aDate.getDate()+ modeVal*7);
				}
				break;
		case "M":
				if(adjustToPriorDate){
					aDate.setMonth(aDate.getMonth() - modeVal);
				}else{
					aDate.setMonth(aDate.getMonth() + modeVal);
				}
				break;
		case "Y":
				if(adjustToPriorDate){
					aDate.setFullYear(aDate.getFullYear() - modeVal);
				}else{
					aDate.setFullYear(aDate.getFullYear() + modeVal);
				}						
	}
	return aDate;
}

/**
 * An event handler registered by calling ns.common.enableAjaxDatepicker.
 * Called before the date picker is shown to get the date information. The handler signature is defined
 * by jQuery ui.datepicker.
 * @param input the javascript object corresponding to the text field  
 * @param inst ui.datepicker instance
 */
ns.common.datepicker_beforeShowHandler = function(input, inst){
	var urlStr = $( '#' + inst.id ).attr('calUrl');
	urlStr = urlStr + "&calUserDate=" +  $(input).val();
	ns.common.refreshCalendar(urlStr);
	var opts = ns.common.datepicker_getMinMaxDate(); 
	return opts;
};

/**
 * An event handler registered by calling ns.common.enableAjaxDatepicker.
 * Called when year or month is changed to get the date information. The handler signature is defined
 * by jQuery ui.datepicker.
 * @param year current selected year
 * @param month current selected month
 * @param inst ui.datepicker instance
 */
ns.common.datepicker_onChangeMonthYearHandler = function(year, month, inst){
	var urlStr = $( '#' + inst.id ).attr('calUrl'); 
	urlStr = urlStr + "&calStartYear="+year+"&calStartMonth="+month;
	ns.common.refreshCalendar(urlStr);

	var minMaxDate = ns.common.datepicker_getMinMaxDate();
	
	if(minMaxDate.minDate != undefined)
		inst.settings.minDate = minMaxDate.minDate; 
	if(minMaxDate.maxDate != undefined)
		inst.settings.maxDate = minMaxDate.maxDate;
};

/**
 * An event handler registered by calling ns.common.enableAjaxDatepicker.
 * Called before showing each cell in the calendar. The handler signature is defined
 * by jQuery ui.datepicker.
 * @param date date object for a specific cell in the calendar
 */
ns.common.datepicker_beforeShowDayHandler = function(date){
	
	var display = $('#ui-datepicker-div-infos').find('ul[name="calInfo"]').children( 'li[name="display"]' ).html();
	if( display == "default" )  	// default mode
	{
		return [ true, '' ];
	} 
	else { 							// not default mode
		var isSelectableStr = $('#ui-datepicker-div-infos')
							.find('ul[name="daysInfo"]')
							.children('li[date="' + date.getDate() + '"]').html();

		var isSelectable = 	(isSelectableStr == "true");
		return [ isSelectable , '' ];															
	}
};


/* =================================================================================
  	Grid Functions
==================================================================================== */

//Restore grid preferences
ns.common.gridRestore = function(event,data){	
	var gridID = data.id;
	if (!gridID.startsWith('#'))
		gridID = '#' + gridID;
	
	//Get the column permuations from saved custom variable
	var columnPermutation =  $(gridID).jqGrid("getGridParam", "columnPermutation");
	if(columnPermutation){
		$(gridID).jqGrid("remapColumns", columnPermutation, true);
	}
	
	//Save grid column ordering when user drags n drops column using mouse
	/*$('.ui-jqgrid-hbox table, '+gridID).parents('.ui-jqgrid-view').bind("sortstop", function () {
		ns.common.saveColumnState(gridID);
	});*/
};

//Show toggle action columns on grid
ns.common.toggleGridActionColumn = function(event,data){	
	var gridID = data.id;
	if (!gridID.startsWith('#')) {
		gridID = '#' + gridID;
	}
	
	if($(gridID).hasClass("skipActionHolderDivAppend")) {
		return;
	}

	// Div to hold action buttons and display on each row's roll over
	var lActionItemHolderDiv = "<div id='"+(data.id)+"ActionItemHolder' class='gridActionColumnHolderDiv'></div>";
	var lActionItemHolderDivRef = "#"+data.id+"ActionItemHolder";
	var actionsCell = "";
	
	// Adding action item holder div at the end of the grid control
	$("#gbox_"+ data.id).after(lActionItemHolderDiv);
	
	if(ns.home.isDevice) {
		$(gridID + ' .jqgrow').click(function(e) {
			if($(this).hasClass("skipRowActionItemRender")) {
				// skipping the row added in transfer showing reject reason or submitted row
				return;
			}

			actionsCell = $(gridID).find("#"+$(this).attr("id")).find("td[class='__gridActionColumn']");
			
			if($(actionsCell)) {
				$(this).append($(lActionItemHolderDivRef));
				$(lActionItemHolderDivRef).html($(actionsCell).html());
				$(lActionItemHolderDivRef).show();//Make div visible if it was made invisible during select columns process
				$(lActionItemHolderDivRef).css("right", 0).css({"line-height": "20px", "margin-top": "5px"}).addClass("gridActionColumnHolderDivHover");
			}
		}).mouseleave(function(e) {
			actionsCell = $(gridID).find("#"+$(this).attr("id")).find("td[class='__gridActionColumn']");
			if($(actionsCell)) {
				$(lActionItemHolderDivRef).removeClass("gridActionColumnHolderDivHover");
			}
		});
	} else {
		$(gridID + ' .jqgrow').mouseenter(function(e) {
			if($(this).hasClass("skipRowActionItemRender")) {
				// skipping the row added in transfer showing reject reason or submitted row
				return;
			}

			actionsCell = $(gridID).find("#"+$(this).attr("id")).find("td[class='__gridActionColumn']");
			
			if($(actionsCell)) {
				$(this).append($(lActionItemHolderDivRef));
				$(lActionItemHolderDivRef).html($(actionsCell).html());
				$(lActionItemHolderDivRef).show();//Make div visible if it was made invisible during select columns process
				$(lActionItemHolderDivRef).css("right", 0).css({"line-height": "20px", "margin-top": "5px"}).addClass("gridActionColumnHolderDivHover");
			}
		}).mouseleave(function(e) {
			actionsCell = $(gridID).find("#"+$(this).attr("id")).find("td[class='__gridActionColumn']");
			if($(actionsCell)) {
				$(lActionItemHolderDivRef).removeClass("gridActionColumnHolderDivHover");
			}
		});		
	}
};

if(ns.home.isDevice) {
	/*$(".dashboardSubmenuItemCls").click(function() {
		event.stopImmediatePropagation();
		event.stopPropagation();
		return;
	});*/
}

//common topic getting called on each grid complete event
$.subscribe('commonGridComplete', function(event,data) {	
	var gridID = data.id;
	if (!gridID.startsWith('#'))
		gridID = '#' + gridID;
	
	//Create toggle like action columns
	ns.common.toggleGridActionColumn(event,data);
	
	//Update onPaging and onSortCol custom event handlers
	ns.common.updateCustomPagingAndSorting(event,data);
	
	//Update paging to more useful paging
	ns.common.updateGridPaging(event,data);
	
	//Show no data message and hide grid
	//ns.common.showEmptyGridMessage(event,data);
	
	//Restore grid preferences
	ns.common.gridRestore(event,data);
	
	//Resize thr grid back it needs to be since we have composed pager after grid has completely rendered
	$(gridID).resize();
});

/**
* Common Grid Error Topic to handle the jqGrid action returning error
* If error is encountered for grid action clear the grid data
*/
$.subscribe('commonGridErrorTopic', function(event,data) {	
	if(data && data.id){
		var gridId = data.id;	
		$("#" +  data.id).jqGrid('clearGridData');
	}
});


ns.common.updateCustomPagingAndSorting = function(event,data){
	var gridID = data.id;
	var fnCustomPaging = $("#" + gridID).data("onCustomPaging");
	if(fnCustomPaging != undefined) {
		if($('#'+gridID).jqGrid('getGridParam', 'onPaging') == null) {
			$('#'+gridID).jqGrid('setGridParam', { onPaging : fnCustomPaging });
		}
	}
	var fnCustomSortCol = $("#" + gridID).data("onCustomSortCol");
	if(fnCustomSortCol != undefined) {
		if($('#'+gridID).jqGrid('getGridParam', 'onSortCol') == null) {
			$('#'+gridID).jqGrid('setGridParam', { onSortCol : fnCustomSortCol });
		}
	}
};

ns.common.showEmptyGridMessage = function(event,data){
	var gridID = data.id;
	var gridContainerId = "#gbox_"+ gridID;
	
	var noDataMsgDiv = "<div id='"+(data.id)+"NoDataMsg' class='noDataMsg'>No records present.</div>";
	
	var recs = parseInt($("#"+gridID).jqGrid('getGridParam','reccount'),10);
    if(isNaN(recs) || recs == 0) {
         $(gridContainerId).hide();
         $(gridContainerId).after(noDataMsgDiv);
    }else {
         $(gridContainerId).show();
         $("#"+(data.id)+"NoDataMsg").html('');
         $("#"+(data.id)+"NoDataMsg").remove();
    }
};

ns.common.updateGridPaging = function(event,data){
	var gridID = data.id;
	var pager = gridID + "_pager";
	
	var isHomePageGrid = ns.common.checkForHomePageGrid(gridID);
	if(isHomePageGrid){
		ns.common.updatePagerWithModuleLink(pager,gridID);
	}else{
		ns.common.jqgridCreatePager(pager,gridID,2);
	}
};

ns.common.updatePagerWithModuleLink = function(pagernav,gridID){
    // hiding previous and next buttons
    $("#prev_"+gridID+"_pager").css("display", "none");
    $("#next_"+gridID+"_pager").css("display", "none");
    $("#first_"+gridID+"_pager").css("display", "none");
    $("#last_"+gridID+"_pager").css("display", "none");
    
	$("#" + gridID + "_pager_center").find(".ui-pg-selbox").hide();
	$('#'+pagernav+' #'+pagernav+'_center td:has(input)').attr('id','pager');
	$('#'+pagernav+' #'+pagernav+'_center #pager').addClass("portletGridGoToModuleLink").html(js_view_all);
	$('#'+pagernav+' #'+pagernav+'_center #pager').on("click", function() {
		$("#"+gridID+"_GotoModule").click();
	});
	
};

ns.common.checkForHomePageGrid = function(gridID){
	var isHomePageGrid = false;
	var isHomePageElementCount = $("#desktopContainer").find('#'+gridID).length;
	if(isHomePageElementCount > 0){
		isHomePageGrid = true;
	}
	return isHomePageGrid;
};

ns.common.jqgridCreatePager = function(pagernav,navgrid,pages){
	//console.log("jqgridCreatePager");
    $('#'+pagernav+' #'+pagernav+'_center td:has(input)').attr('id','pager');
    
    var td = $('#'+pagernav+' #'+pagernav+'_center #pager').html('');
    var page = parseInt(jQuery("#"+navgrid).jqGrid('getGridParam','page'));
    var lastPage = parseInt(jQuery("#"+navgrid).jqGrid('getGridParam','lastpage'));
    
    $('#'+pagernav+' #'+pagernav+'_center').css("width", "auto");
    
    var text='';
    
    if(page-pages > 1){
            text+= ns.common.jqgridCreatePageLink(navgrid,1);
            text+= ' ... ';
    }
    
    for(var i=0;i <pages;i++){
        if(page-pages+i >=1)
            text+= ns.common.jqgridCreatePageLink(navgrid,page-pages+i);
    }

    text += ns.common.jqgridCreatePageLink(navgrid,page,true);

    for(var i=0;i <pages;i++){
        if(page+i+1 <= lastPage) 
            text += ns.common.jqgridCreatePageLink(navgrid,page+i+1);
    }

    if(page+pages <= lastPage){
        text+= ' ... ';
        text+= ns.common.jqgridCreatePageLink(navgrid,lastPage);

    }
    
    // hiding previous and next buttons
    $("#prev_"+navgrid+"_pager").css("display", "none");
    $("#next_"+navgrid+"_pager").css("display", "none");
    
    // styling first and last buttons
    $("#first_"+navgrid+"_pager span").removeClass().html(js_page_first).addClass("gridPagerFirstLastBtn");
    $("#last_"+navgrid+"_pager span").removeClass().html(js_page_last).addClass("gridPagerFirstLastBtn");
    
    $('#'+pagernav+' #'+pagernav+'_center table').addClass("gridPagerControlCls");
    
    var td = $('#'+pagernav+' #'+pagernav+'_center #pager').html(text);
};

ns.common.jqgridCreatePageLink = function (navgrid,page,current){
	//console.log("jqgridCreatePageLink");
    if(!current){
    	var fnCustomPaging = $("#" + navgrid).data("onCustomPaging");
    	if(fnCustomPaging != undefined) {
    		return ' <a href="#" onclick="jQuery(\'#'+navgrid+'\').data(\'onCustomPaging\').call(this, '+page+')" class="gridPagerPgNumberCls">'+page+'</a> ';
    	}
    	return ' <a href="#" onclick="jQuery(\'#'+navgrid+'\').jqGrid(\'setGridParam\',{page:'+page+'}).trigger(\'reloadGrid\')" class="gridPagerPgNumberCls">'+page+'</a> ';
    }else{
    	return "<span class='gridPagerCurrentSelectedPg'>"+page+"</span>";
    }
};

$.subscribe('gridSortingComplete', function(event,data) {
	var gridID = data.id;
	if (!gridID.startsWith('#')){
		gridID = '#' + gridID;
	}
	ns.common.saveColumnState(gridID);
});


$.subscribe('addGridControlsEvents', function(event,data) {		
	var gridID = data.id;
	if (!gridID.startsWith('#'))
		gridID = '#' + gridID;
	var navButtonFlag = $(gridID).data("isGridControlAdded");	
	var supportSearch = $(gridID).data("supportSearch") ? true : false;

	//setting left pager width to auto 
	$(gridID+"_pager_left").width('auto');
	
	if(!navButtonFlag){
		ns.common.addGridControls(gridID,supportSearch);
	}
});

/**
 * This method adds Column Chooser, Refresh, and Search function to grids
 * @param gridID ID of the grid
 * @param supportSearch whether to add Search control 
 */
ns.common.addGridControls = function(gridID, supportSearch){	
	if (!gridID.startsWith('#'))
		gridID = '#' + gridID;
	
	$(gridID).jqGrid('navButtonAdd', gridID + "_pager", {
		caption: jQuery.i18n.prop("js_col_title"),
	    title: jQuery.i18n.prop("js_col_title"),
	    buttonicon: 'ui-icon-newwin',	    
	    onClickButton: function(){
	    	//taking original width of Grid holder div.
	    	var gridHolderDivId = gridID;
	    	if (gridID.startsWith('#')){
	    		gridHolderDivId = gridID.substring(1);
	    	}
	    	gridHolderDivId = "#gbox_"+gridHolderDivId;
	    	var widthOriginal = $(gridHolderDivId).width();
	    	//Open Column chooser
            $(gridID).jqGrid('columnChooser',{
				//This function will be called when either Cancer or Ok is clicked.
            	done: function(perm) {
            		$(gridID).data("columnOrder"+gridID,perm);            		            		
				    if (perm) {
				    	$(".gridActionColumnHolderDiv").hide();//Make this div hidden since it causes UI problems after grid columns are rendered again.
				    	 $(this).jqGrid("remapColumns", perm, true);
				    	//CR 779098 -  Reverting the fix of CR 775280 . Grids has been given different names according to type of account.
				    	// If user agent is view only obo then do not send save grid column request to server 
				    	 if(ns.common.isViewOnlyOBO != 'true') {
								ns.common.saveColumnState(gridID, perm);
						 }
				    }
				    //Hide overlay
				    ns.common.hideColumnChooserLoading();
				    //setting width of grid again to original, so that grid would be visible same 
				    //and none of navigator button would hide.
				    //Note: This is fix need while jqGrid update to version 4.3, as grid width is decreases on reducing number of columns 
				    //causing navigator not visible or reducing.
				    $(this).jqGrid("setGridWidth",widthOriginal,true);				    				   
            	},
            	dlog_opts:function(opts){
					var buttons = {};
					opts.bCancel = jQuery.i18n.prop("js_CANCEL");
					opts.bSubmit = jQuery.i18n.prop("js_OK");
					buttons[opts.bCancel] = function() {
						opts.cleanup(true);
					};
					buttons[opts.bSubmit] = function() {
						opts.apply_perm();
						opts.cleanup(false);
					};
					return {
						"buttons": buttons,
						"close": function() {
							opts.cleanup(true);
						},
						"modal" : false,
						"resizable": false,
						"width": opts.width+20
					};
				}
            });
            
            //Call functionality to show disabled background with overlay.           
            setTimeout("ns.common.disableBackgroundForColumnChooser('"+this.id+"')",450);
	    }
	});
	
	//Add refresh button.
	$(gridID).jqGrid('navButtonAdd', gridID + "_pager", {
		caption: jQuery.i18n.prop("js_refersh_title"),
		title: jQuery.i18n.prop("js_refersh_title"),
		buttonicon: 'ui-icon-refresh',		
		onClickButton: function(){
			var fnCustomRefresh = $(gridID).data("fnCustomRefresh");
			if(fnCustomRefresh != undefined && $.isFunction(fnCustomRefresh)){
				if(typeof fnCustomRefresh === 'function'){
					fnCustomRefresh.call();
				}	
			}else{
				$(gridID)[0].clearToolbar();	
			}
		}
	});	
			
	//Add search button.
	if( supportSearch ){
		//Add search button.
		$(gridID).jqGrid('navButtonAdd', gridID + "_pager", {
		    caption: $.jgrid.search.caption,
		    title: $.jgrid.search.caption,
		    buttonicon: 'ui-icon-search',
		    onClickButton: function(){
				$(gridID)[0].toggleToolbar();
		    }
		});
	}
	$(gridID).jqGrid('filterToolbar');
	if($(gridID)[0]){
		$(gridID)[0].toggleToolbar();	
	}
	$(gridID).data("isGridControlAdded",true);
	
	$(gridID).removeData("supportSearch");

	//disable sortable rows
	if($(gridID).data("ui-jqgrid") != undefined){
		$(gridID + " tbody").sortable("destroy");
	}
	
};

	ns.common.saveColumnState = function (gridID,perm) {	
		var gridConfigJson = ns.common.getGridSettings(gridID);	    
	    var gridPrefToSave = ns.common.prepareGridPreference(gridID,gridConfigJson);
	    
	    $.ajax({
			url: '/cb/pages/jsp/personalization/gridConfigAction_saveGridPreference.action?CSRF_TOKEN='+ ns.home.getSessionTokenVarForGlobalMessage,
			type:"POST",
			data: {gridConfiguration:JSON.stringify(gridPrefToSave)},			 
			success: function(data) {
				//console.info('Grid Prefernce saved successfully.');
			}
		});
	};
	
	
	ns.common.getGridSettings = function(gridID){
		var gridConfigJson ='';
		var jsonstr = $(gridID).jqGrid('jqGridExport', {exptype:'jsonstring'});	    
	    if (jsonstr && typeof jsonstr === 'string') {
			var _jsonparse = false;
			if($.jgrid.useJSON) {
				$.jgrid.useJSON = false;
				_jsonparse = true;
			}
			gridConfigJson = $.jgrid.parse(jsonstr);
			if(_jsonparse) { $.jgrid.useJSON = true; } 
			            
        }
	    return gridConfigJson;
	};
	
	ns.common.prepareGridPreference = function(gridID,gridConfigJson){		
		var colModel = gridConfigJson.grid.colModel;
	    var i;
	    var l = colModel.length;
	    var colItem;
	    var cmName;	
	    //var colstates = new Array();
	    
	    var gridConfig = {
	            //search: $(gridID).jqGrid('getGridParam', 'search'),
	            //page: gridConfigJson.page,
				id		:gridConfigJson.grid.id,
				sortname: gridConfigJson.grid.sortname,
				sortorder: gridConfigJson.grid.sortorder,
				//'gridConfig.rowNum': gridConfigJson.grid.rowNum,
	            permutation: gridConfigJson.grid.remapColumns,
	            colstates:{},
	            //columnModels: colstates
	        };
			colstates = gridConfig.colstates;
	    
	    for (i = 0; i < l; i++) {
	        colItem = colModel[i];
	        cmName = colItem.name;
	        if (cmName !== 'rn' && cmName !== 'cb' && cmName !== 'subgrid') {
	        	
	        	colstates[cmName] =   colItem;
	        }
	    }    
	    
		return gridConfig;
		
	};
	
/**
 * Get the next page number request on the jqGrid.
 * This function needs to call when default paging functionality is 
 * overriden using onPaging function.
 */
ns.common.getNextPageNumber = function(gridID,pgButton){
	if (!gridID.startsWith('#'))
		gridID = '#' + gridID;	
	
	var newUserValue = $(gridID+"_pager_center").find("input.ui-pg-input").val();
    var newValue = 0;
    var currentValue = $(gridID).jqGrid('getGridParam','page');
    if (pgButton.indexOf("next") >= 0)
        newValue = ++currentValue;
    else if (pgButton.indexOf("prev") >= 0)
        newValue = --currentValue;
    else if (pgButton.indexOf("last") >= 0)
        newValue = $(gridID).jqGrid('getGridParam','lastpage');
    else if (pgButton.indexOf("first") >= 0)
        newValue = 1;
    else if (pgButton.indexOf("user") >= 0)
        newValue = newUserValue;    
    else if (pgButton.indexOf("records") >= 0){
    	var rowNumber = $(gridID+"_pager_center").find(".ui-pg-selbox").selected().val();    	
    	$(gridID).jqGrid('setGridParam', {rowNum:rowNumber});
    	newValue =1;
    }   	
    
    return newValue;
};


/**
 * This method removes sortable row functionality on the Grid.
 * @param gridID ID of the grid
 */
ns.common.disableSortableRows = function(gridID){
	if (!gridID.startsWith('#'))
		gridID = '#' + gridID;
	//disable sortable rows
	if(ns.common.isInitialized($(gridID + " tbody"),"ui-jqgrid")){
		$(gridID + " tbody").sortable("destroy");
	}

};

/*------------------ JqGrid Column chooser specific functions : Start----------------------*/
ns.common.disableBackgroundForColumnChooser = function(gridID){
	$('#colchooser_'+gridID).dialog('widget')[0].style.zIndex = "6000";
	$('#colchooser_'+gridID).dialog('option','zIndex','6000');
	ns.common.showColumnChooserLoading();	
};

ns.common.showColumnChooserLoading = function(){
	$('#defaultLoadingIndicator').hide();
	$('#outer-center').showLoading({
		'addClass': 'ui-widget-overlay',
		'afterShow': function() {
			return false;
			//empty function to override default functioanlity
		},
		'indicatorID':'columnLoading'
	});
};

ns.common.hideColumnChooserLoading = function(){
	$('#outer-center').hideLoading({'indicatorID':'columnLoading'});
	$('#defaultLoadingIndicator').hide();
};

/*------------------ JqGrid Column chooser specific functions : Stop ----------------------*/

/* =================================================================================
	Others
==================================================================================== */


ns.common.GoToSessionActivityReport = function(urlString)
{
	   $.ajax({		
				url: urlString,
				success: function(data) {
						$("#sessionActivityReportID").html(data).dialog('open');
				}
	   });		
};


$.subscribe('errorSendUserProfile', function(event,data) {
	$.log('userprofile error! ');
	
	var resStatus = event.originalEvent.request.status;
	if(resStatus === 700) //700 indicates interval validation error
		ns.common.openDialog("authenticationInfo");
});

//this function adjusts size of select box to the length of element selected 
//[only applicable for selectbox with selectId.selectmenu(({width:'xyz'});); option]
ns.common.adjustSelectMenuWidth = function(elementID)
{
	if(elementID)
    {
	    var menu_id = "#" + elementID + "-menu";
		var button_id = "#" + elementID + "-button";
	    var font_ratio = 7;
	    var selected_element_length = 0;
	    var final_length = 0;
	    
	    var selectedElement = $("#"+elementID+">option:selected").text();
	    
	    if(selectedElement!=null && selectedElement !=''){
	    	selected_element_length = selectedElement.length;
	    	if(selected_element_length<12){
	    		selected_element_length = 12;//for text with length less than 12, the down arrow is not displayed correctly. 
	    	}
	    }
	   
	    var menuWidth = ($(menu_id).width()) /font_ratio;
	    
	    if(menuWidth<selected_element_length){//update selectmenu width only if selected element width is larger than selectmenu width
	    	final_length = selected_element_length;
	    }
	    
	    if(final_length!=0)
	    {
	    	$(button_id).width(final_length * font_ratio);
	    	$(menu_id).width(final_length * font_ratio);
	    }
    }	
};


ns.common.printNotesSection = function(){
	$('#globalmessage,#notes').print();
};

ns.common.printWholePage = function(){
	$.print();
};

//Returns true if the last grid data request was through inline search functionality of the grid
ns.common.isGridSearchActive = function(gridID){
	if(!gridID.startsWith('#')){
		gridID = '#' + gridID;
	}
	
	return $(gridID).jqGrid('getGridParam','search');
};

ns.common.reloadFirstGridPage = function(gridID){
	if(!gridID.startsWith('#')){
		gridID = '#' + gridID;
	}
	
	ns.common.setFirstGridPage(gridID);
	$(gridID).jqGrid('setGridParam',{datatype:'json'}).trigger("reloadGrid");
};

ns.common.setFirstGridPage = function(gridID){
	if (!gridID.startsWith('#'))
		gridID = '#' + gridID;
	
	$(gridID).jqGrid('setGridParam',{page:'1'});//set grid page param to 1 
};


ns.common.destroyDialog = function(dialogId){
	var id = '#'+dialogId;
	$(id).dialog('destroy');
	$(id).remove();
};

//this function do spcial symbol convertion
//symbol like "<" ">" can correctly display after convertion
ns.common.spcialSymbolConvertion = function( filterString ){
	    while (filterString.indexOf("\"") != -1) {
            filterString = filterString.substring(0,filterString.indexOf("\"")) + "&quot;" + filterString.substring(filterString.indexOf("\"")+1);
        }
        while (filterString.indexOf("<") != -1) {
            filterString = filterString.substring(0,filterString.indexOf("<")) + "&lt;" + filterString.substring(filterString.indexOf("<")+1);
        }
        while (filterString.indexOf(">") != -1) {
            filterString = filterString.substring(0,filterString.indexOf(">")) + "&gt;" + filterString.substring(filterString.indexOf(">")+1);
        }
        while (filterString.indexOf("  ") != -1) {
            filterString = filterString.substring(0,filterString.indexOf("  ")) + "&nbsp;&nbsp;" + filterString.substring(filterString.indexOf("  ")+2);
        }
		// sometimes nested XML comes out pre-html-converted.  This fixes it.
        while (filterString.indexOf("&amp;lt;") != -1) {
	        filterString = filterString.substring(0,filterString.indexOf("&amp;lt;")) + "&lt;" + filterString.substring(filterString.indexOf("&amp;lt;")+8);
	    }
	    while (filterString.indexOf("&amp;gt;") != -1) {
	        filterString = filterString.substring(0,filterString.indexOf("&amp;gt;")) + "&gt;" + filterString.substring(filterString.indexOf("&amp;gt;")+8);
		}
	    while (filterString.indexOf("&amp;gt") != -1) {
	        filterString = filterString.substring(0,filterString.indexOf("&amp;gt")) + "&gt;" + filterString.substring(filterString.indexOf("&amp;gt")+7);
		}
		
		    return filterString;
};

// Common function to add custom button to sjg:grid
ns.common.addGridButton = function(gridID, buttonConfig) {
	$(gridID).jqGrid('navButtonAdd', gridID + "_pager", buttonConfig);
};

/**
 * Creates/Sets a cookie value
 * @param name cookie name
 * @param value cookie value 
 * @param days number of days of expiry interval
 **/
ns.common.setCookie = function(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else {
    	var expires = "";
    }
    document.cookie = name+"="+value+expires+"; path=/";
};

/**
 * Gets a cookie value
 * @param name cookie name
 **/
ns.common.getCookie = function(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
};

/**
 * Deletes a cookie value
 * @param name cookie name
 **/
ns.common.deleteCookie = function(name) {
    ns.common.setCookie(name,"",-1);
};

//this function do spcial symbol convertion
//symbol like "<" ">" can correctly display after convertion
ns.common.spcialSymbolConvertion = function( filterString ){
	    while (filterString.indexOf("\"") != -1) {
            filterString = filterString.substring(0,filterString.indexOf("\"")) + "&quot;" + filterString.substring(filterString.indexOf("\"")+1);
        }
        while (filterString.indexOf("<") != -1) {
            filterString = filterString.substring(0,filterString.indexOf("<")) + "&lt;" + filterString.substring(filterString.indexOf("<")+1);
        }
        while (filterString.indexOf(">") != -1) {
            filterString = filterString.substring(0,filterString.indexOf(">")) + "&gt;" + filterString.substring(filterString.indexOf(">")+1);
        }
        while (filterString.indexOf("  ") != -1) {
            filterString = filterString.substring(0,filterString.indexOf("  ")) + "&nbsp;&nbsp;" + filterString.substring(filterString.indexOf("  ")+2);
        }
		// sometimes nested XML comes out pre-html-converted.  This fixes it.
        while (filterString.indexOf("&amp;lt;") != -1) {
	        filterString = filterString.substring(0,filterString.indexOf("&amp;lt;")) + "&lt;" + filterString.substring(filterString.indexOf("&amp;lt;")+8);
	    }
	    while (filterString.indexOf("&amp;gt;") != -1) {
	        filterString = filterString.substring(0,filterString.indexOf("&amp;gt;")) + "&gt;" + filterString.substring(filterString.indexOf("&amp;gt;")+8);
		}
	    while (filterString.indexOf("&amp;gt") != -1) {
	        filterString = filterString.substring(0,filterString.indexOf("&amp;gt")) + "&gt;" + filterString.substring(filterString.indexOf("&amp;gt")+7);
		}
		
		    return filterString;
}

//Common function written for formatting total with commas.
ns.common.formatTotal = function(total, currency){
	if(total){
		var formatedTotal = total.toFixed(2);
		if (formatedTotal > 0){
			// add in commas again
			var value = "" + formatedTotal
			x = value.indexOf('.') - 3;
			while (x > 0)
			{
				value = value.substr(0,x) + ',' + value.substr(x, value.length);
				x = value.indexOf(',')-3;
			}
			formatedTotal = value;
			if(currency){
				formatedTotal = formatedTotal + " " + currency;
			}	
		}
		return formatedTotal;
	}
	return "";
};

// Function to remove dialog.
ns.common.removeDialog = function(dialogId){
	if($('#'+dialogId).length){
		//Destroy "Dialog" status of the div
		$('#'+dialogId).dialog('destroy');
	
		//Remove Div markup
		$('#'+dialogId).remove();
	}
};

//This json structure maintains dialog configuration
ns.common.dialogs = {
	contactUs:{
		"id":"contactUs",
		"url":"/cb/pages/jsp/home/inc/contactUsDialog.jsp",
		"isStatic":"true",
		"autoOpen":"true"
	},
	language: {
		"id":"changeLanguage",
		"url":"/cb/pages/jsp/home/inc/languageDialog.jsp",
		"isStatic":"true",
		"autoOpen":"true"
	},
	helpMe:{
		"id":"helpMe",
		"url":"/cb/pages/jsp/home/inc/helpMeDialog.jsp",
		"isStatic":"true",
		"autoOpen":"false"	
	},
	fx:{
		"id":"remotefxdialog",
		"url":"/cb/pages/jsp/home/inc/fxDialog.jsp",
		"autoOpen":"true"		
	},
	contactMyAdmin:{
		"id":"contactMyAdminDialog",
		"url":"/cb/pages/jsp/home/inc/contactMyAdminDialog.jsp",
		"isStatic":"true",
		"autoOpen":"true"			
	},
	debug:{		
		"id":"debugDiv",
		"url":"/cb/pages/jsp/home/inc/debugDialog.jsp"						
	},
	authenticationInfo:{		
		"id":"authenticationInfoDialogID",
		"url":"/cb/pages/jsp/home/inc/authenticationInfoDialog.jsp"						
	},
	fileImportResults:{		
		"id":"checkFileImportResultsDialogID",
		"url":"/cb/pages/jsp/home/inc/fileImportResultsDialog.jsp",
		"autoOpen":"false",
		"isStatic":"true"
	},
	fileImport:{		
		"id":"openFileImportDialogID",
		"url":"/cb/pages/jsp/home/inc/openFileImportDialog.jsp",
		"autoOpen":"false",
		"isStatic":"true"	
	},	
	saveTheme:{
		"id":"saveThemeDialogID",
		"url":"/cb/pages/jsp/home/inc/saveThemeDialog.jsp",
		"autoOpen":"true",
		"isStatic":"true"			
	},
	sessionActivity:{
		"id":"sessionActivityReportID",
		"url":"/cb/pages/jsp/home/inc/sessionActivityDialog.jsp",
		"autoOpen":"true"
		
	},
	checkImage:{
		"id":"checkImageDialogID",
		"url":"/cb/pages/jsp/home/inc/checkImageDialog.jsp",
		"autoOpen":"false",
		"resizable":"true"					
	},
	addShortCut:{		
		"id":"addShortcutDialogID",
		"url":"/cb/pages/jsp/home/inc/addShortcutDialog.jsp",
		"autoOpen":"true",
		"isStatic":"true"	
	},
	deleteMapping:{		
		"id":"deleteMappingDialogID",
		"url":"/cb/pages/jsp/home/inc/deleteMappingDialog.jsp",
		"autoOpen":"false"								
	},
	savePortalPage:{		
		"id":"savePortalPageDialogID",
		"url":"/cb/pages/jsp/home/inc/savePortalPageDialog.jsp",
		"autoOpen":"true",
		"isStatic":"false"
	},
	resetPortalSettings:{		
		"id":"resetPreviousConfirm",
		"url":"/cb/pages/jsp/home/inc/resetPortalSettingsDialog.jsp",
		"autoOpen":"true",
		"isStatic":"true"
	},
	portalOperationNotice:{		
		"id":"portalOperationNoticeDialogID",
		"url":"/cb/pages/jsp/home/inc/portalOperationNoticeDialog.jsp",
		"autoOpen":"true",
		"isStatic":"true"	
	},
	deletePortalSettingsNotice:{		
		"id":"deletePortalSettingNoticeDialogID",
		"url":"/cb/pages/jsp/home/inc/deletePortalSettingsNoticeDialog.jsp",
		"autoOpen":"true",
		"isStatic":"true"								
	},
	loadPortalSettingsNotice:{		
		"id":"loadPortalSettingNoticeDialogID",
		"url":"/cb/pages/jsp/home/inc/loadPortalSettingsNoticeDialog.jsp",		
		"autoOpen":"false",
		"isStatic":"true"
	},
	portalLayoutDialog:{		
		"id":"portalPageLayout",
		"url":"/cb/pages/jsp/home/inc/portal-layout-dialog.jsp",
		"autoOpen":"true",
		"isStatic":"false"
	},
   quickViewMessageDialog:{        
        "id":"quickViewMsgDialogID",
        "url":"/cb/pages/jsp/home/inc/quickViewMessageDialog.jsp",
        "autoOpen":"true",
		"isStatic":"true"
	},
	quickNewMsgDialog:{        
        "id":"quickNewMsgDialogID",
        "url":"/cb/pages/jsp/home/inc/quickNewMessageDialog.jsp",
        "autoOpen":"true",
		"isStatic":"false"
	},	
   quickViewAlertDialog:{        
        "id":"quickViewAlertDialogID",
        "url":"/cb/pages/jsp/home/inc/quickViewAlertDialog.jsp",
        "autoOpen":"true",
		"isStatic":"true"
   },
   accessKeysViewDialog:{
	   "id":"accessKeysViewDialogID",
       "url":"/cb/pages/jsp/home/inc/accessKeysViewDialog.jsp",
       "autoOpen":"false",
	   "isStatic":"true"
   },
   updateProfile:{
		"id":"updateProfileDialogId",
		"url":"/cb/pages/jsp/user/UpdateProfileDialogHolder.jsp",
		"autoOpen":"true",
		"isStatic":"true"
	},
   accountPrefs:{
		"id":"accountPrefsDialogId",
		"url":"/cb/pages/jsp/user/accountPrefsDialogHolder.jsp",
		"autoOpen":"true",
		"isStatic":"true"
	},	
	bankLookup:{
		"id":"bankLookupDialogId",
		"url":"/cb/pages/jsp/user/bankLookupDialogHolder.jsp",
		"autoOpen":"true",
		"isStatic":"true"
	},
	otherSetting:{
		"id":"otherSettingDialogId",
		"url":"/cb/pages/jsp/user/otherSettingDialogHolder.jsp",
		"autoOpen":"true",
		"isStatic":"true"
	},
	manageShortcuts:{
		"id":"manageShortcutsDialogId",
		"url":"/cb/pages/jsp/home/shortcuts/manageShortcutsDialogHolder.jsp",
		"autoOpen":"true",
		"isStatic":"true"
	},
	sessionActivityReport:{
			"id":"sessionActivityDialogId",
			"url":"/cb/pages/jsp/user/sessionActivityReportDialogHolder.jsp",
			"autoOpen":"true"
	},
	siteNavigation:{
		"id":"siteNavigationDialogId",
		"url":"/cb/pages/jsp/user/siteNavigationDialogHolder.jsp",
		"autoOpen":"true",
		"isStatic":"true"
	},
	deleteHomeBillpay:{
		"id":"deleteHomeBillpayDialogID",
        "url":"/cb/pages/jsp/home/inc/deleteHomeBillpayDialog.jsp",
        "autoOpen":"true",
		"isStatic":"false"
	},
	deleteHomeTransfer:{
		"id":"deleteHomeTransferDialogID",
        "url":"/cb/pages/jsp/home/inc/deleteHomeTransferDialog.jsp",
        "autoOpen":"true",
		"isStatic":"false"
	},
	editHomeTransfer:{
		"id":"editHomeTransferDialogID",
        "url":"/cb/pages/jsp/home/inc/editHomeTransferDialog.jsp",
        "autoOpen":"true",
		"isStatic":"false"
	},
	editHomeBillPayment:{
		"id":"editHomeBillPaymentDialogID",
        "url":"/cb/pages/jsp/home/inc/editHomeBillPaymentDialog.jsp",
        "autoOpen":"true",
		"isStatic":"false"
	},
	themePicker:{
		"id":"themePickerDialogId",
		"url":"/cb/pages/jsp/user/themePickerDialogHolder.jsp",
		"autoOpen":"true",
		"isStatic":"true"
	}
};

/*
  This function handles opening of dialog,destroying and removing dialog markup before 
  Getting it opened to resolve memeory issues also in future we will be getting rid of sj:dialog 
  and will use jquery dialog
  @param object dialog	dialog configuration object
*/
ns.common.openDialog = function(dialog){
	//console.info("openDialog: "+ dialog);
	if(dialog){
		var aConfig = ns.common.dialogs[dialog];		
		if(aConfig){
				if(aConfig.isLoaded && aConfig.isStatic && aConfig.isStatic =="true"){
					if(aConfig.beforeOpen){
						aConfig.beforeOpen.call();
					}
					var dialogId = aConfig.id;
					if(dialogId){				
						//default it should open
						var openDialog = true; 
						if(aConfig.autoOpen && aConfig.autoOpen === "false"){
							openDialog = false;
						}

						if(openDialog){
							$("#"+ aConfig.id).dialog('open');
						}
					}
				}else{
					//load the dialog via ajax
					$.ajax({
						url:aConfig.url,
						method:"GET",
						success:function(response){
							//console.info("received dialog content****");
						
							$("#"+ aConfig.id).dialog('destroy');
							$("#"+aConfig.id).remove();					
							$('body').append(response);
						if(ns.common.isInitialized($("#"+ aConfig.id),'ui-dialog')){
							if(aConfig.resizable){
								$("#"+aConfig.id).dialog( "option", "resizable", true );	
							}else{
								$("#"+aConfig.id).dialog( "option", "resizable", false );
							}
						}
							//invoke the beforeOpen callback
							if(aConfig.beforeOpen){
								aConfig.beforeOpen.call();
							}

							//Construct the dialog here.
							if(aConfig.autoOpen && aConfig.autoOpen === "false"){
								aConfig.autoOpen = false;
							}else{
								aConfig.autoOpen = true;
							}							
							$("#"+ aConfig.id).dialog(aConfig);

							//call or publish topics which are subscribed
							if(aConfig.open){
								$.publish(aConfig.open);
							}
							aConfig.isLoaded = true;	
						}
					});
				}								
			
	}else{
			console.error("dialog configuration not available");
		}			
	}else{
		console.warn("please provide dialog name");
	}
};


/**
* Resets the dialogs state to original
*/
ns.common.resetDialogState = function(dialog){
	if(dialog){
		var config = ns.common.dialogs[dialog];
		config.isLoaded = false;
	}else{
		var dialogs = ns.common.dialogs;
		var dialog;		
		$.each(dialogs,function(k,v){
			v.isLoaded = false;
		});
	}
};

//Start Auto Refresh functionality-----------------
ns.common.refreshPanels = function(){
	//console.info("ns.home.refreshPanels");
	//Here we are providing common topic called common.refreshPanel for future auto refresh panel developement turn this
	//publish on so whoever interested in common refresh event will get call
	//$.publish("common.refreshPanel"); 	
	//$.publish("common.refreshMessages");
	//$.publish("common.refreshAlerts");
	$.publish("ns.common.getBankingEvents");
};


$.subscribe('beforeAutoRefresh', function(event,data) {
	var containerId = event.originalEvent.id;
	if(containerId) {
		var containerSelector =	"#" + containerId;
		if ($(containerSelector).pane('isOpen')){
			//$(containerSelector).fadeOut("slow");
			$(containerSelector).showLoading();
		}
	}		
	$.ajaxSetup({global: false});	
});

$.subscribe('afterAutoRefresh', function(event,data) {
	var containerId = data.id;
	if(containerId) {
		var containerSelector =	"#" + containerId;
		if ($(containerSelector).pane('isOpen')){
			//$(containerSelector).fadeIn("slow");
			$(containerSelector).hideLoading();
		}
	}
	$.ajaxSetup({global: true});
    
	//tabify on auto-refresh
	var messagesTabIndex = ns.common.getSectionTabIndex("messages");
	$("#MessageSmallPanel").tabify({baseIndex:messagesTabIndex});
	var alertsTabIndex = ns.common.getSectionTabIndex("alerts");
	$("#AlertSmallPanel").tabify({baseIndex:alertsTabIndex});
});

$.subscribe('common.refreshMessages', function(event,data) {
	$.publish("refreshMessageSmallPanel");
	$.publish("messages.refreshMessagesGrid");	
});

$.subscribe('common.refreshAlerts', function(event,data) {
	$.publish("refreshAlertSmallPanel");
	$.publish("alert.refreshReceivedAlertsGrid");	
});

$.subscribe('common.refreshApprovals', function(event,data) {
	$.publish("refreshApprovalSmallPanel");
	$.publish("approval.refreshApprovalsGrid");	//TODO
});

//End Auto Refresh functionality-----------------

$.subscribe('common.clearValidationErrorsTopic', function(event,data) {
	$('.errorLabel').html('').removeClass('errorLabel');
	$('#formerrors').html('');
});

ns.common.checkUnCheckGridCheckAllBox = function(gridId){
	//console.info("ns.common.checkUnCheckGridCheckAllBox");
	var total = $("#" + gridId + " input[type=checkbox]").size();
	if(total == 0)
		return;
	
    if($("#" + gridId + " input[type=checkbox]:checked").size()<total){
        $("#cb_" + gridId).attr("checked",false);  
    }

    var disabledCount = $("#" + gridId + " input[type=checkbox]:disabled").size();
    var checkedCount = $("#" + gridId + " input[type=checkbox]:checked").size();


    if((checkedCount == total) ||  ((disabledCount+checkedCount) == total)){
        $("#cb_" + gridId).attr("checked",true);   
    }
};

ns.common.addHoverEffect = function(id){
	$("#" + id ).hover(function(){$(this).addClass("ui-state-hover")},function(){$(this).removeClass("ui-state-hover")});
};

ns.common.sortOnTabIndex = function(a,b){
	if(a.tabIndex > b.tabIndex){
		return 1;
	}else if(a.tabIndex < b.tabIndex){
			return -1;
	}else{
			return 0;
	}	
};

//while adding new section to tab index group
ns.common.tabindexes = [
	{
		section:"topMenu",
		tabIndex:1
	},
	{
		section:"mainMenu",
		tabIndex:100
	},
	{
		section:"messages",
		tabIndex:1000
	},
	{
		section:"alerts",
		tabIndex:1100
	},
	{
		section:"approvals",
		tabIndex:1200
	},
	{
		section:"shortcuts",
		tabIndex:2000
	},
	{
		section:"themePicker",
		tabIndex:2100
	},
	{
		section:"forexConverter",
		tabIndex:2200
	},
	{
		section:"extensionMenu",
		tabIndex:10000
	},
	{
		section:"dashboard",
		tabIndex:20000
	},
	{
		section:"details",
		tabIndex:21000
	},
	{
		section:"summary",
		tabIndex:22000
	},
	{
		section:"dialog",
		tabIndex:30000
	}
];

ns.common.getSectionTabIndex = function(section){	
	var indexes = ns.common.tabindexes;	
	for(var i=0;i<indexes.length;i++){
		if(indexes[i].section === section){
			return indexes[i].tabIndex;
		}	
	}	
};

ns.common.getNextTabIndex = function(currentTabIndex, direction){	
	//console.info("ns.common.getNextTabIndex");
	var tabIndexes = ns.common.tabindexes;	
	tabIndexes.sort(ns.common.sortOnTabIndex);

	var nextTabIndex;
	if(direction === true){
		for(var i=0; i<tabIndexes.length; i++){
			if(tabIndexes[i].tabIndex > currentTabIndex){
				nextTabIndex =  tabIndexes[i].tabIndex;
				break;
			}
		}
	}else{
		for(var i=tabIndexes.length-1 ;i>=0 ;i--){
			if(tabIndexes[i].tabIndex < currentTabIndex){
				nextTabIndex =  tabIndexes[i].tabIndex;
				break;
			}
		}	
	}	
	if(nextTabIndex === undefined){
		//console.info("reched to boundaries");
		nextTabIndex = (direction) ? 40000:-1;
	}	
	return nextTabIndex;	
};

ns.common.getActiveSection = function(currentTabIndex){	
	var tabIndexes = ns.common.tabindexes;	
	tabIndexes.sort(ns.common.sortOnTabIndex);
	for(var i=0; i<tabIndexes.length; i++){
		if(tabIndexes[i].tabIndex <= currentTabIndex && tabIndexes[i+1].tabIndex>currentTabIndex){
			var activeSection = tabIndexes[i].section;			
			if(activeSection === "topMenu"){
				//check if dialog is open
				if($("div.ui-dialog:visible").length>0){
					activeSection = "dialog";
				}
			}
			return activeSection;
		}
	}	
};

ns.common.applyCommonTabIndex = function(){
	var tab = ns.common.tabindex;
	var topMenuTabIndex = ns.common.getSectionTabIndex("topMenu");
	var mainMenuTabIndex = ns.common.getSectionTabIndex("mainMenu");
	var messagesTabIndex = ns.common.getSectionTabIndex("messages");
	var alertsTabIndex = ns.common.getSectionTabIndex("alerts");
	var approvalsTabIndex = ns.common.getSectionTabIndex("approvals");
	var shortcutsTabIndex = ns.common.getSectionTabIndex("shortcuts");
	var themeTabIndex = ns.common.getSectionTabIndex("themePicker");
	var forexTabIndex = ns.common.getSectionTabIndex("forexConverter");
	var extensionMenuIndex = ns.common.getSectionTabIndex("extensionMenu");
	var dashboardTabIndex = ns.common.getSectionTabIndex("dashboard");
	var detailsTabIndex = ns.common.getSectionTabIndex("details");
	var summaryTabIndex = ns.common.getSectionTabIndex("summary");	
	
	$("#pageHeaderRightDiv").tabify({baseIndex:topMenuTabIndex});
	$("#main-menu").tabify({baseIndex:mainMenuTabIndex});
	$("#MessageSmallPanel").tabify({baseIndex:messagesTabIndex});
	$("#AlertSmallPanel").tabify({baseIndex:alertsTabIndex});
	$("#ApprovalSmallPanel").tabify({baseIndex:approvalsTabIndex});
	$("#shortcutsPanel").tabify({baseIndex:shortcutsTabIndex});
	$("#themePickerDivID").tabify({baseIndex:themeTabIndex});
	$("#forexConverterPanel").tabify({baseIndex:forexTabIndex});	
	$("#extension-menu").tabify({baseIndex:extensionMenuIndex});
	$("#appdashboard").tabify({baseIndex:dashboardTabIndex});	
	$("#details").tabify({baseIndex:detailsTabIndex});	
	$("#summary").tabify({baseIndex:summaryTabIndex});
}

ns.common.focusToPortletContent = function(portletId){
	//console.info("ns.common.focusToPortletContent" + portletId);	
}

$.subscribe("common.topics.tabifyAll",function(){
	if(accessibility){
		accessibility.deactivateLinks(false)	;
	}
	ns.common.applyCommonTabIndex();
})

$.subscribe("common.topics.tabifyDesktop",function(){
	var dashboardTabIndex = ns.common.getSectionTabIndex("dashboard");
	var detailsTabIndex = ns.common.getSectionTabIndex("details");
	var summaryTabIndex = ns.common.getSectionTabIndex("summary");	
	
	var appdashboard = $("#appdashboard");
	var details = $("#details");
	var summary = $("#summary");
	if(appdashboard.length>0 && details.length>0 && summary.length>0){
		appdashboard.tabify({baseIndex:dashboardTabIndex});	
		details.tabify({baseIndex:detailsTabIndex});	
		summary.tabify({baseIndex:summaryTabIndex});		
	}else{
		//Looks like the page loaded is not as per design in terms of containers, Please fix.
		$.publish("common.topics.tabifyNotes");
	}
});

$.subscribe("common.topics.tabifyNotes",function(){
	var dashboardTabIndex = ns.common.getSectionTabIndex("dashboard");
	$("#notes").tabify({baseIndex:dashboardTabIndex});
});

$.subscribe("common.topics.tabifyDialog",function(){
	//console.info("would this trick work here...?");
	//console.info("tabifyDialog-which is active element now ? " + document.activeElement);
	
	//$("[tabindex]").attr("tabIndex","-1");	
	//$("[href=#]").attr("tabIndex","-1");	
	//$("[href=javascript:void(0)]").attr("tabIndex","-1");
	
	//$("[href=javascript:void(0)]").removeAttr("href");
	//$("[href=javascript:void(0)]").attr("tmpHref","javascript:void(0)").removeAttr("href");
	//$("[href=#]").removeAttr("href");
	//$("[href=#]").attr("tmpHref","#").removeAttr("href");
	
	if(accessibility){
		accessibility.deactivateLinks(true)	;
	}

	var diaContentArea = $(".ui-dialog:visible .ui-dialog-content");
	diaContentArea.tabify({baseIndex:1});	
	var diaButtonPane = $(".ui-dialog-buttonpane:visible");
	if(diaButtonPane.length>0){
		//dialog has external button pane tabify it continue from last sequence
		var lastIndex = $(".ui-dialog:visible .ui-dialog-content :tabbable:last").attr("tabindex");
		diaButtonPane.tabify({baseIndex:lastIndex});
	}

	var anElement = $(".ui-dialog-content:visible :tabbable:first");
	if(anElement.length === 0){
		anElement = $(".ui-dialog:visible :tabbable:first");		//get the tabbable first element on visible dialog portion
	}
	anElement.focus();

	window.focusFlag = false;	
});

$.subscribe("common.topics.openFileOpenDialog",function(){
	$("#importFileInputID").click();
});

$.subscribe('common.topics.updateRequestParamsTopic', function(event, data) {
	var $currentTarget = $(event.originalEvent.currentTarget);
	var requestParam = $currentTarget.attr("reqParam");
	if(requestParam){
		ns.common.updateRequestParams(requestParam);
	}	
});

/**
* Below topics are wrappers for namespaced tabofy functions since multi namespaced topics
* are difficult to invoke from struts:jquery tags
*/
$.subscribe("tabifyNotes",function(){
	$.publish("common.topics.tabifyNotes");
});

$.subscribe("tabifyDesktop",function(){
	$.publish("common.topics.tabifyDesktop");
});

$.subscribe("setFocusOnDashboardTopic",function(){
	if(accessibility){
		accessibility.setFocusOnDashboard();
	}
});

$.subscribe("tabifyDialog",function(){
	$.publish("common.topics.tabifyDialog");
});

$.subscribe("onCompleteTabifyAndFocusCalendarTopic",function(){
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#calendar").setFocus();
	}
});

$.subscribe("closeUploadFileWindowTopic",function(){
	ns.common.closeUploadFileWindow();
});

ns.common.logout = function(){
	window.location.href = "/cb/pages/jsp/invalidate-session.jsp";
}

ns.common.superSelectFocusHandler = function($selectMenu){
	if(accessibility){
		$.publish("common.topics.tabifyNotes");
	}
	//var $aSelectMenu = $("#assignedTypecodesListId_sms");
	var items = $selectMenu.find("ul li a").length;
	if(items>0){
		$selectMenu.find("ul li a:eq(0)").focus();
	}else{
		$selectMenu.find("input.search").focus();
	}
}

ns.common.openDocumentCenter = function(aPath){
	if(aPath){
		window.open(aPath);
	}		
}

$.subscribe("setFocusOnDialogTopic",function(){
	if(accessibility){
		$(".ui-dialog-content:visible").setFocus();
	}
});

ns.common.openEmailClient = function(anEmail){
	document.location.href = anEmail;
}

ns.common.changeLanguage = function(anAction){
	document.location.href = anAction;
}

// Function to handle text area maxlength
ns.common.handleTextAreaMaxlength = function(id){
	$(id).keyup(function(){
		var max = parseInt($(id).attr('maxlength'));
		if($(id).val().length > max){
				$(id).val($(id).val().substr(0, max));				
		}			
	});
};

ns.common.addRejectReasonRow = function(rejectByLabel,rejectedBy,rejectReasonLabel,rejectedReason,className,colspan){
	var trElm = "";
	var rejectedReasonClass = "rejectedReasonWithoutName";
	var rowNumCol = '';
	trElm = '<tr class=\"' + className + ' skipRowActionItemRender\">'+ rowNumCol +'<td colspan="'+colspan + '" class="rejectReasonFont">';
	if(rejectByLabel && rejectByLabel!='' &&  rejectedBy && rejectedBy!=''){
		trElm +='<div class="rejectedRowLabel"><p class="">'+ rejectByLabel + '</p></div><div class="rejectedBy"><p class="">' + rejectedBy +'&nbsp;</p></div>';
		rejectedReasonClass = "rejectedReason";
	}
	
	trElm +='<div class="rejectedRowLabel"><p class="">'+ rejectReasonLabel  + '</p></div><div class="'+rejectedReasonClass+'"><p class="breakWordWhenSpace">' +  rejectedReason +'&nbsp;</p></div></td></tr>';
	
	return trElm;
};

ns.common.addSubmittedForRow = function(approvedByLabel,approverName,submittedForTitle,submittedFor,submittedByLabel,submittedBy,className,colspan){
	var trElm = "";
	var rowNumCol = '';
	trElm = '<tr class=\"' + className + ' skipRowActionItemRender\">'+ rowNumCol +'<td colspan="'+colspan + '" align="center">'+
	'<div class="approverNameLabel"><p class="">'+approvedByLabel + '&nbsp;</p></div><div class="approverName"><p class="">' + approverName +'</p></div>'+
	'<div class="approvalRowLabel"><p class="">'+ submittedForTitle  + '&nbsp;</p></div><div class="submittedFor"><p class="">' +  submittedFor + '</p></div>'+
	'<div class="approvalRowLabel"><p class="">'+submittedByLabel + '&nbsp;</p></div><div class="submittedBy"><p class="">'+ submittedBy +'</p></div></td></tr>';
	return trElm;
};


ns.common.printPopup = function(){
	var dialogsOpened = $(".ui-dialog").filter(":visible");
	if(dialogsOpened.length > 0){
		//Its possible to have more than one dialog open,then get Top most by Z-index
		var topMostDialog = dialogsOpened[0];
		var counter = 1;
		var z_index = parseInt(dialogsOpened[0].style.zIndex, 10);
		var currentZIndex = 0;
		for(counter = 1; counter < dialogsOpened.length; counter++){
			currentZIndex = parseInt(dialogsOpened[counter].style.zIndex, 10);
			if(z_index < currentZIndex){
				z_index = currentZIndex;
				topMostDialog = dialogsOpened[counter];
			}			
		}
		$(topMostDialog).print();
	}
};

$(window).unload(function(){
	ns.common.onPageUnload();
});

//Before calling destroy at this place make sure to check widget function exists.
ns.common.onPageUnload = function(){
	// Destroy all child widgets
	if($(".ui-button").data("ui-button") != undefined){
		$(".ui-button").button('destroy');
	}	
	
	if($("select").data("ui-selectmenu") != undefined){
		$("select").selectmenu('destroy');
	}
	
	if($("select").data("ux-lookupbox") != undefined){
		$('select').lookupbox('destroy');
	}
	
	if($(".hasDatepicker").data("ui-datepicker") != undefined){
		$('.hasDatepicker').datepicker('destroy');
	}
	
	$('#ui-datepicker-div').unbind();
	$('#ui-datepicker-div').remove();
	
	if($(".ui-jqgrid-btable").data("ui-jqgrid") != undefined){
		$(".ui-jqgrid-btable").jqGrid('destroy');
	}
	
	if($("div.pane-content").data("ui-pane") != undefined){
		$("div.pane-content").pane('destroy');
	}
	$('#menu_'+ns.home.lastMenuId).unLoadTopics().removeData('createdTopics').unbind('.topics');
	window.ns = null;
};

/**
* Track element decides to store its reference based on attribute called
* "enableglobaltrack" if any link,button has this attribute with value false
* will not be stored under active submit button
*/
ns.common.trackElement = function($elem){	
	if($elem){
		var trackGloablEvent = $elem.attr("enableglobaltrack") || "true";
		if(trackGloablEvent == "true"){			
			ns.common.activeSubmitButton = $elem.attr("id");
		}
	}
}

/**
* This function handles validation errors thrown from server
* @errors  errors collection
* @type  validation error/error
*/
ns.common.handleErrorMessage = function(errorMessage,type){
	ns.common.hideStatus();
	//check if dialog is open then show error on dialog
	var dialog = $(".ui-dialog:visible");
	if( dialog.length > 0 ){				
		var formErrorContainer;
		//Defensively get the error container as there are many places where formError and formerrors have been provided as id's
		formErrorContainer = dialog.find("#formError"); 
		if(formErrorContainer.length == 0){
			formErrorContainer = dialog.find("#formerrors") ; 
		} 
		if(formErrorContainer.length>0){
			formErrorContainer.html(errorMessage).addClass("serverResponseHolderDiv_ErrorMsg");
		}
		else{
			// Page does not have formErrorContainer, hence closing dialog box and then displaying error
			ns.common.closeDialog();
			ns.common.showError(errorMessage);
		}
	}else{
		ns.common.showError(errorMessage);
	}
};

//Fix for CKEditor not being editable in dialog
$.widget("ui.dialog", $.ui.dialog, {
	_allowInteraction: function(event) {
		return !!$(event.target).closest(".cke").length || this._super(event);
	}
});

/**
* Function to render regular Rich text editor
*/
/*ns.common.renderRichTextEditor = function(selectorWithoutHash,optionalWidth){
	var widthVar = '800';
	if(optionalWidth != undefined){
		widthVar = optionalWidth;
	}
	
    CKEDITOR.replace(selectorWithoutHash, {
		skin : 'office2013',
		extraPlugins : 'font,justify',
		width: widthVar,
		autoGrow_maxHeight: 150,
		fontSize_sizes : '12/12px;13/13px;14/14px;15/15px;16/16px',
		toolbar: [
			[ 'Bold', 'Italic','Underline', '-','TextColor', 'BGColor','Font', 'FontSize'],
			[ 'NumberedList', 'BulletedList' , '-','JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
			  '-','Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo' ]
		]
	});
	CKEDITOR.instances[selectorWithoutHash].on('change', function() { CKEDITOR.instances[selectorWithoutHash].updateElement(); });
};*/

/**
* Function to render Inline Rich text editor
*/
ns.common.renderRichTextEditor = function(selectorWithoutHash,optionalWidth,startupFocus){
	//remove any existing CKEditor instances
	if(!ns.home.isDevice) { 
		//not displaying CKEditor for iPad
	
		for(name in CKEDITOR.instances)
		{
		    CKEDITOR.instances[name].destroy();
		}
		
		var widthVar = '800';
		if(optionalWidth != undefined){
			widthVar = optionalWidth;
		}
		if (startupFocus != undefined && startupFocus) {
			CKEDITOR.config.startupFocus = startupFocus;
		} else {
			CKEDITOR.config.startupFocus = false;
		}
		
		// Turn off automatic editor creation first.
	    CKEDITOR.disableAutoInline = true;
	    CKEDITOR.inline( selectorWithoutHash , {
			skin : 'office2013',
			width: widthVar,
			extraPlugins : 'justify',
			toolbar: [
				[ 'Bold', 'Italic','Underline'],
				[ 'NumberedList', 'BulletedList' , '-','JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
				  '-','Undo', 'Redo' ]
			]
		});
	    CKEDITOR.instances[selectorWithoutHash].on('change', function() { CKEDITOR.instances[selectorWithoutHash].updateElement(); });
	} 
};


/* Functions added for UI Improvements */

jQuery(document).ready(function () {
	$(".notificationWinCloseBtnCls").click(function(event){
		$(".notificationDisplayHolderCls").hide( "drop", { direction: "down" }, "medium" );
	});

	$("#globalMsgIcon, #globalMsgCloseBtn").click(function(){
		if($("#globalMsgDisplayHolder").is(":visible")) {
			ns.common.hideGlobalMsg();
		}
		else {
			ns.common.showGlobalMsg();
		}
	});
	
	ns.common.showGlobalMsg = function(){
		$("#globalMsgDisplayHolder").fadeIn("slow");
		$("#globalMsgIcon").removeClass("ffiUiIco-icon-cloud-active").addClass("ffiUiIco-icon-cloud");
		ns.home.updateGlobalMsgMenuSyncDate();
		// hiding text indicator div
		$("#globalMsgIndicatorTxtHolder").removeClass("showGMsgIndicatorTxt").addClass("hideGMsgIndicatorTxt");
	};
	
	ns.common.showCloudNotification = function() {
		var crntMenuTitleIndex = 0;
		if (ns.home.currentMenuId == 'pmtTran') {
			crntMenuTitleIndex = 1;
		}
		var crntMenuTitle = $('li[menuId="' + ns.home.currentMenuId + '"]').find('a').eq(crntMenuTitleIndex).html();
		$("#globalMsgIcon").removeClass("ffiUiIco-icon-cloud").addClass("ffiUiIco-icon-cloud-active");
		$("#globalMsgIndicatorTxtHolder").removeClass("hideGMsgIndicatorTxt").addClass("showGMsgIndicatorTxt");
		$("#gmTxtHolder").html( js_new_global_message + " " + crntMenuTitle+"!");
	};
	
	ns.common.hideCloudNotification = function() {
		$("#globalMsgIndicatorTxtHolder").removeClass("showGMsgIndicatorTxt").addClass("hideGMsgIndicatorTxt");
	};

	ns.common.hideGlobalMsg = function(){
		$("#globalMsgDisplayHolder").fadeOut();
		$("#globalMsgIcon").removeClass("ffiUiIco-icon-cloud-active").addClass("ffiUiIco-icon-cloud");
		$("#globalMsgIndicatorTxtHolder").removeClass("showGMsgIndicatorTxt").addClass("hideGMsgIndicatorTxt");
	};

	ns.common.showAlertNotification = function(detail,notificationSettings){
		var alertSubject = detail.map.data.subject;
		var alertDate = detail.map.data.createDate;
		var recievedAlertID = detail.map.data.ID;
		var alertBody = detail.map.data.memo;
		
		$("#alertSubject").html(ns.common.HTMLEncode(alertSubject));
		$("#alertDate").html(alertDate);
		$("#recievedAlertID").val(recievedAlertID);
		$("#alertBody").html(alertBody);
		
		
		$("#msgDisplayHolder").css("display", "none");
		$("#approvalDisplayHolder").css("display", "none");
		
		//Show notification only if user has enabled it.
		if(notificationSettings !=undefined && notificationSettings.showAlertNotification == 'true'){
			ns.common.showMe("alertDisplayHolder");	
		}
		
		//Refresh the alert pane
		$.publish("common.refreshAlerts");
	};
	
	ns.common.showMsgNotification = function(detail,notificationSettings){
		var msgFrom = detail.map.data.fromName;
		var msgDate = detail.map.data.createDate;
		var msgCaseNo = detail.map.data.caseNum;
		var msgSubject = detail.map.data.subject;
		var msgMemo = detail.map.data.memo;
		var recievedMsgID = detail.map.data.ID;
		
		$("#msgFrom").html(msgFrom);
		$("#msgDate").html(msgDate);
		$("#msgCaseNo").html(msgCaseNo);
		$("#msgSubject").html(ns.common.HTMLEncode(msgSubject));
		
		// Removed HTML encoding BCP #1680279145 
		$("#msgBody").html(msgMemo);
		$("#recievedMsgID").val(recievedMsgID);
		
		$("#alertDisplayHolder").css("display", "none");
		$("#approvalDisplayHolder").css("display", "none");
		
		//Show notification only if user has enabled it.
		if(notificationSettings !=undefined && notificationSettings.showMailNotification == 'true'){
			ns.common.showMe("msgDisplayHolder");
		}
		
		//Refresh the message pane
		$.publish("common.refreshMessages");
		//Refresh the active messages grid
		$.publish("reloadCurrentMessagesGrid");
		
	};
	
	ns.common.showTransactionApprovalNotification = function(detail,notificationSettings){
		var displayTypeAsString = detail.map.data.item.displayTypeAsString;
		var approvalTitle = displayTypeAsString + " for approval.";
		var approvalDate = detail.map.data.submissionDate;
		var approvalSubmittedBy = detail.map.data.submittingUserName;
		var approvalAmount = detail.map.data.item.amount.amountValue;
		
		$("#approvalTitle").html(approvalTitle);
		$("#approvalDate").html(approvalDate);
		$("#approvalSubmittedBy").html(approvalSubmittedBy);
		$("#approvalAmount").html(approvalAmount);
		$("#approvalAmountArea").show();
		$("#approvalItemType").val(detail.eventType);
		
		$("#msgDisplayHolder").css("display", "none");
		$("#alertDisplayHolder").css("display", "none");
		
		//Show notification only if user has enabled it.
		if(notificationSettings !=undefined && notificationSettings.showApprovalNotification == 'true'){
			ns.common.showMe("approvalDisplayHolder");
		}
		
		//Refresh the approval pane
		$.publish("common.refreshApprovals");
	};
	
	ns.common.showReleaseWireApprovalNotification = function(detail,notificationSettings){
		var approvalTitle =  "Wire transfer to release.";
		var approvalDate = detail.map.data.createDate;
		var approvalSubmittedBy = detail.map.submittedByName;
		var approvalAmount = detail.map.data.displayAmount;
		
		$("#approvalTitle").html(approvalTitle);
		$("#approvalDate").html(approvalDate);
		$("#approvalSubmittedBy").html(approvalSubmittedBy);
		$("#approvalAmount").html(approvalAmount);
		$("#approvalAmountArea").show();
		$("#approvalItemType").val(detail.eventType);
		
		$("#msgDisplayHolder").css("display", "none");
		$("#alertDisplayHolder").css("display", "none");
		
		//Show notification only if user has enabled it.
		if(notificationSettings !=undefined && notificationSettings.showApprovalNotification == 'true'){
			ns.common.showMe("approvalDisplayHolder");	
		}
		
		//Refresh the approval pane
		$.publish("common.refreshApprovals");
	};
	
	ns.common.showDAWirePayeeApprovalNotification = function(detail,notificationSettings){
		var approvalTitle =  "Wire beneficiary to approve.";
		var approvalDate = detail.map.data.submittedDate;
		var approvalSubmittedBy = detail.map.submittedByName;
		
		$("#approvalTitle").html(approvalTitle);
		$("#approvalDate").html(approvalDate);
		$("#approvalSubmittedBy").html(approvalSubmittedBy);
		$("#approvalAmount").html('');
		$("#approvalAmountArea").hide();
		$("#approvalItemType").val(detail.eventType);
		
		$("#msgDisplayHolder").css("display", "none");
		$("#alertDisplayHolder").css("display", "none");
		
		//Show notification only if user has enabled it.
		if(notificationSettings !=undefined && notificationSettings.showApprovalNotification == 'true'){
			ns.common.showMe("approvalDisplayHolder");	
		}
		
		//Refresh the approval pane
		$.publish("common.refreshApprovals");
	};
	
	ns.common.showDAACHePayeeApprovalNotification = function(detail,notificationSettings){
		var approvalTitle =  "ACH participant to approve.";
		var approvalDate = detail.map.data.submittedDate;
		var approvalSubmittedBy = detail.map.submittedByName;
		
		$("#approvalTitle").html(approvalTitle);
		$("#approvalDate").html(approvalDate);
		$("#approvalSubmittedBy").html(approvalSubmittedBy);
		$("#approvalAmount").html('');
		$("#approvalAmountArea").hide();
		$("#approvalItemType").val(detail.eventType);
		
		$("#msgDisplayHolder").css("display", "none");
		$("#alertDisplayHolder").css("display", "none");
		
		//Show notification only if user has enabled it.
		if(notificationSettings !=undefined && notificationSettings.showApprovalNotification == 'true'){
			ns.common.showMe("approvalDisplayHolder");	
		}
		
		//Refresh the approval pane
		$.publish("common.refreshApprovals");
	};
	
	ns.common.showDAAdminPayeeApprovalNotification = function(detail,notificationSettings){
		var approvalTitle =  "Admin changes to approve.";
		var approvalDate = detail.map.data.submittedDate;
		var approvalSubmittedBy = detail.map.submittedByName;
		
		$("#approvalTitle").html(approvalTitle);
		$("#approvalDate").html(approvalDate);
		$("#approvalSubmittedBy").html(approvalSubmittedBy);
		$("#approvalAmount").html('');
		$("#approvalAmountArea").hide();
		$("#approvalItemType").val(detail.eventType);
		
		$("#msgDisplayHolder").css("display", "none");
		$("#alertDisplayHolder").css("display", "none");
		
		//Show notification only if user has enabled it.
		if(notificationSettings !=undefined && notificationSettings.showApprovalNotification == 'true'){
			ns.common.showMe("approvalDisplayHolder");	
		}
		
		//Refresh the approval pane
		$.publish("common.refreshApprovals");
	};
	
	
	ns.common.quickViewMessage = function(event){
		var viewURL = '/cb/pages/jsp/message/MessageViewAction_viewNotificationMessage.action?MessageID=';
		var recievedMsgID = $("#recievedMsgID").val();
		//console.log(recievedMsgID);
		if(recievedMsgID != ''){
			viewURL = viewURL + recievedMsgID;
			$.ajax({
				url : viewURL,
				success : function(data) {
					$("#msgDisplayHolder").css("display", "none");
					var config = ns.common.dialogs["quickViewMessageDialog"];
					var beforeDialogOpen = function() {
						$('#quickViewMsgDlgBand1').html(data);
						$('#quickViewMsgDlgBand1').show();
						$('#quickViewMsgDlgBand2').hide();
						$('#quickViewMsgDialogID').dialog('open');
						
						$('#MessageSmallPanel tr.currentmsg td').removeClass('unreadMessage');
						if ('home_message' === ns.home.lastMenuId
								&& 'Inbox' === ns.message.mailbox) {
							$('#inboxMessagesGridId').trigger("reloadGrid");
						}
					}
					config.beforeOpen = beforeDialogOpen;
					ns.common.openDialog("quickViewMessageDialog");
				}
			});
		}
	};
	
	ns.common.quickViewAlert = function(event) {
		var viewURL = '/cb/pages/jsp/alert/alertQuickViewAction.action?alertId=';
		var recievedAlertID = $("#recievedAlertID").val();
		
		if(recievedAlertID != ''){
			viewURL = viewURL + recievedAlertID;
			$.ajax({
				url : viewURL,
				success : function(data) {
						$("#alertDisplayHolder").css("display", "none");
						var config = ns.common.dialogs["quickViewAlertDialog"];
						var beforeDialogOpen = function() {
							if ($('#quickViewAlertDialogID #quickViewAlertDlgBand1').length == 0) {
								// These two div's are required since html is
								// appended to these div's and not dialog container
								// directly
								$('#quickViewAlertDialogID').html("<div id='quickViewAlertDlgBand1'></div><div id='quickViewAlertDlgBand2'></div>");
							}
							
							$('#quickViewAlertDlgBand1').html(data);
							$('#quickViewAlertDlgBand1').show();
							$('#quickViewAlertDlgBand2').hide();
							$('#quickViewAlertDialogID').dialog('open');
							
							$('#AlertSmallPanel tr.currentmsg td').removeClass('unreadMessage');
							if ('home_alert' === ns.home.lastMenuId && 'inboxAlertsGridId' === ns.alert.whichgrid) {
								$('#inboxAlertsGridId').trigger("reloadGrid");
							}
						}
						config.beforeOpen = beforeDialogOpen;
						ns.common.openDialog("quickViewAlertDialog");
						if($("#inboxAlertsGridId").is(":visible") === true){
							$("#inboxAlertsGridId").trigger("reloadGrid");
						}
					}
			});
		}
	};
	
	ns.common.visitApprovalsArea = function(event) {
		var approvalItemType = $("#approvalItemType").val();
		if(approvalItemType != ''){
			$("#approvalDisplayHolder").css("display", "none");
			switch(approvalItemType){
			case "TRANSACTION_APPROVAL":
				$('li[menuId=\"approvals_pending\"] a').click();
				break;
			case "RELEASE_WIRE_APPROVAL":
				ns.shortcut.goToFavorites('pmtTran_wire,releaseWiresLink');
				break;
			case "DA_WIRE_APPROVAL":
				ns.shortcut.goToFavorites('approvals_pending,pendingPayess');
				break;
			case "DA_ACH_APPROVAL":
				ns.shortcut.goToFavorites('approvals_pending,pendingPayess');
				break;
			case "DA_ADMIN_APPROVAL":
				$('li[menuId=\"admin_summary\"] a').click();
				break;
			}
		}
	};
	
	ns.common.hideImmediately = function(objIdParam){
		$("#"+objIdParam).hide();
	};

	ns.common.hideMe = function(objIdParam){
		$("#"+objIdParam).hide( "drop", { direction: "down" }, "medium");
	};

	ns.common.showMe = function(objIdParam){
		$("#"+objIdParam).fadeIn("slow").delay(4400).fadeOut();
	};
	
	ns.common.processBankingEvents = function(detail, notificationSettings){
		switch(detail.eventType){
		case "MESSAGE":
			ns.common.showMsgNotification(detail, notificationSettings);
			break;
		case "ALERT":
			ns.common.showAlertNotification(detail, notificationSettings);
			break;
		case "TRANSACTION_APPROVAL":
			ns.common.showTransactionApprovalNotification(detail, notificationSettings);
			break;
		case "RELEASE_WIRE_APPROVAL":
			ns.common.showReleaseWireApprovalNotification(detail, notificationSettings);
			break;
		case "DA_WIRE_APPROVAL":
			ns.common.showDAWirePayeeApprovalNotification(detail, notificationSettings);
			break;
		case "DA_ACH_APPROVAL":
			ns.common.showDAACHePayeeApprovalNotification(detail, notificationSettings);
			break;
		case "DA_ADMIN_APPROVAL":
			ns.common.showDAAdminPayeeApprovalNotification(detail, notificationSettings);
			break;
		}
	};
	
});

$.subscribe('ns.common.getBankingEvents', function(event,data) {
	$.ajax({
		url: '/cb/pages/jsp/personalization/getBankingEvents.action',
		global:false,
		data: {lastSyncDate: ns.common.lastSyncDate, loginDate:ns.common.loginDate},
		success: function(data){
			var unreadMsgs = 0;
			var unreadAlerts = 0;
			var unreadApprovals = 0;
			
			ns.common.lastSyncDate = data.lastSyncDate;
			if(data.unreadNotificationCounts != undefined && data.unreadNotificationCounts.unreadMsgs != undefined){
				unreadMsgs = data.unreadNotificationCounts.unreadMsgs;
				if(unreadMsgs == 0){
					$("#mailMsgIndicator").html(unreadMsgs).fadeOut();
				}else{
					$("#mailMsgIndicator").html(unreadMsgs).fadeIn();
				}
			}
			
			if(data.unreadNotificationCounts != undefined &&  data.unreadNotificationCounts.unreadAlerts != undefined){
				unreadAlerts = data.unreadNotificationCounts.unreadAlerts;
				if(unreadAlerts == 0){
					$("#alertMsgIndicator").html(unreadAlerts).fadeOut();
				}else{
					$("#alertMsgIndicator").html(unreadAlerts).fadeIn();
				}
			}
			
			if(data.unreadNotificationCounts != undefined &&  data.unreadNotificationCounts.unreadApprovals != undefined){
				unreadApprovals = 	data.unreadNotificationCounts.unreadApprovals;
				if(unreadApprovals == 0){
					$("#approvalMsgIndicator").html(unreadApprovals).fadeOut();
				}else{
					$("#approvalMsgIndicator").html(unreadApprovals).fadeIn();
				}
			}
			
			var delay = 1000;
			$.each(data.bankingEventList, function(i, detail) {
				setTimeout( function(){ ns.common.processBankingEvents(detail,data.notificationSettings);}, delay);
				delay += 5000;
			});
		}
	});
});

var chartCount = 0;
ns.common.initAvailAcntChartCntr = function (numParam) {
	chartCount = numParam;
	
}

ns.common.updateAvailAcntChartCntr = function (flagParam) {
	/*
	 flagParam = true; // add one counter
	 flagParam = false; // remove one counter
	 */
	
	if(flagParam) {
		chartCount++;
	} else {
		chartCount--;
	}
	
}

/* Cashflow var check */
var cashFlowChartCount = 0;
ns.common.initAvailCashflowAcntChartCntr = function (numParam) {
	cashFlowChartCount = numParam;
	
}

ns.common.updateAvailCashflowAcntChartCntr = function (flagParam) {
	/*
	 flagParam = true; // add one counter
	 flagParam = false; // remove one counter
	 */
	
	if(flagParam) {
		cashFlowChartCount++;
	} else {
		cashFlowChartCount--;
	}
	
}
/* Cashflow var check */

ns.common.setAccountIcon = function (accountType){
	var accountIcon = "";
	if(accountType == "Unknown"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "Checking"){
		accountIcon="<span class='sapUiIconCls icon-commission-check paddingLeft10 marginRight10' style='width:auto;'></span>";
	}
	else if(accountType == "Savings" || accountType == "Deposit Accounts"){
		accountIcon="<span class='sapUiIconCls icon-wallet paddingLeft10 marginRight10' style='width:auto;'></span>";
	}
	else if(accountType == "Credit Card"){
		accountIcon= "<span class='sapUiIconCls icon-credit-card paddingLeft10 marginRight10' style='width:auto;'></span>";
	}
	else if(accountType == "Loan" || accountType == "Loans"){
		accountIcon="<span class='sapUiIconCls icon-capital-projects paddingLeft10 marginRight10' style='width:auto;'></span>";
	}
	else if(accountType == "Mortgage"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "Home Equity"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "Line of Credit"){
		accountIcon="<span class='sapUiIconCls icon-my-sales-order paddingLeft10 marginRight10' style='width:auto;'></span>";
	}
	else if(accountType == "CD"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "IRA"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "Stock"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "Brokerage"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "Money Market"){
		accountIcon = "<span class='sapUiIconCls icon-waiver paddingLeft10 marginRight10' style='width:auto;'></span>";
	}
	else if(accountType == "Business Loan"){
		accountIcon="<span class='sapUiIconCls icon-capital-projects paddingLeft10 marginRight10' style='width:auto;'></span>";
	}
	else if(accountType == "Fixed Deposit"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "Other"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "General Ledger"){
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'>Test</span>";
	}
	else if(accountType == "Asset Accounts"){
		accountIcon="<span class='sapUiIconCls icon-retail-store paddingLeft10 marginRight10' style='width:auto;'></span>";
	}
	else{
		accountIcon="<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10' style='width:auto;'></span>";
	}
	return accountIcon;
};

ns.common.showSAPUI5Help = function(){
	var strProjectPath;
	if( ns.home.appUserType == 'Business'){
		strProjectPath = 'Business';
	} else if(ns.home.appUserType == 'Consumers'){
		strProjectPath = 'Consumers';
	}
	if(typeof mainApp === 'undefined'){
		throw new Error("Application not initialized");
	}
	if(mainApp.helpModule){
	
		mainApp.helpModule.showHelp(strProjectPath);
	}
	
};

ns.common.showDetailsHelp = function(detailDivID){
	if (!detailDivID.startsWith('#')){
		detailDivID = '#' + detailDivID;
	}
	var helpFile = ""; 
	
	if($(detailDivID).hasClass('wizardSupportCls') || $(detailDivID).hasClass('bigWizardSupportCls')){//for portlets having wizard                                                                        
		$(detailDivID).find('.ui-tabs-panel').each(function(){
		    if($(this).attr('aria-hidden') == "false"){//Find visible wizard step div
		    	helpFile = $(this).find('.moduleHelpClass').html();
		    }
		});
	}else if($(detailDivID).hasClass('gridPannelSupportCls')){//for portlets having multiple grids in a panel
		if($(detailDivID +" .gridSummaryContainerCls").length > 0){ // Summary grid is available
            helpFile = $(detailDivID).find(".gridSummaryContainerCls:visible").find('.moduleHelpClass').eq(0).html();  
	    } else { 
	            helpFile = $(detailDivID).find('.moduleHelpClass').eq(0).html();
	    }
	}else if($(detailDivID).hasClass('multipleStepsCls')){
		$(detailDivID).find('.moduleHelpClass').each(function(){
		    if($(this).parent().is(":visible")){//Find visible parent of help div
		    	helpFile = $(this).html();
		    }
		});
		if(helpFile == ''){
			helpFile = $(detailDivID).find('.moduleHelpClass').eq(0).html();
		}
	}
	else{//for any other portlets
		helpFile = $(detailDivID).find('.moduleHelpClass').eq(0).html();	
	}
	
	if(helpFile == undefined){
		helpFile = '';
	}
	
	callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
};

ns.common.showMenuHelp = function(subMenuID){
	$(".submenuHolderBgCls").addClass("hideSubmenuHolder");
	
	var helpFile = ""; 
	helpFile = $('#'+subMenuID).find('.moduleHelpClass').eq(0).html();	
	
	
	if(helpFile == undefined){
		helpFile = '';
	}
	
	callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
};

ns.common.bookmarkThis = function(detailDivID){
	if (!detailDivID.startsWith('#')){
		detailDivID = '#' + detailDivID;
	}
	var path = $(detailDivID).find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.shortcutPathClass').html();
	var ent  = $(detailDivID).find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.shortcutEntitlementClass').html();
	
	if(path == undefined) {
		path = $(detailDivID).find('.shortcutPathClass').html();
		ent  = $(detailDivID).find('.shortcutEntitlementClass').html();
	}
	
	ns.shortcut.openShortcutWindow( path, ent );
};

ns.common.handleOdataError = function(e){
	if(e){
		var sErrBody = e.response.body;			
		var errObj = JSON.parse(sErrBody);
		var oError;
		if(errObj){
			oError = errObj.error;
			if(oError.code == "VE"){
				console.info("Validation Error occured");
				var sMsg = oError.message.value;
				var oErrorMessage = JSON.parse(sMsg);
				

			}
		}

	}	
};

//Function which adds 3 step wizard status to portlet
ns.common.gotoWizardStep = function(portletID, wizardStepNum,numberOfWizardSteps){
	if(portletID !=undefined && portletID != ''){
		if (!portletID.startsWith('#')){
			portletID = '#' + portletID;
		}
		
		var wizardStep = "__wizardStep" + wizardStepNum;
		
		$(portletID + " .wizardProgressIndicatorCls li" ).each(function(i){
			if(wizardStepNum == 1) {
				$(this).removeClass().addClass("wizardStepNormalIndicator");
			}
	    });
		
		//hiding bookmark on step 2 and/or 3
		if (wizardStepNum == 1)
		{
			ns.common.displayBookmark( portletID , "block" ); //show bookmark on step1
		}
		else
		{
			ns.common.displayBookmark( portletID , "none" ); //hide bookmark on step 2 and/or 3
		}
		
		$(portletID).find('#'+wizardStep).removeClass().addClass("wizardStepSelectedIndicator active");
		
		//if numberOfWizardSteps is provided & is lesser than 3, then we need to hide additional wizard steps that are rendered.
		//This is required in case of Custom mapping workflow, which has only 2 steps.
		if(numberOfWizardSteps != undefined &&  numberOfWizardSteps!=''){
			for(var counter = numberOfWizardSteps+1; counter < 4; counter++) {
				var currentWizardStep = "__wizardStep" + counter;
				$(portletID).find('#'+currentWizardStep).hide();
			}
		}
	}
};

//Function to show/hide the bookmark option
ns.common.displayBookmark = function( portletID , displayValue ){
	$(portletID + " .portletHeaderMenuContainer ul li a").each(function(i){
		var bookmarkStr = $(this).attr("onclick").substring(0,22); // extract the bookmark function name
		
		if( bookmarkStr == "ns.common.bookmarkThis" )
		{
			// set the displayValue to either show or hide the bookmark
			$(this).parent().css("display" , displayValue );
		}
	});
	
};

//Function which adds 3 step wizard status to portlet
ns.common.gotoAddDeviceWizardStep = function(portletID, wizardStepNum,numberOfWizardSteps){
	if(portletID !=undefined && portletID != ''){
		if (!portletID.startsWith('#')){
			portletID = '#' + portletID+"_portlet";
		}
		
		var wizardStep = "__wizardStep" + wizardStepNum;
		// debugger;
		$(portletID + " .wizardProgressIndicatorCls li" ).each(function(i){
			if(wizardStepNum == 1) {
				$(this).removeClass().addClass("wizardStepNormalIndicator");
			}
	    });
		
		$(portletID).find('#'+wizardStep).removeClass().addClass("wizardStepSelectedIndicator active");
		
		//if numberOfWizardSteps is provided & is lesser than 3, then we need to hide additional wizard steps that are rendered.
		//This is required in case of Custom mapping workflow, which has only 2 steps.
		if(numberOfWizardSteps != undefined &&  numberOfWizardSteps!=''){
			for(var counter = numberOfWizardSteps+1; counter < 4; counter++) {
				var currentWizardStep = "__wizardStep" + counter;
				$(portletID).find('#'+currentWizardStep).hide();
			}
		}
	}
};

//This function initializes a div as portlet with settings icon
ns.common.initializePortlet = function(portletID, generateDOMFlag, showCalendarLauncher, calendarClickFunc){
	if(portletID !=undefined && portletID != ''){
		if (!portletID.startsWith('#')){
			portletID = '#' + portletID;
		}
		
		if(generateDOMFlag == undefined || generateDOMFlag == '' || generateDOMFlag != true){
			generateDOMFlag = false;
		}
		
		$(portletID).portlet({
			generateDOM: generateDOMFlag,
		});
		
		ns.common.addSettingsIconToPortlet(portletID);
		
		if($(portletID).hasClass("wizardSupportCls")){
			ns.common.addWizardStatusToPortlet(portletID);	
    	}
			
		if(showCalendarLauncher != undefined && showCalendarLauncher == true && $.isFunction(calendarClickFunc)){
			ns.common.addCalendarToPortlet(portletID,calendarClickFunc);	
		}
	}
};

//Function which adds 3 step wizard status to portlet
ns.common.addWizardStatusToPortlet = function(portletID){
    if(portletID !=undefined && portletID != ''){
    	if (!portletID.startsWith('#')){
    		portletID = '#' + portletID;
    	}
    	
    	$(portletID).portlet('addWizardStatusContainer');
	}else{
		console.log("Please provide valid Portlet id.");
	}
};

//Function which adds custom wizard status to portlet
ns.common.addCustomWizardStatusToPortlet = function(portletID){
    if(portletID !=undefined && portletID != ''){
    	if (!portletID.startsWith('#')){
    		portletID = '#' + portletID;
    	}
		var portletWizardStatusId = portletID.substring(1) + "CustomWizardStatus" ;
		var $portletWizardStatus = $('<span/>').attr('id', portletWizardStatusId).addClass("wizardStepIndicatorHolder");
		
		// var wizardStepIndicator = "<span id='__wizardStep1' class='wizardStepCls' /><span id='__wizardStep2' class='wizardStepCls' /><span id='__wizardStep3' class='wizardStepCls' />";
		var wizardStepIndicator = "<ul class='wizardProgressIndicatorCls'><li id='__wizardStep1'><span>"+js_input+"</span></li><li id='__wizardStep2'><span>"+js_verify+"</span></li><li id='__wizardStep3'><span>"+js_confirm+"</span></li></ul>";
		
		$($portletWizardStatus).html(wizardStepIndicator);
		$(portletID).portlet('addCustomWizardStatusContainer', $portletWizardStatus);
	}else{
		console.log("Please provide valid Portlet id.");
	}
};

//Function which adds settings icon to portlet
ns.common.addCalendarToPortlet = function(portletID, calendarClickFunc){
	if(portletID !=undefined && portletID != ''){
		if (!portletID.startsWith('#')){
			portletID = '#' + portletID;
		}
		
		var portletCalendarID = portletID.substring(1) + "CalendarLauncher" ;
		var $portletCalendar = $('<span/>').attr('id', portletCalendarID).addClass("sapUiIconCls icon-calendar portletSettingsIcon ui-widget-content");
		
		$portletCalendar.attr("title","View as Calendar");
		
		$portletCalendar.bind('click', calendarClickFunc);
		
		$(portletID).portlet('addCalendarContainer', $portletCalendar);
		
	}else{
		console.log("Please provide valid Portlet id.");
	}
};

//Function which adds settings icon to portlet
ns.common.addSettingsIconToPortlet = function(portletID){
	if(portletID !=undefined && portletID != ''){
		if (!portletID.startsWith('#')){
			portletID = '#' + portletID;
		}
		
		var portletSettingsMenuDiv = $(portletID+ " .portletHeaderMenuContainer").length;
		if(portletSettingsMenuDiv > 0 ){
			var portletSettingsMenuID = portletID.substring(1) + "SettingsMenu" ;
			var $portletSettings = $('<span/>').attr('id', portletSettingsMenuID).addClass("sapUiIconCls icon-action-settings portletSettingsIcon ui-widget-content");
			$portletSettings.attr('title', 'Settings menu');
		    $portletSettings.mouseover(function(event) {
		        var lMarginFromRight = 56;
		        var lMarginFromTop = 25;
		        var lLeftPos = $(this).position().left;
		        var lTopPos = parseInt($(this).offset().top) + lMarginFromTop;
		        var lMenuWidth = $(portletID+ " .portletHeaderMenuContainer").width() - lMarginFromRight; 

		       $(portletID+ " .portletHeaderMenuContainer").css("left", parseInt(lLeftPos - lMenuWidth)).css("top", lTopPos);
			   $(portletID+ " .portletHeaderMenuContainer").fadeIn().mouseenter(function(event) {
				   		ns.common.isMenuRollOver = true;
			     }).mouseleave(function(event) {
		    	 		ns.common.isMenuRollOver = false;
			            $(portletID+ " .portletHeaderMenuContainer").fadeOut();
			     });
			   
			   $(portletID+ " .portletHeaderMenuContainer li").mouseover(function(){
			     $(this).addClass("ui-state-hover");
			   }).mouseout(function() {
			     $(this).removeClass("ui-state-hover");
			   });
			     }).mouseout(function(){
			           setTimeout(function() {
			                  if(!ns.common.isMenuRollOver) {
			                         $(portletID+ " .portletHeaderMenuContainer").fadeOut();
			                  }
			           }, 300);
			     });
			     
			     $(portletID).portlet('addCustomContainer', $portletSettings);
		}else{
			console.log("No portlet settings menu found in container.");
		}
		
	}else{
		console.log("Please provide valid Portlet id.");
	}
};

$(".dashboardSummaryHolderDiv a span").live("click", function (event){
	//Ignore any click events on home page, since all action open modal dialog, so no need to set any action selected
	var ignoreHomePageElement = ns.common.ignoreHomePageElement(this);
	if(ignoreHomePageElement == true){
		return;
	}
	
	$(".dashboardSelectedItem" ).each(function(i){
		//Unselect any selected dashboard items only if they are not from homepage, as homepage is always present in background, so we need not unselect Home page items.
		var isHomePageElementCount = $("#desktopDashboard").find(this).length;
		if(isHomePageElementCount == 0){
			$(this).removeClass("dashboardSelectedItem");
		}
    });
	
	var prefDbSelectedItem = $(event.target);
	prefDbSelectedItem.attr("id", "__currentSelectedDbItem");
	$(prefDbSelectedItem).addClass("dashboardSelectedItem");
});


ns.common.ignoreHomePageElement = function(element){
	var isHomePageElementCount = $("#desktopDashboard").find(element).length;
	var isSummaryElementCount = $("#desktopDashboard").find(".summaryLabelCls").find(element).length;
	
	if(isHomePageElementCount>0 && isSummaryElementCount==0){
		return true;
	}else{
		return false;
	}
};

ns.common.updatePortletTitle = function(portletId,title,searchAvailable){
	if(portletId !=undefined && portletId != ''){
		if (!portletId.startsWith('#')){
			portletId = '#' + portletId;
		}
	}
	
	if(searchAvailable!= undefined){
		if(searchAvailable == true){
			$(portletId).find(".searchHeaderCls").show();
			var portletTitle = $(portletId).find(".portlet-title").length;
			if(portletTitle>0 && title!= undefined && title !=''){
				$(portletId).find(".portlet-title").hide();
			}
			if(title!= undefined && title !=''){
				$(portletId).find('.selectedTabLbl').html(title);
			}
		}else{
			$(portletId).find(".searchHeaderCls").hide();
			var portletTitle = $(portletId).find(".portlet-title").length;
			if(portletTitle>0 && title!= undefined && title !=''){
				$(portletId).find(".portlet-title").html(title).show();
			}
		}
	}else{
		if(title!= undefined && title !=''){
			$(portletId).find('.selectedTabLbl').html(title);
		}
	}
};

ns.common.showNextTabSummary = function(){
	
};


ns.common.showTabSummary = function(tabId,tabUrl){
	ns.admin.topLavelPermissionTab = tabId;
	
	//Hide all Tab Summary containers
	$(".tabSummaryContainerCls" ).each(function(i) {
        $(this).hide();
    });
	
	var tabSummaryId =  "#tabSummaryContainer";
    
	$.ajax({    
		  url: tabUrl,
		  data: {secondLavelPermissionTabIndex: ns.admin.secondLavelPermissionTab},
		  success: function(data) {
			 $(tabSummaryId).show();
			 $(tabSummaryId).html("");
			 $(tabSummaryId).html(data);
			 $.publish("common.topics.tabifyDesktop");
			 if(accessibility){
				$("#notes").setFocus();
			 }
			 setTimeout(function() {
			      $.publish("permissionTabLoaded");
			}, 300);
			
		  }   
	});   
    
   
    //Show all Grid tab dropdown items
	$(".gridTabDropdownItem" ).each(function(i){
        $(this).show();
    });
	
	//Get title of user selected grid tab
	var selectedTabTitle = $("#"+tabId).html().trim();
	if(selectedTabTitle != undefined && selectedTabTitle!= ''){
		$("#selectedPermissionTab").html(selectedTabTitle); //Set selected grid tab title
	}
	
	
};

ns.common.showGridSummary = function(gridTabId){
	//Hide all Grid Summary containers
	$(".gridSummaryContainerCls" ).each(function(i) {
        $(this).hide();
    });
	
	var gridSummaryId = gridTabId + "SummaryGrid";
	//Show the user selected Grid Summary container
    $('#'+gridSummaryId).show();
    $('#'+gridSummaryId).removeClass('hidden');
    var gridId = $('#'+gridSummaryId).attr('gridId'); //retrive the underlying Grid Id from the 'gridId' attribute of Grid Summary container
    //By default to avoid grid loading, datatype is local, make it json and reload grid.
    ns.common.setFirstGridPage('#'+gridId);
    $('#'+gridId).jqGrid('setGridParam',{datatype:'json'}).trigger("reloadGrid");
    
	//Find user selected grid tab
	var userSelctorGridTab = gridTabId;
	//Hide user selected grid tab from dropdown menu.
	//$("#"+userSelctorGridTab).hide();
	
	//Get title of user selected grid tab
	var selectedGridTabTitle = $("#"+userSelctorGridTab).html().trim();
	if(selectedGridTabTitle != undefined && selectedGridTabTitle!= ''){
		$("#selectedGridTab").html(selectedGridTabTitle); //Set selected grid tab title
	}
};

$.subscribe('reloadSelectedGridTab', function(event,data) {	
	var id = $(".gridSummaryContainerCls:visible").attr('gridId');
	ns.common.reloadFirstGridPage(id);
});

$.subscribe('showSummary', function(event,data) {
	var clickedElement = data.id;
	var summaryDivId = $('#'+clickedElement).attr('summaryDivId');
	var buttonType = $('#'+clickedElement).attr('buttonType');
	ns.common.showSummaryOnSuccess(summaryDivId,buttonType);
});

ns.common.showSummaryOnSuccess = function(summaryDivId,buttonType){
	ns.common.selectDashboardItem($(".summaryLabelCls:visible").prop('id'));//Show current summary selected
	var summaryContent = $.trim($('#'+summaryDivId).html());
	var isGridPresent = false;
	if(summaryContent == ""){
		$(".summaryLabelCls:visible").trigger('click');
	}
	else{
		if(buttonType == 'done'){
			var id = "";
			if($(".gridSummaryContainerCls").length > 0){
				$(".gridSummaryContainerCls").each(function(){
					id = $(this).attr("gridId");
					if(!$(this).hasClass('hidden')){
						if($(this).css('display')!="none"){						
						ns.common.reloadFirstGridPage(id);
						return false;	
						}					
					}else if($(this).css('display') !="none"){
						ns.common.reloadFirstGridPage(id);
						return false;
					}
				});
			}else{
				$("#"+summaryDivId +" .ui-jqgrid").each(function(){
					isGridPresent = true;
					id = $(this).attr("id").substr(5);
					ns.common.reloadFirstGridPage(id);
				});
				if(!isGridPresent){
					$(".summaryLabelCls:visible").trigger('click');
				}
			}
		}
	}
};

ns.common.unselectDashboardItems  = function(dashBoardItemIdWithoutHash){
	if(dashBoardItemIdWithoutHash!=undefined && dashBoardItemIdWithoutHash !=''){
		$("#"+dashBoardItemIdWithoutHash).removeClass("dashboardSelectedItem");
	}else{
		$(".dashboardSelectedItem").each(function(i){
			//Unselect any selected dashboard items only if they are not from homepage, as homepage is always present in background, so we need not unselect Home page items.
			var isHomePageElementCount = $("#desktopDashboard").find(this).length;
			if(isHomePageElementCount == 0){
				$(this).removeClass("dashboardSelectedItem");
			}
		});
	}
};

ns.common.selectDashboardItem  = function(dashBoardItemIdWithoutHash){
	ns.common.unselectDashboardItems();
	if(dashBoardItemIdWithoutHash!=undefined || dashBoardItemIdWithoutHash !=''){
		$("#"+dashBoardItemIdWithoutHash).find("span").addClass("dashboardSelectedItem");
	}
};

ns.common.refreshDashboard = function(summaryDivId,prefixTrimmingRequired){
	var summaryDivIdStr = "";
	if(prefixTrimmingRequired !=undefined && prefixTrimmingRequired == true){
		// start index is set to 4 to remove "show" to get rest of the string value which will represent the targeted div
		summaryDivIdStr = summaryDivId.substr(4, summaryDivId.length);
	}else{
		summaryDivIdStr = summaryDivId;
	}
	
	//$(".dashboardSummaryHolderDiv").addClass("hideDbOnLoad");
	$( ".dashboardSummaryHolderDiv" ).each(function(i){
		//Unselect any selected dashboard items only if they are not from homepage, as homepage is always present in background, so we need not unselect Home page items.
		var isHomePageElementCount = $("#desktopDashboard").find(this).length;
		if(isHomePageElementCount == 0){
			$(this).addClass("hideDbOnLoad");
		}
	});
	  
    $("#"+summaryDivIdStr).removeClass("hideDbOnLoad");
    
    $(".dashboardSelectedItem").each(function(i){
    	//Unselect any selected dashboard items only if they are not from homepage, as homepage is always present in background, so we need not unselect Home page items.
    	var isHomePageElementCount = $("#desktopDashboard").find(this).length;
		if(isHomePageElementCount == 0){
			$(this).removeClass("dashboardSelectedItem");
		}
    });
    
    $("#"+summaryDivIdStr).find(".summaryLabelCls").find("span:first-child").addClass("dashboardSelectedItem");
    
    $(".dashboardSubmenuItem").removeClass("hideDbOnLoad");
    $( ".dashboardSubmenuItem" ).each(function(i) {
    	$(this).find("a").removeClass("hideDbOnLoad");
    });
    
    if(prefixTrimmingRequired !=undefined && prefixTrimmingRequired == true){
    	$("#"+summaryDivId).addClass("hideDbOnLoad");
    	$("#"+summaryDivIdStr).find(".summaryLabelCls").click();
    }else{
    	$("#"+"show"+summaryDivId).addClass("hideDbOnLoad");
    }
};

//Forcefully reload the current visible dashboard to load the summary again
ns.common.forceReloadCurrentSummary = function() {
	var dashboardSummaryId = $(".dashboardSummaryHolderDiv:visible").attr("id");
	ns.common.refreshDashboard('show'+dashboardSummaryId,true);
};

ns.common.isElementVisible = function(elementId) {
	return $("#" + elementId).is(":visible");
};


/*Function which will load the module while passing the user specified summary and specific grid */
ns.common.loadSubmenuSummaryGrid = function(submenuURL,menuId,summaryToLoad,visibleGrid){
	$(".submenuHolderBgCls").addClass("hideSubmenuHolder").removeClass("hideSubmenuHolder");
	
	$.ajax({
		  type: "GET",
		  data: {summaryToLoad: summaryToLoad,visibleGrid:visibleGrid},
		  url: submenuURL,		
		  success: function(data){
		  	if(data){
//				console.log('success');
				ns.common.handleTopMenuEvents(menuId);
				ns.common.handleSubmenuEvents(menuId);
				$("#notes").html(data);
		  	}		  	
		  },
		  error: function(jqXHR, textStatus, errorThrown ){
//			  console.log('error');  	
		  }
	});
};

/*Function which will load the module while passing the user specified summary or any dashboard element to be clicked to load specific form */
ns.common.loadSubmenuSummary = function(submenuURL,menuId,summaryToLoad,dashboardElementId){
	$(".submenuHolderBgCls").addClass("hideSubmenuHolder").removeClass("hideSubmenuHolder");
	
	$.ajax({
		  type: "GET",
		  data: {summaryToLoad: summaryToLoad,dashboardElementId:dashboardElementId},
		  url: submenuURL,		
		  success: function(data){
		  	if(data){
//				console.log('success');
				ns.common.handleTopMenuEvents(menuId);
				ns.common.handleSubmenuEvents(menuId);
				$("#notes").html(data);
		  	}		  	
		  },
		  error: function(jqXHR, textStatus, errorThrown ){
//			  console.log('error');  	
		  }
	});
};

ns.common.handleTopMenuEvents = function(newMenuId){
//	console.log('other function starts');
	//Required for showing module specific loading gif(spinning wheel and blocking div)
	ns.home.hideGlobalMessage();
	var menuId = ns.home.currentMenuId;
	//ns.home.setMenuBackgroundStyle(newMenuId,false);
	
	ns.home.currentMenuId = newMenuId;

//	console.log('old menuId=>'+menuId);
//	console.log('newly clicked ns.home.currentMenuId=>'+ns.home.currentMenuId);

	ns.common.hidePrevMenuLoading(menuId);

//	console.log('other function ends');
	$.publish("onTopMenuClick",[menuId,ns.home.currentMenuId]);	
};
	

ns.common.handleSubmenuEvents = function(menuId){
	
	var submenuItem = $('li[menuId="' + menuId + '"]');
//	console.log('menuId=>'+menuId);
	ns.home.menuRefresh();
	
	ns.home.setMenuBackgroundStyle(menuId,true);
	
	ns.home.currentMenuId = menuId;
	
	//Highlight the selected main menu
	ns.home.highlightMainMenuOfSubmenu($(submenuItem));
	
	ns.home.doSessionCleanUp();
	
	//When user clicks back button, when any dialog is open, this call is necessary to close it
	ns.common.closeDialog();
	
	//remove dialogs
	if(!ns.common.shortcut){
		ns.home.removeDialogs(menuId);
	}
	
	var subMenuName = $('li[menuId="' + menuId + '"]').find('a').text();
//	console.log('subMenuName=>'+subMenuName);
	
	ns.home.loadGlobalMsg(menuId);
	
	// Add browser histort entry. This is added for back and forward button support
	if(window.parent && window.parent.addHistoryEntry){
		window.parent.addHistoryEntry(menuId);
		window.parent.changeWindowTitle(subMenuName);
	}
	
	$.publish("subMenuOnCompleteTopics");
};



ns.common.populateAccountDetails = function() {
	var bankDetailsString = document.getElementsByName('accountHistorySearchCriteria.selectedAccountOption')[0].value;  
	if (bankDetailsString != null) {
		var stringArray = bankDetailsString.split(",");
		if(bankDetailsString != null && stringArray.length > 0){
			document.getElementsByName('accountHistorySearchCriteria.account.ID')[0].value = stringArray[0];
			document.getElementsByName('accountHistorySearchCriteria.account.bankID')[0].value = stringArray[1];
			document.getElementsByName('accountHistorySearchCriteria.account.routingNum')[0].value = stringArray[2];
		}
	}
};
   
ns.common.setModuleType = function(moduleType,successFunction){
	ns.common.moduleType = moduleType;
	// Call extra Success function, if provided.	
	if(successFunction && (typeof successFunction === 'function')){
		successFunction.call();
	}
};

//This function applies local ajax settings
//which will make the non blocking UI ajax calls
//aConfig {object}, pass containerId which would be used to show busyIndicator
ns.common.applyLocalAjaxSettings = function(aConfig){
	var _beforeSend = aConfig.beforeSend;

	var _complete =  aConfig.complete;

	var localAjaxSettings = {
		global: false,
		beforeSend: function(){
			if(aConfig.containerId){
				$("#" + aConfig.containerId).closest(".portlet-content").setBusy(true);	
			}
			
			if(_beforeSend){
				_beforeSend.apply(this, arguments);
			}
		},
		complete: function(){
			//$.ajaxSetup({global: false});
			if(aConfig.containerId){
				$("#" + aConfig.containerId).closest(".portlet-content").setBusy(false);	
			}
			
			if(_complete){
				_complete.apply(this, arguments);
			}
		}
	}

	var ajaxSettings = $.extend(aConfig, localAjaxSettings);
	return ajaxSettings;
};

$.subscribe("common.topics.portletContentLoaded",function(oEvent, data){
	//console.log("common.topics.portletContentLoaded");
	var i ;
	var portlets = ns.common.usersPortlets;
	for (i = 0; i < portlets.length; i++) {
		if(portlets[i].portletId === data.portletId){
			portlets[i].loaded = true;	
		}		
	};

	//check if all portlets are loaded if done stop spinning wheel
	var bLoadComplete = true;
	for (i = 0; i < portlets.length; i++) {
		if(portlets[i].loaded != true){
			bLoadComplete = false;
		}
	};

	if(bLoadComplete){
		//$('#desktopContainer').hideLoading();
		/*
		$('#desktopContainer').hideLoading(
			{
				'indicatorID' : 'home-portlets'
			}
		);
		*/
	}
});

$.subscribe("ns-common-localAjaxBeforeTopic", function(oEvent, oData){
	$.ajaxSetup({global: false});
});

$.subscribe("ns-common-localAjaxCompleteTopic", function(oEvent, oData){
	$.ajaxSetup({global: true});
});

/**
* This function extends the timeout period for below scenarios
* -Ajax Request is pending and timeout has arrived.
*/
ns.common.extendTimeout = function(){
	if(ns.common.sessionIntervalId){
		clearInterval(ns.common.sessionIntervalId);
		ns.common.sessionIntervalId = setInterval("ns.common.timerExpired()",ns.common.timeoutMS);
	}

	//clear timer to show the block loading indicator
	clearTimeout(ns.common.LoadingTimer);
	ns.common.hideLoading();
};

/**
* This function checks if there are active ajax requests which are ongoing
*/
ns.common.isAjaxRequestActive = function(){
	var bActiveAjaxRequest = false,
		aSystemAjaxRequests = [
			"/cb/pages/jsp/personalization/getBankingEvents.action"
		];

	if($.active != 0){
		//if current pending call is from system ajax calls like getBankingEvents, 
		//treat it as inactive request
		if($.active == 1 && ns.common.submittedUrls[aSystemAjaxRequests[0]] != null){
			bActiveAjaxRequest = false;
		}
		bActiveAjaxRequest = true;
	}else{
		bActiveAjaxRequest = false;
	}
	return bActiveAjaxRequest;
};