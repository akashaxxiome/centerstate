var isPortalModified = false;

function promptSavePortal() {
	if(isPortalModified) {
		var answer = confirm("The portal page has been modified, do you wish to continue and discard the changes?");
		return answer;
	}
	return true;
}

$.subscribe('onBeforeSignOut', function(event){
	if(!promptSavePortal()) {
		event.originalEvent.preventDefault();
	}
});