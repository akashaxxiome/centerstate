ns.approval.customApprovalsGroupActionLinks = function(gridID)
{
	var rn, edit, del;
	
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	
	for(var i=0;i<ids.length;i++){
		var rowId = ids[i];
		rn = $(gridID).jqGrid('getCell', ids[i], 'rn');
		var _editURL = $(gridID).jqGrid('getCell', rowId, 'map.EditURL');
		var _deleteURL = $(gridID).jqGrid('getCell', rowId, 'map.DeleteURL');
		edit = "<a title='" +js_edit+ "' href='#' onClick=\"ns.approval.editApprovalsGroup('"+ _editURL +"');\"><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		del  = "<a title='" +js_delete+ "' href='#' onClick=\"ns.approval.deleteApprovalsGroup('"+ _deleteURL +"');\"><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";
		$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_approvalsGroupID']").eq(i).html(edit + ' ' + del);
	}
};

ns.approval.editApprovalsGroup = function( urlString ){
	//var	urlString ="/cb/pages/jsp/approvals/corpadminapprovalgroupedit.jsp?itemIndex=" + groupIndex;

	$.ajax({
		url: urlString,
		success: function(data){
		//	$('#ui-dialog-title-approvalGroupDialogID').html( js_approvals_EditGroupTitle );
			$('#editApprovalGroupDialogID').html(data).dialog('open');
		}
	});
};

ns.approval.deleteApprovalsGroup = function(urlString)
{
	//var	urlString = "/cb/pages/jsp/approvals/corpadminapprovalgroupdelete.jsp?itemIndex=" + groupIndex;
	
	$.ajax({
		url: urlString,
		success: function(data){
			//$('#ui-dialog-title-approvalGroupDialogID').html(js_approvals_DeleteGroupTitle);
			$('#deleteApprovalGroupDialogID').html(data).dialog('open');
		}
	});
};

ns.approval.reloadApprovalGroupsGrid = function()
{
//	alert("call ns.approval.reloadApprovalGroupsGrid()");
	$('#approvalGroupsGrid').trigger("reloadGrid");
};

$.subscribe('approvePaymentsVerifyGridComplete', function(event,data) {
	ns.common.resizeWidthOfGrids();
});

/**
 * Cancel button Click in the pending-verify page
 */
$.subscribe('CancelApprovePaymentsVerify', function(event,data) {
	$("#approvalGroupSummary").show();
	$("#targetApprovePaymentsVerifyContainer").hide();
	
});


/**
 * Submit the pending-verify form 
 */
$.subscribe('submitApprovePaymentsVerifyFormBefore', function(event,data) {
	$('.includeApprovalDialog').each(function(){
		$(this).remove();
	});
});

$.subscribe('submitApprovePaymentsVerifyFormComplete', function(event,data) {
	//$("#approvalGroupSummary").html(data);
});

$.subscribe('submitApprovePaymentsVerifyFormSuccess', function(event,data) {
	$("#targetApprovePaymentsVerifyContainer").hide();
	$("#approvalGroupSummary").show();
});

$.subscribe('approvalGroupsGridCompleteEvents', function(event,data) {
	ns.approval.customApprovalsGroupActionLinks("#approvalGroupsGrid");
	ns.common.resizeWidthOfGrids();
});

$.subscribe('cancelApprovalGroupComplete', function(event,data) {  
	ns.common.closeDialog('#deleteApprovalGroupDialogID');
});

//After the group is deleted, the grid summary should be reloaded.
$.subscribe('cancelApprovalGroupSuccessTopics', function(event,data) {
	var statusMessage = $('#resultmessage').html();
    ns.common.showStatus(statusMessage);
	ns.approval.reloadApprovalGroupsGrid();
});

$.subscribe('errorDeleteApprovalGroup', function(event,data){
	$.log('errorDeleteTransfer');
	ns.common.closeDialog('#deleteApprovalGroupDialogID');
});

/**
 * the approvepayments-error.jsp' OK button is clicked 
 */

$.subscribe('ApprovePamentsErrorOKClickBefore', function(event,data) {
	
	$('.includeApprovalDialog').each(function(){
		$(this).remove();
	});
});
/*
 * for edit group name
 */
$.subscribe('editApprovalGroupComplete', function(event,data) {  
	ns.common.closeDialog('#editApprovalGroupDialogID');
});

//After the group is deleted, the grid summary should be reloaded.
$.subscribe('editApprovalGroupSuccessTopics', function(event,data) {
	var statusMessage = $('#resultmessage').html();
    ns.common.showStatus(statusMessage);
    ns.common.showSummaryOnSuccess("summary","done");
});
$.subscribe('editApprovalGroupError', function(event,data){
	$.log('errorDeleteTransfer');
});
