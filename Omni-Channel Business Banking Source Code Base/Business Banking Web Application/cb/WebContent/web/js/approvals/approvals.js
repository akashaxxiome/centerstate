/**
 * Close Details island and expand Summary island
 */
ns.approval.closeDetails = function(){
	//$('#approvalTabs').portlet('expand');
	$("#approvalWorkflowSummary").show();
    $('#details').slideUp();
};


ns.approval.getLevelsByOpType = function( str, opType )
{
	//str for CB: /cb/pages/jsp/approvals/corpadmincoapwf.jsp, not used for now
	//window.location=str.concat( "?OpType=", comboBox.options[comboBox.selectedIndex].value );
	var urlString = "/cb/pages/jsp/common/include-defined-workflows.jsp";
	//urlString = urlString.concat(" ?OpType=", opType);
	$.ajax({
		url: urlString,
		type: "post",
		data: {CSRF_TOKEN: ns.approval.csrf_tokenStr, OpType: opType},
		success: function(data){
			$('#WorkflowLevelsDiv').html(data);
			$.publish("onLoadDefinedWorkflowTopic");
			$.publish("common.topics.tabifyNotes");
			if(accessibility){
				$("#details").setInitialFocus();
			}
		}
	});
};


ns.approval.getLevelsByOpTypeAccId = function( str, opType,accID )
{
	//str for CB: /cb/pages/jsp/approvals/corpadmincoapwf.jsp, not used for now
	//window.location=str.concat( "?OpType=", comboBox.options[comboBox.selectedIndex].value );
	var urlString = "/cb/pages/jsp/common/include-defined-workflows.jsp";
	//urlString = urlString.concat(" ?OpType=", opType);
	$.ajax({
		url: urlString,
		type: "post",
		data: {CSRF_TOKEN: ns.approval.csrf_tokenStr, OpType: opType, AccountID: accID},
		success: function(data){
			$('#WorkflowLevelsDiv').html(data);
			$.publish("onLoadDefinedWorkflowTopic");
			$.publish("common.topics.tabifyNotes");
			if(accessibility){
				$("#details").setInitialFocus();
			}
		}
	});
};

ns.approval.getLevelsByOpTypeAndAccountId = function()
{

	var opType= $("#OpType option:selected").val();
	var excludeAccountID=false;

	for(i in excludeAccIdOperation)
	{
		if(excludeAccIdOperation[i]==opType)
		{
			excludeAccountID=true;
			break;
		}
	}

	var accountID="";
	if(excludeAccountID)
	{
	$("#accountIDDiv").hide();
	$('#AccountIDHidden').val("All Accounts");
	}
	else
	{

	$("#accountIDDiv").show();
		accountID= $("#accountID option:selected").val();
		if(accountID=="-1")
		{
				$('#AccountIDHidden').val("All Accounts");
		}
		else
		{
			$('#AccountIDHidden').val(accountID);
		}
	}


	//str for CB: /cb/pages/jsp/approvals/corpadmincoapwf.jsp, not used for now
	//window.location=str.concat( "?OpType=", comboBox.options[comboBox.selectedIndex].value );
	var urlString = "/cb/pages/jsp/common/include-defined-workflows.jsp";

	//urlString = urlString.concat(" ?OpType=", opType);
	$.ajax({
		url: urlString,
		type: "post",
		data: {CSRF_TOKEN: ns.approval.csrf_tokenStr, OpType: opType, AccountID: accountID},
		success: function(data){
			$('#WorkflowLevelsDiv').html(data);
			$.publish("onLoadDefinedWorkflowTopic");
			$.publish("common.topics.tabifyNotes");
			if(accessibility){
				$("#details").setInitialFocus();
			}
		}
	});
	$("#accountID").lookupbox("option","source","/cb/pages/common/PermissionAccountsLookupBoxAction.action?AppOpType="+opType)
};




ns.approval.addWorkflow = function( encURL ) {
	$.ajax({
		url: encURL,
		success: function(data){
			$('.includeApprovalDialog').each(function(){
				$(this).remove();
			});
			$('#detailsDiv').html(data);
			$('#details').slideDown();
			//$('#approvalTabs').portlet('fold');
			$('#approvalWorkflowSummary').hide();

			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#details").setInitialFocus();
			}
		}
	});
};


ns.approval.deleteWorkflow = function( encURL ){
	$.ajax({
		url: encURL,
		success: function(data){
			$('#deleteApprovalLevelDialogID').html(data).dialog('open');
		}
	});

};


ns.approval.addLevel = function( encURL ) {
	$.ajax({
		url: encURL,
		success: function(data){
			$('.includeApprovalDialog').each(function(){
				$(this).remove();
			});
			$('#detailsDiv').html(data);
			$('#details').slideDown();
			//$('#approvalTabs').portlet('fold');
			$('#approvalWorkflowSummary').hide();

			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#details").setInitialFocus();
			}
		}
	});
};




ns.approval.editLevel = function( encURL ) {
	$.ajax({
		url: encURL,
		success: function(data){
			$('.includeApprovalDialog').each(function(){
				$(this).remove();
			});
			$('#detailsDiv').html(data);
			$('#details').slideDown();
			//$('#approvalTabs').portlet('fold');
			$('#approvalWorkflowSummary').hide();

			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#details").setInitialFocus();
			}
		}
	});
};

ns.approval.deleteLevel = function( encURL ){
	$.ajax({
		url: encURL,
		success: function(data){
			$('#deleteApprovalLevelDialogID').html(data).dialog('open');
		}
	});

};

ns.approval.setup = function(){

	$('#approvalTabs').find('ul').addClass('portlet-header');


	/*
	 *  Load add/edit level form
	 */
	$.subscribe('beforeLoadApprovalsForm', function(event,data) {
		$.log('About to load form! ');
        $('#details').slideUp();

		$('.includeApprovalDialog').each(function(){
			$(this).remove();
		});
	});

	$.subscribe('loadApprovalsFormComplete', function(event,data) {
		$.log('Form Loaded! ');
		//$('#approvalTabs').portlet('fold');
		$("#approvalWorkflowSummary").hide();


		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		$('#details').slideDown();

		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#details").setInitialFocus();
		}

	});


	$.subscribe('onLoadDefinedWorkflowTopic', function(event,data) {
		var approvalLevelDefined = $("#approvalLevelDefined").val();
		if(approvalLevelDefined!= undefined && approvalLevelDefined != '' && approvalLevelDefined == 'false'){
			$("#tableHeader").hide();
			$("#noDataHeader").show();
		}else{
			$("#noDataHeader").hide();
			$("#tableHeader").show();
		}
	});


	$.subscribe('errorLoadApprovalsForm', function(event,data) {
		$.log('Form Loading error! ');

	});


	/*
	 * Submit add/edit level form
	 */
	$.subscribe('beforeSubmitWorkflowForm', function(event,data) {
		$.log('About to verify form! ');
		//removeValidationErrors();

	});

	$.subscribe('submitWorkflowFormComplete', function(event,data) {
		$.log('Form Verified! ');

		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#details").setInitialFocus();
		}
	});

	$.subscribe('errorSubmitWorkflowForm', function(event,data) {
		$.log('Form Verifying error! ');

		var resStatus = event.originalEvent.request.status;
		if(resStatus === 700) //700 indicates interval validation error
			$('#stallConditionDialogID').dialog('open');
	});

	$.subscribe('successSubmitWorkflowForm', function(event,data) {
		$.log('Form Verifying success! ');

		if( ns.approval.isChainItemListEmpty ) {
			$("#AlertID .alert-text").html(js_approvals_EmptyChainAlert);
			$("#AlertID").dialog('open');
			return;
		}
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);
		ns.approval.getLevelsByOpTypeAndAccountId();
		ns.approval.closeDetails();

	});


	/*
	 * Delete Workflow Level
	 */
	$.subscribe('beforeDeleteApprovalWFLevel', function(event,data) {
		$.log('About to delete level! ');

	});

	$.subscribe('deleteApprovalWFLevelComplete', function(event,data) {
		$.log('deleting level completes! ');

	});

	$.subscribe('deleteApprovalWFLevelSuccess', function(event,data) {
		$.log('deleting level succeeded! ');
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);
		var opType = "";
		if (document.forms.DeleteApprovalsLevelForm.OperationType != null) {
			opType = document.forms.DeleteApprovalsLevelForm.OperationType.value;
		}
		var useAccId=false;
		var accountID = "";
		if (document.forms.DeleteApprovalsLevelForm.approvalsLevelsAccountID != null) {
			accountID = document.forms.DeleteApprovalsLevelForm.approvalsLevelsAccountID.value;
			useAccId=true;
		}
		$.publish('closeDialog');
		if(useAccId)
		{
			ns.approval.getLevelsByOpTypeAccId('', opType,accountID);
		}else
		{
			ns.approval.getLevelsByOpType('', opType);
		}

	});

	$.subscribe('errorDeleteApprovalWFLevel', function(event,data) {
		$.log('deleting level error! ');

	});



	$.subscribe('cancelForm', function(event,data) {

		$.log('About to cancel Form! ');
		ns.approval.closeDetails();
		ns.common.hideStatus();


		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#details").setInitialFocus();
		}
	});

	/*
	 * For edit group
	 */
	$.subscribe('resetGroupEditForm', function(event, data) {
		removeValidationErrors();
		$("#GroupEdit input[name='groupName']").each(function(){
			this.value = $("#GroupEdit_OriginalName").html();
		});
	});

	/*
	 * For add group
	 */
	$.subscribe('loadAddGroupFormComplete', function(event, data){
		$('#ui-dialog-title-approvalGroupDialogID').html(js_approvals_AddGroupTitle);

		$("#approvalGroupDialogID").dialog('open');

		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#details").setInitialFocus();
		}
	});

	$.subscribe('errorLoadAddGroupForm', function(event, data){
		//$("#approvalGroupDialogID").dialog('open');
	});

	$.subscribe('resetAddGroupDialog', function(event, data){
		removeValidationErrors();
		$("#GroupNew input[name='groupName']").each(function(){
			this.value = "";
		});
	});

	$.subscribe('closeAddGroupDialog', function(event, data){
		ns.common.selectDashboardItem("approvalGroupSummaryBtn");
	});

	$.subscribe('addApprovalGroupComplete', function(event,data) {
		ns.common.closeDialog('#approvalGroupDialogID')
	});

	$.subscribe('addApprovalGroupErrorTopics', function(event,data) {
		ns.common.closeDialog('#approvalGroupDialogID');
	});

	//After the group is deleted, the grid summary should be reloaded.
	$.subscribe('addApprovalGroupSuccessTopics', function(event,data) {
		ns.common.showSummaryOnSuccess("summary","done");
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);
	});

};


$(document).ready(function(){
//	jQuery.LINT.level = 1;
	$.debug(false);
	ns.approval.setup();

});

//Show the active dashboard based on the user selected summary
ns.approval.showApprovalsDashboard = function(approvalUserActionType,approvalDashboardElementId){
	//Simulate dashboard item click event if any additional action is required
	if(approvalDashboardElementId && approvalDashboardElementId !='' && $('#'+approvalDashboardElementId).length > 0){
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+approvalDashboardElementId).trigger("click");}, 10);
	}
};


ns.approval.setAccountLook = function(){
	var opType= $("#OpType option:selected").val();
	defaultOption = {value:"All Accounts", label:"All Accounts"};
	$("#accountID").lookupbox({
		"controlType":"server",
		"collectionKey":"1",
		"module":"accountPermissions",
		"size":"45",
		"source":"/cb/pages/common/PermissionAccountsLookupBoxAction.action?AppOpType="+opType,
		"defaultOption":defaultOption
	});
	$("#accountID").change(accountSelect);
};

