//for CB only
ns.approval.addEntity = function(selectedEntities, entity, entityIndex) {
    var result = selectedEntities;

    if (result.length > 0) {
	result = result + "&";
    }

    result = result + "selectedEntity" + entityIndex + "=" + entity;
    return result;
}

ns.approval.isAmountNumeric = function(amtStr){
	amtStr = $.trim( amtStr );
	
	var p = amtStr.indexOf('.')>0 ? amtStr.indexOf('.') : amtStr.length;
	
	
	var value = amtStr.substr( 0, p ) ;
	value = value.replace(',', '');
	
	amtStr = value + ( amtStr.indexOf('.')>0 ? amtStr.substr( amtStr.indexOf('.'), amtStr.length ) : '' );
	
	if (  amtStr == null || !amtStr.match(/^[-]?\d*\.?\d*$/) )	
	{
		return false;	
	}
	
	return true;
};

ns.approval.isChainItemListEmpty = true;

ns.approval.validateSave = function( ApprovalsLevelForm, chainItemList )
{
	ns.approval.isChainItemListEmpty = true;
	if( chainItemList.length != 0 ) {
		ns.approval.isChainItemListEmpty = false;
	}

	// mark all items as selected
	for( i = 0 ; i < chainItemList.options.length ; i++ ){
    	    chainItemList.options[ i ].selected = "true";
    }

	//return false;
}


// remove items from the user and group lists that appear in the chain item list
ns.approval.removeItemsFromCurrentList = function( form )
{
	if( form != null ) {
		for( i = 0 ; i < form.chainItemList.length ; i++ ) {
			var tempItem = form.chainItemList.options[ i ].value;

			// delete the elements of the user list that are in the chain item list
			for( j = 0 ; j < form.userList.length ; j++ ) {
				if( form.userList.options[ j ].value == tempItem ) {
					form.userList.options[ j ] = null;
					break;
				}
			}
		}
	}
}
ns.approval.deselectAllOtherLists = function( form, currentList, isCB )
{
	if ( currentList.name != "groupList" ) {
		for ( i = 0; i < form.groupList.length; i++ ) {
			form.groupList.options[i].selected = false;
		}
	}
	if ( currentList.name != "userList" ) {
		for ( i = 0; i < form.userList.length; i++ ) {
			form.userList.options[i].selected = false;
		}
	}
	if( _isCB ) { 
		if ( currentList.name != "apprGroupList" ) {
			for ( i = 0; i < form.apprGroupList.length; i++ ) {
				form.apprGroupList.options[i].selected = false;
			}
		}
	} 
}

//for CB only
ns.approval.viewPermissions = function(form) {

    var selectedEntities = "";
    var currEntityIndex = 0;
    var currList;

    currList = $("#chainItemList_sms_list").find('.ui-icon-check');

    for (i = 0; i < currList.size(); i++) {
	    selectedEntities = ns.approval.addEntity(selectedEntities, currList.eq(i).parent().parent().data('optionLink').val(), currEntityIndex++);
    }

    currList = $("#userList_sms_list").find('.ui-icon-check');
    
    for (i = 0; i < currList.size(); i++) {
	    selectedEntities = ns.approval.addEntity(selectedEntities, currList.eq(i).parent().parent().data('optionLink').val(), currEntityIndex++);
    }

    currList = $("#groupList_sms_list").find('.ui-icon-check');

    for (i = 0; i < currList.size(); i++) {
	    selectedEntities = ns.approval.addEntity(selectedEntities, currList.eq(i).parent().parent().data('optionLink').val(), currEntityIndex++);
    }

    currList = $("#apprGroupList_sms_list").find('.ui-icon-check');

    for (i = 0; i < currList.size(); i++) {
	    selectedEntities = ns.approval.addEntity(selectedEntities, currList.eq(i).parent().parent().data('optionLink').val(), currEntityIndex++);
    }

    if (selectedEntities == "") {
    	$("#noneSelectedAlertID").dialog('open');
    } else {
    	var OpType = form.approvalsLevelsOperationType.value;
    	var urlString = "/cb/pages/jsp/approvals/corpadmincoapwfviewpermissions.jsp";
		var csrf_tokenStr = $("#ApprovalsLevelForm").find("input[name='CSRF_TOKEN']").val();
    	$.ajax({
    		url: urlString,
			data: "CSRF_TOKEN=" + csrf_tokenStr + "&OpType="+OpType+"&"+selectedEntities,
			type: "post",
    		success: function(data){
    			$('#viewPermissionDialogID').html(data).dialog('open');
    			$('#viewPermissionDialogID').attr('style','max-height: 650px');
    		}
    	});
    	
    }
};


