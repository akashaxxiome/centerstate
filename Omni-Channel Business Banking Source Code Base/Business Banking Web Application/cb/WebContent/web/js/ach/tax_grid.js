	$.subscribe('taxTypeGridCompleteEvents', function(event,data) {
		ns.common.resizeWidthOfGrids();

        if (ns.tax.statusMessage != null)
        {
            ns.common.showStatus(ns.tax.statusMessage);
            ns.tax.statusMessage = null;
        }

	});
	
	$.subscribe('cancelTaxTypeSuccessTopics', function(event,data) {
		$('#taxFederalTypesGridId').trigger("reloadGrid"); 
		$('#taxStateTypesGridId').trigger("reloadGrid");
		if(data.innerHTML != "") {
			ns.common.showStatus();
		} else {
			ns.common.showStatus($("#deleteTypeMessageId").val());
		}
	});
	
	$.subscribe('cancelTaxTypeCompleteTopics', function(event,data) {
		ns.common.closeDialog();
	});

	$.subscribe('cancelTaxTypeErrorTopics', function(event,data) {
		ns.common.showError();
	});

	ns.tax.handleGridEventsAndSetWidth = function (gridID,tabsID){
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
	};
	
	ns.tax.formatTaxTypeActionLinks = function (cellvalue, options, rowObject) {
		//var isBusiness = "false";
		//if(rowObject["map"].BusinessID != undefined) {
		//	isBusiness = "true";
		//}
		var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.tax.deleteTaxType(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		return del;
	};

	ns.tax.formatTaxStateTypeNameLinks = function (cellvalue, options, rowObject) {
	
		var name = rowObject["name"];
		//if( rowObject["state"] != undefined){
			//name = rowObject["state"] + " " + name;
		//}
		if(rowObject["map"].BusinessID != undefined) {
			name = name + "(Business)";
		}
		return name;
	}
	
	ns.tax.deleteTaxType = function ( urlString ){
		
		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteTaxTypeDialogID').html(data).dialog('open');
			}
		});
	};
	
