	   //style for select boxes
    	$("#SIAccountID").selectmenu({width: 300});
    	$("#SITransType").selectmenu({width: 150});
    	
    	
    	//Default dates
		var fromDate = $("#StartDateSessionValue").val();
		var toDate = $("#EndDateSessionValue").val();
		$("#SIPostingDateFrom").val(fromDate);
		$("#SIPostingDateTo").val(toDate);	
    	
    ns.imagesearch.setup = function(){
    	//console.info("setting up image search");
    	ns.imagesearch.initView()
    	ns.imagesearch.registerEvents();
		var config = ns.common.dialogs["checkImage"];
		config.autoOpen = "false";
		ns.common.openDialog("checkImage");	
    }
    
    ns.imagesearch.initView = function(){
    	//console.info("init view");    	
    	ns.imagesearch.showOffsetSummary(false);
    	//ns.common.refreshDashboard('dbAccountSummary');
    	
    	//ns.imagesearch.initDashboard();
    }
    
    ns.imagesearch.initDashboard = function(){
    	//console.info("initDashboard");
    	//$("#imageSeachBox").hide();
    	//$("#pendingImagesControlBox").hide();
    	$("#pendingImagesSummary").hide();
    	//$("#viewCheckedLink").hide();
		
    	//style for select boxes
    	//$("#SIAccountID").selectmenu({width: 300});
    	//$("#SITransType").selectmenu({width: 150});
    }
    
    ns.imagesearch.showImageSearchSummary = function(){
    	//alert('hi')
    	$('#imageSummaryContent').show();
    	$('#pendingImagesSummary').hide();
    }
    
    ns.imagesearch.initializeGridView = function(){
    	ns.imagesearch.addGridButton("#accountImagesSummaryID",{ 
    		caption: js_view,
    		title:js_view_all_check_images,
    		buttonicon: 'ui-icon-image',
    		onClickButton: function(e){
    			ns.imagesearch.viewCheckedImages();
    		}}
    	);
    	ns.imagesearch.addGridButton("#accountOffsetSummaryID",{ 
    		caption: 'View',
    		title:js_view_all_check_images,
    		buttonicon: 'ui-icon-image',
    		onClickButton: function(e){
    			ns.imagesearch.viewCheckedImages();
    		}}
    	);    
    	
    	ns.imagesearch.gridViewInitialized = true;
    }
    
    ns.imagesearch.addGridButton = function(gridID,buttonConfig){
    	$(gridID).jqGrid('navButtonAdd', gridID + "_pager", buttonConfig);
    }
    
    
    
    ns.imagesearch.doSearch = function(){	
		//console.info("doing image search..!");
		$("#imageSeachBox").slideUp();		
		var formURL = $("[name=formImageSearch]").attr("action");		
		var formData = $("[name=formImageSearch]").serialize();
    	$.ajax({
		  type: 'POST',
		  url: formURL,
		  data:formData,
		  success: function(data){
			//console.info("successful search.. reloading grid");
			$("#accountImagesSummaryID").trigger("reloadGrid");
			//$("#viewCheckedLink").show();
			//$("#summary").show("slow");
		  }
		});
    }
    
	ns.imagesearch.reloadImagesSummaryGrid = function(data){
		//console.info("reloading images after form submission:accountImagesSummaryID");			
		$("#accountImagesSummaryID").jqGrid("clearGridData"); //clearing grid data
		var imagesGrid = $("#accountImagesSummaryID")[0];
		imagesGrid.addJSONData(data);
	}
	
	
	ns.imagesearch.resetOffSetGridUrl = function(offsetUrl){
		//console.info("--resetOffSetGridUrl---;")
		$("#accountOffsetSummaryID").jqGrid('setGridParam',{url:offsetUrl});
	}
	
	ns.imagesearch.reloadOffsetGrid = function(url){
		//console.info("loading offset images..." + url);
		ns.imagesearch.resetOffSetGridUrl(url);
		$("#accountOffsetSummaryID").trigger("reloadGrid");
	}	
	
	ns.imagesearch.showImagesGrid = function(show){
    	//console.info("showImagesGrid");
    	if(show== true){
    		$("#summary").show("slow");
    	}else{
    		$("#summary").hide("slow");
    	}    	
    }    	
	
    ns.imagesearch.showOffsetSummary = function(show){
    	 //console.info("showOffsetSummary" + $("#offsetSummary:hidden"));
    	if(show== true){
    		$("#offsetSummary").show();
    	}else{
    		
    		$("#offsetSummary").hide();
    	}    	
    }
    

    ns.imagesearch.disableOffsetCheckBoxes = function(){
    	//console.info("ns.imagesearch.disableOffsetCheckBoxes");
    	if(ns.imagesearch.offsetRecords!=null){
	    	for(var i=0;i<ns.imagesearch.offsetRecords.length;i++){
	    		$("#accountImagesSummaryID input[id=jqg_" + ns.imagesearch.offsetRecords[i] + "]").attr("disabled",true);
	    	}
	    	ns.imagesearch.offsetRecords = null;    		
    	}
    	ns.imagesearch.offsetRecords = [];
    }

    //************************ Column Formatters **********************************//
	ns.imagesearch.viewImageLinkFormatter = function(cellvalue, options, rowObject) { 
		//console.info("Applying action formatting-" +  options.rowId);
		var url;
		var view;
		var gridName = "IMAGES";
		if (rowObject.drCr == "Debit"){
			url = rowObject.map.ViewURL;
			view = "<a title='" +js_view+ "' href='#' onclick='ns.imagesearch.beforeViewCheckedImage(\"" + rowObject.imageHandle + "\", \"" + gridName + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		}else{
			url = rowObject.map.ViewOffsetURL;
			view = "<a title='" +js_view+ "' href='#' onClick='ns.imagesearch.viewImageDetails(\""+ url +"\",\""+ rowObject.drCr +"\",\"" + gridName + "\",\""+ options.rowId +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		} 
		
		if (rowObject.drCr == "Debit"){
			//alert(rowObject.drCr);
			var currentRowId = options.rowId;
			
			//alert($("#gview_accountImagesSummaryID").find("#jqg_accountImagesSummaryID_"+currentRowId).val);
			$("#gview_accountImagesSummaryID").find("#jqg_accountImagesSummaryID_"+currentRowId).attr("disabled",true);
			$("#gview_accountImagesSummaryID").find("#jqg_accountImagesSummaryID_"+currentRowId).attr("disabled",true);
		}
		
		
		
		return ' ' + view + ' ';
	}


	ns.imagesearch.debitCreditFormatter = function(cellvalue, options, rowObject) {
		return (rowObject.drCr == "Debit")? "Check": "Deposit";
	}
	
	ns.imagesearch.checkNumberFormatter = function(cellvalue, options, rowObject) {		
		var result = (rowObject.drCr == "Debit")? rowObject.checkNumber: "Offset";
		if(result == "Offset"){
			ns.imagesearch.offsetRecords.push(options.rowId);
		}		
		return result;		
	}
	
	ns.imagesearch.offsetImageGridLinkFormatter = function(cellvalue, options, rowObject) { 
		var url;
		if (rowObject.drCr == "Check"){
			url = rowObject.map.ViewURL;
		}else{
			url = rowObject.map.ViewOffsetURL;
		} 
		
		var gridName = "OFFSET_IMAGES";
		var module = "OFFSET";
		
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.imagesearch.beforeViewCheckedImage(\""+ rowObject.imageHandle +"\",\"" + module + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		return ' ' + view + ' ';
	}
    //*****************************************************************************//
	
	
	ns.imagesearch.viewImageDetails = function(url,type,gridName,id){		
		if(type== "Debit"){
			if(gridName == "OFFSET_IMAGES"){
				ns.imagesearch.showOffsetSummary(true);
				//ns.imagesearch.showImagesGrid(false);
				
			}else{
				ns.imagesearch.showOffsetSummary(false);
				//ns.imagesearch.showImagesGrid(true);
			}			 
			 //ns.imagesearch.viewCheckImage(url);
			ns.imagesearch.activeGrid == "IMAGES";
			ns.imagesearch.beforeViewImage(id);
		}else{
			// $("#imageSeachBox").slideUp();
			 $("#imageSearchHolder").slideUp();
			// $("#goBackToImgSrcBtnDiv").show();
			 $("#imageSearchButton").show();
			 $("#pendingImagesButton").show();
			 $("#pendingImagesControlBox").hide();
			 ns.imagesearch.hideShowViewCheckButton();
			 $("#pendingImagesSummary").hide();
			 ns.imagesearch.showOffsetSummary(true);
			 ns.imagesearch.reloadOffsetGrid(url);
			 
			
		}
	}
	
	ns.imagesearch.viewOffsetImageDetails = function(url){
		//console.info("viewOffsetImageDetails");
		ns.imagesearch.showOffsetSummary(true);
		ns.imagesearch.viewCheckImage(url);
	}
	
	
	
	ns.imagesearch.viewCheckImage = function( urlString ){	
		//console.info("check image url :" + urlString);	
		$.ajax({
			url: urlString,
			success: function(data){
				$('#checkImageDialogID').html(data).dialog('open');
			}
		});
	}
	
	ns.imagesearch.beforeViewCheckedImage = function(imageId,module){
		//console.info("ns.imagesearch.beforeViewCheckedImage-" + module);
		var userData = {"userSelections":imageId,"module":module};
		ns.imagesearch.viewCheckedImages(userData);			
		if(module == "IMAGES"){
			ns.imagesearch.resetGridSelection("accountOffsetSummaryID");
		}else{
			ns.imagesearch.resetGridSelection("accountImagesSummaryID");
		}
	}
	
	ns.imagesearch.beforeViewCheckedImages = function(imageId){
		var gridId;
		if(ns.imagesearch.activeGrid == "IMAGES"){
			gridId = "accountImagesSummaryID";
		}else{
			gridId = "accountOffsetSummaryID";
		}
		var userSelections = ns.imagesearch.getSelectedValues(gridId,"imageHandle");
		if(userSelections){
			var module = ns.imagesearch.activeGrid;
			var userData = {"userSelections":userSelections,"module":module};
			ns.imagesearch.viewCheckedImages(userData);			
		}else{
			var message = "Please select images to be viewed";
			ns.common.showStatus(message);
		}	
	}
	
	
	ns.imagesearch.beforeViewImage = function(imageId){
		//console.info("ns.imagesearch.beforeViewImage-" + imageId);
    	var checkBoxSelector = "";   
    	if(ns.imagesearch.activeGrid == "IMAGES"){
    		$("#accountImagesSummaryID [type=checkbox]").attr("checked",false);
    		checkBoxSelector = $("#accountImagesSummaryID [type=checkbox][id='jqg_" +  imageId + "']");
    	}else{
    		$("#accountOffsetSummaryID [type=checkbox]").attr("checked",false);
    		checkBoxSelector = $("#accountOffsetSummaryID [type=checkbox][id='jqg_" +  imageId + "']");
    	}

    	$(checkBoxSelector).attr("checked",true);
    	ns.imagesearch.viewCheckedImages();
    }
	
	/*
	 * This function is used for viewing single and multiple images checked
	 */
	ns.imagesearch.viewCheckedImages = function(userData){	
		if(userData){			
			var  URL = "/cb/pages/jsp/account/GetImagesAction_viewCheckedImages.action";
			$.ajax({
				url: URL,
				type: 'GET',
				data:userData,
				success: function(response){
					if(response){
						$('#checkImageDialogID').html(response).dialog('open');
						ns.imagesearch.initCarousel();							
					}
				}
			});			
					
		}else{
			//console.info("Error getting user selections");
		}

	}

	ns.imagesearch.hideShowViewCheckButton = function(){	
		//console.info("ns.imagesearch.hideShowViewCheckButton");
		var archivedImages = $("#accountImagesSummaryID").jqGrid('getDataIDs');
			if(archivedImages.length > 0){
			// $("#viewCheckedLink").show();
			 //$("#viewCheckedLink").removeAttr("style");
		}
	}

	ns.imagesearch.initCarousel = function(){
		//console.info('initializing carousel');
		var imgCount = $(".carouselClass ul li").size();
		if(imgCount){
			if(imgCount>1){
				$(".carouselClass").jCarouselLite({
					btnNext: "#nextImage",
					btnPrev: "#prevImage",
					visible: 1,
					speed:1000,
					circular:false,
					afterEnd: function(li) {
						ns.checkimage.visibleImageListItem = li;
					}
				});						
			}			
		}
	}
	
	ns.imagesearch.resetGridSelection = function(gridId){
		if($("#" + gridId).length>0){
			$("#" + gridId).jqGrid('resetSelection');
		}		
		
	}

	ns.imagesearch.getSelectedValues = function(gridId,columnName){		
		//console.info("ns.imagesearch.getSelectedValues");
		var rows = $('#' + gridId).jqGrid('getGridParam', 'selarrrow');
		var userSelections;
		$.each(rows,function(i,row){
			var rowData = $("#" + gridId).jqGrid('getRowData',row);
			if(rowData.checkNumber != "Offset"){ //exclude offset records from  the user selections
				userSelections = (userSelections)? (userSelections + "," + rowData[columnName]):rowData[columnName];
			}			
		})
		return userSelections;
	}
	
	
	ns.imagesearch.collectUserCheckedIds = function(){
		var selectedImages = $("#accountOffsetSummaryID [type=checkbox]:checked");
		var userSelections = null;
		$.each(selectedImages,function(i,entry){
                    var elementId = $(entry).attr("id");
                    var id = elementId .split("_");
                    userSelections = (userSelections)? (userSelections+ "," + id[1]):id[1];
		})
		return userSelections;
	}
	
	ns.imagesearch.zoomImage = function(urlString){
		$.ajax({    
			url: urlString,     
			success: function(data) {  
			ns.common.closeDialog();
			$("#checkImageDialogID").html(data).dialog('open');
			}   
		});   
	}
	
	ns.imagesearch.rotationFunction = function(urlString){
		$.ajax({    
			url: urlString,     
			success: function(data) {  
			ns.common.closeDialog();
			$("#checkImageDialogID").html(data).dialog('open');
			}   
		});   
	}
	
	ns.imagesearch.registerEvents = function(){		
		var imageSearchButtonClickHandler = function(e){
			$("#imageSeachBox").slideDown();
			$("#summary").show();
			$("#pendingImagesControlBox").hide();
			//$("#viewCheckedLink").hide();	
			$("#offsetSummary").hide();
			$("#pendingImagesSummary").hide();
			$("#imageSearchButton").hide();
			$("#pendingImagesButton").hide();
			$.publish("imagesearch.clearQuickSearchCriteria");
			$("#SIAccountID-menu").focus();
		}

		// Clear fields once user clicks on Quick search button
		$.subscribe('imagesearch.clearQuickSearchCriteria', function(event,data) {
			removeValidationErrors();
			$("#SIAccountID").selectmenu('value',"");
			$("#amountFrom").val("");
			$("#amountTo").val("");
			$("#checkNumberFrom").val("");
			$("#checkNumberTo").val("");
			$("#SITransType").selectmenu('value',"");

			//Default dates
			var fromDate = $("#StartDateSessionValue").val() || "";
			var toDate = $("#EndDateSessionValue").val() || "" ;
			$("#SIPostingDateFrom").val(fromDate);
			$("#SIPostingDateTo").val(toDate);	

			//clear result,error messages if it exists
			$("#operationresult").hide();			
		});

		$("#imageSearchButton").click(imageSearchButtonClickHandler);
		
		var pendingImagesClickHandler = function(){
			var buttonLabel = $("#pendingImagesButton span")[1];
			if($("#pendingImagesButton").hasClass("selected")){
				$(this).removeClass("selected");
				$(buttonLabel).html(js_image_search)
				$.publish("loadPendingImages");
				
				//$("#pendingImagesControlBox").show();
				//$("#viewCheckedLink").hide();
				$("#imageSeachBox").hide();
				$("#imageSearchButton").hide();				
				$("#pendingImagesSummary").show();				
				$("#summary").hide();
				$("#offsetSummary").hide();				
			}else{		
				$(this).addClass("selected");				
				 $(buttonLabel).html(js_requested_images);
				 $("#imageSearchButton").show();				
				 ns.imagesearch.hideShowViewCheckButton();				
				 $("#summary").show();				 
				 $("#pendingImagesControlBox").hide();
				 $("#pendingImagesSummary").hide();
				 ns.imagesearch.resetGridSelection("accountImagesSummaryID");
				 $("#accountImagesSummaryID").trigger("reloadGrid");
			}
		}
		$("#pendingImagesButton").click(pendingImagesClickHandler);

		var viewPendingCheckedImagesHandler = function(e){
			ns.previouslyrequested.beforeViewCheckedImages();
		}
				
		var viewCheckedImagesHandler = function(e){
			//ns.imagesearch.viewCheckedImages();
			ns.imagesearch.beforeViewCheckedImages();
		}
		$("#viewCheckedLink").click(viewCheckedImagesHandler);
		$("#viewPendingImages").click(viewPendingCheckedImagesHandler);		
		
		var cancelSearchHandler = function(e){
			$("#imageSeachBox").slideUp();
			//$("#imageSearchButton").show();
			//$("#pendingImagesButton").show();	
			ns.imagesearch.hideShowViewCheckButton();
		}
		$("#cancelSearch").click(cancelSearchHandler);		
	}
	
	$.subscribe('imagesGridOnComplete', function(event,data) {		
		var imagesCheckClickHandler = function(){
			ns.imagesearch.activeGrid= "IMAGES";
			ns.imagesearch.resetGridSelection("accountOffsetSummaryID");	
			ns.common.checkUnCheckGridCheckAllBox("accountImagesSummaryID"); 					
		}
		$("#gview_accountImagesSummaryID input[type=checkbox]").click(imagesCheckClickHandler);
		
		var hasRows = false;
		if($("#accountImagesSummaryID").jqGrid('getDataIDs').length>0){
			//$("#viewCheckedLink").show();
			//$("#viewCheckedLink").removeAttr("style");
			hasRows = true;
		}else{
			//$("#viewCheckedLink").hide();
		}	
		ns.imagesearch.disableOffsetCheckBoxes();

		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			accessibility.setFocusOnDashboard();
		}

	});

	
	$.subscribe('onSelectRow', function(event,data) {	
		ns.common.disableOffsetCheckBox(event.originalEvent.id);
	});
	$.subscribe('onSelectAllRows', function(event,data) {	
		//event.originalEvent.ids, event.originalEvent.status, event.originalEvent.grid	
		var rowIds = event.originalEvent.ids;
		if(event.originalEvent.status){
			if(rowIds){
				for(var i=0;i<rowIds.length;i++){
					ns.common.disableOffsetCheckBox(rowIds[i]);
				}
			}				
		}	
	});

	ns.common.disableOffsetCheckBox = function(rowId){		
		var rowData = $("#accountImagesSummaryID").jqGrid('getRowData',rowId);
		if(rowData.checkNumber == "Offset"){ 
			$("#accountImagesSummaryID input[id=jqg_" + rowId + "]").attr("checked",false);			
		}	
	}

	$.subscribe('offsetImagesGridOnComplete', function(event,data) {
		var offsetCheckClickHandler = function(){
			ns.imagesearch.activeGrid= "OFFSET";
			ns.imagesearch.resetGridSelection("accountImagesSummaryID");
			ns.common.checkUnCheckGridCheckAllBox("accountOffsetSummaryID"); 
		}		
		$("#gview_accountOffsetSummaryID input[type=checkbox]").click(offsetCheckClickHandler);	
		$.publish("common.topics.tabifyDesktop");
/*		if(accessibility){
			$("#summary").setInitialFocus();
		}	
*/	});

	$.subscribe('beforeImageSearch', function(event,data) {
		//console.info("beforeImageSearch");
		$('.errorLabel').html('').removeClass('errorLabel');
		$('#formerrors').html('');
		
		//adding the error class to the fields for layout issues
		$('.imgSrcErrorMsg').html('&nbsp');
	});

	$.subscribe('reloadArchiveImagesGrid', function(event,data) {
		//console.info("reloading archive images grid");
		$("#accountImagesSummaryID").trigger("reloadGrid");
		//$("#imageSeachBox").slideUp();
		$("#imageSearchButton").show();	
		$("#pendingImagesButton").show();
		$("#summary").show("slow");

	});
			
	$(document).ready(function() {
		ns.imagesearch.setup();
	});
	
	ns.imagesearch.gridViewInitialized = false;
	ns.imagesearch.activeGrid= "IMAGES";
	
	
	
	//ns.imagesearch.backToArchiveImagesGrid = function(){
	$.subscribe("backToArchiveImagesGrid",function(){
		backToArchiveImagesGridFunc();
	});
	
	function backToArchiveImagesGridFunc(){
			//alert('in back func');
			 //$("#goBackToImgSrcBtnDiv").hide();
			 $("#imageSearchHolder").slideDown();
			 ns.imagesearch.showOffsetSummary(false);
	}
	
	ns.imagesearch.showImageSearchDashboard = function(userActionType,dashboardElementId){
		if(userActionType == 'RequestedImageSearch'){
			ns.common.refreshDashboard ('dbPendingImagesSummary');
		}else{
			ns.common.refreshDashboard('dbAccountSummary');
		}
		
   		if(dashboardElementId && dashboardElementId !='' && $('#'+dashboardElementId).length > 0){
   			setTimeout(function(){$("#"+dashboardElementId).trigger("click");}, 10);
   		}
   };