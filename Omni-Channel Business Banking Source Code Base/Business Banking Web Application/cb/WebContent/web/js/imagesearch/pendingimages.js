
    ns.previouslyrequested.setup = function(){
    	//console.info("setting up pending images ");
    	ns.previouslyrequested.registerEvents();
    	$("#pendingImagesButton").addClass("selected");
		//load the check image dialog
		var config = ns.common.dialogs["checkImage"];
		config.autoOpen = "false";
		ns.common.openDialog("checkImage");
    }
	
    ns.previouslyrequested.loadPendingImagesGrid = function(){
		var pendingImagesURL = "/cb/pages/jsp/imagesearch/pending_images_grid.jsp";
    	$.ajax({
		  type: 'GET',
		  url: pendingImagesURL,
		  success: function(response){
			//console.info("received pending images");
			$('#pendingImagesSummary').show();
			$("#pendingImagesSummary").html(response);
			
			$('#imageSummaryContent').hide();
		  }
		});
    }    
    
    ns.previouslyrequested.initDeleteDialog = function(){
    	var cancelDeleteHandler = function(){
    		$("#deleteImageDialog").dialog('close');
    	}
    	$("#cancelDeleteImageBtn").click(cancelDeleteHandler);
    	
    	var deleteImageHandler = function(){
    		//console.info("deleting after confirmation..");    		
    		 ns.previouslyrequested.deleteImages();
    		 cancelDeleteHandler();
    	}
    	$("#deleteImageBtn").click(deleteImageHandler);
    }
    
    //does lazy load for delete dialog
    ns.previouslyrequested.loadDeleteImageDialog = function(){
		var deleteImageDiaURL = "/cb/pages/jsp/imagesearch/inc/deleteimage.jsp";
    	$.ajax({
		  type: 'GET',
		  url: deleteImageDiaURL,
		  success: function(response){
			//console.info("received delete dialog");
			$("#deleteDialogContainer").html(response);
			ns.previouslyrequested.isDeleteDialogLoded = true;
			ns.previouslyrequested.initDeleteDialog();
			$("#deleteImageDialog").dialog('open');
		  }
		});
    }

    ns.previouslyrequested.initializeGridView = function(){
    	var viewCheckedButton = {
    		gridId:"#accountPendingImagesID",
    		config:{ 
        		caption: js_view,
        		title:js_view_all_check_images,
        		buttonicon: 'ui-icon-image',
        		onClickButton: function(e){
        			//console.info("view checked button click");
        		}
    		}
    	}
    	    	
    	var deleteImageButton = {
        		gridId:"#accountPendingImagesID",
        		config:{ 
            		caption: js_delete,
            		title:js_delete_check_images,
            		buttonicon: 'ui-icon-trash',
            		onClickButton: function(e){
            			ns.previouslyrequested.deleteImages(e);
            		}
        		}
        	} 
    	
    	ns.previouslyrequested.addGridButton(viewCheckedButton);
    	ns.previouslyrequested.addGridButton(deleteImageButton);
    	ns.previouslyrequested.gridInitialized = true;
    }
    
    
    ns.previouslyrequested.beforeDeleteImage = function(imageId){    	
    	ns.previouslyrequested.deleteImages();
    }
    
    ns.previouslyrequested.deleteImages = function(e){
    	//console.info("deleting images.....")
    	var userSelections = ns.previouslyrequested.getSelectedValues("accountPendingImagesID","imageHandle");
    	if(userSelections){
    		var  deleteImagesURL = "/cb/pages/jsp/account/GetImagesAction_deletePendingImages.action";
    		//console.info("Images to be deleted:" +userSelections );
    		if(userSelections!==null){
    			$.ajax({
    				url: deleteImagesURL,
    				type: 'GET',
    				data:{"userSelections":userSelections},
    				success: function(response){
    					//console.info("User selecteds images have been deleted!");
    					ns.previouslyrequested.reloadPendingImagesGrid();
    				}				
    			});			
    		}  
    	}else{
    		var message = js_select_images_to_delete;
			ns.common.showStatus(message);
    	}
 	
    }
    
    ns.previouslyrequested.getSelectedValues = function(gridId,columnName){
		var rows = $('#' + gridId).jqGrid('getGridParam', 'selarrrow');
		var userSelections;
		$.each(rows,function(i,row){
			var rowData = $("#" + gridId).jqGrid('getRowData',row);
			userSelections = (userSelections)? (userSelections + "," + rowData[columnName]):rowData[columnName];
		})
		//console.info("userSelections" + userSelections);
		return userSelections;
	}
    
	ns.previouslyrequested.collectUserCheckedIds = function(){
		var selectedImages = $("#accountPendingImagesID [type=checkbox]:checked");
		var userSelections = null;
		$.each(selectedImages,function(i,entry){
            var elementId = $(entry).attr("id");
            var id = elementId .split("_");
            userSelections = (userSelections)? (userSelections+ "," + id[1]):id[1];
		})
		//console.info(userSelections );
		return userSelections;
	}
    
    ns.previouslyrequested.addGridButton = function(button){
    	//console.info("Adding button for grid " + button.gridId);
    	if(button.gridId){
    		$(button.gridId).jqGrid('navButtonAdd', button.gridId + "_pager", button.config);
    	}else{
    		console.error("Button doesn't have grid id, please verify");
    	}    	
    }
    
    ns.previouslyrequested.reloadPendingImagesGrid = function(){
    	//console.info("reloading pending images grid");
		$("#accountPendingImagesID").trigger("reloadGrid");
    }
    
    ns.previouslyrequested.unavailbleImageIds = [];
    ns.previouslyrequested.availabilityFormatter =function(cellvalue, options, rowObject){
    	var status = rowObject.status;
    	var rowId = options.rowId;
    	if(status){
        	if(status.toUpperCase() != "AVAILABLE"){
        		 ns.previouslyrequested.unavailbleImageIds.push(rowId);
        	}    		
    	}
    	return cellvalue;
    }
    
    ns.previouslyrequested.disableUnavailableImagesCheckBox = function(){
    	var checkBoxes =ns.previouslyrequested.unavailbleImageIds;
    	$.each(checkBoxes,function(i,checkId){
    		$("#jqg_" + checkId).attr("disabled",true);
    	})
    	
    }
    
    ns.previouslyrequested.pendingImageActionFormatter = function(cellvalue, options, rowObject){
		var formatedCellValue = "";
		var deleteLink ="";
		var status = rowObject.status; 
		if(status){
        	if(status.toUpperCase() == "AVAILABLE"){
        		formatedCellValue = "<a title='" +js_view+ "' href='#' onclick='ns.previouslyrequested.beforeViewCheckedImage(\"" + rowObject.imageHandle + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
        		deleteLink = "<a title='" +js_delete+ "' href='#' onClick='ns.previouslyrequested.confirmDelete(\"" + rowObject.imageHandle + "\", \"" + options.rowId + "\")'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
        	}    		
    	}
					
		return ' ' + formatedCellValue + ' '+deleteLink;		
	}
    
    ns.previouslyrequested.confirmDelete = function(imageHandle,rowId){
    	$("#accountPendingImagesID input[type=checkbox]").removeAttr("checked");
    	$("#accountPendingImagesID").jqGrid("resetSelection");
    	jQuery("#accountPendingImagesID").jqGrid('setSelection',rowId);
    	$("#accountPendingImagesID td[title='"+imageHandle+"']").siblings().find($("input[type=checkbox]")).attr('checked','checked');
    	$('#deletePendingImages').trigger('click');
    }
    
    ns.previouslyrequested.gridInitialized = false;
    ns.previouslyrequested.gridLoaded = false;
    
    
    ns.previouslyrequested.registerEvents = function(){
		var deletePendingImageHandler = function(e){
			//console.info("deleting pending images");			
			var userSelections  = ns.previouslyrequested.getSelectedValues("accountPendingImagesID","imageHandle");
			if(userSelections){
				if(ns.previouslyrequested.isDeleteDialogLoded == false){
					ns.previouslyrequested.loadDeleteImageDialog();
				}else{
					$("#deleteImageDialog").dialog('open');
				}							
			}else{
				var message = js_select_images_to_delete;
				ns.common.showStatus(message);
			}		
		}
		
		$("#deletePendingImages").click(deletePendingImageHandler);
    }
	
	ns.previouslyrequested.beforeViewCheckedImage = function(imageHandle){
		//console.info("beforeViewCheckedImage -" + imageHandle);
		var userData = {"userSelections":imageHandle,"module":"PENDING"};
		ns.previouslyrequested.viewCheckedImages(userData);		
	}
	
    ns.previouslyrequested.beforeViewCheckedImages = function(){
		var userSelections = ns.previouslyrequested.getSelectedValues("accountPendingImagesID","imageHandle");		
		if(userSelections){
			var userData = {"userSelections":userSelections,"module":"PENDING"};	
			ns.previouslyrequested.viewCheckedImages(userData);			
		}else{
			var message = js_select_images_to_view;
			ns.common.showStatus(message);
		}
	}
		
    ns.previouslyrequested.viewCheckedImages = function( userData  ){			
		var  URL = "/cb/pages/jsp/account/GetImagesAction_viewCheckedImages.action";
		if(userData!==null){
			$.ajax({
				url: URL,
				type: 'GET',
				data:userData,
				success: function(response){
					if(response){
						$('#checkImageDialogID').html(response).dialog('open');
						ns.imagesearch.initCarousel();							
					}
				}
			});			
		}
	}

    ns.previouslyrequested.isDeleteDialogLoded = false;
    
	$(document).ready(function() {
		ns.previouslyrequested.setup();
	});
	
	$.subscribe('pendingImagesGridOnComplete', function(event,data) {
		//console.info("pendingImagesGridOnComplete");
		var pendingImages = $("#accountPendingImagesID").jqGrid('getDataIDs');
		if(pendingImages.length == 0){
			$("#pendingImagesControlBox").hide();
		}
		 ns.previouslyrequested.disableUnavailableImagesCheckBox();		

         var pendingImagesCheckClickHandler = function(){
            ns.common.checkUnCheckGridCheckAllBox("accountPendingImagesID"); 
         }
         $("#accountPendingImagesID input[type=checkbox]").click(pendingImagesCheckClickHandler);
	});

	$.subscribe('loadPendingImages', function(event,data) {
		//console.info("loadPendingImages");
		ns.previouslyrequested.loadPendingImagesGrid();
	});
	
	$.subscribe('emailImageCompleteTopic', function(event,data) {
		ns.common.closeDialog("checkImageDialogID");
	});