/**
 * 	This js file has all looup grid related functionalities
 */

$("input[inputtype=lookupboxSearchField]").on("keyup",function(e){
	var element = $(this).attr("element");
	var excludeItem = $("#"+ element +"LookupSearchBox").attr("excludeitem");	
	var term  = $("#"+ element +"LookupSearchBox").val();	
	var collectionKey  = $("#"+ element +"LookupSearchBox").attr("collectionkey");	
	
	var data = {
		"element":element,
		"collectionKey":collectionKey,
		"term":term,
		"excludeItem":excludeItem
	};	
	$.publish("common.topics.doLookupSearch",[data]);
});

$("a[buttontype=lookupboxButton]").on("click",function(e){
	var element = $(e.currentTarget).attr("element");
	$("#" + element).lookupbox("closeDialog");
});

$.subscribe("common.topics.doLookupSearch",function(e,d){	
	var element = d.element;
	var term  = $("#"+ element +"LookupSearchBox").val();
	var collectionKey = d.collectionKey;
	var excludeItem = d.excludeItem;
	
	var gridUrl = $("#" + element + "LookupGrid").jqGrid('getGridParam', "url");
	var pos;
	if(gridUrl.indexOf("?")!=-1){
		pos = gridUrl.indexOf("?");
	};
	gridUrl = gridUrl.substring(0,pos);
	
	var aUrl = gridUrl + "?collectionKey=" + collectionKey + "&term=" +  term + "&excludeItem=" + excludeItem;
	aUrl = encodeURI(aUrl);
	
	$("#" + element + "LookupGrid").jqGrid('setGridParam', { url: aUrl});
	$.publish("topics.ux.lookupbox.overrideGlobalSetup",[false]);
	$("#" + element + "LookupGrid").trigger("reloadGrid");	
	$.publish("topics.ux.lookupbox.overrideGlobalSetup",[true]);
});

function lookupActionColumnFormatter(cellvalue, options, rowObject){
	var data = rowObject.value || "";
	var selectButtonHtml = "<a buttontype='lookupboxbutton' action='select' data='" + data  + "'>SELECT</a>";
	return selectButtonHtml;
};

$.subscribe("topics.ux.lookupbox.overrideGlobalSetup",	function (e,isGlobal){
	$.ajaxSetup({ global: isGlobal});
});


$.subscribe("topics.ux.lookupbox.gridOnComplete",	function (e,d){
	var gridId = $(d).attr("id");
	$("table#" + gridId + " tr").attr("style","cursor:pointer;")
});

$.subscribe("topics.ux.lookupbox.onSelectRow",	function (e,d){
	//Getting invoker instance
	var gridId = $(d).attr("id");
	var rowId = e.originalEvent.id;
	var rowData = $("#" + gridId).jqGrid('getRowData',rowId);
	
	var item = {
		"value":rowData.ID + ":" + rowData.currencyCode,
		"label":rowData.displayTextNickNameCurrency
	}
			
	var selectElement = gridId.substring(0,gridId.indexOf("LookupGrid"));
	$("#" + selectElement).lookupbox("value",item);
	$("#" + selectElement).lookupbox("closeDialog");
});


//$.ui.combobox.prototype._create.apply(this, arguments);