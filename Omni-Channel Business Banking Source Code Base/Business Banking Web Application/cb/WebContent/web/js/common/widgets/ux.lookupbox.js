/**
 * Home grown lookup widget
 * It extends combobox and provides functionality to do lookup on keyed element
 * Dialog Configuration properties
 * dualOpenMode: true, opens this dialog for more and lookup button
 * dualOpenMode: false, opens this dialog when lookupbutton is clicked
 */
(function($){

	/*
	* This function encodes the value passed which converts the characters to corresponding 
	* encoding value.
	* @param value (String) value to be encoded.
	*/
	var htmlEncode = function(value) {
		return !value?value:String(value)
			.replace(/&/g, "&amp;")
			.replace(/"/g, "&quot;")
			.replace(/'/g, "&#39;")
			.replace(/</g, "&lt;")
			.replace(/>/g, "&gt;");
	};

	
	/*override _renderItem function to convert label as HTML, with default implementation
	 * label is appending as text*/
	var _renderItem = $.ui.autocomplete.prototype._renderItem;
	$.ui.autocomplete.prototype._renderItem = function(ul, item ){	
		var label = item.label;
		label = htmlEncode(label);
		label = label.replace(new RegExp("(" + $.ui.autocomplete.escapeRegex("&lt;strong&gt;") + ")", "gi"),"<strong>" );
		// retore </strong> elements
		label = label.replace (new RegExp("(" + $.ui.autocomplete.escapeRegex("&lt;/strong&gt;") + ")", "gi"),	"</strong>" );							
		
		return $( "<li>" )
			.append( '<a>' + label + '</a>' )
			.appendTo( ul );
	};
	
	
	$.widget('ux.lookupbox', $.ui.combobox, {
		version: "1.0.2",
		options:{
			source:"/cb/pages/common/LookupBoxAction.action",
			controlType:"client",
			collectionKey:"1",
			maxItems:"100",
			pairTo:"",			
			delay:500,
			minLength:0,
			module:"",
			accounts:"",
			term:"",
			value:"",
			defaultOption:"",
			dataReceived:$.noop(),
			dialog:{
				showDialog:false,				
				url:"/cb/pages/jsp/common/lookup.jsp",
				dualOpenMode:false
			}
		},
		
		_currentSearchTerm:"",
		_previousSearchTerm:"",
		_previousOption:"",
		_initialDataLoaded:false,
		_showFetchedList:false,
		_fetchMoreList:false,	

		_hasDefaultOption:function(){			
			return this.options.defaultOption instanceof Object;
		},

		_getDefaultOptionStr:function(){
			var defaultOptionStr = "<option value='" + this.options.defaultOption.value + "'>" + this.options.defaultOption.label + "</option>"
			return defaultOptionStr;
		},

		_getDefaultOption:function(){
			return {
				"id":this.options.defaultOption.value,
				"label":this.options.defaultOption.label,
				"value":this.options.defaultOption.value
			}
		},

		_getExlcludeItemValue:function(){
			var pairValue;
			if(this.options.pairTo){
				pairValue = $("#" + this.options.pairTo).val() || "";
			}
			return pairValue;
		},

		_getDefaultValue:function(){
			var pairValue;
			if(this.options.pairTo){
				pairValue = $("#" + this.options.pairTo).val() || "";
			}
			return pairValue;
		},


		_getCommonQueryParams:function(){
			var queryParams ={
				"collectionKey":this.options.collectionKey,
				"maxItems":this.options.maxItems,
				"module":this.options.module,
				"accounts":this.options.accounts,
				"rows":	this.options.maxItems,
				"term":this.options.term
			}
			return queryParams;
		},

		_overrideGlobalSetup:function( override ){
			$.ajaxSetup({
			   global: override
			 }); 
			
			if(override === false){
				this.input.addClass("ux-lookupbox-loading");
			}else{
				this.input.removeClass("ux-lookupbox-loading");
			}	
		},
		
		_getSelectedOptionData:function(){
			var selectedOption = this._getSelectedOption();
			var selectedOptionData;
			if(selectedOption){
				selectedOptionData = {
						"id":selectedOption.val(),
						"label":selectedOption.text(),
						"value":selectedOption.text() 
				};
			}
			return selectedOptionData;
		},

		_getSelectedOption:function(){
			var selectedOption = this.element.find("option:selected");
			var option;
			if(selectedOption.val()!="-1"){
				option = selectedOption;
			}
			return option;
		},

		_highlightSearch:function(search,list){			
			$(list).each(function(i,option){										
				var sLabel = option.label;				
				option.label = sLabel.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(search) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
			});			
			return list;
		},
		
		closeDialog:function(){
			this.dialog.dialog('close');
		},
		
		setFocus:function(){
			this.input.click();
			this.input.focus();			
		},
		/**
		* This function refresh widget for data, reloads from source option specified
		*/
		refresh:function(){
			this.clear();
			this._createList();

		},

		option:function(key,value){
			this.options[key] = value;
		},

		/**
		* functions works as setter and getter for value field
		*/
		value:function(item){
			if(item){
				this.input.val(item.label);
				this.input.removeAttr('style');	
				
				//Check if element exists if not create an option for it and append
				var option = this.element.find("[value='" + item.value + "']");
				if(option.length == 0){					
					var firstOption = this.element.find("option:first");
					var optionStr = "<option value='" + item.value + "'>" + item.label + "</option>";
					if(firstOption.length>0){
						$(optionStr).insertAfter(firstOption);
					}else{
						this.element.html(optionStr);
					}					
				}
				this.element.val(item.value);
				this.element.change();
				this._trigger("selected", null, {
					item: this.element.find("[value='" + item.value + "']")
				});
			}else{
				return this.input.val();
			}			
		},
		
		/**
		* Load initial list from source only when there are no options in the base select menu or 
		* widget configuration gets value as option. In other cases widhet will load initial load silently
		*/
		_isInitialLoadRequired:function(){
			/*
			var load = true;
			if(this.options.value){
				this.value(this.options.value);
				load = false;
			}
			if(this.element.has("option").length>0){
				load = false;
			}
			return load;
			*/
			
			return false;
		},


		/**
		* This function loads initial list laziliy
		*/
		_createList:function(el){	
			//console.info("_createList");
			var url = this.options.source;						
			var reqParams = {
				maxItems:(this.options.controlType ==="server") ? this.options.maxItems : "-1",
				collectionKey:this.options.collectionKey,
				"pagingModelType":"list",
				"rows":(this.options.controlType ==="server") ? this.options.maxItems : "-1",
				"el":el
			}
			
			var commonParams = this._getCommonQueryParams();
			reqParams = $.extend(commonParams,reqParams);
			
			
			var addOptions = function(options,that){
			//console.info("_createList-addOptions");
				var optionsStr = "<option value='-1'>" + that.options.hint + "</option>";
				if(that._hasDefaultOption()){
					optionsStr = optionsStr + that._getDefaultOptionStr();
				}
				for(var i=0;i<options.length;i++){
					var option = options[i];
					if(option.ismore){
						optionsStr = optionsStr + "<option value='" + option.id + "' currentpage='" + option.page + "'  ismore='true' >" + option.label + "</option>";
					}else{
						optionsStr = optionsStr + "<option value='" + option.id + "'>" + option.label + "</option>";
					}
					
				}		
				that.element.html(optionsStr);				
				if(that.options.dataReceived){
					that.options.dataReceived();
				}
			};
			
			var that = this;
			$.ajax({
				url: url,
				type:"GET",
				data: reqParams,
				dataType: "json",
				cache:false,
				success: function( data ) {
					if(data){
						var initialLookupData =[];								
						if(data.optionsModel){
							initialLookupData = data.optionsModel;								
						}								
						if(data.total<=1){							
							//that.options.controlType = "client";
						}else{
							if(that.options.controlType === "server"){
								var moreOption = {
									"id":that._currentSearchTerm,
									"label":"...more",
									"value":that._currentSearchTerm,
									"ismore":true,
									"page":data.page
								}
								if(initialLookupData.length>0){
									initialLookupData.push(moreOption);
								}
							}				
												
							that._initialDataLoaded = true;
						}	
						addOptions(initialLookupData,that);							
					}else{
						initialLookupData = [];
					}										
				},
				error: function() {
					initialLookupData = [];
				}
			});
		},
		
		/**
		* This function perfroms paging on list fetches next page and updates the options list
		*/
		_fetchMoreItems:function(currentPage){	
		//console.info("_fetchMoreItems");
			var term = this.input.data("searchTerm");
			var url = this.options.source;						
			var pairValue = this._getExlcludeItemValue();			
			var reqParams = {
				"maxItems":(this.options.controlType ==="server") ? this.options.maxItems : "-1",
				"collectionKey":this.options.collectionKey,
				"page":(currentPage)?++currentPage:0,
				"rows":(this.options.controlType ==="server") ? this.options.maxItems : "-1",
				"pagingModelType":"list",
				"term":term,
				"excludeItem":pairValue
			}
						
			var commonParams = this._getCommonQueryParams();
			reqParams = $.extend(commonParams,reqParams);
			
			
			var addOptions = function(options,that){
				//console.info("_fetchMoreItems-addOptions");			
				var previousOption = $(that.element).find("option:selected");
				var previousOptionValue = $(that.element).find("option:selected").val();
				that._previousOption = "";
				if(previousOption.length>0 && previousOptionValue != "-1"){
					//console.info("needs to preserve this previous option");					
					if(previousOption.attr("ismore")!=="true"){
						that._previousOption = previousOption;
					}
				}
				
				var optionsStr = "<option value='-1'>" + that.options.hint + "</option>";
				for(var i=0;i<options.length;i++){
					var option = options[i];
					if(option.ismore){
						optionsStr = optionsStr + "<option value='" + option.id + "' currentpage='" + option.page + "' ismore='true' >" + option.label + "</option>";
					}else{
						optionsStr = optionsStr + "<option value='" + option.id + "'>" + option.label + "</option>";
					}
				}		
				that.element.html(optionsStr);
				//adding if any previous item is selected
				if(that._previousOption != ""){
					//check if previous item already in options list or not?
					var prevOptionVal = that._previousOption.val();
					var hasOption = false;
					if(that.element.find("option[value='" + prevOptionVal + "']").length>0){
						hasOption = true;
					}
					if(!hasOption){
						var firstOption;
						if(!that._hasDefaultOption()){
							firstOption = $(that.element).find("option:first");
						}else{
							firstOption = $(that.element).find("option:eq(1)");
						}
						
						$(that._previousOption).insertAfter(firstOption);						
					}
					that.input.val(that._previousOption.html()); // reset the input box value to the previous value since we are in more mode now.
				}	

				that._showFetchedList = true;
			};
			
			
			var that = this;			
			that._overrideGlobalSetup(false);				
			
			$.ajax({
				url: url,
				data: reqParams,
				dataType: "json",
				cache:false,
				success: function( data ) {
					if(data){
						var initialLookupData =[];								
						if(data.optionsModel){
							var defaultOption = [];
							if(that._hasDefaultOption()){
								var defOpt = that._getDefaultOption();
								if(defOpt){
									defaultOption.push(defOpt);
								}								
							}
							initialLookupData = defaultOption.concat(data.optionsModel);
						}								
						if(data.total<=1){							
						}else{
							if(that.options.controlType === "server" &&  (data.page<data.total) ){
								var searchTermData = that.input.data("searchTerm");
								var moreOption = {
									"id":searchTermData,
									"label":"...more",
									"value":searchTermData,
									"ismore":true,
									"page":data.page
								}
								if(initialLookupData.length>0){
									initialLookupData.push(moreOption);
								}
							}				
							addOptions(initialLookupData,that);										
							var event = $.Event("click");
							event.isCustomEvent = true;							
							that.button.trigger(event);
						}						
					}else{
						initialLookupData = [];
					}
					that._overrideGlobalSetup(true);					
				},
				error: function() {
					initialLookupData = [];
					that._overrideGlobalSetup(true);					
				}
			});
		},

		_createGrid:function(){
			$("#list2").jqGrid({
				id:"lookupGrid",
				url:'/cb/pages/common/GetLookupGridAction.action?collection=collection&searchString=50',
				datatype: "json",
				colNames:['label','value'],
				colModel:[
					{name:'label',index:'label'},
					{name:'value',index:'value'}
				],
				jsonReader : {root:"gridModel",
							page:"page",
							total:"total",
							records:"records",
							repeatitems : false,
							id: "0"},
				rowNum:10,
				rowList:[10,20,30],
				pager: '#pager2',
				viewrecords: true,
				caption:"Accounts Lookup",
				height:"auto",
				width:"auto"			
			});
			$("#list2").jqGrid('navGrid','#pager2',{edit:false,add:false,del:false});	
		},
	
		_openLookUpBox:function(that){	
			//console.info("_openLookUpBox");
			var collectionKey = (that.options.collectionKey)? that.options.collectionKey : "-1";
			var excludeItem;
			if(that.options.pairTo){
				excludeItem = $("#" + that.options.pairTo).val();
			}
			
			var term = that._currentSearchTerm;
			var id = $(that.element).attr("id");				
			var source = that.options.source;
			var requestParams = {
				"collectionKey":collectionKey,
				"term":term,
				"element":id,
				"excludeItem":excludeItem,
				"source":source
			};
			
			var aUrl = that.options.dialog.url;

			$.ajax({				  	
				url: aUrl,
				data: requestParams,
				success: function(data) {
					that.dialog.html(data);
					that.dialog.dialog('open');
				}
			});									
		},
		
		/**
		* Overriden create function it takes care of creating widet
		*/
		_create:function(){	     
			//console.info("_create");
			//added a container div for alignment.
			var selectBoxDiv = $("<div class ='selectBoxHolder'></div");
			var self = this;
			var select = this.element.hide();
			this.select = select;
			select = select.wrap(selectBoxDiv);
			var elementId = $(this.element).attr("id");
			var source = null;
			
			if (this.options.searchFromFirst == true || (this.options.searchFromFirst === null && this.element.hasClass('searchFromFirst'))){				
				source = function(request, response) {
						$("body").bind(((!$.browser.mozilla)?"mousewheel":"DOMMouseScroll")+".combobox",function(){return false;});
						var matcher = new RegExp('^'+request.term, "i");
						response(select.children("option").map(function() {
							var text = $.trim($(this).text());
							if (this.value && (!request.term || matcher.test(text)))
								var textToReplace = text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "i"), "<strong>$1</strong>"); 
								return {
									id: this.value,									
									label: $.parseHTML(textToReplace) ,
									value: text
								};
						}));
					};
			}else if(this.options.source){

				if(this.options.controlType === "server" && !this._showFetchedList){					
					source = function( request, response ) {	
						url = self.options.source;
						self._currentSearchTerm = request.term;
						
						if(!self._showFetchedList){
							self.input.data("searchTerm",request.term);
							var pairValue = self._getExlcludeItemValue();							
							var reqParams = {
								maxItems:self.options.maxItems,
								excludeItem:pairValue||"",
								collectionKey:self.options.collectionKey,
								pagingModelType:"list"
							}					
							
							var commonParams = self._getCommonQueryParams();
							reqParams = $.extend(commonParams,reqParams,request);
							
							self._overrideGlobalSetup(false);				
							$.ajax({
								url: url,
								data: reqParams,
								dataType: "json",
								success: function( data ) {									
									if(data){
										var lookupData =[];	
										if(data.optionsModel){
											var defaultOption = [];
											if(self._hasDefaultOption()){
												var defOpt = self._getDefaultOption();
												if(defOpt){
													defaultOption.push(defOpt);
												}
											}
											lookupData = defaultOption.concat(data.optionsModel);												
										}								
										if(data.total<=1){
											if(self.lookupButton){
												//self.lookupButton.hide();
											}										
										}else{
											if(self.lookupButton){
												self.lookupButton.show();
											}
											var moreOption = {
												"id":self._currentSearchTerm,
												"label":"...more",
												"value":self._currentSearchTerm,
												"ismore":true,
												"page":data.page
											}
											lookupData.push(moreOption);									
										}
										//console.info("before - response");
										response( self._highlightSearch(self._currentSearchTerm,lookupData));
									}else{
										response( [] );
									}
									self._overrideGlobalSetup(true);	
								},
								error: function() {
									response( [] );
									self._overrideGlobalSetup(true);	
								}
							});	
						}else{
								$("body").bind(((!$.browser.mozilla)?"mousewheel":"DOMMouseScroll")+".combobox",function(){return false;});
								var matcher = new RegExp(request.term, "i");
								response(select.children("option").map(function() {
									var text = $.trim($(this).text());
									if (this.value && (!request.term || matcher.test(text)))	
									var isMoreOptionValue = false;
									if($(this).attr("ismore") === "true"){
										isMoreOptionValue = true;
									}										
									var currentPage = ($(this).attr("currentpage"))?  $(this).attr("currentpage"): "";			
									var searchKey = request.term || self.input.val();
									return {
										id: this.value,
										label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(searchKey) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
										value: (isMoreOptionValue)? this.value: text,
										ismore:isMoreOptionValue,
										page:currentPage
									};
								}));
						}							
					}						
				}else{
						//client cotrolled data
						source = function(request, response) {
								$("body").bind(((!$.browser.mozilla)?"mousewheel":"DOMMouseScroll")+".combobox",function(){return false;});
								var matcher = new RegExp(request.term, "i");
								response(select.children("option").map(function() {
									var text = $.trim($(this).text());
									if (this.value && (!request.term || matcher.test(text)))
										return {
											id: this.value,
											label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
											value: text
										};
								}));
						};
				};		
			}else{
				source = function(request, response) {
						$("body").bind(((!$.browser.mozilla)?"mousewheel":"DOMMouseScroll")+".combobox",function(){return false;});
						var matcher = new RegExp(request.term, "i");
						response(select.children("option").map(function() {						
							var text = $.trim($(this).text());
							if (this.value && (!request.term || matcher.test(text)))
								var isMoreOptionValue = ($(this).attr("ismore"))?  Boolean($(this).attr("ismore")): false;
								return {
									id: this.value,
									label: text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
									value: text,
									ismore:isMoreOptionValue
								};
						}));
					};
			};
			var input = $("<input type='text'>")
				.insertAfter(select)
				.attr('id',this.element.attr('id')+"_autoComplete")
				.autocomplete({
					source: source,
					delay: self.options.delay,
					select: function( event, ui ) {
						if(ui.item.ismore === true){
							self._showFetchedList = false;
							if(self.options.dialog.showDialog === true && self.options.dialog.dualOpenMode === true){
								self._openLookUpBox(self);
							}else{
								//load the next set of data and repopulate the list
								self._fetchMoreList = true;
								self._fetchMoreItems(ui.item.page);								
							}
							input.removeAttr('style');
							//event.stopPropagation(); // This is required since it doesn't reset the original text selected
							//return false;
						}else{
							//Check if element exists if not create an option for it and append
							var option = select.find("[value='" + ui.item.id + "']");
							if(option.length == 0){							
								var lastOption = select.find("option:last");
								var sValue = ui.item.value;
								sValue = htmlEncode(sValue);//Encode the value so that it won;t execute, prevents cross site script execution
								if(lastOption.length>0){
									$("<option value='" + ui.item.id + "'>" + sValue + "</option>").insertBefore(lastOption);
								}else{
									$("<option value='" + ui.item.id + "'>" + sValue + "</option>").appendTo(select);
								}								
							}
							
							select.val(ui.item.id);
							select.change();
							self._trigger("selected", event, {
								item: select.find("[value='" + ui.item.id + "']")
							});
							input.removeAttr('style');

							self._fetchMoreList = false;
							self._showFetchedList = false;
						}	
					},
					open: function(ev,ui) { 	
						$(this).data("ui-autocomplete").menu.element.find("li:contains('Type to search')").hide();						
						$(this).data("ui-autocomplete").menu.element.width($(this).width()); 	


						/*  IE fix for white space issue on opened list */
						if($.browserDetect){				
							var _browser = "";
							_browser = $.browserDetect.browser;
							if(_browser === "Explorer"){
								var listSize = $(this).data("ui-autocomplete").menu.element.find("li").size();
								var height = 20;
								var desiredheight = ((listSize * 20)>200)? 200 : listSize * 20 ;
								height = height + desiredheight;
								$(this).data("ui-autocomplete").menu.element.height(height); 
							}
						}


						$(this).autocomplete("widget").css({
								"overflow-x" : "scroll",
								"overflow-y" : "scroll",
								"white-space" : "nowrap"
						});

					},
					change: function(event,ui){
						self._currentSearchTerm = "";
						self._showFetchedList = false;
						if(!ui.item){
							self._clearOnBlur();
						}
					},
					close:function(ev,ui){						
						//console.info("close");
					},
					
					minLength: self.options.minLength
				})
				.addClass("combobox ui-widget ui-widget-content ui-corner-left");
				
			//input.data("autocomplete")._renderItem = self._renderItem;	
			
			//Check accessibility info
			var labelId = select.attr("aria-labelledby");
			var required = select.attr("aria-required");
			if(labelId){
				input.attr("aria-labelledby", labelId);
			}
			if(required){
				input.attr("aria-required", "true");
			}
			
			input.mouseenter(function(){
				$(this).autocomplete('markin');
				self._showFetchedList = false;
			});
			input.mouseleave(function(){
				$(this).autocomplete('markout');
			});
			
			//a fix for getting rid of hint as soon as user starts typing
			input.keyup(function(){
				var text = input.val();		
				var sText = "";
				sText = text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(self.options.hint) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "");
				if(sText){
					input.val(sText);
				}				
			});
			
			input.attr('size', self.options.size);
			this.input = input;

			//save reference to select in data for ease in calling methods
			this.input.data('selectelement', this.element);
			
			var ua = $.browser;
			if ( ua.mozilla && ua.version.slice(0,3) == "1.9" ) {
				var button = $("<button/>")
				.insertAfter(input).html('&nbsp;')
				.attr("tabIndex", -1)
				.attr("id", this.element.attr('id')+"_showAllButton")
				.attr("title", js_showAllItems)
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				}).removeClass("ui-corner-all")
				.addClass("ui-corner-right ui-button-icon")
				.click(function(e) {
					// close if already visible
					if (input.autocomplete("widget").is(":visible")) {
						input.autocomplete("close");
							return false;
					}
					// pass empty string as value to search for, displaying all results
					input.autocomplete("search", "");
					//input.focus();
					return false;
				});
				
				//adding lookup button step2 in creation
				if(this.options.controlType === "server" && this.options.dialog.showDialog === true ){		
					var lookupButton = $("<button/>")
					.insertAfter(button).html('&nbsp;')
					.attr("id", this.element.attr('id')+"LookupBoxButton")
					.attr("tabIndex", 0)
					.attr("title", js_showAllItems)
					.button({
						label: "..."
					})
					.addClass("")
					.click(function(e){
						e.preventDefault();
						self._openLookUpBox(self);
						return false;
					});	
					this.lookupButton = lookupButton;				
				}
			}
			else{
				var button = $("<button/>")
				.insertAfter(input).html('&nbsp;')
				.attr("tabIndex", -1)
				.attr("title", js_showAllItems)
				.button({
					icons: {
						primary: "ui-icon-triangle-1-s"
					},
					text: false
				}).removeClass("ui-corner-all")
				.addClass("ui-corner-right ui-button-icon comboboxbtn")
				.click(function(e) {
					self._showFetchedList = false;
					if(e.isCustomEvent){
						if(e.isCustomEvent === true){
							self._showFetchedList = true; //only show fetched list when custom event on ...more is clicked
						}
					}
					
					// close if already visible
					if (input.autocomplete("widget").is(":visible")) {
						input.autocomplete("close");
						return false;
					}
					// pass empty string as value to search for, displaying all results
					input.autocomplete("search", "");
					//input.focus();
						return false;
				});
				
				//adding lookup button step2 in creation
				if(this.options.controlType === "server" && this.options.dialog.showDialog === true){		
					var lookupButton = $("<button/>")
					.insertAfter(button).html('&nbsp;')
					.attr("id", this.element.attr('id')+"LookupBoxButton")
					.attr("tabIndex", 0)
					.attr("title", js_showAllItems)
					.button({
						label: "..."
					})
					.addClass("")
					.click(function(e){
						e.preventDefault();
						self._openLookUpBox(self);
						return false;
					});	
					this.lookupButton = lookupButton;					
				}		
			}
			this.button = button;
			
			//QTS:758711, Accessibility fix: on arror key down trigger a search or perform keydown/up on opened menu
			this.button.keydown(function(e){
				if(e.keyCode === 38 || e.keyCode === 40){
					self.input.focus();
					var $menu = $(".ui-autocomplete:visible");
					if($menu.length>0){
						$menu.menu( "focus", null, $menu.find( ".ui-menu-item:first"));				
					}else{
						var strSearchText = (self.input.val() === self.options.hint) ? "" : self.input.val();
						self.input.autocomplete("search", strSearchText);	
					}	
				}				
			});

			//Let the <input> box display the current selected value of the <select>
			//If no option is selected (value is '' or -1), display "Type to search'
			var selectedVal = select.find("option:selected").val();
        	var selectedText = select.find("option:selected").text();

			if(!!selectedVal &&	selectedVal != '' && selectedVal != '-1'){
				input.attr('value', $.trim(selectedText));
			} else {
				input.attr('style', 'color:#c8c8c8');
	       		input.attr('value', self.options.hint);
			}        		

		    input.click(function(){		
				if($(this).val() == self.options.hint){
					$(this).attr('value', '');
					input.removeAttr('style');
				}
		    });
			
		    input.focusin(function(){
		    	if(input.val() === self.options.hint){
		    		input.val('').removeAttr("style");
		    	}
		    });
			
			//input.trigger('blur');
			input.blur(function(){
				var $input = $(this);
				if($input.val() == '' || $input.val() == self.options.hint){
					self._clearOnBlur();
				}
			})
			
			if(this.element.attr('disabled')==true) this._setOption('disabled',true);
			
			if(this.options.dialog.showDialog === true){
				//Create Lookup Dialog			
				var dialog = $("<div>")
				.appendTo($('body'))
				.attr('id',this.element.attr('id')+"LookupDialog");			
				this.dialog = dialog;				
				this.dialog.dialog({
					"title":"Account Lookup",
					"autoOpen": false
				});			
			}
			
			if(this._isInitialLoadRequired()){
				this._createList(elementId);				
			}			
		}
	});
})(jQuery);