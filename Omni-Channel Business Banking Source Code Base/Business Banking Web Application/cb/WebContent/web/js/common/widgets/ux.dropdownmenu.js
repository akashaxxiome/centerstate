/**
 * Widget to provide drop down menu on Keyboard keys(Enter/Space or Up/Down/Right/Left).
 * This widget functionality should not be confused with Select functionality. 
 * This widget will work on the markUp in following order. 
 *	<div class="dropdown">
 * 		<ul class="dropdownMenuBar">
 * 			<li class="dropdownMenuParent">
 * 				<span class="dropdownSelectedMenuLbl"></span>
 * 				<ul class="dropdownOptionHolder">
 *  				<li class="dropdownMenuItem">option1</li>
 *  				<li class="dropdownMenuItem">option2</li>
 *  				<li class="dropdownMenuItem">option3</li>
 * 				</ul>
 * 			</li>
 *		<ul>
 *	</div> 
 */
(function($, undefined) {
  $.widget('ux.dropdownmenu', {
	  options: {
	        change : $.noop,// Event triggered on change of options
	        showSelectedMenu : false,
	        menuPopOverClass : ""
	  },
	  _create : function (){
		  this.menuWrapper = this.element;
		  this.menuWrapper.attr("role", "navigation");
		  /*
		   * Set aria attribute and roles accordingly. 
		   */
			  this.$rootUL = this.menuWrapper.children('ul');
			  this.$rootUL.attr("role", "menubar");
			  
			  // jQuerry array of all root-level menu items
			  this.$rootLiItems = this.$rootUL.children('li');
			  this.$rootLiItems.attr({"role": "menuitem","aria-haspopup":"true","tabindex" : "0"});
			  
			  // jQuery array of menu items
			  this.$items = this.$rootUL.find('.dropdownMenuItem');
			  this.$items.attr({"role":"menuitem", "tabindex":"-1"});
			  
			  // jQuery array of drop down option holder
			  var dropdownOptHolders = this.$rootUL.find('.dropdownOptionHolder');
			  dropdownOptHolders.attr({"role":"menu", "aria-hidden":"true"});		  
		  
		  // jQuery array of menu items
		  this.$parents = this.$rootUL.find('.dropdownMenuParent');
		  this.$allItems = this.$parents.add(this.$items); // jQuery array of all menu items
		  this.$activeItem = null; // jQuery object of the menu item with focus
		  
		  this.vmenu = false;
		  this.bChildOpen = false; // true if child menu is open
		  this.keys = {
		            tab:    9,
		            enter:  13,
		            esc:    27,
		            space:  32,
		            left:   37,
		            up:     38,
		            right:  39,
		            down:   40 
		   };
		
		   // bind event handlers
		   this._bindHandlers();		   
		},

		destroy: function () {
			$.Widget.prototype.destroy.apply(this, arguments);
		},		
		
		// Function bindHandlers() is a member function to bind event handlers
		// for the widget.
		//
		// @return N/A
		//
		_bindHandlers: function() {

			var thisObj = this;

		   // bind a click handler
		   this.$allItems.click(function(e) {
		      return thisObj._handleClick($(this), e);
		   });

		   // ////////// bind key event handlers //////////////////
		  
		   // bind a keydown handler
		   this.$allItems.keydown(function(e) {
		      return thisObj._handleKeyDown($(this), e);
		   });

		   // bind a keypress handler
		   this.$allItems.keypress(function(e) {
		      return thisObj._handleKeyPress($(this), e);
		   });

		   // bind a focus handler
		  /* this.$allItems.focus(function(e) {
		      return thisObj._handleFocus($(this), e);
		   });*/

		   // bind a blur handler
		   /*this.$allItems.blur(function(e) {
		      return thisObj._handleBlur($(this), e);
		   });*/

		   // bind a document click handler
		   $(document).click(function(e) {
			   return thisObj._handleDocumentClick(e);
		   });

		}, // end bindHandlers()
		

		// Function handleClick() is a member function to process click events
		// for the top menus.
		//
		// @param($item object) $item is the jquery object of the item firing
		// the event
		//
		// @param(e object) e is the associated event object
		//
		// @return(boolean) Returns false;
		//
		_handleClick: function($item, e) {

		   var $parentUL = $item.parent();
		   var options = this.options, dropdownOptionHolder = null;

		   if ($parentUL.is('.dropdownMenuBar')) {
		         // open the child menu if it is closed
		         dropdownOptionHolder = $item.children('ul').first();
		         this._changeStyleClass(dropdownOptionHolder, false);
		         dropdownOptionHolder.attr('aria-hidden', 'false');
		         //dropdownOptionHolder.show();
		         this.bChildOpen = true;		         
		   }
		   else {
		      // process the menu choice
		      //this.processMenuChoice($item);

		      // remove hover and focus styling
		      //this.$allItems.removeClass('menu-hover menu-hover-checked menu-focus menu-focus-checked');

		      // close the menu
		      dropdownOptionHolder = this.$rootUL.find('ul').not('.dropdownMenuBar');
		      dropdownOptionHolder.attr('aria-hidden','true');
		      this._changeStyleClass(dropdownOptionHolder, true);
		      
		      // move focus to the text area
		      // this.textarea.$rootUL.focus();
		   }		   
		   e.stopPropagation();
	       return false;

		}, // end handleClick()
		
		//
		// Function handleKeyDown() is a member function to process keydown
		// events for the menus.
		//
		// @param($item object) $item is the jquery object of the item firing
		// the event
		// @param(e object) e is the associated event object
		//
		// @return(boolean) Returns false if consuming; true if propagating
		//
		_handleKeyDown: function($item, e) {

		   if (e.altKey || e.ctrlKey) {
		       // Modifier key pressed: Do not process
		       return true;
		   }

		   var $itemUL = $item.parent();
		   switch(e.keyCode) {
		      case this.keys.tab: {

		         // hide all menu items and update their aria attributes
		         //this.$rootUL.find('ul').hide().attr('aria-hidden', 'true');
		    	  var allSubMenuUL = this.$rootUL.find('ul'); 
		    	  allSubMenuUL.attr('aria-hidden', 'true');
		    	  this._changeStyleClass(allSubMenuUL, true);

		         // remove focus styling from all menu items
		         //this.$allItems.removeClass('menu-focus');

		         this.$activeItem = null;
		         this.bChildOpen == false;

		         break;
		      }
		      case this.keys.esc: {
		         if ($itemUL.is('.dropdownMenuBar')) {
		            // hide the child menu and update the aria attributes
		        	var subMenuUL = $item.children('ul').first() 
		            subMenuUL.attr('aria-hidden', 'true');
		        	this._changeStyleClass(subMenuUL, true);
		         }
		         else {

		            // move up one level
		            this.$activeItem = $itemUL.parent();

		            // reset the childOpen flag
		            this.bChildOpen = false;

		            // set focus on the new item
		            this.$activeItem.focus();

		            // hide the active menu and update the aria attributes
		            $itemUL.attr('aria-hidden', 'true');
		            this._changeStyleClass($itemUL, true);
		         }
		         e.stopPropagation();
		         return false;
		      }
		      case this.keys.enter:
		      case this.keys.space: {

		         var $parentUL = $item.parent();

		         if ($parentUL.is('.dropdownMenuBar')) {
		            // open the child menu if it is closed
		        	 var subMenuUL = $item.children('ul').first();
		        	 subMenuUL.attr('aria-hidden', 'false');
		            this.bChildOpen = true;
		            this._changeStyleClass(subMenuUL, false);
		         }
		         else {
		            // process the menu choice
		            //this.processMenuChoice($item);

		            // remove hover styling
		            /*this.$allItems.removeClass('menu-hover menu-hover-checked');
		            this.$allItems.removeClass('menu-focus menu-focus-checked');*/

		            // close the menu
		        	var allSubMenuUL = this.$rootUL.find('ul').not('.dropdownMenuBar');
		        	allSubMenuUL.attr('aria-hidden','true');
		        	this._changeStyleClass(allSubMenuUL, true);

		            // clear the active item
		            this.$activeItem = null;
		            
		            // Trigger child element onclick
		            var childElement = $item.children(), i=0;
		            for(i = 0; i < childElement.length; i++){
		            	childElement.triggerHandler("click");
		            }
		            
		            // Trigger onclick if provided for current li
		            $item.triggerHandler("click");
		            
		            // Update value if showSelectedMenu is true
		            if(this.options.showSelectedMenu){
		            	this._setSelectionOptionValue($item);
		            }
		         }
		         e.stopPropagation();
		         return false;
		      }

		      case this.keys.left: {

		         if (this.vmenu == true && $itemUL.is('.dropdownMenuBar')) {
		            // If this is a vertical menu and the root-level is active,
					// move
		            // to the previous item in the menu
		            this.$activeItem = this._moveUp($item); 
		         }
		         else {
		            this.$activeItem = this._moveToPrevious($item); 
		         }

		         this.$activeItem.focus();

		         e.stopPropagation();
		         return false;
		      }
		      case this.keys.right: {

		         if (this.vmenu == true && $itemUL.is('.dropdownMenuBar')) {
		            // If this is a vertical menu and the root-level is active,
					// move
		            // to the next item in the menu
		            this.$activeItem = this._moveDown($item); 
		         }
		         else {
		            this.$activeItem = this._moveToNext($item);
		         }

		         this.$activeItem.focus();

		         e.stopPropagation();
		         return false;
		      }
		      case this.keys.up: {

		         if (this.vmenu == true && $itemUL.is('.dropdownMenuBar')) {
		            // If this is a vertical menu and the root-level is active,
					// move
		            // to the previous root-level menu
		            this.$activeItem = this._moveToPrevious($item); 
		         }
		         else {
		            this.$activeItem = this._moveUp($item); 
		         }

		         this.$activeItem.focus();

		         e.stopPropagation();
		         return false;
		      }
		      case this.keys.down: {

		         if (this.vmenu == true && $itemUL.is('.dropdownMenuBar')) {
		            // If this is a vertical menu and the root-level is active,
					// move
		            // to the next root-level menu
		            this.$activeItem = this._moveToNext($item); 
		         }
		         else {
		            this.$activeItem = this._moveDown($item); 
		         }

		         this.$activeItem.focus();

		         e.stopPropagation();
		         return false;
		      }
		   } // end switch

		   return true;

		}, // end handleMenuKeyDown()

		//
		// Function moveToNext() is a member function to move to the next menu
		// level. This will be either the next root-level menu or the child of a menu
		// parent. 
		// If at the root level and the active item is the last in the menu, this
		// function will loop to the first menu item.
		// If the menu is a horizontal menu, the first child element of the
		// newly selected menu will be selected
		//
		// @param($item object) $item is the active menu item
		//
		// @return (object) Returns the item to move to. Returns $item is no move is possible
		//
		_moveToNext: function($item) {

			//$item's containing menu	
		   var $itemUL = $item.parent(); // 
		   var $menuItems = $itemUL.children('li'); // the items in the currently active menu
		   var menuNum = $menuItems.length; // the number of items in the active menu
		   var menuIndex = $menuItems.index($item); // the items index in its menu
		   var $newItem = null;
		   var $newItemUL = null;

		   if ($itemUL.is('.dropdownMenuBar')) {
		      /*
		       *  This is the root level move to next sibling. This will require closing
		       *  the current child menu and opening the new one.
		       */
		       //Check if its not the last menu.
			   if (menuIndex < menuNum-1) { 
		         $newItem = $item.next();
		      }
		      else { // wrap to first root level menu item
		         $newItem = $menuItems.first();
		      }

		      // close the current child menu (if applicable)
		      if ($item.attr('aria-haspopup') == 'true') {

		         var $childMenu = $item.children('ul').first();

		         if ($childMenu.attr('aria-hidden') == 'false') {
		            // hide the child and update aria attributes accordingly
		            $childMenu.attr('aria-hidden', 'true');
		            this._changeStyleClass($childMenu, true);
		            this.bChildOpen = true;
		         }
		      }

		      // remove the focus styling from the current menu
		      //$item.removeClass('menu-focus');

		      // open the new child menu (if applicable)
		      if (($newItem.attr('aria-haspopup') == 'true') && (this.bChildOpen == true)) {

		         var $childMenu = $newItem.children('ul').first();

		         // open the child and update aria attributes accordingly
		         $childMenu.attr('aria-hidden', 'false');
		         this._changeStyleClass($childMenu, false);
		         this.bChildOpen = true;

		         /*
					 * Uncomment this section if the first item in the child
					 * menu should be automatically selected
					 * 
					 * if (!this.vmenu) { // select the first item in the child
					 * menu $newItem = $childMenu.children('li').first(); }
					 */

		      }
		   }
		   else {
		      // this is not the root level. If there is a child menu to be moved into, do that;
		      // otherwise, move to the next root-level menu if there is one
		      if ($item.attr('aria-haspopup') == 'true') {
		         
		         var $childMenu = $item.children('ul').first();

		         $newItem = $childMenu.children('li').first();

		         // show the child menu and update its aria attributes
		         $childMenu.attr('aria-hidden', 'false');
		         this._changeStyleClass($childMenu, false);
		         this.bChildOpen = true;
		      }
		      else {
		         // at deepest level, move to the next root-level menu

		         if (this.vmenu == true) {
		            // do nothing
		            return $item;
		         }

		         var $parentMenus = null;
		         var $rootItem = null;

		         // get list of all parent menus for item, up to the root
					// level
		         $parentMenus = $item.parentsUntil('div').filter('ul').not('.dropdownMenuBar');

		         // hide the current menu and update its aria attributes
					// accordingly
		         $parentMenus.attr('aria-hidden', 'true');
		         this._changeStyleClass($parentMenus, true);

		         // remove the focus styling from the active menu
		         /*$parentMenus.find('li').removeClass('menu-focus');
		         $parentMenus.last().parent().removeClass('menu-focus');*/

		         $rootItem = $parentMenus.last().parent(); // the containing
															// root for the menu

		         menuIndex = this.$rootLiItems.index($rootItem);

		         // if this is not the last root menu item, move to the next
					// one
		         if (menuIndex < this.$rootLiItems.length-1) {
		            $newItem = $rootItem.next();
		         }
		         else { // loop
		            $newItem = this.$rootLiItems.first();
		         }

		         if ($newItem.attr('aria-haspopup') == 'true') {
		            var $childMenu = $newItem.children('ul').first();

		            $newItem = $childMenu.children('li').first();

		            // show the child menu and update it's aria attributes
		            $childMenu.attr('aria-hidden', 'false');
		            this._changeStyleClass($childMenu, false);
		            this.bChildOpen = true;
		         }
		      }
		   }

		   return $newItem;
		},

		//
		// Function moveToPrevious() is a member function to move to the
		// previous menu level. This will be either the previous root-level menu or the child of a
		// menu parent. If
		// at the root level and the active item is the first in the menu, this
		// function will loop to the last menu item.
		//
		// If the menu is a horizontal menu, the first child element of the
		// newly selected menu will be selected
		// @param($item object) $item is the active menu item
		// @return (object) Returns the item to move to. Returns $item is no
		// move is possible
		//
		_moveToPrevious: function($item) {

		   var $itemUL = $item.parent(); // $item's containing menu
		   var $menuItems = $itemUL.children('li'); // the items in the
													// currently active menu
		   var menuNum = $menuItems.length; // the number of items in the active
											// menu
		   var menuIndex = $menuItems.index($item); // the items index in its
													// menu
		   var $newItem = null;
		   var $newItemUL = null;

		   if ($itemUL.is('.dropdownMenuBar')) {
		      // this is the root level move to previous sibling. This will
				// require closing
		      // the current child menu and opening the new one.

		      if (menuIndex > 0) { // not the first root menu
		         $newItem = $item.prev();
		      }
		      else { // wrap to last item
		         $newItem = $menuItems.last();
		      }

		      // close the current child menu (if applicable)
		      if ($item.attr('aria-haspopup') == 'true') {

		         var $childMenu = $item.children('ul').first();

		         if ($childMenu.attr('aria-hidden') == 'false') {
		            // hide the child and update aria attributes accordingly
		            $childMenu.attr('aria-hidden', 'true');
		            this._changeStyleClass($childMenu, true);
		            this.bChildOpen = true;
		         }
		      }

		      // remove the focus styling from the current menu
		      //$item.removeClass('menu-focus');

		      // open the new child menu (if applicable)
		      if (($newItem.attr('aria-haspopup') == 'true') && this.bChildOpen == true) {

		         var $childMenu = $newItem.children('ul').first();

		         // open the child and update aria attributes accordingly
		         $childMenu.attr('aria-hidden', 'false');
		         this._changeStyleClass($childMenu, false);

		         /*
					 * Uncomment this section if the first item in the child
					 * menu should be automatically selected
					 * 
					 * if (!this.vmenu) { // select the first item in the child
					 * menu $newItem = $childMenu.children('li').first(); }
					 */

		      }
		   }
		   else {
		      // this is not the root level. If there is a parent menu that is
				// not the
		      // root menu, move up one level; otherwise, move to first item
				// of the previous
		      // root menu.

		      var $parentLI = $itemUL.parent();
		      var $parentUL = $parentLI.parent();

		      var $parentMenus = null;
		      var $rootItem = null;

		      // if this is a vertical menu or is not the first child menu
		      // of the root-level menu, move up one level.
		      if (this.vmenu == true || !$parentUL.is('.dropdownMenuBar')) {

		         $newItem = $itemUL.parent();

		         // hide the active menu and update aria-hidden
		         $itemUL.attr('aria-hidden', 'true');
		         this._changeStyleClass($itemUL, true);

		         // remove the focus highlight from the $item
		         //$item.removeClass('menu-focus');

		         if (this.vmenu == true) {
		            // set a flag so the focus handler does't reopen the menu
		            this.bChildOpen = false;
		         }

		      }
		      else { // move to previous root-level menu

		         // hide the current menu and update the aria attributes
					// accordingly
		         $itemUL.attr('aria-hidden', 'true');
		         this._changeStyleClass($itemUL, true);

		         // remove the focus styling from the active menu
		         /*$item.removeClass('menu-focus');
		         $parentLI.removeClass('menu-focus');*/

		         menuIndex = this.$rootLiItems.index($parentLI);

		         if (menuIndex > 0) {
		            // move to the previous root-level menu
		            $newItem = $parentLI.prev();
		         }
		         else { // loop to last root-level menu
		            $newItem = this.$rootLiItems.last();
		         }

		         // add the focus styling to the new menu
		         //$newItem.addClass('menu-focus');

		         if ($newItem.attr('aria-haspopup') == 'true') {
		            var $childMenu = $newItem.children('ul').first();

		            // show the child menu and update it's aria attributes
		            $childMenu.attr('aria-hidden', 'false');
		            this._changeStyleClass($childMenu, false);
		            this.bChildOpen = true;

		            $newItem = $childMenu.children('li').first();
		         }
		      }
		   }

		   return $newItem;
		},

		// Function moveDown() is a member function to select the next item in a
		// menu. If the active item is the last in the menu, this function will loop
		// to the first menu item.
		//
		// @param($item object) $item is the active menu item
		// @param(startChr char) [optional] startChr is the character to attempt
		// to match against the beginning of the
		// menu item titles. If found, focus moves to the next menu item
		// beginning with that character.
		//
		// @return (object) Returns the item to move to. Returns $item is no
		// move is possible
		//
		_moveDown: function($item, startChr) {

			// $item's containing menu
		   var $itemUL = $item.parent(); 
		   
		   // Items in the currently active menu
		   var $menuItems = $itemUL.children('li').not('.separator'); 
		   var menuNum = $menuItems.length; // the number of items in the active
											// menu
		   var menuIndex = $menuItems.index($item); // the items index in its
													// menu
		   var $newItem = null;
		   var $newItemUL = null;

		   if ($itemUL.is('.dropdownMenuBar')) { // this is the root level menu

		      if ($item.attr('aria-haspopup') != 'true') {
		         // No child menu to move to
		         return $item;
		      }

		      // Move to the first item in the child menu
		      $newItemUL = $item.children('ul').first();
		      $newItem = $newItemUL.children('li').first();

		      // make sure the child menu is visible
		      $newItemUL.attr('aria-hidden', 'false');
		      this._changeStyleClass($newItemUL, false);
		      this.bChildOpen = true;

		      return $newItem;
		   }

		   // if $item is not the last item in its menu, move to the next item.
		   // If startChr is specified, move
		   // to the next item with a title that begins with that character.
		   if (startChr) {
		      var bMatch = false;
		      var curNdx = menuIndex+1;

		      // check if the active item was the last one on the list
		      if (curNdx == menuNum) {
		         curNdx = 0;
		      }

		      // Iterate through the menu items (starting from the current
		      // item and wrapping) until a match is found
		      // or the loop returns to the current menu item
		      while (curNdx != menuIndex)  {

		         var titleChr = $menuItems.eq(curNdx).html().charAt(0);

		         if (titleChr.toLowerCase() == startChr) {
		            bMatch = true;
		            break;
		         }

		         curNdx = curNdx+1;

		         if (curNdx == menuNum) {
		            // reached the end of the list, start again at the beginning
		            curNdx = 0;
		         }
		      }

		      if (bMatch == true) {
		         $newItem = $menuItems.eq(curNdx);

		         // remove the focus styling from the current item
		         //$item.removeClass('menu-focus menu-focus-checked');

		         return $newItem
		      }
		      else {
		         return $item;
		      }
		   }
		   else {
		      if (menuIndex < menuNum-1) {
		         $newItem = $menuItems.eq(menuIndex+1);
		      }
		      else {
		         $newItem = $menuItems.first();
		      }
		   }

		   // remove the focus styling from the current item
		   //$item.removeClass('menu-focus menu-focus-checked');

		   return $newItem;
		},

		//
		// Function moveUp() is a member function to select the previous item in
		// a menu. If the active item is the first in the menu, this function will loop
		// to the last menu item.
		//
		// @param($item object) $item is the active menu item
		//
		// @return (object) Returns the item to move to. Returns $item is no
		// move is possible
		//
		_moveUp: function($item) {

		   var $itemUL = $item.parent(); // $item's containing menu
		   
		   // Items in the currently active menu
		   var $menuItems = $itemUL.children('li').not('.separator');
																		
		   var menuNum = $menuItems.length; // the number of items in the active
											// menu
		   var menuIndex = $menuItems.index($item); // the items index in its
													// menu
		   var $newItem = null;
		   var $newItemUL = null;

		   if ($itemUL.is('.dropdownMenuBar')) { // this is the root level menu

		      // nothing to do
		      return $item;
		   }

		   // if $item is not the first item in its menu, move to the previous
			// item
		   if (menuIndex > 0) {

		      $newItem = $menuItems.eq(menuIndex-1);
		   }
		   else {
		      // loop to top of menu
		      $newItem = $menuItems.last();
		   }

		   // remove the focus styling from the current item
		   //$item.removeClass('menu-focus menu-focus-checked');

		   return $newItem;
		},

		//
		// Function handleKeyPress() is a member function to process keydown
		// events for the menus.
		// @param($item object) $menu is the jquery object of the item firing
		// the event
		// @param(e object) e is the associated event object
		//
		// @return(boolean) Returns false if consuming; true if propagating
		//
		_handleKeyPress: function($item, e) {

		   if (e.altKey || e.ctrlKey || e.shiftKey) {
		       // Modifier key pressed: Do not process
		       return true;
		   }

		   switch(e.keyCode) {
		      case this.keys.tab: {
		         return true;
		      }
		      case this.keys.esc:
		      case this.keys.enter:
		      case this.keys.space:
		      case this.keys.up:
		      case this.keys.down:
		      case this.keys.left:
		      case this.keys.right: {

		         e.stopPropagation();
		         return false;
		      }
		      default : {
		         var chr = String.fromCharCode(e.which);

		         this.$activeItem = this.moveDown($item, chr);
		         this.$activeItem.focus();

		         e.stopPropagation();
		         return false;
		      }

		   } // end switch

		   return true;

		}, // end handleKeyPress()

		//
		// Function handleDocumentClick() is a member function to process click
		// events on the document. Needed
		// to close an open menu if a user clicks outside the menu
		//
		// @param(e object) e is the associated event object
		//
		// @return(boolean) Returns true;
		//
		_handleDocumentClick: function(e) {

		   // get a list of all child menus
		   var $childMenus = this.$rootUL.find('ul').not('dropdownMenuBar');

		   // hide the child menus
		   this._changeStyleClass($childMenus, true);
		   $childMenus.attr('aria-hidden', 'true');
		   

		   //this.$allItems.removeClass('menu-focus menu-focus-checked');

		   this.$activeItem = null;

		   // allow the event to propagate
		   return true;

		}, // end handleDocumentClick()

		/*
		 * Function to apply or remove  
		 */
		_changeStyleClass : function(element, isRemove){
			var className = this.options.menuPopOverClass;
			if(className && element && element.length > 0){
				if(isRemove){
					element.removeClass(className);
				} else {
					element.addClass(className);
				}
			}
		},
		
		/*
		 * 
		 */
		_setSelectionOptionValue : function($element){
			var menuParent = $element.parentsUntil(".dropdownMenuParent").parent();
			var selectedOptElement = $element.children().first();
			var selectedMenuTitle = "";
			if(selectedOptElement.length > 0){
				selectedMenuTitle = selectedOptElement.first().text();
			} else {
				selectedMenuTitle = $element.text();
			}
			
			var menuTitlePlaceHolder = menuParent.children(".dropdownSelectedMenuLbl");
			menuTitlePlaceHolder.text(selectedMenuTitle);
		}
  	});
}(jQuery));