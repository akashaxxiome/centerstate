/** 
*******************************************************************************************
* EXTENDED DATE RANGE PICKER WIDGET
*******************************************************************************************
* This widget is extended version of date range picker from filament group
* It adds ajax support for date range picker
* extended from jQuery-Plugin "daterangepicker.jQuery.js"
* Different preset ranges are available as
*  $.fn.extdaterangepicker.defaults.RANGES
*  e.g $.fn.extdaterangepicker.defaults.RANGES.TODAY, key field in presetRanges defines the constant.
* for i18n implementation create locale file called ux.extdaterangepicker.locale.{locale-name}.js
* @example
* $("input[name=dateRangePicker]").extDateRange();
*/

(function($){
	$.fn.extdaterangepicker = function( options ) {	
	var element = this;
	var config = {
		cssClass:'ui-widget-content ui-corner-all hasDatepicker dateRangePickerInputField',
		url:'',
		arrows: false,
		presetRanges: [
			{text: 'Today', dateStart: 'today', dateEnd: 'today' },
			{text: 'Yesterday', dateStart: 'today-1', dateEnd: 'today-1' },
			{text: 'Current Week', dateStart: function(){return Date.today().moveToDayOfWeek(0,-1).addDays(1);}, dateEnd: function(){return Date.today().moveToDayOfWeek(0,-1).addDays(5);} },
			{text: 'Current Month', dateStart: function(){return Date.parse('today').moveToFirstDayOfMonth();}, dateEnd: function(){return Date.parse('today').moveToLastDayOfMonth();}},
			{text: 'Next Week', dateStart: function(){return Date.parse('today').moveToDayOfWeek(1,1)}, dateEnd: function(){return Date.parse('today').moveToDayOfWeek(1,1).addDays(4)} },
			{text: 'Next Month', dateStart: function(){return Date.parse('today').moveToMonth(Date.parse('today').getMonth()+1).moveToFirstDayOfMonth(); }, dateEnd: function(){return Date.parse('today').moveToMonth(Date.parse('today').getMonth()+1).moveToLastDayOfMonth(); } },
			{text: 'Next 30 Days', dateStart: 'today', dateEnd: 'today+30days' },
			{text: 'Last Week', dateStart: function(){return Date.today().addWeeks(-1).moveToDayOfWeek(0,-1).addDays(1)}, dateEnd: function(){return Date.today().addWeeks(-1).moveToDayOfWeek(0,-1).addDays(5)}},
			{text: 'Last 7 Days', dateStart: 'today-6days', dateEnd: 'today' },
			{text: 'Last 30 Days', dateStart: 'today-29days', dateEnd: 'today' },
			{text: 'Last 60 Days', dateStart: 'today-59days', dateEnd: 'today' },
			{text: 'Last 90 Days', dateStart: 'today-89days', dateEnd: 'today' },
			{text: 'Month to date', dateStart: function(){ return Date.parse('today').moveToFirstDayOfMonth();  }, dateEnd: 'today' },
			{text: 'Last Month', dateStart: function(){ return Date.parse('1 month ago').moveToFirstDayOfMonth();  }, dateEnd: function(){ return Date.parse('1 month ago').moveToLastDayOfMonth();  } },
			{text: '+/- 30Days', dateStart: 'today-30days', dateEnd: 'today+30days' }
			
			//extras:
			//{text: 'Year to date', dateStart: function(){ var x= Date.parse('today'); x.setMonth(0); x.setDate(1); return x; }, dateEnd: 'today' },			
			//{text: 'Tomorrow', dateStart: 'Tomorrow', dateEnd: 'Tomorrow' },
			//{text: 'Ad Campaign', dateStart: '03/07/08', dateEnd: 'Today' },
			//{text: 'Last 30 Days', dateStart: 'Today-30', dateEnd: 'Today' },
			//{text: 'Next 30 Days', dateStart: 'Today', dateEnd: 'Today+30' },
			//{text: 'Our Ad Campaign', dateStart: '03/07/08', dateEnd: '07/08/08' }
		],
		//presetRanges: array of objects for each menu preset.
		//Each obj must have text, dateStart, dateEnd. dateStart, dateEnd accept date.js string or a function which returns a date object
		//Extra	presets allDatesBefore: 'All Dates Before',	allDatesAfter: 'All Dates After',
		presets: {
			specificDate: 'Specific Date',			
			dateRange: 'Date Range'
		},
		rangeSplitter:"-",
		maxLength:30,
		onChange:function(){
			//onDateChangeHandler
		} ,
		dateFormat:"mm/dd/yy",
		datepickerOptions: {		
			onChangeMonthYear: function(){
				//onChangeMonthYearHandler
			},
			beforeShow: function(){
				//beforeShowHandler
			},
			beforeShowDay: function(){
				//beforeShowDayHandler
			}						
		}
	}	

	config = $.extend(config,$.fn.extdaterangepicker.defaults);
	
	/**
	 * Send an Ajax request to server to get the date information for a month.
	 * @param urlStr Encrypted URL for retrieving date information per month
	 */
	var refreshCalendar = function(urlstr){
		$.ajaxSetup({global: false});
		if(urlstr){
			$.ajax({
				url: urlstr,
				async: false,
				cache: false,
				timeout: 30000,
				success: function(data){
					if($('#ui-datepicker-div-infos').size() == 0 )
						$("<div id='ui-datepicker-div-infos' style='display:none'/>")
							.insertAfter('#ui-datepicker-div');

					$('#ui-datepicker-div-infos').html(data);
				},
				complete:function(){
					$.ajaxSetup({global: true});
				}
			});
		}		
	}

	/**
	 * Called when year or month is changed to get the date information. The handler signature is defined
	 * by jQuery ui.datepicker.
	 * @param year current selected year
	 * @param month current selected month
	 * @param instance ui.datepicker instance
	 */
	var onChangeMonthYearHandler = function(year, month, instance){
		//console.info("onChangeMonthYearHandler");
		var target = instance.settings.target;
		//var urlStr = target.attr('calUrl');
		var urlStr = instance.settings.options.url
		if(urlStr){
			urlStr = urlStr + "&calStartYear="+year+"&calStartMonth="+month;
			refreshCalendar(urlStr);
		}

		var minMaxDate = getMinMaxDate();
		if(minMaxDate){
			if(minMaxDate.minDate != undefined)
				instance.settings.minDate = minMaxDate.minDate; 
			if(minMaxDate.maxDate != undefined)
				instance.settings.maxDate = minMaxDate.maxDate;
		}
	}

	/**
	 * Called before the date picker is shown to get the date information. The handler signature is defined
	 * by jQuery ui.datepicker.
	 * @param input the javascript object corresponding to the text field  
	 * @param inst ui.datepicker instance
	 */
	var beforeShowHandler = function(input, inst){
		var target = instance.settings.target;
		//var urlStr = target.attr('calUrl');
		var urlStr = instance.settings.options.url
		
		urlStr = urlStr + "&calUserDate=" +  $(input).val();
		refreshCalendar(urlStr);
		var opts = getMinMaxDate(); 
		return opts;
	}

	/**	 
	 * Called before showing each cell in the calendar. The handler signature is defined
	 * by jQuery ui.datepicker.
	 * @param date date object for a specific cell in the calendar
	 */
	var beforeShowDayHandler = function(date){		
		if(config.url === ""){
			return [ true, '' ]; //No url in config means calendar is not ajaxified.
		}
		
		if($('#ui-datepicker-div-infos').length>0){
			var display = $('#ui-datepicker-div-infos').find('ul[name="calInfo"]').children( 'li[name="display"]' ).html();
			if( display == "default" )  	// default mode
			{
				return [ true, '' ];
			} 
			else { 							// not default mode
				var isSelectableStr = $('#ui-datepicker-div-infos')
									.find('ul[name="daysInfo"]')
									.children('li[date="' + date.getDate() + '"]').html();

				var isSelectable = 	(isSelectableStr == "true");
				return [ isSelectable , '' ];															
			}
		}else{
			return [ true, '' ];
		}		
	};
	
	/**
	 * This handler function sets the dependent from and to date input box
	 * if passed as a configuration, server action do not support to retrieve the from and 
	 * to dates values from date range picker input box	 
	 */
	var onDateChangeHandler = function(){
		var $datePickerBox = this.target || $(this);
		var dateText = $datePickerBox.val();
		if(dateText){
			if(dateText !=""){
				var $startDateBox =  this.startDateBox;
				var $endDateBox = this.endDateBox ;		
				var dates = dateText.split(config.rangeSplitter);
				if(dates.length == 1){		
					var dte = dates[0];
					dte = $.trim(dte);
					$startDateBox.val(dte);
					if($endDateBox.length>0){
						$endDateBox.val(dte);
					}
				}else{
					var dte1 = dates[0];
					var dte2 = dates[1];
					dte1 = $.trim(dte1);
					dte2 = $.trim(dte2);
					$startDateBox.val(dte1);
					$endDateBox.val(dte2);
				}
			}
		}
	}
	
	/**
	 * Get the earliest and latest date in a month from the response from server
	 */
	var getMinMaxDate = function(){	
		var data = $('#ui-datepicker-div-infos').html();
		if(data == null)
			return undefined;

		var year = $(data).children('ul[name="calInfo"]').children('li[name="year"]').html();
		var month = $(data).children('ul[name="calInfo"]').children('li[name="month"]').html();
		var today = $(data).children('ul[name="calInfo"]').children('li[name="today"]').html();
		var serverDate =  $(data).children('ul[name="calInfo"]').children('li[name="serverDate"]').html();
		var minDate = $(data).children('ul[name="calInfo"]').children('li[name="minDate"]').html();
		var maxDate = $(data).children('ul[name="calInfo"]').children('li[name="maxDate"]').html();
		var options = {};		
		var removeLink = $(data).children('ul[name="calInfo"]').children('li[name="removeLink"]').html();

		if(removeLink == 'previous')
			options.minDate = new Date(parseInt(year), parseInt(month) - 1, 1);

		if(removeLink == 'next')
			options.maxDate = new Date(parseInt(year), parseInt(month), 0);

		var sDate = new Date(serverDate);
		
		if(minDate){
			var tmpMinDate = new Date(sDate);
			options.minDate = getAdjustedDate(tmpMinDate,minDate,true);
		}
		
		if(maxDate){
			var tmpMaxDate = new Date(sDate);
			options.maxDate = getAdjustedDate(tmpMaxDate,maxDate,false);
		}
		return options;
	}

	var getAdjustedDate = function(aDate,dateSettings,adjustToPriorDate){		
		var mode,modeVal;
		mode = dateSettings.charAt(dateSettings.length-1);
		mode = mode.toUpperCase();
		modeVal = dateSettings.substring(0,dateSettings.length-1);
		modeVal = Number(modeVal);

		switch(mode){
			case "D":
					if(adjustToPriorDate){
						aDate.setDate(aDate.getDate()-modeVal);
					}else{
						aDate.setDate(aDate.getDate()+ modeVal);
					}
					break;

			case "W":
					if(adjustToPriorDate){
						aDate.setDate(aDate.getDate()-modeVal*7);
					}else{
						aDate.setDate(aDate.getDate()+ modeVal*7);
					}
					break;
			case "M":
					if(adjustToPriorDate){
						aDate.setMonth(aDate.getMonth() - modeVal);
					}else{
						aDate.setMonth(aDate.getMonth() + modeVal);
					}
					break;
			case "Y":
					if(adjustToPriorDate){
						aDate.setFullYear(aDate.getFullYear() - modeVal);
					}else{
						aDate.setFullYear(aDate.getFullYear() + modeVal);
					}
		}
		return aDate;
	}
	
	/**
	* Initializes the configuration object
	*/
	var initConfig = function(options){
		var initConfig = {
			onChange:onDateChangeHandler ,
			datepickerOptions: {
				dateFormat: "mm/dd/yy",
				onChangeMonthYear: onChangeMonthYearHandler,
				beforeShow: beforeShowHandler,
				beforeShowDay: beforeShowDayHandler						
			}
		}
		if(options){			
			//Extend all properties except presets			
			config = $.extend(config,initConfig,options);
			/*if(options.presetRanges === undefined){
				config.presetRanges = [];
			}*/
		}
	}

	/**
	* If from and to dates are provides add it to ex-daterangepicker
	*/
	var setInitialDate = function($dateRangeBox){
		var $startDate = config.startDateBox;
		var $endDate = config.endDateBox;
		var startDate = $startDate.val() || "";
		var endDate = $endDate.val() || "";
		
		if(startDate != "" && endDate != ""){
			$dateRangeBox.val(startDate + " " + config.rangeSplitter + " " + endDate);
		}
	}
	
	var blurEventHandler = function(){
		var $startDate = config.startDateBox;
		var $endDate = config.endDateBox;
		
		var inputDate = element.val();		
		var inputs = inputDate.split(config.rangeSplitter);
		if(inputs.length>1){
			$startDate.val(inputs[0]);
			$endDate.val(inputs[1]);
		}else{
			$startDate.val(inputs[0]);
			$endDate.val(inputs[0]);
		}
	}
	
	var _keyDownHandler = function(event){
		// Allow: backspace, delete, tab, escape, enter and .
		if ( $.inArray(event.keyCode,[46,8,9,27,13,109,189,190,191]) !== -1 ||
			 // Allow: Ctrl+A
			(event.keyCode == 65 && event.ctrlKey === true) || 
			 // Allow: home, end, left, right
			(event.keyCode >= 35 && event.keyCode <= 39)) {
				 // let it happen, don't do anything
				 return;
		}
		else {
			// Ensure that it is a number and stop the keypress
			if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	}

	var registerEvents = function(){
		element.on("blur",blurEventHandler);	
		element.on("keydown",_keyDownHandler);
	}
	
	/**
	* initailizes the widget state
	*/
	var initWidget = function($dateRangeBox){
		$dateRangeBox.addClass(config.cssClass);
		$dateRangeBox.attr("maxlength",config.maxLength);
		setInitialDate($dateRangeBox);
		registerEvents();
	}
	
	/**
	* initailizes the configuration
	*/
	var init = function(element,options){
		initConfig(options);
		initWidget(element);
	}	
		
	init(element,options);				
	element.daterangepicker(config);		
	return this; //to maintain chainability we should return this object as it it
	};	
})(jQuery);

//All preset range defaults are defined over here
if($.fn.extdaterangepicker){
	$.fn.extdaterangepicker.defaults = {
			presetRanges : [
				{key:"TODAY",text: 'Today', dateStart: 'today', dateEnd: 'today' },
				{key:"YESTERDAY",text: 'Yesterday', dateStart: 'today-1', dateEnd: 'today-1' },
				{key:"CURRENT_WEEK",text: 'Current Week', dateStart: function(){return Date.today().moveToDayOfWeek(0,-1).addDays(1)}, dateEnd: function(){return Date.today().moveToDayOfWeek(0,-1).addDays(5)} },
				{key:"CURRENT_MONTH",text: 'Current Month', dateStart: function(){return Date.parse('today').moveToFirstDayOfMonth();}, dateEnd: function(){return Date.parse('today').moveToLastDayOfMonth();}},
				{key:"NEXT_WEEK",text: 'Next Week', dateStart: function(){return Date.parse('today').moveToDayOfWeek(1,1)}, dateEnd: function(){return Date.parse('today').moveToDayOfWeek(1,1).addDays(4)} },
				{key:"NEXT_MONTH",text: 'Next Month', dateStart: function(){return Date.parse('today').moveToMonth(Date.parse('today').getMonth()+1).moveToFirstDayOfMonth(); }, dateEnd: function(){return Date.parse('today').moveToMonth(Date.parse('today').getMonth()+1).moveToLastDayOfMonth(); } },
				{key:"NEXT_30_DAYS",text: 'Next 30 Days', dateStart: 'today', dateEnd: 'today+30days' },
				{key:"LAST_WEEK",text: 'Last Week', dateStart:function(){return Date.today().addWeeks(-1).moveToDayOfWeek(0,-1).addDays(1)}, dateEnd: function(){return Date.today().addWeeks(-1).moveToDayOfWeek(0,-1).addDays(5)} },
				{key:"LAST_7_DAYS",text: 'Last 7 Days', dateStart: 'today-6days', dateEnd: 'today' },
				{key:"LAST_30_DAYS",text: 'Last 30 Days', dateStart: 'today-29days', dateEnd: 'today' },
				{key:"LAST_60_DAYS",text: 'Last 60 Days', dateStart: 'today-59days', dateEnd: 'today' },
				{key:"LAST_90_DAYS",text: 'Last 90 Days', dateStart: 'today-89days', dateEnd: 'today' },
				{key:"MONTH_TO_DATE",text: 'Month to date', dateStart: function(){ return Date.parse('today').moveToFirstDayOfMonth();  }, dateEnd: 'today' },
				{key:"LAST_MONTH",text: 'Last Month', dateStart: function(){ return Date.parse('1 month ago').moveToFirstDayOfMonth();  }, dateEnd: function(){ return Date.parse('1 month ago').moveToLastDayOfMonth();  } },
				{key:"PLUS_MINUS_30_DAYS",text: '+/- 30Days', dateStart: 'today-30days', dateEnd: 'today+30days' }
		],
		dateFormat:"mm/dd/yy"
	};

	/** This function initializes the RANGE object which defines the set of available ranges */
	$.fn.extdaterangepicker.initRanges = function(){
		var ranges = $.fn.extdaterangepicker.defaults.presetRanges;
		$.fn.extdaterangepicker.defaults.RANGES = {};
		for(var i=0;i<ranges.length;i++){
			var key = ranges[i].key;
			var object = ranges[i];
			$.fn.extdaterangepicker.defaults.RANGES[key] = object; 	
		}
	}
	
	/** This function extends the defaults to the values passed as option */
	$.fn.extdaterangepicker.setDefaults = function(options){
		$.extend(true, $.fn.extdaterangepicker.defaults, options);
		$.fn.extdaterangepicker.initRanges();
	};
	
	$.fn.extdaterangepicker.initRanges();	
}