/**
 *
 */
$.extend({
	getUISettings:function(){
		var isInitialized = $(document).data("initalized");
		if(!isInitialized){
			$(document).uisettings();
			$(document).data("initalized", true);
		} 
		return $(document).uisettings('settings');
	}
});

(function($){
	$.widget('ux.uisettings', {
		version: "1.0.1",
		_source:"/cb/pages/utils/GetCSILUISetting.action",
		_uiSettings:{},
		_initialized:false,
		
		_create: function() {
			var $this = this;
			if(!this.initialized){
				$.ajax({
					url:$this._source,
					method:"GET",	
					async:false,
					success:function(data, textStatus, jqXHR){
						if(data.response){
							$this._uiSettings = data.response.settings;
							//If settings are not available, then assign default value
							if(jQuery.isEmptyObject($this._uiSettings)){
								$this._uiSettings = { "AutoRefreshPanelsInterval":"300000", 
														 "AjaxRequestTimeout":"300000",
														 "AjaxSpinningWheelTimeout":"300000"};
							}
							
							/*//Override Jquery AJAX functionality to set our handler to be called on different events.
							registerGlobalAjaxHandler();
							ajaxSetup();
							
							//Set Auto refresh Interval for message panel
							setInterval("ns.common.refreshPanels()",ns.common.UISettings.AutoRefreshPanelsInterval);*/
						}
					}
				});
			}
			this.initialized = true;
		},
		
		settings:function (){
			return jQuery.extend(true, {}, this._uiSettings);
		}
		
	});
})(jQuery);