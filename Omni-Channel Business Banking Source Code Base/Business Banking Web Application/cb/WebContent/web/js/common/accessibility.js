/*
* This js file adds accessibility requirements to applications
* 1. moves to next section on alt+(up/down arrows)
* 2. shows help in module specific and global help depending on active section of page on (alt+6)
* 3. provides accesskey for home page (alt+m)
* 
*
*
*
* Please not that some of the functions will not be required in future once we upgrade jquery ui to 1.10 and jquery 1.9
* and higher(remove bindDialog and unbindDialog events after upgradation)
*
* Note: In IE8 there is problem with setFoucs since even though the 'setFocus' function is called IE8 failed to provide the focus
* To fix this issue setTimeout was added but there is price of CPU utilization and removed, IE 9 and IE 10 works fine
*/

var accessibility = (function($){
	var _isInitilised = false;	
	var config = {
		"sections":["notifications","tools","dashboard","grids"],
		"enterSelector":"a,a.ui-button,img.ui-datepicker-trigger,input[type=button],button,input[type=submit]",
		"uiStateFocus":"ui-state-focus",
		"submitButtonSelector":"[buttontype=submit]",
		"homeKeySelector":"#ffiHomeID",
		"accessKeys":{
			"home":"1",
			"notifications":"o" /* n not working in IE and t not working in ff*/, 
			"tools":"p",
			"dashboard":"d",
			"details":"i",
			"grids":"g",
			"next":"n",
			"cancel":"c",
			"submit":"s",
			"add":"a"
		}
	}	
	
	var keyCodes = {
		LEFT_ARROW:37,
		UP_ARROW:38,
		RIGHT_ARROW:39,
		DOWN_ARROW:40,
		TWO:50,
		TWO_NUMPAD:98,
		SIX:54,
		SIX_NUMPAD:102,
		FOUR:52,
		FOUR_NUMPAD:100,
		ZERO:48,
		ZERO_NUMPAD:96
	}

	var _browser = "";
	var _$dialogEnvoker = "";

	var getKeyCode = function(hotKey){
		hotKey = hotKey.toUpperCase();
		var keyCode = hotKey.charCodeAt(0);	
		return keyCode;
	} 
	
	var _init = function(aConfig){
		if(aConfig){
			$.extend(config,aConfig);
		}		
		_registerEvents();
		_accessifyMenu();

		
		if($.browserDetect){				
			_browser = $.browserDetect.browser;
		}
		if(!_isInitilised){
			$.fn.setInitialFocus = function(arg){
				//console.info("new jquery function setInitialFocus");	
				_setInitialFocus(this,arg);
			}
			
			$.fn.setFocus = function(){
				//console.info("new jquery function setInitialFocus");	
				_setFocus(this);
			}
			
			$.fn.setFocusOnTab = function(){
				//console.info("new jquery function setInitialFocus");	
				_setFocusOnTab(this);
			}
		}

		var anElement = $(config.homeKeySelector); // Keep the focus on home menu after the page loads
		setFocusOnTimeout(anElement,1500);
		_isInitilised = true;				
	}


	var setFocusOnTimeout = function(anElement,aTimeout){
		var timeOut = (aTimeout) ? aTimeout :1000;
		if(_browser === "Explorer"){
			timeOut = aTimeout + 500; // IE is bit slow can't do anything for it now.
		}

		setTimeout(function(){
			_resetFocusState();	
			anElement.focus();	
		},timeOut);		
	}
	
	var _setInitialFocus = function(container,ele){
		var timeOut = 500;
		if(_browser === "Explorer"){
			timeOut = 1000; // IE is bit slow can't do anything for it now.
		}

		//This is trick made to autofocus on dialog, this keeps the initial focus on dialog
		/*	
		setTimeout(function(){
			$(container).find("input:visible:first").focus();
		},timeOut);*/		
		
		if(container){
			_resetFocusState();	
			$(container).find("input:visible:first").focus();		
		}		
	}
	
	var _setFocus = function (container){
		_resetFocusState();	
		$(container).find(":tabbable:first").focus();
	}
	
	var _setInitialFocusOnDashboard = function(container){
		_resetFocusState();	
		$(container).find(":tabbable:first").focus();
	}

	var _focusQuickShortCutMenu = function(){
		$("#shortcutSearchInput").focus();
	}

	/**
	* Toggles quick search form,
	* checks for div#quicksearchcriteria if not found checks for $("div[data-banking-container-type=search]");
	* NOTE: A modules quick search criteria will be accessible if container has following custom attribute
	* data-banking-container-type=search, and Identifier based selection #quicksearchcriteria for search container
	* will not be supported in future
	*/
	var _toggleQuickSearch = function(container){	
		var $quickSearchCriteria;
		$quickSearchCriteria = $('#quicksearchcriteria');
		if($quickSearchCriteria.length == 0){
			$quickSearchCriteria = $("div[data-banking-container-type=search]");
		}	
		$quickSearchCriteria.slideToggle();
		_resetFocusState();	
		$("div.quickSearchAreaCls").find(":tabbable:first").focus();
	}

	var _setFocusAfterPageLoad = function(){
		var anElement = $(config.homeKeySelector);
		setFocusOnTimeout(anElement,1500);
	}
	
	var _setFocusOnDashboard = function(){
		var appdashboard = $("#appdashboard");
		var details = $("#details");
		var summary = $("#summary");
		var container = $("#desktop"); // default focusable container
		var notes = $("#notes");

		if(appdashboard.is(":visible")){
			container = appdashboard;
		}else if(details.is(":visible")){
			container = details;
		}else if(summary.is(":visible")){
			container = summary;
		}else if(!container.is(":visible")){
			container = notes;
		}

		$(container).setFocus();
	}

	var _setFocusOnTab = function(container){
		var selectedTab = $(container).find("ul li.ui-state-active").attr("aria-controls");
		$(selectedTab).setFocus();
	}
	
	/**
	* @deprecated With jQuery upgrade we have removed support for jumped navigation which keeps hopping
	* focus from section to section on ALT+UP/DOWN arrow keys
	* Since after Struts-jQuery upgrade widgets like tabs and menus are handling accessibility 
	* on this combination of keys
	*/
	var _setFocusOnNextElement = function(currentTabIndex,direction){
		var elementFound = false;	
		var previousTabIndex = 	currentTabIndex;
		var ui_state_focus_class = "ui-state-focus";
		while(!elementFound){
			currentTabIndex = ns.common.getNextTabIndex(currentTabIndex,direction);	
			if(currentTabIndex){
				if($("[tabindex=" + currentTabIndex + "]").length > 0 && $("[tabindex=" + currentTabIndex + "]").is(":visible")){
					$("[tabindex=" + currentTabIndex + "]").focus();	
					elementFound = true;
					$("[tabindex=" + previousTabIndex + "]").removeClass(ui_state_focus_class);
					var parent = $("[tabindex=" + previousTabIndex + "]").parent();
					if(parent.is("li")){
						parent.removeClass(ui_state_focus_class);		
					}
				}
				if(currentTabIndex === -1 || currentTabIndex === 40000){
					//console.info("reached to boundaries terminate the loop");
					elementFound = true;// end this setfocus function on reaching boundaries
				}	
			}
		}
	}
	
	var _accessifyMenu = function(){
		//console.info("_accessifyMenu");
		/*
		var menus = $("#tabbuttons li a");
		var accessKey;
		var menu="";
		for(var i=0;i<menus.length;i++){		
			accessKey = i+1;
			menu =  menus[i];
			menu.accessKey = accessKey;
		}
		*/
		$("#tabbuttons li a[accesskeytype=home]").attr("accessKey",config.accessKeys.home);		
	};
	
	var _openHelp = function(currentTabIndex){
		var activeSection = ns.common.getActiveSection(currentTabIndex);
		var helpSelector = "";
		if(activeSection){
			//call section specific help			
			switch (activeSection){
				case "topMenu":
						helpSelector = $("#extension-menu a[title=Help]");
						break;							
				case "mainMenu":
						helpSelector = $("#extension-menu a[title=Help]");
						break;	
				case "messages":
						helpSelector = $("#notificationPanes div.pane-content:visible").siblings().find("span.ui-icon-help");
						break;
				case "alerts":
						helpSelector = $("#notificationPanes div.pane-content:visible").siblings().find("span.ui-icon-help");
						break;
				case "approvals":
						helpSelector = $("#notificationPanes div.pane-content:visible").siblings().find("span.ui-icon-help");
						break;
				case "shortcuts":
						helpSelector = $("#toolsPanesID div.pane-content:visible").siblings().find("span.ui-icon-help");
						break;									
				case "themePicker":
						helpSelector = $("#toolsPanesID div.pane-content:visible").siblings().find("span.ui-icon-help");
						break;	
				case "forexConverter":
						helpSelector = $("#toolsPanesID div.pane-content:visible").siblings().find("span.ui-icon-help");
						break;																					
				case "extensionMenu":
						helpSelector = $("#extension-menu a[title=Help]");
						break;									
				case "dashboard":
						helpSelector = $("#extension-menu a[title=Help]");
						break;
				case "details":
						helpSelector = $("#details span.ui-icon-help:eq(0)");
						break;									
				case "summary":
						helpSelector = $("#summary span.ui-icon-help:eq(0)");
						break;
				case "dialog":
						helpSelector = $("div.ui-dialog:visible span.ui-icon-help");
						break;				
			}
		}else{
			helpSelector = $("#extension-menu a[title=Help]");//Global help file
		}
		
		//$(helpSelector).click();
		$(".portlet:visible span.icon-sys-help-2").parent().click();
	};

	var _accessify = function(container){
		//var formId= container.attr("id");
		//$("#" + formId + " a[accesskeytype=cancel]").attr("accessKey",config.accessKeys.cancel);
		//$("#" + formId + " a[accesskeytype=submit]").attr("accessKey",config.accessKeys.submit);
		_setInitialFocus(container);
	};
	
	var _dialogKeyDownHandler = function(e){
		if (e.keyCode == 9) {
			if(window.focusFlag == false){
				if(e.shiftKey){
					var $last = $('.ui-dialog :visible:tabbable:last');
					$last.focus(1);				
				}else{
					var $first = $('.ui-dialog :visible:tabbable:first');
					$first.focus(1);				
				}
				window.focusFlag = true;
			}
		}
	};
		
	/**
	* This functions holds tab within dialog only
	*/
	
	var _dialogKeyPressHandler = function(e){
		//alert("_dialogKeyPressHandler");
		var $first = $(".ui-dialog :visible:tabbable:first");
		var $last = $(".ui-dialog :visible:tabbable:last");
		//console.info("live:dialog key press");
		if (e.keyCode == $.ui.keyCode.TAB) {			
			setTimeout(function() { 
				$first.focus(); 
				//alert("last element key press");
			}, 1);			
		}
	}
	
	var _dialogFirstElementKeyPressHandler = function(e){
		//console.info("_dialogFirstElementKeyPressHandler");
		var $last = $(".ui-dialog :visible:tabbable:last");
		if (e.keyCode == 9 && e.shiftKey) {			
			setTimeout(function() { 
				$last.focus(); 						
			}, 1);	
			
			if(e.shiftKey){
				//e.stopPropagation();
				//return false;

				if (!e) var e = window.event;
				e.cancelBubble = true;
				if (e.stopPropagation) e.stopPropagation();
			}
		}	
	}
	
	
	var _dialogLastElementKeyPressHandler = function(e){
		//console.info("_dialogLastElementKeyPressHandler");
		var $first = $(".ui-dialog :visible:tabbable:first");
		if (e.keyCode == 9 && !e.shiftKey) {			
				setTimeout(function() { 
					$first.focus(); 						
				}, 1);

				//e.stopPropagation();
				//return false;											

				if (!e) var e = window.event;
				e.cancelBubble = true;
				if (e.stopPropagation) e.stopPropagation();				
		}
	}
	
	
	
	/**
	* This functions bids the keydown event for dialog, this will not be required once we upgrade to latest jquery ui
	* this functions fixes the focus issue with modal dialog
	* @deprecated With jQuery upgrade this function is not required any more jQuery UI dialog handle the focus on its own
	*/
	var _bindDialogEvents = function(){
		//$(document).bind("keydown.dialogKeyDownHandler",_dialogKeyDownHandler);	
		$(".ui-dialog :visible:tabbable:first").bind("keydown.dialogFirstElementKeyPressHandler",_dialogFirstElementKeyPressHandler);
		$(".ui-dialog :visible:tabbable:last").bind("keydown.dialogLastElementKeyPressHandler",_dialogLastElementKeyPressHandler);

		var timeOut = 500;
		if(_browser === "Explorer"){
			timeOut = 1000; // IE is bit slow can't do anything for it now.
		}

		//This is trick made to autofocus on dialog, this keeps the initial focus on dialog
		setTimeout(function(){
			var anElement = $(".ui-dialog-content:visible :tabbable:first");
			if(anElement.length === 0){
				anElement = $(".ui-dialog:visible :tabbable:first");
			}
			anElement.focus();
		},timeOut);
	};
	
	/**
	* @deprecated With jQuery upgrade this function is not required any more jQuery UI dialog handle the focus on its own
	*/
	var _unBindDialogEvents = function(){
		//$(document).unbind("keydown.dialogKeyDownHandler");
		$(".ui-dialog :visible:tabbable:first").unbind("keydown.dialogFirstElementKeyPressHandler");
		$(".ui-dialog :visible:tabbable:last").unbind("keydown.dialogLastElementKeyPressHandler");
		//$(_dialogEnvoker).focus();
	};
	
	/**
	* This function handles enter key on document and triggers its click event
	*/
	var _registerGlobalEnterEvent = function(){
	
		$(document).on("keydown",config.enterSelector,function(e) {
			if(e.which === 13) {
				$(e.currentTarget).trigger('click');
				e.stopPropagation();
				return false;					
			}
		});

		/*
		//active form submission in case enter on form is pressed
		$(document).keypress(function(e){	
			if(e.which === 13){
				//console.info("_registerGlobalEnterEvent for doc");	
				$(e.currentTarget).find(config.submitButtonSelector).trigger('click');
			}				
		});*/
	}


	/**
	* This function handles enter key on form
	*/ 
	var _registerGlobalFormEnterEvent = function(){
		$("body").delegate("form", "keypress", function(e) {
		//$('form').live("keypress" , function(e) {
			if(e.which === 13) {
				var isTextArea;
				//Excluding text area from enter event
				if(_browser === "Explorer"){
					isTextArea = $(e.originalEvent.srcElement).is("textarea");
				}else{
					isTextArea = $(e.originalEvent.target).is("textarea");
				}
				if(!isTextArea){
					return false;
				}				
			}
		});
	}

	/**
	* Disable the context menu
	*/
	var _disableContextMenu = function(){
		$(document).on("contextmenu",function(e){
		    return false;
		});
	}

	/*
	* This will work as fix for calendar issue of remaning open when focus goes out of calendar
	*/
	var _registerCalendarFocusoutEvent = function(){
		$("body").delegate("input.hasDatepicker", "focusout", function() {
		  	$("#ui-datepicker-div").hide();	
		});	
	}

	var _registerAccessKeyEvents = function(){
		$(document).keyup(function(e){	
			var elementWithFocus = document.activeElement;		
			var currentTabIndex = $(elementWithFocus).attr("tabindex") || 0;
			var homeHotKeyCode = getKeyCode(config.accessKeys.home);
			var newTabIndex = 0;
			if(e.altKey){
				if(e.keyCode === keyCodes.FOUR || e.keyCode === keyCodes.FOUR_NUMPAD){//search container ie. dashboard
					var dashboard = $("#appdashboard");
					$("#appdashboard").tabify({baseIndex:ns.common.getSectionTabIndex('dashboard')})
					//_setInitialFocusOnDashboard(dashboard);
					_toggleQuickSearch(dashboard);
				}else if(e.keyCode === keyCodes.LEFT_ARROW){
				}else if(e.keyCode === keyCodes.UP_ARROW){
					//_setFocusOnNextElement(currentTabIndex,false);//Removed support after struts-jquery upgrade
				}else if(e.keyCode === keyCodes.RIGHT_ARROW){
				}else if(e.keyCode === keyCodes.DOWN_ARROW){	
					//_setFocusOnNextElement(currentTabIndex,true);//Removed support after struts-jquery upgrade				
				}else if(e.keyCode === homeHotKeyCode || e.keyCode === 97){	
					//if(_browser === "Explorer" || _browser ==="Firefox" || _browser === "Safari"){}
					_resetFocusState();
					$("#ffiHomeID").click();
					$("#ffiHomeID").focus();
				}else if(e.keyCode === keyCodes.TWO || e.keyCode === keyCodes.TWO_NUMPAD){
					_focusQuickShortCutMenu(currentTabIndex);
				}else if(e.keyCode === keyCodes.SIX){
					_openHelp(currentTabIndex);
				}else if(e.keyCode === keyCodes.ZERO || e.keyCode === keyCodes.ZERO_NUMPAD ){
					if ($("#accessKeysViewDialogID").dialog("isOpen")!==true){
						ns.common.openDialog('accessKeysViewDialog');
					}
				}
			}	
		});
	}

	/*
	* This functions provides all accesskeys to page
	* not used now but can be used later on implementing all accessibility features
	*/	
	var _registerAccessKeyEventsOld = function(){
		//console.info("_registerAccessKeyEvents");		
		var dashBoardHotKeyCode = getKeyCode(config.accessKeys.dashboard);
		var initiateHotKeyCode = getKeyCode(config.accessKeys.details);
		var gridsHotKeyCode = getKeyCode(config.accessKeys.grids);
		var toolsHotKeyCode = getKeyCode(config.accessKeys.tools);
		var notificationHotKeyCode = getKeyCode(config.accessKeys.notifications);
		
		$(document).keyup(function(e){
			if(e.altKey && e.shiftKey){
				if(e.keyCode === dashBoardHotKeyCode){//hotkey 'd':68
					$("#appdashboard a:visible:eq(0)").focus();
				}

				if(e.keyCode === initiateHotKeyCode){//hotkey 'i'
					var  $container = $("#details");
					_setInitialFocus($container);
				}
				
				if(e.keyCode === gridsHotKeyCode){//hotkey 's':83
					$("#summary a:visible:eq(0)").focus();
				}
								
				if(e.keyCode === toolsHotKeyCode){
					$("#toolsPanesID a:visible:eq(0)").focus();
				}
				
				if(e.keyCode === notificationHotKeyCode){
					$("#notificationPanes a:visible:eq(0)").focus();;
				}
				
				//Keep this for IE
				if(_browser === "Explorer"){				
					if(e.keyCode === getKeyCode(config.accessKeys.cancel)){						
						$("#details a[accesskeytype=cancel]").trigger("click");					
					}					
					if(e.keyCode === getKeyCode(config.accessKeys.submit)){
						$("#details a[accesskeytype=submit]").trigger("click");					
					}
				}				
			}
		});
	}
		
	var _registerEvents = function(){
		_registerAccessKeyEvents();
		_registerGlobalEnterEvent();
		_registerGlobalFormEnterEvent();
		_disableContextMenu();
	}



	var _deactivateLinks = function(deactivate){
		//console.info("accessibility : " + deactivate);
		//deactivate the links only if it is modal dialog, since we want focus on dialog and restrict the user for navigating away the dailog
		var dialogId = $(".ui-dialog-content:visible").attr("id");
		var isModal = false;
		if(dialogId){
			isModal = $("#" + dialogId).dialog( "option", "modal" );
		}

		if(deactivate === true && isModal === true){
			$("[tabindex]").attr("tabIndex","-1");	
			$("[href='#']").attr("tabIndex","-1");	
			$("[href='javascript:void(0)']").attr("tabIndex","-1");

			//restore href to tmpHref
			$("[href='javascript:void(0)']").attr("tmpHref","javascript:void(0)").removeAttr("href");
			$("[href='#']").attr("tmpHref","#").removeAttr("href");

			//exclude dialog content links since links on dialog content die after above change
			$(".ui-dialog-content:visible a[tmpHref=#]").attr("href","#");
			$(".ui-dialog-content:visible a[tmpHref='javascript:void(0)']").attr("href","javascript:void(0)");
		}else{
			$("a[tmpHref='#']").attr("href","#").removeAttr("tmpHref");
			$("a[tmpHref='javascript:void(0)']").attr("href","javascript:void(0)").removeAttr("tmpHref");
		}
	}

	/*
	* This function set focus back on dialog caller element
	*/
	var _setFocusOnDialogInvoker = function(){
		var timeOut = 500;
		if(_browser === "Explorer"){
			timeOut = 1000; // IE is bit slow can't do anything for it now.
		}
		/*
		setTimeout(function(){
			_$dialogEnvoker.focus();

		},timeOut);*/
		
		_$dialogEnvoker.focus();
	}
	
	var _focusToErrorField = function($aForm){
		$aForm.find(".ui-state-active").removeClass("ui-state-active");
		var firstErrLbl = $aForm.find("span.errorLabel:eq(0)");
		if(firstErrLbl){
			var errId = firstErrLbl.attr("id");
			var controlId = "#" + errId.charAt(0).toUpperCase() + errId.replace("Error", "").slice(1);
			var $control = $aForm.find("[name*='" + controlId + "']");
			if($control){
				$control.focus();
			}
		}
	}
	
	var _resetFocusState = function(){
		var hoverSelector = "li."+ config.uiStateFocus + ",a." + config.uiStateFocus;
		$(hoverSelector).removeClass(config.uiStateFocus);
	}

	return {
		init:function(aConfig){
			_init(aConfig);
		},
		bindDialogEvents:function(){
			//_bindDialogEvents();
		},
		unbindDialogEvents:function(){
			//_unBindDialogEvents();
		},
		deactivateLinks:function(deactivate){
			_deactivateLinks(deactivate);
		},
		setDialogInvoker:function(anEnvoker){
			_$dialogEnvoker = $(anEnvoker);
		},
		getDialogInvoker:function(){
			return _$dialogEnvoker;
		},
		setFocusOnDialogInvoker:function(){
			_setFocusOnDialogInvoker();
		},
		focusToErrorField:function($aForm){
			_focusToErrorField($aForm);			
		},
		setFocusOnDashboard:function(){
			_setFocusOnDashboard();
		},
		setFocusAfterPageLoad:function(){
			_setFocusAfterPageLoad();
		}
	}
})(jQuery)
