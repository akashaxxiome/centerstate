/****************************************************************
 * This js file includes functions which are extended to
 * jQuery function name space.
 *
 *****************************************************************/

/**
 * This jQuery function constructs JSON object out of given form
 * if any prefix is passed it will prefix all input field names with it
 */
$.fn.serializeForm = function(prefix) {
    var o = {};
    var a = this.serializeArray();
    var TOKEN = "CSRF_TOKEN";
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            if (this.name != TOKEN) {
                var inputName = this.name;
                if (prefix) {
                    inputName = prefix + "." + inputName;
                }
                o[inputName] = this.value || '';
            }
        }
    });
    return o;
};

/**
 * jQuery function to show busy indicator on element
 * TODO: provide a class loading-indicator-throwber which would override styling done by loading-indicator
 */
$.fn.setBusy = function(bBusy) {
    var $element = $(this);
    if (bBusy) {
    	var aConfig = {
			'addClass': 'loading-indicator'
		}
        $element.showLoading(aConfig);
    } else {
       $element.hideLoading();
    }
};

/**
* jQuery function to get script with caching
*/
jQuery.cachedScript = function( url, options ) {
 
  // Allow user to set any option except for dataType, cache, and url
  options = $.extend( options || {}, {
    dataType: "script",
    cache: true,
    url: url
  });
 
  // Use $.ajax() since it is more flexible than $.getScript
  // Return the jqXHR object so we can chain callbacks
  return jQuery.ajax( options );
};

//Gets the UI settings from Server, which will be applied to UI widgets and UI setup
$.getUISettings = function(){
    var dfd = new $.Deferred();
    var bInitialized = $(document).data("data-sap-banking-bUISettingsInitialized"); 
    var oUISettings = $(document).data("data-sap-banking-uisettings");

    if(!bInitialized){
        $.ajax({
            url:"/cb/pages/utils/GetCSILUISetting.action"
        }).done(function(data){
            var _oUISettings;
            if(data && data.response && data.response.settings){
                _oUISettings = data.response.settings;
            }else{
                //Application do not have UI settings
                //Set defaults here
                _oUISettings = {
                    AjaxRequestTimeout: "300000",
                    AjaxSpinningWheelTimeout: "300000",
                    AutoRefreshPanelsInterval: "60000",
                    LookUpBoxMaxItems: "100",
                    LookupBoxSearchClass: "com.ffusion.struts.common.DefaultLookupBoxSearch",
                    LookupBoxSearchType: "SUBSTRING"
                }
            }                   
            $(document).data("data-sap-banking-uisettings",_oUISettings);
            dfd.resolve(_oUISettings);
        }); 
        $(document).data("data-sap-banking-bUISettingsInitialized", true);
    }else if(oUISettings){
        dfd.resolve(oUISettings);
    }
    return dfd.promise();
};