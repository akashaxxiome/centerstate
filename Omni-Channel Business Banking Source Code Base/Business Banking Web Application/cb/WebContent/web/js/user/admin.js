ns.admin.topLavelPermissionTab = '';
ns.admin.secondLavelPermissionTab = '';
ns.admin.currentVisibleDashboard = '';

if(!ns.admin.uiroller){
	ns.admin.uiroller = new ns.common.UIRoller();
	ns.admin.uiroller.addComponent("permissionsDiv");
	ns.admin.uiroller.addComponent("companyTabs", {
		"openCallback": function(compId){
							$('#'+compId).portlet("expand");
						},
		"closeCallback": function(compId){
							$('#'+compId).portlet("fold");
						},
		"isOpenCallback": function(compId){
							return $('#'+compId).portlet("isExpanded");
						}
	});

	ns.admin.uimode1 = new ns.common.UIMode("summary");
	ns.admin.uimode1.addComponent("companyTabs");

	//uimode2("permission") will turn permissionDiv on
	ns.admin.uimode2 = new ns.common.UIMode("permission");
	ns.admin.uimode2.addComponent("permissionsDiv");
	ns.admin.uimode2.setOnComplete(function(){
		ns.admin.uiroller.gotoMode("permission1");
	});

	//uimode3("permission1") will select a first level permission tab
	ns.admin.uimode3 = ns.admin.uimode2.copyTo("permission1");
	ns.admin.uimode3.setOnBefore(function(){
		//click one of the first layer tabs
		if(ns.admin.l1TabId){
			$('#permTabs').find("#"+ns.admin.l1TabId).find('a').click();
		} else {
			$('#permTabs').tabs('option','active', 0);
		}
	});

	ns.admin.uiroller.addMode(ns.admin.uimode1);
	ns.admin.uiroller.addMode(ns.admin.uimode2);
	ns.admin.uiroller.addMode(ns.admin.uimode3);

}

ns.admin.setup = function()
{
	$('#companyDetails').hide();
	$('#details').hide();
	
	$.subscribe('cancelForm', function(event, data)
	{
		ns.admin.closeDetails();
		ns.common.hideStatus();
	});
	
	$.subscribe('cancelUserForm', function(event, data)
	{
		ns.common.selectDashboardItem("usersLink");
		ns.admin.closeUserDetails();
		ns.common.hideStatus();
	});

	$.subscribe('cancelUserEditForm', function(event, data)
	{
		ns.admin.closeUserDetails();
		ns.common.hideStatus();
		ns.admin.reloadPendingUsersAndUsersGrid();
	});

	$.subscribe('cancelGroupForm', function(event, data) {
		ns.admin.closeGroupDetails();
		ns.common.hideStatus();
		ns.common.selectDashboardItem("groupsLink");
	});

	$.subscribe('cancelLocationForm', function(event, data)
	{
		ns.admin.reloadDivisions();
		var isValidLocation = $('#dashBoardDivisionId').val();
		if($.trim(isValidLocation) == "" || isValidLocation == null){
			ns.common.refreshDashboard('showdbDivisionSummary',true);
		}else{
			var buttonType = 'done';
			if(data != undefined){
				buttonType = $('#'+data.id).attr('buttontype');
				if($.trim(buttonType) == ''){
					buttonType = 'done';
				}
			}
			ns.common.showSummaryOnSuccess("summary",buttonType);
			ns.common.selectDashboardItem("locationsLink");
		}
		ns.admin.closeLocationDetails();
		ns.common.hideStatus();
	});
	
	$.subscribe('loadLocationGrid', function(event, data)
	{
		ns.common.refreshDashboard('showdbLocationSummary',true);
	});

	$.subscribe('beforeLoad', function(event, data)
	{
		$('#companyDetails').hide();
			$('#companyDetails #companyContent').html('');
		$('#details').hide();
			$('#details #inputDiv').html('');
			$('#details #verifyDiv').html('');
			$('#details #confirmDiv').html('');
		$('#permissionsDiv').hide();
		$('#profileSummary').hide();
	});
	
	$.subscribe('beforeEditLinkClick', function(event, data)
	{
		$('#summary').hide();
		ns.common.selectDashboardItem("editCompany");
	});
	
	
	$.subscribe('onSummaryLinkClick', function(event, data)
	{
		$('#companyDetails').hide();
		$('#details').hide();
		$('#summary').show();
		$("#permissionsDiv").hide();
	});
	
	$.subscribe('beforeCloneUserAndAccountPermLoad', function(event, data)
	{
		$('#details').hide();
		$('#permissionsDiv').hide();
	});

	$.subscribe('completeCloneUserPermLoad', function(event, data)
	{
		$('#permissionsDiv').show();
		$('#details').hide();
		$("#cloneUserPermissionsButton span.dashboardSelectedItem").removeClass('dashboardSelectedItem');
	});
	
	$.subscribe('cancelCloneUserPermLoad', function(event, data)
	{
		$('#permissionsDiv').show();
		$('#details').hide();
		$("#cloneAccountPermissionsButton span.dashboardSelectedItem").removeClass('dashboardSelectedItem');
	});

	$.subscribe('completeCloneAccountPermLoad', function(event, data)
	{
		$('#permissionsDiv').show();
		$('#details').hide();
		$("#cloneAccountPermissionsButton span.dashboardSelectedItem").removeClass('dashboardSelectedItem');

		if ($('#accountAccessPermissionsDiv').length > 0) {
			var url = "/cb/pages/jsp/user/accountaccess.jsp";
			$.ajax({
				type: "GET",
				url: url,
				success: function(data) {
					$('#Accounts').html(data);
				}
			});
		} else {
			$('#permTabs ul li').each(function (index,element){
				if ($(element).attr('id') == 'acctAccess') {
					$('#permTabs').tabs('option','active', index);
					return false;
				}
			});
		}
	});

	$.subscribe('completeLoad', function(event, data)
	{
		// alert('Form Loaded! ');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		ns.admin.gotoStep(1);
		$('#details').slideDown();
		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#crudTabs").setFocusOnTab();
		}
	});

	$.subscribe('completeUserLoad', function(event, data)
	{
		// alert('Form Loaded! ');
		$('#admin_usersTab').portlet('fold');

		// Fold pending user grid if DA is on and user clicks edit/add user button
		if ($('#admin_usersPendingApprovalTab').length > 0) {
			$('#admin_usersPendingApprovalTab').portlet('fold');
		}

		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		ns.admin.gotoStep(1);
		$('#details').slideDown();

		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#details").setInitialFocus();
		}
		$('#summary').hide();
	});

	$.subscribe('completeGroupLoad', function(event, data)
	{
		// alert('Form Loaded! ');
		$('#admin_groupsTab').portlet('fold');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		ns.admin.gotoStep(1);
		$('#details').slideDown();

		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#details").setInitialFocus();
		}
		$("#summary").hide();
	});

	$.subscribe('completeCompanyProfileLoad', function(event, data)
	{
		$('#companyTabs').portlet('fold');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#companyDetails .portlet-title');
		$title.html(heading);
		$('#companyDetails').slideDown();

		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#companyDetails").setInitialFocus();
		}
	});

	$.subscribe('completeCompanyLoad', function(event, data)
	{
		$('#companyTabs').portlet('fold');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);
		ns.admin.gotoStep(1);
		$('#details').slideDown();

		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#details").setInitialFocus();
		}
		$('#summary').hide();
	});

	$.subscribe('errorLoad', function(event, data)
	{
		// alert('Form error! ');
	});

	$.subscribe('beforeVerify', function(event, data)
	{
		ns.admin.forbiddencompleteVerify = false;
		// alert('About to verify form! ');
		removeValidationErrors();
		// Hide Validation Errors Row
		$('span.errorLabel').parent().parent().attr('style', 'display:none');

	});

	/* Invokes custom validation and shows Validation Errors on separate rows in the Grid */
	ns.admin.showValidationErrors = function (form, errors) {
		customValidation(form, errors);
		$('span.errorLabel').filter(function(index) {
		return $(this).html().length > 0;
		}).parent().parent().attr('style', 'display:visible');
	};

	$.subscribe('beforeVerify2', function(event, data)
	{
		// alert('About to verify form! ');
		removeValidationErrors();
		$('#resultmessage').html('');
		$('#operationresult').hide();
		$('#editAdminerDialogId').dialog('close');
	});
	
	$.subscribe('beforeVerify3', function(event, data)
	{
		// alert('About to verify form! ');
		removeValidationErrors();
		$('#resultmessage').html('');
		$('#operationresult').hide();
		
		if($("#adminList").val()!=null){
			$('#editAdminerDialogId').dialog('close');
		}
	});
	
	$.subscribe('completeVerify', function(event, data)
	{
		// alert('Form Verified! ');
		if(!ns.admin.forbiddencompleteVerify){
 			ns.admin.gotoStep(2);
 		} else {
 			ns.admin.forbiddencompleteVerify = false;
 		}

	});

	$.subscribe('errorVerify', function(event, data)
	{
		ns.admin.forbiddencompleteVerify = true;
		// $.log('Form Verifying error! ');
	});


	$.subscribe('beforeSubmit', function(event, data)
	{
		// alert('About to send edit! ');
	});


	$.subscribe('completeSubmit', function(event, data)
	{
		// alert('sending edit complete! ');
 			ns.admin.gotoStep(3);
	});
	
	$.subscribe('completeAddUserAutoEntileSubmit', function(event, data)
	{
		// alert('sending edit complete! ');
		ns.admin.gotoStep(3);
		ns.admin.refreshUsersGrid();
	});

	$.subscribe('errorSubmit', function(event, data)
	{
	});

	$.subscribe('backToInput', function(event, data)
	{
		// alert('About to go back to input! ');
		ns.admin.gotoStep(1);
	});

	$.subscribe('beforePermLoad', function(event, data)
	{
		$('#permissionsDiv').hide();
	});


	$.subscribe('completePermLoad', function(event, data)
	{
		// alert('Form Loaded! ');
		$('#permissionsDiv').slideDown();
	});

	$.subscribe('completeUserPermLoad', function(event, data)
	{
		// alert('Form Loaded! ');
		$('#admin_usersPendingApprovalTab').portlet('fold');
		$('#admin_usersTab').portlet('fold');
		$('#permissionsDiv').slideDown();
		$('#permTabs').tabs('option','active', 0);
		
		var firstPermissionTabId = $(".gridTabDropdownHolder span.summaryTabDropdownItem").eq(0).prop('id');
		if(firstPermissionTabId != undefined && firstPermissionTabId!= '' ){
			ns.admin.topLavelPermissionTab = firstPermissionTabId;
		}
	});

	$.subscribe('completeGroupPermLoad', function(event, data)
	{
		// alert('Form Loaded! ');
		$('#admin_groupsTab').portlet('fold');
		$('#permissionsDiv').slideDown();
		$('#permTabs').tabs('option','active', 0);
		
		var firstPermissionTabId = $(".gridTabDropdownHolder span.summaryTabDropdownItem").eq(0).prop('id');
		if(firstPermissionTabId != undefined && firstPermissionTabId!= '' ){
			ns.admin.topLavelPermissionTab = firstPermissionTabId;
		}
	});

	$.subscribe('completeDivisionPermLoad', function(event, data)
	{
		// alert('Form Loaded! ');
		$('#admin_divisionsTab').portlet('fold');
		$('#permissionsDiv').slideDown();
		$('#permTabs').tabs('option','active', 0);
		
		var firstPermissionTabId = $(".gridTabDropdownHolder span.summaryTabDropdownItem").eq(0).prop('id');
		if(firstPermissionTabId != undefined && firstPermissionTabId!= '' ){
			ns.admin.topLavelPermissionTab = firstPermissionTabId;
		}
	});

	$.subscribe('completeCompanyPermLoad', function(event, data)
	{
		 //alert('Form Loaded! ');
		//$('#companyTabs').portlet('fold');
		//$('#permissionsDiv').slideDown();
		ns.admin.uiroller.gotoMode("permission");
		
	});

	$.subscribe('cancelPermForm', function(event, data)
	{
		ns.admin.closePermissions();
		ns.common.hideStatus();
		$('#appdashboard .dashboardUiCls .summaryLabelCls').each(function(){
            if(this.id == 'usersLink'){
            	//if (ns.common.isElementVisible("dbUsersSummary")) {
            	if(ns.admin.currentVisibleDashboard == 'USERS') {
            		ns.common.refreshDashboard('dbUsersSummary');
            		ns.common.showSummaryOnSuccess("summary","done");
            		$('#profileSummary').hide();
            		$('#summary').show();
            	} else if(ns.admin.currentVisibleDashboard == 'PROFILES') {
            		//Displaying the grid without refreshing it to retain the last location of editing.
            		//ns.common.refreshDashboard('dbProfilesSummary');
            		//ns.common.showSummaryOnSuccess("profileSummary","done");
            		$('#summary').hide();
            		$('#profileSummary').show();
            		event.stopPropagation();
            		//Added condition for getting back to Edit profile page when viewing permissions from Edit page.
            	}else if(ns.admin.currentVisibleDashboard == 'EDIT_PROFILE') {
            		//Again hiding the permission screen and displaying the edit screen.
            		$('#summary').hide();
            		$('#details').show();
            		//Stopping further event propogation since dont want to execute other events.
            		event.stopPropagation();
            	}
            }else if(this.id == 'divisionsLink'){
            	$.publish('cancelDivisionForm');
            	$('#summary').show();
            }else if(this.id == 'groupsLink'){
				$.publish('cancelGroupForm');
				ns.admin.reloadPendingGroupsGrid();
				$('#summary').show();
            }
		});
		
		if ($('#admin_groupsTab').length > 0) {
			ns.admin.reloadPendingGroupsGrid();
		}
		if ($('#admin_divisionsTab').length > 0) {
			ns.admin.reloadPendingDivisionsGrid();
		}
		if ($('#companySummaryTabs').length > 0) {
			$('#summary').show();
			if(ns.company.isCompanyDualApprovalMode()) {
				ns.company.refreshPendingCompanyGrid();	
			}
			ns.admin.reloadCompanyPermissions();
		}
	});

	$.subscribe('cancelAddUserCloneForm', function(event, data)
	{
		$('#details').slideUp();
		$('#summary').show();
		ns.admin.closePermissions();
		ns.common.hideStatus();
		// Need to refresh while cloning
		ns.common.refreshDashboard('dbUsersSummary');
	});

	$.subscribe('cancelCompanyForm', function(event, data)
	{
		$('#companyTabs').portlet('expand');
		$('#companyTabs').show();
		$('#details').slideUp();
		ns.common.hideStatus();
	});
	
	$.subscribe('submitPendingApprovalForCompany', function(event, data)
	{
		$('#companyDetails').hide();
		$('#companyTabs').show();
		ns.common.hideStatus();
		refreshDashboardWithSelectionHistory();
	});
	
	ns.company.isCompanyDualApprovalMode = function() {
		return ns.admin.dualApprovalMode == "TRUE" || $('#pendingChangesID').length > 0;
	};

	$.subscribe('completeBAIExportSettingsSubmit', function(event, data)
	{
		ns.admin.gotoStep(3);
		if(ns.company.isCompanyDualApprovalMode()) {
			ns.company.refreshPendingCompanyGrid();	
		}
	});

	$.subscribe('refreshBAIExportSettings', function(event, data)
	{
		ns.common.refreshDashboard('showdbAdminCompanyBAISummary',true);
		$("#summary").show();
	});

	$.subscribe('completeCompanyProfileSubmit', function(event, data)
	{
		ns.common.refreshDashboard('showdbAdminCompanyProfileSummary',true);
		/*if( ns.company.isCompanyDualApprovalMode()) {
			ns.common.refreshDashboard('showdbAdminCompanyProfileSummary',true);
		}else{
			ns.common.selectDashboardItem("profile");
			ns.admin.refreshCompanyModule();
			$('#companyTabs').portlet('expand');
			$('#companyDetails').slideUp();
			ns.common.hideStatus();
			$("#summary").show();
		}*/
	});

	$.subscribe('completeAdministratorsSubmit', function(event, data)
	{
		ns.admin.gotoStep(3);
	});

	$.subscribe('completeAdministratorsSubmit2', function(event, data)
 	{
 		ns.admin.gotoStep(1);
 	});

	$.subscribe('beforeLocLoad', function(event, data)
	{
		var isValidLocation = $('#dashBoardDivisionId').val();
		if($.trim(isValidLocation) == "" || isValidLocation == null){
			event.stopPropagation();
			$('#showdbDivisionSummary').trigger('click');
		}
		$('#details').hide();
		$('#summary').show();
		$("#permissionsDiv").hide();
		//$('#admin_divisionsTab').tabs('enable', 1);

	});

	$.subscribe('completeLocLoad', function(event, data)
	{
		//$('#admin_divisionsTab').tabs('option','active', 1);
	});

	$.subscribe('completedTGAdd', function(event, data)
	{

	});

	$.subscribe('successTGAdd', function(event, data)
	{
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);
		ns.admin.closeCompanyDetails();
		if($('#adminCompanyPendingApprovalLink').length > 0) {
  			ns.admin.refreshGrid('#pendingChangesID');
  		}
	});

	$.subscribe('errorTGAdd', function(event,data){
		ns.common.showError();
	});

	$.subscribe('completeTGDel', function(event,data) {
		ns.common.closeDialog('#deleteTransactionGroupDialog');
		ns.admin.refreshTG();
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);
		if ($('#adminCompanyPendingApprovalLink').length > 0) {
  			ns.admin.refreshGrid('#pendingChangesID');
  		}
		ns.common.refreshDashboard('showdbAdminCompanyTransactionGrpSummary',true);
	});

	$.subscribe('completedPerLocationEdit', function(event, data)
	{
		if(!ns.admin.forbiddencompleteSubmit){
            var statusMessage = $('#resultmessage').html();
            ns.common.showStatus(statusMessage);
		}
		ns.admin.goToInputPage("/cb/pages/jsp/user/perlocationpermissions.jsp","#Per_Locations");

	});
	$.subscribe('completedWireTemplatesEdit', function(event, data)
	{
		if(!ns.admin.forbiddencompleteSubmit){
            var statusMessage = $('#resultmessage').html();
            ns.common.showStatus(statusMessage);
		}
		ns.admin.goToInputPage("/cb/pages/jsp/user/wiretemplatesaccess.jsp","#Wire_Templates");
	});

	$.subscribe('completedAutoEntitleEdit', function(event, data)
	{
        var statusMessage = $('#resultmessage').html();
        ns.common.showStatus(statusMessage);
		ns.admin.goToInputPage("/cb/pages/jsp/user/editautoentitle.jsp","#Auto_Entitlement");
	});

	$.subscribe('completedAccountAccessEdit', function(event, data)
	{
		if(!ns.admin.forbiddencompleteSubmit){
            var statusMessage = $('#resultmessage').html();
            ns.common.showStatus(statusMessage);
		}
		ns.admin.goToInputPage("/cb/pages/jsp/user/accountaccess.jsp","#Accounts");
	});

	$.subscribe('completedAccountGroupsEdit', function(event, data)
	{
		if(!ns.admin.forbiddencompleteSubmit){
            var statusMessage = $('#resultmessage').html();
            ns.common.showStatus(statusMessage);
		}
		ns.admin.goToInputPage("/cb/pages/jsp/user/accountgroupaccess.jsp","#Account_Groups");
	});

	$.subscribe('completedXACHCompanyEdit', function(event, data)
	{
		if(!ns.admin.forbiddencompleteSubmit){
            var statusMessage = $('#resultmessage').html();
            ns.common.showStatus(statusMessage);
		}
		//QTS687878 get method cannot refresh page, use post now
		$.post('/cb/pages/jsp/user/crossachcompanyaccess.jsp',
		{
			CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage
		},
		function(data) {
			$('#crossachcompanyaccessDiv').html(data);
		});


		//ns.admin.goToInputPage("/cb/pages/jsp/user/crossachcompanyaccess.jsp","#crossachcompanyaccessDiv");
	});

	$.subscribe('completedPerACHCompanyEdit', function(event, data)
	{
		if(!ns.admin.forbiddencompleteSubmit){
            var statusMessage = $('#resultmessage').html();
            ns.common.showStatus(statusMessage);
		}
		ns.admin.goToInputPage("/cb/pages/jsp/user/achcompanyaccess.jsp","#achcompanyaccessDiv");
	});

	//for the first level permission tabs

	$.subscribe('changePermTabs', function(event, data)
	{
		var $tabs = $("#permTabs");
//		if($tabs.tabs('option', 'active') == event.originalEvent.ui.index)
//			return;
		$tabs.find("div.ui-tabs-panel").html('');
	});

	//for the first level permission tabs
	$.subscribe('loadPermTabComplete', function(event, data)
	{
		//click one of the second layer tabs
		if(ns.admin.l1TabId){
			if(!ns.admin.l2TabNum) ns.admin.l2TabNum = 0;
			//find the tabbedpanel contained in the tab
			$('#' + ns.admin.l1TabId + 'Tabs').tabs('option','active',ns.admin.l2TabNum);
			delete ns.admin.l1TabId;
			delete ns.admin.l2TabNum;
		} else {
			$('#permTabs').find('.ui-tabs').tabs('option','active', 0);
		}

		ns.admin.tabifyPermissionTabs();
	});

	//for cross account sub tabs
	$.subscribe('changeCrossAccountTab', function(event, data)
	{
		var $tabs = $("#XAcctTabs");
//		if($tabs.tabs('option', 'active') == event.originalEvent.ui.index)
//			return;
		$tabs.find("div.ui-tabs-panel").html('');
	});

	//for per account sub tabs
	$.subscribe('changePerAccountTab', function(event, data)
	{
		var $tabs = $("#perAcctTabs");
//		if($tabs.tabs('option', 'active') == event.originalEvent.ui.index)
//			return;
		$tabs.find("div.ui-tabs-panel").html('');
	});

	//for per account groups sub tabs
	$.subscribe('changePerAccountGroupsTab', function(event, data)
	{
		var $tabs = $("#perAcctGrpTabs");
//		if($tabs.tabs('option', 'active') == event.originalEvent.ui.index)
//			return;
		$tabs.find("div.ui-tabs-panel").html('');
	});

	//refresh account configuration grid
	$.subscribe('refreshAcctConfigGrid', function(event, data)
	{
		ns.admin.refreshAccountConfigurationGrid();
		$("#summary").show();
	});

	// hide clone user permissions button and clone account permissions button
	$.subscribe('hideCloneUserButtonAndCloneAccountButton', function(event, data)
	{
		ns.admin.gotoAdmin();
	});

	$.subscribe('location_selectBankTopics', function(event,data) {
		ns.admin.selectDestBank("add");
	});

	$.subscribe('locationEdit_selectBankTopics', function(event,data) {
		ns.admin.selectDestBank("edit");
	});

	$.subscribe('location_closeSelectBankTopics', function(event,data) {
		$('#locationSearchBankDialogID').dialog('close');
	});

	// Insert the selected destination bank, used by "banklistitem.jsp"
	$.subscribe('location_insertSelectedBankTopics', function(event,data) {

		var fields = $("#user_insertSelectedDestinationBankFormID").serializeArray();

		jQuery.each(fields, function(i, field){
		  if (field.name == "bankName") {
			$("#BANKNAME").val(field.value);
		  }
		  if (field.name == "rtNum") {
			  $("#ROUTINGNUMBER").val(field.value);
		  }
		});
		$('#locationSearchBankDialogID').dialog('close');

		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$("#details").setInitialFocus();
		}
	});

    $.subscribe('verifyDiscardUserCompleteTopics', function(event,data) {
    	$.ajaxSetup({
			async: false
		});

		ns.admin.reloadPendingUsersAndUsersGrid();

		$.ajaxSetup({
			async: true
		});
    });

    $.subscribe('submitUserForApprovalCompleteTopics', function(event,data) {
    	ns.common.showSummaryOnSuccess("summary","done");
    });
    
    $.subscribe('rejectDAChangesSuccessTopics', function(event,data) {
    	
    });

    $.subscribe('rejectDAChangesCompleteTopics', function(event,data) {
		if ($('#admin_pendingUsersTab').length > 0) {
			if (ns.common.isElementVisible("dbUsersSummary")) {
				ns.admin.reloadPendingUsersAndUsersGrid();
			} else if (ns.common.isElementVisible("dbProfilesSummary")) {
				ns.admin.reloadPendingProfilesAndProfilesGrid();
			}
		}
		if ($('#admin_groupsTab').length > 0) {
			ns.admin.reloadPendingGroupsGrid();
		}
		if ($('#admin_divisionsTab').length > 0) {
			ns.admin.reloadPendingDivisionsGrid();
		}
		if ($('#companyTabs').length > 0) {
			ns.admin.refreshCompanyModule();
		}
    	if(event.originalEvent.status === "success"){ // incase of any error let the dialog be open
			ns.common.closeDialog("#approvalWizardDialogID");
		}
    });

    $.subscribe('deleteUserVerifyCompleteTopics', function(event,data) {
		//ns.admin.reloadPendingUsersAndUsersGrid();
		var errorMsg = $('#resultmessage').html();
		if(errorMsg == ""){
			ns.common.showSummaryOnSuccess("summary","done");
            ns.admin.checkResizeApprovalDialog();
		}
    });

    $.subscribe('addModifyUserVerifyCompleteTopics', function(event,data) {
		ns.admin.reloadPendingUsersAndUsersGrid();
        ns.admin.checkResizeApprovalDialog();
    });

    // Function to close delete profile close dialogo.
    $.subscribe('completeDeleteUserProfileVerify', function(event,data) {
    	ns.common.showSummaryOnSuccess("profileSummary","done");
		ns.common.closeDialog();
    });
    
    

	$.subscribe('successSubmitUserChanges', function(event, data)
 	{
 		ns.admin.gotoStep(3);
 	});
	
}


ns.admin.accountAccessSort = function(toggleSortedBy)
{
	var url = "/cb/pages/user/SortAccountsOnAccountAccess.action?ToggleSortedBy="+toggleSortedBy;
	$.ajax({
		type: "GET",
		url: url,
		success: function(data) {
			$('#accountaccessDiv').html(data);
		}
	});
}

ns.admin.initialize = function()
{
	ns.admin.setup();
}


ns.admin.gotoStep = function(stepnum)
{
	ns.common.gotoWizardStep('detailsPortlet',stepnum);
	
	var tabindex = stepnum - 1;
	$('#crudTabs').tabs('enable', tabindex);
	$('#crudTabs').tabs('option','active', tabindex);
	$('#crudTabs').tabs('disable', (tabindex + 1) % 3);
	$('#crudTabs').tabs('disable', (tabindex + 2) % 3);

	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#crudTabs").setFocusOnTab();
	}
}

/**
 * Close Details island and expand Summary island
 */
ns.admin.closeDetails = function(){
    $('#details').slideUp();
    $('#summary').show();
    ns.common.refreshDashboard('showdbUsersSummary',true);
}
/**
 * Close Details island and expand Summary island for Admin - Users
 */
ns.admin.closeUserDetails = function(){
	$('#admin_usersTab').portlet('expand');

	// Expand pending user grid if DA is on and user clicks edit/add user button
	if ($('#admin_usersPendingApprovalTab').length > 0) {
		$('#admin_usersPendingApprovalTab').portlet('expand');
	}
    $('#details').slideUp();
    $('#summary').show();
};
/**
 * Close Details island and expand Summary island for Admin - Groups
 */
ns.admin.closeGroupDetails = function(){
	$('#admin_groupsTab').portlet('expand');
    $('#details').slideUp();
    $('#summary').show();
    $('#permissionsDiv').hide();
}
/**
 * Close Details island and expand Summary island for Admin - Divisions
 */
ns.admin.closeDivisionDetails = function(){	
	$('#admin_divisionsTab').portlet('expand');	
	//$('#admin_divisionsTab').tabs('option','active',0);
	//$('#admin_divisionsTab').tabs('option','disabled',[1]);	
	// Expand pending grid if DA is on and has data
	if ($('#division_PendingApprovalTab').length > 0) {
		$('#division_PendingApprovalTab').portlet('expand');
	}
    $('#details').slideUp();
    $('#summary').show();
    $('#permissionsDiv').hide();
};

/**
 * Close Details island and expand Summary island for Admin - Locations
 */
ns.admin.closeLocationDetails = function(){	
    $('#details').slideUp();
    $('#summary').show();
};


/**
 * Close Details island and expand Summary island
 */
ns.admin.closePermissions = function(){
	$('#companyTabs').portlet('expand');
	$('#admin_divisionsTab').portlet('expand');
	$('#admin_groupsTab').portlet('expand');
	$('#admin_usersTab').portlet('expand');
	$('#admin_usersPendingApprovalTab').portlet('expand');
    $('#permissionsDiv').slideUp();
}
/**
 * Admin - Company
 * Close Details island and expand Summary island
 */
ns.admin.closeCompanyDetails = function() {
	$('#companyTabs').portlet('expand');
	$('#companyDetails').slideUp();
}

/**
* Refresh Company Module
*/
ns.admin.refreshCompanyModule = function()
{
	$.get('/cb/pages/jsp/user/admin_company_summary.jsp',function(data){
	$('#summary').html(data);
	});
}
/**
* Refresh Company Profile
*/
ns.admin.refreshCompanyProfile = function()
{
	//$("#companyTabs").tabs("load", $("#profile").index());
	//$("#companyTabs").tabs('option','active', $("#profile").index());
}
/**
*Refresh BAI Export Settings
*/
ns.admin.refreshBAIExportSettings = function()
{
	//$("#companyTabs").tabs("load", $("#baiExportSettings").index());
	//$("#companyTabs").tabs('option','active', $("#baiExportSettings").index());
}
/**
*Refresh Administrators
*/
ns.admin.refreshAdministrators = function()
{
	//$("#companyTabs").tabs("load", $("#administrators").index());
	//$("#companyTabs").tabs('option','active', $("#administrators").index());
}
/**
*Refresh Transaction Groups
*/
ns.admin.refreshTG = function()
{
	//$("#companyTabs").tabs("load", $("#tranGroups").index());
	//$("#companyTabs").tabs('option','active', $("#tranGroups").index());
}
/**
*edit transaction group based on a list(on page).
*/
ns.admin.editTG = function( urlString ){
	$.ajax({
		url: urlString,
		success: function(data){
			$('#companyContent').html(data);
			var heading = $('#PageHeading').html();
 			var $title = $('#companyDetails .portlet-title');
 			$title.html(heading);
			$('#companyDetails').slideDown();
			$('#companyTabs').portlet('fold');
			$('#summary').hide();
			ns.common.selectDashboardItem("addTransactionGroupBtn");
		}
	});
}
/**
*delete transaction group based on a list(on page).
*/
ns.admin.delTG = function(urlString){
	$.ajax({
		url: urlString,
		success: function(data){
		$('#deleteTransactionGroupDialog').html(data).dialog('open');
		}
	});
}
/**
*applied to admin
*/
ns.admin.gotoAdmin = function(){

	//$('#appdashboard').pane('hide');
	//$('#cloneUserPermissionsButton').attr('style','display:none');
	//$('#cloneAccountPermissionsButton').attr('style','display:none');
	$.publish("adminSaveOnCompleteTopic");

}

// Get admin permissions
function getPermissionsData( urlString, divID )
{
	$.get(urlString, function(data) {
		$(divID).html(data);
	});
}

function clearPermissionsData(divID )
{
	$(divID).html("");
}
/**
*View admin summary
*/
ns.admin.viewAdminSummaryByGroupId = function( urlString ){
	$.ajax({
		url: urlString,
		success: function(data){
			$('#viewSummaryByGroupIdDialog').html(data).dialog('open');	
		}
	});
};

/**
* Remove all items from receiver list
*/
ns.admin.removeAllFromAdminList = function() {

	var homeId, option, item;
	var $userList = $("#userList").data('ownner'),
		$groupList = $("#groupList").data('ownner'),
		$adminList = $("#adminList").data('ownner');


	$("#adminList_sms_list").find('li.ui-element').each(
		function(){
			item = $(this);
			option = item.data('optionLink');

			homeId = option.attr('mother');
			if (homeId == "userList"){
				//item.find('font').eq(0).html( option.text() );
				$adminList.removeItem(item, $userList);
			} else if (homeId == "groupList"){
				//item.find('font').eq(0).html( option.text() );
				$adminList.removeItem(item, $groupList);
			} else {
				item.data('optionLink').remove();
				item.remove();
			}
	});

	$adminList.updateSelect();
}

ns.admin.beforeApplyRightItem = function(item, adminList)
{
	var listName = item.data('optionLink').val().substr( 0, 6 );
	var homeList = "";
	if( listName.indexOf( "USER" ) != -1 ) {
		homeList = "userList";
	} else {
    	homeList = "groupList";
	}
	item.data('optionLink').attr('mother', homeList);

	return true;
}

ns.admin.viewAdminPermAcctGrpList = function( urlString ){
	$.ajax({
		url: urlString,
		success: function(data){
		$('#viewPermAcctGrpDialog').html(data).dialog('open');
		}
	});
}
// this function would be triggered when clicking on each permission Tab.
ns.admin.changePermissionTab = function(urlString, divID){

	$.get(urlString, function(data) {
	$(divID).html(data);
	});
}
// go back the page given for per account account & limits of permission.
//ComponentIndex=1&perAcctFrm_Search=PT_AcctSearchFrm&perAcctFrm_View=PT_AcctListFrm&permAcctFrm_Limit=PT_AcctLimitFrm&perAcctFrm_Targerts=targetPerAcctPaymentsTransfers
ns.admin.goBackAnotherAccount = function (componentIndex, target, divID){
	urlString = "/cb/pages/jsp/user/peraccountpermissions.jsp?" + "ComponentIndex=" + componentIndex + "&perm_target=" + target;
	$.get(urlString, function(data) {
		$(divID).html(data);
	});
}

ns.admin.viewPerLocations = function(){
	$.ajax({
		type: "POST",
		url: "/cb/pages/jsp/user/perlocationpermissions.jsp",
		data: $("#LocationDivisionSelect").serialize(),
		success: function(data) {
			$('#Per_Locations').html(data);

			$.publish("common.topics.tabifyNotes");
			if(accessibility){
				$('#Per_Locations').setFocus();
			}
		}
	});
}

ns.admin.goToInputPage = function(urlString, divID){
	$.get(urlString, function(data) {
		$(divID).html(data);
		$.publish("common.topics.tabifyNotes");
		if(accessibility){
			$(divID).setFocus();
		}
	});
}
// This function would be used for sort with ordinary fashion

ns.admin.sort = function(urlString, divID){
	$.get(urlString, function(data){
	$(divID).html(data);
	});
}

ns.admin.reset = function(urlString, divID)
{
	$.get(urlString, function(data){
		$(divID).html(data);
	});
};
// for next permission button on permission part
ns.admin.nextPermission = function(buttonId, tabId)
{
	if($(tabId).tabs().length > 0){
		var $tabs = $(tabId).tabs();
		var selected = $tabs.tabs('option', 'active');
		$(tabId).tabs('option','active', selected + 1);		
	}else{
		var nextTopLevelPermissionTab = $('#'+ns.admin.topLavelPermissionTab + '~ span.summaryTabDropdownItem').eq(0);
		ns.admin.topLavelPermissionTab = $(nextTopLevelPermissionTab).prop('id');
		$(nextTopLevelPermissionTab).click();
	}
};
/**
* Remove all items from Assigned Typecodes List
*/
ns.admin.removeAllFromAssignedTypecodesList = function() {

	var homeId, option, item;
	var $availableTypecodesListId = $("#availableTypecodesListId").data('ownner'),
		$assignedTypecodesListId = $("#assignedTypecodesListId").data('ownner');


	$("#assignedTypecodesListId_sms_list").find('li.ui-element').each(
		function(){
			item = $(this);
			option = item.data('optionLink');

			homeId = option.attr('mother');
			if (homeId == "availableTypecodesListId"){
				//item.find('font').eq(0).html( option.text() );
				$assignedTypecodesListId.removeItem(item, $availableTypecodesListId);
			} else {
				item.data('optionLink').remove();
				item.remove();
			}
	});

	$assignedTypecodesListId.updateSelect();
}

// refresh account configuration grid
ns.admin.refreshAccountConfigurationGrid = function ()
{

	//$("#companyTabs").tabs("load", $("#accountConfiguration").index());
	//$("#companyTabs").tabs('option','active', $("#accountConfiguration").index());
	if(accessibility){
		$.publish("common.topics.tabifyNotes");
		$("#Account_Configuration").setFocus();
	}
}

// refresh users grid
ns.admin.refreshUsersGrid = function ()
{
	$.get('/cb/pages/jsp/user/corpadminusers.jsp', function(data) {
		$("#adminUsersDiv").html(data);
	});
}

// unlock account
ns.admin.unlockAccount = function ( urlString )
{
	$.ajax({
		url: urlString,
		success: function(jsonResponse)
		{					
			var status = jsonResponse.response.data || "";						
			$('#unlockAccountId').html(status);
			if(status == 'Unlocked'){
				$('#passwordStatus').val('0');
			}
		}
	});
}

$(document).ready(function()
{
	$.debug(false);
	ns.admin.initialize();
});

ns.admin.selectDestBank = function(addOrEdit)
{
	var actionURL = "";
	frm = document.AddEditLocationForm;

	var destURL = "/cb/pages/jsp/user/banklistselect.jsp";
	if (addOrEdit == "edit") {
    	actionURL = "/cb/pages/user/editLocation-preProcess.action";
	} else {
		actionURL = "/cb/pages/user/addLocation-preProcess.action";
	}
	ns.admin.searchDestinationBankForm(destURL, actionURL);
}

ns.admin.searchDestinationBankForm = function (urlString, actionString){
	var formId="#AddEditLocationFormID";
	$.ajax({
	  type: "POST",
	  url: actionString,
	  data: $(formId).serialize(),
	  success: function(data) {
		$.ajax({
			  type: "POST",
			  url: urlString,
			  data: $(formId).serialize(),
			  success: function(data) {
				 $('#locationSearchBankDialogID').html(data).dialog('open');
			  }
			});
	  }
	});
};

ns.admin.reloadEmployees = function()
{
	$.ajax({
		  type: "GET",
		  url: "/cb/pages/jsp/user/inc/corpadminusers-pre.jsp",
		  success: function(data) {
				$("#tempDivIDForMethodNsAdminReloadEmployees").html(data);
				if( ns.admin.pendingApprovalEmployeesNum == "0" ){
					$('#admin_usersPendingApprovalTab').attr('style','display:none');
				} else {
					$('#admin_usersPendingApprovalTab').removeAttr('style');
				}
		  }
	});
};

ns.admin.reloadPendingUsersGrid = function()
{
	$.ajaxSetup({
        async: false
	});

	ns.admin.reloadEmployees();
	if( ns.admin.dualApprovalMode == "TRUE" ) {
		$('#usersWithPendingChangesGrid').trigger("reloadGrid");
		$('#profilesWithPendingChangesGrid').trigger("reloadGrid");
	}

    $.ajaxSetup({
        async: true
	});
};

ns.admin.reloadPendingUsersAndUsersGrid = function()
{
	ns.admin.reloadPendingUsersGrid();
	if($('#usersWithPendingChangesGrid').jqGrid('getDataIDs').length  == 0){
		$('#admin_usersPendingApprovalTab').hide();
	}
	$('#adminUserGrid').trigger("reloadGrid");
};

ns.admin.reloadPendingProfilesGrid = function()
{
	$.ajaxSetup({
        async: false
	});

	if( ns.admin.dualApprovalMode == "TRUE" ) {
		$('#profilesWithPendingChangesGrid').trigger("reloadGrid");
	}

    $.ajaxSetup({
        async: true
	});
};

ns.admin.reloadPendingProfilesAndProfilesGrid = function()
{
	ns.admin.reloadPendingProfilesGrid();
	if($('#profilesWithPendingChangesGrid').jqGrid('getDataIDs').length  == 0){
		$('#admin_profilesPendingApprovalTab').hide();
	}
	$('#adminProfileGrid').trigger("reloadGrid");
};

ns.admin.reloadProfiles = function()
{
	$.ajax({
		  type: "GET",
		  url: "/cb/pages/jsp/user/inc/corpadminprofiles-pre.jsp",
		  success: function(data) {
		  }
	});
};

ns.admin.reloadPendingAfterApprove = function()
{
	
	if ($('#admin_usersTab').length > 0) {
		if (ns.common.isElementVisible("dbUsersSummary")) {
			ns.admin.reloadPendingUsersAndUsersGrid();
		} else if (ns.common.isElementVisible("dbProfilesSummary")) {
			ns.admin.reloadPendingProfilesAndProfilesGrid();
		}
	}
	if ($('#admin_groupsTab').length > 0) {
		ns.admin.reloadPendingGroupsGrid();
	}
	if ($('#admin_divisionsTab').length > 0) {
		ns.admin.reloadPendingDivisionsGrid();
	}
	if ($('#companyTabs').length > 0) {
		ns.admin.refreshCompanyModule();
	}
};

ns.admin.reloadDivisions = function()
{
	$.ajax({
		  type: "GET",
		  url: "/cb/pages/jsp/user/inc/corpadmindiv-pre.jsp",
		  success: function(data) {
				$("#tempDivIDForMethodNsAdminReloadDivisions").html(data);
				if( ns.admin.pendingApprovalDivisionsNum == "0" ){
					$('#division_PendingApprovalTab').attr('style','display:none');
				} else {
					$('#division_PendingApprovalTab').removeAttr('style');
				}
		  }
	});
};

ns.admin.reloadPendingDivisionsGrid = function()
{
	$.ajaxSetup({
        async: false
	});

	ns.admin.reloadDivisions();
	if( ns.admin.dualApprovalMode == "TRUE" ) {
		$('#pendingDivisionChangesID').trigger("reloadGrid");
	}

    $.ajaxSetup({
        async: true
	});

	$('#adminDivisionGrid').trigger("reloadGrid");
}

ns.admin.reloadGroups = function()
{
	$.ajax({
		  type: "GET",
		  url: "/cb/pages/jsp/user/inc/corpadmingroups-pre.jsp",
		  success: function(data) {
				$("#tempDivIDForMethodNsAdminReloadGroups").html(data);
				if( ns.admin.pendingApprovalGroupsNum == "0" ){
					$('#group_PendingApprovalTab').attr('style','display:none');
				} else {
					$('#group_PendingApprovalTab').removeAttr('style');
				}
		  }
	});
};

ns.admin.reloadPendingGroupsGrid = function()
{
	$.ajaxSetup({
        async: false
	});

	ns.admin.reloadGroups();
	if( ns.admin.dualApprovalMode == "TRUE" ) {
		$('#pendingGroupChangesID').trigger("reloadGrid");
	}

    $.ajaxSetup({
        async: true
	});

	$('#adminGroupGrid').trigger("reloadGrid");
}

ns.admin.reloadCompanyPermissions = function()
{
//	if( ns.admin.dualApprovalMode == "TRUE" ) {
		$.ajaxSetup({
			async: false
		});

		$('#Permissions').html("");
		$.ajax({
			type: "POST",
			url: "/cb/pages/jsp/user/corpadmincompanypermissions.jsp",
			success: function(data) {
				$('#Permissions').html(data);
			}
		});

		$.ajaxSetup({
			async: true
		});
//	}
}

ns.admin.reloadPendingCompanyGrid = function()
{
		$.ajaxSetup({
			async: false
		});

		ns.admin.reloadCompany();
        if( ns.admin.dualApprovalMode == "TRUE" ) {
            $('#pendingChangesID').trigger("reloadGrid");
        }

		$.ajaxSetup({
			async: true
		});
}

ns.admin.guideUserToSubmenu = function( option )
{
	$('li[menuId="' + option + '"] a').click();
};

ns.admin.addAdmin = function(urlString){
	$.get( urlString, function(result){
	$("#inputDiv").html(result);
    });
};

ns.admin.showPermission = function(l1TabId, l2TabNum){
	ns.admin.l1TabId = l1TabId;
	ns.admin.l2TabNum = l2TabNum;
	$('#companyPermissionsLink').click();
	$("#summary").hide();
};

ns.admin.gotoPermission = function(topLevelTabId, secondLevelTabId){
	ns.admin.topLavelPermissionTab = topLevelTabId;
	ns.admin.secondLavelPermissionTab = secondLevelTabId;
	
	$('#companyPermissionsLink').click();
	$("#summary").hide();
};

//for cross account sub tabs
$.subscribe('permissionTabLoaded', function(event, data)
{
	/* This logic is not required now, since we are loading the user requested tab by default 
	var tabContainerId = $("#permissionTabsSummary").find(".ui-tabs").prop('id');
	if(tabContainerId != undefined && tabContainerId !='' && $('#'+tabContainerId).length > 0 ){
		$('#'+tabContainerId).tabs('option','active',ns.admin.secondLavelPermissionTab);	
	}*/
	
});

ns.admin.postForm = function(FormId, ActionUrl){
	$('#'+FormId).attr('action', ActionUrl);
}

ns.admin.setHiddenFiledValue = function(hiddenFieldId, newValue){
 $('#'+ hiddenFieldId).val(newValue);
}

$.subscribe('openEditAdminerDialog', function(event,data) {
	$('#editAdminerDialogId').dialog('open');
	ns.common.selectDashboardItem("adminitratorsEditLink");
});

$.subscribe('closeEditAdminerDialog', function(event,data) {
	$('#editAdminerDialogId').dialog('close');

});$.subscribe('closeEditAdminerAutoEntitleDialog', function(event,data) {
	$('#editAdminerAutoEntitleDialogId').dialog('close');
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		ns.admin.focusToEditAdminButton();
	}
});

ns.admin.editAdminerCancelButtonOnClick = function(){
	$('#editAdminerDialogId').dialog('close');
}

ns.admin.editAdminerAutoEntitleCancelButtonOnClick = function(){
	$('#editAdminerAutoEntitleDialogId').dialog('close');
	if(ns.admin.editAdminerAutoEntitleCancelUrl != null && typeof(ns.admin.editAdminerAutoEntitleCancelUrl) != 'undefined') {
	$.ajax({
		url: ns.admin.editAdminerAutoEntitleCancelUrl,
		success: function(data) {
			$('#inputDiv').html(data);
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				ns.admin.focusToEditAdminButton();
			}
		}
	});
	}
}

$.subscribe('editAdminerOnCancel', function(event,data) {
	ns.admin.editAdminerCancelButtonOnClick();
});

$.subscribe('editAdminerAutoEntitleOnCancel', function(event,data) {
	ns.admin.editAdminerAutoEntitleCancelButtonOnClick();
});

$.subscribe('editAdminerAutoEntitleOnback', function(event,data) {
	$.ajax({
		url: ns.admin.editAdminerAutoEntitleBackUrl,
		success: function(data) {
			$('#editAdminerAutoEntitleDialogId').dialog('close');
			$('#editAdminerDialogId').dialog('open').html(data);
		}
	});
});

//-------------------------------
//Dual Approval Division Module
//-------------------------------

$.subscribe('openDivisionAdminerDialog', function(event,data) {
	$('#editAdminerDialogId').dialog('open');
});

$.subscribe('viewAdminInViewDivTopic', function(event,data) {
	$('#viewAdminInViewDiv').dialog('open');
});

$.subscribe('openViewLocationDialog', function(event,data) {
	if(ns.admin.PageHeadingForLocation.length>0)
		$('#viewLocationDialogID').dialog('option','title',ns.admin.PageHeadingForLocation);
	$('#viewLocationDialogID').dialog('open');
});

$.subscribe('openDeleteLocationDialog', function(event,data) {
	if(ns.admin.PageHeadingForLocation.length>0)
		$('#deleteLocationDialogID').dialog('option','title',ns.admin.PageHeadingForLocation);
	$('#deleteLocationDialogID').dialog('open');
});

$.subscribe('beforeLoadDivision', function(event, data)
{
	$('#permissionsDiv').hide();
	$('#details').hide();

});


$.subscribe('editAdminerSaveOnComplete', function(event,data) {
	$('#editAdminerDialogId').dialog('close');
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		ns.admin.focusToEditAdminButton();
	}
	if(ns.admin.canAutoEntitle){
		$('#editAdminerAutoEntitleDialogId').dialog('open');
	}
});

$.subscribe('openEditAddDivisionAdminerDialog', function(event,data) {
	$('#editAdminerDialogId').dialog('open');
});

ns.admin.addAdminComplete = function(urlString){
	if(ns.admin.getActionUrl=='setAdministrators-editAddDiv')
	{
		$.get( urlString, function(result){
			$("#inputDiv").html(result);
			ns.admin.forbiddencompleteVerify = false;
		});

	}
	else
	{
		$.get( urlString, function(result){
			if(ns.home.lastMenuId === 'admin_company'){
				//This is only needed for company administrators
				ns.admin.refreshPendingBusinessIsland();
				ns.admin.refreshAdministrators();
				$('#editAdminerAutoEntitleDialogId').dialog('open').html(result);
			} else {
				ns.admin.gotoStep(1);
				$("#inputDiv").html(result);
				ns.admin.forbiddencompleteVerify = false;
			}
		});
	}

};

$.subscribe('editAdminerOnCancelNotForCompany', function(event,data) {
	$('#editAdminerDialogId').dialog('close');
});

$.subscribe('completeVerifyEditAddLocation', function(event, data)
{
 	ns.admin.forbiddencompleteVerify = false;
});

$.subscribe('completeDivisionLoad', function(event, data)
{
	// alert('Form Loaded! ');
	$('#admin_divisionsTab').portlet('fold');
	//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
	var heading = $('#PageHeading').html();
	var $title = $('#details .portlet-title');
	$title.html(heading);

	ns.admin.gotoStep(1);
	$('#details').slideDown();

	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#details").setInitialFocus();
	}
	
	$("#summary").hide();

});


$.subscribe('successSubmitLocation', function(event, data)
{
	ns.admin.gotoStep(3);
	ns.common.closeDialog("viewLocationDialogID");
});



$.subscribe('deleteLocationDoneTopic', function(event, data)
{
	$('#viewLocationDialogID').dialog('close');
	ns.admin.refreshDivisionModule();
});


$.subscribe('completeVerify2', function(event, data)
{
	// alert('Form Verified! ');
	if($('#resultmessage').html()!=''){
		ns.admin.forbiddencompleteVerify = true;
	} else {
		ns.admin.forbiddencompleteVerify = false;
	}
	if(!ns.admin.forbiddencompleteVerify){
 		ns.admin.gotoStep(2);
 	} else {
 		ns.admin.forbiddencompleteVerify = false;
 	}
	if ($('#adminDivisionPendingApprovalLink').length > 0) {
		ns.admin.refreshGrid('#pendingDivisionChangesID');
	}
});


$.subscribe('completeAddDivVerify', function(event, data)
{
	// alert('Form Verified! ');
	if($('#resultmessage').html()!=''){
		ns.admin.forbiddencompleteVerify = true;
	} else {
		ns.admin.forbiddencompleteVerify = false;
	}
	if(!ns.admin.forbiddencompleteVerify){
		ns.admin.gotoStep(2);
	} else {
		ns.admin.forbiddencompleteVerify = false;
 	}
	ns.admin.refreshDivisionModule();
});

$.subscribe('completeVerify3', function(event, data)
{
	// alert('Form Verified! ');
	if($('#resultmessage').html()!=''){
		ns.admin.forbiddencompleteVerify = true;
	} else {
		ns.admin.forbiddencompleteVerify = false;
	}
	if(!ns.admin.forbiddencompleteVerify){
 		ns.admin.gotoStep(3);
 	} else {
 		ns.admin.forbiddencompleteVerify = false;
 	}
});

$.subscribe('completeVerifyForUser', function(event, data)
{
	// alert('Form Verified! ');
	if(!ns.admin.forbiddencompleteVerify){
		ns.admin.gotoStep(3);
	} else {
 		ns.admin.forbiddencompleteVerify = false;
 	}

});

/**
 * if the dialog content is taller than the window size and the dialog height is set to auto, the dialog will resize but no scrollbar is created.
 * To fix this, set the dialog height to a fixed value smaller than the window height.
 * Changing the height back to auto doesn't work either.  So if the dialog isn't bigger than the window and the height isn't auto,
 * resize the dialog back down to the content height + a little bit.
 * TODO: In jQuery 1.5 there is an autoresize option that should be used in place of this function.
*/
ns.admin.checkResizeApprovalDialog = function()
{
	
	$('#approvalWizardDialogID').dialog("option", "height", "auto");
	// $('#approvalWizardDialogID').dialog('option', 'position', 'top');
	$('#approvalWizardDialogID').parent().addClass("positionTop100");	
	return;
	
	// Get size of current window
	var wHeight = $(window).height();
	var contentHeight = 0;
	// calculate the height of the dialog contents
	$('#approvalWizardDialogID').children().each(function(index) {
		contentHeight += $(this).height();
	});
	// if the dialog contents are taller than the window size, set a fixed height.  Otherwise, use auto.
	if (contentHeight > wHeight) {
		contentHeight = wHeight * 0.9;
		$('#approvalWizardDialogID').dialog("option", "height", contentHeight);
		$('#approvalWizardDialogID').dialog('option', 'position', 'top');
	} else {
		var dHeight = $('#approvalWizardDialogID').dialog("option", "height");

		// if auto, just leave it alone - it will resize.  If not, manually resize for the content (plus allowance for title bar) but limit it to window height
		if (dHeight != "auto") {
			dHeight = contentHeight + 40;
			if (dHeight > wHeight) {
				dHeight = wHeight * 0.9;
			}
			$('#approvalWizardDialogID').dialog("option", "height", dHeight);
		}
	}
}


ns.admin.toggleTextBoxes = function() {
	if ( document.editBAIExportSettingsForm.senderIDType[0].checked ) {
		ns.admin.toggleCustomDataField( true, document.editBAIExportSettingsForm.senderIDCustom );
	}

	if ( document.editBAIExportSettingsForm.senderIDType[1].checked ) {
		ns.admin.toggleCustomDataField( false, document.editBAIExportSettingsForm.senderIDCustom );
	}

	if ( document.editBAIExportSettingsForm.receiverIDType[0].checked ) {
		ns.admin.toggleCustomDataField( true, document.editBAIExportSettingsForm.receiverIDCustom );
	}

	if ( document.editBAIExportSettingsForm.receiverIDType[1].checked ) {
		ns.admin.toggleCustomDataField( false, document.editBAIExportSettingsForm.receiverIDCustom );
	}

	if ( document.editBAIExportSettingsForm.ultimateReceiverIDType[0].checked ) {
		ns.admin.toggleCustomDataField( true, document.editBAIExportSettingsForm.ultimateReceiverIDCustom );
	}

	if ( document.editBAIExportSettingsForm.ultimateReceiverIDType[1].checked ) {
		ns.admin.toggleCustomDataField( false, document.editBAIExportSettingsForm.ultimateReceiverIDCustom );
	}

	if ( document.editBAIExportSettingsForm.originatorIDType[0].checked ) {
		ns.admin.toggleCustomDataField( true, document.editBAIExportSettingsForm.originatorIDCustom );
	}

	if ( document.editBAIExportSettingsForm.originatorIDType[1].checked ) {
		ns.admin.toggleCustomDataField( false, document.editBAIExportSettingsForm.originatorIDCustom );
	}
}

ns.admin.toggleCustomDataField = function( disable, customDataField ) {
	if ( disable == true ) {
		customDataField.value = "";
		customDataField.disabled = true;
	} else {
		customDataField.disabled = false;
	}
}

$.subscribe('admincompanySummaryCompleteTopics', function(event, data){
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		var pendingApprovalTab = $("#company_PendingApprovalTab");
		if(!pendingApprovalTab.is(":visible")){
			$("#companyTabs").setFocusOnTab(); //focus only if pending approval iland is not present
		}
	}
});

ns.admin.tabifyPermissionTabs = function(){
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#permTabs").setFocusOnTab();
	}
}

ns.admin.focusToEditAdminButton = function () {
	$("[buttontype='editadmin']").focus();

}

$.subscribe('crossAcctTabsCompleteTopics', function(){
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#XAcctTabs").setFocusOnTab();
	}
});

$.subscribe('perAcctTabsCompleteTopics', function(){
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#perAcctTabs").setFocusOnTab();
	}
});

$.subscribe('perAcctGrpTabsCompleteTopics', function(){
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#perAcctGrpTabs").setFocusOnTab();
	}
});
$.subscribe('beforeAccountAccessPermissionsApplyAll', function(event, data) {
	// $.log('About to verify form! ');
	removeValidationErrors();
});
$.subscribe('confirmAccountAccessPermissionsApplyAll', function(event, data) {
	// $.log('Form Verified! ');
});
$.subscribe('errorAccountAccessPermissionsApplyAll', function(event, data) {
	// $.log('Form Verifying error! ');
});
$.subscribe('successAccountAccessPermissionsApplyAll', function(event, data) {
	// $.log('Form success! ');
	$('#accountAccessPermissionsGrid').trigger("reloadGrid");
});
/**
* Below topics postFormTopic/postOnlyFormTopic have been replacements for onclick event registered on sj:a
*/
$.subscribe('postFormTopic', function(event, data) {
	var $currentTarget = $(event.originalEvent.currentTarget);
	var aPostAction = $currentTarget.attr("postaction");
	var formId = $currentTarget.attr("formid");
	var field1 = $currentTarget.attr("field1");
	var value1 = $currentTarget.attr("value1");
	if(aPostAction){
		ns.admin.setHiddenFiledValue(field1, value1);
		ns.admin.postForm(formId, aPostAction);
	}
});

$.subscribe('postOnlyFormTopic', function(event, data) {
	var $currentTarget = $(event.originalEvent.currentTarget);
	var aPostAction = $currentTarget.attr("postaction");
	var formId = $currentTarget.attr("formid");
	if(aPostAction){
		ns.admin.postForm(formId, aPostAction);
	}
});

$.subscribe('validateSaveOnClickTopic', function(event, data) {
	var formId  = $(event.originalEvent.currentTarget).attr("validatesaveform");
	var form = document.getElementById(formId);
	if( form.adminList.length > 0 ) {

		// mark all items as selected
		for( i = 0 ; i < form.adminList.length ; i++ ) {
		    	form.adminList.options[ i ].selected = "true";
	    	}

		// mark all items as selected
		for( i = 0 ; i < form.userList.length ; i++ ) {
		    	form.userList.options[ i ].selected = "true";
	    	}

		// mark all items as selected
		for( i = 0 ; i <form.groupList.length ; i++ ) {
			form.groupList.options[ i ].selected = "true";
		}
  }

	return true;
});

$.subscribe('onCompleteCompanyProfileTopic', function(event, data) {
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#companyContent").setFocus();
	}

});

$.subscribe('onCompleteEditAdminTopic', function(event, data) {
	if(ns.admin.forbiddencompleteVerify)
	{
	ns.admin.closeDivisionDetails();
	}

/*	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#addDivAdminButtonId").focus();
	}
*/});


$.subscribe('adminSaveOnCompleteTopic', function(event, data) {
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#permTabs div.ui-tabs").setFocusOnTab();
	}
});

$.subscribe('onCompletePermissionSaveTopic', function(event, data) {
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#permTabs").setFocusOnTab();
	}
});

$.subscribe('onCompleteCompanySearchTopic', function(event, data) {
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#permTabs").setFocusOnTab();
	}
});

$.subscribe('onCompletePerAccountTopic', function(event, data) {
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#perAcctTabs").setFocusOnTab();
	}
});

$.subscribe('onCompleteTabifyAndFocusTopic', function(event, data) {
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#permTabs").setFocusOnTab();
	}
});

$.subscribe('nextPermissionTopic', function(event, data) {
	ns.admin.nextPermission('#permAutoEntitleNext', '#permTabs');
});

ns.admin.superSelectFocusHandler = function($selectMenu){
	if(accessibility){
		$.publish("common.topics.tabifyNotes");
	}
	//var $aSelectMenu = $("#assignedTypecodesListId_sms");
	var items = $selectMenu.find("ul li a").length;
	if(items>0){
		$selectMenu.find("ul li a:eq(0)").focus();
	}else{
		$selectMenu.find("input.search").focus();
	}
}


$.subscribe('backButtonTopic', function(event, data) {
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#permTabs").setFocusOnTab();
	}
});


$.subscribe('setFocusTopic', function(event, data) {
	if(accessibility){
		$("#wholeworld").setFocus();
	}
});

$.subscribe('cleanupDivisionDetails', function(event, data) {
	$('#details #inputDiv').html('');
	$('#details #verifyDiv').html('');
	$('#details #confirmDiv').html('');
});

// User Profile

$.subscribe('verifyDiscardProfileCompleteTopics', function(event,data) {
	ns.common.showSummaryOnSuccess("summary","done");
});

$.subscribe('submitProfileForApprovalCompleteTopics', function(event,data) {
	$.ajaxSetup({
		async: false
	});

	ns.admin.reloadPendingUsersAndUsersGrid();

	$.ajaxSetup({
		async: true
	});
});

$.subscribe('deleteProfileVerifyCompleteTopics', function(event,data) {
	var errorMsg = $('#resultmessage').html();
	if(errorMsg == ""){
		ns.admin.reloadPendingUsersAndUsersGrid();
        ns.admin.checkResizeApprovalDialog();
	}
});

$.subscribe('addModifyProfileVerifyCompleteTopics', function(event,data) {
	ns.admin.reloadPendingUsersAndUsersGrid();
    ns.admin.checkResizeApprovalDialog();
});

function loadLocationData(){
	var groupId = $('#dashBoardDivisionId').val();
	if(groupId == undefined || $.trim(groupId) == '' || groupId == null){
		var defaultDivisionId = $('#defaultDivisionId').val();
		var defaultDivisionName = $('#defaultDivisionName').val();
		if(defaultDivisionId === "" || defaultDivisionId == undefined){
			defaultDivisionId = ns.admin.defaultSelectedDivisionId;
			defaultDivisionName = ns.admin.defaultSelectedDivisionName;
		}
		groupId = defaultDivisionId;
		$("#dashBoardDivisionId").lookupbox('destroy');
		var optionDiv ="<option selected value="+defaultDivisionId+">"+defaultDivisionName+"</option>";
		$('#dashBoardDivisionId').html('');
		$('#dashBoardDivisionId').append(optionDiv);
		$("#dashBoardDivisionId").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"size":"30",
			"source":"/cb/pages/user/DivisionLookupBoxAction.action?CollectionName=Descendants&dualApprovalMode=2"
		});	
	}
	if(groupId != undefined && $.trim(groupId) != ''){
		var url="/cb/pages/jsp/user/corpadminlocations-selected.jsp?EditDivision_GroupId="+groupId;
		ns.admin.divisionLocations(url);
	}
}

function refreshDashboardWithSelectionHistory(){
	var menuActionLinkId = "MenuLink";
	
	$('#appdashboard .dashboardUiCls .summaryLabelCls').each(function(){
		if(this.parentElement.className.indexOf('hideDbOnLoad') == '-1'){
			var parentId = this.parentElement.id;
			menuActionLinkId = parentId + menuActionLinkId;
		}
	});
	//Click on the menu action link which will directly load required summary retaining selection histroy
	$("#"+menuActionLinkId).trigger("click");
}

function updateAdminChanges(tabName){
	var menuActionLinkId = "MenuLink";
	var isDA = $('#isDA').val() || ""; 
	if(isDA){
		menuActionLinkId = tabName + menuActionLinkId;
		//Click on the menu action link which will directly load required summary retaining selection histroy
		$("#"+menuActionLinkId).trigger("click");
	}else{
		ns.common.refreshDashboard('show'+tabName,true);
	}
}

//Show the active dashboard based on the user selected summary
ns.admin.showDashboard = function(userActionType,dashboardElementId){
	if(dashboardElementId == 'addDivisionLink'){
		ns.common.selectDashboardItem('addDivisionLink');
	}else if(dashboardElementId == 'showdbLocationSummary'){
		ns.common.refreshDashboard('dbLocationSummary');
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(dashboardElementId && dashboardElementId !='' && $('#'+dashboardElementId).length > 0){
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+dashboardElementId).trigger("click");}, 10);
	}
};

ns.admin.showAdminSummaryDashboard = function(adminSummaryUserActionType,adminSummaryDashboardElementId){
	if(adminSummaryUserActionType == 'AdminSummary'){
		ns.common.refreshDashboard('dbAdministrableAreasSummary');
	}
	//Simulate dashboard item click event if any additional action is required
	if(adminSummaryDashboardElementId && adminSummaryDashboardElementId !='' && $('#'+adminSummaryDashboardElementId).length > 0){
			//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+adminSummaryDashboardElementId).trigger("click");}, 10);
	}
};

$.subscribe('showSuccessMessageTopic', function(event, data)
{
	ns.common.showStatus(js_secuser_updatesqa_message);
});