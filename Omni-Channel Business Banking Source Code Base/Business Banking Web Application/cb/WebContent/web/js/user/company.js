ns.company.setup = function()
{
	
}
$(document).ready(function()
{
	$.debug(false);
	ns.company.setup();
});

//Show the active dashboard based on the user selected summary
ns.company.showCompanyDashboard = function(adminCompanyActionType,adminCompanyDashboardElementId){
	if(adminCompanyActionType == 'Profile'){
		ns.common.refreshDashboard('dbAdminCompanyProfileSummary');	
	}else if(adminCompanyActionType == 'BAI'){
		ns.common.refreshDashboard('dbAdminCompanyBAISummary');
	}else if(adminCompanyActionType == 'Administrators'){
		ns.common.refreshDashboard('dbAdminCompanyAdministratorSummary');
	}else if(adminCompanyActionType == 'AccountConfiguration'){
		ns.common.refreshDashboard('dbAdminCompanyAccConfSummary');
	}else if(adminCompanyActionType == 'AccountGroups'){
		ns.common.refreshDashboard('dbAdminCompanyAccGrpSummary');
	}else if(adminCompanyActionType == 'TransactionGroups'){
		ns.common.refreshDashboard('dbAdminCompanyTransactionGrpSummary');
	}else if(adminCompanyActionType == 'CompanyPermissions'){
		ns.common.refreshDashboard('dbAdminCompanyPermissionsSummary');
	}else{//In case of normal Admin Company menu click or clicking on user shortcut
		ns.common.refreshDashboard('dbAdminCompanyProfileSummary');
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(adminCompanyDashboardElementId && adminCompanyDashboardElementId !='' && $('#'+adminCompanyDashboardElementId).length > 0){
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+adminCompanyDashboardElementId).trigger("click");}, 10);
	}
};

$.subscribe('accountEditSuccessTopic', function(event, data)
{
	if(ns.company.isCompanyDualApprovalMode()) {
		ns.company.refreshPendingCompanyGrid();	
	}
});


//This function ensures that proper Edit/View dashboard links are shown to the user based on the status of Business DA Item.
ns.company.updateCompanyDashboardEditLinks = function(DAItemStatus)
{
	if(ns.company.isCompanyDualApprovalMode()) {
		if(ns.common.isElementVisible('pendingChangesID')){
			if(DAItemStatus == 'Pending Approval'){//If item is pending for approval, then Edit links should not be shown
				if($("#editCompany").length > 0){
					refreshDashboardWithSelectionHistory();
				}else{
					//Do nothing
				}
			}else{//If item is not pending for approval, then Edit links should be shown
				if($("#editCompany").length > 0){
					//Do nothing
				}else{
					refreshDashboardWithSelectionHistory();
				}
			}
		}
	}
};


//Show & reload the company pending changes grid only if it is already not visible.
ns.company.showPendingCompanyGrid = function()
{
	if(!$('#company_PendingApprovalTab').is(":visible")){
		$('#company_PendingApprovalTab').show();
		ns.admin.refreshGrid('#pendingChangesID');
	}
};

ns.company.refreshPendingCompanyGrid = function()
{
	//Make a call to refresh the data in session and the reload the grid
	ns.admin.reloadCompany();
};

$.subscribe('onProfileComplete', function(event, data)
{
	ns.company.updatePageHeading();
});

$.subscribe('onSuccessCompanyProfileTopic', function(event, data)
{
	if(ns.company.isCompanyDualApprovalMode()) {
		ns.company.refreshPendingCompanyGrid();	
	}
});

$.subscribe('onBAIComplete', function(event, data)
{
	$('#companyDetails').hide();
	$('#companyTabs').show();
	ns.company.updatePageHeading();
});

$.subscribe('onAdministratorsComplete', function(event, data)
{
	$('#companyDetails').hide();
	$('#companyTabs').show();
	ns.company.updatePageHeading();
});
$.subscribe('onAccConfComplete', function(event, data)
{
	$('#companyDetails').hide();
	$('#companyTabs').show();
	ns.company.updatePageHeading();
});
$.subscribe('onCompanyAccGrpComplete', function(event, data)
{
	$('#companyDetails').hide();
	$('#companyTabs').show();
	ns.company.updatePageHeading();
});
$.subscribe('onAdminCompanyTransactionGrpComplete', function(event, data)
{
	$('#companyDetails').hide();
	$('#companyTabs').show();
	ns.company.updatePageHeading();
});
$.subscribe('onAdminCompanyPermissionsComplete', function(event, data)
{
	ns.company.updatePageHeading();
});

ns.company.updatePageHeading = function()
{
	var heading = $('#pageHeading').html();
	//console.log(heading)
	ns.common.updatePortletTitle('companySummaryTabs',heading,false);
};

$.subscribe('cancelCompanyProfileForm', function(event, data)
{
	ns.common.showSummaryOnSuccess("companySummaryTabsContent","cancel");
	$('#companyTabs').portlet('expand');
	$('#companyDetails').slideUp();
	ns.common.hideStatus();
	$("#summary").show();
});

$.subscribe('onBAIViewDialogClose', function(event, data)
{
	ns.common.showSummaryOnSuccess("companySummaryTabsContent","cancel");
});

$.subscribe('onCompanyProfileViewDialogClose', function(event, data)
{
	ns.common.showSummaryOnSuccess("companySummaryTabsContent","cancel");
});


$.subscribe('closeViewProfileDialog', function(event, data)
{

});

$.subscribe('closeViewBAIDialog', function(event, data)
{
	ns.common.refreshDashboard('dbAdminCompanyBAISummary');
});

$.subscribe('viewAdminDialogSuccessTopics', function(event,data) {
	$('#editAdminerDialogId').dialog('open');
});

$.subscribe('beforeOpenEditAdminerDialog', function(event, data)
{
	ns.common.selectDashboardItem("AdminitratorsViewLink");
});

$.subscribe('cancelAddEditTransGrpForm', function(event, data)
{
	$('#companyTabs').portlet('expand');
	$('#companyDetails').slideUp();
	ns.common.hideStatus();
	$("#summary").show();
});

$.subscribe('successTgGrpAdded', function(event, data)
{
	$("#summary").show();
	ns.common.refreshDashboard('showdbAdminCompanyTransactionGrpSummary',true);
});

$.subscribe('beforeEditBAILinkClick', function(event, data)
{
	ns.common.selectDashboardItem("BAIEditLink");
});

$.subscribe('onProfileClick', function(event, data)
{
	
});

$.subscribe('onBAIClick', function(event, data)
{
	$('#companySummaryTabs').show();
});

$.subscribe('cancelEditBAICompanyForm', function(event, data)
{
	$("#summary").show();
});

$.subscribe('onAdministratorsClick', function(event, data)
{
	ns.common.refreshDashboard('dbAdminCompanyAdministratorSummary');
});

$.subscribe('onAdministratorsDialogClose', function(event, data)
{
	ns.common.refreshDashboard('showdbAdminCompanyAdministratorSummary',true);
});

$.subscribe('onAdministratorsDialogCloseTopic', function(event, data)
{
	ns.common.showSummaryOnSuccess("companySummaryTabsContent","cancel");
});


$.subscribe('cancelAccConfEditForm', function(event, data)
{
	$("#summary").show();
});

$.subscribe('onAdminCompanyTransactionGrpClick', function(event, data)
{
	
});

$.subscribe('onAccConfClick', function(event, data)
{
	$('#companySummaryTabs').show();
});

$.subscribe('onCompanyAccGrpClick', function(event, data)
{
	
});

$.subscribe('beforeAddTransGrpClick', function(event, data)
{
	ns.common.selectDashboardItem("addTransactionGroupBtn");
});

$.subscribe('onAdminCompanyPermClick', function(event, data)
{
	$("#permissionsDiv").hide();
	$('#companyDetails').hide();
	$('#details').hide();
	$('#summary').show();
});

$.subscribe('reloadCompanyApprovalGrid', function(event, data)
		{
			if(ns.company.isCompanyDualApprovalMode()) {
				ns.company.refreshPendingCompanyGrid();	
			}
});