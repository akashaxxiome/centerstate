ns.groups.setup = function()
{
	ns.common.refreshDashboard('dbGroupSummary');
	$('#details').hide();
	$('#permissionsDiv').hide();
}
$(document).ready(function()
{
	$.debug(false);
	ns.groups.setup();
});

$.subscribe('openGroupAdminerDialog', function(event,data) {
	$('#editAdminerDialogId').dialog('open');
});

ns.groups.showAdminGroupDashboard= function(adminGroupUserActionType,adminGroupDashboardElementId){
	if(adminGroupUserActionType == 'AdminGroupSummary'){
		ns.common.refreshDashboard('dbGroupSummary');
	}
	//Simulate dashboard item click event if any additional action is required
	if(adminGroupDashboardElementId && adminGroupDashboardElementId !='' && $('#'+adminGroupDashboardElementId).length > 0){
		ns.common.selectDashboardItem(adminGroupDashboardElementId);
			//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+adminGroupDashboardElementId).trigger("click");}, 10);
	}
};