ns.secondaryUsers.initView = function(){

};

ns.secondaryUsers.initPersonalInformationView = function(){
	//console.info("ns.secondaryUsers.initPersonalInformationView");
	$("#secUsersCountry").selectmenu({"width":"245px"});	
	$("#secUsersState").hide();
	$("#secUsersAccountStatus").selectmenu({"width":"150px"});
	$("#secUsersPrefLanguage").selectmenu({"width":"150px"});
};

ns.secondaryUsers.registerEvents = function(){	
	var addSecondaryUsersClickHandler = function(){
		//console.info("addSecondaryUsersClickHandler");
		ns.secondaryUsers.showTermsDialog(ns.secondaryUsers.ADD_MODE);
		ns.common.selectDashboardItem("addSecondaryUserBtn");
	}
	$("#addSecondaryUserBtn").live('click',addSecondaryUsersClickHandler);
	
	var cancelDeleteSecondaryUserHandler = function(){
		ns.secondaryUsers.closeDeleteUserDialog();
	};
	$("#cancelDeleteUser").live('click',cancelDeleteSecondaryUserHandler);
	
	var deleteSecondaryUserHandler = function(){
		ns.secondaryUsers.deleteUser();
	};
	$("#deleteUser").live('click',deleteSecondaryUserHandler);
		
	//Terms Dialog	
	$(ns.secondaryUsers.selectors.TERMS_DIALOG_ACCEPT_BUTTON).live('click',ns.secondaryUsers.acceptTerms);	
	$(ns.secondaryUsers.selectors.TERMS_DIALOG_CANCEL_BUTTON).live('click',ns.secondaryUsers.closeTermsDialog);	
	
	//Wizard
	var nextButtonClickHandler = function(e){		
		var step = $(e.currentTarget).attr("wizardStep");
		//console.info("nextButtonClickHandler-" + step);		
	}
	$("a[wizardButtonType=next]").click(nextButtonClickHandler);
		
	//AddEdit events
	var countryChangeHandler = function(){
		//console.info("change  handler");
		ns.secondaryUsers.updateStates();
	}
	$("#secUsersCountry").live('change',countryChangeHandler);
	
	
	$("input[type=radio][togglepermissions=true]").live('click',ns.secondaryUsers.togglePermissionsBlock);
	$("input[type=checkbox][enabledisable=true]").live('click',ns.secondaryUsers.clearLimitsRow);
	//Topics for Wizard
	$.subscribe("initPersonalInformationView",ns.secondaryUsers.initPersonalInformationView);
	
	//Wizard back button handling
	$("a[buttontype=back]").live('click',ns.secondaryUsers.goBack)
	
	$.subscribe("secondaryUsers.closeAddEditConfirmationDialog",ns.secondaryUsers.closeAddEditConfirmDialog);
};

ns.secondaryUsers.togglePermissionsBlock = function(e){
	var $checkedRadioButton = $(e.currentTarget);
	var $formId = $checkedRadioButton.closest("form");
	var formId = $formId.attr("id");
	
	var $permissionsBlock = $("#"+ formId + " div[containerType='permissionsBlock']");	
	if($checkedRadioButton.val() === "TRUE"){
		$permissionsBlock.show();
	}else{
		$permissionsBlock.hide();
	}	
};



ns.secondaryUsers.clearLimitsRow = function(e){
	var $checkedBox = $(e.currentTarget);
	var $formId = $checkedBox.closest("form");
	var formId = $formId.attr("id");
	var rowFor = $checkedBox.attr("name");
	
	var inputBox = $("#"+ formId + " input[inputfor='" + rowFor + "']");	
	var toDisable = true;
	
	if($checkedBox.is(":checked") === true){
		toDisable = false;
	}	
	$.each(inputBox,function(i,iBox){
		$(iBox).val("");
		$(iBox).attr("disabled",toDisable);
	});
};

ns.secondaryUsers.goBack = function(e){
	//console.info("ns.secondaryUsers.goBack");
	var currentTab = $(e.currentTarget);
	//var backStep = parseInt(currentTab.attr("backstep"));
	var backStep = ns.secondaryUsers.prevStep.pop();
	ns.secondaryUsers.enableTab(backStep-1);
	ns.secondaryUsers.disableTab(backStep+1);	
	$.publish("su_tabifyAndSetInitialFocus");	
};

ns.secondaryUsers.disableTab = function(tabId){
	$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_TABS).tabs( "disable",tabId);
};

ns.secondaryUsers.disableTabs = function(){	
	//console.info("ns.secondaryUsers.disbaleTabs ");
	var tabIndex = 0;
	for (tabIndex=0;tabIndex<5;tabIndex++){
		$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_TABS).tabs( "disable",tabIndex);
	}
}

ns.secondaryUsers.enableTab = function(tabId){	
	//console.info("ns.secondaryUsers.enableTab " + tabId);	
	ns.secondaryUsers.disableTabs();
	$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_TABS).tabs("enable",tabId);	
	//$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_TABS).tabs("select",tabId);
	$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_TABS).tabs('option', 'active', tabId);  
	
	ns.secondaryUsers.disableTab(tabId-1);
};

ns.secondaryUsers.editUser = function(userId){
	//console.info("ns.secondaryUsers.editUser");
	ns.secondaryUsers.editedUserId = userId;
	ns.secondaryUsers.ACTIVE_MODE = ns.secondaryUsers.EDIT_MODE;
	ns.secondaryUsers.showTermsDialog(ns.secondaryUsers.EDIT_MODE);
};

/**
* This function sets the terms accepted to true and loads the add/modify 
* view for secondary users wizard.
*/
ns.secondaryUsers.acceptTerms = function(){	
	ns.secondaryUsers.closeTermsDialog();
	var userId = "";
	var mode = ns.secondaryUsers.ACTIVE_MODE;
	if(mode === ns.secondaryUsers.EDIT_MODE){
		userId = ns.secondaryUsers.editedUserId ;
	}
	
	var URL = "/cb/pages/jsp/user/acceptSecondaryUsersTerms.action?mode=" + mode + "&userId=" + userId ;
	$.ajax({
		type: 'GET',
		url:URL,
		success:function(response){
			//console.info("terms are now accepted load show the content from response");			
			if(response){
				if(ns.secondaryUsers.ACTIVE_MODE === ns.secondaryUsers.ADD_MODE){
					ns.secondaryUsers.loadAddEditSecondaryUsersWizard(response);
				}else{
					ns.secondaryUsers.loadEditSecondaryUsersWizard(response);
				}		
				$(ns.secondaryUsers.selectors.SECONDARY_USERS_CONTAINER).hide();
				$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_CONTAINER).show();		
			}
			ns.common.selectDashboardItem("addSecondaryUserBtn");
		}
	});
}


ns.secondaryUsers.loadAddEditSecondaryUsersWizard = function(response){
	var tabConfig = {};
	if(response){
		ns.secondaryUsers.addEditWizardLoaded= true;	
		$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_CONTAINER).html(response);
		$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_TABS).tabs(tabConfig);	
		ns.secondaryUsers.disableTabs();
		$.publish("su_tabifyAndSetInitialFocus");
	}	
}

ns.secondaryUsers.loadEditSecondaryUsersWizard = function(response){
	var tabConfig = {};
	if(response){
		ns.secondaryUsers.addEditWizardLoaded= true;	
		$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_CONTAINER).html(response);
		$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_TABS).tabs(tabConfig);	
		ns.secondaryUsers.disableTabs();
		$.publish("su_tabifyAndSetInitialFocus");					
	}
}

ns.secondaryUsers.showTermsDialog = function(aMode){
	//console.info("ns.secondaryUsers.showTermsDialog");
	ns.secondaryUsers.ACTIVE_MODE = aMode;
	if(!ns.secondaryUsers.termsDialogLoaded){
		ns.secondaryUsers.loadTermsDialog();
	}else{
		ns.secondaryUsers.openTermsDialog();
	}
};

ns.secondaryUsers.loadTermsDialog = function(){
	var URL = "/cb/pages/jsp/user/secondaryUserTermsView.action";
	//console.info("ns.secondaryUsers.loadTermsDialog:");
	$.ajax({
		type: 'GET',
		url:URL,
		success:function(response){
			if(response){
				ns.secondaryUsers.termsDialogLoaded= true;		
				if($(ns.secondaryUsers.selectors.TERMS_DIALOG).length === 0){
					$(response).appendTo('body');
					var dialogConfig = ns.secondaryUsers.dialogConfig.termsDialogConfig;
					$(ns.secondaryUsers.selectors.TERMS_DIALOG).dialog(dialogConfig);			
				}else{
					$(ns.secondaryUsers.selectors.TERMS_DIALOG).dialog('open');
				}					
			}
		},
		error:function(){
			ns.secondaryUsers.termsDialogLoaded= false;
		}
	});					
}

ns.secondaryUsers.openTermsDialog = function(){
	$(ns.secondaryUsers.selectors.TERMS_DIALOG).dialog('open');
}

ns.secondaryUsers.closeTermsDialog = function(){
	//console.info('ns.secondaryUsers.closeTermsDialog');
	$(ns.secondaryUsers.selectors.TERMS_DIALOG).dialog('close');
	ns.common.selectDashboardItem("goBackSummaryLink");
	$.publish("secondaryUsers.closeWizard");
}

// Add Edit confirmation Dialog functions
ns.secondaryUsers.showAddEditConfirmDialog = function(){
	//console.info("ns.secondaryUsers.showAddEditConfirmDialog");
	if(!ns.secondaryUsers.addEditConfirmDialogLoaded){
		ns.secondaryUsers.addEditConfirmDialogLoaded = true;
		if($(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).length === 0 || $(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).length === 1){
			var dialogConfig = ns.secondaryUsers.dialogConfig.addEditConfirmDialog;			
			$(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).dialog(dialogConfig);			
		}else{
			ns.secondaryUsers.openAddEditConfirmDialog();
		}		
	}else{
		ns.secondaryUsers.openAddEditConfirmDialog();
	}
};

ns.secondaryUsers.openAddEditConfirmDialog = function(){
	//console.info("openAddEditConfirmDialog");
	$(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).dialog('open');
}

ns.secondaryUsers.closeAddEditConfirmDialog = function(){
	//console.info("closeAddEditConfirmDialog");
	$(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).dialog('close');
}

ns.secondaryUsers.openDeleteUserDialog = function(){
	var deleteUserDialog = ns.secondaryUsers.selectors.DELETE_USER_DIALOG;
	$(deleteUserDialog).dialog('open');
};

ns.secondaryUsers.closeDeleteUserDialog = function(){
	var deleteUserDialog = ns.secondaryUsers.selectors.DELETE_USER_DIALOG;
	$(deleteUserDialog).dialog('close');
	$(deleteUserDialog).dialog('destroy');
};

ns.secondaryUsers.openPendingTxPaymentsDialog = function(){
	var pendingTxPaymentsDialog = ns.secondaryUsers.selectors.PENDING_TX_PAYEMENTS_DIALOG;
	$(pendingTxPaymentsDialog).dialog('open');
};

ns.secondaryUsers.closePendingTxPaymentsDialog = function(){
	var pendingTxPaymentsDialog = ns.secondaryUsers.selectors.PENDING_TX_PAYEMENTS_DIALOG;
	$(pendingTxPaymentsDialog).dialog('close');
	$(pendingTxPaymentsDialog).dialog('destroy');
};

ns.secondaryUsers.beforeDeleteUser = function(userId){
	var URL = "/cb/pages/jsp/user/confirmDeleteSecondaryUser.action?secondaryUserID=" + userId;
	//console.info("beforeDeleteUser:" +userId );
	if(userId){		
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.DELETE_USER_DIALOG).html(response);
				var dialogConfig = ns.secondaryUsers.dialogConfig.deleteSecondaryUserDialog;
				$(ns.secondaryUsers.selectors.DELETE_USER_DIALOG).dialog(dialogConfig);
			}
		});					
	}
};

ns.secondaryUsers.deleteUser = function(){
	//console.info("ns.secondaryUsers.deleteUser");
	var URL = "/cb/pages/jsp/user/deleteSecondaryUser.action";
	var TransferOrCancelTransactions = $("input[name=TransferOrCancelTransactions]:checked").val();
	URL = URL + "?TransferOrCancelTransactions=" + TransferOrCancelTransactions;
	$.ajax({
		  type: 'GET',
		  url:URL,
		  success:function(responseObj){		
			ns.secondaryUsers.closeDeleteUserDialog();
			if(responseObj.response){
				var response = responseObj.response;
				var message = response.data || "";
				if(response.status == "success"){				
					ns.common.showStatus(message);
				}else{
					ns.common.showError(message);
				}
			}			
			$('#secondaryUsersGrid').trigger("reloadGrid");
		  },
		  error:function(e){
			ns.secondaryUsers.closeDeleteUserDialog();
			ns.common.showError(e);
		  }
	});						
};

//Wizard methods
ns.secondaryUsers.updateStates = function(){
	//console.info("update states");
	var URL = "/cb/pages/jsp/user/inc/states.jsp";	
	var country = $("#secUsersCountry").val() || "";
	URL = URL + "?countryCode=" + country;
	
	$.ajax({
		  type: 'GET',
		  url:URL,
		  success:function(response){				  
			  optionsHTML = $.trim(response);
				if(optionsHTML!=""){
					var HTML = $.trim(optionsHTML);
					$("#secUsersState").html(optionsHTML);				
					$("#secUsersState").selectmenu({"width":"200px"});
					$("#stateRow").show();
					$("#stateRowLabel").show();
				}else{
					$("#stateRow").hide();
					$("#stateRowLabel").hide();
				}				
		  },
		  error:function(e){
			$("#stateRow").hide();
			$("#secUsersState").selectmenu();
		  }
	});							
}

// Wizard Step 1: getting personal information
$.subscribe("secondaryUsers.addPersonalInformationSuccess", function(event,data) {
	//console.info("secondaryUsers.addPersonalInformationSuccess");	
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.ADD_MODE,event);
	
	/*if(data){
		//var URL = "/cb/pages/jsp/user/user_addedit_accounts.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_addAccountsSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_ACCOUNT_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(1);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});					
	}*/
});

$.subscribe("secondaryUsers.savePersonalInformationSuccess", function(event,data) {
	//console.info("secondaryUsers.savePersonalInformationSuccess");
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.EDIT_MODE,event);

	/*
	if(data){
		//var URL = "/cb/pages/jsp/user/user_edit_accounts.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyAccountsSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				//console.info("Received user_edit_accounts.jsp");
				$(ns.secondaryUsers.selectors.WIZARD_TAB_ACCOUNT_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(1);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});					
	}*/
});

// Wizard Step 2: Accounts setup
$.subscribe("secondaryUsers.addAccountsSuccess", function(event,data) {
	//console.info("secondaryUsers.addAccountsSuccess");
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.ADD_MODE,event);
	/*if(data){
		//var URL = "/cb/pages/jsp/user/user_addedit_transfers.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_addTransferSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_TRANSFER_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(2);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});		
	}*/
});

$.subscribe("secondaryUsers.saveAccountsSuccess", function(event,data) {
	//console.info("secondaryUsers.saveAccountsSuccess");
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.EDIT_MODE,event);
	/*if(data){
		//var URL = "/cb/pages/jsp/user/user_edit_transfers.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyTransferSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_TRANSFER_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(2);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});		
	}*/
});

$.subscribe("secondaryUsers.addUserSuccess", function(event,data) {
	//console.info("user creation -add");
	ns.common.showSummaryOnSuccess("summary","done");
	ns.common.selectDashboardItem("goBackSummaryLink");
	ns.common.showStatus(js_secuser_adduser_message);
	$(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).dialog('close');
	$.publish("secondaryUsers.closeWizard");
});

$.subscribe("secondaryUsers.saveUserSuccess", function(event,data) {
	//console.info("secondaryUsers.saveUserSuccess");
	ns.common.showSummaryOnSuccess("summary","done");
	ns.common.selectDashboardItem("goBackSummaryLink");
	ns.common.showStatus(js_secuser_updateuser_message);
	$(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).dialog('close');
	$.publish("secondaryUsers.closeWizard");
});

$.subscribe("secondaryUsers.cancel", function(event,data) {
	$.publish("secondaryUsers.closeWizard");
	ns.common.selectDashboardItem("goBackSummaryLink");
	/*
	//TODO:hit ajax action to cleanup all session data
	var URL = "/cb/pages/jsp/user/SaveUserAction_cancel.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$.publish("secondaryUsers.closeWizard");
			 }
	});
	*/	
});

$.subscribe("secondaryUsers.closeWizard", function(event,data) {
	//console.info("secondaryUsers.closeWizard");
	$(ns.secondaryUsers.selectors.ADDEDIT_SECONDARY_USERS_CONTAINER).hide();
	$(ns.secondaryUsers.selectors.SECONDARY_USERS_CONTAINER).show();	
});

$.subscribe("secondaryUsers.addTransfersSuccess", function(event) {
	//console.info("secondaryUsers.addTransfersSuccess");
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.ADD_MODE,event);
	/*if(event.originalEvent.data.response.data.nextStep == "3"){
		//var URL = "/cb/pages/jsp/user/user_addedit_exttransfers.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_addExternalTransferSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_EXTRANSFER_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(3);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});		
	}
	else{
		$.publish("secondaryUsers.addExTransfersSuccess", event.originalEvent.data);
	}*/
});

$.subscribe("secondaryUsers.saveTransfersSuccess", function(event) {
	//console.info("secondaryUsers.saveTransfersSuccess");
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.EDIT_MODE,event);
	/*if(event.originalEvent.data.response.data.nextStep == "3"){
		//var URL = "/cb/pages/jsp/user/user_edit_exttransfers.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyExternalTransferSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_EXTRANSFER_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(3);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});		
	}
	else{
		$.publish("secondaryUsers.saveExTransfersSuccess", event.originalEvent.data);
	}*/
});

$.subscribe("secondaryUsers.addExTransfersSuccess", function(event,data) {
	//console.info("secondaryUsers.addExTransfersSuccess");
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.ADD_MODE,event);
	/*if(data){
		//var URL = "/cb/pages/jsp/user/user_addedit_payments.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_addPayeeSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_BILLPAYMENT_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(4);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});		
	}*/
});

$.subscribe("secondaryUsers.saveExTransfersSuccess", function(event,data) {
	//console.info("secondaryUsers.addExTransfersSuccess");
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.EDIT_MODE,event);
	/*if(data){
		//var URL = "/cb/pages/jsp/user/user_edit_payments.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyPayeeSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_BILLPAYMENT_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(4);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});		
	}*/
});

$.subscribe("secondaryUsers.addPaymentsSuccess", function(event,data) {
	//console.info("secondaryUsers.addPaymentsSuccess-show confirmation dialog");
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.ADD_MODE,event);
	/*if(data){
		//var URL = "/cb/pages/jsp/user/user_addedit_confirm.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_addConfirm.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).html(response);
				ns.secondaryUsers.showAddEditConfirmDialog();
			}
		});		
	}*/
});

$.subscribe("secondaryUsers.savePaymentsSuccess", function(event,data) {
	//console.info("secondaryUsers.savePaymentsSuccess-show confirmation dialog");
	ns.secondaryUsers.moveToNextStep(ns.secondaryUsers.EDIT_MODE,event);
	/*if(data){
		//var URL = "/cb/pages/jsp/user/user_edit_confirm.jsp";
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyConfirm.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).html(response);
				ns.secondaryUsers.showAddEditConfirmDialog();
			}
		});		
	}*/
});

ns.secondaryUsers.loadViewPersonalInfo = function(mode){	
};

ns.secondaryUsers.loadViewAccounts = function(mode){			
	//console.info("loading accounts set up view");
	if(mode === ns.secondaryUsers.ADD_MODE){
		var aUrl = "/cb/pages/jsp/user/secondaryUserView_addAccountsSetup.action";
		$.ajax({
			  type: 'GET',
			  url:aUrl,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_ACCOUNT_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(1);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});
	}else{
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyAccountsSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				//console.info("Received user_edit_accounts.jsp");
				$(ns.secondaryUsers.selectors.WIZARD_TAB_ACCOUNT_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(1);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});
	}	
};

ns.secondaryUsers.loadViewTransfers = function(mode){	
	//console.info("loading transfers set up view");
	if(mode === ns.secondaryUsers.ADD_MODE){
		var URL = "/cb/pages/jsp/user/secondaryUserView_addTransferSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_TRANSFER_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(2);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});
	}else{
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyTransferSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_TRANSFER_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(2);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});
	}
};

ns.secondaryUsers.loadViewExTransfers = function(mode){	
	//console.info("loading ex transfers set up view");
	if(mode === ns.secondaryUsers.ADD_MODE){
		var URL = "/cb/pages/jsp/user/secondaryUserView_addExternalTransferSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_EXTRANSFER_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(3);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});
	}else{
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyExternalTransferSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_EXTRANSFER_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(3);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});
	}
};

ns.secondaryUsers.loadViewPayments = function(mode){	
	//console.info("loading payments set up view");
	if(mode === ns.secondaryUsers.ADD_MODE){
		var URL = "/cb/pages/jsp/user/secondaryUserView_addPayeeSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_BILLPAYMENT_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(4);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});
	}else{
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyPayeeSetup.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.WIZARD_TAB_BILLPAYMENT_SETUP_CONTAINER).html(response);
				ns.secondaryUsers.enableTab(4);
				$.publish("su_tabifyAndSetInitialFocus");
			}
		});
	}
};

ns.secondaryUsers.loadUserConfirmationView = function(mode){	
	//console.info("loadUserConfirmationView");
	if(mode === ns.secondaryUsers.ADD_MODE){
		var URL = "/cb/pages/jsp/user/secondaryUserView_addConfirm.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).html(response);
				ns.secondaryUsers.showAddEditConfirmDialog();
			}
		});
	}else{
		var URL = "/cb/pages/jsp/user/secondaryUserView_modifyConfirm.action";
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
				$(ns.secondaryUsers.selectors.ADD_EDIT_CONFIRM_DIALOG).html(response);
				ns.secondaryUsers.showAddEditConfirmDialog();
			}
		});
	}
};

/**
* This method moves wizard to next step
*/
ns.secondaryUsers.moveToNextStep = function(mode,event){	
	//ns.secondaryUsers.showMessage(event);
	var nextStep = event.originalEvent.data.response.data.nextStep;
	var prevStep = event.originalEvent.data.response.data.prevStep;	
	
	ns.secondaryUsers.nextStep.push(nextStep);
	ns.secondaryUsers.prevStep.push(prevStep)
	
	//console.info("moveToNextStep:" + nextStep);
	
	if(nextStep === 0){
		//initial view of users grid
	} else if(nextStep === 1){
		ns.secondaryUsers.loadViewPersonalInfo(mode);
	}else if(nextStep === 2){
		ns.secondaryUsers.loadViewAccounts(mode);
	}else if(nextStep === 3){
		ns.secondaryUsers.loadViewTransfers(mode);
	}else if(nextStep === 4){
		ns.secondaryUsers.loadViewExTransfers(mode);
	}else if(nextStep === 5){
		ns.secondaryUsers.loadViewPayments(mode);
	}else if(nextStep === 6){
		ns.secondaryUsers.loadUserConfirmationView(mode);
	}else if(nextStep === 7){
		//pending transactions
	}
}

/**
* This method moves wizard to previous step
*/
ns.secondaryUsers.moveToPrevStep = function(prevStep){	
	
}

ns.secondaryUsers.showMessage = function(event){	
	var message = event.originalEvent.data.response.message;
	if(message !=""){
		ns.common.showStatus(message);
	}
}

$.subscribe('secondaryUsers.clearErrors', function(event,data) {
	//console.info("secondaryUsers.clearErrors");
	$('.errorLabel').html('').removeClass('errorLabel');
	$('#formerrors').html('');
});

//Sec users admin functions
$.subscribe('secondaryUsers.resetQA', function(event,data) {
	//console.info("secondaryUsers.resetQA");
	var URL = "/cb/pages/jsp/user/resetQA.action";
	$.ajax({
		  type: 'GET',
		  url:URL,
		  success:function(response){
			ns.common.showStatus(js_secuser_updatesqa_message);
			}
	});
});

$.subscribe('secondaryUsers.unlock', function(event,data) {
	//console.info("secondaryUsers.resetQA");
	var URL = "/cb/pages/jsp/user/unlock.action";
	$.ajax({
		  type: 'GET',
		  url:URL,
		  success:function(response){
				//console.info("secondaryUsers.unlock, response:" + response);
				ns.common.showStatus(js_secuser_unlockuser_message);
				$("#unlockButtonLabel").html(js_secuser_unlockbutton_label);
				$("#unlockButtonContainer").hide();
		}
	});
});

$.subscribe("secondaryUsers.resetUsers", function(event,data) {
	//console.info("secondaryUsers.resetUsers");	
	$(ns.secondaryUsers.selectors.SECONDARY_USERS_GRID).trigger("reloadGrid");
});

$.subscribe("secondaryUsers.activateUsers", function(event,data) {
	//console.info("secondaryUsers.activateUsers");
	var URL = "/cb/pages/jsp/user/activateUsers.action";
	$.ajax({
		  type: 'POST',
		  data:$("#secondaryUsersForm").serialize(),
		  url:URL,
		  success:function(response){
				//console.info("secondaryUsers.activateUsersSuccess");
				if(response){
					response = $.trim(response);
					if(response === ""){
						//console.info("Status has been updated");
						ns.common.showStatus(js_secuser_statuschange_message);
					}else{
						//console.warn("There are some pending transactions payments");						
						$(ns.secondaryUsers.selectors.PENDING_TX_PAYEMENTS_DIALOG).html(response);
						var dialogConfig = ns.secondaryUsers.dialogConfig.pendingTxPaymentsDialog;
						$(ns.secondaryUsers.selectors.PENDING_TX_PAYEMENTS_DIALOG).dialog(dialogConfig);
					}
					
					$(ns.secondaryUsers.selectors.SECONDARY_USERS_GRID).trigger("reloadGrid");
				}
		}
	});
});

$.subscribe("secondaryUsers.TransferOrCancelTransactions", function(event,data) {
	//console.info("secondaryUsers.TransferOrCancelTransactions");
	var URL = "/cb/pages/jsp/user/transferOrCancelTransactions.action";
	$.ajax({
		  type: 'POST',
		  data:$("#transferCancelSaveStatusUserForm").serialize(),
		  url:URL,
		  success:function(response){
				//console.info("secondaryUsers.TransferOrCancelTransactions");
				ns.common.showStatus(js_secuser_statuschange_message);
				ns.secondaryUsers.closePendingTxPaymentsDialog();
			}
	});
});

$.subscribe("secondaryUsers.closePendingTxPayementsDialog", function(event,data) {
	//console.info("secondaryUsers.TransferOrCancelTransactions");
	ns.secondaryUsers.closePendingTxPaymentsDialog();
});

//Secondary Users grid
$.subscribe('secondaryUsers.secUsersGridOnComplete', function(event,data) {
	//console.info("secondaryUsers.secUsersGridOnComplete");
	var usersCount = $("#secondaryUsersGrid").jqGrid('getDataIDs').length || "";
	if(usersCount==""){
		$("#activeResetControlBox").hide();
	}else{
		$("#activeResetControlBox").show();
	}
	$("#secondaryUsersCount").val(usersCount);
	ns.secondaryUsers.rowCounter = 0;
});

/**
* Applies tabify and sets up the initial focus on wizard tabs
*/
$.subscribe("su_tabifyAndSetInitialFocus", function(event,data) {
	$.publish("common.topics.tabifyNotes");
	if(accessibility){
		$("#addEditSecondaryUsersTabs").setInitialFocus();
	}
});

$.subscribe('openUsersSummary', function(event,data) {
	/*$('#secondaryUsersContainer').show();
    $('#addEditSecondaryUsersContainer').slideUp();*/
    
    $.publish("secondaryUsers.closeWizard");
});
ns.secondaryUsers.activeColumnFormatter = function(cellvalue, options, rowObject){
	//console.info("secondaryUsers.activeColumnFormatter");
	var checked = "";
	if(rowObject.accountStatus==1){
		checked = "checked";
	}
	var checkBox = "<input type='checkbox' id='" + rowObject.id  + "' " + checked + " value='true' name='ActivateUser_"+ ns.secondaryUsers.rowCounter +"' />";
	ns.secondaryUsers.rowCounter++;
	return checkBox;	
}

ns.secondaryUsers.actionColumnFormatter = function(cellvalue, options, rowObject){	
	var viewLink = "<a  id='edit" + rowObject.id  + "' class='' title='" + js_edit + "' href='#' onClick='ns.secondaryUsers.editUser(\"" + rowObject.id  + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var trashLink = "<a id='delete" + rowObject.id  + "' class='' title='" + js_delete + "' href='#' onClick='ns.secondaryUsers.beforeDeleteUser(\"" + rowObject.id + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>"; 
	return  viewLink + ' ' + trashLink + ' ';
};

ns.secondaryUsers.selectors = {
	DELETE_USER_DIALOG: "#deleteSecondaryUserDialog",
	ADD_EDIT_CONFIRM_DIALOG: "#userAddEditConfirmDialog",
	TERMS_DIALOG:"#secondaryUsersTermsDialog",
	PENDING_TX_PAYEMENTS_DIALOG:"#pendingTxPayementsDialog",	
	TERMS_DIALOG_ACCEPT_BUTTON:"#acceptTermsSecondaryUsers",
	TERMS_DIALOG_CANCEL_BUTTON:"#cancelTermsSecondaryUsers",
	SECONDARY_USERS_CONTAINER:"#secondaryUsersContainer",
	ADDEDIT_SECONDARY_USERS_CONTAINER:"#addEditSecondaryUsersContainer",
	ADDEDIT_SECONDARY_USERS_TABS:"#addEditSecondaryUsersTabs",
	WIZARD_TAB_ACCOUNT_SETUP_CONTAINER:"#accountSetupContainer",
	WIZARD_TAB_TRANSFER_SETUP_CONTAINER:"#transferSetupContainer",
	WIZARD_TAB_EXTRANSFER_SETUP_CONTAINER:"#exTransferSetupContainer",
	WIZARD_TAB_BILLPAYMENT_SETUP_CONTAINER:"#billPaymentSetupContainer",
	SECONDARY_USERS_GRID:"#secondaryUsersGrid"
};

ns.secondaryUsers.isViewInitialized= false;
ns.secondaryUsers.termsDialogLoaded= false;
ns.secondaryUsers.addEditConfirmDialogLoaded= false;
ns.secondaryUsers.ADD_MODE = "add";
ns.secondaryUsers.EDIT_MODE = "edit";
ns.secondaryUsers.ACTIVE_MODE = "";
ns.secondaryUsers.rowCounter = 0;
ns.secondaryUsers.nextStep = [];
ns.secondaryUsers.prevStep = [];

ns.secondaryUsers.dialogConfig = {
	addEditConfirmDialog:{
		"title": "Add/Edit Confirmation",
		"dialogClass":"home_prefsDialog",
		"modal":true,
		"width":"800",
		"resizable":false
	},
	userAddEditConfirmDialog:
	{
		"title": "Edit Confirmation",
		"dialogClass":"home_prefsDialog",
		"modal":true,
		"width":"800",
		"resizable":false
	},	
	termsDialogConfig:{ 
		"title": jQuery.i18n.prop("termsAndConditions.termsofuse"),
		"dialogClass":"home_prefsDialog staticContentDialog",
		"modal":true,
		"width":"800",
		"resizable":false
	},
	deleteSecondaryUserDialog:{
		"title": "Delete Secondary User - Confirmation",
		"modal":true,
		"width":"800",
		"resizable":false
	},
	pendingTxPaymentsDialog:{
		"title": "Pending Transactions - Confirmation",
		"modal":true,
		"width":"800",
		"resizable":false
	}
}

ns.secondaryUsers.setup =function(){
	//console.info("secondaryUsers setup*****");
	ns.secondaryUsers.initView();
	ns.secondaryUsers.registerEvents();
};

$(document).ready(function() {
	ns.secondaryUsers.setup();
});

//Show the active dashboard based on the user selected summary
ns.secondaryUsers.showSecondaryUsersDashboard = function(actionType,secondaryUsersDashboardElementId){
	//Simulate dashboard item click event if any additional action is required
	if(secondaryUsersDashboardElementId && secondaryUsersDashboardElementId !='' && $('#'+secondaryUsersDashboardElementId).length > 0){
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+secondaryUsersDashboardElementId).trigger("click");}, 10);
	}
};