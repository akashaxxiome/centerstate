ns.users.setup = function()
{
	//ns.common.refreshDashboard('dbUsersSummary');
	
};
$(document).ready(function()
{
	$.debug(false);
	ns.users.setup();
});


//Show the active dashboard based on the user selected summary
ns.admin.showUsersDashboard = function(actionType, usersdashboardElementId){
	if(actionType == 'UsersSummary') {
		ns.common.refreshDashboard('dbUsersSummary');
	} else if (actionType == 'ProfilesSummary') {
		ns.common.refreshDashboard('dbProfilesSummary');	
	} else {
		ns.common.refreshDashboard('dbUsersSummary');
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(usersdashboardElementId && usersdashboardElementId !='' && $('#'+usersdashboardElementId).length > 0){
		//$("#"+transferDashboardElementId).trigger("click");
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+usersdashboardElementId).trigger("click");}, 10);
	}
};

$.subscribe('successUserLoad', function(event, data){
	ns.common.selectDashboardItem("addUserLink");
});

$.subscribe('successProfileLoad', function(event, data){
	ns.common.selectDashboardItem("addProfileLink");
});
$.subscribe('successBusinessProfileLoad', function(event, data){
	ns.common.selectDashboardItem("addBusinessProfileLink");
});
/**
 * Close Details island and expand Summary island for Admin - Profiles
 */
ns.admin.closeProfileDetails = function(){
    $('#details').slideUp();
    $("#summary").hide();
    $('#profileSummary').show();
};

$.subscribe('onLoadUsersSummaryLoadedSuccess', function(event,data){
	
	$("#summary").show();
	$("#details").hide();
	$("#permissionsDiv").hide();
	$("#profileSummary").hide();
});


$.subscribe('onLoadProfileSummaryLoadedSuccess', function(event,data){
	
	$("#summary").hide();
	$("#details").hide();
	$("#permissionsDiv").hide();
	$("#profileSummary").show();
});

$.subscribe('cancelProfileForm', function(event, data)
{
	ns.admin.closeProfileDetails();
	ns.common.hideStatus();
	ns.common.selectDashboardItem("profilesLink");
});


$.subscribe('pendingDAChangesGridCompleteEventsForUsers', function(event,data) {
	var pId = data.p.id;
	var isGridSearchActive = ns.common.isGridSearchActive(pId);
	if(isGridSearchActive != true){
		if($("#"+pId).jqGrid('getGridParam', 'reccount') == 0){
			$('#admin_pendingUsersTab').hide();
			$('#userBlankDiv').hide();
		}else{
			$('#admin_pendingUsersTab').slideDown();
			$('#userBlankDiv').show();
		}	
	}
	
	//ns.admin.addRejectReasonRowsForGroupPendingChanges('#pendingGroupChangesID');
	ns.admin.addRejectReasonRows("#usersWithPendingChangesGrid");
 	ns.common.resizeWidthOfGrids();	
});