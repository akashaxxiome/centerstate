	$(document).ready(function(){
		$('#customMappingDetails').hide();
		$('#mappingDiv').hide();
	});

ns.cash.resizeWidthOfGridCashSummary = function(){
		if( $("#summary").length > 0 ){
			ns.common.resizeWidthOfGrids();
		}
	};

ns.cash.refreshRPPayGrid = function(urlString){
		$.ajax({    
		  url: urlString,     
		  success: function(data) {  
			 $("#firstDiv").html(data);
			 $("#secondDiv").hide();
			 ns.cash.gotoStep(1);
			 
			 $.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#notes").setFocus();
			}
		  }   
		});   
		ns.cash.gotoStep(1);
	};
	
	//need to send selected value of account to server
ns.cash.refreshRPPayListGrid = function(urlString, selectedValue){
		$.ajax({    
		  url: urlString,
		  data: {"selectedValue":selectedValue},
		  success: function(data) {  
			 $("#allTypesOfAccount").html(data);
		  }   
		});   
	};
	
	
ns.cash.refreshDashboardForm = function(urlString){
		$.ajax({    
		  url: urlString,     
		  success: function(data) {  
			 $("#appdashboard").html(data);
		  }   
		});   
	};

	//formatter the date
ns.cash.dateFormatter = function(cellvalue, options, rowObject){
		var date = "";
		if(cellvalue == null || cellvalue==""){
			date = "---";
		} else {
			date = cellvalue.substr(0,10);
		}
		return date;
	};


	//ppay exception grid complete event.
	$.subscribe('RPPayExceptionEvent', function(event,data) {
	    // refresh the approval small panel in left bar
	    $.publish('refreshApprovalSmallPanel');
		
		ns.cash.formatRPPayAccountColumn("#rppayExceptionGridID");
		//$("#rppayExceptionGridID_pager_center").hide(); //no need to hide pager as page size is 1000, QTS : 694005
		ns.cash.resizeWidthOfGridCashSummary();
		$('#firstDiv').slideDown();
		//QTS:709292.remove the hand pointer
		$('div[id="jqgh_map.pay"]').removeClass('ui-jqgrid-sortable');
		$('div[id="jqgh_map.return"]').removeClass('ui-jqgrid-sortable');

		$.publish("common.topics.tabifyNotes");
	});
	
	//ppay exception grid complete event.
	$.subscribe('RPPayApprovExceptionEvent', function(event,data) {
	    // refresh the approval small panel in left bar
	    $.publish('refreshApprovalSmallPanel');
		
		ns.cash.formatRPPayAccountColumn("#rppayApprovalExceptionGridID");
		
		ns.cash.resizeWidthOfGridCashSummary();
		$('#firstDiv').slideDown();
		
		$('div[id="jqgh_map.pay"]').removeClass('ui-jqgrid-sortable');
		$('div[id="jqgh_map.return"]').removeClass('ui-jqgrid-sortable');

		$.publish("common.topics.tabifyNotes");
	});
	
	$.subscribe('RPPayExceptionConfirmEvent', function(event,data) {
		ns.cash.formatRPPayAccountColumn("#rppayExceptionConfirmGridID");
		//$("#ppayExceptionConfirmGridID_pager_center").hide(); //no need to hide pager as page size is 1000, QTS : 694005
		
		//Need this code here, somehow exceptionGridSubmitSuccessTopic topic doesn't get called.
		ns.cash.gotoStep(2);
		$('#rpPayList').hide();
		
		ns.cash.resizeWidthOfGridCashSummary();
		$('#secondDiv').slideDown();
		$.publish("common.topics.tabifyNotes");
	});
	
	$.subscribe('RPPayExceptionCompleteEvent', function(event,data) {
		ns.cash.formatRPPayAccountColumn("#rppayExceptionCompleteGridID");
		//$("#ppayExceptionConfirmGridID_pager_center").hide(); //no need to hide pager as page size is 1000, QTS : 694005
		
		//Need this code here, somehow exceptionGridSubmitSuccessTopic topic doesn't get called.
		ns.cash.gotoStep(3);
		$('#rpPayList').hide();
		
		ns.cash.resizeWidthOfGridCashSummary();
		$('#secondDiv').hide();
		$.publish("common.topics.tabifyNotes");
	});
	
	//ppay exception grid complete event.
	$.subscribe('RPPayExceptionForAccountEvent', function(event,data) {
		ns.cash.formatRPPayAccountColumn("#rppayExceptionForAccountGridID");
		//$("#ppayExceptionForAccountGridID_pager_center").hide(); //no need to hide pager as page size is 1000, QTS : 694005
		ns.cash.resizeWidthOfGridCashSummary();
		$('#firstDiv').slideDown();
		//QTS:709292.remove the hand pointer
		$('div[id="jqgh_map.pay"]').removeClass('ui-jqgrid-sortable');
		$('div[id="jqgh_map.return"]').removeClass('ui-jqgrid-sortable');
		$.publish("common.topics.tabifyNotes");
	});
	//ppay summary grid complete event.
	$.subscribe('RPPaySummaryEvent', function(event,data) {
		ns.cash.formatRPPaySummaryAccountColumn("#rppaySummaryGridID");
		ns.cash.resizeWidthOfGridCashSummary();
	});
	
	
	$.subscribe('exceptionGridSubmitSuccessTopic', function(event,data) {
		ns.cash.gotoStep(2);
		$('#rpPayList').hide();
	});
	
	$.subscribe('exceptionGridSubmitErrorTopic', function(event,data) {
		//console.log('exceptionGridSubmitErrorTopic');
	});
	
	$.subscribe('exceptionGridSubmitCompleteTopic', function(event,data) {
		//console.log('exceptionGridSubmitCompleteTopic');
	});
	
	$.subscribe('exceptionGridSubmitVerifySuccessTopic', function(event,data) {
		ns.cash.gotoStep(3);
		$('#rpPayList').hide();
	});
	
	$.subscribe('exceptionGridSubmitVerifyErrorTopic', function(event,data) {
		//TODO
	});
	
	$.subscribe('exceptionGridSubmitVerifyCompleteTopic', function(event,data) {
		//TODO
	});
	
	$.subscribe('cancelForm', function(event,data) {
		//TODO
	});
	
	
	$.subscribe('exceptionGridSubmitConfirmComplete', function(event,data) {
		ns.common.showStatus();
		
		ns.common.setFirstGridPage('#rppaySummaryGridID');//set grid page param to 1 to reload grid from first
		$('#rppaySummaryGridID').trigger("reloadGrid");
		
		ns.common.setFirstGridPage('#rppayExceptionGridID');//set grid page param to 1 to reload grid from first
		$('#rppayExceptionGridID').trigger("reloadGrid");
		
		ns.common.resizeWidthOfGrids();
		
		ns.cash.gotoStep(1);
		$('#rpPayList').show();
	});
	
	//formatter for ppay account column
	ns.cash.formatRPPayAccountColumn = function(gridID){
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			for(var i=0;i<ids.length;i++){  
				var rowId = ids[i];
				//Construct "To Account Nickname" column.
				var _toAccountRoutingNumber = $(gridID).jqGrid('getCell', rowId, 'map.routingNumber');
				var _toAccountID = $(gridID).jqGrid('getCell', rowId, 'checkRecord.accountID');
				var _toAccountDisplayText = $(gridID).jqGrid('getCell', rowId, 'map.accountDisplayText');
				var _toAccountNickName = $(gridID).jqGrid('getCell', rowId, 'map.nickName');
				var _toAccountCurrencyType = $(gridID).jqGrid('getCell', rowId, 'map.currencyType');
				var _toAccountName = "";
				
				if(_toAccountDisplayText != undefined && _toAccountDisplayText !=''){
					_toAccountName = _toAccountDisplayText;
				}
				else{
					_toAccountName = _toAccountRoutingNumber;
					if( _toAccountID.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountID;
					} 
					if( _toAccountNickName.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountNickName;
					} 
					if( _toAccountCurrencyType.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountCurrencyType;
					}
				}
				
				$(gridID).jqGrid('setCell', rowId, 'account', _toAccountName);
			}
		};
		
	ns.cash.formatRPPaySummaryAccountColumn = function(gridID){
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			for(var i=0;i<ids.length;i++){  
				var rowId = ids[i];
				//Construct "To Account Nickname" column.
				var _toAccountRoutingNumber = $(gridID).jqGrid('getCell', rowId, 'PPayAccount.routingNumber');
				var _toAccountDisplayText = $(gridID).jqGrid('getCell', rowId, 'PPayAccount.accountDisplayText');
				var _toAccountNickName = $(gridID).jqGrid('getCell', rowId, 'PPayAccount.nickName');
				var _toAccountCurrencyType = $(gridID).jqGrid('getCell', rowId, 'PPayAccount.currencyType');
				var linkVar = $(gridID).jqGrid('getCell', rowId, 'map.ViewURL');
				
				var _toAccountName = "";
				
				if(_toAccountDisplayText != undefined && _toAccountDisplayText !=''){
					_toAccountName = _toAccountDisplayText;
				}
				else{
					_toAccountName = _toAccountRoutingNumber;
					
					if( _toAccountNickName.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountNickName;
					} 
					if( _toAccountCurrencyType.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountCurrencyType;
					}
				}
				
				var link = "<a href=\"#\" onClick=\"ns.cash.refreshExceptionGridForAccount('"+linkVar+"')\" >" + _toAccountName + "</a>";
				
				$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_account']:eq("+i + ")").html(link);
			}
		};
	
	ns.cash.searchImageFormatter = function(cellvalue, options, rowObject){ 
		var checkNumber = "<a href=\"#\" onClick=\"ns.cash.openImageWindow('"+rowObject["map"].ViewURL+"')\" >" + cellvalue + "</a>";
		return ' ' + checkNumber;
	};
	
ns.cash.openImageWindow = function(urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#searchImageDialog').html(data).dialog('open');
			}
		});
	};
	
ns.cash.zoomImage = function(urlString){
	$.ajax({    
		url: urlString,     
		success: function(data) {  
		//ns.common.closeDialog();
		//$("#searchImageDialog").html(data).dialog('open');
		$('#imgHolder').html(data);
		}   
	});   
};
	
ns.cash.rotationFunction = function(urlString){
	$.ajax({    
		url: urlString,     
		success: function(data) {  
		//ns.common.closeDialog();
		//$("#searchImageDialog").html(data).dialog('open');
		$('#imgHolder').html(data);
		
		}   
	});   
};

ns.cash.rppayZoomImage = function(urlString){
	$.ajax({
		url: urlString,
		success: function(data) {
		//ns.common.closeDialog();
		//$("#searchImageDialog").html(data).dialog('open');
		$('#imgRGridHolder').html(data);
		}
	});
};

ns.cash.rppayRotationFunction = function(urlString){
	$.ajax({
		url: urlString,
		success: function(data) {
		//ns.common.closeDialog();
		//$("#searchImageDialog").html(data).dialog('open');
		$('#imgRGridHolder').html(data);

		}
	});
};

	
ns.cash.RadioButtonPayFormatter = function(cellvalue, options, rowObject){ 
    var Pay = "";
    var rowId = options.rowId;  
    var defaultdecision = rowObject["map"].defaultdecision;
    var currentDecision = rowObject["map"].currentDecision;
    var finalDecision = defaultdecision;
    if(currentDecision!=null && currentDecision!=''){
    	finalDecision = currentDecision;	
    }
    
    if(finalDecision == "Pay"){
		Pay = "<input onclick=\"disabledDropDown("+rowId+");\" type=\"radio\" class=\"payclass\" checked value=\"Pay\" name=\"decision" +cellvalue+ "\" >";
    } else
    {
        Pay = "<input  onclick=\"disabledDropDown("+rowId+");\" type=\"radio\" class=\"payclass\" value=\"Pay\" name=\"decision" +cellvalue+ "\" >";
    }
    
	return ' ' + Pay;
};

ns.cash.RadioButtonReturnFormatter = function(cellvalue, options, rowObject) { 
    var Return = "";
    var rowId = options.rowId;  
    var defaultdecision = rowObject["map"].defaultdecision;
    var currentDecision = rowObject["map"].currentDecision;
    var finalDecision = defaultdecision;
    if(currentDecision!=null && currentDecision!=''){
    	finalDecision = currentDecision;	
    }
    
    if(finalDecision == "Return"){
		Return = "<input id=\"returnId"+rowId+"\" onclick=\"enabledDropDown("+rowId+");\" type=\"radio\" class=\"returnclass\" checked value=\"Return\" name=\"decision" +cellvalue+ "\" >";
    } else
    {
        Return = "<input id=\"returnId"+rowId+"\" onclick=\"enabledDropDown("+rowId+");\" type=\"radio\" class=\"returnclass\" value=\"Return\" name=\"decision" +cellvalue+ "\" >";
    }
    
	return ' ' + Return;
};

ns.cash.RadioButtonHoldFormatter = function(cellvalue, options, rowObject) { 
	var hold = "";
	var rowId = options.rowId;
    var defaultdecision = rowObject["map"].defaultdecision;
    var currentDecision = rowObject["map"].currentDecision;
    var finalDecision = defaultdecision;
    if(currentDecision!=null && currentDecision!=''){
    	finalDecision = currentDecision;	
    }
    
    if(finalDecision == "Hold"){
		hold = "<input onclick=\"disabledDropDown("+rowId+");\" type=\"radio\" class=\"holdclass\" checked value=\"Hold\" name=\"decision" +cellvalue+ "\" >";
    } else
    {
        hold = "<input onclick=\"disabledDropDown("+rowId+");\"  type=\"radio\" class=\"holdclass\" value=\"Hold\" name=\"decision" +cellvalue+ "\" >";
    }
    
	return ' ' + hold;
};

function disabledDropDown(rowId) {
	 $('#rejectReasonsId'+rowId).attr('disabled', 'disabled');
}

function enabledDropDown(rowId) {
	 $('#rejectReasonsId'+rowId).removeAttr('disabled');
}

ns.cash.RadioButtonPayFormatterConfirm = function(cellvalue, options, rowObject) { 
		
		var Pay = "";
		
		if(rowObject["decision"] == "Pay"){
			Pay = "<input type=\"radio\" checked value=\"Pay\" name=\"decision" +cellvalue+ "\" disabled>";
		}else{
			Pay = "<input type=\"radio\" value=\"Pay\" name=\"decision" +cellvalue+ "\" disabled>";
		}
			
		return ' ' + Pay;
	};
ns.cash.RadioButtonReturnFormatterConfirm = function(cellvalue, options, rowObject) { 
		
		var Return = "";
		
		if(rowObject["decision"] == "Return"){
			Return = "<input type=\"radio\" checked value=\"Return\" name=\"decision" +cellvalue+ "\" disabled>";
		}else{
			Return = "<input type=\"radio\" value=\"Return\" name=\"decision" +cellvalue+ "\" disabled>";
		}	
		return ' ' + Return;
	};
	
	 ns.cash.OptionFormatter = function(cellvalue, options, rowObject) { 
		 var rowId = options.rowId;  
		 var optoinListHtml = "<select id=\"rejectReasonsId"+rowId+"\" class=\"rejectReasonsclass\" name=\"rejectReason" + rowObject["map"]["return"] + "\" disabled ></select>";         
		 return optoinListHtml;
	};
	
ns.cash.refreshExceptionGridForAccount = function(urlString){
		$.ajax({    
		  url: urlString,     
		  success: function(data) {  
			 $("#firstDiv").html(data);
			 $("#secondDiv").hide();
			 ns.cash.gotoStep(1);
		  }   
		});   
	};

	
	$.subscribe('deleteMappingComplete', function(event,data) {
		ns.common.closeDialog("#deleteMappingDialogID");
		$('#custommappingGridID').trigger("reloadGrid");
		ns.common.showStatus();
		
	});
	
	$.subscribe('mappingSuccess', function(event,data) {
		$('#rpPaysummary').hide();
		$('#rpPayList').hide();
		$('#mappingDiv').show();
		$('#customMappingDetails').hide();
	});
	$.subscribe('addMappingSuccessCash', function(event,data) {
		$('#rpPaysummary').hide();
		$('#mappingDiv').hide();
		$('#rpPayList').hide();
		$('#customMappingDetails').show();
	});
	
	$.subscribe('buildRPPayFormBefore', function(event,data) {
		removeValidationErrors();
	});
	
	$.subscribe('buildRPPayVerifySuccess', function(event,data) {
		ns.cash.gotoStep(3);
		ns.common.showStatus($(data).find("#rppayMessage").html());
	});
	
	$.subscribe('buildRPPayVerifyError', function(event,data) {
	});
	
	$.subscribe('buildRPPayVerifyComplete', function(event,data) {
	});
	
	$.subscribe('customMappingBefore', function(event,data) {
		removeValidationErrors();	
	});
	
	$.subscribe('uploadCheckingRecordSuccess', function(event,data) {
		//$('#rpPayList').show();
		
		var heading = $('#uploadInputTab').html();
        var $title = $('#inputTab a');
        $title.attr('title',heading);
        $title = $('#inputTab a span');
        $title.html(heading);

        heading = $('#uploadVerifyTab').html();
        $title = $('#verifyTab a');
        $title.attr('title',heading);
        $title = $('#verifyTab a span');
        $title.html(heading);

//        $('#inputDiv').html("");            // before we load, clear previous contents
//        $('#verifyTab').html("");            // before we load, clear previous contents
        $('#verifyTab').show();

        
        //set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
        heading = $('#PageHeading').html();
        $title = $('#rpPaysummary .portlet-title');
        $title.html(heading);
        ns.common.updatePortletTitle("rppay_workflowID",heading,false);
        
		$('#rpPayList').hide();
		$('#rpPaysummary').show();
		$('#mappingDiv').hide();
		$('#customMappingDetails').hide();
		$("#rppay_workflowID").find(".wizardStatusContainerCls").hide();
		
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#RPPayWizardTabs").setFocusOnTab();
		}
		
		$('#approvalRPPaySummaryGrid').hide();
		$('#pendingRPPaySummaryGrid').show();
		
	});
	
	$.subscribe('onClickUploadCheckingRecord', function(event,data) {
		//load import dialog if not loaded
		var config = ns.common.dialogs["fileImport"];
		config.autoOpen = "false";
		ns.common.openDialog("fileImport");
		
		var configImportResults = ns.common.dialogs["fileImportResults"];
		configImportResults.autoOpen = "false";
		ns.common.openDialog("fileImportResults");	
	});
	
	$.subscribe('buildManualFileSuccess', function(event,data) {
		$('#rpPayList').hide();
		//$('#rppaybuildLink').hide();
		
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			//$("#RPPayWizardTabs").setFocusOnTab();
			$("#RPPayWizardTabs").setFocus();
		}
		$("#rppaySummaryBtn").find("span").removeClass("dashboardSelectedItem");
		$("#rppaybuildLink").find("span").addClass("dashboardSelectedItem");
	});
	
	
	$.subscribe('mappingListDoneSuccessCash', function(event,data) {
		$('#appdashboard').show();
		//$('#rpPayList').show();
		$('#rpPayList').hide();
		//$('#rpPaysummary').hide();
		$('#rpPaysummary').show();
		$('#mappingDiv').hide();
		$('#customMappingDetails').hide();
		
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#RPPayWizardTabs").setFocusOnTab();
		}
	});
	
	
ns.cash.gotoStep = function(stepnum){
	
		ns.common.gotoWizardStep('rppay_workflowID',stepnum);
	
		var tabindex = stepnum - 1;
		$('#RPPayWizardTabs').tabs( 'enable', tabindex );
		$('#RPPayWizardTabs').tabs( 'option','active', tabindex );
		$('#RPPayWizardTabs').tabs( 'disable', (tabindex+1)%3 );
		$('#RPPayWizardTabs').tabs( 'disable', (tabindex+2)%3 );
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#RPPayWizardTabs").setFocusOnTab();
		}
	};
	
	$.subscribe('addMappingFormComplete', function(event,data) {
		ns.cash.gotoStep(2);
	});
	
	$.subscribe('addMappingFormComplete2', function(event,data) {
		$('#rpPaysummary').hide();
		$('#appdashboard').show();
		$('#mappingDiv').show();
		//$('#custommappingGridID').trigger("reloadGrid");
		
	});
	
	$.subscribe('reloadReversePositivePayPortletGrid', function(event,data) {
		//retrive dynamic grid of positive pay portlet
		var reversePositivePayPortletGridId = $("#reversePositivePayPortletDataDiv .ui-jqgrid").prop('id').substring(5);
		$('#'+reversePositivePayPortletGridId).trigger("reloadGrid"); //reload grid
	});
	
	$.subscribe('FileUploadFormComplete', function(event,data) {
		ns.common.closeDialog("#uploadFileDialogID");
		ns.common.showStatus();
	});
	
	$.subscribe('onUploadCancelButton', function(event,data) {
		ns.shortcut.goToMenu('cashMgmt_rppay');
		/*
		ns.cash.refreshDashboardForm(ns.cash.originalDashboardFormURL);
		ns.cash.refreshRPPayListGrid(ns.cash.refreshPPayListGridURL);
		
		// UPLOAD might change the names of the tabs, so change them back.
        var heading = $('#normalInputTab').html();
        var $title = $('#inputTab a');
        $title.attr('title',heading);
        $title = $('#inputTab a span');
        $title.html(heading);

        heading = $('#normalVerifyTab').html();
        $title = $('#verifyTab a');
        $title.attr('title',heading);
        $title = $('#verifyTab a span');
        $title.html(heading);
		
        //set portlet title
        heading = $('#normalSummaryTab').html();
        $title = $('#rpPaysummary .portlet-title');
        //$title.html(heading);
        ns.common.updatePortletTitle("rppay_workflowID",heading,false);
        
		$('#rpPayList').show();
		$('#rpPaysummary').show();
		ns.cash.refreshRPPayGrid(ns.cash.refreshRPPayGridURL);
		*/
	});
	
	$.subscribe('rpPayExpSubmitForm', function(event,data) {
		$.ajaxSetup({
			async : false
		});
		var selectedValue = $('#rppaySummaryGridID').val();
		
		var urlString ='/cb/pages/jsp/reversepositivepay/getReversePositivePaySummariesAction.action';
		ns.cash.refreshRPPayListGrid(urlString,selectedValue);
		$('#rppaySummaryGridID').val(selectedValue);
		
		if($('#rppaySummaryGridID').val() == null) {
			selectedValue = '';
		}

		if(ns.common.isInitialized($('#rppaySummaryGridID'),'ui-selectmenu')){
			$('#rppaySummaryGridID').selectmenu("destroy");	
		}	

		if(selectedValue!=''){
			var accountIdIndex = selectedValue.indexOf("A");
			var bankIndex = selectedValue.indexOf("B");
			var accountId = selectedValue.substring(accountIdIndex+1,bankIndex);
			var bankId = selectedValue.substring(bankIndex+1);
			var url ='/cb/pages/jsp/reversepositivepay/getReversePositivePayIssuesForAccountAction_init.action?accountID='+accountId+"&bankID="+bankId;
			ns.cash.refreshExceptionGridForAccount(url);
		}
		else{
			$('#rppaySummaryGridID').trigger("change");
		}
		
		$("#rppaySummaryGridID").selectmenu({width: 350});
		
		$.ajaxSetup({
			async : true
		});		
	});
	
	$.subscribe("changeActionNameBackTopic",function(e,d){
		document.emailFormName.action = "/cb/pages/jsp/reversepositivepay/EmailImagesAction_execute.action";
	});

	$.subscribe("changeActionNameTopic",function(e,d){
		document.emailFormName.action = "/cb/pages/jsp/reversepositivepay/EmailImagesAction_cancelEmail.action";
		ns.common.closeDialog();
	});

	$.subscribe('emailImageCompleteTopic', function(event,data) {
		ns.common.closeDialog("searchImageDialog");
	});
	
	$.subscribe('pendingApprovalGridCompleteEvents', function(event,data) {
		// refresh the approval small panel in left bar
		if($('#isConsumerUserLoggedIn').length != '0'){
			$.publish('refreshApprovalSmallPanel');		
		}
		ns.common.resizeWidthOfGrids();
		ns.wire.addApprovalsRowsForPendingApproval("#rppayApprovalExceptionGridID");
	});
	
	
	//Add approval information in the pending approval summary gird.
	ns.wire.addApprovalsRowsForPendingApproval = function(gridID){
			
			var approvalsOrReject = "";
			var approvalerNameOrRejectReason = "";
			var rejectedOrSubmitedBy = "";
			var approverNameOrUserName = "";
			var approverGroup = "";
			var submittedForTitle = js_submitted_for;
			
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			for(var i=0;i<ids.length;i++){  
			
				var rowId = ids[i];
				var approverName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverName'));
				var userName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'userName'));
				var approverIsGroup = $(gridID).jqGrid('getCell', rowId, 'approverIsGroup');
				var status = $(gridID).jqGrid('getCell', rowId, 'status');
				var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
				var submittedFor = $(gridID).jqGrid('getCell', rowId, 'resultOfApproval');
				var transType = $(gridID).jqGrid('getCell', rowId, 'map.transType');
				var curRow = $("#" + rowId, $(gridID));
				
				if( approverIsGroup == 'true'){
					approverGroup = "(group)";
				}
				

				if( status == 101 ){
					approvalsOrReject = js_reject_reason;
					rejectedOrSubmitedBy = js_rejected_by;
					if(rejectReason==''){
						approvalerNameOrRejectReason = "No Reason Provided";
					} else{
						approvalerNameOrRejectReason = rejectReason;
					}
					approverNameOrUserName = approverName + approverGroup;
				
					if(approverName != ""){	
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}
						
						className = 'ui-widget-content jqgrow ui-row-ltr';
						curRow.after(ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameOrUserName,approvalsOrReject,approvalerNameOrRejectReason,className,10));
					} else {
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}						
						className = 'ui-widget-content jqgrow ui-row-ltr';
						
						curRow.after(ns.common.addRejectReasonRow('','',approvalsOrReject,approvalerNameOrRejectReason,className,10));
					}			
				//if status is not 101(WireDefines.WIRE_STATUS_APPROVAL_REJECTED), means that the transfer's status is not rejected.
				} else {
					approvalsOrReject = js_approval_waiting_on;
					approvalerNameOrRejectReason = approverName + approverGroup;
					rejectedOrSubmitedBy = js_submitted_by;	
					approverNameOrUserName = userName;
				
					if(approverName != ""){	
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}
						
						className = 'ui-widget-content jqgrow ui-row-ltr';
						curRow.after(ns.common.addSubmittedForRow(approvalsOrReject,approvalerNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,10));
					} 
				}
			}
	};

	$.subscribe('backToInput', function(event, data) {
		ns.cash.gotoStep(1);
	});