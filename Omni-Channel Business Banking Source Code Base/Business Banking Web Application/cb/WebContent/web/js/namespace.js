var Namespace =
{
    Register: function(_Name)
    {
        var chk = false;
        var cob = "";
        var spc = _Name.split(".");
        for(var i = 0; i < spc.length; i++)
        {
            if(cob != "")
			{
				cob += ".";
			}
            cob += spc[i];
            chk = this.Exists(cob);
            if(!chk)
			{
				this.Create(cob);
			}
        }
        if(chk)
		{ 
			throw "Namespace: " + _Name + " is already defined."; 
		}
    },

    Create: function(_Src)
    {
        eval("window." + _Src + " = new Object();");
    },

    Exists: function(_Src)
    {
        eval("var NE = false; try{if(" + _Src + "){NE = true;}else{NE = false;}}catch(err){NE=false;}");
        return NE;
    },
	
	Clear: function(_Src)
	{
		eval("window." + _Src + " = undefined;");
	}
};


Namespace.Register("ns.login");
Namespace.Register("ns.logout");
//common facilities
Namespace.Register("ns.common");
//Home/landing page 
Namespace.Register("ns.home");
Namespace.Register("ns.alert");
Namespace.Register("ns.message");
Namespace.Register("ns.shortcut");
//Account Management Module
Namespace.Register("ns.account");
//Cash Management Module
Namespace.Register("ns.cash");
//Payments & Transfers
Namespace.Register("ns.transfer");
Namespace.Register("ns.wire");
Namespace.Register("ns.ach");
Namespace.Register("ns.childsp");
Namespace.Register("ns.tax");
Namespace.Register("ns.billpay");
Namespace.Register("ns.cashcon");
Namespace.Register("ns.stops");
//Admin Module
Namespace.Register("ns.admin");
Namespace.Register("ns.company");
Namespace.Register("ns.division");
Namespace.Register("ns.groups");
Namespace.Register("ns.users");
//Approval Module
Namespace.Register("ns.approval");
//Reporting Module
Namespace.Register("ns.report");
//Foreign Exchange Utility Module
Namespace.Register("ns.fx");
//Online Statement module
Namespace.Register("ns.onlineStatements");
//Image Search module
Namespace.Register("ns.imagesearch");
Namespace.Register("ns.previouslyrequested");
//Service Center Module
Namespace.Register("ns.servicecenter"); 
//User Preference module
Namespace.Register("ns.userPreference");
Namespace.Register("ns.checkimage");
Namespace.Register("ns.contactpoints");
Namespace.Register("ns.useralerts");
//Namespace used for Pending Transactions portal
Namespace.Register("ns.pendingTransactionsPortal");
//Namespace for history plugin
Namespace.Register("ns.history");
//Secondary Users
Namespace.Register("ns.secondaryUsers");
//Layout
Namespace.Register("ns.layout");