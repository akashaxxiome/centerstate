;(function($){
/**
 * full Calendar Chinese Translation
 */
	$.fullCalendar.setDefaults({
		isRTL: false,
		firstDay: 1,
		monthNames: ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708','\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
		monthNamesShort: ['\u4E00','\u4E8C','\u4E09','\u56DB','\u4E94','\u516D','\u4E03','\u516B','\u4E5D','\u5341','\u5341\u4E00','\u5341\u4E8C'],
		dayNames: ['\u661F\u671F\u65E5','\u661F\u671F\u4E00','\u661F\u671F\u4E8C','\u661F\u671F\u4E09','\u661F\u671F\u56DB','\u661F\u671F\u4E94','\u661F\u671F\u516D'],
		dayNamesShort: ['\u65E5','\u4E00','\u4E8C','\u4E09','\u56DB','\u4E94','\u516D'],
		buttonText: {
			prev: '&nbsp;&#9668;&nbsp;',
			next: '&nbsp;&#9658;&nbsp;',
			prevYear: '&nbsp;&lt;&lt;&nbsp;',
			nextYear: '&nbsp;&gt;&gt;&nbsp;',
			today: '\u4ECA \u5929 ', 
			month: '\u6708',
			week: '\u5468',
			day: '\u65E5'
		},
		allDayText: '\u5168\u5929',
		moreText: '\u66F4\u591A...'
	});	
})(jQuery);
