var logout = (function(){
	function _initView(){
		_initLayout();
		var openSecurityDialog = function(){
			$("#security").dialog('open');
		}

		/*$("#logoutPanel").pane({
				"title":js_bank_title,
				"minmax":false,
				"close":false,
				"customIcons":"ui-icon-locked",
				"customTips":js_tooltip_security,
				"customCallbacks":openSecurityDialog
		});*/
	};
	
	function _initLayout(){
		$('body').layout({ 
			//applyDefaultStyles: true,
			north:{
				size:100,
				resizable :false,
				closable :false
			},
			south:{
				size:35,
				resizable :false,
				closable :false
			}				
		});
	};
	
	return {
		init:function(){
			_initView();
		}
}
	
}());

$(document).ready(function() {
	logout.init();
});