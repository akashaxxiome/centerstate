function customValidation(form, errors) {
try{ 
        //List for errors
        var list = $('#formerrors');
        
        //Handle non field errors 
        if (errors.errors) {
                $.each(errors.errors, function(index, value) { 
                        list.append('<li class="errorLabel">'+value+'</li>\n');
                });
        }
        
        //Handle field errors 
        if (errors.fieldErrors) {
                $.each(errors.fieldErrors, function(index, value) { 
						index = index.replace(".", "\\.");
                        var elem = $('#'+index+'Error');
                        if(elem)
                        {
                                elem.html(value[0]);
                                elem.addClass('errorLabel');
                        }
                });
        }
		}catch(err) {	
		console.warn("Error "+err.message);
	}
}


function removeValidationErrors(){
	 $('.errorLabel').html('').removeClass('errorLabel');
     $('#formerrors').html('');

}