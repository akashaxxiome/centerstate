var loginController = (function(){
	function _initView(){
		_initLayout();
		if (self == top) {
			document.documentElement.style.display = 'block';
		} else {
			top.location = self.location;
		}
				
		if($("#languagesDropdown")){
			var iconsArr = new Array();
			var iconsArrN = 0;
			$("#languagesDropdown").find('option').each(function(){
				iconsArr[ iconsArrN ] = {find: "." + $(this).attr('value')};
				iconsArrN++;
			});
			
			$("#languagesDropdown").selectmenu({
				icons: iconsArr,
				width: 210
			});
			
		}		

		$.subscribe('onClickSubmitButton',function() {
			removeValidationErrors();
		});
		
		$.subscribe('resetUserName',function() {
			var url = "/cb/pages/jsp-ns/authenticateInit.action";
			$.ajax({
				type: "GET",
				url: url
			});
		});

		$.subscribe('removeValidationErrors',function() {
			removeValidationErrors();
		});

		$.subscribe('goBackToLoginStep1',function() {
			loginController.changeLoginPanelTitle(js_login_step1);
		});

		
		//$('#stepLabelSpanID').html('<s:text name="jsp.login_48"/>');
		
		var openSecurityDialog = function(){
			$("#security").dialog('open');			
		};
		/*$.subscribe('initLoginStep1Pane',function() {
			$("#signin_menu").pane({
				"title":js_login_step1,
				"minmax":false,
				"close":false,
				"customIcons":"ui-icon-locked",
				"customTips":js_tooltip_security,
				"customCallbacks":openSecurityDialog
			});	
		});*/
		
		
		$.subscribe('showProcessErrorPane',function() {
			loginController.changeLoginPanelTitle(js_bank_title);			
		});
		
		$.subscribe('showLookupAccountFailedPane',function() {
			loginController.changeLoginPanelTitle(js_bank_title);
		});
		
		$.subscribe('changeLoginPaneTitle',_changeLoginPaneTitle);
		
	};
	
	function _setUpConstants(config){
		if(config){
			if(config.appType){
				ns.common.appType = config.appType;
			}
			
			if(config.themeName){
				var defaultThemeCookie = _getCookie();				
				if(defaultThemeCookie ==null){
					_setCookie("DefaultTheme",config.themeName,30);				
				}
				
			}
		}
	};
	
	function _getCookie(name) {
		var nameEQ = name + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	};

	function _setCookie(name,value,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else {
			var expires = "";
		}
		document.cookie = name+"="+value+expires+"; path=/";
	};

	
	function _initLayout(){
		$('body').layout({ 
			north:{
				size:100,
				resizable :false,
				closable :false
			},
			south:{
				size:35,
				resizable :false,
				closable :false
			}				
		});
	};

	function _registerEvents(){
		$("#ajaxlink").click(function(e) {
			$('#bank-page').slideUp('slow',function(){
				$("#signin_menu").toggleClass("hiddenClass");
				$('#bank-page').remove();
			});
		});

		$("#signin_menu").mouseup(function() {
			return false;
		});

		//$("#signin_menu").ajaxStart(function(){
			$(document).ajaxStart(function(){
			$('#signin_menu').showLoading();
		});

		$(document).ajaxStop(function(){
			if(!$('#login-init-target') || !$('#login-init-target').html()) {
				$('#signin_menu').hideLoading();
			}
		});
		
		$("#languagesDropdown").change(_changeLanuageHandler);
		$.publish("initLoginStep1Pane");
		
		//Disable the context menu
		$(document).on("contextmenu",function(e){
		    return false;
		});
	};
	
	function _changeLanuageHandler(){
		var lan = $("#languagesDropdown").val();
		var location;
		if(ns.common.appType == "EFS"){
			location = "login-cons.jsp?request_locale=" + lan;
		}else{
			location = "login-corp.jsp?request_locale=" + lan;
		}
		window.location = location;
	};
	
	function _changeLoginPaneTitle(aTitle){
		if(aTitle){
			/*if($("#signin_menu").data("ui-pane") != undefined)
				$("#signin_menu").pane('title',aTitle);*/
		}		
	};
	
	return {
		init:function(config){						
			_initView();
			_setUpConstants(config);
			_registerEvents();			
			
		},
		changeLoginPanelTitle:function(aTitle){
			_changeLoginPaneTitle(aTitle);
		}		
}
	
}());
