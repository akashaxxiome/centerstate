/**
 * Functions of shortcut.
 * Name-space of function is ns.shortcut.
 */
/**
 * Go to favorite page
 */
ns.shortcut.goToFavorites = function( paths ){
	ns.common.showLoading();
	setTimeout('ns.shortcut.performNavigation("'+paths+'")','500');	
};

ns.shortcut.navigateToSubmenus = function(paths){
	$(".submenuHolderBgCls").addClass("hideSubmenuHolder").removeClass("hideSubmenuHolder");
	
	//ns.common.showLoading();
	setTimeout('ns.shortcut.performNavigation("'+paths+'")','500');	
};

/* Common functionality to be called on goToFavorites and goToMenu. */
ns.shortcut.performNavigation = function(paths){
	if(paths == "")
		return;
	
	$('#notes').hide();
	ns.common.shortcut = true;
	
	$.ajaxSetup({
        async: false
	});
	
	ids = paths.split(','); 
	ns.home.removeDialogs(ids[0]);
	
	/* 
	$('li[menuId="' + ids[0] + '"]').find('a').eq(0).click();
	
	for( var i = 1; i < ids.length;  i ++ ){
    // QTS 762662 - Timeout required for page to load for REPORTING shortcuts
	var currentElementId = ids[i];
	console.log("currentElementId=>"+currentElementId);
	setTimeout(function(){ns.shortcut.clickElement(ids[i]);},500);
	}*/
	
	$.each(ids, function(index, currentElementId){
		if(index == 0){//Click on the main menu
			if(currentElementId == 'userPreferencesVirtualMenu'){
				//Do nothing
			}else{
				$('li[menuId="' + currentElementId + '"]').find('a').eq(0).click();
			}
		}else{
			 // QTS 762662 - Timeout required for page to load for REPORTING shortcuts
	    	setTimeout(	function(){ns.shortcut.clickElement(currentElementId);},500);
		}
	});
    
    ns.common.shortcut = false;
    
    $.ajaxSetup({
        async: true
	});
	$('#notes').show();
};

/**
 * Wrapper function used to simulate click of an element
 */
ns.shortcut.clickElement = function(elementId){
	$("#"+elementId).click();
};


/**
 * Wrapper function used to go to a specified menu by the path(menuId/submenuId).
 */
ns.shortcut.goToMenu = function(menuId){
	ns.common.showLoading();
	setTimeout('ns.shortcut.performNavigation("'+menuId+'")','500');
};


/**
 * Edit the shortcut in place.
 */
ns.shortcut.editShortcut = function(shortcutItemID){

	var content = $("#" + shortcutItemID).html();
	
	/**
	 * Initialize the editInPlace.
	 */
	$("#" + shortcutItemID).editInPlace({
		saving_animation_color: "#ECF2F8",
		save_if_nothing_changed:true,
		callback: function(idOfEditor, enteredText, orinalHTMLContent, settingsParams, animationCallbacks) {
			if(enteredText != orinalHTMLContent){
				var results = ns.shortcut.modifyShortcutName(enteredText, orinalHTMLContent);

				$("#shortcutErrorMsgID").html(results[0]);
				$("#shortcutErrorMsgID").show(); 
				setTimeout('$("#shortcutErrorMsgID").hide("slow")',2500);

				//$("#" + shortcutItemID).unbind('click.editInPlace');
				//Refresh the panel to unbind the editInPlace event.
				setTimeout('$.publish("refreshShortcutsPanelPanel")',2500);
				
				if( results[1] == "false" && results[1].length != 0 ){
					return orinalHTMLContent;
				} else {
					//$("#shortcutErrorMsgID").jAlert(results[0], "success");
					return enteredText;
				}
			}else{
				//Refresh the panel to unbind the editInPlace event.
				setTimeout('$.publish("refreshShortcutsPanelPanel")',2000);
				return orinalHTMLContent;
			}
		}
	});
	//Simulate the click operation to open input(edit) field.
	$("#" + shortcutItemID).click();
}

/**
 * Delete one shortcut
 * A notification will show after one shortcut is deleted.
 */
ns.shortcut.deleteOneShortcut = function( urlString ){
	//Send request to delete the shortcut.
	$.ajax({    
		  //URL Encryption Edit by Dongning
		  url: urlString,
		  success: function(data) {
				statusMsg = data.statusMessage;
				successFlag = data.successFlag;
				
				$("#shortcutErrorMsgID").html(statusMsg);
				$("#shortcutErrorMsgID").show(); 

				setTimeout('$("#shortcutErrorMsgID").hide("slow")',2500);
				//Reload the shortcut panel.
				if( successFlag == "true"){
					ns.common.closeDialog();
					ns.common.openDialog('manageShortcuts');
					//Refresh the panel to unbind the editInPlace event.
					setTimeout('$.publish("refreshShortcutsPanelPanel")',3000);
				} 
		  }
	});	
}

/**
 * Do no perform default event of tag a.
 */
$("#shortCutsQuickLinksDivID a").click(function(event){
	   event.preventDefault();
});

/**
 * Modify one shortcut name, and retura an array which includes status and result information.
 */
ns.shortcut.modifyShortcutName = function( newName, orinalName ){
	var resultsArray = new Array();
	$.ajax({
		  //URL Encryption: Change "get" method to "post" method
		  //Edit by Dongning
		  async: false,    
		  type: "POST",
		  url: "/cb/pages/shortcut/manageShortcut_modifyShortcutName.action",
		  data: {orinalName: orinalName, newName: newName, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
		  //data: "orinalName=" + orinalName + "&newName=" + newName,
		  success: function(data) {
				resultsArray[0] = data.statusMessage;
				resultsArray[1] = data.successFlag;
		  }
	});
	return resultsArray;
}

/**
 * Open the window of adding shortcut.
 * The page will be accessed.
 * @param shortcutPath shortcut's path which include a bound of button's id
 * @param entitlementSetting Entitlement check, such as:
 * <ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS%>" >
 * <ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES%>" >
 */
ns.shortcut.openShortcutWindow = function( shortcutPath, entitlementSetting ){
	$.ajax({    
		  //URL Encryption: Change "get" method to "post" method
		  //Edit by Dongning

		  type: "POST",
		  url: "/cb/pages/jsp/home/shortcuts/addeditshortcut.jsp",
		  data: {newShortcutPath: shortcutPath, entitlementSetting: entitlementSetting, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
		  //data: "newShortcutPath=" + shortcutPath + "&entitlementSetting=" + entitlementSetting,
		  success: function(data) {
			var config = ns.common.dialogs["addShortCut"];
			var beforeDialogOpen = function(){			
				$('#addShortcutDialogID').html(data);
			}
			config.beforeOpen = beforeDialogOpen;
			ns.common.openDialog("addShortCut");
		  }
	});
}

/**
 * Get the shortcut settings which is on the page just like:
 * <span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTransferLink,newSingleWiresTransferID</span>
 * <span class="shortcutEntitlementClass" style="display:none;">Wires</span>
 * If the shortcut resides in a portlet which has tab, then the argument noTabs should be true, otherwise false.
 */
ns.shortcut.getShortcutSettingWithPortlet = function( portletID, noTabs){
	var settingsArray = new Array();
	var path = "";
	var ent = "";
	if( noTabs ){
		path = $(portletID).find('.shortcutPathClass').html();
		ent  = $(portletID).find('.shortcutEntitlementClass').html();
	} else {
		path = $(portletID).find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.shortcutPathClass').html();
		ent  = $(portletID).find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.shortcutEntitlementClass').html();
	}

	settingsArray[0] = path;
	settingsArray[1] = ent;
	return settingsArray;
}

/*Shortcut lookupbox related topics start*/

$.subscribe('editShortcutFormComplete', function(event,data) {
	
});    	

$.subscribe('errorEditShortcutForm', function(event,data) {
	
});    	

$.subscribe('successEditShortcutForm', function(event,data) {
	
});

$.subscribe('editShortcutFormSubmit', function(event,data) {
	var newShortcutName = $("#newShortcutName").val();
	var originalShortcutName = $("#originalShortcutName").val();
	if(newShortcutName == undefined || newShortcutName.trim() == ''){
		$("#blankShortcutNameWarning").show();
		return false;
	}
	
	$.ajax({
		  async: false,    
		  type: "POST",
		  url: "/cb/pages/shortcut/manageShortcut_modifyShortcutName.action",
		  data: {orinalName: originalShortcutName, newName: newShortcutName, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
		  success: function(data) {
			  statusMsg = data.statusMessage;
			  successFlag = data.successFlag;
			  
			  if(successFlag == "true"){
				  ns.common.showStatus(statusMsg);
			  }else{
				//  ns.common.showError(statusMsg);
					$("#blankShortcutNameWarning").html(statusMsg);
					$("#blankShortcutNameWarning").show();
						return false;
			  }
			  $.publish('closeDialog');
		  }
	});
	
	
	
});  

$.subscribe('deleteShortcutFormComplete', function(event,data) {
	
});    	

$.subscribe('errorDeleteShortcutForm', function(event,data) {
	
});    	

$.subscribe('successDeleteShortcutForm', function(event,data) {
	
});    	

$.subscribe('deleteShortcutFormSubmit', function(event,data) {
	var originalShortcutName = $("#originalShortcutName").val();
	var shortcutID = $("#shortcutID").val();
	
	//Send request to delete the shortcut.
	$.ajax({    
		  //URL Encryption Edit by Dongning
		  url: "/cb/pages/shortcut/manageShortcut_deleteOneShortcut.action",
		  data: {CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage,shortcutID: shortcutID, shortcutName:originalShortcutName},
		  success: function(data) {
				statusMsg = data.statusMessage;
				successFlag = data.successFlag;

				if( successFlag == "true"){
					ns.common.showStatus(statusMsg);
				}else{
					ns.common.showError(statusMsg);
				}
				$.publish('closeDialog');
		  }
	});	
});  

jQuery(document).ready(function () {
	
	/*  $("#shortcutSearchInput").lookupbox({
		"source":"/cb/pages/shortcut/shortcutsLookupBoxAction.action",
		"controlType":"server",
		"maxItems":"3",
		"collectionKey":"1",
		"size":"35",
		"hint":"Select shorcut..."
		
	});  */
	
	ns.shortcut.showAllShortcuts = function(){
		$("#shortcutSearchInput").autocomplete("search", "");
		$("#shortcutSearchInput").focus();
	};
	
	editUserShortcut = function(shortcutID,shortCutName){
		$.ajax({
			url: '/cb/pages/jsp/home/shortcuts/manageShortcutsDialog.jsp',
			type:"POST",
			data: {opeartion: 'modify', CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage,shortcutID: shortcutID, shortCutName:shortCutName},
			success: function(data){
				
				var config = ns.common.dialogs["manageShortcuts"];
				var beforeDialogOpen = function(){			
					$('#manageShortcutsDialogId').html(data);				
				}
				config.beforeOpen = beforeDialogOpen;
				ns.common.openDialog("manageShortcuts");			
			}
		});
	};

	deleteUserShortcut = function(shortcutID,shortCutName){
			$.ajax({
			url: '/cb/pages/jsp/home/shortcuts/manageShortcutsDialog.jsp',
			type:"POST",
			data: {opeartion: 'delete', CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage,shortcutID: shortcutID, shortCutName:shortCutName},
			success: function(data){
				
				var config = ns.common.dialogs["manageShortcuts"];
				var beforeDialogOpen = function(){			
					$('#manageShortcutsDialogId').html(data);				
				}
				config.beforeOpen = beforeDialogOpen;
				ns.common.openDialog("manageShortcuts");			
			}
		});
	};
	
	shortcutSearchFunction = function(request, response){
			$.ajax({
				url: '/cb/pages/shortcut/shortcutsLookupBoxAction.action',
				data: {term: request.term},
				global:false,
				success: function(data){
					 response($.map( data.optionsModel, function(item){
							var text = $.trim(item.shortCutName);	
						 	var textToReplace = text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "i"), "<strong>$1</strong>");
						 	return {
				 	              	label: textToReplace,
				 	            	shortcutID: item.shortcutID,
				 	          		shortCutName:item.shortCutName,
				 	          		shortCutPath:item.shortCutPath,
				 	          		entitlementSetting:item.entitlementSetting,
				 	          		categoryID:item.categoryID,
							};
			 	        }));
				}
			});
		};
		
	$("#shortcutOptHolderDiv").on("click", "li", function(event) {
		var shortcutID = $(this).data("uiAutocompleteItem").shortcutID;
		var shortCutName = $(this).data("uiAutocompleteItem").shortCutName;
		
		if($(event.target).hasClass("ffiUiIcoSmall ffiUiIco-icon-edit")){
     		editUserShortcut(shortcutID, shortCutName);
     	}else if($(event.target).hasClass("ffiUiIcoSmall ffiUiIco-icon-trash")){
     		deleteUserShortcut(shortcutID, shortCutName);
     	}else{
     		$("#shortcutSearchInput").val("");
     		var shortCutPath = $(this).data("uiAutocompleteItem").shortCutPath;
          	if(shortCutPath !='' && shortCutPath !='-1'){
          		ns.shortcut.navigateToSubmenus(shortCutPath);
          	}
     	}
	});
	
	$("#shortcutSearchInput").autocomplete({
	    source: shortcutSearchFunction,
	    appendTo:"#shortcutOptHolderDiv",
	    minLength : "0",
	    create: function () {
	        $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
	        	var userShortcutActions = "";
	        	var li = '<li class="bookmarkItemListCls">';
	        	
	        	if(item.categoryID == '0'){
	        		userShortcutActions = '<div class="actionContainer"><span class="ffiUiIcoSmall ffiUiIco-icon-edit" ></span><span class="actionItemDivider">|</span><span class="ffiUiIcoSmall ffiUiIco-icon-trash" ></span></div>';
	        	}
	        	if(item.categoryID == '998') {
	        		// system shortcut
	        		li = '<li class="shortcutHeaderItem">';
	        		userShortcutActions = '<div class="shortcutTypeIconHolder"><span class="ffiUiIcoMedium ffiUiIco-icon-systemShortcut" ></span></div>';
	        	}
	        	if(item.categoryID == '999') {
	        		// user shortcut
	        		li = '<li class="shortcutHeaderItem">';
	        		userShortcutActions = '<div class="shortcutTypeIconHolder"><span class="ffiUiIcoMedium ffiUiIco-icon-userShortcut"></span></div>';
	        	}
	        	
	        	
	        	$(li).data(item);
	        	return $(li).append('<a class="shortcutHolderAnchor">' + '<span style="float:left"></span>'+ item.label + userShortcutActions +' </a>')
	            .appendTo(ul);
	        };
	    },
	    select: function(event, ui){
	    	$("#shortcutSearchInput").val("");
	     	return false;
	    },
	    focus: function( event, ui ) {
	    	return false;
	    },
	    open: function() { $('#shortcutOptHolderDiv .ui-menu').width(280); } 
	    
	});
	
});



/*Shortcut lookupbox related topics end*/