/*Common utilities*/
ns.home.openColumnChooser = function(id, updateURL, callback) {
	$('#' + id).jqGrid('columnChooser', {
		done: function(perm) {
			if (perm)  {
				this.jqGrid("remapColumns", perm, true);
				var gwdth = this.jqGrid("getGridParam","width");
				this.jqGrid("setGridWidth",gwdth);
				var colModel = this.jqGrid("getGridParam", "colModel");
				var columns = ns.common.sortableCol.getSetting(id);
				if(colModel) {
					var hiddenColumnsList = '';
			      	for(var i = 0; i < colModel.length; i++) {
			        	if(colModel[i].name && colModel[i].hidden) {
			        		hiddenColumnsList += colModel[i].name +',';
			        	}
			      	}
			      	var aConfig = {
						url: updateURL,
						type:"POST",
						data: {hiddenColumnNames: hiddenColumnsList, columnPerm: columns},
						success: function(data) {
							isPortalModified = true;
						}
					};

					var localAjaxSettings = ns.common.applyLocalAjaxSettings(aConfig); 
					$.ajax(localAjaxSettings);
					if($.isFunction(callback)) {
			        	callback(hiddenColumnsList);
			    	}
	          }
			}
		},
		dlog_opts:function(opts){
			var buttons = {};
			opts.bCancel = jQuery.i18n.prop("js_CANCEL");
			opts.bSubmit = jQuery.i18n.prop("js_OK");
			buttons[opts.bCancel] = function() {
				opts.cleanup(true);
			};
			buttons[opts.bSubmit] = function() {
				opts.apply_perm();
				opts.cleanup(false);
			};
			return {
				"buttons": buttons,
				"close": function() {
					opts.cleanup(true);
				},
				"modal" : false,
				"resizable": false,
				"width": opts.width+20
			};
		}
	});
}

ns.home.addColumnChooser = function(portletId, gridID, updateURL, callback) {
	/*var $col = $('<span/>').addClass("ui-icon ui-icon-calculator colChooser");
	$('#' + portletId).portlet('addCtrlIcon', $col);
    $col.bind('click', function(event) {
        ns.home.openColumnChooser(gridID, updateURL, callback);
    });*/
	
	//ns.common.addGridControls(gridID,false);
};

ns.home.removeColumnChooser = function(portletId) {
	$('#' + portletId).portlet('removeCtrlIcon', '.colChooser');
}

ns.home.removeEditButton = function(portletId) {
	$('#' + portletId).portlet('removeCtrlIcon', '.edit');
}

$.subscribe('disableGridAutoWidth', function(){
	jQuery.extend(jQuery.jgrid.defaults, {autowidth: false});
});

$.subscribe('enableGridAutoWidth', function(){
	jQuery.extend(jQuery.jgrid.defaults, {autowidth: true});
});

ns.home.setHeaderDate = function(portletId, urlString, prefix) {

	var dateString = $("#currentDateId").val();
	var title = $('#' + portletId).portlet('title');
	if(title && title.indexOf(prefix + dateString) == -1) {
		title = title + prefix + dateString;
		$('#' + portletId).portlet('title', title);
	}
	
}

ns.home.subscribeGridEvent = function(gridID, gridCompleteTopic, columnPerm, hiddenColumnNames) {
	$.subscribe(gridCompleteTopic, function() {
		if(columnPerm) {
			ns.common.sortableCol.rearrangeColumns(columnPerm,gridID);
		}
		
		if(hiddenColumnNames) {
			var columns = hiddenColumnNames.split(',');
			if(columns) {
				var columnArray = new Array();
				for(var i=0;i < columns.length; i++) {
					var name = columns[i];
					if(name) {
						columnArray[i] = name;
					}
				}
				if(columnArray && columnArray.length) {
					$('#' + gridID).jqGrid('hideCol', columnArray);
					var gwdth = $('#' + gridID).jqGrid('getGridParam','width');
					$('#' + gridID).jqGrid('setGridWidth',gwdth);
				}
			}
		}
		
		$.destroyTopic(gridCompleteTopic);
	});
}

/*Cash flow portlet*/
ns.home.initCashFlowPortlet = function(portletId, gridID, updateURL, gridCompleteTopic, columnPerm, hiddenColumnNames) {
	var callback = function(names) {
		ns.home.hideCashflowFooter(portletId, names);
	};
	
	ns.home.addColumnChooser(portletId, gridID, updateURL, callback);
	ns.home.subscribeGridEvent(gridID, gridCompleteTopic, columnPerm, hiddenColumnNames);
	$("#" + gridID).data("updateURL",updateURL);
}

ns.home.hideCashflowFooter = function(portletId, hiddenColumnNames) {
	$('#' + portletId + ' .ui-jqgrid-ftable').css('display', 'block');
	if(hiddenColumnNames) {
		var names = hiddenColumnNames.split(',');
		if(names && names.length > 5) {
			var count = 0;
			for(var i=0;i<names.length;i++) {
				if(names[i]) {
					count++;
				}
			}
			if((count >= 6) || (count == 5 && hiddenColumnNames.indexOf('nickName') == -1)) {
				$('#' + portletId + ' .ui-jqgrid-ftable').hide();
			}
		}
	}
}

ns.home.openBalFormatter = function(cellvalue, options, rowObject) {
	if(rowObject && options.rowId) {
		if(rowObject.showOpeningBal) {
			if(rowObject.openingBalColor) {
				return '<span style="color:' + rowObject.openingBalColor + '">' + cellvalue + '</span>';
			} else {
				return cellvalue;
			}
		} else {
			return '--';
		}
	}
	return cellvalue;
}

ns.home.closeBalFormatter = function(cellvalue, options, rowObject) {
	if(cellvalue==""||cellvalue==null||cellvalue==undefined){
		cellvalue = "0.00";
	}
	if(rowObject && rowObject.closingBalColor) {
		var color = rowObject.closingBalColor;
		if(color=="negativetrue"){
			color = "RED";
		}else if(color=="negativefalse"){
			color = "BLACK";
		}
		return '<span style="color:' + color + '">' + cellvalue + '</span>';
	} else {
		return cellvalue;
	}
}

/*end of cash flow portlet*/

/*Positive page portlet*/
ns.home.initPPayPortlet = function(portletId, gridID, updateURL, gridCompleteTopic, columnPerm, hiddenColumnNames) {
	ns.home.addColumnChooser(portletId, gridID, updateURL);
	ns.home.subscribeGridEvent(gridID, gridCompleteTopic, columnPerm, hiddenColumnNames);
	$("#" + gridID).data("updateURL",updateURL);
}
/*end of positive page portlet*/

/*Reverse Positive page portlet*/
ns.home.initRPPayPortlet = function(portletId, gridID, updateURL, gridCompleteTopic, columnPerm, hiddenColumnNames) {
	ns.home.addColumnChooser(portletId, gridID, updateURL);
	ns.home.subscribeGridEvent(gridID, gridCompleteTopic, columnPerm, hiddenColumnNames);
	$("#" + gridID).data("updateURL",updateURL);
}
/*end of reverse positive page portlet*/

/*Lockbox portlet*/
ns.home.initLockboxPortlet = function(portletId, gridID, updateURL, gridCompleteTopic, columnPerm, hiddenColumnNames) {
	ns.home.addColumnChooser(portletId, gridID, updateURL);
	ns.home.subscribeGridEvent(gridID, gridCompleteTopic, columnPerm, hiddenColumnNames);
	$("#" + gridID).data("updateURL",updateURL);
}

ns.home.lockboxAccountFormatter = function(cellvalue, options, rowObject) {
	if(rowObject) {
		var str = rowObject.lockboxAccount.displayText;
		str = "<a href=\"#\" onClick=\"ns.shortcut.goToFavorites('cashMgmt_lockbox')\">" + str + "</a>";
		return str;
	}
	var defaultStr = "<a href=\"#\" onClick=\"ns.shortcut.goToFavorites('cashMgmt_lockbox')\">" + cellvalue + "</a>";
	return defaultStr;
}

ns.home.lockboxAccountNickNameFormatter = function(cellvalue, options, rowObject) {
	var str = "";
	if(rowObject) {
		str = rowObject.lockboxAccount.routingNumber + " : " + rowObject.lockboxAccountNickname;
		if(rowObject.lockboxAccount.currencyType) {
			str += " - " + rowObject.lockboxAccount.currencyType;
		}
	}
	return str;
}

ns.home.lockboxCreditsFormatter = function(cellvalue, options, rowObject) {
	if(rowObject && cellvalue) {
		if(cellvalue != '-1') {
			return cellvalue;
		}
	}
	return '';
}
/*end of lockbox portlet*/


ns.home.initAccountDisPortlet = function(portletId, gridID, updateURL, gridCompleteTopic, columnPerm, hiddenColumnNames) {
	ns.home.addColumnChooser(portletId, gridID, updateURL);
	ns.home.subscribeGridEvent(gridID, gridCompleteTopic, columnPerm, hiddenColumnNames);
	$("#" + gridID).data("updateURL",updateURL);
	
	$.subscribe(gridCompleteTopic, function(){
		var data = $('#' + gridID).jqGrid('getGridParam','userData');
		var startDate = data.startDate;
		var endDate = data.endDate;
		var title = $('#' + portletId).portlet('title');
		if(title && title.indexOf(prefix + endDate) == -1) {
			title = title + prefix + endDate;
			$('#' + portletId).portlet('title', title);
		}
	});
};

ns.home.accountDisAccountIDFormatter = function(cellvalue, options, rowObject) {
	if(rowObject) {
		var str = rowObject.map.AcctDisAccountDisplayText;
		return str;
	}
	return cellvalue;
}

ns.home.accountDisAccountNickNameFormatter = function(cellvalue, options, rowObject) {
	if(rowObject) {
		var str = rowObject.account.routingNumber;
		if(rowObject.account.accountName) {
			str += " : " + rowObject.account.accountName;
		}
		str += " - " + rowObject.account.currencyType;
		return str;
	}
	return cellvalue;
}

ns.home.accountDisAccountItemsPendingFormatter = function(cellvalue, options, rowObject) {
	if( rowObject ) {
		var str = "";
		if(cellvalue != -1) {
			str = "<a href=\"#\" onClick=\"ns.home.viewDisbursementDetail()\">" + cellvalue + "</a>";
		}
		return str;
	}
	return cellvalue;
}

ns.home.openItemsFormatter= function(cellvalue, options, rowObject) {
	if( rowObject && cellvalue > 0 ) {
		var str = "<a href=\"#\" onClick=\"ns.shortcut.goToFavorites('cashMgmt_cashflow')\">" + cellvalue + "</a>";
		return str;
	}
	return cellvalue;
}

ns.home.initPresentDisPortlet = function(portletId, gridID, updateURL, gridCompleteTopic, columnPerm, hiddenColumnNames) {
	ns.home.addColumnChooser(portletId, gridID, updateURL);
	ns.home.subscribeGridEvent(gridID, gridCompleteTopic, columnPerm, hiddenColumnNames);
	$("#" + gridID).data("updateURL",updateURL);
	
	$.subscribe(gridCompleteTopic, function(){
		var data = $('#' + gridID).jqGrid('getGridParam','userData');
		var startDate = data.startDate;
		var endDate = data.endDate;
		var title = $('#' + portletId).portlet('title');
		if(title && title.indexOf(prefix + endDate) == -1) {
			title = title + prefix + endDate;
			$('#' + portletId).portlet('title', title);
		}
	});
};


ns.home.FAPrevBalFormatter = function(cellvalue, options, rowObject) {
    var compClosingLedger = rowObject['map'].displayClosingBalanceValue;
    if(cellvalue == ""||cellvalue==null || cellvalue=="0.00")
        return '---';
    if(compClosingLedger < 0){
        cellvalue = '<span style="color:red;">' + cellvalue + '</span>';
    }
    return cellvalue;
}

ns.home.initFavorAccPortlet = function(portletId, gridID, updateURL, gridCompleteTopic, columnPerm, hiddenColumnNames) {
	ns.home.addColumnChooser(portletId, gridID, updateURL);
	ns.home.subscribeGridEvent(gridID, gridCompleteTopic, columnPerm, hiddenColumnNames);
	$("#" + gridID).data("updateURL",updateURL);
}

ns.home.initAccountDepositGrid = function(gridID, topic){
	$.publish('disableGridAutoWidth');
	$.subscribe(topic, function(){
		var data = $('#' + gridID).jqGrid('getGridParam','userData');
		if(data) {
			var openLedgerHTML = '<span style="color:' + data.openingLedgerColor + '" encode="false">' + data.openingLedger + '</span>';
			var closeLedgerHTML = '<span style="color:' + data.closingLedgerColor + '" encode="false">' + data.closingLedger + '</span>';
			$('#' + gridID).jqGrid('footerData','set',{'summaryDateDisplay': data.total, 'displayOpeningLedger': openLedgerHTML, 'totalDebits': data.totalDebits, 'totalCredits': data.totalCredits, 'displayClosingAmountValue':closeLedgerHTML}, false);
		}
	});
}

ns.home.initAccountCcardGrid = function(gridID, topic){
	$.publish('disableGridAutoWidth');
	$.subscribe(topic, function(){
		var data = $('#' + gridID).jqGrid('getGridParam','userData');
		if(data) {
			var availCreditHTML = '<span style="color:' + data.availCreditTotalColor + '" encode="false">' + data.availCreditTotal + '</span>';
			var amtDueHTML = '<span style="color:' + data.amtDueTotalColor + '" encode="false">' + data.amtDueTotal + '</span>';
			var balanceHTML = '<span style="color:' + data.balanceColor + '" encode="false">' + data.balance + '</span>';
			$('#' + gridID).jqGrid('footerData','set',{'summaryDateDisplay': data.total, 'availableCredit': availCreditHTML, 'amountDue': amtDueHTML, 'balance': balanceHTML}, false);
		}
	});
}

ns.home.initAccountLoanGrid = function(gridID, topic){
	$.publish('disableGridAutoWidth');
	$.subscribe(topic, function(){
		var data = $('#' + gridID).jqGrid('getGridParam','userData');
		if(data) {
			var availCreditHTML = '<span style="color:' + data.availCreditTotalColor + '" encode="false">' + data.availCreditTotal + '</span>';
			var amtDueHTML = '<span style="color:' + data.amtDueTotalColor + '" encode="false">' + data.amtDueTotal + '</span>';
			var balanceHTML = '<span style="color:' + data.balanceColor + '" encode="false">' + data.balance + '</span>';
			$('#' + gridID).jqGrid('footerData','set',{'summaryDateDisplay': data.total, 'availableCredit': availCreditHTML, 'amountDue': amtDueHTML, 'balance': balanceHTML}, false);
		}
	});
}

ns.home.initAccountAssetGrid = function(gridID, topic){
	$.publish('disableGridAutoWidth');
	$.subscribe(topic, function(){
		var data = $('#' + gridID).jqGrid('getGridParam','userData');
		if(data) {
			var bookValueHTML = '<span style="color:' + data.bookValueTotalColor + '" encode="false">' + data.bookValueTotal + '</span>';
			var marketValueHTML = '<span style="color:' + data.marketValueTotalColor + '" encode="false">' + data.marketValueTotal + '</span>';
			$('#' + gridID).jqGrid('footerData','set',{'summaryDateDisplay': data.total, 'bookValue': bookValueHTML, 'marketValue':marketValueHTML}, false);
		}
	});
}

ns.home.initAccountOtherGrid = function(gridID, topic){
	$.publish('disableGridAutoWidth');
	$.subscribe(topic, function(){
		var data = $('#' + gridID).jqGrid('getGridParam','userData');
		if(data) {
			var openLedgerHTML = '<span style="color:' + data.openingLedgerColor + '" encode="false">' + data.openingLedger + '</span>';
			var closeLedgerHTML = '<span style="color:' + data.closingLedgerColor + '" encode="false">' + data.closingLedger + '</span>';
			$('#' + gridID).jqGrid('footerData','set',{'summaryDateDisplay': data.total, 'displayOpeningLedger': openLedgerHTML, 'totalDebits': data.totalDebits, 'totalCredits': data.totalCredits, 'account.displayClosingBalance':closeLedgerHTML}, false);
		}
	});
}

ns.home.initConBalPortlet = function(portletId, gridID, updateURL, gridCompleteTopic, columnPerm, hiddenColumnNames){
	ns.home.addColumnChooser(portletId, gridID, updateURL);
	ns.home.subscribeGridEvent(gridID, gridCompleteTopic, columnPerm, hiddenColumnNames);
	$("#" + gridID).data("updateURL",updateURL);	
}

ns.home.PPayExceptionSummaryFormatter = function(cellvalue, options, rowObject){
	var checkNumber = "<a href=\"#\" onClick=\"ns.shortcut.goToFavorites('cashMgmt_ppay')\">" + cellvalue + "</a>";
	return checkNumber;
}

ns.home.RPPayExceptionSummaryFormatter = function(cellvalue, options, rowObject){
	var checkNumber = "<a href=\"#\" onClick=\"ns.shortcut.goToFavorites('cashMgmt_rppay')\">" + cellvalue + "</a>";
	return checkNumber;
}

ns.home.ControlledDisbursementsSummaryFormatter = function(cellvalue, options, rowObject){
	var itemsPending = "<a href=\"#\" onClick=\"ns.home.viewDisbursementDetail()\">" + cellvalue + "</a>";
	return itemsPending;
}

ns.home.viewDisbursementDetail = function () {
	var urlString = "/cb/pages/jsp/cash/cashdisbursement_selected.jsp";
	$.ajax({
		url: urlString,
		success: function(data){
			ns.shortcut.goToFavorites("cashMgmt_disburs");
		}
	});
}
ns.home.formatCurrentBalance= function(cellvalue, options, rowObject) {
			var action = cellvalue;
			if(rowObject['currentBalance']!=null && rowObject['currentBalance']!=undefined && rowObject['currentBalance']!=""){
					isNegative = 0.00;
					 isNegative = rowObject['currentBalance'].trim();
					 if(isNegative.indexOf("-") === 0)
					 action = "<font color='red'>"+cellvalue+"</font>";
					 }
			return action;
}
/* functions added for account summary & pending transactions portlets */
ns.home.formatInternalAccountsActionLinks = function(cellvalue, options, rowObject) {
	
	if(rowObject) 
	{
		var action = '';
		
		var isEntitledForHistory = 	options.colModel.formatoptions.isEntitledForHistory;

		if(isEntitledForHistory == 'true')
		{
 			action = "<a title='"+js_export+"' onClick='ns.home.exportAccountHistory(\"" + rowObject.ExportURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-export'></span> </a>";
		}

		return ' ' + action;
	}
	else
	{
		return '';
	}
	
}

ns.home.boldFormatter = function(cellvalue, options, rowObject) {
	if(rowObject) 
	{
		var action = '';
		
		action = "<b>"+ cellvalue + "</b>";
		
		if(options!=null && options!= undefined && options.colModel!=null&& options.colModel!=undefined){
			if(options.colModel.index == 'displayClosingBalance'){
				if(rowObject['closingBalance']!=null && rowObject['closingBalance']!=undefined && rowObject['closingBalance']!=""){
					 isNegative = 0.00;
					 isNegative = rowObject['closingBalance'].trim();
					 if(isNegative.indexOf("-") === 0)
					 action = "<font color='red'>"+action+"</font>";
					 }
					
				}
			else if(options.colModel.index == 'CurrentBalanceField'){
				if(rowObject['currentBalance']!=null && rowObject['currentBalance']!=undefined && rowObject['currentBalance']!=""){
					isNegative = 0.00;
					 isNegative = rowObject['currentBalance'].trim();
					 if(isNegative.indexOf("-") === 0)
					 action = "<font color='red'>"+action+"</font>";
					 }
			}else if(options.colModel.index == 'CurrentBalanceFieldInt'){
				if(rowObject['currentBalanceInternal']!=null && rowObject['currentBalanceInternal']!=undefined && rowObject['currentBalanceInternal']!=""){
					isNegative = 0.00;
					 isNegative = rowObject['currentBalanceInternal'].trim();
					 if(isNegative.indexOf("-") === 0)
					 action = "<font color='red'>"+action+"</font>";
					 }
			}else if(options.colModel.index == 'CurrentBalanceFieldExt'){
				if(rowObject['currentBalanceExternal']!=null && rowObject['currentBalanceExternal']!=undefined && rowObject['currentBalanceExternal']!=""){
					isNegative = 0.00;
					 isNegative = rowObject['currentBalanceExternal'].trim();
					 if(isNegative.indexOf("-") === 0)
					 action = "<font color='red'>"+action+"</font>";
					 }
			}else if(options.colModel.index == 'displayAvailableBalance'){
			if(rowObject['availableBalance']!=null && rowObject['availableBalance']!=undefined && rowObject['availableBalance']!=""){
					isNegative = 0.00;
					 isNegative = rowObject['availableBalance'].trim();
					 if(isNegative.indexOf("-") === 0)
					 action = "<font color='red'>"+action+"</font>";
					 }
			}
		}
		
		return action;
	}
	else
	{
		return '';
	}
	
}


ns.home.exportAccountHistory = function(urlString)
{
	$.ajax({
		url: urlString,
		success: function(data){
			if($("#formExportHistory")){
				$("#formExportHistory").empty().remove();//to resolve issue occuring due to multiple export popups
			}
			$('#accountsSummaryExportDialogID').html(data).dialog('open');
		}
	});
}

$.subscribe('updateRadioButtonValues',function(event,data){
	setRadioButton( 'date3', document.formExportHistory );
});

// Initialize Internal accounts grid
ns.home.initInternalAccountsGrid = function(gridID, topic){
	$.subscribe(topic, function(){
		var ids = jQuery('#' + gridID).jqGrid('getDataIDs');
		
		if (ids.length == 0) {
            var data = $('#' + gridID).jqGrid('getGridParam','userData');
            if (data && data.noInternalAccounts && !$('#' + gridID).hasClass("noInternalAccounts"))
            {
                $('#' + gridID).addClass('noInternalAccounts');     // don't repeat this action
				var className = "ui-widget-content jqgrow ui-row-ltr";
  				$("#" + gridID + " tbody").after('<tr class=\"' + className + '\"><td colspan="8" align="left"> <b> ' + data.noInternalAccounts + '</b></td></tr>');
				//Hide footer row
				$('#gview_' + gridID + " .ui-jqgrid-ftable").hide();
				$('#internalAccountsData').hide();
				$('#internalAccountsNoData').show();
			}
		}else{
			$('#internalAccountsNoData').hide();
			$('#internalAccountsData').show();
		}
		ns.common.resizeWidthOfGrids();
	});
};

// Initialize External accounts grid
ns.home.initExternalAccountsGrid = function(gridID, topic){
	$.subscribe(topic, function(){
        var data = $('#' + gridID).jqGrid('getGridParam','userData');
        if (data && data.noExternalAccounts && !$('#' + gridID).hasClass("noExternalAccounts"))
        {
            $('#' + gridID).addClass('noExternalAccounts');     // don't repeat this action
            var className = "ui-widget-content jqgrow ui-row-ltr";
            $("#" + gridID + " tbody").after('<tr class=\"' + className + '\"><td colspan="8" align="left"> <b> ' + data.noExternalAccounts + '</b></td></tr>');
            //Hide footer row
            $('#gview_' + gridID + " .ui-jqgrid-ftable").hide();
            $('#externalAccountsData').hide();
			$('#externalAccountsNoData').show();
		}else{
			$('#externalAccountsNoData').hide();
			$('#externalAccountsData').show();
		}
	});
};

// Initialize total grid
ns.home.initTotalsGrid = function(gridID, topic){
	$.subscribe(topic, function(){
		var ids = jQuery('#' + gridID).jqGrid('getDataIDs');
		
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			var displayClosingBalance = $('#'+gridID).jqGrid('getCell', rowId, 'displayClosingBalance');
			var displayCurrentBalance = $('#'+gridID).jqGrid('getCell', rowId, 'displayCurrentBalance');
			var displayAvailableBalance = $('#'+gridID).jqGrid('getCell', rowId, 'displayAvailableBalance');
						
			if (displayClosingBalance == "<b>0.00</b>" && displayCurrentBalance == "<b>0.00</b>" && displayAvailableBalance == "<b>0.00</b>") {
				$("#gbox_accountsSummaryTotalUrlID").hide();
			}
		}
	});
};

//Customs to account nick name column for template
ns.home.customToAccountNickNameColumn = function( cellvalue, options, rowObject ){
	var _toAccountNickName = rowObject["account"].nickName;
	var _toAccountCurrencyCode = rowObject["account"].currencyCode;
	var _acountRoutingNum = rowObject["account"].routingNum;
	var _toAccountName = _acountRoutingNum;

	if( _toAccountNickName != undefined && _toAccountNickName != "" ){
		_toAccountName = _toAccountName + " : " + _toAccountNickName;
	}
	
	if( _toAccountCurrencyCode != undefined && _toAccountCurrencyCode != "" ){
		_toAccountName = _toAccountName + " - " + _toAccountCurrencyCode;
	}
	var link = _toAccountName;
	return link;		
}

ns.home.customAccountAndNickNameColumn = function( cellvalue, options, rowObject ){
	var _toAccountName = "";
	var _toAccountDisplayText = "";
	var acc = rowObject["account"];
	var _toAccountNickName = "";
	var _toAccountCurrencyCode = "";
	
	if( acc != undefined ) {
		_toAccountDisplayText = rowObject["account"].displayText;
		_toAccountNickName = rowObject["account"].nickName;
		_toAccountCurrencyCode = rowObject["account"].currencyCode;
	}
	if( _toAccountDisplayText != undefined && _toAccountDisplayText != "" ){
		_toAccountName =  _toAccountDisplayText;
	} 
	
	if( _toAccountNickName != undefined && _toAccountNickName != "" ){
		_toAccountName =  _toAccountName + " - " +  _toAccountNickName;
	} 
	
	if( _toAccountCurrencyCode != undefined && _toAccountCurrencyCode != "" ){
		_toAccountName =  _toAccountName + " - " +  _toAccountCurrencyCode;
	} 
	var link = ns.common.setAccountIcon(rowObject.account.type)+_toAccountName;
	return link;
	
}


/**Format Account column for cash flow grid*/
ns.home.formatAccountColumn = function(cellvalue, options, rowObject){
	var _toAccountName = "";
	var _toAccountDisplayText = "";
	var acc = rowObject["account"];
	
	if( acc != undefined ) {
		_toAccountDisplayText = rowObject["account"].displayText;
	}
	if( _toAccountDisplayText != undefined && _toAccountDisplayText != "" ){
		_toAccountName =  ""+_toAccountDisplayText;
	} 
	
	//var accountType = cellvalue.substring(cellvalue.indexOf('-') + 1);
	var accountType = _toAccountDisplayText.substring(_toAccountDisplayText.indexOf('-') + 1);
	switch(accountType){
		case 'Business Loan' : 
			_toAccountName = "<span class='sapUiIconCls icon-capital-projects paddingLeft10 marginRight10'></span>"+_toAccountName;
			break;
		case 'Checking' :
			_toAccountName = "<span class='sapUiIconCls icon-commission-check paddingLeft10 marginRight10'></span>"+_toAccountName;
			break;
		case 'Savings' : 
			_toAccountName = "<span class='sapUiIconCls icon-wallet paddingLeft10 marginRight10'></span>"+_toAccountName;
			break;
		case 'Credit Card' : 
			_toAccountName = "<span class='sapUiIconCls icon-credit-card paddingLeft10 marginRight10'></span>"+_toAccountName;
			break;
		case 'Money Market' :
			_toAccountName = "<span class='sapUiIconCls icon-waiver paddingLeft10 marginRight10'></span>"+_toAccountName;
			break;
		case 'Line of Credit' : 
			_toAccountName = "<span class='sapUiIconCls icon-my-sales-order paddingLeft10 marginRight10'></span>"+_toAccountName;
			break;
		default : 
			_toAccountName = "<span class='sapUiIconCls icon-customer-order-entry paddingLeft10 marginRight10'></span>"+_toAccountName;
	}	
	return _toAccountName;
}


//format the totalDebits
ns.home.totalDebitsFormatter = function(cellvalue, options, rowObject){  
	var totalDebits = "";
	if(cellvalue=="-1"||cellvalue==""||cellvalue==null||cellvalue==undefined){
		totalDebits="---";
	}else{
		totalDebits=cellvalue;
	}
	 return totalDebits;
}

//format the balance
ns.home.balanceFormatter = function(cellvalue, options, rowObject){  
	var balance = "";
	if(cellvalue==""||cellvalue==null||cellvalue==undefined){
		balance="0.00";
	}else{
		balance=cellvalue;
		
		var amt = 0.00;
		if(options!=null && options!= undefined && options.colModel!=null&& options.colModel!=undefined){
			if(options.colModel.index == 'closingLedger.currencyStringNoSymbol'){
				if(rowObject['closingLedger']!=null && rowObject['closingLedger']!=undefined){
					 amt = rowObject['closingLedger'].amountValue;
				}
			}else if(options.colModel.index == 'MARKET_VALUE'){
				if(rowObject['marketValueCurrency']!=null && rowObject['marketValueCurrency']!=undefined){
					 amt = rowObject['marketValueCurrency'].amountValue;
				}
			}else if(options.colModel.index == 'CLOSINGBALANCE'){
			if(rowObject['intraDayOrClosingBalanceCurrency']!=null && rowObject['intraDayOrClosingBalanceCurrency']!=undefined){
					 amt = rowObject['intraDayOrClosingBalanceCurrency'].amountValue;
				}
			}
			if(amt<0.00){
				balance="<font color='red'>"+cellvalue+"</font>";
			}
		}
		
		
	}
	 return balance;
}

//format the totalCredits
ns.home.totalCreditsFormatter = function(cellvalue, options, rowObject){
	var totalCredits = "";
	if(cellvalue=="-1"||cellvalue==""||cellvalue==null||cellvalue==undefined){
		totalCredits="---";
	}else{
		totalCredits=cellvalue;
	}
	return totalCredits;
}

//customs the opening ledger column for deposit grid
ns.home.openingLedgerDepositFormatter = function(cellvalue, options, rowObject){
	var openingLedger ="";
	var compOpeningLeger = rowObject['displayOpeningLedgerAmountValue'];
	if(compOpeningLeger==undefined){
		compOpeningLeger= 0.00;
	}
	if(cellvalue==""||cellvalue==null){
		openingLedger="---";
	}else{
		if(compOpeningLeger<0){
			cellvalue="<font color='red'>"+cellvalue+"</font>";
		}
		return cellvalue;
	}
	return openingLedger;
}

//format the closing ledger column for deposit grid
ns.home.closingLedgerDepositeGridFormatter = function(cellvalue, options, rowObject){
	var closingLeger = "";
	var compClosingLedger ;
	var closingLedgerObj = rowObject["closingLedger"];
	if(closingLedgerObj != null && closingLedgerObj != undefined  ){
	 compClosingLedger = closingLedgerObj.amountValue;
	 }
	
	
	if(compClosingLedger==undefined){
		compClosingLedger= 0.00;
	}
	if(cellvalue == ""||cellvalue==null){
		closingLeger="---";
	}else{
	
		if(compClosingLedger<0){
			cellvalue = "<font color='red'>"+cellvalue+"</font>";
		}
	  return cellvalue;
	}
	return closingLeger;
}

/**Format Account column for fav account grid*/
ns.home.formatFavAccountColumn = function(cellvalue, options, rowObject){
	
	var _toAccountName = "";
	var _toAccountDisplayText = rowObject.displayText;
	if( _toAccountDisplayText != undefined && _toAccountDisplayText != "" ){
		_toAccountName =  ""+_toAccountDisplayText;
	} 
	var lLinkTxt = "<a href=\"javascript:void(0)\" onClick=\"$('#menu_acctMgmt_history a').trigger('click')\" >"+_toAccountName+"</a>";
	var formattedTxt = "";
	formattedTxt = ns.common.setAccountIcon(rowObject.type);
	return formattedTxt+lLinkTxt;	
}



$.subscribe('attachIconsToFavAccountColumn', function(event,data) {
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	for(var i=0;i<ids.length;i++){  
		var rowId = ids[i];
		var _toAccountRountingNumber = $(gridID).jqGrid('getCell', rowId, 'map.rountingNumber');
		var _toAccountName = $(gridID).jqGrid('getCell', rowId, 'map.accountName');
		var _toAccountcurrencyType = $(gridID).jqGrid('getCell', rowId, 'map.currencyType');
		var _toAccountDisbursementName = _toAccountRountingNumber;
		
		if( _toAccountName.length > 0 ){
			_toAccountDisbursementName = _toAccountDisbursementName + " : " + _toAccountName;
		} 	
		 
		if( _toAccountcurrencyType.length > 0 ){
			_toAccountDisbursementName = _toAccountDisbursementName + " - " + _toAccountcurrencyType;
		}
		$(gridID).jqGrid('setCell', rowId, 'accountNickName', _toAccountDisbursementName);
	}
});


/*ns.home.formatFavAccountColumn = function(cellvalue, options, rowObject){
	var _toAccountName = "";
	var _toAccountDisplayText = rowObject.displayText;
	if( _toAccountDisplayText != undefined && _toAccountDisplayText != "" ){
		_toAccountName =  ""+_toAccountDisplayText;
	} 
	var link = "<a href=\"javascript:void(0)\" onClick=\"ns.home.redirectFavAccountHistrory('"+rowObject.map.favAccountURL+"');\" >"+_toAccountName+"</a>";
	return link;
}*/


ns.home.redirectFavAccountHistrory = function (urlString){
	$.ajax({
		   url: urlString,
		   type: "POST",
		   success: function(data){
		   $("#notes").html(data);
		   // remove actie state of home 
			$("#menu_home").removeClass("ui-state-active").removeClass("ui-corner-all");
			// remove selection of all tabs which are inside the submenu
			$('#subList ul').addClass('hidden').removeClass('active');
			// add active state to accnt management
			$('#menu_acctMgmt').addClass("ui-state-active").addClass("ui-corner-all");
			// add active state to account history tab
			$("#menu_acctMgmt_history").addClass("ui-state-active")
			// show the submenu tab only for account mangemnt
			$('#subList ul li#menu_acctMgmt_history').parent().removeClass('hidden');

			// set page title
			var menuId = "acctMgmt_history";
			var menuName = $("li[menuId='acctMgmt'] a").eq(0).html();
			var submenuId = "acctMgmt_history";
			var submenuName = $("li[menuId='acctMgmt_history'] a").eq(0).html();
			ns.home.updateBreadcrumb(menuName, submenuName, menuId);
		   
			//Add browser history entry. This is added for back and forward button support.
			if(window.parent && window.parent.addHistoryEntry){
			   //add menu id entry in history
			   window.parent.addHistoryEntry(submenuId);
			   // set window title
			   window.parent.changeWindowTitle(submenuName);
			}
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
			   accessibility.setFocusOnDashboard();
	   }
	   }
	   });
}

ns.home.positivePayAccount = function(cellvalue, options, rowObject){
	var map = rowObject.map;
	var icon = "";
	if(map!=null){
		var ppAccount = map.PPayAccount;
		if(ppAccount!=null){
			var account = ppAccount.account;
			if(account !=null){
				var accountType = account.type;
				if(accountType !=null){
					icon = ns.common.setAccountIcon(accountType);
				}
				
			}
		}
	}
	
	if(icon!= ""){
		return  icon + cellvalue;
	}else{
		return cellvalue;
	}
}

/**Format Account column for portlet favorite accounts*/
ns.home.formatFavoAcctColumn = function(cellvalue, options, rowObject){
	
	var link = "<a href=\"javascript:void(0)\" onClick=\"ns.home.redirectAnyAccHistory('"+rowObject["map"].favAccountURL+'&amp;DataClassification=P'+"');\" >"+cellvalue+"</a>";
	
	return link;	 
}

/**Format Account column for portlet asset accounts*/
ns.home.formatAssetAcctColumn = function(cellvalue, options, rowObject){
	
	var url ="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=2&AccountID="+rowObject["account"].ID+"&AccountBankID="+rowObject["account"].bankID+"&AccountRoutingNum="+rowObject["account"].routingNum+"&DataClassification="+rowObject["dataClassification"]+"&redirection=true&ProcessAccount=true&TransactionSearch=false";
	
	var link = "<a href=\"javascript:void(0)\" onClick=\"ns.home.redirectAnyAccHistory('"+url+"');\" >"+cellvalue+"</a>";
	
	return link;	 
}

/**Format Account column for portlet credit card accounts*/
ns.home.formatCreditCardAcctColumn = function(cellvalue, options, rowObject){
	
	var url ="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=4&AccountID="+rowObject["account"].ID+"&AccountBankID="+rowObject["account"].bankID+"&AccountRoutingNum="+rowObject["account"].routingNum+"&DataClassification="+rowObject["dataClassification"]+"&redirection=true&ProcessAccount=true&TransactionSearch=false";
	
	var link = "<a href=\"javascript:void(0)\" onClick=\"ns.home.redirectAnyAccHistory('"+url+"');\" >"+cellvalue+"</a>";
	
	return link;	 
}

/**Format Account column for portlet deposit accounts*/
ns.home.formatDepositAcctColumn = function(cellvalue, options, rowObject){
	
	var url ="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=1&AccountID="+rowObject["account"].ID+"&AccountBankID="+rowObject["account"].bankID+"&AccountRoutingNum="+rowObject["account"].routingNum+"&DataClassification="+rowObject["dataClassification"]+"&redirection=true&ProcessAccount=true&TransactionSearch=false";
	
	var link = "<a href=\"javascript:void(0)\" onClick=\"ns.home.redirectAnyAccHistory('"+url+"');\" >"+cellvalue+"</a>";
	
	return link;	 
}

/**Format Account column for portlet loan accounts*/
ns.home.formatLoanAcctColumn = function(cellvalue, options, rowObject){
	
	var url ="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=3&AccountID="+rowObject["account"].ID+"&AccountBankID="+rowObject["account"].bankID+"&AccountRoutingNum="+rowObject["account"].routingNum+"&DataClassification="+rowObject["dataClassification"]+"&redirection=true&ProcessAccount=true&TransactionSearch=false";
	
	var link = "<a href=\"javascript:void(0)\" onClick=\"ns.home.redirectAnyAccHistory('"+url+"');\" >"+cellvalue+"</a>";
	
	return link;	 
}

/**Format Account column for portlet cashflow accounts*/
ns.home.formatCashflowAcctColumn = function(cellvalue, options, rowObject){
	
	var url ="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=1&AccountID="+rowObject["account"].ID+"&AccountBankID="+rowObject["account"].bankID+"&AccountRoutingNum="+rowObject["account"].routingNum+"&DataClassification="+rowObject["dataClassification"]+"&redirection=true&ProcessAccount=true&TransactionSearch=false";
	var dcVar="&DataClassification=P";
	var link = "<a href=\"javascript:void(0)\" onClick=\"ns.home.redirectAnyAccHistory('"+url+dcVar+"');\" >"+cellvalue+"</a>";
	
	return link;	 
}

	/**Redirect to AccountHistory*/
ns.home.redirectAnyAccHistory = function(urlString){

	urlString = urlString+"&CSRF_TOKEN="+ns.home.getSessionTokenVarForGlobalMessage;
	$.ajax({
		   url: urlString,
		   type: "POST",
		   success: function(data){	
		   $("#notes").html(data);
		   
				// set Account History menu as "Active"
				// remove current 2nd-level menu ui-state-active class
				$(".active").children(".ui-state-active").removeClass("ui-state-active").removeClass("ui-corner-all");
				// add ui-state-active class to account history menu 
				$("li[menuId='acctMgmt_history']").addClass("ui-state-active").addClass("ui-corner-all");

				// set page title
				var menuId = "acctMgmt";
				var menuName = $("li[menuId='acctMgmt'] a").eq(0).html();
				var submenuId = "acctMgmt_history";
				var submenuName = $("li[menuId='acctMgmt_history'] a").eq(0).html();
				ns.home.updateBreadcrumb(menuName, submenuName, menuId);
			   
				//Add browser history entry. This is added for back and forward button support.
				if(window.parent && window.parent.addHistoryEntry){
				   //add menu id entry in history
				   window.parent.addHistoryEntry(submenuId);
				   // set window title
				   window.parent.changeWindowTitle(submenuName);
				}
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
				   accessibility.setFocusOnDashboard();
		   }
		ns.home.hideDesktopLayout();		   
	    
	   }
	   });
}