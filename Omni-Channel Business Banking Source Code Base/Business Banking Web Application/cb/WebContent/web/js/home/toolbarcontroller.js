var toolBarController = (function($,window,undefined){
	var tc = {};

	var config = {
		urls:{
			"SOME_ACTION":"/cb/pages/jsp/home/resetLayout.action"
		}
	};

	//jq instances
	var $homeQuickAccessButton = $("#homeQuickAccessButton");

	/**
	* initialization function
	*/
	function _init(aConfig){
		$.extend(config,aConfig);
		registerEvents();
		addStylingEffects();
		
		//reset min-width property for consumer
		if ($('#approvalMsgIndicator').length) {
			// making min-width to accommodate approval icon
			$(".topbarRightDiv").css("min-width", "434px");
		} else {
			// in case of consumer, reset the min-width att. of class
			if(ns.home.isDevice) {
	        	$(".topbarRightDiv").css("min-width", "220px");
	        } else {
	        	$(".topbarRightDiv").css("min-width", "330px");
	        }
		}
	}

	/**
	* This function registers event handlers for tool bar
	*/
	function registerEvents(){
		$homeQuickAccessButton.click(quickhomeClickHandler);


		//Hook some external events
		$.subscribe("onLayoutComplete",onLayoutCompleteListner);
		$.subscribe("onTopMenuClick",onTopMenuClickListner);
		
		//Register hover event on topbar to have deffered content loading
		$(".topbarRightDiv li").hover(function(oEvent){
			var $element = $(oEvent.currentTarget);			
			var topBarMenuType = $element.attr("data-sap-banking-type");
			var bContentLoaded = $element.data("contentloaded") === undefined ? false : true;
			if(bContentLoaded === false){
				if(topBarMenuType === "topbarmessage"){
					$.publish("home-page-topbarMessageTopic");
				}else if(topBarMenuType === "topbaralerts"){
					$.publish("home-page-topbarMessageTopic");
					$.publish("home-page-topbarAlertsTopic");
				}else if(topBarMenuType === "topbarapprovals"){
					$.publish("home-page-topbarApprovalsTopic");
				}	
			}			

			$element.data("contentloaded", "true");
		});
	}

	//TODO: Do this in better way
	function addStylingEffects(){
		//console.info("addStylingEffects");
 		$("ul li ul li a").hover(
             function(){
                    $(this).addClass("ui-state-hover");
             },
             function(){
                    $(this).removeClass("ui-state-hover");   
             }).focus (function(){
           $(this).addClass("ui-state-hover");
       }).focusout (function(){
           $(this).removeClass("ui-state-hover");
       })
	}


	function quickhomeClickHandler(e){
		//console.info("quickhomeClickHandler");
		if(ns.home.currentMenuId != "home"){
			if($("#desktopContainer").hasClass("desktopContainerModelPopup")){
				ns.home.hideDesktopLayout();
			}else{
				ns.layout.toggleOverlayView(true);		
			}
		}else{
			$('li[id="menu_home"] a').click();
		}
	}

	//Some external events listeners
	function onLayoutCompleteListner(e,layout){
		//console.info("onLayoutCompleteListner");
		ns.common.resizeWidthOfGrids();
		if(layout.type === "system"){
			//You can't add portlet to system defined one but you could save it
			$("#addPortletBtnLink").hide();
		}else{
			$("#addPortletBtnLink").show();
		}
	}

	function onTopMenuClickListner(e,previousMenuId, menuId){		
		if(menuId == "home"){
			if(menuId == "home" && ns.home.currentMenuId.indexOf("home") == 0){
				ns.layout.toggleInlineView(true);
				return;
			}
			
			if(previousMenuId != undefined && previousMenuId!= ''){
				if(previousMenuId.indexOf("home_")==0){
					ns.layout.toggleInlineView(true);
				}else{
					//ns.layout.toggleOverlayView(true);
					ns.layout.toggleInlineView(true);
				}
			}else{//Coming first time on Home page, show it as inline view
				ns.layout.toggleInlineView(true);
			}
		}else{//Navigating to other menu than home
			//Ignore navigation to notes section when menu clicked is one of the submenu of Preferences, which get loaded in Dialog 
			if($('li[menuId="' + menuId + '"]').hasClass('preferencesMenuDummyCls')){
				return false;
			}
			ns.layout.toggleInlineView(false);
		}
	}


	/*****************************************************************
	* Public methods for toolbar controller
	******************************************************************/
	tc.init = function(aConfig){
		_init(aConfig);
	}


	return tc;
}(jQuery,window));