$.subscribe('inboxAlertsGridCompleteEvents', function(event,data) {
	ns.alert.handleGridEventsAndSetWidth("#inboxAlertsGridId", "#AlertInbox");	
	ns.alert.checkIfUnreadMessagesExists();
	if(accessibility){
		$.publish("common.topics.tabifyNotes");	
		$("#details").setFocus();
	}
	//Adding Date Picker for receive Date
	 $("#gbox_inboxAlertsGridId #gs_date").bind('click', function(event){
		$("#gbox_inboxAlertsGridId #gs_date").datepicker();
		$("#gbox_inboxAlertsGridId #gs_date").blur();
		$("#gbox_inboxAlertsGridId #gs_date").focus();
    });
	
	//Hook delete handler on grid column	
	$("#inboxAlertsGridId").data("supportSearch",true);
	
	$("#jqgh_inboxAlertsGridId_Delete").removeClass( "ui-jqgrid-sortable" ).addClass( "sapUiIconCls icon-delete" );
	$("#jqgh_inboxAlertsGridId_Delete").css({"cursor" : "pointer"});
	$("#inboxAlertsGridId tbody > tr > td:first-child input ").css({ "margin-left" : "14px" });
	
	//jQuery("#list1").jqGrid('navGrid','#pager1',{edit:false,add:false,del:false,search:false,refresh:false}); 	    	
	$('#jqgh_inboxAlertsGridId_Delete').on('click',function(){
		 $('#multicheck').val(true);
		 //$("#deleteInboxAlertsLink").trigger('click');
		 ns.alert.triggerDeleteReceivedAlert();
	 }); 
});

$.subscribe('subscribedAlertsGridCompleteEvents', function(event,data) {
	ns.alert.handleGridEventsAndSetWidth("#subscribedAlertsGridId", "#subscribedAlertsDiv");
});

$.subscribe('subscribedBalAlertsGridCompleteEvents', function(event,data) {
	ns.alert.handleGridEventsAndSetWidth("#subscribedBalAlertsGridId", "#subscribedAlertsDiv");
});

$.subscribe('subscribedStockAlertsGridCompleteEvents', function(event,data) {
	ns.alert.handleGridEventsAndSetWidth("#subscribedStockAlertsGridId", "#subscribedAlertsDiv");
});

$.subscribe('subscribedTransAlertsGridCompleteEvents', function(event,data) {
	ns.alert.handleGridEventsAndSetWidth("#subscribedTransAlertsGridId", "#subscribedAlertsDiv");
});

ns.alert.handleGridEventsAndSetWidth = function(gridID,tabsID){
	//Adjust the width of grid automatically.
	ns.common.resizeWidthOfGrids();
}


$.subscribe('inboxAlertsGridPagingEvents', function(event,data) {
	//$('#jqghinboxAlertsGridId_ID #selectionCtlInboxAlerts').attr('checked', false);
});


//for Alert Inbox
ns.alert.formatSubjectLink = function(cellvalue, options, rowObject) {
	var link;
	ns.alert.currentView = rowObject.ID;
	
	var linkId = "subjectLink" + rowObject.ID;
	if (rowObject.readDate){
		link = "<a title='" +js_view+ "' id='" + linkId + "' isRead='true' href='#' onClick='ns.alert.viewAlert(\""+ rowObject.map.LinkURL +"\",\"" + linkId + "\");'>" + cellvalue + "</a>";		
		}
	else{
		link = "<a title='" +js_view+ "' id='" + linkId + "' isRead='false' href='#' onClick='ns.alert.viewAlert(\""+ rowObject.map.LinkURL +"\",\"" + linkId + "\");'>*" + cellvalue + "</a>";
		}
		
	return link;
}

ns.alert.deleteAllIds = function(cellvalue, options, rowObject) {
	var link = rowObject.ID;
	return link;
}

/**
 * For received alerts
 */
ns.alert.formatDeleteLinks = function(cellvalue, options, rowObject) {
	ns.alert.currentView = rowObject.ID;
	
	var linkId = "subjectLink" + rowObject.ID;

	var view = "<a title='" +js_view+ "' id='" + linkId + "' isRead='false' href='#' onClick='ns.alert.viewAlert(\""+ rowObject.map.LinkURL +"\",\"" + linkId + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></a>";
	var deleteLink = "<a title='" +js_delete+ "' href='#' onClick='ns.alert.deleteReceivedAlert(\"" + rowObject.ID + "\")'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	return ' ' + view + ' ' + deleteLink;
}

/**
 * For all subscribed alerts
 */
ns.alert.formatDeleteIcons = function(cellvalue, options, rowObject) {

	var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.alert.deleteSubscription(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	return ' ' + del;
}

/**
 * For subscribed Balance alerts
 */
ns.alert.formatBalanceAlertIcons = function(cellvalue, options, rowObject) {
	var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.alert.editBalAlert(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.alert.deleteSubscription(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	if(rowObject.map.Restricted=='true'){
		edit = "";
	}
	return ' ' + edit + ' '  + del ;
	
}

/**
 * For subscribed Transaction alerts
 */
ns.alert.formatTransactionAlertIcons = function(cellvalue, options, rowObject) {
	var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.alert.editTranAlert(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.alert.deleteSubscription(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	if(rowObject.map.Restricted=='true'){
	 edit = "";
	}
	return ' ' + edit + ' '  + del ;
	
}

/**
 * For subscribed Stock alerts
 */
ns.alert.formatStockAlertIcons = function(cellvalue, options, rowObject) {
	var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.alert.editStockAlert(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.alert.deleteSubscription(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	if(rowObject.map.isCurrentReport=='true'){
    edit = "";
    del = "";
	}
	return ' ' + edit + ' '  + del ;
	
}


ns.alert.editBalAlert = function(urlString){
	//var urlString = "/cb/pages/jsp/alert/AlertCRUDAction?method=modify-active&SetAlert.ID=" + alertId;
	var urlString2 = "/cb/pages/jsp/alert/alertsbalance.jsp";
	$.ajax({   
		  url: urlString,   
		  success: function(data) { 
			ns.common.showStatus(data);
			$.ajax({
				url: urlString2,
				success: function(data) {
					$('#alertConfigDiv').html(data);
				}
			});
		  }   
	});
}

ns.alert.editTranAlert = function(urlString){
	//var urlString = "/cb/pages/jsp/alert/AlertCRUDAction?method=modify-active&SetAlert.ID=" + alertId;
	var urlString2 = "/cb/pages/jsp/alert/alertstransaction.jsp";
	$.ajax({   
		  url: urlString,   
		  success: function(data) { 
			ns.common.showStatus(data);
			$.ajax({
				url: urlString2,
				success: function(data) {
					$('#alertConfigDiv').html(data);
				}
			});
		  }   
	});
}

ns.alert.editStockAlert = function(urlString){
	//var urlString = "/cb/pages/jsp/alert/AlertCRUDAction?method=modify-active&SetAlert.ID=" + alertId;
	var urlString2 = "/cb/pages/jsp/alert/alertsstock.jsp";
	$.ajax({   
		  url: urlString,   
		  success: function(data) { 
			ns.common.showStatus(data);
			$.ajax({
				url: urlString2,
				success: function(data) {
					$('#alertConfigDiv').html(data);
				}
			});
		  }   
	});
}

ns.alert.viewAlert = function(urlString,linkId){
	
	//var urlString = "/cb/pages/jsp/alert/alertview.jsp?MessageID=" + messageId;
	$.ajax({   
		  url: urlString,   
		  success: function(data) { 
			$('#alertView').html(data);
			//$('#details1').slideUp();
			$('#alertTabs').slideUp();
			$('#viewAlertPortlet').portlet('show');
			$('#viewAlertPortlet').portlet('title', $('#subjectInViewAlert').html());
			$('#details2').show();
			var str = $("#"+linkId).html();
			if(str){
				$("#"+linkId).attr("isread","true");
				$("#"+linkId).html(str.replace("*",""));
			}
			ns.alert.checkIfUnreadMessagesExists();

			$("#closeViewAlertContainerButton").attr("invokerlinkid",linkId);
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#alertView").setFocus();
			}
			
		  }   
	});
	
};



ns.alert.deleteSubscription = function(urlString){

	//var urlString = "/cb/pages/jsp/alert/alert_unsub_confirm.jsp?AlertID=" + alertId;
	$.ajax({   
		  url: urlString,   
		  success: function(data) { 
			$('#UnsubscribeAlertConfirmDialogID').html(data).dialog('open');
		  }   
	});
	
};


ns.alert.syncCheckbox = function(){
	
	var unchecked = false;
	var $chkboxs = $("table#inboxAlertsGridId").find("input[name='selectedAlertsIDs'][type='checkbox']");
	
	for(var i = 0; i < $chkboxs.length; i++){
		var $chkbox = $chkboxs.eq(i);
		if(!$chkbox.is(":checked")){
			unchecked = true;
			break;
		}
	}

	$("#selectionCtlInboxAlerts").attr('checked', !unchecked);
	$("#deleteInboxAlertsLink").trigger('click');
};

ns.alert.deleteReceivedAlert = function(alertId){
	$("#selectedAlertsIDs").val(alertId);
	ns.alert.triggerDeleteReceivedAlert();
};

ns.alert.triggerDeleteReceivedAlert = function(){
	ns.alert.beforeDeletingAlertsConfirm();
	
	var formData = $('#frmDelAlertFromInbox').serializeArray();
	formData.push({name: 'CSRF_TOKEN', value: ns.home.getSessionTokenVarForGlobalMessage});
	
	$.ajax({
		url:"/cb/pages/jsp/alert/deleteInboxAlertsAction_confirm.action",
		type: "POST",
		data:formData,
		success: function(data){
			$("#DeleteAlertsConfirmDialogID").html(data);			
			$.publish("successDeletingAlertsConfirm");
		},
		error: function(error){
			$.publish("errorDeletingAlertsConfirm");
		},
		complete: function(jqXHR){
			$.publish("DeletingAlertsConfirmComplete");
		}
	})
};

ns.alert.checkIfUnreadMessagesExists = function(){
	var unreadMessages = $("#inboxAlertsGridId tr td a[isread=false]");
	if(unreadMessages.length>0){
		$("#unreadMessage").removeClass("ffiHidden");
	}else{
		$("#unreadMessage").addClass("ffiHidden");
	}
};

$.subscribe("alert.refreshReceivedAlertsGrid",function(event,data){
	$("#inboxAlertsGridId").trigger("reloadGrid");
});