ns.useralerts.initView = function(config){
	ns.useralerts.mode = "ADD";
	ns.useralerts.registerEvents();
	ns.useralerts.initializePortlets();
	$("#alertPortletTitle").hide();	
	$("#transactionAmount").hide();
	$("#immediateTransactionAmount").hide();
	ns.useralerts.removeDialgIfAlreadyExists();
	ns.useralerts.config = config;
};

ns.useralerts.initLookupBox = function(){
	//console.info("ns.useralerts.initLookupBox");
	var appCode = ns.useralerts.getPropertyValue("appCode");
	var optionValue = "";
	var appType = (appCode === "0") ? "corporate": "consumer";
	var defaultOption = "";
	if(appType === "consumer"){		
		defaultOption = {value:"All Accounts", label:"All Accounts"};// do not change all accounts value this should be All Accounts only
	}else{
		defaultOption = "";
	}
	
	
	$("#alertsAccountSelectBox").lookupbox({
		"source":"/cb/pages/jsp/alert/alertsAccountsLookupBoxAction.action",
		"controlType":"server",
		"collectionKey":"5",
		"maxItems":"10",
		"size":"48",
		"defaultOption":defaultOption
	});		
};

ns.useralerts.registerEvents = function(){
	$("#availableAlertTypesContainer .availAlertTypeTable a").click(ns.useralerts.addNewAlertClickHandler);
	$("a[alertbuttontype=cancel]").click(ns.useralerts.closeAlertPortlet);
	$.subscribe('deliveryOptionsGridOnComplete',ns.useralerts.deliveryOptionsGridOnComplete );	
	$.subscribe('useralerts.topics.alertsGridOnComplete',ns.useralerts.alertsGridOnComplete );	
	
	
	$("#editPortfolioLink").click(ns.useralerts.switchToEditPortfolio);
	$("input[name=portfolioStocks]").click(ns.useralerts.checkUncheckPortfolioStocks);
};

ns.useralerts.getUserAlerts = function(){
	//console.info("ns.useralerts.getUserAlerts");
	var URL = "/cb/pages/jsp/alert/getUserAlertsAction.action";
	$.ajax({   
		  url: URL,   
		  success: function(data) {
			  if(data && data.response){
				  var contactPoints = data.response.contactPoints;
				  if(contactPoints){
					  ns.useralerts.deliveryOptionsGrid.contactPoints = contactPoints;
					  ns.useralerts.addGridData(contactPoints); 
				  }
				  
				   
				  var properties = data.response.properties;
				  if(properties){
					//console.info("setting additional propoerties");
					ns.useralerts.properties = properties;
				  }
				  
				  if(ns.useralerts.isViewInitialized == false){
					  var accounts = data.response.accounts;
					  if(accounts){
						  ns.useralerts.accounts = accounts;
						  ns.useralerts.checkForInactiveAccounts(accounts);
					  }
				  }
				  
				  if(data.response.userAlerts){
					  ns.useralerts.subscribedAlerts = data.response.userAlerts;
					  var deliveryOptions = data.response.userAlerts.deliveryOptions;				  
						if(deliveryOptions){
							ns.useralerts.deliveryPreferences = deliveryOptions;
						}
				  }
				  
				if(ns.useralerts.isViewInitialized == false){
					  var amountFilterOptions = data.response.amountFilterOptions;
					  if(amountFilterOptions){
						  ns.useralerts.initAmmountFilter(amountFilterOptions);
					  }				  
				   }
				  
				  if(ns.useralerts.isViewInitialized == false){
					  var transactionTypes = data.response.transactionTypes;
					  if(transactionTypes){
						  ns.useralerts.initTransactionBox(transactionTypes);
					  }				  
				  }
				  
				  if(ns.useralerts.isViewInitialized == false){
					ns.useralerts.initLookupBox();
				  }
				  
				  
				  				  
				  $.publish("dataReceived");
				  ns.useralerts.isViewInitialized= true;
			  }
		  }   
	});

};




ns.useralerts.getPortfolioStocks = function(){
	//console.info("ns.useralerts.getPortfolioStocks");
	ns.useralerts.resetPortfolioStocks();	
	
	var URL = "/cb/pages/jsp/home/StockPortletAction_getPortfolioStocks.action";	
	$.ajax({   
		  url: URL,   
		  success: function(data) {
			  if(data && data.userStocks && data.userStocks.portfolioStocks){
				  ns.useralerts.setupPortfolioView(data.userStocks.portfolioStocks);
			  }
		}
	});

};

ns.useralerts.setupPortfolioView = function(portfolioStocks){
	//console.info("ns.useralerts.setupPortfolioView");
	
	var portfolioStocksCheckBoxes = $("input[name=portfolioStocks]")
	$.each(portfolioStocks,function(i,stock){
		var symbol = stock.symbol;
		$(portfolioStocksCheckBoxes[i]).attr("value",symbol);
		$(portfolioStocksCheckBoxes[i]).siblings().text(symbol);
		
		$("#portfolioStocksTable").show();
		$(portfolioStocksCheckBoxes[i]).show();
		$(portfolioStocksCheckBoxes[i]).siblings().show();
	});	
};

ns.useralerts.resetPortfolioStocks = function(){
	$("input[name=portfolioStocks]").attr("value","");
	$("input[name=portfolioStocks]").attr("checked",false);
	$("input[name=portfolioStocks]").siblings().text('');
	ns.useralerts.hidePortfolioStocks(true);
};



ns.useralerts.hidePortfolioStocks = function(toHide){
	if(toHide){
		$("#portfolioStocksTable").hide();
		$("input[name=portfolioStocks]").hide();
		$("input[name=portfolioStocks]").siblings().hide();
	}else{
		$("#portfolioStocksTable").show();
		$("input[name=portfolioStocks]").show();
		$("input[name=portfolioStocks]").siblings().show();
	}
};

ns.useralerts.checkUncheckPortfolioStocks = function(e){
	
	var isChecked = false;
	isChecked = $(e.currentTarget).attr("checked");
	var symbol = $(e.currentTarget).siblings().text();
	//console.info("checkUncheckPortfolioStocks-> isChecked-" + isChecked + ",symbol-" + symbol);
	if(isChecked){
		$(e.currentTarget).attr("value",symbol);
	}else{
		$(e.currentTarget).attr("value","");
	}
};

ns.useralerts.switchToEditPortfolio = function(){
	ns.shortcut.goToFavorites('home');
};

ns.useralerts.addNewAlertClickHandler = function(e){
	//console.info('ns.useralerts.addNewAlertClickHandler');
	ns.common.hideStatus();
	ns.useralerts.mode = "ADD";
	$("input[name=operation]").val(ns.useralerts.mode);
	$("input[name=alertId]").val('');
	var alertCode = $(e.currentTarget).attr("alertcode");
	ns.useralerts.addNewAlertCode = alertCode;
	ns.useralerts.toggleAlertFormContainer(alertCode);
	ns.useralerts.toggleAlertGridContainer();
	ns.useralerts.showAccountDropDown(alertCode);
	ns.useralerts.showDelOptionsGrid(alertCode);
	ns.useralerts.resetDeliveryPreferences(alertCode);
	ns.useralerts.resetFormFields();
	$("input[name=portfolioStocks]").attr("checked",false);
	
	$("a[alertbuttontype='addNew']").show();
	$("a[alertbuttontype='save']").hide();
	
	$("#addEditAlerts").hide();
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#desktop").setInitialFocus();	
	}	
};

ns.useralerts.editAlertClickHandler = function(e){
	//console.info("ns.useralerts.editAlertClickHandler");
	var id = $("#updateAlert").data("alertId");
	var code = $("#updateAlert").data("alertCode");
};

$.subscribe('onCompleteAddEditAlert', function(event,data) {
	$.publish("common.topics.tabifyDesktop");	
	if(accessibility){
		accessibility.setFocusOnDashboard();		
	}
	var isErrorOccurred = false;
	if(event && event.originalEvent.status){
		if(event.originalEvent.status === "error"){
			isErrorOccurred = true;
		}
	}
	if(!isErrorOccurred){
		$("#alertsMainSummaryContainer").hide();	
	}
	
});


$.subscribe('beforeAddEditAlert', function(event,data) {
	//console.info("ns.useralerts.beforeAddEditAlert ");	
	$.publish("ns_ua_removeErrorsOnBegin");
});

$.subscribe('onSuccessAddEditAlert', function(event,data) {
	$.publish('ns_ua_removeErrorsOnBegin');
	//console.info("ns.useralerts.onSuccessAddEditAlert ");
	ns.useralerts.toggleAlertFormContainer();
  	ns.useralerts.toggleAlertGridContainer();

	if (ns.useralerts.mode == "ADD")
	{
		ns.common.showStatus(js_status_user_alert_added);
	} else if (ns.useralerts.mode == "EDIT")
	{
		ns.common.showStatus(js_status_user_alert_modified);
	}
	
	$('#alertsGrid').trigger("reloadGrid");
	ns.useralerts.getUserAlerts();	
	ns.common.refreshDashboard('dbAddEditAlerts');
});

/**
* This topic clears all form errors
*/
$.subscribe('ns_ua_removeErrorsOnBegin', function(event, data) {
	$('.errorLabel').html('').removeClass('errorLabel');	
	$('[id^=formerrors]').html('');
});

$.subscribe('onTransactionSelectChange', function(event, data) {
	//console.info("onTransactionSelectChange" + $("#transactionAmountSelectBox").val());
	var selectedValue = $("#transactionAmountSelectBox").val();
	if(selectedValue){
		if(selectedValue != 1){
			$("#transactionAmount").show();
		}else{			
			$("#transactionAmount").hide();
			$("#transactionAmount").val('');
		}
	}
	
	var immediateTrxnSelectedValue = $("#immediateTransactionAmountSelectBox").val();
	if(immediateTrxnSelectedValue){
		if(immediateTrxnSelectedValue != 1){
			$("#immediateTransactionAmount").show();
		}else{			
			$("#immediateTransactionAmount").hide();
			$("#immediateTransactionAmount").val('');
		}
	}
});

$.subscribe('dataReceived', function(event, data) {
	ns.useralerts.unbindOneTimeAlerts();
	var alerts = ns.useralerts.subscribedAlerts;
	var appCode = ns.useralerts.getPropertyValue("appCode");
	
	if(appCode==="0"){
		$.each(alerts,function(i,alert){
			if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_UNREAD_BANK_MESSAGE ){
				$("#unreadBankMessageAddAlertLink").fadeTo('slow', 0.5);
				$("#unreadBankMessageAddAlertLink").unbind("click");
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_PASSWORD_CHANGE){			
				$("#passChangeAddAlertLink").fadeTo('slow', 0.5);
				$("#passChangeAddAlertLink").unbind("click");			
			}else if(alert.alertType ==  ns.useralerts.constants.ALERT_TYPE_INQUIRY_RESPONSE){			
				$("#bankEnquiryAddAlertLink").fadeTo('slow', 0.5);
				$("#bankEnquiryAddAlertLink").unbind("click");			
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_LOCKOUT){
				$("#failedAttemptLockoutAddAlertLink").fadeTo('slow', 0.5);
				$("#failedAttemptLockoutAddAlertLink").unbind("click");									
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_INVALID_ACCESS){
				$("#invalidAccountAccessAddAlertLink").fadeTo('slow', 0.5);
				$("#invalidAccountAccessAddAlertLink").unbind("click");									
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_PANIC_PAY){
				$("#panicPayAddAlertLink").fadeTo('slow', 0.5);
				$("#panicPayAddAlertLink").unbind("click");									
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_POSITIVE_PAY){
				$("#positivePayAddAlertLink").fadeTo('slow', 0.5);
				$("#positivePayAddAlertLink").unbind("click");									
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_REVERSE_POSITIVE_PAY){
				$("#reversePositivePayAddAlertLink").fadeTo('slow', 0.5);
				$("#reversePositivePayAddAlertLink").unbind("click");									
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_NFS_CORPORATE){
				$("#insufficientFundsAddAlertLink").fadeTo('slow', 0.5);
				$("#insufficientFundsAddAlertLink").unbind("click");									
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_PAYMENT_APPROVAL){
				$("#paymentApprovalsAddAlertLink").fadeTo('slow', 0.5);
				$("#paymentApprovalsAddAlertLink").unbind("click");
			}
		});
	}else{
		$.each(alerts,function(i,alert){
			if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_UNREAD_BANK_MESSAGE){
				$("#unreadBankMessageAddAlertLink").fadeTo('slow', 0.5);
				$("#unreadBankMessageAddAlertLink").unbind("click");
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_PASSWORD_CHANGE){			
				$("#passChangeAddAlertLink").fadeTo('slow', 0.5);
				$("#passChangeAddAlertLink").unbind("click");			
			}else if(alert.alertType ==  ns.useralerts.constants.ALERT_TYPE_INQUIRY_RESPONSE){			
				$("#bankEnquiryAddAlertLink").fadeTo('slow', 0.5);
				$("#bankEnquiryAddAlertLink").unbind("click");			
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_LOCKOUT){
				$("#failedAttemptLockoutAddAlertLink").fadeTo('slow', 0.5);
				$("#failedAttemptLockoutAddAlertLink").unbind("click");									
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_PANIC_PAY){
				$("#panicPayAddAlertLink").fadeTo('slow', 0.5);
				$("#panicPayAddAlertLink").unbind("click");									
			}else if(alert.alertType == ns.useralerts.constants.ALERT_TYPE_INVALID_ACCESS){
				$("#invalidAccountAccessAddAlertLink").fadeTo('slow', 0.5);
				$("#invalidAccountAccessAddAlertLink").unbind("click");									
			}
		});
	}

});

ns.useralerts.unbindOneTimeAlerts = function(){
	$("#unreadBankMessageAddAlertLink").fadeTo('slow', 1);
	$("#unreadBankMessageAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);

	$("#passChangeAddAlertLink").fadeTo('slow', 1);
	$("#passChangeAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);

	$("#bankEnquiryAddAlertLink").fadeTo('slow', 1);
	$("#bankEnquiryAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);

	$("#failedAttemptLockoutAddAlertLink").fadeTo('slow', 1);
	$("#failedAttemptLockoutAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);

	$("#invalidAccountAccessAddAlertLink").fadeTo('slow', 1);
	$("#invalidAccountAccessAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);	
	
	$("#positivePayAddAlertLink").fadeTo('slow', 1);
	$("#positivePayAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);		
	
	$("#reversePositivePayAddAlertLink").fadeTo('slow', 1);
	$("#reversePositivePayAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);		

	$("#insufficientFundsAddAlertLink").fadeTo('slow', 1);
	$("#insufficientFundsAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);	
	
	$("#paymentApprovalsAddAlertLink").fadeTo('slow', 1);
	$("#paymentApprovalsAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);
	
	$("#panicPayAddAlertLink").fadeTo('slow', 1);
	$("#panicPayAddAlertLink").unbind("click").bind("click",ns.useralerts.addNewAlertClickHandler);
	
	
}

ns.useralerts.viewAlertLog = function(alertCode){
	//console.info("ns.useralerts.viewAlertLog" + alertCode);
	var optionValue="";
	optionValue	= ns.useralerts.getAlertTypeDropDownValue(alertCode);
	//console.info("ns.useralerts.viewAlertLog -optionIndex " + optionValue);
	$("#FindLogMessages_Type").selectmenu("value",optionValue);	
	$("#showAlertsLogsLink").attr("alertType",optionValue);
	$("#showAlertsLogsLink").click(); // ns.alert.gotoLog();
	
	
}

ns.useralerts.editAlert = function(alertId){
	
	ns.common.setModuleType("SubscribedAlertsSummary"); //To track last page.
	
	// Hide result messages
	//console.info("ns.useralerts.editAlert");
	ns.common.hideStatus();
	if(alertId){
		ns.useralerts.mode = "EDIT";
		
		var alerts = ns.useralerts.subscribedAlerts;
		var alertCode;
		$.each(alerts,function(i,alert){
			if(alert.userAlertId == alertId){
				alertCode = alert.alertType;
			}
		});
			
		$("input[name=alertId]").val(alertId);
		$("input[name=alertType]").val(alertCode);
		$("input[name=operation]").val(ns.useralerts.mode);
			
		
		ns.useralerts.addNewAlertCode = alertCode;
		ns.useralerts.toggleAlertFormContainer(alertCode);
		ns.useralerts.toggleAlertGridContainer();		
		ns.useralerts.showDelOptionsGrid(alertCode);
		ns.useralerts.showAccountDropDown(alertCode);

		$("a[alertbuttontype='addNew']").hide();
		$("a[alertbuttontype='save']").show();

		$("a[alertbuttontype='save']").data("alertId",alertId);
		$("a[alertbuttontype='save']").data("alertCode",alertCode);

		ns.useralerts.setEditForm(alertId,alertCode);
	}
	$("#alertsMainSummaryContainer").show();
	$("#addEditAlerts").hide();
	$.publish("common.topics.tabifyDesktop");
	$("#availableAlertTypesContainer").hide();

};

ns.useralerts.beforeDeleteAlert = function(alertId,message){
	//console.info("ns.useralerts.beforeDeleteAlert " + alertId + "," + message );	
	$("#deleteAlertDialogContainer").data("alertId",alertId);
	$("#deleteAlertDialogContainer").data("alertMsg",message);
	if(ns.useralerts.isDeleteDialogLoaded){
		if(message){
			$("#deleteAlertMessage").html($("#spanDelMsg").html() + "<br>\"" + message + "\"" );
		}
		$("#deleteAlertDialog").dialog('open');		
	}else{
		ns.useralerts.loadDeleteConfirmDialog();
	}	
}

ns.useralerts.deleteAlert = function(alertId){
	//console.info("ns.useralerts.deleteAler-" + alertId);
	var URL = "/cb/pages/jsp/alert/deleteAlertAction.action?alertId=" + alertId;
	if(alertId){		
		$.ajax({
			  type: 'GET',
			  url:URL,
			  success:function(response){
					ns.common.showStatus(js.alert.deleteAlert.message);
					$('#alertsGrid').trigger("reloadGrid");
					ns.useralerts.getUserAlerts();	
					$.publish("common.topics.tabifyDesktop");	
					if(accessibility){
						$('#alertsGrid').setFocus();
					}				
			  }
		});					
	}
};

ns.useralerts.loadDeleteConfirmDialog = function(){
	var deleteAlertDiaURL = "/cb/pages/jsp/alert/inc/delete_alert_dialog.jsp";
	$.ajax({
	  type: 'GET',
	  url: deleteAlertDiaURL,
	  success: function(response){
		//console.info("received delete dialog");
		$("#deleteAlertDialogContainer").html(response);
		ns.useralerts.initDeleteDialog();
		$("#deleteAlertDialog").dialog('open');
		ns.useralerts.isDeleteDialogLoaded = true;
	  }
	});
};

ns.useralerts.initDeleteDialog = function(){
	//console.info("ns.useralerts.initDeleteDialog");
	//update the message
	$("#deleteAlertMessage").html($("#spanDelMsg").html() + "<br>\"" + $("#deleteAlertDialogContainer").data("alertMsg")+ "\"" );
	
	var cancelDeleteHandler = function(){
		$("#deleteAlertDialog").dialog('close');
		$("#deleteCPDialogContainer").removeData("alertId");
		$("#deleteCPDialogContainer").removeData("alertMsg");
	};
	$("#cancelDeleteAlertBtn").on("click",cancelDeleteHandler);
	
	var deleteAlertHandler = function(){
		//console.info("deleting after confirmation..");   
		var alertId = $("#deleteAlertDialogContainer").data("alertId");
		var alertMessage = $("#deleteAlertDialogContainer").data("alertMsg");
		if(alertId){
			ns.useralerts.deleteAlert(alertId);			
		}		
		cancelDeleteHandler();
	}
	$("#deleteAlertBtn").on("click",deleteAlertHandler);
};

ns.useralerts.removeDialgIfAlreadyExists = function(){
	var deleteDia = $("#deleteAlertDialog");
	if(deleteDia){
		//console.info("delete alert dialog exists");
		$("#deleteAlertDialog").remove();
	}else{
		//console.info("there is no delete alert dialog");
	}
};


//Stock Alert Dialog
/*
ns.useralerts.isStockPortfolioDialogLoaded = false;
ns.useralerts.loadStockPortfolioDialog = function(){
	if(!ns.useralerts.isStockPortfolioDialogLoaded){
		console.info("loadStockPortfolioDialog-init and load");
		var stockPortfolioAlertDiaURL = "/cb/pages/jsp/alert/inc/stock_portfolio_dialog.jsp";
		$.ajax({
		  type: 'GET',
		  url: stockPortfolioAlertDiaURL,
		  success: function(response){
			console.info("received stock Portfolio dialog");
			$("#stockPortfolioDialogContainer").html(response);
			$("#stockPortfolioDialog").dialog('open');
			ns.useralerts.isStockPortfolioDialogLoaded = true;
			var additionalSettings = {persistSettings:true};
			ns.home.addPortlet(ns.home.createPortletId('STOCKS'), 'Stocks Portfolio', 'StockPortletAction_loadViewMode','StockPortletAction_loadEditMode', false,null,null,additionalSettings);
		  }
		});	
	}else{
		console.info("loadStockPortfolioDialog-open");
		$("#stockPortfolioDialog").dialog('open');
	}
	
};
*/

ns.useralerts.getDeliveryPreferences = function(){
	var deliveryPrefs = [];
	var options = $("#alertDeliveryOptions input[type=radio]:checked");
	$.each(options,function(i,option){
		var id = $(option).attr("contactpointid");
		var name = "";
		var order= $(option).val();
		var pref = {
				contactPointId:id,
				contactPointName:name,
				deliveryOrder:order
		}
		deliveryPrefs.push(pref);
	});
	return deliveryPrefs;
};

ns.useralerts.closeAlertPortlet = function(e){
	//console.info("ns.useralerts.closeAlertPortlet ");
	$.publish('ns_ua_removeErrorsOnBegin');
	
	//Get last page and display
	if(ns.common.moduleType!= undefined && ns.common.moduleType=="SubscribedAlertsSummary"){
		$("#addEditAlerts").show(); //show subscribed summary grid
		ns.common.setModuleType(""); //reset
	}else if(ns.common.moduleType!= undefined && ns.common.moduleType == "AddEditAlert"){
		$("#availableAlertTypesContainer").show(); //show AddEdit Alert
	}
	$("#addEditAlertContainer").hide();

};

ns.useralerts.deliveryOptionsGridOnComplete = function(e,d){
	//console.info("ns.useralerts.deliveryOptionsGridOnComplete");		
};


ns.useralerts.alertsGridOnComplete = function(e,d){
	$("#alertsGrid_pager_center").hide();
	$.publish("common.topics.tabifyNotes");	
	if(accessibility){		
		$("#alertsGrid").setFocus();
	}
	
};

ns.useralerts.getPropertyValue = function(property){
	var properties = ns.useralerts.properties;
	var propertyValue="";
	if(properties){
		$.each(properties,function(i,prop){
			if(prop.key === property){
				propertyValue=prop.value;
			}
		});
	}
	//console.info("ns.useralerts.getPropertyValue: property-" + property + ", value-"+propertyValue);
	return propertyValue;
};
ns.useralerts.addGridData = function(contactPoints){
	//console.info("ns.useralerts.addGridData " + contactPoints );
	if(contactPoints){
		if(contactPoints.length === 0){
			//if no contact points are add message center here.
			var contactPoints = [{"address":null,"contactPointID":0,"contactPointType":1,"name":"Alerts Center","secure":true,"order":1,"disabled":""},{"address":null,"contactPointID":3,"contactPointType":4,"name":"Push Notification","secure":false,"order":0,"disabled":""}];
		}
		var gridModel = contactPoints;		
		//Reinitialze the grid
		var columnNames = [
			js.alert.deliveryoptionsgrid.column.contactpoint.id || "Id",
			js.alert.deliveryoptionsgrid.column.contactpoint.type || "Type",
			js.alert.deliveryoptionsgrid.column.deliveryoptions.name || "Contact Point",
			js.alert.deliveryoptionsgrid.column.primary.name || "Primary",
			js.alert.deliveryoptionsgrid.column.backup.name || "Backup",
			js.alert.deliveryoptionsgrid.column.off.name || "Off"
		];
		

		$("#deliveryOptionsGrid").jqGrid("clearGridData");//destroy the old instance and recreate
		$("#delOptions").empty();
		$("<table id='deliveryOptionsGrid'></table>").appendTo($("#delOptions"));

		$("#deliveryOptionsGrid").jqGrid({
			datatype: "local",
			height: 'auto',
		   	colNames:columnNames,
		   	colModel:[
		   		{name:'contactPointID',index:'id',hidden:true,hidelg:true,formatter:ns.useralerts.contactsPointFormatter},
		   		{name:'contactPointType',index:'type',hidden:true,hidelg:true},
		   		{name:'name',index:'name', },
		   		{name:'primary',index:'primary', formatter:ns.useralerts.radioButtonFormatter},
		   		{name:'backup',index:'backup', formatter:ns.useralerts.radioButtonFormatter},
		   		{name:'off',index:'off', formatter:ns.useralerts.radioButtonFormatter}
		   	],
		   	onComplete:function(){
		   		$.publish("deliveryOptionsGridOnComplete")
		   	}
		});

		$("#deliveryOptionsGrid").jqGrid("clearGridData");
		$.each(gridModel,function(r,rowData){
			$("#deliveryOptionsGrid").jqGrid('addRowData',r,rowData);
		});		
		ns.useralerts.deliveryOptionsGrid.initialized = true;			
	}
};


ns.useralerts.getLookupBoxOptionForAccount = function(accountId){
	var accounts = ns.useralerts.accounts;
	var accountOption = {
		"label":"",
		"value":""
	};
	if(accounts){
		for(var i=0;i<accounts.length;i++){
			var account = accounts[i];
			if(account.ID === accountId){
				var optionValue = "";
				if(ns.useralerts.isConsumer()){
					optionValue = account.routingNum + ";" + accountId + ";" + account.type;
				}else{
					optionValue = accountId;
				}
				accountOption = {
					"label":account.searchDisplayText,
					"value":optionValue
				}
				return accountOption;
			}
		}
	}	
};

ns.useralerts.checkForInactiveAccounts = function(accounts){
	if(accounts){
		if(accounts.length==0){
			$("span[messageFor=inactiveAccount]").show();
		}else{
			$("span[messageFor=inactiveAccount]").hide();
		}
	}
};
ns.useralerts.initTransactionBox = function(options){
	
	var defaultOption = "<option value=''>" + js_all_transactions + "</option>";	
	$(defaultOption).appendTo($("#transactionTypeSelectBox"));
	$(defaultOption).appendTo($("#immediateTransactionTypeSelectBox"));
	if(options){
		$.each(options,function(i,option){
			var opValue = option.value;
			var option = "<option value='" + option.key +"'>" + option.value + "</option>";
			$(option).appendTo($("#transactionTypeSelectBox"));
			if(opValue =='Debit' || opValue =='Credit'){
				$(option).appendTo($("#immediateTransactionTypeSelectBox"));
			}
		});
		
		$("#transactionTypeSelectBox").selectmenu({"width":200	});
		$("#immediateTransactionTypeSelectBox").selectmenu({"width":200	});
	}	
};

ns.useralerts.initAmmountFilter = function(options){
	if(options){
		$.each(options,function(i,option){
			var option = "<option value='" + option.key +"'>" + option.value + "</option>";
			$(option).appendTo($("#transactionAmountSelectBox"));
			$(option).appendTo($("#immediateTransactionAmountSelectBox"));
		});
		
		$("#transactionAmountSelectBox").selectmenu({
			width:200,
			"change":function(){
					$.publish("onTransactionSelectChange");
					$.publish("ns_ua_removeErrorsOnBegin");					
			}
		});			
		$("#immediateTransactionAmountSelectBox").selectmenu({
			width:200,
			"change":function(){
					$.publish("onTransactionSelectChange");
					$.publish("ns_ua_removeErrorsOnBegin");					
			}
		});	
	}	
};

ns.useralerts.showAccountDropDown = function(alertCode){
	var accountSelectDropDown = $("#alertsAccountSelect");	
	var container = "div[alertcode="+ alertCode  + "][alertcontainer=accountSelectBox]:eq(0)";
	//console.info("showAccountDropDown-alert code:" + alertCode + ", container:" + container);
	$(container).append(accountSelectDropDown);	
};


ns.useralerts.showTrasactionAlertBox = function(alertCode){
	
	if(alertCode == 3 || alertCode == 300){
		$("#trasactionAlertBox").show();
	}else{
		$("#trasactionAlertBox").hide();
	}
};


ns.useralerts.showMinMaxAmount = function(alertCode){
	if(alertCode == 1){
		$("#minMaxAmountBox").show();
	}else{
		$("#minMaxAmountBox").hide();
	}
};

ns.useralerts.showStockBox = function(alertCode){
	if(alertCode == 5){
		$("#stockBox").show();
	}else{
		$("#stockBox").hide();
	}
};

ns.useralerts.showDelOptionsGrid = function(alertCode){
	
	var grid = $("#delOptions");	
	var container = "div[alertcode="+ alertCode  + "][alertcontainer=delOptionsGrid]";
	//console.info("showDelOptionsGrid- " + container);
	$("#deliveryOptionsGrid").jqGrid('setGridWidth',620,true);
	var x = $("#deliveryOptionsGrid input[data-contactpoint-type='"+ns.useralerts.contactTypes.CONTACTPOINT_TYPE_PUSH_NOTIFICATION+"']");
	var row = $(x).closest('tr');
	row.hide();
	if(alertCode == ns.useralerts.constants.ALERT_TYPE_PAYMENT_APPROVAL){
		row.show();
	}
	$(container).append(grid);		
};


ns.useralerts.setEditForm = function(alertId,alertCode){
	//console.info("setEditForm---");
	ns.useralerts.resetDeliveryPreferences(alertCode);
	ns.useralerts.resetFormFields();
	
	var subscribedAlerts = ns.useralerts.subscribedAlerts;
	$.each(subscribedAlerts,function(i,subscribedAlert){
		if(subscribedAlert.userAlertId == alertId){
			var additionalProperties = subscribedAlert.additionalProperties;
			var primaryContacts = additionalProperties.PrimaryContacts;
			var secondaryContacts = additionalProperties.SecondaryContacts;
			//console.info("primary contacts-" + primaryContacts + " and  secondary contacts-" + secondaryContacts);
			
			var primaryContactPoints;
			var selector;
			if(primaryContacts){
				primaryContactPoints = primaryContacts.split(",");
				
				for(i=0;i<primaryContactPoints.length;i++){
				//console.info(primaryContactPoints[i]);
					selector = "input[name=deliveryOption" + primaryContactPoints[i]  + "][value='primary']";
					$(selector).attr("checked",true);				
				}
			}
			
			var secondaryContactPoints;			
			if(secondaryContacts){
				secondaryContactPoints = secondaryContacts.split(",");
				for(i=0;i<secondaryContactPoints.length;i++){
					//console.info(secondaryContactPoints[i]);
					selector = "input[name=deliveryOption" + secondaryContactPoints[i]  + "][value='backup']";
					$(selector).attr("checked",true);				
				}				
			}

			//showing account
			var acccountOption;	
			if(subscribedAlert.accountId){			
				acccountOption = ns.useralerts.getLookupBoxOptionForAccount(subscribedAlert.accountId)
			}else if(subscribedAlert.accountId == null){
				acccountOption = {value:"All Accounts", label:"All Accounts"};
			}
			
			if($("#alertsAccountSelectBox").data("uxLookupbox")){
				$("#alertsAccountSelectBox").lookupbox("value",acccountOption);	
			}
	
			
			
			//Check the freq
			//debugger;
			if(subscribedAlert.oneTime){
				if(subscribedAlert.oneTime === "true"){
					$("div[alertcode=" + alertCode + "] input[id=radioFirstTime]").attr("checked","checked");
					//$("#radioFirstTime").attr("checked","checked");
				}else{
					$("div[alertcode=" + alertCode + "] input[id=radioEveryTime]").attr("checked","checked");
					//$("#radioEveryTime").attr("checked","checked");
				}				
			}
			
			//show additional properties
			if(additionalProperties["MIN_AMOUNT"]){
				$("#minAmount").val(additionalProperties["MIN_AMOUNT"]);
				$("#minAmount").show();
			}
			if(additionalProperties["MAX_AMOUNT"]){
				$("#maxAmount").val(additionalProperties["MAX_AMOUNT"]);
				$("#maxAmount").show();
			}
			if(additionalProperties["AMOUNT"]){
				$("#transactionAmount").val(additionalProperties["AMOUNT"]);
				$("#transactionAmount").show();
				$("#immediateTransactionAmount").val(additionalProperties["AMOUNT"]);
				$("#immediateTransactionAmount").show();
				
				var amount = additionalProperties["AMOUNT"];
				if(amount !== "0.0"){
					$("#stockAmount").val(amount); 
					$("#stockAmount").show();
				}
			}
			if(additionalProperties["COMPARE"]){
				$("#transactionAmountSelectBox").selectmenu("value",additionalProperties["COMPARE"]);
				$("#immediateTransactionAmountSelectBox").selectmenu("value",additionalProperties["COMPARE"]);
				$("#stockCriteria").selectmenu("value",additionalProperties["COMPARE"]); //corp stock compare
				if(additionalProperties["COMPARE"] == 1){
						$("#stockAmount").hide();
				}else{
						$("#stockAmount").show();
				}
				
			}
			if(additionalProperties["TRANSACTION TYPE"]){
				$("#transactionTypeSelectBox").selectmenu("value",additionalProperties["TRANSACTION TYPE"]);
				$("#immediateTransactionTypeSelectBox").selectmenu("value",additionalProperties["TRANSACTION TYPE"]);
			}else {
				$("#transactionTypeSelectBox").selectmenu("value","");
				$("#immediateTransactionTypeSelectBox").selectmenu("value","");
			}
			if(additionalProperties["FreeFormStockList"]){
					var otherStocksBoxes = $("input[name=otherStocks]");
					var otherStocksStr = additionalProperties["FreeFormStockList"];
					if(otherStocksStr){
						var otherStocks =  otherStocksStr.split(",");
						for(i=0;i<otherStocks.length;i++){
							var stockBox = $("input[name=otherStocks]")[i];
							$(stockBox).val(otherStocks[i]);
						}						
					}
			}
			if(additionalProperties["PortfolioStockList"]){
				var portfolioStocksStr = additionalProperties["PortfolioStockList"];				
				if(portfolioStocksStr){
					$("input[name=portfolioStocks]").attr("checked",false);
					var portfolioStocks = portfolioStocksStr.split(",");
					for(i=0;i<portfolioStocks.length;i++){
						$("input[name=portfolioStocks][value=" + portfolioStocks[i]  + "]").attr("checked", true);						
					}
					
					//for corp fields
					$("#stockSymbol").val(portfolioStocksStr);
				}
			}
			if(additionalProperties["Symbol"]){
				var stockSymbol = additionalProperties["Symbol"];
				$("#stockSymbol").val(stockSymbol);
			}
			
		}
		
	});
	
};


ns.useralerts.resetDeliveryPreferences = function(alertCode){
	var rows = $("#deliveryOptionsGrid").jqGrid('getGridParam', 'records');
	var alertCenter = $("#deliveryOptionsGrid input[type=radio][value=primary][data-contactpoint-type='"+ns.useralerts.contactTypes.CONTACTPOINT_TYPE_ALERT_CENTRE+"']");
	var pushNotification = $("#deliveryOptionsGrid input[type=radio][value=primary][data-contactpoint-type='"+ns.useralerts.contactTypes.CONTACTPOINT_TYPE_PUSH_NOTIFICATION+"']"); 
	if(rows === 1 || (rows === 2 && alertCode != ns.useralerts.constants.ALERT_TYPE_PAYMENT_APPROVAL && alertCenter != null && alertCenter.length >0 && pushNotification != null && pushNotification.length >0)){
		//this means we have one record of message center preset with primary as active and others disabled		
		$("#deliveryOptionsGrid input[type=radio][value=backup]").attr("disabled","disabled");
		$("#deliveryOptionsGrid input[type=radio][value=off]").attr("disabled","disabled");
		$("#deliveryOptionsGrid input[type=radio][value=primary]").attr("checked","checked");
	} else{
		$("#deliveryOptionsGrid [value=off]").attr("checked", true);
		$("#deliveryOptionsGrid input[type=radio][value=backup]").removeAttr("disabled");
		$("#deliveryOptionsGrid input[type=radio][value=off]").removeAttr("disabled");
		/*for(var i=0; i < rows; i++) {
			//var row = $("#deliveryOptionsGrid").jqGrid('getRowData', i);			
			$("#deliveryOptionsGrid [id=off"+i+"]").attr("checked", true);			
		}*/
	}	
};

ns.useralerts.resetFormFields = function(){
	$("#minAmount").val('');
	$("#maxAmount").val('');
	$("#transactionAmount").val('');
	$("#transactionAmount").hide();
	$("#immediateTransactionAmount").val('');
	$("#immediateTransactionAmount").hide();
	$("input[name=otherStocks]").val('');
	//ns.useralerts.resetPortfolioStocks();
	$("#transactionAmountSelectBox").selectmenu().selectmenu("value","1");	
	$("#transactionTypeSelectBox").selectmenu().selectmenu("value","");
	$("#immediateTransactionAmountSelectBox").selectmenu().selectmenu("value","1");	
	$("#immediateTransactionTypeSelectBox").selectmenu().selectmenu("value","");
	ns.useralerts.resetLookUpBox();
	
	
	$("#stockSymbol").val('');
	$("#stockCriteria").selectmenu().selectmenu("index", 0);
	$("#stockAmount").val('').hide();
	
	removeValidationErrors();//remove validation errors if any.
};


ns.useralerts.resetLookUpBox = function(){
	if($("#alertsAccountSelectBox").data("uxLookupbox")){
		$("#alertsAccountSelectBox").lookupbox("clear");
		$("#alertsAccountSelectBox").html('');
	}
};

ns.useralerts.radioButtonFormatter = function(cellvalue, options, rowObject) { 
	//console.info("ns.useralerts.radioButtonFormatter");
	var colName = options.colModel.name;
	var id =  colName + options.rowId;	
	var name = "deliveryOption";
	var contactPointId = "";
	contactPointId = rowObject.contactPointID;
	var contactPointType = "";
	contactPointType = rowObject.contactPointType;
	var value = "<input type='radio' data-contactpoint-type='"+contactPointType+"' value='" + colName + "' name='" + name + contactPointId + "' id='" + id + "' contactPointId = '" + contactPointId + "'>";	
	return ' ' + value + ' ';
};

ns.useralerts.contactsPointFormatter = function(cellvalue, options, rowObject) { 
	var contactPointId = "";
	contactPointId = rowObject.contactPointID;
	
	var cps = "<input type='text' name='deliveryOptions' value='" + contactPointId + "'/>"
	return ' ' + cps + ' ';
};

ns.useralerts.initializePortlets = function(){	
	
	//ns.home.addPortlet(ns.home.createPortletId('STOCKS'), 'Stocks Portfolio', 'StockPortletAction_loadViewMode','StockPortletAction_loadEditMode', false);
	
	$('#availableAlertTypesContainer').portlet({
		bookmark: true,
		bookMarkCallback: function(){
			var path = $('#availableAlertTypesContainer').find('.shortcutPathClass').html();
			var ent  = $('#availableAlertTypesContainer').find('.shortcutEntitlementClass').html();
			ns.shortcut.openShortcutWindow( path, ent );
		},
		helpCallback: function(){
			var fileName = $('#alertsMainSummaryContainer').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/','/cb/web/help/' + fileName);
		},
		title: "Alerts",
		generateDOM: true
	});
	
	
	
	/*$('#alertAccountBalanceThresholdForm').portlet({
		title: js_alert_balance_threshold_form_title,
		generateDOM: true
	});	*/
	
	/*$('#alertAccountBalanceSummaryForm').portlet({
		title: js_alert_balance_summary_form_title,
		generateDOM: true
	});	*/

	/*$('#alertPositivePayForm').portlet({
		title: "Add/Edit Positive Pay Alert",
		generateDOM: true
	});	*/
	
	/*$('#alertTransactionForm').portlet({
		title: js_alert_transaction_form_title,
		generateDOM: true
	});	*/
	
	/*$('#alertInsufficientFundsForm').portlet({
		title: js_alert_insufficient_funds_title,
		generateDOM: true
	});	*/
	
	/*$('#alertChangePasswordForm').portlet({
		title: js_alert_change_password_form_title,
		generateDOM: true
	});	
		*/

	/*$('#alertInvalidAccountForm').portlet({
		title: js_alert_invalid_account_access_form_title,
		generateDOM: true
	});	*/
	

	/*$('#alertFailedAttemptLockoutForm').portlet({
		title: js_alert_lockout_form_title,
		generateDOM: true
	});	*/
	
	/*$('#alertBankInquiryResponseForm').portlet({
		title: js_alert_bank_response_form_title,
		generateDOM: true
	});	*/
	
	/*$('#alertUnreadBankMessageForm').portlet({
		title: js_alert_unread_message_form_title,
		generateDOM: true
	});*/
	
	/*$('#alertPanicPayForm').portlet({
		title: js_alert_panic_pay_form_title,
		generateDOM: true
	});*/
	
	/*$('#alertStockPortfolioForm').portlet({
		title: js_alert_stock_form_title,
		generateDOM: true
	});	*/

	/*$('#alertPaymentApprovalsForm').portlet({
		title: js_alert_payment_approvals_form_title,
		generateDOM: true
	});	*/

	
};

ns.useralerts.toggleAlertFormContainer = function(alertCode){
	//console.info("ns.useralerts.toggleAlertFormContainer-");
	$("#availableAlertTypesContainer").slideToggle("slow");
	$("#addEditAlertContainer").slideToggle("slow");
	var formSelector = "#addEditAlertContainer>div[alertcode=" + alertCode + "]";
	var action = "";
	if(alertCode){
		var appCode;
		if(ns.useralerts.isConsumer()){
			appCode = "consumer";
		}else{
			appCode = "corporate";
		}	
		var mode  = ns.useralerts.mode;
		mode = mode.toLowerCase();
		if(ns.useralerts.config.actions[appCode][alertCode]){
			action = ns.useralerts.config.actions[appCode][alertCode][mode];	 
			$(formSelector).find("form").attr("action",action);
		}		
	}	
	
	$(formSelector).show();
	$(formSelector).siblings().hide();
	if(accessibility){
		$("#addEditAlertContainer .portlet-content:visible").setInitialFocus();	
	}	
};

ns.useralerts.toggleAlertGridContainer = function(){
	$("#addEditAlerts").slideToggle();
};

ns.useralerts.amountBoxDisabler = function(){
	$.publish("ns_ua_removeErrorsOnBegin");
	var stockCriteria = $("#stockCriteria").val();
	var $stockAmount = $("#stockAmount");
	if(stockCriteria === "1"){
		$stockAmount.val("").hide();
	}else{
		$stockAmount.show();
	}
};

ns.useralerts.selectors = {
		ACCOUNT_BALANCE_THRESHOLD_ALERT_FORM:"#accountBalanceThresholdAlertForm",
		ACCOUNT_BALANCE_THRESHOLD_ALERT_PORTLET_TITLE:"#accBalanceThresholdPortletTitle",
		INSUFFICIENT_FUNDS_ALERT_FORM:"#insufficientFundsAlertForm",
		INSUFFICIENT_FUNDS_ALERT_PORTLET_TITLE:"#insufficentFundsPortletTitle",
		UNREAD_BANK_MESSAGE_ALERT_FORM:"#unreadBankMessageAlertForm",
		UNREAD_BANK_MESSAGE_ALERT_PORTLET_TITLE:"#unreadBankMessagePortletTitle",
		PANIC_PAY_ALERT_FORM:"#panicPayAlertForm",
		PANIC_PAY_ALERT_PORTLET_TITLE:"#panicPayPortletTitle",
		PASSWORD_CHANGE_ALERT_FORM:"#passwordChangeAlertForm",
		PASSWORD_CHANGE_ALERT_PORTLET_TITLE:"#passwordChangePortletTitle",
		STOCK_PORTFOLIO_ALERT_FORM:"#stockPortfolioAlertForm",
		STOCK_PORTFOLIO_ALERT_PORTLET_TITLE:"#stockPortfolioPortletTitle",
		ACCOUNT_BALANCE_SUMMARY_ALERT_FORM:"#accountBalanceSummaryAlertForm",
		ACCOUNT_BALANCE_SUMMARY_ALERT_PORTLET_TITLE:"#accountBalanceSummaryPortletTitle",
		TRANSACTION_ALERT_FORM:"#transactionAlertForm",
		TRANSACTION_ALERT_PORTLET_TITLE:"#transactionPortletTitle",
		BANK_ENQUIRY_RESPONSE_ALERT_FORM:"#bankEnquiryResponseAlertForm",
		BANK_ENQUIRY_RESPONSE_ALERT_PORTLET_TITLE:"#bankEnquiryPortletTitle",
		FAILED_ATTEMPT_LOCKOUT_ALERT_FORM:"#failedAttemptLockOutAlertForm",
		FAILED_ATTEMPT_LOCKOUT_ALERT_PORTLET_TITLE:"#failedAttemptPortletTitle",
		INVALID_ACCOUNT_ACCESS_ALERT_FORM:"#invalidAccountAccessAlertForm",
		INVALID_ACCOUNT_ACCESS_ALERT_PORTLET_TITLE:"#invalidAccountAccessPortletTitle",
		AVAILABLE_ALERT_TYPES_CONTAINER:"#availableAlertTypesContainer",
		ACCOUNT_BALANCE_THRESHOLD_HEADER:"#accBalanceThresholdHeader",
		ACCOUNT_BALANCE_SUMMARY_HEADER:"#accountBalanceSummaryHeader",
		INSUFFICENT_FUNDS_HEADER:"#insufficentFundsHeader",
		TRANSACTION_HEADER:"#transactionHeader",
		UNREAD_BANK_MESSAGE_HEADER:"#unreadBankMessageHeader",
		BANK_ENQUIRY_HEADER:"#bankEnquiryHeader",
		FAILED_ATTEMPT_HEADER:"#failedAttemptHeader",
		PASSWORD_CHANGE_HEADER:"#passwordChangeHeader",
		STOCK_PORTFOLIO_HEADER:"#stockPortfolioHeader",
		INVALID_ACCOUNT_ACCESS_HEADER:"#invalidAccountAccessHeader",
		ACCOUNT_BALANCE_THRESHOLD_FOOTER:"#accBalanceThresholdFooter",
		ACCOUNT_BALANCE_SUMMARY_FOOTER:"#accountBalanceSummaryFooter",
		INSUFFICENT_FUNDS_FOOTER:"#insufficentFundsFooter",
		TRANSACTION_FOOTER:"#transactionFooter",
		UNREAD_BANK_MESSAGE_FOOTER:"#unreadBankMessageFooter",
		BANK_ENQUIRY_FOOTER:"#bankEnquiryFooter",
		FAILED_ATTEMPT_FOOTER:"#failedAttemptFooter",
		PASSWORD_CHANGE_FOOTER:"#passwordChangeFooter",
		STOCK_PORTFOLIO_FOOTER:"#stockPortfolioFooter",
		INVALID_ACCOUNT_ACCESS_FOOTER:"#invalidAccountAccessFooter",
		ACCOUNT_BALANCE_THRESHOLD_FREQUENCY:"#accountBalanceThresholdFrequency",
		INSUFFICENT_FUNDS_FREQUENCY:"#insufficentFundsFrequency",
		TRANSACTION_FREQUENCY:"#transactionFrequency",
		IMMEDIATE_TRANSACTION_ALERT_FORM:"#immediateTransactionAlertForm",
		IMMEDIATE_TRANSACTION_ALERT_PORTLET_TITLE:"#immediateTransactionPortletTitle",
		IMMEDIATE_TRANSACTION_FOOTER:"#immediateTransactionFooter",
		IMMEDIATE_TRANSACTION_HEADER:"#immediateTransactionHeader",
};


ns.useralerts.mode = "ADD";
ns.useralerts.addNewAlertCode = -1;
ns.useralerts.viewInitialized = false;
ns.useralerts.deliveryOptionsGrid = {
	initialized :false,
	contactPoints:[]
};

ns.useralerts.subscribedAlerts = {};
ns.useralerts.deliveryPreferences = {};
ns.useralerts.properties = {};

//These constants definitions are taken from UserAlert.java These constants will take care UI part
ns.useralerts.constants = {
	ALERT_TYPE_BANK_MESSAGE : "0",
	ALERT_TYPE_UNREAD_BANK_MESSAGE : "6",
	ALERT_TYPE_ACCOUNT_BALANCE:"1",
	ALERT_TYPE_ACCOUNT_SUMMARY : "2",	
	ALERT_TYPE_NSF : "4",
	ALERT_TYPE_TRANSACTION : "3",
	ALERT_TYPE_PAYMENT_APPROVAL : "10",   //changed the name to PAYMENT_APPROVAL alert as 5 values is been used for this alert type 
	ALERT_TYPE_POSITIVE_PAY : "15",
	ALERT_TYPE_REVERSE_POSITIVE_PAY : "21",
	ALERT_TYPE_PASSWORD_CHANGE : "11",
	ALERT_TYPE_INQUIRY_RESPONSE : "12",
	ALERT_TYPE_LOCKOUT : "13",
	ALERT_TYPE_INVALID_ACCESS : "14",
	ALERT_TYPE_ACCOUNT_BALANCE_CORPORATE:"17",
	ALERT_TYPE_TRANSACTION_CORPORATE : "18",
	ALERT_TYPE_NFS_CORPORATE:"16",
	ALERT_TYPE_PANIC_PAY:"19",
	ALERT_TYPE_IMMEDIATE_TRANSACTION:"300"
};

ns.useralerts.contactTypes = {
	CONTACTPOINT_TYPE_ALERT_CENTRE:"1",
	CONTACTPOINT_TYPE_SMS:"2",
	CONTACTPOINT_TYPE_EMAIL:"3",
	CONTACTPOINT_TYPE_PUSH_NOTIFICATION:"4"
};

ns.useralerts.alertTypes = {
	consumer:{
		"1":{
			key:"ACCOUNT_BALANCE",
			value:"AccountBalance"
		},
		"2":{
			key:"ACCOUNT_SUMMARY",
			value:"ACCOUNTSUMMARY"
		},	
		"3":{
			key:"TRANSACTION",
			value:"Transaction"
		},
		"4":{
			key:"NSF",
			value:"NSF"
		},
		"5":{
			key:"STOCK",
			value:"STOCK"
		},
		"6":{
			key:"BANK_MESSAGE",
			value:"BankMessage"
		},
		"11":{
			key:"PASSWORD_CHANGE",
			value:"PasswordChange"
		},
		"12":{
			key:"INQUIRY_RESPONSE",
			value:"InquiryResponse"
		},
		"13":{
			key:"LOCKOUT",
			value:"Lockout"
		},
		"14":{
			key:"INVALID_ACCESS",
			value:"InvalidAccess"
		},
		"19":{
			key:"PANIC_PAY",
			value:"PanicPay"
		},
		"300":{
			key:"IMMEDIATE_TRANSACTION",
			value:"ImmediateTransaction"
		}
	},
	corporate:
	{
		"6":{
			key:"BANK_MESSAGE",
			value:"BankMessage"
		},
		"17":{
			key:"ACCOUNT_BALANCE",
			value:"AccountBalance"
		},
		"9":{
			key:"STOCK",
			value:"ProcessStockPortfolio"	
		},	
		"16":{
			key:"NSF",
			value:"NSF"
		},
		"18":{
			key:"TRANSACTION",
			value:"Transaction"
		},
		"10":{
			key:"PAYMENT_APPROVALS",
			value:"PaymentApprovals"
		},
		"15":{
			key:"POSITIVE_PAY",
			value:"PositivePay"
		},
		"21":{
			key:"REVERSE_POSITIVE_PAY",
			value:"ReversePositivePay"
		},
		"7":{
			key:"STOCK_PORTFOLIO",
			value:"ProcessStockPortfolio"
		},
		"11":{
			key:"PASSWORD_CHANGE",
			value:"PasswordChange"
		},
		"12":{
			key:"INQUIRY_RESPONSE",
			value:"InquiryResponse"
		},
		"13":{
			key:"LOCKOUT",
			value:"Lockout"
		},
		"14":{
			key:"INVALID_ACCESS",
			value:"InvalidAccess"
		},
		"19":{
			key:"PANIC_PAY",
			value:"PanicPay"
		}
	}
};

ns.useralerts.isConsumer = function(){
	var isConsumer = false;
	var appCode = ns.useralerts.getPropertyValue("appCode");	
	return (appCode === "0") ? false: true;
};

ns.useralerts.getAlertTypeDropDownValue = function(alertCode){
	//console.info("ns.useralerts.getAlertTypeDropDownValue");
	var appCode = ns.useralerts.getPropertyValue("appCode");
	var optionValue = "";
	var appType = (appCode === "0") ? "corporate": "consumer";
	optionValue =  ns.useralerts.alertTypes[appType][alertCode].value;
	return optionValue;
}

//Subscribed alerts grid
ns.useralerts.actionColumnFormatter = function(cellvalue, options, rowObject){
	var viewAlretLogLink = "<a  id='viewLog" + rowObject.userAlertId  + "' title='" + js_view_alertLog + "' href='#' onClick='ns.useralerts.viewAlertLog(\"" + rowObject.alertType  + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var testLink = "<a  id='edit" + rowObject.userAlertId  + "' title='" + js_edit + "' href='#' onClick='ns.useralerts.editAlert(\"" + rowObject.userAlertId  + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var trashLink = "<a id='delete" + rowObject.userAlertId  + "' title='" + js_delete + "' href='#' onClick='ns.useralerts.beforeDeleteAlert(\"" + rowObject.userAlertId + "\",\"" + rowObject.alertDisplayText + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>"; 
	return ' ' + viewAlretLogLink + ' ' + testLink + ' ' + trashLink + ' ';

}

ns.useralerts.setup =function(config){
	ns.useralerts.isViewInitialized = false;
	ns.useralerts.deliveryOptionsGrid.initialized = false;
	ns.useralerts.isDeleteDialogLoaded = false;
	ns.useralerts.initView(config);
	ns.useralerts.getUserAlerts();
	ns.useralerts.getPortfolioStocks();
};
