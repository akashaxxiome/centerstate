

ns.alert.setup = function(){
	
	ns.alert.whichgrid = 'inboxAlertsGridId';
	ns.alert.lastgrid = ns.alert.whichgrid;
	ns.common.refreshDashboard('dbAlertsSummary');
	
	//$('#showAlertsInboxLink').attr('style','display:none');

	/**
	 * Loading subscribing Alerts form
	 */
	$.subscribe('beforeLoadAlertsForm', function(event,data) {
		//alert('beforeLoadAlertsForm');
	});

	$.subscribe('loadAlertsFormComplete', function(event,data) {
		//alert('loadAlertsFormComplete');
		$('#summary').slideUp(); 
	});

	$.subscribe('errorLoadAlertsForm', function(event,data) {
		//alert('errorLoadAlertsForm');
		
		
	});

	$.subscribe('successLoadAlertsForm', function(event,data) {
		//alert('successLoadAlertsForm');
		
		//if SubscribedSummary clicked from dashboard
		ns.common.setModuleType("SubscribedAlertsSummary"); //To track last page.

		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
//		var heading = $('#PageHeading').html();
//		var $title = $('#details .portlet-title');
//		$title.html(heading);

		$('#configAlertsPortlet').portlet('show');
		$('#details').slideDown();
		$('#details2').slideUp(); //viewAlertPortlet

		ns.alert.gotoSubscribe();
		$.publish("common.topics.tabifyDesktop");

	});


	/**
	 * Saving Alerts Subscription
	 */
	$.subscribe('beforeSavingAlertSubscription', function(event,data) {
		//alert('beforeSavingAlertSubscription');
	});

	$.subscribe('savingAlertSubscriptionComplete', function(event,data) {
		//alert('savingAlertSubscriptionComplete');
		
	});

	$.subscribe('errorSavingAlertSubscription', function(event,data) {
		//alert('errorSavingAlertSubscription');
		
	});

	$.subscribe('successSavingAlertSubscription', function(event,data) {
		//alert('successSavingAlertSubscription');
		//$('#subscribedAlertsGridId').trigger("reloadGrid");
		//$.publish('reloadSubscribedAlerts');
		$.publish('refreshAlertSubscriptionForm');
		ns.common.showStatus();
	});

	
	/**
	 * Deleting Received Alerts Confirmation (From Grid)
	 */
	$.subscribe('beforeDeletingAlertsConfirm', function(event,data) {
		//alert('beforeDeletingAlertsConfirm');
		ns.alert.beforeDeletingAlertsConfirm();		
	});


	$.subscribe('DeletingAlertsConfirmComplete', function(event,data) {
		//alert('DeletingAlertsConfirmComplete');
	});

	$.subscribe('errorDeletingAlertsConfirm', function(event,data) {
		//alert('errorDeletingAlertsConfirm');		
	});

	$.subscribe('successDeletingAlertsConfirm', function(event,data) {
		//alert('successDeletingAlertsConfirm');
		$('#DeleteAlertsConfirmDialogID').dialog('open');
	});

	
	
	/**
	 * Deleting Received Alerts Confirmation (From View)
	 */
	$.subscribe('beforeDeletingAlertConfirm', function(event,data) {
		//alert('beforeDeletingMessageConfirm');
	});

	$.subscribe('deletingAlertConfirmComplete', function(event,data) {
		//alert('deletingMessageConfirmComplete');
		
	});

	$.subscribe('errorDeletingAlertConfirm', function(event,data) {
		//alert('errorDeletingMessageConfirm');
		
	});

	$.subscribe('successDeletingAlertConfirm', function(event,data) {
		//alert('successDeletingMessageConfirm');
		$('#DeleteAlertsConfirmDialogID').dialog('open');

	});	


	/**
	 * Delete Received Alerts
	 */
	$.subscribe('beforeDeletingAlerts', function(event,data) {
		//alert('beforeDeletingAlerts');
	});

	$.subscribe('deletingAlertsComplete', function(event,data) {
		//alert('deletingAlertsComplete');
		
	});

	$.subscribe('errorDeletingAlerts', function(event,data) {
		//alert('errorDeletingAlerts');
		
	});

	$.subscribe('successDeletingAlerts', function(event,data) {
		//alert('successDeletingAlerts');
		
		$('#inboxAlertsGridId').trigger("reloadGrid");

		ns.common.showStatus();
		$.publish('closeDialog');
		$('#details2').slideUp();
		$('#summary').slideDown();
		$('#alertTabs').slideDown();

		$.publish('refreshAlertSmallPanel');
	});	

	
	/**
	 * Unsubscribe alert
	 */
	$.subscribe('beforeUnsubscribingAlert', function(event,data) {
		//alert('beforeUnsubscribingAlert');
		
	});
	
	$.subscribe('unsubscribingAlertComplete', function(event,data) {
		//alert('unsubscribingAlertComplete');
		
	});
	
	$.subscribe('errorUnsubscribingAlert', function(event,data) {
		//alert('errorUnsubscribingAlert');
		
	});
	
	$.subscribe('successUnsubscribingAlert', function(event,data) {
		//alert('successUnsubscribingAlert');
		ns.common.showStatus();
		$.publish('closeDialog');
		if(ns.alert.whichgrid === 'subscribedAlertsGridId')
			$.publish('refreshAlertSubscriptionForm');
		else
			$('#'+ns.alert.whichgrid).trigger("reloadGrid");
		
		
	});
	

	/**
	 * Loading alert configuration form for Account Balance/Transaction/Stock Portolio alerts
	 */
	/**
	 * Configuring Balance alerts
	 */	 
	// $.subscribe('beforeLoadingBalAlertConfigForm', function(event,data) {
		// var urlString = "/cb/pages/jsp/alert/alert_subscribedBalance.jsp";
		// $.ajax({   
			  // url: urlString,   
			  // success: function(data) { 
				// $('#subscribedSpecificAlertsDiv').html(data);
			  // }   
		// });
	// });
	/**
	 * Configuring Balance alerts
	 */	 
	$.subscribe('beforeLoadingBalAlertConfigForm', function(event,data) {
		$.ajax({   
			  //URL Encryption Edit By Dongning
			  url: ns.alert.beforeLoadingBalAlertConfigFormURL,   
			  success: function(data) { 
				$('#subscribedSpecificAlertsDiv').html(data);
			  }   
		});
	});

	$.subscribe('successLoadingBalAlertConfigForm', function(event,data) {
		//alert('successLoadingAlertConfigForm');
		ns.alert.whichgrid = 'subscribedBalAlertsGridId';
		$('#alertSubscriptionFormDiv').slideUp();
		$('#subscribedAlertsDiv').slideUp();
		$('#alertConfigDiv').slideDown();
		$('#subscribedSpecificAlertsDiv').slideDown();
	});
	
	/**
	 * Configuring Transaction alerts
	 */
	$.subscribe('beforeLoadingTransAlertConfigForm', function(event,data) {
		$.ajax({   
			  //URL Encryption Edit By Dongning
			  url: ns.alert.beforeLoadingTransAlertConfigFormURL,   
			  success: function(data) { 
				$('#subscribedSpecificAlertsDiv').html(data);
			  }   
		});
	});

	$.subscribe('successLoadingTransAlertConfigForm', function(event,data) {
		//alert('successLoadingAlertConfigForm');
		ns.alert.whichgrid = 'subscribedTransAlertsGridId';
		$('#alertSubscriptionFormDiv').slideUp();
		$('#subscribedAlertsDiv').slideUp();
		$('#alertConfigDiv').slideDown();
		$('#subscribedSpecificAlertsDiv').slideDown();
	});
	

	/**
	 * Configuring Stock alerts
	 */
	$.subscribe('beforeLoadingStockAlertConfigForm', function(event,data) {
		//URL Encryption Edit By Dongning
		$.ajax({   
			  url: ns.alert.beforeLoadingStockAlertConfigFormURL,   
			  success: function(data) { 
				$('#subscribedSpecificAlertsDiv').html(data);
			  }   
		});
	});

	$.subscribe('successLoadingStockAlertConfigForm', function(event,data) {
		//alert('successLoadingAlertConfigForm');
		ns.alert.whichgrid = 'subscribedStockAlertsGridId';
		$('#alertSubscriptionFormDiv').slideUp();
		$('#subscribedAlertsDiv').slideUp();
		$('#alertConfigDiv').slideDown();
		$('#subscribedSpecificAlertsDiv').slideDown();
	});

	
	/**
	 * Configuring(Saving) Account Balance/Transaction/Stock alerts
	 */
	$.subscribe('beforeConfiguringAlert', function(event,data) {
		//alert('beforeConfiguringAlert');
		
	});
	$.subscribe('configuringAlertComplete', function(event,data) {
		//alert('configuringAlertComplete');
		
	});
	$.subscribe('errorConfiguringAlert', function(event,data) {
		//alert('errorConfiguringAlert');
		
	});
	
	//$.subscribe('successConfiguringAlert', function(event,data) {
		// //alert('successConfiguringAlert');
		// ns.common.showStatus();
		
		// var pageName = "alertsbalance.jsp";
		// if (ns.alert.whichgrid === 'subscribedBalAlertsGridId') {
			// pageName = "alertsbalance.jsp";
		// } else if (ns.alert.whichgrid === 'subscribedTransAlertsGridId') {
			// pageName = "alertstransaction.jsp";
		// } else if (ns.alert.whichgrid === 'subscribedStockAlertsGridId') {
			// pageName = "alertsstock.jsp";
		// }
		
		// var urlString = "/cb/pages/jsp/alert/" + pageName;
		// $.ajax({   
			  // url: urlString,   
			  // success: function(data) { 
				// $('#'+ns.alert.whichgrid).trigger("reloadGrid");
				// $('#alertConfigDiv').html(data);
			  // }   
		// });

		
	// });
	
	
	$.subscribe('successConfiguringAlert', function(event,data) {
		ns.common.showStatus();
		//URL Encryption Edit By Dongning
		 var pageName = ns.alert.successConfiguringAlertBalanceURL;
		 if (ns.alert.whichgrid === 'subscribedBalAlertsGridId') {
			 pageName = ns.alert.successConfiguringAlertBalanceURL;
		 } else if (ns.alert.whichgrid === 'subscribedTransAlertsGridId') {
			 pageName = ns.alert.successConfiguringAlertTranURL;
		 } else if (ns.alert.whichgrid === 'subscribedStockAlertsGridId') {
			 pageName = ns.alert.successConfiguringAlertStockURL;
		 }

		$.ajax({   
			  url: pageName,   
			  success: function(data) { 
				$('#'+ns.alert.whichgrid).trigger("reloadGrid");
				$('#alertConfigDiv').html(data);
			  }   
		});

		
	});

	
	
	
	/**
	 * Cancel configuring Balance/Transaction/Stock alerts
	 */
	$.subscribe('cancelConfigAlert', function(event,data){
		ns.alert.whichgrid = 'subscribedAlertsGridId';
		$('#alertConfigDiv').slideUp();
		$('#subscribedSpecificAlertsDiv').slideUp();
		$('#alertSubscriptionFormDiv').slideDown();
		$('#subscribedAlertsDiv').slideDown();
		$.publish('refreshAlertSubscriptionForm');
	});

	
	$.subscribe('beforeShowAlertsInbox', function(event, data){
		//var urlString = "/cb/pages/jsp/alert/alert_summary.jsp";
		//URL Encryption Edit By Dongning
		$.ajax({   
			  url: ns.alert.beforeShowAlertsInboxURL,   
			  success: function(data) { 
				$('#summary').html(data);
			  }   
		});			

		ns.alert.gotoInbox();
		return false;
		
	});

	$.subscribe('filterLogs', function(event, data){
		var type = $("#FindLogMessages_Type").val() || "";
		var sDate = $("#StartDateID").val();
		var eDate = $("#EndDateID").val();
		var logFilter = {
			alertType: type,
			startDate: sDate,
			endDate: eDate
		};
		
		$("#alertLogGridId").jqGrid('setGridParam',{postData:logFilter}).trigger("reloadGrid");
	});

	
	$.subscribe('beforeShowAlertLogs', function(event, data){
		$('#summary').html('');
		var $filter = $('#quicksearchcriteria');
		$filter.show();
		ns.alert.getAlertLogsView();
		ns.alert.gotoLog();
		return false;
	});

	
	$.subscribe('successFilteringAlertLogs', function(event, data){
		//URL Encryption Edit By Dongning
		//var urlString = "/cb/pages/jsp/alert/alert_summary2.jsp";		
		$.ajax({   
			  url: ns.alert.successFilteringAlertLogsURL,   
			  success: function(data) { 
				$('#summary').html(data);
			  }   
		});
	});
	
	$.subscribe('cancelForm', function(event,data) {
	    
		$.log('About to cancel Form! ');
		if (ns.alert.lastgrid === 'alertLogGridId') {
			ns.alert.gotoLog();
		} else if (ns.alert.lastgrid === 'inboxAlertsGridId') {
			ns.alert.gotoInbox();
		}
	    
	});
	
	$.subscribe('loadAddEditForm', function(event,data) {
		$("#addEditAlertContainer").hide();
		$("#addEditAlerts").hide();
		$("#alertsMainSummaryContainer").show();
		$("#availableAlertTypesContainer").show();
	});
	
	$.subscribe('successAddEditForm', function(event,data) {
		
		//if AddEdit clicked from dashboard
		ns.common.setModuleType("AddEditAlert"); //To track last page.
		
		$('#configAlertsPortlet').portlet('show');
		$('#details').slideDown();
		$('#details2').slideUp(); //viewAlertPortlet
		$("#addEditAlertContainer").hide();
		$("#addEditAlerts").hide();
		$("#alertsMainSummaryContainer").show();
		$("#availableAlertTypesContainer").show();
		$("#dbAddEditAlerts a span.dashboardSelectedItem").removeClass('dashboardSelectedItem');
		$("#subscribeAlertsLink span").addClass("dashboardSelectedItem");
	});

	
	$.subscribe('cancelViewAlert', function(event,data) {
		ns.alert.whichgrid = 'inboxAlertsGridId';
	    $('#details2').hide();
	    $('#alertTabs').slideDown();
	    var linkId = $("#closeViewAlertContainerButton").attr("invokerlinkid");
	    $("#"+linkId).focus();
	});
	
	$.subscribe('closeAlertDialog', function(event,data) {
		$("#selectedAlertsIDs").val('');
		ns.common.closeDialog();
	});

}

ns.alert.getAlertLogsView = function(){
	//check if came from view alert logs on subscribed alerts link
	var type = $("#showAlertsLogsLink").attr("alertType");
	if(type === undefined){
		type="";
	}
	var aUrl = ns.alert.successFilteringAlertLogsURL + "&alertType="+ type; 
	$.ajax({   
	  url: aUrl ,   
	  success: function(data) { 
		$('#summary').html(data);
		$('#showAlertsLogsLink').removeAttr("alertType");
	  }   
	});
};

ns.alert.gotoLog = function(){
	ns.common.refreshDashboard('dbAlertLogsSummary');
	/*$('#showAlertsLogsLink').attr('style','display:none');
	$('#deleteInboxAlertsLink').attr('style','display:none');
	$('#showAlertsInboxLink').removeAttr('style');
	$('#subscribeAlertsLink').removeAttr('style');
	$('#cpControlBox').attr('style','display:none');
	$('#mobileControlBox').attr('style','display:none');
	$('#alerts_main_summary').hide();*/
	
	ns.alert.lastgrid = ns.alert.whichgrid; 
	ns.alert.whichgrid = 'alertLogGridId';

	
	$('#summary').slideDown();//inbox alerts
    $('#details').slideUp(); //subscription form
	$.publish("useralerts.closeAddNewCPForm");
	if(accessibility){
		$.publish("common.topics.tabifyNotes");	
		$("#appdashboard").setFocus();
	}
};


ns.alert.gotoInbox = function(){

	//$('#showAlertsInboxLink').attr('style','display:none');
	//$('#deleteInboxAlertsLink').removeAttr('style');
	$('#subscribeAlertsLink').removeAttr('style');
	$('#showAlertsLogsLink').removeAttr('style');
	
	$('#quicksearchcriteria').hide();
	$('#cpControlBox').attr('style','display:none');
	/*$('#mobileControlBox').attr('style','display:none');*/
	$('#alerts_main_summary').hide();
	

	ns.alert.lastgrid = ns.alert.whichgrid; 
	ns.alert.whichgrid = 'inboxAlertsGridId';

	ns.common.hideStatus();

	$('#summary').slideDown();//inbox alerts
    $('#details').slideUp(); //subscription form
	$.publish("useralerts.closeAddNewCPForm");
};

ns.alert.gotoSubscribe = function(){
	ns.common.refreshDashboard('dbAddEditAlerts');
	
	/*$('#deleteInboxAlertsLink').attr('style','display:none');
	$('#subscribeAlertsLink').attr('style','display:none');
	$('#showAlertsInboxLink').removeAttr('style');
	$('#showAlertsLogsLink').removeAttr('style');

	$('#quicksearchcriteria').hide();
	$('#cpControlBox').attr('style','display:none');
	$('#mobileControlBox').attr('style','display:none');
	$('#alerts_main_summary').hide();*/

	ns.alert.lastgrid = ns.alert.whichgrid; 
	ns.alert.whichgrid = 'subscribedAlertsGridId';

	ns.common.hideStatus();
	$.publish("useralerts.closeAddNewCPForm");
};

ns.alert.beforeDeletingAlertsConfirm = function(){
	var selectedInboxMsgIDs="";
	var multiSelectedInboxMsgIDs = $('#inboxAlertsGridId').jqGrid('getGridParam', 'selarrrow');
	var multiSelectedInboxMsgIDsArray=[];
	var selectedId = "";
	for(var i=0;i<multiSelectedInboxMsgIDs.length;i++){
		selectedId = $("#inboxAlertsGridId").jqGrid('getRowData',multiSelectedInboxMsgIDs[i]).alertIds;
		multiSelectedInboxMsgIDsArray.push(selectedId);
	}
	// either a single alert delete is clicked or single is selected for deletion
	if(multiSelectedInboxMsgIDsArray.length>1 && $('#multicheck').val()=="true"){
		$("#allSelectedAlertsIDs").val(multiSelectedInboxMsgIDsArray);
		selectedInboxMsgIDs = multiSelectedInboxMsgIDsArray;
		$("#selectedAlertsIDs").val('');
	}else{
		$("#allSelectedAlertsIDs").val('');
		selectedInboxMsgIDs =  multiSelectedInboxMsgIDsArray[0] || $("#selectedAlertsIDs").val();
		$("#selectedAlertsIDs").val(selectedInboxMsgIDs);
	}
	if(selectedInboxMsgIDs == undefined || selectedInboxMsgIDs == ""){
		ns.common.showStatus("Please select messages to delete!");
		return false;
	}
};

//Show the active dashboard based on the user selected summary
ns.alert.showAlertDashboard = function(alertUserActionType,alertDashboardElementId){

	if(alertUserActionType == 'SubscribedAlerts'){
		ns.common.refreshDashboard('dbAddEditAlerts');
		
		//To track last page
		if(alertDashboardElementId == "" ){ //if SubscribedAlerts summary menu clicked from top menu
			ns.common.setModuleType("SubscribedAlertsSummary");
		}else{
			ns.common.setModuleType("AddEditAlert"); //if AddEdit menu clicked from top menu
		}
	}else if(alertUserActionType == 'AlertLogs'){
		ns.common.refreshDashboard('dbAlertLogsSummary');
	}else if(alertUserActionType == 'ContactPoints'){
		ns.common.refreshDashboard('dbContactPointsSummary');
		if(alertDashboardElementId=='addNewCPLink'){
			ns.common.selectDashboardItem('addNewCPLink');
		}
	}else if(alertUserActionType == 'MobileDevices'){
		ns.common.refreshDashboard('dbMobileDevicesSummary');
		//load summary by default if dashboard item not clicked
		if(alertDashboardElementId ==''){
			ns.contactpoints.loadMobileDevicesView();
		}
	}else{//In case of normal Transfer top menu click or clicking on user shortcut
		ns.common.refreshDashboard('dbAlertsSummary');
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(alertDashboardElementId && alertDashboardElementId !='' && $('#'+alertDashboardElementId).length > 0){
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+alertDashboardElementId).trigger("click");}, 10);
	}
};


$(document).ready(function() {
	$.debug(false);
	ns.alert.setup();
});
