// Function to show Statement Agreement Dialog
ns.onlineStatements.showStatementAgreement = function() {
	$.ajax({
		url: '/cb/pages/jsp/onlinestatements/inc/online_statement_agreement.jsp',
		success: function(data){
			$('#viewStatementAgreementDialogID').html(data).dialog('open');
		}
	});
};

//Function to close and destroy dialog.
ns.onlineStatements.closeAgreementDialog = function(){
	$('#viewStatementAgreementDialogID').dialog('close');
};

// Function to call action to generate Printer Ready report.
ns.onlineStatements.getPrinterReadyPage = function() {
	$.ajax({
		url: '/cb/pages/jsp/account/GenerateOnlineReportAction_generateReportBase.action',
		data: $("#statementSearchForm").serialize(),
		type: 'post',
		success: function(data){
			$('#viewPrintReadyVersionDialogID').html(data).dialog('open');
		}
	});
};

// Fuction to show export JSP page.
ns.onlineStatements.getExportPage = function() {
	$.ajax({
		url: '/cb/pages/jsp/onlinestatements/inc/statement_export.jsp',
		data: $("#statementSearchForm").serialize(),
		type: 'post',
		success: function(data){
			$('#viewExportDialogID').html(data).dialog('open');
		}
	});
};

// Function to call action to initialize printing process.
ns.onlineStatements.onPrinterReadyClick = function(){
	$.ajax({
		url: '/cb/pages/jsp/account/NewOnlineReportAction_initializePrintReportBase.action',
		data: $("#statementSearchForm").serialize(),
		type: 'post',
		success: function(data){
			ns.onlineStatements.getPrinterReadyPage();
		}
	});
};

// Function to call action to initialize export process. 
ns.onlineStatements.onExportClick = function(){
	$.ajax({
		url: '/cb/pages/jsp/account/NewOnlineReportAction_initializeExportReportBase.action',
		type: 'post',
		data: $("#statementSearchForm").serialize(),
		success: function(data){
			ns.onlineStatements.getExportPage();
		}
	});
};

// Function to show Bank Notice dialog on click of "Bank Reserve Notice" link.
ns.onlineStatements.openBankReserveNoticeDialog = function(){
	$.ajax({
		url: '/cb/pages/jsp/onlinestatements/inc/online_statement_notice.jsp',
		success: function(data){
			$('#viewBankNoticeDialogID').html(data).dialog('open');
		}
	});
};

// Function to perform POST operation and fetch data, when Account selection is changed.
ns.onlineStatements.accountChanged = function() {
	//Make details section empty since the Account number relevant to Statement has changed
	$("#statementDetails").html("");
	
	var accountNumber = $("#accountNumber").val();
	if(accountNumber == undefined || accountNumber == ''){
		ns.onlineStatements.resetButtons();
		return;
	}
    document.forms["statementSearchForm"].elements["GetDates"].value = "true";
    
    // reset statementSearchDate selected index
    document.forms["statementSearchForm"].elements["statementSearchDate"].selectedIndex = 0;
            	
    //Submit the form by Using JQuery Ajax. Using Post operation will help us to keep most of the JSP as it is */
    var	urlString = '/cb/pages/jsp/onlinestatement/onlineStatementSearchAction.action';
	$.ajax({
		url: urlString,	
		type: 'post',
		data: $("#statementSearchForm").serialize(),
		success: function(data){
			$('#searchContents').html(data);

			$.publish("common.topics.tabifyDesktop");
			$("#accountNumber").focus();


		}
	});
};

//Function to reset button states if account is not selected.
ns.onlineStatements.resetButtons = function() {
	//Reset statementSearchDate and statementTransactionType selected index
	document.forms["statementSearchForm"].elements["statementSearchDate"].selectedIndex = 0;
	document.forms["statementSearchForm"].elements["statementTransactionType"].selectedIndex = 0;
	
	if(ns.common.isInitialized($('#statementDate'),'ui-selectmenu')){
        	$("#statementDate").selectmenu('destroy');
    }
	
	$('#statementDate').attr("disabled","disabled");
    $("#statementDate").selectmenu({width: 100});
	
    ns.onlineStatements.toggleButtons();
};

// Function to enable or disable buttons, depending on Input selection.
ns.onlineStatements.toggleButtons = function() {
	$("#viewOnlineStatement").unbind('click');
	$("#viewExportDetails").unbind('click');
	$("#viewPrinterReadyVersion").unbind('click');
	
    if (document.forms["statementSearchForm"].elements["statementSearchDate"].selectedIndex >= 1) {
        if(ns.common.isInitialized($('#transactionType'),'ui-selectmenu')){
        	$("#transactionType").selectmenu('destroy');
        }
        $('#transactionType').removeAttr('disabled');
        $("#transactionType").selectmenu({width: 130});
        
        $("#viewOnlineStatement").button({ disabled: false });
        $("#viewOnlineStatement").bind('click',ns.onlineStatements.performSearch);
        
        $("#viewExportDetails").button({ disabled: false });
        $("#viewExportDetails").bind('click',ns.onlineStatements.onExportClick);
        
    	$("#viewPrinterReadyVersion").button({ disabled: false });
    	$("#viewPrinterReadyVersion").bind('click',ns.onlineStatements.onPrinterReadyClick);
        
    } else {
    	if(ns.common.isInitialized($('#transactionType'),'ui-selectmenu')){
        	$("#transactionType").selectmenu('destroy');
        }
    	$('#transactionType').attr("disabled","disabled");
    	$("#transactionType").selectmenu({width: 130});
    	
    	$("#viewOnlineStatement").button({ disabled: true });
    	$("#viewExportDetails").button({ disabled: true });
    	$("#viewPrinterReadyVersion").button({ disabled: true });
    	
    }
};

// Function to perform Search operation
ns.onlineStatements.performSearch = function(){
	//Destroy portlet created.
	$('#onlineStatementSummaryContainer_portlet').empty();
	$('#onlineStatementSummaryContainer_portlet').remove();
	
	$("#viewExportDetails").button({ disabled: false });
	$("#viewPrinterReadyVersion").button({ disabled: false });
		
	//Submit the form by Using JQuery Ajax. Using Post operation will help us to keep most of the JSP as it is */
    var	urlString = '/cb/pages/jsp/onlinestatement/getOnlineStatementAction.action';
    
	$.ajax({
		url: urlString,	
		type: 'post',
		data: $("#statementSearchForm").serialize(),
		success: function(data){
			$("#statementDetails").html(data);
			
			$.publish("common.topics.tabifyNotes");
			if(accessibility){
				$("#onlineStatementSummaryContainer").setFocus();
			}
		}
	});	
};

/* Subscribe various events for different Struts JQuery plugin controls used in Online Statement module*/ 
$.subscribe('functionalityComplete', function(event,data) {
	ns.onlineStatements.closeAgreementDialog();
});