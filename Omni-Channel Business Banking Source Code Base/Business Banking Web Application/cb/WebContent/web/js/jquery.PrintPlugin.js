//This function is required for IE. In IE, unless and until you get focus, IFrame does not print itself.
function _printFrameForPrintingPlugin(frameName) {
	var printableSection = $('#' + frameName).contents().find(
			'div.printableSection');

	// Check if its report page by checking Header section is present or not.
	var rptHeaderSection = printableSection.find('td.tbrd_lr');
	if (rptHeaderSection.length) {
		fixReportSize(rptHeaderSection);
	}
	rptHeaderSection = null;

	// Fix grid size
	var gridsCollection = printableSection.find('div.ui-jqgrid');
	if (gridsCollection.length) {
		fixGridSize(gridsCollection);
	}
	gridsCollection = null;

	// Fix selectmenu size
	fixSelectMenuSize(printableSection);

	
	var target = document.getElementById(frameName);
	target.contentWindow.focus();
	//Firefox fix
	if(navigator.userAgent.toUpperCase().search("FIREFOX") != -1){
		target.contentWindow.print();
	} else {
	    try {
	    	//IE 10/11 fix
	    	target.contentWindow.document.execCommand('print', false, null);
	    } catch(e) {
	    	target.contentWindow.print();
	    }
	}
	
	// Have the frame remove itself in about a three minute so that
	// we don't build up too many of these frames.
	setTimeout(function() {
		$('#' + frameName).remove();
	}, (180 * 1000));
}

function makePortletHeaderPositionRelative(printableSection) {
	var portletTitle = printableSection.find('.portlet-title');
	var counter = 0;
	if (portletTitle.length) {
		for (counter = 0; counter < portletTitle.length; counter++) {
			$(portletTitle[counter]).css('position', 'relative');
		}
	}
}

function fixGridSize(printableSection) {
	var gridsCollection = printableSection.find('.ui-jqgrid');
	var counter = 0;
	var header, body, titlebar, view, pager;
	if (gridsCollection.length) {
		for (counter = 0; counter < gridsCollection.length; counter++) {
			grid = $(gridsCollection[counter]);
			view = grid.find('.ui-jqgrid-view');
			pager = grid.find('.ui-jqgrid-pager');
			header = grid.find('.ui-jqgrid-hdiv');
			body = grid.find('.ui-jqgrid-bdiv');
			titlebar = grid.find('.ui-jqgrid-titlebar');
			grid.css('width', '100%');
			$(view[0]).css('width', '100%');
			$(pager[0]).css('width', '100%');
			$(header[0]).css('width', '100%');
			$(body[0]).css('width', '100%');
			$(titlebar[0]).css('width', '100%');
		}
	}
}

/*
 * ifrm.$('.ui-selectmenu').css('width','auto');
 * ifrm.$('.ui-selectmenu-status').css('padding','0px');
 * ifrm.$('#printerReadyReportDialog').find('td.tbrd_lr').parents("table:first").find('table').css('width','98%');
 */
function makePortletHeaderPositionRelative(printableSection) {
	var portletTitle = printableSection.find('.portlet-title');
	var counter = 0;
	if (portletTitle.length) {
		for (counter = 0; counter < portletTitle.length; counter++) {
			$(portletTitle[counter]).css('position', 'relative');
		}
	}
}

function fixReportSize(rptHeader) {
	// Set width of parent table
	var mainReportTable = rptHeader.parents("table:first");
	mainReportTable.css('width', '100%');
	mainReportTable.find('table').css('width', '98%');
}

function fixSelectMenuSize(printableSection) {
	printableSection.find('a.ui-selectmenu').css({
		"width" : "auto",
		"display" : "block",
		"padding" : "0px"
	});
	printableSection.find('span.ui-selectmenu-status').css('padding', '0px');
}

// Make Grid width in percentage so it can be fit in all Printing mode.
function fixGridSize(gridsCollection) {
	var counter = 0;
	var titlebar, view, pager, grid;
	var widthArray;
	if (gridsCollection.length) {
		for (counter = 0; counter < gridsCollection.length; counter++) {
			grid = $(gridsCollection[counter]);
			view = grid.find('div.ui-jqgrid-view');
			pager = grid.find('div.ui-jqgrid-pager');
			titlebar = grid.find('div.ui-jqgrid-titlebar');
			widthArray = new Array();
			gridsCollection[counter].style.width = '98%';
			view[0].style.width = '98%';
			setGridHeaderColWidth(view[0], widthArray);
			setGridDataColWidth(view[0], widthArray);
			setGridFooterRowWidth(view[0], widthArray);

			if (pager.length)
				pager[0].style.width = '98%';

			if (titlebar.length)
				titlebar[0].style.width = '98%';

			// Remove all reference
			grid = null;
			view = null;
			pager = null;
			titlebar = null;
			widthArray = null;
		}
	}
}

// Function to set header and headerColumn width in percentage.
function setGridHeaderColWidth(view, widthArray) {
	var headerDiv = $(view).find('div.ui-jqgrid-hdiv');
	var headerTable = $(headerDiv[0]).find('table');
	var headerColumns = $(headerTable[0]).find('tr.ui-jqgrid-labels')
			.find('th');

	// Save original width and set width in percentile
	var headerWidth = parseInt(headerDiv[0].style.width);
	headerDiv[0].style.width = '100%';
	headerTable[0].style.width = '100%';

	var counter = 0;
	var percentileWidth = 0;
	var headerColWidth = 0;
	for (counter = 0; counter < headerColumns.length; counter++) {
		if (headerColumns[counter].style.display === "none")
			continue;
		headerColWidth = parseInt(headerColumns[counter].style.width);
		percentileWidth = Math.round((100 / headerWidth) * headerColWidth);
		headerColumns[counter].style.width = percentileWidth + '%';
		widthArray[counter] = percentileWidth;
	}
	headerDiv = null;
	headerTable = null;
	headerColumns = null;
}

// Function to set dataTable and dataColumn width in percentage.
function setGridDataColWidth(view, widthArray) {
	var dataDiv = $(view).find('div.ui-jqgrid-bdiv');
	var dataTable = $(dataDiv[0]).find('table');
	var dataRows = $(dataTable[0]).find('tr.jqgrow');
	dataDiv[0].style.width = '100%';
	dataTable[0].style.width = '100%';

	var i = 0, j = 0;
	var dataRowColumns;
	for (i = 0; i < dataRows.length; i++) {
		dataRowColumns = $(dataRows[i]).find('td');
		for (j = 0; j < dataRowColumns.length; j++) {
			if (dataRowColumns[j].style.display === "none")
				continue;
			dataRowColumns[j].style.width = widthArray[j] + '%';
		}
	}
	dataDiv = null;
	dataTable = null;
	dataRows = null;
}

// Function to set dataTable and dataColumn width in percentage.
function setGridFooterRowWidth(view, widthArray) {
	var footerDiv = $(view).find('div.ui-jqgrid-sdiv');
	if (!footerDiv.length)
		return;

	var footerTable = $(footerDiv[0]).find('table.ui-jqgrid-ftable');
	var footerRows = $(footerTable[0]).find('tr.footrow');
	footerDiv[0].style.width = '100%';
	footerTable[0].style.width = '100%';

	var i = 0, j = 0;
	var footerRowColumns;
	for (i = 0; i < footerRows.length; i++) {
		footerRowColumns = $(footerRows[i]).find('td');
		for (j = 0; j < footerRowColumns.length; j++) {
			if (footerRowColumns[j].style.display === "none")
				continue;
			footerRowColumns[j].style.width = widthArray[j] + '%';
		}
	}
	footerDiv = null;
	footerTable = null;
	footerRows = null;
}

// Unload handler to release memory
function _printFrameUnloadHandler(frameName) {
	$('#' + frameName).remove();
}

$.extend({
	print : function() {
		// Check if print Css is already added or not. If added then remove it.
		// We will add it again so that we will be sure, that Css is appended at
		// the end of head tag.
		if ($('#printWholePageCss').length) {
			$('#printWholePageCss').remove();
		}
		// Add PrintCss so that we can use window.print();
		var headID = document.getElementsByTagName("head")[0];
		var cssNode = document.createElement('link');
		cssNode.type = 'text/css';
		cssNode.rel = 'stylesheet';
		cssNode.href = '/cb/web/css/PrintWholePage.css';
		cssNode.media = 'print';
		cssNode.id = 'printWholePageCss';
		headID.appendChild(cssNode);

		// We need some time in between- adding a Css and Printing window, esp.
		// Firefox version > 4
		ns.common.showLoading();
		setTimeout(function() {
			ns.common.hideLoading();
			window.print();
		}, 2000);

		// Add function to remove added css.
		/*
		 * setTimeout(function() { $('#outer-center').bind('mousemove keypress',
		 * function () { setTimeout(function(){ if($('#'+linkId).length){
		 * $('#'+linkId).remove(); }
		 * 
		 * //Remove bound events $('#outer-center').unbind('mousemove
		 * keypress'); },10000); }, 1000); });
		 */
	}
});


// This function will create an Iframe and attach it to the body
function createIframe() {
	// Create a random name for the print frame.
	var strFrameName = ("printer_" + (new Date()).getTime());

	// Check if any Iframe with Such name previously exist or not
	var prevPrintIframe = $("[id^='printer_']");
	if (prevPrintIframe.length) {
		prevPrintIframe.remove();
	}

	// Create an iFrame with the new name.
	var iFrameEle = $("<iframe name='" + strFrameName + "' id='" + strFrameName	+ "' src='javascript:false'></iframe>");

	// Hide the frame (sort of) and attach to the body.
	iFrameEle.css("width", "1px").css("height", "1px").css("position",
			"absolute").css("left", "-9999px").appendTo($("body:first"));
	
	// Get a FRAME reference to the new frame.
	return window.frames[strFrameName];
}

// Create a jquery plugin that prints the given div with a scroll.
jQuery.fn.print = function() {	
	if (!this.size()) {
		return;
	}

	//Chrome issue Save scrollTop 
	var offset = document.body.scrollTop || 0;
	
	// Create Iframe
	var objFrame = createIframe();

	// Get a reference to the DOM in the new frame.
	var objDoc = objFrame.document;

	// Write the HTML for the document. In this, we will
	// write out the HTML of the current element.
	objDoc.open();
	objDoc.write("<!DOCTYPE html>");
	objDoc.write("<html>");
	objDoc.write("<head>");
	objDoc.write("<title>");
	objDoc.write(document.title);
	objDoc.write("</title>");
	// Grab all the CSS and copy to maintain look and feel.
	$("link").each(
			function() {
				objDoc.write("<link type='text/css' rel='stylesheet' href='"
						+ $(this).attr("href") + "' media='print'/>");
			});
	objDoc.write("<link type='text/css' rel='stylesheet' href='/cb/web/css/PrintPlugin.css' media='print'/>");
	objDoc.write("</head>");
	objDoc.write("<body>");
	objDoc.write("<table align='center' style='width:100%;'>");
	jQuery.each(this,function(index, divElement) {
		// Check if element is visible or not. If visible then
		// only print.	
		if ($(divElement).is(':visible')) {
			//code to remove script tags from the page content
			var pageContent = $(divElement).find("script,noscript,style").remove().end().html();
			objDoc.write("<tr><td align='center' style='width:100%;'>");
			objDoc.write("<div id='printableSection' class='ui-widget-content printableSection' align='center'>");
			objDoc.write(pageContent);
			objDoc.write("</div>");
			objDoc.write("</td></tr>");
		}
	});
	objDoc.write("</table>");
	objDoc.write("</body>");
	objDoc.write("</html>");
	objDoc.close();
	
	var timeOutInterval = 10;
	//Reset scrollTop Chrome issue
	//This seems to be Layout and Chrome issue
	if(navigator.userAgent.toUpperCase().search("CHROME") != -1){
		var objectLayoutCenter = document.getElementById("outer-north");
		objectLayoutCenter.scrollIntoView(true);
		timeOutInterval = 3000;
	}
	// Fix width and height of jquery controls inside of iframe
	ns.common.showLoading();
	setTimeout(function() {
		_printFrameForPrintingPlugin(objFrame.name);
		ns.common.hideLoading();
	}, timeOutInterval);
	
		
};
