ns.cash.setup = function(){
	ns.cash.gotoPositivePay();
};

ns.cash.gotoPositivePay = function(){
	ns.common.refreshDashboard('dbPositivePaySummary');
};

$.subscribe('buildPPayFormBefore', function(event,data) {
	removeValidationErrors();
});

$.subscribe('buildPPayFormSuccess', function(event,data) {
	ns.cash.gotoStep(2);
});

$.subscribe('buildPPayFormError', function(event,data) {
});

$.subscribe('buildPPayFormComplete', function(event,data) {
});

$.subscribe('clearValidationErrorsTopic', function(event,data) {
	$('.errorLabel').html('').removeClass('errorLabel');
	$('#formerrors').remove();
});

$(document).ready(function(){
	$.debug(false);
	ns.cash.setup();
});

//Show the active dashboard based on the user selected summary
ns.cash.showDashboard = function(userActionType,dashboardElementId){
	if(userActionType == 'positivePayUpload'){
		ns.common.refreshDashboard('dbPPayFileUploadSummary');
	}else{
		 if(dashboardElementId == 'ppaybuildLink'){
			$("#ppaySummaryBtn").find("span").removeClass("dashboardSelectedItem");
			$("#ppaybuildLink").find("span").addClass("dashboardSelectedItem");
		 }
		 else{
			/*ns.cash.refreshPPayGridURL = "/cb/pages/jsp/positivepay/positivepay_exception_approval_grid.jsp";
			ns.cash.refreshPPayGrid(ns.cash.refreshPPayGridURL);
			ns.common.refreshDashboard('dbPositivePaySummary');*/
		}
	}
	
	//Simulate dashboard item click event if any additional action is required
	if(dashboardElementId && dashboardElementId !='' && $('#'+dashboardElementId).length > 0){
		//Timeout is required to display spinning wheel for the dashboard button click
		setTimeout(function(){$("#"+dashboardElementId).trigger("click");}, 10);
	}
};

showPPayGridSummary = function(gridTabId){
	//Hide all Grid Summary containers
	$(".gridSummaryContainerCls" ).each(function(i) {
        $(this).hide();
    });
	
	var gridSummaryId  = gridTabId + "SummaryGrid";

	//Show the user selected Grid Summary container
    $('#'+gridSummaryId).show();
    $('#'+gridSummaryId).removeClass('hidden');
    var gridId = $('#'+gridSummaryId).attr('gridId'); //retrive the underlying Grid Id from the 'gridId' attribute of Grid Summary container
    //By default to avoid grid loading, datatype is local, make it json and reload grid.
    ns.common.setFirstGridPage('#'+gridId);
    $('#'+gridId).jqGrid('setGridParam',{datatype:'json'}).trigger("reloadGrid");
    
	//Find user selected grid tab
	var userSelctorGridTab = gridTabId;
	//Hide user selected grid tab from dropdown menu.
	//$("#"+userSelctorGridTab).hide();
	
	//Get title of user selected grid tab
	var selectedGridTabTitle = $("#"+userSelctorGridTab).html().trim();
	if(selectedGridTabTitle != undefined && selectedGridTabTitle!= ''){
		$("#selectedGridTab").html(selectedGridTabTitle); //Set selected grid tab title
	}
	if(gridTabId == 'approvalPPay') {
		$('#ppay_workflowID .wizardStatusContainerCls').hide();
		$('#ppay_workflowID .searchPanelToggleAreaCls').hide()
		if($('#allTypesOfAccount').is(':visible')) {
			$('#allTypesOfAccount').slideToggle();
		}
	} else {		
		$('#ppay_workflowID .wizardStatusContainerCls').show();
		$('#ppay_workflowID .searchPanelToggleAreaCls').show()
		ns.cash.gotoStep(1);
	}
};