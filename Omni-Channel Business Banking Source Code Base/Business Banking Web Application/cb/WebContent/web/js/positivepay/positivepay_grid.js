	$(document).ready(function(){
		$('#customMappingDetails').hide();
		$('#mappingDiv').hide();
	});

ns.cash.resizeWidthOfGridCashSummary = function(){
		if( $("#summary").length > 0 ){
			ns.common.resizeWidthOfGrids();
		}
	};

ns.cash.refreshPPayGrid = function(urlString){
		$.ajax({
		  url: urlString,
		  success: function(data) {
			 $("#firstDiv").html(data);
			 $("#secondDiv").hide();
			 ns.cash.gotoStep(1);

			 $.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#notes").setFocus();
			}
		  }
		});
		ns.cash.gotoStep(1);
	};

	//need to send selected value of account to server
ns.cash.refreshPPayListGrid = function(urlString, selectedValue){
		$.ajax({
		  url: urlString,
		  data: {"selectedValue":selectedValue},
		  success: function(data) {
			 $("#allTypesOfAccount").html(data);
		  }
		});
	};


ns.cash.refreshDashboardForm = function(urlString){
		$.ajax({
		  url: urlString,
		  success: function(data) {
			 $("#appdashboard").html(data);
		  }
		});
	};

	//formatter the date
ns.cash.dateFormatter = function(cellvalue, options, rowObject){
		var date = "";
		if(cellvalue == null || cellvalue==""){
			date = "---";
		} else {
			date = cellvalue.substr(0,10);
		}
		return date;
	};


	//ppay exception grid complete event.
	$.subscribe('PPayExceptionEvent', function(event,data) {
	    // refresh the approval small panel in left bar
	    $.publish('refreshApprovalSmallPanel');

		ns.cash.formatPPayAccountColumn("#ppayExceptionGridID");
		//$("#ppayExceptionGridID_pager_center").hide(); //no need to hide pager as page size is 1000, QTS : 694005
		ns.cash.resizeWidthOfGridCashSummary();
		$('#firstDiv').slideDown();
		//QTS:709292.remove the hand pointer
		$('div[id="jqgh_map.pay"]').removeClass('ui-jqgrid-sortable');
		$('div[id="jqgh_map.return"]').removeClass('ui-jqgrid-sortable');

		$.publish("common.topics.tabifyNotes");
	});

	//ppay exception grid complete event.
	$.subscribe('PPayApprovExceptionEvent', function(event,data) {
	    // refresh the approval small panel in left bar
	    $.publish('refreshApprovalSmallPanel');

		ns.cash.formatPPayAccountColumn("#ppayApprovalExceptionGridID");

		ns.cash.resizeWidthOfGridCashSummary();
		$('#firstDiv').slideDown();

		$('div[id="jqgh_map.pay"]').removeClass('ui-jqgrid-sortable');
		$('div[id="jqgh_map.return"]').removeClass('ui-jqgrid-sortable');

		$.publish("common.topics.tabifyNotes");
	});

	$.subscribe('PPayExceptionConfirmEvent', function(event,data) {
		ns.cash.formatPPayAccountColumn("#ppayExceptionConfirmGridID");
		//$("#ppayExceptionConfirmGridID_pager_center").hide(); //no need to hide pager as page size is 1000, QTS : 694005

		//Need this code here, somehow exceptionGridSubmitSuccessTopic topic doesn't get called.
		ns.cash.gotoStep(2);
		$('#pPayList').hide();

		ns.cash.resizeWidthOfGridCashSummary();
		$('#secondDiv').slideDown();
		$.publish("common.topics.tabifyNotes");
	});

	$.subscribe('PPayExceptionCompleteEvent', function(event,data) {
		ns.cash.formatPPayAccountColumn("#ppayExceptionCompleteGridID");
		//$("#ppayExceptionConfirmGridID_pager_center").hide(); //no need to hide pager as page size is 1000, QTS : 694005

		//Need this code here, somehow exceptionGridSubmitSuccessTopic topic doesn't get called.
		ns.cash.gotoStep(3);
		$('#pPayList').hide();

		ns.cash.resizeWidthOfGridCashSummary();
		$('#secondDiv').hide();
		$.publish("common.topics.tabifyNotes");
	});

	//ppay exception grid complete event.
	$.subscribe('PPayExceptionForAccountEvent', function(event,data) {
		ns.cash.formatPPayAccountColumn("#ppayExceptionForAccountGridID");
		//$("#ppayExceptionForAccountGridID_pager_center").hide(); //no need to hide pager as page size is 1000, QTS : 694005
		ns.cash.resizeWidthOfGridCashSummary();
		$('#firstDiv').slideDown();
		//QTS:709292.remove the hand pointer
		$('div[id="jqgh_map.pay"]').removeClass('ui-jqgrid-sortable');
		$('div[id="jqgh_map.return"]').removeClass('ui-jqgrid-sortable');
		$.publish("common.topics.tabifyNotes");
	});
	//ppay summary grid complete event.
	$.subscribe('PPaySummaryEvent', function(event,data) {
		ns.cash.formatPPaySummaryAccountColumn("#ppaySummaryGridID");
		ns.cash.resizeWidthOfGridCashSummary();
	});


	$.subscribe('exceptionGridSubmitSuccessTopic', function(event,data) {
		ns.cash.gotoStep(2);
		$('#pPayList').hide();
	});

	$.subscribe('exceptionGridSubmitErrorTopic', function(event,data) {
		//console.log('exceptionGridSubmitErrorTopic');
	});

	$.subscribe('exceptionGridSubmitCompleteTopic', function(event,data) {
		//console.log('exceptionGridSubmitCompleteTopic');
	});

	$.subscribe('exceptionGridSubmitVerifySuccessTopic', function(event,data) {
		ns.cash.gotoStep(3);
		$('#pPayList').hide();
	});

	$.subscribe('exceptionGridSubmitVerifyErrorTopic', function(event,data) {
		//TODO
	});

	$.subscribe('exceptionGridSubmitVerifyCompleteTopic', function(event,data) {
		//TODO
	});

	$.subscribe('cancelForm', function(event,data) {
		//TODO
	});


	$.subscribe('exceptionGridSubmitConfirmComplete', function(event,data) {
		ns.common.showStatus();

		ns.common.setFirstGridPage('#ppaySummaryGridID');//set grid page param to 1 to reload grid from first
		$('#ppaySummaryGridID').trigger("reloadGrid");

		ns.common.setFirstGridPage('#ppayExceptionGridID');//set grid page param to 1 to reload grid from first
		$('#ppayExceptionGridID').trigger("reloadGrid");

		ns.common.resizeWidthOfGrids();

		ns.cash.gotoStep(1);
		$('#pPayList').show();
	});

	//formatter for ppay account column
	ns.cash.formatPPayAccountColumn = function(gridID){
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			for(var i=0;i<ids.length;i++){
				var rowId = ids[i];
				//Construct "To Account Nickname" column.
				var _toAccountRoutingNumber = $(gridID).jqGrid('getCell', rowId, 'map.routingNumber');
				var _toAccountID = $(gridID).jqGrid('getCell', rowId, 'checkRecord.accountID');
				var _toAccountDisplayText = $(gridID).jqGrid('getCell', rowId, 'map.accountDisplayText');
				var _toAccountNickName = $(gridID).jqGrid('getCell', rowId, 'map.nickName');
				var _toAccountCurrencyType = $(gridID).jqGrid('getCell', rowId, 'map.currencyType');
				var _toAccountName = "";

				if(_toAccountDisplayText != undefined && _toAccountDisplayText !=''){
					_toAccountName = _toAccountDisplayText;
				}
				else{
					_toAccountName = _toAccountRoutingNumber;
					if( _toAccountID.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountID;
					}
					if( _toAccountNickName.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountNickName;
					}
					if( _toAccountCurrencyType.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountCurrencyType;
					}
				}

				$(gridID).jqGrid('setCell', rowId, 'account', _toAccountName);
			}
		};

	ns.cash.formatPPaySummaryAccountColumn = function(gridID){
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			for(var i=0;i<ids.length;i++){
				var rowId = ids[i];
				//Construct "To Account Nickname" column.
				var _toAccountRoutingNumber = $(gridID).jqGrid('getCell', rowId, 'PPayAccount.routingNumber');
				var _toAccountDisplayText = $(gridID).jqGrid('getCell', rowId, 'PPayAccount.accountDisplayText');
				var _toAccountNickName = $(gridID).jqGrid('getCell', rowId, 'PPayAccount.nickName');
				var _toAccountCurrencyType = $(gridID).jqGrid('getCell', rowId, 'PPayAccount.currencyType');
				var linkVar = $(gridID).jqGrid('getCell', rowId, 'map.ViewURL');

				var _toAccountName = "";

				if(_toAccountDisplayText != undefined && _toAccountDisplayText !=''){
					_toAccountName = _toAccountDisplayText;
				}
				else{
					_toAccountName = _toAccountRoutingNumber;

					if( _toAccountNickName.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountNickName;
					}
					if( _toAccountCurrencyType.length > 0 ){
						_toAccountName = _toAccountName + " - " + _toAccountCurrencyType;
					}
				}

				var link = "<a href=\"#\" onClick=\"ns.cash.refreshExceptionGridForAccount('"+linkVar+"')\" >" + _toAccountName + "</a>";

				$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_account']:eq("+i + ")").html(link);
			}
		};

	ns.cash.searchImageFormatter = function(cellvalue, options, rowObject){
		var checkNumber = "<a href=\"#\" onClick=\"ns.cash.openImageWindow('"+rowObject["map"].ViewURL+"')\" >" + cellvalue + "</a>";
		return ' ' + checkNumber;
	};

ns.cash.openImageWindow = function(urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#searchImageDialog').html(data).dialog('open');
			}
		});
	};

 ns.cash.zoomImage = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data) {
			//ns.common.closeDialog();
			//$("#searchImageDialog").html(data).dialog('open');
			$('#imgHolder').html(data);
			}
		});
	};

ns.cash.rotationFunction = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data) {
			//ns.common.closeDialog();
			//$("#searchImageDialog").html(data).dialog('open');
			$('#imgHolder').html(data);

			}
		});
	};
	
ns.cash.ppayZoomImage = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data) {
			//ns.common.closeDialog();
			//$("#searchImageDialog").html(data).dialog('open');
			$('#imgGridHolder').html(data);
			}
		});
	};

ns.cash.ppayRotationFunction = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data) {
			//ns.common.closeDialog();
			//$("#searchImageDialog").html(data).dialog('open');
			$('#imgGridHolder').html(data);

			}
		});
	};

ns.cash.RadioButtonPayFormatter = function(cellvalue, options, rowObject){
    var Pay = "";

    var defaultdecision = rowObject["map"].defaultdecision;
    var currentDecision = rowObject["map"].currentDecision;
    var finalDecision = defaultdecision;
    if(currentDecision!=null && currentDecision!=''){
    	finalDecision = currentDecision;
    }

    if(finalDecision == "Pay"){
		Pay = "<input type=\"radio\" class=\"payclass\" checked value=\"Pay\" name=\"decision" +cellvalue+ "\" >";
    } else
    {
        Pay = "<input type=\"radio\" class=\"payclass\" value=\"Pay\" name=\"decision" +cellvalue+ "\" >";
    }

	return ' ' + Pay;
};
ns.cash.RadioButtonReturnFormatter = function(cellvalue, options, rowObject) {
    var Return = "";

    var defaultdecision = rowObject["map"].defaultdecision;
    var currentDecision = rowObject["map"].currentDecision;

    var finalDecision = defaultdecision;
    if(currentDecision!=null && currentDecision!=''){
    	finalDecision = currentDecision;
    }

    if(finalDecision == "Return"){
		Return = "<input type=\"radio\" class=\"returnclass\" checked value=\"Return\" name=\"decision" +cellvalue+ "\" >";
    } else
    {
        Return = "<input type=\"radio\" class=\"returnclass\" value=\"Return\" name=\"decision" +cellvalue+ "\" >";
    }

	return ' ' + Return;
};

ns.cash.RadioButtonHoldFormatter = function(cellvalue, options, rowObject) {
	 var hold = "";

    var defaultdecision = rowObject["map"].defaultdecision;
    var currentDecision = rowObject["map"].currentDecision;

    var finalDecision = defaultdecision;
    if(currentDecision!=null && currentDecision!=''){
    	finalDecision = currentDecision;
    }

    if(finalDecision == "Hold"){
		hold = "<input type=\"radio\" class=\"holdclass\" checked value=\"Hold\" name=\"decision" +cellvalue+ "\" >";
    } else
    {
        hold = "<input type=\"radio\" class=\"holdclass\" value=\"Hold\" name=\"decision" +cellvalue+ "\" >";
    }

	return ' ' + hold;
};

ns.cash.RadioButtonPayFormatterConfirm = function(cellvalue, options, rowObject) {

		var Pay = "";

		if(rowObject["decision"] == "Pay"){
			Pay = "<input type=\"radio\" checked value=\"Pay\" name=\"decision" +cellvalue+ "\" disabled>";
		}else{
			Pay = "<input type=\"radio\" value=\"Pay\" name=\"decision" +cellvalue+ "\" disabled>";
		}

		return ' ' + Pay;
	};
ns.cash.RadioButtonReturnFormatterConfirm = function(cellvalue, options, rowObject) {

		var Return = "";

		if(rowObject["decision"] == "Return"){
			Return = "<input type=\"radio\" checked value=\"Return\" name=\"decision" +cellvalue+ "\" disabled>";
		}else{
			Return = "<input type=\"radio\" value=\"Return\" name=\"decision" +cellvalue+ "\" disabled>";
		}
		return ' ' + Return;
	};


ns.cash.refreshExceptionGridForAccount = function(urlString){
		$.ajax({
		  url: urlString,
		  success: function(data) {
			 $("#firstDiv").html(data);
			 $("#secondDiv").hide();
			 ns.cash.gotoStep(1);
		  }
		});
	};


	$.subscribe('deleteMappingComplete', function(event,data) {
		ns.common.closeDialog("#deleteMappingDialogID");
		$('#custommappingGridID').trigger("reloadGrid");
		ns.common.showStatus();

	});

	$.subscribe('mappingSuccess', function(event,data) {
		$('#pPaysummary').hide();
		$('#pPayList').hide();
		$('#mappingDiv').show();
		$('#customMappingDetails').hide();
	});
	$.subscribe('addMappingSuccessCash', function(event,data) {
		$('#pPaysummary').hide();
		$('#mappingDiv').hide();
		$('#pPayList').hide();
		$('#customMappingDetails').show();
	});

	$.subscribe('buildPPayFormBefore', function(event,data) {
		removeValidationErrors();
	});

	$.subscribe('buildPPayVerifySuccess', function(event,data) {
		ns.cash.gotoStep(3);
		ns.common.showStatus($(data).find("#ppayMessage").html());
	});

	$.subscribe('buildPPayVerifyError', function(event,data) {
	});

	$.subscribe('buildPPayVerifyComplete', function(event,data) {
	});

	$.subscribe('customMappingBefore', function(event,data) {
		removeValidationErrors();
	});

	$.subscribe('uploadCheckingRecordSuccess', function(event,data) {
		//$('#pPayList').show();

		var heading = $('#uploadInputTab').html();
        var $title = $('#inputTab a');
        $title.attr('title',heading);
        $title = $('#inputTab a span');
        $title.html(heading);

        heading = $('#uploadVerifyTab').html();
        $title = $('#verifyTab a');
        $title.attr('title',heading);
        $title = $('#verifyTab a span');
        $title.html(heading);

//        $('#inputDiv').html("");            // before we load, clear previous contents
//        $('#verifyTab').html("");            // before we load, clear previous contents
        $('#verifyTab').show();


        //set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
        heading = $('#PageHeading').html();
        $title = $('#pPaysummary .portlet-title');
        $title.html(heading);
        ns.common.updatePortletTitle("ppay_workflowID",heading,false);

		$('#pPayList').hide();
		$('#pPaysummary').show();
		$('#mappingDiv').hide();
		$('#customMappingDetails').hide();
		$("#ppay_workflowID").find(".wizardStatusContainerCls").hide();

		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#PPayWizardTabs").setFocusOnTab();
		}
		$('#approvalPPaySummaryGrid').hide();
		$('#pendingPPaySummaryGrid').show();
	});

	$.subscribe('onClickUploadCheckingRecord', function(event,data) {
		//load import dialog if not loaded
		var config = ns.common.dialogs["fileImport"];
		config.autoOpen = "false";
		ns.common.openDialog("fileImport");

		var configImportResults = ns.common.dialogs["fileImportResults"];
		configImportResults.autoOpen = "false";
		ns.common.openDialog("fileImportResults");
	});

	$.subscribe('enableManualFileDiv', function(event,data) {
		$('#approvalPPaySummaryGrid').hide();
		$('#pendingPPaySummaryGrid').show();
	});


	$.subscribe('buildManualFileSuccess', function(event,data) {
		$('#pPayList').hide();
		//$('#ppaybuildLink').hide();

		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			//$("#PPayWizardTabs").setFocusOnTab();
			$("#PPayWizardTabs").setFocus();
		}
		$("#ppaySummaryBtn").find("span").removeClass("dashboardSelectedItem");
		$("#ppaybuildLink").find("span").addClass("dashboardSelectedItem");
	});


	$.subscribe('mappingListDoneSuccessCash', function(event,data) {
		$('#appdashboard').show();
		//$('#pPayList').show();
		$('#pPayList').hide();
		//$('#pPaysummary').hide();
		$('#pPaysummary').show();
		$('#mappingDiv').hide();
		$('#customMappingDetails').hide();

		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#PPayWizardTabs").setFocusOnTab();
		}
	});


ns.cash.gotoStep = function(stepnum){

		ns.common.gotoWizardStep('ppay_workflowID',stepnum);

		var tabindex = stepnum - 1;
		$('#PPayWizardTabs').tabs( 'enable', tabindex );
		$('#PPayWizardTabs').tabs( 'option','active', tabindex );
		$('#PPayWizardTabs').tabs( 'disable', (tabindex+1)%3 );
		$('#PPayWizardTabs').tabs( 'disable', (tabindex+2)%3 );
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#PPayWizardTabs").setFocusOnTab();
		}
	};

	$.subscribe('addMappingFormComplete', function(event,data) {
		ns.cash.gotoStep(2);
	});

	$.subscribe('addMappingFormComplete2', function(event,data) {
		$('#pPaysummary').hide();
		$('#appdashboard').show();
		$('#mappingDiv').show();
		//$('#custommappingGridID').trigger("reloadGrid");

	});

	$.subscribe('reloadPositivePayPortletGrid', function(event,data) {
		//retrive dynamic grid of positive pay portlet
		var positivePayPortletGridId = $("#positivePayPortletDataDiv .ui-jqgrid").prop('id').substring(5);
		$('#'+positivePayPortletGridId).trigger("reloadGrid"); //reload grid
	});

	$.subscribe('FileUploadFormComplete', function(event,data) {
		ns.common.closeDialog("#uploadFileDialogID");
		ns.common.showStatus();
	});

	$.subscribe('onUploadCancelButton', function(event,data) {
		ns.shortcut.goToMenu('cashMgmt_ppay');
		/*
		ns.cash.refreshDashboardForm(ns.cash.originalDashboardFormURL);
		ns.cash.refreshPPayListGrid(ns.cash.refreshPPayListGridURL);

		// UPLOAD might change the names of the tabs, so change them back.
        var heading = $('#normalInputTab').html();
        var $title = $('#inputTab a');
        $title.attr('title',heading);
        $title = $('#inputTab a span');
        $title.html(heading);

        heading = $('#normalVerifyTab').html();
        $title = $('#verifyTab a');
        $title.attr('title',heading);
        $title = $('#verifyTab a span');
        $title.html(heading);

        //set portlet title
        heading = $('#normalSummaryTab').html();
        $title = $('#pPaysummary .portlet-title');
        //$title.html(heading);
        ns.common.updatePortletTitle("ppay_workflowID",heading,false);

		$('#pPayList').show();
		$('#pPaysummary').show();
		ns.cash.refreshPPayGrid(ns.cash.refreshPPayGridURL);
		*/
	});

	$.subscribe('pPayExpSubmitForm', function(event,data) {
		$.ajaxSetup({
			async : false
		});
		var selectedValue = $('#ppaySummaryGridID').val();

		var urlString ='/cb/pages/jsp/positivepay/getPositivePaySummariesAction.action';
		ns.cash.refreshPPayListGrid(urlString,selectedValue);
		$('#ppaySummaryGridID').val(selectedValue);

		if($('#ppaySummaryGridID').val() == null) {
			selectedValue = '';
		}

		if(ns.common.isInitialized($('#ppaySummaryGridID'),'ui-selectmenu')){
			$('#ppaySummaryGridID').selectmenu("destroy");
		}

		if(selectedValue!=''){
			var accountIdIndex = selectedValue.indexOf("A");
			var bankIndex = selectedValue.indexOf("B");
			var accountId = selectedValue.substring(accountIdIndex+1,bankIndex);
			var bankId = selectedValue.substring(bankIndex+1);
			var url ='/cb/pages/jsp/positivepay/getPositivePayIssuesForAccountAction_init.action?accountID='+accountId+"&bankID="+bankId;
			ns.cash.refreshExceptionGridForAccount(url);
		}
		else{
			$('#ppaySummaryGridID').trigger("change");
		}

		$("#ppaySummaryGridID").selectmenu({width: 350});

		$.ajaxSetup({
			async : true
		});
	});

	$.subscribe("changeActionNameBackTopic",function(e,d){
		document.emailFormName.action = "/cb/pages/jsp/positivepay/EmailImagesAction_execute.action";
	});

	$.subscribe("changeActionNameTopic",function(e,d){
		document.emailFormName.action = "/cb/pages/jsp/positivepay/EmailImagesAction_cancelEmail.action";
		ns.common.closeDialog();
	});

	$.subscribe('emailImageCompleteTopic', function(event,data) {
		ns.common.closeDialog("searchImageDialog");
	});

	$.subscribe('pendingApprovalGridCompleteEvents', function(event,data) {
	    // refresh the approval small panel in left bar
		if($('#isConsumerUserLoggedIn').length != '0'){
			$.publish('refreshApprovalSmallPanel');
		}
		ns.common.resizeWidthOfGrids();
		ns.wire.addApprovalsRowsForPendingApproval("#ppayApprovalExceptionGridID");
	});

ns.cash.formatApproversInfo = function(cellvalue, options, rowObject) {
		var approverNameInfo="";
		var approverNameOrUserName="";
		var approverGroup="";
		var approverInfos=rowObject.approverInfos;
			if(approverInfos!=undefined)
			{
				for(var i=0;i<approverInfos.length;i++){
				var approverInfo = approverInfos[i];
				if(i!=0)
				{
				approverNameInfo="," + approverNameInfo
				}

				var approverName = approverInfo.approverName;
				var approverIsGroup=approverInfo.approverIsGroup;

				if( approverIsGroup){
					approverGroup = "(" + js_group + ")";
				} else {
					approverGroup = "";
				}
				approverNameOrUserName = approverName + approverGroup;
				approverNameInfo=approverNameOrUserName+approverNameInfo;
				}
			}
			return approverNameInfo;
};

	//Add approval information in the pending approval summary gird.
	ns.wire.addApprovalsRowsForPendingApproval = function(gridID){

			var approvalsOrReject = "";
			var approvalerNameOrRejectReason = "";
			var rejectedOrSubmitedBy = "";
			var approverNameOrUserName = "";
			var approverGroup = "";
			var submittedForTitle = js_submitted_for;

			var ids = jQuery(gridID).jqGrid('getDataIDs');
			for(var i=0;i<ids.length;i++){

				var rowId = ids[i];
				var approverName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverName'));
				var userName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'userName'));
				var approverIsGroup = $(gridID).jqGrid('getCell', rowId, 'approverIsGroup');
				var approverNameInfo = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverInfos'));
				var status = $(gridID).jqGrid('getCell', rowId, 'status');
				var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
				var submittedFor = $(gridID).jqGrid('getCell', rowId, 'resultOfApproval');
				var transType = $(gridID).jqGrid('getCell', rowId, 'map.transType');
				var curRow = $("#" + rowId, $(gridID));



				if( status == 101 ){
					approvalsOrReject = js_reject_reason;
					rejectedOrSubmitedBy = js_rejected_by;
					if(rejectReason==''){
						approvalerNameOrRejectReason = "No Reason Provided";
					} else{
						approvalerNameOrRejectReason = rejectReason;
					}


					if(approverNameInfo != ""){
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}

						className = 'ui-widget-content jqgrow ui-row-ltr';
						curRow.after(ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameInfo,approvalsOrReject,approvalerNameOrRejectReason,className,10));
					} else {
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}
						className = 'ui-widget-content jqgrow ui-row-ltr';

						curRow.after(ns.common.addRejectReasonRow('','',approvalsOrReject,approvalerNameOrRejectReason,className,10));
					}
				//if status is not 101(WireDefines.WIRE_STATUS_APPROVAL_REJECTED), means that the transfer's status is not rejected.
				} else {
					approvalsOrReject = js_approval_waiting_on;
					approvalerNameOrRejectReason = approverNameInfo;
					rejectedOrSubmitedBy = js_submitted_by;
					approverNameOrUserName = userName;

					if(approverNameInfo != ""){
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}

						className = 'ui-widget-content jqgrow ui-row-ltr';
						curRow.after(ns.common.addSubmittedForRow(approvalsOrReject,approvalerNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,10));
					}
				}
			}
	};
	$.subscribe('backToInput', function(event, data) {
		ns.cash.gotoStep(1);
	});

