var wireRelease = {};
var wireReleaseAll = false;
var wireRejectAll = false;
function setReleaseWire(obj){
	var releaseIdVal = obj.value;
	var id = releaseIdVal.split('-')[0];
	var val = releaseIdVal.split('-')[1];
	if(id != "" && val != "") {
			wireRelease[id]=val;
	}
}


$.subscribe('pendingWiresGridCompleteEvents', function(event,data) {
	    // refresh the approval small panel in left bar
		if($('#isConsumerUserLoggedIn').length != '0'){
			$.publish('refreshApprovalSmallPanel');
		}

	    ns.common.resizeWidthOfGrids();
	});

	$.subscribe('pendingApprovalWiresGridCompleteEvents', function(event,data) {
	    // refresh the approval small panel in left bar
		if($('#isConsumerUserLoggedIn').length != '0'){
			$.publish('refreshApprovalSmallPanel');
		}
		ns.common.resizeWidthOfGrids();
		ns.wire.addApprovalsRowsForPendingApprovalWireTransfers("#pendingApprovalWiresGridID");
	});

	ns.wire.formatApproversInfo = function(cellvalue, options, rowObject) {
		var approverNameInfo="";
		var approverNameOrUserName="";
		var approverGroup="";
		var approverInfos=rowObject.approverInfos;
			if(approverInfos!=undefined)
			{
				for(var i=0;i<approverInfos.length;i++){
				var approverInfo = approverInfos[i];
				if(i!=0)
				{
				approverNameInfo="," + approverNameInfo
				}

				var approverName = approverInfo.approverName;
				var approverIsGroup=approverInfo.approverIsGroup;

				if( approverIsGroup){
					approverGroup = "(" + js_group + ")";
				}
				approverNameOrUserName = approverName + approverGroup;
				approverNameInfo=approverNameOrUserName+approverNameInfo;
				}
			}
			return approverNameInfo;
};


	//Add approval information in the pending approval summary gird.
	ns.wire.addApprovalsRowsForPendingApprovalWireTransfers = function(gridID){
			var approvalsOrReject = "";
			var approvalerNameOrRejectReason = "";
			var rejectedOrSubmitedBy = "";
			var approverNameOrUserName = "";
			var approverGroup = "";
			var submittedForTitle = js_submitted_for;

			var ids = jQuery(gridID).jqGrid('getDataIDs');
			for(var i=0;i<ids.length;i++){

				var rowId = ids[i];
				var approverName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverName'));
				var approverNameInfo = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverInfos'));
				var userName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'userName'));
				var approverIsGroup = $(gridID).jqGrid('getCell', rowId, 'approverIsGroup');
				var status = $(gridID).jqGrid('getCell', rowId, 'status');
				var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
				var submittedFor = $(gridID).jqGrid('getCell', rowId, 'resultOfApproval');
				var transType = $(gridID).jqGrid('getCell', rowId, 'map.transType');
				var curRow = $("#" + rowId, $(gridID));



				//if status is 101(WireDefines.WIRE_STATUS_APPROVAL_REJECTED), means that the transfer is rejected.
				if( status == 101 ){
					approvalsOrReject = js_reject_reason;
					rejectedOrSubmitedBy = js_rejected_by;
					if(rejectReason==''){
						approvalerNameOrRejectReason = "No Reason Provided";
					} else{
						approvalerNameOrRejectReason = rejectReason;
					}
					approverNameOrUserName = approverNameInfo;

					if(approverNameInfo != ""){
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}

						className = 'ui-widget-content jqgrow ui-row-ltr';
						curRow.after(ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameOrUserName,approvalsOrReject,approvalerNameOrRejectReason,className,10));
					} else {
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}
						className = 'ui-widget-content jqgrow ui-row-ltr';

						curRow.after(ns.common.addRejectReasonRow('','',approvalsOrReject,approvalerNameOrRejectReason,className,10));
					}
				//if status is not 101(WireDefines.WIRE_STATUS_APPROVAL_REJECTED), means that the transfer's status is not rejected.
				} else {
					approvalsOrReject = js_approval_waiting_on;
					approvalerNameOrRejectReason = approverNameInfo;
					rejectedOrSubmitedBy = js_submitted_by;
					approverNameOrUserName = userName;

					if(approverNameInfo != ""){
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}

						className = 'ui-widget-content jqgrow ui-row-ltr';
						curRow.after(ns.common.addSubmittedForRow(approvalsOrReject,approvalerNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,10));
					} else {
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}

						className = 'ui-widget-content jqgrow ui-row-ltr';
						if( transType != "Batch" ){
							curRow.after('<tr class=\"' + className + ' skipRowActionItemRender\"><td colspan="10" align="center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
									'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'+js_submission_approval_failed+'&nbsp&nbsp&nbsp  </b>' + '&nbsp</td></tr>');
						}
					}
				}
			}
	};

	$.subscribe('completedWiresGridCompleteEvents', function(event,data) {
		ns.common.resizeWidthOfGrids();
	});

	$.subscribe('wireTemplatesUserGridCompleteEvents', function(event,data) {
		ns.wire.addApprovalsRowsForTemplate("#wireTemplatesUserGridID");
		ns.common.resizeWidthOfGrids();
	});
	$.subscribe('wireTemplatesBusinessGridCompleteEvents', function(event,data) {
		ns.wire.addApprovalsRowsForTemplate("#wireTemplatesBusinessGridID");
		ns.common.resizeWidthOfGrids();
	});
	$.subscribe('wireTemplatesBankGridCompleteEvents', function(event,data) {
		ns.wire.addApprovalsRowsForTemplate("#wireTemplatesBankGridID");
		ns.common.resizeWidthOfGrids();
	});

	ns.wire.viewWireTransfersDetails = function(urlString,isRecModel){
	if(isRecModel) {
		 var btns = {
			'Print':function() { ns.common.printPopup(); },
			'Done':function() { ns.common.closeDialog(); },
		    'View Original Transfer': function () {
				viewModel();
			}
		};
	} else {
		var btns = {
		 'Print':function() { ns.common.printPopup(); },
			'Done':function() { ns.common.closeDialog(); }
		};
	}
	 $('#viewWiresDetailsDialogID').dialog('option', 'buttons', btns);
		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewWiresDetailsDialogID').html(data).dialog('open');
			}
		});
	};

	ns.wire.editWireTransfers = function(urlString,isBatch){
		$.ajax({
			url: urlString,
			success: function(data){
					$('#inputDiv').html(data);
					ns.wire.openDetailsDiv();

					$.publish("common.topics.tabifyDesktop");
					if(accessibility){
						$("#details").setInitialFocus();
					}
					hideDashboard();
					ns.common.selectDashboardItem("newSingleWiresTransferID");
					if(isBatch == 'true'){
						ns.common.selectDashboardItem("newBatchWiresTransferID");
					}
			}
		});
	};

	ns.wire.deleteWireTransfers = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteWiresDialogID').html(data).dialog('open');
			}
		});
	};

	ns.wire.skipWireTransfers = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#skipWiresDialogID').html(data).dialog('open');
			}
		});
	};


	$.subscribe('deleteWireCompleteTopics', function(event,data) {
		ns.common.closeDialog('#deleteWiresDialogID');
	});

	$.subscribe('deleteWireSuccessTopics', function(event,data) {
/*		var selectedTab = $("#wiresGridTabs").tabs("option","active")

		if(selectedTab == 0){
			$('#pendingApprovalWiresGridID').trigger("reloadGrid");
		}
		if(selectedTab == 1){
			$('#pendingWiresGridID').trigger("reloadGrid");
		}
		if(selectedTab == 2){
			$('#completedWiresGridID').trigger("reloadGrid");
		}*/

		ns.common.setFirstGridPage('#pendingApprovalWiresGridID');
    	$('#pendingApprovalWiresGridID').trigger("reloadGrid");
    	ns.common.setFirstGridPage('#pendingWiresGridID');
    	$('#pendingWiresGridID').trigger("reloadGrid");
    	ns.common.setFirstGridPage('#completedWiresGridID');
    	$('#completedWiresGridID').trigger("reloadGrid");

		ns.common.showStatus();
	});

	$.subscribe('deleteWireErrorTopics', function(event,data) {
		ns.common.showError();
	});

	ns.wire.inquiryWireTransfersDetails = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#InquiryWiresDialogID').html(data).dialog('open');
			}
		});
	};

	$.subscribe('quickSearchWiresComplete', function(event,data) {
/*		$(".gridSummaryContainerCls").each(function(i){
			if($(this).is(":visible")){
		        var gridId = $(this).attr('gridId'); //retrive the underlying Grid Id from the 'gridId' attribute of Grid Summary container
		        ns.common.setFirstGridPage('#'+gridId);
		        $('#'+gridId).trigger("reloadGrid");
			}
	    });
*/		$.publish("reloadSelectedGridTab");
		$('#details').hide();
		$('#wiresGridTabs').portlet('expand');
	});

	$.subscribe('releseWireSearchComplete', function(event,data) {
		ns.common.setFirstGridPage('#releaseWiresGridID');//set grid page param to 1 to reload grid from first
		$('#releaseWiresGridID').trigger("reloadGrid");
		$('#details').hide();
	});

	$.subscribe('quickSearchWiresTemplateComplete', function(event,data) {
		$(".gridSummaryContainerCls").each(function(i){
			if($(this).is(":visible")){
		        var gridId = $(this).attr('gridId'); //retrive the underlying Grid Id from the 'gridId' attribute of Grid Summary container
		        ns.common.setFirstGridPage('#'+gridId);
		        $('#'+gridId).trigger("reloadGrid");
			}
	    });

		$('#details').hide();
		$('#wireTemplateGridTabs').portlet('expand');
	});

	//Formatter of pending wire summary gird.
	ns.wire.formatPendingWiresActionLinks = function(cellvalue, options, rowObject) {
	    //common parameters for all the icon buttons
		//cellvalue is the ID of the wire transfer.
	    var collectionName = "PendingWireTransfers";
		var pendApproval = "";
		return ns.wire.formatWiresActionLinks( collectionName, pendApproval, cellvalue, options, rowObject );
	};

	//Formatter of pending approvals wire transfers summary gird.
	ns.wire.formatPendingApprovalWiresActionLinks = function(cellvalue, options, rowObject) {
	    //common parameters for all the icon buttons
		//cellvalue is the ID of the wire transfer.
		var wireRootUrl = "/cb/pages/jsp/wires/";
	    var collectionName = "PendingApprovalWireTransfers";
		var pendApproval = "true";

		return ns.wire.formatWiresActionLinks( collectionName, pendApproval, cellvalue, options, rowObject );
	};

	//Formatter of completed wire transfers summary grid
	ns.wire.formatCompletedWiresActionLinks = function(cellvalue, options, rowObject) {
	    //common parameters for all the icon buttons
		//cellvalue is the ID of the wire transfer.
		var wireRootUrl = "/cb/pages/jsp/wires/";
	    var collectionName = "CompletedWireTransfers";
		var pendApproval = "";

		return ns.wire.formatWiresActionLinks( collectionName, pendApproval, cellvalue, options, rowObject );
	};

	//Formatter of wires summary link on the grid.
	ns.wire.formatWiresActionLinks = function( collectionName, pendApproval, cellvalue, options, rowObject){
		//common parameters for all the icon buttons
		//cellvalue is the ID of the wire transfer.
		var wireRootUrl = "/cb/pages/jsp/wires/";
		var wireTask = "ModifyWireTransfer";
		var isBatch =false;
		//Initialize page's variables
		// define parameters for old edit JSP page
		var viewPage = "wiretransferview.jsp";
	    var editPage = "wiretransfernew.jsp";
	    var delPage  = "wiretransferdelete.jsp";
	    if( rowObject["map"].destination == "Host" ){
	    	editPage = "wirehost.jsp";
	    	viewPage = "wirehostview.jsp";
	    	delPage = "wirehostdelete.jsp";
	    	wireTask = "ModifyHostWire";
	    } else if( rowObject["map"].destination == "Book" ){
	    	editPage = "wirebook.jsp";
	    	viewPage = "wirebookview.jsp";
	    	delPage = "wirebookdelete.jsp";
	    } else if( rowObject["map"].destination == "Drawdown" ){
	    	editPage = "wiredrawdown.jsp";
	    	viewPage = "wiredrawdownview.jsp";
	    	delPage = "wiredrawdowndelete.jsp";
	    } else if( rowObject["map"].destination == "FED Wire" ){
	    	editPage = "wirefed.jsp";
	    	viewPage = "wirefedview.jsp";
	    	delPage = "wirefeddelete.jsp";
	    }

		//--------------------------------  implement view icon button  --------------------------------//
		// define parameters for old view JSP page
		var date = rowObject["date"];
        // compose url for old view JSP page
		var urlViewString = wireRootUrl + viewPage + "?ID=" + cellvalue + "&collectionName="+ collectionName + "&Date=" + date;

        // compose url for old edit JSP page
		var urlEditString = wireRootUrl + editPage + "?ID=" + cellvalue + "&wireTask=" + wireTask +"&collectionName="+ collectionName + "&pendApproval=" + pendApproval;

		//compose url for old del JSP page
		var urlDelString = wireRootUrl + delPage + "?ID=" + cellvalue + "&collectionName="+ collectionName + "&pendApproval=" + pendApproval;

		// url='inquiretransaction.jsp?CollectionName=${collectionName}&FundsID=${wireTransfer.ID}&Subject=Wires%20Inquiry'/>
	    var inquiryPage = "inquiretransaction.jsp";
        // compose url for old view JSP page
		      var urlInquiryString = wireRootUrl + inquiryPage + "?CollectionName="+ collectionName + "&FundsID=" + cellvalue + "&Subject=Wires Inquiry";


	    if( rowObject["transType"] == "BATCH" ){

	    	if( rowObject["destination"] != "INTERNATIONAL" ){
	    		viewPage = "wirebatchview.jsp";
	    		delPage = "wirebatchdelete.jsp";
	    		editPage = "wirebatchedit.jsp";
	    	}
	    	if( rowObject["destination"] == "INTERNATIONAL" ){
	    		viewPage = "wirebatchintview.jsp";
	    		delPage = "wirebatchintdelete.jsp";
	    		editPage = "wirebatchedit.jsp";
	    	}
	    	isBatch = true;
	    	urlViewString = wireRootUrl + viewPage + "?ID=" + cellvalue + "&collectionName="+ collectionName;
	    	urlDelString  = wireRootUrl + delPage + "?ID=" + cellvalue + "&collectionName="+ collectionName;
	    	urlEditString = wireRootUrl + editPage + "?ID=" + cellvalue + "&collectionName="+ collectionName;
	    }
		var transType = rowObject["transType"];
		var isRecModel = false;
		if(transType == "RECURRING" && rowObject["recModel"] == false) {
			isRecModel = true;
		}
		// define view icon button
	    if(rowObject['isReleaseWire']){
	    	var view = "<span align=\"center\"><a  style='width:100px' class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-selectmenu' href=\"#\" onClick=\"ns.wire.viewWireTransfersDetails('"+ rowObject.map.ViewURL +"',"+isRecModel+");\">" +js_view+ "</a></span>";
		}else{
	    	var view = "<a title='" +js_view+ "' href='#' onClick='ns.wire.viewWireTransfersDetails(\""+rowObject.map.ViewURL+"\","+isRecModel+");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		}

		//--------------------------------  implement edit icon button  --------------------------------//
		// define edit icon button
		var edit;
		if(rowObject["canEdit"] == "false" && rowObject["status"]!="101") {
			edit = "";
		} else if (rowObject["map"].mixedStatusCannotModify == "true"){
			edit = "";
		} else {
			edit = "<a title='" +js_edit+ "' href='#' onClick='ns.wire.editWireTransfers(\""+rowObject.map.EditURL+"\", \""+ isBatch +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	    }

		var skip;
		if((rowObject["canSkip"] == "false" || rowObject["recModel"] == true) && rowObject["status"]!="101") {
			skip = "";
		} else {
			skip = "<a title='" +js_skip+ "' href='#' onClick='ns.wire.skipWireTransfers(\""+rowObject.map.SkipURL+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-skipRecurring'></span></a>";
		}
		//--------------------------------  implement del icon button  --------------------------------//
		// define del icon button
		var del;
		if(rowObject["canDelete"] == "false"&& rowObject["status"]!="101") {
			del  = "";
		}
		else{
			del  = "<a title='" +js_delete+ "' href='#' onClick='ns.wire.deleteWireTransfers(\""+rowObject.map.DeleteURL+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		}

		//if resultOfApproval="Deletion", and status didnot equal "101", hide deletion and edit icons
		if(rowObject["resultOfApproval"] == "Deletion"&& rowObject["status"]!="101")
		{
			del  = "";
	        edit = "";
	        skip = "";
		}
		if(rowObject["resultOfApproval"] == "Skip")
		{
			skip = "";
		}

		//--------------------------------  implement inquiry icon button  --------------------------------//
		// define view icon button
	    var inquiry = "<a title='" +js_inquiry+ "' href='#' onClick='ns.wire.inquiryWireTransfersDetails(\""+rowObject.map.InqueryURL+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";
		if(rowObject.map.canInquire == "false") {
			inquiry ="";
		}

	    if(collectionName == "WiresRelease")return ' ' + view;
	    else return ' ' + view + ' ' + edit + ' '  + del+ ' '  + skip + ' ' + inquiry;
	};

	//Formatter wire templates summary gird.
	ns.wire.formatWireTemplatesActionLinks = function(cellvalue, options, rowObject) {
		var wireRootUrl="/cb/pages/jsp/wires/";
		var recurringID="";
		var templatePrefix = "";
		var collectionName = rowObject["map"].collectionName;
		if(collectionName == "WireTemplatesUser"){
			templatePrefix="USER_";
		}else if(collectionName == "WireTemplatesBusiness"){
			templatePrefix="BUSI_";
		}else if(collectionName == "WireTemplatesBank"){
			templatePrefix="BANK_";
		}
		if(typeof rowObject["map"].recurringID!="undefined"){
			recurringID=rowObject["map"].recurringID;
		}
		var urlLoadString = wireRootUrl + rowObject["map"].wireEditPage + "?wireTask=AddWireTransfer&LoadFromTransferTemplate=" + templatePrefix + rowObject["templateID"] + "&userTemplate=true";
		var urlCloneString = wireRootUrl + rowObject["map"].wireEditPage + "?wireTask=AddWireTransfer&templateTask=add&LoadFromTransferTemplate=" + templatePrefix + rowObject["templateID"];
		var urlViewString = wireRootUrl + rowObject["map"].wireViewPage + "?ID=" + cellvalue + "&recID=" + recurringID + "&collectionName=" + collectionName + "&wireInBatch=true";
		var urlEditString = wireRootUrl + rowObject["map"].wireEditPage + "?ID=" + cellvalue + "&recID=&wireTask=ModifyWireTransfer&templateTask=edit&collectionName=" + collectionName
				+ "&wireInBatch=true";
		var urlDelString = wireRootUrl + rowObject["map"].wireDeletePage + "?ID=" + cellvalue + "&recID=&deleteTemplate=true&collectionName=" + collectionName + "&wireInBatch=true";
		var load = "<a class='' title='" +js_load+ "' href='#' onClick='ns.wire.loadWireTemplate(\"" + rowObject.map.LoadURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-upload'></span></a>";
		var clone = "<a class='' title='" +js_clone+ "' href='#' onClick='ns.wire.cloneWireTemplate(\"" + rowObject.map.CloneURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-clone'></span></a>";
		var view = "<a class='' title='" +js_view+ "' href='#' onClick='ns.wire.viewWireTemplate(\"" + rowObject.map.ViewURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.wire.editWireTemplate(\"" + rowObject.map.EditURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.wire.deleteWireTemplate(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

		if(rowObject["map"].allowEdit=="false"){
			edit ="";
			del ="";
			clone="";
		}else{
			if(rowObject["canEdit"] == "false") {
				edit ="";
	        	clone="";
		}
			if(rowObject["canDelete"] == "false") {
				del ="";
			}
		}
		if(rowObject["canLoad"] == "false") {
				load = "";
			}
		//if resultOfApproval="Deletion", and status didnot equal "101", hide deletion and edit icons
		if(rowObject["resultOfApproval"] == "Deletion"&& rowObject["status"]!="101")
		{
			edit ="";
			del ="";
			clone="";
		}
		if( rowObject["map"].containPendingPayee == "true" ){
			edit ="";
			del ="";
			clone="";
            load = "";
		}
		return ' ' + load +' ' + clone + ' ' + view + ' ' + edit + ' '  + del;
	}

	ns.wire.formatWireAmount = function(cellvalue, options, rowObject)
	{
		//  WireDefines.WIRE_TYPE_BATCH
		if (rowObject.map.transType == "Batch") {
			return rowObject.transaction.totalOrigAmount;
		} else {
			return rowObject.amountValue.currencyStringNoSymbol;
		}
	};

	ns.wire.addApprovalsRowsForTemplate  =	function (gridID){
		var approvalsOrReject = "";
		var approverNameOrRejectReason = "";
		var rejectedOrSubmitedBy = "";
		var approverNameOrUserName = "";
		var approverGroup = "";
        var submittedForTitle = js_submitted_for;

		var ids = jQuery(gridID).jqGrid('getDataIDs');
        var num = 0;
        for(var i=0;i<ids.length;i++){
            var rowId = ids[i];
			var approverName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverName'));
			var userName = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'userName'));
			var approverIsGroup = $(gridID).jqGrid('getCell', rowId, 'approverIsGroup');
			var approverNameInfo = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverInfos'));
			var status = $(gridID).jqGrid('getCell', rowId, 'status');
			var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
			var submittedFor = $(gridID).jqGrid('getCell', rowId, 'resultOfApproval');
			var curRow = $("#" + rowId, $(gridID));


            if(submittedFor != ""){
				// if status is 101 (WireDefines.WIRE_STATUS_APPROVAL_REJECTED), the wire template is rejected.
				if( status == 101 ){
					approvalsOrReject = js_reject_reason;
					rejectedOrSubmitedBy = js_rejected_by;
					if(rejectReason==''){
						approverNameOrRejectReason = "No Reason Provided";
					} else{
						approverNameOrRejectReason = rejectReason;
					}
					approverNameOrUserName = approverNameInfo;

					if(approverNameInfo != ""){
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}

						className = 'ui-widget-content jqgrow ui-row-ltr';

						curRow.after(ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameOrUserName,approvalsOrReject,approverNameOrRejectReason,className,10));
					} else {
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}
						className = 'ui-widget-content jqgrow ui-row-ltr';
						curRow.after(ns.common.addRejectReasonRow('','',approvalsOrReject,approverNameOrRejectReason,className,10));
					}
				// if status is not 101 (WireDefines.WIRE_STATUS_APPROVAL_REJECTED), the wire template was not rejected
				} else {
					approvalsOrReject = js_approval_waiting_on;
					approverNameOrRejectReason = approverNameInfo;
					rejectedOrSubmitedBy = js_submitted_by;
					approverNameOrUserName = userName;

					if(approverName != ""){
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}

						className = 'ui-widget-content jqgrow ui-row-ltr';
						curRow.after(ns.common.addSubmittedForRow(approvalsOrReject,approverNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,10));

					} else {
						var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}

						className = 'ui-widget-content jqgrow ui-row-ltr';
						curRow.after('<tr class=\"' + className + '\"><td colspan="10" align="center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
								'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'+js_submission_approval_failed+'&nbsp&nbsp&nbsp  </b>' + '&nbsp</td></tr>');
					}
				}
			}
        }
	};

	ns.wire.formatWirePage = function(){
		$('#details').slideUp();
		$('#quicksearchcriteria').hide();
		$('#wiresGridTabs').portlet('fold');
		$('#wireTemplateGridTabs').portlet('fold');
		$('#beneficiariesGridTabs').portlet('fold');
		$('#daBeneficiariesGridTabs').portlet('fold');
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

		ns.wire.gotoStep(1);
		$('#details').slideDown();
	};

	ns.wire.cloneWireTemplate = function(urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$("#inputDiv").html(data);
				ns.wire.formatWirePage();
				hideDashboard();
			}
		});
	};

	ns.wire.loadWireTemplate  = function(urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$("#inputDiv").html(data);
				ns.wire.formatWirePage();
				hideDashboard();
			}
		});
	};

	ns.wire.viewWireTemplate = function(urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewWireTempateDialogID').html(data).dialog('open');
			}
		});
	};

	ns.wire.deleteWireTemplate = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteWireTemplateDialogID').html(data).dialog('open');
			}
		});
	};

	ns.wire.editWireTemplate = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$("#inputDiv").html(data);
				ns.wire.formatWirePage();

				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#details").setInitialFocus();
				}
				hideDashboard();
				ns.common.selectDashboardItem("newSingleWiresTemplateID");
			}
		});
	};
	$.subscribe('deleteWireTemplateCompleteTopics', function(event,data) {
		ns.common.closeDialog('#deleteWireTemplateDialogID');
		var statusMessage = $('#resultmessage').html();
		ns.common.showStatus(statusMessage);
	});

	$.subscribe('deleteWireTemplateSuccessTopics', function(event,data) {
		//var selectedTab = $("#wireTemplateGridTabs").tabs("option","active")
		/*var collectionName = $("#collectionName").val();

		if(collectionName=="WireTemplatesUser"){
			$('#wireTemplatesUserGridID').trigger("reloadGrid");
		}else if(collectionName=="WireTemplatesBank"){
			$('#wireTemplatesBankGridID').trigger("reloadGrid");
		}else if(collectionName=="WireTemplatesBusiness"){
			$('#wireTemplatesBusinessGridID').trigger("reloadGrid");
		}*/

		/*if(selectedTab == 0){
			$('#wireTemplatesUserGridID').trigger("reloadGrid");
		}
		if(selectedTab == 1){
			$('#wireTemplatesBusinessGridID').trigger("reloadGrid");
		}
		if(selectedTab == 2){
			$('#wireTemplatesBankGridID').trigger("reloadGrid");
		}*/

		ns.common.setFirstGridPage('#wireTemplatesUserGridID');
    	ns.common.setFirstGridPage('#wireTemplatesBusinessGridID');
    	ns.common.setFirstGridPage('#wireTemplatesBankGridID');

		$('#wireTemplatesUserGridID').trigger("reloadGrid");
		$('#wireTemplatesBusinessGridID').trigger("reloadGrid");
		$('#wireTemplatesBankGridID').trigger("reloadGrid");

		ns.common.showStatus();
	});

	$.subscribe('deleteWiresTransfersCompleteTopics', function(event,data) {
		ns.common.closeDialog('#deleteWiresDialogID');
	});

	$.subscribe('skipWiresTransfersCompleteTopics', function(event,data) {
		ns.common.closeDialog('#skipWiresDialogID');
	});

	$.subscribe('deleteWiresTransfersSuccessTopics', function(event,data) {
		/*	var collectionName = $("#collectionName").val();
		}*/
		ns.common.showSummaryOnSuccess('summary','done');
		ns.common.showStatus();
	});

	//complete topics of deleting host wire.
	$.subscribe('deleteHostWireCompleteTopics', function(event,data) {
		ns.common.closeDialog('#deleteWiresDialogID');
	});

	//success topics of after deleting host wire, used to reload summary grid.
	$.subscribe('deleteHostWireSuccessTopics', function(event,data) {
		var collectionName = $("#collectionName").val();

		if(collectionName=="WireTemplatesUser"){
			ns.common.setFirstGridPage('#wireTemplatesUserGridID');
			$('#wireTemplatesUserGridID').trigger("reloadGrid");
		}else if(collectionName=="WireTemplatesBank"){
			ns.common.setFirstGridPage('#wireTemplatesBankGridID');
			$('#wireTemplatesBankGridID').trigger("reloadGrid");
		}else if(collectionName=="WireTemplatesBusiness"){
			ns.common.setFirstGridPage('#wireTemplatesBusinessGridID');
			$('#wireTemplatesBusinessGridID').trigger("reloadGrid");
		}
		ns.common.showStatus();
	});


	//The method is used by wire_summary.jsp and templates_summary.jsp to get transfers and templates , to improve performance.
	ns.wire.getWiresSummaryDatas = function( urlString, divID ){
		$.get(urlString, function(data) {
			$(divID).html(data);
		});
	};

	ns.wire.customWireNameColumnForTemplate = function( cellvalue, options, rowObject ){
		var _wireNameDisplayText = ns.common.HTMLEncode(rowObject["wireName"]);
		var _fromAccountNickName = ns.common.HTMLEncode(rowObject["nickName"]);

		if( _fromAccountNickName != undefined && _fromAccountNickName != "" ){
			_wireNameDisplayText = _wireNameDisplayText + "(" + _fromAccountNickName + ")";
		}

		return 	_wireNameDisplayText;
	};

	ns.wire.formatWireBeneficiariesActionLinks = function(cellvalue, options, rowObject) {
		var wireRootUrl="/cb/pages/jsp/wires/";
		var urlViewString = wireRootUrl+rowObject["map"].wirePayeeViewPage+"?ID="+cellvalue;
		var urlEditString = wireRootUrl+rowObject["map"].wirePayeeEditPage+"?ID="+cellvalue+"&Initialize=true&isBeneficiary=true&payeeTask=EditWireTransferPayee&daItemId=";
		var urlDelString = wireRootUrl+rowObject["map"].wirePayeeDeletePage+"?ID="+cellvalue;

		var view = "<a class='' title='View Info' href='#' onClick='ns.wire.viewWireBeneficiary(\""+rowObject.map.ViewURL+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var edit = "<a class='' title='Edit' href='#' onClick='ns.wire.editWireBeneficiary(\""+rowObject.map.EditURL+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a class='' title='Delete' href='#' onClick='ns.wire.deleteWireBeneficiary(\""+rowObject.map.DeleteURL+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

		if(rowObject["canEdit"] == "false") {
			edit ="";
		}
		if(rowObject["canDelete"] == "false") {
			del ="";
		}

		return  ' ' + view + ' ' + edit + ' '  + del + ' ';
	};

	$.subscribe('wireBeneficiariesGridCompleteEvents', function(event,data) {
		var gridID = 'wireBeneficiariesGridID';
		var ids = jQuery('#' + gridID).jqGrid('getDataIDs');
		if (ids.length > 0) {
				ns.common.resizeWidthOfGrids();
				$('#wireBeneficiariesGridTabs').show();
		}
	});

	ns.wire.customNameColumnForBeneficiaries = function(cellvalue, options, rowObject) {
		var _nameDisplayText = ns.common.HTMLEncode(rowObject["payeeName"]);
		var _nickName = ns.common.HTMLEncode(rowObject["nickName"]);
		if( _nickName != undefined && _nickName != ""){
			_nameDisplayText = _nameDisplayText + "(" + _nickName + ")";
		}
		return _nameDisplayText;
	};

	ns.wire.customAddressColumnForBeneficiaries = function(cellvalue, options, rowObject){
		var _addressDisplayText = ns.common.HTMLEncode(rowObject["city"]);
		var _state = rowObject["state"];
		if( _state != undefined && _state != "" ){
			_addressDisplayText = _addressDisplayText + "," + _state ;
		}
		return _addressDisplayText;
	};

	ns.wire.viewWireBeneficiary = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewWireBeneficiariesDialogID').html(data).dialog('open');
			}
		});
	};

	ns.wire.deleteWireBeneficiary = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteWireBeneficiariesDADialogID').html(data).dialog('open');
			}
		});
	};

	ns.wire.editWireBeneficiary = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				 $("#inputDiv").html(data);
				 ns.wire.formatWirePage();

				 $.publish("common.topics.tabifyDesktop");
				 if(accessibility){
					 $("#details").setInitialFocus();
				 }
				 hideDashboard();
				 ns.common.selectDashboardItem("createBeneficiaryLink");
				 $("#summary").hide();
			}
		});
	};

	$.subscribe('deleteWireBeneficiaryCompleteTopics', function(event,data) {
		ns.common.closeDialog();
	});

	$.subscribe('deleteWireBeneficiarySuccessTopics', function(event,data) {

		ns.common.setFirstGridPage('#daWireBeneficiariesGridID');
		ns.common.setFirstGridPage('#wireBeneficiariesGridID');
		$('#daWireBeneficiariesGridID').trigger("reloadGrid");
		$('#wireBeneficiariesGridID').trigger("reloadGrid");
		ns.common.showStatus();
	});

	$.subscribe('sendInquiryWiresTransferFormSuccess', function(event,data) {
		ns.common.closeDialog("#InquiryWiresDialogID");
		//Display the result message on the status bar.
		ns.common.showStatus();
        $.publish('refreshMessageSmallPanel');
	});

	$.subscribe('releaseWiresGridCompleteEvents', function(event,data) {
		//ns.wire.addSelectMenuOnReleaseWiresGrid();
		ns.common.resizeWidthOfGrids();

		$.publish("common.topics.tabifyNotes");
		 if(accessibility){
			 $("#wiresReleaseDiv").setFocus();
		 }

		 ns.wire.releaseWireActionEvent();

		 $('#releaseWiresGridID_pager_left').find("td[title='Reload Grid']").click(function() {
			 wireRelease = {};
			 wireReleaseAll = false;
			 wireRejectAll = false;
			});
	});


		ns.wire.releaseWireActionEvent = function(){

		$('#releaseWiresGridID').find('select').each(function(){
			var name = $(this).attr('id');
			var wireId = name.replace('select','');
			wireId = wireId.replace('ID','');

			var notRelease = $(this).find('option')[1].disabled;

			if(!notRelease && (wireReleaseAll  || wireRejectAll)){
				if(wireRelease[wireId] != null) {
					var selectedVal = wireRelease[wireId];
					 $(this).val(wireId+'-'+selectedVal);
				} else if(wireReleaseAll) {
					$(this).val(wireId+'-18');
				} else if (wireRejectAll){
					$(this).val(wireId+'-19');
				}
			} else 	if(!notRelease && wireRelease[wireId] != null) {
				var selectedVal = wireRelease[wireId];
				 $(this).val(wireId+'-'+selectedVal);
			}

		});

	};

	ns.wire.addSelectMenuOnReleaseWiresGrid = function(){
		//Get all Select HTML elements
		var htmlSelectElements = $("#releaseWiresGridID").find('select');

		// Iterate over these elements make them SelectMenu1
		var counter = 0;
		for(;counter < htmlSelectElements.length; counter++){
			$(htmlSelectElements[counter]).selectmenu({width:'8em'});
		}
	};

	//Formatter of pending wire summary gird.
	ns.wire.formatReleaseWiresActionLinks = function(cellvalue, options, rowObject) {
		//common parameters for all the icon buttons
		// wrap options into dropdownList
		var ID = rowObject["ID"];
        var canRelease = rowObject.map.canRelease;
        var disabled = "";
        if (canRelease == "false") {
            disabled = "disabled";
        }
        // WireDefines.WIRE_STATUS_RELEASE_PENDING = 21, WireDefines.WIRE_STATUS_RELEASED = 18, WireDefines.WIRE_STATUS_RELEASE_REJECTED = 19
     //   var options = "<option value=21>" + js_appr_hold + "</option><option value=18 " + disabled + ">" + js_appr_release + "</option><option value=19 " + disabled + ">" + js_appr_rejected + "</option>";
		//var strDropdownList = "<select id=\"select" + ID + "ID\" name=\"select" + ID + "\" >" + options + "</select>";

		var options = "<option value="+ ID +"-21>" + js_appr_hold + "</option><option value="+ ID +"-18 " + disabled + ">" + js_appr_release + "</option><option value="+ ID +"-19 " + disabled + ">" + js_appr_rejected + "</option>";
		var strDropdownList = "<select id=\"select" + ID + "ID\" name=\"wireId\"  class=\"wireReleaseSelectMenu\"  onChange=\"setReleaseWire\(this)\"\>" + options + "</select>";


	    var collectionName = "WiresRelease";
		var pendApproval = "";
		rowObject['isReleaseWire']=true;
		var view = ns.wire.formatWiresActionLinks( collectionName, pendApproval, cellvalue, options, rowObject );

		return  '<span title="">' + strDropdownList + " " + view + "</span>";
	};

	$.subscribe('releaseWiresConfirmGridCompleteEvents', function(event,data) {
		ns.common.resizeWidthOfGrids();

		$.publish("common.topics.tabifyNotes");
		 if(accessibility){
			 $("#wiresReleaseDiv").setFocus();
		 }
	});

	$.subscribe('submitWiresReleaseOnClickTopics', function(event,data) {
		$('#releaseWiresGridID').find('select').each(function(){
			var name = $(this).attr('id');
			var selectedIndex = this.selectedIndex;
			var text = $(this).next('a').find(".ui-selectmenu-status").html();
			$('#releaseWiresGridID').find('select').each(function(){
				var innerName = $(this).attr('id');
				if(name == innerName){
					this.selectedIndex=selectedIndex;
					$(this).next('a').find(".ui-selectmenu-status").html(text);
				}
			});
		});

		var wireReleaseIdVal="";
		for (var i in wireRelease) {
			if(wireReleaseIdVal =="") {
				wireReleaseIdVal= wireReleaseIdVal  + i + '-'+ wireRelease[i] ;
			} else {
				wireReleaseIdVal= wireReleaseIdVal +':' + i + '-'+ wireRelease[i] ;
			}
		}
		$('#wireReleaseDetails').val(wireReleaseIdVal);
		$('#releaseAll').val(wireReleaseAll);
		$('#rejectAll').val(wireRejectAll);
	});

    ns.wire.WireImportAddenda =    function (  ){
        var	urlString ="/cb/pages/jsp/wires/wirefedaddenda.jsp";
        $.ajax({
            url: urlString,
            success: function(data){
                $('#importFED_CTPAddendaDialogID').html(data).dialog('open');
            }
        });
    };


	// Dual Approval scripts
	// Script to set dual approval action links of pending grid
	ns.wire.formatDAWireBeneficiariesActionLinks = function(cellvalue, options, rowObject) {
		var action = '';
		var actionStr = rowObject.action;

		var urlViewString = rowObject["map"].daWirePayeeViewPage;
		var urlEditString = rowObject["map"].daWirePayeeEditPage;
		var urlDiscardString = rowObject["map"].daWirePayeeDiscardPage+"&userAction="+actionStr;
		var urlModifyString = rowObject["map"].daWirePayeeModifyPage+"&userAction="+actionStr;

		// Set dynamic task name for wire beneficiary in edit link URL
		var daTask = "";
		if(actionStr == 'Added') {
			daTask = "AddWireTransferPayee";
		} else {
			daTask = "EditWireTransferPayee";
		}

		urlEditString += "&payeeTask="+ daTask +"&daItemId="+rowObject.daItemId;

		var viewAction = "<a class='' title='View' href='#' onClick='ns.wire.viewWireBeneficiary(\""+urlViewString+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-view'></span></a>";
		var editAction = "<a class='' title='Edit' href='#' onClick='ns.wire.editWireBeneficiary(\""+urlEditString+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var discardAction = "<a class='ui-button' title='Discard' href='#' onClick='ns.wire.confirmDiscardWireBeneficiary(\""+urlDiscardString+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-discard'></span></a>";
		var modifyAction = "<a class='ui-button' title='Modify Change' href='#' onClick='ns.wire.confirmModifyWireBeneficiary(\""+urlModifyString+"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";

		// View permission to user
		var view = rowObject.view;
		if (view != 'undefined' && view == 'true') {
			action += viewAction;
		}

		// If User has Manage Beneficiary permission
		/*if(options.colModel.formatoptions.canManageBeneficiary == 'true')
		{*/
			var canManage = options.colModel.formatoptions.canManageBeneficiary;

			// Edit permission to user
			var edit = rowObject.edit;
			var discard = rowObject.discard;
			if(edit != 'undefined' && edit == 'true' && (canManage == 'true' || (canManage=='false' && rowObject.payeeScope == 'USER'))) {
				action += editAction;
			}
			// Discard permission to user
			var discard = rowObject.discard;
			if(discard != 'undefined' && discard == 'true' && (canManage == 'true' || (canManage=='false' && rowObject.payeeScope == 'USER'))) {
				action += discardAction;
			}
			// Modify permission to user
			var modifyChange = rowObject.modifyChange;
			if(modifyChange != 'undefined' && modifyChange == 'true' && (canManage == 'true' || (canManage=='false' && rowObject.payeeScope == 'USER'))) {
				action += modifyAction;
			}
		/*}*/
		return ' ' + action;
	};

	// Script to set beneficiary name and address fields in pending grid.
	$.subscribe('daWireBeneficiariesGridCompleteEvents', function(event,data) {
		var gridID = 'daWireBeneficiariesGridID';
		var ids = jQuery('#' + gridID).jqGrid('getDataIDs');
		if (ids.length > 0) {
			//Add additional row(approval/rejection information)
			ns.wire.addRejectReasonRowsForPendingWireBeneficiary("#daWireBeneficiariesGridID");
			ns.common.resizeWidthOfGrids();
			$('#wires_beneficiaryPendingApprovalSummary').show();
		}else{
			// hiding the whole div
			$('#wires_beneficiaryPendingApprovalSummary').hide();
		}
	});


	// Confirm discard wire beneficiary function
	ns.wire.confirmDiscardWireBeneficiary = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#discardWireBeneficiaryDialogID').html(data).dialog('open');
			}
		});
	};

	// Confirm discard wire beneficiary function
	ns.wire.discardWireBeneficiary = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
			}
		});
	};

	// Confirm discard wire beneficiary function
	ns.wire.confirmModifyWireBeneficiary = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#modifyWireBeneficiaryDialogID').html(data).dialog('open');
			}
		});
	};

	// Confirm discard wire beneficiary function
	ns.wire.confirmDiscardWireBeneficiary = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#discardWireBeneficiaryDialogID').html(data).dialog('open');
			}
		});
	};

	// Confirm discard wire beneficiary function
	ns.wire.discardWireBeneficiary = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
			}
		});
	};

	// Confirm discard wire beneficiary function
	ns.wire.confirmModifyWireBeneficiary = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				ns.common.setFirstGridPage('#daWireBeneficiariesGridID');
				$('#daWireBeneficiariesGridID').trigger("reloadGrid");
			}
		});
	};

	// Delete beneficiary in DA topic complete script
	$.subscribe('daDeleteWireBeneficiaryCompleteTopics', function(event,data) {
		ns.common.closeDialog();
		ns.common.showStatus();
	});


	// Modify pending changes topic complete script
	$.subscribe('modifyWireBeneficiaryCompleteTopics', function(event,data) {
		ns.common.closeDialog('#modifyWireBeneficiaryDialogID');
		ns.common.showStatus();
	});

	// Discard pending changes topic complete script
	$.subscribe('discardWireBeneficiaryCompleteTopics', function(event,data) {
		ns.common.closeDialog('#discardWireBeneficiaryDialogID');
	});

	// Discard pending changes topic success script
	$.subscribe('discardWireBeneficiarySuccessTopics', function(event,data) {
		ns.common.showStatus();
	});

	// Refresh pending and master beneficiary grids
	$.subscribe('refreshWireBeneficiariesGridSuccessTopics', function(event,data) {
		// Synchronous Call
		$.ajaxSetup({
			async: false
		});
		// Refresh Pending and Approved beneficiary grids
		$('#daWiresLink').click();
		//$('#approvedWiresLink').click();
		if($('#daWireBeneficiariesGridID').length != '0'){
			ns.common.setFirstGridPage('#daWireBeneficiariesGridID');
			$('#daWireBeneficiariesGridID').trigger("reloadGrid");
		}
		ns.common.setFirstGridPage('#wireBeneficiariesGridID');
		$('#wireBeneficiariesGridID').trigger("reloadGrid");

		// Asynchronous Call
		$.ajaxSetup({
			async: true
		});
	});

	//Add reject information in pending wire beneficiary grid.
	ns.wire.addRejectReasonRowsForPendingWireBeneficiary = function(gridID){

		if( gridID == "#daWireBeneficiariesGridID" ){
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			var reasonText = "";

			for(var i=0;i<ids.length;i++){
				var rowId = ids[i];
				var daStatus = $(gridID).jqGrid('getCell', rowId, 'daStatus');
				var rejectReason = $(gridID).jqGrid('getCell', rowId, 'rejectReason');
				var rejectedBy = $(gridID).jqGrid('getCell', rowId, 'rejectedBy');

				//if status is 12, means that the transfer is rejected.
				var currentID = "#daWireBeneficiariesGridID #" + rowId;

				if(daStatus == 'Rejected') {
					rejectReasonTitle = js_reject_reason;
					rejectedByTitle = js_rejected_by;
					if(rejectReason == ''){
						reasonText = "No Reason Provided";
					} else{
						reasonText = ns.common.spcialSymbolConvertion( rejectReason );
					}

					var className = "pltrow1";
					if(rowId % 2 == 0){
						className = "pdkrow";
					}
					className = 'ui-widget-content jqgrow ui-row-ltr';
					$(currentID).after(ns.common.addRejectReasonRow(rejectedByTitle,rejectedBy,rejectReasonTitle,reasonText,className,11));
				}
			}
		}
	};

	// Load  beneficiary grid
	$.subscribe('loadBeneficiaryGrid', function(event,data) {
		$('#approvedWiresLink').click();
	});
