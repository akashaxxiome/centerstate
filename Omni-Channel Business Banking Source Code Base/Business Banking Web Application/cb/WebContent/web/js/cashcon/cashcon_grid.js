	$.subscribe('pendingApprovalCashconGridCompleteEvents', function(event,data) {
		ns.cashcon.handleGridEventsAndSetWidth("#pendingApprovalCashconGridId", "#cashconGridTabs");
		ns.cashcon.addApprovalsRowsForPendingApprovalCashcon("#pendingApprovalCashconGridId");
	});

	$.subscribe('pendingCashconGridCompleteEvents', function(event,data) {
		ns.cashcon.handleGridEventsAndSetWidth("#pendingCashconGridId", "#cashconGridTabs");
	});


	$.subscribe('completedCashconGridCompleteEvents', function(event,data) {
		ns.cashcon.handleGridEventsAndSetWidth("#completedCashconGridId", "#cashconGridTabs");
	});

	$.subscribe('cancelSingleCashconComplete', function(event,data) {
		ns.common.closeDialog('#deleteCashconDialogID');
	});

	$.subscribe('sendInquiryCashconFormSuccess', function(event,data) {
		ns.common.closeDialog("#inquiryCashconDialogID");
		//Display the result message on the status bar.
		ns.common.showStatus();
        $.publish('refreshMessageSmallPanel');
	});

	//After the Cashcon is deleted, the grid summary should be reloaded.
	$.subscribe('cancelCashconSuccessTopics', function(event,data) {
		ns.cashcon.reloadCashconSummaryGrid();
		ns.common.showStatus();
	});

	// reload currently selected grid
	$.subscribe('reloadSelectedCashconGridTab', function(event,data) {
		var selectedTab = $("#cashconGridTabs").tabs("option","active");
		if(selectedTab == 0){
			$('#pendingApprovalCashconGridId').trigger("reloadGrid");
		}
		if(selectedTab == 1){
			$('#pendingCashconGridID').trigger("reloadGrid");
		}
		if(selectedTab == 2){
			$('#completedCashconGridID').trigger("reloadGrid");
		}
	});

	ns.cashcon.handleGridEventsAndSetWidth = function(gridID,tabsID){
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
	}

	ns.cashcon.formatPendingApprovalCashconActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the Cashcon.
		var viewDeposit = "<a class='' title='" +js_view+ "' href='#' onClick='ns.cashcon.viewCashconDetails(\""+ rowObject.map.ViewDepositURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var editDeposit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.cashcon.editSingleCashcon(\""+ rowObject.map.EditDepositURL +"\",\"" + rowObject["typeString"] +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var delDeposit  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.cashcon.deleteCashcon(\""+ rowObject.map.DeleteDepositURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		var inquiryDeposit = "<a class='' title='" +js_inquiry+ "' href='#' onClick='ns.cashcon.inquireTransaction(\""+ rowObject.map.InquireDepositURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		var viewDisbursement = "<a class='' title='" +js_view+ "' href='#' onClick='ns.cashcon.viewCashconDetails(\""+ rowObject.map.ViewDisbursementURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var editDisbursement = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.cashcon.editSingleCashcon(\""+ rowObject.map.EditDisbursementURL +"\",\"" + rowObject["typeString"] +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var delDisbursement  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.cashcon.deleteCashcon(\""+ rowObject.map.DeleteDisbursementURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		var inquiryDisbursement  = "<a class='' title='" +js_inquiry+ "' href='#' onClick='ns.cashcon.inquireTransaction(\""+ rowObject.map.InquireDisbursementURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		if(rowObject["canEdit"] == "false") {
			editDeposit = "";
			editDisbursement = "";
		}
		if(rowObject["canDelete"] == "false") {
			delDeposit = "";
			delDisbursement = "";
		}
		if(rowObject["canView"] == "false") {
			viewDeposit = "";
			viewDisbursement = "";
		}
		if(rowObject.map.canInquire == "false") {
			inquiryDeposit = "";
			inquiryDisbursement = "";
		}

		//if resultOfApproval="Deletion", and status didnot equal "11", hide deletion and edit icons
		if(rowObject["resultOfApproval"] == "Deletion"&& rowObject["status"]!="11")
		{
			delDeposit = "";
			delDisbursement = "";

			editDeposit = "";
			editDisbursement = "";

		}
		if(rowObject["type"] =="15") {
			return ' ' + viewDeposit + ' ' + editDeposit + ' '  + delDeposit + ' ' + inquiryDeposit;
		} else {
			return ' ' + viewDisbursement + ' ' + editDisbursement + ' '  + delDisbursement + ' ' + inquiryDisbursement;
		}
	};

	ns.cashcon.formatPendingCashconActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the Cashcon.
		var viewDeposit = "<a class='' title='" +js_view+ "' href='#' onClick='ns.cashcon.viewCashconDetails(\""+ rowObject.map.ViewDepositURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var editDeposit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.cashcon.editSingleCashcon(\""+ rowObject.map.EditDepositURL +"\",\"" + rowObject["typeString"] +"\" );'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var delDeposit  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.cashcon.deleteCashcon(\""+ rowObject.map.DeleteDepositURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		var inquiryDeposit  = "<a class='' title='" +js_inquiry+ "' href='#' onClick='ns.cashcon.inquireTransaction(\""+ rowObject.map.InquireDepositURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		var viewDisbursement = "<a class='' title='" +js_view+ "' href='#' onClick='ns.cashcon.viewCashconDetails(\""+ rowObject.map.ViewDisbursementURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var editDisbursement = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.cashcon.editSingleCashcon(\""+ rowObject.map.EditDisbursementURL +"\",\"" + rowObject["typeString"] +"\" );'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var delDisbursement  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.cashcon.deleteCashcon(\""+ rowObject.map.DeleteDisbursementURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		var inquiryDisbursement  = "<a class='' title='" +js_inquiry+ "' href='#' onClick='ns.cashcon.inquireTransaction(\""+ rowObject.map.InquireDisbursementURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		if(rowObject["canEdit"] == "false") {
			editDeposit = "";
			editDisbursement = "";
		}
		if(rowObject["canDelete"] == "false") {
			delDeposit = "";
			delDisbursement = "";
		}
		if(rowObject["canView"] == "false") {
			viewDeposit = "";
			viewDisbursement = "";
		}
		if(rowObject.map.canInquire == "false") {
			inquiryDeposit = "";
			inquiryDisbursement = "";
		}

		if(rowObject["type"] =="15") {
			return ' ' + viewDeposit + ' ' + editDeposit + ' '  + delDeposit + ' ' + inquiryDeposit;
		} else {
			return ' ' + viewDisbursement + ' ' + editDisbursement + ' '  + delDisbursement + ' ' + inquiryDisbursement;
		}
	};

	ns.cashcon.formatCompletedCashconActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the Cashcon.
		var viewDeposit = "<a class='' title='" +js_view+ "' href='#' onClick='ns.cashcon.viewCashconDetails(\""+ rowObject.map.ViewDepositURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var inquiryDeposit  = "<a class='' title='" +js_inquiry+ "' href='#' onClick='ns.cashcon.inquireTransaction(\""+ rowObject.map.InquireDepositURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		var viewDisbursement = "<a class='' title='" +js_view+ "' href='#' onClick='ns.cashcon.viewCashconDetails(\""+ rowObject.map.ViewDisbursementURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var inquiryDisbursement  = "<a class='' title='" +js_inquiry+ "' href='#' onClick='ns.cashcon.inquireTransaction(\""+ rowObject.map.InquireDisbursementURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		if(rowObject["canView"] == "false") {
			viewDeposit = "";
			viewDisbursement = "";
		}
		if(rowObject.map.canInquire == "false") {
			inquiryDeposit = "";
			inquiryDisbursement = "";
		}
		if(rowObject["type"] =="15") {
			return ' ' + viewDeposit + ' ' + inquiryDeposit;
		} else {
			return ' ' + viewDisbursement + ' ' + inquiryDisbursement;
		}
	};

	//Reload Cashcon summary jgrid..
	ns.cashcon.reloadCashconSummaryGrid = function(){
		$('#pendingApprovalCashconGridId').trigger("reloadGrid");
		$('#pendingCashconGridId').trigger("reloadGrid");
		$('#completedCashconGridId').trigger("reloadGrid");
	};

	//inquire Cashcon .
	ns.cashcon.inquireTransaction = function( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#inquiryCashconDialogID').html(data).dialog('open');
			}
		});
	};

	ns.cashcon.viewCashconDetails = function( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewCashconDetailsDialogID').html(data).dialog('open');
				ns.common.resizeDialog('viewCashconDetailsDialogID');
			}
		});
	};

	ns.cashcon.deleteCashcon = function( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteCashconDialogID').html(data).dialog('open');
			}
		});
	};

	ns.cashcon.editSingleCashcon = function( urlString, ccType ){

		$.ajax({
			url: urlString,
			success: function(data){
				ns.cashcon.foldPortlet('summary');
				//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
				var heading = "";
				if (ccType.trim() == js_cashcon_disbursement_entry.trim())
				{
					heading = js_cashcon_disbursement_edit;
					ns.common.selectDashboardItem("addDisbursementLink");
				}
				if (ccType.trim() == js_cashcon_deposit_entry.trim())
				{
					heading = js_cashcon_deposit_edit;
					ns.common.selectDashboardItem("addDepositLink");
				}

				//var $title = $('#details .portlet-title');
				//$title.html(heading);
				ns.common.updatePortletTitle('detailsPortlet',heading,false);
		        ns.cashcon.gotoStep(1);
				$('#details').slideDown();
				$("#inputDiv").html(data);

				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#inputDiv").setFocus();
				}
			}
		});
	};

	ns.cashcon.formatApproversInfo = function(cellvalue, options, rowObject) {
		var approverNameInfo="";
		var approverNameOrUserName="";
		var approverGroup="";
		var approverInfos=rowObject.approverInfos;
			if(approverInfos!=undefined)
			{
				for(var i=0;i<approverInfos.length;i++){
				var approverInfo = approverInfos[i];
				if(i!=0)
				{
				approverNameInfo="," + approverNameInfo
				}

				var approverName = approverInfo.approverName;
				var approverIsGroup=approverInfo.approverIsGroup;

				if( approverIsGroup){
					approverGroup = "(" + js_group + ")";
				} else {
					approverGroup = "";
				}
				approverNameOrUserName = approverName + approverGroup;
				approverNameInfo=approverNameOrUserName+approverNameInfo;
				}
			}
			return approverNameInfo;
	};

	ns.cashcon.addApprovalsRowsForPendingApprovalCashcon = function (gridID){
		if( gridID = "#pendingApprovalCashconGridId" ){
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			var approvalsOrReject = "";
			var approvalerNameOrRejectReason = "";
			var rejectedOrSubmitedBy = "";
			var approverNameOrUserName = "";
			var approverGroup = "";
			var submittedForTitle = js_submitted_for;
			var num = 1;
			for(var i=0;i<ids.length;i++){
				var rowId = ids[i];

				var userName = ns.common.HTMLEncode($('#pendingApprovalCashconGridId').jqGrid('getCell', rowId, 'userName'));
				var approverIsGroup = $('#pendingApprovalCashconGridId').jqGrid('getCell', rowId, 'approverIsGroup');
				var status = $('#pendingApprovalCashconGridId').jqGrid('getCell', rowId, 'status');
				var rejectReason = $('#pendingApprovalCashconGridId').jqGrid('getCell', rowId, 'rejectReason');

				var approverNameInfo = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverInfos'));
				var submittedFor = $('#pendingApprovalCashconGridId').jqGrid('getCell', rowId, 'resultOfApproval');
				//if status is 12, means that the Cashcon is rejected.
				var currentID = "#pendingApprovalCashconGridId #" + rowId;

				if( status == 11 ){
					approvalsOrReject = js_reject_reason;
					rejectedOrSubmitedBy = js_rejected_by;
					if(rejectReason==''){
						approvalerNameOrRejectReason = "No Reason Provided";
					} else{
						approvalerNameOrRejectReason = ns.common.spcialSymbolConvertion( rejectReason );
					}
					approverNameOrUserName = approverNameInfo;
					num = num + 1;
					if(approverNameInfo != ""){
		                var className = "pltrow1";
	                    if(rowId % 2 == 0){
	                        className = "pdkrow";
	                    }
		                className = 'ui-widget-content jqgrow ui-row-ltr';

		                $(gridID + ' tr').eq(i + num - 1).after(ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameOrUserName,approvalsOrReject,approvalerNameOrRejectReason,className,11));
	                } else {
	                    var className = "pltrow1";
	                    if(rowId % 2 == 0){
	                        className = "pdkrow";
	                    }
	                    className = 'ui-widget-content jqgrow ui-row-ltr';

	                    $(gridID + ' tr').eq(i + num - 1).after(ns.common.addRejectReasonRow('','',approvalsOrReject,approvalerNameOrRejectReason,className,11));
	                }
				} else {
					approvalsOrReject = js_approval_waiting_on;
					approvalerNameOrRejectReason = approverNameInfo;
					rejectedOrSubmitedBy = js_submitted_by;
					approverNameOrUserName = userName;
					num = num + 1;
					if(approverNameInfo != ""){
		            	var className = "pltrow1";
		                if(rowId % 2 == 0){
		                    className = "pdkrow";
		                }
		                //remove the grid class, and add FF class.
		                //$(currentID).removeClass();
		                className = 'ui-widget-content jqgrow ui-row-ltr';

		                $(gridID + ' tr').eq(i + num - 1).after(ns.common.addSubmittedForRow(approvalsOrReject,approvalerNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,11));
		            } else {
		                var className = "pltrow1";
		                if(rowId % 2 == 0){
		                    className = "pdkrow";
		                }
		                //remove the grid class, and add FF class.

		                //QTS 638252: if a transaction (ACH, Wires, Transfer, billpay, cashcon) is in approvals,
		                // is then edited, then values are modified so it exceeds the limits with no approval
		                // OR exceeds bank limits, it will get an error.  If the transaction is then cancelled,
		                // the transaction is still in the "Approvals Summary" window, but not in approvals
		                // because it was cancelled and not resubmitted.  Added the following error message to the
		                // "Approvals Summary" window, "Submission to approvals FAILED, please resubmit or delete"
		                // so the transaction can be edited to start submission again.
		                //$(currentID).removeClass();
		                className = 'ui-widget-content jqgrow ui-row-ltr';
		                $(currentID).after('<tr class=\"' + className + ' skipRowActionItemRender\"><td colspan="11" align="center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
		                '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'+js_submission_approval_failed+'&nbsp&nbsp&nbsp  </b>' + '&nbsp</td></tr>');
		            }
				}

			}
		}
	};

