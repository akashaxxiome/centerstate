(function($) {
	$.widget("ui.pane", {
		options: {
			title: 'Pane Title',						//title displayed in pane header
			displayLeftIcon: false,						//whether to display min/max icon at the left of pane title
			leftIconFold: 'ui-icon-triangle-1-s',		//the icon at the left of pane title indicating folded pane
			leftIconExpand:	'ui-icon-triangle-1-e',		//the icon at the left of pane title indicating expanded pane
			minmax: true,								//whether to display min/max icon
			beforeMinmax: '',							//function to be called before folding/expanding the pane
			afterMinmax: '',							//function to be called after folding/expanding the pane
			titleControl: true,							//whether to use pane title to control min/max
			headerControl: false,						//whether to use pane header to control min/max
			showHeader: true,							//whether to show pane header
			help: true,									//whether to display Help icon
			helpCallback: '',							//function to be called when clicking on Help icon
			customIcons: '',							//custom icon(s) to be added to pane header. can be a String or an Array of String
			customTips: '',								//custom tool tip(s) corresponding to custom icon(s)
			customCallbacks: '',						//callback function(s) corresponding to custom icon(s)
			useSAPIcons: false,
			expand: true,								//determine the initial state of the pane
			disabled: false,							//not supported yet
			closeCallback: '',							//function to be called when clicking on Close icon - will override default behaviour
			removeOnClose: false,						//whether to remove this portlet from DOM when close
			duration: 'slow',							//duration will be applied to animation for UI state change of panes
			close: false ,								//whether to display Close icon
			url: undefined								//url to load content to the pane
		},    	
    	
        _create: function() {
            var self = this,
                pane_classes = "ui-helper-reset pane ui-widget",
                pane_header_classes = "pane-header ui-state-default ui-corner-top",
                pane_content_classes = "pane-content ui-widget-content ui-corner-bottom",
                pane_title_classes = "pane-title",
                pane_header_licon_classes = "pane-left-icon ui-icon",
                DOT = ".",
                HEADER_CLS = "pane-header",
                TITLE_CLS = "pane-title",
                MINUS_CLS = "ui-icon ui-icon-minus shrink",
                PLUS_CLS = "ui-icon ui-icon-plus shrink",
                HELP_CLS = "ui-icon ui-icon-help help",
                LEFT_ICON_CLS = "pane-left-icon",
                CONTENT_CLS = "ui-widget-content",
            	PANE_ID_SUFFIX = "_pane",
				CLOSE_CLS = "ui-icon ui-icon-close close";
				

            this.expanded = this.options.expand; //fold/expand status - expanded by default
            this.shown = true;
            this.el = $(this.element);

            var pane = $("<div/>").insertBefore(this.el).addClass(pane_classes).attr('id', this.el.attr('id') + PANE_ID_SUFFIX);
        	//content
        	this.el.addClass(pane_content_classes).prependTo(pane);
        	//header
        	var title = $('<span/>');
			var headDiv = $("<div/>").prependTo(pane).append(title).addClass(pane_header_classes);
			title.attr('style','float:left;').addClass(pane_title_classes).html(this.options.title);
			
        	if(this.options.displayLeftIcon) { //add lefthand icon
        		var leftIcon = $('<span/>').prependTo(headDiv).attr('style','float:left;').addClass(pane_header_licon_classes);
				if (this.expanded) {
        			leftIcon.addClass(this.options.leftIconFold);
        		} else {
            		leftIcon.addClass(this.options.leftIconExpand);
        		}
        	}
        	

        	this.el = pane;
            this.hd = this.el.find(DOT + HEADER_CLS)
				.bind( "mouseenter.pane", function() {
					if ( self.options.disabled ) {
						return;
					}
					$( this ).addClass( "ui-state-hover" );
				})
				.bind( "mouseleave.pane", function() {
					if ( self.options.disabled ) {
						return;
					}
					$( this ).removeClass( "ui-state-hover" );
				})
				.bind( "focus.pane", function() {
					if ( self.options.disabled ) {
						return;
					}
					$( this ).addClass( "ui-state-focus" );
				})
				.bind( "blur.pane", function() {
					if ( self.options.disabled ) {
						return;
					}
					$( this ).removeClass( "ui-state-focus" );
				});
			
            //Add a container to hold custom span. 
            this.customContainer = $('<span/>').appendTo(this.hd).attr('class',"customContainerCls").hide();
            
			//Add a container to hold all header controls
            this.hdctl = $('<span/>').appendTo(this.hd).attr('class','headerctls').hide();
            
        	//Display/hide header controls
            this.el.mouseenter(function() {
				self.hdctl.fadeIn();
			});
        	this.el.mouseleave(function() {
				self.hdctl.fadeOut();
        	});
        	
            if (this.expanded) {
            	this.hd.addClass("ui-state-active").addClass(".pane-header-active"); //expanded/active by default
            } else {
				this.el.find(DOT + CONTENT_CLS).hide();
			};
            
            //custom icons
			if ($.isArray(this.options.customCallbacks)) {
				var customCallbackLength = this.options.customCallbacks.length;
				var useSAPIconsFlag = this.options.useSAPIcons;
				$.each(this.options.customCallbacks, function(index, callback){
		            if ($.isFunction(callback)){
		            	var tempClasses = "ctllink ui-corner-all";
		            	if(index != (customCallbackLength -1)){
		            		tempClasses = tempClasses + " marginRight10";
		            	}
			            var customCtl = $('<span/>');
			            $('<a/>').prependTo(self.hdctl).append(customCtl)
			            .attr('title', self.options.customTips[index]).attr('class',tempClasses).attr('href',"javascript:void(0)")
       	            	.hover(function(){
       	            		$(this).toggleClass('ui-state-highlight');
       	            	});
			            
			            var customClass = self.options.customIcons[index];
			            
			            if(useSAPIconsFlag){
			            	customCtl.addClass("sapUiIconCls " + customClass);
			            }else{
			            	customCtl.addClass("ui-icon " + customClass);
			            }
			            
			            self.hd.find(DOT + self.options.customIcons[index]).bind('click', function(event) {
							event.stopPropagation();
			            	callback(event, self);
			            });
		            };
				});
			} else {
	            var customHandler = this.options.customCallbacks;
	            if ($.isFunction(customHandler)){
		            var customCtl = $('<span/>');
					$('<a/>').prependTo(this.hdctl).append(customCtl)
			        .attr('title', this.options.customTips).attr('class',"ctllink ui-corner-all").attr('href',"javascript:void(0)")
   	            	.hover(function(){
   	            		$(this).toggleClass('ui-state-highlight');
   	            	});
					
					var customClass = self.options.customIcons;
					
		            if(this.options.useSAPIcons){
		            	customCtl.addClass("sapUiIconCls " + customClass);
		            }else{
		            	customCtl.addClass("ui-icon " + customClass);
		            }
					
		            this.hd.find(DOT + this.options.customIcons).bind('click', function(event) {
						event.stopPropagation();
		            	customHandler(event, self);
		            });
	            };
			}
            
            //needs "self" to reference this object as the function may be used as event handlers
            this.minmaxFunc = function(){
            	
            	var beforeHandler = self.options.beforeMinmax;
	            if ($.isFunction(beforeHandler)) {
	            	if (!beforeHandler(self)) {
	            		return;
	            	}
	            }

            	if(self.expanded) {
            		self.hd.find('.shrink').removeClass("ui-icon-minus").addClass("ui-icon-plus");
            		if(self.options.displayLeftIcon && (self.options.leftIconFold != self.options.leftIconExpand)){
                		self.hd.find('.pane-left-icon').addClass(self.options.leftIconExpand).removeClass(self.options.leftIconFold);
            		}
            		$(self.element).parents(".pane:first").find(".ui-widget-content").slideUp();
	                self.hd.removeClass('ui-state-active').removeClass('pane-header-active');
            	} else {
            		self.hd.find('.shrink').addClass("ui-icon-minus").removeClass("ui-icon-plus");
            		if(self.options.displayLeftIcon && (self.options.leftIconFold != self.options.leftIconExpand)){
                		self.hd.find('.pane-left-icon').removeClass(self.options.leftIconExpand).addClass(self.options.leftIconFold);
            		}
	                $(self.element).parents(".pane:first").find(".ui-widget-content").slideDown();
	                self.hd.addClass('ui-state-active').addClass('pane-header-active');
            	}
            	self.expanded = !self.expanded;

            	var afterHandler = self.options.afterMinmax;
	            if ($.isFunction(afterHandler)) {
	            	afterHandler(self);
	            }

            };

            //disable minmax icon and title control if the whole header will be used to control min/max
            if(this.options.headerControl){
            	this.options.minmax = false;
            	this.options.titleControl = false;
            };
            
            
            //help button
            if(this.options.help){

	            var helpHandler = this.options.helpCallback;
	        	if ($.isFunction(helpHandler)) {
					var help = $('<span/>');
					$('<a/>').prependTo(this.hdctl).append(help).attr('class',"ctllink ui-corner-all").attr('href',"javascript:void(0)")
	            	.hover(function(){
	            		$(this).toggleClass('ui-state-highlight');
	            	});
					help.addClass(HELP_CLS);

					this.hd.find('.help').bind('click', function(event) {
							helpHandler(event, self);
					});
	            }
            };
            
            
            //minimize/maximize button
            if(this.options.minmax){
            	var $minmaxctl=$('<span/>');
	            $('<a/>').prependTo(this.hdctl).append($minmaxctl).attr('class',"ctllink ui-corner-all").attr('href',"javascript:void(0)")
            	.hover(function(){
            		$(this).toggleClass('ui-state-highlight');
            	});
				
				if (this.expanded) {
					$minmaxctl.addClass(MINUS_CLS);
				} else {
					$minmaxctl.addClass(PLUS_CLS);
				};
	            
	            this.hd.find('.shrink').bind('click', this.minmaxFunc);
            };
            
			 //close button
            if(this.options.close){
            	var close = $('<span/>');
            	$('<a/>').prependTo(this.hdctl).append(close).attr('class',"ctllink ui-corner-all").attr('href',"javascript:void(0)")
            	.hover(function(){
            		$(this).toggleClass('ui-state-highlight');
            	});
				close.addClass(CLOSE_CLS);

	            var closeHandler = this.options.closeCallback;
	            var duration = this.options.duration;
	            var removeOnClose = this.options.removeOnClose;
	            var pane = this.el;
	            
		        this.hd.find('.close').bind('click', function(event) {
		        	if ($.isFunction(closeHandler)) {
		        		closeHandler(event, pane);
		            }else {
		        		pane.slideUp(duration, function(){
				            if(removeOnClose) {
				            	$(this).remove();
					        }
				        });
		        	}
	            });
            };
			
            if(this.options.headerControl){
	            this.hd.bind('click', this.minmaxFunc);
            };

            if(this.options.titleControl){
	            this.hd.find('.pane-title').bind('click', this.minmaxFunc);
            };
            
            
            if(!this.options.showHeader){
            	this.hd.hide();
            }
            
        },
        
        title : function(title){
        	$(this.element).parents(".pane:first").find('.pane-title').html(title);
        },
        
		destroy: function() {
			this.hdctl.children('.ctllink').each(function(i, alink){
				var $icon = $(alink).children('.ui-icon');
				if ($icon){
					$icon.unbind();
					$icon.removeClass();
					$icon.empty();
				}
				$(alink).unbind();
				$(alink).remove();
			});
			this.hdctl.removeClass();
			this.hdctl.unbind();
			this.hdctl.empty();
			this.hd.removeClass();
			this.hd.unbind();
			$(this.element).insertAfter(this.el);
        	$(this.element).removeClass('pane-content').removeClass('ui-widget-content').removeClass('ui-corner-bottom');
        	this.el.unbind();
        	this.el.remove();
			$.Widget.prototype.destroy.apply(this,arguments);
			this.hdctl = null;
			this.hd=null;
			this.element=null;
			this.el = null;
		},
        
        fold : function(event) {
        	if (this.expanded)
        		this.minmaxFunc();
        },

        expand : function(event) {
        	if (!this.expanded)
        		this.minmaxFunc();
        },
        
        isOpen: function() {
        	return this.expanded;
        },
        
		addCustomContainer: function($icon) {
			this.hd.find('.customContainerCls').append($icon);
			this.hd.find('.customContainerCls').show();
		},
        
        hideHeader : function(event) {
        	$(this.element).parents(".pane:first").find('.pane-header').hide();
        },
        
		showHeader : function(event) {
			$(this.element).parents(".pane:first").find('.pane-header').show();
		},

        hide : function(event) {
        	$(this.element).parents(".pane:first").hide();
        	this.shown = false;
        },
        
		show : function(event) {
			$(this.element).parents(".pane:first").show();
        	this.shown = true;
		},
		
		isShown: function(event) {
			return this.shown;
		},
		
		load : function(isAsync) {
			var self = this;
			if(self.options.url){
				$.ajax({
					url: this.options.url,
					async: isAsync,
					success: function(data, textStatus, xmlHttpRequest){
						$(self.element).html(data);
						self.contentLoaded = true;
					},
					error: function(xmlHttpRequest, textStatus, errorThrown){
					},
					complete: function(xmlHttpRequest, textStatus){
					}
				});
				
			}
		}
    });

})(jQuery); 
