
$.subscribe('completedStopsGridCompleteEvents', function(event,data) {
	ns.stops.handleGridEventsAndSetWidth("#completedStopsGrid", "#stopsGridTabs");
});

//Formatter of Amount column.
ns.stops.formatTotalAmount = function ( cellvalue, options, rowObject ){
	if (cellvalue=='999'){
			return "--";
	} else if (cellvalue){
		return cellvalue;
	} else {
		return '';
	}
};

//Add approval information in the stops summary gird.
ns.stops.addStopsExtraRow = function(gridID,totalAmountValue, totalText){
	var ids = jQuery(gridID).jqGrid('getDataIDs');
	var rowId = ids[ids.length-1];
	var curRow = $("#" + rowId, $(gridID));
	var className = "ui-widget-content jqgrow ui-row-ltr";
	if(ids.length % 2 == 1)
	{
		className = "ui-widget-content jqgrow ui-row-ltr ui-state-zebra";
	}
	curRow.after('<tr class=\"' + className + '\"><td colspan="6"></td><td style="text-align: right"> <b> ' + totalText + " : " + '</b></td><td style="text-align: right"> <b> ' + totalAmountValue + '</b></td> <td colspan="3"></td></tr>');
};

ns.stops.handleGridEventsAndSetWidth = function (gridID,tabsID){
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
	};
	$.subscribe('cancelStopsFormComplete', function(event,data) {
		$('#completedStopsGrid').trigger("reloadGrid");
		ns.common.closeDialog('#deleteStopsDialogID');
	});

	$.subscribe('cancelStopsSuccessTopics', function(event,data) {
		ns.common.showStatus();
	});

	$.subscribe('errorDeleteStops', function(event,data) {
		ns.common.showError();
	});

	$.subscribe('cancelStopsFormComplete1', function(event,data) {
		ns.common.closeDialog('#deleteStopsDialogID');
		ns.common.showStatus();
	});

	$.subscribe('cancelStopsFormComplete2', function(event,data) {
		ns.common.closeDialog('#deleteStopsDialogID');
		ns.common.showStatus();
	});

	$.subscribe('closeViewStopsDialog', function(event,data) {
		ns.common.closeDialog("#viewStopsDetailsDialogID");
	});

	$.subscribe('closeDeleteStopsDialog', function(event,data) {
		ns.common.closeDialog("#deleteStopsDialogID");
	});

	$.subscribe('sendInquiryStopsFormSuccess', function(event,data) {
        ns.common.closeDialog("#InquiryStopsDialogID");
		//Display the result message on the status bar.
		ns.common.showStatus();
        $.publish('refreshMessageSmallPanel');
	});


	ns.stops.formatCheckDateColumn =  function (cellvalue, options, rowObject) {
		var checkDate = "";
		if( cellvalue == ""){
			checkDate = "---";
		} else {
			checkDate = cellvalue;
		}
		return checkDate;
	};


	ns.stops.formatPayeeNameColumn =  function (cellvalue, options, rowObject) {
		var payeeName = "";
		if( cellvalue == ""){
			payeeName = "---";
		} else {
			payeeName = ns.common.HTMLEncode(cellvalue);
		}
		return payeeName;
	};


ns.stops.formatCompletedStopsActionLinks =  function (cellvalue, options, rowObject) {
		//cellvalue is the ID of the transfer.
		var view = "<a class='' title='" +js_view+ "' href='#' onClick='ns.stops.viewStopsDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var inquiry  = "<a class='' title='" +js_inquiry+ "' href='#' onClick='ns.stops.inquireTransaction(\""+ rowObject.map.InquireURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";
        var edit = "<a class='' title='" +js_edit+ "' href='#' onClick='ns.stops.editStopPayment(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
        var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.stops.deleteStops(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

		if(rowObject.map.canInquire == "false") {
			inquiry = "";
		}
		if(rowObject["isEditable"] == "FALSE" || rowObject["canEdit"] == "false") {
			edit = "";
		}
		if(rowObject["isEditable"] == "FALSE" || rowObject["canDelete"] == "false") {
			del  = "";
		}
        return ' ' + view + ' ' + edit + ' '  + del + ' ' + inquiry ;
	};

ns.stops.formatAmoutColumn = function (cellvalue, options, rowObject) {
	if(rowObject["amountValue"]){
		return (rowObject["amountValue"].currencyStringNoSymbol + ' ' + rowObject["amountValue"].currencyCode);
	}
	return cellvalue;//Cell value will be available only in case of footer row.
};

ns.stops.inquireTransaction = function ( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#InquiryStopsDialogID').html(data).dialog('open');
			}
		});
	};

ns.stops.viewStopsDetails =  function (urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewStopsDetailsDialogID').html(data).dialog('open');
			}
		});
	};

ns.stops.deleteStops = function ( urlString ){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteStopsDialogID').html(data).dialog('open');
			}
		});
	};


ns.stops.editStopPayment = function ( urlString ){
		$.publish('beforeLoadStopPaymentForm');
		$.ajax({
			url: urlString,
			success: function(data){
				$('#inputDiv').html(data);
				$.publish('loadStopPaymentFormComplete');
			}
		});
		ns.common.updatePortletTitle("detailsPortlet","dfdsfds",false);
		ns.common.selectDashboardItem("stopsLink");
	};

//Reload Stop Payments summary jgrid..
ns.stops.reloadStopsSummaryGrid = function ( gridId ){
	$('#completedStopsGrid').trigger("reloadGrid");
};

