/**
 * Asynchronous loader
 * It loads js and css files asynchronously, provides a callback when
 * loading in done.
 */
var asyncLoader = (function(window, undefined) {
    var _load = function(sType, oAttributes, fnCallback) {
        var s, r, t, attr, attributeValue;
        r = false;
        s = document.createElement(sType);
        for (attr in oAttributes) {
            attributeValue = oAttributes[attr];
            s.setAttribute(attr, attributeValue);
        }
        s.onload = s.onreadystatechange = function() {
            //console.log( this.readyState ); //uncomment this line to see which ready states are called.  
            if (!r && (!this.readyState || this.readyState == "complete")) {
                r = true;
                fnCallback();
            }
        };
        t = document.getElementsByTagName(sType)[0];
        t.parentElement.insertBefore(s, t);
    };

    return {
        /**
         * loads the script
         * @param oAttributes {Object} accepts the attributes for script
         * @param fnCallback {function} callback function which will be called when script loads completely
         */
        loadScript: function(oAttributes, fnCallback) {
            oAttributes.type = "text/javascript";
            _load("script", oAttributes, fnCallback);
        },

        /**
         * loads the style
         * @param oAttributes {Object} accepts the attributes for style
         * @param fnCallback {function} callback function which will be called when style loads completely
         */
        loadStyle: function(oAttributes, fnCallback) {
            oAttributes.type = "text/css";
            _load("css", oAttributes, fnCallback);
        }
    };
})(window);