$.subscribe('inboxMessagesGridCompleteEvents', function(event,data) {
	ns.message.handleGridEventsAndSetWidth("#inboxMessagesGridId", "#Inbox");
	
	$('#inboxMessagesGridId').find('tr').each(function(){
		var $cIcon = $(this).find('.ui-icon-mail-closed');
		if($cIcon.length > 0 ){
			$(this).find('td').addClass('ui-state-important');
		}
	});
	//Adding Date Picker for receive Date
	 $("#gbox_inboxMessagesGridId #gs_date").bind('click', function(event){
		$("#gbox_inboxMessagesGridId #gs_date").datepicker();
		$("#gbox_inboxMessagesGridId #gs_date").blur();
		$("#gbox_inboxMessagesGridId #gs_date").focus();
    });
	
	 $('#jqgh_inboxMessagesGridId_Delete').on('click',function(){
		 $('#multicheck').val(true);
		 $("#deleteInboxMessageLink").trigger('click');
	 });
	
});


$.subscribe('inboxMessagesGridPagingEvents', function(event,data) {
	$('#jqgh_inboxMessagesGridId_ID #selectionCtl1').attr('checked', false);
});

$.subscribe('sentMessagesGridPagingEvents', function(event,data) {
	$('#jqgh_sentMessagesGridId_ID #selectionCtl2').attr('checked', false);
});


$.subscribe('inboxMessagesLoadCompleteEvents', function(event,data) {
	$("#jqgh_inboxMessagesGridId_Delete").removeClass( "ui-jqgrid-sortable" ).addClass( "sapUiIconCls icon-delete" );
	$("#jqgh_inboxMessagesGridId_Delete").css({"cursor" : "pointer"});
	$("#inboxMessagesGridId tbody > tr > td:first-child input").css({ "margin-left" : "14px" });
	
	var rows = $('#inboxMessagesGridId').jqGrid('getGridParam','reccount');
	$('#gbox_inboxMessagesGridId').find(".ui-jqgrid-norecords").remove();
	$('#jqgh_inboxMessagesGridId_ID #selectionCtl1').attr('checked', false);
	
	/**
	 * Commented the following code to match consistency across grids when no records are available. 
	 * Ref:CR#705583
	 */
//	if(rows === 0){
//		$('#gbox_inboxMessagesGridId').find(".ui-jqgrid-hdiv").after('<div class="ui-jqgrid-norecords">'+js_nomsg_inbox+'</div>');
//	}

});


$.subscribe('sentMessagesLoadCompleteEvents', function(event,data) {
	$("#jqgh_sentMessagesGridId_Delete").removeClass( "ui-jqgrid-sortable" ).addClass( "sapUiIconCls icon-delete" );
	$("#jqgh_sentMessagesGridId_Delete").css({"cursor" : "pointer"});
	$("#sentMessagesGridId tbody > tr > td:first-child input").css({ "margin-left" : "14px" });
	
	var rows = $('#sentMessagesGridId').jqGrid('getGridParam','reccount');
	$('#gbox_sentMessagesGridId').find(".ui-jqgrid-norecords").remove();
	$('#jqgh_sentMessagesGridId_ID #selectionCtl2').attr('checked', false);
	
	/**
	 * Commented the following code to match consistency across grids when no records are available. 
	 * Ref:CR#705583
	 */
//	if(rows === 0){
//		$('#gbox_sentMessagesGridId').find(".ui-jqgrid-hdiv").after('<div class="ui-jqgrid-norecords">'+js_nomsg_inbox+'</div>');
//	}
	
});


$.subscribe('sentMessagesGridCompleteEvents', function(event,data) {
	ns.message.handleGridEventsAndSetWidth("#sentMessagesGridId", "#Sentbox");
	if(accessibility){
		$("frmSendMessage").setInitialFocus();
	}
	//Adding Date Picker for receive Date
	 $("#gbox_sentMessagesGridId #gs_createDate").bind('click', function(event){
		$("#gbox_sentMessagesGridId #gs_createDate").datepicker();
		$("#gbox_sentMessagesGridId #gs_createDate").blur();
		$("#gbox_sentMessagesGridId #gs_createDate").focus();
   });
	 
	 $('#jqgh_sentMessagesGridId_Delete').on('click',function(){
		 $('#multicheck').val(true);
		 $("#deleteSentMessageLink").trigger('click');
	 });
	
});



ns.message.handleGridEventsAndSetWidth = function(gridID,tabsID){
	//Adjust the width of grid automatically.
	ns.common.resizeWidthOfGrids();
}


ns.message.formatToName = function(cellvalue, options, rowObject) {
	return cellvalue;
}


ns.message.formatFromName = function(cellvalue, options, rowObject) {
	if (rowObject.readDate!= undefined && rowObject.readDate!= ''){
		return "<a><span class='sapUiIconCls icon-email-read inlineSapIconClass'></span></a><span class='paddingLeft10'>" + cellvalue + "</span>";
	} else {
		return "<a><span class='sapUiIconCls icon-email inlineSapIconClass'></span></a><span class='paddingLeft10'>" + cellvalue + "</span>";
	}
}

//for Inbox
ns.message.formatSubjectLink1 = function(cellvalue, options, rowObject) {
	var link = "<a title='" +js_view+ "' href='#' onClick='ns.message.viewMessage(\""+ options.rowId + "\",\"" + rowObject.map.ViewURL +"\",\"Inbox\");'>" + ns.common.HTMLEncode(cellvalue) + "</a>";
	return ' ' + link;
}

//for Sentbox
ns.message.formatSubjectLink2 = function(cellvalue, options, rowObject) {
	var link = "<a title='" +js_view+ "' href='#' onClick='ns.message.viewMessage(\""+ options.rowId + "\",\"" + rowObject.map.ViewURL +"\",\"Sentbox\");'>" + ns.common.HTMLEncode(cellvalue) + "</a>";
	return ' ' + link;
}

ns.message.formatSentDeleteLinks = function(cellvalue, options, rowObject) {
	
	//var ele = "<input type=\"checkbox\" name=\"selectedMsgIDs\" onClick=\"ns.message.syncCheckbox()\" value=\"" + rowObject.ID + "\">";
	var link = "<a title='" +js_view+ "' href='#' onClick='ns.message.viewMessage(\""+ options.rowId + "\",\"" + rowObject.map.ViewURL +"\",\"Sentbox\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var ele = "<a title='" +js_delete+ "' href='#' onClick='ns.message.deleteSentMsg(\"" + rowObject.ID + "\")'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	return ' ' + link + ' ' + ele;
};

ns.message.formatDeleteLinks = function(cellvalue, options, rowObject) {
	
	//var ele = "<input type=\"checkbox\" name=\"selectedMsgIDs\" onClick=\"ns.message.syncCheckbox()\" value=\"" + rowObject.ID + "\">";
	var link = "<a title='" +js_view+ "' href='#' onClick='ns.message.viewMessage(\""+ options.rowId + "\",\"" + rowObject.map.ViewURL +"\",\"Inbox\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var ele = "<a title='" +js_delete+ "' href='#' onClick='ns.message.deleteReceivedMsg(\"" + rowObject.ID + "\")'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
	return ' ' + link + ' ' + ele;
};

ns.message.deleteAllIds = function(cellvalue, options, rowObject) {
	//var ele = "<input type=\"checkbox\" name=\"selectedMsgIDs\" onClick=\"ns.message.syncCheckbox()\" value=\"" + rowObject.ID + "\">";setSelectedMsgIDs
	var link = rowObject.ID;
	return link;
};

ns.message.deleteReceivedMsg = function(msgId){
	//console.log(msgId);
	$("#selectedInboxMsgIDs").val(msgId);
	$("#deleteInboxMessageLink").trigger('click');
};

ns.message.deleteSentMsg = function(msgId){
	//console.log(msgId);
	$("#selectedSentMsgIDs").val(msgId);
	$("#deleteSentMessageLink").trigger('click');
};


ns.message.viewMessage = function(rowId, urlString, mailbox){
	
	//alert(messageId);
	var urlString;
	if ('Inbox' === mailbox) {
		ns.message.mailbox = 'Inbox';
	} else if ('Sentbox' === mailbox) {
		ns.message.mailbox = 'Sentbox';
	}

	$.ajax({   
		  url: urlString,   
		  success: function(data) { 
			$('#messageView').html(data);
			
			$('#newMsgPortlet').portlet('hide');
			$('#actionMsgPortlet').portlet('expand');
			$('#actionMsgPortlet').show();
			//$('#viewMsgPortlet').portlet('title', $('#subjectInViewMsg').html());
			//$('#messageReply').pane('hide');
			$('#messageReplyWrapper').slideUp();
			//$('#messageView').pane('hideHeader');
			//$('#messageView').pane('expand');
			$('#messageView').slideDown();
			$('#details').slideDown();
			
			$('#messageTabs').portlet('fold');
			$('#messageTabs').hide();
		    $('#newMsgPortlet').hide();
		    
		    $.publish("reloadCurrentMessagesGrid");

			$.publish("common.topics.tabifydesktop");
			if(accessibility){
				$("#details").setFocus();
			}

		  }   
	});

	var $currentRow = $('#inboxMessagesGridId').find('tr#'+rowId);
	var $cIcon = $currentRow.find('.ui-icon-mail-closed');
	if($cIcon.length > 0){
		$('<a class="ui-button"><span class="ui-icon ui-icon-mail-open"></span></a>').insertBefore($cIcon);
		$cIcon.remove();
		$currentRow.find('td').removeClass('ui-state-important');
		$.publish('refreshMessageSmallPanel');
	}
	
	
};

ns.message.syncCheckbox = function(){
	
	var unchecked = false;
	var $chkboxs = null;
	if ('Inbox' === ns.message.mailbox) {
		$chkboxs = $("table#inboxMessagesGridId").find("input[name='selectedMsgIDs'][type='checkbox']");
	} else if ('Sentbox' === ns.message.mailbox) {
		$chkboxs =  $("table#sentMessagesGridId").find("input[name='selectedMsgIDs'][type='checkbox']");
	}	
	for(var i = 0; i < $chkboxs.length; i++){
		var $chkbox = $chkboxs.eq(i);
		if(!$chkbox.is(":checked")){			
			unchecked = true;
			break;
		}
	}

	if ('Inbox' === ns.message.mailbox) {
		$("#selectionCtl1").attr('checked', !unchecked);		
	} else if ('Sentbox' === ns.message.mailbox) {
		$("#selectionCtl2").attr('checked', !unchecked);
	}
};

$.subscribe("messages.refreshMessagesGrid",function(event,data){
	$("#inboxMessagesGridId").trigger("reloadGrid");
});

