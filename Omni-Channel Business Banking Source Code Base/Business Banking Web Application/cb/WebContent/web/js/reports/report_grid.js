$.subscribe('savedReportsGridCompleteEvents', function(event,data) {
	//handleGridEventsAndSetWidth("#savedReportsGridId", "#savedReportsDiv");
});

ns.report.handleGridEventsAndSetWidth = function(gridID,tabsID){
	//Adjust the width of grid automatically.
	ns.common.resizeWidthOfGrids();
};



ns.report.formatSavedReportsActionLinks = function(cellvalue, options, rowObject) { 
	//cellvalue is the ID of the report.
	var edit = "<a class='ui-button' title='" +js_edit+ "' href='#' onClick='ns.report.editReport2(\""+ rowObject.map.EditURL +  "\",\"" + rowObject.reportName + "\");'><span class='ui-icon ui-icon-wrench'></span></a>";
	var del  = "<a class='ui-button' title='" +js_delete+ "' href='#' onClick='ns.report.deleteReport2(\"" + rowObject.map.DeleteURL + "\");'><span class='ui-icon ui-icon-trash'></span></a>";
	if(rowObject.map.isCurrentReport=='true'){
		   edit = "<a class='ui-button' title='" +js_cannot_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled'></span></a>";
           del = "<a class='ui-button' title='" +js_cannot_delete+ "' href='#' ><span class='ui-icon ui-icon-trash ui-state-disabled'></span></a>";
	}
	return ' ' + edit + ' '  + del ;
};


ns.report.formatReportNameLink = function(cellvalue, options, rowObject) {
	//cellvalue is the name of the report.
	var link = "<a title='" +js_generate+ "' href='#' onClick='ns.report.go2(\""+ rowObject.map.ViewURL + "\",\"" + cellvalue + "\");'>" + cellvalue + "</a>";
	return ' ' + link;
};