ns.report.setup = function(){
	
	ns.report.reportType = false;
	
	$('#reportTabs').find('ul').addClass('portlet-header');

	$.subscribe('removeErrorsOnSuccess', function(event, data) {
			$('.errorLabel').html('').removeClass('errorLabel');
			$('#formerrors').html('');
	});
	
	$.subscribe('removeErrorsOnBegin', function(event, data) {
			$('.errorLabel').html('').removeClass('errorLabel');
			$('#formerrors').html('');
	});
	
	/**
	 * Open Save Report Dialog
	 */
	$.subscribe('errorSaveReportDialogCriteria', function(event, data){
		
	});
	
	$.subscribe('successSaveReportDialogCriteria', function(event, data){
		$('#savereportdialog').dialog('open');
	});

	
	/**
	 * Save Report
	 */
	$.subscribe('beforeSaveReportCriteria', function(event, data) {
		ns.report.newReportName = $('#addReport #name').val().replace(/^\s+|\s+$/g, "");
	});
	
	$.subscribe('completeSaveReportCriteria', function(event, data) {
	});

	$.subscribe('successSaveReportCriteria', function(event, data) {
		//reload saved reports grid
		$('#savedReportsGridId').trigger("reloadGrid");
		
		//only refresh the modified list
		ns.report.refreshListSelectmenu();
		if(ns.report.newReportName){
			ns.report.editReport2('/cb/pages/jsp/reports/GetReportFromNameAction_edit.action?reportName='+ns.report.newReportName, ns.report.newReportName);
			delete ns.report.newReportName;
		}
	
        var statusMessage = $('#resultmessage').html();
		ns.common.showStatus(statusMessage);
		$.publish('closeDialog');  	// TODO It is possible that the report types list is still loading
									//      when the dialog has been closed and the old types list is displayed to user directly. 
	});

	$.subscribe('errorSaveReportCriteria', function(event, data) {
		$.publish('closeDialog');
	});
	
	
	/**
	 * Delete Report Confirmation
	 */
	$.subscribe('beforeConfirmDeleteReportCriteria', function(event,data) {
	});

	$.subscribe('successConfirmDeleteReportCriteria', function(event,data) {
		$('#deletereportdialog').dialog('open');
	});

	$.subscribe('errorConfirmDeleteReportCriteria', function(event,data) {
	});

	$.subscribe('completeConfirmDeleteReportCriteria', function(event,data) {
	});

	
	/**
	 * Delete Report
	 */
	$.subscribe('beforeDeleteReportCriteria', function(event,data) {
	});

	$.subscribe('successDeleteReportCriteria', function(event,data) {
		//reload saved reports list
		$('#savedReportsGridId').trigger("reloadGrid");
 
		//only refresh the modified list
		ns.report.refreshListSelectmenu();
		
        var statusMessage = $('#resultmessage').html();
		ns.common.showStatus(statusMessage);
		$('#deletereportdialog').dialog('close');
		
		
	});
	
	$.subscribe('errorDeleteReportCriteria', function(event,data) {
	});
	
	$.subscribe('completeDeleteReportCriteria', function(event,data) {
	});

	/**
	 * Generate saved report
	 */
	$.subscribe('beforeGenerateSavedReport', function(event,data) {
	});
	
	$.subscribe('successGenerateSavedReport', function(event,data) {
	});
	
	$.subscribe('errorGenerateSavedReport', function(event,data) {
	});

	$.subscribe('completeGenerateSavedReport', function(event,data) {
	});
	
	/**
	 * Edit report criteria
	 */
	$.subscribe('beforeEditReportCriteria', function(event,data) {
	});
	
	$.subscribe('successEditReportCriteria', function(event,data) {
		$('#reportTabs').portlet('fold');

		$('#details').slideDown();

		ns.report.gotoStep(1);
		
	});
	
	$.subscribe('errorEditReportCriteria', function(event,data) {
	});

	$.subscribe('completeEditReportCriteria', function(event,data) {
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#criteriaForm").setFocus();
		}
	});
	

	/**
	 * Setting header/footer options
	 */
	$.subscribe('beforeSaveHeadFootSetting', function(event,data) {
	});
	
	$.subscribe('successSaveHeadFootSetting', function(event,data) {
	});
	
	$.subscribe('errorSaveHeadFootSetting', function(event,data) {
	});

	$.subscribe('completeSaveHeadFootSetting', function(event,data) {
		$.publish('closeDialog');
	});

			
	
	/**
	 * Click on a report link but before making request
	 */
	$.subscribe('clickOnReportTitleBefore', function(event,data) {
	});
	
	$.subscribe('clickOnReportTitle', function(event, data) {
		$("#reportTabs a[invokerlink]").attr("invokerlink",false);
		var link = $(event.originalEvent.currentTarget);
		link.attr("invokerlink","true");
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			$("#criteriaForm").setFocus();
		}
		$('#summary').hide();
	});
	
	$.subscribe('clickOnReportTitleSuccess', function(event,data) {
		// remove duplicated dialogs
//		ns.report.removeDupCriteriaDialog();
		
		$('#reportTabs').portlet('fold');
		$('#details').slideDown();
		ns.report.gotoStep(1);
	});
	
	$.subscribe('clickOnReportTitleComplete', function(event, data) {
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			// Tab setting is handled by topic clickOnReportTitleSuccess.
			$("#criteriaForm").setFocus();   
		}
	});
	


	$.subscribe('gotoCriteria', function(event,data) {
	    
		ns.report.gotoStep(1);
	});
	
	$.subscribe('onCancelDisplayReportClick', function(event,data) {
		
	    if( $('#targetRptResultDivController').attr('closeDetail') == 'true' )
    	{
	    	$('#summary').show();
	    	ns.report.closeDetails();
    	}
	    else
    	{
	    	if(ns.report.currentGo && (ns.report.currentEdit != ns.report.currentGo)){
				ns.report.editReport2('/cb/pages/jsp/reports/GetReportFromNameAction_edit.action?reportName='+ns.report.currentGo, ns.report.currentGo);
				delete ns.report.currentGo;
	    	}
	    	
	    	//if loading saved report 
	    	if(ns.report.reportType)
    		{
	    		$('#summary').show();
	    		$('#details').hide();
    		}
	    	else{
	    		ns.report.gotoStep(1);
	    	}
    	}

	    $("#targetRptResultDiv").slideUp();
	    $("#targetRptCriteriaDiv").show();
	});
	
	$.subscribe('cancelReportForm', function(event,data) {
	    
		$.log('About to cancel Form! ');
		ns.report.closeDetails();
		ns.common.hideStatus();

		$.publish("common.topics.tabifyDesktop");
		$("#reportTabs a[invokerlink=true]").focus();
		$('#summary').show();
	});
}; // end ns.report.setup()

//Commented because of no need to remove it 
/*ns.report.removeDupCriteriaDialog = function(){
	$(".reportCriteriaDialog").each(function(){
		$(this).remove();
	});
};*/

ns.report.gotoStep = function(stepnum){
	var tabindex = stepnum - 1;
	$('#criteriaTabs').tabs( 'enable', tabindex );
	$('#criteriaTabs').tabs( 'option','active', tabindex );
	$('#criteriaTabs').tabs( 'disable', (tabindex+1)%2 );
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#criteriaTabs").setFocusOnTab();
	}
}; // end ns.report.gotoStep()


/**
 * Close Details island and expand Summary island
 */
ns.report.closeDetails = function(){
	$('#reportTabs').portlet('expand');
    $('#details').slideUp();
}; // end ns.report.closeDetails()

/**
 * Click on Generate Report on report criteria page
 */
ns.report.generateReport = function() {
	
	if( !ns.report.masterValidate('criteriaFormName') )
		return;
	
	var url = "/cb/pages/jsp/reports/UpdateReportCriteriaAction_generateReport.action";
	
	var destination = "";
	if($("#report_rptcrtr_dest_list").length > 0) {
		destination = $("#report_rptcrtr_dest_list option:selected").val();
	} else if($("[name=destination]").length > 0) {
		destination = $("[name=destination]").val();
	}
	var format =$("#report_format option:selected").val();
	
	if(destination == "HTTP"){
		//This request for generating report may take long time, hence explicitly making ajax timeout as infinite.
		if(format == "PDF"){
					$("#criteriaForm").attr('action', url);
					$('#criteriaForm').attr('target', '_blank');
					$('#criteriaForm')[0].submit();
		}
		else{
		$.ajax({  
				type: "POST"
			,	url: url
			,	timeout: "0"
			,	data:$("#criteriaForm").serialize()
			,	success: function(data, textStatus, req){
						// display online					
						// set the page back to criteria page when the 'CANCEL' button is clicked
						var type = req.getResponseHeader('Content-Type');
						$('#targetRptResultDivController').attr('closeDetail','false');
						ns.report.displayGeneratedReport(
								data
							, 	type
						);
						
					}
			});
		}
	 }
	 else {
		 //export the report.
		 ns.report.exportReport( url, $("#criteriaForm")[0] );
	 }
	
	
	$("#targetRptResultDiv").hide();
	$("#targetRptResultDivController").hide();
	
}; // end ns.report.generateReport()


ns.report.displayGeneratedReport = function(data, type)
{
	
	var type = type == null ? "text/html" : type.toLowerCase();
	
	
	if(type.indexOf('text/html') > -1 )
	{
		$('.reportDisplayInnerDialog').remove();	// remove the duplicated dialogs in the displaying page.
		
		$("#targetRptResultDivContent").css("white-space","normal").html(data);
		$("#targetRptResultDivController").hide();
		
		ns.report.gotoStep(2);
		if($('#details').css('display') == 'none')
		{
			$('#reportTabs').portlet('fold');
			$('#details').slideDown();
			$('#summary').hide();
		}
	}
	else if(type.indexOf('text/plain') > -1 )
	{
		
		$("#targetRptResultDivContent").html("<div style='white-space: pre; overflow: auto;'>" + data + "</div>");
		$("#targetRptResultDivController").show();
		
		ns.report.gotoStep(2);
		
		if($('#details').css('display') == 'none')
		{
			$('#reportTabs').portlet('fold');
			$('#details').slideDown();
		}
	} 
	$("#targetRptResultDiv").slideDown();
	$("#targetRptCriteriaDiv").hide();
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#targetRptResultDivContent").setFocus();
	}
}; // ns.report.displaygeneratedReport

/**
 * Click Export Report on report criteria page or display report page
 */
ns.report.exportReport = function(url, formDom){

	if(url.indexOf('?') >0)
		url = url + "&" + new Date().getTime();
	else
		url = url + "?" + new Date().getTime();
	
	formDom.action = url;
	//formDom.target = "_blank";
	formDom.submit();
}; // end ns.report.exportreport()

/**
 * Click Save Report on report criteria page
 */
ns.report.saveReport = function() {
	if(ns.report.masterValidate("criteriaFormName")){
		$.ajax({
			url:  "/cb/pages/jsp/reports/UpdateReportCriteriaAction_saveReport.action",
			data: $('#criteriaForm').serialize(),
			type: 'post',
			success: function(data){
				$('#savereportdialog').html(data).dialog('open');
			}
		});
	}
}; // end ns.report.saveReport()


/**
 * Click Save Report on report criteria page for existing report
 */
ns.report.modifyReport = function() {
	if(ns.report.masterValidate("criteriaFormName")){
		$.ajax({
			url:  "/cb/pages/jsp/reports/UpdateReportCriteriaAction_modifyReport.action",
			data: $('#criteriaForm').serialize(),
			type: 'post',
			success: function(data){
				$('#resultmessage').html(data);
				$('#savedReportsGridId').trigger('reloadGrid');

				//only refresh the modified list
				ns.report.refreshListSelectmenu();
				
                var statusMessage = $('#resultmessage').html();
                ns.common.showStatus(statusMessage);
			}
		});
	}
}; // end ns.report.modifyReport()

/**
 * Click on Custom Reporting on report criteria page
 */
ns.report.customReport = function() {
	var url = "/cb/pages/jsp/reports/UpdateReportCriteriaAction_inputCustom.action";

	$.ajax({   
		  type: "POST",   
		  url: url,   
		  data: $('#criteriaForm').serialize(), 
		  success: function(data) {
		
//			ns.report.removeDupCriteriaDialog();
			$('#reportCriteriaDiv').html(data);

			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#criteriaForm").setFocus();
			}
		  }   
	});
}; // end ns.report.customReport()

/**
 * Click Edit on report type list page
 */
ns.report.editReport = function(formName) {
	ns.report.currentEdit = $('#' + formName + '_select').val();
	$.ajax({   
		  type: "POST",   
		  url: "/cb/pages/jsp/reports/GetReportFromNameAction_edit.action",   
		  data: $('#'+formName).serialize(), 
		  success: function(data) { 
//			ns.report.removeDupCriteriaDialog();
			$('#targetRptCriteriaDiv').html(data);
			$.publish('successEditReportCriteria');

			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#criteriaForm").setFocus();
			}
		  }   
	});
	$("#summary").hide();
}; // end ns.report.editReport


/**
 * Click Edit on saved report list page
 */
ns.report.editReport2 = function(urlString,reportName) {
	ns.report.currentEdit = reportName;
	$.ajax({   
		  url: urlString,   
		  success: function(data) {
//			ns.report.removeDupCriteriaDialog();
			$('#targetRptCriteriaDiv').html(data);
			$.publish('successEditReportCriteria');
			
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#criteriaForm").setFocus();	
			}
		  }   
	});
}; // end ns.report.editReport2


/**
 * Click Delete on report type list page
 */
ns.report.deleteReport = function(formName) {
	$.ajax({   
		  type: "POST",   
		  url: "/cb/pages/jsp/reports/GetReportIDFromNameAction_delete.action",   
		  data: $('#'+formName).serialize(), 
		  success: function(data) { 
			
			$('#deletereportdialog').html(data).dialog('open');
		  }   
	});
}; // end ns.report.deleteReport


/**
 * Click Delete on saved report list page
 */
ns.report.deleteReport2 = function(urlString) {

	$.ajax({   
		  url: urlString,   
		  success: function(data) { 
			$('#deletereportdialog').html(data).dialog('open');
		  }   
	});
}; // end ns.report.deleteReport2

/**
 * Click Go on report type list page
 */
ns.report.go = function(formName) {

 var selectMenu= formName+ "_select";	
 var selector="#"+selectMenu + " option:selected";
 var destination = $(selector).attr("dest");
 var format = $(selector).attr("format");
 
 if(destination == "USER_FILE_SYSTEM"){
	// need to download;
	ns.report.exportReport( "/cb/pages/jsp/reports/GetReportFromNameAction_generate.action", $('#'+formName)[0] );
 }
 else{
	if(format == "PDF"){
							var url= "/cb/pages/jsp/reports/GetReportFromNameAction_generate.action?"
													+ $('#'+formName).serialize();
							window.open(url, "newWindow");
	}
	else{
	$.ajax({   
		  type: "POST",   
		  url: "/cb/pages/jsp/reports/GetReportFromNameAction_generate.action",   
		  data: $('#'+formName).serialize(), 
		  success: function(data, textStatus, req) { 
				var type = req.getResponseHeader('Content-Type');
				$('#targetRptResultDivController').attr('closeDetail','true');
				ns.report.displayGeneratedReport(
						data
					, 	type
				);
				ns.report.reportType = true;
			}	
	});
   }
 }
}; // end ns.report.go()

/**
 * Click on report name on saved reports page
 */
ns.report.go2 = function(urlString,reportName){
	ns.report.currentGo = reportName;
	$.ajax({   
		  url: urlString,   
		  success: function(data, textStatus, req) {
				var disposition = req.getResponseHeader("Content-Disposition");
				
				if( disposition != null && disposition.indexOf('attachment') > -1 )
				{	// download the report file
					$("#ifrmDownload")[0].src = urlString;
				}else{
					var type = req.getResponseHeader('Content-Type');
					var typeValue = type == null ? "text/html" : type.toLowerCase();
					$('#targetRptResultDivController').attr('closeDetail','false');
					if(typeValue.indexOf('text/html') == -1 && typeValue.indexOf('text/plain') == -1){
							// display pdf report file in a new window.
							window.open(urlString, "newWindow");
					}
					ns.report.displayGeneratedReport(
							data
						, 	type
					);
				}
			  }
	});
}; // end ns.report.go2()

/**
 * Refresh report criteria upon selection change of certain criteria when there are 
 * dependencies among criteria
 */
ns.report.refresh = function(){
	var urlString = "/cb/pages/jsp/reports/UpdateReportCriteriaAction_input.action";
	$.ajax({   
		  type: "POST",   
		  url: urlString,   
		  data: $('#criteriaForm').serialize(), 
		  success: function(data) { 
		
//			ns.report.removeDupCriteriaDialog();
			$('#reportCriteriaDiv').html(data);
			
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#criteriaForm").setFocus();
			}
		  }   
	});
}; // end ns.report.refresh()

ns.report.refreshListSelectmenu = function(){

	$.ajax({
		url:  "/cb/pages/jsp/reports/GetSavedReportsAction_queryall.action",
		success: function(data){

			var category, optVals, disabled, destVals , formatVals;
			
			if($.browser.msie)
			{
				
				var xmlDoc=new ActiveXObject("Microsoft.XMLDOM");
				xmlDoc.async="false";
				xmlDoc.loadXML(data);
				category = xmlDoc.getElementsByTagName('category')[0].childNodes[0].nodeValue + "Form";
				var optNodes = xmlDoc.getElementsByTagName('value');
				var formatNodes = xmlDoc.getElementsByTagName('format');
				var destNodes = xmlDoc.getElementsByTagName('destination');
				
				optVals = new Array();
				destVals = new Array();
				formatVals = new Array();
				
				for ( var i = 0 ; i < optNodes.length ; i++ ){
					optVals[i] = optNodes[i].childNodes[0].nodeValue;
					formatVals[i] = formatNodes[i].childNodes[0].nodeValue;
					destVals[i] = destNodes[i].childNodes[0].nodeValue;
				}
				
				disabled = xmlDoc.getElementsByTagName('disabled').length > 0 ? true : false;
			} else {
				category = $(data).find('category').html() + "Form";
				optVals = $.map( $(data).find('value'), function(value){return $(value).html(); } );
				destVals = $.map( $(data).find('destination'), function(destination){return $(destination).html(); } );
				formatVals = $.map( $(data).find('format'), function(format){return $(format).html(); } );
				disabled = $(data).find('disabled').size() > 0 ? true : false;
			}
		
			var $select = $('#' + category + '_select');
			$select.selectmenu('destroy');
			$select.html('');
			
			if( disabled )
			{
				$('#' + category + "_btnGo" ).addClass('ui-state-disabled').attr('onClick','{return false;}');
				$('#' + category + "_btnEdit" ).addClass('ui-state-disabled').attr('onClick','{return false;}');
				$('#' + category + "_btnDelete" ).addClass('ui-state-disabled').attr('onClick','{return false;}');
				
				$select.attr('disabled', 'true');
			} else {
				$('#' + category + "_btnGo" ).removeClass('ui-state-disabled')
					.attr('onClick','ns.report.go("' + category + '")');
				$('#' + category + "_btnEdit" ).removeClass('ui-state-disabled')
					.attr('onClick','ns.report.editReport("' + category + '")');
				$('#' + category + "_btnDelete" ).removeClass('ui-state-disabled')
					.attr('onClick','ns.report.deleteReport("' + category + '")');
				
				$select.removeAttr('disabled');
			}
			
			var optionVal="";
				
			for ( var i = 0 ; i < optVals.length ; i++ ){
				/*if(i==0) {
					optionVal=optionVal + '<option value="0">Select Report</option>';
				}*/
				optionVal=optionVal + '<option value="' + optVals[i] + '" ' + 'format="' + formatVals[i] + '" ' + 'dest="' + destVals[i] + '">' + optVals[i] + '</option>';
			}
			
			if(optionVal==""){
				optionVal = '<option>'+js_report_label_no_saved_reports+'</option>';
			}
			
			$select.append(optionVal);
			$select.selectmenu({width: 300});
			
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				$("#criteriaForm").setFocus();
			}
		}
	});
};

ns.report.setHeaderFooter = function(){
	var urlString = "/cb/pages/jsp/reports/reportheadfoot2.jsp";
	var formatStr = $('#report_format').val();
	var csrf_tokenStr = $("#criteriaForm").find("input[name='CSRF_TOKEN']").val();
	$.ajax({   
		  url: urlString,  
		  type:"post",
		  data: {CSRF_TOKEN: csrf_tokenStr, format: formatStr},
		  success: function(data) { 
			$('#setheadfootdialog').html(data).dialog('open');
		  }   
	});
}; // end ns.report.setHeaderFooter()


/* TO show the PRINTER READY window */
ns.report.printerReady = function(){
	
	var url = "/cb/pages/jsp/reports/inc/printreport.jsp";
	$.ajax({
		url: url,
		success: function(data){
			$("#printerReadyReportDialog").html(data).dialog('open');
		}
	});
};



$(document).ready(function() {
	$.debug(false);
	ns.report.setup();
});


ns.report.renderSelectedReport = function(url,element){
	$.ajax({
		url: url,
		data: {dashboardElementId: element.id},
		success: function(data){
			$("#summary").html(data);
		}
	});
};
