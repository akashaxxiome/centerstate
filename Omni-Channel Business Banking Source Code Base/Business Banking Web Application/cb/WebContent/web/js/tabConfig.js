
	
	ns.common.tabs = new Object();


	ns.common.tabs.saveSeq = function(tabID,flag){
		var tabs;
		if(flag == "save"){
			tabs = $(tabID).data('current');
		}
		if(flag == "revert"){
			tabs = $(tabID).data('defaultSeq');
		}
		$.ajax({
			type: "POST",
			url: "/cb/pages/util/TabConfigAction_saveTabSetting.action",
			data: {tabSeq : tabs, transID : $(tabID).data('transid'), CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
			success: function(data, textStatus) {
				if(flag == "save"){
					$(tabID+" #tabChangeNote").html("<span>Saved successfully</span>");
					$(tabID+" #tabRevertNote").html("");
				}
				if(flag == "revert"){
					$(tabID+" #tabChangeNote").html("<span>Restored successfully</span>");
					$(tabID+" #tabRevertNote").html("");
					ns.common.tabs.revertToDefault(tabID);
				}
				$(tabID).data('original', tabs);
				setTimeout('ns.common.tabs.clear("'+tabID+'")',2000);
			}
		});
	}
	
	ns.common.tabs.clear = function(tabID){
		$(tabID+" #tabChangeNote span").remove();
	}
	
	ns.common.tabs.getDefaultSeq = function(tabID){
		$.ajax({
			type: "POST",
			url: "/cb/pages/util/TabConfigAction_getDefaultSetting.action",
			data: {transID : $(tabID).data('transid'), CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
			success: function(data, textStatus) {
				$(tabID).data('defaultSeq', data);
			}
		});
	}
	
	ns.common.tabs.getSeq = function(tabID){
		var seq = "";
		var $list = $(tabID+" li a");
		for(i=0; i<$list.length-1; i++){
			seq += $list.eq(i).attr("id");
			seq += ",";
		}
		seq += $list.eq(i).attr("id");
		return seq;
	} 
	
	ns.common.tabs.revertToDefault = function(tabID){
		var list = $(tabID).data('defaultSeq').split(",");
		var i;
		for (i=list.length-1; i>=0; i--){
			$(tabID+" li").each(function(){
				if(list[i] == $(this).find('a').attr('id')){
					$(this).prependTo(tabID+" ul");
				}
			});
		}  
	} 
	
	ns.common.tabs.tabUpdate = function(tabID){
		$(tabID).data('current', ns.common.tabs.getSeq(tabID));
		if($(tabID).data('current')==$(tabID).data('original')){
			$(tabID+" #tabChangeNote").html("");
			$(tabID+" #tabRevertNote").html("");
		}
		if($(tabID).data('current')!=$(tabID).data('original')){
			$(tabID+' #tabChangeNote').html("<a href='javascript:void(0);' title='Save user preference' onclick='ns.common.tabs.saveSeq(\""+tabID+"\",\"save\")'>Save Change</a>");
			$(tabID+" #tabRevertNote").html("");
			if($(tabID).data('original')!=$(tabID).data('defaultSeq') && $(tabID).data('current')!=$(tabID).data('defaultSeq')){
				$(tabID+' #tabRevertNote').html("<a href='javascript:void(0);' title='Revert to system default' onclick='ns.common.tabs.saveSeq(\""+tabID+"\",\"revert\")'>Restore</a>");
			}
		}
	
	}
	
	ns.common.tabs.tabInit = function(tabID){
		$(tabID).data('transid', $(tabID+" #getTransID").val());
		$(tabID).data('original', ns.common.tabs.getSeq(tabID));
		ns.common.tabs.getDefaultSeq(tabID);

	}

	