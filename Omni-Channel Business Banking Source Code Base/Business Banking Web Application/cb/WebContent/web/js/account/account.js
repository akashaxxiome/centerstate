	ns.account.displayType = "";   
	ns.account.consolidatedBalanceDisplayType = "";
   ns.account.refreshConsolidatedSummaryForm = function(urlString){
	   $.ajax({
		   url: urlString,
		   success: function(data){
		   $("#consolidatedBalanceSummary").html(data);
	   }
	   });
   };
    
   ns.account.refreshdepositAccountSummary = function(urlString){
	   $.ajax({
		   url: urlString,
		   success: function(data){
		   $("#depositAccountSummary").html(data);
	   }
	   });
   };
   
   
   ns.account.refreshAssetAccountSummary = function(urlString){
	   $.ajax({
		   url: urlString,
		   success: function(data){
		   $("#assetAccountSummary").html(data);
	   }
	   });
   };
   
   ns.account.refreshCcardAccountSummary = function(urlString){
	   $.ajax({
		   url: urlString,
		   success: function(data){
		   $("#creditCardsAccountSummary").html(data);
	   }
	   });
   };
   
   ns.account.refreshLoanAccountSummary = function(urlString){
	   $.ajax({
		   url: urlString,
		   success: function(data){
		   $("#loanAccountSummary").html(data);
	   }
	   });
   };
   
   ns.account.refreshOtherAccountSummary = function(urlString){
	   $.ajax({
		   url: urlString,
		   success: function(data){
		   $("#otherAccountSummary").html(data);
	   }
	   });
   };
   
   //show account balance grid summaries
   ns.account.showGridSummary = function(gridId, urlString, divIdToLoad) {
	   if($('#'+gridId).length > 0)
				{
					ns.common.showGridSummary(divIdToLoad);
				} else {
					$(".gridSummaryContainerCls" ).each(function(i) {
						$(this).hide();
					});
					var gridSummaryId = divIdToLoad + "SummaryGrid";
					$('#'+gridSummaryId).removeClass('hidden');
					$('#'+gridSummaryId).show();
					
					var userSelctorGridTab = divIdToLoad;
					//Get title of user selected grid tab
					var selectedGridTabTitle = $("#"+userSelctorGridTab).html().trim();
					if(selectedGridTabTitle != undefined && selectedGridTabTitle!= ''){
						$("#selectedGridTab").html(selectedGridTabTitle); //Set selected grid tab title
					}
					if(divIdToLoad ==='depositAccounts'){
						ns.account.refreshdepositAccountSummary(urlString);
					} else if(divIdToLoad ==='assetAccounts'){
						ns.account.refreshAssetAccountSummary(urlString);
					} else if(divIdToLoad ==='creditCardsAccounts'){
						ns.account.refreshCcardAccountSummary(urlString);
					} else if(divIdToLoad ==='loanAccounts'){
						ns.account.refreshLoanAccountSummary(urlString);
					} else if(divIdToLoad ==='otherAccounts'){
						ns.account.refreshOtherAccountSummary(urlString);
					} 
				}
	   			if(ns.account.displayType){
	   				changeAccountSummaryPortletView(ns.account.displayType);
	   			}	
	};
   
// called by ()
	ns.account.redirectAccountDetail = function(accountGroupID,dataClassfication){
		var urlString = "/cb/pages/jsp/accountsbygroup.jsp?AccountGroupID="+accountGroupID+"&AccountBalancesDataClassification="+dataClassfication;
		$.ajax({
			url: urlString,	
			success: function(data){
				$('#depositAccountSummary').html('');
		   }
		});
	};
	
// the js for account register account module
	 ns.account.backReportButton = function(urlString){
    	 //var urlString ="/cb/pages/jsp/account/register-reports.jsp";
         $.ajax({
  			url: urlString,	
  			success: function(data){
  			$('#accountRegisterDashbord').html(data);
  		}
  		});
        };
	 ns.account.cancleButtonRedirectRegiste = function(urlString){
		    //var urlString ="/cb/pages/jsp/account/register-wait.jsp";
		    $.ajax({
					url: urlString,	
					success: function(data){
					$('#accountRegisterDashbord').html(data);
				}
				});
			   };
			   
 
   //Show the active dashboard based on the user selected summary
   ns.account.showAccountDashboard = function(userActionType,dashboardElementId){
   		if(dashboardElementId && dashboardElementId !='' && $('#'+dashboardElementId).length > 0){
   			setTimeout(function(){$("#"+dashboardElementId).trigger("click");}, 10);
   		}
   };