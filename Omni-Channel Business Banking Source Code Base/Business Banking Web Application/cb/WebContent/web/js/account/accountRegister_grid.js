   
    ns.account.setup1 = function(){
    //define events for account register summary.
	$.subscribe('accountRegisterSummaryEvent', function(event,data) {
		ns.account.accountRegisterSummaryEventDetail("#accountRegisterSummaryID");
		ns.account.customRegisterNicknameColumn("#accountRegisterSummaryID","dateIssued");
		ns.account.customRegisterCategoryColumn("#accountRegisterSummaryID","payeeName");
		ns.common.resizeWidthOfGrids();
	});
	$.subscribe('futureAccountRegisterSummaryEvent', function(event,data) {
		ns.account.customFutureCategoryColumn("#futureAccountRegisterSummaryID","map\\.payeeName");
		ns.common.resizeWidthOfGrids();
	});
	
	//define events for payee summary.
	$.subscribe('payeeSummaryEvent', function(event,data) {
		ns.common.resizeWidthOfGrids()
	});
	
	$.subscribe('deletePayeeSuccessTopicss', function(event,data) {  
		$('#payeeSummaryID').trigger("reloadGrid");
		ns.common.closeDialog('#deletePayeeDialogID');
	});
	// current categories data grid js
	$.subscribe('currentCategoriesSummaryEvent', function(event,data) {
		ns.common.resizeWidthOfGrids()
	});
	
	$.subscribe('deleteCategorySuccessTopics', function(event,data) {
		$('#currentCategoriesSummaryID').trigger("reloadGrid");
		ns.common.closeDialog('#deleteCategoryDialogID');
	});
	
	$.subscribe('deleteRecordSuccessTopicss', function(event,data) {  
		$('#accountRegisterSummaryID').trigger("reloadGrid");
		$('#futureAccountRegisterSummaryID').trigger("reloadGrid");
		ns.common.closeDialog('#deleteRecordDialogID');
	});
	
		//load the check image dialog
		var config = ns.common.dialogs["checkImage"];
		config.autoOpen = "false";
		ns.common.openDialog("checkImage");	
	}
    ns.account.customRegisterNicknameColumn = function(gridID,type){
    	if($('#accountNumberID_'+type).size() == 0) {
			$('<span>' + js_DateIssued + '</span>')
			.attr('id', 'accountNumberID_'+type)
			.attr('style','cursor:pointer')
			.bind('click', function() {
				if($('#accountNumberID_'+type).attr('order') == 'asc') {
					$('span.ui-icon-desc',$('#accountNumberID_'+type)).removeClass("ui-state-disabled");
					$('span.ui-icon-asc',$('#accountNumberID_'+type)).addClass("ui-state-disabled");
                    ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
                    $(gridID).jqGrid('setGridParam',{sortorder:'desc'});
					$('#accountNumberID_'+type).attr('order','desc');
				} else {
					$('span.ui-icon-asc',$('#accountNumberID_'+type)).removeClass("ui-state-disabled");	
					$('span.ui-icon-desc',$('#accountNumberID_'+type)).addClass("ui-state-disabled");
                    ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
					$(gridID).jqGrid('setGridParam',{sortorder:'asc'});
					$('#accountNumberID_'+type).attr('order','asc');
				}
				$('.s-ico').hide();
				$(gridID).trigger('reloadGrid');
			}).insertBefore($("#jqgh_"+type));
		} else {
			if($(gridID).jqGrid('getGridParam','sortname') == 'dateIssued') {
				$('span.s-ico',$("#jqgh_"+type)).show();
			}
		}
    }
    
    ns.account.customRegisterCategoryColumn = function(gridID,type){
    	if($('#accountNumberID_'+type).size() == 0) {
			$('<span>' + js_PayeePayor + '</span>')
			.attr('id', 'accountNumberID_'+type)
			.attr('style','cursor:pointer')
			.bind('click', function() {
				if($('#accountNumberID_'+type).attr('order') == 'asc') {
					$('span.ui-icon-desc',$('#accountNumberID_'+type)).removeClass("ui-state-disabled");
					$('span.ui-icon-asc',$('#accountNumberID_'+type)).addClass("ui-state-disabled");
                    ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
					$(gridID).jqGrid('setGridParam',{sortorder:'desc'});
					$('#accountNumberID_'+type).attr('order','desc');
				} else {
					$('span.ui-icon-asc',$('#accountNumberID_'+type)).removeClass("ui-state-disabled");	
					$('span.ui-icon-desc',$('#accountNumberID_'+type)).addClass("ui-state-disabled");
                    ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
					$(gridID).jqGrid('setGridParam',{sortorder:'asc'});
					$('#accountNumberID_'+type).attr('order','asc');
				}
				$('.s-ico').hide();
				$(gridID).trigger('reloadGrid');
			}).insertBefore($("#jqgh_"+type));
		} else {
			if($(gridID).jqGrid('getGridParam','sortname') == 'payeeName') {
				$('span.s-ico',$("#jqgh_"+type)).show();
			}
		}
    }
    
    ns.account.customFutureCategoryColumn = function(gridID,type){
    	
    	if($('#accountNumberIDs_map\\.payeeName').size() == 0) {
			$('<span>' + js_PayeePayor + '</span>')
			.attr('id', 'accountNumberIDs_map.payeeName')
			.attr('style','cursor:pointer')
			.bind('click', function() {
				if($('#accountNumberIDs_'+type).attr('order') == 'asc') {
					$('span.ui-icon-desc',$('#accountNumberIDs_'+type)).removeClass("ui-state-disabled");
					$('span.ui-icon-asc',$('#accountNumberIDs_'+type)).addClass("ui-state-disabled");
                    ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
					$(gridID).jqGrid('setGridParam',{sortorder:'desc'});
					$('#accountNumberIDs_'+type).attr('order','desc');
				} else {
					$('span.ui-icon-asc',$('#accountNumberIDs_'+type)).removeClass("ui-state-disabled");	
					$('span.ui-icon-desc',$('#accountNumberIDs_'+type)).addClass("ui-state-disabled");
                    ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
					$(gridID).jqGrid('setGridParam',{sortorder:'asc'});
					$('#accountNumberIDs_'+type).attr('order','asc');
				}
				$('.s-ico').hide();
				$(gridID).trigger('reloadGrid');
			}).insertBefore($("#jqgh_"+type));
		} else {
			if($(gridID).jqGrid('getGridParam','sortname') == 'map.payeeName') {
				$('span.s-ico',$("#jqgh_"+type)).show();
			}
		}
    }
	
	ns.account.accountRegisterSummaryEventDetail = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		var currencyCode='';
		var accountBalance='';
		var sessRegBalance=' 0.00';
		for(var i=0;i<ids.length;i++){  
			var rowId = ids[i];
			currencyCode = $(gridID).jqGrid('getCell', rowId, 'map.currencyCode');
			accountBalance = $(gridID).jqGrid('getCell', rowId, 'map.accountBalance');
			sessRegBalance = $(gridID).jqGrid('getCell', rowId, 'map.sessRegBalance');
			//get this data compared with sessionRegBalance and change the font color
			var compData = $(gridID).jqGrid('getCell', rowId, 'map.currencyAmountValue');
			
		}
		$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_status']:eq("+ids.length + ")").html( js_CurrentBankBalance + "("+currencyCode+"):"+accountBalance);
		if(compData<0){
			$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_amountValue.currencyStringNoSymbol']:eq("+ids.length + ")").html(js_RegisterBalance + ":<font color='red'>"+sessRegBalance+"</font>");
		}else{
			$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_amountValue.currencyStringNoSymbol']:eq("+ids.length + ")").html(js_RegisterBalance+ ":"+sessRegBalance+"");
		}
		
	}
	
	
	ns.account.payeeActionFormatter = function( cellvalue, options, rowObject ){
		var edit = "<a class='ui-button' title='" +js_edit+ "' href='#' onclick=\"ns.account.editPayee('"+ rowObject.map.EditURL +"');\"><span class='ui-icon ui-icon-wrench'></span></a>";				
		var del  = "<a class='ui-button' title='" +js_delete+ "' href='#' onclick=\"ns.account.deletePayee('"+ rowObject.map.DeleteURL +"');\"><span class='ui-icon ui-icon-trash'></span></a>";
	    return ' '+edit+'   '+del;
	}
	
	ns.account.editPayee = function(urlString){
		  //var urlString ="/cb/pages/jsp/account/register-payees.jsp?SetEditRegisterPayeeId="+id;
		    $.ajax({
					url: urlString,	
					success: function(data){
					$('#accountRegisterDashbord').html(data);
				}
				});
	}
	ns.account.deletePayee = function(urlString){
		  //var urlString ="/cb/pages/jsp/account/register-payee-delete.jsp?SetRegisterPayeeId="+id;
		    $.ajax({
					url: urlString,	
					success: function(data){
					$('#deletePayeeDialogID').html(data).dialog('open');
				}
				});
	}
	ns.account.redirectA = function(filter){
		var urlString = "/cb/pages/jsp/account/payees_grid.jsp?filter="+filter;
		$.ajax({
				url: urlString,
				success: function(data){
				$('#payeeSummaryDiv').html(data);
			}
			});
		
	}

	
	// categories type formatter
	ns.account.categoriesTypeFormatter = function( cellvalue, options, rowObject ){
		var type="";
		if(rowObject["map"].catType=="root"){
		if(cellvalue==0){
			type=js_Income;
		}if(cellvalue==2){
			type=js_Income;
		}else{
			type=js_Expense;
		}
		 }
	    return type;
	}
	// taxRelated formatter
	ns.account.taxRelatedFormatter = function( cellvalue, options, rowObject ){
		var related="";
		if(cellvalue=="true"){
			related=js_Yes;
		}if(cellvalue=="false"){
			related=js_No;
		}
		return related;
	}
	//action formatter
	ns.account.categoryActionFormatter = function(cellvalue, options, rowObject){
		var edit = "<a class='ui-button' title='" +js_edit+ "' href='#' onclick=\"ns.account.editCategory('"+ rowObject.map.EditURL +"');\"><span class='ui-icon ui-icon-wrench'></span></a>";	
		var del  = "<a class='ui-button' title='" +js_delete+ "' href='#' onclick=\"ns.account.deleteCategory('"+ rowObject.map.DeleteURL +"');\"><span class='ui-icon ui-icon-trash'></span></a>";
		if(rowObject["map"].hassub=="hassub"){
			del = "<a class='ui-button' title='" +js_delete+ "' href='#' ><span class='ui-icon ui-icon-trash ui-state-disabled'></span></a>";
			return ' '+edit+'   '+del;
		}else{
			 return ' '+edit+'   '+del;
		}
	}
	ns.account.editCategory = function(urlString){
		//var urlString ="/cb/pages/jsp/account/register-categories.jsp?SetEditRegisterCategory="+id;
		$.ajax({
			url: urlString,	
			success: function(data){
			$('#accountRegisterDashbord').html(data);
		}
		});
	}
	ns.account.deleteCategory = function(urlString){
		//var urlString="/cb/pages/jsp/account/register-cat-delete.jsp?SetRegisterCategoryId="+id;
		 $.ajax({
				url: urlString,	
				success: function(data){
				$('#deleteCategoryDialogID').html(data).dialog('open');
			}
			});
	}
	ns.account.categoryNameFormatter = function(cellvalue, options, rowObject){
		var catName =cellvalue;
		if(rowObject["map"].catType=="sub"){
			catName="     "+cellvalue;
		}
		return catName;
	}
	
	//index for categories
	ns.account.redirectCategoryFilter = function(filter){
		var urlString = "/cb/pages/jsp/account/currentcategories_grid.jsp?filter="+filter;
		$.ajax({
				url: urlString,
				success: function(data){
				$('#categoriesSummaryDiv').html(data);
			}
			});
	}
	// account register main page for the first grid action formatter
	ns.account.accountRegisterActionFormatter = function(cellvalue, options, rowObject){
		var eidtFlag = rowObject["map"].registerStatusType;
		var collectionType = rowObject["map"].collectionType;
		// if status<1 eauals true ,this record can be deleted
		var status = rowObject["status"];
		var edit ="<a class='ui-button' title='" +js_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled'></span></a>";
		var dele ="<a class='ui-button' title='" +js_delete+ "' href='#' ><span class='ui-icon ui-icon-trash ui-state-disabled'></span></a>";
		if(eidtFlag=='016'){
	     edit ="<a class='ui-button' title='" +js_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled'></span></a>";
		 dele ="<a class='ui-button' title='" +js_delete+ "' href='#' ><span class='ui-icon ui-icon-trash ui-state-disabled'></span></a>";
		
		}else if(eidtFlag=='025'){ 
			edit ="<a class='ui-button' title='" +js_edit+ "' href='#' ><span class='ui-icon ui-icon-wrench ui-state-disabled'></span></a>";
		    dele ="<a class='ui-button' title='" +js_delete+ "' href='#' ><span class='ui-icon ui-icon-trash ui-state-disabled'></span></a>";
		
		}else{ 
			if(status<1){
				edit = "<a class='ui-button' title='" +js_edit+ "' href='#' onclick=\"ns.account.editAccountRegister('"+ rowObject.map.EditURL +"');\"><span class='ui-icon ui-icon-wrench'></span></a>";
				dele = "<a class='ui-button' title='" +js_delete+ "' href='#' onclick=\"ns.account.deleteAccountRegister('"+ rowObject.map.DeleteURL +"');\"><span class='ui-icon ui-icon-trash'></span></a>";
			}else{
				edit = "<a class='ui-button' title='" +js_edit+ "' href='#' onclick=\"ns.account.editAccountRegister('"+ rowObject.map.EditURL +"');\"><span class='ui-icon ui-icon-wrench'></span></a>";
				dele ="<a class='ui-button' title='" +js_delete+ "' href='#' ><span class='ui-icon ui-icon-trash ui-state-disabled'></span></a>";
		
			}
		}
		return ' '+edit+'  '+dele+' ';
	}
	ns.account.editAccountRegister = function(urlString){
		//var urlString ="/cb/pages/jsp/account/register-rec-edit-wait.jsp?SetRegisterTransactionId="+id+"&CollectionSessionName="+collectionType;
		$.ajax({
			url: urlString,	
			success: function(data){
			$('#accountRegisterDashbord').html(data);
		}
		});
	}
	// delete account register record and pending transactions
	ns.account.deleteAccountRegister = function(urlString){
		//var urlString="/cb/pages/jsp/account/register-rec-delete.jsp?SetRegisterTransactionId="+id+"&CollectionSessionName="+collectionType;
		 $.ajax({
				url: urlString,	
				success: function(data){
				$('#deleteRecordDialogID').html(data).dialog('open');
			}
			});
	}
	
	//account register data formatter
	ns.account.dateIssuedFormatter = function(cellvalue, options, rowObject){
		var dateCleared = rowObject["map"].dateClearedAlia;
		var date = cellvalue+" \n"+dateCleared;
		return date;
	}
	// payee name and categories formatter
	ns.account.payeeNameCategoriesFormatter = function(cellvalue, options, rowObject){
		var categName = rowObject["map"].categoryName;
		var memo = rowObject["memo"];
		var showMemoOrNot = rowObject["map"].showMemoOrNot;
		if(cellvalue==undefined){
			cellvalue="";
		}
		var data = "";
		if((showMemoOrNot=="true")&&(memo!=undefined)){
			if(memo!=""){
				data = cellvalue+"\n"+categName+"\n"+"Memo:"+memo;
			}
		}else{
			data = cellvalue+"\n"+categName;
		}
		return data;
	}
	//amount formatter
	ns.account.amountsFormatter = function(cellvalue, options, rowObject){
		var data=cellvalue;
		var compareData = rowObject['amountValue'].amountValue;
		if(compareData<0){
			data="<font color='red'>"+data+"</font>";
		}
		return data;
	}
	// account register Reconciled 
	ns.account.reconciledFormatter = function(cellvalue, options, rowObject){
		var data="";
		if(cellvalue==0){
			data ="<img src='/cb/web/multilang/grafx/account/i_in_process.gif' border='0' width='16' height='16' alt='Manual'>";
		}else if(cellvalue==1){
			data = "---";
		}else if(cellvalue==2){
			data = "<img src='/cb/web/multilang/grafx/account/gen-checkmark.gif' border='0' width='16' height='16' alt='Reconciled'>";
		}else if(cellvalue==3){
			data = "<img src='/cb/web/grafx/account/gen-checkmarkauto.gif' border='0' width='16' height='16' alt='Matched &amp; Reconciled'>";
		}else{
			data = "<img src='/cb/web/${ImgExt}grafx/account/gen-checkmarkautodisc.gif' border='0' width='16' height='16' alt='Matched &amp; Reconciled with Amount Discrepancy'>";
		}
		return data;
	}
	//account register image formatter
	ns.account.imageFormatter = function(cellvalue, options, rowObject){
        var data="";
        var transStatus = rowObject["statusValue"];
        var registerId = rowObject["ID"];
        if(cellvalue!=3){
        	data="---";
        }if(cellvalue==3){
        	if(transStatus ==1 ||
					transStatus ==2 ||
					transStatus ==3 ||
					transStatus ==4){
        		data="<a href='#' onclick=\"ns.account.imageCheckImage('REGTRANSACTION','"+registerId+"')\"><img height=10 alt=\"\" src=\"/cb/web/multilang/grafx/account/checkicon.gif\" width=24 border=0></a>";
        	}else{                                          
        		data="---";
        	}
        }
        return data;
	}
	//
	ns.account.imageCheckImage = function(module,transID){
		var urlString = "/cb/pages/jsp/account/SearchImageAction.action?module=" + module + "&transID=" + transID;
		$.ajax({
			url: urlString,
			success: function(data){
				$('#checkImageDialogID').html(data).dialog('open');
			}
		});
	}
	// future account register grid formatter
	ns.account.payeeNameCategoryFormatter = function(cellvalue, options, rowObject){
    	var categName = rowObject["map"].categoryName;
    	var memo = rowObject["memo"];
		var showMemoOrNot = rowObject["map"].showMemoOrNot;
		if(cellvalue==undefined){
			cellvalue="";
		}
		var data = "";
		if((showMemoOrNot=="true")&&(memo!=undefined)){
			if(memo!=""){
				data = cellvalue+"\n"+categName+"\n"+"Memo:"+memo;
			}
		}else{
			data = cellvalue+"\n"+categName;
		}
		return data;
    }
//Pending transaction data grid formatter
	ns.account.antiBalanceFormatter = function(cellvalue, options, rowObject){
    	var data = cellvalue;
    	var compData = rowObject['map'].amountValue;
    	if(compData<0){
    		data="<font color='red'>"+cellvalue+"</font>";
    	}
    	return data;
    }
    // amount formatter
	ns.account.amountFormatter = function(cellvalue, options, rowObject){
    	var data= cellvalue;
    	var compData = rowObject['amountValue'].amountValue;
    	if(compData<0){
    		data="<font color='red'>"+cellvalue+"</font>";
    	}
    	return data;
    }
    
	$(document).ready(function() {
		ns.account.setup1();
	});