	
    ns.account.setup = function(){

    function accountHistorySummaryCommon(gridID){   
		$("#" + gridID).data("isMemoAdded",false);
		ns.account.accountHistorySummaryEventDetail("#"+gridID);
		ns.common.resizeWidthOfGrids();
    }

	//define events for account history summary.
	$.subscribe('accountHistorySummaryEvent', function(event,data) {
		var gridID = data.id;
		accountHistorySummaryCommon(gridID);
		// setting grid title for Account History Search only.
		// for Transaction search, action will take care of setting title value.
		var isTransactionSearch = $("#isTransactionSearch").val();
		if(isTransactionSearch != "true"){
			if ($("#corporateAccountID option:selected").text() != "") {
				if(ns.common.isInitialized($('#accountHistoryPortletsContainer'),"ui-portlet")){
					$('#accountHistoryPortletsContainer').portlet('title', $("#corporateAccountID option:selected").text());
				}
			} else if ($("#consumerAccountsDropDown option:selected").text() != ""){
				if(ns.common.isInitialized($('#accountHistoryPortletsContainer'),"ui-portlet")){
					$('#accountHistoryPortletsContainer').portlet('title', $("#consumerAccountsDropDown option:selected").text());
				}
			}
		}
	});

	$.subscribe('accountHistoryAssetSummaryEvent', function(event,data) {
		var gridID = data.id;
		accountHistorySummaryCommon(gridID);
		var isTransactionSearch = $("#isTransactionSearch").val();
		if(isTransactionSearch != "true"){
			if ($("#corporateAccountID option:selected").text() != "") {
				$('#accountHistoryPortletsContainer').portlet('title', $("#corporateAccountID option:selected").text());	
			} else if ($("#consumerAccountsDropDown option:selected").text() != ""){
				$('#accountHistoryPortletsContainer').portlet('title', $("#consumerAccountsDropDown option:selected").text());
			}
		} 
	});
	$.subscribe('accountHistoryCcardSummaryEvent', function(event,data) {
		var gridID = data.id;
		accountHistorySummaryCommon(gridID);
	});
	$.subscribe('accountHistoryLoanSummaryEvent', function(event,data) {
		var gridID = data.id;
		accountHistorySummaryCommon(gridID);
	});
	$.subscribe('accountHistoryOtherSummaryEvent', function(event,data) {
		var gridID = data.id;
		accountHistorySummaryCommon(gridID);
	});

	//define events for account history summary.
	$.subscribe('savedSearchGridEvent', function(event,data) {
		ns.account.showCurrentSearchRow();
		$.publish("common.topics.tabifyDesktop");
		if(accessibility){
			accessibility.setFocusOnDashboard();
		}
	});

	//added a topic to call addShowMemoButton as direct call to function 
	//is not working after struts jQuery plugin upgrade
	$.subscribe('addShowMemoButtonEvent', function(event,data) {		
		ns.account.addShowMemoButton(data.id);
	});	
	
	// Add grid button for showing and hiding memo field
	ns.account.addShowMemoButton = function( gridId ){
		var buttonConfig = {
			id: gridId+'_completedShowMemo',
			caption: js_showMemo_btnCaption,
			title:js_showHideMemo_btnTitle,
			buttonicon: 'ui-icon-image',
			onClickButton: function(e) {
				var isMemoAdded = $("#" + gridId).data("isMemoAdded") || false;				
				if(!isMemoAdded){					
					ns.account.addMemoRowsForAccountHistory(gridId, gridId+"_memoRows");
				}
				var buttonLabel = $("#"+gridId+"_completedShowMemo"+" div");
				var buttonId = "#"+gridId+"_completedShowMemo";
				if($(buttonId).hasClass("selected")) {
					$(buttonId).removeClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_showMemo_btnCaption);
					$("tr[rowtype="+gridId+"_memoRows]").hide();
				} else {
					$(buttonId).addClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_hideMemo_btnCaption);
					$("tr[rowtype="+gridId+"_memoRows]").show();
				}
			}
		};
		ns.common.addGridButton("#"+gridId, buttonConfig);
	};


	// Add memo rows for account history.
	ns.account.addMemoRowsForAccountHistory = function(gridID, memoRowType) {
		var memoCells = $("td[aria-describedby=" + gridID +"_memo]");
		$.each(memoCells,function(i,memoCell){
			var memo = $(memoCell).html();
			if(memo){
				var parentRow = $(memoCell).parent();
				var className = parentRow.attr("class");
				$(parentRow).after('<tr rowType=\"'+memoRowType+'\"  class=\"' + className + '\"><td colspan="10"><div class="memo"><b>'+js_memoDescr_Start+':</b>' + memo + '</div></td></tr>');				
			};
	});
	
		$("#" + gridID).data("isMemoAdded",true);
	}



	//use to inquiry send message in account history inquiry pupup window.
	
	$.subscribe('sendMessageFormSuccess', function(event,data) {
		$.publish('refreshMessageSmallPanel');
		ns.common.closeDialog();
		//Display the result message on the status bar.
		ns.common.showStatus();
	});
	
	
	//Added by haitaoyu
	$.subscribe('consolidatedBalanceSummaryEvent',function(event,data){
		ns.account.customToConsolidatedBalanceColumn("#consolidatedBalanceSummaryID");
		ns.common.resizeWidthOfGrids();
	});
	
	$.subscribe('consolidatedBalanceDepositSummaryEvent',function(event,data){		
		ns.account.customAccountNicknameColumn("#consolidatedBalanceDepositSummaryID","deposit");		
		ns.account.writeFooterRowInfor("#consolidatedBalanceDepositSummaryID");
		ns.common.resizeWidthOfGrids();
	});
	
	$.subscribe('consolidatedBalanceAssetSummaryEvent',function(event,data){
		ns.account.customAccountNicknameColumn("#consolidatedBalanceAssetSummaryID","asset");		
		ns.account.writeFooterRowAsset("#consolidatedBalanceAssetSummaryID")
		ns.common.resizeWidthOfGrids();
	});
	
	$.subscribe('consolidatedBalanceLoanSummaryEvent',function(event,data){
		ns.account.customAccountNicknameColumn("#consolidatedBalanceLoanSummaryID","loan");
		ns.account.writeFooterRowLoan("#consolidatedBalanceLoanSummaryID");
		ns.common.resizeWidthOfGrids();
	});
	
	$.subscribe('consolidatedBalanceCcardSummaryEvent',function(event,data){
		ns.account.customAccountNicknameColumn("#consolidatedBalanceCcardSummaryID","ccard");
		ns.account.writeFooterRowCCard("#consolidatedBalanceCcardSummaryID");
		ns.common.resizeWidthOfGrids();
	});
	
	$.subscribe('consolidatedBalanceOtherSummaryEvent',function(event,data){
		ns.account.customAccountNicknameColumn("#consolidatedBalanceOtherSummaryID","other");
		ns.account.writeFooterRowOther("#consolidatedBalanceOtherSummaryID");
		ns.common.resizeWidthOfGrids();
	});
	
		//load the check image dialog
		var config = ns.common.dialogs["checkImage"];
		config.autoOpen = "false";
		ns.common.openDialog("checkImage");	
		
      };
    
      ns.account.customAccountNicknameColumn = function(gridID,type){
      	var gridIdAppendForColumn = gridID;
      	if (gridID.startsWith('#')){
      		gridIdAppendForColumn = gridID.substring(1);
      	}
      	/* if($('#accountNumberID_'+type).size() == 0) {
  			$('<div>Name</div>')
  			.attr('id', 'accountNumberID_'+type)
  			.attr('style','cursor:pointer')
  			.bind('click', function() {
  				$(gridID).jqGrid('setGridParam',{sortname:'NUMBER'});
  				if($('#accountNumberID_'+type).attr('order') == 'asc') {
  					$('span.ui-icon-desc',$('#accountNumberID_'+type)).removeClass("ui-state-disabled");
  					$('span.ui-icon-asc',$('#accountNumberID_'+type)).addClass("ui-state-disabled");
                      ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
  					$(gridID).jqGrid('setGridParam',{sortorder:'desc'});
  					$('#accountNumberID_'+type).attr('order','desc');
  				} else {
  					$('span.ui-icon-asc',$('#accountNumberID_'+type)).removeClass("ui-state-disabled");	
  					$('span.ui-icon-desc',$('#accountNumberID_'+type)).addClass("ui-state-disabled");
                      ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
                      $(gridID).jqGrid('setGridParam',{sortorder:'asc'});
  					$('#accountNumberID_'+type).attr('order','asc');
  				}
  				$('.s-ico').hide();
  				$(gridID).trigger('reloadGrid');
  			}).insertBefore($("#jqgh_"+gridIdAppendForColumn+"_nameNickName"+type).css("width", "auto"))
  			.append("<span class='s-ico' style='display:none'><span sort='asc' class='ui-grid-ico-sort ui-icon-asc ui-state-disabled ui-icon ui-icon-triangle-1-n ui-sort-ltr'></span><span sort='desc' class='ui-grid-ico-sort ui-icon-desc ui-state-disabled ui-icon ui-icon-triangle-1-s ui-sort-ltr'></span></span>");
  			$('span.ui-icon-asc',$('#accountNumberID_'+type)).removeClass("ui-state-disabled");	
  			$('span.ui-icon-desc',$('#accountNumberID_'+type)).addClass("ui-state-disabled");
              ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
  			$(gridID).jqGrid('setGridParam',{sortorder:'asc'});
  			$('#accountNumberID_'+type).attr('order','asc');
  			$('span.s-ico',$('#accountNumberID_'+type)).show();
  			$('span.s-ico',$("#jqgh_"+gridIdAppendForColumn+"_nameNickName"+type)).hide();
  			
  			ns.common.setFirstGridPage(gridID);//set grid page param to 1 to reload grid from first
  			$(gridID).jqGrid('setGridParam',{sortorder:'asc'});
  			$('#accountNumberID_'+type).attr('order','asc');
  			$('span.s-ico',$('#accountNumberID_'+type)).show();
  			$('span.s-ico',$("#jqgh_"+gridIdAppendForColumn+"_nameNickName"+type)).hide();
  		} else { */
  			$('span.s-ico',$("#jqgh_"+gridIdAppendForColumn+"account")).show();
			if($(gridID).jqGrid('getGridParam','sortname') == 'NUMBER') {
				$('span.s-ico',$("#jqgh_"+gridIdAppendForColumn+"account")).show();
			}
			if($(gridID).jqGrid('getGridParam','sortname') == 'ACCNICKNAME') {
				$('span.s-ico',$("#jqgh_"+gridIdAppendForColumn+"account")).hide();
				$('span.s-ico',$("#jqgh_"+gridIdAppendForColumn+"nameNickName"+type)).show();
			}
  		//}
      };
	
  
	//formater for account history summary list actions
	ns.account.formatAccountHistoryActionLinks = function(cellvalue, options, rowObject) { 
		//cellvalue is the ID of the transfer.
		
		var inquiry  = "<a title='" +js_inquiry+ "' href='javascript:void(0)' onClick='ns.account.inquirehistory(\""+ rowObject.map.InquireURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";
		var view = "<a title='" +js_view+ "' href='javascript:void(0)' onClick='ns.account.viewTransactionDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		
		if(rowObject.map.canInquire == "false") {
			inquiry ="";
		}
		
		return ' ' + view + ' ' + inquiry;
	}
	
	//use to view transaction details in transaction list
	ns.account.viewTransactionDetails = function( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewTransactionDetailDialogID').html(data).dialog('open');
			}
		});
	}
	
	//use to inquire history for every transaction.
    ns.account.inquirehistory = function( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewInquiryDialogID').html(data).dialog('open');
			}
		});
	}
	
	//use to get the transaction index in the transaction list.
	ns.account.accountHistorySummaryEventDetail = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		var data = $(gridID).jqGrid('getGridParam','userData');
		var gridIdAppendForColumn = gridID;

		if (ids.length > 0 || !data.noRecords){
			//$(gridID+"_pager").find("#last").hide();
			//$(gridID+"_pager").find(".ui-pg-input").attr("disabled","disabled");
		}else{	
			if(data){				
				//var className = "ui-widget-content jqgrow ui-row-ltr";
				$(gridID + " tbody").after('<tr><td colspan="9" align="left"><span class="acct_mgmt_font">' + data.noRecords + '</span></td></tr>');	
			}
			$(gridID+"_pager").hide();
		}
		//CR 699201, Disable the 'Page Number' input and 'Navigate to Last' button since we don't provide "jumping to a specific page" function.
		// CR 729800. Enable grid buttons to allow navigate-to-last and jump-to-page operations.
    	if (gridID.startsWith('#')){
    		gridIdAppendForColumn = gridID.substring(1);
    	}
	}
	

	
	//use to refresh account history summary for all account types.
	ns.account.refreshSummaryForm = function(urlString, gridID){
		$.ajax({     
		  url: urlString,     
		  success: function(data) {  
			  if(gridID != null) {
				  //clears the previous content of the summary div before appending the 
				  // results for the first grid. 
				  if(gridID=="accountHistoryGrid1") { 
					  //console.info("gridID="+gridID);
					  $("#accountHistorySummaryGrid").html("");
				  }
				  ns.common.setFirstGridPage(gridID);
					$(gridID).trigger("reloadGrid");
				  
				$(gridID).append(data);
				$("#accountHistorySummaryGrid").append(data);
			  } else {  
			 $("#accountHistorySummaryGrid").html(data);
		  }   
		  }
		});   
	}
	
	//use to load export part
	ns.account.loadExportDiv =function(urlString){
		$.ajax({    
		  url: urlString,     
		  success: function(data) {  
			 $("#export").html(data);
		  }   
		});   
	}
	
	ns.account.formatAmountColumn = function( cellvalue, options, rowObject ){
		var amount = "";
		if( cellvalue == ""){
			amount = "---";
		} else {
			amount = cellvalue;
		}
		return amount;
	}
	
	ns.account.formatRunningBalance= function(cellvalue, options, rowObject){
	if(cellvalue != null && cellvalue != undefined && cellvalue != ""){
		var value = cellvalue.trim();
		if(value.indexOf("-")==0){
			value = value.replace("-","");
			value="("+value+")";
		}
		return value;
	}
	if(cellvalue==undefined){
		cellvalue= "";
	}
	return cellvalue;
	}
	
	ns.account.formatBalanceValue = function( cellvalue, options, rowObject ){
		var balance = "";

		/*var s = new String(Math.abs(cellvalue));
        if(s.indexOf('.') < 0) { s += '.00'; }
        if(s.indexOf('.') == (s.length - 2)) { s += '0'; }*/
        var decimalVal = rowObject["runningBalanceValue"].amountValue;
		
		/*(if( decimalVal < 0){
			balance = "(" +cellvalue + ")";
		} else {
			balance = cellvalue + "&nbsp;";
		}*/
        alert(cellvalue);
        balance = cellvalue;
		return balance;   
	}

	//Formatter of Data Classification column.
	ns.account.formatDataClassificationColumn = function( cellvalue, options, rowObject ){
		dataClassification="";
		if(cellvalue=="I") {
			dataClassification = "<img src='/cb/web/multilang/grafx/account/i_in_process.gif' border='0' width='12' height='12' align='absmiddle' title=\"In-Process\" alt=\"In-Process\">";
		} else {
			dataClassification = "<img src=\"/cb/web/multilang/grafx/account/gen-checkmark.gif\" border=\"0\" width=\"12\" height=\"12\" align=\"absmiddle\" title=\"Cleared\" alt=\"Cleared\">";
		}
		return dataClassification;
	}
	
	//Formatter of reference number column.
	ns.account.formatReferenceNumberColumn = function( cellvalue, options, rowObject ){
		var referenceNumber = "";
		if(cellvalue==""||cellvalue==null){
			referenceNumber = "---";
		}else{
			referenceNumber = cellvalue;
		}
		
		if( rowObject["typeValue"] == "3"){//Show View Check Image link if type is "Check"
			referenceNumber = "<a href=\"javascript:void(0)\" onClick=\"ns.account.viewCheckImage('"+rowObject["map"].LinkURL+"')\" >"+referenceNumber+"</a>";
		}
		return referenceNumber;
	};
	
	//Formatter of description number column.
	ns.account.formatDescriptionNumberColumn = function( cellvalue, options, rowObject ){
		var referenceNumber = "";
		if(cellvalue==""||cellvalue==null){
			referenceNumber = "---";
		}else{
			referenceNumber = cellvalue;
		}
		
		if( rowObject["typeValue"] == "3"){//Show View Check Image link if type is "Check"
			var res = referenceNumber.split("|");
			//res = res[1];
			referenceNumber = "<a href=\"javascript:void(0)\" onClick=\"ns.account.viewCheckImage('"+'/cb/pages/jsp/account/transactionCheckImage.jsp?module=TRANSACTION&tranIndex='+res[1]+"')\" >"+res[0]+"</a>";
		}
		return referenceNumber;
	};
	
	//Formatter for Consumer reference number column.
	ns.account.formatConsumerReferenceNumberColumn = function( cellvalue, options, rowObject ){
	var referenceNumber = cellvalue;
	if( rowObject["typeValue"] == "3"){
			referenceNumber = "<a href=\"javascript:void(0)\" onClick=\"ns.account.viewCheckImage('"+rowObject["map"].LinkURL+"')\" >"+cellvalue+"</a>";
		}
		return referenceNumber;
	};
	

	ns.account.viewCheckImage = function( urlString ){
	
		//var urlString = "/cb/pages/jsp/account/SearchImageAction?module=" + module + "&transID=" + transID;
	
		$.ajax({
			url: urlString,
			success: function(data){
				$('#checkImageDialogID').html(data).dialog('open');
			}
		});
	}
	
	ns.account.zoomImage = function(urlString){
		$.ajax({    
			url: urlString,     
			success: function(data) {  
			ns.common.closeDialog();
			$("#checkImageDialogID").html(data).dialog('open');
			}   
		});   
	}
	
	ns.account.rotationFunction = function(urlString){
		$.ajax({    
			url: urlString,     
			success: function(data) {  
			ns.common.closeDialog();
			$("#checkImageDialogID").html(data).dialog('open');
			}   
		});   
	}
	
	//for consolidated balance summary grid only
	ns.account.customToConsolidatedBalanceColumn =function(gridID){
		
		//var test = rowObject['map'].ViewURL;
		//alert(test);
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		var data = $(gridID).jqGrid('getGridParam','userData');

		if (ids.length > 0 || !data.noRecords) {
			var totalValue='';
			var currencyCode='';
			var linkVar='';
			var totalAmountValue = 0;
			for(var i=0;i<ids.length;i++){  
				var rowId = ids[i];
				var accountsType = $(gridID).jqGrid('getCell', rowId, 'accountsType');
				var accountGroupID = $(gridID).jqGrid('getCell', rowId, 'accountGroupID');
				var dataClassfication = $(gridID).jqGrid('getCell', rowId, 'dataClassfication');
				var linkVar = $(gridID).jqGrid('getCell', rowId, 'map.ViewURL');
				currencyCode = $(gridID).jqGrid('getCell', rowId, 'currencyCode');
				totalValue = $(gridID).jqGrid('getCell', rowId, 'totalValue');
				totalAmountValue = $(gridID).jqGrid('getCell', rowId, 'totalAmountValue');
				var link = "<a href=\"javascript:void(0)\" onClick=\"ns.account.redirectAccountDetail('"+linkVar+"');\">"+accountsType+"</a>";
				$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_accountsType']:eq("+i + ")").html(link);
			}
			if(totalAmountValue<0){
				totalValue = "<font color='red'>"+totalValue+"</font>"
			}
			$("td[aria-describedby='" + gridID.substr(1, gridID.length-1) + "_balance']:eq("+ids.length + ")").html(js_total+"("+currencyCode+"):"+totalValue);
		}else{	
			if(data) {				
				//var className = "ui-widget-content jqgrow ui-row-ltr";
				$(gridID + " tbody").after('<tr><td colspan="9" align="left"> <span class="acct_mgmt_font">' + data.noRecords + '</span></td></tr>');	
			}
			//Hide footer row
			$('#gview_' + gridID.replace('#', '') + " .ui-jqgrid-ftable").hide();
			$(gridID+"_pager").hide();
		}
	}
	
	// called by customToConsolidatedBalanceColumn()
	ns.account.redirectAccountDetail = function(urlString){
		//var urlString = "/cb/pages/jsp/accountsbygroup.jsp?AccountGroupID="+accountGroupID+"&AccountBalancesDataClassification="+dataClassfication;
		$.ajax({
			url: urlString,	
			success: function(data){
				$('#querybord').html(data);
				$('#consolidatedBalanceSummary').html('');
		   }
		});
	}
  
	//Customs to account nick name column for template
	ns.account.customToAccountNickNameColumn = function( cellvalue, options, rowObject ){			
		var _toAccountNickName = rowObject["account"].nickName;
		var _toAccountCurrencyCode = rowObject["account"].currencyCode;
		var _acountRoutingNum = rowObject["account"].routingNum;
		var _dataClassfication =  rowObject["dataClassification"];
		var linkVar = rowObject["map"].LinkURL;
		var _toAccountName = _acountRoutingNum;
		if( _toAccountNickName != undefined && _toAccountNickName != "" ){
			_toAccountName = _toAccountName + " : " + _toAccountNickName;
		}
		
		if( _toAccountCurrencyCode != undefined && _toAccountCurrencyCode != "" ){
			_toAccountName = _toAccountName + " - " + _toAccountCurrencyCode;
		}
		return _toAccountName;		
	}
	
	/**Format Account column for cash flow grid*/
	ns.account.formatAccountColumn = function(cellvalue, options, rowObject){
		/*var _toAccountName = "";
		var _toAccountDisplayText = rowObject["account"].displayText;
		if( _toAccountDisplayText != undefined && _toAccountDisplayText != "" ){
			_toAccountName =  ""+_toAccountDisplayText;
		} */
		var link = "<a href=\"javascript:void(0)\" onClick=\"ns.account.redirectHistory('"+rowObject["map"].LinkURL+"');\" >"+cellvalue+"</a>";
		return link;	 
	}
	
	ns.account.consolidatedBalanceFormatter =function(cellvalue, options, rowObject){
		var data = cellvalue;
		var compValue= rowObject['balanceAmountValue'];
		if(compValue<0){
           data = "<font color='red'>"+cellvalue+"</font>";			
		}
		return data;
	}
	// customs the opening ledger column for deposit grid
	ns.account.openingLedgerDepositFormatter = function(cellvalue, options, rowObject){
		var openingLedger ="";
		var compOpeningLeger = rowObject['displayOpeningLedgerAmountValuel'];
		if(compOpeningLeger==undefined){
			compOpeningLeger= 0.00;
		}
		if(cellvalue==""||cellvalue==null){
			openingLedger="---";
		}else{
			if(compOpeningLeger<0){
				cellvalue="<font color='red'>"+cellvalue+"</font>";
			}
			return cellvalue;
		}
		return openingLedger;
	}
	//format the closing ledger column for deposit grid
	ns.account.closingLedgerDepositeGridFormatter = function(cellvalue, options, rowObject){
		var closingLeger = "";
		var compClosingLedger ;
		if(rowObject["account"] != null && rowObject["account"] != undefined && rowObject["account"].displayClosingBalance != null && rowObject["account"].displayClosingBalance != undefined && rowObject["account"].displayClosingBalance.amountValue != null && rowObject["account"].displayClosingBalance.amountValue != undefined){
		 compClosingLedger = rowObject["account"].displayClosingBalance.amountValue.amountValue;
		 }
		
		
		if(compClosingLedger==undefined){
			compClosingLedger= 0.00;
		}
		if(cellvalue == ""||cellvalue==null){
			closingLeger="---";
		}else{
		
			if(compClosingLedger<0){
				cellvalue = "<font color='red'>"+cellvalue+"</font>";
			}
		  return cellvalue;
		}
		return closingLeger;
	}
		
	//format the current ledger column
	ns.account.currentLedgerDepositeFormatter = function(cellvalue, options, rowObject){
		var currentLedger="";
		var compData = rowObject['displayIntradayCurrentBalanceAmount'];
		if(compData==undefined){
			compData = 0.00;
		}
		if(cellvalue==""||cellvalue==null){
			currentLedger="---";
		}else{
			if(compData<0){
				cellvalue = "<font color='red'>"+cellvalue+"</font>";
			}
			return cellvalue;
		}
		return currentLedger;
	}
	//Redirect to AccountHistory
	ns.account.redirectHistory = function(urlString){
		//deposit and other Account request the same java server page
		// var urlString = "/cb/pages/jsp/account/index.jsp?AccountGroupID="+accountGroupID+"&SetAccount.ID="+acountID+"&SetAccount.BankID="+accountBankID+"&SetAccount.RoutingNum="+acountRoutingNum+"&ProcessAccount=true&GetPagedTransactions.Page=first&StartDate=&EndDate=&ProcessTransactions=true&GetPagedTransactions.DataClassification="+dataClassfication+"&redirection=true";
		urlString = urlString+"&CSRF_TOKEN="+ns.home.getSessionTokenVarForGlobalMessage;
		$.ajax({
			   url: urlString,
			   type: "POST",
			   success: function(data){
			   $("#notes").html(data);
				// set Account History menu as "Active"
				// remove current 2nd-level menu ui-state-active class
				$(".active").children(".ui-state-active").removeClass("ui-state-active").removeClass("ui-corner-all");
				// add ui-state-active class to account history menu 
				$("li[menuId='acctMgmt_history']").addClass("ui-state-active").addClass("ui-corner-all");

				// set page title
				var menuId = "acctMgmt";
				var menuName = $("li[menuId='acctMgmt'] a").eq(0).html();
				var submenuId = "acctMgmt_history";
				var submenuName = $("li[menuId='acctMgmt_history'] a").eq(0).html();
				ns.home.updateBreadcrumb(menuName, submenuName, menuId);
			   
				//Add browser history entry. This is added for back and forward button support.
				if(window.parent && window.parent.addHistoryEntry){
				   //add menu id entry in history
				   window.parent.addHistoryEntry(submenuId);
				   // set window title
				   window.parent.changeWindowTitle(submenuName);
				}
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
				   accessibility.setFocusOnDashboard();
		   }
		   }
		   });
	}

	//formatter the bankName column
	ns.account.nameFormatter = function(cellvalue, options, rowObject){
		var bankName = "";
		if(cellvalue == ""||cellvalue==null){
			cellvalue="---";
		}else{
			return cellvalue;
		}
		return bankName;
	}
	//formatter the dateTime
	ns.account.dateTimeFormatter = function( cellvalue, options, rowObject ){
		var date = "";
		if(cellvalue == "" || cellvalue == null){
			date = "---";
		} else {
			date = cellvalue.substr(0,10);
		}
		return date;
	}
	// customs the opening ledger column
	ns.account.openingLedgerFormatter = function(cellvalue, options, rowObject){
		var openingLedger ="";
		if(cellvalue==""||cellvalue==null){
			openingLedger="---";
		}else{
			return cellvalue;
		}
		return openingLedger;
	}
	//format the totalDebits
	ns.account.totalDebitsFormatter = function(cellvalue, options, rowObject){  
	var totalDebits = "";
		if(cellvalue=="-1"||cellvalue==""||cellvalue==null){
			totalDebits="---";
		}else{
		 var displayTotalDebits =  "0.00";
		 if(rowObject['totlaDebitsForSorting'] != null && rowObject['totlaDebitsForSorting'] != undefined){
		displayTotalDebits = rowObject['totlaDebitsForSorting'].amountValue;
		}
			if(displayTotalDebits <0){
			totalDebits="<font color='red'>"+cellvalue+"</font>";
			}else{
			totalDebits=cellvalue;
			}
		}
		 return totalDebits;
	}
	//format the totalCredits
	ns.account.totalCreditsFormatter = function(cellvalue, options, rowObject){
		var totalCredits = "";
		if(cellvalue=="-1"||cellvalue==""||cellvalue==null){
			totalCredits="---";
		}else{
		 var displayTotalCredits = "0.00";
		if(rowObject['totlaCreditsForSorting'] != null && rowObject['totlaCreditsForSorting'] != undefined){
		displayTotalCredits = rowObject['totlaCreditsForSorting'].amountValue;
		}
		if(displayTotalCredits<0){
			totalCredits="<font color='red'>"+cellvalue+"</font>";
		}else{
			totalCredits=cellvalue;
		}
		}
		return totalCredits;
	}
	//format the closing ledger column
	ns.account.closingLedgerFormatter = function(cellvalue, options, rowObject){
		var closingLeger = "";
		if(cellvalue == ""||cellvalue==null){
			closingLeger="---";
		}else{
			closingLeger = cellvalue;
		}
		return closingLeger;
	}
	//format the current ledger column
	ns.account.currentLedgerFormatter = function(cellvalue, options, rowObject){
	
		var currentLedger="";
		if(cellvalue==""||cellvalue==null){
			currentLedger="---";
		}else{
		var amt = 0.00;
		if(options!=null && options!= undefined && options.colModel!=null&& options.colModel!=undefined){
			if(options.colModel.index == 'bookValueCurrency'){
				if(rowObject['bookValueCurrency']!=null && rowObject['bookValueCurrency']!=undefined){
					 amt = rowObject['bookValueCurrency'].amountValue;
				}
			}else if(options.colModel.index == 'marketValueCurrency'){
				if(rowObject['marketValueCurrency']!=null && rowObject['marketValueCurrency']!=undefined){
					 amt = rowObject['marketValueCurrency'].amountValue;
				}
			}else if(options.colModel.index == 'AVAIL_CREDIT'){
			if(rowObject['displayAvailCredit']!=null && rowObject['displayAvailCredit']!=undefined){
					 amt = rowObject['displayAvailCredit'].amountValue;
				}
			}else if(options.colModel.index == 'AMT_DUE'){
			if(rowObject['displayAmtDue']!=null && rowObject['displayAmtDue']!=undefined){
					 amt = rowObject['displayAmtDue'].amountValue;
				}
			}else if(options.colModel.index == 'CLOSINGBALANCE'){
			if(rowObject['displayCurrentBalance']!=null && rowObject['displayCurrentBalance']!=undefined){
					 amt = rowObject['displayCurrentBalance'].amountValue;
				}
			}else if(options.colModel.index == 'displayIntradayCurrentBalance.amountValue.currencyStringNoSymbol'){
			if(rowObject['displayClosingOrIntralBalanceCurrency']!=null && rowObject['displayClosingOrIntralBalanceCurrency']!=undefined){
					 amt = rowObject['displayClosingOrIntralBalanceCurrency'].amountValue;
				}
			}
			
		}

		if(amt<0){
			cellvalue="<font color='red'>"+cellvalue+"</font>";
		}
			return cellvalue;
		}
		return currentLedger;
	}
	
	// the formatter for other summary of displayTotalDebits
	ns.account.dispTotalDebitOrCredits = function(cellvalue, options, rowObject){
		var debits ="";
		if(cellvalue==""||cellvalue==null){
			debits="---";
		}else{
			return cellvalue;
		}
		return debits;
	}

	//Write the footer row information for deposit account grid 
	ns.account.writeFooterRowInfor = function(gridID){	
	var data = $(gridID).jqGrid('getGridParam','userData');
		if(data) {	
			var opeColor = data.displayOpeningLedgerColor;
			var tempOpenLedger = data.displayOpeningLedger;
			var closingColor = data.displayClosingOrIntralBalanColor;
			var closingBal = data.displayClosingOrIntralBalan;
			var debits = data.totalDebitsTotal;
			var credits = data.totalCreditsTotal;
			var creditsTotalValue = data.totalCreditsTotalValue;
			var debitsTotalValue = data.totalDebitsTotalValue;
			var opeLedger = data.displayOpeningLedger;
			var totalDisplay ="";
			
			if(tempOpenLedger!=undefined && tempOpenLedger!=""&&tempOpenLedger!=null){
				opeLedger = tempOpenLedger.substring(tempOpenLedger.indexOf(") ")+1);
				totalDisplay = tempOpenLedger.substring(0, tempOpenLedger.indexOf(") ")+1);
			}
			if (closingBal==undefined || closingBal==""||closingBal==null){
				closingBal = 0.00;
			}
			 
			if (debits==undefined  || debits==""|| debits==null){
				debits = 0.00;
			}
			 
			if (credits==undefined  || credits==""|| credits==null){
				credits = 0.00;
			}	
			
			if ( opeColor==""|| opeColor==null){
				opeColor = "BLACK";
			}
			
			if (closingColor==""|| closingColor==null){
				closingColor = "BLACK";
			}
			if (creditsTotalValue==undefined  || creditsTotalValue==""|| creditsTotalValue==null){
				creditsTotalValue = 0.00;
			}
			if (debitsTotalValue==undefined  || debitsTotalValue==""|| debitsTotalValue==null){
				debitsTotalValue = 0.00;
			}
			if(creditsTotalValue<0){
			credits = "<font color='red'>"+credits+"</font>";
			}
			if(debitsTotalValue<0){
			debits = "<font color='red'>"+debits+"</font>";
			}
			var openLedgerHTML = totalDisplay+'<span style="color:' + opeColor+ '" encode="false">' + opeLedger + '</span>';
			var closeLedgerHTML = '<span style="color:' +closingColor+'" encode="false">' + closingBal + '</span>';
			$(gridID).jqGrid('footerData','set',{'displayOpeningLedger': openLedgerHTML, 'totalDebits': debits, 'totalCredits': credits, 'displayClosingAmountValue': closeLedgerHTML}, false);
		}		
	}
	
	//write the rooter row for asset grid
	ns.account.writeFooterRowAsset = function(gridID){
		var data = $(gridID).jqGrid('getGridParam','userData');
		if(data) {	
		var temp = data.bookValueTotal;
		var endPart=data.bookValueTotal;
		var totalDisplay="";
		if(temp!=null && temp!=undefined && temp!=""){		
		endPart = temp.substring(temp.indexOf(") ")+1);
		totalDisplay = temp.substring(0, temp.indexOf(") ")+1);
		}
		var bookValueTotalHTML=totalDisplay+'<span style="color:' +data.bookValueTotalColor+'" encode="false">' + endPart+ '</span>';
		var marketValueTotalHTML='<span style="color:' +data.marketValueTotalColor+'" encode="false">' + data.marketValueTotal + '</span>';
		
			$(gridID).jqGrid('footerData','set',{'bookValue': bookValueTotalHTML, 'marketValue': marketValueTotalHTML}, false);
		}
	}
	
	//write the footer row for loan grid
	ns.account.writeFooterRowLoan = function(gridID){
		var data = $(gridID).jqGrid('getGridParam','userData');
		if(data) {			
		var temp = data.loanAvailCreditTotal;
		var endPart=data.loanAvailCreditTotal;
		var totalDisplay="";
		if(temp!=null && temp!=undefined && temp!=""){		
		endPart = temp.substring(temp.indexOf(") ")+1);
		totalDisplay = temp.substring(0, temp.indexOf(") ")+1);
		}
		
		var loanAvailCreditTotalHTML=totalDisplay+'<span style="color:' +data.availCreditTotalColor+'" encode="false">' + endPart+ '</span>';
		var amountDueTotalHTML='<span style="color:' +data.amtDueTotalColor+'" encode="false">' + data.amountDueTotal + '</span>';
		var balanceTotalHTML='<span style="color:' +data.balanceColor+'" encode="false">' + data.balanceTotal + '</span>';
			$(gridID).jqGrid('footerData','set',{'availableCredit': loanAvailCreditTotalHTML, 'amountDue': amountDueTotalHTML, 'balance': balanceTotalHTML}, false);
		}		
	}
	
	//write the footer row for ccard grid
	ns.account.writeFooterRowCCard = function(gridID){
		var data = $(gridID).jqGrid('getGridParam','userData');
		
		if(data) {	
		var temp = data.loanAvailCreditTotal;
		var endPart=data.loanAvailCreditTotal;
		var totalDisplay="";
		if(temp!=null && temp!=undefined && temp!=""){		
		endPart = temp.substring(temp.indexOf(") ")+1);
		totalDisplay = temp.substring(0, temp.indexOf(") ")+1);
		}
		
		var loanAvailCreditTotalHTML=totalDisplay+'<span style="color:' +data.availCreditTotalColor+'" encode="false">' + endPart+ '</span>';
		var amountDueTotalHTML='<span style="color:' +data.amtDueTotalColor+'" encode="false">' + data.amountDueTotal + '</span>';
		var balanceTotalHTML='<span style="color:' +data.balanceColor+'" encode="false">' + data.balanceTotal + '</span>';
			$(gridID).jqGrid('footerData','set',{'availableCredit': loanAvailCreditTotalHTML, 'amountDue': amountDueTotalHTML, 'balance': balanceTotalHTML}, false);
		}
	}
	
	//write the footer row for other grid
	ns.account.writeFooterRowOther = function(gridID){
		var data = $(gridID).jqGrid('getGridParam','userData');
		if(data) {	
			var temp = data.displayOpeningLedger;
			var endPart=data.displayOpeningLedger;
			var totalDisplay="";
			if(temp!=null && temp!=undefined && temp!=""){		
			endPart = temp.substring(temp.indexOf(") ")+1);
			totalDisplay = temp.substring(0, temp.indexOf(") ")+1);
			}
					
			var openLedgerHTML = totalDisplay+'<span style="color:' + data.displayOpeningLedgerColor + '" encode="false">' + endPart + '</span>';
			var closeLedgerHTML = '<span style="color:' + data.displayClosingOrIntralBalanColor + '" encode="false">' + data.displayClosingOrIntralBalan + '</span>';
			var totalDebitsTotalHTML = '<span style="color:' + data.totalDebitsTotalColor + '" encode="false">' + data.totalDebitsTotal + '</span>';
			var totalCreditsTotalHTML = '<span style="color:' + data.totalCreditsTotalColor + '" encode="false">' + data.totalCreditsTotal + '</span>';
			$(gridID).jqGrid('footerData','set',{'map.displayOpeningLedger': openLedgerHTML, 'map.totalDebits': totalDebitsTotalHTML, 'map.totalCredits': totalCreditsTotalHTML, 'displayClosingOrIntralBalance': closeLedgerHTML}, false);
		}
	}
	
	$(document).ready(function() {
		ns.account.setup();
	});


$.subscribe('errorSaveSearchCriteria', function(event, data) {
	ns.common.closeDialog('#saveSearchDialog');
});

$.subscribe('beforeSaveSearchCriteria', function(event, data) {
	removeValidationErrors();
	ns.account.newReportName = $('#addReport #name').val();
});

$.subscribe('completeSaveSearchCriteria', function(event, data) {
});

$.subscribe('successSaveSearchCriteria', function(event, data) {
	ns.common.showStatus();
	ns.common.closeDialog('#saveSearchDialog');
	ns.account.reloadSavedSearch();
});

$.subscribe('removeErrorsOnBegin', function(event, data) {
	$('.errorLabel').html('').removeClass('errorLabel');
	$('#formerrors').html('');
});

// Saved Search Grid script
$.subscribe('savedSearchGridCompleteEvents', function(event,data) {
	//handleGridEventsAndSetWidth("#savedReportsGridId", "#savedReportsDiv");
});

ns.account.handleGridEventsAndSetWidth = function(gridID,tabsID){
	//Adjust the width of grid automatically.
	ns.common.resizeWidthOfGrids();
};

ns.account.formatSavedReportsActionLinks = function(cellvalue, options, rowObject) {
	var edit = "<a title='" +js_edit+ "' href='javascript:void(0)' onClick='ns.account.editSearch(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
	var del  = "<a title='" +js_delete+ "' href='javascript:void(0)' onClick='ns.account.deleteSearch(\"" + rowObject.map.DeleteURL + "\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-trash'></span></a>";
	if(rowObject.map.isCurrentReport=='true'){
		edit='';
		del='';
	}
	return ' ' + edit + ' '  + del ;
};


ns.account.formatReportNameLink = function(cellvalue, options, rowObject) {
	//cellvalue is the name of the report.
	var link = "<a title='" +js_generate+ "' href='javascript:void(0)' onClick='ns.account.loadSearch(\""+ rowObject.map.ViewURL +"\");'>" + cellvalue + "</a>";
	return ' ' + link;
};

ns.account.showCurrentSearchRow = function() {
	// get rowId, aim to get number of rows
	var ids = $("#savedSearchGridId").jqGrid('getDataIDs');
	// operation each row
	for(var i=0;i<ids.length;i++){
		var rowId = ids[i];
		var currentID = "#" + rowId;
		var isCurrentReport = $("#savedSearchGridId").jqGrid('getCell', rowId, 'isCurrentReport');
		if(isCurrentReport == 'true') {
			var lastCellInGrid = $(currentID);
			lastCellInGrid.addClass("ui-state-active");
		}
	}
};

/**
 * Click Save Report on report criteria page
 */
ns.account.saveSearch = function() {

	$.ajax({
		url:  "/cb/pages/jsp/account/inc/saveTransactionSearch.jsp",
		data: $('#accountCriteriaForm').serialize(),
		type: 'post',
		success: function(data){
//			if($('#saveSearchDialog').length > 1) {
//				ns.common.destroyDialog('saveSearchDialog');
//			}
//			if ($("#addTransactionSearchForm")){
//				$('#addTransactionSearchForm').empty().remove();
//			}
			$('#saveSearchDialog').html(data).dialog('open');
		}
	});

}; // end ns.account.saveSearch()

/**
 * Click Save Report on report criteria page for existing report
 */
ns.account.modifySearch = function() {
	$.ajax({
		url:  "/cb/pages/jsp/account/inc/account-history-edit-search.jsp",
		data: $('#accountCriteriaForm').serialize(),
		type: 'post',
		success: function(data){
			ns.account.updateSearch();
		}
	});
};


/**
 * Click Save Report on report criteria page for existing report
 */
ns.account.updateSearch = function() {
	$.ajax({
		url:  "/cb/pages/jsp/account/ModifyTransactionSearchAction.action",
		data: $('#accountCriteriaForm').serialize(),
		type: 'post',
		success: function(data){
			$('#resultmessage').html(data);
			ns.common.showStatus();
			ns.account.reloadSavedSearch();
		}
	});
};


/**
 * Click on report name on saved reports page
 */
ns.account.loadSearch = function(urlString) {
	$.ajax({
		  url: urlString,
		  success: function(data) {
			$('#appdashboard').empty();
			$('#appdashboard').html(data);
		}
	});
};

/**
 * Click Edit on saved report list page
 */
ns.account.editSearch = function(urlString) {
	$.ajax({
		url: urlString,
		success: function(data) {
			$('#appdashboard').empty();
			$('#appdashboard').html(data);
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
				accessibility.setFocusOnDashboard();
			}
		}
	});
};

/**
 * Click Delete on saved report list page
 */
ns.account.deleteSearch = function(urlString) {

	$.ajax({
		url: urlString,
		success: function(data) {
//			if($('#deleteSaveSearchDialog').length > 1) {
//				ns.common.destroyDialog('deleteSaveSearchDialog');
//			}
//			if ($("#deleteSavedSearchForm")){
//				$('#deleteSavedSearchForm').empty().remove();
//			}
			$('#deleteSaveSearchDialog').html(data).dialog('open');
		  }
	});
};

ns.account.reloadSavedSearch = function() {
	var urlString = "/cb/pages/jsp/account/inc/account_saveSearch.jsp";
	$.ajax({
		url: urlString,
		success: function(data) {
			$('#savedReportsDiv').html(data);
		}
	});
};

/**
 * Delete Search
 */
$.subscribe('beforeDeleteSavedSearch', function(event,data) {
	removeValidationErrors();
});

$.subscribe('successDeleteSavedSearch', function(event,data) {
	ns.common.showStatus();
	ns.common.closeDialog('#saveSearchDialog');
	ns.common.closeDialog('#deleteSaveSearchDialog');
	ns.account.reloadSavedSearch();
	ns.shortcut.goToMenu("acctMgmt_search"); 
	ns.common.isTrxnSearchDeleteConfirmed = true;
});

$.subscribe('errorDeleteSavedSearch', function(event,data) {
	ns.common.closeDialog('#deleteSaveSearchDialog');
});

$.subscribe('completeDeleteSavedSearch', function(event,data) {
});

//clears the 2 text fields
ns.account.clearDateTextFields = function() {
	$("#startDate").val('');
	$("#endDate").val('');
	if ($("#dateRangeValue").val() == null || $("#dateRangeValue").val() == "") {
		$("#startDate").val($("#StartDateSessionValue").val());
		$("#endDate").val($("#EndDateSessionValue").val());
	}
};

//resets the selected index of the given drop down to 0
ns.account.clearDropDown = function() {	
	$("#dateRangeValue").selectmenu('value', '');	
};

ns.account.reloadAccount = function() {
	//CR#774917;Fixed
//	document.FormName.elements['ResetTSZBADisplay'].value = 'true';
};

//reloads Account drop-down dependent on Account Group 
ns.account.reloadAccountType = function() {
	document.FormName.elements['ResetTSZBADisplay'].value = 'true';
	var isTransactionSearch = $("#isTransactionSearch").val();
	if(isTransactionSearch=="true"){
		var urlString = "/cb/pages/jsp/account/GetAccountsFromGroupAction.action";
		$.ajax({
			type: "POST",
			url:  urlString,
			data: $('#accountCriteriaForm').serialize(),
			success: function(data) {
				var accountDropdownID = "";
					accountDropdownID = "#corporateAccountMultiSelectDropDown";
					$(accountDropdownID).get(0).options.length = 0;
					$.each(data.accountList, function(index, item) {
						$(accountDropdownID).get(0).options[$(accountDropdownID).get(0).options.length] = new Option(item.value, item.key);
					});
					$($(accountDropdownID+' option').get(0)).attr('selected', 'selected');
					$(accountDropdownID).extmultiselect('refresh');
			},
			error: function() {
				alert("Failed to load accounts");
			}
		});
	}else{
		var accountGrpID = $("#accountGroupID").val() || "";
		$("#corporateAccountID").lookupbox("option","source","/cb/pages/jsp/account/AccountHistoryLookupBoxAction.action?accountGroupId="+accountGrpID);
		$("#corporateAccountID").lookupbox("refresh");
		/* accountDropdownID = "#corporateAccountID";
		 $(accountDropdownID).get(0).options.length = 0;
			$.each(data.accountList, function(index, item) {
				$(accountDropdownID).get(0).options[$(accountDropdownID).get(0).options.length] = new Option(item.value, item.key);
			});
			$($(accountDropdownID+' option').get(0)).attr('selected', 'selected');
			$(accountDropdownID+"_autoComplete").val($(accountDropdownID).get(0).options[0].text);
			*/
	}
};

//opens export dialog
ns.account.exportAccountHistory = function(urlString){
	$.ajax({
		url: urlString,
		type: 'post',
		data: $("#accountCriteriaForm").serialize(),
		success: function(data){
			if($("#formExportHistory")){
				$("#formExportHistory").empty().remove();//to resolve issue occuring due to multiple export popups
			}
			$('#accountsHistoryExportDialogID').html(data).dialog('open');
		}
	});
};

/** Function to override date ranges as per account specific requirements.*/
ns.account.overrideDateRanges = function() { 
	/** Default Ranges. */
	var defaultRanges = $.fn.extdaterangepicker.defaults.RANGES; 
	var mergedRanges= {};
	
	/** Ranges to override . */
	var overiddenCurrentMonth = {key:"CURRENT_MONTH",text: 'Current Month', dateStart: function(){return Date.parse('today').moveToFirstDayOfMonth();}, dateEnd: 'today'};
	var overiddenCurrentWeek  = {key:"CURRENT_WEEK",text: 'Current Week', dateStart: function(){return Date.today().moveToDayOfWeek(0,-1).addDays(1)}, dateEnd: 'today'};
	
	/** Override Values.*/ 
	mergedRanges = [defaultRanges.TODAY,defaultRanges.YESTERDAY,overiddenCurrentWeek,defaultRanges.LAST_WEEK,defaultRanges.LAST_7_DAYS,defaultRanges.LAST_30_DAYS,defaultRanges.LAST_60_DAYS,defaultRanges.LAST_90_DAYS,overiddenCurrentMonth,defaultRanges.LAST_MONTH];
	
	return mergedRanges;
}


ns.account.resetOperationFlag = function() {
	$('input[id=operationFlag]').val("");
};

ns.account.setOperationFlag = function(operation) {
	$('input[id=operationFlag]').val(operation);
};

$.subscribe('beforeAccountHistorySearch', function(event, data) {
	// $.log('About to verify form! ');
	removeValidationErrors();
});
$.subscribe('confirmAccountHistorySearch', function(event, data) {
	// $.log('Form Verified! ');
});
$.subscribe('errorAccountHistorySearch', function(event, data) {
	// $.log('Form Verifying error! ');
});
$.subscribe('successAccountHistorySearch', function(event, data) {
	//var isTransactionSearch = $("#isTransactionSearch").val();
	ns.common.setFirstGridPage('#accountHistorySummaryID_0');	
});

// New topics fro transaction search.
$.subscribe('beforeTransactionSearch', function(event, data) {
	// $.log('About to verify form! ');
	removeValidationErrors();
});
$.subscribe('confirmTransactionSearch', function(event, data) {
	// $.log('Form Verified! ');
});
$.subscribe('errorTransactionSearch', function(event, data) {
	// $.log('Form Verifying error! ');
});
$.subscribe('successTransactionSearch', function(event, data) {
	ns.common.setFirstGridPage('#accountHistorySummaryID_0');
	$('#accountHistorySummaryID_0').trigger("reloadGrid");
	if(ns.common.isTrxnSearchDeleteConfirmed == true){
		$('.acntDashboard_masterItemHolder').slideToggle();
		ns.common.isTrxnSearchDeleteConfirmed = false;
	}
});
//prints account history / Transaction search check image dialog. 
ns.account.printImage = function() {
	$("#checkImageDialogID div[containerType=image]").print();
}

//Export History
$.subscribe('beforeExportHistory', function(event, data) {
	// $.log('About to verify form! ');
	removeValidationErrors();
});
$.subscribe('errorExportHistory', function(event, data) {
	// $.log('Form Verifying error! ');
});
$.subscribe('successExportHistory', function(event, data) {
	// $.log('Form success! ');
	ns.account.exportAccountHistory($('input[id=exportHistoryUrlValue]').val());
});

//Save Search
$.subscribe('beforeSaveSearch', function(event, data) {
	// $.log('About to verify form! ');
	removeValidationErrors();
});
$.subscribe('successSaveSearch', function(event, data) {
	// $.log('Form success! ');
	ns.account.saveSearch();
});

//Modify Search
$.subscribe('beforeModifySearch', function(event, data) {
	// $.log('About to verify form! ');
	removeValidationErrors();
});
$.subscribe('successModifySearch', function(event, data) {
	// $.log('Form success! ');
	ns.account.modifySearch();
});

$.subscribe('accountHistorySummaryClick', function(event, data) {
	 ns.shortcut.navigateToSubmenus('acctMgmt_history');
});

$.subscribe('transactionSearchSummaryClick', function(event, data) {
	 ns.shortcut.navigateToSubmenus('acctMgmt_search');
});
