	ns.transfer.showPendingApprovalTab = true;
	ns.transfer.pendingApprovalRecordCount = 0;
	ns.transfer.isLoadTransferTemplate = false;

	$.subscribe('pendingApprovalTransfersGridCompleteEvents', function(event,data) {
		ns.transfer.showPendingApprovalTab = true;
		ns.transfer.handleGridEventsAndSetWidth("#pendingApprovalTransfersID", "#transferGridTabs");

		//Add additional row(memo information)
		$("#pendingApprovalTransfersID").data("isMemoAdded",false);
		ns.transfer.AddPendingApprovalShowMemoButton("pendingApprovalTransfersID");
		ns.transfer.addApprovalsRowsForPendingApprovalTransfers("#pendingApprovalTransfersID");
	});

	$.subscribe('pendingTransfersGridCompleteEvents', function(event,data) {
		ns.transfer.handleGridEventsAndSetWidth("#pendingTransfersGridId", "#transferGridTabs");

		//Add additional row(approval information)
		$("#pendingTransfersGridId").data("isMemoAdded",false);
		ns.transfer.AddPendingShowMemoButton("pendingTransfersGridId");
	});

	$.subscribe('completedTransfersGridCompleteEvents', function(event,data) {
		ns.transfer.handleGridEventsAndSetWidth("#completedTransfersGrid", "#transferGridTabs");

		//Add additional row(approval information)
		$("#completedTransfersGrid").data("isMemoAdded",false);
		ns.transfer.AddCompletedShowMemoButton("completedTransfersGrid");
	});

	ns.transfer.handleGridEventsAndSetWidth = function(gridID,tabsID){
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
	};

	$.subscribe('cancelSingleTransferComplete', function(event,data) {
		ns.common.closeDialog('#deleteTransferDialogID');
	});


	$.subscribe('skipSingleTransferComplete', function(event,data) {
		ns.common.closeDialog('#skipTransferDialogID');
	});

	$.subscribe('cancelSingleTransferTemplateComplete', function(event,data) {
		//$('#singleTransfersTemplatesGridID').trigger("reloadGrid");
		ns.common.closeDialog("#deleteSingleTransferTemplatesDialogID");
	});

	//After the transfer templates is deleted, the grid summary should be reloaded.
	$.subscribe('cancelSingleTransferTemplateSuccessTopics', function(event,data) {
		ns.transfer.reloadSingleTransferTemplatesSummaryGrid();
		//ns.common.showStatus();
	});

	$.subscribe('cancelMultipleTransferTemplateComplete', function(event,data) {
		$('#multipleTransfersTemplatesGridID').trigger("reloadGrid");
		ns.common.closeDialog("#deleteMultipleTransferTemplatesDialogID");
	});

	$.subscribe('cancelMultipleTransferTemplateSuccessTopics', function(event,data) {
		ns.transfer.reloadMultipleTransferTemplatesSummaryGrid();
		//ns.common.showStatus();
	});

	$.subscribe('sendInquiryTransferFormSuccess', function(event,data) {
		ns.common.closeDialog("#InquiryTransferDialogID");
		//Display the result message on the status bar.
		//ns.common.showStatus();
        $.publish('refreshMessageSmallPanel');
	});

	$.subscribe('singleTemplatesGridCompleteEvents', function(event,data) {
		ns.transfer.addSubmittedForRowsForTransferTemplates("#singleTransfersTemplatesGridID");
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
		//display estimated amount indicator for single template
		//ns.transfer.displayEstimatedAmountIndicator("#singleTransfersTemplatesGridID");
	});

	$.subscribe('multileTemplatesGridCompleteEvents', function(event,data) {
		ns.transfer.addSubmittedForRowsForTransferTemplates("#multipleTransfersTemplatesGridID");
		//Adjust the width of grid automatically.
		ns.common.resizeWidthOfGrids();
		//display estimated amount indicator for multiple template
		//ns.transfer.displayEstimatedAmountIndicator("#multipleTransfersTemplatesGridID");
	});

	//After the transfer is deleted, the grid summary should be reloaded.
	$.subscribe('cancelTransferSuccessTopics', function(event,data) {
		$.publish("reloadSelectedTransferGridTab");
		//ns.common.showStatus();
	});

	//After the transfer is skipped, the grid summary should be reloaded.
	$.subscribe('skipTransferSuccessTopics', function(event,data) {
		$.publish("reloadSelectedTransferGridTab");
		//ns.common.showStatus();
	});




	ns.transfer.formatPendingApprovalsTransfersActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the transfer.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.transfer.viewTransferDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var edit = "<a title='" +js_edit+ "' href='#' onClick='ns.transfer.editSingleTransfer(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a title='" +js_delete+ "' href='#' onClick='ns.transfer.deleteTransfer(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		var skip  = "<a title='" +js_skip+ "' href='#' onClick='ns.transfer.skipTransfer(\""+ rowObject.map.SkipURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-skipRecurring'></span></a>";
		//var transferId = $('#pendingApprovalTransfersID').jqGrid('getCell', rowId, 'ID');
		var inquiry  = "<a title='" +js_inquiry+ "' href='#' onClick='ns.transfer.inquireTransaction(\""+ rowObject.map.InquireURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		if(rowObject["canEdit"] == "false") {
			  edit = "";
		}
		if(rowObject["canDelete"] == "false") {
			  del = "";
		}
		if(rowObject["canSkip"] == "false" || rowObject["recModel"] == true) {
			skip = "";

		}
		if(rowObject.map.canInquire == "false") {
			  inquiry = "";
		}
		//if resultOfApproval="Deletion", and status didnot equal "12", hide deletion and edit icons
		if(rowObject["resultOfApproval"] == "Deletion"&& rowObject["status"]!="12")
		{
			del = "";
	    	edit = "";
	    	skip = "";
		}
		if(rowObject["resultOfApproval"] == "Skip")
		{
			skip = "";
		}
		return ' ' + view + ' ' + edit + ' '  + del + ' '  + skip + ' ' + inquiry;
	};


	ns.transfer.formatApproversInfo = function(cellvalue, options, rowObject) {
		var approverNameInfo="";
		var approverNameOrUserName="";
		var approverGroup="";
		var approverInfos=rowObject.approverInfos;
			if(approverInfos!=undefined)
			{
				for(var i=0;i<approverInfos.length;i++){
				var approverInfo = approverInfos[i];
				if(i!=0)
				{
				approverNameInfo="," + approverNameInfo
				}

				var approverName = approverInfo.approverName;
				var approverIsGroup=approverInfo.approverIsGroup;

				if( approverIsGroup){
					approverGroup = "(" + js_group + ")";
				} else {
					approverGroup = "";
				}
				approverNameOrUserName = approverName + approverGroup;
				approverNameInfo=approverNameOrUserName+approverNameInfo;
				}
			}
			return approverNameInfo;
};

	ns.transfer.formatPendingTransfersActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the transfer.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.transfer.viewTransferDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var edit = "<a title='" +js_edit+ "' href='#' onClick='ns.transfer.editSingleTransfer(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a title='" +js_delete+ "' href='#' onClick='ns.transfer.deleteTransfer(\""+ rowObject.map.DeleteURL +"\", \"PendingTransfers\", \"false\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";
		var skip  = "<a title='" +js_skip+ "' href='#' onClick='ns.transfer.skipTransfer(\""+ rowObject.map.SkipURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-skipRecurring'></span></a>";
		//var transferId = $('#pendingApprovalTransfersID').jqGrid('getCell', rowId, 'ID');
		var inquiry  = "<a title='" +js_inquiry+ "' href='#' onClick='ns.transfer.inquireTransaction(\""+ rowObject.map.InquireURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		if(rowObject["canEdit"] == "false") {
			 edit = "";
		}
		if(rowObject["canDelete"] == "false") {
			  del = "";
		}
		if(rowObject["canSkip"] == "false") {
			skip = "";

		}
		if(rowObject.map.canInquire == "false") {
			 inquiry = "";
		}

		return ' ' + view + ' ' + edit + ' '  + del + ' '  + skip + ' ' + inquiry;
	};

	ns.transfer.formatCombinedAccount = function(cellvalue, options, rowObject) {

		var fromAccount = ns.common.HTMLEncode(rowObject.fromAccount.accountDisplayText);
		var toAccount = ns.common.HTMLEncode(rowObject.toAccount.accountDisplayText);

		return "<span class='formatCombinedAccountColumn'>"+fromAccount+ "</span><span class='gridColumnIcon'><span class='ffiUiMiscIco ffiUiIco-misc-multilinerowdataindicatorico'></span>" +toAccount+"</span>";
	};

	ns.transfer.formatBatchCombinedAccount = function(cellvalue, options, rowObject) {
		var fromAccountText = ns.transfer.customFromAccountNickNameColumnForMultipleTemplate(cellvalue, options, rowObject);
		var toAccountText = ns.transfer.customToAccountNickNameColumnForMultipleTemplate(cellvalue, options, rowObject);
		return "<span class='formatCombinedAccountColumn'>"+fromAccountText +"</span><span class='gridColumnIcon'><span class='ffiUiMiscIco ffiUiIco-misc-multilinerowdataindicatorico'></span>"+ toAccountText+"</span>";
	};

	ns.transfer.formatCombinedAmount = function(cellvalue, options, rowObject) {
		var toAmount = ns.transfer.formatToAmountColumn(rowObject.toAmountValue.currencyStringNoSymbol, options, rowObject);
		var fromAmount = ns.transfer.formatFromAmountColumn(rowObject.amountValue.currencyStringNoSymbol, options, rowObject);
		if(toAmount=="--"){
			return fromAmount;
		}else{
			return fromAmount + "/"+ toAmount;
		}
	};

	ns.transfer.formatCompletedTransfersActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the transfer.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.transfer.viewTransferDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var inquiry  = "<a title='" +js_inquiry+ "' href='#' onClick='ns.transfer.inquireTransaction(\""+ rowObject.map.InquireURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-help'></span></a>";

		if(rowObject.map.canInquire == "false") {
		     inquiry = "";
		}
		return ' ' + view + ' ' + inquiry;
	};

	ns.transfer.formatMultipleTransferTemplatesActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the transfer.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.transfer.viewTransferTemplateDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var load = "<a title='" +js_load+ "' href='#' onClick='ns.transfer.loadMultipleTransferTemplate(\""+ rowObject.map.LoadURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-upload'></span></a>";
		var edit = "<a title='" +js_edit+ "' href='#' onClick='ns.transfer.editMultipleTransferTemplate(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a title='" +js_delete+ "' href='#' onClick='ns.transfer.deleteMultipleTransferTemplate(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

		if(rowObject["map"].canLoadForMultipleTemplates == "false" || rowObject["commonStatusName"]=="Approval Pending"
			 || rowObject["commonStatusName"] == "" ) {
			load = "";
		}
		if(rowObject["map"].canEditForMultipleTemplates == "false") {
				 edit = "";
		}
		if(rowObject["map"].canDeleteForMultipleTemplates == "false") {
				   del = "";
		}
		//if resultOfApproval="Deletion", and status didnot equal "12", hide deletion and edit icons
		if(rowObject["resultOfApproval"] == "Deletion"&& rowObject["commonStatusName"]=="Approval Pending")
		{
			 del = "";
             edit = "";
		}
		return view + ' ' + load + ' ' + edit + ' '  + del  ;
	};

	ns.transfer.formatSingleTransferTemplatesActionLinks = function(cellvalue, options, rowObject) {
		//cellvalue is the ID of the transfer.
		var view = "<a title='" +js_view+ "' href='#' onClick='ns.transfer.viewTransferTemplateDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
		var load = "<a title='" +js_load+ "' href='#' onClick='ns.transfer.loadSingleTransferTemplate(\""+ rowObject.map.LoadURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-upload'></span></a>";
		var edit = "<a title='" +js_edit+ "' href='#' onClick='ns.transfer.editSingleTransferTemplate(\""+ rowObject.map.EditURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-edit'></span></a>";
		var del  = "<a title='" +js_delete+ "' href='#' onClick='ns.transfer.deleteSingleTransferTemplate(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

		if(rowObject["canLoad"] == "false" || rowObject["statusName"]=="Approval Pending") {
				load = "";
		}
		if(rowObject["canEdit"] == "false") {
			    edit = "";
		}
		if(rowObject["canDelete"] == "false") {
			 del = "";
		}
		//if resultOfApproval="Deletion", and status didnot equal "12", hide deletion and edit icons
		if(rowObject["resultOfApproval"] == "Deletion"&& rowObject["status"]!="12")
		{
			 del = "";
             edit = "";
		}
		return view + ' ' + load + ' ' + edit + ' '  + del  ;
	};

	//Formatter of Frequency column
	ns.transfer.formatTransferFrequencyColumn = function(cellvalue, options, rowObject){

		if(cellvalue == undefined ){
			if( rowObject.frequency == undefined ){
				return js_transfer_freq_NONE;
			} else {
				return rowObject.frequency;  //rowObject.frequency has value, means the transfer is a RecTransfer.
			}
		} else {
			return cellvalue;
		}
	};

	//Formatter of From Amount column.
	ns.transfer.formatFromAmountColumn = function( cellvalue, options, rowObject ){
		var fromAmount = cellvalue + ' ' + rowObject["amountValue"].currencyCode;
		if(rowObject["isAmountEstimated"]){
			fromAmount = '&#8776' + ' ' + fromAmount;
		}
		return fromAmount;
	};

	//Formatter of To Amount column.
	ns.transfer.formatToAmountColumn = function( cellvalue, options, rowObject ){
		var toAmount = "";
		if( cellvalue == "0.00"){
			toAmount = "--";
		} else {
			toAmount = cellvalue + ' ' + rowObject["toAmountValue"].currencyCode;
			if(rowObject["isToAmountEstimated"]){
				toAmount = '&#8776' + ' ' + toAmount;
			}
		}
		return toAmount;
	};

	//Formatter of multiple transfer templates columnes of FromAccount and ToAccount.
	ns.transfer.formatFromToAccountColumns = function( cellvalue, options, rowObject ){
		return "--";
	};

	//Formatter of multiple template summary Amount column
	ns.transfer.formatMultipleTemplateAmountColumn = function( cellvalue, options, rowObject ){
		var amount = "";
		if( cellvalue == "" || rowObject["amountValue"].currencyStringNoSymbol == '0.00'){
			amount = "--";
		} else {
			amount = rowObject["amountValue"].currencyStringNoSymbol + ' ' + rowObject["amountValue"].currencyCode;
		}
		return amount;
	};

	//Formatter of multiple template summary Status column.
	ns.transfer.formatMultipleTemplateCommonStatusNameColumn = function( cellvalue, options, rowObject ){
		var commonStatusName = "";
		if( cellvalue == ""){
			commonStatusName = "--";
		} else {
			commonStatusName = cellvalue;
		}
		return commonStatusName;
	};

	ns.transfer.customTypeColumn = function(cellvalue, options, rowObject) {

		//Construct Type column start.
		var _transferDestination = rowObject["transferDestination"];

		if( _transferDestination == "INTERNAL" || _transferDestination == "ITOI" ){
			_transferDestination = js_transfer_dest_ITOI;
		} else if ( _transferDestination == "ITOE" ){
			_transferDestination = js_transfer_dest_ITOE;
		} else if ( _transferDestination == "ETOI" ){
			_transferDestination = js_transfer_dest_ETOI;
		}
		return _transferDestination;

	};

	//Customs type column for Single template summary
	ns.transfer.customTypeColumnForSingleTemplate = function(cellvalue, options, rowObject) {

		var _transferDestination = rowObject["transferDestination"];

		if( _transferDestination == "INTERNAL" || _transferDestination == "ITOI" ){
			_transferDestination = js_transfer_dest_ITOI;
		} else if ( _transferDestination == "ITOE" ){
			_transferDestination = js_transfer_dest_ITOE;
		} else if ( _transferDestination == "ETOI" ){
			_transferDestination = js_transfer_dest_ETOI;
		}
		return _transferDestination;
	};

	//Customs type column for Multiple template summary
	ns.transfer.customTypeColumnForMultipleTemplate = function(cellvalue, options, rowObject) {
		var _convertDestination = "";
		var _commonDestination = rowObject["commonDestination"];
		if( _commonDestination == undefined || _commonDestination =="" ){
				_convertDestination = " -- ";
			}
		else{
			if(_commonDestination == "ITOI"){
				_convertDestination = js_transfer_dest_ITOI;
			}else if(_commonDestination == "ITOE"){
				_convertDestination = js_transfer_dest_ITOE;
			}else if(_commonDestination == "ETOI"){
				_convertDestination = js_transfer_dest_ETOI;
			}
		}
		return _convertDestination;
	};

	//Add approval information in the pending approval summary gird.
	ns.transfer.addApprovalsRowsForPendingApprovalTransfers = function(gridID){
		if( gridID == "#pendingApprovalTransfersID" ){
			var ids = jQuery(gridID).jqGrid('getDataIDs');
			var approvalsOrReject = "";
			var approvalerNameOrRejectReason = "";
			var rejectedOrSubmitedBy = "";
			var approverNameOrUserName = "";
			var approverGroup = "";
			var submittedForTitle = js_submitted_for;
			for(var i=0;i<ids.length;i++){
				var rowId = ids[i];

				var userName = ns.common.HTMLEncode($('#pendingApprovalTransfersID').jqGrid('getCell', rowId, 'userName'));

				var status = $('#pendingApprovalTransfersID').jqGrid('getCell', rowId, 'status');
				var rejectReason = $('#pendingApprovalTransfersID').jqGrid('getCell', rowId, 'rejectReason');
				var submittedFor = $('#pendingApprovalTransfersID').jqGrid('getCell', rowId, 'displayTextRoA');


				//var currentID = "#pendingApprovalTransfersID #" + rowId;
				var tableDOM=$("#pendingApprovalTransfersID");
				var approverName = ns.common.HTMLEncode($('#pendingApprovalTransfersID').jqGrid('getCell', rowId, 'approverName'));
				var approverIsGroup = $('#pendingApprovalTransfersID').jqGrid('getCell', rowId, 'approverIsGroup');
				var approverNameInfo = ns.common.HTMLEncode($('#pendingApprovalTransfersID').jqGrid('getCell', rowId, 'approverInfos'));



				//if status is 12, means that the transfer is rejected.
				if( status == 12 ){
					approvalsOrReject = js_reject_reason;
					rejectedOrSubmitedBy = js_rejected_by;
					if(rejectReason==''){
						approvalerNameOrRejectReason = "No Reason Provided";
					} else{
						approvalerNameOrRejectReason = ns.common.spcialSymbolConvertion( rejectReason );
					}
					if(approverNameInfo != ""){
					var className = "pltrow1";
						if(rowId % 2 == 0){
							className = "pdkrow";
						}
							className = 'ui-widget-content jqgrow ui-row-ltr';

							//$(currentID).after(ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameOrUserName,approvalsOrReject,approvalerNameOrRejectReason,className,10));
							var nextRow=ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameInfo,approvalsOrReject,approvalerNameOrRejectReason,className,10);
							var currentDOM=tableDOM.find("#"+rowId);
							currentDOM.after(nextRow);

					} else {
							var className = "pltrow1";
							if(rowId % 2 == 0){
								className = "pdkrow";
							}
							className = 'ui-widget-content jqgrow ui-row-ltr';

							//$(currentID).after(ns.common.addRejectReasonRow('','',approvalsOrReject,approvalerNameOrRejectReason,className,10));
							var nextRow=ns.common.addRejectReasonRow('','',approvalsOrReject,approvalerNameOrRejectReason,className,10);
							var currentDOM=tableDOM.find("#"+rowId);
							currentDOM.after(nextRow);
						}
					//if status is not 12.
					} else {
						approvalsOrReject = js_approval_waiting_on;
						approvalerNameOrRejectReason = approverNameInfo;
						rejectedOrSubmitedBy = js_submitted_by;
						approverNameOrUserName = userName;
						if(approverNameInfo != ""){
							var className = "pltrow1";
							if(rowId % 2 == 0){
								className = "pdkrow";
							}
							//remove the grid class, and add FF class.
							//$(currentID).removeClass();
							className = 'ui-widget-content jqgrow ui-row-ltr';
							//$(currentID).after(ns.common.addSubmittedForRow(approvalsOrReject,approvalerNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,10));
							var nextRow=ns.common.addSubmittedForRow(approvalsOrReject,approvalerNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,10);
							var currentDOM=tableDOM.find("#"+rowId);
							currentDOM.after(nextRow);
						} else {
							var className = "pltrow1";
							if(rowId % 2 == 0){
								className = "pdkrow";
							}
							//remove the grid class, and add FF class.

							//QTS 638252: if a transaction (ACH, Wires, Transfer, billpay, cashcon) is in approvals,
							// is then edited, then values are modified so it exceeds the limits with no approval
							// OR exceeds bank limits, it will get an error.  If the transaction is then cancelled,
							// the transaction is still in the "Approvals Summary" window, but not in approvals
							// because it was cancelled and not resubmitted.  Added the following error message to the
							// "Approvals Summary" window, "Submission to approvals FAILED, please resubmit or delete"
							// so the transaction can be edited to start submission again.
							//$(currentID).removeClass();
							className = 'ui-widget-content jqgrow ui-row-ltr';
							var nextRow='<tr class=\"' + className + ' skipRowActionItemRender\"><td colspan="10" align="center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
							'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'+js_submission_approval_failed+'&nbsp&nbsp&nbsp  </b>' + '&nbsp</td></tr>';
							var currentDOM=tableDOM.find("#"+rowId);
							currentDOM.after(nextRow);

					}
				}
			}
		}
	};


    //Add "Submitted For" information in the transfer templates summary gird.
	ns.transfer.addSubmittedForRowsForTransferTemplates = function(gridID){
		var ids = jQuery(gridID).jqGrid('getDataIDs');
		var submittedForTitle = js_submitted_for;
		var className = 'ui-widget-content jqgrow ui-row-ltr';

		var insertedRowCount = 1;
		var toInsertRowNum = 0;

		for(var i=0;i<ids.length;i++){

            var approvalsOrReject = "";
            var approvalerNameOrRejectReason = "";
            var rejectedOrSubmitedBy = "";
            var approverNameOrUserName = "";
            var approverGroup = "";

			var rowId = parseInt(ids[i]);

			var resultOfApproval = "";
			resultOfApproval = $(gridID).jqGrid('getCell', rowId, 'displayTextRoA');

			var approverNameInfo = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverInfos'));


			if( gridID == '#singleTransfersTemplatesGridID' && $(gridID).jqGrid('getCell', rowId, 'resultOfApproval') != "" ){
				toInsertRowNum = rowId + insertedRowCount;
				var currentID = "#singleTransfersTemplatesGridID tr:nth-child(" + toInsertRowNum + ")";
//				$(currentID).after("<tr class=\"" + className + "\"><td colspan=\"9\" align=\"center\">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp" +
//					"&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>" + submittedForTitle + " &nbsp&nbsp&nbsp  </b>" + resultOfApproval + "</td></tr>");
				++insertedRowCount;



                var userName = ns.common.HTMLEncode($('#singleTransfersTemplatesGridID').jqGrid('getCell', rowId, 'userName'));

                var status = $('#singleTransfersTemplatesGridID').jqGrid('getCell', rowId, 'status');
                var rejectReason = $('#singleTransfersTemplatesGridID').jqGrid('getCell', rowId, 'rejectReason');
                var submittedFor = $('#singleTransfersTemplatesGridID').jqGrid('getCell', rowId, 'displayTextRoA');



                //if status is 12, means that the transfer is rejected.
                if( status == 12 ){
                    approvalsOrReject = js_reject_reason;
                    rejectedOrSubmitedBy = js_rejected_by;
                    if(rejectReason==''){
                        approvalerNameOrRejectReason = "No Reason Provided";
                    } else{
                        approvalerNameOrRejectReason = ns.common.spcialSymbolConvertion( rejectReason );
                    }


                    if(approverNameInfo != ""){
                    var className = "pltrow1";
                        if(rowId % 2 == 0){
                            className = "pdkrow";
                        }
                            className = 'ui-widget-content jqgrow ui-row-ltr';
                            /*$(currentID).after('<tr class=\"' + className + '\"><td colspan="10" align="center" style="overflow:auto;">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
                            '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>' + approvalsOrReject + ' &nbsp&nbsp&nbsp  </b>' + approvalerNameOrRejectReason +'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>' + rejectedOrSubmitedBy  + '</b> &nbsp&nbsp&nbsp ' +  approverNameOrUserName + '&nbsp</td></tr>');*/

                            $(currentID).after(ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameInfo,approvalsOrReject,approvalerNameOrRejectReason,className,10));
                 } else {
                            var className = "pltrow1";
                            if(rowId % 2 == 0){
                                className = "pdkrow";
                            }
                            className = 'ui-widget-content jqgrow ui-row-ltr';
                            /*$(currentID).after('<tr class=\"' + className + '\"><td colspan="10" align="center" style="overflow:auto;">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
                            '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>' + approvalsOrReject + ' &nbsp&nbsp&nbsp  </b>' + approvalerNameOrRejectReason +'&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'+ '&nbsp</td></tr>');
                            */

                            $(currentID).after(ns.common.addRejectReasonRow('','',approvalsOrReject,approvalerNameOrRejectReason,className,10));
                        }
                    //if status is not 12.
                    } else {
                        approvalsOrReject = js_approval_waiting_on;
                        approvalerNameOrRejectReason = approverNameInfo;
                        rejectedOrSubmitedBy = js_submitted_by;
                        approverNameOrUserName = userName;
                        if(approverNameInfo != ""){
                            var className = "pltrow1";
                            if(rowId % 2 == 0){
                                className = "pdkrow";
                            }
                            //remove the grid class, and add FF class.
                            //$(currentID).removeClass();
                            className = 'ui-widget-content jqgrow ui-row-ltr';
                            $(currentID).after(ns.common.addSubmittedForRow(approvalsOrReject,approvalerNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,10));
                        } else {
                            var className = "pltrow1";
                            if(rowId % 2 == 0){
                                className = "pdkrow";
                            }
                            //remove the grid class, and add FF class.

                            //QTS 638252: if a transaction (ACH, Wires, Transfer, billpay, cashcon) is in approvals,
                            // is then edited, then values are modified so it exceeds the limits with no approval
                            // OR exceeds bank limits, it will get an error.  If the transaction is then cancelled,
                            // the transaction is still in the "Approvals Summary" window, but not in approvals
                            // because it was cancelled and not resubmitted.  Added the following error message to the
                            // "Approvals Summary" window, "Submission to approvals FAILED, please resubmit or delete"
                            // so the transaction can be edited to start submission again.
                            //$(currentID).removeClass();
                            className = 'ui-widget-content jqgrow ui-row-ltr';
                            $(currentID).after('<tr class=\"' + className + '\"><td colspan="10" align="center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
                            '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'+js_submission_approval_failed+'&nbsp&nbsp&nbsp  </b>' + '&nbsp</td></tr>');
                    }
                }


			} else if( gridID == '#multipleTransfersTemplatesGridID'
				&&  $(gridID).jqGrid('getCell', rowId, 'resultOfApproval')!="" )
			{
				toInsertRowNum = rowId + insertedRowCount;
				var currentID  = "#multipleTransfersTemplatesGridID tr:nth-child(" + toInsertRowNum + ")";



                var userName = ns.common.HTMLEncode($('#multipleTransfersTemplatesGridID').jqGrid('getCell', rowId, 'userName'));
                var approverNameInfo = ns.common.HTMLEncode($(gridID).jqGrid('getCell', rowId, 'approverInfos'));
                var status = $('#multipleTransfersTemplatesGridID').jqGrid('getCell', rowId, 'commonStatusName');
                var rejectReason = $('#multipleTransfersTemplatesGridID').jqGrid('getCell', rowId, 'rejectReason');
                var submittedFor = $('#multipleTransfersTemplatesGridID').jqGrid('getCell', rowId, 'displayTextRoA');

                if (status == "--")     // if mixed statuses, don't insert a row because the information is mixed
                    continue;
				++insertedRowCount;

                //if status is 12, means that the transfer is rejected.
                if( status != "Approval Pending" ){
                    approvalsOrReject = js_reject_reason;
                    rejectedOrSubmitedBy = js_rejected_by;
                    if(rejectReason==''){
                        approvalerNameOrRejectReason = "No Reason Provided";
                    } else{
                        approvalerNameOrRejectReason = ns.common.spcialSymbolConvertion( rejectReason );
                    }


                    if(approverNameInfo != ""){
                    var className = "pltrow1";
                        if(rowId % 2 == 0){
                            className = "pdkrow";
                        }
                            className = 'ui-widget-content jqgrow ui-row-ltr';

                            $(currentID).after(ns.common.addRejectReasonRow(rejectedOrSubmitedBy,approverNameInfo,approvalsOrReject,approvalerNameOrRejectReason,className,10));
                 } else {
                            var className = "pltrow1";
                            if(rowId % 2 == 0){
                                className = "pdkrow";
                            }
                            className = 'ui-widget-content jqgrow ui-row-ltr';
                            $(currentID).after(ns.common.addRejectReasonRow('','',approvalsOrReject,approvalerNameOrRejectReason,className,10));
                        }
                    //if status is not 12.
                    } else {
                        approvalsOrReject = js_approval_waiting_on;
                        approvalerNameOrRejectReason = approverNameInfo;
                        rejectedOrSubmitedBy = js_submitted_by;
                        approverNameOrUserName = userName;
                        if(approverNameInfo != ""){
                            var className = "pltrow1";
                            if(rowId % 2 == 0){
                                className = "pdkrow";
                            }
                            //remove the grid class, and add FF class.
                            //$(currentID).removeClass();
                            className = 'ui-widget-content jqgrow ui-row-ltr';

                            $(currentID).after(ns.common.addSubmittedForRow(approvalsOrReject,approvalerNameOrRejectReason,submittedForTitle,submittedFor,rejectedOrSubmitedBy,approverNameOrUserName,className,10));

                        } else {
                            var className = "pltrow1";
                            if(rowId % 2 == 0){
                                className = "pdkrow";
                            }
                            //remove the grid class, and add FF class.

                            //QTS 638252: if a transaction (ACH, Wires, Transfer, billpay, cashcon) is in approvals,
                            // is then edited, then values are modified so it exceeds the limits with no approval
                            // OR exceeds bank limits, it will get an error.  If the transaction is then cancelled,
                            // the transaction is still in the "Approvals Summary" window, but not in approvals
                            // because it was cancelled and not resubmitted.  Added the following error message to the
                            // "Approvals Summary" window, "Submission to approvals FAILED, please resubmit or delete"
                            // so the transaction can be edited to start submission again.
                            //$(currentID).removeClass();
                            className = 'ui-widget-content jqgrow ui-row-ltr';
                            $(currentID).after('<tr class=\"' + className + '\"><td colspan="10" align="center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp' +
                            '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<b>'+js_submission_approval_failed+'&nbsp&nbsp&nbsp  </b>' + '&nbsp</td></tr>');
                    }
                }



			}
		}
	};

	//Formatter of multiple templates status column.
	ns.transfer.formatMultipleTemplateCommonStatusNameColumn = function( cellvalue, options, rowObject ){
		if( cellvalue == undefined || cellvalue =="" ){
			return "--";
		} else {
			return cellvalue;
		}
	};

	ns.transfer.customFromAccountNicknameColumn = function(cellvalue, options, rowObject) {

			//Construct "From Account Nickname" column.
			var _fromAccountDisplayText = rowObject["fromAccount"].displayText;
			var _fromAccountNickName = rowObject["fromAccount"].nickName;
			var _fromAccountCurrencyCode = rowObject["fromAccount"].currencyCode;
			var _fromAccountName = _fromAccountDisplayText;
			if( _fromAccountNickName != undefined  &&  _fromAccountNickName != "" ){
				_fromAccountName = _fromAccountName + " - " + _fromAccountNickName;
			}
			if( _fromAccountCurrencyCode != undefined  &&  _fromAccountCurrencyCode != "" ){
				_fromAccountName = _fromAccountName + " - " + _fromAccountCurrencyCode;
			}
			return _fromAccountName;
	};

	ns.transfer.customToAccountNickNameColumn = function(cellvalue, options, rowObject) {

			//	Construct "To Account Nickname" column.
			var _toAccountDisplayText = rowObject["toAccount"].displayText;
			var _toAccountNickName = rowObject["toAccount"].nickName;
			var _toAccountCurrencyCode = rowObject["toAccount"].currencyCode;
			var _toAccountName = _toAccountDisplayText;
			if( _toAccountNickName != undefined  &&  _toAccountNickName != "" ){
				_toAccountName = _toAccountName + " - " + _toAccountNickName;
			}
			if( _toAccountCurrencyCode != undefined  &&  _toAccountCurrencyCode != "" ){
				_toAccountName = _toAccountName + " - " + _toAccountCurrencyCode;
			}
			return _toAccountName;
	};

	//Customs from account nick name column for Single template
	ns.transfer.customFromAccountNickNameColumnForSingleTemplate = function(cellvalue, options, rowObject) {

		//Construct "From Account Nickname" column.
		var _fromAccountDisplayText = rowObject["fromAccount"].displayText;
		var _fromAccountNickName = rowObject["fromAccount"].nickName;
		var _fromAccountCurrencyCode = rowObject["fromAccount"].currencyCode;
		var _fromAccountName = _fromAccountDisplayText;
		if( _fromAccountNickName != undefined  &&  _fromAccountNickName != "" ){
			_fromAccountName = _fromAccountName + " - " + _fromAccountNickName;
		}
		if( _fromAccountCurrencyCode != undefined  &&  _fromAccountCurrencyCode != "" ){
			_fromAccountName = _fromAccountName + " - " + _fromAccountCurrencyCode;
		}
		return _fromAccountName;


	};
	//Customs from account nick name column for Multiple template
	ns.transfer.customFromAccountNickNameColumnForMultipleTemplate = function(cellvalue, options, rowObject) {
		var	_commonFromAccount= rowObject["commonFromAccount"];

		if( _commonFromAccount == undefined || _commonFromAccount =="" ){
			_commonFromAccountName = " -- ";

		}else{
			var _commonFromAccountDisplayText = rowObject["commonFromAccount"].accountDisplayText;
			_commonFromAccountName = _commonFromAccountDisplayText;
		}
		return _commonFromAccountName;

	};


	//Customs to account nick name column for Single template
	ns.transfer.customToAccountNickNameColumnForSingleTemplate = function(cellvalue, options, rowObject) {
		//Construct "To Account Nickname" column.
		var _toAccountDisplayText = rowObject["toAccount"].displayText;
		var _toAccountNickName = rowObject["toAccount"].nickName;
		var _toAccountCurrencyCode = rowObject["toAccount"].currencyCode;
		var _toAccountName = _toAccountDisplayText;
		if( _toAccountNickName != undefined  &&  _toAccountNickName != "" ){
			_toAccountName = _toAccountName + " - " + _toAccountNickName;
		}
		if( _toAccountCurrencyCode != undefined  &&  _toAccountCurrencyCode != "" ){
			_toAccountName = _toAccountName + " - " + _toAccountCurrencyCode;
		}
		return _toAccountName;
	};

	//Customs to account nick name column for Multiple template
	ns.transfer.customToAccountNickNameColumnForMultipleTemplate = function(cellvalue, options, rowObject) {
		var	_commonToAccount= rowObject["commonToAccount"];

		if( _commonToAccount == undefined || _commonToAccount =="" ){
			_commonToAccountName = " -- ";
		}else{
			var _commonToAccountDisplayText = rowObject["commonToAccount"].accountDisplayText;
			_commonToAccountName = _commonToAccountDisplayText;
		}
		return _commonToAccountName;
	};

	//Reload transfer summary jgrid..
	ns.transfer.reloadTransfersSummaryGrid = function( gridId ){
		ns.common.setFirstGridPage('#pendingApprovalTransfersID');
		ns.common.setFirstGridPage('#pendingTransfersGridId');
		ns.common.setFirstGridPage('#completedTransfersGrid');
		$('#pendingApprovalTransfersID').trigger("reloadGrid");
		$('#pendingTransfersGridId').trigger("reloadGrid");
		$('#completedTransfersGrid').trigger("reloadGrid");
		ns.transfer.reloadHomePendingTransfersSummaryGrid();
		// To refresh pending approval grid based on application type( Small business, Consumer and Corporate)
		//$('#summary').load('/cb/pages/jsp/transfers/transfer_summary.jsp');
	};

	//Reload the pending transfer portlet grid on home page
	ns.transfer.reloadHomePendingTransfersSummaryGrid = function(){
		var urlString = '/cb/pages/jsp/home/portlet/inc/pendingtransfers.jsp';
		ns.pendingTransactionsPortal.getHomeSummaryDatas(urlString,'#pendingTransfersData');
	};

	//Reload single transfer templates summary jgrid..
	ns.transfer.reloadSingleTransferTemplatesSummaryGrid = function( gridId ){
		ns.common.setFirstGridPage('#singleTransfersTemplatesGridID');
		$('#singleTransfersTemplatesGridID').trigger("reloadGrid");
	};

	//Reload multiple transfer templates summary jgrid..
	ns.transfer.reloadMultipleTransferTemplatesSummaryGrid = function( gridId ){
		ns.common.setFirstGridPage('#multipleTransfersTemplatesGridID');
		$('#multipleTransfersTemplatesGridID').trigger("reloadGrid");
	};

	//Reload single and multiple transfer templates summary grid.
	ns.transfer.reloadSingleAndMultipleTransferTemplates = function(){
		ns.common.setFirstGridPage('#singleTransfersTemplatesGridID');
		ns.common.setFirstGridPage('#multipleTransfersTemplatesGridID');
		$('#singleTransfersTemplatesGridID').trigger("reloadGrid");
		$('#multipleTransfersTemplatesGridID').trigger("reloadGrid");
	};

	//view transfer templates.
	ns.transfer.loadTransferTemplate = function( transferID, collectionName ){
		ns.transfer.viewTransferDetails( transferID, collectionName );
	};
	//edit transfer template.
	ns.transfer.editSingleTransferTemplate = function( urlString ){
		$.publish('beforeLoadTransferForm');
		$.ajax({
			url: urlString,
			success: function(data){
				ns.common.removeDialog('innerdeletetransferdialog');//defined in accounttransferedit.jsp
				$('#inputDiv').html(data);
				$.publish('editSingleTransferTemplateFormComplete');
				$('#transferFileImportHolder').hide();
			}
		});
	};

	ns.transfer.loadSingleTransferTemplate = function(urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#summary').empty();
				$('#inputDiv').html(data);
				ns.transfer.loadTransferPageForSelectedTemplate();
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#details").setInitialFocus();
				}
				ns.common.refreshDashboard('dbTransferSummary');
				ns.transfer.isLoadTransferTemplate = true;
				ns.common.selectDashboardItem("addSingleTransferLink");
			}
		});
	};

	ns.transfer.loadMultipleTransferTemplate = function(urlString){
		//Add MULTI_ prefix for template ID, it will be used by the jsp accounttransfermultiplenewform.jsp
		$.ajax({
			url: urlString,
			success: function(data){
				$('#summary').empty();
				$('#inputDiv').html(data);
				ns.transfer.loadTransferPageForSelectedTemplate();
				$.publish('showTransferFileImportLink');
				$('#transferFileImportLink').removeAttr('style');
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
					$("#details").setInitialFocus();
				}
				ns.common.refreshDashboard('dbTransferSummary');
				ns.transfer.isLoadTransferTemplate = true;
				ns.common.selectDashboardItem("addMultipleTransferLink");
//				toggleGridOnFormToggle();
			}
		});
	};

	// The topic to show file import link in transfers
	$.subscribe('showTransferFileImportLink', function(event,data) {
		$('#transferFileImportLink').attr('style','display: block');
	});

	// The topic to hide file import link in transfers
	$.subscribe('hideTransferFileImportLink', function(event,data) {
		$('#transferFileImportLink').attr('style','display: none');
	});

	//The function is used by above two load template functions : loadSingleTransferTemplate() and loadMultipleTransferTemplate().
	ns.transfer.loadTransferPageForSelectedTemplate = function(){
		//$.log('About to load form! ');
		$('#details').hide();
		$('#quicksearchcriteria').hide();

		$('#transferTemplateGridTabs').hide();
		//$('#transferTemplateGridTabs').portlet('fold');

		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);
		ns.transfer.gotoStep(1);
		$('#details').slideDown();
	};


	ns.transfer.editMultipleTransferTemplate = function( urlString){
		$.publish('beforeLoadTransferForm');
		$.ajax({
			url: urlString,
			success: function(data){
				$('#inputDiv').html(data);
				$.publish('loadTransferFormComplete');
				$.publish('hideShowEditTransferTemplateForm');
//				toggleGridOnFormToggle();
			}
		});
	};


	//inquire transfer transaction.
	ns.transfer.inquireTransaction = function( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#InquiryTransferDialogID').html(data).dialog('open');
			}
		});
	};

	ns.transfer.viewTransferDetails = function( urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewTransferDetailsDialogID').html(data).dialog('open');

				ns.common.resizeDialog('viewTransferDetailsDialogID');
			}
		});
	};

	ns.transfer.viewTransferTemplateDetails = function( urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#viewTransferTemplateDetailsDialogID').html(data).dialog('open');

				ns.common.resizeDialog('viewTransferTemplateDetailsDialogID');
			}
		});
	};

	ns.transfer.deleteTransfer = function( urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteTransferDialogID').html(data).dialog('open');
			}
		});
	};

	ns.transfer.skipTransfer = function( urlString){

		$.ajax({
			url: urlString,
			success: function(data){
				$('#skipTransferDialogID').html(data).dialog('open');
			}
		});
	};

	ns.transfer.deleteMultipleTransferTemplate = function( urlString ){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteMultipleTransferTemplatesDialogID').html(data).dialog('open');
			}
		});
	};

	//delete transfer template.
	ns.transfer.deleteSingleTransferTemplate = function( urlString){
		$.ajax({
			url: urlString,
			success: function(data){
				$('#deleteSingleTransferTemplatesDialogID').html(data).dialog('open');
			}
		});
	};

	ns.transfer.editSingleTransfer = function( urlString ){
		$.publish('beforeLoadTransferForm');
		$.ajax({
			url: urlString,
			success: function(data){
				ns.common.removeDialog('innerdeletetransferdialog');//defined in accounttransferedit.jsp
				$('#inputDiv').html(data);
				$.publish('editSingleTransferFormComplete');
//				toggleGridOnFormToggle();
				$('#transferFileImportHolder').hide();
			}
		});
	};
	//Displays an estimated amount indicator
	ns.transfer.displayEstimatedAmountIndicator = function( gridId ){
		$('#singleTransfersTemplatesGridID_pager_right').html('&#8776; ' + js_indicatesEstimatedAmount + '&nbsp;&nbsp;');
	};
	// Script to add grid button for showing and hiding memo field

	// Pending Approval - Script to add grid button for showing and hiding memo field
	ns.transfer.AddPendingApprovalShowMemoButton = function( gridId ){
		var buttonConfig = {
			id: gridId+'pendingApprovalShowMemo',
			caption:js_showMemo_btnCaption,
			title:js_showHideMemo_btnTitle,
			buttonicon: 'ui-icon-image',
			onClickButton: function(e) {
				var isMemoAdded = $("#" + gridId).data("isMemoAdded") || false;
				if(!isMemoAdded){
					ns.transfer.addMemoRows(gridId, gridId+"pendingApprovalMemo");
				}
				var buttonLabel = $("#"+gridId+"pendingApprovalShowMemo"+" div");
				var buttonId = "#"+gridId+"pendingApprovalShowMemo";
				if($(buttonId).hasClass("selected")) {
					$(buttonId).removeClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_showMemo_btnCaption);
					$("tr[rowtype="+gridId+"pendingApprovalMemo]").hide();
				} else {
					$(buttonId).addClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_hideMemo_btnCaption);
					$("tr[rowtype="+gridId+"pendingApprovalMemo]").show();
				}
			}
		};
		$("#" + gridId + "pendingApprovalShowMemo").remove();
		ns.common.addGridButton("#"+gridId, buttonConfig);
	};

	// Pending - Script to add grid button for showing and hiding memo field
	ns.transfer.AddPendingShowMemoButton = function( gridId ){
		var buttonConfig = {
			id: gridId+'pendingShowMemo',
			caption:js_showMemo_btnCaption,
			title:js_showHideMemo_btnTitle,
			buttonicon: 'ui-icon-image',
			onClickButton: function(e) {
				var isMemoAdded = $("#" + gridId).data("isMemoAdded") || false;
				if(!isMemoAdded){
					ns.transfer.addMemoRows(gridId, gridId+"pendingMemo");
				}
				var buttonLabel = $("#"+gridId+"pendingShowMemo"+" div");
				var buttonId = "#"+gridId+"pendingShowMemo";
				if($(buttonId).hasClass("selected")) {
					$(buttonId).removeClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_showMemo_btnCaption);
					$("tr[rowtype="+gridId+"pendingMemo]").hide();
				} else {
					$(buttonId).addClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_hideMemo_btnCaption);
					$("tr[rowtype="+gridId+"pendingMemo]").show();
				}
			}
		};
		$("#" + gridId + "pendingShowMemo").remove();
		ns.common.addGridButton("#"+gridId, buttonConfig);
	};

	// Pending - Script to add grid button for showing and hiding memo field
	ns.transfer.AddCompletedShowMemoButton = function( gridId ){
		var buttonConfig = {
			id: gridId+'completedShowMemo',
			caption:js_showMemo_btnCaption,
			title:js_showHideMemo_btnTitle,
			buttonicon: 'ui-icon-image',
			onClickButton: function(e) {
				var isMemoAdded = $("#" + gridId).data("isMemoAdded") || false;
				if(!isMemoAdded){
					ns.transfer.addMemoRows(gridId, gridId+"completedMemo");
				}
				var buttonLabel = $("#"+gridId+"completedShowMemo"+" div");
				var buttonId = "#"+gridId+"completedShowMemo";
				if($(buttonId).hasClass("selected")) {
					$(buttonId).removeClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_showMemo_btnCaption);
					$("tr[rowtype="+gridId+"completedMemo]").hide();
				} else {
					$(buttonId).addClass("selected");
					$(buttonLabel).html("<span class='ui-icon ui-icon-image'></span>"+js_hideMemo_btnCaption);
					$("tr[rowtype="+gridId+"completedMemo]").show();
				}
			}
		};
		$("#" + gridId + "completedShowMemo").remove();
		ns.common.addGridButton("#"+gridId, buttonConfig);
	};

	// Function to add Memo rows
	ns.transfer.addMemoRows = function(gridID, memoRowType) {
		var memoCells = $("td[aria-describedby=" + gridID +"_memo]");
		$.each(memoCells,function(i,memoCell){
			var memo = $(memoCell).html();
			if(memo){
				var parentRow = $(memoCell).parent();
				var className = parentRow.attr("class");
				$(parentRow).after('<tr rowType=\"'+memoRowType+'\"  class=\"' + className + '\"><td colspan="10"><div class="memo"><b>'+js_memoDescr_Start+':</b>' + memo + '</div></td></tr>');
			};
		});

		$("#" + gridID).data("isMemoAdded",true);
	};

	//Formatter of Number of Transfers column
	ns.transfer.formatTransferNumberColumn = function(cellvalue, options, rowObject){
		if(cellvalue=='999'){
			return js_transfer_frequency_unlimited;
		}else if(cellvalue){
			return cellvalue;
		}else{
			return '';
		}
	};

	//Formatter of Transfer Status column
	ns.transfer.formatTransferStatusColumn = function(cellvalue, options, rowObject){
		if(cellvalue == 20){//TRS_INPROCESS
			return js_status_in_process;
		}else if(cellvalue == 19){//TRS_IMMED_INPROCESS
			return js_status_in_process;
		}else if(cellvalue == 11){//TRS_BATCH_INPROCESS
			return js_status_in_process;
		}else if(cellvalue == 23){//TRS_FUNDS_INPROCESS
			return js_status_in_process;
		}else if(cellvalue == 21){//TRS_FUNDSPROCESSED
			return js_status_in_process;
		} else if(cellvalue == 24){//TRS_SKIPPED
			return js_status_skip;
		} else{//other status
			return js_status_pending;
		}
	};

	//After the transfer templates is deleted, the grid summary should be reloaded.
	$.subscribe('extTransferAccountsCompleteTopic', function(event,data) {
		ns.common.resizeWidthOfGrids();
	});