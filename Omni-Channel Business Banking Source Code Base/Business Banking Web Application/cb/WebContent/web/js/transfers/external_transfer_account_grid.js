ns.transfer.html_element_input="input";
ns.transfer.element_type_checkbox="checkbox";
ns.transfer.element_type_text="text";
ns.transfer.element_name_prefix="ModifyExtTransferAccounts.CurrentAccountByBPWID=";
ns.transfer.active_status_element_postfix="&ModifyExtTransferAccounts.Active";
ns.transfer.nick_name_element_postfix="&ModifyExtTransferAccounts.Nickname";
ns.transfer.verify_status_not_required="0";
ns.transfer.verify_status_not_deposited="1";
ns.transfer.verify_status_deposited="2";
ns.transfer.verify_status_deposit_failed="3";
ns.transfer.verify_status_verified="4";
ns.transfer.verify_status_not_verified="5";

/* Subscription of all topics required by the JqGrid */

//Subscribe event to fire when grid loading completes
$.subscribe('makeRowsEditable', function(event, data) {
	// Get no. of rows in the grid
	var noOfRows = jQuery("#externalTransferAccountsGrid").jqGrid('getGridParam',
	'records');

	// Iterate over all rows and make them editable
	for ( var counter = 1; counter <= noOfRows; counter++) {
		jQuery("#externalTransferAccountsGrid").jqGrid('editRow', counter, false,
				ns.transfer.setProperIdsToEditableControls);
	}
});

//Function to mangle ids of editable controls in a grid row. This is needed to
//post data correctly on the server side.
ns.transfer.setProperIdsToEditableControls = function(rowId) {
	// Get all input tags in a row, using rowId supplied
	var inputElements = jQuery("#externalTransferAccountsGrid").find("#" + rowId).find(ns.transfer.html_element_input);

	// Get rowData, a map representing values shown in a table row against their
	// column names as key
	var rowData = jQuery("#externalTransferAccountsGrid").jqGrid('getRowData', rowId);
	var idValue = rowData["bpwID"];
	var verifyStatus = rowData["verifyStatusValue"];
	var isAcctActive = rowData["status"].search(/checked/i) != -1 ? true : false;
	var canEdit = rowData["map.canEdit"];
	// Iterate over input tag array and change their ids as desired.
	var element = null;
	for ( var counter = 0; counter < inputElements.length; counter++) {
		element = inputElements[counter];
		if (element.type == ns.transfer.element_type_checkbox) {
			element.name = ns.transfer.element_name_prefix + idValue + ns.transfer.active_status_element_postfix;
			if(verifyStatus != ns.transfer.verify_status_not_required && verifyStatus != ns.transfer.verify_status_verified) {
				if (!element.checked) {
					element.disabled = !isAcctActive;
				}
			}
			// provide on click event handler
			if(canEdit != "false") {
				element.onclick = function() {
					ns.transfer.toggleEditableControlState(this);
				};
			} else if(canEdit == "false") {
				element.disabled = true;
			}
		} else if (element.type == ns.transfer.element_type_text) {
			element.name = ns.transfer.element_name_prefix + idValue + ns.transfer.nick_name_element_postfix;
			if(verifyStatus != ns.transfer.verify_status_not_required && verifyStatus != ns.transfer.verify_status_verified) {
				element.disabled = !isAcctActive;
			}
			if(canEdit == "false") {
				element.disabled = true;
			}
		}
	}
};

//Function to toggle disabled state of Other editable controls depending on
//whether Active checkbox is checked or not.
ns.transfer.toggleEditableControlState = function(activeChkBox) {
	// Get parent row and find elements in parent Row to enable or disable
	var inputElements = $(activeChkBox).parents('tr:first').find(ns.transfer.html_element_input);

	// Iterate over input tag array and enable or disable them
	for ( var counter = 0; counter < inputElements.length; counter++) {
		if (inputElements[counter].id == activeChkBox.id) {
			continue;
		} else {
//			inputElements[counter].disabled = !activeChkBox.checked;

			// If element of type checkbox then uncheck that
			if (inputElements[counter].type == ns.transfer.element_type_checkbox
					&& !activeChkBox.checked) {
				inputElements[counter].checked = false;
			}
		}
	}
};

//not in use
ns.transfer.formatVerifiedStatusLink = function(cellvalue, options, rowObject) {
	//cellvalue is the verification status.
	var link = "";
	if(rowObject.verifyStatusValue == ns.transfer.verify_status_deposited) {
		link = "<a title='" +cellvalue+ "' href='javascript:void(0)' onClick='ns.transfer.verifyExternalTransferAccount(\""+ rowObject.map.VerifyURL +"\");'>" + cellvalue + "</a>";
	} else {
		link = cellvalue;
	}
	return ' ' + link;
};

ns.transfer.formatExternalTransferActionLinks = function (cellvalue, options, rowObject) { 
	//cellvalue is the ID of the External Account.
	var verify = "";
	if(rowObject.verifyStatusValue == ns.transfer.verify_status_not_deposited) {
		verify = "<a class='' title='" +js_verify_now+ "' href='#' onClick='ns.transfer.depositExternalTransferAccount(\""+ rowObject.map.VerifyURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-apply'></span></a>";
	} else if(rowObject.verifyStatusValue == ns.transfer.verify_status_deposited) {
		verify = "<a class='' title='" +js_verify_now+ "' href='#' onClick='ns.transfer.verifyExternalTransferAccount(\""+ rowObject.map.VerifyURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-apply'></span></a>";
	}
	var view = "<a class='' title='" +js_view+ "' href='#' onClick='ns.transfer.viewExternalTransferDetails(\""+ rowObject.map.ViewURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-info'></span></a>";
	var del  = "<a class='' title='" +js_delete+ "' href='#' onClick='ns.transfer.deleteExternalTransferAccount(\""+ rowObject.map.DeleteURL +"\");'><span class='gridActionColumnIconCls ffiUiIcoSmall ffiUiIco-icon-delete'></span></a>";

	if(rowObject["map"].canVerify == "false" && verify != "") {
		verify = "";
	}
	if(rowObject["map"].canDelete == "false") {
		del = "";
	}
	
	return verify + ' ' + view + ' ' + ' '  + del;
};

ns.transfer.depositExternalTransferAccount = function(urlString){
	$.ajax({
		url: urlString,
		success: function(data){
			$('#depositExternalTransferAccountLink').click();
		}
	});
};

ns.transfer.verifyExternalTransferAccount = function(urlString){
	$.ajax({
		url: urlString,
		success: function(data){
			$('#verifyExternalTransferAccountLink').click();
		}
	});
};

ns.transfer.viewExternalTransferDetails = function(urlString){
	$.ajax({
		url: urlString,
		success: function(data){
			$('#viewExternalTransferDetailsDialogID').html(data).dialog('open');
		}
	});
};

ns.transfer.deleteExternalTransferAccount = function(urlString) {
	$.ajax({
		url: urlString,
		success: function(data){
			$('#deleteExternalTransferAccountDialogID').html(data).dialog('open');
		}
	});
};

ns.transfer.resetExternalAccounts = function () {
	ns.transfer.reloadExternalAccounts();
};

ns.transfer.saveExternalAccounts = function () {
	var urlString = '/cb/pages/jsp/transfers/saveExtTransferAccounts.action';
	$.ajax({
		type: "POST",
		url: urlString,
		data: $("#externalAccountsSummaryFormID").serialize(),
		success: function(data) {
			// Refresh the external accounts tab
			ns.transfer.reloadExternalAccounts();
		}
	});
	return false;
};

//Reload external transfer accounts summary grid.
ns.transfer.reloadExternalAccounts = function(){
	$('#manageExternalAccountsLink').click();
};