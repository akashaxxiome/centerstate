	/*
	 * Control dashboard button display
	 */
	$.subscribe('externalAccountsSummaryLoadedSuccess', function(event,data){
		//ns.common.refreshDashboard('dbExternalAccountSummary');
		ns.transfer.gotoExternalTransfer();
	});

	ns.transfer.gotoExternalTransfer = function(){
		/*$('#addExternalTransferAccountLink').removeAttr('style');
		$('#transfersLink').removeAttr('style');
		$('#templatesLink').removeAttr('style');
		$('#addSingleTemplateLink').attr('style','display:none');
		$('#addMultipleTemplateLink').attr('style','display:none');
		$('#addSingleTransferLink').attr('style','display:none');
		$('#addMultipleTransferLink').attr('style','display:none');
		$('#manageExternalAccountsLink').attr('style','display:none');
		$('#details').hide();
		$('#quickSearchLink').hide();
		$('#quicksearchcriteria').hide();
		$('#transferfileupload').hide();
		$('#mappingDiv').hide();
		$('#transferAddCustomMappingsLink').hide();
		$('#transferFileImportLink').attr('style','display:none');
		$('#customMappingDetails').hide();
		$('#transferCustomMappingsLink').attr('style','display:none');
		
		$('#inputTab').hide();
	    $('#verifyTab').hide();
	    $('#confirmTab').hide();*/
		
	};


	/*
	 *  Load Externl Transfer Account Form
	 */
	$.subscribe('beforeLoadExternalTransferAccountForm', function(event,data) {
		$.log('About to load form! ');
		$('#details').slideUp();
		$('#quicksearchcriteria').hide();
		$("#summary").hide();
		// When user clicks calendar and then clicks new account link then this code is used to hide calendar
		if ($("#calendar").length > 0) {
			$('#goBackSummaryLink').click();
		}
	});
	
	$.subscribe('loadExternalTransferAccount', function(event, data) {
		ns.common.refreshDashboard('dbExternalAccountSummary');
		ns.common.selectDashboardItem("addExternalTransferAccountLink");
	});
	
	$.subscribe('loadExternalAccSummary', function(event,data) {
		ns.common.refreshDashboard('dbExternalAccountSummary');
		$('#details').hide();
		if($("#summary").not(':visible')){
			$("#summary").show();
		} 
	});
	$.subscribe('loadExternalTransferAccountComplete', function(event,data) {
		$.log('Form Loaded! ');
		//$('#extTransferAccountGridTabs').portlet('fold');
		$('#detailsPortlet').portlet('expand');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);

        ns.transfer.gotoStep(1);
		$('#details').slideDown();

	});
	
	
	$.subscribe('errorLoadExternalTransferAccountForm', function(event,data) {
		$.log('Form Loading error! ');

	});
	
	// Function to reload templates grid
	$.subscribe('cancelExternalTransferAccountForm', function(event,data) {
		$.log('About to cancel external transfer account Form! ');
		ns.transfer.closeExternalAccountTransferTabs();
		ns.transfer.reloadExternalAccounts();
		ns.common.hideStatus();
		//$('#extTransferAccountGridTabs').portlet('expand');
		$("#summary").show();
	});
	
	/*
	 * Add External Transfer Account form
	 */
	$.subscribe('beforeAddExtTransferAccountForm', function(event,data) {
		$.log('About to add form! ');
		removeValidationErrors();

	});

	$.subscribe('addExtTransferAccountFormComplete', function(event,data) {
		$.log('Form Verified! ');
		//$('#extTransferAccountGridTabs').portlet('fold');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);
//		ns.transfer.gotoStep(2);
	});

	$.subscribe('errorAddExtTransferAccountForm', function(event,data) {
		$.log('Form Adding error! ');
	});

	$.subscribe('successAddExtTransferAccountForm', function(event,data) {
		$.log('Form Adding success! ');
		ns.common.gotoWizardStep('detailsPortlet',2);
	});
	
	/*
	 * Verify External Transfer Account form
	 */
	$.subscribe('beforeVerifyExtTransferAccountForm', function(event,data) {
		$.log('About to verify form! ');
		removeValidationErrors();

	});

	$.subscribe('verifyExtTransferAccountFormComplete', function(event,data) {
		$.log('Form Verified! ');
		//$('#extTransferAccountGridTabs').portlet('fold');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);
//		ns.transfer.gotoStep(2);
	});

	$.subscribe('errorVerifyExtTransferAccountForm', function(event,data) {
		$.log('Form Verifying error! ');
	});

	$.subscribe('successVerifyExtTransferAccountForm', function(event,data) {
		$.log('Form Verifying success! ');
	});
	
	/*
	 * Deposit External Transfer Account form
	 */
	$.subscribe('beforeDepositExtTransferAccountForm', function(event,data) {
		$.log('About to deposit form! ');
		removeValidationErrors();

	});

	$.subscribe('depositExtTransferAccountFormComplete', function(event,data) {
		$.log('Form Verified! ');
		//$('#extTransferAccountGridTabs').portlet('fold');
		//set portlet title (defined in each page loaded to inputDiv by Id "PageHeading"
		var heading = $('#PageHeading').html();
		var $title = $('#details .portlet-title');
		$title.html(heading);
//		ns.transfer.gotoStep(2);
	});

	$.subscribe('errorDepositExtTransferAccountForm', function(event,data) {
		$.log('Form Depositing error! ');
	});

	$.subscribe('successDepositExtTransferAccountForm', function(event,data) {
		$.log('Form Depositing success! ');
		ns.common.gotoWizardStep('detailsPortlet',3);
	});
	
	
	/*
	 * Delete External Transfer Account form
	 */
	$.subscribe('deleteExtTransferAccountFormComplete', function(event,data) {
		$.log('Form Verified! ');
//		ns.transfer.gotoStep(2);
	});

	$.subscribe('errorDeleteExtTransferAccountForm', function(event,data) {
		$.log('Form Deleteing error! ');
	});

	$.subscribe('successDeleteExtTransferAccountForm', function(event,data) {
		$.log('Form Deleteing success! ');
		ns.common.closeDialog('#deleteExternalTransferAccountDialogID');
		ns.transfer.reloadExternalAccounts();
		ns.common.showStatus();
	});
	
	
	/**
	 * Close Details island and expand Summary island
	 */
	ns.transfer.closeExternalAccountTransferTabs = function(){
		//console.info("ns.transfer.closeExternalAccountTransferTabs");
		//$('#extTransferAccountGridTabs').portlet('expand');
		$('#transferTemplateGridTabs').portlet('expand');
		$('#transferGridTabs').portlet('expand');
	    $('#details').slideUp();
	}
	