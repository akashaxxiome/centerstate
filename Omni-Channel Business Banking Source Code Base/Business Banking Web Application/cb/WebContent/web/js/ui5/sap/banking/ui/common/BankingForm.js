/**
 * Control: BankingForm
 * This control is extension to SimpleForm, SAP online banking application uses Banking Form
 * maxContainerCols: 1,2 Supports to max 2 column form
 *
 * Form Conventions:
 * FormContainer: A container for grouping form fields, new container starts with defining new Title element
 * It will have title and multiple form rows
 *
 * Form Row:
 * A Form Row is composed of label and fields grouping, a new form row is considered whenever new label is defined in content
 * @Since 8.3SP03
 */
(function($, sap, window, undefined) {

    "use strict";

    //Declare namespace for control
    $.sap.declare("sap.banking.ui.common.BankingForm");

    //Define the BankingForm Control
    sap.ui.layout.form.SimpleForm.extend("sap.banking.ui.common.BankingForm", {
        renderer: {
            render: function(oRm, oControl) {
                console.log("render from inner");


                var oContent, i, oFormElement, oMetaData, oClassName;
                oContent = oControl.getContent();
                var oFormContainers = this._prepareFormObject(oContent);
                this.renderFormObject(oControl, oRm, oFormContainers);

            },

            renderTitle: function(oRm, oTitle) {
                if(oTitle){
                    var sTitle = oTitle.getText();
                    oRm.write("<div");
                    oRm.addClass("formTitle");
                    oRm.writeClasses();
                    oRm.write(">");
                    oRm.write("<span>" + sTitle + "</span>");
                    oRm.write("</div>");
                }
            },

            renderLabel: function(oRm, oLabel) {
                if(oLabel){
                    var sLabel = oLabel.getText();
                    oRm.write("<div");
                    oRm.addClass("formLabel");
                    oRm.writeClasses();
                    oRm.write(">");
                    oRm.renderControl(oLabel);
                    oRm.write("</div>");
                }                
            },

            renderField: function(oRm, oField) {
                if(oField){
                    oRm.write("<div");
                    oRm.addClass("sapUxFormField");
                    oRm.writeClasses();
                    oRm.write(">");
                    oRm.renderControl(oField);
                    oRm.write("</div>");
                }                
            },

            _prepareFormObject: function(oFormContent) {
                var _oFormContent = $.extend({}, oFormContent);
                var i, oFormContainers = [],
                    oFormContainer, oFormRow, oFormElement, oMetaData, oClassName;

                for (i = 0; i < oFormContent.length; i++) {
                    oFormElement = oFormContent[i];
                    oMetaData = oFormElement.getMetadata();
                    oClassName = oMetaData._sClassName;
                    if (oClassName === "sap.ui.core.Title") {
                        oFormContainer = {
                            "title": oFormElement,
                            "rows": []
                        }
                        oFormRow = {
                            "label": {},
                            "fields": []
                        }
                        oFormContainers.push(oFormContainer);
                    } else if (oClassName === "sap.ui.commons.Label") {
                        oFormRow = {
                            "label": oFormElement,
                            "fields": []
                        }
                        if(!oFormContainer){
                            //This form has now title
                            oFormContainer = {
                                "rows": []
                            }
                        }
                        oFormContainer.rows.push(oFormRow);
                    } else {
                        oFormRow.fields.push(oFormElement)
                    }
                };

                if (oFormContainers.length === 0) {
                    oFormContainer.rows = [oFormRow];
                    oFormContainers.push(oFormContainer);
                }

                return oFormContainers;
            },

            renderFormObject: function(oControl, oRm, oFormContainers) {

                var i, oFormContainer, oTitle, oRow, oRows, oField, oFields, oLabel, maxContainerCols, columnClass;

                maxContainerCols = oControl.mProperties.maxContainerCols;

                oRm.write("<div");
                oRm.writeControlData(oControl);
                oRm.addClass("sapUxBankingForm");
                oRm.writeClasses();
                oRm.write(">");

                for (var i = 0; i < oFormContainers.length; i++) {
                    oFormContainer = oFormContainers[i];
                    oTitle = oFormContainer.title;
                    if ((i % maxContainerCols) === 0) {
                        columnClass = "column0";
                    } else {
                        columnClass = "column1";
                    }

                    oRm.write("<div class='formContainer " + columnClass + "'>");
                    this.renderTitle(oRm, oTitle);
                    oRows = oFormContainer.rows;
                    for (var j = 0; j < oRows.length; j++) {
                        oRm.write("<div class='sapUxFormRow'>");
                        oRow = oRows[j];
                        oLabel = oRow.label;
                        oRm.write("<div class='sapUxFormFields'>");
                        this.renderLabel(oRm, oLabel);
                        oFields = oRow.fields;
                        for (var k = 0; k < oFields.length; k++) {
                            oField = oFields[k];
                            this.renderField(oRm, oField);
                        };
                        oRm.write("</div>");
                        oRm.write("</div>");
                    };
                    oRm.write("</div>");
                };

                oRm.write("</div>");
            }
        }
    });

}(jQuery, sap, window));