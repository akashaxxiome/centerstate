jQuery.sap.declare("sap.banking.ui.commons.Tab");
jQuery.sap.require("sap.ui.core.Control");

sap.ui.core.Control.extend("sap.banking.ui.commons.Tab", {      // call the new Control type "my.Hello" 
                                              // and let it inherit from sap.ui.core.Control
    metadata : {                              // the Control API
        properties : {
            "name" : "string"                 // setter and getter are created behind the scenes, 
                                              // including data binding and type validation
        },
       defaultAggregation : "content",
        aggregations : {
            "content" : {type : "sap.ui.core.Control", multiple : true, singularName : "content"}
        }
    },

    //TODO:onAfterRendering intilize jq call
    
    renderer : function(rm, oControl) {  
        console.info("render tab control");
        // render content
        var aContent = oControl.getContent();
        for ( var i = 0; i < aContent.length; i++) {
            rm.write("<div");
            rm.writeControlData(oControl);      
            rm.writeStyles();
            rm.writeClasses();
            rm.write(">"); // DIV element
            rm.renderControl(aContent[i]);
            rm.write("</div>");
        }
    }
});