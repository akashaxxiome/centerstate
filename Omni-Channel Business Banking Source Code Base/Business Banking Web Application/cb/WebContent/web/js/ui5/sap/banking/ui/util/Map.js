/**
* This is Map class used to hold key value pairs
*/
(function(window,undefined){
	jQuery.sap.declare("sap.banking.ui.util.Map");
	sap.banking.ui.util.Map = function(){
		var _map = {};		
		this.getMap = function(){
			return _map;
		}
	}

	sap.banking.ui.util.Map.prototype = {
		put: function(sKey,oValue){
			if(!sKey){
				throw new Error("Key can not be empty");
			}

			var map = this.getMap();
			map[sKey] = oValue;
		},

		get: function(sKey){
			var map = this.getMap();
			return map[sKey];
		},

		containsKey: function(sKey){
			if(typeof sKey !== "string"){
				throw new Error("Key should be string");
			}
			return Object.prototype.hasOwnProperty.call(this.getMap(),sKey);
		},

		keys: function(){
			return Object.keys(this.getMap());
		},

		remove: function(sKey){
			var map = this.getMap();
			delete map[sKey];
		}

	}
}(window));