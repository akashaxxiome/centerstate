/**
* Banking Validation Util Class
*/

jQuery.sap.declare("sap.banking.ui.util.Validator");

sap.banking.ui.util.Validator = (function($,window,undefined){

	var instance = {};

	var CONSTANTS = {
		ERROR_MODEL: "ERRORS",
		ERROR_TYPES: {
			VALIDATION_ERROR:"VE",
			SERVICE_ERROR:"SE",
			BUSINESS_ERROR:"BE"
		}
	};

	/***********************************************************
	*					Private functions
	************************************************************/

	/**
	* This function creates the error model and sets to passed view
	*/
	function setErrorModel(oErrors,oView){
		//console.info("instance.setErrorModel");
		if(oErrors && oView){
			var eModel = new sap.ui.model.json.JSONModel();				
			eModel.setData(oErrors);
			oView.setModel(eModel,CONSTANTS.ERROR_MODEL);			
		}
	}

	function removeEmptyBlankErrors(oErrors){
		var errors = {}
		for ( var prop in oErrors) {
			var aProp = $.trim(prop);
			var aValue = oErrors[aProp];
			if(aProp != "" && aValue != ""){
				errors[aProp] = aValue.Desc;
			}else if(aProp =="" && aValue !=""){
				errors["formError"] = aValue.Desc;
			}
		}
		return errors;
	}

	instance.showValidationError = function(validationResult){
		var control = null;
		for ( var prop in validationResult) {
			//Set Error CSS and tooltip
			control = sap.ui.getCore().byId(prop);
			if(control && control != null){
				control.setTooltip(validationResult[prop]);
				control.addStyleClass("validationError");
				if(typeof control.getValueState === 'function'){
					control.setValueState(sap.ui.core.ValueState.Error);
				}
			}
		}
	},
	
	instance.clearValidationError = function(view){
		var errorControls = $(".validationError");
		var counter = 0, id = null, control= null;
		for(counter = 0; counter < errorControls.length; counter++){
			id = errorControls[counter].id;
			control = sap.ui.getCore().byId(id);
			if(control){
				control.removeStyleClass("validationError");
				control.setTooltip("");
				if(typeof control.getValueState === 'function'){
					control.setValueState(sap.ui.core.ValueState.Success);
				}
			}
		}
	},
	
	instance.handleError = function(oError, oView){
		this.removeErrors(oView);
		var errorResponse = {};
    	//if(oError.response && oError.response.body && oError.response.body.length){
    		 //errorResponse = JSON.parse(oError.response.body);
    		 //Handle Validation Error
    		 if(oError.code === CONSTANTS.ERROR_TYPES.VALIDATION_ERROR){
    			 var validationErrorRes = JSON.parse(oError.message.value);
    			 
    			 var errors = [];
    			 for (var i in validationErrorRes.errorDetails){
    				 var item = validationErrorRes.errorDetails[i];
    				 
    				 errors[ item.FieldName ] = item.Desc
    			 }
    			 
    			 setErrorModel(errors,oView);
    			 //this.showValidationError(validationErrorRes.VE);
    		 } else {
    			 //Handle Service or Business Error show Message Toast
    			 var errorMsg = "";
    			 var validationErrorRes = JSON.parse(oError.message.value);
    			 if(oError.code === CONSTANTS.ERROR_TYPES.SERVICE_ERROR){
    				 errorMsg = validationErrorRes["errorMessage"];
    			 }else {
    				 errorMsg = validationErrorRes["errorMessage"];
    			 }
    			if(errorMsg === ""){
    				errorMsg = "Error in processing your request, please try again later!";
    			} 
    			ns.common.showError(errorMsg);
    		 }
    	//}
	},

	/**
	* This function removes the error model attached to view
	*/
	instance.removeErrors = function(oView){
		//console.info("removeErrorModel");
		if(oView){
			var viewModel = oView.getModel(CONSTANTS.ERROR_MODEL)
			if(viewModel){
				viewModel.setData({});	
			}	
		}			
	}


	return instance;

})(jQuery,window);