/**
 * Control: Lookupbox
 * This control is extension to autocomplete and provides below functionalities
 * 1. MaxItems
 * 2. Search Text Highlighting
 * 3. Arrow Button, which opens searched items
 * 4. If items are more than maxItems can be paged using more option
 * 5. Pairing functionality,when paired with other lookupbox can exclude the value  if its pair during search
 * 6. Default Option
 * 7. In edit mode retains the already set value
 * 8. Events provided: dataReceived, a listener can be passe to this event which will get invoked when data is received
 * maxPopupItems: 10,
 */
(function($, sap, window, undefined) {

    "use strict";

    //Declare namespace for control
    $.sap.declare("sap.banking.ui.common.LookupBox");

    //Provide the dependencies
    $.sap.require("sap.ui.commons.AutoComplete");

    //Extend the control with Lookupbox metadata
    sap.ui.commons.AutoComplete.extend("sap.banking.ui.common.LookupBox", {
        metadata: {
            properties: {
                "source": {
                    type: "string"
                },
                "searchField": {
                    type: "string",
                    defaultValue: "SearchText"
                },
                "controlType": {
                    type: "string",
                    defaultValue: "client"
                },
                "pairToControl": {
                    type: "object",
                    defaultValue: null
                },
                "delay": {
                    type: "int",
                    defaultValue: 500
                },
                "minLength": {
                    type: "int",
                    defaultValue: 1
                },
                "maxItems": {
                    type: "int",
                    defaultValue: 10
                },
                "defaultOption": {
                    type: "object",
                    defaultValue: null //TODO: Correct UI5 Type
                },
                "extraParams": {
                    type: "object",
                    defaultValue: null
                },
                "dataSourceKey": {
                    type: "string"
                },
                "dataSourceLabel": {
                    type: "string"
                }
            },
            events: {
                "dataReceived": {}
            }
        },

        logger: $.sap.log //window.console //$.sap.log
    });

    //Prtotoype Methods
    sap.banking.ui.common.LookupBox.prototype.init = function() {
        this.logger.info("init..");
        sap.ui.commons.AutoComplete.prototype.init.apply(this, arguments);
        this.mobile = false;
        this._filter = sap.ui.commons.AutoComplete._DEFAULTFILTER;
        this._skipFilter = true; //*Turning of client filtering
    };

    sap.banking.ui.common.LookupBox.prototype.onAfterRendering = function() {
        this.logger.info("onAfterRendering");
        //Check if has default option passed        
        if (this.getDefaultOption() !== null) {
            var oDefaultOption = this.getDefaultOption();
            if (oDefaultOption !== null) {
                this.setSelectedKey(oDefaultOption.getKey());
                //this.setSelectedItemId()
                this.setValue(oDefaultOption.getText());
            }
        }
    };

    sap.banking.ui.common.LookupBox.prototype.onclick = function(oEvent) {
        this.logger.info("onclick");
        //Open options list only when clicked on Lookupbox drop down arrow button
        var $sourceElement = $(event.target);
        if ($sourceElement.hasClass("sapUiTfComboIcon")) {
            var that = this;
            var oItems = this.getItems();
            if (oItems.length === 0) {
                var oSuggestEvent = {
                    suggestValue: "",
                    source: "onclick"
                };
                this.fireSuggest(oSuggestEvent);
            } else {
                sap.ui.commons.ComboBox.prototype.onclick.apply(that, arguments);
            }
        }
    };

    sap.banking.ui.common.LookupBox.prototype.fireChange = function(oEvent) {
        this.logger.info("fireChange");
        var oSelectedItem = oEvent.selectedItem;
        if (oSelectedItem !== null && oSelectedItem.getKey() === "Next") {
            this._moveToNextPage();
            this.setValue("");

            var oSuggestEvent = {
                suggestValue: ""
            };

            this.fireSuggest(oSuggestEvent);
        }
    };

    sap.banking.ui.common.LookupBox.prototype.fireSuggest = function(oEvent) {
        this.logger.info("fireSuggest");
        var sSearchText = oEvent.suggestValue;

        //Check if suggest value exceeds min length requirement to perform search, this is exepted from click event on Lookupbox
        /*if (oEvent.source !== "onclick" &&  (sSearchText.length < this.getMinLength())) {
            return;
        }*/

        this._search(sSearchText);
    };

    /*
     * Searches the back end service with search text passed
     * @param {string} sSearchText
     */
    sap.banking.ui.common.LookupBox.prototype._search = function(sSearchText, fnSuccess) {
        this.logger.info("_search");
        var that = this;
        var source = this.getSource();
        if (!source) {
            return;
        }

        var sServiceURI = this._buildSearchURI();
        //TODO: change CSRF refference
        $.ajax({
            url: sServiceURI,
            dataType: "json",
            global: false,
            headers: {
                "x-csrf-token": ns.home.getSessionTokenVarForGlobalMessage
            },
            success: function(oResponse) {
                that.logger.info("success");
                if (oResponse && oResponse.d && oResponse.d.results) {
                    that.destroyItems(); //forget the old items
                    var aData = oResponse.d.results;

                    //Process Data
                    var oData = that._processData(oResponse.d);

                    //Filter Data
                    var listItems = [];
                    //var filteredList = that._filterItems(oData, sSearchText);
                    listItems = that._createItems(oData);

                    //Add Items to prototype combo
                    that._addListItems(listItems);

                    if (fnSuccess) {
                        fnSuccess.apply(this, arguments);
                    }

                    if (that.fireDataReceived) {
                        var oParams = {
                            data: aData
                        };
                        that.fireDataReceived(oParams);
                    }
                }
            },
            error: function(oError) {
                that.logger.info("error", oError);
            }
        });
    };

    /**
     * This function build the URI for search, it adds basic oData Query Params for paging, pagesize
     * and also adds some additional extra params
     *  Refference URL
     *  http://localhost:8080/cb/odata/services/stopsservice/SearchStopPaymentAccounts?$top=100&SearchText='xx1'&$format=json&$inlinecount=allpages
     */
    sap.banking.ui.common.LookupBox.prototype._buildSearchURI = function() {
        this.logger.info("_buildSearchURI");
        var oParams = {};

        //ODATA Query Params for paging 
        var oPagingParams = this._getPagingParameters();

        //Search Param
        //var sSearchField = this.getSearchField(); //TODO: enable search when oData service is ready to take on!!!
        var oSearchParams = this.getExtraParams();
        oSearchParams = (oSearchParams === null) ? {} : oSearchParams;
        //oSearchParams[sSearchField] = this._sTypedChars;

        //Prepare the params finally
        oParams = $.extend({}, oPagingParams, oSearchParams);

        var searchURI = this.getSource() + "?" + $.param(oParams);

        return searchURI;
    };

    sap.banking.ui.common.LookupBox.prototype._setPagingParameters = function(oPagingParams) {
        this.logger.info("_setPagingParameters");
        this.pagingParameters = oPagingParams;
    };

    sap.banking.ui.common.LookupBox.prototype._getPagingParameters = function() {
        this.logger.info("_getPagingParameters");
        if (!this.pagingParameters) {
            //If not defined return the default
            this.pagingParameters = {
                $top: this.getMaxItems() || 10,
                $skip: 0, //TODO
                $format: "json",
                $inlinecount: "allpages"
            };
        }
        return this.pagingParameters;
    };

    sap.banking.ui.common.LookupBox.prototype._moveToNextPage = function() {
        this.logger.info("_moveToNextPage");
        var oPagingParams = this._getPagingParameters();
        var currentPage = oPagingParams.$skip;
        oPagingParams.$skip = ++currentPage;
        this._setPagingParameters(oPagingParams);
    };

    sap.banking.ui.common.LookupBox.prototype._moveToPreviousPage = function() {
        this.logger.info("_moveToPreviousPage");
        var oPagingParams = this._getPagingParameters();
        var currentPage = oPagingParams.$skip;
        oPagingParams.$skip = (currentPage <= 0) ? 0 : --currentPage;
        this._setPagingParameters(oPagingParams);
    };

    /**
     * Filter items based on search
     * TODO: Check control type property if its client then only filter
     */
    sap.banking.ui.common.LookupBox.prototype._filterItems = function(itmes, sSearchText) {
        this.logger.info("_filterItems");
        var filteredList = [];
        var dsKey = this.getDataSourceKey();
        var dsLabel = this.getDataSourceLabel();

        for (var i = 0; i < itmes.length; i++) {
            var sLabel = itmes[i][dsLabel];
            if (sSearchText && $.sap.startsWithIgnoreCase(sLabel, sSearchText)) {
                filteredList.push(new sap.ui.core.ListItem({
                    text: sLabel
                }));
            } else {
                filteredList.push(new sap.ui.core.ListItem({
                    text: sLabel
                }));
            }
        }
        return filteredList;
    };


    /**
     * Prepare lookupbox items
     * TODO: Check control type property if its client then only filter
     */
    sap.banking.ui.common.LookupBox.prototype._createItems = function(itmes) {
        this.logger.info("_createItems");
        var listItems = [];

        //Check if control is set with default option
        var oDefaultOption = this.getDefaultOption();
        if (null !== oDefaultOption) {
            listItems.push(oDefaultOption.clone());
        }

        var dsKey = this.getDataSourceKey();
        var dsLabel = this.getDataSourceLabel();
        
        var sLabel, sKey;
        for (var i = 0; i < itmes.length; i++) {
            sLabel = itmes[i][dsLabel];
            sKey = itmes[i][dsKey];
            listItems.push(new sap.ui.core.ListItem({
                text: sLabel,
                key: sKey
            }));
        }

        this._excludePairedItem(listItems);

        return listItems;
    };

    sap.banking.ui.common.LookupBox.prototype._excludePairedItem = function(listItems) {
        this.logger.info("_excludePairedItem");
        //Check if control is set with default option
        var oPairControl = this.getPairToControl();
        var bExcludeItem = false;
        var excludeItemKey = "";
        var index;
        if (null !== oPairControl) {
            excludeItemKey = oPairControl.getSelectedKey();
        }

        for (var i = 0; i < listItems.length; i++) {
            var sKey = listItems[i].getKey();
            if (sKey === excludeItemKey) {
                bExcludeItem = true;
                index = i;
                break;
            }
        }

        if (bExcludeItem) {
            listItems.splice(index, 1);
        }

        return listItems;
    };
    /**
     * This function processed data before adding to lookupbox
     */
    sap.banking.ui.common.LookupBox.prototype._processData = function(oData) {
        this.logger.info("_processData");
        var totalCount,
            processedData = [];

        if (oData.__count && oData.results) {
            totalCount = oData.__count;
            processedData = oData.results;

            var oPagingParams = this._getPagingParameters();
            var currentPage = oPagingParams.$skip;
            var pageSize = oPagingParams.$top;

            //Determine if there are more pages
            var totalPages = Math.ceil(totalCount / pageSize);
            if (currentPage < totalPages) {
                var oPagingItem = this._createPagingItem();
                processedData.push(oPagingItem);
                var maxItems = this.getMaxItems() + 1; //TODO: reset back when no more pages
                this.logger.info("maxPopupItems", maxItems);
                this.setMaxPopupItems(maxItems);
            }
        }
        return processedData;
    };

    /**
     * This function returs the paging option item
     */
    sap.banking.ui.common.LookupBox.prototype._createPagingItem = function() {
        this.logger.info("_createPagingItem");
        
        var dsKey = this.getDataSourceKey();
        var dsLabel = this.getDataSourceLabel();

        var oPagingItem = {};
        oPagingItem[dsKey] = "Next";
        oPagingItem[dsLabel] = "..more.."; //TODO: i18n

        return oPagingItem;
    };


    /**
     * Add items to list
     */
    sap.banking.ui.common.LookupBox.prototype._addListItems = function(oItems) {
        this.logger.info("_addItems");
        var i;
        for (i = 0; i < oItems.length; i++) {
            //this.addStyleClass("sapUiTfFoc");
            this.addItem(oItems[i]);
        }
    };

    /**
     * Highlight the search text
     * TODO: Currently UI5 do not supports html with text properties
     * This prevents passing text with html content in it.
     */
    sap.banking.ui.common.LookupBox.prototype._highlight = function(sLabel, sValue) {
        this.logger.info("_highlight");
        sLabel = sLabel.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + sValue + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
        //sLabel = escape(sLabel);
        return sLabel;
    };

    //Destroy the control, binding if any
    sap.banking.ui.common.LookupBox.prototype.exit = function() {
        this.logger.info("exit");
        sap.ui.commons.AutoComplete.prototype.exit.apply(this, arguments);
    };



    /************************************************************************************************/
    /* Overriding Prototypes For Autocomplete                                                       */
    /* The default behavior of Autocomplete is that it only searches when there is atleast one      */
    /* character typed in, Lookupbox provides feature of opening options list on arrow button click */
    /* For it to work Autocomplete methods have been overriden for Lookupbox                        */
    /* NOTE: When framework is upgraded, Make sure to verify Lookupbox                              */
    /************************************************************************************************/
    sap.banking.ui.common.LookupBox.prototype._fireLiveChange = function(oEvent) {
        this.logger.info("_fireLiveChange");
        var bFireSuggest = false;
        if (!this.getEnabled() || !this.getEditable()) {
            this._close();
        } else {
            this._sTypedChars = $(this.getInputDomRef()).val();
            switch (oEvent.type) {
                case "keyup":
                    if (!sap.ui.commons.ComboBox._isHotKey(oEvent)) {
                        var iKC = oEvent.which || oEvent.keyCode;
                        if (iKC === $.sap.KeyCodes.ESCAPE) {
                            this._close();
                            break;
                        } else {
                            bFireSuggest = true;
                        }
                    } else {
                        break;
                    }
                default: //paste or no hotkey or not escape
                    this.refreshListBoxItems(this);
                    bFireSuggest = true;
            }
        }

        if (bFireSuggest) {
            this.fireSuggest({
                suggestValue: this._sTypedChars
            });
        }

        sap.ui.commons.ComboBox.prototype._fireLiveChange.apply(this, arguments);
    };

    sap.banking.ui.common.LookupBox.prototype._doTypeAhead = function() {
        this.logger.info("_doTypeAhead");
        this._sTypeAhead = null;
        this._sWantedSelectedKey = undefined;
        this._sWantedSelectedItemId = undefined;
        this._sTypedChars = $(this.getInputDomRef()).val();

        this.refreshListBoxItems(this);
    };

    sap.banking.ui.common.LookupBox.prototype.insertItem = function(oItem, iIndex) {
        this.logger.info("insertItem");
        this.insertAggregation("items", iIndex, oItem, true);
        this.refreshListBoxItems(this);
        return this;
    };

    sap.banking.ui.common.LookupBox.prototype.addItem = function(oItem) {
        this.logger.info("addItem");
        this.addAggregation("items", oItem, true);
        this.refreshListBoxItems(this);
        return this;
    };

    sap.banking.ui.common.LookupBox.prototype.removeItem = function(oItem) {
        this.logger.info("removeItem");
        var res = this.removeAggregation("items", oItem, true);
        this.refreshListBoxItems(this);
        return res;
    };

    sap.banking.ui.common.LookupBox.prototype.removeAllItems = function() {
        this.logger.info("removeAllItems");
        var res = this.removeAllAggregation("items");
        this.refreshListBoxItems(this);
        return res;
    };

    sap.banking.ui.common.LookupBox.prototype.indexOfItem = function(oItem) {
        this.logger.info("indexOfItem");
        return this.indexOfAggregation("items", oItem);
    };

    sap.banking.ui.common.LookupBox.prototype.destroyItems = function() {
        this.logger.info("destroyItems");
        this.destroyAggregation("items", true);
        this.refreshListBoxItems(this);
        return this;
    };

    sap.banking.ui.common.LookupBox.prototype.setEnableScrolling = function(bEnableScrolling) {
        this.logger.info("setEnableScrolling");
        this.setProperty("enableScrolling", bEnableScrolling, true);
        if (this.oPopup && this.oPopup.isOpen()) {
            this.refreshListBoxItems(this);
        }
        return this;
    };

    sap.banking.ui.common.LookupBox.prototype.refreshListBoxItems = function(oAuto) {
        this.logger.info("refreshListBoxItems");
        /*if(!oAuto.getDomRef() || !oAuto.$().hasClass("sapUiTfFoc")){ //Nothing to do if not rendered or the TF does not have the focus
            return false;
        }*/

        var oItem,
            aItems = oAuto.getItems(),
            //bFilter = oAuto._sTypedChars && oAuto._sTypedChars.length > 0,
            bFilter = true,
            oLB = oAuto._getListBox(),
            iMaxPopupItems = oAuto.getMaxPopupItems(),
            bScroll = oAuto.getEnableScrolling(),
            aHitItems = [];


        if (!bFilter) {
            oAuto._close();
            return;
        }

        oLB.removeAllItems();
        oLB.clearSelection();

        for (var i = 0; i < aItems.length; i++) {
            oItem = aItems[i];
            if (!oItem.__CLONE) {
                oItem.__CLONE = oItem.clone(oItem.getId() + "-CLONE", null, {
                    cloneBindings: false
                });
                oItem.__origexit = oItem.exit;
                oItem.exit = function() {
                    this.__CLONE.destroy();
                    delete this.__CLONE;
                    this.exit = this.__origexit;
                    delete this.__origexit;
                    // apply restored exit if a function
                    if (typeof this.exit === "function") {
                        this.exit.apply(this, arguments);
                    }
                };
            }

            if ((!bFilter || oAuto._filter(oAuto._sTypedChars, oItem)) && (bScroll || (!bScroll && aHitItems.length < iMaxPopupItems))) {
                aHitItems.push(oItem.__CLONE);
            }
        }

        var iItemsLength = aHitItems.length;

        if (iItemsLength > 0) {

            if (oAuto._sort) {
                aHitItems.sort(function(oItem1, oItem2) {
                    if (oItem1.getText() > oItem2.getText())
                        return 1;
                    if (oItem1.getText() < oItem2.getText())
                        return -1;
                    return 0;
                });
            }

            for (var i = 0; i < iItemsLength; i++) {
                oLB.addItem(aHitItems[i]);
            }

            oLB.setVisibleItems(iMaxPopupItems < iItemsLength ? iMaxPopupItems : iItemsLength);

            if (!oAuto.oPopup || !oAuto.oPopup.isOpen()) {
                oAuto._open();
            }
        } else {
            oAuto._close();
        }
    };
    /********************** End of Overrides on Autocomplete Prototypes *************************/


}(jQuery, sap, window));