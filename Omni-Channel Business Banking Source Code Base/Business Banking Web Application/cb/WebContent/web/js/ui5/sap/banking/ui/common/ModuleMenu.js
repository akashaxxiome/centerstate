/**
 * SAP UI5 Custom Control for Settings Menu
 * This control provides quick settings for portlet,
 * Available options would be shown on mouse hover action
 *
 * This is composite control created in UI5 which uses hover button and menu
 * options available when user hovers over button
 *
 * TODO: MenuItems is deprecated from commons, need to bring in unified library
 */
(function($, sap, window, undefined) {
    "use strict";

    //Declare the namespace
    $.sap.declare("sap.banking.ui.common.ModuleMenu");

    //Load dependencies
    $.sap.require("sap.banking.ui.common.HoverButton");

    //Extend the contol, This is composite one
    sap.ui.core.Control.extend("sap.banking.ui.common.ModuleMenu", {
        metadata: {
            properties: {
                "visible": {
                    type: "boolean",
                    group: "Appearance",
                    defaultValue: true
                }
            },
            defaultAggregation: "items",
            aggregations: {
                "items": {
                    type: "sap.ui.commons.MenuItemBase",
                    multiple: true,
                    singularName: "item"
                }
            },
            publicMethods: [
                "setSelectedItem"
            ]
        },

        /**
         * Init function
         * This takes care of initializing hover button and menu control
         */
        init: function() {
            var that = this;

            //Hover Button with gear icon
            this.oHoverButton = new sap.banking.ui.common.HoverButton({
                icon: "sap-icon://settings",
            }).addStyleClass("sapUiPortletSettingsIcon");

            //Menu Control
            this.oMenu = new sap.ui.commons.Menu({
                "text": "Stops"
            }).addStyleClass("sapUxModuleMenuIcon");

            //Menu will be shown when user hovers over button
            this.oHoverButton.attachHover(function(oEvent) {
                var eDock = sap.ui.core.Popup.Dock;
                that.oMenu.open(
                    false /*First item already highlighted*/ ,
                    that.oHoverButton.getFocusDomRef() /*Dom reference which gets the focus back when the menu is closed*/ ,
                    eDock.BeginTop, /*"Edge" of the menu (see sap.ui.core.Popup)*/
                    eDock.BeginBottom, /*"Edge" of the related opener position (see sap.ui.core.Popup)*/
                    that.oHoverButton.getDomRef() /*Related opener position (see sap.ui.core.Popup)*/
                );
            })
        },

        /**
         * Renderer function which renders the composite control
         * SettingMenu Control will just renders hover button
         */
        renderer: function(oRenderManager, oControl) {
            var controlId = oControl.getId();

            // return immediately if control is invisible
            if (!oControl.getVisible()) {
                return;
            }

            // convenience variable
            var rm = oRenderManager;
            rm.write("<div class='todoModuleMenuContainer'>");
            rm.renderControl(oControl.oHoverButton);
            rm.write("</div>");
        },

        /**
         * OnAfterRendering, life cycle method
         * Menu items are only added when control is rendered
         */
        onAfterRendering: function() {
            var menuItems = this.getItems();
            for (var i = 0; i < menuItems.length; i++) {
                this.oMenu.addItem(menuItems[i]);
            }
        },

        setSelectedItem: function(sItemId) {
            console.log("setSelectedItem");
            var menuItems = this.getItems();
            var oItemToSelect;
            for (var i = 0; i < menuItems.length; i++) {
                if (menuItems[i].getId() === sItemId) {
                    oItemToSelect = menuItems[i];
                }
            }

            if (oItemToSelect) {
                this.oMenu.selectItem(sItemId);
            }
        }
    });
}(jQuery, sap, window));