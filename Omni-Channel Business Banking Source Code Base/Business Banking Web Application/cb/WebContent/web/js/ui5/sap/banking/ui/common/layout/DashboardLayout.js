sap.ui.layout.HorizontalLayout.extend("sap.banking.ui.common.layout.DashboardLayout", { // call the new Control type "HighlightField" 
    // and let it inherit from sap.ui.commons.TextField

    renderer: function(oRenderManager, oControl) {
        // return immediately if control is invisible
        if (!oControl.getVisible()) {
            return;
        }

        // convenience variable
        var rm = oRenderManager;
        var bNoWrap = !oControl.getAllowWrapping();

        // write the HTML into the render manager
        rm.write("<div");
        rm.writeControlData(oControl);
        rm.addClass("sapUxDashboardLayout");
        if (bNoWrap) {
            rm.addClass("sapUiHLayoutNoWrap");
        }
        rm.writeClasses();
        rm.write(">"); // div element

        var aChildren = oControl.getContent();
        for (var i = 0; i < aChildren.length; i++) {
            var oFirstControlClass = "sapUxDashboardLeftElementHolder";
            if (i == 0) {
                oFirstControlClass = "sapUxDashboardLeftElementHolder";
            } else {
                oFirstControlClass = "";
            }
            if (bNoWrap) {
                rm.write("<div class='sapUxDashboardElement " + oFirstControlClass + "'>");
            }
            rm.renderControl(aChildren[i]);
            if (bNoWrap) {
                rm.write("</div>");
            }
        }

        rm.write("</div>");
    }
});