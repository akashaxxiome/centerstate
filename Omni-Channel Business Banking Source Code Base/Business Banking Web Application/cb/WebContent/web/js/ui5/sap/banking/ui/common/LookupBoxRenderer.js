// Provides default renderer for control sap.ui.commons.AutoComplete
jQuery.sap.declare("sap.banking.ui.common.LookupBoxRenderer");
jQuery.sap.require("sap.ui.commons.AutoCompleteRenderer");
jQuery.sap.require("sap.ui.core.Renderer");
jQuery.sap.require("sap.ui.commons.ComboBoxRenderer");

/**
 * @class Renderer for the sap.ui.commons.AutoComplete
 * @static
 */
sap.banking.ui.common.LookupBoxRenderer = sap.ui.core.Renderer.extend(sap.ui.commons.AutoCompleteRenderer);

sap.banking.ui.common.LookupBoxRenderer.renderExpander = function(rm, oCtrl){
	sap.ui.commons.ComboBoxRenderer.renderExpander.apply(this, arguments);
	/*if(!oCtrl.__sARIATXT) {
		var rb = sap.ui.getCore().getLibraryResourceBundle("sap.ui.commons");
		oCtrl.__sARIATXT = rb.getText("AUTOCOMPLETE_ARIA_SUGGEST");
	}
	
	rm.write("<span id=\"", oCtrl.getId(), "-ariaLbl\" style=\"display:none;\">", oCtrl.__sARIATXT, "</span>");*/
};






