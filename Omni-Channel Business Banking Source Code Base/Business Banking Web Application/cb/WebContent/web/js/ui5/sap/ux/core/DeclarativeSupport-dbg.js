jQuery.sap.declare("sap.ux.core.DeclarativeSupport");
jQuery.sap.require("sap.ui.core.DeclarativeSupport");


/**
 * Enhances the given DOM element by parsing the Control and Elements info and creating
 * the SAPUI5 controls for them.
 * 
 * @param {DomElement} oElement the element to compile
 * @param {sap.ui.core.mvc.HTMLView} [oView] The view instance to use
 * @param {boolean} [isRecursive] Whether the call of the function is recursive.
 * @public
 */
sap.ui.core.routing.Route.prototype.compile = function(oElement, oView, isRecursive) {
	// Find all defined classes
	var self = this;
	jQuery(oElement).find("[data-sap-ui-type]").filter(function() {
		return jQuery(this).parents("[data-sap-ui-type]").length === 0;
	}).each(function() {
		self._compile(this, oView, true);
	});
};