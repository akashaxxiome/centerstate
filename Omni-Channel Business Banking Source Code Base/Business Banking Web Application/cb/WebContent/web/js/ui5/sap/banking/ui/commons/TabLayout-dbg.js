jQuery.sap.declare("sap.banking.ui.commons.TabLayout");
jQuery.sap.require("sap.ui.core.Control");

sap.ui.core.Control.extend("sap.banking.ui.commons.TabLayout", {
                                              
    metadata : {                              
        properties : {
            "name" : "string"
        },
        defaultAggregation : "tabs",
		aggregations : {
	    	"tabs" : {type : "sap.banking.ui.commons.Tab", multiple : true, singularName : "tab"}
		}
    },
    
    widget: undefined /* jQuery widget instance for UI5 control */,

    renderer : function(oRm, oControl) {// the part creating the HTML   
        var aTabs = oControl.getTabs();
        if(aTabs.length>0){
            oRm.write("<div id='" + oControl.getId() + "'>");
            //oRm.writeControlData();            
            oRm.write("<ul>");
            for (var i = 0; i < aTabs.length; i++) {
                aTabs[i]
                oRm.write("<li>");
                oRm.write("<a href='#" + aTabs[i].getId() + "'>");                
                oRm.writeEscaped(aTabs[i].getName());
                oRm.write("</a>");
                oRm.write("</li>");
                
            };
            oRm.write("</ul>");
            for (var i = 0; i < aTabs.length; i++) {
                sap.banking.ui.commons.TabLayout.renderTabContents(oRm,aTabs[i]);
            };
            oRm.write("</div>");            
        }

        
        //oRm.writeEscaped(oControl.getName()); // write the Control property 'name', with XSS protection        
    },

    onAfterRendering: function(){        
        console.info("onAfterRendering");
        var $tabs = $("#"+ this.sId).tabs();
        this.setWidget($tabs);
    },

    /**
    * This methods returns the jquery widget instance for this widget
    */
    //TODO: could be moved to base control class
    getWidget: function(){
        return this.widget;
    },

    setWidget: function(obj){
        this.widget = obj
    }
});

sap.banking.ui.commons.TabLayout.renderTabContents = function(rm, oControl) {    
    rm.renderControl(oControl);    
}