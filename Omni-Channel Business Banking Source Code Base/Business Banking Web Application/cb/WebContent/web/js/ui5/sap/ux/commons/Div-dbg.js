jQuery.sap.declare("sap.ux.commons.Div");

sap.ui.core.Control.extend("sap.ux.commons.Div", {      // call the new Control type "my.Hello" 
                                              // and let it inherit from sap.ui.core.Control
    metadata : {                              // the Control API
        properties : {
            "name" : "string"                 // setter and getter are created behind the scenes, 
                                              // including data binding and type validation
        }
    },

    renderer : function(oRm, oControl) {      // the part creating the HTML
        oRm.write("<span>Hello ");
        oRm.writeEscaped(oControl.getName()); // write the Control property 'name', with XSS protection
        oRm.write("</span>");
    }
});