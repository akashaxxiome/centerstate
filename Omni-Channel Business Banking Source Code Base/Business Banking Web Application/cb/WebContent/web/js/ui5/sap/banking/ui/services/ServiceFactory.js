/**
 * Description: Service Factory implementation, which takes care of finding appropriate service instance
 * loading its Service class. It also maintains pool of all singleton instance of services which are loaded on demand
 */

(function($, window, undefined) {
    $.sap.declare("sap.banking.ui.services.ServiceFactory");
    $.sap.require("sap.banking.ui.util.Map");

    sap.banking.ui.services.ServiceFactory = (function() {

        var instance = {};

        //Service instance map
        var serviceMap = new sap.banking.ui.util.Map();

        /**
         * Function returns the service instance
         * @param {String} Service Name
         * @params{Object} Service Parameters
         */
        function _getService(sServiceName, sParams) {
            console.info("_getService " + sServiceName);
            if (!sServiceName) {
                throw new Error("Missing service name");
            }

            if (sServiceName.indexOf(".") >= 0) {
                throw new Error("Unsupported service name");
            }

            var service, serviceInstance;
            if (!serviceMap.containsKey(sServiceName)) {
                //If service is not loaded before it would be loaded synchronously 
                //and executed and its singleton instance is stored in services map
                var sModule = "sap.banking.ui.services." + sServiceName;
                $.sap.require(sModule);
                service = $.sap.getObject(sModule);
                if (!service) {
                    throw new Error("Error in getting service instance");
                }
                serviceInstance = service.getInstance();
                serviceMap.put(sServiceName, serviceInstance);
            }
            service = serviceMap.get(sServiceName);
            return service;
        };

        function _getServiceInstance(sServiceName, sParams) {
            console.info("_getService " + sServiceName);
            if (!sServiceName) {
                throw new Error("Missing service name");
            }

            if (sServiceName.indexOf(".") >= 0) {
                throw new Error("Unsupported service name");
            }

            var service, serviceInstance;

            var sModule = "sap.banking.ui.services." + sServiceName;
            $.sap.require(sModule);
            Service = $.sap.getObject(sModule);
            if (!Service) {
                throw new Error("Error in getting service instance");
            }

            var serviceInstance = new Service(sParams);
            return serviceInstance;
        };



        /**
         * Returns the service instace for passed service name
         */
        instance.getService = function(sServiceName, sParams) {
            return _getService(sServiceName, sParams);
        }

        /**
         * API which creates service instance, this is non singleton implementation
         */
        instance.getServiceInstance = function(sServiceName, sParams) {
            return _getServiceInstance(sServiceName, sParams);
        }

        return instance;
    }());
}(jQuery, window));