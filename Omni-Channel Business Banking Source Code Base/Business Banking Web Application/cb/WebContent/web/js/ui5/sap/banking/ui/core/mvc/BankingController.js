/**
* Banking Controller, which is base base class of all controllers inside SAP Online Banking UI
* This controller defines commonoly used instance properties and methods which extening class gets
* to reuse
*/

//Class NameSpace declaration 
jQuery.sap.declare("sap.banking.ui.core.mvc.BankingController");

//CLass Dependencies
jQuery.sap.require("sap.ui.core.mvc.Controller");
jQuery.sap.require("sap.banking.ui.services.ServiceConstants");
jQuery.sap.require("sap.banking.ui.services.BankingService");
jQuery.sap.require("sap.banking.ui.services.ServiceFactory");
jQuery.sap.require("sap.banking.ui.common.Common");
jQuery.sap.require("sap.banking.ui.common.Help");

//Class Definition
sap.ui.core.mvc.Controller.extend("sap.banking.ui.core.mvc.BankingController", {

    //Banking Service class instance
    service: null,

    //Router instance
    router: null,

    //i18n resource model
    resourceModel: null,

    //Logger instance
    logger: null,

    //Common instance
    common: null,

    //Common instance
    help: null,


    /**
    * Constructor
    * this initializes logger,common module
    */
    constructor: function(){
        this.logger = jQuery.sap.log;
        this.logger.info("initializing banking controller");
        this.common = sap.banking.ui.common.Common; 
        this.help = sap.banking.ui.common.Help;       
    },

    /********************************************
    * Life cycle methods for controller			*
    ********************************************/
    /**
    * Initializes logging, common modules
    */
    onInit: function() {        
        //this.logger.info("super init");
        console.info("super init");
    },

    /**
     * Frees up the instance variables
     */
    onExit: function() {
        this.logger.info("onExit banking controller");
        this.service = null;
        this.router = null;
        this.resourceModel = null;
        this.logger = null;
        this.common = null;
    },

    /********************************************
     * Banking methods definitions				*
     ********************************************/
    /**
     * Initializes service instance
     * @param {serviceName} string, Name of service to be initialzed
     *	Service Names are defined in ServiceConstants file, Constants are defined under
     *	sap.banking.ui.services.ServiceConstants
     */
    initService: function(serviceName) {
        if (!serviceName) {
            throw new Error("Service name is required!!!");
        }
        this.service = sap.banking.ui.services.ServiceFactory.getService(serviceName);
    },

    /**
     * Initializes the router
     * @param {routerName} string, A router name for module, a router names can be found from
     *	sap.banking.ui.routing.Routers
     */
    /*initRouter: function(routerName) {
        if (!routerName) {
            throw new Error("Router name is required to initialze!!!");
        }
        this.router = sap.banking.ui.core.routing.ApplicationRouter.getRouter(routerName);
    },*/

    /**
     * Initializes the resource model
     * if name of resource file is not passed it will initialze with common resources
     * @param {resourceFile | optional} string, A resource file name
     */
    initResourceModel: function(resourceFile) {
        if (!resourceFile) {
            this.logger.info("Resource file not defined,intializing resource model with common properties");
            resourceFile = "common";
        }
        this.resourceModel = this.common.getResourceModel(resourceFile + ".properties");
    },

    /**
     * Getter for service instance
     * @returns Object instance of type sap.banking.ui.services.BankingService
     */
    getService: function() {
        return this.service;
    },

    resetHashPath: function(){
        //sap.banking.ui.common.Common.getRouter().navOut();
        window.location.hash = "";
    },
    /**
     * Getter for router
     */
    getRouter: function() {
        this.router = sap.ui.core.UIComponent.getRouterFor(this);
    },

    /**
     * Getter for resource model
     */
    getResourceModel: function() {
        //return this.resourceModel;
        //Not Required when SAPui5 upgraded to 1.23.0
        var oResourceModel = sap.banking.ui.common.Common.getComponentResourceModel();
        return oResourceModel;
    },

    /**
     * Getter for common module
     */
    getCommon: function() {
        return this.common;
    },

    onShowHelp: function(){
        this.help.show();
    }
});