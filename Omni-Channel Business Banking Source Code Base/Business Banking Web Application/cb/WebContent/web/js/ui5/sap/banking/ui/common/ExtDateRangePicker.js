/**
 * Extended Date Range Picker
 * Provides user to select date range and predefined ranges
 * Shows calendar
 * This control initializes Extended Date Range Picker jQuery control
 */
(function($, sap, window, undefined) {
    "use strict";

    $.sap.declare("sap.banking.ui.common.ExtDateRangePicker");

    sap.ui.commons.TextField.extend("sap.banking.ui.common.ExtDateRangePicker", {

        metadata: {
            properties: {
                "url": {
                    type: "string"
                },
                "startDateBox": {
                    type: "object"
                },
                "endDateBox": {
                    type: "object"
                }
            }
        },

        onAfterRendering: function() {
            //this.logger.info("onAfterRendering");
            var sId = this.getId();
            var aConfig = {
                url: this.getUrl(),
                startDateBox: $("#" + this.getStartDateBox().getId()),
                endDateBox: $("#" + this.getEndDateBox().getId())
            };

            $("#" + sId).extdaterangepicker(aConfig);
        },

        renderer: {} /*This has no renderer*/

    });
}(jQuery, sap, window));