/**
 * SAPUI5 custom hover control
 * This control extends the Button control and provides hover effect to button
 */
(function($, sap, window, undefined) {
    "use strict";

    $.sap.declare("sap.banking.ui.common.HoverButton");

    sap.ui.commons.Button.extend("sap.banking.ui.common.HoverButton", {
        metadata: {
            events: {
                "hover": {} // this Button has also a "hover" event, in addition to "press" of the normal Button
            }
        },

        // the hover event handler:
        onmouseover: function(evt) { // is called when the Button is hovered - no event registration required
            this.fireHover();
        },

        renderer: {} // add nothing, just inherit the ButtonRenderer as is; 
        // In this case (since the renderer is not changed) you could also specify this explicitly with:  renderer:"sap.ui.commons.ButtonRenderer"
        // (means you reuse the ButtonRenderer instead of creating a new view
    });
}(jQuery, sap, window));