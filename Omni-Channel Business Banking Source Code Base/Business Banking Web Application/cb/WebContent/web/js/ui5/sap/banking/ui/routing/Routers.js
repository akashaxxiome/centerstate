/**
* Routers Class
* This is static class defining Router names as constants
*/
jQuery.sap.declare("sap.banking.ui.routing.Routers");
(function(window,undefined){
	sap.banking.ui.routing.Routers = {
		APPLICATION:"application",
		DEVICEMANAGEMENT:"deviceManagement",
		CHARTS:"charts"
	}
})(window);