jQuery.sap.declare("sap.ux.core.mvc.BankingHTMLView");
jQuery.sap.require("sap.ui.core.mvc.HTMLView");

sap.ui.core.mvc.View.extend("sap.ux.core.mvc.BankingHTMLView", {});
(function(){
	sap.ux.core.mvc.BankingHTMLView.prototype.onControllerConnected = function(oController) {
			// unset any preprocessors (e.g. from an enclosing HTML view)
			var self = this;
			sap.ui.base.ManagedObject.runWithPreprocessors(function() {
				sap.ui.core.DeclarativeSupport.compile(self._oTemplate, self,true); //pass true as recursive
			});
	};
});
