/**
* AppRouter Class
*/

/*jQuery.sap.declare("sap.banking.ui.core.routing.AppRouter");
jQuery.sap.require("sap.ui.core.routing.Router");
jQuery.sap.require("sap.banking.ui.core.routing.ApplicationRouter");*/



/**
* UI5 router needs to have view of its own type, below method  is
* overridden to support any html container for view placement 
*/
sap.ui.define(['jquery.sap.global','sap/ui/core/routing/async/Route', 'sap/ui/core/routing/sync/Route'], function(jQuery, asyncRoute, syncRoute) {
	"use strict";

	var _originalRouteMatched = syncRoute._routeMatched;
	syncRoute._routeMatched = function(oArguments, bInital, oNestingChild) {
		var oView,
			oParentInfo, 
			oTargetParent,
			oTargetControl,
			oRouter = this._oRouter,
			oConfig =  jQuery.extend({}, oRouter._oConfig, this._oConfig);
		
		if(oConfig["views"]){//Kept for older implementation
			if (this._oParent) {
				oParentInfo = this._oParent._routeMatched(oRouter, oArguments);
				
				oTargetParent = oParentInfo.oTargetParent;
				oTargetControl = oParentInfo.oTargetControl;
				
			}
			$.each(oConfig.views,function(key,value){
				var sViewName = value.view;
				if (value.viewPath) {
					sViewName = value.viewPath + "." + sViewName;
				}
				oView = oRouter.getView(sViewName, value.viewType);
	            if(oView && oView["getController"] && oView.getController()["onBeforeShow"]){
	            	oView.getController().onBeforeShow();
	            }
				oView.placeAt(value.div,"only");
			});
		}
	_originalRouteMatched.apply(this, arguments);
	};
});