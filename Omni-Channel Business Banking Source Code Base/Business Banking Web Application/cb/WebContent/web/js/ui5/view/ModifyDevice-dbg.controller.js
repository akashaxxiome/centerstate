/**
* Controller for add device wizard
*/
jQuery.sap.require("sap.banking.ui.services.DeviceManagementService");
jQuery.sap.require("sap.banking.ui.services.ServiceConstants");
sap.ui.controller("view.ModifyDevice",{	
	
	service: undefined,

	currentStep:0,

	data:undefined,

	$tabWiget:undefined,

	onInit: function(){
		console.info("AddDeviceController-onInit");
		this.initializeService();
		this.getView().setBusy(true);
		var aData = {
			"name":"",	
			"country":"",
			"number":"",
			"carrier":"",
			"terms":"",
			"countries":[{
				"name":"USA",
				"carriers":[]
				},{
				"name":"IN",
				"carriers":[]}],
			"carriers":[{
					"id":1,
					"name":"AT&T"
				},{
					"id":2,
					"name":"TMOBILE"
				},{
					"id":3,
					"name":"VERIZON"
				},{
					"id":4,
					"name":"SPRINT"
				},{
					"id":5,
					"name":"VODAFONE"
				},{
					"id":6,
					"name":"AIRTEL"
				}]
		};

		this.data = aData;

		var oModel = new sap.ui.model.json.JSONModel();
		oModel.setData(aData);
		this.getView().setModel(oModel);

		//Init events
		var oBus = sap.ui.getCore().getEventBus();		
		oBus.subscribe("ModifyDeviceChannel","editDetails",this.editDeviceListner);
	},

	/**
	* This function initializes the service instance for device managent
	*/	
	initializeService: function(){
		var constant = sap.banking.ui.services.ServiceConstants;
		var uri = constant.deviceManagement.URI;
		var userName = "username";
		var password = "password";
		this.service = new sap.banking.ui.services.DeviceManagementService(uri,userName,password);
	},

	//TODO: can this fit for banking view
	initializeJQWidgets: function(){
		//TODO:Initialize the portlet for details
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		
	},

	onBeforeRendering: function(){
		console.info("AddDeviceController-onBeforeRendering");
		//this.getView().setBusy(true);
	},

	onAfterRendering: function(){
		console.info("AddDeviceController-onAfterRendering");
		this.initializeJQWidgets();		
		this.getView().setBusy(false);
	},

	verifyDeviceHandler:function(){
		this.service.verifyDevice();
		this.moveToNextStep();
	},

	saveDeviceHandler:function(){
		this.moveToNextStep();
	},

	confirmDeviceHandler: function(){
		//TODO:close the wizard and change the view
	},

	cancelWizardHandler:function(){
		//TODO: clear the data model and change the  view
	},

	backHandler: function(){
		this.moveToPrevStep();
	},

	moveToPrevStep:function(){
		this.currentStep--;
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		
	},

	moveToNextStep:function(){
		this.currentStep++;
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		
	},

	getTabWidget: function(){
		return this.getView().byId("deviceWizard").getWidget();
	},

	/** Global Events	*/
	editDeviceListner: function(sChannelId, sEventId, oData){
		console.info("editDeviceListner" +  oData);
		/*var oModel = new sap.ui.model.json.JSONModel();
		oModel.setData(oData);
		wizardView.setModel(oModel);*/

	},

	onExit: function(){
		console.info("AddDeviceController-onExit");
	}	
});