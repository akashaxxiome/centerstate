/**
* Controller for charts
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.AssetAccountsDonutChart",{

	model:undefined,
	
	onInit: function(){
		jQuery.sap.log.info("onInit");
		this.initResourceModel("charts");
		this.initializei18NModel();
	},

	initializei18NModel: function(){	
		//Register device i18n model
		this.getView().setModel(this.resourceModel, "i18n");
	},
	
	// call the action and copies data in model 
	initializeModel:function(){
		var reportId = $('#reportOn').val() || "";
		var displayCurrencyCode = $('#AccountDisplayCurrencyCode').val() || "";
		var test = "/cb/pages/jsp/account/GetConsolidateBalanceAssetsAccountAction.action?dataClassification="+reportId+"&displayCurrencyCode="+displayCurrencyCode+"&isChart=true";
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		 var oConfig = {
				url:test,
				success: function(data) {
					hasData=false;
					// convert data as required
					var newDataModel = controllerHandler.updateAmounts(data.gridModel);
					
					// set data in json model
					controllerHandler.model.setData({
	                      businessData : newDataModel
					 });
					
					// set data in view
					controllerHandler.getView().setModel(controllerHandler.model);
				},
				error : function(jqXHR, sTextStatus, oError) {
					console.info(sTextStatus);
				},
				complete : function(jqXHR,textStatus){
					// once the call is complete render the donut
					controllerHandler.renderDonut();
				}
			};
			
		 this.getChartData(oConfig);
	},

	renderDonut: function(){
	  		// pie charts holder
	  		var pieContainer = this.byId("pieContainerAssetAccount");
	  		// div holder
	  		var messageHolder = this.byId("textPieAssetAccount");
	  		// note holder
	  		var noteHolder = this.byId("donutChartNoteLabel");
	  		
	  		var oTextView = new sap.ui.commons.TextView();
			 // while data is loading
			 oTextView.setText("Please wait we are loading you chart...");
			 if(hasData){
					messageHolder.setVisible(false);
					var dimensionName = 'Debits';
					var oChartConfig = {
						chartName : "Asset Accounts Balance",
						legendSectionName : "Account Group",
						legendValueProperty : "map/label",
						dataValueProperty : "marketValueCurrency/amountValue",
						dataSectionName : "Debits",
						dataPath : "/businessData"
					}
					var oPieChart = this.createDonutChart(oChartConfig);
					pieContainer.destroyContent(); 
					pieContainer.addContent(oPieChart);
			 }else{
				 pieContainer.setWidth("0%");
				 noteHolder.setVisible(false);
			 }
	},
	
	hasData : false,
	
	updateAmounts : function(data) {
		for(var i=0;i < data.length; i++) {
			var model = data[i];
			var value = 0;
			if(model.marketValue){
				value= model.marketValue;
				hasData=true;
			}
			/*if(model.marketValue){
				var amountNoComma = model.marketValue.replace(/,/g, "");
				data[i].marketValue = amountNoComma;
			}else{
				data[i].marketValue = 0;
			}
			if(data[i].marketValue>0){
				hasData=true;
			}*/
			var label=  model.account.accountDisplayText + " : " + value;
			model.map["label"]=label;
		}
		return data;
	},

	onAfterRendering: function(){
		jQuery.sap.log.info("onAfterRendering");
	},
	
	onBeforeRendering: function(){
		jQuery.sap.log.info("onBeforeRendering donut");
		this.initializeModel();
	},
	
	onExit: function(){
		jQuery.sap.log.info("onExit");
	}	
});