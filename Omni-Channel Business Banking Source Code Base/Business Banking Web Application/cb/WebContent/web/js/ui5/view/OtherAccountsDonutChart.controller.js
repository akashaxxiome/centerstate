/**
* Controller for charts
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.OtherAccountsDonutChart",{

	model:undefined,
	
	onInit: function(){
		this.initResourceModel("charts");
		this.initializei18NModel();
	},

	initializei18NModel: function(){	
		//Register device i18n model
		this.getView().setModel(this.resourceModel, "i18n");
	},
	
	// call the action and copies data in model 
	initializeModel:function(){
		var reportId = $('#reportOn').val() || "";
		var displayCurrencyCode = $('#AccountDisplayCurrencyCode').val() || "";
		var test = "/cb/pages/jsp/account/GetConsolidatedBalanceOtherAction.action?dataClassification="+reportId+"&displayCurrencyCode="+displayCurrencyCode+"&isChart=true";
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		var oConfig = {
				url:test,
				dataType: 'json',
				success: function(data) {
					hasData=false;
					// convert data as required
					var newDataModel = controllerHandler.updateAmounts(data.gridModel);
					
					// set data in json model
					controllerHandler.model.setData({
	                      businessData : newDataModel
					 });
					
					// set data in view
					controllerHandler.getView().setModel(controllerHandler.model);
				},
				error : function(jqXHR, sTextStatus, oError) {
					console.info(sTextStatus);
				},
				complete : function(jqXHR,textStatus){
					// once the call is complete render the donut
					controllerHandler.renderDonut();
				}
			};
			
		this.getChartData(oConfig);
	},

	renderDonut: function(){
	  		// pie charts holder
	  		var pieContainer = this.byId("pieContainerOtherAccount");
	  		// div holder
	  		var messageHolder = this.byId("textOtherAccount");
	  		// note holder
	  		var noteHolder = this.byId("donutChartNoteLabel");
	  		
	  		var oTextView = new sap.ui.commons.TextView();
			 // while data is loading
			 oTextView.setText("Please wait we are loading you chart...");
			 if(hasData){
					messageHolder.setVisible(false);
					var dimensionName = 'Debits';
					var oChartConfig = {
						chartName : "Other Accounts Balance",
						legendSectionName : "Account Group",
						legendValueProperty : "map/label",
						dataValueProperty : "totalDebitsCurrency/amountValue",
						dataSectionName : "Debits",
						dataPath : "/businessData"
					}
					var oPieChart = this.createDonutChart(oChartConfig);
					pieContainer.destroyContent(); 
					pieContainer.addContent(oPieChart);
			 }else{
				 pieContainer.setWidth("0%");
				 noteHolder.setVisible(false);
			 }
			
	},
	
	hasData : false,
	
	updateAmounts : function(data) {
		for(var i=0;i < data.length; i++) {
			var model = data[i];
			var debt = 0;
			//totalDebits value is extracting from currency so as to reflect actual value.
			if(model.totalDebits){
			debt = model.totalDebits;
			hasData=true;
			}
			/*if(model.totalDebits){
				var indexOfBlank = model.totalDebits.indexOf(" (");
				var originalString = model.totalDebits.substring(0,indexOfBlank);
				var amountNoComma = originalString.replace(/,/g, "");
				data[i].totalDebits = amountNoComma;
			}else{
				data[i].totalDebits = 0;
			}
			if(data[i].totalDebits>0){
				hasData=true;
			}*/
			var label=  model.account.accountDisplayText + " : " + debt;
			model.map["label"]=label;
		}
		return data;
	},

	onAfterRendering: function(){
		jQuery.sap.log.info("onAfterRendering");
	},
	
	onBeforeRendering: function(){
		jQuery.sap.log.info("onBeforeRendering donut");
		this.initializeModel();
	},
	
	onExit: function(){
		jQuery.sap.log.info("onExit");
	}	
});