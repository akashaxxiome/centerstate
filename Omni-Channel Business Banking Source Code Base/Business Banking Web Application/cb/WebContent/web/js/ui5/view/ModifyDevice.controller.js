/**
* Modify Device Controller Class
* This class handles the modify device functionality in wizard
* It calls the DeviceManagementService for communicating to oData REST
* service
* It uses DeviceManagementService
*/
jQuery.sap.require("sap.banking.ui.services.ServiceFactory");
jQuery.sap.require("sap.banking.ui.services.DeviceManagementService");
jQuery.sap.require("sap.banking.ui.services.ServiceConstants");
jQuery.sap.require("sap.banking.ui.util.Validator");
jQuery.sap.require("sap.banking.ui.common.Common");
sap.ui.controller("view.ModifyDevice",{	
	
	i18NModel: undefined,
	
	common: null,

	service: null,

	sheduleService: null,

	oCountriesModel:null,

	oCountriesList: null,

	validator:null,

	currentStep:0,

	data:{
		MobileDeviceAlertSettings:{}
	},

	$tabWiget:undefined,

	that:this,

	onInit: function(){
		this.common = sap.banking.ui.common.Common;
		this.getView().setBusy(true);
		this.initializeService();
		this.initializeModel();
		this.intializeEvents();

		//Route matched event
		var routers = sap.banking.ui.routing.Routers;		
		var router =  sap.banking.ui.core.routing.ApplicationRouter.getRouter("deviceManagement");
		router.attachRouteMatched(function(oEvent) {
			if (oEvent.getParameter("name") !== "_editDevice") {
				return; //we only want to react to events for the specificProductRoute
			}
			        //We now know we hit the specificProduct route and retrieve the id
			this._selectItemWithId(oEvent.getParameter("arguments").id);
			  //bind the this pointer to the callback
		}, this);
	},

	_getDeviceSuccessHandler: function(d,id){	
	},


	_getDeviceErrorHandler: function(e){
	},

	_selectItemWithId: function(deviceId) {
		var that = this;
		var oEntry = {
			"DeviceId": deviceId,
			__metadata:{
				"type" : "com.sap.banking.mobile.endpoint.v1_0.beans.MobileDevice"
			}
		};	

		//Service invocation for init device bean
		var fnInitSuccess = function(response){
			if(response){
				that.initDeviceProperties(response);
				var jModel = new sap.ui.model.json.JSONModel();				
				jModel.setData(response);
				that.getView().setModel(jModel);

				//Select time zone with default job timezone
				var timeZoneId = response.MobileDeviceAlertSettings.NoAlertTimezone;
				that.byId("timeZone").setSelectedKey(timeZoneId);

				that.setNoAlertStartTime(response.MobileDeviceAlertSettings.NoAlertStartTime);
				that.setNoAlertEndTime(response.MobileDeviceAlertSettings.NoAlertEndTime);

				that.validator.removeErrors(that.getView());
			}				
		}

		var fnInitError = function(error){
			console.error(error);
		}
		
		this.service.initDevice(oEntry,fnInitSuccess,fnInitError);
	},

	/**
	* This function initializes the service instance for device managent
	*/	
	initializeService: function(){
		var CONSTANTS = sap.banking.ui.services.ServiceConstants.DEVICEMANAGEMENTSERVICE;
		this.service = sap.banking.ui.services.ServiceFactory.getService(CONSTANTS.NAME);

		//Schedule service is required for getting timezones
		var SCHEDULE_CONSTANTS = sap.banking.ui.services.ServiceConstants.SCHEDULESERVICE;
		this.sheduleService = sap.banking.ui.services.ServiceFactory.getService(SCHEDULE_CONSTANTS.NAME);
	},

	/**
	* This function initializes the device model and i18n model required for the view
	*/
	initializeModel: function(){
		var that = this;
		//Register device i18n model
		var i18NModel = this.common.getResourceModel("devicemanagement.properties");	
		this.getView().setModel(i18NModel,"i18n");
		this.i18NModel = i18NModel;
			
		//Get Countries and Carriers
		var fnCountriesSuccess = function(countries){
			if(countries){
				var countriesModel = new sap.ui.model.json.JSONModel();
				that.oCountriesList = countries.results;
				countriesModel.setData(countries);
				that.getView().setModel(countriesModel,"COUNTRIES");				
			}				
		}

		var fnCountriesError = function(error){
			console.error(error);
		}

		this.service.getCountries(fnCountriesSuccess,fnCountriesError);

		
		//Get timeszones
		/*var timeZones = this.service.getTimeZones();
		var timeZonesModel = new sap.ui.model.json.JSONModel();
		timeZonesModel.setData(timeZones);
		this.getView().setModel(timeZonesModel,"TIMEZONES");*/
		var fnTZSuccess = function(response){
			if(response){
				var timeZones = response.results;
				var timeZonesModel = new sap.ui.model.json.JSONModel();
				timeZonesModel.setData(timeZones);
				that.getView().setModel(timeZonesModel,"TIMEZONES");
			}				
		}

		var fnTZError = function(error){
			console.error(error);
		}
		
		this.sheduleService.getTimeZones(fnTZSuccess,fnTZError);		

		this.validator = sap.banking.ui.util.Validator;
		this.validator.removeErrors(this.getView());
	},

	initDeviceProperties: function(device){
		if(device.MobileDeviceAlertSettings.NoAlertStartTime == ""){
			device.MobileDeviceAlertSettings.NoAlertStartTime = null;
		}
		if(device.MobileDeviceAlertSettings.NoAlertEndTime == ""){
			device.MobileDeviceAlertSettings.NoAlertEndTime = null;
		}
	},

	/**
	* This function initializes the events
	*/
	intializeEvents: function(){
		//console.info("intializeEvents");
	},

	//TODO: can this fit for banking view
	initializeJQWidgets: function(){		
		$("#details").show();
		
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		

		//Hide the registered devices portlet bug 773596-:Folding doesn't work here, so we have applied hiding the div.
		//$("#registeredDevicesGridContainer").portlet("fold");
		$("#registeredDevicesGridContainer").hide();
		//Initialize the portlet
		var portletId = "#" + this.getGeneratedId("detailsPortlet");
		$(portletId).addClass("wizardSupportCls");
		$(portletId).css("margin-top", "20px");
		ns.common.initializePortlet(portletId, true);
		
		ns.common.updatePortletTitle(this.getGeneratedId("detailsPortlet")+"_portlet", this.i18NModel.getResourceBundle().getText("MODIFY_DEVICE_PORTLET_TITLE"), false);
		
		/*var portletId = "#" + this.getGeneratedId("detailsPortlet");
		$(portletId ).portlet({
			bookmark: false,
			bookMarkCallback: function(){
				var path = $(portletId).find('.shortcutPathClass').html();
				var ent  = $(portletId).find('.shortcutEntitlementClass').html();
				ns.shortcut.openShortcutWindow( path, ent );
			},
			title: this.i18NModel.getResourceBundle().getText("MODIFY_DEVICE_PORTLET_TITLE"),
			generateDOM: true,
			helpCallback: function(){
				var helpFile = $('#summary').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
		});*/
		
		// Updating wizard color status
		ns.common.gotoAddDeviceWizardStep(this.getGeneratedId("detailsPortlet"), 1);

		//Make Start/End Date fields datepicker widgets
		var startDate = this.getGeneratedId("disableAlertStartDate");
		var endDate = this.getGeneratedId("disableAlertEndDate");

		if(startDate){
			$("#" + startDate).datepicker({
		      showOn: "button",
		      buttonImage: "images/calendar.gif",
		      buttonImageOnly: true
		    });
		}

		if(endDate){
			$("#" + endDate).datepicker({
		      showOn: "button",
		      buttonImage: "images/calendar.gif",
		      buttonImageOnly: true
		    });
		}
	},

	/**
	* BUG: Attaching live change events here, since  there is some data binding issue with time fields
	* may be mix approach with jQuery widgets,declarative HTML view and custiom tab control is causing some influence 
	* in data binding
	*/
	registerEvents: function(){
		var that = this;
		this.byId("noAlertStartTime").attachLiveChange(function(oEvent){			
			that.setNoAlertStartTime(oEvent.getParameters().liveValue);
		});

		this.byId("noAlertEndTime").attachLiveChange(function(oEvent){			
			that.setNoAlertEndTime(oEvent.getParameters().liveValue);
		});		
	},

	setNoAlertStartTime: function(value){
		if(this.data.MobileDeviceAlertSettings === undefined){
			this.data.MobileDeviceAlertSettings = {}
		}
		this.data.MobileDeviceAlertSettings.NoAlertStartTime = value;
	},

	setNoAlertEndTime: function(value){
		if(this.data.MobileDeviceAlertSettings === undefined){
			this.data.MobileDeviceAlertSettings = {}
		}
		this.data.MobileDeviceAlertSettings.NoAlertEndTime = value;
	},

	onBeforeRendering: function(e){
		if(mainApp.helpModule){
			mainApp.helpModule.setCurrentActiveHash(hasher.getHashAsArray());
		}
		this.currentStep = 0;
		this.resetHashPath();
		var oView = this.getView();
		var data = oView.getModel().getData();
		var token = data.AcceptLoginToken;
		this.enableDisableAcceptToken(token);
		
	},

	onAfterRendering: function(){
		this.initializeJQWidgets();
		this.registerEvents();		
		this.getView().setBusy(false);
	},

	resetHashPath: function(){
	var routers = sap.banking.ui.routing.Routers;
		sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.DEVICEMANAGEMENT).navOut();
	},
	enableDisableAcceptToken: function(value){
                if(value == true){
				   this.byId("acceptLoginTokenId").setEnabled(false);
				}else {
				   this.byId("acceptLoginTokenId").setEnabled(true);
				}
    },
	countriesChangeHandler: function(e){
		var params = e.getParameters();
		if(params){
			var selectedCountry = params.newValue;
			var carriersModel = new sap.ui.model.json.JSONModel();
			var carriers;
			var len = this.oCountriesList.length;
			for(var i = 0; i < len; i++){
				if(this.oCountriesList[i].Code == selectedCountry){
					var mobileCarriers = this.oCountriesList[i].MobileCarriers;
					if(mobileCarriers){
						carriers = mobileCarriers;
					}
					break;
				}
			}				
			carriersModel.setData(carriers);
			this.getView().setModel(carriersModel,"CARRIERS");	
		}
	},
	verifyDeviceHandler:function(){
		var that = this;		
		var oView = this.getView();
		var oEntry = oView.getModel().getData();		
		var oData = $.extend({},oEntry);
		//oEntry.MSISDN = "1" + oEntry.SubscriberNumber;
		oData = this.updateDateProperties(oData);
		this.updateTimeZone(oData);
		
		var fnSuccess = function(response){
			// Updating wizard color status
			ns.common.gotoAddDeviceWizardStep(that.getGeneratedId("detailsPortlet"), 2);
			that.moveToNextStep();
		}

		var fnError = function(error){
			console.error("Error in getting response: " + error);
			that.validator.handleError(error, oView);
		}


		this.service.verifyDevice(oEntry,fnSuccess,fnError);
	},

	saveDeviceHandler:function(){
		var that = this;		
		var oView = this.getView();
		var data = oView.getModel().getData();
		var oEntry = {};
		oEntry.DeviceId = data.DeviceId;

		var fnSuccess = function(response){
			// Updating wizard color status
			ns.common.gotoAddDeviceWizardStep(that.getGeneratedId("detailsPortlet"), 3);
			that.moveToNextStep();
		}

		var fnError =  function(error){
			that.validator.handleError(error, oView);
		}
		this.service.confirmDevice(oEntry,fnSuccess,fnError);		
	},

	confirmDeviceHandler: function(){
		var message = this.i18NModel.getResourceBundle().getText("DEVICE_MODIFY_SUCCESS_MESSAGE");
		ns.common.showStatus(message);
		var that = this;
		setTimeout(function(){
			that.cancelWizardHandler.apply(that);
		},300);	
	},

	cancelWizardHandler:function(){
		var portletId = "#" + this.getGeneratedId("detailsPortlet");
		$(portletId).portlet("fold");
		$("#registeredDevicesGridContainer").show();
		ns.common.refreshDashboard('dbMobileDevicesSummary');
		$("#details").hide();
		$("#summary").show();

		var router =  sap.banking.ui.core.routing.ApplicationRouter.getRouter("deviceManagement");
		router.navOut();

		var registeredDevicesController = registeredControllers.get("view.RegisteredDevices");
		if(registeredDevicesController){
			registeredDevicesController.reloadGridData();
		}
	},

	backHandler: function(){
		this.moveToPrevStep();
	},

	moveToPrevStep:function(){
		this.currentStep--;
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		
		this.validator.removeErrors(this.getView());
	},

	moveToNextStep:function(){
		this.currentStep++;
		this.getTabWidget().tabs("disable").tabs("enable",this.currentStep).tabs("option", "active", this.currentStep);		
		this.validator.removeErrors(this.getView());
	},

	getTabWidget: function(){
		return this.getView().byId("deviceWizard").getWidget();
	},

	/** Global Events */
	editDeviceListner: function(sChannelId, sEventId, oData){
	},

	/**
	* This function returns dynamically generated id for passed dom id
	* @param {String} sDomId dom id defined in HTML view
	*/
	getGeneratedId: function(sDomId){
		//console.info("getGeneratedId");
		var element = this.getView().byId(sDomId);
		var id;
		if(element){
			id = element.sId;
		}
		return id;
	},

	onExit: function(){
		//TODO: Free JQ widgets here
	},

	/****************** Helper Functions ******************************/
	deviceStatusFormatter: function(status){
		var sStatus = this.i18NModel.getResourceBundle().getText("DEVICE_STATUS_INACTIVE");
		if(status == "1"){
			sStatus = this.i18NModel.getResourceBundle().getText("DEVICE_STATUS_ACTIVE");
		}
		if(sStatus == "3"){
			sStatus = that.i18NModel.getResourceBundle().getText("DEVICE_STATUS_RESTRICTED");		
		}
		return sStatus;
	},

	tAndFFormatter: function(value){
		if(value === "T"){
			return true;
		}else{
			return false;
		}
	},

	updateDateProperties: function(device){
		var sDate,sDateId,startDate,eDate,eDateId,endDate;
		sDateId = this.getGeneratedId("disableAlertStartDate");
		eDateId = this.getGeneratedId("disableAlertEndDate");

		var sDate = $("#" + sDateId).val();
		var eDate = $("#" + eDateId).val();

		//startDate = new Date(sDate);
		//endDate = new Date(endDate);
		device.MobileDeviceAlertSettings.NoAlertStartDate = sDate;
		device.MobileDeviceAlertSettings.NoAlertEndDate = eDate;

		//Update time properties
		device.MobileDeviceAlertSettings.NoAlertStartTime = this.data.MobileDeviceAlertSettings.NoAlertStartTime;
		device.MobileDeviceAlertSettings.NoAlertEndTime = this.data.MobileDeviceAlertSettings.NoAlertEndTime;

		//Update the verify fields
		this.byId("verifyNoAlertStartDate").setText(sDate);
		this.byId("verifyNoAlertEndDate").setText(eDate);	
		this.byId("verifyNoAlertEndDate").setText(eDate);	
		this.byId("verifyNoAlertStartTime").setText(device.MobileDeviceAlertSettings.NoAlertStartTime);	
		this.byId("verifyNoAlertEndTime").setText(device.MobileDeviceAlertSettings.NoAlertEndTime);	
					
		return device;
	},

	updateTimeZone: function(device){
		var timeZoneDropDown = this.byId("timeZone");
		var selectedTimeZoneId = timeZoneDropDown.getSelectedKey();
		var selectedValue = timeZoneDropDown.getValue();
		device.MobileDeviceAlertSettings.NoAlertTimezone = selectedTimeZoneId;
		this.byId("verifyDeviceTimeZone").setText(selectedValue);
	},

	/** Event handlers **/
	noAlertDateRangeChangeHandler: function(oEvent){
		var params = oEvent.getParameters();
		if(params.checked == false){
			this.byId("disableAlertStartDate").setValue('');
			this.byId("disableAlertEndDate").setValue('');

			this.byId("verifyNoAlertStartDate").setText('');
			this.byId("verifyNoAlertEndDate").setText('');			
		}		
	},

	noAlertTimeRangeChangeHandler: function(oEvent){
		var params = oEvent.getParameters();
		if(params.checked == false){
			this.data.MobileDeviceAlertSettings.NoAlertStartTime = null;
			this.data.MobileDeviceAlertSettings.NoAlertEndTime = null;

			this.byId("noAlertStartTime").setValue('');
			this.byId("noAlertEndTime").setValue('');

			this.byId("verifyNoAlertStartTime").setText('');
			this.byId("verifyNoAlertEndTime").setText('');
		}
	}
});