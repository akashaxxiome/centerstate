/**
* Controller for charts
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.CreditCardsBarChart",{

	model:'',
	
	onInit: function(){
	},

	initializeModel:function(){
		var reportId = $('#reportOn').val() || "";
		var displayCurrencyCode = $('#AccountDisplayCurrencyCode').val() || "";
		var test = "/cb/pages/jsp/account/GetConsolidatedBalanceCcardAction.action?dataClassification="+reportId+"&displayCurrencyCode="+displayCurrencyCode+"&isChart=true";
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		var oConfig = {
			url:test,
			dataType: 'json',
			success: function(data) {
				hasData=false;
				console.log("ajax success");
				// convert data as required
				var newDataModel = controllerHandler.updateAmounts(data.gridModel);
				// set data in json model
				controllerHandler.model.setData({
                      businessData : newDataModel
				 });
				// set data in view
				controllerHandler.getView().setModel(controllerHandler.model);
			},
			error : function(jqXHR, sTextStatus, oError) {
				console.info(sTextStatus);
			},
			complete:function(jqXHR, textStatus){
				controllerHandler.renderBarChart();
			}
		};
		this.getChartData(oConfig);
	},

	renderBarChart : function(){
		 var dimensionName = 'Debits';
		 var barContainer = this.byId("creditCardAccBarContainer");
		 var messageHolder = this.byId("textCreditCardAccBar");
				
				var oTextView = new sap.ui.commons.TextView();
				oTextView.setText("Please wait we are loading you chart...");
				if(hasData){
					messageHolder.setVisible(false);
					var oBarChartConfig = {
					  		chartName : "Credit Cards Balance",
					  		x_axisName : "Accounts Group",
					  		y_axisName : "Debits",
					  		x_axisDataProperty : "map/label",
					  		y_axisDataProperty : "displayAmtDue/amountValue",
					  		dataPath : "/businessData"
					  }
					 
					var oBarChart = this.createColumnChart(oBarChartConfig);
					barContainer.destroyContent(); 
					barContainer.addContent(oBarChart);
				}else{
					barContainer.setWidth("0%");
				}
	},
	
	hasData : false,
	
	updateAmounts : function(data) {
		for(var i=0;i < data.length; i++) {
			var model = data[i];
			var amtDue = 0;
			//amtDue value is extracting from currency so as to reflect actual value.
			if(model.displayAmtDue){
				amtDue = model.displayAmtDue.amountValue;
				hasData=true;
			}
			/*if(model.amountDue){
				var amountNoComma = model.amountDue.replace(/,/g, "");
				data[i].amountDue = amountNoComma;
			}else{
				data[i].amountDue = 0;
			}
			if(data[i].amountDue>0){
				hasData=true;
			}*/
			var label=  model.account.nickName + " : " + amtDue;
			model.map["label"]=label;
		}
		return data;
	},

	onBeforeRendering: function(){
		window.location.hash = "";
		this.initializeModel();
	},

	onAfterRendering: function(){
	},

	onExit: function(){
	}	
});