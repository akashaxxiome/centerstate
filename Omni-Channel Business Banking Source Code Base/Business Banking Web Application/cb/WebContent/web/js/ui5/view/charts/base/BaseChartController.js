/**
* BaseChartController which will have all the common chart related methods.
*/
//Class NameSpace declaration 
jQuery.sap.declare("view.charts.base.BaseChartController");

//Class Dependencies
jQuery.sap.require("sap.banking.ui.core.mvc.BankingController");

//Class Definition
sap.banking.ui.core.mvc.BankingController.extend("view.charts.base.BaseChartController", {
	getChartData : function(oConfig){
		var oBaseConfig  = {
				url : "",
				dataType : "json",
				containerId : "",
				global : false,
				success : function(data){
					console.info("Call to url->" + oBaseConfig.url +" is successful");
				},
				error : function(jqXHR, sTextStatus, oError){
					console.info(sTextStatus);
				},
				complete : function(jqXHR,textStatus){
					console.info("Call to url->" + oBaseConfig.url +" is complete");
				}
		};
		var oFinalConfig = jQuery.extend(oBaseConfig, oConfig);
		var ajaxSettings = ns.common.applyLocalAjaxSettings(oFinalConfig);
		$.ajax(ajaxSettings);
	},
	
	createDonutChart : function(oConfig){
		/*{
			chartName : "",
			legendSectionName : "",
			dataSectionName : "",
			legendValueProperty : "displayTextAccountsType",
			dataValueProperty : "balanceAmountValue",
			dataPath : "/businessData"
		}*/
		if(!oConfig.legendValueProperty || !oConfig.dataValueProperty || !oConfig.dataPath){
			throw new Error("Wrong input for Donut Chart! Following fields are mandatory -> legendValueProperty, dataValueProperty and dataPath.");
		}
		var dimensionName = oConfig.legendSectionName ? oConfig.legendSectionName :  "Dimensions";
		var measureName = oConfig.dataSectionName ? oConfig.dataSectionName :  "measures";
		var oDataSet = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [new sap.viz.ui5.data.DimensionDefinition({
							name : dimensionName, 
							value : this.getCorrectBindingProperty(oConfig.legendValueProperty)
						})],
						
			measures : [new sap.viz.ui5.data.MeasureDefinition({
								name : measureName,
								value : this.getCorrectBindingProperty(oConfig.dataValueProperty)
						})],
			
			data : { path : this.getDataPath(oConfig.dataPath)}
		}); 
  		
  		var arrFeeds = [];
  		var oFeedItem = new sap.viz.ui5.controls.common.feeds.FeedItem({
  			uid : "color",
  			type : "Dimension",
  			values : [dimensionName],
  		});
  		arrFeeds.push(oFeedItem);
  		oFeedItem = new sap.viz.ui5.controls.common.feeds.FeedItem({
  			uid : "size",
  			type : "Measure",
  			values : [measureName],
  		});
  		arrFeeds.push(oFeedItem);

		//var oPopOver = new sap.viz.ui5.controls.Popover();
		var oPieChart = new sap.viz.ui5.controls.VizFrame({
			height : "300px",
			width : "100%",
			vizType : "donut",
		});
		var oVizProperties = {
			plotArea: {
            	//colorPalette : d3.scale.category20().range(),
            	drawingEffect: "glossy",
            	innerRadiusRatio : 0.40
            },
            legend : {
            	isScrollable : true	
            },
            legendGroup: {
            	layout : {
            		//position : "bottom",
            		width : "35%",
            		alignment : "center"
            	},
            	linesOfWrap : 2            	
            },
          	interaction:{
        		selectability:{
        			mode:"NONE"
        		}
          	},
		};
		
		if(oConfig.chartName){
			oVizProperties["title"] = {
				text : oConfig.chartName
			};
		}
		
		oPieChart.setVizProperties(oVizProperties);
		/*oPieChart.setVizScales({
			color : {
				palette : ["#748CB2", "#9CC677", "#EACF5E", "#F9AD79", "#D16A7C", "#8873A2", "#3A95B3", "#B6D949", "#FDD36C", "#F47958", "#A65084", "#0063B1", "#0DA841", "#FCB71D", "#F05620", "#B22D6E", "#3C368E", "#8FB2CF", "#95D4AB", "#EAE98F", "#F9BE92", "#EC9A99", "#BC98BD", "#1EB7B2", "#73C03C", "#F48323", "#EB271B", "#D9B5CA", "#AED1DA", "#DFECB2", "#FCDAB0", "#F5BCB4"]
			}
		});*/
		
		oPieChart.setDataset(oDataSet);
  		oPieChart.addFeed(arrFeeds[0]);
  		oPieChart.addFeed(arrFeeds[1]);
  		return oPieChart;
	},
	
	createColumnChart : function(oConfig){
		/*
		 * {
		 * 		chartName : "",
		 * 		x_axisName : "",//Horizontal axis
		 * 		y_axisName : "",//Vertical axis
		 * 		x_axisDataProperty : "",
		 * 		y_axisDataProperty : "",
		 * 		y_axisMultipleDataArray : [], // Supposed to contain information for more than one measure. e.g. OpeningBal and ClosingBal etc.
		 * 		dataPath : ""
		 * }
		 */
		/*[{
			y_axisName : "",
			y_axisDataProperty : ""
		}]*/
		if(!oConfig.x_axisDataProperty || !(oConfig.y_axisDataProperty || oConfig.y_axisMultipleDataArray) || !oConfig.dataPath){
			throw new Error("Wrong input for Column Chart! Following fields are mandatory -> x_axisDataProperty, y_axisDataProperty and dataPath.");
		}
		
		//Create Instance of Column chart and set necessary chart properties.
		var oColumnChart = new sap.viz.ui5.controls.VizFrame({
			height : "300px",
			width : "100%",
			vizType : "column",
		});
		var oVizProperties = {
			/*plotArea: {
		        colorPalette : d3.scale.category20().range()
		    },*/
		    legendGroup: {
            	layout : {
            		//position : "bottom",
            		width : "17%",
            	},
            	linesOfWrap : 2
		    },
			interaction:{
        		selectability:{
        			mode:"NONE"
        		}
          	},
		    categoryAxis: {
		    	label : {
		    		angle : 0,
		    		rotation : "fixed",
		    		linesOfWrap : 3,
		    		truncatedLabelRatio : 0.9
		    	}
		    }
		};
		if(oConfig.chartName){
			oVizProperties["title"] = {
				text : oConfig.chartName
			};
		}
		oColumnChart.setVizProperties(oVizProperties);
		
		/*
		 * Manage Data binding. 
		 * Its possible that there could be multiple measures (and FeedValueAxis) values for Y-axis.
		 */
		var measureProperty = null;
		var feedValueAxis = null;
		if(oConfig.y_axisMultipleDataArray && oConfig.y_axisMultipleDataArray instanceof Array){
			var counter = 0, y_axisConfigData = null, y_axisChartData = null;
			measureProperty = [];
			for(counter = 0; counter < oConfig.y_axisMultipleDataArray.length; counter++){
				y_axisConfigData = oConfig.y_axisMultipleDataArray[counter];
				y_axisChartData = {
						name : y_axisConfigData.y_axisName,
						value : this.getCorrectBindingProperty(y_axisConfigData.y_axisDataProperty)
				}
				measureProperty.push(y_axisChartData);
				feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [y_axisConfigData.y_axisName]
			    });
				oColumnChart.addFeed(feedValueAxis);
			}
		} else {
			measureProperty = [{
				name : oConfig.y_axisName,
				value : this.getCorrectBindingProperty(oConfig.y_axisDataProperty)}];

				feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid': "valueAxis",
				'type': "Measure",
				'values': [oConfig.y_axisName]
		    });
			oColumnChart.addFeed(feedValueAxis);

		}
		
		
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [{
				name : oConfig.x_axisName,
				value : this.getCorrectBindingProperty(oConfig.x_axisDataProperty)}],
			               
			measures : measureProperty,
			             
			data : {
				path : this.getDataPath(oConfig.dataPath)
			}
		});
		var feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
			'uid': "categoryAxis",
			'type': "Dimension",
			'values': [oConfig.x_axisName]
		});
		oColumnChart.setDataset(oDataset);
		oColumnChart.addFeed(feedCategoryAxis);
		
		return oColumnChart;
	},
	
	getCorrectBindingProperty : function(boundProp){
		if(boundProp){
			if(typeof boundProp === 'string'){
				// Check if '[' and '}' is already provided/available
				if(boundProp.indexOf('{') < 0 && boundProp.indexOf('}') < 0){
					return "{" + boundProp + "}";
				}
			}
		}
		return boundProp;
	},
	
	getDataPath : function(dataPath){
		return dataPath && dataPath.charAt(0)==='/'? dataPath : "/" + dataPath; 
	},
	
	getChartAccountDisplayText : function(accountDisplayText,AcBal){
		if (AcBal === undefined) {
			AcBal = 0.00;
	    }
		//return "{" +accountDisplayText+":"+AcBal+"-"+AcCurrency+ "}";
		return  accountDisplayText+" : "+AcBal;
	}
	
});