/**
* Controller for charts
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.AssetAccountsBarChart",{
	model:'',
	
	onInit: function(){
		jQuery.sap.log.info("onInit ConsolidatedBalanceBarChart bar");
	},

	initializeModel:function(){
		var reportId = $('#reportOn').val() || "";
		var displayCurrencyCode = $('#AccountDisplayCurrencyCode').val() || "";
		var test = "/cb/pages/jsp/account/GetConsolidateBalanceAssetsAccountAction.action?dataClassification="+reportId+"&displayCurrencyCode="+displayCurrencyCode+"&isChart=true";
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		var oConfig = {
			url:test,
			success: function(data) {
				hasData=false;
				console.log("ajax success");
				// convert data as required
				var newDataModel = controllerHandler.updateAmounts(data.gridModel);
				// set data in json model
				controllerHandler.model.setData({
                      businessData : newDataModel
				 });
				// set data in view
				controllerHandler.getView().setModel(controllerHandler.model);
			},
			error : function(jqXHR, sTextStatus, oError) {
				console.info(sTextStatus);
			},
			complete:function(jqXHR, textStatus){
				controllerHandler.renderBarChart();
			}			
		};
		
		this.getChartData(oConfig);		 
	},

	renderBarChart : function(){
		 var dimensionName = 'Debits';
   		 var barContainer = this.byId("assetAccBarContainer");
		 var messageHolder = this.byId("textAssetAccBar");
				
				var oTextView = new sap.ui.commons.TextView();
				oTextView.setText("Please wait we are loading you chart...");
				if(hasData){
					messageHolder.setVisible(false);
					var oBarChartConfig = {
					  		chartName : "Asset Accounts Balance",
					  		x_axisName : "Accounts Group",
					  		y_axisName : "Debits",
					  		x_axisDataProperty : "map/label",
					  		y_axisDataProperty : "marketValueCurrency/amountValue",
					  		dataPath : "/businessData"
					  }
					var oBarChart = this.createColumnChart(oBarChartConfig);
					barContainer.destroyContent(); 
					barContainer.addContent(oBarChart);
				}else{
					barContainer.setWidth("0%");
				}
				
	},
	
	hasData : false,
	
	updateAmounts : function(data) {
		for(var i=0;i < data.length; i++) {
			var model = data[i];
			var value = 0;
			if(model.marketValueCurrency){
				value= model.marketValueCurrency.amountValue;
				hasData=true;
			}
			/*if(model.marketValue){
				var amountNoComma = model.marketValue.replace(/,/g, "");
				data[i].marketValue = amountNoComma;
			}else{
				data[i].marketValue = 0;
			}
			if(data[i].marketValue>0){
				hasData=true;
			}*/
			var label=  model.account.nickName + " : " + value;
			model.map["label"]=label;
		}
		return data;
	},

	onBeforeRendering: function(){
		jQuery.sap.log.info("onBeforeRendering");
		this.initializeModel();
	},

	onAfterRendering: function(){
		jQuery.sap.log.info("onAfterRendering");
	},

	onExit: function(){
		jQuery.sap.log.info("onExit");
	}	
});