/**
* Controller for charts
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.ConsolidatedBalanceBarChart",{	

	model:'',
	
	onInit: function(){
		//jQuery.sap.log.info("onInit ConsolidatedBalanceBarChart bar");
	},

	initializeModel:function(){
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		
		var oConfig = {
				url:'/cb/pages/jsp/home/ConsolidatedBalancePortletAction_loadConBalData.action?isChart=true',
				containerId:"ConsolidatedBalancePortletContainer",
				success: function(data) {
					hasData=false;
					//console.log("ajax success");
					// convert data as required
					var newDataModel = controllerHandler.updateAmounts(data.gridModel);
					// set data in json model
					controllerHandler.model.setData({
	                      businessData : newDataModel
					 });
					// set data in view
					controllerHandler.getView().setModel(controllerHandler.model);
				},
				error : function(jqXHR, sTextStatus, oError) {
					console.info(sTextStatus);
				},
				complete:function(jqXHR, textStatus){
					controllerHandler.renderBarChart();
					//controllerHandler.showBusyIndicator(false);
				}
				
			};
		this.getChartData(oConfig);
	},

	renderBarChart : function(){
		 var dimensionName = 'Balance';
			    
				var barContainer = this.byId("barContainer");
				var messageHolder = this.byId("textConsolidatedBalanceBar");
				
				var oTextView = new sap.ui.commons.TextView();
				oTextView.setText("Please wait we are loading you chart...");
				if(hasData){
					messageHolder.setVisible(false);
					var oChartConfig = {
					  		chartName : "Consolidated Balance",
					  		x_axisName : "Accounts Group",
					  		y_axisName : "Balance",
					  		x_axisDataProperty : "displayTextAccountsType",
					  		y_axisDataProperty : "balanceAmountValue",
					  		dataPath : "/businessData"
					  }
					 
					var oBarChart = this.createColumnChart(oChartConfig);
					barContainer.destroyContent(); 
					barContainer.addContent(oBarChart);
				}else{
					barContainer.setWidth("0%");
				}
				
	},
	
	hasData : false,
	
	updateAmounts : function(data) {
		for(var i=0;i < data.length; i++) {
			var model = data[i];
			if(model.balance){
				var amountNoComma = model.balance.replace(/,/g, "");
				//var amountNoDot = amountNoComma.slice(0,amountNoComma.length-3);
				data[i].amountString = amountNoComma;
			}else{
				data[i].amountString = 0;
			}
			//if(data[i].amountString>0){
				hasData=true;
			//}
		}
		return data;
	},

	onBeforeRendering: function(){
		//jQuery.sap.log.info("onBeforeRendering");
		window.location.hash = "";
		//this.showBusyIndicator(true);
		this.initializeModel();			
	},

	onAfterRendering: function(){
		//jQuery.sap.log.info("onAfterRendering");
	},

	onExit: function(){
		//jQuery.sap.log.info("onExit");
	},

	//This function sets the busy indicator on parent portlet
	showBusyIndicator: function(bBusy){
		var oView = this.getView();
		//var $portletContainer = $("#" + oView.getId()).closest(".portlet-content");
		var $portletContainer = $("#ConsolidatedBalancePortletContainer").closest(".portlet-content");		
		if($portletContainer){
			if(bBusy){
				$portletContainer.setBusy(true);
			}else{
				$portletContainer.setBusy(false);
			}	
		}		
	}
});
