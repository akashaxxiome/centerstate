/**
* Controller for charts of cash flow
*/
$.sap.require("view.charts.base.BaseChartController");
view.charts.base.BaseChartController.extend("view.CashFlowSummaryBarChart",{

	model:undefined,
	
	dimensionName: undefined,
	
	onInit: function(){
		//jQuery.sap.log.info("onInit");
	},

	initializeModel:function(){
		var controllerHandler=this;
		controllerHandler.model = new sap.ui.model.json.JSONModel();
		var oConfig = {
			url:"/cb/pages/jsp/home/CashFlowPortletAction_loadCashFlowData.action?displayType=bar&isChart=true",
			containerId: "cashFlowPortletContainer",
			success: function(data) {
				hasData = false;
				//console.log("ajax success");
				if(data.userdata.displayDimension == undefined || data.userdata.displayDimension == ''){
					data.userdata.displayDimension = 'openBal';
				}
				controllerHandler.dimensionName = data.userdata.displayDimension;
				
				// convert data as required
				var newDataModel = controllerHandler.formatData(data.gridModel);
				// set data in json model
				controllerHandler.model.setData({
                      businessData : newDataModel
				 });
				// set data in view
				controllerHandler.getView().setModel(controllerHandler.model);
			},
			error : function(jqXHR, sTextStatus, oError) {
				console.info(sTextStatus);
			},
			complete : function(jqXHR,textStatus){
				controllerHandler.renderBarChart();
			}
		};
		this.getChartData(oConfig);
	},

	renderBarChart : function (){
		 // prepare dimensions 
		if(this.dimensionName){
			 var dimensionsArray = this.dimensionName.split(",");
			 var displayDimension = "";
			 var measuresArray =[];
			 for(var i=0;i<dimensionsArray.length;i++){
				if(dimensionsArray[i]=="openBal"){
					if(displayDimension==""){
						displayDimension = "Opening Balance";
					}else{
						displayDimension =  displayDimension +", "+"Opening Balance";
					}
					measuresArray.push({
						y_axisName : 'Opening Balance',
						y_axisDataProperty : 'openingBalString'
					});
				} 
				else if(dimensionsArray[i]=="totalDebits"){
					if(displayDimension==""){
						displayDimension = "Total Debits";
					}else{
						displayDimension =  displayDimension +", "+"Total Debits";
					}
					measuresArray.push({
						y_axisName : 'Total Debits',
						y_axisDataProperty : 'totalDebits'
					});
				} 
				else if(dimensionsArray[i]=="totalCredits"){
					if(displayDimension==""){
						displayDimension = "Total Credits";
					}else{
						displayDimension =  displayDimension +", "+"Total Credits";
					}
					measuresArray.push({
						y_axisName : 'Total Credits',
						y_axisDataProperty : 'totalCredits'
					});
				} 
				else if(dimensionsArray[i]=="closeBal"){
					if(displayDimension==""){
						displayDimension = "Closing Balance";
					}else{
						displayDimension =  displayDimension +", "+ "Closing Balance";
					}
					measuresArray.push({
						y_axisName : 'Closing Balance',
						y_axisDataProperty : 'closingBalString'
					});
				} 
				else if(dimensionsArray[i]=="items"){
					if(displayDimension==""){
						displayDimension = "No. of Items";
					}else{
						displayDimension =  displayDimension +", "+"No. of Items";
					}
					measuresArray.push({
						y_axisName : '# of Items',
						y_axisDataProperty : 'itemsCount'
					});
				} 
			 }
		
			var barContainer = this.byId("barContainerCashflow");
	  		var messageHolder = this.byId("messageHolderBarCashflow");
	  		var noBalanceText = this.byId("noBalanceTextBarCashflow");
	  		
			var oTextView = new sap.ui.commons.TextView();
			oTextView.setText("Please wait we are loading you chart...");
			if(this.model.oData.businessData.length!=0){
				if(hasData){
					messageHolder.setVisible(false);
					noBalanceText.setVisible(false);
					var oBarChartConfig = {
					  		chartName : "Cash Flow Balance",
					  		x_axisName : "Account Number",
					  		//y_axisName : measuresArray[0].name,
					  		x_axisDataProperty : "account/nickName",
					  		//y_axisDataProperty : measuresArray[0].value,
					  		y_axisMultipleDataArray: measuresArray,
					  		dataPath : "/businessData"
					  }
					var oBarChart = this.createColumnChart(oBarChartConfig);				
					barContainer.destroyContent(); 
					barContainer.addContent(oBarChart);
				}else{
					messageHolder.setVisible(false);
					noBalanceText.setText("No "+  displayDimension + " in Selected Accounts.");
					noBalanceText.setVisible(true);
				}
			 }else{
				 noBalanceText.setVisible(false);
			 }
		}
	},
	
	getDisplayDimensionName : function(dimension) {
		var name = '';
		if(dimension) {
			if(dimension == 'openBal') {
				name = 'Opening Balance';
			} else if(dimension == 'totalDebits') {
				name = 'Total Debits';
			} else if(dimension == 'totalCredits') {
				name = 'Total Credits';
			} else if(dimension == 'closeBal') {
				name = 'Closing Balance';
			} else if(dimension == 'items') {
				name = '# of Items';
			}
		}
		if(name) {
			return name;
		} else {
			return '0';
		}
	},
	
	hasData:false,
	
	formatData : function(data) {
		for(var i=0;i < data.length; i++) {
			var model = data[i];
			if(model.closingBalString){
				var closingBalNoComma = model.closingBalString.replace(/,/g, "");
				if(model.closingBalColor == "negativetrue"){
					data[i].closingBalString = "-"+closingBalNoComma;	
				}else {
					data[i].closingBalString = closingBalNoComma;
				}
			}else{
				data[i].closingBalString = 0;
			}
			
			if(model.itemsCount){
				var itemsCountNoComma = model.itemsCount.replace(/,/g, "");
				data[i].itemsCount = itemsCountNoComma;
			}else{
				data[i].itemsCount = 0;
			}
			
			if(model.totalDebits){
				var totalDebitsNoComma = model.totalDebits.replace(/,/g, "");
				data[i].totalDebits = totalDebitsNoComma;
			}else{
				data[i].totalDebits = 0;
			}
			
			if(model.totalCredits){
				var totalCreditsNoComma = model.totalCredits.replace(/,/g, "");
				data[i].totalCredits = totalCreditsNoComma;
			}else{
				data[i].totalCredits = 0;
			}
			
			if(model.openingBalString){
				var openingBalStringNoComma = model.openingBalString.replace(/,/g, "");
				if(model.openingBalColor == "negativetrue"){
					data[i].openingBalString = "-"+openingBalStringNoComma;
				} else {
					data[i].openingBalString = openingBalStringNoComma;
				}
			}else{
				data[i].openingBalString = 0;
			}
			
			if(data[i].closingBalString != 0 || data[i].itemsCount != 0 || data[i].totalDebits != 0 || data[i].totalCredits != 0 || data[i].openingBalString!= 0){
				hasData=true;
			}
			
		}
		return data;
	},

	onBeforeRendering: function(){
		//jQuery.sap.log.info("onBeforeRendering");
		window.location.hash = "";
		this.initializeModel();
	},

	onAfterRendering: function(){
		//jQuery.sap.log.info("onAfterRendering");
	},

	onExit: function(){
		//jQuery.sap.log.info("onExit");
	},
	
	onCashFlowSummaryBarChartSettingsClick:function (){
		ns.home.changeCashFlowPortletView('chartSettings');
	}
});