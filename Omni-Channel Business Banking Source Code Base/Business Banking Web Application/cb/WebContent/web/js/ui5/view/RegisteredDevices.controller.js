jQuery.sap.require("sap.banking.ui.services.ServiceFactory");
jQuery.sap.require("sap.banking.ui.services.DeviceManagementService");
jQuery.sap.require("sap.banking.ui.services.ServiceConstants");
jQuery.sap.require("sap.banking.ui.util.Validator");
jQuery.sap.require("sap.banking.ui.common.Common");
/**
* Registered devices controller
* Uses DeviceManagementService
*/
sap.ui.controller("view.RegisteredDevices", {

	service: {},	

	smsSenderService: null,
	
	common: null,
	
	i18NModel: undefined,

	deviceList: undefined,

	//Validator object
	validator:null,

	eventBus:sap.ui.getCore().getEventBus(),

	$registeredDeviesGrid:null,

	controllerName: "view.RegisteredDevices",
	
	bJQEventsInitialized: false,

	/**
	* onInit UI5 framework controller initialization event
	*/
	onInit: function() {
		this.common = sap.banking.ui.common.Common;
		this.getView().setBusy(true);
		this.initializeService();	
		this.initializeModel();
		
		var that = this;
		var fnSuccess = function(response){
			if(response){
				that.deviceList = response.results;
				that.getView().setBusy(false);	
			}			
		}

		var fnError = function(error){
			that.getView().setBusy(false);
		}

		this.service.getDevices(fnSuccess,fnError);
		this.validator = sap.banking.ui.util.Validator;	

		//Store registered controllers
		if(registeredControllers){
			registeredControllers.put(this.controllerName, this);	
		}
		
	},

	/**
	* This function initializes the service instance for device managent
	*/	
	initializeService: function(){
		//Initialize Device Management Service
		var CONSTANTS = sap.banking.ui.services.ServiceConstants.DEVICEMANAGEMENTSERVICE;
		this.service = sap.banking.ui.services.ServiceFactory.getService(CONSTANTS.NAME);

		//Initialize SMS sender service
		var CONSTANTS = sap.banking.ui.services.ServiceConstants.SMSSENDERSERVICE;
		this.smsSenderService = sap.banking.ui.services.ServiceFactory.getService(CONSTANTS.NAME);		
	},

	/**
	* This function initializes the model and i18n model
	*/
	
	initializeModel: function(){	
	//Register device i18n model
		var i18NModel = this.common.getResourceModel("devicemanagement.properties");	
		this.getView().setModel(i18NModel,"i18n");
		this.i18NModel = i18NModel;
	},

	/**
	* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
	* (NOT before the first rendering! onInit() is used for that one!).
	* @memberOf view.RegisteredDevices
	*/
	onBeforeRendering: function() {
	if(mainApp.helpModule){
		mainApp.helpModule.setCurrentActiveHash(hasher.getHashAsArray());
	}
	this.resetHashPath();
	},

	resetHashPath: function(){
	var routers = sap.banking.ui.routing.Routers;
		sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.DEVICEMANAGEMENT).navOut();
	},
	
	testDevice: function(msisdn){
		var that = this;
		var fnSuccess = function(response){	
			console.info("Success");
			var message = that.i18NModel.getResourceBundle().getText("DEVICE_TEST_MESSAGE_SUCCESS_MESSAGE");
			ns.common.showStatus(message);			
		}
		var fnError = function(error){
			console.info("error");				
			var message = that.i18NModel.getResourceBundle().getText("DEVICE_TEST_MESSAGE_ERROR_MESSAGE");
			ns.common.showError(message);
		}

		var message = this.i18NModel.getResourceBundle().getText("DEVICE_TEST_MESSAGE");
		var msisdns = [msisdn];
		this.smsSenderService.sendTestMessage(message,msisdns,fnSuccess,fnError);
	},

	deleteDevice: function(deviceId){
		var that = this;
		var deleteButtonLabel = this.i18NModel.getResourceBundle().getText("DEVICE_DELETE_BUTTON_MESSAGE");
		var cancelButtonLabel = this.i18NModel.getResourceBundle().getText("DEVICE_CANCEL_BUTTON_MESSAGE");
		var confirmationTitle = this.i18NModel.getResourceBundle().getText("DEVICE_DELETE_COFIRMATION_TITLE_MESSAGE");
		var oButtons = {};
		oButtons.deleteButton = {text:deleteButtonLabel,
			click:function(){
				$( this ).dialog( "close" );
					var fnSuccess = function(response){	
					var message = that.i18NModel.getResourceBundle().getText("DEVICE_DELETE_MESSAGE_SUCCESS_MESSAGE");
					ns.common.showStatus(message);
					that.reloadGridData();
					}
					var fnError = function(error){				
					that.validator.handleError(error);
					}
					that.service.deleteMobileDevice(Number(deviceId),fnSuccess,fnError);
				}
		};
		oButtons.cancelButton = { text:cancelButtonLabel,click:this.closeDialog};
	$( "#deleteDeviceDialog" ).dialog({
			resizable: true,
			modal: true,
			width:400,
			buttons:oButtons,
			title:confirmationTitle
		});
	},
	closeDialog: function(){
		$( this ).dialog( "close" );
	},
	editDevice: function(deviceId){
		var device = this.getDeviceDetails(deviceId);		
		var routers = sap.banking.ui.routing.Routers;
		sap.banking.ui.core.routing.ApplicationRouter.getRouter(routers.DEVICEMANAGEMENT).navTo("_editDevice",{id:deviceId,name:"testDevice"});
	},

	registerEvents: function(){
		var that = this;

		var actionButtonsClickHandler = function(e){
			var $actionLink = $(e.currentTarget);
			var deviceId = $actionLink.attr("ux-data-device-id");
			var action = $actionLink.attr("ux-data-action-type");
			var msisdn = $actionLink.attr("ux-data-device-number");
			if(action === "test"){
				that.testDevice(msisdn);
			}else if(action === "edit"){
				that.editDevice(deviceId);
			}else if(action === "delete"){
				that.deleteDevice(deviceId);
			}
		};
		$(document).on("click","#registeredDevicesGrid a[ux-data-column-type=action]",actionButtonsClickHandler);

	},

	/**
	* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
	* This hook is the same one that SAPUI5 controls get after being rendered.
	* @memberOf view.RegisteredDevices
	*/
	onAfterRendering: function() {
		this.initializeJQWidgets();
		if(!this.bJQEventsInitialized){
			this.registerEvents();
			this.bJQEventsInitialized = true;
		}
		this.initializei18N();
	},

	initializei18N: function(){
    var titleMsg = this.i18NModel.getResourceBundle().getText("REGISTERED_DEVICES_PORTLET_TITLE");
	var helpMsg = this.i18NModel.getResourceBundle().getText("DEVICE_REGISRATION_HELP");
	var deleteConfirmMsg = this.i18NModel.getResourceBundle().getText("DEVICE_REGISRATION_DELETE_CONFIRM_MESSAGE");
	$('#portletTitleId').html(titleMsg);
	$('#helpId').html(helpMsg);
	$('#deleteConfirmMsgId').html(deleteConfirmMsg);
	},
	initializeJQWidgets: function(){
		var serviceConstants = sap.banking.ui.services.ServiceConstants;

		var aUrl = "/cb/odata/mobiledeviceservice/MobileDevices";
		//Initialize the data grid
		/*this.$registeredDeviesGrid = $("#registeredDevicesGrid").datagrid({
			url:aUrl,
			datatype: "json",
		   	colNames:['Id','Name', 'Number', 'Carrier','Status','Action'],
		   	colModel:[
		   		{name:'DeviceId',index:'DeviceId', width:50,align:"right"},
		   		{name:'DeviceName',index:'DeviceName', width:150,align:"center"},
		   		{name:'SubscriberNumber',index:'SubscriberNumber', width:150,align:"center"},
		   		{name:'CarrierCode',index:'CarrierCode', width:100,align:"center"},
		   		{name:'Status',index:'Status', width:100,align:"center"},
		   		{name: 'action',index:'action',width:100,align:"right",formatter:this.actionColumnFormatter}
		   	],
		   	rowNum:10,
		   	rowList:[10,20,30],
		   	pager: "#registeredDevicesPager",
		   	sortname: 'DeviceId',
		    viewrecords: true,
		    sortorder: "asc"
		});*/
		var Id=this.i18NModel.getResourceBundle().getText("ID");
		var Name=this.i18NModel.getResourceBundle().getText("NAME");
		var Number=this.i18NModel.getResourceBundle().getText("NUMBER");
		var Carrier=this.i18NModel.getResourceBundle().getText("CARRIER");
		var Status=this.i18NModel.getResourceBundle().getText("STATUS");
		var Action=this.i18NModel.getResourceBundle().getText("ACTION");
		var AcceptLoginToken=this.i18NModel.getResourceBundle().getText("ACCEPTLOGINTOKEN");
		var that = this;	
		this.$registeredDeviesGrid = $("#registeredDevicesGrid").jqGrid({			
			datatype: "local",
		   	colNames:[Id,Name, Number, Carrier,Status,Action,AcceptLoginToken],
		   	colModel:[{
	   			name:'DeviceId',
	   			index:'DeviceId', 
	   			width:50,	
	   			align:"right",	
	   			hidden:true
	   		},{	
	   			name:'DeviceName',
	   			index:'DeviceName', 
	   			width:150,
	   			align:"center",
	   			formatter: function(cellvalue, options, rowObject){
	   				var formattedText = cellvalue;
	   				if(rowObject.AcceptLoginToken === "T"){
	   					formattedText = " * " + cellvalue; 
	   				}
	   				return formattedText;
	   			}
	   		},{
		   		name:'SubscriberNumber',
		   		index:'SubscriberNumber', 
		   		width:150,
		   		align:"center"
		   	},{
		   		name:'CarrierName',
		   		index:'CarrierName', 
		   		width:100,
		   		align:"center"
		   	}/*,{
		   		name:'CarrierName',
		   		index:'CarrierName', 
		   		width:100,
		   		align:"center"
		   	}*/,{
		   		name:'Status',
		   		index:'Status', 
		   		width:100,
		   		align:"center",
		   		formatter:function(cellvalue, options, rowObject){
					var status = that.i18NModel.getResourceBundle().getText("DEVICE_STATUS_INACTIVE");
					if(cellvalue == "1"){
						status = that.i18NModel.getResourceBundle().getText("DEVICE_STATUS_ACTIVE");		
					}
					if(cellvalue == "3"){
						status = that.i18NModel.getResourceBundle().getText("DEVICE_STATUS_RESTRICTED");		
					}
					return status;
				}
			},{
				name: 'action',
				index:'action',
				sortable:false,
				width:100,
				align:"right",
				formatter:this.actionColumnFormatter
			},{
				name: 'AcceptLoginToken',
				index:'AcceptLoginToken',
				hidden:true
			}],
		    viewrecords: true
		});

		//Initialize the portlet
		/*$('#registeredDevicesGridContainer').portlet({
			bookmark: true,
			bookMarkCallback: function(){
				var path = $('#registeredDevicesGridContainer').find('.shortcutPathClass').html();
				var ent  = $('#registeredDevicesGridContainer').find('.shortcutEntitlementClass').html();
				ns.shortcut.openShortcutWindow( path, ent );
			},
			title: this.i18NModel.getResourceBundle().getText("REGISTERED_DEVICES_PORTLET_TITLE"),
			generateDOM: true,
			helpCallback: function(){
				var helpFile = $('#summary').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
		});*/
		ns.common.initializePortlet("registeredDevicesGridContainer");
		this.reloadGridData();
	},

	actionColumnFormatter: function(cellvalue, options, rowObject){
		var links = "";
		var testLink = "<a class='ui-button' ux-data-column-type='action' ux-data-device-id='" + rowObject.DeviceId + "' ux-data-device-number='" + rowObject.SubscriberNumber + "' ux-data-action-type='test'><span class='ui-icon ui-icon-mail-closed'></span></a>";
		var editLink = "<a class='ui-button' ux-data-column-type='action' ux-data-device-id='" + rowObject.DeviceId + "' ux-data-device-number='" + rowObject.SubscriberNumber + "' ux-data-action-type='edit'><span class='ui-icon ui-icon-pencil'></span></a>";
		var deleteLink = "<a class='ui-button' ux-data-column-type='action' ux-data-device-id='" + rowObject.DeviceId + "' ux-data-device-number='" + rowObject.SubscriberNumber + "' ux-data-action-type='delete'><span class='ui-icon ui-icon-trash'></span></a>";
		if(rowObject.Status == "3"){
			links = links + deleteLink;
		}else if(rowObject.AcceptLoginToken === "T"){
			links = links + testLink + editLink;
		}else{
			links = links + testLink + editLink + deleteLink;
		}
		return links;
	},

	getDeviceDetails: function(deviceId){			
		var device = {};
		for(var i=0;i<this.deviceList.length;i++){
			if(this.deviceList[i].id === deviceId){
				device = this.deviceList[i];
				break;
			}	
		}
		return device;
	},

	reloadGridData: function(){
		var that = this;
		var fnSuccess = function(response){
			if(response){
				var devices = response.results;
				if(devices){
					var LEN = devices.length;
					that.$registeredDeviesGrid.jqGrid('clearGridData');
					for(var i=0;i<=LEN;i++){
						 that.$registeredDeviesGrid.jqGrid('addRowData',i+1,devices[i]);	
					}
				}				
			}			
		}

		var fnError = function(error){
			that.getView().setBusy(false);
		}

		this.service.getDevices(fnSuccess,fnError);



		
	},

	/**
	* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
	* @memberOf view.RegisteredDevices
	*/
	onExit: function() {
		//TODO: Destroy the JQ widgets here
	}

});