/**
 * Drop Down Menu :
 * This fragment generates HTML required for Module drop down menu
 * This menu provides quicker navigation withing the related modules
 * It depends on controller to provide implmentation method for getEntitledMenus, which should return the list of menus
 * based on users entitlement, fires onModuleMenuSelect event on controller
 * below is example of its usage,
 * Creating instance of Drop Down Menu,
 *
 * var oDropDownMenu = sap.ui.jsfragment(this.createId("dropDownMenuFragment"), "fragment.ModuleDropDownMenu", oController);
 *
 * var entitledMenus = [{
 *   menuId: "stopsModuleMenu",
 *   name: "Stops",
 *   iconCls: "stops",
 *   selected: true
 *  },{
 *   menuId: "transferModuleMenu",
 *   name: "Transfers",
 *   iconCls: "transfer",
 *   selected: false
 *  }]
 *
 *
 * @Since 8.3SP03
 */
(function($, sap, window, undefined) {

    "use strict";

    sap.ui.jsfragment("fragment.ModuleDropDownMenu", {

        logger: window.console,

        fragmentName: "fragment.ModuleDropDownMenu",

        menuId: null,

        createContent: function(oController) {
            this.logger.info("createContent-" + this.fragmentName);

            var menus = oController.getEntitledMenus();

            var menuItemsHtml = "",
                menuName, menuIcon, selectedMenu, id;

            this.menuId = this.createId("moduleDDMenu");

            for (var i = 0; i < menus.length; i++) {
                id = menus[i].menuId;
                menuName = menus[i].name;
                menuIcon = menus[i].iconCls;
                if (menus[i].selected && menus[i].selected === true) {
                    selectedMenu = menus[i];
                }

                menuItemsHtml = menuItemsHtml + "<li data-banking-menu=" + menuName + ">" +
                    "<span class='moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-" + menuIcon + "'></span>" +
                    "<span class='moduleSubmenuDropdownLbl' data-sap-ux-menuid='" + id + "'>" + menuName + "</span>" +
                    "</li>";
            }

            if (!selectedMenu && menus.length > 0) {
                selectedMenu = menus[0];
            }

            var menuListHtml = "<div id='" + this.menuId + "''>" +
                "<span class='moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-" + selectedMenu.iconCls + "'>" +
                "</span><span class='sapUxDashboardSubmenuLbl'>" + selectedMenu.name + "</span>" +
                "<span class='moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow'></span>" +
                "<ul class='moduleSubmenuDropdownHolder'>" + menuItemsHtml + "</ul>" +
                "</div>";
            var moduleMenuHtml = new sap.ui.core.HTML({
                content: menuListHtml,
                afterRendering: $.proxy(function(oEvent) {
                    $("#" + this.menuId).click(function(e) {
                        var $target = $(e.target);
                        if ($target.hasClass("moduleSubmenuDropdownLbl")) {
                            var params = {
                                menuId: $target.attr("data-sap-ux-menuid")
                            }

                            if (oController.onModuleMenuSelect) {
                                oController.onModuleMenuSelect.apply(this, [params]);
                            }
                        }
                    });
                }, this)
            });

            return moduleMenuHtml;
        }
    });
}(jQuery, sap, window));