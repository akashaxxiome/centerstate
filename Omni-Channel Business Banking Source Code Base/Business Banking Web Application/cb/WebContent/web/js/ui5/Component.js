/**
 * Banking UI component
 * This is main banking component, It currently defines the
 * Stops Module with routing, view extensions, view replacements and controller extensions
 */


jQuery.sap.declare("ui5.Component");

//Define the depedencies
jQuery.sap.require("sap.banking.ui.core.routing.AppRouter"); //Load the router
jQuery.sap.require("sap.banking.ui.core.routing.ApplicationRouter"); //Load the extended application router
jQuery.sap.require("sap.banking.ui.common.Common");

sap.ui.core.UIComponent.extend("ui5.Component", {
    metadata: {
        routing: {
            config: {
                routerClass: sap.banking.ui.core.routing.ApplicationRouter,
                viewPath: "ui5.view"
            },
            routes: [{
                name: "_stops",
                pattern: "stops",
                views: [{
                    view: "view.Stops",
                    viewType: "JS",
                    div: "appdashboard"
                }]
            }, {
                name: "_stopsSummary",
                pattern: "stopsSummary",
                views: [{
                    view: "view.StopsSummary",
                    viewType: "JS",
                    div: "details"
                }]
            }, {
                name: "_newStopPayment",
                pattern: "newStopPayment",
                views: [{
                    view: "view.NewStopPayment",
                    viewType: "JS",
                    div: "details"
                }]
            }, {
                name: "_modifyStopPayment",
                pattern: "modifyStopPayment/{id}",
                views: [{
                    view: "view.ModifyStopPayment",
                    viewType: "JS",
                    div: "details"
                }]
            }]
        }
    },

    resourceModel: null,

    init: function() {
        console.log("init component..................");
        var rootPath = jQuery.sap.getModulePath("ui5");

        sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

        // this component should automatically initialize the router
        this.getRouter().initialize();
        
        //Not required with SAPUI5 later releases 1.23 onwards
        sap.banking.ui.common.Common.setRouter(this.getRouter());

        // Set i18n model, 
        var i18nModel = new sap.ui.model.resource.ResourceModel({
            bundleUrl: rootPath + "/i18n/stops.properties"
        });

        this.setResourceModel(i18nModel);

        //Not required with SAPUI5 later releases 1.23 onwards
        sap.banking.ui.common.Common.setComponentResourceModel(i18nModel);

        this.setModel(i18nModel, "i18n");
    },

    setResourceModel: function(oResourceModel) {
        this.resourceModel = oResourceModel;
    },

    getResourceModel: function() {
        return this.resourceModel;
    }
});