This directory contains image files that are common to both CB and BC.
All files in this directory (except for this readme.txt) will get packaged
into the CB and BC war files in the directories cb/grafx/common and
bc/grafx/common, respectively.

Feel free to add to this readme.txt descriptions of the common image files
as they get added.
-------------------------------------------------------------------------------

