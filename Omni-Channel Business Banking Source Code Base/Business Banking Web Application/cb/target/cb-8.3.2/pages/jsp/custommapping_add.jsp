<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:help id="custommapping_add" />

<s:set var="tmpI18nStr" value="%{getText('jsp.default_34')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
<ffi:setProperty name='PageText' value=''/>
<ffi:setProperty name="BackURL" value="${SecurePath}account/Mapping.jsp"/>

<ffi:object id="GetOutputFormatNames" name="com.ffusion.tasks.fileImport.GetOutputFormatDisplayNamesTask" scope="session"/>
<ffi:setProperty name="GetOutputFormatNames" property="CollectionName" value="OutputFormatNames" />
<ffi:process name="GetOutputFormatNames"/>


<script type="text/javascript"><!--

$(function(){
	$("#MappingDefinitionOutputFormatNameID").selectmenu({width: 267});
	$("#MappingDefinitionInputFormatTypeID").selectmenu({width: 267});
	
});

function addMappingReset()
{
	
	document.MappingAddForm.reset();
	$("#MappingDefinitionOutputFormatNameID").selectmenu('destroy').selectmenu({width: 267});
	$("#MappingDefinitionInputFormatTypeID").selectmenu('destroy').selectmenu({width: 267});
	gotoControl();
}

function gotoControl()
{
	var val = document.MappingAddForm.InputFormatType.options[document.MappingAddForm.InputFormatType.selectedIndex].value;
	if (val == "1")
		//QTS679668
		document.MappingAddForm.action ='<ffi:urlEncrypt url="/cb/pages/jsp/custommapping_fixed.jsp?Initialize=true"/>';		
	if (val == "0")
		document.MappingAddForm.action = '<ffi:urlEncrypt url="/cb/pages/jsp/custommapping_delimited.jsp"/>';
}

// --></script>

<%-- include javascripts for top navigation menu bar
Since Payments, Transfers, ACH and Wire all are in the SubDirectory payments, just make that the default. --%>
<% request.setAttribute("SubDirectory", "payments"); %>
<ffi:cinclude value1="${Section}" value2="PPAY">
	<% request.setAttribute("SubDirectory", "cash"); %>
</ffi:cinclude>

<ffi:flush/>
		<div align="left">

			<s:form name="MappingAddForm" id="MappingFormID" action="/pages/jsp/custommapping_delimited.jsp" method="post" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td class="columndata" nowrap align="right">
						<span class="sectionsubhead"><s:text name="jsp.default_287.2"/> </span>
						</td>
						<td>
						<%-- input class="txtbox" type="text" name="MappingDefinition.Name" value="" size="40" maxlength="255" border="0" --%>
						<s:textfield name="MappingDefinition.Name" id="MappingDefinitionNameID" size="40" style="width:258px;" maxlength="255" value="" cssClass="txtbox ui-widget-content ui-corner-all"/>
						</td>
						</td>
					</tr>
					<tr>
						<td class="columndata" nowrap align="right">
						<span class="sectionsubhead"><s:text name="jsp.default_171"/> </span>
						</td>
						<td>
						<%-- input class="txtbox" type="text" name="MappingDefinition.Description" value="" size="40" maxlength="255" border="0" --%>
						<s:textfield name="MappingDefinition.Description" id="MappingDefinitionDescriptionID" size="40" style="width:258px;" maxlength="255" value="" cssClass="txtbox ui-widget-content ui-corner-all"/>
						</td>
						</td>
					</tr>
					<tr>
						<td class="columndata" nowrap align="right">
						<span class="sectionsubhead"><s:text name="jsp.default_309"/> </span>
						</td>
						<td>
						<select name="MappingDefinition.OutputFormatName" id="MappingDefinitionOutputFormatNameID" class="txtbox">
							<ffi:list collection="OutputFormatNames" items="OutputFormatName">
								<option value="<ffi:getProperty name='OutputFormatName' property='Name'/>"><ffi:getProperty name="OutputFormatName" property="DisplayName"/></option>
							</ffi:list>
						</select>
						</td>
					</tr>
					<tr>
						<td class="columndata" nowrap align="right">
						<span class="sectionsubhead"><s:text name="jsp.default_246"/> </span>
						</td>
						<td>
						<select name="InputFormatType" id="MappingDefinitionInputFormatTypeID" class="txtbox" onChange="gotoControl()">
							<option value="0"><s:text name="jsp.default_168"/></option>
							<option value="1"><s:text name="jsp.default_212"/></option>
						</select>
						</td>
					</tr>
					<tr><td>&nbsp;</td></tr>
					<tr>
						<td colspan="3" align="center" nowrap>
						
						<sj:a id="resetCustomMappingAdding" 
								button="true" 
								onclick="addMappingReset()"
						><s:text name="jsp.default_358"/></sj:a>
						
						<sj:a id="cancelFormButtonOnInputID" 
								button="true" 
								 onclick="$('#ppaymappingLink').trigger('click')"
								 onClickTopics="cancelAddMapping"
						><s:text name="jsp.default_82"/></sj:a>
							        
							         <%-- input class="submitbutton" type="button" value="NEXT" onclick="gotoControl(Mapping['MappingDefinition.InputFormatType'])" --%>
							         
							         
							        <sj:a 
										id="addMappingSubmit"
										formIds="MappingFormID"
										targets="verifyCustomMappingDiv" 
										button="true" 
										onCompleteTopics="addCustomMappingFormCompleteTopics"
										><s:text name="jsp.default_291"/></sj:a>
			                        
			                        </td>
					</tr>
				</table>
			</s:form>
		</div>
