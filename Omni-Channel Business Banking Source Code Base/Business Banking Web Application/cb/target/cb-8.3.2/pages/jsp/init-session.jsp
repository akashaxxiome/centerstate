<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%
    Object createSession = session.getAttribute("CreateSession");
    if ( createSession == null || !(createSession instanceof com.ffusion.tasks.user.CreateSession) ) {
%>
	<ffi:object id="CreateSession" name="com.ffusion.tasks.user.CreateSession" scope="session"/>
	<ffi:process name="CreateSession" />
<%}%>