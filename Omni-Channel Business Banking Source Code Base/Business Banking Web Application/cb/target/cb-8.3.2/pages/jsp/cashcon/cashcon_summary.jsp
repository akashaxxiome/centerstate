<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

	<ffi:object id="cashcontabs" name="com.ffusion.tasks.util.TabConfig"/>
	<ffi:setProperty name="cashcontabs" property="TransactionID" value="CashCon"/>
	
	<ffi:setProperty name="cashcontabs" property="Tab" value="approving"/>
	
	<%-- <ffi:setProperty name="cashcontabs" property="Href" value="/cb/pages/jsp/cashcon/initCashConSummaryAction_getPendingApproval.action"/>
	<ffi:setProperty name="cashcontabs" property="LinkId" value="approving"/>
	<ffi:setProperty name="cashcontabs" property="Title" value="jsp.default_531"/>
	
	<ffi:setProperty name="cashcontabs" property="Tab" value="pending"/>
	<ffi:setProperty name="cashcontabs" property="Href" value="/cb/pages/jsp/cashcon/initCashConSummaryAction_getPending.action"/>
	<ffi:setProperty name="cashcontabs" property="LinkId" value="pending"/>
	<ffi:setProperty name="cashcontabs" property="Title" value="jsp.default_530"/>
	
	<ffi:setProperty name="cashcontabs" property="Tab" value="completed"/>
	<ffi:setProperty name="cashcontabs" property="Href" value="/cb/pages/jsp/cashcon/initCashConSummaryAction_getCompleted.action"/>
	<ffi:setProperty name="cashcontabs" property="LinkId" value="completed"/>
	<ffi:setProperty name="cashcontabs" property="Title" value="jsp.default_518"/> 
	
	<ffi:process name="cashcontabs"/>--%>
	
	<div id="cashconGridTabs" class="portlet gridPannelSupportCls" style="float: left; width: 100%;" role="section" aria-labelledby="summaryHeader">
	<div class="portlet-header">
	 <h1 id="summaryHeader" class="portlet-title"><s:text name="jsp.cashcon_50"/></h1>
	 	    <div class="searchHeaderCls">
				<span class="searchPanelToggleAreaCls" onclick="$('#quicksearchcriteria').slideToggle();">
					<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
				</span>
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						<s:text name="jsp.default_531"/>
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
					
					<div class="gridTabDropdownHolder">
						<span class="gridTabDropdownItem" id='approving' onclick="ns.common.showGridSummary('approving')" >
							<s:text name="jsp.default_531"/>
						</span>
						<span class="gridTabDropdownItem" id='pending' onclick="ns.common.showGridSummary('pending')">
							<s:text name="jsp.default_530"/>
						</span>
						<span class="gridTabDropdownItem" id='completed' onclick="ns.common.showGridSummary('completed')">
							<s:text name="jsp.default_518"/>
						</span>
					</div>
				</div>
			</div>
	</div>

	    
	<div class="portlet-content">
	    	<div class="searchDashboardRenderCls">
				<s:include value="/pages/jsp/cashcon/cashcon_searchCriteria.jsp" />
			</div>
			<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="approvingSummaryGrid" gridId="pendingApprovalCashconGridId" class="gridSummaryContainerCls" >
					<s:action namespace="/pages/jsp/cashcon" name="initCashConSummaryAction_getPendingApproval" executeResult="true"/>
				</div>
				<div id="pendingSummaryGrid" gridId="pendingCashconGridId" class="gridSummaryContainerCls hidden" >
					<s:action namespace="/pages/jsp/cashcon" name="initCashConSummaryAction_getPending" executeResult="true"/>
				</div>
				<div id="completedSummaryGrid" gridId="completedCashconGridId" class="gridSummaryContainerCls hidden" >
					<s:action namespace="/pages/jsp/cashcon" name="initCashConSummaryAction_getCompleted" executeResult="true"/>
				</div>
			</div>
	    </div>
		
		<div id="transferSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('cashconGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	    
	   
	</div>
	
	
	<%--  <ul class="portlet-header">
		<ffi:list collection="cashcontabs.Tabs" items="tab">
	        <li>
	            <a href='<ffi:getProperty name="tab" property="Href"/>' id='<ffi:getProperty name="tab" property="LinkId"/>'>
	            	<ffi:setProperty name="TabTitle" value="${tab.Title}"/><s:text name="%{#session.TabTitle}"/>
	            </a>
	        </li>
		</ffi:list>
	    	<a id="tabChangeNote" style="height:0px;  position:relative; top:3px; left:10px; display:inline-block;"></a>
			<a id="tabRevertNote" style="height:0px;  position:relative; top:3px; left:20px; display:inline-block;"></a>
	    </ul> --%>
	    <input type="hidden" id="getTransID" value="<ffi:getProperty name='cashcontabs' property='TransactionID'/>" />
		<s:hidden name="cashconSummaryType" value="PendingCashcon" id="cashconSummaryTypeID"/>

	<script>    

	//Initialize portlet with settings & calendar icon
	ns.common.initializePortlet("cashconGridTabs", false, false, function( ) {
	});
	
	</script>	
