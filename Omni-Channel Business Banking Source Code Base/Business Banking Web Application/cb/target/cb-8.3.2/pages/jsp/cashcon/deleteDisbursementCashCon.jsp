<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ffusion.beans.FundsTransactionTypes,
                 com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_cashcondelete" className="moduleHelpClass"/>

<s:form action="/pages/jsp/cashcon/deleteDisbursementCashconAction_execute.action" method="post" name="cashconDelete" id="deleteCashconFormId" >	
<ffi:removeProperty name="tmp_url"/>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<%-- <div class="paneWrapper">
  	<div class="paneInnerWrapper">
   		<div class="header"><ffi:getProperty name="CashCon" property="TypeString" /></div>
   		<div class="paneContentWrapper"> --%>
	   		<div class="confirmationDetails">
				<span id="" class="sectionLabel"><s:text name="jsp.default_435"/>:</span>
		        <span id="" class="columndata"><ffi:getProperty name="CashCon" property="TrackingID" /></span>
			</div>
			<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
			<div class="blockContent">
				<div class="blockRow">
					<span class="sectionLabel"><s:text name="jsp.default_388"/>:</span>
					<span><ffi:getProperty name="CashCon" property="StatusName" /></span>
				</div>
			</div>
			<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
			<div class="blockContent label130">
	   			<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_174"/></span>
						<span class="columndata">
							<ffi:getProperty name="CashCon" property="DivisionName"/>
						</span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_266"/></span>
						<span class="columndata">
							<ffi:getProperty name="CashCon" property="LocationName"/>
						</span>
					</div>
				</div>
				<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_86"/></span>
						<span class="columndata">
							<ffi:getProperty name="CashConCompany" property="CompanyName" /> / <ffi:getProperty name="CashConCompany" property="CompanyID" />
						</span>
					</div>
					<div class="inlineBlock">
						<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_603"/></span>
						<span class="columndata">
							<ffi:getProperty name="CashConCompany" property="DisbNextCutOffTime" />
						</span>
					</div>
				</div>
				
					
				<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">					
					<ffi:cinclude value1="true" value2="${CashConCompany.DisbSameDayCutOffDefined}" operator="equals" >
						<div class="blockRow">
							<div class="inlineBlock" style="width: 50%">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_604"/>:</span>
								<span class="columndata ltrow2_color">
										<ffi:getProperty name="CashConCompany" property="DisbNextSameDayCutOffTime" />
								</span>
							</div>
							<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel">
									<s:text name="jsp.default_602"/>:
								</span>
								<span class="columndata ltrow2_color">
									<ffi:cinclude value1="${CashCon.SameDayCashCon}" value2="true" operator="equals"><s:text name="jsp.cashcon_47"/></ffi:cinclude>
									<ffi:cinclude value1="${CashCon.SameDayCashCon}" value2="false" operator="equals"><s:text name="jsp.cashcon_48"/></ffi:cinclude>
								</span>
							</div>
						</div>
					</ffi:cinclude>
				</ffi:cinclude>	
				
				<div class="blockRow">
					<span class="sectionsubhead ltrow2_color"  >
						<s:text name="jsp.default_485"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
					</span>
					<span class="columndata">
						<ffi:getProperty name="CashCon" property="AmountValue.CurrencyStringNoSymbol"/>
					</span>
				</div>
	 		</div>
 
	<%-- 	<div align="center">
			<div align="left">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td></td>
					<td class="ltrow2_color">
						
								<table border="0" cellspacing="0" cellpadding="3" width="711">
									<tr>
										<td colspan="3" class="sectionhead tbrd_b ltrow2_color">
										&gt; <s:text name="jsp.default_163"/>
										<ffi:getProperty name="CashCon" property="TypeString" />
										</td>
									</tr>
									<tr>
										<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="154" height="1"></td>
										<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="150" height="1"></td>
										<td class="ltrow2_color"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="100" height="1"></td>
									</tr>
									<tr>
										<td class="sectionsubhead ltrow2_color" colspan="3" ><s:text name="jsp.cashcon_29"/></td>
									</tr>
									<tr>
										<td class="sectionsubhead ltrow2_color" align="Right" ><s:text name="jsp.default_174"/></td>
										<td class="columndata ltrow2_color">
											<ffi:getProperty name="CashCon" property="DivisionName"/>
										</td>
									</tr>
									<tr>
										<td class="sectionsubhead ltrow2_color" align="Right" ><s:text name="jsp.default_266"/></td>
										<td class="columndata ltrow2_color">
											<ffi:getProperty name="CashCon" property="LocationName"/>
										</td>
									</tr>
									<tr>
										<td class="sectionsubhead ltrow2_color" align="Right" ><s:text name="jsp.default_86"/></td>
										<td class="columndata ltrow2_color">
											<ffi:getProperty name="CashConCompany" property="CompanyName" /> / <ffi:getProperty name="CashConCompany" property="CompanyID" />
										</td>
									</tr>
									<tr>
										<td class="sectionsubhead ltrow2_color" align="Right" ><s:text name="jsp.default_493"/></td>
										<td class="columndata ltrow2_color">
											<ffi:getProperty name="CashConCompany" property="DisbNextCutOffTime" />
										</td>
									</tr>
									<tr>
										<td class="sectionsubhead ltrow2_color" align="Right" >
										<s:text name="jsp.default_485"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
										</td>
										<td class="columndata ltrow2_color">
											<ffi:getProperty name="CashCon" property="AmountValue.CurrencyStringNoSymbol"/>
										</td>
									</tr>
	<tr>
		<td class="sectionsubhead" align="Right" ><s:text name="jsp.default_388"/></td>
		<td class="columndata"><ffi:getProperty name="CashCon" property="StatusName" /></td>
	</tr>
	<tr>
		<td class="sectionsubhead" align="Right" ><s:text name="jsp.default_435"/></td>
		<td class="columndata"><ffi:getProperty name="CashCon" property="TrackingID" /></td>
	</tr>
									<tr class="ltrow_color">
										<td class="columndata ltrow2_color" align="center" colspan="3">
											<br>
												<sj:a 
										                button="true" 
														onClickTopics="closeDialog"
										        	><s:text name="jsp.default_82"/></sj:a>
										        
											<sj:a id="deleteSingleCashconLink" formIds="deleteCashconFormId" targets="resultmessage" button="true"   
						  							title="%{getText('jsp.cashcon_14')}" onCompleteTopics="cancelSingleCashconComplete" onSuccessTopics="cancelCashconSuccessTopics" onErrorTopics="errorDeleteCashcon" 
						  							effectDuration="1500" ><s:text name="jsp.cashcon_15.1" /></sj:a>
										</td>
									</tr>
								</table>
							
					</td>
					<td></td>
				</tr>
			</table>
				<br>
			</div>
		</div> --%>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div align="center" class="ui-widget-header customDialogFooter"> 
<sj:a button="true" onClickTopics="closeDialog"><s:text name="jsp.default_82"/></sj:a>
<sj:a id="deleteSingleCashconLink" formIds="deleteCashconFormId" targets="resultmessage" button="true"   
	title="%{getText('jsp.cashcon_14')}" onCompleteTopics="cancelSingleCashconComplete" onSuccessTopics="cancelCashconSuccessTopics" onErrorTopics="errorDeleteCashcon" 
	effectDuration="1500" ><s:text name="jsp.default_162" /></sj:a>
</div>   
<!-- </div>
 </div>
</div>  -->
</s:form>