<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ffusion.beans.FundsTransactionTypes,
                 com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_cashconsend" className="moduleHelpClass"/>

<s:set var="cashCon" value="#session.cashCon" />

<div style="display:none">
	<span id="cashconResultMessage"><s:text name="jsp.cashcon_40"/></span>
</div>
<form action="cashcon.jsp" method="post" name="CashConNew">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<div class="leftPaneWrapper">
<div class="leftPaneInnerWrapper">
	<div class="header"><ffi:getProperty name="CashCon" property="TypeString" /> Summary</div>
	<div class="leftPaneInnerBox summaryBlock"  style="padding: 15px 5px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_174"/>:</span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="CashCon" property="DivisionName"/></span>
			</div>
	</div>
</div>
</div>
<div class="confirmPageDetails">
<div class="templateConfirmationWrapper">
		<div class="confirmationDetails">
			<span id="" class="sectionLabel"><s:text name="jsp.default_435"/></span>
	        <span id="" class="columndata"><ffi:getProperty name="CashCon" property="TrackingID" /></span>
		</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
	<div class="blockContent">
		<s:set var="CASH_CON_STATUS_FAILED" value="%{@com.ffusion.beans.cashcon.CashConStatus@CASH_CON_STATUS_FAILED==#attr.cashCon.status}" />
		<s:set var="CASH_CON_STATUS_LIMITCHECK_FAILED" value="%{@com.ffusion.beans.cashcon.CashConStatus@CASH_CON_STATUS_LIMITCHECK_FAILED==#attr.cashCon.status}" />
		<s:set var="CASH_CON_STATUS_PENDING" value="%{@com.ffusion.beans.cashcon.CashConStatus@CASH_CON_STATUS_PENDING==#attr.cashCon.status}" />
		<s:set var="CASH_CON_STATUS_PROCESSED" value="%{@com.ffusion.beans.cashcon.CashConStatus@CASH_CON_STATUS_PROCESSED==#attr.cashCon.status}" />
		<s:set var="CASH_CON_STATUS_PENDING_APPROVAL" value="%{@com.ffusion.beans.cashcon.CashConStatus@CASH_CON_STATUS_PENDING_APPROVAL==#attr.cashCon.status}" />
		<s:set var="CASH_CON_STATUS_UNKNOWN" value="%{@com.ffusion.beans.cashcon.CashConStatus@CASH_CON_STATUS_UNKNOWN==#attr.cashCon.status}" />
		<s:if
			test="%{#CASH_CON_STATUS_FAILED}">
			<s:set var="HighlightStatus" value="true" />
		</s:if> 
		<s:if test="%{#CASH_CON_STATUS_LIMITCHECK_FAILED}">
			<s:set var="HighlightStatus" value="true" />
		</s:if> 
		<s:if test="%{#HighlightStatus=='true'}">
			<div class="blockRow failed">
				<%-- Display CashCon Status --%>
				<span id="confirmCashCon_statusValueId"><s:set var="HighlightStatus" value="false" />
						<span class="sapUiIconCls icon-decline"></span> <!-- failed mean give red here -->
						<s:property value="#attr.cashCon.statusName" />
				</span>
		</s:if> 
		<s:elseif test="%{#CASH_CON_STATUS_PENDING}">
			<div class="blockRow pending">
				<%-- Display CashCon Status --%>
				<span id="confirmCashCon_statusValueId"><s:set var="HighlightStatus" value="false" />
						<span class="sapUiIconCls icon-future"></span><!-- this is scheduled/pending giv orange -->
						<s:property value="#attr.cashCon.statusName" />
				</span>
		</s:elseif>
		<s:elseif test="%{#CASH_CON_STATUS_PROCESSED}">
			<div class="blockRow completed">
				<%-- Display CashCon Status --%>
				<span id="confirmCashCon_statusValueId"><s:set var="HighlightStatus" value="false" />
					<span class="sapUiIconCls icon-accept"></span><!-- this is transfered give it green --> <s:property value="#attr.cashCon.statusName" />
				</span>
		</s:elseif>
		<s:elseif test="%{#CASH_CON_STATUS_PENDING_APPROVAL}">
			<div class="blockRow pending">
				<%-- Display CashCon Status --%>
				<span id="confirmCashCon_statusValueId"><s:set var="HighlightStatus" value="false" />
					<span class="sapUiIconCls icon-pending"></span> <!-- this is pending approval give color orange icon different -->
					<s:property value="#attr.cashCon.statusName" />
				</span>
		</s:elseif>
		<span id="cashConResultMessage" class="floatRight"> 
			<s:text name="jsp.cashcon_40"/>
		</span>	
	</div>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
<div class="blockContent label130">
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_266"/>:</span>
			<span class="columndata">
				<ffi:getProperty name="CashCon" property="LocationName"/>
			</span>
		</div>
		<div class="inlineBlock">
			<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_86"/>:</span>
			<span class="columndata">
				<ffi:getProperty name="CashConCompany" property="CompanyName" /> / <ffi:getProperty name="CashConCompany" property="CompanyID" />
			</span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_603"/>:</span>
			<span class="columndata">
				<ffi:getProperty name="CashConCompany" property="DisbNextCutOffTime" />
			</span>
		</div>
		<div class="inlineBlock">
			<span class="sectionsubhead sectionLabel"  >
				<s:text name="jsp.default_485"/>:
			</span>
			<span class="columndata">
				<ffi:getProperty name="CashCon" property="AmountValue.CurrencyStringNoSymbol"/>(<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
			</span>
		</div>
	</div>
	
		
	<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
		<ffi:cinclude value1="true" value2="${CashConCompany.DisbSameDayCutOffDefined}" operator="equals" >
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_604"/>:</span>
					<span class="columndata ltrow2_color">
							<ffi:getProperty name="CashConCompany" property="DisbNextSameDayCutOffTime" />
					</span>
				</div>
				<div class="inlineBlock">
					<span class="sectionsubhead sectionLabel">
						<s:text name="jsp.default_602"/>:
					</span>
					<span class="columndata ltrow2_color">
						<ffi:cinclude value1="${sameDayCashCon}" value2="true" operator="equals"><s:text name="jsp.cashcon_47"/></ffi:cinclude>
						<ffi:cinclude value1="${sameDayCashCon}" value2="false" operator="equals"><s:text name="jsp.cashcon_48"/></ffi:cinclude>
					</span>
				</div>
			</div>
		</ffi:cinclude>
	</ffi:cinclude>	
	
	
	<div class="blockRow">
		<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.cashcon_22"/>:</span>
		<span class="columndata"><ffi:getProperty name="CashCon" property="SubmitDate" /> <s:text name="jsp.cashcon_5"/> <ffi:getProperty name="User" property="Name" /></span>
	</div>
	
	<ffi:setProperty name="CCStatusApprovalPending" value="<%= String.valueOf(com.ffusion.beans.cashcon.CashConStatus.CASH_CON_STATUS_PENDING_APPROVAL) %>"/>									
	<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
		<ffi:cinclude value1="true" value2="${sameDayCashCon}" operator="equals">
			<ffi:cinclude value1="true" value2="${CashConCompany.DisbSameDayCutOffDefined}" operator="equals" >
				<ffi:cinclude value1="${status}" value2="${CCStatusApprovalPending}" operator="equals">
					 <div class="blockRow">
						<span class="columndata ltrow2_color">
							<s:text name="jsp.cashcon_49">
								<s:set var="cutOffTime" scope="page"><ffi:getProperty name="CashConCompany" property="DisbNextSameDayCutOffTime" /></s:set>
								<s:param name="value" value="%{#attr.cutOffTime}"/>
							</s:text>
						</span>
					 </div>	
				</ffi:cinclude>	
			</ffi:cinclude>	
		</ffi:cinclude>	
	</ffi:cinclude>	
</div>
<div class="btn-row">
<sj:a button="true" summaryDivId="summary" buttonType="done" onClickTopics="cancelCashconForm,resetCashconConfirmDiv"><s:text name="jsp.default_175"/></sj:a>
</div>	
</form>