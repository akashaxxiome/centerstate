<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:help id="payments_cashcontransferInquiry" className="moduleHelpClass"/>

<%
	String Subject = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.message_37", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
%>

<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<%-- the table will be identical if the transaction type is either FUNDS_TYPE_BILL_PAYMENT or FUNDS_TYPE_REC_BILL_PAYMENT --%>
				<s:if test="%{cashCon.type == @com.ffusion.beans.FundsTransactionTypes@FUNDS_TYPE_BILL_PAYMENT}">
					
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_316"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="PayeeName" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_21"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="AccountDisplayText" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_143"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="TransactionDate" /></td>
					</tr>					
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_45"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="TransactionAmount" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_430"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><s:text name="jsp.default_320"/></td>
					</tr>
				</s:if>
				<s:if test="%{cashCon.type == @com.ffusion.beans.FundsTransactionTypes@FUNDS_TYPE_REC_BILL_PAYMENT}">
					<%-- the block below is copied from the above cinclude, as the condition has an "OR" clause --%>
					<tr>
						<td colspan="2" class="sectiontitle sectionsubhead" align="left" width="282" height="14" background="/cb/web/multilang/grafx/payments/sechdr_blank_282.gif">
							&nbsp;&gt; <s:text name="jsp.default_249"/>
						</td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_316"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="PayeeName" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_21"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="AccountDisplayText" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_143"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="TransactionDate" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_45"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="TransactionAmount" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_430"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><s:text name="jsp.default_320"/></td>
					</tr>
				</s:if>
								<%-- the table will be identical if the transaction type is either FUNDS_TYPE_BILL_PAYMENT or FUNDS_TYPE_REC_BILL_PAYMENT --%>
				<s:if test="%{cashCon.type == @com.ffusion.beans.FundsTransactionTypes@FUNDS_TYPE_TRANSFER}">
					<%-- <tr>
						<td colspan="2" class="sectiontitle sectionsubhead" align="left" width="282" height="14" background="/cb/web/multilang/grafx/payments/sechdr_blank_282.gif">
							&nbsp;&gt; <s:text name="jsp.default_249"/>
						</td>
					</tr> --%>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_219"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="FromAccountDisplayText" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_425"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="ToAccountDisplayText" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_143"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="TransactionDate" /></td>
					</tr>
                    <ffi:cinclude value1="${FundTransactionMessage.TransactionUserAssignedAmountFlagName}" value2="single" operator="equals">
                    <tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_45"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground">
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyStringNoSymbol" />
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyCode" />
                        </td>
					</tr>
                    </ffi:cinclude>
                    <ffi:cinclude value1="${FundTransactionMessage.TransactionUserAssignedAmountFlagName}" value2="single" operator="notEquals">
                    <tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_221"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground">
                            <ffi:cinclude value1="${FundTransactionMessage.IsAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyStringNoSymbol" />
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyCode" />
                        </td>
					</tr>
                    <tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_427"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground">
                            <ffi:cinclude value1="${FundTransactionMessage.IsToAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionToAmountValue.CurrencyStringNoSymbol" />
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionToAmountValue.CurrencyCode" />
                        </td>
					</tr>
                    </ffi:cinclude>
                    <tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_430"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><s:text name="jsp.default_442"/></td>
					</tr>
				</s:if>
				<s:if test="%{cashCon.type == @com.ffusion.beans.FundsTransactionTypes@FUNDS_TYPE_REC_TRANSFER}">
				<%-- the block below is copied from the above transfer cinclude, as the condition has an "OR" clause --%>
					<tr>
						<td colspan="2" class="sectiontitle sectionsubhead" align="left" width="282" height="14" background="/cb/web/multilang/grafx/payments/sechdr_blank_282.gif">
							&nbsp;&gt; <s:text name="jsp.default_249"/>
						</td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_219"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="FromAccountDisplayText" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_425"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="ToAccountDisplayText" /></td>
					</tr>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_143"/></span></div>						</td>
						<td align="left" class="columndata lightBackground"><ffi:getProperty name="FundTransactionMessage" property="TransactionDate" /></td>
					</tr>					
					<ffi:cinclude value1="${FundTransactionMessage.TransactionUserAssignedAmountFlagName}" value2="single" operator="equals">
                    <tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_45"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground">
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyStringNoSymbol" />
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyCode" />
                        </td>
					</tr>
                    </ffi:cinclude>
                    <ffi:cinclude value1="${FundTransactionMessage.TransactionUserAssignedAmountFlagName}" value2="single" operator="notEquals">
                    <tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_221"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground">
                            <ffi:cinclude value1="${FundTransactionMessage.IsAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyStringNoSymbol" />
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionAmountValue.CurrencyCode" />
                        </td>
					</tr>
                    <tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_427"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground">
                            <ffi:cinclude value1="${FundTransactionMessage.IsToAmountEstimated}" value2="true">&#8776;</ffi:cinclude>
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionToAmountValue.CurrencyStringNoSymbol" />
                            <ffi:getProperty name="FundTransactionMessage" property="TransactionToAmountValue.CurrencyCode" />
                        </td>
					</tr>
                    </ffi:cinclude>
					<tr>
						<td class="columndata lightBackground">
							<div align="right">
								<span class="sectionsubhead"><s:text name="jsp.default_430"/></span></div>
						</td>
						<td align="left" class="columndata lightBackground"><s:text name="jsp.default_442"/></td>
					</tr>
				</s:if>
				<tr>
					<td class="columndata lightBackground" colspan="2">
						<div align="center">
							<span class="sectionsubhead"><s:text name="jsp.default.inquiry.message.RTE.label" /><span class="required">*</span></span>
						</div>
					</td>					
				</tr>
				<tr>
					<td class="columndata lightBackground" colspan="2">
						<s:form id="inquiryForm" name="form4" theme="simple" method="post" action="/pages/jsp/cashcon/sendCashConFundsTransMessageAction_sendCashConFundsInquiry.action" style="text-align: left;">							
							<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<input type="hidden" name="fundTransactionMessage.subject" value="<ffi:getProperty name='Subject'/>"/>																			
							<s:hidden name="cashConType" value="%{#attr.cashConType}"></s:hidden>
							<s:hidden name="fundTransactionMessage.from" value="%{#session.User.id}"></s:hidden>
							<s:hidden name="fundTransactionMessage.comment" value="%{cashCon.inquireComment}"></s:hidden>
							<div align="center">
								<p>
									<textarea class="txtbox ui-widget-content ui-corner-all" name="fundTransactionMessage.memo" rows="10" cols="75" style="overflow-y: auto;" text-align: left;" wrap="virtual" ></textarea>
								</p>
							</div>
						</s:form>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="50">&nbsp;</td>
				</tr>
				<tr>
				<td  class="ui-widget-header customDialogFooter" colspan="2" align="center">
								<sj:a id="closeInquireCashconLink" button="true" 
							  			onClickTopics="closeDialog" 
							  			title="%{getText('jsp.default_83')}"><s:text name="jsp.default_82" /></sj:a>
							  
									<sj:a id="sendCashconSubmitButton" 
										  formIds="inquiryForm" 
										  targets="resultmessage" 
										  button="true" 
										  validate="true" 
										  validateFunction="customValidation"
										  title="%{getText('Send_Inquiry')}"
										  onSuccessTopics="sendInquiryCashconFormSuccess"><s:text name="jsp.default_378" /></sj:a>
					</td>
				</tr>
			</table>
			<p></p>
		</div>

<script>
	setTimeout(function(){ns.common.renderRichTextEditor('fundTransactionMessage.memo','600',true);}, 600);
</script>