<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib uri="/struts-jquery-tags" prefix="sj"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%  
boolean isConcentration = false;
boolean isDisbursement = false;
String ft_type = String.valueOf( com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_CASH_CONCENTRATION );      // default to concentration
%>
<ffi:cinclude value1="${CashCon.Type}" value2="<%= ft_type %>" operator="equals" >
	<% isConcentration = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${CashCon.Type}" value2="<%= ft_type %>" operator="notEquals" >
	<% isDisbursement = true; %>
</ffi:cinclude>

<ffi:setProperty name="viewCashConDoneURL" value="${FullPagesPath}payments/cashcon.jsp"/>
<% if( session.getAttribute( "cashcon_details_post_jsp" ) != null ) { %>
    <ffi:setProperty name="viewCashConDoneURL" value="${cashcon_details_post_jsp}"/>
<% } %>

<div class="approvalDialogHt">
<form name="" method="post" action='<ffi:urlEncrypt url="${viewCashConDoneURL}"/>' >
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

<div class="templateConfirmationWrapper">
		<div class="confirmationDetails">
			<span id="" class="sectionLabel"><s:text name="jsp.default_435"/>:</span>
	        <span id="" class="columndata"><ffi:getProperty name="CashCon" property="TrackingID" /></span>
		</div>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
<div class="blockContent">
	<div class="blockRow">
		<span class="sectionLabel"><s:text name="jsp.default_388"/></span>
		<span><ffi:getProperty name="CashCon" property="StatusName" /></span>
	</div>
</div>
<div  class="blockHead"><s:text name="jsp.transaction.summary.label" /></div>
<div class="blockContent label130">
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span class="sectionsubhead sectionLabel" ><s:text name="jsp.common_75"/>:</span>
			<span class="columndata"><ffi:getProperty name="CashCon" property="SubmitDate" /></span>
		</div>
		<div class="inlineBlock">
			<span class="sectionsubhead sectionLabel" >
				<% if( session.getAttribute( "CashConView" ) == null ) { %>
							<s:text name="jsp.common_76"/>:
				<% } %>
			</span>
			<span class="columndata" >
				<% if( session.getAttribute( "CashConView" ) == null ) { %>
					    <ffi:getProperty name="CashCon" property="SubmittedByUserName"/>
				<% } %>
			</span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_174"/>:</span>
			<span class="columndata">
				<ffi:getProperty name="CashCon" property="DivisionName"/>
			</span>
		</div>
		<div class="inlineBlock">
			<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_266"/>:</span>
			<span class="columndata">
				<ffi:getProperty name="CashCon" property="LocationName"/>
			</span>
		</div>
	</div>
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_86"/>:</span>
        	<span class="columndata"><ffi:getProperty name="CashConCompany" property="CompanyName" /> / <ffi:getProperty name="CashConCompany" property="CompanyID" /></span>
		</div>
		<%-- QTS 403846: Don't show NextCutOffTime if completed --%>
		<ffi:cinclude value1="${CashCon.Status}" value2="6" operator="notEquals" >
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.default_603"/>:</span>
				<span class="columndata">
				    <% if (isConcentration) { %>
					    <ffi:getProperty name="CashConCompany" property="ConcNextCutOffTime" />
				    <% } %>
				    <% if (isDisbursement) { %>
					    <ffi:getProperty name="CashConCompany" property="DisbNextCutOffTime" />
				    <% } %>
				</span>
			</div>
		</ffi:cinclude>
	</div>
		
	<ffi:cinclude value1="${CashCon.Status}" value2="6" operator="notEquals" >
		<ffi:cinclude value1="${SameDayCashConEnabled}" value2="true" operator="equals">
		
				<% if (isConcentration) { %>					
					<ffi:cinclude value1="true" value2="${CashConCompany.ConcSameDayCutOffDefined}" operator="equals" >
						<div class="blockRow">
							<div class="inlineBlock" style="width: 50%">
								<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_604"/>:</span>
								<span class="columndata ltrow2_color">
									<ffi:getProperty name="CashConCompany" property="ConcNextSameDayCutOffTime" />
								</span>
							</div>
							<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel">
									<s:text name="jsp.default_602"/>:
								</span>
								<span class="columndata ltrow2_color">
									<ffi:cinclude value1="${CashCon.SameDayCashCon}" value2="true" operator="equals"><s:text name="jsp.cashcon_47"/></ffi:cinclude>
									<ffi:cinclude value1="${CashCon.SameDayCashCon}" value2="false" operator="equals"><s:text name="jsp.cashcon_48"/></ffi:cinclude>
									 
							</span>
							</div>
						</div>
					</ffi:cinclude>
				   
				<% } %>
				<% if (isDisbursement) { %>
						<ffi:cinclude value1="true" value2="${CashConCompany.DisbSameDayCutOffDefined}" operator="equals" >
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_604"/>:</span>
									<span class="columndata ltrow2_color">
											<ffi:getProperty name="CashConCompany" property="DisbNextSameDayCutOffTime" />
									</span>
								</div>
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel">
										<s:text name="jsp.default_602"/>:
									</span>
									<span class="columndata ltrow2_color">
										<ffi:cinclude value1="${CashCon.SameDayCashCon}" value2="true" operator="equals"><s:text name="jsp.cashcon_47"/></ffi:cinclude>
										<ffi:cinclude value1="${CashCon.SameDayCashCon}" value2="false" operator="equals"><s:text name="jsp.cashcon_48"/></ffi:cinclude>
									</span>
								</div>
							</div>
						</ffi:cinclude>	
					
				<% } %>
		</ffi:cinclude>
	</ffi:cinclude>
	
	<div class="blockRow">
		<div class="inlineBlock" style="width: 50%">
			<span class="sectionsubhead sectionLabel"  >
				<% if (isConcentration) { %>
				<s:text name="jsp.default_484"/>:
				<% } %>
				<% if (isDisbursement) { %>
				<s:text name="jsp.default_485"/>:
				<% } %>
			</span>
			<span class="columndata">
				<ffi:getProperty name="CashCon" property="AmountValue.CurrencyStringNoSymbol"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
			</span>
		</div>
		<ffi:cinclude value1="${CashCon.Status}" value2="6" >
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"  ><s:text name="jsp.common_133"/>:</span>
				<span class="columndata"><ffi:getProperty name="CashCon" property="ProcessedOnDate" /></span>
			</div>
		</ffi:cinclude>
	</div>
</div>
<%-- <table border="0" cellspacing="0" cellpadding="3" width="711">
	<tr>
		<td class="sectionsubhead" colspan="3" ><s:text name="jsp.common_47"/></td>
	</tr>
	<tr>
		<td class="columndata" colspan="3" >
			<ffi:getProperty name="CashConCompany" property="CompanyName" />&nbsp;-&nbsp;
			<ffi:getProperty name="CashCon" property="DivisionName" />&nbsp;-&nbsp;
			<ffi:getProperty name="CashCon" property="LocationName" />
		</td>
	</tr>
	<tr>
		<td class="columndata" colspan="3" >&nbsp;</td>
	</tr>
	
	<tr>
		<td class="sectionsubhead" ><s:text name="jsp.common_75"/></td>
		<td>&nbsp;</td>
		<td class="sectionsubhead" >
<% if( session.getAttribute( "CashConView" ) == null ) { %>
			<s:text name="jsp.common_76"/>
<% } %>
		</td>
	</tr>
	<tr>
		<td class="columndata"><ffi:getProperty name="CashCon" property="SubmitDate" /></td>
		<td class="columndata">&nbsp;</td>
		<td class="columndata" align="left">
<% if( session.getAttribute( "CashConView" ) == null ) { %>
	    <ffi:getProperty name="CashCon" property="SubmittedByUserName"/>
<% } %>
		</td>
	</tr>
    
	<tr>
		<td class="sectionsubhead" align="Right" ><s:text name="jsp.default_174"/></td>
		<td class="columndata">
			<ffi:getProperty name="CashCon" property="DivisionName"/>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="sectionsubhead" align="Right" ><s:text name="jsp.default_266"/></td>
		<td class="columndata">
			<ffi:getProperty name="CashCon" property="LocationName"/>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="sectionsubhead" align="Right" ><s:text name="jsp.default_86"/></td>
        <td class="columndata"><ffi:getProperty name="CashConCompany" property="CompanyName" /> / <ffi:getProperty name="CashConCompany" property="CompanyID" /></td>
		<td>&nbsp;</td>
	</tr>
QTS 403846: Don't show NextCutOffTime if completed
<ffi:cinclude value1="${CashCon.Status}" value2="6" operator="notEquals" >
	<tr>
		<td class="sectionsubhead" align="Right" ><s:text name="jsp.default_493"/></td>
		<td class="columndata">
		    <% if (isConcentration) { %>
			    <ffi:getProperty name="CashConCompany" property="ConcNextCutOffTime" />
		    <% } %>
		    <% if (isDisbursement) { %>
			    <ffi:getProperty name="CashConCompany" property="DisbNextCutOffTime" />
		    <% } %>
		</td>
		<td>&nbsp;</td>
	</tr>
</ffi:cinclude>
	<tr>
		<td class="sectionsubhead" align="Right" >
		<% if (isConcentration) { %>
		<s:text name="jsp.default_484"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
		<% } %>
		<% if (isDisbursement) { %>
		<s:text name="jsp.default_485"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
		<% } %>
		</td>
		<td class="columndata">
			<ffi:getProperty name="CashCon" property="AmountValue.CurrencyStringNoSymbol"/>
		</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="sectionsubhead" align="Right" ><s:text name="jsp.default_388"/></td>
		<td class="columndata"><ffi:getProperty name="CashCon" property="StatusName" /></td>
		<td>&nbsp;</td>
	</tr>
	<ffi:cinclude value1="${CashCon.Status}" value2="6" >
		<tr>
			<td class="sectionsubhead" align="Right" ><s:text name="jsp.common_133"/></td>
			<td class="columndata"><ffi:getProperty name="CashCon" property="ProcessedOnDate" /></td>
			<td>&nbsp;</td>
		</tr>
	</ffi:cinclude>
	<tr>
		<td class="sectionsubhead" align="Right" ><s:text name="jsp.default_435"/></td>
		<td class="columndata"><ffi:getProperty name="CashCon" property="TrackingID" /></td>
		<td>&nbsp;</td>
	</tr>
<% if( session.getAttribute( "CashConView" ) != null ) { %>
	<tr>
		<td class='sectionsubhead' colspan="6">
			include batch Transaction History
			<ffi:include page="${PagesPath}cashcon/inc/include-view-transaction-history.jsp" />
		</td>
	</tr>
<% } %>
    
</table> --%>
<% if( session.getAttribute( "CashConView" ) != null ) { %>
	<%-- include batch Transaction History --%>
	<ffi:include page="${PagesPath}cashcon/inc/include-view-transaction-history.jsp" />
<% } %>
</form>
</div>