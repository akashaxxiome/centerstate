<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
	<div id="detailsPortlet" class="portlet wizardSupportCls ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
        <div class="portlet-header ui-widget-header ui-corner-all">
        	<h1 class="portlet-title"><s:text name="jsp.cashcon_2"/></h1>
		</div>
        <div class="portlet-content">

		    <sj:tabbedpanel id="TransactionWizardTabs" >
		    	
		        <sj:tab id="inputTab" target="inputDiv" label="%{getText('jsp.default_390')}"/>
		        <sj:tab id="verifyTab" target="verifyDiv" label="%{getText('jsp.default_392')}"/>
		        <sj:tab id="confirmTab" target="confirmDiv" label="%{getText('jsp.default_393')}"/>
		        <div id="inputDiv" style="border:1px">
					<s:text name="jsp.cashcon_21"/>
		            <br><br>
		        </div>
		        <div id="verifyDiv" style="border:1px">
		            <s:text name="jsp.cashcon_37"/>
		            <br><br>
		        </div>
		        <div id="confirmDiv" style="border:1px">
		            <s:text name="jsp.cashcon_10"/>
		            <br><br>
		            <div align="center">
			            <sj:a 
	      					button="true" 
							onClick="ns.cashcon.backInput();"
	    				><s:text name="jsp.default_57"/></sj:a>
    				</div>
		        </div>
		        <div id="resetConfirmDiv" style="display:none">
		            <s:text name="jsp.cashcon_10"/>
		            <br><br>
		            <div align="center">
			            <sj:a 
	      					button="true" 
							onClick="ns.cashcon.backInput();"
	    				><s:text name="jsp.default_57"/></sj:a>
    				</div>
				</div> 
				
		    </sj:tabbedpanel>
			
		</div>
		<div id="transferDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	       		  <li><a href='#' onclick="ns.common.bookmarkThis('detailsPortlet')"><span class="sapUiIconCls icon-create-session" style="float:left;"></span>Bookmark</a></li>
	              <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
    </div>
<script>    
	//Initialize portlet with settings icon
	ns.common.initializePortlet("detailsPortlet");
</script>