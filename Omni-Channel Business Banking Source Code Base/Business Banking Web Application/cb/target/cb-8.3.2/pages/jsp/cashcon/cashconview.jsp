<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.ffusion.beans.FundsTransactionTypes,
                 com.ffusion.csil.core.common.EntitlementsDefines,
				 com.ffusion.beans.user.User,
				 com.ffusion.efs.adapters.profile.interfaces.CustomerAdapter"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_cashconview" className="moduleHelpClass"/>
<%  boolean isConcentration = false;
	boolean isDisbursement = false;
	String ft_type = null;
	ft_type = "" + FundsTransactionTypes.FUNDS_TYPE_CASH_CONCENTRATION;      // default to concentration
	%>
	<ffi:cinclude value1="${CashCon.Type}" value2="<%= ft_type %>" operator="equals" >
	<% isConcentration = true; %>
		<s:set var="tmpI18nStr" value="%{getText('jsp.cashcon_38')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
	</ffi:cinclude>
	<ffi:cinclude value1="${CashCon.Type}" value2="<%= ft_type %>" operator="notEquals" >
	<% isDisbursement = true; %>
		<s:set var="tmpI18nStr" value="%{getText('jsp.cashcon_39')}" scope="request" /><ffi:setProperty name='PageHeading' value='${tmpI18nStr}'/>
	</ffi:cinclude>

<s:include value="inc/include-view-transaction-details-constants.jsp"/>
<ffi:setProperty name="tmp_url" value="cashcon.jsp" URLEncrypt="true"/>
<form action="<ffi:getProperty name='tmp_url'/>" method="post" name="CashConNew">
<ffi:removeProperty name="tmp_url"/>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<!-- <div class="paneWrapper">
  	<div class="paneInnerWrapper"> -->
   		<!-- <div class="blockHead"><ffi:getProperty name="PageHeading" /></div>
   		<div class="paneContentWrapper"> -->
	   		<ffi:setProperty name="CashConView" value="true" />
			<s:include value="inc/include-view-cashcon-details.jsp" />
			<ffi:removeProperty name="CashConView" />
	 <!--  </div>
 </div>
</div>   -->
</form>
<div class="ffivisible" style="height:50px;">&nbsp;</div>
<div align="center" class="ui-widget-header customDialogFooter"> 
	    	<sj:a button="true" onClickTopics="closeDialog"><s:text name="jsp.default_175"/></sj:a>
</div>
