<%@ page import="com.ffusion.beans.user.User,
				 com.ffusion.beans.user.UserLocale,
				 com.ffusion.util.UserLocaleConsts,com.ffusion.efs.adapters.profile.exception.ProfileException"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- ---- TRANSACTION HISTORY ---- --%>
<div class="marginTop10">
          <ffi:cinclude value1="${isTemplate}" value2="true">
   			<span  id="templateHis" class="toggleClick"><strong><s:text name="jsp.default_411"/></strong> <span class="sapUiIconCls icon-navigation-down-arrow"></span></span>
  		</ffi:cinclude>
   		<ffi:cinclude value1="${isTemplate}" value2="true" operator="notEquals">
       		<span id="transactionHis" class="toggleClick"><strong><s:text name="jsp.default_437"/></strong> <span class="sapUiIconCls icon-navigation-down-arrow"></span></span>
       </ffi:cinclude>	
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="tableAlerternateRowColor tdWithPadding transactionHistoryTable marginTop10 tableData" style="display:none">
   <tr>
       <td class="sectionsubhead" nowrap><s:text name="jsp.default_142"/></td>
       <td class="sectionsubhead" nowrap><s:text name="jsp.default_438"/></td>
       <td class="sectionsubhead" nowrap><s:text name="jsp.default_333"/></td>
       <td class="sectionsubhead" nowrap><s:text name="jsp.default_170"/></td>
   </tr>
   <s:iterator value="TransactionHistory" var="historyItem">
   	<tr>
       <td class="columndata"><ffi:getProperty name="historyItem" property="TranDate"/></td>
       <td class="columndata"><ffi:getProperty name="historyItem" property="State"/></td>
       <td class="columndata"><ffi:getProperty name="historyItem" property="ProcessedBy"/></td>
       <td class="columndata"><div class="description"><ffi:getProperty name="historyItem" property="Message"/></div></td> 
   </tr>
   </s:iterator>
</table></div>
<ffi:removeProperty name="stateDisplay"/>
<script type="text/javascript">
	$(document).ready(function(){
	
	<ffi:cinclude value1="${isTemplate}" value2="true">
	 $("#templateHis").next().slideDown();
		$("#templateHis").find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
		$("#templateHis").removeClass("expandArrow").addClass("collapseArrow");
	 </ffi:cinclude>
      <ffi:cinclude value1="${isTemplate}" value2="true" operator="notEquals">
		
		  $("#transactionHis").next().slideDown();
		$("#transactionHis").find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
		$("#transactionHis").removeClass("expandArrow").addClass("collapseArrow");
		 
	  </ffi:cinclude>	
	  
	  
		$( ".toggleClick" ).click(function(){ 
			if($(this).next().css('display') == 'none'){
				$(this).next().slideDown();
				$(this).find('span').removeClass("icon-navigation-down-arrow").addClass("icon-navigation-up-arrow");
				$(this).removeClass("expandArrow").addClass("collapseArrow");
			}else{
				$(this).next().slideUp();
				$(this).find('span').addClass("icon-navigation-down-arrow").removeClass("icon-navigation-up-arrow");
				$(this).addClass("expandArrow").removeClass("collapseArrow");
			}
		});
	});

</script>