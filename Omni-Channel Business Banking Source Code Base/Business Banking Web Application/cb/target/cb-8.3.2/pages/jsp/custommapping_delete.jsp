<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object id="DeleteMappingDefinition" name="com.ffusion.tasks.fileImport.DeleteMappingDefinitionTask" scope="session"/>
<ffi:process name="DeleteMappingDefinition"/>


<s:text name="jsp.default_mapping_deleted"/>
