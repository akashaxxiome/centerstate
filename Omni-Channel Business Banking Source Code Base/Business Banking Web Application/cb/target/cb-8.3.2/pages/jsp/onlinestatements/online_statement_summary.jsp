<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.banking.Transactions"%>
<%@ page import="com.ffusion.beans.reporting.ExportFormats"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<style>
<!--
	td {
	    font-weight: normal;
	}
-->
</style>

<% String v34_4a = ""; String v34_2b = ""; String v34_1d = ""; String v34_1e = ""; %>

<%-- check if Statement is available/searched or not --%>
<s:if test="statement != null">
<div id="onlineStatementSummaryContainer">
	<ffi:help id="acctMgmt_onlineStatements_details" />

	<%-- set variables to display records in alternate color --%>
	<ffi:setProperty name="pband1" value="class=\"tabledata_white\""/>
	<ffi:setProperty name="pband2" value="class=\"tabledata_gray\""/>
	
	<%-- Set variable to decide Check image entitlement --%>
	<ffi:setProperty name="imageViewAllowed" value="false"/>
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CHECK_IMAGING %>" >
		<ffi:setProperty name="imageViewAllowed" value="true"/>
	</ffi:cinclude>
		
	<%-- Task for searching image by setting Transaction Id --%>
	<ffi:cinclude value1="${imageViewAllowed}" value2="true" operator="equals">
		<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="SearchImage" scope="session"/>
		<ffi:setProperty name="SearchImage" property="Init" value="true"/>
		<ffi:setProperty name="SearchImage" property="TransactionsInSessionName" value="StatementTransactions"/>
		<ffi:setProperty name="SearchImage" property="AccountInSessionName" value="StatementAccount"/>
		<ffi:process name="SearchImage"/><!-- Initialize Search Image task.-->
		<ffi:setProperty name="SearchImage" property="Init" value="false"/>
		<s:set var="FFISearchImage" value="#session.SearchImage" scope="session"/>
	</ffi:cinclude>
	
	<%-- This is required for dynamic sizing of the printer ready window. --%>
	<%-- <ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase" scope="request"/>
	<ffi:setProperty name="NewReportBase" property="ReportName" value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.RPT_TYPE_ONLINE_STATEMENT %>"/>
	<ffi:process name="NewReportBase"/> --%>
	
            <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff">
                 <tr>
                    <td class="filter_table" valign="top">
                        <table cellpadding="0" cellspacing="0" border="0">
                        	<tr>
                        		<td align="right"><span class="txt_normal"><s:text name="jsp.account_7"/>:</span></td>
                        		<td width="10">&nbsp;</td>
                        		<td><span class="txt_normal_bold"><s:property value="%{statementAccount.ConsumerDisplayText}"/></span></td>
                        	</tr>
                        	<tr>
                        		<td align="right"><span class="txt_normal"><s:text name="jsp.onlineStatements.summary.period"/>:</span></td>
                        		<td>&nbsp;</td>
                        		<td><span class="txt_normal_bold"><s:property value="%{statement.StatementStartDate}"/> - <s:property value="%{statement.StatementEndDate}"/></span></td>
                        	</tr>
                        	<tr>
                        		<td align="right" valign="top"><span class="txt_normal">Address:</span></td>
                        		<td>&nbsp;</td>
                        		<td valign="top">
			                        <%-- Display user's name and address --%>
			                        <span class="txt_normal ffiVisible"><s:property value="%{statement.NameAddress1}"/></span>
			                        <span class="txt_normal ffiVisible"><s:property value="%{statement.NameAddress2}"/></span>
			                        <span class="txt_normal ffiVisible"><s:property value="%{statement.NameAddress3}"/></span>
			                        <span class="txt_normal ffiVisible"><s:property value="%{statement.NameAddress4}"/></span>
			                        <span class="txt_normal ffiVisible"><s:property value="%{statement.NameAddress5}"/></span>
			                        <span class="txt_normal ffiVisible"><s:property value="%{statement.NameAddress6}"/></span>
				                </td>
                        	</tr>
                        </table>
                    </td>
                    <td class="filter_table" valign="top">&nbsp;&nbsp;</td>
                    <td class="filter_table" valign="top" align="right">
                        <!-- Export and Printer Ready button -->
                        <%-- <p>
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH %>"/>
							<ffi:setProperty name="Math" property="Value1" value="${ReportData.ReportCriteria.CurrentReportOptionValue}"/>
							<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PRINTER_READY_PAGEWIDTH %>"/>
							<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="notEquals">
							    <ffi:setProperty name="Math" property="Value1" value="${ReportData.ReportCriteria.CurrentReportOptionValue}"/>
							</ffi:cinclude>
							<ffi:setProperty name="Math" property="Value2" value="100"/>
							<ffi:setProperty name="printerReadyWindowWidth" value="${Math.Add}"/>
						</p>
						
                            <ffi:object id="FFINewReportBase" name="com.ffusion.tasks.reporting.NewReportBase" scope="session"/>
	                        <ffi:setProperty name="FFINewReportBase" property="ReportName" value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.RPT_TYPE_ONLINE_STATEMENT %>"/>
	                                               
	                        <sj:a id="viewExportDetails" onclick="ns.onlineStatements.onExportClick();"
									button="true" buttonIcon="ui-icon-info"><s:text name="jsp.default_195"/></sj:a>
							<sj:a id="viewPrinterReadyVersion" onclick="ns.onlineStatements.onPrinterReadyClick();"  
									button="true" buttonIcon="ui-icon-info"><s:text name="jsp.default_332"/></sj:a> --%>
					 </td>
                </tr>
            </table>
            
            
            <table class="ui-widget marginTop10" style="border-collapse: collapse;" width="98%" cellpadding="3" cellspacing="1" align="center">
            	<thead class="ui-widget-header">
                <tr>
                    <th>
                        <div align="center"><s:text name="onlinestatement_label_beginning_balance"/></div>
                    </th>
                    <th>
                        <div align="center"><s:text name="onlinestatement_label_deposits"/></div>
                    </th>
                    <th align="right">
                        <div align="center"><s:text name="onlinestatement_label_withdrawals"/></div>
                    </th>
                    <th align="right">
                        <div align="center"><s:text name="onlinestatement_label_ending_balance"/></div>
                    </th>
                    <th align="right">
                        <div align="center"><s:text name="onlinestatement_label_interest_earned"/></div>
                    </th>
                    <th align="right">
                        <div align="center"><s:text name="jsp.onlineStatements.summary.earnedAPRYield"/>&nbsp;&nbsp;</div>
                    </th>
                </tr>
                </thead>
                
                <tbody class="ui-widget-content">
                <tr>
                    <td height="25">
                        <div align="center"><s:property value="%{statement.BalanceSummary.PreviousAmountValue}"/></div>
                    </td>
                    <td>
                        <div align="center"><s:property value="%{statement.BalanceSummary.TotalDepositsValue}"/></div>
                    </td>
                    <td align="right">
                        <div align="center"><s:property value="%{statement.BalanceSummary.TotalWithdrawalsValue}"/></div>
                    </td>
                    <td align="right">
                        <div align="center"><s:property value="%{statement.BalanceSummary.NewAmountValue}"/></div>
                    </td>
                    <td align="right">
                        <div align="center"><s:property value="%{statement.InterestSummary.InterestEarnedValue}"/></div>
                    </td>
                    <td align="right">
                        <div align="center"><s:property value="%{statement.Map.FormattedAPYInterestRate}"/></div>
                    </td>
                </tr>
                </tbody>
            </table>
            
			<s:set var="includeCredits" value="true" />
			<s:set var="includeDebits" value="true" />
			<s:set var="trueFlag" value="true" />
			
			 <s:set var="statementTransactionType" value="statementSearchCriteria.statementTransactionType" /> 
             <s:set var="TRANS_CREDITS" value="@com.ffusion.beans.istatements.IStatementsRptConsts@SEARCH_CRITERIA_VALUE_WITHDRAWALS_TRANS_TYPE" />
             <s:set var="TRANS_DEBITS" value="@com.ffusion.beans.istatements.IStatementsRptConsts@SEARCH_CRITERIA_VALUE_DEPOSITS_TRANS_TYPE" />
			
			<s:if test="%{#statementTransactionType==#TRANS_CREDITS}">
				 <s:set var="includeCredits" value="false" />
			</s:if>
			<s:elseif test="%{#statementTransactionType==#TRANS_DEBITS}">
				 <s:set var="includeDebits" value="false" />
			</s:elseif>
			
			<%
				String tempStr = "";
			%>
			<div style="width:98%; margin:0 auto;">
			<s:if test="#includeCredits == #trueFlag">
			    &nbsp;&nbsp;<s:set var="gridCaptionText" value="getText('onlinestatement_label_credits_totalling',{statement.BalanceSummary.NumDeposits,statement.BalanceSummary.TotalDeposits})"/>
				
			    <ffi:setProperty name="gridCaption" value="${gridCaptionText}"/>
			    
			    <s:if test="statement.BalanceSummary.NumDeposits > 0">
			   	 	<ffi:setProperty name="gridType" value="Credit"/>
			        <s:include value="/pages/jsp/onlinestatements/inc/online_statement_grid.jsp" />
			    </s:if>
			</s:if>
			
			<s:if test="#includeDebits == #trueFlag">
			    &nbsp;&nbsp;<s:set var="gridCaptionText" value="getText('onlinestatement_label_dedits_totalling',{statement.BalanceSummary.NumWithdrawals,statement.BalanceSummary.TotalWithdrawals})"/>
                
                <ffi:setProperty name="gridCaption" value="${gridCaptionText}"/>
			    
			    <s:if test="statement.BalanceSummary.NumWithdrawals > 0">
			    	<s:if test="!statement.Transactions.isEmpty()">
			    		<ffi:setProperty name="gridType" value="Debit"/>
			            <s:include value="/pages/jsp/onlinestatements/inc/online_statement_grid.jsp" />	
			    	</s:if>
			    </s:if>
			</s:if>
			</div>
			<br>
			<%-- Display Reserve Summary Details start --%>
			<s:if test="statement.ReserveSummary != null">
			<%-- <ffi:cinclude value1="${Statement.ReserveSummary}" value2="" operator="notEquals"> --%>
	            <table class="ui-widget" style="border-collapse: collapse;" width="98%" cellpadding="3" cellspacing="1" align="center">
	                <thead class="ui-widget-header">
		                <tr>
		                    <th colspan="4" align="left">&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_banking_reserve_summary"/></th>
		                </tr>
	                </thead>
			                <tbody class="ui-widget-content">
			                	<tr class="ui-widget-content jqgrow ui-row-ltr">
			                   		 <td height="25" align="left">&nbsp;<s:text name="onlinestatement_label_previous_reserve_in_use"/>
			                   		 </td>
				                    <td>
				                        <div align="right"><s:property value="%{statement.ReserveSummary.PreviousReserveAmountValue}"/></div>
				                    </td>
				                    <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_periodic_interest_rate"/>
				                    </td>
				                    <td>
				                        <div align="right"><s:property value="%{statement.Map.FormattedPeriodIntRate}"/></div>
				                    </td>
			                	</tr>
			                	
			                	<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
			                    	<td height="25" align="left">&nbsp;<s:text name="onlinestatement_label_payments_on_reserve"/>
			                    	</td>
				                    <td>
				                        <div align="right"><s:property value="%{statement.ReserveSummary.PmtsOnReserveAmountValue}"/></div>
				                    </td>
			                    	<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_annual_percentage_rate"/>
			                    	</td>
				                    <td align="right">
				                        <div align="right"><s:property value="%{statement.Map.FormattedAnnualIntRate}"/></div>
				                    </td>
		                		</tr>
		                		
			                	<tr class="ui-widget-content jqgrow ui-row-ltr">
			                    	<td height="25" align="left">&nbsp;<s:text name="onlinestatement_label_reserve_transactions"/>
			                    	</td>
				                    <td align="right">
				                        <div align="right"><s:property value="%{statement.ReserveSummary.ReserveTransAmountValue}"/></div>
				                    </td>
			                    	<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_approved_reserve"/>
			                    	</td>
				                    <td align="right">
				                        <div align="right"><s:property value="%{statement.ReserveSummary.ApprovedReserveAmountValue}"/></div>
				                    </td>
			                	</tr>
			                	
			                	<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
			                    	<td  height="25" align="left">&nbsp;<s:text name="onlinestatement_label_finance_charge"/>
			                    	</td>
				                    <td align="right">
				                        <div align="right"><s:property value="%{statement.ReserveSummary.FinanceChargeAmountValue}"/></div>
				                    </td>
			                    	<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_available_reserve"/>
			                    	</td>
				                    <td align="right">
				                        <div align="right"><s:property value="%{statement.ReserveSummary.AvailableReserveAmountValue}"/></div>
				                    </td>
			                	</tr>
			                	<tr class="ui-widget-content jqgrow ui-row-ltr">
			                    	<td  height="25" align="left">&nbsp;<s:text name="onlinestatement_label_new_reserve_in_use"/>
			                    	</td>
				                    <td align="right">
				                        <div align="right"><s:property value="%{statement.ReserveSummary.NewReserveAmountValue}"/></div>
				                    </td>
				                    <td align="left">&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_avg_daily_reserve_in_use"/>
				                    </td>
				                    <td align="right">
				                        <div align="right"><s:property value="%{statement.ReserveSummary.ResInUseFinanceChargeAmountValue}"/></div>
				                    </td>
			                	</tr>
			                </tbody>
			            </table>
			            
			            <br>
			            
			            <table class="ui-widget" style="border-collapse: collapse;" width="98%" cellpadding="3" cellspacing="0" align="center">
			            	<thead class="ui-widget-header">
			                <tr>
			                    <th>
			                        <div align="left" class="datagrid_actionColumn">&nbsp;&nbsp;<ffi:link url="#" onclick="ns.onlineStatements.openBankReserveNoticeDialog();"><s:text name="jsp.onlineStatements.summary.bankNotice"/></ffi:link></div>
			                    </th>
			                </tr>
			                </thead>
			                
			    <ffi:setProperty name="Statement" property="ReserveSummary.Key" value="<%= com.ffusion.istatements.adapters.interfaces.IStatementDefines.RESERVE_SUMMARY_TEXT %>"/>
			    <ffi:cinclude value1="${Statement.ReserveSummary.Value}" value2="" operator="notEquals">
			    			<tbody class="ui-widget-content">
			                <tr class="ui-widget-content jqgrow ui-row-ltr">
			                    <td  height="25" align="left">
			                        <s:property value="%{statement.ReserveSummary.Value}"/>
			                    </td>
			                </tr>
			                </tbody>
			    </ffi:cinclude>
			            </table>
			            <br>
			<%-- </ffi:cinclude> --%>
			</s:if>
			<%-- Display Reserve Summary Details ends --%>

			<%-- Display Monthly Account Summary Details start --%>
			<s:if test="statement.MonthlyAccountSummary != null">
            	<table class="ui-widget" style="border-collapse: collapse;" width="98%" cellpadding="3" cellspacing="0" align="center">
            		<thead class="ui-widget-header">
                		<tr>
		                    <th colspan="4" align="left">
		                        <span>&nbsp;<s:text name="onlinestatement_label_resource_checking_acct_summary"/></span>
		                    </th>
                		</tr>
               		</thead>
               		<tbody class="ui-widget-content">
                		<tr class="ui-widget-content jqgrow ui-row-ltr">
		                    <td height="25">&nbsp;<s:text name="onlinestatement_label_avg_collected_balance"/>
		                    </td>
		                    <td>
                        		<div align="right"><s:property value="%{statement.MonthlyAccountSummary.AvgColBalValue}"/></div>
		                    </td>
		                    <td>
		                        &nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_cash_deposited"/>
		                    </td>
		                    <td>
		                        <div align="right"><s:property value="%{statement.MonthlyAccountSummary.CashItemsDeposited}"/></div>
		                    </td>
                		</tr>
                		
                		<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
		                    <td  height="25">&nbsp;<s:text name="onlinestatement_label_deposits"/></td>
		                    <td>
		                        <div align="right"><s:property value="%{statement.MonthlyAccountSummary.NumDeposits}"/></div>
		                    </td>
		                    <td>
		                        &nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_transactions_counted_toward_limit"/>
		                    </td>
		                    <td align="right">
		                        <div align="right"><s:property value="%{statement.MonthlyAccountSummary.TransTowardLimit}"/></div>
		                    </td>
		                </tr>
		                
                		<tr class="ui-widget-content jqgrow ui-row-ltr">
		                    <td  height="25">&nbsp;<s:text name="onlinestatement_label_items_deposited"/>
		                    </td>
		                    <td align="right">
		                        <div align="right"><s:property value="%{statement.MonthlyAccountSummary.ItemsDeposited}"/></div>
		                    </td>
		                    <td>
		                        &nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_transactions_not_counted_toward_limit"/>
		                    </td>
		                    <td align="right">
		                        <div align="right"><s:property value="%{statement.MonthlyAccountSummary.NumElectronicTrans}"/></div>
		                    </td>
                		</tr>
                		
                		<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
		                    <td  height="25">&nbsp;<s:text name="onlinestatement_label_withdrawals"/></td>
		                    <td align="right">
		                        <div align="right"><s:property value="%{statement.MonthlyAccountSummary.NumWithdrawals}"/></div>
		                    </td>
		                    <td>
		                        &nbsp;&nbsp;&nbsp;&nbsp;<ffi:getL10NString rsrcFile="efs" msgKey="jsp/statement.jsp-4" parm0="${Statement.MonthlyAccountSummary.ExcessLimit}" parm1="${Statement.MonthlyAccountSummary.NumExcessTrans}" parm2="${Statement.MonthlyAccountSummary.ExcessTransTotalFeeAmount}"/>
		                        <s:text name="onlinestatement_label_transactions_in_excess_of">
				                      	<s:param >
				                      		<s:property value="%{statement.MonthlyAccountSummary.ExcessLimit}"/>
				                      	</s:param>
				                      	<s:param >
				                      		<s:property value="%{statement.MonthlyAccountSummary.NumExcessTrans}"/>
				                      	</s:param>
				                      	<s:param >
				                      		<s:property value="%{statement.MonthlyAccountSummary.ExcessTransTotalFeeAmount}"/>
				                      	</s:param>
				                </s:text>
		                    </td>
		                    <td align="right">
		                        <div align="right"><s:property value="%{statement.MonthlyAccountSummary.ExcessFeeAmountValue}"/></div>
		                    </td>
                		</tr>
               		</tbody>
            	</table>
            <br>
		</s:if>
		<%-- Display Monthly Account Summary Details ends --%>
	
		<%-- Display Interest Balance Summary Details start --%>
			<s:if test="statement.InterestBalanceSummary != null">
            	<table class="ui-widget" style="border-collapse: collapse;" width="98%" cellpadding="3" cellspacing="0" align="center">
            		<thead class="ui-widget-header">
		                <tr>
		                    <th colspan="4" align="left">
		                        &nbsp;<ffi:getL10NString rsrcFile="efs" msgKey="jsp/statement.jsp-5" parm0="${Statement.InterestBalanceSummary.ReserveReqAmount}"/>
		                         <s:text name="onlinestatement_label_transactions_in_excess_of">
				                      	<s:param >
				                      		<s:property value="%{statement.InterestBalanceSummary.ReserveReqAmount}"/>
				                      	</s:param>
				                </s:text>
		                    </th>
		                </tr>
                	</thead>
                	<tbody class="ui-widget-content">
	                <tr class="ui-widget-content jqgrow ui-row-ltr">
	                    <td colspan="4"  height="25">
	                        &nbsp;
	                    </td>
	                </tr>
	                
                	<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
	                    <td colspan="4" valign="bottom"  height="25">
	                        <span class="txt_normal_bold"><s:text name="onlinestatement_label_interest_calculation"/></span>
	                    </td>
                	</tr>
                	
                	<tr class="ui-widget-content jqgrow ui-row-ltr">
	                    <td  height="25">&nbsp;<s:text name="onlinestatement_label_avg_ledger_balance"/>
	                    </td>
	                    <td>
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.AvgLedgerBalanceAmountValue}"/>
	                        </div>
	                    </td>
	                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_avg_float"/>
	                    </td>
	                    <td>
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.AvgFloatAmountValue}"/>
	                        </div>
	                    </td>
                	</tr>
                	
                	<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
                    	<td  height="25">&nbsp;<s:text name="onlinestatement_label_avg_collected_balance"/>
                    	</td>
	                    <td>
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.AvgColAmountValue}"/>
	                        </div>
	                    </td>
	                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_avg_reserve_requirement"/></td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.AvgReserveReqAmountValue}"/>
	                        </div>
	                    </td>
                	</tr>
                	
                	<tr class="ui-widget-content jqgrow ui-row-ltr">
                    	<td  height="25">&nbsp;<s:text name="onlinestatement_label_avg_balance_qualifying_for_interest"/>
						</td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.AvgBalQualIntAmountValue}"/>
	                        </div>
	                    </td>
                    	<td>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_avg_interest_rate"/></td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.Map.FormattedAvgIntRate}"/>
	                        </div>
	                    </td>
                	</tr>
                	
                	<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
                    	<td  height="25">&nbsp;<s:text name="onlinestatement_label_avg_daily_accural"/>
                    	</td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.AvgDailyAccrAmountValue}"/>
	                        </div>
	                    </td>
	                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_no_of_days_qualifying"/></td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.NumDaysQual}"/>
	                        </div>
	                    </td>
                	</tr>
                	
                	<tr class="ui-widget-content jqgrow ui-row-ltr">
	                    <td  height="25">&nbsp;<s:text name="onlinestatement_label_total_int_to_be_credited"/>
	                    </td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.TotalIntCreditAmountValue}"/>
	                        </div>
	                    </td>
                    	<td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;
                    	</td>
                	</tr>
                	
	                <tr class="ui-widget-content jqgrow ui-row-ltr">
	                    <td colspan="4"  height="25">
	                        &nbsp;
	                    </td>
	                </tr>
	                
                	<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
	                    <td colspan="6" valign="bottom"  height="25">
	                        <span class="txt_normal_bold"><s:text name="onlinestatement_label_overdraft_charge_calculation"/></span>
	                    </td>
                	</tr>
                	
                	<tr class="ui-widget-content jqgrow ui-row-ltr">
	                    <td  height="25">&nbsp;<s:text name="onlinestatement_label_number_of_days_overdrawn"/></td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.NumDaysOver}"/>
	                        </div>
	                    </td>
	                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_avg_overdraft"/></td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.AvgDailyOverAmountValue}"/>
	                        </div>
	                    </td>
                	</tr>
                	
                	<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
	                    <td  height="25">&nbsp;<s:text name="onlinestatement_label_avg_overdraft_rate"/></td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.Map.FormattedAvgOverRate}"/>
	                        </div>
	                    </td>
	                    <td >&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_avg_daily_overdraft_charge"/></td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.AvgDailyOverAmountValue}"/>
	                        </div>
	                    </td>
                	</tr>
                	
                	<tr class="ui-widget-content jqgrow ui-row-ltr">
	                    <td  height="25">&nbsp;<s:text name="onlinestatement_label_no_of_days_charged"/></td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.NumDaysCharged}"/>
	                        </div>
	                    </td>
	                    <td>&nbsp;&nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_total_overdraft_to_be_charged"/></td>
	                    <td align="right">
	                        <div align="right">
	                        	<s:property value="%{statement.InterestBalanceSummary.TotalOverChargedAmountValue}"/>
	                        </div>
	                    </td>
               		 </tr>
                	</tbody>
            	</table>
           	<br>
		</s:if>
		<%-- Display Interest Balance Summary Details end --%>
		
		<%-- Display Statement Daily Balances Details start --%>
		<s:if test="statementDailyBalances != null">
            <table class="ui-widget" style="border-collapse: collapse;" width="98%" cellpadding="3" cellspacing="0" align="center">
            	<thead class="ui-widget-header">
	                <tr>
	                    <th colspan="3" align="left">
	                        &nbsp;<span><s:text name="onlinestatement_label_daily_bal_acct_summary"/></span>
	                    </th>
	                </tr>
                </thead>
                <tbody class="ui-widget-content">
                <tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
                    <td  height="25" class="txt_normal_bold">
                        &nbsp;&nbsp;&nbsp;<s:text name="onlinestatement_label_column_date"/>
                    </td>
                    <td class="txt_normal_bold">
			               <div align="right"><s:text name="onlinestatement_label_column_balance"/></div>
                    </td>
                    <td align="right" class="txt_normal_bold"><s:text name="onlinestatement_label_column_reserve_in_use"/>&nbsp;&nbsp;
                    </td>
                </tr>

    			<ffi:setProperty name="Math" property="Value1" value="3" />
    			<ffi:setProperty name="Math" property="Value2" value="1" />

	    			<s:iterator value="%{statementDailyBalances}" status="listStatus" var="statementDailyBalance">
	        			<ffi:getProperty name="pband${Math.Value2}" assignTo="v34_4a"/>
	        			<ffi:setProperty name="band" value="<%= v34_4a %>"/>
	                	
	                	<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
		                    <td  height="25" <ffi:getProperty name="band" encode="false"/>>
		                        &nbsp;<s:property value="%{#statementDailyBalance.BalanceDate}"/>
		                    </td>
		                    <td  height="25" <ffi:getProperty name="band" encode="false"/>>
		                        <div align="right" style="height:25px;">
		                            <s:property value="%{#statementDailyBalance.AmountValue}"/>
		                       </div>
		                    </td>
		                    <td  height="25" align="right" <ffi:getProperty name="band" encode="false"/>>
		                        <div align="right" style="height:25px;">
		                            <s:property value="%{#statementDailyBalance.ReserveInUseAmountValue}"/>
		                        </div>
		                    </td>
	                	</tr>
	        			<ffi:setProperty name="Math" property="Value2" value="${Math.Subtract}"/>
	        		</s:iterator>
    			</tbody>
    		</table>
		</s:if>
		<%-- Display Statement Daily Balances Details end --%>

		<%-- Display Statement Messages Details start --%>
		<s:if test="statementMessages != null">
			<s:if test="!statementMessages.isEmpty()">
    		<br>
            	<table class="ui-widget" style="border-collapse: collapse;" width="98%" cellpadding="3" cellspacing="0" border="0" align="center">
               		<thead class="ui-widget-header">
                		<tr>
                    		<th align="left">&nbsp;<s:text name="onlinestatement_label_imp_info"/>
                    		</th>
                		</tr>
               		 </thead>
			        <ffi:setProperty name="Math" property="Value1" value="3" />
			        <ffi:setProperty name="Math" property="Value2" value="1" />
			        
        			<s:iterator value="%{statementMessages}" status="listStatus" var="statementMessage">
						<ffi:getProperty name="pband${Math.Value2}" assignTo="v34_4a"/>
						<ffi:setProperty name="band" value="<%= v34_4a %>"/>
						<tbody class="ui-widget-content">
		                	<tr>
		                    	<td <ffi:getProperty name="band" encode="false"/> style="height:25px !important;">
		            				<s:property value="%{#statementMessage.Memo}"/>
		                   		</td>
		                	</tr>
                		</tbody>
            			<ffi:setProperty name="Math" property="Value2" value="${Math.Subtract}" />
            		</s:iterator>
            	</table>
            	<br>
    		</s:if>
		</s:if>
		<%-- Display Statement Messages Details end --%>
<%-- <ffi:removeProperty name="GenerateReportBase"/>
<ffi:removeProperty name="UpdateReportCriteria"/>
<ffi:removeProperty name="ExportReport"/>
<ffi:removeProperty name="ReportData"/>
<ffi:removeProperty name="FilterValue"/>
<ffi:removeProperty name="FilterValue1"/>
<ffi:removeProperty name="FilterValue2"/>
<ffi:removeProperty name="current_account_nick_name"/>
<ffi:removeProperty name="includeCredits"/>
<ffi:removeProperty name="includeDebits"/>
<ffi:removeProperty name="printerReadyWindowWidth"/>
<ffi:removeProperty name="TempProperty"/>
<ffi:removeProperty name="tempURL"/>
<ffi:removeProperty name="imageViewAllowed"/> --%>
</div>
</s:if>

 <script type="text/javascript">
<!--
$(document).ready(function(){
	$('#onlineStatementSummaryContainer').portlet({
		generateDOM: true,
		helpCallback: function(){
			var helpFile = $('#onlineStatementSummaryContainer').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		},
		title : js_online_statement_portlet_title
	});
});
//-->
</script> 