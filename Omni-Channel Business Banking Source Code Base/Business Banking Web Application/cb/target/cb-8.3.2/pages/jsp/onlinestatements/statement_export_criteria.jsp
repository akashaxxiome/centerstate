<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<s:include value="/pages/jsp/onlinestatements/inc/statement_common_criteria.jsp" /> 

<ffi:object id="FFIUpdateReportCriteria" name="com.ffusion.tasks.reporting.UpdateReportCriteria" scope="session"/>
<ffi:setProperty name="FFIUpdateReportCriteria" property="ReportName" value="ReportData"/>