<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%
session.setAttribute("StatementAccountNumber", request.getParameter("accountNumber"));
session.setAttribute("StatementDate", request.getParameter("statementSearchDate"));
session.setAttribute("StatementTransactionType", request.getParameter("statementTransactionType"));
session.setAttribute("StatementAccountCurrency", request.getParameter("StatementAccountCurrency"));
%>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.SEARCH_CRITERIA_ACCOUNT_NUM %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${StatementAccountNumber}"/>

<ffi:setProperty name="Statement" property="StatementDateValue.Format" value="${UserLocale.DateFormat}"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.SEARCH_CRITERIA_STMT_DATE %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${StatementDate}"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterionValue" value="${StatementTransactionType}"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_DATE_FORMAT %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" value="${UserLocale.DateFormat}"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_REPORT_CURRENCY_CODE %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOptionValue" value="${StatementAccountCurrency}"/>

<ffi:object id="FFIGenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase" scope="session"/>