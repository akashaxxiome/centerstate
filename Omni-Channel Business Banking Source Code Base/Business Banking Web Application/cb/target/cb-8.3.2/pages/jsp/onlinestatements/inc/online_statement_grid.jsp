<%@page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
	<script type="text/javascript">
		ns.onlineStatements.typeCheckValue = <%=("" + com.ffusion.beans.banking.TransactionTypes.TYPE_CHECK) %>;
	</script>
	<ffi:setGridURL grid="GRID_onlineStatement" name="ViewURL" url="/cb/pages/jsp/onlinestatement/getOnlineStatementTransactionDetailAction.action?transactionID={0}&statementID=${statementID}&accountNumber=${statementSearchCriteria.accountNumber}&statementSearchDate=${statementSearchDate}" parm0="ID"/>
	<ffi:setGridURL grid="GRID_onlineStatement" name="LinkURL" url="/cb/pages/jsp/account/SearchImageAction.action?module=OnlineStatements&accountNumber=${statementSearchCriteria.accountNumber}&statementId=${statementID}&transID={0}&statementSearchDate=${statementSearchDate}" parm0="ID"/>
	
	<ffi:cinclude value1="${gridType}" value2="Credit" operator="equals">
		<ffi:help id="statement_debit_transactions" />
		<ffi:setProperty name="tempGridURL" value="/pages/jsp/onlinestatement/getOnlineStatementTransactions_getCreditTransactions.action?GridURLs=GRID_onlineStatement&statementID=${statementID}&accountNumber=${statementSearchCriteria.accountNumber}&statementSearchDate=${statementSearchDate}&test=" URLEncrypt="true"/>
		<ffi:setProperty name="gridID" value="creditTransactionsGrid"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${gridType}" value2="Debit" operator="equals">
		<ffi:help id="statement_credit_transactions" />
		<ffi:setProperty name="tempGridURL" value="/pages/jsp/onlinestatement/getOnlineStatementTransactions_getDebitTransactions.action?GridURLs=GRID_onlineStatement&statementID=${statementID}&accountNumber=${statementSearchCriteria.accountNumber}&statementSearchDate=${statementSearchDate}&test=" URLEncrypt="true"/>
		<ffi:setProperty name="gridID" value="debitTransactionsGrid"/>
	</ffi:cinclude>
	
	<s:url id="statementDebitTransactionURL" value="%{#session.tempGridURL}" escapeAmp="false"/>
	<sjg:grid  id="%{#session.gridID}"  
		dataType="json"  
		href="%{statementDebitTransactionURL}"
		caption="%{gridCaptionText}"
		sortable="true"
		pager="false"
		gridModel="gridModel"
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		>
		<sjg:gridColumn name="date" index="date" title="%{getText('jsp.default_137')}" sortable="true" width="60" align="center"/>
		<sjg:gridColumn name="amount" index="amount" title="%{getText('jsp.default_43')}" sortable="true" width="80" align="right"/>
		<ffi:cinclude value1="${imageViewAllowed}" value2="true" operator="equals">
			<sjg:gridColumn name="description" index="description" title="%{getText('jsp.default_170')}" sortable="true" width="180" align="left" formatter="ns.onlineStatements.formatDescription"/>	
		</ffi:cinclude>
       <ffi:cinclude value1="${imageViewAllowed}" value2="true" operator="notEquals">
			<sjg:gridColumn name="description" index="description" title="%{getText('jsp.default_170')}" sortable="true" width="180" align="left"/>
		</ffi:cinclude>
        <sjg:gridColumn name="transactionNum" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.onlineStatements.formatActionLinks" width="40" align="center"  hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
		<sjg:gridColumn name="ID" index="id" title="%{getText('jsp.account_97')}" sortable="false" width="70" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="typeValue" index="typeValue" title="%{getText('jsp.account_97')}" sortable="false" width="70" hidden="true" hidedlg="true"/> 
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.LinkURL" index="LinkURL" title="%{getText('jsp.default_262')}" sortable="true" width="60" hidden="true" hidedlg="true"/> 
	</sjg:grid>	
	
	<ffi:removeProperty name="gridID"/>
	<ffi:removeProperty name="tempGridURL"/>
	<ffi:removeProperty name="gridType"/>	