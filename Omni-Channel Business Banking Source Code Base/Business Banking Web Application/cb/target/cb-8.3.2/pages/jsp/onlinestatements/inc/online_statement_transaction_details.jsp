<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div class="blockWrapper">
    <div class="blockHead"><s:text name="jsp.account_242" /><!-- Transaction Summary --></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width:49%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_213"></s:text>:</span>
				<span class="columndata valueCls">
					<s:property value="%{transaction.Date}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.account.type"/>:</span>
				<span class="columndata valueCls">
					<s:property value="%{transaction.Type}"/>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width:49%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"></s:text>:</span>
				<span class="columndata valueCls">
					<s:property value="%{transaction.AmountValue}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.account_description"/>:</span>
				<span class="columndata valueCls">
					<s:property value="%{transaction.Description}"/>
				</span>
			</div>
		</div>
	</div>
</div>

<div class="marginTop30 ffiVisible">&nbsp;</div>

<div style="width:100%" align="center" class="ui-widget-header customDialogFooter" >
	<sj:a id="exportDialogCancelBtn"
		button="true" onClickTopics="closeDialog" theme="simple"><s:text name="jsp.default_102"/>	 
	</sj:a>
</div>
<ffi:setProperty name="DoneURL" value="${BackURL}"/>
<ffi:removeProperty name="DoneURL"/>
