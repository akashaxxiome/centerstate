<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<!--
	This page is used to tidy up the session before (re-)visting the "Online Statements" section.
-->

<ffi:removeProperty name="AvailableAccounts"/>
<ffi:removeProperty name="AvailableImages"/>
<ffi:removeProperty name="CheckedImages"/>
<ffi:removeProperty name="CheckedImagesId"/>
<ffi:removeProperty name="DisplayBackImage"/>
<ffi:removeProperty name="DisplayBackImagePopup"/>
<ffi:removeProperty name="DisplayFrontImage"/>
<ffi:removeProperty name="DisplayFrontImagePopup"/>
<ffi:removeProperty name="EmailImages"/>
<ffi:removeProperty name="ExportedReport"/>
<ffi:removeProperty name="ExportedReportFooter"/>
<ffi:removeProperty name="ExportedReportHeader"/>
<ffi:removeProperty name="exportFooter"/>
<ffi:removeProperty name="exportForDisplay"/>
<ffi:removeProperty name="exportHeader"/>
<ffi:removeProperty name="FromAddr"/>
<ffi:removeProperty name="get-image-flag"/>
<ffi:removeProperty name="GetCheckedImages"/>
<ffi:removeProperty name="GetDates"/>
<ffi:removeProperty name="GetImageIndex"/>
<ffi:removeProperty name="GetImages"/>
<ffi:removeProperty name="GetStatement"/>
<ffi:removeProperty name="GetStatementAgreement"/>
<ffi:removeProperty name="GetStatementEnabledAccts"/>
<ffi:removeProperty name="GetStatementID"/>
<ffi:removeProperty name="GetStatementList"/>
<ffi:removeProperty name="GetStatementSettings"/>
<ffi:removeProperty name="ImageError"/>
<ffi:removeProperty name="imageHeight"/>
<ffi:removeProperty name="imageLayout"/>
<ffi:removeProperty name="ImageRequest"/>
<ffi:removeProperty name="ImageResults"/>
<ffi:removeProperty name="imageWidth"/>
<ffi:removeProperty name="ModifyStatementSettings"/>
<ffi:removeProperty name="MsgText"/>
<ffi:removeProperty name="ReportData"/>
<ffi:removeProperty name="ReportImageList"/>
<ffi:removeProperty name="rotationBy"/>
<ffi:removeProperty name="SetStatementAgreement"/>
<ffi:removeProperty name="SetTransaction"/>
<ffi:removeProperty name="statement-sub2menu-display-full"/>
<ffi:removeProperty name="statement-sub2menu-display"/>
<ffi:removeProperty name="Statement"/>
<ffi:removeProperty name="StatementAccount"/>
<ffi:removeProperty name="StatementAccounts"/>
<ffi:removeProperty name="StatementDailyBalances"/>
<ffi:removeProperty name="StatementDates"/>
<ffi:removeProperty name="StatementMessages"/>
<ffi:removeProperty name="Statements"/>
<ffi:removeProperty name="StatementTransaction"/>
<ffi:removeProperty name="StatementTransactions"/>
<ffi:removeProperty name="StatementTransactionType"/>
<ffi:removeProperty name="StatementView"/>
<ffi:removeProperty name="StmtAccepted"/>
<ffi:removeProperty name="StocksEdit"/>
<ffi:removeProperty name="SubjText"/>
<ffi:removeProperty name="ToAddr"/>
<ffi:removeProperty name="Transaction"/>
<ffi:removeProperty name="zoom"/>
