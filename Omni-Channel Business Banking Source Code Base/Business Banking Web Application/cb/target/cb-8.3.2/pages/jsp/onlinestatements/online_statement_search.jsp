<%-- Statement Search JSP for Online Statements module for One App. --%>
<%-- author : Simit K. --%>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
    <!--//
    $(function(){
    	$("#accountNumber").selectmenu({width: 300});
    	$("#statementDate").selectmenu({width: 100});
    	$("#transactionType").selectmenu({width: 130});
     });
    //-->
</script>
<%-- This is required for dynamic sizing of the printer ready window. --%>
	<ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase" scope="request"/>
	<ffi:setProperty name="NewReportBase" property="ReportName" value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.RPT_TYPE_ONLINE_STATEMENT %>"/>
	<ffi:process name="NewReportBase"/>
<ffi:help id="acctMgmt_onlineStatements" />	
	
<div class="searchDashboardRenderCls quickSearchAreaCls onlineAccContentDiv" style="display:block" id="stmtSeachBox">   		
<div id="statamentSearchcriteriaDiv" >	
<s:form method="post" name="FormName" id="statementSearchForm" theme="simple">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<input name="GetDates" type="hidden" value="false">
   
   <div class="templateAccPane leftAccountPane leftPaneInnerWrapper" style="width:98% !important;">
		<div class="header"><s:text name="jsp.account.onlineStatement.enable.online.account" /></div>
		
	
	
   <!-- <div class="acntDashboard_masterItemHolder paddingLeft10 marginTop15"> -->
		<div class="acntDashboard_itemHolder marginTop10 paddingLeft10 marginBottom20">
			<span id="accountSelect" class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.account_7"/></span>
			<select name="accountNumber" id="accountNumber" onChange="ns.onlineStatements.accountChanged()" class="form_elements_nowidth" >
				<option value=""><s:text name="jsp.common_Select"/></option>
                     <s:if test="!onlineStatementAccounts.isEmpty()">
						<s:iterator value="%{onlineStatementAccounts}" status="listStatus" var="onlineStatementAccount">
							<s:if test="statementAccount.Number == #onlineStatementAccount.Number">
								<option value='<s:property value="%{#onlineStatementAccount.Number}"/>' selected="selected">
									<s:property value="%{#onlineStatementAccount.ConsumerMenuDisplayText}"/>
								</option>
							</s:if>
							<s:else>
								<option value='<s:property value="%{#onlineStatementAccount.Number}"/>'>
									<s:property value="%{#onlineStatementAccount.ConsumerMenuDisplayText}"/>
								</option>
							</s:else>
						</s:iterator>
                 	</s:if>
			</select>
			<input name="StatementAccountCurrency" type="hidden" value="<s:property value='%{statementAccount.CurrencyCode}'/>" >
		</div>
		<div class="acntDashboard_itemHolder marginTop10">
			<span id="dateSelect" class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.onlineStatements.search.statementDate"/></span>
			<s:set var="DisableStatementDates" value="true" />
				<s:if test="!statementDates.isEmpty()">
					<s:select 
					id="statementDate"
					name="statementSearchDate"
					cssClass="form_elements"
					onchange="ns.onlineStatements.toggleButtons();"
					headerKey="-1"
					headerValue="%{getText('jsp.common_Select')}"
					list="%{#request.statementDates}"
					/>
				</s:if>
				<s:else>
					
					<select name="statementSearchDate" id="statementDate" 
						class="form_elements" disabled="disabled"
						onchange="ns.onlineStatements.toggleButtons();">
                              <option value=""><s:text name="jsp.common_Select"/></option>
					</select>
				</s:else>
		</div>
		<div class="acntDashboard_itemHolder marginTop10">
			<span id="transactionTypeSelect" class="sectionsubhead dashboardLabelMargin"><s:text name="jsp.account_153"/></span>
			<s:set var="DisableOptionsSelect" value="" scope="request" />
			<s:if test="onlineStatementAccounts.isEmpty()">
			    <s:set var="DisableOptionsSelect" value="disabled" scope="request" />
			</s:if>
			<s:if test="statementDates.isEmpty()">
			   <s:set var="DisableOptionsSelect" value="disabled" scope="request" />
			</s:if>
			<select name="statementTransactionType" id="transactionType" class="form_elements" disabled="<s:property value='%{#DisableOptionsSelect}'/>">
				<option value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>"><s:text name="jsp.account.transactionTypeAll"/></option>
				<option value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.SEARCH_CRITERIA_VALUE_DEPOSITS_TRANS_TYPE %>"><s:text name="jsp.account.transactionTypeCredit"/></option>
				<option value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.SEARCH_CRITERIA_VALUE_WITHDRAWALS_TRANS_TYPE %>"><s:text name="jsp.account.transactionTypeDebit"/></option>
			</select>
		</div>
		<div class="acntDashboard_itemHolder marginTop10 ">
            <s:set var="falseFlag" value="false" />
			<s:set var="DisableViewButton" value="false" />
			
			<s:if test="onlineStatementAccounts.isEmpty()">
			    <s:set var="DisableViewButton" value="false" />
			</s:if>
			<s:if test="%{#DisableStatementDates == #falseFlag}">
				 <s:set var="DisableViewButton" value="true" />
			</s:if>
			<s:if test="%{#DisableViewButton== #falseFlag}">
				<script type="text/javascript">
			    	$("#viewOnlineStatement").button({ disabled: true });
			    	$("#viewExportDetails").button({ disabled: true });
			    	$("#viewPrinterReadyVersion").button({ disabled: true });
			    </script>
			</s:if>
			<sj:a
				id="viewOnlineStatement"
				button="true"
				><s:text name="jsp.default_6"/></sj:a>
         </div> 
         
          <div class="acntDashboard_itemHolder marginTop10 ">
          <p>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PAGEWIDTH %>"/>
			<ffi:setProperty name="Math" property="Value1" value="${ReportData.ReportCriteria.CurrentReportOptionValue}"/>
			<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentReportOption" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_PRINTER_READY_PAGEWIDTH %>"/>
			<ffi:cinclude value1="${ReportData.ReportCriteria.CurrentReportOptionValue}" value2="" operator="notEquals">
			    <ffi:setProperty name="Math" property="Value1" value="${ReportData.ReportCriteria.CurrentReportOptionValue}"/>
			</ffi:cinclude>
			<ffi:setProperty name="Math" property="Value2" value="100"/>
			<ffi:setProperty name="printerReadyWindowWidth" value="${Math.Add}"/>
		</p>
		
            <ffi:object id="FFINewReportBase" name="com.ffusion.tasks.reporting.NewReportBase" scope="session"/>
            <ffi:setProperty name="FFINewReportBase" property="ReportName" value="<%= com.ffusion.beans.istatements.IStatementsRptConsts.RPT_TYPE_ONLINE_STATEMENT %>"/>
                                      
             <sj:a id="viewExportDetails" 
					button="true"><s:text name="jsp.default_195"/></sj:a>
			<sj:a id="viewPrinterReadyVersion"  
					button="true"><s:text name="jsp.default_332"/></sj:a>
         </div> 
         
         <s:set var="trueFlag" value="true" />
			 <ffi:cinclude value1="${SecureUser.AppType}" value2="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
		          <ffi:cinclude value1="${SecureUser.PrimaryUserID}" value2="${SecureUser.ProfileID}" operator="notEquals">
		           		<s:set var="DisableSelectAccounts" value="true"/>		           	
		          </ffi:cinclude>					
			 </ffi:cinclude>
         
          <s:if test="%{#DisableSelectAccounts != #trueFlag}">
	         <div id="enableOlAccountsOr" class="acntDashboard_itemHolder spacerDiv marginTop10">
					<s:text name="jsp.account.imagesearch.or" />        
	         </div>
	      </s:if>
         
         <div id="enableOlAccounts"  class="acntDashboard_itemHolder marginTop10 ">
			<!-- Saved searches element -->
			<div class="acntDashboard_itemHolder" align="center">
				<s:set var="DisableStatementAccounts" value="" scope="request" />
				 <s:if test="%{#DisableSelectAccounts != #trueFlag}">
					<s:if test="!onlineStatementAccounts.isEmpty()">
		                  <%-- <s:text name="jsp.onlineStatements.search.info"/>,<br>  --%>
		                  <sj:a href="javascript:void(0)" button="true"  onclick="ns.common.openDialog('accountPrefs');"><s:text name="jsp.account.onlineStatement.select.accounts.btn"/></sj:a>
		                  
		            </s:if>
		            <s:elseif test="onlineStatementAccounts.isEmpty()"> 
		              	<s:set var="DisableStatementAccounts" value="disabled" scope="request" />
		              	<%-- <s:text name="jsp.onlineStatements.noAccountEnabled"/>,<br>  --%>
		                   <sj:a href="javascript:void(0)" button="true"  onclick="ns.common.openDialog('accountPrefs');"><s:text name="jsp.account.onlineStatement.select.accounts.btn"/></sj:a>
		           </s:elseif>
		        </s:if>		          
			</div>
		</div>
	<!-- </div> -->
  </div>		
</s:form>
</div>

<%-- Display Statement Messages Details end --%>
<ffi:removeProperty name="GenerateReportBase"/>
<ffi:removeProperty name="UpdateReportCriteria"/>
<ffi:removeProperty name="ExportReport"/>
<ffi:removeProperty name="ReportData"/>
<ffi:removeProperty name="FilterValue"/>
<ffi:removeProperty name="FilterValue1"/>
<ffi:removeProperty name="FilterValue2"/>
<ffi:removeProperty name="current_account_nick_name"/>
<ffi:removeProperty name="includeCredits"/>
<ffi:removeProperty name="includeDebits"/>
<ffi:removeProperty name="printerReadyWindowWidth"/>
<ffi:removeProperty name="TempProperty"/>
<ffi:removeProperty name="tempURL"/>
<ffi:removeProperty name="imageViewAllowed"/>
</div>