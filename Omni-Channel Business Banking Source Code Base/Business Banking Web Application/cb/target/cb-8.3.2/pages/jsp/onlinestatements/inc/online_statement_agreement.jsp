<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Task to set, the user had aggreed or refused the Statement Aggrement --%>
<s:form action="onlineStatementAcceptanceAction" method="post" name="statementAgreementForm" namespace="/pages/jsp/onlinestatement" id="statementAgreementForm">
<table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
	<tr>
		<td align="left">
<div id="agreementContainer" align="left" class="approvalDialogHt2">
	<table border="0" cellspacing="0" cellpadding="0" id="agreementTable">
		<tbody>
			<tr>
				<td class="txt_normal_bold"><br><s:text name="jsp.onlineStatements.statementAgreement.header"/><br></td>
				<td class="txt_normal" valign="top"><br> <a class="approvalsCls hiddenWhilePrinting" href="javascript:void(0)"
					onclick='$("#agreementContainer").print();'>
						<s:text name="jsp.onlineStatements.statementAgreement.printAgreement"/>
				</a></td>
			</tr>
			<tr>
				<td colspan="2" class="txt_normal"><br> <s:text name="jsp.onlineStatements.statementAgreement.headerInfo1"/>
				<s:text name="jsp.onlineStatements.statementAgreement.headerInfo2"/></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2">
				<table border="1" cellspacing="5" cellpadding="5">
					<tr>
						<td class="txt_normal"><br> <s:text name="jsp.onlineStatements.statementAgreement.electronicDeliveryInfo"/>
							<p>
								<s:text name="jsp.onlineStatements.statementAgreement.internetStatementHeader"/>
								<br>
								<s:text name="jsp.onlineStatements.statementAgreement.internetStatementInfo"/>
							<p>
								<s:text name="jsp.onlineStatements.statementAgreement.termsConditionHeader"/>
								<br>
								<s:text name="jsp.onlineStatements.statementAgreement.termsConditionInfo"/>
							<p>
								<s:text name="jsp.onlineStatements.statementAgreement.termsChangeNoticeHeader"/>
								<br>
								<s:text name="jsp.onlineStatements.statementAgreement.termsChangeNoticeInfo"/>
							<p>
								<s:text name="jsp.onlineStatements.statementAgreement.internetStatRequirementHeader"/>
								<br>
								<s:text name="jsp.onlineStatements.statementAgreement.internetStatRequirementInfo"/>
							<p>
								<s:text name="jsp.onlineStatements.statementAgreement.emailChangeHeader"/>
								<br>
								<s:text name="jsp.onlineStatements.statementAgreement.emailChangeInfo"/>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="txt_normal" valign="top"><br> <s:text name="jsp.onlineStatements.statementAgreement.acceptanceDeclaration"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" height="50">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<input type="hidden" name="CSRF_TOKEN"	value="<ffi:getProperty name='CSRF_TOKEN'/>" />
</div>

<div align="center" class="marginTop20 marginBottom10 ui-widget-header customDialogFooter">
	<sj:a id="declineAgreementBtn" onclick="ns.onlineStatements.closeAgreementDialog()" 
		button="true" ><s:text name="jsp.common_Decline"/></sj:a>
	<sj:a id="acceptAgreementBtn" button="true" 
		formIds="statementAgreementForm" targets="notes" onCompleteTopics="functionalityComplete"><s:text name="jsp.common_Accept"/></sj:a>		
</div>	
</td>
</tr>
</table>
</s:form>