<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

	<ffi:setGridURL grid="GRID_favoriteRates" name="DeleteURL" url="/cb/pages/jsp/transfers/favoriteRates_deleteFavoriteRateFromFavoriteList.action?fromCurrencyCode={0}&toCurrencyCode={1}" parm0="fromCurrencyCode" parm1="toCurrencyCode"/>
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/transfers/favoriteRates_getExistingFavoriteRatesConverted.action?GridURLs=GRID_favoriteRates" URLEncrypt="true"/>
    <s:url id="remoteurl" value="%{#session.tempURL}" escapeAmp="false"/>
     <sjg:grid
       id="StoredGrid"
       caption=""
	   sortable="true"  
       dataType="json"
       href="%{remoteurl}"
       pager="true"
       viewrecords="true"
       gridModel="gridModel"
	   rowList="%{#session.StdGridRowList}" 
	   rowNum="%{#session.StdGridRowNum}" 
       rownumbers="false"
       scroll="false"
       altRows="true"
       shrinkToFit="true"
       navigator="true"
	   navigatorSearch="false"
       navigatorAdd="false"
       navigatorDelete="false"
       navigatorEdit="false"
       navigatorRefresh="false"
	   onGridCompleteTopics="tabifyDialog"
     >
     <sjg:gridColumn name="map.fromCurrencyCode" width="180" index="fromCurrency" title="%{getText('jsp.fx_11')}" sortable="false" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.toCurrencyCode" width="165" index="toCurrency" title="%{getText('jsp.fx_19')}" sortable="false" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.buyPrice" width="100" index="buyPrice" title="%{getText('jsp.fx_3')}" sortable="false" cssClass="datagrid_amountColumn"/>
	 <sjg:gridColumn name="map.sellPrice" width="100" index="sellPrice" title="%{getText('jsp.fx_16')}" sortable="false" cssClass="datagrid_amountColumn"/>
	 <sjg:gridColumn name="map.asOfDate" width="100" index="asOfDate" title="%{getText('jsp.fx_2')}" sortable="false" cssClass="datagrid_dateColumn"/>
     <sjg:gridColumn name="actionID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="formatFavoriteRateActionLinks" hidedlg="true" search="false" width="50" hidden="true" cssClass="__gridActionColumn"/>
	 <sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" sortable="true" width="60" hidden="true" hidedlg="true"/>

	</sjg:grid>