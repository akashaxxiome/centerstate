<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>
<script type="text/javascript">

$(document).ready(function(){

	$('#fromAmount').val(1);	
	setTimeout( function(){
		$('#CalculateRatesButton').click();
	},10);
	
});	

function chkVal (param) {
	
}

function getCurrency(){
	
	
	$("#CalculateRatesButton").click();
}

$("#fromCurrencyCode").combobox({'size':'25'});
$("#toCurrencyCode").combobox({'size':'25'});
	
swapCurrencies = function(){
	$.ajax({   
		type: "POST",   
		url: "/cb/pages/jsp/fx/fx.jsp?CSRF_TOKEN=" + '<ffi:getProperty name='CSRF_TOKEN'/>' +"&fromCurrencyCode=" +  $('#toCurrencyCode').val() + 
				"&toCurrencyCode="+$('#fromCurrencyCode').val(),
		success: function(data) {
			// flushing old data
			$('#fxCalculatorDiv').html('');
			$('#fxTabs').html('');
			$('#fxCalculatorDiv').html(data);
			$.publish("common.topics.tabifyDialog");
			if(accessibility){
				$("#fxCalculatorDiv").setFocus();
			}
			
		}   
	});
}
</script>


	<s:form id="FXCalculator" name="FXCalculator" action='FxRateAction_calculate' namespace="/pages/jsp/transfers" method="post" theme="simple">
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" id="fxcalculate_1" name="fxcalculate" value="true"/>
			<input type="hidden" id="UtilityFlagId" name="UtilityFlag" value="CALCULATE"/>
			
			<div class="paneWrapper">
			<div class="paneInnerWrapper">
			
				<div id="calcHolderDiv" class="calcHolderDivCls header">
				
					<div class="fxConvertFromArea floatleft">
						<span class="fxCalcLable"><s:text name="jsp.fx_7"/></span>
					</div>
					<div class="fxCalcIconHolder floatleft">
						<span>&nbsp;</span>
					</div>
					
					<ffi:cinclude value1="0" value2="${CalculatedRates.Size}" operator="notEquals">
					
						<div class="fxConvertToArea floatleft">
							<span class="fxCalcLable"><s:text name="jsp.fx_8"/></span>
						</div>
				</div>
					
			</div>
			</div>
			
			
			<div class="marginleft10">
			
			<div id="calcHolderDiv" class="calcHolderDivCls marginTop10">
				<div class="fxConvertFromArea floatleft">
				
					
					<select id="fromCurrencyCode" name="fromCurrencyCode" class="txtbox"  onchange="selectFromCurrency();">
						<ffi:list collection="FOREIGN_EXCHANGE_BASE_CURRENCIES" items="currency">
							<option value='<ffi:getProperty name="currency" property="CurrencyCode"/>' 
							<ffi:cinclude value1="${currency.CurrencyCode}" value2="${fromCurrencyCode}" operator="equals">selected</ffi:cinclude>>
							<ffi:getProperty name="currency" property="CurrencyCode"/>-<ffi:getProperty name="currency" property="Description"/>
							</option>
						</ffi:list>
					</select>
				</div>
				<div class="fxCalcIconHolder floatleft">
					<span><img alt="<s:text name="jsp.home_90"/>" src="<s:url value="/web/grafx/icons/doubleSideArrow.png"/>" 
					onclick="swapCurrencies();"/></span>
				</div>
				  
				<ffi:cinclude value1="0" value2="${CalculatedRates.Size}" operator="notEquals">
				</ffi:cinclude>
					<div class="fxConvertToArea floatleft">
						
						<select id="toCurrencyCode" name="toCurrencyCode" class="txtbox" onchange="getCurrency()">
							<ffi:list collection="CalculatedRates" items="rate">
								<ffi:cinclude value1="${fromCurrencyCode}" value2="${rate.TargetCurrencyCode}" operator="notEquals">
									<ffi:cinclude value1="" value2="${rate.BuyPrice}" operator="notEquals">
										<option value='<ffi:getProperty name="rate" property="TargetCurrencyCode"/>' 
										<ffi:cinclude value1="${rate.TargetCurrencyCode}" value2="${toCurrencyCode}" operator="equals">selected
										</ffi:cinclude>>
										<ffi:getProperty name="rate" property="TargetCurrencyCode"/>-<ffi:getProperty name="rate" property="TargetCurrencyDescription"/>
										</option>
									</ffi:cinclude>
								</ffi:cinclude>
							</ffi:list>
						</select>
					</div>
			</div>		
					
					
					
					
					
					
			
			
					
					<div class="fxFromAmountCls marginTop10 floatleft" style="clear: left;">
						<s:textfield id="fromAmount" name="fromAmount" value="%{fromAmount}" cssClass="ui-widget-content ui-corner-all txtbox" size="27px" maxlength="20" onkeyup="getCurrency()"/>
						<br>&nbsp;<span id="fromAmountError"></span>
					</div>

					<div class="fxCalcIconHolder fxSingleSideArrow floatleft">
						<span><img alt="<s:text name="jsp.home_90"/>" src="<s:url value="/web/grafx/icons/singleSideArrow.png"/>"/></span>
					</div>
					
					<div id="calResultId" class="fxToAmountCls marginTop10 floatleft">
						<%-- <s:textfield type="text" cssClass="ui-widget-content ui-corner-all txtbox" name="convertedAmtInput" size="27" maxlength="20" 
						id="convertedAmtInput" disabled="true"/> --%>
						<span id="toAmountError"></span>
						<span id="convertedAmountHolder" class="convertedAmountHolderCls">
						</span>
						<!-- <ffi:getProperty name="fromAmount"/>
						<ffi:getProperty name="fromCurrencyCode"/>
						<ffi:getProperty name="FOREIGN_EXCHANGE_BASE_CURRENCY_AMOUNT" property="CurrencyStringNoSymbolNoComma"/>
						<ffi:getProperty name="toCurrencyCode"/> -->
					</div>
					
					<div class="fxActionBtnCls floatleft marginTop10 marginBottom10">
						<sj:a href="#" button="true" validate="true" onclick="ns.fx.addFavoriteRate();" onCompleteTopics=""
		                        onSuccessTopics=""><s:text name="jsp.fx_1"/></sj:a>
		                        
		                        
		                    <span style="display:none">
		                    	<sj:a 
								href="#"
								id="CalculateRatesButton"
								formIds="FXCalculator"
		                        targets="convertedAmountHolder" 
		                        button="true" 
		                        buttonIcon="ui-icon-refresh"
					            validate="true" 
		                        validateFunction="customValidation"
		                        onBeforeTopics="beforeCalculateFxRates"
		                        onCompleteTopics="calculateFxRatesComplete"
								onErrorTopics="errorCalculateFxRates"
		                        onSuccessTopics="successCalculateFxRates"
			                ><s:text name="jsp.fx_4"/>
			                </sj:a>
		                    </span>		                        
			                
					</div>
				</ffi:cinclude>
				<ffi:cinclude value1="0" value2="${CalculatedRates.Size}" operator="equals">
					<div style="float: left;padding-bottom: 3px;padding-top: 3px; text-align: center; width: 100%;" class="marginTop10">
						<span style="float: left; text-align: center; width: 100%;" class="sectionsubhead">&nbsp;There are no conversion rates for the selected currencies.&nbsp;</span>
					</div>
				</ffi:cinclude>
				
	</div>
	
	
			
			
				<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="35%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="1" height="1" border="0"></td>
						<td width="60%"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="1" height="1" border="0"></td>
					</tr>
					<tr>
					<td class="sectionhead ltrow2_color" align="right" nowrap><s:text name="jsp.fx_7"/>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td class="sectionhead ltrow2_color" align="left">
						<div class="selectBoxHolder">
							<select id="fromCurrencyCode" name="fromCurrencyCode" class="txtbox"  onchange="selectFromCurrency();">
							<ffi:list collection="FOREIGN_EXCHANGE_BASE_CURRENCIES" items="currency">
								<option value='<ffi:getProperty name="currency" property="CurrencyCode"/>' <ffi:cinclude value1="${currency.CurrencyCode}" value2="${fromCurrencyCode}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="currency" property="Description"/></option>
							</ffi:list>
							</select>
						</div>
					</td>
					</tr>	
			<ffi:cinclude value1="0" value2="${CalculatedRates.Size}" operator="notEquals">
					<tr>
					<td class="sectionhead ltrow2_color" align="right" nowrap><s:text name="jsp.fx_8"/>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td class="sectionhead ltrow2_color" align="left">
					<div class="selectBoxHolder">
						<select id="toCurrencyCode" name="toCurrencyCode" class="txtbox" >
						<ffi:list collection="CalculatedRates" items="rate">
							<ffi:cinclude value1="${fromCurrencyCode}" value2="${rate.TargetCurrencyCode}" operator="notEquals">
									<ffi:cinclude value1="" value2="${rate.BuyPrice}" operator="notEquals">
							<option value='<ffi:getProperty name="rate" property="TargetCurrencyCode"/>' <ffi:cinclude value1="${rate.TargetCurrencyCode}" value2="${toCurrencyCode}" operator="equals">selected</ffi:cinclude>><ffi:getProperty name="rate" property="TargetCurrencyDescription"/>
							</option>
							</ffi:cinclude>
							</ffi:cinclude>
						</ffi:list>
						</select></td>
					</div>
				    </tr>	
					<tr>
					<td class="sectionhead ltrow2_color" align="right"><s:text name="jsp.default_43"/>&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td class="sectionhead ltrow2_color" align="left" style="padding-top:6px;">
						<s:textfield id="fromAmount" name="fromAmount" value="%{fromAmount}" cssClass="ui-widget-content ui-corner-all txtbox" size="20" maxlength="20"/><span id="fromAmountError"></span>
					</td>
				    </tr>	
					<tr height="30">
					<td id="calResultId" colspan="4" class="sectionhead ltrow2_color">
						<s:if test="%{fromAmount > 0}">
							&nbsp;<s:text name="jsp.fx_9"/>&nbsp;
							<ffi:getProperty name="fromAmount"/>&nbsp;<ffi:getProperty name="fromCurrencyCode"/>&nbsp;=&nbsp;
							<ffi:getProperty name="FOREIGN_EXCHANGE_BASE_CURRENCY_AMOUNT" property="CurrencyStringNoSymbolNoComma"/>&nbsp;
							<ffi:getProperty name="toCurrencyCode"/>
						</s:if>&nbsp;
					</td>
				    </tr>	
				<tr>
					<td  colspan="2" class="sectionhead ltrow2_color" align="center">
							<sj:a
								href="#"
					            button="true" 
					            buttonIcon="ui-icon-refresh"
					            onclick="ns.fx.calculateReset();"
					        ><s:text name="jsp.default_358"/></sj:a>
							
					       <sj:a 
						   href="#"
		                        button="true" 
		                        buttonIcon="ui-icon-refresh"
		                        onclick="ns.fx.calculateRate();"
			                ><s:text name="jsp.fx_4"/></sj:a>
							
					       <sj:a 
								href="#"
								id="CalculateRatesButton"
								formIds="FXCalculator"
		                        targets="calResultId" 
		                        button="true" 
		                        buttonIcon="ui-icon-refresh"
					            validate="true" 
		                        validateFunction="customValidation"
		                        onBeforeTopics="beforeCalculateFxRates"
		                        onCompleteTopics="calculateFxRatesComplete"
								onErrorTopics="errorCalculateFxRates"
		                        onSuccessTopics="successCalculateFxRates"
			                ><s:text name="jsp.fx_4"/>
			                </sj:a>


							<sj:a 
								href="#"
		                        button="true" 
		                        buttonIcon="ui-icon-add"
					            validate="true" 
								onclick="ns.fx.addFavoriteRate();"
		                        onCompleteTopics=""
		                        onSuccessTopics=""
			                ><s:text name="jsp.fx_1"/></sj:a>

					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude value1="0" value2="${CalculatedRates.Size}" operator="equals">
					<tr>
							<td width="35%"><img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/spacer.gif" alt="" width="1" height="10" border="0"></td>
							<td width="60%"><img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/spacer.gif" alt="" width="1" height="10" border="0"></td>
				</tr>
					<tr>
							<td width="35%"><img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/spacer.gif" alt="" width="1" height="10" border="0"></td>
							<td width="60%"><img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/spacer.gif" alt="" width="1" height="10" border="0"></td>
				</tr>
				 <tr>
			
					<td colspan="2">
					<div style="padding-top: 3px; padding-bottom: 3px; text-align: center;">
					<span class="sectionsubhead">
					&nbsp;There are no conversion rates for the selected currencies.&nbsp;
					</span></div>
					<td>
				    </tr>	
					<tr>
							<td width="35%"><img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/spacer.gif" alt="" width="1" height="10" border="0"></td>
							<td width="60%"><img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/spacer.gif" alt="" width="1" height="10" border="0"></td>
					</tr>
			</ffi:cinclude>
		</table> --%>
	</s:form>





