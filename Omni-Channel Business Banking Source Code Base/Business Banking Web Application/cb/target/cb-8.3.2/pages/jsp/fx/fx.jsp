<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList" %>

<ffi:help id="fx_fx" />

<script type="text/javascript" src="<s:url value='/web/js/fx/fx%{#session.minVersion}.js'/>"></script>

<s:include value="fx_selected.jsp"/>

<div id="fxCalculatorDiv" align="left">
	<div class="portlet-header">
		<%-- <s:text name="Calculator"/> --%>
		<span class="portlet-title" title="Calculator"><s:text name="Calculator"/></span>
	</div>
	
	<div class="portlet-content">
	   <s:include value="fx_calculator.jsp"/>
	</div>
	   
	<div class="searchHeaderCls marginBottom10" style="margin-top:10px; position:static;" >
		<div class="summaryGridTitleHolder">
			<span id="selectedGridTab" class="selectedTabLbl">
				<s:text name="jsp.fx_10"/>
			</span>
			<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight marginRight10"></span>
			
			<div class="gridTabDropdownHolder">
				<span class="gridTabDropdownItem" id='favExchangeRate' onclick="ns.common.showGridSummary('favExchangeRate')" >
					<s:text name="jsp.fx_10"/>
				</span>
				<span class="gridTabDropdownItem" id='foreignExchangeRate' onclick="ns.common.showGridSummary('foreignExchangeRate')">
					<s:text name="jsp.default_214"/>
				</span>
			</div>
		</div>
	</div>
	<div id="fxTabsgridContainer" class="summaryGridHolderDivCls">
		<div id="favExchangeRateSummaryGrid" class="gridSummaryContainerCls" >
			<div id="favExchangeRateSummary">
				<s:include value="/pages/jsp/fx/fx_favoriterate_grid.jsp" />
			</div>
		</div>
		<div id="foreignExchangeRateSummaryGrid" class="gridSummaryContainerCls hidden" >
			<div id="foreignExchangeRateSummary"  class="ui-corner-all ui-widget-content">
				<s:include value="/pages/jsp/fx/fx_rate.jsp" />
			</div>
		</div>
	</div>	
	
	
</div>
<br/>

<script type="text/javascript"><!--


/* $(function(){

	$('#fxCalculatorDiv').pane({
		title: js_calculator,
		minmax: true,
		helpCallback: function(){
			var helpFile = $('#remotefxdialog').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
	});


	
});
 */
 
 ns.common.initializePortlet("fxCalculatorDiv");
	function selectFromCurrency()
	{
		$('#fxcalculate_1').attr('value','false');	
 		$.ajax({  
 			url: '<ffi:urlEncrypt url="/cb/pages/jsp/fx/fx.jsp?fxcalculate=false&fromAmount=0&UtilityFlag="/>',
 			type:"POST",
			data: $("#FXCalculator").serialize(),
 			success: function(data) {
				$('#remotefxdialog').html(data);
 			}   
 		});
	}

//-->
</script>
	


<script type="text/javascript">
/* ns.common.initializePortlet("fxCalculatorDiv");
	$(function(){
		$("#fxTabs").tabs({
			load:function(){
				$.publish("common.topics.tabifyDialog");
				if(accessibility){
					$("#fxTabs").setFocusOnTab();
				}	
			},
			show: function(event,ui){
				$.publish("common.topics.tabifyNotes");
				if(accessibility){
					$("#fxTabs").setFocusOnTab();
				}				
			}		
		});
		$("#fxTabs").sortable({ items: 'li'});
		$("#fxTabs").sortable({ update: function(){
			ns.common.tabs.tabUpdate("#fxTabs");
		} });   
		ns.common.tabs.tabInit("#fxTabs");   	
	}); */
</script>

	<ffi:object id="fxtab" name="com.ffusion.tasks.util.TabConfig"/>
	<ffi:setProperty name="fxtab" property="TransactionID" value="FX"/>
	
	<ffi:setProperty name="fxtab" property="Tab" value="favorite"/>
	<ffi:setProperty name="fxtab" property="Href" value="${FullPagesPath}fx/fx_favoriterate_grid.jsp"/>
	<ffi:setProperty name="fxtab" property="LinkId" value="favorite"/>
	<ffi:setProperty name="fxtab" property="Title" value="jsp.fx_10"/>
	
	<ffi:setProperty name="fxtab" property="Tab" value="foreign"/>
	<ffi:setProperty name="fxtab" property="Href" value="${FullPagesPath}fx/fx_rate.jsp"/>
	<ffi:setProperty name="fxtab" property="LinkId" value="foreign"/>
	<ffi:setProperty name="fxtab" property="Title" value="jsp.default_214"/>
		
	<ffi:process name="fxtab"/>
	
<%-- <div id="fxTabs">
	<ul>
		<ffi:list collection="fxtab.Tabs" items="tab">
	        <li>
	            <a href='<ffi:getProperty name="tab" property="Href"/>' id='<ffi:getProperty name="tab" property="LinkId"/>'>
	            	<ffi:setProperty name="TabTitle" value="${tab.Title}"/><s:text name="%{#session.TabTitle}"/>
	            </a>
	        </li>
		</ffi:list>
			<div id="tabChangeNote" style="height:0px;  position:relative; top:3px; left:10px; display:inline-block;"></div>
			<a id="tabRevertNote" style="height:0px;  position:relative; top:3px; left:20px; display:inline-block;"></a>
	</ul>
	<input type="hidden" id="getTransID" value="<ffi:getProperty name='fxtab' property='TransactionID'/>" />
	
</div> --%>
