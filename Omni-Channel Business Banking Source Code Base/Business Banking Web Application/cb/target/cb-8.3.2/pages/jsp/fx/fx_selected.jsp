<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<%@ page contentType="text/html; charset=UTF-8" %>
<%

request.setAttribute("UtilityFlag", request.getParameter("UtilityFlag"));

request.setAttribute("Init", request.getParameter("Init"));
request.setAttribute("objectID", request.getParameter("objectID"));
request.setAttribute("objectType", request.getParameter("objectType"));

request.setAttribute("fromAmount", request.getParameter("fromAmount"));
request.setAttribute("fromCurrencyCode", request.getParameter("fromCurrencyCode"));
request.setAttribute("fxcalculate", request.getParameter("fxcalculate"));
request.setAttribute("toCurrencyCode", request.getParameter("toCurrencyCode"));
%>

<%-- ###################################################################### 
			 Miscellaneous variables required for this page.
	objectType, objectID required			    
	 ###################################################################### --%>
<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="session"/>
<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.dateFormat}" />
<ffi:process name="GetCurrentDate"/>

<ffi:cinclude value1="" value2="${OrigBackURL}" operator="equals">
<ffi:setProperty name="OrigBackURL" value="${BackURL}" />
</ffi:cinclude>
<ffi:cinclude value1="" value2="${OrigBackURL}" operator="notEquals">
	<ffi:cinclude value1="${BackURL}" value2="${FXBackURL}" operator="equals">
	<ffi:setProperty name="init" value="true" />
	<ffi:setProperty name="calInit" value="true" />
	</ffi:cinclude>
<ffi:setProperty name="BackURL" value="${OrigBackURL}" />
</ffi:cinclude>
<% String useLastRequest=""; %>
<ffi:getProperty name="LastRequest" assignTo="useLastRequest"/>
<ffi:cinclude value1="${Init}" value2="true" operator="equals">
	<ffi:setProperty name="Init" value=""/>
	<ffi:setProperty name="calInit" value="true"/>
	<ffi:object name="com.ffusion.tasks.fx.GetFXRateSheet" id="GetFXRateSheetCalculation" scope="session"/>
	<ffi:object name="com.ffusion.tasks.fx.GetFXRateSheet" id="GetFXRateSheet" scope="session"/>
	<ffi:object name="com.ffusion.tasks.fx.ConvertToBaseCurrency" id="ConvertToBaseCurrency" scope="session"/>
	<ffi:setProperty name="GetFXRateSheet" property="ObjectType" value="${objectType}"/>  
	<ffi:setProperty name="GetFXRateSheet" property="ObjectID" value="${objectID}"/>
	<ffi:setProperty name="GetFXRateSheetCalculation" property="ObjectType" value="${objectType}"/>  
	<ffi:setProperty name="GetFXRateSheetCalculation" property="ObjectID" value="${objectID}"/> 

	<% session.setAttribute("FFIGetFXRateSheet", session.getAttribute("GetFXRateSheet")); %>

	<ffi:setProperty name="fxcalculate" value=""/>
	<ffi:setProperty name="fromCurrencyCode" value=""/>
	<ffi:setProperty name="relativeCurrencyCode" value=""/>
	<ffi:setProperty name="toCurrencyCode" value=""/>
	<ffi:setProperty name="fromAmount" value=""/>

	<ffi:object name="com.ffusion.tasks.fx.GetBaseCurrencies" id="GetBaseCurrencies" scope="session"/>
	<ffi:process name="GetBaseCurrencies"/>
	<ffi:list collection="FOREIGN_EXCHANGE_BASE_CURRENCIES" items="currency1" startIndex="1" endIndex="1">
		<ffi:setProperty name="firstCurrencyCode" value="${currency1.CurrencyCode}"/> 
	</ffi:list>
	<ffi:setProperty name="relativeCurrencyCode" value="${firstCurrencyCode}"/> 
	<ffi:setProperty name="fromCurrencyCode" value="${firstCurrencyCode}"/> 
</ffi:cinclude>

<ffi:cinclude value1="${calInit}" value2="true" operator="equals">
<ffi:setProperty name="calInit" value=""/>
<ffi:object name="com.ffusion.tasks.fx.ConvertToBaseCurrency" id="ConvertToBaseCurrency" scope="session"/>
<ffi:setProperty name="fxcalculate" value=""/>

<ffi:list collection="FOREIGN_EXCHANGE_BASE_CURRENCIES" items="currency1" startIndex="1" endIndex="1">
	<ffi:setProperty name="firstCurrencyCode" value="${currency1.CurrencyCode}"/> 
</ffi:list>
	<ffi:setProperty name="fromCurrencyCode" value="${firstCurrencyCode}"/> 
	<ffi:setProperty name="fromAmount" value=""/> 
<ffi:setProperty name="GetFXRateSheetCalculation" property="FXSessionName" value="CalculatedRateSheet"/>  
<ffi:setProperty name="GetFXRateSheetCalculation" property="RatesSessionName" value="CalculatedRates"/>  

</ffi:cinclude>

<ffi:setProperty name="GetFXRateSheet" property="BaseCurrencyCode" value="${relativeCurrencyCode}"/>  

 
<ffi:setProperty name="GetFXRateSheetCalculation" property="BaseCurrencyCode" value="${fromCurrencyCode}"/>  
<ffi:setProperty name="GetFXRateSheetCalculation" property="FXSessionName" value="CalculatedRateSheet"/>  
<ffi:setProperty name="GetFXRateSheetCalculation" property="RatesSessionName" value="CalculatedRates"/>  


<ffi:cinclude value1="${fxcalculate}" value2="true" operator="equals">
<ffi:object name="com.ffusion.beans.common.Currency" id="FOREIGN_EXCHANGE_CURRENCY_AMOUNT" scope="session"/> 
	<ffi:setProperty name="FOREIGN_EXCHANGE_CURRENCY_AMOUNT" property="Amount" value="${fromAmount}"/>  
	<ffi:setProperty name="FOREIGN_EXCHANGE_CURRENCY_AMOUNT" property="Locale" value="${UserLocale.Locale}"/>  
	<ffi:setProperty name="FOREIGN_EXCHANGE_CURRENCY_AMOUNT" property="CurrencyCode" value="${fromCurrencyCode}"/>  
	<ffi:setProperty name="ConvertToBaseCurrency" property="TargetCurrencyCode" value="${toCurrencyCode}"/>  
	<ffi:setProperty name="ConvertToBaseCurrency" property="FromAmount" value="${fromAmount}"/>  

<ffi:setProperty name="ConvertToBaseCurrency" property="ObjectType" value="${objectType}"/>
<ffi:setProperty name="ConvertToBaseCurrency" property="ObjectID" value="${objectID}"/>
<ffi:process name="ConvertToBaseCurrency"/>
</ffi:cinclude>	
<ffi:setProperty name="GetFXRateSheetCalculation" property="RemoveEmptyBuyPriceRate" value="true"/>
<ffi:process name="GetFXRateSheetCalculation"/>

<ffi:cinclude value1="${GetFXRateSheet.AsOfDate}" value2="" operator="equals">
	<ffi:setProperty name="GetFXRateSheet" property="AsOfDate" value="${GetCurrentDate.Date}" />
</ffi:cinclude>

<ffi:cinclude value1="${fxcalculate}" value2="true" operator="notEquals">
<ffi:process name="GetFXRateSheet"/>
</ffi:cinclude>

<ffi:cinclude value1="${fxcalculate}" value2="true" operator="equals">
	<ffi:cinclude value1="${UtilityFlag}" value2="CALCULATE" operator="equals">
		<s:if test="%{fromAmount > 0}">
			<%-- &nbsp;<s:text name="jsp.fx_9"/>&nbsp;
			<ffi:getProperty name="fromAmount"/>&nbsp;<ffi:getProperty name="fromCurrencyCode"/>&nbsp;=&nbsp; --%>
			<span class="ui-widget-content ui-corner-all txtbox">
							<ffi:getProperty name="FOREIGN_EXCHANGE_BASE_CURRENCY_AMOUNT" property="CurrencyStringNoSymbolNoComma"/>
			</span>
			<!-- &nbsp; -->
			<%-- <ffi:getProperty name="toCurrencyCode"/> --%>
		</s:if>&nbsp;
	</ffi:cinclude>
</ffi:cinclude>
<%-- <input id="convertedAmtSpan" class="someNewCls" value=" --%>
<%-- <ffi:getProperty name="FOREIGN_EXCHANGE_BASE_CURRENCY_AMOUNT" property="CurrencyStringNoSymbolNoComma"/> --%>


