<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
session.setAttribute("Init", request.getParameter("Init"));
session.setAttribute("objectID", request.getParameter("objectID"));
session.setAttribute("objectType", request.getParameter("objectType"));
%>

<%-- ######################################################################
			 Miscellaneous variables required for this page.
	objectType, objectID required
	 ###################################################################### --%>
<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="session"/>
<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.dateFormat}" />
<ffi:process name="GetCurrentDate"/>

<ffi:cinclude value1="" value2="${OrigBackURL}" operator="equals">
<ffi:setProperty name="OrigBackURL" value="${BackURL}" />
</ffi:cinclude>
<ffi:cinclude value1="" value2="${OrigBackURL}" operator="notEquals">
	<ffi:cinclude value1="${BackURL" value2="${FXBackURL}" operator="equals">
	<ffi:setProperty name="init" value="true" />
	<ffi:setProperty name="calInit" value="true" />
	</ffi:cinclude>
<ffi:setProperty name="BackURL" value="${OrigBackURL}" />
</ffi:cinclude>
<% String useLastRequest=""; %>
<ffi:getProperty name="LastRequest" assignTo="useLastRequest"/>
<ffi:cinclude value1="${Init}" value2="true" operator="equals">
	<ffi:setProperty name="Init" value=""/>
	<ffi:setProperty name="calInit" value="true"/>
	<ffi:object name="com.ffusion.tasks.fx.GetFXRateSheet" id="GetFXRateSheetCalculation" scope="session"/>
	<ffi:object name="com.ffusion.tasks.fx.GetFXRateSheet" id="GetFXRateSheet" scope="session"/>
	<ffi:object name="com.ffusion.tasks.fx.ConvertToBaseCurrency" id="ConvertToBaseCurrency" scope="session"/>
	<ffi:setProperty name="GetFXRateSheet" property="ObjectType" value="${objectType}"/>
	<ffi:setProperty name="GetFXRateSheet" property="ObjectID" value="${objectID}"/>
	<ffi:setProperty name="GetFXRateSheetCalculation" property="ObjectType" value="${objectType}"/>
	<ffi:setProperty name="GetFXRateSheetCalculation" property="ObjectID" value="${objectID}"/>

	<% session.setAttribute("FFIGetFXRateSheet", session.getAttribute("GetFXRateSheet")); %>

	<ffi:setProperty name="fxcalculate" value=""/>
	<ffi:setProperty name="fromCurrencyCode" value=""/>
	<ffi:setProperty name="relativeCurrencyCode" value=""/>
	<ffi:setProperty name="toCurrencyCode" value=""/>
	<ffi:setProperty name="fromAmount" value=""/>

	<ffi:object name="com.ffusion.tasks.fx.GetBaseCurrencies" id="GetBaseCurrencies" scope="session"/>
	<ffi:process name="GetBaseCurrencies"/>
	<ffi:list collection="FOREIGN_EXCHANGE_BASE_CURRENCIES" items="currency1" startIndex="1" endIndex="1">
		<ffi:setProperty name="firstCurrencyCode" value="${currency1.CurrencyCode}"/>
	</ffi:list>
	<ffi:setProperty name="relativeCurrencyCode" value="${firstCurrencyCode}"/>
	<ffi:setProperty name="fromCurrencyCode" value="${firstCurrencyCode}"/>
</ffi:cinclude>

<ffi:cinclude value1="${calInit}" value2="true" operator="equals">
<ffi:setProperty name="calInit" value=""/>
<ffi:object name="com.ffusion.tasks.fx.ConvertToBaseCurrency" id="ConvertToBaseCurrency" scope="session"/>
<ffi:setProperty name="fxcalculate" value=""/>

<ffi:list collection="FOREIGN_EXCHANGE_BASE_CURRENCIES" items="currency1" startIndex="1" endIndex="1">
	<ffi:setProperty name="firstCurrencyCode" value="${currency1.CurrencyCode}"/>
</ffi:list>
	<ffi:setProperty name="fromCurrencyCode" value="${firstCurrencyCode}"/>
	<ffi:setProperty name="fromAmount" value=""/>
<ffi:setProperty name="GetFXRateSheetCalculation" property="FXSessionName" value="CalculatedRateSheet"/>
<ffi:setProperty name="GetFXRateSheetCalculation" property="RatesSessionName" value="CalculatedRates"/>

</ffi:cinclude>

<ffi:setProperty name="GetFXRateSheet" property="BaseCurrencyCode" value="${relativeCurrencyCode}"/>


<ffi:setProperty name="GetFXRateSheetCalculation" property="BaseCurrencyCode" value="${fromCurrencyCode}"/>
<ffi:setProperty name="GetFXRateSheetCalculation" property="FXSessionName" value="CalculatedRateSheet"/>
<ffi:setProperty name="GetFXRateSheetCalculation" property="RatesSessionName" value="CalculatedRates"/>


<ffi:cinclude value1="${fxcalculate}" value2="true" operator="equals">
<ffi:object name="com.ffusion.beans.common.Currency" id="FOREIGN_EXCHANGE_CURRENCY_AMOUNT" scope="session"/>
	<ffi:setProperty name="FOREIGN_EXCHANGE_CURRENCY_AMOUNT" property="Amount" value="${fromAmount}"/>
	<ffi:setProperty name="FOREIGN_EXCHANGE_CURRENCY_AMOUNT" property="Locale" value="${UserLocale.Locale}"/>
	<ffi:setProperty name="FOREIGN_EXCHANGE_CURRENCY_AMOUNT" property="CurrencyCode" value="${fromCurrencyCode}"/>
	<ffi:setProperty name="ConvertToBaseCurrency" property="TargetCurrencyCode" value="${toCurrencyCode}"/>
	<ffi:setProperty name="ConvertToBaseCurrency" property="FromAmount" value="${fromAmount}"/>

<ffi:setProperty name="ConvertToBaseCurrency" property="ObjectType" value="${objectType}"/>
<ffi:setProperty name="ConvertToBaseCurrency" property="ObjectID" value="${objectID}"/>
<ffi:process name="ConvertToBaseCurrency"/>
</ffi:cinclude>
<ffi:setProperty name="GetFXRateSheetCalculation" property="RemoveEmptyBuyPriceRate" value="true"/>
<ffi:process name="GetFXRateSheetCalculation"/>

<ffi:cinclude value1="${GetFXRateSheet.AsOfDate}" value2="" operator="equals">
	<ffi:setProperty name="GetFXRateSheet" property="AsOfDate" value="${GetCurrentDate.Date}" />
</ffi:cinclude>

<ffi:cinclude value1="${fxcalculate}" value2="true" operator="notEquals">
<ffi:process name="GetFXRateSheet"/>
</ffi:cinclude>
<script type="text/javascript"><!--


$(function(){

	$("#relativeCurrencyCode").combobox({'size':'30'});


});


	function selectRelativeCurrency()
	{
		//document.ExchangeRateForm.submit();
		$('#RefreshRatesButton').click();
	}


//-->
</script>

<div id="relateRates">
	
	<s:form id="ExchangeRateForm" name="ExchangeRateForm" action='FxRateAction_refresh' namespace="/pages/jsp/transfers" method="post" theme="simple">
	<input type="hidden" name="fxcalculate" value="false"/>
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

	<table border="0" width="100%" cellpadding="0" cellspacing="0">
		<tr>
			<td nowrap>
			<div style="padding-top: 3px; padding-bottom: 3px; text-align: right;"><span class="columndata"><s:text name="jsp.fx_Currency"/>&nbsp;</span></div></td>
			<td colspan="3" align="left"><div style="padding-top: 3px; padding-bottom: 3px;">
				<div class="selectBoxHolder">
					<select id="relativeCurrencyCode" name="relativeCurrencyCode" class="txtbox" onchange="selectRelativeCurrency();">
						<ffi:list collection="FOREIGN_EXCHANGE_BASE_CURRENCIES" items="currency">
							<option  <ffi:cinclude value1='${currency.CurrencyCode}' value2='${relativeCurrencyCode}' operator='equals'>
								selected
								</ffi:cinclude>  value='<ffi:getProperty name="currency" property="CurrencyCode"/>' >
								<ffi:getProperty name="currency" property="Description"/>
							</option>
						</ffi:list>
					</select>
				</div>
			</td>
		</tr>
		
		<tr>
		
		<td align="left" nowrap>
			<div align="right">
				<span class="columndata"><s:text name="jsp.fx_Date"/></span>
			</div>
		</td>

		<td colspan="2">
		<div class="marginTop10">
			<sj:datepicker value="%{#session.GetFXRateSheet.asOfDate}" id="asOfDate" name="asOfDate" label="%{getText('jsp.default_137')}"
				maxlength="10" size="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all txtbox"/>
		<!-- </div>
		</td>
		<td> 
		<div class="">-->
			       &nbsp;&nbsp;&nbsp;
			        <sj:a
						id="RefreshRatesButton"
						formIds="ExchangeRateForm"
                        targets="relateRates"
                        button="true"
                        onBeforeTopics="beforeRefreshFxRates"
                        onCompleteTopics="refreshFxRatesComplete,tabifyDialog"
						onErrorTopics="errorRefreshFxRates"
                        onSuccessTopics="successRefreshFxRates"
	               ><s:text name="jsp.default_6"/></sj:a>
	     </div>
		</td>
		</tr>
		<tr>
			<td colspan="4">&nbsp;</td>
		</tr>
  </table>

<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<div id="calcHolderDiv" class="calcHolderDivCls header">
			<div class="floatleft" style="width:40%">
				<span class="fxCalcLable"><s:text name="jsp.default_125"/></span>
			</div>
			<div class="floatleft" style="width:30%">
				<span><s:text name="jsp.fx_5"/>&nbsp;(<s:text name="jsp.fx_12"/>)</span></span>
			</div>
			<div class="floatleft" style="width:30%">
				<span class="fxCalcLable"><s:text name="jsp.fx_6"/>&nbsp;(<s:text name="jsp.fx_14"/>)</span></span>
			</div>
		</div>
		<ffi:cinclude value1="${FOREIGN_EXCHANGE_RATES}" value2="" operator="equals">
		<div class="ffivisible marginBottom10">
				<br/>
					<s:text name="jsp.fx_18"/>
				<br/>
		</div>
    	</ffi:cinclude>
    	
    	<ffi:cinclude value1="${FOREIGN_EXCHANGE_RATES}" value2="" operator="notEquals">
        <% int band = 0; %>
    	<ffi:list collection="FOREIGN_EXCHANGE_RATES" items="rate">
    	<ffi:cinclude value1="${relativeCurrencyCode}" value2="${rate.TargetCurrencyCode}" operator="notEquals">
		<div class="ffivisible">
			<div class="ffivisible floatleft" style="width:40%;height:25px;"><span class="columndata"><ffi:getProperty name="rate" property="TargetCurrencyDescription"/></span></div>
			<div class="ffivisible floatleft" style="width:30%;height:25px;">
				<span class="columndata">
				  <ffi:cinclude value1="" value2="${rate.BuyPrice}" operator="notEquals">
				  <ffi:getProperty name="rate" property="BuyPrice.StringAbsolute"/>
				  </ffi:cinclude>
				  <ffi:cinclude value1="" value2="${rate.BuyPrice}" operator="equals">N/A</ffi:cinclude>
				</span>
			</div>
			<div class="ffivisible floatleft" style="width:30%;height:25px;">
				<span class="columndata">
				  <ffi:cinclude value1="" value2="${rate.SellPrice}" operator="notEquals">
				  <ffi:getProperty name="rate" property="SellPrice.StringAbsolute"/>
				  </ffi:cinclude>
				  <ffi:cinclude value1="" value2="${rate.SellPrice}" operator="equals">N/A</ffi:cinclude>
				</span>
			</div>
		</div>	
			<% band++; %>
		</ffi:cinclude>
		</ffi:list>
    </ffi:cinclude>
	</div>
</div>
</s:form>
</div>