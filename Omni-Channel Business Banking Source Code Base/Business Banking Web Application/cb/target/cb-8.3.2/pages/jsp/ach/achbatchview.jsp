<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achbatchview" className="moduleHelpClass"/>
<script type="text/javascript"><!--
$(document).ready(function(){
    // we want to clear out any DIV IDS that could be lurking because of a previous DELETE BATCH
    // this can cause problems because the DIV "ViewACHEntries" could be around somewhere and hidden from view
    // so when you do a REVERSE, the reverse entries show up in the hidden DIV
    $('#deleteACHBatchDialogID').empty();
});
 //-->
function viewModel() {
	var urlString = "<ffi:urlEncrypt url='/cb/pages/jsp/ach/viewAchBatch.action?ViewRecurringModel=true&RecID=${ACHBatch.RecID}&IsVirtualInstance=${ACHBatch.IsVirtualInstance}'/>";

	$.ajax({
		url: urlString,
		success: function(data){
			ns.common.closeDialog();
			$('#viewACHBatchDetailsDialogID').html(data).dialog('open');
		}
	});
} 

function exportModel() {
	  $("#exportACHBatchLink").click(function(){
	     $("#ACHBatchForm_Export").submit();
	  });
	}
 </script>

<%-- session clean-up so that batch_header.inc will work right --%>
<ffi:removeProperty name="ViewCompletedBatch" />
<ffi:removeProperty name="ReverseACHBatch" />
<ffi:removeProperty name="CanReverseACHBatch" />
<ffi:removeProperty name="DeleteACHBatch" />
<ffi:removeProperty name="ViewTemplateBatch" />

<ffi:object name="com.ffusion.tasks.ach.ExportACHBatch" id="ExportACHBatch" scope="session"/>
    <ffi:setProperty name='ExportACHBatch' property='BatchName' value='ACHBatch'/>

    <s:form id="ACHBatchForm_View" 
        method="post" name="ViewForm" theme="simple" action="/pages/jsp/ach/achbatch-reverse-send.jsp">
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
    </s:form>
    <s:form id="ACHBatchForm_Export"
        method="post" name="ExportForm" theme="simple" action="/pages/jsp/ach/ExportACHBatchAction.action">
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
    </s:form>
    <div class="approvalDialogHt">
            <!-- using s:include caused buffer overflow -->
            <ffi:include page="/pages/jsp/ach/achbatchcommon.jsp" />
    </div>
    <div style="height:50px; display:block;"></div>
<div  class="ui-widget-header customDialogFooter">
<s:if test="%{!#attr.isTemplate}">
	<sj:a id="printACHDetail" button="true" 
	onClick="ns.common.printPopup();" cssClass="hiddenWhilePrinting"><s:text name="jsp.default_331"/></sj:a>
</s:if>	
    <sj:a id="closeViewACHBatchLink" button="true"
          onClickTopics="closeDialog"
          title="Cancel" cssClass="hiddenWhilePrinting">Done</sj:a>
		  <% String recId = null; %>
		  <ffi:getProperty name="ACHBatch" property="RecID" assignTo="recId"/>
 <% if (recId != null && !recId.isEmpty()) { %>
 <s:if test="%{!#attr.viewRecurringModel && !#attr.isTemplate}">         
    <sj:a 
	id="viewModelLink"
	button="true"
	onClick="viewModel()"
	><s:text name="jsp.default.view_orig_trans"/></sj:a>
 </s:if> 
     <% } %>
<!-- POP Dialog Contains are rendered from this JSP page -->
<s:if test="%{!#attr.isTemplate}">
	<s:if test="%{#request.CanExportBatch}">
		<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
	          <sj:a id="exportACHBatchLink" button="true" title="%{getText('jsp.default_195')}" 
					onClick="exportModel()" cssClass="hiddenWhilePrinting">
					<s:text name="jsp.default_195" />
				</sj:a>
			</ffi:cinclude>
		<ffi:cinclude value1="${subMenuSelected}" value2="child" >
	          <sj:a id="exportACHBatchLink" button="true" title="%{getText('jsp.default_195')}" 
					onClick="exportModel()" cssClass="hiddenWhilePrinting">
					<s:text name="jsp.default_195" />
			  </sj:a>
		</ffi:cinclude>
		<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
	           <sj:a id="exportACHBatchLink" button="true" title="%{getText('jsp.default_195')}" 
					onClick="exportModel()" cssClass="hiddenWhilePrinting">
					<s:text name="jsp.default_195" />
			  </sj:a>
		</ffi:cinclude>
	</s:if>
</s:if>
</div>
<ffi:removeProperty name="TemplateID,CanReverseACHBatch"/>
<ffi:removeProperty name="CanExportBatch"/>
