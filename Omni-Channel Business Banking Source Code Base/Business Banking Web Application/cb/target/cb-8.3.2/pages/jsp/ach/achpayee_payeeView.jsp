<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-jquery-grid-tags" prefix="sjg"%>
<%@ page contentType="text/html; charset=UTF-8" %>

    <s:form id="ACHPayeeForm_Edit" 
        method="post" name="FormName" theme="simple" >
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
    </s:form>
        <div class="type-text">
            <%-- using s:include caused buffer overflow --%>
            <ffi:include page="/pages/jsp/ach/achpayeeview.jsp" />
        </div>
    <div class="ffivisible" style="height:45px;">&nbsp;</div>
    <div  class="ui-widget-header customDialogFooter">
	    <sj:a id="closeViewACHPayeeLink" button="true"
	          onClickTopics="closeViewACHPayee"
	          title="Cancel">
	          	<s:text name="jsp.default_175" /><!-- DONE -->
	    </sj:a>
	</div>