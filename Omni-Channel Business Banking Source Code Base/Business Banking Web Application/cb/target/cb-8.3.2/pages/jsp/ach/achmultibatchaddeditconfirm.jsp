<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.beans.ach.ACHBatch,
				 com.ffusion.beans.SecureUser,
                 com.ffusion.csil.core.common.EntitlementsDefines,
				 com.ffusion.beans.ach.ACHClassCode,
				 com.ffusion.beans.common.Currency,
				 java.lang.Double,
                 java.math.BigDecimal" %>
<%--
	This file is used by both Add/Edit ACH AND Add/Edit ACH Templates
	If you modify this file, make sure your change works for both please.
--%>

<ffi:help id="payments_achmultibatchaddeditconfirm" className="moduleHelpClass"/>

<%
	boolean disabledControls = true;        // always disabled on CONFIRM PAGE.
	boolean showValidationErrors = true;
	boolean addEntryDisabled = false;
  	boolean isEdit = false;
    boolean isView = false;
	boolean isTemplate = true;
	boolean fromTemplate = false;
    boolean isConfirmPage = false;

%>

<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="View">
    <% disabledControls = true; isView = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="Load">
    <% fromTemplate = true; isTemplate = false; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="Edit">
    <% isEdit = true; %>
</ffi:cinclude>

<ffi:removeProperty name="ImportErrors" />

<s:action namespace="/pages/jsp/ach" name="ACHInitAction_initACHBatches" />

<%
    if (request.getParameter("confirm") != null) {
        isConfirmPage = true;
    }
    if (request.getParameter("ToggleSortedBy") != null) {
  	String toggle = ""; %>
	<ffi:getProperty name="ToggleSortedBy" assignTo="toggle"/>
  	<ffi:setProperty name="MultipleACHBatches" property="ToggleSortedBy" value="<%= toggle %>" />
  	<ffi:setProperty name="AddEditMultiACHTemplate" property="ACHBatches.ToggleSortedBy" value="<%= toggle %>" />
<%    }

    String title = "";

    title = "<!--L10NStart-->Confirm ACH Multiple Batches (from template)<!--L10NEnd-->";
	String pageHeading = title;
	session.setAttribute("PageHeading", pageHeading + "<br>");
    if (request.getParameter("Initialize") != null) { %>
    <ffi:removeProperty name="Initialize" />
    <% } %>

<script language="JavaScript">
<!--


function holdAllBatches(holdCheckBox){
    var size = <ffi:getProperty name="AddEditMultiACHTemplate" property="ACHBatches.Size"/>;        // number of entries
    var Checked = holdCheckBox.checked;
    for (i = 0; i < size; i++)
    {
        var hold = document.getElementById("hold"+ i);
        // if we changeHold when already on/off hold, totals get messed up
        if (hold.checked != Checked)       // only change state if not already at that state
        {
            hold.checked = Checked;
            changeHold(hold, i);
        }
    }
}

function changeHold(holdCheckBox, ActiveID){

	var totals = document.getElementById("totals"+ActiveID).value;
<%--        String totals = "" + batch.getTotalNumberCredits() + "_" + batch.getTotalNumberDebits() +"_" + batch.getTotalCreditAmountValue().getCurrencyStringNoSymbolNoComma() + "_" + batch.getTotalDebitAmountValue().getCurrencyStringNoSymbolNoComma(); --%>
	frm = document.forms["AddBatchConfirmForm"];
	tagCredit = frm['TotalNumberCredits'];
    tagDebit = frm['TotalNumberDebits'];
	spanCredit=$('#TotalNumberCreditsVal');
	spanDebit=$('#TotalNumberDebitsVal');
	valueCredit = tagCredit.value;
	valueDebit = tagDebit.value;
	totalCredit = parseInt(valueCredit);
	totalDebit = parseInt(valueDebit);
    changeCredit = parseInt(totals.substr(0, totals.indexOf('_')));
    totals = totals.substr(totals.indexOf('_') + 1);
    changeDebit = parseInt(totals.substr(0, totals.indexOf('_')));
    totals = totals.substr(totals.indexOf('_') + 1);

    creditAmount = totals.substr(0, totals.indexOf('_'));
    debitAmount  = totals.substr(totals.indexOf('_') + 1);

    var creditAmt = parseFloat(creditAmount);
    if (isNaN(creditAmt))
        creditAmt = parseFloat("0.00");
    var debitAmt = parseFloat(debitAmount);
    if (isNaN(debitAmt))
        debitAmt = parseFloat("0.00");

    tagCreditAmount = frm['TotalCreditAmount'];
    tagDebitAmount = frm['TotalDebitAmount'];
	spanCreditAmount = $('#TotalCreditAmountVal');
    spanDebitAmount = $('#TotalDebitAmountVal');
	valueCreditAmount = tagCreditAmount.value;
	while (valueCreditAmount.indexOf(',') > -1)
	{
		valueCreditAmount = valueCreditAmount.substr(0,valueCreditAmount.indexOf(',')) + valueCreditAmount.substr(valueCreditAmount.indexOf(',')+1);
	}
	valueDebitAmount = tagDebitAmount.value;
	while (valueDebitAmount.indexOf(',') > -1)
	{
		valueDebitAmount = valueDebitAmount.substr(0,valueDebitAmount.indexOf(',')) + valueDebitAmount.substr(valueDebitAmount.indexOf(',')+1);
	}

	creditamtTotal = parseFloat(valueCreditAmount);
	debitamtTotal = parseFloat(valueDebitAmount);

	if (holdCheckBox.checked == true){
		document.getElementById("active"+ActiveID).value = "false";
		totalDebit -= changeDebit;			// subtract out previous count
		totalCredit -= changeCredit;			// subtract out previous count
        creditamtTotal -= creditAmt;
        debitamtTotal -= debitAmt;
	} else {
		document.getElementById("active"+ActiveID).value = "true";
		totalDebit += changeDebit;			// Add in  previous count
		totalCredit += changeCredit;			// Add in previous count
        creditamtTotal += creditAmt;
        debitamtTotal += debitAmt;
	}
    amtStr = "" + totalDebit;
    tagDebit.value = amtStr;
	spanDebit.text(amtStr);
    amtStr = "" + totalCredit;
    tagCredit.value = amtStr;
	spanCredit.text(amtStr);

	debitamtTotal = Math.round(debitamtTotal*Math.pow(10,2))/Math.pow(10,2);
	debitamtStr = "" + debitamtTotal;
// WARNING.... Custom Code for US follows, Decimal = ".", grouping separator = ","
	if (debitamtStr.indexOf('.') == -1) { debitamtStr = debitamtStr+".0" } // Add .0 if a decimal point doesn't exist
	temp=(debitamtStr.length-debitamtStr.indexOf('.')); // Find out if the # ends in a tenth (ie 145.5)
	if (temp <= 2) { debitamtStr=debitamtStr+"0"; }; // if it ends in a tenth, add an extra zero to the string
// add in commas again
	var x = debitamtStr.indexOf('.') - 3;
	while (x > 0)
	{
		debitamtStr = debitamtStr.substr(0,x) + ',' + debitamtStr.substr(x, debitamtStr.length);
		x = debitamtStr.indexOf(',')-3;
	}
	tagDebitAmount.value = debitamtStr;
	spanDebitAmount.text('$'+debitamtStr);

	creditamtTotal = Math.round(creditamtTotal*Math.pow(10,2))/Math.pow(10,2);
	creditamtStr = "" + creditamtTotal;
// WARNING.... Custom Code for US follows, Decimal = ".", grouping separator = ","
	if (creditamtStr.indexOf('.') == -1) { creditamtStr = creditamtStr+".0" } // Add .0 if a decimal point doesn't exist
	temp=(creditamtStr.length-creditamtStr.indexOf('.')); // Find out if the # ends in a tenth (ie 145.5)
	if (temp <= 2) { creditamtStr=creditamtStr+"0"; }; // if it ends in a tenth, add an extra zero to the string
// add in commas again
	var x = creditamtStr.indexOf('.') - 3;
	while (x > 0)
	{
		creditamtStr = creditamtStr.substr(0,x) + ',' + creditamtStr.substr(x, creditamtStr.length);
		x = creditamtStr.indexOf(',')-3;
	}
	tagCreditAmount.value = creditamtStr;
	spanCreditAmount.text('$'+creditamtStr);

    if (document.getElementById("holdBatches") != null)
    {
        if (holdCheckBox.checked == true)
        {
            if (frm['TotalNumberCredits'].value == "0" && frm['TotalNumberDebits'].value == "0")
                document.getElementById("holdBatches").checked = true;
        } else
        {
            if (frm['TotalNumberCredits'].value != "0" || frm['TotalNumberDebits'].value != "0")
                document.getElementById("holdBatches").checked = false;
        }
    }
<%--	totalThisAmount(ActiveID);--%>
}

function doSort(URL)
{
	document.AddBatchConfirmForm.action=URL;
	document.AddBatchConfirmForm.submit();
	return false;
}

function totalAmounts() {

    frm = document.forms["AddBatchConfirmForm"];
    var creditAmtTotal = 0.0;
    var debitAmtTotal = 0.0;
    tag = frm['TotalDebitAmount'];
    if (frm['CalculatedTotalDebits'] == null || frm['CalculatedTotalDebits'] == undefined || frm['CalculatedTotalDebits'] == 'undefined')
        return;
    var value = frm['CalculatedTotalDebits'].value;
    while (value.length < 3)
    {
        value = "0" + value;			// leading zeros, for decimal point addition
    }
    value = value.substr(0, value.length - 2) + "." + value.substr(value.length - 2);
// add in commas again
    x = value.indexOf('.') - 3;
    while (x > 0)
    {
        value = value.substr(0,x) + ',' + value.substr(x, value.length);
        x = value.indexOf(',')-3;
    }
    tag.value = value;

    tag = frm['TotalCreditAmount'];
    value = frm['CalculatedTotalCredits'].value;
    while (value.length < 3)
    {
        value = "0" + value;			// leading zeros, for decimal point addition
    }
    value = value.substr(0, value.length - 2) + "." + value.substr(value.length - 2);
// add in commas again
    x = value.indexOf('.') - 3;
    while (x > 0)
    {
        value = value.substr(0,x) + ',' + value.substr(x, value.length);
        x = value.indexOf(',')-3;
    }
    tag.value = value;

    tag = frm['TotalNumberCredits'];
    value = frm['CalculatedNumberCredits'].value;
    tag.value = value;

    tag = frm['TotalNumberDebits'];
    var value = frm['CalculatedNumberDebits'].value;
    tag.value = value;

    var item = document.getElementById('holdBatches');
    if (item != null)
    {
        var value = frm['CalculatedHoldBatches'].value;
        if (value == "true")
            item.checked = true;
        else item.checked = false;
    }

    return;
}


// --></script>
<s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />
<div align="center">
<div class="leftPaneWrapper">
		<div class="leftPaneInnerWrapper">
		<div class="header"><s:text name="jsp.default.templates.summary" /></div>
		<!-- ACHBatch Entry header and Grid details -->
			<div class="paneContentWrapper summaryBlock">
					<div>
						<span class="sectionsubhead sectionLabel inlineSection floatleft"><s:text name="ach.grid.templateName" /><% if (!disabledControls) { %><span class="required">*</span><% } %>:</span>
						<span class="labelValue inlineSection floatleft">
							<ffi:getProperty name="AddEditMultiACHTemplate" property="TemplateName"/>
						</span>
					</div>
					<div class="marginTop10 clearBoth floatleft">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_418" /><% if (!disabledControls) { %><span class="required">*</span><% } %>:</span>
						<span class="labelValue">
							<ffi:getProperty name="AddEditMultiACHTemplate" property="BatchScope"/>
							<%-- <select id="confirmScopeId" <%=disabledControls?"disabled":""%> class="txtbox" name="AddEditMultiACHTemplate.BatchScope">
								<option value="BUSINESS" <ffi:cinclude value1="BUSINESS" value2="${AddEditMultiACHTemplate.BatchScope}" operator="equals" >selected</ffi:cinclude>><!--L10NStart-->Business (General Use)<!--L10NEnd--></option>
								<option value="USER" <ffi:cinclude value1="USER" value2="${AddEditMultiACHTemplate.BatchScope}" operator="equals" >selected</ffi:cinclude>><!--L10NStart-->User (My Use)<!--L10NEnd--></option>
							</select> --%>
						</span>
					</div>
			</div>
		</div>
</div>
<div class="confirmPageDetails">
<s:form id="AddBatchConfirmFormId" namespace="/pages/jsp/ach" validate="false" action="editMultiACHTemplateAction_execute" method="post" name="AddBatchConfirmForm" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<ffi:cinclude operator="equals" value1="true" value2="${AddEditMultiACHTemplate.BatchValidationErrors}">
<div class="mainfontbold errortext" colspan="7">(<s:text name="jsp.ach.ACH.batch.errorMsg" />)</div>
<div class="mainfont"><s:text name="jsp.ach.error.processing" /></div>
</ffi:cinclude>
<div align="left">


<%-- use hidden form fields because if using setProperty, it messes up achmultitemplateaddedit.jsp because this file is included inside of it --%>

									<%-- <tr>
										<td class="sectionhead lightBackground" colspan="7"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="692" height="1" border="0"></td>
									</tr>
									<tr valign="top">
										<td class="sectionhead lightBackground" colspan="7"><span class="sectionhead">&gt; <%=title%><br>
										</span></td>
									</tr> --%>

<%-- Create a new table with two columns, the first column will be the left side,
the second column will be the right side (repeating and Offset Account) --%>
                                    <tr class="lightBackground">

									<%-- <tr class="lightBackground">
										<td align="right" width="200">
											<span class="sectionsubhead"><!--L10NStart-->Template Name<!--L10NEnd--> </span>
                                            <% if (!disabledControls) { %>
                                                <span class="required">*</span>
                                            <% } %>
											</td>
										<td class="columndata" ><input <%=disabledControls?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="32" border="0" name="AddEditMultiACHTemplate.TemplateName" value="<ffi:getProperty name="AddEditMultiACHTemplate" property="TemplateName"/>"></td>
									</tr>

									<tr class="lightBackground">
										<td align="right">
											<span class="sectionsubhead"><!--L10NStart-->Template Scope<!--L10NEnd--> </span>
                                            <% if (!disabledControls) { %>
                                                <span class="required">*</span>
                                            <% } %>
										</td>
										<td class="columndata">
											<select id="confirmScopeId" <%=disabledControls?"disabled":""%> class="txtbox" name="AddEditMultiACHTemplate.BatchScope">
												<option value="BUSINESS" <ffi:cinclude value1="BUSINESS" value2="${AddEditMultiACHTemplate.BatchScope}" operator="equals" >selected</ffi:cinclude>><!--L10NStart-->Business (General Use)<!--L10NEnd--></option>
												<option value="USER" <ffi:cinclude value1="USER" value2="${AddEditMultiACHTemplate.BatchScope}" operator="equals" >selected</ffi:cinclude>><!--L10NStart-->User (My Use)<!--L10NEnd--></option>
											</select>
										</td>
 --%>
									</tr>



<%-- end of table --%>

                                <%-- <% if (!disabledControls) { %>
                                    <tr>
                                        <td colspan="5"><div align="center"><span class="required">* <!--L10NStart-->indicates a required field<!--L10NEnd--></span></div></td>
                                    </tr>
                                <% } %> --%>
								<%-- <ffi:cinclude operator="equals" value1="true" value2="${AddEditMultiACHTemplate.BatchValidationErrors}">
									<tr>
										<td class="lightBackground" colspan="7" height="20"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="1" border="0"></td>
									</tr>
 									<tr>
										<td class="lightBackground mainfontbold errortext" colspan="7">(<!--L10NStart-->There were some errors detected in your ACH batches.<!--L10NEnd-->)</td>
									</tr>
 									<tr>
										<td class="lightBackground mainfont" colspan="7"><!--L10NStart-->Error messages are displayed in red below each entry with an error. You must correct all errors before you can submit this batch for processing.<!--L10NEnd--></td>
									</tr>
								</ffi:cinclude> --%>
		<%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
	    <td class="sectionhead" rowspan="2" width="36%"><span class="sectionhead">&gt;&nbsp;<!--L10NStart-->Batches<!--L10NEnd--><br>
<% if (!disabledControls) { %>
			<input id="holdBatches" type="checkbox" name="checkbox" onclick="holdAllBatches(this);" >
            <!--L10NStart-->Hold Batches<!--L10NEnd-->
<% } %>
	    </span></td> --%>

		<%-- <td class="sectionsubhead" valign="center" width="16%">
			<!--L10NStart-->Total # Credits<!--L10NEnd-->
		</td>
            <td class="sectionsubhead" width="16%">
                <input type="text" readonly name="TotalNumberCredits" maxlength="6" size="8" value="" class="ui-widget-content ui-corner-all txtbox">
            </td>
            <td class="sectionsubhead" width="16%">
                <!--L10NStart-->Total # Debits<!--L10NEnd-->
            </td>
            <td class="sectionsubhead" width="16%">
                <input type="text" readonly name="TotalNumberDebits" maxlength="6" size="8" value="" class="ui-widget-content ui-corner-all txtbox">
            </td>
            </tr>
            <tr>
            <td class="sectionsubhead" width="16%">
                <!--L10NStart-->Total Credits<!--L10NEnd--> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
            </td>
            <td class="sectionsubhead" width="16%">
                <input type="text" readonly name="TotalCreditAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all txtbox">
            </td>
            <td class="sectionsubhead" width="16%">
                <!--L10NStart-->Total Debits<!--L10NEnd--> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
            </td>
            <td class="sectionsubhead" width="16%">
                <input type="text" readonly name="TotalDebitAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all txtbox">
            </td> --%>
	<!-- 	</tr>
		</table> -->
</div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
<table width="100%" border="0" cellspacing="0" cellpadding="1">
	<tr class="header">
        <% if (!isConfirmPage) { %>
		<td class="sectionsubhead" width="10%">
<% if (isTemplate) { %>
		<s:text name="jsp.default_238" />
<% } else { %>
		<s:text name="jsp.default_228" />
<% } %>
                    </td>
        <% } %>
                    <td class="sectionsubhead" width="11%"><s:text name="jsp.ach.SEC.Code" /><br>
                    <s:text name="ach.grid.batchName" />
                    </td>
                    <td class="sectionsubhead" width="15%"><s:text name="ach.company.description" /><br>
                    <s:text name="jsp.default_24" />
                    </td>
                    <td class="sectionsubhead" width="10%"><s:text name="jsp.default_513" />
                    </td>
                    <td class="sectionsubhead" width="13%"><s:text name="jsp.ach.Total.Credits.USD" />
                    </td>
                    <td class="sectionsubhead" width="10%"><s:text name="jsp.default_514" />
                    </td>
                    <td class="sectionsubhead" width="13%"><s:text name="jsp.ach.Total.Debits.USD" />
                    </td>
                    <td class="sectionsubhead" width="8%"><s:text name="jsp.ach.Total.Entries" />
                    </td>
			        <% if (isConfirmPage) { %>
			            <td class="sectionsubhead" width="8%"><s:text name="jsp.default_388" />
			            </td>
			        <% } %>
			        <% if (!disabledControls) { %>
			            <td><s:text name="jsp.default_27" /></td>
					<% }else { %><td class="sectionsubhead"></td>
					<% } %>
					<% if (fromTemplate) { %>
			    	<ffi:setProperty name="AddEditMultiACHTemplate" property="CurrentBatch" value="${ACHBatch.ID}"/>
			            <td>
			                <span class="sectionsubhead"><s:text name="ach.grid.effectiveDate" /></span><span class="">*</span>
			            </td>
			    	<% } %>
	</tr>
	<%
		Double totalCredits = 0.0;
		Double totalDebits = 0.0;
        int numberCredits = 0;
        int numberDebits = 0;
        boolean holdBatches = true;
        int count = 0;
	%>
        <%--Getting the Individual Transfer object by iterating over the transfers  --%>
        <s:set var="count" value="0" />
<ffi:list collection="AddEditMultiACHTemplate.ACHBatches" items="ACHBatch">
<% boolean isActive = false;
		ACHBatch batch = (ACHBatch)request.getAttribute("ACHBatch");
        batch.fixupEntriesAmount();             // need totals to be correct
%>
	<ffi:cinclude value1="${ACHBatch.Active}" value2="true" >
		<% isActive = true; holdBatches = false;%>
	</ffi:cinclude>
<%
        String totals = "" + batch.getTotalNumberCredits() + "_" + batch.getTotalNumberDebits() +"_" + batch.getTotalCreditAmountValue().getCurrencyStringNoSymbolNoComma() + "_" + batch.getTotalDebitAmountValue().getCurrencyStringNoSymbolNoComma();
		String ID = batch.getID();
        if (isActive)
        {
        	if (batch.getTotalDebitAmountValue() != null)
                totalDebits += Double.parseDouble(batch.getTotalDebitAmountValue().getCurrencyStringNoSymbolNoComma());
            numberDebits += batch.getTotalNumberDebits();
            if (batch.getTotalCreditAmountValue() != null)
                totalCredits += Double.parseDouble(batch.getTotalCreditAmountValue().getCurrencyStringNoSymbolNoComma());
            numberCredits += batch.getTotalNumberCredits();
        }
%>
	<% if (isActive) { %>
	<tr>
        <% if (!isConfirmPage) { %>
        <% String ACHBatchActive = "";  %>
        <ffi:getProperty name="ACHBatch" property="Active" assignTo="ACHBatchActive" />
        <% session.setAttribute("ACHBatchActive", ACHBatchActive); %>

		    <td align="center">
                <input id="hold<s:property value="#count" />" type="checkbox" <%=disabledControls?"disabled":""%> name="checkbox" onclick="changeHold(this,'<s:property value="#count" />');" <ffi:cinclude value1="${ACHBatch.Active}" value2="false">checked</ffi:cinclude>>
                <input id="active<s:property value="#count" />" type="hidden" <%=disabledControls?"disabled":""%> name="ACHBatches[<s:property value="#count" />].Active" value="<s:property value="#session.ACHBatchActive" />" >

            <input type="hidden" id="totals<s:property value="#count" />" name="totals<s:property value="#count" />" value='<%=totals %>'>
			</td>
        <% } %>
			<td>
                <ffi:getProperty name="ACHBatch" property="StandardEntryClassCode"/><br>
                <ffi:getProperty name="ACHBatch" property="TemplateName"/>
            </td>
			<td>
                <ffi:getProperty name="ACHBatch" property="CoEntryDesc"/><br>
                <ffi:getProperty name="ACHBatch" property="CoName"/>/ <ffi:getProperty name="ACHBatch" property="CompanyID"/>
            </td>
			<td>
				<ffi:getProperty name="ACHBatch" property="TotalNumberCredits"/>
			</td>
			<td>
				<ffi:getProperty name="ACHBatch" property="TotalCreditAmount"/>
			</td>
			<td>
				<ffi:getProperty name="ACHBatch" property="TotalNumberDebits"/>
			</td>
			<td>
				<ffi:getProperty name="ACHBatch" property="TotalDebitAmount" />
            </td>
            <td>
                <ffi:getProperty name="ACHBatch" property="ACHEntries.Size" />
            </td>

<%-- tabindex='1' added so amounts are all first to tab to --%>
		<td>
		<% if (!disabledControls) { %>
            <a class="ui-button" onclick="ns.ach.editACHMultiTemplateBatch('<ffi:urlEncrypt url="/cb/pages/jsp/ach/editMultiACHTemplateAction_editbatch.action?EditBatchID=${ACHBatch.ID}"/>');"><span class="ui-icon ui-icon-wrench"></span></a>
            <% if (!fromTemplate) { %>
                <a class="ui-button" onclick="ns.ach.deleteACHMultiTemplateBatch('<ffi:urlEncrypt url="/cb/pages/jsp/ach/editMultiACHTemplateAction_deletebatch.action?DeleteBatchID=${ACHBatch.ID}"/>');"><span class="ui-icon ui-icon-trash"></span></a>
            <% } %>
		<% } if (isConfirmPage) { %>
            <ffi:getProperty name="ACHBatch" property="Status" />
        <% } else { %>
			&nbsp;
		<% } %>
	</td>
	<% if (fromTemplate) { %>
		 <td class="columndata">
                <ffi:getProperty name="ACHBatch" property="Date" />
           </td>
     <% } %>
	</tr>
    <%-- <% if (fromTemplate) { %>
    	<ffi:setProperty name="AddEditMultiACHTemplate" property="CurrentBatch" value="${ACHBatch.ID}"/>
        <tr class="lightBackground">
            <td align="right" colspan='2'>
                <span class="sectionsubhead"><!--L10NStart-->Effective Date<!--L10NEnd--></span><span class="required">*</span>
            </td>
            <td class="columndata" colspan='3'>
                <ffi:getProperty name="ACHBatch" property="Date" />
            </td>

        </tr>
    <% } %> --%>

	<% if (showValidationErrors) { %>
	<ffi:cinclude operator="equals" value1="${ACHBatch.hasValidationErrors}" value2="true">
		<% String lineContent = ""; String lineNumber = ""; String recordNumber = ""; %>
		<ffi:list collection="ACHBatch.validationErrors" items="validationError">
			<ffi:getProperty name="ACHBatch" property="lineContent" assignTo="lineContent"/>
			<ffi:getProperty name="ACHBatch" property="lineNumber" assignTo="lineNumber"/>
			<ffi:getProperty name="ACHBatch" property="recordNumber" assignTo="recordNumber"/>
		<tr>
			<td colspan="10" class="columndata_error columndata_bold">&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="validationError" property="title"/></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="9" class="columndata_error">
			<% if (lineNumber != null && recordNumber != null) { %>
      	<span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-1" parm0="${ACHBatch.lineNumber}" parm1="${ACHBatch.recordNumber}"/>:</span>
			<% } %>
			<% if (lineNumber != null && recordNumber == null) { %>
        <span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-2" parm0="${ACHBatch.lineNumber}"/>:</span>
			<% } %>
			<% if (lineNumber == null && recordNumber != null) { %>
        <span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-3" parm0="${ACHBatch.recordNumber}"/>:</span>
			<% } %>
        <ffi:getProperty name="validationError" property="message"/>
			</td>
		</tr>
			<% if (lineContent != null) { %>
			<tr>
				<td></td>
				<td colspan="9" class="columndata_error">
					<span class="columndata_bold"><s:text name="jsp.default_209"/>:</span>
					<%= lineContent %>
				</td>
			</tr>
			<tr>
				<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="5" border="0"></td>
			</tr>
			<% } %>
		</ffi:list>
	</ffi:cinclude>
	<% } %>
<% } // only the active ones get displayed%>
</ffi:list>

			<%
				SecureUser suser = (SecureUser) session.getAttribute("SecureUser");
				java.util.Locale userLocale = suser.getLocale();
			%>
		<tr class="totalAmountRow">
       		<% if (!isConfirmPage) { %><td colspan="3"><s:text name="jsp.default_431" />:</td>
       		<% } else { %><td colspan="2">Total:</td><% } %>
            <td class="sectionsubhead" >
                <input id="viewAchMultipleBatchTemplateTotaltalNumberCredit" type="hidden" readonly name="TotalNumberCredits" maxlength="6" size="8" value="" class="ui-widget-content ui-corner-all txtbox">
                <span id="TotalNumberCreditsVal">
                	<%="" + numberCredits%>
                </span>
            </td>
            <td class="sectionsubhead" >
                <input id="viewAchMultipleBatchTemplateTotalCredits" type="hidden" readonly name="TotalCreditAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all txtbox">
                <span id="TotalCreditAmountVal">
                <%Currency totalCreditsCurrency=new Currency(totalCredits.toString(),userLocale); %>
				<%=totalCreditsCurrency.toString()%>


                </span>
            </td>
             <td class="sectionsubhead" >
                <input id="viewAchMultipleBatchTemplateTotalNumberDebit" type="hidden" readonly name="TotalNumberDebits" maxlength="6" size="8" value="" class="ui-widget-content ui-corner-all txtbox">
                <span id="TotalNumberDebitsVal">
                	<%="" + numberDebits%>
                </span>
            </td>
            <td class="sectionsubhead" >
                <input id="viewAchMultipleBatchTemplatetotalDebit" type="hidden" readonly name="TotalDebitAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all txtbox">
                <span id="TotalDebitAmountVal">
                <%Currency totalDebitsCurrency=new Currency(totalDebits.toString(),userLocale); %>
				<%=totalDebitsCurrency.toString()%>

                </span>
            </td>
       		<td colspan="4"></td>
		</tr>
    <input type="hidden" name="CalculatedTotalDebits" value="<%="" + totalDebits%>" >
    <input type="hidden" name="CalculatedTotalCredits" value="<%="" + totalCredits%>" >
    <input type="hidden" name="CalculatedNumberDebits" value="<%="" + numberDebits%>" >
    <input type="hidden" name="CalculatedNumberCredits" value="<%="" + numberCredits%>" >
    <input type="hidden" name="CalculatedHoldBatches" value="<%="" + holdBatches%>">
</table>
</div>
</div>
</div>
<% if (!disabledControls) { %>
  <div align="center" class="marginTop10"><span class="required">* <s:text name="jsp.default_240" /></span></div>
<% } %>
<div class="btn-row">
<% if (!isConfirmPage) { %>
<sj:a
             button="true"
	summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHForm"
     	><s:text name="jsp.default_82"/>
</sj:a>
<% } %>
<%  String cancelGoesWhere;
if (isTemplate) {
	cancelGoesWhere = "payments/achtemplates.jsp";
} else {
	cancelGoesWhere = "payments/achpayments.jsp";
}
%>
<% if (!isConfirmPage) { %>

<%--
<input class="submitbutton" type="button" value="BACK" onclick="document.location='<ffi:getProperty name="SecurePath"/>payments/achmultibatchaddedit.jsp'; return false;">
--%>
<sj:a button="true" onClickTopics="ach_backToInput,clearDateError"> <s:text name="jsp.default_57" /> </sj:a>
<%--
<input class="submitbutton" type="button" value="CANCEL" onclick="document.location='<ffi:getProperty name="SecurePath"/><%= cancelGoesWhere %>';return false;">
--%>
<% } %>
<% if (!isView) { %>
<ffi:cinclude operator="equals" value1="false" value2="${AddEditMultiACHTemplate.BatchValidationErrors}">
<ffi:cinclude operator="equals" value1="true" value2="<%= String.valueOf( isTemplate ) %>">
<input class="submitbutton" type="submit" value="Save Multi Batch Template">
</ffi:cinclude>
<ffi:cinclude operator="notEquals" value1="true" value2="<%= String.valueOf( isTemplate ) %>">
      <% if (!isConfirmPage) { %>
<input class="submitbutton" type="hidden" value="SUBMIT BATCHES FOR PAYMENT" onClick="document.forms['AddBatchConfirmForm'].action='<ffi:urlEncrypt url="${SecurePath}payments/achmultitemplateaddeditsave.jsp?Submit=true"/>'; document.forms['AddBatchConfirmForm'].submit();">
<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
<sj:a
id="submitBatchesSubmitId"
formIds="AddBatchConfirmFormId"
                        targets="resultmessage"
                        button="true"
                        validate="true"
                        validateFunction="customValidation"
                        onSuccessTopics="successSubmitACHBatchesForPaymentForm"
                        onErrorTopics="errorSubmitBatchesForPaymentForm"
                  ><s:text name="jsp.default_395" />
	</sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" >
<sj:a
id="submitBatchesSubmitId"
formIds="AddBatchConfirmFormId"
                        targets="resultmessage"
                        button="true"
                        validate="true"
                        validateFunction="customValidation"
                        onSuccessTopics="successSubmitChildBatchesForPaymentForm"
                        onErrorTopics="errorSubmitBatchesForPaymentForm"
                  ><s:text name="jsp.default_395" />
	</sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
<sj:a
id="submitBatchesSubmitId"
formIds="AddBatchConfirmFormId"
                        targets="resultmessage"
                        button="true"
                        validate="true"
                        validateFunction="customValidation"
                        onSuccessTopics="successSubmitTaxBatchesForPaymentForm"
                        onErrorTopics="errorSubmitBatchesForPaymentForm"
                  ><s:text name="jsp.default_395" />
	</sj:a>
</ffi:cinclude>
<% } else { %>
      <ffi:cinclude value1="${subMenuSelected}" value2="ach" >
          <sj:a
          button="true" summaryDivId="summary" buttonType="done"
          onClickTopics="showSummary,ACHSummaryLoadedSuccess,calendarToSummaryLoadedSuccess"
          >Done
      </sj:a>
      </ffi:cinclude>
      <ffi:cinclude value1="${subMenuSelected}" value2="child" >
          <sj:a
          button="true"
		  summaryDivId="summary" buttonType="done"
          onClickTopics="showSummary,childspSummaryLoadedSuccess,calendarToSummaryLoadedSuccess"
          >Done
      </sj:a>
      </ffi:cinclude>
      <ffi:cinclude value1="${subMenuSelected}" value2="tax" >
          <sj:a
              button="true"
			   summaryDivId="summary" buttonType="done"
				onClickTopics="showSummary,taxSummaryLoadedSuccess,calendarToSummaryLoadedSuccess"
          >Done
      </sj:a>
      </ffi:cinclude>
      <% } %>
</ffi:cinclude>
</ffi:cinclude>
<% if (!isConfirmPage) { %>
	<ffi:cinclude operator="equals" value1="true" value2="${AddEditMultiACHTemplate.BatchValidationErrors}">
	<input class="submitbutton" type="hidden" value="RECHECK VALIDATION" onClick="document.forms['AddBatchConfirmForm'].action='<ffi:urlEncrypt url="${SecurePath}payments/achmultibatchaddedit.jsp?ValidateBatch=true"/>'; document.forms['AddBatchConfirmForm'].submit();">
	<input class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'  type="button" value="RECHECK VALIDATION" onClick="ns.ach.recheckValidatonMultiTemplate();">
	</ffi:cinclude>
<%  } %>
<%  } %>

</s:form>
</div>
</div>
<script type="text/javascript">
<!--
$(document).ready(function(){
	totalAmounts();
	$("#confirmScopeId").selectmenu({width: 200});
});
//-->
</script>
<ffi:removeProperty name="ModifyACHEntry" />
<ffi:removeProperty name="ACHEntryPayee" />
<ffi:removeProperty name="coEntrySecureDesc"/>
