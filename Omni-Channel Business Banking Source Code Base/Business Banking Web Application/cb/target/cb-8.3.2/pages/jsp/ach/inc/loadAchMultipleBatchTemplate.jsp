<%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
	

			<select id="TemplateID" class="txtbox" name="TemplateID" onChange="ns.ach.enableLoadButton(this.value);" aria-labelledby="standardEntryClassCodeId">
				<option>
					<ffi:getProperty name="AddEditMultiACHTemplate" property="TemplateName"/>
				</option>
			</select>
			<input id="loadButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-disabled' style="display:none" type="button" disabled="true" name="load" value="Load" onclick="ns.ach.loadTemplate('<ffi:getProperty name='CSRF_TOKEN'/>','<ffi:getProperty name='ACHType'/>');"/>
	
<script type="text/javascript">
<!--
		
		$("#TemplateID").lookupbox({
			"source":"/cb/pages/common/ACHLoadTempateAction.action",
			"controlType":"server",
			"size":"55",
			"module":"multipleLoadTemplate"
		});
		
//-->
function enableLoadButton(selectedvalue)
	{
		$('#loadButtonId').trigger('click');
	}
</script>			

<style>

	#loadButtonId {
		font-family: arial !important;
	    font-size: 13px !important;
	    font-weight: normal !important;
	    margin-left: 5px !important;
	    padding: 5px !important;
	    top: 1px !important;
	}
	
</style>

