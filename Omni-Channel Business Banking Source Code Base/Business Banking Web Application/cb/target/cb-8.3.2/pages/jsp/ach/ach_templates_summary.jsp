<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<% session.removeAttribute("viewForACHCompanyTemplates"); %>
	
<div id="ACHTemplateGridTabs" class="portlet gridPannelSupportCls" style="float: left; width: 100%; role="section" aria-labelledby="achTemplatesSummaryHeader">
    <div class="portlet-header">
    	<h1 id="achTemplatesSummaryHeader" class="portlet-title"><s:text name="jsp.ach.templatesummary"/></h1>
	    <div class="searchHeaderCls">
			<span class="searchPanelToggleAreaCls" onclick="ns.ach.showTemplateSearchCriteria();">
				<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
			</span>
			<div class="summaryGridTitleHolder">
				<span id="selectedGridTab" class="selectedTabLbl" >
					<s:text name="ach.singleTemplatesTab"/>
				</span>
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
				
				<div class="gridTabDropdownHolder">
					<span class="gridTabDropdownItem" id='single' onclick="ns.common.showGridSummary('single');ns.ach.setCurrentTemplateSearchCriteria('templatessearchcriteria','multitemplatessearchcriteria');" >
						<s:text name="ach.singleTemplatesTab"/>
					</span>
					<span class="gridTabDropdownItem" id='multiple' onclick="ns.common.showGridSummary('multiple');ns.ach.setCurrentTemplateSearchCriteria('multitemplatessearchcriteria','templatessearchcriteria');">
						<s:text name="ach.multiTemplatesTab"/>
					</span>
				</div>
			</div>
		</div>
    </div>
    
    <div class="portlet-content">
    	<div class="searchDashboardRenderCls">
			<%
			    String type = (String)session.getAttribute("subMenuSelected");
			    if ("ach".equals(type)) { %>
			    	<s:include value="/pages/jsp/ach/inc/achTemplateSearchCriteria.jsp"/>
			    <%} else if ("tax".equals(type)) { %>
			    	<s:include value="/pages/jsp/ach/inc/taxTemplateSearchCriteria.jsp"/>
			    <%} else if ("child".equals(type)) { %>
			    	<s:include value="/pages/jsp/ach/inc/childSupportTemplateSearchCriteria.jsp"/>
			   	<%}
			%>
		</div>
		<div id="gridContainer" class="summaryGridHolderDivCls">
			<div id="singleSummaryGrid" gridId="singleACHTemplatesGridID" class="gridSummaryContainerCls" >
				<s:include value="/pages/jsp/ach/ach_singletemplates.jsp"/>
			</div>
			<div id="multipleSummaryGrid" gridId="multipleACHTemplatesGridID" class="gridSummaryContainerCls hidden" >
				<s:include value="/pages/jsp/ach/ach_multipletemplates.jsp"/>
			</div>
		</div>
    </div>
	
	<div id="achTemplateSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('ACHTemplateGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
	
</div>

<input type="hidden" id="getTransID" value="<ffi:getProperty name='ACHtemptabs' property='TransactionID'/>" />	

<script>
	//Initialize portlet with settings icon
	ns.common.initializePortlet("ACHTemplateGridTabs");
</script>