<%@ page import="com.ffusion.beans.ach.ACHPayee"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_ACHBanklist" className="moduleHelpClass"/>
<ffi:cinclude value1="${ACHCountryList}" value2="" operator="equals">
    <ffi:object name="com.ffusion.tasks.util.GetBankLookupStandardCountryNames" id="GetBankLookupStandardCountryNames" scope="session"/>
    <ffi:setProperty name="GetBankLookupStandardCountryNames" property="CollectionSessionName" value="ACHCountryList"/>
    <ffi:process name="GetBankLookupStandardCountryNames"/>
    <ffi:removeProperty name="GetBankLookupStandardCountryNames"/>
</ffi:cinclude>
<% String bankIDType = "";
if( request.getParameter("bankIDType") != null && request.getParameter("bankIDType").length() > 0 ){ bankIDType = request.getParameter("bankIDType"); }
%>

<s:if test="%{'true'.equalsIgnoreCase(#parameters.searchAgain[0])">
	<ffi:object id="GetACHParticipantBanks" name="com.ffusion.tasks.ach.GetACHParticipantBanks" scope="session"/>
</s:if>
<s:else>
	<ffi:setProperty name="GetACHParticipantBanks" property="AchRTN" value=""/>
</s:else>
<ffi:removeProperty name="searchAgain"/>
<% session.setAttribute("FFIGetACHParticipantBanks", session.getAttribute("GetACHParticipantBanks")); %>

			<div align="center">
			<table width="500" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<tr>
					<td class="columndata" align="left" class="ltrow2_color">
						<table width="100%" border="0" cellspacing="0" cellpadding="3" class="ltrow2_color">
							<tr>
								<td align="center">
									<s:form action="/pages/jsp/ach/getACHParticipantBanksAction.action" method="post" name="BankForm" id="ach_BankFormID">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<input type="hidden" name="ACHBankFormUsed" value="true">
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_61" />&nbsp;</td>
											<td><input id="Lookup_BankName" class="ui-widget-content ui-corner-all txtbox" type="text" name="BankName" size="30" value="<ffi:getProperty name="GetACHParticipantBanks" property="BankName"/>"></td>
										</tr>
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_101" />&nbsp;</td>
											<td><input id="Lookup_BankCity" class="ui-widget-content ui-corner-all txtbox" type="text" name="BankCity" size="30" value="<ffi:getProperty name="GetACHParticipantBanks" property="BankCity"/>"></td>
										</tr>
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_386" />&nbsp;</td>
											<td><input id="Lookup_BankState" class="ui-widget-content ui-corner-all txtbox" type="text" name="BankState" size="30" value="<ffi:getProperty name="GetACHParticipantBanks" property="BankState"/>"></td>
										</tr>
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_115" />&nbsp;</td>
											<td>
												<% String tempDefaultValue = ""; String tempCountry = ""; %>
	                                            <ffi:getProperty name="GetACHParticipantBanks" property="BankCountry" assignTo="tempDefaultValue"/>
												<% if (tempDefaultValue == null) tempDefaultValue = ""; %>
	                                            <% tempDefaultValue = ( tempDefaultValue == "" ? com.ffusion.banklookup.beans.FinancialInstitution.PREFERRED_COUNTRY : tempDefaultValue ); %>
	                                            <select id="Lookup_BankCountry" name="BankCountry" style="width: 162;"  class="ui-widget-content txtbox">
	                                            <ffi:list collection="ACHCountryList" items="country">
	                                                <ffi:getProperty name="country" property="BankLookupCountry" assignTo="tempCountry"/>
	                                                <option value="<ffi:getProperty name='tempCountry'/>" <%= ( tempDefaultValue.equals( tempCountry ) ? "selected" : "" ) %>><ffi:getProperty name="country" property='Name'/></option>
	                                            </ffi:list>
	                                            </select></td>
										</tr>
										<tr>
											<td align="right" class="sectionhead"><s:text name="jsp.default_201" />&nbsp;</td>
											<td><input id="Lookup_BankRTN" class="ui-widget-content ui-corner-all txtbox" type="text" name="AchRTN" size="30" value="<ffi:getProperty name="GetACHParticipantBanks" property="AchRTN"/>"></td>
										</tr>
    <% if (bankIDType != null && bankIDType.equals(ACHPayee.PAYEE_BANK_IDENTIFIER_TYPE_OTHER_RTN)) { %>
										<tr>
											<td align="right" class="sectionhead"><s:text name="ach.nationalId" />&nbsp;</td>
											<td><input id="Lookup_BankNationalID" class="ui-widget-content ui-corner-all txtbox" type="text" name="NationalID" size="30" value="<ffi:getProperty name="GetACHParticipantBanks" property="NationalID"/>"></td>
										</tr>
    <% } %>
    <% if (bankIDType != null && bankIDType.equals(ACHPayee.PAYEE_BANK_IDENTIFIER_TYPE_BIC)) { %>
										<tr>
											<td align="right" class="sectionhead"><s:text name="ach.swiftid" />&nbsp;</td>
											<td><input id="Lookup_BankSwiftRTN" class="ui-widget-content ui-corner-all txtbox" type="text" name="SwiftRTN" size="30" value="<ffi:getProperty name="GetACHParticipantBanks" property="SwiftRTN"/>"></td>
										</tr>
    <% } %>
										
										<tr>
											<td colspan="2" height="25">&nbsp;</td>
										</tr>
										<tr>
											<td align="center" colspan="2"  class="ui-widget-header customDialogFooter">
<%--
												<input class="submitbutton" type="submit" value="SEARCH" >&nbsp;
												<input class="submitbutton" type="button" value="CLOSE" onclick="self.close();return false;">
												--%>
											    <sj:a
											        onClickTopics="ach_selectDestBankTopics"
											        button="true"
											        ><s:text name="jsp.default_373" /><!-- SEARCH -->
											    </sj:a>
											    <sj:a 
											            button="true"
											            onClickTopics="ach_closeSearchBankListOnClickTopics"
											    ><s:text name="jsp.default_102" /><!-- CLOSE -->
											    </sj:a>
											</td>
										</tr>
									</table>
									&nbsp;
									</s:form>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</div>
<script>
	$("#Lookup_BankCountry").selectmenu({width: 240});
</script>
			
