<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:removeProperty name="BatchID,TemplateID"/>
<%
	String templateEdit = request.getParameter("templateEdit");
	session.setAttribute("templateEdit", templateEdit);
	String collectionName = request.getParameter("SetACH.Name");
	String achID = request.getParameter("SetACH.ID");
%>

<ffi:cinclude value1="${templateEdit}" value2="true">
<%
    session.setAttribute("subMenuSelected", "ach");
    session.setAttribute("TemplateID", achID);
    session.setAttribute("Collection", collectionName);
    session.setAttribute("CollectionName", collectionName);
%>
</ffi:cinclude>

<ffi:cinclude value1="${templateEdit}" value2="true" operator="notEquals">
<%
    session.setAttribute("subMenuSelected", "ach");
    session.setAttribute("BatchID", achID);
    session.setAttribute("Collection", collectionName);
    session.setAttribute("CollectionName", collectionName);
%>
</ffi:cinclude>



