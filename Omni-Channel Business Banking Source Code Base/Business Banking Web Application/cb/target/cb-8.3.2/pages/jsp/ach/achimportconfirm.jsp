<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achimportconfirm" className="moduleHelpClass"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<ffi:setProperty name="actionNameNewReportURL" value="NewReportBaseAction.action?NewReportBase.ReportName=${ReportName}&flag=generateReport" URLEncrypt="true" />
<ffi:setProperty name="actionNameGenerateReportURL" value="GenerateReportBaseAction_display.action?doneCompleteDirection=${doneCompleteDirection}" URLEncrypt="true" />

		<div align="center">
            <table width="750" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="columndata lightBackground">
						<s:form id="achentryuploadconfirmform" name="FormName" namespace="/pages/fileupload" enctype="multipart/form-data" action="ACHFileUploadAction_continueProcess" method="post" theme="simple">
               				 <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
							<input type="hidden" name="processImportTask" value="<ffi:getProperty name="ProcessImportTask"/>">
							 <input type="hidden" name="displayImportErrorsInReport" value="false"/>
                            <ffi:setProperty name="ProcessACHImport" property="ClearACHEntries" value="${CheckACHImport.ClearACHEntries}"/>
                            
                            <div id="importErrorsDiv">
                            			<span class="sectionhead">&gt; <!--L10NStart-->Confirm ACH Import<!--L10NEnd--><br></span><br/><br/>
										<span class="sectionhead"><s:text name="jsp.default_421"/>:<br/><br/></span>
										
										<ffi:cinclude value1="${ImportErrors}" value2="" operator="notEquals">
										
											<s:include value="%{#session.PagesPath}/common/fileImportErrorsGrid.jsp"/>
									
										</ffi:cinclude>
									
										<br/>
										<ffi:cinclude value1="${ImportErrors.operationCanContinue}" value2="false" operator="equals">
											<s:text name="jsp.default.label.fix.errors"/><br/>
										</ffi:cinclude>											
											<div align="center" valign="bottom">
												<ffi:cinclude value1="${CheckACHImport.MultipleBatchImport}" value2="true" operator="equals">
													<sj:a
													id="cancelImport"
													button="true" 
													onClickTopics="closeDialog"
													><s:text name="jsp.default_82"/></sj:a>
												</ffi:cinclude>
												<ffi:cinclude value1="${CheckACHImport.MultipleBatchImport}" value2="true" operator="notEquals">
													<sj:a
													id="cancelImport"
													button="true" 
													onClickTopics="closeDialog"
													><s:text name="jsp.default_82"/></sj:a>
												</ffi:cinclude>
											
												<ffi:cinclude value1="${ImportErrors.operationCanContinue}" value2="false" operator="notEquals">
													<sj:a
													id="continueImport" 
													targets="checkFileImportResultsDialogID"
													formIds="achentryuploadconfirmform"
													button="true" 
													onCompleteTopics="completeConfirmUploadedAchEntryProcess"
													onSuccessTopics="successConfirmUploadedAchEntryProcess"
													onErrorTopics="errorConfirmUploadedAchEntryProcess"
													onBeforeTopics="beforeConfirmUploadedAchEntryProcess"
													><s:text name="jsp.default_111"/></sj:a>
												</ffi:cinclude>
											</div>
										<br/>
                            </div>
                            
                        </s:form>
                    </td>
                </tr>
            </table>
		</div>
