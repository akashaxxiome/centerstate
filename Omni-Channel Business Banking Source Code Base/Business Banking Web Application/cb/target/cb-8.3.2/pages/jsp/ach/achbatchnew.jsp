<%@ page import="java.util.Calendar,
				 com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achbatchnew" className="moduleHelpClass"/>
<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_ach,addSingleACHBatchLink</span>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_tax,addSingleTaxBatchLink</span>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_childsp,addSingleChildspBatchLink</span>
</ffi:cinclude>
<ffi:removeProperty name="SearchString"/>

<%-- load SetACHCompany task --%>
<ffi:object id="SetACHCompany" name="com.ffusion.tasks.ach.SetACHCompany" scope="session"/>
<%-- QTS 636909: remove possible recurring session values Frequency and NumberPayments --%>
<ffi:removeProperty name="Frequency"/>
<ffi:removeProperty name="NumberPayments"/>
<%
	String tempID = null;
	String scope = null;
    boolean isEdit = false;
    session.setAttribute("strutsActionName", "addACHBatch");
%>
<%-- NewFromBatchID - create a new batch to add, using NewFromBatchID as a model
--%>
<%	if (request.getParameter("NewFromBatchID") != null) { %>
	<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.AddACHBatch" scope="session"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.AddACHTaxPayment" scope="session"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.AddChildSupportPayment" scope="session"/>
	</ffi:cinclude>
		<ffi:setProperty name="AddEditACHBatch" property="BatchesName" value='<%=(String)session.getAttribute("Collection")%>' />
		<ffi:setProperty name='AddEditACHBatch' property='BatchID' value='<%=(String)session.getAttribute("NewFromBatchID")%>'/>
		<ffi:removeProperty name="NewFromBatchID"/>
<% } else %>
<%	if (request.getParameter("ID") != null) {
        isEdit = true;
        session.setAttribute("Collection",request.getParameter("Collection"));
        session.setAttribute("BatchID",request.getParameter("ID"));
        session.setAttribute("TransactionIndex",request.getParameter("TransactionIndex"));
        session.setAttribute("strutsActionName", "editACHBatchAction");
%>
	<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.ModifyACHBatch" scope="session"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.ModifyACHTaxPayment" scope="session"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.ModifyChildSupportPayment" scope="session"/>
	</ffi:cinclude>
		<ffi:setProperty name="AddEditACHBatch" property="BatchesName" value='<%=(String)session.getAttribute("Collection")%>' />
		<ffi:setProperty name='AddEditACHBatch' property='BatchID' value='<%=(String)session.getAttribute("BatchID")%>'/>
<% } else if (request.getParameter("TemplateID") != null && (request.getParameter("TemplateID").length() > 0 && !"-1".equals(request.getParameter("TemplateID")))) { 
	tempID = request.getParameter("TemplateID");
%>

<%		String collectionName = "ACHBatchTemplates";
		scope = null;
		collectionName = "ACHBatchTemplates";
		if (tempID.startsWith("USER")) {
			tempID = tempID.substring(4);
			scope = "USER";
		} else
		if (tempID.startsWith("BUSINESS")) {
			tempID = tempID.substring(8);
			scope = "BUSINESS";
		}
%>
	<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.AddACHBatch" scope="session"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.AddACHTaxPayment" scope="session"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.AddChildSupportPayment" scope="session"/>
	</ffi:cinclude>
    <% if (request.getParameter("ReturnToTemplatesPage") != null) { %>
		<ffi:setProperty name='AddEditACHBatch' property='ReturnToTemplatesPage' value='true'/>
    <% } %>
		<ffi:setProperty name='AddEditACHBatch' property='TemplateID' value='<%=tempID%>'/>
		<ffi:setProperty name="AddEditACHBatch" property="TemplatesCollection" value="<%=collectionName%>" />
        <% if (scope != null) { %>
		<ffi:setProperty name="AddEditACHBatch" property="BatchScope" value="<%=scope%>" />
        <% } %>
		<ffi:removeProperty name="TemplateID"/>
<% } else { %>
	<%
		String tempEntToCheck = null;
		int companyCount = 0;
		String companyID = null;
	// if there is only one ACH Company that we are entitled to use, set that ACH Company
	%>

	<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.AddACHBatch" scope="session"/>
		<%
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
		%>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.AddACHTaxPayment" scope="session"/>
		<%
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_PAYMENT_ENTRY );
		%>
	</ffi:cinclude>
	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
		<ffi:object id="AddEditACHBatch" name="com.ffusion.tasks.ach.AddChildSupportPayment" scope="session"/>
		<%
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_PAYMENT_ENTRY );
		%>
	</ffi:cinclude>
	<ffi:list collection="ACHCOMPANIES" items="ACHCompany">

<% boolean include_company = true; %>
	    <ffi:cinclude ifEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCompany.CompanyID}" checkParent="true">
<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
	<ffi:cinclude value1="${ACHCompany.ACHPaymentEntitled}" value2="FALSE">
		<% include_company = false; 		// there are no SEC Codes available for this ACH Company - don't display it %>
	</ffi:cinclude>
</ffi:cinclude>
<%
	if (include_company) {
			if (companyCount == 0)
			{
			%>
				<ffi:getProperty name="ACHCompany" property="CompanyID" assignTo="companyID" />
			<%
			}
			companyCount++;
	}
		%>
		</ffi:cinclude>
	</ffi:list>
	<%
		if (companyCount == 1) {
	%>
		<ffi:setProperty name="AddEditACHBatch" property="CompanyID" value="<%=companyID%>"/>
	<% } %>
<% } %>



<%-- The following code may need to be modified.--%>

	<ffi:setProperty name="AddEditACHBatch" property="Initialize" value="true" />
    <ffi:setProperty name="AddEditACHBatch" property="CreateNoAddFlag" value="true" />
	<ffi:setProperty name="AddEditACHBatch" property="Validate" value="" />
	<ffi:setProperty name="AddEditACHBatch" property="DateFormat" value="${UserLocale.DateFormat}" />
	<ffi:setProperty name="AddEditACHBatch" property="Process" value="FALSE" />
<%	if (request.getParameter("TransactionIndex") != null) { %>
    <ffi:setProperty name="AddEditACHBatch" property="TransactionIndex" value="${TransactionIndex}" />
<% } %>
	<ffi:setProperty name="AddEditACHBatch" property="Frequency" value="None" />	
	<ffi:cinclude value1="${subMenuSelected}" value2="child" >
		<ffi:setProperty name="AddEditACHBatch" property="UseDefaultTaxForm" value="true" />	
	</ffi:cinclude>
	
<%
String instanceType= null;
instanceType=request.getParameter("ScheduledInstance");
if(instanceType != null){
session.setAttribute("InstanceType",instanceType);
}
%>	


<ffi:setProperty name="AddEditACHBatch" property="InstanceType" value="${InstanceType}"/>
<ffi:removeProperty name="InstanceType"/>
<ffi:process name="AddEditACHBatch"/>

<%-- now set TemplateID and BatchScope, in case not stored in template --%>
<% if (tempID != null) { %>
	<ffi:setProperty name='AddEditACHBatch' property='TemplateID' value='<%=tempID%>'/>
<% }
if (scope != null) { %>
	<ffi:setProperty name="AddEditACHBatch" property="BatchScope" value="<%=scope%>" />
<% }
if (isEdit == true) { %>
		<ffi:object id="SetACHCompany" name="com.ffusion.tasks.ach.SetACHCompany" scope="session"/>
		<ffi:setProperty name="SetACHCompany" property="CompanyID" value="${AddEditACHBatch.CompanyID}" />
		<ffi:process name="SetACHCompany" />
		<ffi:setProperty name="ACHCOMPANY" property="CurrentClassCode" value="${AddEditACHBatch.StandardEntryClassCode}" />
<% } %>


<%-- we remove AddEditACHTemplate because achbatchaddedit.jsp is used by template code, too --%>
<ffi:removeProperty name="AddEditACHTemplate" />

<%
    session.setAttribute("FFIAddEditACHBatch", session.getAttribute("AddEditACHBatch"));
%>
<ffi:setProperty name="Initialize" value="true"/>
<div id="achbatchaddedit_div">
    <ffi:include page="/pages/jsp/ach/achbatchaddedit.jsp"/>
</div>
<ffi:removeProperty name="GetDefaultEffectiveDate"/>
