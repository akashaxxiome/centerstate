<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%@ page contentType="text/html; charset=UTF-8" %>
<style type="text/css">
.formPanel {
}

.formPane {
    padding-right: 2em;
    width: 60%;
    float: left;
}

.selectFilePane {
    padding-top: 1em;
    padding-left: 2em;
    width: 30%;
    float: left;
}
</style>


<div id="fileuploadPortlet">
    <div id="fileUploaderPanelID" class="formPanel" align="left">
    	<div id="selectImportFormatOfFileUploadID" class="formPane" align="left">

		</div>
        <div id="selectFileFormAndStatusID" class="selectFilePane">
			<%-- <div id="fileUploadingStatusID" style="display:none;">loading...</div> --%>
			<%-- <div id="fileUploadingStatusID"><a href="javascript:void(0)" onclick="checkFileImportResults();">CheckStatus</a></div> --%>
			<div id="selectFileOFFileUploadFormID"></div>
        </div>
    </div>
	<div id="custommappingPanelID" class="formPanel" align="right">
	        <s:url id="CustomMappingsUrl" value="/pages/jsp/fileuploadcustom.jsp" escapeAmp="false">
				<s:param name="Section" value="%{'ACH'}"/>
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            </s:url>
            <sj:a
                id="customMappingsLink"
                href="%{CustomMappingsUrl}"
                targets="mappingDiv"
                title="Custom Mappings"
                indicator="indicator"
                button="true"
                buttonIcon="ui-icon-disk"
                onClickTopics="beforeCustomMappingsOnClickTopics" 
				onCompleteTopics="customMappingsOnCompleteTopics" 
				onErrorTopics="errorCustomMappingsOnErrorTopics"
            >
                  &nbsp;<s:text name="jsp.ach.custom.mappings" />
            </sj:a>      
	</div>
</div>

    <script>
	$('#fileuploadPortlet').portlet({
		title: 'File Upload ',
		bookmark: true,
		generateDOM: true,
		helpCallback: function(){
			var helpFile = $('#fileuploadPortlet').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		},
		bookMarkCallback: function(){ 
			var settingsArray = ns.shortcut.getShortcutSettingWithPortlet( '#fileuploadPortlet' );
			var path = settingsArray[0];
			var ent  = settingsArray[1];
			ns.shortcut.openShortcutWindow( path, ent );
		}	
	});
    </script>