<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- SESSION CLEANUP --%>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="account" operator="equals">
	<s:include value="%{#session.PathExt}inc/accountSessionCleanup.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="admin" operator="equals">
	<s:include value="%{#session.PathExt}inc/adminSessionCleanup.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="cash" operator="equals">
	<s:include value="%{#session.PathExt}inc/cashSessionCleanup.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="home" operator="equals">
	<s:include value="%{#session.PathExt}inc/homeSessionCleanup.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${SessionCleanupCurrTab}" value2="reports" operator="equals">
	<s:include value="%{#session.PathExt}inc/reportsSessionCleanup.jsp"/>
</ffi:cinclude>

<%
/**
 * Used to clear the 'verified' session variable if user navigates away from the User Profile page after having
 * authentiacated. This will ensure that that user is prompted for authentication each time they try to go back to 
 * the User Profile page after being on any other page.
 */
%> 
<ffi:cinclude value1="${profileSubMenu}" value2="true" operator="equals">
    <ffi:cinclude value1="${subMenuSelected}" value2="user profile" operator="notEquals">	
        <s:include value="%{#session.PathExt}inc/profileSessionCleanup.jsp" />
    </ffi:cinclude>	
</ffi:cinclude>

<ffi:setProperty name="SessionCleanupCurrTab" value="payments"/>
<%-- END OF SESSION CLEANUP --%>


<s:include value="/cb/pages/jsp/inc/init/menu-init.jsp"/>

<table border="0" cellpadding="0" cellspacing="0" width="750">
    <tr>
	<td colspan="3">
	    <table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
		    <td class="logoStyle"><img src="/cb/web/multilang/grafx/hdr_fusion_logo.gif" alt=""></td>
		    <td valign="top" align="right">
			<ffi:getProperty name="top-left-menu" encode="false"/>
		    </td>
		</tr>
	    </table>
	</td>
    </tr>
    <tr>
        <%-- 1st column --%>
        <%-- 2nd column --%>
	<td valign="bottom" width="10" height="35"><img src="/cb/web/multilang/grafx/payments/hdr_left_end_curve.gif" alt="" width="10" height="35"></td>
        <td width="730" height="35"><s:include value="/cb/pages/jsp/ach/inc/nav_menu_top.jsp"/></td>
	<td valign="bottom" width="10" height="35"><img src="/cb/web/multilang/grafx/payments/hdr_right_end_curve.gif" alt="" width="10" height="35"></td>
    </tr>
    <tr>
        <%-- 1st column --%>
        <%-- 2nd column --%>
        <td colspan="3" valign="top" style="width: 750; height: 23;"><s:include value="/cb/pages/jsp/ach/inc/nav_menu_sub.jsp"/></td>
    </tr>
    <tr>
        <%-- 1st column --%>
        <%-- 2nd column --%>
        <td colspan="3" style="width: 750; height: 36;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="3"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" width="0" height="10"></td>
                </tr>
                <tr>
                    <%-- 1st column --%>
		    <td valign="top" width="25"><img src="/cb/web/multilang/grafx/hdr_title_arrow.gif" alt=""></td>
                    <td valign="top" class="pagehead"><ffi:getProperty name="PageHeading" encode="false"/></td>
                    <td align="right" valign="middle" class="columndata"><ffi:getProperty name='PageText' encode="false"/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<ffi:removeProperty name="sub_menu_payments" />
<ffi:removeProperty name="sub_menu_user" />
<ffi:removeProperty name="sub_menu_reports" />
<ffi:removeProperty name="sub_menu_home" />
<ffi:removeProperty name="sub_menu_cash" />
<ffi:removeProperty name="sub_menu_account" />
<br>
