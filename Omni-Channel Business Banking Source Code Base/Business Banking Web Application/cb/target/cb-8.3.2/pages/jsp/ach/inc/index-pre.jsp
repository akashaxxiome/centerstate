<%@ page import="com.ffusion.util.Filterable,
                 com.ffusion.beans.ach.*,
				 com.ffusion.tasks.dualapproval.ach.*,
				 com.ffusion.beans.user.User,
				 com.ffusion.efs.tasks.SessionNames,
				 com.ffusion.beans.user.UserLocale"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<ffi:setL10NProperty name='PageHeading' value='ACH Payments'/>

<%-- Start: Clean up DA information --%>
<ffi:removeProperty name="ACHMultipleCategories"/>
<ffi:removeProperty name="IsPending"/>
<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
<ffi:removeProperty name="<%=IDualApprovalConstants.OLD_DA_OBJECT%>"/>
<%-- End: Clean up DA information --%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<%-- Retrieve category details information in session --%>
	<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails" />
		<ffi:setProperty name="GetDACategoryDetails" property="FetchMutlipleCategories" value="<%=IDualApprovalConstants.TRUE%>" />
		<ffi:setProperty name="GetDACategoryDetails" property="multipleCategoriesSessionName" value="<%=SessionNames.ACH_MULTIPLE_CATEGORIES%>"/>
		<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_ACH_PAYEE%>" />
		<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}" />
	<ffi:process name="GetDACategoryDetails" />
</ffi:cinclude>	
