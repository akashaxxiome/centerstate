<%@ page import="com.ffusion.util.HTMLUtil" %>
<%--
This page is a dialog page for discarding ach payee pending changes.

Pages that request this page
----------------------------
Discard and Cancel button

Pages this page requests
------------------------
DONE button requests achpayee_summary.jsp

Pages included in this page
---------------------------

--%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<ffi:help id="payments_achpayeediscard" className="moduleHelpClass"/>
<ffi:setL10NProperty name='PageHeading' value='Verify Discard ACH Payee' />

<%
	if (request.getParameter("PayeeID") != null) {
		session.setAttribute("PayeeID", request.getParameter("PayeeID"));
	}
	if (request.getParameter("itemId") != null) {
		session.setAttribute("itemId", request.getParameter("itemId"));
	}
	if (request.getParameter("itemType") != null) {
		session.setAttribute("itemType",
				request.getParameter("itemType"));
	}
	if (request.getParameter("Nickname") != null) {
		session.setAttribute("Nickname", request.getParameter("Nickname"));
	}
	if (request.getParameter("Action") != null) {
		session.setAttribute("Action", request.getParameter("Action"));
	}
%>
<ffi:object id="DiscardPendingChangeTask"
	name="com.ffusion.tasks.dualapproval.DiscardPendingChange" />
<ffi:setProperty name="DiscardPendingChangeTask" property="itemType"
	value="ACHPayee" />
<ffi:setProperty name="DiscardPendingChangeTask" property="itemId"
	value="${PayeeID}" />
<ffi:setProperty name="DiscardPendingChangeTask" property="userAction"
	value="${Action}" />
<%
	session.setAttribute("FFICommonDualApprovalTask",
			session.getAttribute("DiscardPendingChangeTask"));
%>

<div align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="columndata ltrow2_color">
				<div align="center">
					<table width="100%" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td width="100%" valign="top">
								<table width="100%" cellpadding="3" cellspacing="0" border="0">
									<tr>
										<td class="sectionsubhead" align="center">
											<s:text name="jsp.default_da.achpayee.discard">
												<s:set var="encNickname" scope="page"><s:property value="#session.Nickname" escapeHtml="true"/></s:set>
												<s:param name="value" value="%{#attr.encNickname}"/>
											</s:text>
										</td>
										<td class="columndata">&nbsp;</td>
									</tr>
								</table></td>
						</tr>
					</table>
				</div></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="columndata ltrow2_color">
				<div align="center">
					<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td class="columndata" colspan="6" nowrap align="center">
								<form method="post">
									<input type="hidden" name="CSRF_TOKEN"
										value="<ffi:getProperty name='CSRF_TOKEN'/>" />
										

									<s:url id="discardURL" value="/pages/dualapproval/dualApprovalAction-discardChanges.action?daAction=Discarded&module=ACHPayee" escapeAmp="false">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
									<s:param name="itemId" value="%{#session.itemId}"></s:param>
									<s:param name="userAction" value="%{#session.userAction}"></s:param>
									</s:url>
									
									<sj:a id="discard" button="true" title="Discard ACH Payee"
										targets="resultmessage"
										href="%{discardURL}"
										onClickTopics="closeDiscardACHPayee"
										onBeforeTopics="beforeDiscardACHPayeeForm"
										onSuccessTopics="successDisardACHPayeeForm"
										effectDuration="1500"><s:text name="jsp.default_467"/>
									</sj:a>									
									<sj:a id="discardCancel" button="true" title="No" onClickTopics="closeDialog"><s:text name="jsp.default_295"/></sj:a>
								</form></td>
						</tr>
					</table>
				</div></td>
		</tr>
	</table>
</div>
