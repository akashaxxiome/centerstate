<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>

<%-- At this point, the batch/template to be viewed should be in the session, called ACHBatch
 Possible flags, ViewCompletedBatch, ReverseACHBatch, DeleteACHBatch, ViewTemplateBatch could be set --%>
<div align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData">
		<tr>
			<td class="columndata lightBackground" align="left">
				<table width="720" border="0" cellspacing="0" cellpadding="3" class="tableData">
					<%-- <tr valign="top">
						<td class="sectionhead nameSubTitle" colspan="6">
						<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
								<!--L10NStart-->ACH Batch<!--L10NEnd-->
						</ffi:cinclude>
						<ffi:cinclude value1="${subMenuSelected}" value2="child" >
								<!--L10NStart-->Child Support Batch<!--L10NEnd-->
						</ffi:cinclude>
						<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
								<!--L10NStart-->Tax Batch<!--L10NEnd-->
						</ffi:cinclude>
						<ffi:cinclude value1="${ViewTemplateBatch}" value2="" operator="notEquals">
								<!--L10NStart-->Template<!--L10NEnd-->
						</ffi:cinclude>
						</td>
					</tr> --%>
					<%-- Start: Dual approval processing --%>
					<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
						<s:if test="%{!#session.ACHBatch.IsCompletedStatus}">
						    <ffi:cinclude value1="${ACHBatch.CoEntryDesc}" value2="REVERSAL" operator="notEquals">
					<tr>
						<td class="columndata" style="color:red;" align="center" colspan="6">
							<b><ffi:getL10NString rsrcFile="cb" msgKey="da.message.ach.NonManagedACHParticipantsInRed"/></b>
						</td>
					</tr>
                                        </ffi:cinclude>
                                    </s:if>
					</ffi:cinclude>
					<%-- End: Dual approval processing--%>

				</table>

			</td>
		</tr>
		<tr>
			<td class="columndata lightBackground" align="left">
                            <s:include value="inc/include-view-achbatch-details.jsp"/>  
			</td>
		</tr>
	</table>
</div>
