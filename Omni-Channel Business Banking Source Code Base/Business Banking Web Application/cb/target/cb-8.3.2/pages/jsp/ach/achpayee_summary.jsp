<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<ffi:removeProperty name="IsManagedParticipants"/>
<ffi:removeProperty name="ManagedGridPayee"/>
<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
	<ffi:setProperty name="canManageAchParticipants" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
	<ffi:setProperty name="canManageAchParticipants" value="false"/>
</ffi:cinclude>


<%-- The following divIDForReloadACHpayees is used to hold variables pending ach payee grid. --%>
<div id="divIDForReloadACHpayees" style="display: none;"></div>
<%-- Pending Approval Users changes island start --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="equals">
	<s:include value="inc/achpayee_pendingapproval-pre.jsp"/>
	<div id="ach_payeesPendingApprovalTab" class="portlet"  role="section" aria-labelledby="achPayeesSummaryHeader">
		<div class="portlet-header">
			<h1 id="achPayeesSummaryHeader" class="portlet-title"><s:text name="jsp.ach.payeessummary"/></h1>
			<span class="portlet-title"><s:text name="jsp.default.label.pending.approval.changes"/></span>
		</div>
	
		<div id="achPayeesPendingApprovalChangeDiv" class="portlet-content">
			<s:include value="/pages/jsp/ach/achpayee_pendingapproval_summary.jsp"/>
		</div>	
		
		<div id="achPendingPayeeSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('ach_payeesPendingApprovalTab')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	</div>
	<br/>
</ffi:cinclude>
<%-- Pending Approval Users changes island end --%>

<div id="ACHPayeeGridTabs" class="portlet gridPannelSupportCls" style="float: left; width: 100%; role="section" aria-labelledby="achPayeesSummaryHeader">
    <div class="portlet-header">
    	<h1 id="achPayeesSummaryHeader" class="portlet-title"><s:text name="jsp.ach.payeessummary"/></h1>
	    <div class="searchHeaderCls">
			<div class="summaryGridTitleHolder" style="margin-left:20px;">
				<span id="selectedGridTab" class="selectedTabLbl">
					<s:text name="jsp.ach.ACH.Payees"/>
				</span>
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
				
				<div class="gridTabDropdownHolder">
					<span class="gridTabDropdownItem" id='ACH' onclick="ns.common.showGridSummary('ACH')" >
						<s:text name="jsp.ach.ACH.Payees"/>
					</span>
					<span class="gridTabDropdownItem" id='IAT' onclick="ns.common.showGridSummary('IAT')">
						<s:text name="jsp.ach.IAT.Payees" />
					</span>
				</div>
			</div>
		</div>
    </div>
    
    <div class="portlet-content">
    	<div class="searchDashboardRenderCls">
			<%-- <s:include value="/pages/jsp/ach/inc/achTemplateSearchCriteria.jsp"/> --%>
		</div>
		<div id="gridContainer" class="summaryGridHolderDivCls">
			<div id="ACHSummaryGrid" gridId="standardACHPayeeGridId" class="gridSummaryContainerCls" >
				<s:include value="/pages/jsp/ach/achpayee_standard_summary.jsp"/>
			</div>
			<div id="IATSummaryGrid" gridId="IATACHPayeeGridId" class="gridSummaryContainerCls hidden" >
				<s:include value="/pages/jsp/ach/achpayee_IAT_summary.jsp"/>
			</div>
		</div>
    </div>
	
	<div id="achPayeeSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('ACHPayeeGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
	
</div>
	
<input type="hidden" id="getTransID" value="<ffi:getProperty name='ACHpayeetabs' property='TransactionID'/>" />
<s:hidden name="ACHPayeeSummaryType" value="PendingACH" id="ACHSummaryTypeID"/>
		
<div id="view_delete_ACHPayee_dialog"></div>
<ffi:removeProperty name="AddEditACHBatch"/>
	
<script>
	
	//Initialize portlet with settings icon
	ns.common.initializePortlet("ach_payeesPendingApprovalTab");
	
	//Initialize portlet with settings icon
	ns.common.initializePortlet("ACHPayeeGridTabs");

    ns.ach.setInputTabText('#normalInputTab');
    ns.ach.setVerifyTabText('#normalVerifyTab');
    ns.ach.setConfirmTabText('#normalConfirmTab');
</script>