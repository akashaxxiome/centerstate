<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<div class="dashboardUiCls">
	<div class="moduleSubmenuItemCls">
		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-ach"></span>
			<span class="moduleSubmenuLbl">
				<s:text name="jsp.default_22" />
			</span>
			<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>

			<!-- dropdown menu include -->    		
			<s:include value="/pages/jsp/home/inc/transferSubmenuDropdown.jsp" />
	</div>
	
	
	<!-- dashboard items listing for ACH -->
    <div id="dbACHSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
			<s:url id="ACHUrl" value="/pages/jsp/ach/achSummaryAction.action" escapeAmp="false">
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            </s:url>
			<sj:a
				id="ACHBatchesLink"
				href="%{ACHUrl}"
				targets="summary" 
				indicator="indicator" 
				button="true"
				cssClass="summaryLabelCls"
				onSuccessTopics="ACHSummaryLoadedSuccess,calendarToSummaryLoadedSuccess"
			><s:text name='ach.batches'/>
			</sj:a>
			
			<s:url id="goBackSummaryUrl" value="/pages/jsp/ach/achSummaryAction.action" escapeAmp="false">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	         </s:url>
		    <sj:a
				id="goBackSummaryLink" 
				href="%{goBackSummaryUrl}" 
				targets="summary" 
				button="true" 
		    	title="Go Back Summary"
		    	cssStyle="display:none"
				onCompleteTopics="calendarToSummaryLoadedSuccess"
				><s:text name="SUMMARY"/>
		    </sj:a>
			
			<ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
				<s:url id="singleACHBatchUrl" value="/pages/jsp/ach/addACHBatch_init.action">
		            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		        </s:url>
		        
			    <sj:a id="addSingleACHBatchLink" href="%{singleACHBatchUrl}" targets="inputDiv" button="true"
			    	title="%{getText('ach.newBatchTT')}"
			    	onClickTopics="beforeLoadACHForm,loadSingleACHBatch" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
				  	<s:text name="ach.newBatch"/>
			    </sj:a>
			    
				<s:url id="multipleACHBatchUrl" value="/pages/jsp/ach/loadMultiACHTemplateAction_initLoad.action">
					<s:param name="loadMultipleBatch">true</s:param>
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				    
			    <!-- button to add multiple ach batch -->
				<sj:a id="addMultipleACHBatchLink" href="%{multipleACHBatchUrl}"
					targets="inputDiv" button="true"
					title="Multiple ACH Batch from Template"
					onClickTopics="beforeLoadACHForm,loadMultipleACHBatch"
					onCompleteTopics="loadACHFormComplete"
					onErrorTopics="errorLoadACHForm">
					<s:text name="ach.newMultipleBatch.fromTemplate"/>
				</sj:a>
			
				<div class="hidden">
					<sj:a id="loadTemplateLink" 
				    	button="true" 
				    	title="%{getText('jsp.default_543')}"
				    	onClick="$('#quicksearchcriteria').hide();$('#loadTemplateId').toggle();ns.ach.getTemplates();"
				    	cssStyle="display:none"
					  	><s:text name="jsp.default_264"/>
				    </sj:a>
				</div>
			</ffi:cinclude>	
			
			<s:url id="viewSingleACHBatchUrl" value="/pages/jsp/ach/achbatch_batchView.jsp" escapeAmp="false">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	        </s:url>
	        <sj:a id="viewSingleACHBatchLink" href="%{viewSingleACHBatchUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
	            cssStyle="display:none;" title="%{getText('ach.viewBatchTT')}"
	            onClickTopics="beforeViewACHBatchLoadForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	            <s:text name='ach.viewBatch'/>
	        </sj:a>
	        
		     <s:url id="deleteSingleACHBatchUrl" value="/pages/jsp/ach/achbatch_batchDelete.jsp" escapeAmp="false">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	        </s:url>
	        <sj:a id="deleteSingleACHBatchLink" href="%{deleteSingleACHBatchUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
	            cssStyle="display:none;" title="%{getText(ach.deleteBatchTT')}"
	            onClickTopics="beforeDeleteACHBatchLoadForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	            <s:text name='ach.deleteBatch'/>
	        </sj:a>		
	</div>
	<!-- dashboard items listing for Templates -->
    <div id="dbTemplateSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
		<s:url id="templatesUrl" value="/pages/jsp/ach/achTemplateSummary.action">
        	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
        </s:url>
		<sj:a
			id="ACHtemplatesLink"
			href="%{templatesUrl}" 
			targets="summary" 
			indicator="indicator" 
			button="true"
			cssClass="summaryLabelCls"
			onSuccessTopics="ACHtemplatesSummaryLoadedSuccess"
		><s:text name='ach.templates'/>
		</sj:a> 
		
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_ACH_TEMPLATE %>"> 
			<s:url id="singleACHBatchTemplateUrl" action="addACHBatchTemplate_init" namespace="/pages/jsp/ach" escapeAmp="false">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="Initialize" value="true"/>
	        </s:url>
           
		   <sj:a id="addSingleTemplateLink" href="%{singleACHBatchTemplateUrl}" targets="inputDiv" button="true"
		    	title="%{getText('ach.addBatchTemplateTT')}"
		    	onClickTopics="beforeLoadACHForm,loadACHSingleBatchTemplate" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
			  	<s:text name="jsp.default.single.template"/>
		    </sj:a>
		
			<s:url id="multipleACHTemplateUrl" value="/pages/jsp/ach/addMultiACHTemplateAction_init.action" escapeAmp="false">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="Initialize" value="true"/>
	        </s:url>
	        
		    <sj:a id="addMultipleTemplateLink" href="%{multipleACHTemplateUrl}" targets="inputDiv" button="true"
		    	title="%{getText('ach.newMultiBatchTT')}"
		    	onClickTopics="beforeLoadACHForm,loadACHBatchTemplate" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
			  	<s:text name="ach.newMultiBatch"/>
		    </sj:a>
		 </ffi:cinclude>
		 
		<s:url id="editMultipleACHTemplateUrl" value="/pages/jsp/ach/editMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	    </s:url>
	    <sj:a id="editMultipleACHTemplateLink" href="%{editMultipleACHTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
	    	 cssStyle="display:none;" title="%{getText(ach.editMultiTemplateTT')}"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name='ach.editMultiTemplate'/>
	    </sj:a>
	    
	   	<s:url id="viewSingleACHTemplateUrl" value="/pages/jsp/ach/accounttransferedit.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="templateEdit" value="true"/>
            <s:param name="SetFundsTran.Name" value="%{'SingleACHTemplateList'}"/>
        </s:url>
        <sj:a id="viewSingleACHTemplateLink" href="%{viewSingleACHTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
             cssStyle="display:none;" title="%{getText('ach.viewTemplateTT')}"
            onClickTopics="beforeViewACHBatchLoadForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
            <s:text name='ach.viewTemplate'/>
        </sj:a>
	    
        <s:url id="deleteSingleACHTemplateUrl" value="/pages/jsp/ach/accounttransferedit.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="templateEdit" value="true"/>
            <s:param name="SetFundsTran.Name" value="%{'SingleACHTemplateList'}"/>
        </s:url>
        <sj:a id="deleteSingleACHTemplateLink" href="%{deleteSingleACHTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
             cssStyle="display:none;" title="%{getText('ach.deleteTemplateTT')}"
            onClickTopics="beforeDeleteACHBatchLoadForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
            <s:text name='ach.deleteTemplate'/>
        </sj:a>
        
	</div>
	<!-- dashboard items listing for Payee -->
    <div id="dbPayeeSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
    	   <s:url id="ACHPayeesUrl" value="/pages/jsp/ach/achpayee_summary.jsp">
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
           </s:url>
           <sj:a
                id="ACHPayeesLink"
                href="%{ACHPayeesUrl}"
                targets="summary"
                indicator="indicator"
                button="true"
                onSuccessTopics="payeesSummaryLoadedSuccess"
                cssClass="summaryLabelCls"
            ><s:text name='ach.payees'/>
            </sj:a>
            
	        <s:url id="addACHPayeeUrl" value="/pages/jsp/ach/addACHPayeeAction_init.action">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	            <s:param name="Action" value="%{#session.Action}"></s:param>
	        </s:url>
	        <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>" >
		        <sj:a id="addACHPayeeLink" href="%{addACHPayeeUrl}" targets="inputDiv" button="true"
		            title="%{getText('ach.newPayeeTT')}"
		            onClickTopics="beforeLoadACHForm,addACHPayeeLoadForm" onCompleteTopics="loadACHFormCompletePayee" onErrorTopics="errorLoadACHForm">
		            <s:text name="ach.newPayee"/>
		        </sj:a>
	        </ffi:cinclude>
	        
		   	<s:url id="editACHPayeeUrl" value="/pages/jsp/ach/editACHPayeeAction_init.action">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	        </s:url>
	        <sj:a id="editACHPayeeLink" href="%{editACHPayeeUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
	            title="%{getText('ach.editPayeeTT')}"
	            cssStyle="display:none;"
	            onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	            &nbsp;<s:text name="ach.editPayee"/>
	        </sj:a>
	</div>
	<!-- dashboard items listing for File Upload -->
    <div id="dbFileUploadSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
    	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_FILE_UPLOAD %>" >
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD %>" >
				<s:url id="ACHUploadUrl" value="/pages/jsp/ach/achupload.jsp">
                	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
                </s:url>
	            <sj:a
	                id="ACHUploadLink"
	                href="%{ACHUploadUrl}"
	                targets="inputDiv"
	                title="%{getText(ach.fileUploadTT')}"
	                indicator="indicator"
	                button="true"
	                cssClass="summaryLabelCls"
	                onClickTopics="beforeUploadACHForm,loadFileUploadACHForm" onCompleteTopics="uploadACHFormComplete" onErrorTopics="errorUploadACHForm"
	            ><s:text name='ach.fileUpload'/>
	            </sj:a>
			</ffi:cinclude>
		</ffi:cinclude>
		
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
				<s:url id="CustomMappingsUrl" value="/pages/jsp/fileuploadcustom.jsp" escapeAmp="false">
					 <s:param name="Section" value="%{'ACH'}"/>
		             <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		        </s:url>
				<sj:a
					id="customMappingsLink"
					href="%{CustomMappingsUrl}"
					targets="mappingDiv"
					title="%{getText('jsp.default_135')}"
					indicator="indicator"
					button="true"
					cssStyle="display:none;"
					onCompleteTopics="achCustomMappingsOnCompleteTopics,closeImportACHFilePageTopics" 
				>
				<s:text name='CUSTOM_MAPPINGS'/>
				</sj:a> 
				<s:url id="addCustomMappingUrl" value="/pages/jsp/custommapping.jsp" escapeAmp="false">
					<s:param name="SetMappingDefinitionID" value="0"/>
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
				</s:url>
				<sj:a
					id="addCustomMappingsLink"
					href="%{addCustomMappingUrl}"
					targets="inputCustomMappingDiv"
					title="%{getText('jsp.default_135')}"
					indicator="indicator"
					button="true"
					cssStyle="display:none;"
					onClickTopics="beforeCustomMappingsOnClickTopics" 
					onCompleteTopics="achAddCustomMappingsOnCompleteTopics" 
					onErrorTopics="errorCustomMappingsOnErrorTopics"
				>
					<s:text name="jsp.default_34" />
				</sj:a> 
			</ffi:cinclude>
		</ffi:cinclude>
	</div>
	
	<!-- More list option listing -->
	<div class="dashboardSubmenuItemCls">
		<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
	
		<!-- UL for rendering tabs -->
		<ul class="dashboardSubmenuItemList">
			<li class="dashboardSubmenuItem"><a id="showdbACHSummary" onclick="ns.common.refreshDashboard('showdbACHSummary',true)"><s:text name="ach.batch" /></a></li>
		
			<li class="dashboardSubmenuItem"><a id="showdbTemplateSummary" onclick="ns.common.refreshDashboard('showdbTemplateSummary',true)"><s:text name="ach.templates"/></a></li>
			
			<li class="dashboardSubmenuItem"><a id="showdbPayeeSummary" onclick="ns.common.refreshDashboard('showdbPayeeSummary',true)"><s:text name="ach.payees" /></a></li>
			
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_FILE_UPLOAD %>" >
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD %>" >
					<li class="dashboardSubmenuItem"><a id="showdbFileUploadSummary" onclick="ns.common.refreshDashboard('showdbFileUploadSummary',true),ns.ach.achFileUpload()"><s:text name="ach.fileUpload" /></a></li>
				</ffi:cinclude>
			</ffi:cinclude>
		</ul>
	</div>
</div>



<%-- <script type="text/javascript">
$(function(){
	$("#viewAllACHId").selectmenu({width: 160});
	$("#viewForACHCompanyId").selectmenu({width: 200});
	$("#viewForACHCompanyId2").selectmenu({width: 200});
	$("#viewForTemplateScopeID").selectmenu({width: 160});
	$("#multiACHTemplateScopeID").selectmenu({width: 160});
	
	var aConfig = {
		startDateBox:$("#StartDateID"),
		endDateBox:$("#EndDateID")
	}
	$("#achDateRangeBox").extdaterangepicker(aConfig);
});

$("#appdashboard-right").hide();
$("#loadTemplateLink").hide();

function openACHBatches(){
	$('#ACHBatchesLink').trigger('click');
	$(".uploadACHMsg").remove();
}

function openACHPayee(){
	$('#ACHPayeesLink').trigger('click');
	$(".uploadACHMsg").remove();
}

function openACHTemplate(){
	$('#ACHtemplatesLink').trigger('click');
	$(".uploadACHMsg").remove();
}

function openACHFileUpload(){
	$('#ACHUploadLink').trigger('click');
	$('#appdashboard-left a').hide();
	$('#appdashboard-left').append("<div class='uploadACHMsg'>Upload ACH Batch File</div>");
}
</script> --%>

<%-- <ul class="dashboardTabsCls">
		<li><a onclick="openACHBatches()" href="#appdashboard-left" class="sapUiIconCls icon-create"><s:text name="ach.batch" /><!--  ACH  Batch --></a>	</li>
	   	 
		<li><a onclick="openACHTemplate()" href="#appdashboard-left" class="sapUiIconCls icon-survey"><s:text name="ach.templates" /><!-- ACH Templates --></a></li>
		
		<li><a onclick="openACHPayee()" href="#appdashboard-left" class="sapUiIconCls icon-person-placeholder"><s:text name="ach.payees" /><!-- ACH Payee --></a></li>
		
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_FILE_UPLOAD %>" >
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD %>" >
				<s:url id="ACHUploadUrl" value="/pages/jsp/ach/achupload.jsp">
                	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
                </s:url>
				<li><a onclick="openACHFileUpload()" href="#appdashboard-left" class="sapUiIconCls icon-home-share"><s:text name="ach.fileUpload" /><!-- ACH File Upload --></a></li>
			</ffi:cinclude>
		</ffi:cinclude>
</ul> --%>
<%-- 
    <div id="appdashboard-left" class="formLayout">
    
		<sj:a 
			id="quickSearchLink"
			button="true" 
			onClick="$('#loadTemplateId').hide();$('#quicksearchcriteria').toggle();"
		><span class="sapUiIconCls icon-search"></span><s:text name="jsp.default_4"/>
		</sj:a>
		
		
         <s:url id="goBackSummaryUrl" value="/pages/jsp/ach/achSummaryAction.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
         </s:url>
	    <sj:a
			id="goBackSummaryLink" 
			href="%{goBackSummaryUrl}" 
			targets="summary" 
			button="true" 
			buttonIcon="ui-icon-transfer-e-w" 
	    	title="Go Back Summary"
			onCompleteTopics="calendarToSummaryLoadedSuccess"
			><s:text name="SUMMARY"/>
	    </sj:a>
		
	    <s:url id="singleACHBatchUrl" value="/pages/jsp/ach/addACHBatch_init.action">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
        </s:url>
        
        <s:url id="multipleACHBatchUrl" value="/pages/jsp/ach/loadMultiACHTemplateAction_initLoad.action">
			<s:param name="loadMultipleBatch">true</s:param>
		</s:url>

<ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
	    <sj:a id="addSingleACHBatchLink" href="%{singleACHBatchUrl}" targets="inputDiv" button="true"
	    	title="%{getText('ach.newBatchTT')}"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
		  	<span class="sapUiIconCls icon-add"></span><s:text name="ach.newBatch"/>
	    </sj:a>
	    
	    <!-- button to add multiple ach batch -->

		<sj:a id="addMultipleACHBatchLink" href="%{multipleACHBatchUrl}"
			targets="inputDiv" button="true"
			title="Multiple ACH Batch from Template"
			onClickTopics="beforeLoadACHForm"
			onCompleteTopics="loadACHFormComplete"
			onErrorTopics="errorLoadACHForm">
			<span class="sapUiIconCls icon-add-activity-2"></span><s:text name="ach.newMultipleBatch.fromTemplate"/>

		</sj:a>

<div class="hidden">
		<sj:a id="loadTemplateLink" 
	    	button="true" 
	    	title="%{getText('jsp.default_543')}"
	    	onClick="$('#quicksearchcriteria').hide();$('#loadTemplateId').toggle();ns.ach.getTemplates();"
	    	cssStyle="display:none"
		  	><span class="sapUiIconCls icon-add-activity-2"></span><s:text name="jsp.default_264"/>
	    </sj:a>
</div>
</ffi:cinclude>
        <s:url id="addACHPayeeUrl" value="/pages/jsp/ach/addACHPayeeAction_init.action">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="Action" value="%{#session.Action}"></s:param>
            </s:url>
        <ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>" >
        <sj:a id="addACHPayeeLink" href="%{addACHPayeeUrl}" targets="inputDiv" button="true"
            title="%{getText('ach.newPayeeTT')}"
            onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormCompletePayee" onErrorTopics="errorLoadACHForm">
            <span class="sapUiIconCls icon-add-contact"></span><s:text name="ach.newPayee"/>
        </sj:a>
        </ffi:cinclude>

        <s:url id="editACHPayeeUrl" value="/pages/jsp/ach/editACHPayeeAction_init.action">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            </s:url>
        <sj:a id="editACHPayeeLink" href="%{editACHPayeeUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
            title="%{getText('ach.editPayeeTT')}"
            cssStyle="display:none;"
            onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
            &nbsp;<s:text name="ach.editPayee"/>
        </sj:a>

	    <s:url id="singleACHBatchTemplateUrl" action="addACHBatchTemplate_init" namespace="/pages/jsp/ach" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
            </s:url>
        <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_ACH_TEMPLATE %>">    
		   <sj:a id="addSingleTemplateLink" href="%{singleACHBatchTemplateUrl}" targets="inputDiv" button="true"
		    	title="%{getText('ach.addBatchTemplateTT')}"
		    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
			  	<span class="sapUiIconCls icon-add"></span><s:text name="NEW_TEMPLATE"/>
		    </sj:a>
		</ffi:cinclude>
	    <s:url id="multipleACHTemplateUrl" value="/pages/jsp/ach/addMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
        </s:url>
        <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ENT_ACH_TEMPLATE %>">    
		    <sj:a id="addMultipleTemplateLink" href="%{multipleACHTemplateUrl}" targets="inputDiv" button="true"
		    	title="%{getText('ach.newMultiBatchTT')}"
		    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
			  	<span class="sapUiIconCls icon-add-activity-2"></span><s:text name="ach.newMultiBatch"/>
		    </sj:a>
		</ffi:cinclude>
		
		edit multiple template
	    <s:url id="editMultipleACHTemplateUrl" value="/pages/jsp/ach/editMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	    </s:url>
	    <sj:a id="editMultipleACHTemplateLink" href="%{editMultipleACHTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
	    	 cssStyle="display:none;" title="%{getText(ach.editMultiTemplateTT')}"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name='ach.editMultiTemplate'/>
	    </sj:a>
	    

        View single ACH Batch
        <s:url id="viewSingleACHBatchUrl" value="/pages/jsp/ach/achbatch_batchView.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
        </s:url>
        <sj:a id="viewSingleACHBatchLink" href="%{viewSingleACHBatchUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
            cssStyle="display:none;" title="%{getText('ach.viewBatchTT')}"
            onClickTopics="beforeViewACHBatchLoadForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
            <s:text name='ach.viewBatch'/>
        </sj:a>

        View single ACH Batch template
        <s:url id="viewSingleACHTemplateUrl" value="/pages/jsp/ach/accounttransferedit.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="templateEdit" value="true"/>
            <s:param name="SetFundsTran.Name" value="%{'SingleACHTemplateList'}"/>
        </s:url>
        <sj:a id="viewSingleACHTemplateLink" href="%{viewSingleACHTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
             cssStyle="display:none;" title="%{getText('ach.viewTemplateTT')}"
            onClickTopics="beforeViewACHBatchLoadForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
            <s:text name='ach.viewTemplate'/>
        </sj:a>

        Delete single ACH Batch
        <s:url id="deleteSingleACHBatchUrl" value="/pages/jsp/ach/achbatch_batchDelete.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
        </s:url>
        <sj:a id="deleteSingleACHBatchLink" href="%{deleteSingleACHBatchUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
            cssStyle="display:none;" title="%{getText(ach.deleteBatchTT')}"
            onClickTopics="beforeDeleteACHBatchLoadForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
            <s:text name='ach.deleteBatch'/>
        </sj:a>

        De;ete single ACH Batch template
        <s:url id="deleteSingleACHTemplateUrl" value="/pages/jsp/ach/accounttransferedit.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            <s:param name="templateEdit" value="true"/>
            <s:param name="SetFundsTran.Name" value="%{'SingleACHTemplateList'}"/>
        </s:url>
        <sj:a id="deleteSingleACHTemplateLink" href="%{deleteSingleACHTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
             cssStyle="display:none;" title="%{getText('ach.deleteTemplateTT')}"
            onClickTopics="beforeDeleteACHBatchLoadForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
            <s:text name='ach.deleteTemplate'/>
        </sj:a>
		
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD_ADMIN %>">
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CUSTOM_FILE_UPLOAD %>">
				<s:url id="CustomMappingsUrl" value="/pages/jsp/fileuploadcustom.jsp" escapeAmp="false">
					 <s:param name="Section" value="%{'ACH'}"/>
		             <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		        </s:url>
				<sj:a
					id="customMappingsLink"
					href="%{CustomMappingsUrl}"
					targets="mappingDiv"
					title="%{getText('jsp.default_135')}"
					indicator="indicator"
					button="true"
					buttonIcon="ui-icon-disk"
					onCompleteTopics="customMappingsOnCompleteTopics,closeImportACHFilePageTopics" 
				>
				<s:text name='CUSTOM_MAPPINGS'/>
				</sj:a> 
				<s:url id="addCustomMappingUrl" value="/pages/jsp/custommapping.jsp" escapeAmp="false">
					<s:param name="SetMappingDefinitionID" value="0"/>
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
				</s:url>
				<sj:a
					id="addCustomMappingsLink"
					href="%{addCustomMappingUrl}"
					targets="inputCustomMappingDiv"
					title="%{getText('jsp.default_135')}"
					indicator="indicator"
					button="true"
					buttonIcon="ui-icon-plus"
					onClickTopics="beforeCustomMappingsOnClickTopics" 
					onCompleteTopics="addCustomMappingsOnCompleteTopics" 
					onErrorTopics="errorCustomMappingsOnErrorTopics"
					style="display:none;"
				>
					<s:text name="jsp.default_34" />
				</sj:a> 
			</ffi:cinclude>
		</ffi:cinclude>
    </div>
     --%>
    
   <%--  
    <div id="appdashboard-right" >
    
  
    	<div style="float:right;">		
            <s:url id="ACHPayeesUrl" value="/pages/jsp/ach/achpayee_summary.jsp">
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
                </s:url>
           <sj:a
                id="ACHPayeesLink"
                href="%{ACHPayeesUrl}"
                targets="summary"
                indicator="indicator"
                button="true"
                buttonIcon="ui-icon-copy"
                onSuccessTopics="payeesSummaryLoadedSuccess"
                cssStyle="display:none"
            >
                  &nbsp;<s:text name='ach.payees'/>
   
            </sj:a>

		    <s:url id="ACHUrl" value="/pages/jsp/ach/achSummaryAction.action" escapeAmp="false">
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
                </s:url>
			<sj:a
				id="ACHBatchesLink"
				href="%{ACHUrl}"
				targets="summary" 
				indicator="indicator" 
				button="true" 
				buttonIcon="ui-icon-pencil"
				onSuccessTopics="ACHSummaryLoadedSuccess"
				cssStyle="display:none"
			>
			  	&nbsp;<s:text name='ach.batches'/>
			</sj:a>
			
			<s:url id="templatesUrl" value="/pages/jsp/ach/ach_templates_summary.jsp">
                <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
                </s:url>
			<sj:a
				id="ACHtemplatesLink"
				href="%{templatesUrl}" 
				targets="summary" 
				indicator="indicator" 
				button="true" 
				buttonIcon="ui-icon-copy"
				onSuccessTopics="ACHtemplatesSummaryLoadedSuccess"
				cssStyle="display:none"
			>
			  	&nbsp;<s:text name='ach.templates'/>
			</sj:a>

			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_FILE_UPLOAD %>" >
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD %>" >
				
					<s:url id="ACHUploadUrl" value="/pages/jsp/ach/achupload.jsp">
	                	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	                </s:url>
		            <sj:a
		                id="ACHUploadLink"
		                href="%{ACHUploadUrl}"
		                targets="inputDiv"
		                title="%{getText(ach.fileUploadTT')}"
		                indicator="indicator"
		                button="true"
		                buttonIcon="ui-icon-disk"
		                onClickTopics="beforeUploadACHForm" onCompleteTopics="uploadACHFormComplete" onErrorTopics="errorUploadACHForm"
		                cssStyle="display:none"
		            >
		               &nbsp;<s:text name='ach.fileUpload'/>
		            </sj:a>

				</ffi:cinclude>
			</ffi:cinclude>

		</div>
    </div>
	 --%>
	<div class="selectBoxHolder">
		<div id="loadTemplateId"  style="display:none;">
    	</div>
    </div>
		
	<%-- Following three dialogs are used for view, delete and inquiry ACH Batch --%>
	
    <sj:dialog id="inquiryACHBatchDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.batchInquiryDialog')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
    </sj:dialog> 

    <sj:dialog id="viewACHPayeeDetailsDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.payeeDetailDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
	
	<sj:dialog id="viewACHBatchDetailsDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.batchDetailDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="500" cssStyle="overflow:hidden;">
	</sj:dialog>
	
    <sj:dialog id="viewACHEntryErrorDetailsDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.entryErrorDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
    </sj:dialog>

	<sj:dialog id="viewACHMultiBatchDetailsDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.multiBatchTemplateDetailDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
		
    <sj:dialog id="editPendingACHBatchDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.editBatchConfirmDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

    <sj:dialog id="deleteACHBatchDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.deleteBatchConfirmDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="500" cssStyle="overflow:hidden;">
	</sj:dialog>
    
    <sj:dialog id="deleteACHPayeeDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.deletePayeeConfirmDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

	<sj:dialog id="modifyACHPayeeDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.editPayeeConfirmDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>

	<sj:dialog id="discardACHPayeeDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.discardPayeeConfirmDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
	</sj:dialog>
	
	<sj:dialog id="deleteACHTemplateDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.deleteTemplateConfirmDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

    <sj:dialog id="setAllAmountsDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.setAllAmountsDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
    </sj:dialog>

    <sj:dialog id="importEntryAddendaDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.importAddendaDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
    </sj:dialog>

    <sj:dialog id="addEditACHEntryDialogID" cssClass="pmtTran_achDialog"  title="%{getText('ach.addEditEntryDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
    </sj:dialog>
     
    <sj:dialog id="deleteMultiACHTemplateDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.deleteMultiTemplateConfirmDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>
	
	<sj:dialog id="deleteSingleACHTemplatesDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.deleteBatchTemplateConfirmDialog')}" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500">
	</sj:dialog>
	
	<sj:dialog id="setACHBatchTypeDialogID" cssClass="pmtTran_childspDialog" title="%{getText('ach.changeBatchType.header')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="700">
    </sj:dialog>
	
	<sj:dialog id="saveAsACHTemplateDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.templatesDialog')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
    </sj:dialog>
    
    <sj:dialog id="setAllAmountDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.setAllAmountDialog')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350"  height="auto">
    </sj:dialog>
    
    <sj:dialog id="ACHsearchDestinationBankDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.bankLookupResultsDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="auto">
	</sj:dialog>
	
	<sj:dialog id="skipACHBatchDialogID" cssClass="pmtTran_achDialog" title="%{getText('ach.skipBatchConfirmDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="500" cssStyle="overflow:hidden;">
	</sj:dialog>

<script>
	
	$('#importEntryAddendaDialogID').addHelp(function(){
		var helpFile = $('#importEntryAddendaDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
</script>

<script>
	$("#ACHBatchesLink").find("span").addClass("dashboardSelectedItem");
</script>