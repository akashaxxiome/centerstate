<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_stateTaxTypeSummary" className="moduleHelpClass"/>

	<ffi:setGridURL grid="GRID_state" name="DeleteURL" url="/cb/pages/jsp/ach/taxformdelete.jsp?ID={0}&isBusiness={1}&type=state" parm0="ID" parm1="isBusiness" />

	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getChildTaxTypesAction.action?formType=STATE&GridURLs=GRID_state" URLEncrypt="true"/>
    <s:url id="taxTypeUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
		id="taxStateTypesGridId"
		caption=""  
		sortable="true"  
		dataType="json"  
		href="%{taxTypeUrl}"
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,taxTypeGridCompleteEvents"
		> 
		
        <sjg:gridColumn name="state" width="6" index="state" title="%{getText('jsp.default_386')}" sortable="true" cssClass="datagrid_textColumn"/>
        <sjg:gridColumn name="name" index="name" title="%{getText('jsp.label.taxes')}" sortable="false" search="true" formatter="ns.tax.formatTaxStateTypeNameLinks" cssClass="datagrid_textColumn"/>
        
        <sjg:gridColumn name="ID" index="action" title="Action" sortable="false" search="false" formatter="ns.tax.formatTaxTypeActionLinks" width="10" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
        <sjg:gridColumn name="map.BusinessID" width="100" index="businessID" title="BusinessID" search="false" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
        
	</sjg:grid>
	
	<script type="text/javascript">
	<!--
		$("#taxStateTypesGridId").data("supportSearch",true);		
		$("#taxStateTypesGridId").jqGrid('setColProp','ID',{title:false}); 
	//-->
	</script>	
