<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<div id="quicksearchcriteria" class="quickSearchAreaCls">
	<s:form action="/pages/jsp/ach/achSummaryAction_verifyACH.action" method="post" id="QuickSearchACHFormID" name="QuickSearchTransfersForm" theme="simple">
		<div id="achsearchcriteria" style="display:block; margin-left:10px;" class="marginTop10">
			<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
			
			<!-- quick search items holder -->
			<div class="acntDashboard_masterHolder">
			
				<!-- Date range picker -->
				<div class="acntDashboard_itemHolder">
					<span class="dashboardLabelMargin" style="display:block;"><s:text name="jsp.default.label.date.range"/></span>
					<input type="text" id="achDateRangeBox" />
		
					<input type="hidden" value="<s:property value='achBatchSearchCriteria.startDate'/>" id="StartDateID" name="achBatchSearchCriteria.startDate" />
					<input type="hidden" value="<s:property value='achBatchSearchCriteria.endDate'/>" id="EndDateID" name="achBatchSearchCriteria.endDate" />
				</div>
				
				<!-- drop down holder -->
				<div class="acntDashboard_itemHolder">
					<%-- <label for="">&nbsp;<s:text name='ach.showQS'/></label> --%>
					<span class="dashboardLabelMargin" style="display:block;"><s:text name="ach.showQS"/></span>
					<select id="viewAllACHId" name="achBatchSearchCriteria.viewAll" class="txtbox"  >
						<option value="false"><s:text name="ach.viewMyACH"/></option>
						<s:if test="%{!consumerUser}">	
						<option value="true"><s:property value="groupOption"/></option>
						</s:if>
					</select>	
					
					<%-- Select box for filtered company list --%>
					<s:select
					id="viewForACHCompanyId"
					cssClass="txtbox"
					headerKey="" headerValue="%{getText('ach.allCompanyIDQS')}"
					list="companyList" name="achBatchSearchCriteria.companyID"
					listKey="companyID" listValue="%{companyName+'('+companyID+')'}"/> 
		
				</div>
				
		        <!-- search button -->
		        <div class="acntDashboard_itemHolder">
		            <span class="dashboardLabelMargin" style="display:block;">&nbsp;</span>
					
					<sj:a targets="quick" 
						id="quicksearchbutton" 
						formIds="QuickSearchACHFormID" 
						button="true" 
		                validate="true" 
		                validateFunction="customValidation"
						onclick="removeValidationErrors();"
						onCompleteTopics="quickSearchACHComplete"
						><s:text name="jsp.default_6" />
					</sj:a>
		   		</div>				
			</div>		
			<span id="startDateError"></span>
			<span id="endDateError"></span>
			<span id="dateRangeValueError"></span>
		<ffi:removeProperty name="GetDatesFromDateRange"/>
		</div>
	</s:form>
</div>
<script type="text/javascript">
	$(function(){
		$("#viewAllACHId").selectmenu({width: 160});
		$("#viewForACHCompanyId").selectmenu({width: 200});
		
		var aConfig = {
			startDateBox:$("#StartDateID"),
			endDateBox:$("#EndDateID")
		};
		$("#achDateRangeBox").extdaterangepicker(aConfig);
	});
</script>