<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achpayeedelete" className="moduleHelpClass"/>
<ffi:setL10NProperty name='PageHeading' value='Delete ACH Participant'/>


<div align="center" class="deletePayee achPayeeDialogHt">
<div style="width:100%; position:relative; overflow:hiddne">
<ffi:include page="/pages/jsp/ach/inc/payee_view.jsp" />
</div>
<div class="clearBoth"></div>
<div class="marginTop10"></div>
<ffi:cinclude value1="${AddEditACHPayee.TemplateUseList.Size}" value2="0" operator="notEquals">
<div class="paneWrapper">
<div class="paneInnerWrapper">
	   	<div class="header"><s:text name="jsp.ach.Participant.Reference.Summary" /></div>
	   <div class="paneContentWrapper">
   			<div><s:text name="jsp.ach.Participant.delete.msg" /></div>
			<ul>
				<ffi:list collection="AddEditACHPayee.TemplateUseList" items="list">
				        <li><ffi:getProperty name="list"/></li>
				</ffi:list>
			</ul>
	  </div>
</div>
</div>
</ffi:cinclude>
<div style="color:red" class="marginTop10"><s:text name="jsp.ach.delete.participant.msg"/></div>
</div>
