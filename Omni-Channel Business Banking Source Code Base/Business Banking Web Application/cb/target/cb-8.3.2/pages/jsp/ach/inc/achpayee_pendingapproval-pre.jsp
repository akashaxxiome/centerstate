<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="anyPendingDAPayees" value="true"/>
<s:action namespace="/pages/jsp/ach" name="viewACHPayeeAction_anyPendingDAPayees" />
<% if ("true".equals(request.getAttribute("anyPendingDAPayees"))) {
// NOTE: This file is used by ach_grid.js to know if there are any pending ACH DA Payees
// so that the pending grid can be shown.
%>
    <script> ns.ach.pendingApprovalPayees = "1"; $('#ach_payeesPendingApprovalTab').removeAttr('style');</script>
<% } else {%>
    <script> ns.ach.pendingApprovalPayees = "0"; $('#ach_payeesPendingApprovalTab').attr('style','display:none');</script>
<% } %>
