<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

	<script type="text/javascript" src="<s:url value='/web/js/ach/ach%{#session.minVersion}.js'/>"></script>
    <script type="text/javascript" src="<s:url value='/web/js/ach/ach_grid%{#session.minVersion}.js'/>"></script>
    <script type="text/javascript" src="<s:url value='/web/js/ach/tax%{#session.minVersion}.js'/>"></script>
    <script type="text/javascript" src="<s:url value='/web/js/ach/tax_grid%{#session.minVersion}.js'/>"></script>

    <ffi:setProperty name="subMenuSelected" value="tax"/>
	<s:set var="ACHType" value="%{@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_TAX}" scope="session"/>
	<ffi:setProperty name="heading" value="Tax"/>
<%
    String type = (String)session.getAttribute("subMenuSelected");
    String entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
    if ("ach".equals(type)) {
        entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
    } else if ("tax".equals(type)) {
        entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_PAYMENT_ENTRY );
    } else if ("child".equals(type)) {
        entitlementName = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_PAYMENT_ENTRY );
    }
    session.setAttribute( "tempEntToCheck", entitlementName );

%>
	<s:include value="%{#session.PagesPath}/ach/inc/index-pre.jsp"/>

        <div id="desktop" align="center">

            <div id="operationresult">
                <div id="resultmessage">Operation Result</div>
            </div><!-- result -->

            <div id="appdashboard">
               <s:action namespace="/pages/jsp/ach" name="taxAchDashBoardAction" executeResult="true"/>

            </div>

			<div id="details" style="display:none;">
				<s:include value="/pages/jsp/ach/ach_details.jsp"/>
			</div>

			<div id="summary">
				 <ffi:cinclude value1="${dashboardElementId}" value2=""	operator="equals">
					<ffi:cinclude value1="${summaryToLoad}" value2="TaxBatch"	operator="equals">
						<s:action namespace="/pages/jsp/ach" name="achSummaryAction" executeResult="true">
							 <s:param name="module" value="#session.subMenuSelected"/>
						</s:action>
					</ffi:cinclude>
					<ffi:cinclude value1="${summaryToLoad}" value2="TaxBatchTemplate"	operator="equals">
						<s:include value="/pages/jsp/ach/ach_templates_summary.jsp"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${summaryToLoad}" value2="TaxType"	operator="equals">
							<s:include value="/pages/jsp/ach/tax/taxtype_summary.jsp"/>
					</ffi:cinclude>
				</ffi:cinclude>
			</div>

    </div>
	<script type="text/javascript">
	$(document).ready(function(){
		ns.tax.taxDashboard("<s:property value='#parameters.summaryToLoad' />","<s:property value='#parameters.dashboardElementId' />");
	});
	</script>
