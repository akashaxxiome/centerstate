<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>


<div class="dashboardUiCls">
	<div class="moduleSubmenuItemCls">
		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-childSupport"></span>
			<span class="moduleSubmenuLbl">
				<s:text name="jsp.default_97" />
			</span>
			<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>

			<!-- dropdown menu include -->    		
			<s:include value="/pages/jsp/home/inc/transferSubmenuDropdown.jsp" />
	</div>
	
	<!-- dashboard items listing for Child Support -->
    <div id="dbACHSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
		<s:url id="ChildspUrl" value="/pages/jsp/ach/achSummaryAction.action" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a
				id="ChildspBatchesLink"
				href="%{ChildspUrl}"
				targets="summary"
				indicator="indicator"
				button="true"
				cssClass="summaryLabelCls"
				onSuccessTopics="childspSummaryLoadedSuccess,calendarToSummaryLoadedSuccess"
			><s:text name="jsp.default_400" />
		</sj:a>
		
        <s:url id="goBackSummaryUrl" value="/pages/jsp/ach/childsp/childsp_summary.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
        </s:url>
	    <sj:a
			id="goBackSummaryLink"
			href="%{goBackSummaryUrl}"
			targets="summary"
			button="true"
	    	title="Go Back Summary"
	    	cssStyle="display:none"
			onCompleteTopics="calendarToSummaryLoadedSuccess"
			><s:text name="jsp.default_97" />
	    </sj:a>
	    
		<ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
		    <s:url id="singleChildspBatchUrl" value="/pages/jsp/ach/addChildSupportBatch_init.action">
		           <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="Initialize" value="true"/>
		       </s:url>
		    
		    <sj:a id="addSingleChildspBatchLink" href="%{singleChildspBatchUrl}" targets="inputDiv" button="true"
		    	title="Add Single Child Support Batch"
		    	onClickTopics="beforeLoadACHForm,loadSingleChildspBatch" onCompleteTopics="loadACHFormComplete,showWizardFormOnComplete" onErrorTopics="errorLoadACHForm">
			  	<s:text name="jsp.default.single.batch" />
		    </sj:a>
		    
		    <s:url id="multipleACHBatchUrl" value="/pages/jsp/ach/loadMultiACHTemplateAction_initLoad.action">
				<s:param name="loadMultipleBatch">true</s:param>
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
		    <sj:a id="addMultipleChildSupportBatchLink" href="%{multipleACHBatchUrl}"
				targets="inputDiv" button="true"
				title="Multiple ACH Batch from Template"
				onClickTopics="beforeLoadACHForm,loadMultipleChildspBatch"
				onCompleteTopics="loadACHFormComplete,showWizardFormOnComplete"
				onErrorTopics="errorLoadACHForm">
				<s:text name="ach.newMultipleBatch.fromTemplate"/>
		
			</sj:a>
			<div class="hidden">
			    <sj:a id="loadTemplateLink"
			    	button="true"
			    	title="Load Template"
			    	onClick="$('#quicksearchcriteria').hide();$('#loadTemplateId').toggle();ns.ach.getTemplates();"
				  	><s:text name="jsp.default_264" />
			    </sj:a>
		    </div>
		</ffi:cinclude>		    
		
		<s:url id="editSingleChildspBatchUrl" value="/pages/jsp/ach/achbatchaddedit.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
	    <sj:a id="editSingleBatchLink" href="%{editSingleChildspBatchUrl}" targets="inputDiv" button="true" 
	    	cssStyle="display:none;" title="Edit Child support Batch"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name="jsp.default_594" />
	    </sj:a>	
	
	</div>
	<!-- dashboard items listing for Templates -->
    <div id="dbTemplateSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
		<s:url id="templatesUrl" value="/pages/jsp/ach/achTemplateSummary.action" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a
			id="ChildsptemplatesLink"
			href="%{templatesUrl}"
			targets="summary"
			indicator="indicator"
			button="true"
			cssClass="summaryLabelCls"
			onSuccessTopics="childspTemplatesSummaryLoadedSuccess"
		><s:text name="jsp.default_5" />
		</sj:a>  
		
	    <s:url id="singleChildspBatchTemplateUrl" action="addChildSupportTemplate_init" namespace="/pages/jsp/ach" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
            </s:url>
	    <sj:a id="addSingleTemplateLink" href="%{singleChildspBatchTemplateUrl}" targets="inputDiv" button="true" 
	    	title="Add Single Child Support Template"
	    	onClickTopics="beforeLoadACHForm,loadACHSingleBatchTemplate" onCompleteTopics="loadACHFormComplete,showWizardFormOnComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="jsp.default.single.template" />
	    </sj:a>

	    <s:url id="multipleChildspTemplateUrl" value="/pages/jsp/ach/addMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
        </s:url>
	    <sj:a id="addMultipleTemplateLink" href="%{multipleChildspTemplateUrl}" targets="inputDiv" button="true" 
	    	title="Add Multiple Batch Template"
	    	onClickTopics="beforeLoadACHForm,loadACHBatchTemplate" onCompleteTopics="loadACHFormComplete,showWizardFormOnComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="ach.newMultiBatch" />
	    </sj:a>
	    
	    	    <s:url id="editSingleChildspTemplateUrl" value="/pages/jsp/ach/achbatchaddedit.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	    </s:url>
	    <sj:a id="editSingleTemplateLink" href="%{editSingleChildspTemplateUrl}" targets="inputDiv" button="true" 
	    	 cssStyle="display:none;" title="Edit Single Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name="ach.editTemplate" />
	    </sj:a>
	    
	    <s:url id="editMultipleChildspTemplateUrl" value="/pages/jsp/ach/editMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	    </s:url>
	    <sj:a id="editMultipleACHTemplateLink" href="%{editMultipleChildspTemplateUrl}" targets="inputDiv" button="true" 
	    	 cssStyle="display:none;" title="Edit Multiple Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name="ach.editMultiTemplate" />
	    </sj:a>		  
	</div>
	<!-- dashboard items listing for Payee -->
    <div id="dbPayeeSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
		<s:url id="ChildspTypesUrl" value="/pages/jsp/ach/childsp/childsptype_summary.jsp" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a
			id="ChildspPayeesLink"
			href="%{ChildspTypesUrl}"
			targets="summary"
			indicator="indicator"
			button="true"
			cssClass="summaryLabelCls"
			onSuccessTopics="childspTypesSummaryLoadedSuccess"
		><s:text name="jsp.default_595" />
		</sj:a>   
			
		<s:url id="addChildspPayeeUrl" value="/pages/jsp/ach/taxformadd.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            </s:url>
        <sj:a id="addChildspPayeeLink" href="%{addChildspPayeeUrl}" targets="inputDiv" button="true" 
            title="New Child Support Type"
            onClickTopics="beforeLoadACHForm,beforeAddChildspPayee" onCompleteTopics="loadACHFormComplete,hideWizardFormOnComplete" onErrorTopics="errorLoadACHForm">
            <s:text name="ach.childsp.add_type" />
        </sj:a> 
	</div>
	
	<!-- Summary link added to fix issue in IE -->
      <div id="dbFileUploadSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
            <s:url id="addAchFileImporUrl" value="/pages/jsp/ach/achimport.jsp" escapeAmp="false">
	            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
             <sj:a id="fileUploadSummary" cssClass="summaryLabelCls"  button="true" title="File Import">
            	 <s:text name="jsp.billpay_57" /><!-- File Import -->
      		 </sj:a>
      </div>
      
	<!-- More list option listing -->
	<div class="dashboardSubmenuItemCls">
		<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
	
		<!-- UL for rendering tabs -->
		<ul class="dashboardSubmenuItemList">
			<li class="dashboardSubmenuItem"><a id="showdbACHSummary" onclick="ns.common.refreshDashboard('showdbACHSummary',true)"><s:text name="ach.batch" /></a></li>
		
			<li class="dashboardSubmenuItem"><a id="showdbTemplateSummary" onclick="ns.common.refreshDashboard('showdbTemplateSummary',true)"><s:text name="ach.templates"/></a></li>
			
			<li class="dashboardSubmenuItem"><a id="showdbPayeeSummary" onclick="ns.common.refreshDashboard('showdbPayeeSummary',true)"><s:text name="jsp.default_595" /></a></li>
		</ul>
	</div>
</div>

<div class="selectBoxHolder">
	<div id="loadTemplateId"  style="display:none;">
	</div>
</div>

<%-- <div id="appdashboard-left" class="formLayout">
		<sj:a
			id="quickSearchLink"
			button="true"
			buttonIcon="ui-icon-search"
			onClick="$('#loadTemplateId').hide(); $('#quicksearchcriteria').toggle();"
		><s:text name="jsp.default_4" />
		</sj:a>

        <s:url id="goBackSummaryUrl" value="/pages/jsp/ach/childsp/childsp_summary.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
        </s:url>
	    <sj:a
			id="goBackSummaryLink"
			href="%{goBackSummaryUrl}"
			targets="summary"
			button="true"
			buttonIcon="ui-icon-transfer-e-w"
	    	title="Go Back Summary"
			onCompleteTopics="calendarToSummaryLoadedSuccess"
			><s:text name="jsp.default_400" />
	    </sj:a>

	    <s:url id="singleChildspBatchUrl" value="/pages/jsp/ach/addChildSupportBatch_init.action">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
        </s:url>
<ffi:cinclude ifEntitled="${tempEntToCheck}" checkParent="true" >
	    <sj:a id="addSingleChildspBatchLink" href="%{singleChildspBatchUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
	    	title="Add Single Child Support Batch"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="jsp.default_591" />
	    </sj:a>
	    
	    
	     <s:url id="multipleACHBatchUrl" value="/pages/jsp/ach/loadMultiACHTemplateAction_initLoad.action">
			<s:param name="loadMultipleBatch">true</s:param>
		</s:url>
	    <sj:a id="addMultipleChildSupportBatchLink" href="%{multipleACHBatchUrl}"
			targets="inputDiv" button="true"
			title="Multiple ACH Batch from Template"
			onClickTopics="beforeLoadACHForm"
			onCompleteTopics="loadACHFormComplete"
			onErrorTopics="errorLoadACHForm">
			<span class="sapUiIconCls icon-add-activity-2"></span><s:text name="ach.newMultipleBatch.fromTemplate"/>

		</sj:a>
		<div class="hidden">
	    <sj:a id="loadTemplateLink"
	    	button="true"
	    	buttonIcon="ui-icon-plus"
	    	title="Load Template"
	    	onClick="$('#quicksearchcriteria').hide();$('#loadTemplateId').toggle();ns.ach.getTemplates();"
		  	><s:text name="jsp.default_264" />
	    </sj:a>
	    </div>
</ffi:cinclude>
        <s:url id="addChildspPayeeUrl" value="/pages/jsp/ach/taxformadd.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
            </s:url>
        <sj:a id="addChildspPayeeLink" href="%{addChildspPayeeUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
            title="New Child Support Type"
            onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
            <s:text name="jsp.default_593" />
        </sj:a>

	    <s:url id="singleChildspBatchTemplateUrl" action="addChildSupportTemplate_init" namespace="/pages/jsp/ach" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
            </s:url>
	    <sj:a id="addSingleTemplateLink" href="%{singleChildspBatchTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plus"
	    	title="Add Single Child Support Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="NEW_TEMPLATE" />
	    </sj:a>

	    <s:url id="multipleChildspTemplateUrl" value="/pages/jsp/ach/addMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			<s:param name="Initialize" value="true"/>
        </s:url>
	    <sj:a id="addMultipleTemplateLink" href="%{multipleChildspTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-plusthick"
	    	title="Add Multiple Batch Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
		  	<s:text name="ach.newMultiBatch" />
	    </sj:a>

		edit single Childsp
	    <s:url id="editSingleChildspBatchUrl" value="/pages/jsp/ach/achbatchaddedit.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
	    <sj:a id="editSingleBatchLink" href="%{editSingleChildspBatchUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
	    	cssStyle="display:none;" title="Edit Child support Batch"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name="jsp.default_594" />
	    </sj:a>

		edit single template
	    <s:url id="editSingleChildspTemplateUrl" value="/pages/jsp/ach/achbatchaddedit.jsp" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	    </s:url>
	    <sj:a id="editSingleTemplateLink" href="%{editSingleChildspTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
	    	 cssStyle="display:none;" title="Edit Single Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name="ach.editTemplate" />
	    </sj:a>
	    edit multiple template
	    <s:url id="editMultipleChildspTemplateUrl" value="/pages/jsp/ach/editMultiACHTemplateAction_init.action" escapeAmp="false">
            <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	    </s:url>
	    <sj:a id="editMultipleACHTemplateLink" href="%{editMultipleChildspTemplateUrl}" targets="inputDiv" button="true" buttonIcon="ui-icon-wrench"
	    	 cssStyle="display:none;" title="Edit Multiple Template"
	    	onClickTopics="beforeLoadACHForm" onCompleteTopics="loadACHFormComplete" onErrorTopics="errorLoadACHForm">
	    	<s:text name="ach.editMultiTemplate" />
	    </sj:a>

</div>
<div id="appdashboard-right" >
    	<div style="float:right;">
				<s:url id="ChildspTypesUrl" value="/pages/jsp/ach/childsp/childsptype_summary.jsp" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					</s:url>
				<sj:a
					id="ChildspPayeesLink"
					href="%{ChildspTypesUrl}"
					targets="summary"
					indicator="indicator"
					button="true"
					buttonIcon="ui-icon-copy"
					onSuccessTopics="childspTypesSummaryLoadedSuccess"
				><s:text name="jsp.default_595" />
				</sj:a>
				
				<s:url id="ChildspUrl" value="/pages/jsp/ach/achSummaryAction.action" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					</s:url>
				<sj:a
					id="ChildspBatchesLink"
					href="%{ChildspUrl}"
					targets="summary"
					indicator="indicator"
					button="true"
					buttonIcon="ui-icon-pencil"
					onSuccessTopics="childspSummaryLoadedSuccess"
				><s:text name="jsp.default_97" />
				</sj:a>
				<s:url id="templatesUrl" value="/pages/jsp/ach/ach_templates_summary.jsp" escapeAmp="false">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				<sj:a
					id="ChildsptemplatesLink"
					href="%{templatesUrl}"
					targets="summary"
					indicator="indicator"
					button="true"
					buttonIcon="ui-icon-copy"
					onSuccessTopics="childspTemplatesSummaryLoadedSuccess"
				><s:text name="jsp.default_5" />
				</sj:a>
		</div>
		<span class="ui-helper-clearfix">&nbsp;</span>
</div> --%>

	<%-- Following three dialogs are used for view, delete and inquiry Childsp Batch --%>

    <sj:dialog id="inquiryACHBatchDialogID" cssClass="pmtTran_childspDialog" title="Child Support Batch Inquiry" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
    </sj:dialog>

	<sj:dialog id="viewACHBatchDetailsDialogID" cssClass="pmtTran_childspDialog" title="Child Support Batch Detail" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="500"  cssStyle="overflow:hidden;">
	</sj:dialog>

    <sj:dialog id="viewACHEntryErrorDetailsDialogID" cssClass="pmtTran_childspDialog" title="Child Support Entry Error Detail" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
    </sj:dialog>

	<sj:dialog id="viewACHMultiBatchDetailsDialogID" cssClass="pmtTran_childspDialog" title="ACH Multiple Batch Template Detail" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

    <sj:dialog id="editPendingACHBatchDialogID" cssClass="pmtTran_childspDialog" title="Edit Child Support Batch Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
    </sj:dialog>

    <sj:dialog id="importEntryAddendaDialogID" cssClass="pmtTran_childspDialog" title="%{getText('ach.importAddendaDialog')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
    </sj:dialog>

	<sj:dialog id="deleteACHBatchDialogID" cssClass="pmtTran_childspDialog" title="Delete Child Support Batch Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="500"  cssStyle="overflow:hidden;">
	</sj:dialog>

    <sj:dialog id="deleteChildspTypeDialogID" cssClass="pmtTran_childspDialog" title="Delete Child Support Type Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
    </sj:dialog>

    <sj:dialog id="deleteACHTemplateDialogID" cssClass="pmtTran_childspDialog" title="Delete Child Support Batch Template Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

    <sj:dialog id="deleteSingleACHTemplatesDialogID" cssClass="pmtTran_childspDialog" title="Delete Child Support Batch Template Confirm" modal="true" resizable="false" autoOpen="false" showEffect="fold" hideEffect="clip" width="500">
	</sj:dialog>

    <sj:dialog id="addEditACHEntryDialogID" cssClass="pmtTran_childspDialog" title="Add/Edit Entry" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="820">
    </sj:dialog>

    <sj:dialog id="setAllAmountDialogID" cssClass="pmtTran_childspDialog" title="Set All Amount" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
    </sj:dialog>

    <sj:dialog id="saveAsACHTemplateDialogID" cssClass="pmtTran_childspDialog" title="Child Support Templates" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
    </sj:dialog>

    <sj:dialog id="ACHsearchDestinationBankDialogID" cssClass="pmtTran_childspDialog" title="Bank Lookup Results" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350">
	</sj:dialog>

	<sj:dialog id="deleteMultiACHTemplateDialogID" cssClass="pmtTran_childspDialog" title="Delete Multiple ACH Template Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800">
	</sj:dialog>

	 <sj:dialog id="setACHBatchTypeDialogID" cssClass="pmtTran_childspDialog" title="%{getText('ach.changeBatchType.header')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="700">
    </sj:dialog>
    
    <sj:dialog id="skipACHBatchDialogID" cssClass="pmtTran_childspDialog" title="Skip Child Support Batch Confirm" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="800" height="500"  cssStyle="overflow:hidden;">
	</sj:dialog>

<script>

	$('#importEntryAddendaDialogID').addHelp(function(){
		var helpFile = $('#importEntryAddendaDialogID').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	$("#ChildspBatchesLink").find("span").addClass("dashboardSelectedItem");
</script>