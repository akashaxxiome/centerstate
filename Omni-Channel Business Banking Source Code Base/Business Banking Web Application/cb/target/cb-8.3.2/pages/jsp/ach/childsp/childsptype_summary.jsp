<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="childsptypeGridTabs" class="portlet">
    <div class="portlet-header">
		<span class="portlet-title"><s:text name="ach.childsp.state.forms"/></span>
	</div>
	<div class="portlet-content">
		<s:include value="/pages/jsp/ach/childsp/childsptypegrid.jsp"/>
	</div>
	<div id="childSupportTypesDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('childsptypeGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
</div>
<input type="hidden" id="getTransID" value="<ffi:getProperty name='childsptabs' property='TransactionID'/>" />
<script>    
	//Initialize portlet with settings icon
	ns.common.initializePortlet("childsptypeGridTabs");
</script>
