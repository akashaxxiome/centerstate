<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<% String subMenuSelected = "ach";
  	boolean isEdit = true;
	boolean isTemplate = false;
	boolean fromTemplate = false;
	String secCodeDisplay = "";

	if ((session.getAttribute("AddEditACHBatch") instanceof com.ffusion.tasks.ach.AddACHBatch)
		||(session.getAttribute("AddEditACHBatch") instanceof com.ffusion.tasks.ach.AddACHTaxPayment)
		||(session.getAttribute("AddEditACHBatch") instanceof com.ffusion.tasks.ach.AddChildSupportPayment)
		) {
		isEdit = false;
	}
%>
<ffi:help id="payments_achbatchaddeditsubmitted" className="moduleHelpClass"/>
<ffi:cinclude value1="${AddEditACHBatch.FromTemplate}" value2="true" >
	<% fromTemplate = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsTemplate}" value2="true" >
	<% isTemplate = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
	<% subMenuSelected = "tax";
    	secCodeDisplay = "<!--L10NStart-->Tax Payment (CCD + TXP Credit)<!--L10NEnd-->";
	%>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
	<% subMenuSelected = "child";
		secCodeDisplay = "<!--L10NStart-->Child Support Payment (CCD + DED Credit)<!--L10NEnd-->";
	%>
</ffi:cinclude>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/achbatchaddedit.jsp" />
<%
	String pageHeading = "";
	boolean canRecurring = true;
	String frequencyValue = "";		// used in assignTo below
	if (subMenuSelected.equals("tax"))
	{
		pageHeading = "<!--L10NStart-->Tax Payment<!--L10NEnd-->";
		canRecurring = false;
	} else
	if (subMenuSelected.equals("child"))
		pageHeading = "<!--L10NStart-->Child Support Payment<!--L10NEnd-->";
	else
		pageHeading = "<!--L10NStart-->ACH Payment<!--L10NEnd-->";
	session.setAttribute("PageHeading", pageHeading + " <!--L10NStart-->Confirm<!--L10NEnd--><br>");


    String headerTitle = "";
	String classCode = null;

%>
<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="classCode" />
    <ffi:getProperty name="AddEditACHBatch" property="FrequencyValue" assignTo="frequencyValue" />
<%
	if (classCode == null)
		classCode = "";
	classCode = classCode.toUpperCase();
	if ("ARC".equals(classCode) || "POP".equals(classCode) ||
		"BOC".equals(classCode) || "RCK".equals(classCode) || "CIE".equals(classCode))
			canRecurring = false;
	if (isEdit && "0".equals(frequencyValue))
		canRecurring = false;

%>


<script language="JavaScript">

$(document).ready(function(){
	var invalidTempalteTypes = ["RCK","POP","BOC","TEL","CIE","ARC"]
	var currentACHBatchType = '<ffi:getProperty name="AddEditACHBatch" property="standardEntryClassCode"/>';
	// if ongoing ACH batch code matches the inavalid type then hide else show the add tempalte screen
	if(invalidTempalteTypes.indexOf(currentACHBatchType)!=-1){
		$('#achAddTempalteHolder').hide();
	}
});

function openSaveTemplate(URLExtra)  {
	MyWindow=window.open(''+URLExtra,'','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=750,height=340');
}

</script>
<div style="display:none">
	<% if (!isTemplate) { %>
		<span id="achBatchResultMessage">Your <%=pageHeading %> Batch has been sent.</span>
	<% }else{ %>
		<span id="achBatchResultMessage">Your <%=pageHeading %> template has been saved.</span>
	<% } %>
</div>
<div id="" class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header"><s:text name="jsp.ach.ACH.Batch.details" /></div>
		<div class="leftPaneInnerBox summaryBlock">
			<div>
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="ach.grid.companyName" />:</span>
				<span class="inlineSection floatleft labelValue">
					<s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyName}"/>&nbsp;-&nbsp;<s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyID}"/>
				</span>
			</div>
			<div class="marginTop10 clearBoth floatleft">
				<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="jsp.default_67" />:</span>
				<span class="inlineSection floatleft labelValue">
					<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
						<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
						<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
						<ffi:process name="ACHResource" />
						<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}"/>
						<ffi:getProperty name="ACHResource" property="Resource"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
									<%= secCodeDisplay %>
					</ffi:cinclude>
				</span>
			</div>
		</div>
	</div>
	<% if (!isTemplate) { %>


		<div class="leftPaneInnerWrapper" id="achAddTempalteHolder">
			<div class="header"><s:text name="jsp.default_371" /></div>
				   <div id="templateSaveAsID" style="padding: 15px 10px;">
				    <ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="equals" >
					<s:action name="addACHBatchTemplate_init_saveAsTemplate" namespace="/pages/jsp/ach" executeResult="true">
					 </s:action>

					</ffi:cinclude>
					<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" operator="equals" >
					<s:action name="addChildSupportTemplate_init_saveAsTemplate" namespace="/pages/jsp/ach" executeResult="true">
					 </s:action>

					</ffi:cinclude>
					<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" operator="equals" >
					 <s:action name="addTaxBatchTemplate_init_saveAsTemplate" namespace="/pages/jsp/ach" executeResult="true">
					 </s:action>

					</ffi:cinclude>

			       </div>
		</div>
	<% } %>
</div>

<div class="confirmPageDetails">
		<div align="center" class="label130">
<form action="<ffi:urlEncrypt url="${SecurePath}payments/achpayments.jsp?Refresh=true"/>" method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="PayeeID" size="15">
<input type="hidden" name="SavedNumberPayments" value="<ffi:getProperty name="AddEditACHBatch" property="NumberPayments" />">
<s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">
		<div class="confirmationDetails" align="left"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditsubmitted.jsp-1" parm0="${AddEditACHBatch.TrackingID}"/></div>
</s:if>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.transaction.status.label" /></div>
	<div class="blockContent">
		<div class="blockRow" id="statusBlock">
			<span id="confirmAchBatchStatusLabel" class="sectionsubhead sectionLabel">
				<s:text name="jsp.default_388" />:
			</span>
			<span id="confirmAchBatchStatus" class="sectionsubhead  labelCls">
				<ffi:getProperty name="AddEditACHBatch" property="Status"/>
			</span>
			<s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">
			<span class="floatRight"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditsubmitted.jsp-2"/></span>
			</s:if>
		</div>
	</div>
</div>

<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.ach.ACH.Batch.Details"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBatchCompanyNameLabel"  class="sectionsubhead sectionLabel"><s:text name="ach.grid.companyName" />:</span>
				<span id="confirmWireBatchCompanyName" class="sectionsubhead  labelCls">
		                                    <s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyName}"/>&nbsp;-&nbsp;<s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyID}"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBatchBatchTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_67" />:</span>
				<span id="confirmWireBatchBatchType" class="sectionsubhead  labelCls">
					<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
						<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
						<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
						<ffi:process name="ACHResource" />
						<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}"/>
						<ffi:getProperty name="ACHResource" property="Resource"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
						<%= secCodeDisplay %>
					</ffi:cinclude>
				</span>
			</div>
		</div>

		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBatchBatchNameLabel" class="sectionsubhead sectionLabel">
					<% if (!isTemplate) { %>
							<s:text name="ach.grid.batchName" />:
					<% } else { %>
							<s:text name="ach.grid.templateName" />:
					<% } %>
				</span>
					<% if (!isTemplate) { %>
						<span id="confirmWireBatchName" class="sectionsubhead  labelCls lightBackground"><ffi:getProperty name="AddEditACHBatch" property="Name"/></span>
					<% } else { %>

					<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
						<span id="confirmWireBatchTemplateName" class="sectionsubhead  labelCls"><ffi:getProperty name="AddEditACHBatch" property="TemplateName"/></span>
					</ffi:cinclude>
					<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
						<span id="confirmWireBatchTemplateName" class="sectionsubhead  labelCls"><ffi:getProperty name="AddEditACHBatch" property="TemplateName"/></span>
					</ffi:cinclude>
													<% } %>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBatchCompanyDiscriptionLabel" class="sectionsubhead sectionLabel" ><s:text name="ach.company.description" />:</span>
				<span id="confirmWireBatchcompanyDescription" class="sectionsubhead  labelCls">
					<ffi:getProperty name="AddEditACHBatch" property="CoEntryDesc"/>
				</span>
			</div>
		</div>
		<% if (!"IAT".equals(classCode)) { %>
		    <div class="blockRow">
				<span id="confirmWireBatchDiscretionaryDataLabel" class="sectionsubhead sectionLabel" >
						<s:text name="ach.Discretionary.Data" />:
				</span>
				<span id="confirmWireBatchDiscretionaryData" class="sectionsubhead  labelCls">
						<ffi:getProperty name="AddEditACHBatch" property="CoDiscretionaryData"/>
				</span>
			</div>
		<% } else {
		// IAT doesn't have Discretionary Data
		%>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBatchOriginalCurrencyLabel" class="sectionsubhead sectionLabel" >
					<s:text name="ach.originating.currency" />:
				</span>
				<span id="confirmWireBatchUSDDolarLabel" class="sectionsubhead  labelCls">
						<s:text name="ach.USD.United.States.Dollar" />
				</span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBatchDestinationCountryLabel" >
        			<span class="sectionsubhead sectionLabel"><s:text name="da.field.ach.destinationCountry" />:  </span>
                </span>
                <span id="confirmWireDestinationalCountry" class="sectionsubhead  labelCls">
                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                    <ffi:process name="ACHResource" />
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${AddEditACHBatch.ISO_DestinationCountryCode}"/>
                    <ffi:getProperty name="ACHResource" property="Resource"/>
                </span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBatchDestinationCurrencyLabel" >
        			<span class="sectionsubhead sectionLabel"><s:text name="ach.Destination.Currency" />: </span>
                </span>
                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                    <ffi:process name="ACHResource" />
                <span id="confirmWireBatchDestinationCurrency" class="sectionsubhead  labelCls">
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${AddEditACHBatch.ISO_DestinationCurrencyCode}"/>
                    <ffi:getProperty name="AddEditACHBatch" property="ISO_DestinationCurrencyCode"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
                </span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBatchForgineExchangeMethodLabel" >
        			<span class="sectionsubhead sectionLabel"><s:text name="da.field.ach.foreignExchangeMethod" />: </span>
                </span>
                <span id="confirmWireBatchForgineExchanage" class="sectionsubhead  labelCls">
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${AddEditACHBatch.ForeignExchangeIndicator}"/>
                    <ffi:getProperty name="AddEditACHBatch" property="ForeignExchangeIndicator"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
                </span>
			</div>
		</div>
<% } %>

	<div class="blockRow">
		<% if (!isTemplate) { %>
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBatchEffectiveDateLabel" class="sectionsubhead sectionLabel">
					<s:text name="ach.grid.effectiveDate" />:
				</span>
				<span id="confirmWireBatchEffectiveDate" class="sectionsubhead  labelCls">
		      		<ffi:getProperty name="AddEditACHBatch" property="Date" />
				</span>
			</div>
		<% } %>
		<div class="inlineBlock">
			<% if (canRecurring) { %>
				<span style="width:100px" class="sectionsubhead sectionLabel"><s:text name="jsp.default_351" />: </span>
				<span class="sectionsubhead  labelCls">
					<ffi:getProperty name="AddEditACHBatch.Frequency"/> /
					 <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999"><s:text name="jsp.default_446" /></ffi:cinclude>
					 <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999" operator="notEquals" >
					 <ffi:getProperty name="AddEditACHBatch.NumberPayments"/> <s:text name="jsp.default_321" /></ffi:cinclude>
					<%-- <input type="radio" name="OpenEnded" disabled value="true" border="0" <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999">checked</ffi:cinclude>>
					<label><!--L10NStart-->Unlimited<!--L10NEnd--></label>
					<input disabled type="radio" name="OpenEnded" value="false" border="0" <ffi:cinclude value1="${NumberPayments}" value2="999" operator="notEquals" >checked</ffi:cinclude>>
					<label><!--L10NStart--># of Payments<!--L10NEnd--> <ffi:getProperty name="AddEditACHBatch.NumberPayments"/></label> --%>
				</span>
			<% } %>
		</div>
      </div>
	 <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHHeaderCompanyName${AddEditACHBatch.StandardEntryClassCode}"/>
	 <ffi:getProperty name="ACHResource" property="Resource" assignTo="headerTitle" />
	<% if (headerTitle != null && headerTitle.length() > 0) { %>
			<div class="blockRow">
				<span id="confirmAchBatchOriginalPayeeLabel" class="sectionsubhead sectionLabel">
					<%=	headerTitle %>
				</span>
				<span id="confirmAchBatchOriginalPayee" class="sectionsubhead  labelCls">
					<ffi:getProperty name="AddEditACHBatch" property="HeaderCompName"/>
				</span>
			</div>
	<% } %>
	<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="equals">
		<div class="blockRow">
			<% if ("IAT".equals(classCode))
			 {
			// QTS 577543: If IAT, we don't support Batch Balanced or Entry Balanced batches
			%>
			   <s:text name="ach.IAT.noOffsetAcct"/>
			 <% } else { %>
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_494" />: </span>
				<span class="sectionsubhead  labelCls">
					<s:property value="#session.AddEditACHBatch.OffsetAccount.Number"/>&nbsp; - &nbsp;<s:property value="#session.AddEditACHBatch.OffsetAccount.Type"/>
				</span>
			<% } %>
		</div>
	</ffi:cinclude>
</div>
</div>
<ffi:cinclude value1="${AddEditACHBatch.Status}" value2="Approval Pending" operator="equals">
	<s:if test="%{#session.AddEditACHBatch.SameDay==1}">

		<div class="blockWrapper">
		<br/>
		<s:if test="%{sameDayNextCutOffTime!=null &&  !sameDayNextCutOffTime.equals(sameDayFinalCutOffTime) }">
			<b><s:text name="ach.sameday.confirm.message">
				<s:param ><s:property value="sameDayNextCutOffTime" /></s:param>
			</s:text></b>
		</s:if>
		<s:else>
			<b><s:text name="ach.sameday.confirm.message">
				<s:param ><s:property value="sameDayFinalCutOffTime" /></s:param>
			</s:text></b>
		</s:else>

		</div>

	</s:if>
</ffi:cinclude>

<div class="blockWrapper">
	<div  class="blockHead"><s:text name="ach.grid.entries" /></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireTotalNumberCreditsLabel" class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_513" />:
				</span>
				<span class="sectionsubhead sectionLabel" width="16%">
		            <%-- <input id="confirmWireBatchTotalNumberCredits" type="text" disabled name="TotalNumberCredits" maxlength="6" size="8" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberCredits" />" class="ui-widget-content ui-corner-all txtbox"> --%>
		            <ffi:getProperty name="AddEditACHBatch" property="TotalNumberCredits" />
		        </span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBatchTotalNumberDebitLabel" class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_514" />:
				</span>
				<span class="sectionsubhead sectionLabel" width="16%">
					<%-- <input id="confirmWireTotalNumberDebit" type="text" disabled name="TotalNumberDebits" maxlength="6" size="8" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberDebits" />" class="ui-widget-content ui-corner-all txtbox"> --%>
					<ffi:getProperty name="AddEditACHBatch" property="TotalNumberDebits" />
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBatchTotalCreditsLabel" class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_433" /> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):
				</span>
				<span class="sectionsubhead sectionLabel">
		            <%-- <input id="confirmWireBatchTotalCredits" type="text" disabled name="TotalCreditAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalCreditAmount" />" class="ui-widget-content ui-corner-all txtbox"> --%>
		            <ffi:getProperty name="AddEditACHBatch" property="TotalCreditAmount" />
				</span>
			</div>
			<div class="inlineBlock">
				<span id="confirmWireBatchTotalDebitsLabel" class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_434" /> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):
				</span>
				<span id="confirmWireBatchTotalDebits" class="sectionsubhead sectionLabel">
					<%-- <input id="confirmWireBatchTotalDebits" type="text" disabled name="TotalDebitAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalDebitAmount" />" class="ui-widget-content ui-corner-all txtbox"> --%>
					<ffi:getProperty name="AddEditACHBatch" property="TotalDebitAmount" />
				</span>
			</div>
		</div>
	</div>
</div>






					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td></td>
							<td class="sectionsubhead  labelCls lightBackground">
								<div align="left">
								<%-- <form action="<ffi:urlEncrypt url="${SecurePath}payments/achpayments.jsp?Refresh=true"/>" method="post">
                    				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<input type="hidden" name="PayeeID" size="15">
									<input type="hidden" name="SavedNumberPayments" value="<ffi:getProperty name="AddEditACHBatch" property="NumberPayments" />"> --%>
					<%-- <s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">
								<table width="98%" border="0" cellspacing="0" cellpadding="3">
									<tr valign="top">
										<td class="columndata" ><span class="sectionsubhead"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditsubmitted.jsp-1" parm0="${AddEditACHBatch.TrackingID}"/><br>
										<br></span>
									</td>
									</tr>
								</table>
					</s:if>		 --%>
								<%-- <table width="98%" border="0" cellspacing="0" cellpadding="3">
									<tr valign="top">
									<tr class="lightBackground">
										<td id="confirmWireBatchCompanyNameLabel"  class="sectionsubhead" align="right" width="213"><!--L10NStart-->Company Name<!--L10NEnd--></td>
										<td id="confirmWireBatchCompanyName" class="columndata" align="left">
                                            <s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyName}"/>&nbsp;-&nbsp;<s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyID}"/>
										</td>
									</tr>
									<tr class="lightBackground">
										<td id="confirmWireBatchBatchTypeLabel" class="sectionsubhead" align="right" width="213"><!--L10NStart-->Batch Type<!--L10NEnd--></td>
										<td id="confirmWireBatchBatchType" class="columndata" align="left">
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
								<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
								<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
								<ffi:process name="ACHResource" />
												<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}"/>
												<ffi:getProperty name="ACHResource" property="Resource"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
										<%= secCodeDisplay %>
</ffi:cinclude>

									    </td>
									</tr>
								</table> --%>
								<table width="98%" border="0" cellspacing="0" cellpadding="3">
								    <tr class="lightBackground">
<%-- FIRST COLUMN --%>
								        <td width="60%">
								            <table>
								               <%--  <tr>
										<td id="confirmWireBatchBatchNameLabel" class="sectionsubhead" align="right" width="210">
								<% if (!isTemplate) { %>
											<!--L10NStart-->Batch Name<!--L10NEnd-->
								<% } else { %>
											<!--L10NStart-->Template Name<!--L10NEnd-->
								<% } %></td>

								<% if (!isTemplate) { %>
										<td id="confirmWireBatchName" class="columndata lightBackground"><ffi:getProperty name="AddEditACHBatch" property="Name"/></td>
								<% } else { %>

<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
										<td id="confirmWireBatchTemplateName" class="columndata lightBackground"><ffi:getProperty name="AddEditACHBatch" property="TemplateName"/></td>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
										<td id="confirmWireBatchTemplateName" class="columndata lightBackground"><ffi:getProperty name="AddEditACHBatch" property="TemplateName"/></td>
</ffi:cinclude>
								<% } %>
								                </tr>
								                <tr>
										<td id="confirmWireBatchCompanyDiscriptionLabel" class="sectionsubhead" align="right"><!--L10NStart-->Company Description<!--L10NEnd--></td>
										<td id="confirmWireBatchcompanyDescription" class="columndata">
											<ffi:getProperty name="AddEditACHBatch" property="CoEntryDesc"/>
										</td>
								                </tr> --%>
<%-- <% if (!"IAT".equals(classCode)) { %>
    <tr>
	<td id="confirmWireBatchDiscretionaryDataLabel" class="sectionsubhead" align="right">
			<!--L10NStart-->Discretionary Data<!--L10NEnd-->
	</td>
	<td id="confirmWireBatchDiscretionaryData" class="columndata">
			<ffi:getProperty name="AddEditACHBatch" property="CoDiscretionaryData"/>
	</td>
	</tr>
<% } else {
// IAT doesn't have Discretionary Data
%>
    <tr>
	<td id="confirmWireBatchOriginalCurrencyLabel" class="sectionsubhead" align="right">
			<!--L10NStart-->Originating Currency<!--L10NEnd-->
	</td>
	<td id="confirmWireBatchUSDDolarLabel" class="columndata">
			<!--L10NStart-->USD - United States Dollar<!--L10NEnd-->
	</td>
	</tr>
            <tr >
                <td id="confirmWireBatchDestinationCountryLabel" align="right">
        <span class="sectionsubhead"><!--L10NStart-->Destination Country<!--L10NEnd--> </span>
                </td>
                <td id="confirmWireDestinationalCountry" class="columndata">
                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                    <ffi:process name="ACHResource" />

                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${AddEditACHBatch.ISO_DestinationCountryCode}"/>
                    <ffi:getProperty name="ACHResource" property="Resource"/>

                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
            <tr>
                <td id="confirmWireBatchDestinationCurrencyLabel" align="right">
        <span class="sectionsubhead"><!--L10NStart-->Destination Currency<!--L10NEnd--> </span>
                </td>
                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                    <ffi:process name="ACHResource" />
                <td id="confirmWireBatchDestinationCurrency" class="columndata">
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${AddEditACHBatch.ISO_DestinationCurrencyCode}"/>
                    <ffi:getProperty name="AddEditACHBatch" property="ISO_DestinationCurrencyCode"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
            <tr>
                <td id="confirmWireBatchForgineExchangeMethodLabel" align="right">
        <span class="sectionsubhead"><!--L10NStart-->Foreign Exchange Method<!--L10NEnd--> </span>
                </td>
                <td id="confirmWireBatchForgineExchanage" class="columndata">
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${AddEditACHBatch.ForeignExchangeIndicator}"/>
                    <ffi:getProperty name="AddEditACHBatch" property="ForeignExchangeIndicator"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
<% } %> --%>
								                <%-- <tr>
										<td id="confirmWireBatchEffectiveDateLabel" class="sectionsubhead" align="right">
										<% if (!isTemplate) { %>
											<!--L10NStart-->Effective Date<!--L10NEnd-->
										<% } else {
										// NO Date required for Templates
										%>
											&nbsp;
										<% } %></td>
										<td id="confirmWireBatchEffectiveDate" class="columndata">
										<% if (!isTemplate) { %>
	                                		<ffi:getProperty name="AddEditACHBatch" property="Date" />
										<% } else {
										// NO Date required for Templates
										%>
											&nbsp;
										<% } %> </td>
								                </tr> --%>
<%-- 		<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHHeaderCompanyName${AddEditACHBatch.StandardEntryClassCode}"/>
		<ffi:getProperty name="ACHResource" property="Resource" assignTo="headerTitle" />
<% if (headerTitle != null && headerTitle.length() > 0) { %>
									<tr class="lightBackground">
										<td id="confirmAchBatchOriginalPayeeLabel" class="sectionsubhead" align="right">
											<%=	headerTitle %>
										</td>
										<td id="confirmAchBatchOriginalPayee" class="columndata">
											<ffi:getProperty name="AddEditACHBatch" property="HeaderCompName"/>
										</td>

									</tr>
<% } %> --%>
									<%-- <tr  class="lightBackground">
										<td id="confirmAchBatchStatusLabel" class="sectionsubhead" align="right">
											<!--L10NStart-->Status<!--L10NEnd-->
										</td>
										<td id="confirmAchBatchStatus" class="columndata">
											<ffi:getProperty name="AddEditACHBatch" property="Status"/>
										</td>
									</tr> --%>
								            </table>
								        </td>



<%-- SECOND COLUMN --%>
								        <td width="40%">
								            <table>
								                <%-- <tr>
										<% if (canRecurring) { %>
										<td class="sectionsubhead tbrd_r" rowspan="4" width="10">&nbsp;</td>
										<td class="sectionsubhead" rowspan="4" width="10">&nbsp;</td>
										<td class="sectionsubhead" align="right"><!--L10NStart-->Repeating<!--L10NEnd--></td>
										<td class="columndata"><ffi:getProperty name="AddEditACHBatch.Frequency"/></td>
										<% } %>
								                </tr>
								                <tr>
										<% if (canRecurring) { %>
										<td class="sectionsubhead" colspan="2" align="right"><input type="radio" name="OpenEnded" disabled value="true" border="0" <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999">checked</ffi:cinclude>></td>
										<td class="sectionsubhead"><!--L10NStart-->Unlimited<!--L10NEnd--></td>
										<% } %>
								                </tr>
								                <tr>
										<% if (canRecurring) { %>
										<td class="sectionsubhead" colspan="2" align="right"><input disabled type="radio" name="OpenEnded" value="false" border="0" <ffi:cinclude value1="${NumberPayments}" value2="999" operator="notEquals" >checked</ffi:cinclude>></td>
										<td class="sectionsubhead"><!--L10NStart--># of Payments<!--L10NEnd--> <ffi:getProperty name="AddEditACHBatch.NumberPayments"/></td>
										<% } %>
								                </tr> --%>
								                <tr>
											<%-- <ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="equals">
<% if ("IAT".equals(classCode))
 {
// QTS 577543: If IAT, we don't support Batch Balanced or Entry Balanced batches
%>
                                        <td class="sectionsubhead" align="right" colspan='3'>
                                        <s:text name="ach.IAT.noOffsetAcct"/>
                                        </td>
 <% } else { %>
												<td class="sectionsubhead" colspan="2" align="right"><!--L10NStart-->Offset Account<!--L10NEnd--></td>
												<td class="columndata">
													<s:property value="#session.AddEditACHBatch.OffsetAccount.Number"/>&nbsp; - &nbsp;<s:property value="#session.AddEditACHBatch.OffsetAccount.Type"/>
												</td>
<% } %>
											</ffi:cinclude> --%>
											<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="notEquals">
												<td class="sectionsubhead " align="right">&nbsp;</td>
												<td colspan="2">&nbsp;</td>
											</ffi:cinclude>
								                </tr>
								            </table>
								        </td>
								    </tr>
								</table>
								<!-- <table width="98%" border="0" cellspacing="0" cellpadding="3">
									<tr class="lightBackground">
										<td height="20"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="157" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="87" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
									</tr>
									<tr>
										<td class="tbrd_b" colspan="6"> -->
		<%-- <table width="98%" border="0" cellspacing="0" cellpadding="0">
		<tr>
	    <td id="confirmWireBatchEntriesLabel" class="sectionhead" rowspan="2" width="36%"><span class="sectionhead">&gt;&nbsp;<!--L10NStart-->Entries<!--L10NEnd--><br></span></td>

		<td id="confirmWireTotalNumberCreditsLabel" class="sectionsubhead" valign="center" width="16%">
			<!--L10NStart-->Total # Credits<!--L10NEnd-->
		</td>
		<td class="sectionsubhead" width="16%">
            <input id="confirmWireBatchTotalNumberCredits" type="text" disabled name="TotalNumberCredits" maxlength="6" size="8" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberCredits" />" class="ui-widget-content ui-corner-all txtbox">
        </td>
		<td id="confirmWireBatchTotalNumberDebitLabel" class="sectionsubhead" width="16%">
			<!--L10NStart-->Total # Debits<!--L10NEnd-->
		</td>
		<td class="sectionsubhead" width="16%">
			<input id="confirmWireTotalNumberDebit" type="text" disabled name="TotalNumberDebits" maxlength="6" size="8" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberDebits" />" class="ui-widget-content ui-corner-all txtbox">
		</td>
		</tr>
		<tr>
		<td id="confirmWireBatchTotalCreditsLabel" class="sectionsubhead" width="16%">
			<!--L10NStart-->Total Credits<!--L10NEnd--> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
		</td>
		<td class="sectionsubhead" width="16%">
            <input id="confirmWireBatchTotalCredits" type="text" disabled name="TotalCreditAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalCreditAmount" />" class="ui-widget-content ui-corner-all txtbox">
		</td>
		<td id="confirmWireBatchTotalDebitsLabel" class="sectionsubhead" width="16%">
			<!--L10NStart-->Total Debits<!--L10NEnd--> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
		</td>
		<td id="confirmWireBatchTotalDebits" class="sectionsubhead" width="16%">
			<input id="confirmWireBatchTotalDebits" type="text" disabled name="TotalDebitAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalDebitAmount" />" class="ui-widget-content ui-corner-all txtbox">
		</td>
		</tr>
		</table> --%>

										<!-- </td>
									</tr>
									<tr class="lightBackground">
										<td height="20"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="157" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="87" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
									</tr> -->
									<%-- <tr class="lightBackground">
										<td class="sectionsubhead" align="right">
											<!--L10NStart-->Batch submitted on<!--L10NEnd-->
										</td>
										<td class="columndata">
<%
	    com.ffusion.beans.DateTime date = new com.ffusion.beans.DateTime();
    	if( date != null ) {
    		date.setFormat( "M/d/yyyy hh:mm a" );
    	}
%>
											<%= date.toString() %>
										</td>
									</tr>
								</table> --%>
<%-- don't display the entries on the Submitted Page as per requirements document
<ffi:setProperty name="CallerURL" value="${SecurePath}payments/achbatchaddeditsubmitted.jsp"/>
<ffi:include page="${PathExt}payments/achbatchaddeditentries.jsp" />
--%>
</div>
		</td>
	</tr>
</table>
<div class="blockWrapper">
	<div class="blockContent">
		<div class="blockRow">
			<span class="sectionLabel"><s:text name="jsp.ach.Batch.submitted.on" />:</span>
			<span>
				<% com.ffusion.beans.DateTime date = new com.ffusion.beans.DateTime(); if( date != null ) { date.setFormat( "M/d/yyyy hh:mm a" ); } %>
				<%= date.toString() %>
		</span>
		</div>
	</div>
</div>

<div class="btn-row">
<input class="submitbutton" type="hidden" value="DONE">
<% if (isTemplate) { %>
<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
<sj:a button="true" summaryDivId="summary" buttonType="done" onClickTopics="showSummary,ACHTemplateGridTabsShow,ACHtemplatesSummaryLoadedSuccess"><s:text name="jsp.default_175"/></sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" >
<sj:a button="true" summaryDivId="summary" buttonType="done" onClickTopics="showSummary,ACHTemplateGridTabsShow,childspTemplatesSummaryLoadedSuccess"><s:text name="jsp.default_175"/></sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
 <sj:a button="true" summaryDivId="summary" buttonType="done" onClickTopics="showSummary,ACHTemplateGridTabsShow,taxTemplatesSummaryLoadedSuccess"><s:text name="jsp.default_175"/></sj:a>
</ffi:cinclude>

<% } else { %>
<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
    <sj:a id="DONE" button="true" summaryDivId="summary" buttonType="done" onClickTopics="showSummary,ACHGridTabsShow,ACHSummaryLoadedSuccess,calendarToSummaryLoadedSuccess" ><s:text name="jsp.default_175"/></sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" >

<sj:a button="true" summaryDivId="summary" buttonType="done"   onClickTopics="showSummary,ACHGridTabsShow,childspSummaryLoadedSuccess,calendarToSummaryLoadedSuccess"><s:text name="jsp.default_175"/></sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
<sj:a  button="true" summaryDivId="summary" buttonType="done"   onClickTopics="showSummary,ACHGridTabsShow,taxSummaryLoadedSuccess,calendarToSummaryLoadedSuccess"><s:text name="jsp.default_175"/></sj:a>
</ffi:cinclude>
<% } %>
</div>
</form>
</div>
</div>
<ffi:setProperty name="OpenEnded" value="false"/>
<ffi:removeProperty name="strutsActionName"/>
<ffi:removeProperty name="AddEditACHBatch"/>

<ffi:cinclude value1="${ACHType}" value2="ACHBatch" >
	<ffi:setProperty name="subMenuSelected" value="ach"/>
</ffi:cinclude>
<ffi:cinclude value1="${ACHType}" value2="ChildSupportPayment" >
	<ffi:setProperty name="subMenuSelected" value="child"/>
</ffi:cinclude>
<ffi:cinclude value1="${ACHType}" value2="TaxPayment" >
	<ffi:setProperty name="subMenuSelected" value="tax"/>
</ffi:cinclude>
