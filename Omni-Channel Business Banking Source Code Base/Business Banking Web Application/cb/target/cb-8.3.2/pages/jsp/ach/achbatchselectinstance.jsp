
<%@ page
	import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.ach.ACHOffsetAccount,
		 com.ffusion.beans.ach.ACHClassCode"%>

<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_achbatchrecedit" className="moduleHelpClass" />
<div align="center">
	<br />
	<form method="post"
		name="EditACHInstanceForm" id="EditACHInstanceForm">
		<input type="hidden" name="CSRF_TOKEN"
			value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		<s:hidden name="ID" value="%{#parameters.ID}" />
		<s:hidden name="RecID" value="%{#parameters.RecID}" />
		<s:hidden name="OriginalDate" value="%{#parameters.OriginalDate}" />

		<div align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="3">


				<tr class="lightBackground">
					<td width="25%" />
					<td class="sectionsubhead" align="right"><input type="radio"
						checked=checked name="editRecurringModel" value="false"
						border="0"></td>

					<td class="sectionsubhead">
						<s:text name="jsp.ach.Modify.ACH.Batch.msg" />

					</td>
				</tr>

				<tr class="lightBackground">
					<td width="25%" />
					<td class="sectionsubhead" align="right"><input type="radio"
						name="editRecurringModel" value="true" border="0"></td>

					<td class="sectionsubhead">
						<s:text name="jsp.ach.Modify.all.ACH.Batch.msg" />
					</td>
				<tr>
				<tr>

					<td colspan="15" align="center"><sj:a button="true"
							onClickTopics="closeDialog"><s:text name="jsp.default_82"/>
			</sj:a> &nbsp;&nbsp; <sj:a button="true"
							onClick="ns.ach.continueEditPendingRec();"><s:text name="jsp.default_111" />
			</sj:a></td>
				</tr>
			</table>

		</div>
	</form>
</div>
