<%--
This page processes the add wire from import request.  Wires are just created
at this point, but not saved to the back-end.  That happens after the wires are
confirmed.

Pages that request this page
----------------------------
wiretransferimport.jsp
	This happens in a somewhat roundabout way.  Wiretransferimport.jsp makes
	the request, but the request is initiated by uploadpprecord.jsp (a pop-up)
	to a task defined on wiretransferimport.jsp (FileUpload).  FileUpload, in
	turn, requests this page.
	
Pages this page requests
------------------------
Javascript auto-refreshes to wiretransferimportview.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

