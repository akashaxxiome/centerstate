<%@ page import="com.ffusion.beans.ach.ACHEntry" %>
<%@ page import="com.ffusion.beans.ach.ACHBatch" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achbatchentryerrors" className="moduleHelpClass"/>

<%
	if (request.getParameter("EntryID") != null) {
        com.ffusion.beans.ach.ACHBatch achBatch = (com.ffusion.beans.ach.ACHBatch)session.getAttribute("AddEditACHBatch");
        com.ffusion.beans.ach.ACHEntry achEntry = achBatch.getByID((String)request.getParameter("EntryID"));
        session.setAttribute("ACHEntry", achEntry);
	}
 %>
	<ffi:cinclude operator="equals" value1="${ACHEntry.hasValidationErrors}" value2="true">
        <table>
		<% String lineContent; String lineNumber; String recordNumber; %>
		<ffi:list collection="ACHEntry.validationErrors" items="validationError">
			<ffi:getProperty name="ACHEntry" property="lineContent" assignTo="lineContent"/>
			<ffi:getProperty name="ACHEntry" property="lineNumber" assignTo="lineNumber"/>
			<ffi:getProperty name="ACHEntry" property="recordNumber" assignTo="recordNumber"/>
		<tr>
			<td colspan="10" class="columndata_error columndata_bold">&nbsp;&nbsp;&nbsp;&nbsp;<ffi:getProperty name="validationError" property="title"/></td>
		</tr>
		<tr>
			<td></td>
			<td colspan="9" class="columndata_error">
			<% if (lineNumber != null && recordNumber != null) { %>
      	<span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-1" parm0="${ACHEntry.lineNumber}" parm1="${ACHEntry.recordNumber}"/>:</span>
			<% } %>
			<% if (lineNumber != null && recordNumber == null) { %>
        <span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-2" parm0="${ACHEntry.lineNumber}"/>:</span>
			<% } %>
			<% if (lineNumber == null && recordNumber != null) { %>
        <span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-3" parm0="${ACHEntry.recordNumber}"/>:</span>
			<% } %>
        <ffi:getProperty name="validationError" property="message"/>
			</td>
		</tr>
			<% if (lineContent != null) { %>
			<tr>
				<td></td>
				<td colspan="9" class="columndata_error">
					<span class="columndata_bold"><s:text name="jsp.default_209" />:</span>
					<%= lineContent %>
				</td>
			</tr>
			<tr>
				<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="5" border="0"></td>
			</tr>
			<% } %>
		</ffi:list>
        </table>
    </ffi:cinclude>
    <ffi:removeProperty name="ACHEntry"/>