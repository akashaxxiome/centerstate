<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />
<ffi:help id="payments_achbatchaddedit" className="moduleHelpClass"/>
<script language="JavaScript">
<!--

function checkSubmit()
{
//	if (!checkACHAmount(document.getElementById("commonAmountId")))
//	{
//		return false;
//	}

	var amount = $("#commonAmountId").val();

    var elem = $('#setAllAmountsError');
    if(elem)
    {
        elem.addClass('errorLabel');
    }
    //Check for negative amount
    var negIdx = amount.indexOf('-');
    if (negIdx != -1) {

        if (negIdx == 0) {
            elem.html('Negative amounts are not allowed. Please enter a positive amount');
        } else {
            elem.html('The amount contains an invalid character');
        }
        return false;
    }

    var tempAmt = amount.replace(/[^0-9|\.]/g,"");

    var decIdx = tempAmt.indexOf('.');
    var intPartLen = 0;
    var decPartLen = 0;

    if (decIdx != -1) {
        intPartLen = tempAmt.substring(0, decIdx).length;
        decPartLen = tempAmt.substring(decIdx+1, tempAmt.length).length;
    } else {
        intPartLen = tempAmt.length;
    }

    //Check 8 digit integer constraint
    if (intPartLen > 8) {
        elem.html('Amount too large, only eight digits before decimal please');
        return false;
    }
    //Check total 10 digit constraint
    if ((intPartLen + decPartLen) > 10) {
        elem.html('Amount too large, only 10 digits please');
        return false;
    }
    // WARNING.... Custom Code for US follows, Decimal = ".", grouping separator = ","
    var amtStr = "" + amount.replace(/[^0-9|\.]/g,"");
    var amt = parseFloat(amtStr);
    if (isNaN(amt)) {
        elem.html('Please enter a valid amount');
        return false;
    }

    ns.common.closeDialog("#setAllAmountDialogID");
    
    //Get struts action to hit
    var type = $("#achType").val();
    var strutsActionName = "/cb/pages/jsp/ach/setAllAmounts.action";
    $.ajax({
        url: strutsActionName,
		type: "POST",
		data: {setAllAmount: amount,achType: type, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
        success: function(data) {
        	$('#achEntryDetails').html(data);
        }
    });
}
// --></script>

        <div align="center">
			<table width="300" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color">
				<tr>
					<td class="columndata" align="center" class="ltrow2_color">
						<table width="261" border="0" cellspacing="0" cellpadding="3" class="ltrow2_color">
							<tr>
								<td>
                    				<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<table width="242" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="247" align="right" class="sectionhead paddingRight10"><s:text name="jsp.default_43" /></td>
											<td><input id="commonAmountId" class="ui-widget-content ui-corner-all txtbox" type="text" name="CommonAmount" size="10" maxlength="10"></td>
										</tr>
										<tr>
                                            <td height="60" align="center" colspan="2">
                                               <span id="setAllAmountsError"></span>
                                            </td>
										</tr>
										<tr>
											<td align="center" colspan="4">
												<div class="ui-widget-header customDialogFooter">
													<sj:a
											            button="true" 
														onClick="checkSubmit();"
											       	><s:text name="jsp.default_303" /><!-- OK -->
											       	</sj:a>
													<sj:a 
											            button="true" 
														onClickTopics="closeDialog"
											       	><s:text name="jsp.default_82" /><!-- CANCEL -->
											       	</sj:a>
											     </div>
											</td>
										</tr>
									</table>
									&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
