<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<%
    String viewAllText1 = "<!--L10NStart-->Business Tax<!--L10NEnd-->";
    String viewAllText2 = "<!--L10NStart-->Group Tax<!--L10NEnd-->";
    String viewMyText = "<!--L10NStart-->My Tax<!--L10NEnd-->";
%>

<div id="quicksearchcriteria" class="quickSearchAreaCls">
	<s:form action="/pages/jsp/ach/taxSummary_verifyACH.action" method="post" id="QuickSearchTaxFormID" name="QuickSearchTransfersForm" theme="simple">
    	<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
		<div id="achsearchcriteria" style="display:block">
        	<!-- quick search item holder -->
          	<div class="acntDashboard_masterHolder">
	          	<!-- date range picker holder -->
	          	<div class="acntDashboard_itemHolder">
	          		<span class="dashboardLabelMargin" style="display:block;"><s:text name="jsp.default.label.date.range"/></span>
					<input type="text" id="taxDateRangeBox" />
					<input type="hidden" value="<s:property value='achBatchSearchCriteria.startDate'/>" id="StartDateID" name="achBatchSearchCriteria.startDate" />
					<input type="hidden" value="<s:property value='achBatchSearchCriteria.endDate'/>" id="EndDateID" name="achBatchSearchCriteria.endDate" />
	          	</div>
	          	
	          	<!-- drop down options -->
	          	<div class="acntDashboard_itemHolder">
	          		<!-- <label for="">&nbsp;Show:</label> -->
	          		<span class="dashboardLabelMargin" style="display:block;"><s:text name="jsp.default_381"/></span>
	          		<select id="viewAllTaxId" name="achBatchSearchCriteria.viewAll" class="txtbox"  >
						<option value="false"><s:text name="taxach.viewMyACH"/></option>
						<option value="true"><s:property value="groupOption"/></option>
					</select>	
	          		
	           		<s:select
						id="viewForTaxCompanyId"
						cssClass="txtbox"
						headerKey="" headerValue="%{getText('ach.allCompanyIDQS')}"
						list="companyList" name="achBatchSearchCriteria.companyID"
						listKey="companyID" listValue="%{companyName+'('+companyID+')'}">
					</s:select> 
	          	</div>
	          	
	          	<!-- search button holder -->
	          	<div class="acntDashboard_itemHolder">
	          		<span class="dashboardLabelMargin" style="display:block;">&nbsp;</span>
		           	<sj:a targets="quick"
						title="%{getText('jsp.default_6')}"
						id="quicksearchbutton"
						formIds="QuickSearchTaxFormID"
						button="true"
						validate="true"
					                   validateFunction="customValidation"
						onclick="removeValidationErrors();"
						onCompleteTopics="quickSearchACHComplete" >
						<s:text name="jsp.default_6" />
					</sj:a>
	            </div>
			</div>
			<span id="startDateError"></span>
			<span id="endDateError"></span>
			<span id="dateRangeValueError"></span>
		</div>
	</s:form>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#viewAllTaxId").selectmenu({width: 200});
		$("#viewForTaxCompanyId").selectmenu({width: 200});
	});

	var aUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?onlyOneDirection=true&calDirection=back&calForm=LogForm&calTarget=FindLogMessages.StartDate"/>';	
	var aConfig = {
		url:aUrl,
		startDateBox:$("#StartDateID"),
		endDateBox:$("#EndDateID")
	}
	$("#taxDateRangeBox").extdaterangepicker(aConfig);
</script>





