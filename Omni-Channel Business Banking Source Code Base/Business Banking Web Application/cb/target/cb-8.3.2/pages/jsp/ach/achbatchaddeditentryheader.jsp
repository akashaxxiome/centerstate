<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<!-- replacing table structure with div: Title area-->
<%-- <div class="acntDashboard_masterHolder">
	<!-- title tag -->
	<div class="acntDashboard_itemHolder">
		<span class="sectionhead">&gt;&nbsp;<!--L10NStart-->Entries<!--L10NEnd--></span><br>
	</div>
</div> --%>

<!-- misc fields -->
<div class="acntDashboard_masterHolder" style="float:left; width:100%;">
	<table border="0" cellspacing="0" cellpadding="3" width="100%" class="tableData">
		<tr>
			<td>
				<!-- Hold Entries -->
				<%-- <div class="acntDashboard_itemHolder" style="line-height:30px;">
					<span style="vertical-align:middle; position:relative;"><!--L10NStart-->Hold Entries<!--L10NEnd--></span>
					<input style="vertical-align:middle; position:relative;display: none" id="holdAllId" type="checkbox" title="Hold All ACH Entries" name="checkbox" <s:if test="%{#session.AddEditACHBatch.AllEntriesHeldValue}">checked</s:if> onclick="holdAll(this);" >
				</div> --%>
			</td>
			<td>
				<!-- Total # Credits -->
				<div class="acntDashboard_itemHolder" id="TotalNumberCredits">
				<input style="vertical-align:middle; position:relative;display: none" id="holdAllId" type="checkbox" title="Hold All ACH Entries" name="checkbox" <s:if test="%{#session.AddEditACHBatch.AllEntriesHeldValue}">checked</s:if> onclick="holdAll(this);" >
					<span><s:text name="jsp.ach.No.of.Credits" /></span>
					<span class="updatedValue">
						<ffi:getProperty name="AddEditACHBatch" property="TotalNumberCredits"/>
					</span>
					<input type="text" name="TotalNumberCredits" maxlength="6" size="6" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberCredits"/>" class="ui-widget-content ui-corner-all txtbox updatedValue hidden">
				</div>
			</td>
			<td>
				<!-- Total # Credits (Base Currency) -->
				<div class="acntDashboard_itemHolder" id="TotalCreditAmount">
					<span><s:text name="ach.grid.totalCredits" /> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)</span>
					<span class="updatedValue">
						<ffi:getProperty name="AddEditACHBatch" property="TotalCreditAmountValue.CurrencyStringNoSymbol"/>
					</span>
					<input type="text" name="AddEditACHBatch.TotalCreditAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalCreditAmountValue.CurrencyStringNoSymbol"/>" class="ui-widget-content ui-corner-all txtbox updatedValue hidden">
				</div>
			</td>
			<td>
				<!-- Total # Debits -->
				<div class="acntDashboard_itemHolder" id="TotalNumberDebits">
					<span><s:text name="jsp.ach.No.of.Debits" /></span>
					<span class="updatedValue">
						<ffi:getProperty name="AddEditACHBatch" property="TotalNumberDebits"/>
					</span>
					<input type="text" name="TotalNumberDebits" id="TotalNumberDebits" maxlength="6" size="6" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberDebits"/>" class="ui-widget-content ui-corner-all txtbox updatedValue hidden">
				</div>
			</td>
			<td>
				<!-- Total # Debits (Base Currency) -->
				<div class="acntDashboard_itemHolder" id="TotalDebitAmount">
					<span><s:text name="jsp.default_434" /> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)</span>
					<span class="updatedValue">
						<ffi:getProperty name="AddEditACHBatch" property="TotalDebitAmountValue.CurrencyStringNoSymbol"/>
					</span>
					<input type="text" name="AddEditACHBatch.TotalDebitAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalDebitAmountValue.CurrencyStringNoSymbol"/>" class="ui-widget-content ui-corner-all txtbox updatedValue hidden">
				</div>
			</td>
		</tr>
	</table>
</div>