<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<% 
session.setAttribute("taxType", request.getParameter("type"));
%>
<ffi:cinclude value1="${taxType}" value2="state" >
	<ffi:help id="payments_stateTaxTypeDelete" className="moduleHelpClass"/>
</ffi:cinclude>
<ffi:cinclude value1="${taxType}" value2="federal" >
	<ffi:help id="payments_federalTaxTypeDelete" className="moduleHelpClass"/>
</ffi:cinclude>
<ffi:cinclude value1="${taxType}" value2="childsp" >
	<ffi:help id="payments_taxChildspTypeDelete" className="moduleHelpClass"/>
</ffi:cinclude>
<ffi:removeProperty name="taxType"/>

<% String subMenu = "tax"; %>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
	<% subMenu = "tax";	%>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" >
	<% subMenu = "child";	%>
</ffi:cinclude>

<% if (subMenu.equals("tax")) { %>
	<ffi:setL10NProperty name='PageHeading' value='Delete Tax Form'/>
<% } else { %>
	<ffi:setL10NProperty name='PageHeading' value='Delete Child Support Form'/>
<% } %>

<% if (request.getParameter("ID") != null) { 
	session.setAttribute("ID", request.getParameter("ID"));
	session.setAttribute("isBusiness", request.getParameter("isBusiness"));
} %>

<s:action namespace="/pages/jsp/ach" name="deleteChildTaxTypeAction_init" />

<div align="center">
<s:form action="/pages/jsp/ach/deleteChildTaxTypeAction.action" method="post" name="chilspTypeDelete" id="deleteChildTaxTypeFormId" >
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td></td>
      <td class="lightBackground">
        <div align="left">
			
                    <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    <input type="hidden" name="ID" value="<ffi:getProperty name='ID'/>"/>
                    <% if (subMenu.equals("tax")) { %>
                        <input type="hidden" name="taxFormType" value="TaxPayment"/>
                    <% } else { %>
                        <input type="hidden" name="taxFormType" value="ChildSupport"/>
                    <% } %>
                    <input type="hidden" name="IsBusiness" value="<ffi:getProperty name='isBusiness'/>"/>
					<table border="0" cellspacing="0" cellpadding="3" width="98%">
						<tr class="lightBackground">
							<td colspan="3">
								<div align="center"><span class="sectionhead">
                    <ffi:cinclude value1="${isBusiness}" value2="true" operator="equals">
<% if (subMenu.equals("tax")) { %>
									<s:text name="jsp.ach.delete.business.tax.form.msg" />
<% } else { %>
									<s:text name="jsp.ach.delete.business.childsupport.form.msg" />
<% } %>
                    </ffi:cinclude>
                    <ffi:cinclude value1="${isBusiness}" value2="true" operator="notEquals">
<% if (subMenu.equals("tax")) { %>
									<s:text name="jsp.ach.delete.business.tax.form.msg" />
<% } else { %>
									<s:text name="jsp.ach.delete.business.childsupport.form.msg" />
<% } %>
                    </ffi:cinclude>
									</span></div>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center"><span class="sectionsubhead"><ffi:getProperty name="TaxForm" property="State" /> <ffi:getProperty name="TaxForm" property="Name" />
                    <ffi:cinclude value1="${isBusiness}" value2="true" operator="equals">
                        (<s:text name="jsp.ach.Business" />)
                    </ffi:cinclude>
                                </span>
							</td>
							<td nowrap>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3" height="40">&nbsp;</td>
						</tr>
					</table>
					<div class="ui-widget-header customDialogFooter">
					<input class="submitbutton" type="hidden" value="Cancel" onclick="document.location='taxforms.jsp';return false;">
												<sj:a 
										                button="true" 
														onClickTopics="closeDialog"
										        	><s:text name="jsp.default_82"/>
										        </sj:a>
					&nbsp;&nbsp;
<% if (subMenu.equals("tax")) { %>
					<input id="deleteTypeMessageId" class="submitbutton" type="hidden" value="Tax type has been deleted!">
												<sj:a id="deleteChildTaxTypeLink" 
										        	formIds="deleteChildTaxTypeFormId" 
										        	targets="resultmessage" 
										        	button="true"   
						  							title="Delete Tax Form" 
						  							onCompleteTopics="cancelTaxTypeCompleteTopics" 
						  							onSuccessTopics="cancelTaxTypeSuccessTopics" 
													onErrorTopics="cancelTaxTypeErrorTopics"
						  							effectDuration="1500" ><s:text name="jsp.default_162"/>
						  						</sj:a>
<% } else { %>
					<input id="deleteTypeMessageId" class="submitbutton" type="hidden" value="Child Support type has been deleted!">
					
										        <sj:a id="deleteChildTaxTypeLink" 
										        	formIds="deleteChildTaxTypeFormId" 
										        	targets="resultmessage" 
										        	button="true"   
						  							title="Delete Child Support Form" 
						  							onCompleteTopics="cancelChildspTypeCompleteTopics"
						  							onSuccessTopics="cancelChildspTypeSuccessTopics" 
													onErrorTopics="cancelChildspTypeErrorTopics" 
						  							effectDuration="1500" ><s:text name="jsp.default_162"/>
						  						</sj:a>
<% } %>
					</div>
					
					
		</div>
		
	</td>
			
      <td></td>
    </tr>
  </table>
  </s:form>
</div>
