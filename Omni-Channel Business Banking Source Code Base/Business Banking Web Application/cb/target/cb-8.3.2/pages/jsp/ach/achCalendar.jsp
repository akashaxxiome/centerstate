<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.util.DateConsts"%>	
	<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
    <ffi:process name="GetCurrentDate"/>			
    <%-- Format and display the current date/time --%>
   <ffi:setProperty name="GetCurrentDate" property="DateFormat" value="<%= DateConsts.FULL_CALENDAR_DATE_FORMAT %>"/>
	<ffi:setGridURL grid="ACH_Calendar_Event_URL" name="ViewURL" url="/cb/pages/jsp/ach/viewAchBatch_calendarData.action?ID={0}&BatchIsland=Scheduled&RecID={1}" parm0="tranId" parm1="recACHId"/>
	
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar-extra.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.print.css' media='print' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.overrides.css' />
	
	<!-- <script type='text/javascript' src='/cb/web/js/calendar/fullcalendar.js'></script> -->
	<style type="text/css">
		.fc td.fc-sat{
			width: 122px;
			}
	</style>
	
	<s:if test='%{"ach"==#session.subMenuSelected}'>
	<input type="hidden" id="moduleType" value="ACHBatch"/>	
	</s:if>
	<s:elseif test='%{"tax"== #session.subMenuSelected}'>
		<input type="hidden" id="moduleType" value="TaxPayment"/>
	</s:elseif>
	<s:elseif test='%{"child"==#session.subMenuSelected}'>
		<input type="hidden" id="moduleType" value="ChildSupportPayment"/>
	</s:elseif>
	<script type='text/javascript'>
		$(document).ready(function() {
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		var dateArray=[];
		var url = '/cb/pages/jsp/calendar/getCalendarEvents.action?moduleName=ACHCalendar&GridURLs=ACH_Calendar_Event_URL&moduleType=' +$('#moduleType').val(); 
		$('#achCalendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},			
			editable: true,
			disableDragging:true,
			events: url,
			eventClick: function(calEvent, jsEvent, view){
				if(view.name == "month"){
					moveToDay(calEvent.start);	
				}else if(view.name == "basicDay"){
					var temp = $(this);
					var isLoaded = false;
					for(var i =0;i<$.parseHTML(this.innerHTML).length;i++){
						if($.parseHTML(this.innerHTML)[i].localName =="div"
								&& $.parseHTML(this.innerHTML)[i].className.indexOf("marginTop10")!="-1"){
							isLoaded = true;
							break;
						}
					}
					 if(!isLoaded){
						 clearData();
					$.ajax({
						   url: "/cb/pages/jsp/ach/viewAchBatch_calendarData.action",
						   data:{
							   ID:calEvent.tranId,
							   BatchIsland:"Scheduled",
							   RecID:calEvent.recACHId,
							   isCalendar:true
						   },
						   success: function(data){
								   temp.append(data);
								   var lHeight = $(temp).parent().height();
								   $("div .fc-content").height(lHeight+50);
								  
								   $('.achCalendarDetails').remove();
								   $('.toggleClick').trigger('click');
								   $('.toggleClick').hide();
							}
						});
					 }
				}else{
					moveToDay(calEvent.start);	
				}
			},
					
			eventRender: function(event, element) {
				//removing css class for events o
				element.removeClass("fc-event fc-event-skin");
				element.find("div").removeClass("fc-event fc-event-skin");
				
				var view  =  $('#achCalendar').fullCalendar('getView');				
				if(view.name == "month"){
					var y = event.start;
					var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
					var cal = $("#achCalendar")[0];
					var data = $.data(cal,d.toString());
					if(dateArray.indexOf(d)=='-1'){
						dateArray.push(d);
						var holderDiv = "<div>";
						var text = '';
						for (var key in event.calendarDateData) {
							  if (event.calendarDateData.hasOwnProperty(key)) {
								 /*  text = text + holderDiv;
								  text= text+" " +event.calendarDateData[key] + " "+ key; */
								  if(event.calendarDateData[key] > 0) {
									  if(key == "Pending" || key == "Skipped") {
										  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarPendingApprovalRecordCls sapUiIconCls icon-history'>";
									  } else if(key == "Pending Approval") {
										  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarPendingRecordCls sapUiIconCls icon-pending'>";
									  } else if (key == "Completed") {
										  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
									  }
	
									  text = text + holderDiv;
									  text = text+" " +event.calendarDateData[key] + " "+ key;
									  text += "</div>";
								  }
							  }
						}
						$(".completedevent").css("position", "absolute");
						element.find('.fc-event-title').html(text);
						$.data(cal,d.toString(),data);
						}
					else{
						return false;
					}
				}
				if(view.name == "basicWeek") {
					text = "";
					key = event.statusName;
					
					if(key!=null && key!=''){

						if(key.trim() == "Pending" || key.trim() == "Scheduled" || key.trim() == "Skipped") {
							holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewPendingApprovalRecordCls sapUiIconCls icon-history'>";
						} else if(key.trim() == "Pending Approval"|| event.statusName.trim() == "Approval Pending" ) {
							holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewPendingRecordCls sapUiIconCls icon-pending'>";
						} else if (key == "Completed") {
							holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
						}

						text = text + holderDiv;
						text = text+" " +this.title+ "<br>Debit Amount "+ this.debitAmount+ "<br>Credit Amount "+ this.creditAmount; 
						text += "</div>";
						
						$(".completedevent").css("position", "absolute");
						element.find('.fc-event-title').html(text);	
					}
				}
				if(view.name == "basicDay"){
					text = "";
					var statusName = ""; 
					if(event.statusName.trim() == "Pending" || event.statusName.trim() == "Scheduled" || "Funds Allocated" == event.statusName.trim() || event.statusName.trim() == "Skipped") {
						statusName = "<div class='marginRight20 marginleft10 floatleft'>"+event.statusName+"<span class='marginleft5 calendarPendingApprovalRecordCls sapUiIconCls icon-history'></span></div>";
					} else if(event.statusName.trim() == "Pending Approval" || "Approval Pending" == event.statusName.trim()) {
						statusName = "<div class='marginRight20 floatleft'>"+event.statusName+"<span class='marginleft5 calendarPendingRecordCls sapUiIconCls icon-pending'></div>";
					} else if (event.statusName.trim() == "Completed") {
						statusName = "<div class='marginRight20 floatleft'>"+event.statusName+"<span class='marginleft5 calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'></span></div>";
					}
					var referenceNumber = "<div class='marginRight20 floatleft'>"+js_reference_number+": <span class='boldIt'>"+event.referenceNumber+"</span></div>";
					var account = "<div class='marginRight20 floatleft'>"+js_account+":  <span class='boldIt'>"+event.title+"<span class='marginleft10 sapUiIconCls hidden icon-arrow-right'></span></span></div>";
					var debitAmount = "<div class='marginRight20 floatleft'> Debit Amount : <span class='boldIt'> "+event.debitAmount+"</span></div>";
					var creditAmount = "<div class='marginRight20 floatleft'> Credit Amount : <span class='boldIt'> "+event.creditAmount +"</span></div>";
					
					var existingTitle = event.title;
					
					//var newTitle = statusName + "Reference Number: " + referenceNumber + existingTitle;
					var newTitle = "<span class='heading'>"+statusName+referenceNumber+account+debitAmount+creditAmount+"<span>";
					
					//$(".completedevent").addClass("completedeventDayView");
					$("div[class*='fc-view-basicDay'] div .fc-event-hori").addClass("completedeventDayView");
					element.find('.fc-event-title').html(newTitle);
				}
			},
			
			eventAfterRender :function( event, element, view ) { 
				var y = event.start;
				var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
				var cal = $("#achCalendar")[0];
				$.removeData(cal,d.toString());
				dateArray.length=0;
			},
			
			windowResize: function(view) {
				/* $('#achCalendar').fullCalendar('refetchEvents');
				$('#achCalendar').fullCalendar('render'); */
			}
			
		});	
		});
		function moveToDay(date){
			var toDate = new Date(date);
			$('#achCalendar').fullCalendar( 'changeView', 'basicDay' );
			$('#achCalendar').fullCalendar( 'gotoDate', toDate );
		}
				
		function getServerDate(){
		    var date = new Date('<ffi:getProperty name="GetCurrentDate" property="Date" />');
			return date;
		} 
		
		function clearData(){
			$('.marginTop10').each(function(){
				$(this).slideToggle();
				$(this).remove();
			});
		}
	</script>

<style type='text/css'>

		label {
			margin-top: 40px;
			text-align: left;
			font-size: 14px;
			font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
			}

		#achCalendar {
			width: 95%;
			margin: 0 auto;
			}
			
		/* style added to increase the height of the content when a transaction is expanded for DayView */
		
		.fc-view-basicDay {
			height: 100%;
		}
	
		.fc-border-separate {
		     height: 100%;
		}	
		
		.tableAlerternateRowColor{
			cursor: default;
		}
	</style>
	<div id='achCalendar'></div>
