<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-jquery-grid-tags" prefix="sjg"%>
<%@page contentType="text/html; charset=UTF-8" %>

<s:form id="ACHPayeeForm_Add2" validate="false" namespace="/pages/jsp/ach" action="addACHPayeeAction"
    method="post" name="FormName" theme="simple" >
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
            <div class="type-text">
                <s:include value="achpayeeaddeditconfirm.jsp"/>
            </div>
<div class="btn-row">
 		<%if(session.getAttribute("ManagedGridPayee") != null) { %>        
	        <sj:a 
	                button="true"
	                onClick="ns.ach.gotoManagedPayees();"
	        >
	                <s:text name="jsp.default_82"/> <!-- Cancel -->
	        </sj:a>
        <%} else { %>
        	<sj:a 
                button="true"
                summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHPayeeForm"
        >
                <s:text name="jsp.default_82"/> <!-- Cancel -->
        </sj:a>
        <%} %>

    <%if(session.getAttribute("ManagedGridPayee") != null) { %>
                <sj:a id="BackFormButtonOnInput3"
                        button="true"
                        onClickTopics="ach_backToACHEntry"
                >
                        <s:text name="jsp.default_57"/> <!-- Back -->
                </sj:a>
    <%}else { %>
                <sj:a id="BackFormButtonOnInput3"
                        button="true"
                        onClickTopics="ach_backToInput"
                >
                        <s:text name="jsp.default_57"/> <!-- Back -->
                </sj:a>
    <%} %>
    <%
    if(session.getAttribute("ManagedGridPayee") != null) { 
    	%>   
	    <sj:a
	        id="verifyACHPayeeSubmit3"
			formIds="ACHPayeeForm_Add2"
	        targets="confirmDiv"
	        button="true"
	        onBeforeTopics="beforeConfirmACHPayeeForm"
	        onErrorTopics="errorConfirmACHPayeeForm"
	        onSuccessTopics="successConfirmACHSecurePayeeForm"
	        ><s:text name="jsp.default_395" /></sj:a> <!--  Submit Payee -->
    <% }else { 
    	
    	%>
	    <sj:a
	        id="verifyACHPayeeSubmit3"
			formIds="ACHPayeeForm_Add2"
	        targets="confirmDiv"
	        button="true"
	        onBeforeTopics="beforeConfirmACHPayeeForm"
	        onCompleteTopics="confirmACHPayeeFormComplete"
	        onErrorTopics="errorConfirmACHPayeeForm"
	        onSuccessTopics="successConfirmACHPayeeForm"
	        ><s:text name="jsp.default_395" /></sj:a>  <!--  Submit Payee -->
    <%} %>
</div>
</s:form>

