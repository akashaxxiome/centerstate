<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.ach.ACHOffsetAccount,
				 com.ffusion.beans.ach.ACHClassCode" %>
<%--
	This file is used by both Add/Edit ACH AND Add/Edit ACH Templates
	If you modify this file, make sure your change works for both please.
--%>
<ffi:help id="payments_achbatchaddedit" className="moduleHelpClass"/>

<s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />
<script type="text/javascript" src="<s:url value='/web/js/ach/achAddEdit%{#session.minVersion}.js'/>"></script>

<%
	String subMenuSelected = "ach";
	boolean addEntryDisabled = false;
	boolean importedBatch = false;
	boolean isTemplate = false;
   boolean fromTemplate = false;

%>
<ffi:getProperty name="subMenuSelected" assignTo="subMenuSelected"/>
<ffi:cinclude value1="${AddEditACHBatch.CompanyID}" value2="" operator="equals">
    <% addEntryDisabled = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.CoID}" value2="0" operator="equals">
    <% addEntryDisabled = true; %>
</ffi:cinclude>

<ffi:cinclude value1="${AddEditACHBatch.FromTemplate}" value2="true" >
	<% fromTemplate = true; %>
</ffi:cinclude>


<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
	<% if (!fromTemplate)
        isTemplate = true;%>
</ffi:cinclude>

<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
<% isTemplate = true;%>
</s:if>
<%if (isTemplate) { %>
<ffi:cinclude value1="${subMenuSelected}" value2="ach" operator="equals" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_ach,addSingleTemplateLink</span>
	<span class="shortcutEntitlementClass" style="display: none;">Overall-ACH Template</span>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" operator="equals" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_tax,addSingleTemplateLink</span>
	<span class="shortcutEntitlementClass" style="display: none;">CCD + TXP</span>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" operator="equals" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_childsp,addSingleTemplateLink</span>
	<span class="shortcutEntitlementClass" style="display: none;">CCD + DED</span>
</ffi:cinclude>

<% } else { %>
<ffi:cinclude value1="${subMenuSelected}" value2="ach" operator="equals" >
	<span class="shortcutPathClass" style="display: none;">pmtTran_ach,addSingleACHBatchLink</span>
	<span class="shortcutEntitlementClass" style="display: none;">ACHBatch</span>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="tax" operator="equals" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_tax,addSingleTaxBatchLink</span>
	<span class="shortcutEntitlementClass" style="display: none;">CCD + TXP</span>
</ffi:cinclude>
<ffi:cinclude value1="${subMenuSelected}" value2="child" operator="equals" >
	<span class="shortcutPathClass" style="display:none;">pmtTran_childsp,addSingleChildspBatchLink</span>
	<span class="shortcutEntitlementClass" style="display: none;">CCD + DED</span>
</ffi:cinclude>
<%} %>


<ffi:setProperty name="CallerForm" value="AddBatchForm"/>
<ffi:setProperty name="ProcessACHImport" property="ClearACHEntries" value="false"/> <%-- just in case our referring page is the cancel button of the achimportconfirm page --%>
<ffi:removeProperty name="ImportErrors" />

<%  boolean anyEntries = false;
	String headerTitle = "";		// used in assignTo below
   	String headerValue = "";		// used in assignTo below
	String pageHeading = "";
	if (subMenuSelected.equals("tax"))
	{
		pageHeading = "<!--L10NStart-->Tax Payment<!--L10NEnd-->";
	} else
	if (subMenuSelected.equals("child"))
	{
		pageHeading = "<!--L10NStart-->Child Support Payment<!--L10NEnd-->";
	}
	else
	{
		pageHeading = "<!--L10NStart-->ACH Payment<!--L10NEnd-->";
	}
	session.setAttribute("PageHeading", "Add " + pageHeading + (isTemplate?" <!--L10NStart-->Template<!--L10NEnd-->":"") );
%>


<span id="PageHeading" style="display:none;"><%="" + session.getAttribute("PageHeading")%></span>

	<ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999"><ffi:setProperty name="OpenEnded" value="true" /></ffi:cinclude>
    <ffi:setProperty name="NumberPayments" value="${AddEditACHBatch.NumberPayments}" />
    <ffi:setProperty name="Frequency" value="${AddEditACHBatch.Frequency}" />

<ffi:cinclude value1="${OpenEnded}" value2="true" >
<ffi:setProperty name="NumberPayments" value="999"/>
<ffi:setProperty name="AddEditACHBatch" property="NumberPayments" value="${NumberPayments}"/>
</ffi:cinclude>

<div id="achBatchInputDiv">
<s:form id="AddBatchNew" namespace="/pages/jsp/ach" validate="false" action="%{strutsActionName + '_verify'}" method="post" name="AddBatchForm" theme="simple">
<s:hidden id="strutsActionName" value="%{strutsActionName}" />
<s:hidden id="achType" value="%{#session.AddEditACHBatch.ACHType}"></s:hidden>
<s:hidden id="fromTemplate" value="%{#session.AddEditACHBatch.FromTemplate}"></s:hidden>

			<% if (!isTemplate) { %>
					<s:include value="/pages/jsp/ach/inc/loadTemplate.jsp" />
			<% } else { %>
				<div class="leftPaneWrapper">
					<div class="leftPaneInnerWrapper">
						<div class="header"><h2 id="achSaveTemplateHeader"><s:text name="jsp.default_371" /></h2></div>
						<div class="leftPaneInnerBox leftPaneLoadPanel">
							<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
								<div class="inputBlockSection">
									<span id="achBatchNameLabel"class="sectionsubhead"><s:text name="ach.grid.templateName" /><span class="required">*</span></span><input type="hidden" name="template" value="true">
									<span class="columndata"><input id="AchBatchTemplateName" type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="32" border="0" name="AddEditACHBatch.TemplateName" value="<ffi:getProperty name="AddEditACHBatch" property="TemplateName"/>"></span>
									<span id="templateNameError"></span>
									<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="false" operator="equals" >
									<s:if test="%{#consumerUser}">
										<input type="hidden" name="AddEditACHBatch.BatchScope" value="USER"/>
									</s:if>
									<s:else>
									<span class="sectionsubhead marginTop10"><s:text name="jsp.default_418" /><span class="required">*</span></span>
									<span class="columndata">
										<select id="AddEditACHBatchBatchScopeID" class="txtbox" name="AddEditACHBatch.BatchScope">
											<s:if test="%{!#session.SecureUser.ConsumerUser}">
												<option value="BUSINESS" <ffi:cinclude value1="BUSINESS" value2="${AddEditACHBatch.BatchScope}" operator="equals" >selected</ffi:cinclude>><s:text name="jsp.default_77" /></option>
											</s:if>
											<!-- Start: Dual approval processing -->
											<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
												<option value="USER" <ffi:cinclude value1="USER" value2="${AddEditACHBatch.BatchScope}" operator="equals" >selected</ffi:cinclude>><s:text name="jsp.default_454" /></option>
											</ffi:cinclude>
											<!-- End: Dual approval processing -->
										</select>
									</span>
									</s:else>
									</ffi:cinclude>
								</div>
							</s:if>
						</div>
					</div>
				</div>
			<% } %>
<div class="rightPaneWrapper w71">
 <div class="paneWrapper">
   	<div class="paneInnerWrapper">
   	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
   	<input type="hidden" id="batchTypeId" name="batchTypeh" value="<ffi:getProperty name='subMenuSelected'/>"/>
		<%
									String title = "";
                                    String[] titleStrings;
	String[] achStrings = {
						      "<!--L10NStart-->Create ACH Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create ACH Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create ACH Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit ACH Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit ACH Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit ACH Batch (from template)<!--L10NEnd-->",
    };
	String[] taxStrings = {
						      "<!--L10NStart-->Create Tax Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create Tax Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create Tax Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit Tax Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Tax Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Tax Batch (from template)<!--L10NEnd-->",
    };
	String[] childStrings = {
						      "<!--L10NStart-->Create Child Support Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create Child Support Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create Child Support Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit Child Support Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Child Support Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Child Support Batch (from template)<!--L10NEnd-->",
    };
                                        int titleIndex = 0;
										if (subMenuSelected.equals("tax")) {
                                            titleStrings = taxStrings;
										} else if (subMenuSelected.equals("child")) {
                                            titleStrings = childStrings;
										} else {
                                            titleStrings = achStrings;
										}
   //    if (isView)
//        titleIndex = 6;
//    if (isDelete)
//        titleIndex = 9;
    if (isTemplate)
        titleIndex ++;
    else
    if (fromTemplate)
        titleIndex += 2;
    title = titleStrings[titleIndex];
    String tempEntToCheck;
	if (isTemplate) {
		if (subMenuSelected.equals("tax")) {
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_TEMPLATE );
		} else if (subMenuSelected.equals("child")) {
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_TEMPLATE );
		} else {
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_TEMPLATE );
		}
	} else
	{
		if (subMenuSelected.equals("tax")) {
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_PAYMENT_ENTRY );
		} else if (subMenuSelected.equals("child")) {
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_PAYMENT_ENTRY );
		} else {
			tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
		}
	}

    %>
		<div class="header"><h2 id="createACHBatchPanelHeader"> <%= title %></h2>
		<s:text name="jsp.default_371" />
			<div style="float:right; display: inline-block;">
				<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
					Batch Type :  <s:property value="%{selectedSECCodeDisplay}"/>
				</ffi:cinclude>
			</div>
		</div>
		<div class="paneContentWrapper">

<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="tableData formTablePadding5">
		<tr>
			<td id="achBatchCompanyLabel"><span class="sectionsubhead"><label for="AddEditACHBatchCompanyID" > <s:text name="ach.grid.companyName" /></label> </span><span class="required" title="required">*</span></td>
			<s:if test="%{#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
				<ffi:cinclude value1="${AddEditACHBatch.BatchImported}" value2="true" >
					<% importedBatch = true; %>
				</ffi:cinclude>
				<td><span class="sectionsubhead"><s:text name="jsp.default_481" /> </span><span class="required">*</span></td>
			</s:if>
			<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" ><td id="achBatchTypeLabel"><span class="sectionsubhead"><label for="standardEntryClassCodeId"> <s:text name="jsp.default_67" /></label> </span><span class="required" title="required">*</span></td></ffi:cinclude>
		</tr>
		<tr>
			<td class="columndata" width="50%">
				<ffi:cinclude ifEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" checkParent="true">
					<s:select list="AchCompanies" name="AddEditACHBatch.CoID" id="AddEditACHBatchCompanyID" value="%{#session.AddEditACHBatch.CoID}"
					listKey="ID" listValue="CompanyName +'-'+CompanyID" onchange='ns.ach.changeACHCompany(this.value);'
					headerKey="-1" headerValue="Please select ACH Company"  aria-labelledby="AddEditACHBatchCompanyID" aria-required="true"></s:select>
				</ffi:cinclude>
            </td>
            <s:if test="%{#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
            	<td class="columndata" width="50%">
		            <select class="txtbox" id="defaultTaxFormID" name="AddEditACHBatch.TaxFormID" onchange="changeChildspType();">
					<option value="-1"><s:text name="ach.child.support.type" /></option>
					<s:iterator value="taxForms" var="TaxForm">
							<option value="<ffi:getProperty name='TaxForm' property='ID'/>"
								<ffi:cinclude value1="${TaxForm.ID}" value2="${AddEditACHBatch.TaxFormID}">selected</ffi:cinclude>>
								<ffi:cinclude value1="${TaxForm.type}" value2="CHILDSUPPORT"><ffi:getProperty name="TaxForm" property="state" /> <s:text name="jsp.default_386" /></ffi:cinclude>
								<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /></b> - <ffi:getProperty name="TaxForm" property="TaxFormDescription" />
		                        <ffi:cinclude value1="${TaxForm.BusinessID}" value2="" operator="notEquals">
		                                         <s:text name="jsp.reports_526" />
		                        </ffi:cinclude>
							</option>
					</s:iterator>
					</select>
				</td>
            </s:if>
			<td class="columndata" width="50%">
				<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
					<s:select list="ClassCodeItems" listKey="id" listValue="label" name="AddEditACHBatch.StandardEntryClassCodeFromScreen" id="standardEntryClassCodeId"
					onchange="ns.ach.changeACHClassCode();" value="%{#session.AddEditACHBatch.StandardEntryClassCodeDebitBatch}"
					headerKey="-1" headerValue="Please select ACH Batch Type" aria-labelledby="standardEntryClassCodeId" aria-required="true"></s:select>
				</ffi:cinclude>
			</td>
		</tr>
		<tr>
			<td width="50%"><span id="coIDError"></span></td>
			<s:if test="%{#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
				<td width="50%"><span id="taxFormIDError"></span></td>
			</s:if>
			<td width="50%"><span id="standardEntryClassCodeError"></span></td>
		</tr>
<%-- Create a new table with two columns, the first column will be the left side,
the second column will be the right side (repeating and Offset Account) --%>

 	<tr>
           <td colspan="3">
            <div id="addEditVariableSection">
            	<s:include value="inc/achBatchAddEditVariableSection.jsp" />
            </div>
    	</td>
    </tr>

<tr>
<td class="sectionsubhead " colspan="3" align="left" nowrap>
<div class="marginTop20">
	<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
	<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
	<input id="setACHBatchTypeId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only formSubmitBtn' type="button"
	value="Set ACHBatch Type" onClick="openACHBatchTypeDialog();">
	&nbsp;
	  </ffi:cinclude>
	</ffi:cinclude>
	<input id="setAllMountsButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only formSubmitBtn' type="button"
	 value="Set All Amounts" onClick="setAllAmounts();">
	&nbsp;
	<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >


	<s:if test="%{!#session.SecureUser.ConsumerUser}">
	<input id="importAmmounts" class="formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" \

	name="Submit3" value="Import Amounts" onclick="ns.ach.importAmounts();">&nbsp;


	<s:if test="%{!#session.AddEditACHBatch.FromTemplateValue}">

	<input id="IMPORTENTRIES" class='formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= addEntryDisabled?"ui-state-disabled":"" %>'

	type="button" <%= addEntryDisabled?"disabled":"" %> name="Submit3" value="Import Entries" onclick="ns.ach.importEntries();">&nbsp;
	</s:if>
	</s:if>
	</ffi:cinclude>
	<s:if test="%{!#session.AddEditACHBatch.FromTemplateValue}">

	<input id="addEntryButtonId" class='formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= addEntryDisabled?"ui-state-disabled":"" %>' type="button" <%= addEntryDisabled?"disabled":"" %>
	name="addEntry" value="Add Entry" onclick="addACHEntry('<s:property value="%{#session.AddEditACHBatch.ACHType}"/>');">
	</s:if>
</div>
		</td>
	</tr>
</table>
<ffi:setProperty name="ACHEntries" property="FilterPaging" value="none"/>
<%-- <div class="nameSubTitle marginTop20">Entries</div>
<s:include value="%{#session.PagesPath}/ach/achbatchentryaddeditdetails.jsp"></s:include> --%>
</div>
</div>
</div>
<div class="paneWrapper marginTop20">
   	<div class="paneInnerWrapper">
		<div class="header"><h2 id="achEntrieslHeader"><s:text name="ach.grid.entries"/></h2></div>
<!-- ACHBatch Entry header and Grid details -->
<div id="achEntryDetails" class="paneContentWrapper">
	<s:include value="%{#session.PagesPath}/ach/achbatchentryaddeditdetails.jsp"></s:include>
</div>
</div>
</div>
</div>
<div class="clearBoth"></div>
<%-- <div class="paneWrapper marginTop20">
   	<div class="paneInnerWrapper">
		<div class="header">Entries</div>
<!-- ACHBatch Entry header and Grid details -->
<div id="achEntryDetails" class="paneContentWrapper">
	<s:include value="%{#session.PagesPath}/ach/achbatchentryaddeditdetails.jsp"></s:include>
</div>
</div>
</div> --%>
<div class="btn-row">
	<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
		<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
			<sj:a
                button="true"
				summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHTemplateForm"
        	><s:text name="jsp.default_82" />
        	</sj:a>
		</s:if>
		<s:else>
			<sj:a
                button="true"
				summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHForm"
        	><s:text name="jsp.default_82" />
        	</sj:a>
		</s:else>

	</ffi:cinclude>
	<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
						<sj:a
	                              button="true"
	                              onClickTopics="backToACHMultiBatchForm"
	                        ><s:text name="jsp.default_82" />
	                      	</sj:a>
	</ffi:cinclude>
							&nbsp;
    		<%-- <ffi:cinclude operator="notEquals" value1="true" value2="${AddEditACHBatch.EntryValidationErrors}"> --%>
	<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
		<sj:a
			id="verifyACHSubmit"
			formIds="AddBatchNew"
                           targets="inputDiv"
                           button="true"
                           validate="true"
                           validateFunction="ns.ach.batchCustomValidation"
                           onBeforeTopics="beforeVerifyACHForm"
                           onClickTopics="clickSaveToMultiTempForm"
                           onErrorTopics="errorAddACHTempForm"
                     ><s:text name="jsp.default_366" />
                   	</sj:a>
	</ffi:cinclude>

	<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
			<%-- <input type="hidden" name="isTemplateEdit" value='<ffi:getProperty name="isTemplateEdit"/>'>
			<sj:a
				id="verifyACHSubmit"
				formIds="AddBatchNew"
                            targets="resultmessage"
                            button="true"
                            validate="true"
                            validateFunction="customValidation"
                            onBeforeTopics="beforeVerifyACHForm"
                            onClickTopics="clickAddACHTempForm"
                            onCompleteTopics="completeAddACHTempForm"
                            onSuccessTopics="successAddACHTempForm"
                            onErrorTopics="errorAddACHTempForm"
                      >SUBMIT TEMPLATE
                    	</sj:a> --%>


			<sj:a
				id="verifyACHSubmit"
				formIds="AddBatchNew"
                            targets="verifyDiv"
                            button="true"
                            validate="true"
                            validateFunction="ns.ach.batchCustomValidation"
                            onBeforeTopics="beforeVerifyACHForm"
                            onCompleteTopics="verifyACHFormComplete"
				onErrorTopics="errorVerifyACHForm"
                            onSuccessTopics="successVerifyACHForm"
                            onClickTopics="clickVerifyACHForm"
                      ><s:text name="jsp.default_395" />
                    	</sj:a>
                   </ffi:cinclude>

<%-- </ffi:cinclude> --%>
<%--  <ffi:cinclude operator="equals" value1="true" value2="${AddEditACHBatch.EntryValidationErrors}">
							<sj:a
								id="verifyCheckACHSubmit"
								formIds="AddBatchNew"
                                targets="verifyDiv"
                                button="true"
                                validate="true"
                                validateFunction="ns.ach.batchCustomValidation"
                                onCompleteTopics="recheckACHFormComplete"
                                onClickTopics="clickVerifyACHForm"
		                        >RECHECK VALIDATION
                        	</sj:a>
<script>
	ns.ach.showErrorEntriesURL = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achbatchaddedit.jsp?ShowErrors=true"/>';
	</script>
							<sj:a
                                      id="showErrorRows"
                                      formIds="AddBatchNew"
                                      targets="inputDiv"
                                      button="true"
                                      onCompleteTopics="showErrorRowsACHFormComplete"
                                      onClickTopics="clickVerifyACHForm"
                                      >SHOW ERROR ENTRIES
                                  </sj:a>
                                  <span
                                      id="showingErrorRows"
                                      style="display:none;"
                                      >&nbsp;SHOWING ONLY ERROR ENTRIES
                                  </span>

          </ffi:cinclude> --%>
</div>
		</s:form>
</div>

<%-- This div will be used to show import Amount or import entries section. --%>
<div id="achBatchImportDiv"></div>

<s:if test="%{#session.AddEditACHBatch.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
<script type="text/javascript">
<!--
$(document).ready(function(){
changeChildspType();
});
//-->
</script>
</s:if>

<ffi:setProperty name="OpenEnded" value="false"/>
<ffi:removeProperty name="ModifyACHEntry" />
<ffi:removeProperty name="ACHEntryPayee" />
<ffi:removeProperty name="coEntrySecureDesc"/>
<ffi:removeProperty name="NumberPayments" />
<ffi:setProperty name="AddEditACHBatch" property="CurrentEntry" value=""/>
<script type="text/javascript">

$(document).ready(function(){
 $("#setACHBatchTypeId").button({ disabled: true });
});

</script>
