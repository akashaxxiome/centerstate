<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page import="com.ffusion.beans.ach.ACHBatch" %>


<%
	ACHBatch  batch=(ACHBatch)session.getAttribute("AddEditACHBatch");
	if(batch.getDateValue()==null)
	{
	batch.setDate((String)null);
	}
 %>


<sj:datepicker value="%{#session.AddEditACHBatch.Date}"
												id="AddEditACHBatchDateId"
												name="AddEditACHBatch.Date"
												label="Date"
												maxlength="10"
												buttonImageOnly="true"
												cssClass="ui-widget-content ui-corner-all" />
											<script>
												var urlStr = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=9&calDisplay=select&calForm=AddBatchForm&calTarget=AddEditACHBatch.Date&fromACHorTax=true&ACHCompanyID=${AddEditACHBatch.CoID}"/>';
												<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
													ns.common.enableAjaxDatepicker("AddEditACHBatchDateId", urlStr + '&SECCode=<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode"/><ffi:getProperty name="AddEditACHBatch" property="DebitBatch"/>');
												</ffi:cinclude>
												<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
													ns.common.enableAjaxDatepicker("AddEditACHBatchDateId", urlStr + '&SECCode=DED');
												</ffi:cinclude>
												<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
													ns.common.enableAjaxDatepicker("AddEditACHBatchDateId", urlStr + '&SECCode=TXP');
												</ffi:cinclude>
												$('img.ui-datepicker-trigger').attr('title','<s:text name='default_calendarTitleText'/>');
											</script>
