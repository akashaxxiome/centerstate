<%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
	
<div class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header"><h2 id="loadTemplatePanelHeader"><s:text name='jsp.default_264' /></h2></div>
		<div class="leftPaneInnerBox leftPaneLoadPanel">
			<select id="TemplateID" class="txtbox" name="TemplateID" onChange="ns.ach.enableLoadButton(this.value);">
				<option>
					<ffi:getProperty name="AddEditACHBatch" property="Name"/>
				</option>
			</select>
			<input id="loadButtonId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-disabled' style="display:none" type="button" disabled="true" name="load" value="Load" onclick="ns.ach.loadTemplate('<ffi:getProperty name='CSRF_TOKEN'/>','<ffi:getProperty name='ACHType'/>');"/>
		</div>
	</div>
</div>			
<script type="text/javascript">
<!--
		
		$("#TemplateID").lookupbox({
			"source":"/cb/pages/common/ACHLoadTempateAction.action",
			"controlType":"server",
			"size":"55",
			"module":"singleAchLoadTemplate"
		});
		
//-->
function enableLoadButton(selectedvalue)
	{
		$('#loadButtonId').trigger('click');
	}
</script>			

<style>

	#loadButtonId {
		font-family: arial !important;
	    font-size: 13px !important;
	    font-weight: normal !important;
	    margin-left: 5px !important;
	    padding: 5px !important;
	    top: 1px !important;
	}
	
</style>
