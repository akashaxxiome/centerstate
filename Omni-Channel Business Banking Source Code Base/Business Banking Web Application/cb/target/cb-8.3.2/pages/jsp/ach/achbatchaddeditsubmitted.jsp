<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achbatchaddeditsubmitted" className="moduleHelpClass"/>
<% String subMenuSelected = "ach";
  	boolean isEdit = true;
	boolean isTemplate = false;
	boolean fromTemplate = false;
	String secCodeDisplay = "";

	if ((session.getAttribute("AddEditACHBatch") instanceof com.ffusion.tasks.ach.AddACHBatch)
		||(session.getAttribute("AddEditACHBatch") instanceof com.ffusion.tasks.ach.AddACHTaxPayment)
		||(session.getAttribute("AddEditACHBatch") instanceof com.ffusion.tasks.ach.AddChildSupportPayment)
		) {
		isEdit = false;
	}
	if (session.getAttribute("AddEditACHTemplate") != null)
	{
		isTemplate = true;
		isEdit = false;
		if (session.getAttribute("AddEditACHTemplate") instanceof com.ffusion.tasks.ach.ModifyACHTemplate)
			isEdit = true;
	}
%>
<ffi:cinclude value1="${AddEditACHBatch.FromTemplate}" value2="true" >
	<% fromTemplate = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
	<% subMenuSelected = "tax";
    	secCodeDisplay = "<!--L10NStart-->Tax Payment (CCD + TXP Credit)<!--L10NEnd-->";
	%>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
	<% subMenuSelected = "child";
		secCodeDisplay = "<!--L10NStart-->Child Support Payment (CCD + DED Credit)<!--L10NEnd-->";
	%>
</ffi:cinclude>
<%
	String pageHeading = "";
	boolean canRecurring = true;
	String frequencyValue = "";		// used in assignTo below
	if (subMenuSelected.equals("tax"))
	{
		pageHeading = "<!--L10NStart-->Tax Payment<!--L10NEnd-->";
		canRecurring = false;
	} else
	if (subMenuSelected.equals("child"))
		pageHeading = "<!--L10NStart-->Child Support Payment<!--L10NEnd-->";
	else
		pageHeading = "<!--L10NStart-->ACH Payment<!--L10NEnd-->";
	session.setAttribute("PageHeading", pageHeading + " <!--L10NStart-->Confirm<!--L10NEnd--><br>");

    String headerTitle = "";
	String classCode = null;
%>
<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="classCode" />
    <ffi:getProperty name="AddEditACHBatch" property="FrequencyValue" assignTo="frequencyValue" />
<%
	if (classCode == null)
		classCode = "";
	classCode = classCode.toUpperCase();
	if ("ARC".equals(classCode) || "POP".equals(classCode) || 
		"BOC".equals(classCode) || "RCK".equals(classCode) || "CIE".equals(classCode))
			canRecurring = false;
	if (isEdit && "0".equals(frequencyValue))
		canRecurring = false;

%>
		<script language="JavaScript">
<!--

function openSaveTemplate(URLExtra)  {
	MyWindow=window.open(''+URLExtra,'','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=750,height=340');
}

// --></script>

					<table width="750" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="columndata lightBackground">
								<form action="<ffi:urlEncrypt url="${SecurePath}payments/achpayments.jsp?Refresh=true"/>" method="post" name="AddBatchForm">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<input type="hidden" name="PayeeID" size="15">
<input type="hidden" name="SavedNumberPayments" value="<ffi:getProperty name="AddEditACHBatch" property="NumberPayments" />">
								<div align="center">
								<table width="720" border="0" cellspacing="0" cellpadding="3">
									<tr valign="top">
										<td class="columndata" ><span class="sectionsubhead"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditsubmitted.jsp-1" parm0="${AddEditACHBatch.TrackingID}"/><br>
										<br></span>
									</td>
									</tr>
								</table>
								<table width="720" border="0" cellspacing="0" cellpadding="3">
									<tr valign="top">
									<tr class="lightBackground">
										<td class="sectionsubhead" align="right" width="213"><s:text name="ach.grid.companyName" /></td>
										<td class="columndata" align="left">
										<ffi:getProperty name="ACHCOMPANY" property="CompanyName" />&nbsp;-&nbsp;<ffi:getProperty name="ACHCOMPANY" property="CompanyID" />
										</td>
									</tr>
									<tr class="lightBackground">
										<td class="sectionsubhead" align="right" width="213"><s:text name="jsp.default_67" /></td>
										<td class="columndata" align="left">
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
								<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
								<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
								<ffi:process name="ACHResource" />
												<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}"/>
												<ffi:getProperty name="ACHResource" property="Resource"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
										<%= secCodeDisplay %>
</ffi:cinclude>

									    </td>
									</tr>
								</table>
								<table width="720" border="0" cellspacing="0" cellpadding="3">
								    <tr class="lightBackground">
<%-- FIRST COLUMN --%>
								        <td width="60%">
								            <table>
								                <tr>
										<td class="sectionsubhead" align="right" width="210">
								<% if (!isTemplate) { %>
											<s:text name="ach.grid.batchName"/ >
								<% } else { %>
											<s:text name="ach.grid.templateName" />
								<% } %></td>

								<% if (!isTemplate) { %>
										<td class="columndata lightBackground"><ffi:getProperty name="AddEditACHBatch" property="Name"/></td>
								<% } else { %>

<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
										<td class="columndata lightBackground"><ffi:getProperty name="AddEditACHBatch" property="TemplateName"/></td>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
										<td class="columndata lightBackground"><ffi:getProperty name="AddEditACHTemplate" property="TemplateName"/></td>
</ffi:cinclude>
								<% } %>
								                </tr>
								                <tr>
										<td class="sectionsubhead" align="right"><s:text name="ach.company.description" /></td>
										<td class="columndata">
											<ffi:getProperty name="AddEditACHBatch" property="CoEntryDesc"/>
										</td>
								                </tr>
<% if (!"IAT".equals(classCode)) { %>
    <tr>
	<td class="sectionsubhead" align="right">
			<s:text name="ach.Discretionary.Data" /> 
	</td>
	<td class="columndata">
			<ffi:getProperty name="AddEditACHBatch" property="CoDiscretionaryData"/>
	</td>
	</tr>
<% } else {
// IAT doesn't have Discretionary Data
%>
    <tr>
	<td class="sectionsubhead" align="right">
			<s:text name="ach.originating.currency" />
	</td>
	<td class="columndata">
			<s:text name="ach.USD.United.States.Dollar" />
	</td>
	</tr>
            <tr >
                <td align="right">
        <span class="sectionsubhead"><s:text name="da.field.ach.destinationCountry" /> </span>
                </td>
                <td class="columndata">
                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                    <ffi:process name="ACHResource" />

                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${AddEditACHBatch.ISO_DestinationCountryCode}"/>
                    <ffi:getProperty name="ACHResource" property="Resource"/>

                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
            <tr>
                <td align="right">
        <span class="sectionsubhead"><s:text name="ach.Destination.Currency" /> </span>
                </td>
                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                    <ffi:process name="ACHResource" />
                <td class="columndata">
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${AddEditACHBatch.ISO_DestinationCurrencyCode}"/>
                    <ffi:getProperty name="AddEditACHBatch" property="ISO_DestinationCurrencyCode"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
            <tr>
                <td align="right">
        <span class="sectionsubhead"><s:text name="da.field.ach.foreignExchangeMethod" /> </span>
                </td>
                <td class="columndata">
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${AddEditACHBatch.ForeignExchangeIndicator}"/>
                    <ffi:getProperty name="AddEditACHBatch" property="ForeignExchangeIndicator"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
<% } %>
								                <tr>
										<td class="sectionsubhead" align="right">
										<% if (!isTemplate) { %>
											<s:text name="ach.grid.effectiveDate" />
										<% } else {
										// NO Date required for Templates
										%>
											&nbsp;
										<% } %></td>
										<td class="columndata">
										<% if (!isTemplate) { %>
	                                		<ffi:getProperty name="AddEditACHBatch" property="Date" />
										<% } else {
										// NO Date required for Templates
										%>
											&nbsp;
										<% } %> </td>
								                </tr>
		<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHHeaderCompanyName${AddEditACHBatch.StandardEntryClassCode}"/>
		<ffi:getProperty name="ACHResource" property="Resource" assignTo="headerTitle" />
<% if (headerTitle != null && headerTitle.length() > 0) { %>
									<tr class="lightBackground">
										<td class="sectionsubhead" align="right">
											<%=	headerTitle %>
										</td>
										<td class="columndata">
											<ffi:getProperty name="AddEditACHBatch" property="HeaderCompName"/>
										</td>

									</tr>
<% } %>
									<tr class="lightBackground">
										<td class="sectionsubhead" align="right">
											<s:text name="jsp.default_388" />
										</td>
										<td class="columndata">
											<ffi:getProperty name="AddEditACHBatch" property="Status"/>
										</td>
									</tr>
								            </table>
								        </td>



<%-- SECOND COLUMN --%>
								        <td width="40%">
								            <table>
								                <tr>
										<% if (canRecurring) { %>
										<td class="sectionsubhead tbrd_r" rowspan="4" width="10">&nbsp;</td>
										<td class="sectionsubhead" rowspan="4" width="10">&nbsp;</td>
										<td class="sectionsubhead" align="right"><s:text name="jsp.default_351" /></td>
										<td class="columndata"><ffi:getProperty name="Frequency"/></td>
										<% } %>
								                </tr>
								                <tr>
										<% if (canRecurring) { %>
										<td class="sectionsubhead" colspan="2" align="right"><input type="radio" name="OpenEnded" disabled value="true" border="0" <ffi:cinclude value1="${NumberPayments}" value2="999">checked</ffi:cinclude>></td>
										<td class="sectionsubhead"><s:text name="jsp.default_446" /></td>
										<% } %>
								                </tr>
								                <tr>
										<% if (canRecurring) { %>
										<td class="sectionsubhead" colspan="2" align="right"><input disabled type="radio" name="OpenEnded" value="false" border="0" <ffi:cinclude value1="${NumberPayments}" value2="999" operator="notEquals" >checked</ffi:cinclude>></td>
										<td class="sectionsubhead"><s:text name="jsp.default_2" /> <ffi:getProperty name="NumberPayments"/></td>
										<% } %>
								                </tr>
								                <tr>
											<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="equals">
<% if ("IAT".equals(classCode))
 {
// QTS 577543: If IAT, we don't support Batch Balanced or Entry Balanced batches
%>
                                        <td class="sectionsubhead" align="right" colspan='3'>
                                        <s:text name="ach.IAT.noOffsetAcct" />
                                        </td>
 <% } else { %>
												<td class="sectionsubhead" colspan="2" align="right"><s:text name="jsp.default_494" /></td>
												<td class="columndata">
													<ffi:setProperty name="ACHOffsetAccounts" property="Filter" value="ID=${AddEditACHBatch.OffsetAccountID}" />
													<ffi:list collection="ACHOffsetAccounts" items="OffsetAccount">
														<ffi:getProperty name="OffsetAccount" property="Number"/>-<ffi:getProperty name="OffsetAccount" property="Type"/>
													</ffi:list>
													<ffi:setProperty name="ACHOffsetAccounts" property="Filter" value="All" />
												</td>
<% } %>
											</ffi:cinclude>
											<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="notEquals">
												<td class="sectionsubhead " align="right">&nbsp;</td>
												<td colspan="2">&nbsp;</td>
											</ffi:cinclude>
								                </tr>
								            </table>
								        </td>
								    </tr>
								</table>
								<table width="720" border="0" cellspacing="0" cellpadding="3">
									<tr class="lightBackground">
										<td height="20"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="157" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="87" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
									</tr>
									<tr>
										<td class="tbrd_b" colspan="6">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
	    <td class="sectionhead" rowspan="2" width="36%"><span class="sectionhead">&gt;&nbsp;<s:text name="ach.grid.entries" /><br></span></td>

		<td class="sectionsubhead" valign="center" width="16%">
			<s:text name="jsp.default_513"/>
		</td>
		<td class="sectionsubhead" width="16%">
            <input type="text" disabled name="TotalNumberCredits" maxlength="6" size="8" value="<ffi:getProperty name="TotalNumberCredits" />" class="ui-widget-content ui-corner-all txtbox">
        </td>
		<td class="sectionsubhead" width="16%">
			<s:text name="jsp.default_514"/>
		</td>
		<td class="sectionsubhead" width="16%">
			<input type="text" disabled name="TotalNumberDebits" maxlength="6" size="8" value="<ffi:getProperty name="TotalNumberDebits" />" class="ui-widget-content ui-corner-all txtbox">
		</td>
		</tr>
		<tr>
		<td class="sectionsubhead" width="16%">
			<s:text name="jsp.default_433"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
		</td>
		<td class="sectionsubhead" width="16%">
            <input type="text" disabled name="TotalCreditAmount" maxlength="15" size="15" value="<ffi:getProperty name="TotalCreditAmount" />" class="ui-widget-content ui-corner-all txtbox">
		</td>
		<td class="sectionsubhead" width="16%">
			<s:text name="jsp.default_433"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
		</td>
		<td class="sectionsubhead" width="16%">
			<input type="text" disabled name="TotalDebitAmount" maxlength="15" size="15" value="<ffi:getProperty name="TotalDebitAmount" />" class="ui-widget-content ui-corner-all txtbox">
		</td>
		</tr>
		</table>

										</td>
									</tr>
									<tr class="lightBackground">
										<td height="20"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="157" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="87" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
									</tr>
									<tr class="lightBackground">
										<td class="sectionsubhead" align="right">
											<s:text name="jsp.ach.Batch.submitted.on" />
										</td>
										<td class="columndata">
<%
	    com.ffusion.beans.DateTime date = new com.ffusion.beans.DateTime();
    	if( date != null ) {
    		date.setFormat( "M/d/yyyy hh:mm a" );
    	}
%>
											<%= date.toString() %>
										</td>
									</tr>
								</table>
<%-- don't display the entries on the Submitted Page as per requirements document
<ffi:setProperty name="CallerURL" value="${SecurePath}payments/achbatchaddeditsubmitted.jsp"/>
<ffi:include page="${PathExt}payments/achbatchaddeditentries.jsp" />
--%>
								</form>
							</td>
						</tr>
					</table>
<ffi:setProperty name="OpenEnded" value="false"/>
