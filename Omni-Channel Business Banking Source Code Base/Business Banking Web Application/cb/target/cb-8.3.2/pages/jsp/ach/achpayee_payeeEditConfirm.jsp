<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-jquery-grid-tags" prefix="sjg"%>
<%@page contentType="text/html; charset=UTF-8" %>

<s:form id="ACHPayeeForm_Edit2" validate="false" namespace="/pages/jsp/ach" action="editACHPayeeAction"
    method="post" name="FormName" theme="simple" >
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
            <div class="type-text">
                <s:include value="achpayeeaddeditconfirm.jsp"/>
            </div>
    <div class="btn-row">
            <%if(session.getAttribute("ManagedGridPayee") != null) { %>        
	        <sj:a 
	                button="true"
	                onClick="ns.ach.gotoManagedPayees();"
	        >
	                 <s:text name="jsp.default_82"/>
	        </sj:a>
        <%} else if(session.getAttribute("IsManagedParticipants") != null) { %>        
	        <sj:a 
	                button="true"
                    onClick="ns.ach.cancelManagedPayee('/cb/pages/jsp/ach/editACHEntryAction_cancelManagedParticipants.action');"
	        >
	                <s:text name="jsp.default_82"/>
	        </sj:a>
        <%} else { %>
        	<sj:a 
                button="true"
                summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHPayeeForm"
        >
                <s:text name="jsp.default_82"/>
        </sj:a>
        <%} %>
    <%if(session.getAttribute("IsManagedParticipants") != null) { %>
                <sj:a id="BackFormButtonOnInput3"
                        button="true"
                        onClickTopics="ach_backToACHEntry"
                >
                        <s:text name="jsp.default_57"/>
                </sj:a>
    <%}else { %>
                <sj:a id="BackFormButtonOnInput3"
                        button="true"
                        onClickTopics="ach_backToInput"
                >
                        <s:text name="jsp.default_57"/>
                </sj:a>
    <%} %>
                <%
                	if(session.getAttribute("IsManagedParticipants") != null) {
                %>
	                <sj:a
	                    id="verifyACHPayeeSubmit3"
						formIds="ACHPayeeForm_Edit2"
	                    targets="confirmDiv"
	                    button="true"
	                    onBeforeTopics="beforeConfirmACHPayeeForm"
	                    onErrorTopics="errorConfirmACHPayeeForm"
	                    onSuccessTopics="successConfirmACHSecurePayeeForm"	                    
	                ><s:text name="jsp.default_395" /></sj:a>
                <%
                	} else {
                %>
                	<sj:a
	                    id="verifyACHPayeeSubmit3"
						formIds="ACHPayeeForm_Edit2"
	                    targets="confirmDiv"
	                    button="true"
	                    onBeforeTopics="beforeConfirmACHPayeeForm"
	                    onCompleteTopics="confirmACHPayeeFormComplete"
	                    onErrorTopics="errorConfirmACHPayeeForm"
	                    onSuccessTopics="successConfirmACHPayeeForm"
	                ><s:text name="jsp.default_395" /></sj:a>
	            <%
                	}
	            %>
</div>
</s:form>

