<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

	<s:set var="StrutsAction" value="%{'addACHBatchTemplate'}"/>


   <ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="equals" >
				   		<s:set var="StrutsAction" value="%{'addACHBatchTemplate'}"/>
	</ffi:cinclude>	
	<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" operator="equals" >
						<s:set var="StrutsAction" value="%{'addChildSupportTemplate'}"/>
	</ffi:cinclude>	
	<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" operator="equals" >
					<s:set var="StrutsAction" value="%{'addTaxBatchTemplate'}"/>
	</ffi:cinclude>	





<ffi:help id="payments_achbatchaddeditconfirm_saveastemplate" className="moduleHelpClass"/>

			<div align="center " class="leftPaneLoadPanel">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr class="ltrow2_color">
						<td></td>
						<td class="columndata" class="ltrow2_color">
						<s:form id="AddTemplateNew" namespace="/pages/jsp/ach" validate="false" action="%{#StrutsAction +'_saveAsTemplate'}" method="post" name="AddTemplate" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
								<div align="center">
									<table width="100%" border="0" cellspacing="0" cellpadding="3">
										<%-- <tr>
											<td class="tbrd_b ltrow2_color" colspan="6" align="left"><span class="sectionhead">&gt;&nbsp;
											<ffi:cinclude value1="${subMenuSelected}" value2="ach" >
																							<!--L10NStart-->ACH Payments<!--L10NEnd-->
											</ffi:cinclude>
											<ffi:cinclude value1="${subMenuSelected}" value2="tax" >
																							<!--L10NStart-->Tax Payments<!--L10NEnd-->
											</ffi:cinclude>
											<ffi:cinclude value1="${subMenuSelected}" value2="child" >
																							<!--L10NStart-->Child Support Payments<!--L10NEnd-->
											</ffi:cinclude>
												<br>
												</span></td>
										</tr> --%>
										<tr class="ltrow2_color">
											<td class="columndata sectionsubhead">
												<s:text name="jsp.default_290" />
											</td>
										</tr>
										<tr>
											<td class="columndata">
												<input type="text" name="AddEditACHBatch.templateName" size="40" maxlength="32" class="ui-widget-content ui-corner-all txtbox"/><span id="templateNameError"></span>
												<input type="Hidden" name="AddEditACHTemplate.Process" value="true">
												<input type="Hidden" name="ProcessAndClose" value="true">
											</td>
										</tr>
										<tr class="ltrow2_color">
											<td class="columndata ltrow2_color">
												<span class="sectionsubhead"><s:text name="jsp.default_418"/></span>
											</td>
										</tr>
										<tr class="ltrow2_color">
											<td class="columndata">
											<select id="batchScopeTemplateId" class="txtbox" name="AddEditACHBatch.batchScope">
											<s:if test="%{!#session.SecureUser.ConsumerUser}">
												<s:if test="%{BusinessScopeAllowed}">
													<option value="BUSINESS" ><s:text name="jsp.default_77" /></option>
												</s:if>
												</s:if>
	
													<%-- Start: Dual approval processing --%>
													<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
														<option value="USER" ><s:text name="jsp.default_454"/></option>
													</ffi:cinclude>
													<%-- End: Dual approval processing --%>												
											</select>
											</td>
										</tr>
										<tr>
											<td><span id="batchScopeError"></span></td>
										</tr>
										<tr class="ltrow2_color">
											<td><div align="center">
												<%-- 
												<input type="button" name="Button" value="CANCEL" class="submitbutton" onClick="document.forms['AddTemplate'].action='<ffi:urlEncrypt url="${SecurePath}payments/achtemplateaddeditsave.jsp?Cancel=true"/>'; document.forms['AddTemplate'].submit();return false;">
								                <input type="button" name="Submit222" value="SAVE TEMPLATE" onClick="document.forms['AddTemplate'].submit();return false;" class="submitbutton">
												--%>
               									<%-- <sj:a 
									                button="true" 
													onClickTopics="ach_removeAddEditACHTemplateObject"
									        	><s:text name="jsp.default_82"/>
									        	</sj:a> --%>
									        	<sj:a 
													id="verifyACHTemplateNameSubmit"
													formIds="AddTemplateNew"
					                                targets="resultmessage" 
					                                button="true" 
													validate="true"
													validateFunction="customValidation"
													onclick="removeValidationErrors();"
					                                onCompleteTopics="closeDialog"
					                                onSuccessTopics="successSaveAsTemplateTopic"
							                        ><s:text name="jsp.default_366" />
					                        	</sj:a>
												</div></td>
										</tr>
									</table>
								</div>
							</s:form>
						</td>
						<td></td>
					</tr>
				</table>
</div>
<script type="text/javascript">
<!--
	$("#batchScopeTemplateId").selectmenu({width: '92%'});
//-->
</script>
