<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

    <span id="normalInputTab" style="display:none;"><s:text name="jsp.default_390"/></span>
    <span id="normalVerifyTab" style="display:none;"><s:text name="jsp.default_392"/></span>
    <span id="normalConfirmTab" style="display:none;"><s:text name="jsp.default_393"/></span>
    <span id="uploadInputTab" style="display:none;"><s:text name="ach.fileUploadStep1"/></span>
    <span id="uploadVerifyTab" style="display:none;"><s:text name="ach.fileUploadStep2"/></span>
    <span id="viewInputTab" style="display:none;"><s:text name="ach.viewBatchTab"/></span>
    <span id="deleteInputTab" style="display:none;"><s:text name="ach.deleteBatchTab"/></span>
    <span id="templateInputTab" style="display:none;"><s:text name="ach.templateBatchTab"/></span>
    <span id="templateVerifyTab" style="display:none;"><s:text name="ach.templateBatchVerifyTab"/></span>
    <span id="templateConfirmTab" style="display:none;"><s:text name="ach.templateBatchConfirmTab"/></span>
	<div id="detailsPortlet" class="portlet wizardSupportCls ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">
        <div class="portlet-header ui-widget-header ui-corner-all">
        	<h1 id="achDetailHeader" class="portlet-title"><s:text name="ach.addBatchHeader"/></h1>
		</div>
        <div class="portlet-content">

		    <sj:tabbedpanel id="ACHTransactionWizardTabs" >
		        <sj:tab id="inputTab" target="inputDiv" label="%{getText('jsp.default_390')}"/>
		        <sj:tab id="verifyTab" target="verifyDiv" label="%{getText('jsp.default_392')}"/>
		        <sj:tab id="confirmTab" target="confirmDiv" label="%{getText('jsp.default_393')}"/>
		        <div id="inputDiv" style="border:1px">
					Enter ACH Batch Information
		            <br><br>
		        </div>
		        <div id="verifyDiv" style="border:1px">
		            Verify ACH Batch Information
		            <br><br>
		        </div>
		        <div id="confirmDiv" style="border:1px">
		            Confirm ACH Batch Information
		            <br><br>
		        </div>
		    </sj:tabbedpanel>
		     <div id="achAddEditEntrySection" style="display: hidden;">
		     </div>   
		    
		</div>
		<div id="transferDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	       		  <li><a href='#' onclick="ns.common.bookmarkThis('detailsPortlet')"><span class="sapUiIconCls icon-create-session" style="float:left;"></span>Bookmark</a></li>
	              <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
    </div>
<script>    
	
	//Initialize portlet with settings icon
	ns.common.initializePortlet("detailsPortlet");
</script>