<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<ffi:help id="payments_achbatchtaxpayentry" className="moduleHelpClass"/>


<%if (request.getParameter("depositAccountID") != null || request.getParameter("EntryTaxFormsID") != null) { %>
	<s:action namespace="/pages/jsp/ach" name="ACHInitAction_initACHBatches" ignoreContextParams="true"/>
		<% }else{%>
		<s:action namespace="/pages/jsp/ach" name="ACHInitAction_initACHBatches" />
		<%}%>
<%
	boolean noneSelected = true;
	boolean fromTemplate = false;
	boolean deleteEntry = false;
    boolean addEntry = false;
	boolean isTemplate = false;
	if (session.getAttribute("AddEditACHTemplate") != null)
	{
		isTemplate = true;
	}
	if (request.getAttribute("DeleteEntry") != null){
		deleteEntry = true;
	}

%>
<ffi:cinclude value1="${AddEditACHBatch.FromTemplate}" value2="true" >
	<% fromTemplate = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${ModifyACHEntry.Action}" value2="ADD" >
    <% addEntry = true; %>
</ffi:cinclude>


		<% String pageHeading = "";%>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
		<% if (deleteEntry) {
				pageHeading = "<!--L10NStart-->Delete Tax Payment Entry<!--L10NEnd-->"; %>
				<title><ffi:getProperty name="product"/>: <s:text name="jsp.ach.Delete.Tax.Payment.Entry" /></title>
        <% } else if (addEntry) { %>
                <title><ffi:getProperty name="product"/>: <s:text name="jsp.ach.Add.Tax.Payment.Entry" /></title>
                <% pageHeading = "<!--L10NStart-->Add Tax Payment Entry<!--L10NEnd-->"; %>
		<% } else { %>
                <title><ffi:getProperty name="product"/>: <s:text name="jsp.ach.Edit.Tax.Payment.Entry" /></title>
                <% pageHeading = "<!--L10NStart-->Edit Tax Payment Entry<!--L10NEnd-->"; %>
		<% } %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
		<% if (deleteEntry) {
			pageHeading = "<!--L10NStart-->Delete Child Support Payment Entry<!--L10NEnd-->"; %>
			<title><ffi:getProperty name="product"/>: <s:text name="jsp.ach.Delete.Child.Support.Payment.Entry" /></title>
		<% } else if (addEntry) { %>
            <title><ffi:getProperty name="product"/>: <s:text name="jsp.ach.Add.Child.Support.Payment.Entry" /></title>
            <% pageHeading = "<!--L10NStart-->Add Child Support Payment Entry<!--L10NEnd-->"; %>
        <% } else { %>
            <title><ffi:getProperty name="product"/>: <s:text name="jsp.ach.Edit.Child.Support.Payment.Entry" /></title>
            <% pageHeading = "<!--L10NStart-->Edit Child Support Payment Entry<!--L10NEnd-->"; %>
		<% } %>
</ffi:cinclude>

		<% String batchName = "";
           String companyName = "";
           String companyID = "";
        %>
		<ffi:getProperty name="AddEditACHBatch" property="Name" assignTo="batchName" />
        <ffi:getProperty name="AddEditACHBatch" property="CoName" assignTo="companyName" />
        <ffi:getProperty name="AddEditACHBatch" property="CompanyID" assignTo="companyID" />
		<% if (isTemplate) { %>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
            <ffi:getProperty name="AddEditACHBatch" property="TemplateName" assignTo="batchName"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
	    	<ffi:getProperty name="AddEditACHTemplate" property="TemplateName" assignTo="batchName"/>
</ffi:cinclude>
		<% }
			pageHeading += "<br><span class='submain'>"+(isTemplate?"<!--L10NStart-->Template Name:<!--L10NEnd-->":"<!--L10NStart-->Batch Name:<!--L10NEnd-->")+com.ffusion.util.HTMLUtil.encode(batchName)+"  (" + companyName + " - " + companyID + ")</span>";
		%>
		<ffi:setProperty name="PageHeading" value="<%=pageHeading%>" />


<script language="JavaScript">
<!--
function prenoteChanged( value )
{
	frm = document.TaxPayForm;
	if(frm.elements){
		frm.elements["prenote"].value = value;
	    if (value == true)
	    {
	        totalAmounts();
	    }
	}
}

function thirdPartyChanged( value )
{
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
    return;     // nothing to do.
</ffi:cinclude>
	var frm = document.TaxPayForm;
	if(frm.elements){
		frm.elements["ThirdParty"].value = value;
	}
    thirdPartyShow();
    totalAmounts();
}
function thirdPartyShow(  )
{
    // go through the document and show/hide the rows that are TPP or TXP class
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
    return;     // nothing to do.
</ffi:cinclude>
<ffi:cinclude value1="${CurrentTaxForm.type}" value2="FEDERAL" operator="equals">
    return;     // nothing to do.
</ffi:cinclude>
    var frm = document.TaxPayForm;
    var hidePrefix = "TPP";
    var showPrefix = "TXP";
    if(frm.elements){
<ffi:cinclude value1="${CurrentAddenda.ThirdParty}" value2="true" >
    if ( frm.elements["ThirdParty"] == null)
    {
        hidePrefix = "TXP";
        showPrefix = "TPP";
    }
</ffi:cinclude>
    //alert(frm.elements["CurrentTaxForm.ThirdParty"].value);
    if ( frm.elements["ThirdParty"] != null && frm.elements["ThirdParty"].value == "true" )        // Third Party, hide TXP
    {
        // TXP, hide Third Party
        hidePrefix = "TXP";
        showPrefix = "TPP";
    }
    // disable the hidePrefix, show the showPrefix, use the classname
    $("."+showPrefix).show();
    $("."+hidePrefix).hide();
    // set amounts being hidden to empty amounts for TOTAL AMOUNT to work
    $("."+hidePrefix+" .N2" ).val("");
    }

    // OLD WAY, not jQuery...
//    for (var i=1; i < 11; i++)
//    {
//        var num = "0" + i;
//        if (i > 9) num = "" + i;
//        if (document.getElementById(hidePrefix+num) != null)
//            document.getElementById(hidePrefix+num).style.display = "none";        // hide for Managed Participants
//        if (document.getElementById(hidePrefix+num+"Error") != null)
//            document.getElementById(hidePrefix+num+"Error").style.display = "none";        // hide for Managed Participants
//
//        if (document.getElementById(showPrefix+num) != null)
//            document.getElementById(showPrefix+num).style.display = "";        // show
//        if (document.getElementById(showPrefix+num+"Error") != null)
//            document.getElementById(showPrefix+num+"Error").style.display = "";        // show
//    }

}

function disableAmounts() {
	var frm = document.TaxPayForm;
	var options = "";
	var required = "";
    var mathfunction = "";
	var fieldName = "";
	var dataType = "";
	var prevIDValue = "";
    var isDelete = "<%=deleteEntry%>";
    if (isDelete == "true")
        return;             // don't do anything!!! we are delete, everything should be disabled
	var prevType = "";
    var txp02Value = "<ffi:getProperty name="ModifyACHEntry" property="Addenda.TXP02" />";
    var disableNonRequiredAmounts = "";
    var taxBreakupAmountsHidden = "false";
    var isFederal = false;
    <ffi:cinclude value1="${CurrentTaxForm.type}" value2="FEDERAL" operator="equals">
    	isFederal = true;
    </ffi:cinclude>
    if(frm.length > 0 && frm.elements){
		for( var i=0; i < frm.length; i++ ) {
			var curr_name=frm.elements[i].name;
			if (curr_name.indexOf("options") >= 0)
			{
	            options = frm.elements[i].value;  // format = "RXNNFFFFFF" where R=required T/F, X=+0- Add or subtract, NN=DataType, FFFFFF = fieldName
				required = options.substring(0,1);
	            mathfunction = options.substring(1,2);
	            dataType = options.substring(2,4);
	            fieldName= options.substring(4);
			} else if (fieldName.length != 0)
			{
	            if (txp02Value == frm.elements[i].value)
	                disableNonRequiredAmounts = "true";
				if (dataType == "ID" && required== "F" )	// this will only get set if EDITABLE, meaning a drop-down
				{
					// if ID value == none, disable amount
					prevIDValue = frm.elements[i].value;
					prevType = "ID";
				}
				if (curr_name=="TXP05" ||
					curr_name=="TXP07" ||
					curr_name=="TXP09" ||
					curr_name=="DED04")
					{
						if (dataType=="N2" && required=="F" )
						{
							containerFieldID = curr_name.substring(curr_name.indexOf(".")+1, curr_name.length) + "Container";
	                        if (disableNonRequiredAmounts == "true")
	                        {
	                        	frm.elements[i].disabled = true;
	                            frm.elements[i].value = "";			// value is not important if disabled
	                            if(isFederal) {
		                            frm.elements[i].style.visibility = "hidden";
		                            if (document.getElementById(containerFieldID) != null) {
		                                document.getElementById(containerFieldID).style.display = "none";
		                            }
		                            taxBreakupAmountsHidden = "true";
	                            }
	                        } else
							if (prevIDValue != null && prevIDValue.length == 0 && prevType.length != 0)
							{
								frm.elements[i].disabled = true;
								frm.elements[i].value = "";			// value is not important if disabled
								if(isFederal) {
									frm.elements[i].style.visibility = "visible";
									if (document.getElementById(containerFieldID) != null) {
		                                document.getElementById(containerFieldID).style.display = "";
		                            }
									taxBreakupAmountsHidden = "false";
								}
							}
							else {
								frm.elements[i].disabled = false;
								if(isFederal) {
									frm.elements[i].style.visibility = "visible";
									if (document.getElementById(containerFieldID) != null) {
		                                document.getElementById(containerFieldID).style.display = "";
		                            }
									taxBreakupAmountsHidden = "false";
								}
							}
						}
						// set label text for Social Security Amount field
						
						if(isFederal) {
							if(curr_name=="AddEditACHBatch.TXP05" && dataType=="N2") {
								labelFieldID = "TXP05Label";
								if(disableNonRequiredAmounts == "true") {
									//set label as "Total Tax Amount"
									if (document.getElementById(labelFieldID) != null) {
										document.getElementById(labelFieldID).innerHTML = "Total Amount";
									}
								} else {
									//set label back to original label value
									if (document.getElementById(labelFieldID) != null) {
										document.getElementById(labelFieldID).innerHTML = fieldName;
									}
								}
							}
						}
						prevIDValue = "";		// clear it out
						prevType = "";
					}
				fieldName = "";
			}
		}
	}
		// If Tax breakup amounts are hidden then hide the Total Amount field
		if(isFederal) {
			if(taxBreakupAmountsHidden == "true") {
				if (document.getElementById("TotalAmountID") != null) {
					document.getElementById("TotalAmountID").style.display = "none";
				}
			} else {
				if (document.getElementById("TotalAmountID") != null) {
					document.getElementById("TotalAmountID").style.display = "";
				}
			}
		}
}

function totalAmounts() {
	var frm = document.TaxPayForm;
	var tag = frm['TaxPaymentAmount'];
	var amtTotal = 0.0;
	var amtStr = "";
	var options = "";
	var required = "";
    var mathfunction = "";
	var fieldName = "";
	var dataType = "";
    var calcElement = -1;
    var isDelete = "<%=deleteEntry%>";
    var decPoint = 2;
    if(frm.length > 0 && frm.elements){
	    for( var i=0; i < frm.length; i++ ) {
			var curr_name=frm.elements[i].name;
			if (curr_name.indexOf("options") >= 0)
			{
	            options = frm.elements[i].value;  // format = "RXNNFFFFFF" where R=required T/F, X=+0- Add or subtract, NN=DataType, FFFFFF = fieldName
				required = options.substring(0,1);
	            mathfunction = options.substring(1,2);
	            dataType = options.substring(2,4);
	            fieldName= options.substring(4);
			} else if (fieldName.length != 0)
			{
	            if (mathfunction == "=")
	                calcElement = i;
	            if (dataType == "D0")
	                decPoint = 0;
	            // QTS 638145 - if the tax payment is PRENOTE, change amounts to 0.00
				if ( (dataType=="N2" || dataType=="D0" || dataType=="D2") &&
	                    frm.elements["prenote"].value == 'true')
	                frm.elements[i].value = "0.00";         // prenote
	            if ( (dataType=="N2" || dataType=="D0" || dataType=="D2") &&
	                    (frm.elements[i].value.length != 0) )
				{
	                dataType = "";
	                var illegalChar = frm.elements[i].value.replace(/[^0-9|\.]/g,"x");
	                if (illegalChar.indexOf('x') >= 0)
	                    frm.elements[i].value = "0.00";         // if illegal characters, replace with "0.00"
					var myTemp = frm.elements[i].value.replace(/[^0-9|\.]/g,"");
	                if (dataType=="D0" && myTemp.indexOf('.') >= 0)
	                    myTemp = myTemp.substr(0, myTemp.indexOf('.'));
					if (myTemp == "") { myTemp = 0 }
	                if (isDelete != "true" && frm.elements[i].disabled == true)
						continue;
	                var amt = parseFloat(myTemp);
	
					if ( !isNaN( amt )) {
	                    if (mathfunction == "+")
						amtTotal += amt;
	                    if (mathfunction == "-")
						    amtTotal -= amt;
	                    // if mathfunction == "0" don't add it at all
					}
				}
				fieldName = "";
			}
		}
	}
	amtTotal = Math.round(amtTotal*Math.pow(10,2))/Math.pow(10,2);
// WARNING.... Custom Code for US follows, Decimal = ".", grouping separator = ","
	amtStr = "" + amtTotal;
	if (amtStr.indexOf('.') == -1) { amtStr = amtStr+".0" } // Add .0 if a decimal point doesn't exist
	var temp=(amtStr.length-amtStr.indexOf('.')); // Find out if the # ends in a tenth (ie 145.5)
	if (temp <= 2) { amtStr=amtStr+"0"; } // if it ends in a tenth, add an extra zero to the string
// add in commas again
	var x = amtStr.indexOf('.') - 3;
    if (dataType=="D0" && amtStr.indexOf('.') >= 0)
        amtStr = amtStr.substr(0, amtStr.indexOf('.'));
    if (amtStr.indexOf('.') == -1)
        x = amtStr.length;
	while (x > 0)
	{
		amtStr = amtStr.substr(0,x) + ',' + amtStr.substr(x, amtStr.length);
		x = amtStr.indexOf(',')-3;
	}
	if (tag != null) tag.value = amtStr;
	$('#totalAmount').html(amtStr);
    if (calcElement != -1){
        frm.elements[calcElement].value = amtStr;
    }
}
// --></script>

<div align="center">
<div align="left" class="batchEntryLeftMargin">
<% if (deleteEntry == true) {
    request.setAttribute("tax_strutsActionName", "deleteTaxChildACHEntryAction");
 } else if (addEntry == true) {
    request.setAttribute("tax_strutsActionName", "addTaxChildACHEntryAction_addEntry");
 } else {
    request.setAttribute("tax_strutsActionName", "editTaxChildACHEntryAction_editEntry");
 } %>
 <div class="leftPaneWrapper">
	<div class="leftPaneInnerWrapper">
		<div class="header">
			<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
			    <ffi:cinclude value1="${CurrentTaxForm}" value2="" operator="notEquals">
				<% noneSelected = false; %>
			    </ffi:cinclude>
			</ffi:cinclude>
			<% if (deleteEntry == true) { %>
				<s:text name="jsp.ach.DELETE.ENTRY" />:
				<% } else if (addEntry == true) { %>
                                 <s:text name="jsp.ach.Add.Entry" />:
                         <% } else { %>
						<s:text name="jsp.ach.EDIT.ENTRY" />:
				<% } %>
				<ffi:cinclude value1="${CurrentTaxForm.type}" value2="FEDERAL"><s:text name="jsp.default_488" /></ffi:cinclude>
				<ffi:cinclude value1="${CurrentTaxForm.type}" value2="STATE"><s:text name="jsp.default_507" /></ffi:cinclude>
				<ffi:cinclude value1="${CurrentTaxForm.type}" value2="OTHER"><s:text name="jsp.default_501" /></ffi:cinclude>
				<ffi:cinclude value1="${CurrentTaxForm.type}" value2="CHILDSUPPORT"><s:text name="jsp.default_97" /></ffi:cinclude>
		</div>
		<div class="leftPaneInnerBox leftPaneLoadPanel">
		<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
			<span class="sectionsubhead">
			<ffi:cinclude value1="${CurrentTaxForm.type}" value2="FEDERAL"><s:text name="jsp.default_487" /></ffi:cinclude>
			<ffi:cinclude value1="${CurrentTaxForm.type}" value2="STATE"><s:text name="jsp.default_506"/> <ffi:getProperty name="CurrentTaxForm" property="state" /></ffi:cinclude>
			<ffi:cinclude value1="${CurrentTaxForm.type}" value2="CHILDSUPPORT"><s:text name="jsp.ach.State.Child.Support.Form.for" /> <ffi:getProperty name="CurrentTaxForm" property="state" /></ffi:cinclude>
			<ffi:cinclude value1="${CurrentTaxForm.type}" value2="OTHER"><s:text name="jsp.default_500" /></ffi:cinclude>
			<ffi:getProperty name="CurrentTaxForm" property="IRSTaxFormNumber" /></span> - <span class="sectionsubhead"><ffi:getProperty name="CurrentTaxForm" property="TaxFormDescription" /></span>
		</ffi:cinclude>
		<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
		
			<% boolean zeroSize = false; %>
							<ffi:setProperty name="TaxForms" property="filter" value="TYPE==FEDERAL,TYPE==STATE,TYPE==OTHER"/>
                               <ffi:cinclude value1="${TaxForms.size}" value2="0" operator="equals" >
                                      <% zeroSize = true; %>
                                  </ffi:cinclude>
                                  
				<select class="txtbox" id="entryTaxFormsID" name="EntryTaxFormsID" <%=(fromTemplate || zeroSize || deleteEntry)?"disabled":""%> onchange="ns.ach.taxFormTypeChange();">
					<option value='' ><!--L10NStart-->Please select a Tax Payment Type<!--L10NEnd--></option>
					<ffi:list collection="TaxForms" items="TaxForm">
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CCD_TXP %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.STATE %>" objectId="${TaxForm.State}">
							<option value="<ffi:getProperty name='TaxForm' property='ID'/>"
								<ffi:cinclude value1="${TaxForm.ID}" value2="${CurrentTaxForm.ID}">selected
								<% noneSelected = false; %>
								</ffi:cinclude>>
							<%-- no default tax form, commented out this line
																					<ffi:cinclude value1="${TaxForm.ID}" value2="${AddEditACHBatch.DefaultTaxForm.ID}">* </ffi:cinclude>
							--%>
								<ffi:cinclude value1="${TaxForm.type}" value2="STATE"><ffi:getProperty name="TaxForm" property="state" /> <s:text name="jsp.default_386"/></ffi:cinclude>
								<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /> - <span class="sectionsubhead"><ffi:getProperty name="TaxForm" property="TaxFormDescription" />
								<ffi:cinclude value1="${TaxForm.BusinessID}" value2="" operator="notEquals">
							                     <!--L10NStart-->(<s:text name="jsp.reports_526" />)<!--L10NEnd-->
								</ffi:cinclude>
								</span>
							</option>
						</ffi:cinclude>
					</ffi:list>
					<ffi:setProperty name="TaxForms" property="filter" value="All"/>
				</select>
				<div><span id="entryTaxFormsIDError"></span></div>
		</ffi:cinclude>
		</div>
	</div>
</div>
<div class="rightPaneWrapper payeeDetailsWrapper">
<s:form action="%{#request.tax_strutsActionName}" namespace="/pages/jsp/ach" method="post" validate="false" theme="simple" id="TaxPayFormId" name="TaxPayForm">
<s:if test='%{#request.DiscretionaryDataErrorMsg || #request.CoEntryDescMsg}'>
	<span class="errorLabel"><s:property value="#request.Message" /></span>
</s:if>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="curAmount" value="<ffi:getProperty name='curAmount'/>">
<div class="paneWrapper">
	<div class="paneInnerWrapper">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData formTablePadding10">
								<%-- <tr>
							<td class="tbrd_b lightbackGround" colspan="5"><span class="sectionhead">
							&gt;&nbsp;
							<% if (deleteEntry == true) { %>
								<!--L10NStart-->DELETE ENTRY<!--L10NEnd-->:
							<% } else if (addEntry == true) { %>
                                    <!--L10NStart-->ADD ENTRY<!--L10NEnd-->:
                            <% } else { %>
									<!--L10NStart-->EDIT ENTRY<!--L10NEnd-->:
							<% } %>
							&nbsp;
<ffi:cinclude value1="${CurrentTaxForm.type}" value2="FEDERAL"><!--L10NStart-->Federal Taxes<!--L10NEnd--></ffi:cinclude>
<ffi:cinclude value1="${CurrentTaxForm.type}" value2="STATE"><!--L10NStart-->State Taxes<!--L10NEnd--></ffi:cinclude>
<ffi:cinclude value1="${CurrentTaxForm.type}" value2="OTHER"><!--L10NStart-->Other Taxes<!--L10NEnd--></ffi:cinclude>
<ffi:cinclude value1="${CurrentTaxForm.type}" value2="CHILDSUPPORT"><!--L10NStart-->Child Support<!--L10NEnd--></ffi:cinclude>
							</span></td>
						</tr> --%>
<%-- <% if (deleteEntry == true) { %>
							<tr>
								<td class="sectionhead lightBackground" colspan="5" align="center"><span class="sectionhead">
									<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
										<!--L10NStart-->Are you sure you want to delete this Tax Payment entry?<!--L10NEnd-->
									</ffi:cinclude>
									<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
										<!--L10NStart-->Are you sure you want to delete this Child Support entry?<!--L10NEnd-->
									</ffi:cinclude>
									<br>
									<br>
									</span>
								</td>
							</tr>
<% } %> --%>
<tr class="header">
	<td colspan="3">
		<s:text name="jsp.ach.Entry.Details"/>
	</td>
</tr>
<tr><td colspan="3"></td></tr>
<%--<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
 <tr class="header">
<td colspan="4">
	<ffi:cinclude value1="${CurrentTaxForm.type}" value2="FEDERAL"><!--L10NStart-->Federal Taxes<!--L10NEnd--></ffi:cinclude>
	<ffi:cinclude value1="${CurrentTaxForm.type}" value2="STATE"><!--L10NStart-->State Taxes<!--L10NEnd--></ffi:cinclude>
	<ffi:cinclude value1="${CurrentTaxForm.type}" value2="OTHER"><!--L10NStart-->Other Taxes<!--L10NEnd--></ffi:cinclude>
</td>
</tr> --%>
							<%-- <tr>
								<td width="100px">Select Type: <span class="required">*</span></td>
								<td colspan="3">
                                        <% boolean zeroSize = false; %>
    									<ffi:setProperty name="TaxForms" property="filter" value="TYPE==FEDERAL,TYPE==STATE,TYPE==OTHER"/>
	                                    <ffi:cinclude value1="${TaxForms.size}" value2="0" operator="equals" >
                                            <% zeroSize = true; %>
                                        </ffi:cinclude>
                                        
										<select class="txtbox" id="entryTaxFormsID" name="EntryTaxFormsID" <%=(fromTemplate || zeroSize || deleteEntry)?"disabled":""%> onchange="ns.ach.taxFormTypeChange();">
											<option value='' ><!--L10NStart-->Please select a Tax Payment Type<!--L10NEnd--></option>
											<ffi:list collection="TaxForms" items="TaxForm">
												<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CCD_TXP %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.STATE %>" objectId="${TaxForm.State}">
													<option value="<ffi:getProperty name='TaxForm' property='ID'/>"
														<ffi:cinclude value1="${TaxForm.ID}" value2="${CurrentTaxForm.ID}">selected
														<% noneSelected = false; %>
														</ffi:cinclude>>
no default tax form, commented out this line
														<ffi:cinclude value1="${TaxForm.ID}" value2="${AddEditACHBatch.DefaultTaxForm.ID}">* </ffi:cinclude>

														<ffi:cinclude value1="${TaxForm.type}" value2="STATE"><ffi:getProperty name="TaxForm" property="state" /> <s:text name="jsp.default_386"/></ffi:cinclude>
														<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /> - <span class="sectionsubhead"><ffi:getProperty name="TaxForm" property="TaxFormDescription" />
														<ffi:cinclude value1="${TaxForm.BusinessID}" value2="" operator="notEquals">
													                     <!--L10NStart-->(Business)<!--L10NEnd-->
														</ffi:cinclude>
														</span>
													</option>
												</ffi:cinclude>
											</ffi:list>
											<ffi:setProperty name="TaxForms" property="filter" value="All"/>
										</select>
										<span id="entryTaxFormsIDError"></span>
								</td>
							</tr>
no default tax form selected anymore, just comment it out
							<tr>
								<td colspan="5">
									<span class="columndata">
									<!--L10NStart-->* is the Tax Payment Batch Default Tax Form<!--L10NEnd-->
									</span>
								</td>
							</tr>

</ffi:cinclude> --%>
<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Entry Balanced" operator="equals">
	<tr>
		<td class="sectionsubhead" style="width:100px" align="right"><s:text name="jsp.default_494"/></td>
		<td class="columndata" colspan="2">
			<select id="currentEntryOffsetAccountID" <%=(fromTemplate || noneSelected || deleteEntry)?"disabled":""%> class="txtbox" name="offsetAccountID">
				<ffi:cinclude value1="${ACHOffsetAccounts.Size}" value2="1" operator="notEquals" >
				<option value="">(<s:text name="jsp.ach.select.offset.account" />)</option>
				</ffi:cinclude>
				<ffi:setProperty name="ACHOffsetAccounts" property="Filter" value="All" />
				<ffi:list collection="ACHOffsetAccounts" items="OffsetAccount">
                                         <% String selected=""; %>
                                         <ffi:cinclude value1="${OffsetAccount.RoutingNum}-${OffsetAccount.Number}-${OffsetAccount.TypeValue}" value2="${ModifyACHEntry.OffsetAccountBankID}-${ModifyACHEntry.OffsetAccountNumber}-${ModifyACHEntry.OffsetAccountTypeValue}">
                                             <% selected = "selected"; %>
                                         </ffi:cinclude>
                                         <ffi:cinclude value1="${OffsetAccount.ID}" value2="${ModifyACHEntry.OffsetAccountID}">
                                             <% selected = "selected"; %>
                                         </ffi:cinclude>
                                             <option value="<ffi:getProperty name="OffsetAccount" property="ID"/>" <%=selected%>><ffi:getProperty name="OffsetAccount" property="Number"/>-<ffi:getProperty name="OffsetAccount" property="Type"/></option>
				</ffi:list>
			</select>
		</td>
	</tr>
    <tr>
        <td colspan="3"><span id="offsetAccountIDError"></span></td>
    </tr>
</ffi:cinclude>

<% boolean displayDepositAccounts = false; %>
<ffi:cinclude value1="${CurrentTaxForm.MultipleDepositAccounts}" value2="true" operator="equals">
<% displayDepositAccounts = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${UserModifiedDepositAccount}" value2="true" operator="equals">
<% displayDepositAccounts = true; %>
</ffi:cinclude>
<% if (displayDepositAccounts) { %>
	<tr>
		<td class="columndata" colspan="3">
			<select id="depositAccountID" <%=(fromTemplate || noneSelected || deleteEntry)?"disabled":""%> class="txtbox" name="depositAccountID" onchange="ns.ach.revertChildTaxEntry();"> 
	<ffi:cinclude value1="${UserModifiedDepositAccount}" value2="true" operator="equals">
	<option value="" selected>(<s:text name="jsp.ach.deposit.account.msg" />)</option>
	</ffi:cinclude>
	<ffi:list collection="CurrentTaxForm.DepositAccounts" items="DepositAccount">
	<% String selected=""; %>
	<ffi:cinclude value1="${DepositAccount.ID}" value2="${ACHEntryPayee.TaxDepositAccountID}">
	<% selected = "selected"; %>
	</ffi:cinclude>
	<option value="<ffi:getProperty name="DepositAccount" property="ID"/>" <%=selected%>>
	<ffi:cinclude value1="${DepositAccount.DisplayName}" value2="" operator="notEquals">
	<ffi:getProperty name="DepositAccount" property="DisplayName"/>-
	</ffi:cinclude>
	<ffi:cinclude value1="${DepositAccount.BankName}" value2="" operator="notEquals">
	<ffi:getProperty name="DepositAccount" property="BankName"/>-
	</ffi:cinclude>
	<ffi:getProperty name="DepositAccount" property="BankRoutingNumber"/>-<ffi:getProperty name="DepositAccount" property="BankAccountNumber"/>
	</option>
	</ffi:list>
	</select>
		</td>
	</tr>
<% } %>
<tr><td colspan="3" class="nameTitle">Bank Details</td></tr>
<tr>
	
	<td id="taxNameLabel" width="33%"><span class="sectionsubhead"><s:text name="jsp.ach.name" /> </span><span class="required">*</span></td>
	<td id="taxRoutingNumberLabel" width="33%"><span class="sectionsubhead"><s:text name="jsp.default_365" /> </span><span class="required">*</span></td>
	<td id="taxAccountNumberLabel" width="33%"><span class="sectionsubhead"><s:text name="jsp.default_19" /> </span><span class="required">*</span></td>
	
</tr>
<tr>
	<td  class="columndata"><input id="taxNameValue"  <%=(fromTemplate || noneSelected || deleteEntry)?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="25" maxlength="22" border="0" name="achPayee.name" value="<ffi:getProperty name="ACHEntryPayee" property="Name"/>"></td>
	<td  class="columndata"><input id="taxRoutingNumberValue"  <%=(fromTemplate || noneSelected || deleteEntry)?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="25" maxlength="9" border="0" name="achPayee.routingNumber" value="<ffi:getProperty name="ACHEntryPayee" property="RoutingNumber"/>"></td>
	<td  class="columndata"><input id="taxAccountNumberValue"  <%=(fromTemplate || noneSelected || deleteEntry)?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="25" maxlength="17" border="0" name="achPayee.accountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="AccountNumber"/>"></td>
</tr>
<tr>
	<td class="columndata"><span id="achPayee.nameError"></span></td>
	<td class="columndata"><span id="achPayee.routingNumberError"></span></td>
	<td class="columndata"><span id="achPayee.accountNumberError"></span></td>
	
</tr>
<tr>
	<td id="taxIdLabel" class="sectionsubhead"><s:text name="da.field.ach.userAccountNumber" /></td>
	<td id="taxAccountTypeLabel"><span class="sectionsubhead"><s:text name="jsp.default_20" /> </span><span class="required">*</span></td>
	<td></td>
</tr>
<tr>
	<td  class="columndata"><input id="taxIdValue"  <%=(fromTemplate || noneSelected || deleteEntry)?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="25" maxlength="15" border="0" name="achPayee.userAccountNumber" value="<ffi:getProperty name="ACHEntryPayee" property="UserAccountNumber"/>"></td>
	<td id="taxAccountTypeValue" class="columndata">
	<% String taxAccountType = ""; %>
		<select id="taxAccountTypeID" <%=(fromTemplate || noneSelected || deleteEntry)?"disabled":""%> class="txtbox" name="achPayee.accountType">
			<ffi:list collection="ACHAccountTypes" items="ACHAccountType">
			<ffi:getProperty name="ACHAccountType" assignTo="taxAccountType" />
			<% if (taxAccountType.equalsIgnoreCase("Checking") || taxAccountType.equalsIgnoreCase("Savings")) { %>
				<option value="<ffi:getProperty name="ACHAccountType"/>" <ffi:cinclude value1="${ACHAccountType}" value2="${ACHEntryPayee.AccountType}">selected</ffi:cinclude>><ffi:getProperty name="ACHAccountType"/></option>
			<% } %>
			</ffi:list>
		</select>
	</td>
	<td></td>
</tr>
<tr>
	<td class="columndata"><span id="achPayee.userAccountNumberError"></span></td>
	<td class="columndata"><span id="achPayee.accountTypeError"></span></td>
</tr>
<tr><td colspan="3" class="nameTitle">
	<ffi:cinclude value1="${CurrentTaxForm.type}" value2="FEDERAL"><s:text name="jsp.default_488" /></ffi:cinclude>
	<ffi:cinclude value1="${CurrentTaxForm.type}" value2="STATE"><s:text name="jsp.default_507" /></ffi:cinclude>
	<ffi:cinclude value1="${CurrentTaxForm.type}" value2="OTHER"><s:text name="jsp.default_501" /></ffi:cinclude>
	<ffi:cinclude value1="${CurrentTaxForm.type}" value2="CHILDSUPPORT"><s:text name="jsp.default_97" /></ffi:cinclude>
	Details
</td></tr>
<ffi:cinclude value1="${AddEditACHBatch.BatchImported}" value2="true">
    <ffi:cinclude value1="${ModifyACHEntry.LineContent}" value2="" operator="notEquals">
		<tr>
			<td colspan="3" class="columndata">
				<span class="sectionsubhead"><s:text name="jsp.default_209" />:</span></td>
		</tr>
		<tr>
			<td colspan="3">
				<input class="ui-widget-content ui-corner-all txtbox" type="text" name="textfieldx5" value="<ffi:getProperty name="ModifyACHEntry" property="LineContent" encode="false" />" disabled size="115">
			</td>
		</tr>
    </ffi:cinclude>
</ffi:cinclude>

<% if (!noneSelected) {
int number=0;
boolean tpp_disabled = false;
boolean show_TPP_check = true;
boolean foundNumber = false;
String field_prefix = "";
%>
<ffi:cinclude value1="${ThirdPartyAllowed}" value2="false" >
    <% show_TPP_check = false; %>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CCD_TPP %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCOMPANY.CompanyID}" checkParent="true">
    <% if (show_TPP_check) { %>
        <tr>
            <td id="taxThirPartyPaymentLabel" colspan="3">
            	<%-- Hidden field for AddEditACHBatch.Prenote so uncheck will pass through to task --%>
                <input <%=(noneSelected || deleteEntry)?"disabled":""%> type="hidden" name="ThirdParty" value="<ffi:getProperty name="ThirdPartyFlag" />" >
                <input id="taxThirPartyPaymentValue" <%=(noneSelected || deleteEntry)?"disabled":""%> type="checkbox" onclick="thirdPartyChanged(this.checked)" name="ThirdPartyFlag" value="true" <ffi:cinclude value1="${ThirdPartyFlag}" value2="true">checked</ffi:cinclude>>
               <label for="taxThirPartyPaymentValue"><s:text name="jsp.ach.Third.Party.Payment" /></label>
            </td>
        </tr>
    <% } %>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CCD_TPP %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCOMPANY.CompanyID}" checkParent="true">
    <ffi:cinclude value1="${ThirdPartyFlag}" value2="true" >
        <% tpp_disabled = true; %>
        <tr>
            <td colspan="3">
                <s:text name="jsp.ach.Not.Entitled.msg" />
            </td>
        </tr>
    </ffi:cinclude>
</ffi:cinclude>

<% int fieldCounter = 0; %>
<ffi:list collection="CurrentTaxForm.fields" items="field">
<% boolean disabled = tpp_disabled;
   String maxLength;
   String maxSize = "12";
%>
	<ffi:getProperty name="field" property="MaxLength" assignTo="maxLength" />
	<%
		if (maxLength != null)
		{
			int i = Integer.parseInt(maxLength);
			i = new Double(i * 1.5).intValue();
			maxSize = "" + i;
		}
	%>

<%--
	// currently, only editable fields are displayed, but if ALL fields are supposed to be displayed, do the following:
	change the above disabled to true
	change the below cinclude to only include one line, which is disabled = false;
	end the cinclude after the one line and remove the extra </ffi:cinclude>
--%>
<ffi:cinclude value1="${field.editable}" value2="TRUE" >
	<ffi:cinclude value1="${field.TaxTypePrefix}" value2="${field_prefix}" operator="notEquals">
		<%
			if(fieldCounter > 0) 
			{
				for(int i=3; i>fieldCounter; i--) {
		%>
				<td valign="top">&nbsp;</td>
		<%			
				}
				fieldCounter = 0; %>
			</tr>
		<%	} %>
	</ffi:cinclude>
 	<ffi:getProperty name='field' property='TaxTypePrefix' assignTo="field_prefix"/>
		<%
		if(fieldCounter == 0){
			
		%>
			<tr class="<ffi:getProperty name='field' property='TaxTypePrefix'/>">
		<%}%>
						
		<%  boolean required = false;
			String name = "";
            String function = "+";
			String dataType = "";%>
			<ffi:getProperty name="field" property="name" assignTo="name"/>
			<%System.err.println(name); %>
			<%System.err.println(fieldCounter); %>
			<ffi:getProperty name="field" property="dataType" assignTo="dataType"/>
			<ffi:cinclude value1="${field.required}" value2="TRUE">
				<% required = true; %>
			</ffi:cinclude>
            <ffi:cinclude value1="${field.ignoreAmountInTotal}" value2="TRUE">
                <% function = "0"; %>
            </ffi:cinclude>
            <ffi:cinclude value1="${field.calculatedTotal}" value2="TRUE">
                <% function = "="; %>
            </ffi:cinclude>
            <ffi:cinclude value1="${field.subtractAmountInTotal}" value2="TRUE">
                <% function = "-"; %>
            </ffi:cinclude>
			<td valign="top">
				<div class="marginTop10 floatLeft"></div>
				<div id="<ffi:getProperty name="field" property="fieldname" />Container" class="sectionsubhead" style="height: 16px;"><span id="<ffi:getProperty name="field" property="fieldname" />Label"><%=name%></span> (<ffi:cinclude value1="${field.MinLength}" value2="0">1</ffi:cinclude><ffi:cinclude value1="${field.MinLength}" value2="0" operator="notEquals"><ffi:getProperty name="field" property="MinLength" /></ffi:cinclude><ffi:cinclude value1="${field.MinLength}" value2="${field.MaxLength}" operator="notEquals" >-<ffi:getProperty name="field" property="MaxLength" /></ffi:cinclude>) <span class="required"><%=(required?"&nbsp;*":"") %></span></div>
				<div class="marginTop10 floatLeft"></div>
                <input type="hidden" name="options" value='<%=(required?"T":"F")+function+dataType+name %>'>
                <div class="floatLeft entryFormInputFieldWrapper">
				<ffi:cinclude value1="${field.dataType}" value2="ID">
					<% String fieldValue = ""; %>
					<% String optionValue = ""; %>
					<ffi:getProperty name="CurrentAddenda" property="${field.fieldname}" assignTo="fieldValue" />
					<% if (fieldValue == null) fieldValue ="";
					fieldValue = fieldValue.trim();
					%>
					<ffi:cinclude value1="${field.options}" value2="" operator="notEquals">
						<ffi:setProperty name="field" property="options.SortedBy" value="Key"/>
							<select <%=(disabled || deleteEntry)? "disabled" : ""%> id="field<%=number %>" class="txtbox selectMenuCss" name="<ffi:getProperty name="field" property="fieldname" />"  onchange="disableAmounts();totalAmounts();">
								<ffi:list collection="field.options" items="option">
									<ffi:getProperty name="option" property="value" assignTo="optionValue"/>
									<% if (optionValue == null)
										optionValue ="";
									optionValue = optionValue.trim();
									%>
									<option value="<ffi:getProperty name="option" property="value" />"
									<ffi:cinclude value1="<%=fieldValue%>" value2="<%=optionValue%>">
										selected
									</ffi:cinclude>><ffi:getProperty name="option" property="key" /></option>
								</ffi:list>
								<ffi:removeProperty name="fieldValue"/>
								<ffi:removeProperty name="optionValue"/>
							</select>
							<%number++; %>
					</ffi:cinclude>
					<ffi:cinclude value1="${field.options}" value2="" operator="equals">
							<input id="taxIdentificationValue" type="text" <%=(disabled || deleteEntry) ? "disabled" : ""%> name="<ffi:getProperty name="field" property="fieldname" />" size="<%=maxSize%>" value="<ffi:getProperty name="CurrentAddenda" property="${field.fieldname}" />" <ffi:cinclude value1="${field.MaxLength}" value2="" operator="notEquals">maxlength=<ffi:getProperty name="field" property="MaxLength" /></ffi:cinclude> class="ui-widget-content ui-corner-all txtbox">&nbsp;&nbsp;(<ffi:cinclude value1="${field.MinLength}" value2="0">1</ffi:cinclude><ffi:cinclude value1="${field.MinLength}" value2="0" operator="notEquals"><ffi:getProperty name="field" property="MinLength" /></ffi:cinclude><ffi:cinclude value1="${field.MinLength}" value2="${field.MaxLength}" operator="notEquals" >-<ffi:getProperty name="field" property="MaxLength" /></ffi:cinclude>)
					</ffi:cinclude>
				</ffi:cinclude>
			<ffi:cinclude value1="${field.dataType}" value2="ID" operator="notEquals">
				<ffi:cinclude value1="${field.dataType}" value2="DT" operator="equals">
			
			        <% String nameString = ""; %>
			        <% String dateString = ""; %>
			        <ffi:getProperty name="field" property="fieldname" assignTo="nameString" />
			        <ffi:getProperty name="CurrentAddenda" property="${field.fieldname}" assignTo="dateString" />
			        <% String payDateID = "PayDate" + nameString; %>
			        <%request.setAttribute("nameString", nameString); %>
			        <%request.setAttribute("payDateID", payDateID); %>
			        <%request.setAttribute("dateString", dateString); %>
			        <% if (deleteEntry || disabled) { %>
			            <sj:textfield value="%{#request.dateString}" id="%{#request.payDateID}" name="%{#request.nameString}" label="Date" disabled="true"
			                        buttonImageOnly="true" minDate="-5y" cssClass="ui-widget-content ui-corner-all txtbox" maxlength="10"/>
			        <% } else {%>        
			            <sj:datepicker value="%{#request.dateString}" id="%{#request.payDateID}" name="%{#request.nameString}" label="Date"
			                        buttonImageOnly="true" minDate="-5y" cssClass="ui-widget-content ui-corner-all" maxlength="10"/>
			
			        <% String isAnnual; %>
			        <% String isQuarterly; %>
			        <ffi:getProperty name="field" property="ForceFirstOfQuarter" assignTo="isQuarterly" />
			        <ffi:getProperty name="field" property="ForceAnnual" assignTo="isAnnual" />
			        <%request.setAttribute("isQuarterly", isQuarterly); %>
			        <%request.setAttribute("isAnnual", isAnnual); %>
			
			        <script>
			            var urlStr ='';
			            var isFirstOfMonth = false;
			            var payID = "<ffi:getProperty name="payDateID"/>";
			            <ffi:cinclude value1="${CurrentTaxForm.type}" value2="FEDERAL" operator="equals">
			                isFirstOfMonth = true;
			            </ffi:cinclude>
			            <ffi:cinclude value1="${field.ForceFirstOfMonth}" value2="TRUE">
			                isFirstOfMonth = true;
			            </ffi:cinclude>
			            if (isFirstOfMonth) {
			                urlStr = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calTransactionType=12&isAnnual=${isAnnual}&isQuarterly=${isQuarterly}&calDisplay=select&calForm=TaxPayForm&calTarget=${nameString}&fromACHorTax=true"/>';
			                ns.common.enableAjaxDatepicker(payID, urlStr );
			            }
			        </script>
			
			
			
			        <% } %>
				</ffi:cinclude>
			    <ffi:cinclude value1="${field.dataType}" value2="N2" operator="equals">
			        <% foundNumber = true; %>
			        <input id="taxAmountValue" type="text" <%=(disabled || deleteEntry) ? "disabled" : ""%> name="<ffi:getProperty name="field" property="fieldname" />" maxlength="11" size="15" value="<ffi:getProperty name="CurrentAddenda" property="${field.fieldname}" />" class="ui-widget-content ui-corner-all txtbox N2" onchange="totalAmounts();">
			      
			    </ffi:cinclude>
			    <ffi:cinclude value1="${field.dataType}" value2="D2" operator="equals">
			        <% foundNumber = true; %>
			        <input id="taxAmountValue" type="text" <%=(disabled || deleteEntry) ? "disabled" : ""%> name="<ffi:getProperty name="field" property="fieldname" />" maxlength="11" size="15" value="<ffi:getProperty name="CurrentAddenda" property="${field.fieldname}" />" class="ui-widget-content ui-corner-all txtbox N2" onchange="totalAmounts();">
			    </ffi:cinclude>
			    <ffi:cinclude value1="${field.dataType}" value2="D0" operator="equals">
			        <% foundNumber = true; %>
			        <input id="taxAmountValue" type="text" <%=(disabled || deleteEntry) ? "disabled" : ""%> name="<ffi:getProperty name="field" property="fieldname" />" maxlength="11" size="15" value="<ffi:getProperty name="CurrentAddenda" property="${field.fieldname}" />" class="ui-widget-content ui-corner-all txtbox N2" onchange="totalAmounts();">
			    </ffi:cinclude>
				<ffi:cinclude value1="${field.dataType}" value2="AN" operator="equals">
			        <input id="taxIdentificationValue" type="text" <%=(disabled || deleteEntry) ? "disabled" : ""%> name="<ffi:getProperty name="field" property="fieldname" />" size="<%=maxSize%>" value="<ffi:getProperty name="CurrentAddenda" property="${field.fieldname}" />" <ffi:cinclude value1="${field.MaxLength}" value2="" operator="notEquals">maxlength=<ffi:getProperty name="field" property="MaxLength" /></ffi:cinclude> class="ui-widget-content ui-corner-all txtbox">
				</ffi:cinclude>
			</ffi:cinclude>
		</div>
		<span class="calcHolderDivCls" style="margin-top:5px;" id="<ffi:getProperty name="field" property="fieldname" />Error"></span>
	    <ffi:cinclude value1="${field.memo}" value2="" operator="notEquals">
	    	<span style="margin-top:5px;" class="<ffi:getProperty name='field' property='TaxTypePrefix'/> calcHolderDivCls marginTop10">
	    		<ffi:getProperty name="field" property="memo"/>
	    	</span>
	    </ffi:cinclude>
	</td>
						
						
	<%
	fieldCounter++;
	if(fieldCounter == 3){
		fieldCounter = 0;%>
		</tr>
	<%}%>
						


</ffi:cinclude>
</ffi:list>

<% if (!foundNumber) // HAWAII doesn't have an amount field
{ %>
						<tr>
	                        <td va id="taxTotalAmountLabel" colspan="3" class="sectionsubhead">
	                           <s:text name="jsp.ach.Tax.Amount" /> <span class="required">*</span>
	                        </td>
                        </tr> 
                        <tr>
	                        <td colspan="3" class="columndata">
	                            <input id="taxTotalAmountValue" type="text" <%=deleteEntry ? "disabled" : ""%> name="amount" maxlength="15" size="15" value="<ffi:getProperty name="ModifyACHEntry" property="AmountValue.CurrencyStringNoSymbol" encode="false" />" class="ui-widget-content ui-corner-all txtbox">
	                             <%-- Hidden field for AddEditACHBatch.Prenote so uncheck will pass through to task --%>
								<input <%=(noneSelected || deleteEntry)?"disabled":""%> type="hidden" name="prenote" value="<ffi:getProperty name="ModifyACHEntry" property="Prenote" />" >
								<input <%=(noneSelected || deleteEntry)?"disabled":""%> type="checkbox" onclick="prenoteChanged(this.checked)" name="PrenoteCheckbox" value="true" <ffi:cinclude value1="${ModifyACHEntry.Prenote}" value2="true">checked</ffi:cinclude>>
								<label for="PrenoteCheckbox"><s:text name="ach.grid.prenote"/></label>
	                        </td>
                    	</tr>
<% } else { %>
                    <tr>
                        <td id="taxTotalAmountLabel" colspan="4" class="sectionsubhead" style="background:#eee">
                        	<span id="TotalAmountID" style="font-size:18px;"><!--L10NStart-->Total : <!--L10NEnd-->
							<%-- 
								<input id="taxTotalAmountValue" type="text" <%=deleteEntry ? "disabled" : ""%> readonly name="TaxPaymentAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all txtbox">
							--%>
							<span style="font-size:18px;" id="totalAmount"></span>
							</span>
                        	<%-- <input id="taxTotalAmountValue" type="text" <%=deleteEntry ? "disabled" : ""%> readonly name="TaxPaymentAmount" maxlength="15" size="15" value="" class="ui-widget-content ui-corner-all txtbox"> --%>
                        	<%-- Hidden field for AddEditACHBatch.Prenote so uncheck will pass through to task --%>
							<input <%=(noneSelected || deleteEntry)?"disabled":""%> type="hidden" name="prenote" value="<ffi:getProperty name="ModifyACHEntry" property="Prenote" />" >
							<input <%=(noneSelected || deleteEntry)?"disabled":""%> type="checkbox" onclick="prenoteChanged(this.checked)" name="PrenoteCheckbox" value="true" <ffi:cinclude value1="${ModifyACHEntry.Prenote}" value2="true">checked</ffi:cinclude>>
							<label for="PrenoteCheckbox"><s:text name="ach.grid.prenote"/></label>
						</td>
					</tr>
<% } %>

<% } %>
						<tr>
							<td id="TaxAddendaLabel" colspan="3" class="sectionsubhead">
								<s:text name="jsp.default_477" />
							</td>
						</tr>
						<tr>
							<td colspan="3" class="columndata">
								<% String addendaString; %>
								<ffi:getProperty name="CurrentAddenda" property="TaxAddendaString" assignTo="addendaString" />
								<% if (addendaString == null || addendaString.length() == 0)
								{
									addendaString = "Click 'View' button to see addenda";
								}
								%>
								<input id="addendaInputId" class="ui-widget-content ui-corner-all txtbox" type="text" name="textfield5" value="<%=addendaString%>" disabled size="88">
							</td>
						</tr>
<ffi:cinclude value1="${AddEditACHBatch.BatchImported}" value2="true">
    <ffi:cinclude value1="${ModifyACHEntry.AddendaLineContent}" value2="" operator="notEquals">
						<tr>
							<td colspan="3" align="right" class="columndata">
								<span class="sectionsubhead"><s:text name="jsp.default_209"/>:</span>
								<input class="ui-widget-content ui-corner-all txtbox" type="text" name="textfieldy5" value="<ffi:getProperty name="ModifyACHEntry" property="AddendaLineContent" encode="false" />" disabled size="115">
							</td>
						</tr>
    </ffi:cinclude>
</ffi:cinclude>
					</table>
  </table>
  <% if (deleteEntry == true) { %>
<div align="center" class="columndata_error">
		<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
			<s:text name="jsp.ach.delete.Tax.Payment.entry" />?
		</ffi:cinclude>
		<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
			<s:text name="jsp.ach.delete.Child.Support.entry" />?
		</ffi:cinclude>
</div>
<% } %>
<div align="center" class="marginTop10"><span class="required">* <s:text name="jsp.default_240" /></span></div>
<div class="btn-row">
<sj:a button="true" onClickTopics="cancelACHEntry">Cancel</sj:a>

<% if (!deleteEntry) {%>
		<sj:a
		    id="taxBatchViewAddenda"
            formIds="TaxPayFormId"
            targets="tempDiv"
            button="true"
            validate="true"
			validateFunction="customValidation"
            onClickTopics="viewAddendaClick"
            onSuccessTopics="viewTaxAddendaSuccess"
            >
            <s:text name="jsp.default_6" />
       	</sj:a>
<% } %>
<% if (deleteEntry == true) { %>
        <ffi:setProperty name="tempURL" value="/cb/pages/jsp/ach/deleteTaxChildACHEntryAction.action?DeleteEntryID=${ModifyACHEntry.ID}" URLEncrypt="true"/>

        <script>
            ns.ach.deleteAchEntryByIDUrl = '<ffi:getProperty name="tempURL"/>';
        </script>
		<sj:a
			button="true"
            onClickTopics="deleteACHEntryClick"
			><s:text name="jsp.default_162" />
		</sj:a>
		
<% } else if (addEntry != true) { %>
        <sj:a
        	id="taxBatchEditEntry"
            formIds="TaxPayFormId"
            targets="resultmessage"
            button="true"
            validate="true"
            validateFunction="customValidation"
            onClickTopics="saveChildTaxEntryClick"
            onSuccessTopics="saveChildTaxEntrySuccess"
            ><s:text name="jsp.default_366" />
        </sj:a>
<% } else {%>
        <sj:a
            id="taxBatchAddEntry"
            formIds="TaxPayFormId"
            targets="resultmessage"
            button="true"
            validate="true"
            validateFunction="customValidation"
            onClickTopics="saveChildTaxEntryClick"
            onSuccessTopics="saveChildTaxEntrySuccess"
            ><s:text name="jsp.default_29" />
        </sj:a>
<% } %>
		<input id="viewAddendaId" name="viewAddenda" type="hidden" value="false">
        </div></div></div>
	</s:form>
	</div>
</div></div>
<div style="display:none">
	<div id="tempDiv"></div>
</div>

<script type="text/javascript">
<!--
$(document).ready(function(){
	$("#entryTaxFormsID").selectmenu({width: '95%'});
	$("#currentEntryOffsetAccountID").selectmenu({width: 200});
	$("#depositAccountID").selectmenu({width: 320});
	$("#taxAccountTypeID").selectmenu({width: 200});
// todo: Use CLASS instead of ID for the following because there are taxforms with more than 2 drop-down sections
	$(".selectMenuCss").selectmenu({width: 200});
	disableAmounts();
    thirdPartyShow();
	totalAmounts();
});

    //ns.ach.commonChildTaxEntryUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achbatchaddedit.jsp?Initialize=true"/>';
//-->
</script>

<div id="tempGridDiv"></div>
