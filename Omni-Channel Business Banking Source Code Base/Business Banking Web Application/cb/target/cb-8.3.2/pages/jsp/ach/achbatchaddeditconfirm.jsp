<%@page import="com.ffusion.beans.ach.ACHBatch"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%--
    		This file is used by both Add/Edit ACH AND Add/Edit ACH Templates
            If you modify this file, make sure your change works for both please.
--%>
<ffi:help id="payments_achbatchaddeditconfirm" className="moduleHelpClass"/>

<% if (request.getParameter("ToggleSortedBy") != null) {
  	String toggle; %>
	<ffi:getProperty name="ToggleSortedBy" assignTo="toggle"/>
  	<ffi:setProperty name="ACHEntries" property="ToggleSortedBy" value="<%= toggle %>" />
<% }

    String subMenuSelected = "ach";
  	boolean isEdit = false;
	boolean isTemplate = false;
	boolean isMultiBatchTemplate = false;
	boolean fromTemplate = false;
	boolean canRecurring = true;
    boolean isTaxbatch=false;
    boolean isChildbatch=false;
	String secCodeDisplay = "";
	String frequencyValue = "";		// used in assignTo below
%>

<s:if test="%{isEdit}">
<% isEdit = true; %>
</s:if>

<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
<% isTemplate = true; %>
</s:if>

<ffi:cinclude value1="${AddEditACHBatch.FromTemplate}" value2="true" >
	<% fromTemplate = true; %>
</ffi:cinclude>

<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
	<% isMultiBatchTemplate = true;
       if (!fromTemplate)
          isTemplate = true; %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
	<% subMenuSelected = "tax";
    	secCodeDisplay = "<!--L10NStart-->Tax Payment (CCD + TXP Credit)<!--L10NEnd-->";
		canRecurring = false;
        isTaxbatch = true;
	%>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
	<% subMenuSelected = "child";
		secCodeDisplay = "<!--L10NStart-->Child Support Payment (CCD + DED Credit)<!--L10NEnd-->";
        isChildbatch = true;
	%>
</ffi:cinclude>
<ffi:setProperty name="BackURL" value="${SecurePath}payments/achbatchaddedit.jsp" />




<%  String headerTitle = "";
	String classCode = null;

	String pageHeading = "";
	if (subMenuSelected.equals("tax"))
		pageHeading = "<!--L10NStart-->Tax Payment<!--L10NEnd-->";
	else
	if (subMenuSelected.equals("child"))
		pageHeading = "<!--L10NStart-->Child Support Payment<!--L10NEnd-->";
	else
		pageHeading = "<!--L10NStart-->ACH Payment<!--L10NEnd-->";
	session.setAttribute("PageHeading", pageHeading + (isTemplate?" <!--L10NStart-->Template Confirm<!--L10NEnd-->":" <!--L10NStart-->Verify<!--L10NEnd-->") + "<br>");
%>

<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="classCode" />
    <ffi:getProperty name="AddEditACHBatch" property="FrequencyValue" assignTo="frequencyValue" />
<%
	if (classCode == null)
		classCode = "";
	classCode = classCode.toUpperCase();
	if ("ARC".equals(classCode) || "POP".equals(classCode) ||
		"BOC".equals(classCode) || "RCK".equals(classCode) || "CIE".equals(classCode))
			canRecurring = false;
	if (isEdit && "0".equals(frequencyValue))
		canRecurring = false;
%>
		<% if (isTemplate) { %>
			<title><ffi:getProperty name="product"/>:
				<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
						<s:text name="jsp.ach.Tax.Payment.Templates" />
				</ffi:cinclude>
				<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
						<s:text name="jsp.ach.Child.Support.Payment.Templates" />
				</ffi:cinclude>
				<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
						<s:text name="jsp.ach.ACH.Payment.Templates" />
				</ffi:cinclude>
			</title>
		<% } else { %>
			<title><ffi:getProperty name="product"/>:
				<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
						<s:text name="jsp.default_407" />
				</ffi:cinclude>
				<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
						<s:text name="jsp.default_98" />
				</ffi:cinclude>
				<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
						<s:text name="jsp.default_25" />
				</ffi:cinclude>
			</title>
		<% } %>
		<script language="JavaScript">
<!--

var theChild = null;

function closeTemplateName() {
	try{
		if (theChild != null && !theChild.closed)
		{
			theChild.close();
		}
	}
	catch(e)
	{
	}
	theChild = null;
}
function doSearch()
{
    document.AddBatchForm.action='<ffi:urlEncrypt url="${SecurePath}payments/achbatchaddeditconfirm.jsp?DoSearch=true"/>';
    document.forms['AddBatchForm'].submit();
    return false;
}
function gotoPage(page)
{
    document.AddBatchForm.action='<ffi:urlEncrypt url="${SecurePath}payments/achbatchaddeditconfirm.jsp?GoToPage=true"/>';
    document.forms['AddBatchForm'].submit();
    return false;
}

function openSaveTemplate(URLExtra)  {
	theChild=window.open(''+URLExtra,'','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=750,height=340');
}
// --></script>

<%-- include javascripts for top navigation menu bar
	<body onFocus="closeTemplateName()" >
--%>
<div align="center">
<% if (isTemplate || isMultiBatchTemplate) { %>
<ffi:setProperty name="tempURL" value="${SecurePath}wait.jsp?target=payments/achtemplateaddeditsave.jsp" URLEncrypt="true"/>
    <% } else { %>
<ffi:setProperty name="tempURL" value="${SecurePath}wait.jsp?target=payments/achbatchaddeditsave.jsp" URLEncrypt="true"/>
    <% } %>
<s:if test="%{#session.AddEditACHBatch.FromTemplateValue & #session.AddEditACHBatch.IsMultipleBatchTemplateValue}">
   		<s:set var="actionName" value="%{'_executeLoadBatchForMultiple'}" scope="action"></s:set>
</s:if>
<s:else>
		<s:set var="actionName" value="%{'_execute'}" scope="action"></s:set>
</s:else>
<s:form id="AddBatchSendFormId" namespace="/pages/jsp/ach" validate="false" action="%{strutsActionName + #actionName}" method="post" name="AddBatchConfirmForm" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<s:hidden id="ID" name="ID" value="%{ID}"/>
<ffi:removeProperty name="tempURL"/>
<input type="hidden" name="PayeeID" size="15">
                           <input type="hidden" name="SavedNumberPayments" value="<ffi:getProperty name="AddEditACHBatch" property="NumberPayments" />">
<s:hidden name="editRecurringModel" value="%{editRecurringModel}"/>
<s:hidden id="strutsActionName" value="%{strutsActionName}" />
<s:hidden id="achType" value="%{#session.AddEditACHBatch.ACHType}"></s:hidden>

<!--  We want to avoid MFA dialog in case, when its batch load from multiple template -->
<s:if test="%{#session.AddEditACHBatch.FromTemplateValue & #session.AddEditACHBatch.IsMultipleBatchTemplateValue}">
<s:hidden name="ignoreMfa" value="true"/>
</s:if>
<%-- <div class="nameSubTitle marginTop10" align="left">
	<%=(isEdit?"<!--L10NStart-->Edit<!--L10NEnd--> ":"<!--L10NStart-->Add<!--L10NEnd--> ") + pageHeading + (fromTemplate?" <!--L10NStart-->(from Template)<!--L10NEnd-->":"") + (isTemplate?" <!--L10NStart-->Template<!--L10NEnd-->":"")%>
</div> --%>

<div class="leftPaneWrapper">
		<div class="leftPaneInnerWrapper">
		<div class="header"><s:text name="jsp.ach.ACH.Batch.details"/></div>
		<!-- ACHBatch Entry header and Grid details -->
			<div id="" class="paneContentWrapper summaryBlock">
					<div>
						<span class="sectionsubhead sectionLabel floatleft inlineSection"><s:text name="ach.grid.companyName"/>:</span>
						<span class="inlineSection floatleft labelValue">
							<s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyName}"/>&nbsp;-&nbsp;<s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyID}"/>
						</span>
					</div>
					<div class="marginTop10 clearBoth floatleft">
						<span class="sectionsubhead sectionLabel inlineSection floatleft"><s:text name="jsp.default_67"/>:</span>
						<span class="inlineSection floatleft labelValue">
							<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
								<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
								<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
								<ffi:process name="ACHResource" />
								<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}"/>
								<ffi:getProperty name="ACHResource" property="Resource"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
											<%= secCodeDisplay %>
							</ffi:cinclude>
						</span>
					</div>
			</div>
		</div>
</div>
<div class="confirmPageDetails label130">
<%if(isEdit && !isTemplate){%>
<!-- Commenting below code for QTS CR# 779617 fix. No need of so many checks to show the warning message. Only need to check
whether the transaction is recurring model and editing whole model.

<ffi:cinclude value1="${AddEditACHBatch.InstanceType}" value2="Scheduled" operator="notEquals">
<ffi:cinclude value1="${AddEditACHBatch.TransactionType}" value2="ACH">
<div class="sectionsubhead" style="color:red">
<s:text name="jsp.ach.Warning.update" /><br><br>
</div>
</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${AddEditACHBatch.TransactionType}" value2="ACH" operator="notEquals">
<div class="sectionsubhead" style="color:red">
<s:text name="jsp.ach.Warning.update" /><br><br>
</div>
</ffi:cinclude>
-->
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
<ffi:cinclude value1="${AddEditACHBatch.RecID}" value2="" operator="notEquals">
<ffi:cinclude value1="${AddEditACHBatch.ID}" value2="${AddEditACHBatch.RecID}" operator="equals">
<div class="sectionsubhead" style="color:red">
<s:text name="jsp.ach.Warning.update" /><br><br>
</div>
</ffi:cinclude>
</ffi:cinclude>
</ffi:cinclude>

<%}%>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.ach.Batch.Details"/></div>
	<div class="blockContent">


		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
								<% if (!isTemplate) { %>
											<s:text name="ach.grid.batchName"/>:
								<% } else { %>
											<s:text name="ach.grid.templateName"/>:
								<% } %>
				</span>

				<% if (!isTemplate) { %>
						<span class="sectionsubhead labelCls"><ffi:getProperty name="AddEditACHBatch" property="Name"/></span>
				<% } else { %>
						<span class="sectionsubhead labelCls"><ffi:getProperty name="AddEditACHBatch" property="TemplateName"/></span>
				<% } %>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" ><s:text name="ach.company.description"/>:</span>
				<span class="sectionsubhead labelCls">
					<ffi:getProperty name="AddEditACHBatch" property="CoEntryDesc"/>
				</span>
			</div>
		</div>

		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" >
					<% if (!isTemplate) { %>
						<s:text name="ach.grid.effectiveDate"/>:
					<% } else { %>
						<s:text name="jsp.default_418"/>:
					<%}%>
				</span>
				<span class="sectionsubhead labelCls">
					<% if (!isTemplate) { %>
               			 <ffi:getProperty name="AddEditACHBatch" property="Date" />
					<% } else {%>
						<ffi:getProperty name="AddEditACHBatch" property="TemplateScope" />
					<%}%>
				 </span>
			</div>
			<div class="inlineBlock">
				<% if (!"IAT".equals(classCode)) { %>
					<span class="sectionsubhead sectionLabel" >
							<s:text name="ach.Discretionary.Data"/>:
					</span>
					<span class="sectionsubhead labelCls">
							<ffi:getProperty name="AddEditACHBatch" property="CoDiscretionaryData"/>
					</span>
				<% } %>
			</div>
		</div>

		<% if (!"IAT".equals(classCode)) { %>
		<% } else {
		// IAT doesn't have Discretionary Data
		%>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel" >
					<s:text name="ach.originating.currency"/>
					</span>
					<span class="sectionsubhead labelCls">
							<s:text name="ach.USD.United.States.Dollar"/>
					</span>
				</div>
				<div class="inlineBlock">
					<span>
	       				 <span class="sectionsubhead sectionLabel"><s:text name="da.field.ach.destinationCountry"/> </span>
	                </span>
	                <span class="sectionsubhead labelCls">
	                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
	                    <ffi:process name="ACHResource" />
	                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${AddEditACHBatch.ISO_DestinationCountryCode}"/>
	                    <ffi:getProperty name="ACHResource" property="Resource"/>
	                </span>
				</div>
			</div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span >
	        			<span class="sectionsubhead sectionLabel"><s:text name="ach.Destination.Currency"/>  </span>
	                </span>
	                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
	                    <ffi:process name="ACHResource" />
	                <span class="sectionsubhead labelCls">
	                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${AddEditACHBatch.ISO_DestinationCurrencyCode}"/>
	                    <ffi:getProperty name="AddEditACHBatch" property="ISO_DestinationCurrencyCode"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
	                </span>
				</div>
				<div class="inlineBlock">
					<span >
        				<span class="sectionsubhead sectionLabel"><s:text name="da.field.ach.foreignExchangeMethod"/> </span>
                	</span>
	                <span class="sectionsubhead labelCls">
	                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${AddEditACHBatch.ForeignExchangeIndicator}"/>
	                    <ffi:getProperty name="AddEditACHBatch" property="ForeignExchangeIndicator"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
	                </span>
				</div>
			</div>
		<% } %>

		<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHHeaderCompanyName${AddEditACHBatch.StandardEntryClassCode}"/>
		<ffi:getProperty name="ACHResource" property="Resource" assignTo="headerTitle" />
		<% if (headerTitle != null && headerTitle.length() > 0) { %>
            <div class="blockRow">
				<span class="sectionsubhead sectionLabel" >
					<%=	headerTitle %>
				</span>
				<span class="sectionsubhead labelCls">
					<ffi:getProperty name="AddEditACHBatch" property="HeaderCompName"/>
				</span>
	    	</div>
		<% } %>
		<% if (!isTemplate) { %>
		<ffi:cinclude value1="${AddEditACHBatch.DateHasChanged}" value2="true" >
			<div class="blockRow sectionsubhead labelCls_error">
				<s:text name="jsp.ach.Effective.Date.msg"/><br>
				<s:text name="jsp.ach.Effective.Date.error.msg"/>
			</div>
		</ffi:cinclude>
		<% } %>
		<% if (canRecurring) { %>
		<div class="blockRow">
                     <span class="sectionsubhead sectionLabel"><s:text name="jsp.billpay_repeating"/>:</span>
                     <span class="sectionsubhead labelCls">
                     	<ffi:getProperty name="AddEditACHBatch.Frequency"/> / <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999">Unlimited</ffi:cinclude>
					    <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999" operator="notEquals" >
					 <s:property value="%{#session.AddEditACHBatch.NumberPayments}" /> <s:text name="jsp.default_321" /></ffi:cinclude>
                     	<%-- <span><input type="radio" name="OpenEnded" disabled value="true" border="0" <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999">checked</ffi:cinclude>></span>
						<label><!--L10NStart-->Unlimited<!--L10NEnd--></label>
						<span><input disabled type="radio" name="OpenEnded" value="false" border="0" <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999" operator="notEquals" >checked</ffi:cinclude>></span>
						<label><!--L10NStart--># of Payments<!--L10NEnd--> <s:property value="%{#session.AddEditACHBatch.NumberPayments}" /></label> --%>
                     </span>
		</div>
		<% } %>
	</div>

	<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="equals">
		<% if ("IAT".equals(classCode))
		 {
		// QTS 577543: If IAT, we don't support Batch Balanced or Entry Balanced batches
		%>
	        <div class="blockRow">
	       	 <s:text name="ach.IAT.noOffsetAcct"/>
	        </div>
		 <% } else { %>
		 	<div class="blockRow">
				<span class="sectionsubhead sectionLabel" ><s:text name="jsp.default_494" /></span>
				<span class="sectionsubhead labelCls">
						<s:property value="#session.AddEditACHBatch.OffsetAccount.Number"/>&nbsp; - &nbsp;<s:property value="#session.AddEditACHBatch.OffsetAccount.Type"/>
				</span>
			</div>
		<% } %>
	</ffi:cinclude>
		<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="notEquals">
		</ffi:cinclude>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="ach.grid.entries"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" valign="center" >
					<s:text name="jsp.default_513"/>:
				</span>
				<span class="sectionsubhead" >
				    <%-- <input type="text" disabled name="TotalNumberCredits" maxlength="6" size="8" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberCredits" />" class="ui-widget-content ui-corner-all txtbox"> --%>
				    <ffi:getProperty name="AddEditACHBatch" property="TotalNumberCredits" />
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" >
					<s:text name="jsp.default_514"/>:
				</span>
				<span class="sectionsubhead" >
					<%-- <input type="text" disabled name="TotalNumberDebits" maxlength="6" size="8" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberDebits" />" class="ui-widget-content ui-corner-all txtbox"> --%>
					<ffi:getProperty name="AddEditACHBatch" property="TotalNumberDebits" />
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_433"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):
				</span>
				<span class="sectionsubhead">
		            <%-- <input type="text" disabled name="TotalCreditAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalCreditAmount" />" class="ui-widget-content ui-corner-all txtbox"> --%>
		            <ffi:getProperty name="AddEditACHBatch" property="TotalCreditAmount" />
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_434"/> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):
				</span>
				<span class="sectionsubhead">
					<%-- <input type="text" disabled name="TotalDebitAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalDebitAmount" />" class="ui-widget-content ui-corner-all txtbox"> --%>
					<ffi:getProperty name="AddEditACHBatch" property="TotalDebitAmount" />
				</span>
			</div>
		</div>
	</div>
</div>

			<%-- include page header --%>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>&nbsp;</td>
							<td class="sectionsubhead labelCls lightBackground">
							<div align="left">
<%-- <% if (isTemplate || isMultiBatchTemplate) { %>
<ffi:setProperty name="tempURL" value="${SecurePath}wait.jsp?target=payments/achtemplateaddeditsave.jsp" URLEncrypt="true"/>
    <% } else { %>
<ffi:setProperty name="tempURL" value="${SecurePath}wait.jsp?target=payments/achbatchaddeditsave.jsp" URLEncrypt="true"/>
    <% } %> --%>
    <br>

   <%-- <s:if test="%{#session.AddEditACHBatch.FromTemplateValue & #session.AddEditACHBatch.IsMultipleBatchTemplateValue}">
   		<s:set var="actionName" value="%{'_executeLoadBatchForMultiple'}" scope="action"></s:set>
   </s:if>
   <s:else>
   		<s:set var="actionName" value="%{'_execute'}" scope="action"></s:set>
   </s:else> --%>


                                	<%-- <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                    <ffi:removeProperty name="tempURL"/>
									<input type="hidden" name="PayeeID" size="15">
                                    <input type="hidden" name="SavedNumberPayments" value="<ffi:getProperty name="AddEditACHBatch" property="NumberPayments" />">
                                    <s:hidden name="editRecurringModel" value="%{editRecurringModel}"/>
                                    <s:hidden id="strutsActionName" value="%{strutsActionName}" />
                                    <s:hidden id="achType" value="%{#session.AddEditACHBatch.ACHType}"></s:hidden>

                                    <!--  We want to avoid MFA dialog in case, when its batch load from multiple template -->
                                    <s:if test="%{#session.AddEditACHBatch.FromTemplateValue & #session.AddEditACHBatch.IsMultipleBatchTemplateValue}">
                                    	<s:hidden name="ignoreMfa" value="true"/>
                                    </s:if> --%>

                                <table width="98%" border="0" cellspacing="0" cellpadding="3">
									<%-- <tr valign="top">
										<td class="sectionhead lightBackground" colspan="2"><span class="sectionhead">&gt;&nbsp;<%=(isEdit?"<!--L10NStart-->Edit<!--L10NEnd--> ":"<!--L10NStart-->Add<!--L10NEnd--> ") + pageHeading + (fromTemplate?" <!--L10NStart-->(from Template)<!--L10NEnd-->":"") + (isTemplate?" <!--L10NStart-->Template<!--L10NEnd-->":"")%><br>
										</span></td>
									</tr> --%>
									<%-- <%if(isEdit && !isTemplate){%>
										<%if(canRecurring){%>

										<ffi:cinclude value1="${AddEditACHBatch.InstanceType}" value2="Scheduled" operator="notEquals">
										<ffi:cinclude value1="${AddEditACHBatch.TransactionType}" value2="ACH">

										<tr>
											<td class="sectionsubhead" colspan="3" style="color:red" align="center">
											<!--L10NStart-->WARNING: Any changes to the recurring ACH batch instance will also update all other pending recurring instances of the model<!--L10NEnd--><br><br>
											</td>
										</tr>
										</ffi:cinclude>
										</ffi:cinclude>

								<ffi:cinclude value1="${AddEditACHBatch.TransactionType}" value2="ACH" operator="notEquals">

										<tr>
											<td class="sectionsubhead" colspan="3" style="color:red" align="center">
											<!--L10NStart-->WARNING: Any changes to the recurring ACH batch instance will also update all other pending recurring instances of the model<!--L10NEnd--><br><br>
											</td>
										</tr>
										</ffi:cinclude>

										<%}%>

									<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >

									<ffi:cinclude value1="${AddEditACHBatch.TransactionType}" value2="ACH" >

									<ffi:cinclude value1="${AddEditACHBatch.RecID}" value2="" operator="notEquals">

									<ffi:cinclude value1="${AddEditACHBatch.ID}" value2="${AddEditACHBatch.RecID}" operator="notEquals">
										<tr>
										 <td class="sectionsubhead" colspan="3" style="color:red" align="center">
										<!--L10NStart-->WARNING: Any changes to the recurring ACH batch instance will only update this instance of the model<!--L10NEnd--><br><br>
										</td>
										</tr>
									</ffi:cinclude>
									</ffi:cinclude>
									</ffi:cinclude>
									</ffi:cinclude>
									<%}%> --%>
									<tr class="lightBackground">
										<%-- <td class="sectionsubhead" align="right" width="217"><!--L10NStart-->Company Name<!--L10NEnd--></td>
										<td class="sectionsubhead labelCls" align="left">
										<s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyName}"/>&nbsp;-&nbsp;<s:property value="%{#session.AddEditACHBatch.AchCompany.CompanyID}"/>
										</td> --%>
									</tr>
									<%-- <tr class="lightBackground">
										<td class="sectionsubhead" align="right" width="217"><!--L10NStart-->Batch Type<!--L10NEnd--></td>
										<td class="columndata" align="left">
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
								<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
								<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
								<ffi:process name="ACHResource" />
												<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}"/>
												<ffi:getProperty name="ACHResource" property="Resource"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
										<%= secCodeDisplay %>
</ffi:cinclude>
											</td>
									</tr> --%>
									</table>
								<table width="98%" border="0" cellspacing="0" cellpadding="3">
									<%-- <tr class="lightBackground">
									    										<td class="sectionsubhead" align="right" width="217">
								<% if (!isTemplate) { %>
											<!--L10NStart-->Batch Name<!--L10NEnd-->
								<% } else { %>
											<!--L10NStart-->Template Name<!--L10NEnd-->
								<% } %></td>

								<% if (!isTemplate) { %>
										<td class="columndata"><ffi:getProperty name="AddEditACHBatch" property="Name"/></td>
								<% } else { %>
										<td class="columndata"><ffi:getProperty name="AddEditACHBatch" property="TemplateName"/></td>
								<% } %>
									            </tr>

									            <tr>
										<td class="sectionsubhead" align="right"><!--L10NStart-->Company Description<!--L10NEnd--></td>
										<td class="columndata">
											<ffi:getProperty name="AddEditACHBatch" property="CoEntryDesc"/>
										</td>
									            </tr> --%>


<% if (!"IAT".equals(classCode)) { %>
  <%--   <tr>
	<td class="sectionsubhead" align="right">
			<!--L10NStart-->Discretionary Data<!--L10NEnd-->
	</td>
	<td class="columndata">
			<ffi:getProperty name="AddEditACHBatch" property="CoDiscretionaryData"/>
	</td>
	</tr> --%>
<% } else {
// IAT doesn't have Discretionary Data
%>
    <%-- <tr>
	<td class="sectionsubhead" align="right">
			<!--L10NStart-->Originating Currency<!--L10NEnd-->
	</td>
	<td class="columndata">
			<!--L10NStart-->USD - United States Dollar<!--L10NEnd-->
	</td>
	</tr>
            <tr >
                <td align="right">
        <span class="sectionsubhead"><!--L10NStart-->Destination Country<!--L10NEnd--> </span>
                </td>
                <td class="columndata">
                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                    <ffi:process name="ACHResource" />

                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${AddEditACHBatch.ISO_DestinationCountryCode}"/>
                    <ffi:getProperty name="ACHResource" property="Resource"/>

                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
            <tr>
                <td align="right">
        <span class="sectionsubhead"><!--L10NStart-->Destination Currency<!--L10NEnd--> </span>
                </td>
                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                    <ffi:process name="ACHResource" />
                <td class="columndata">
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${AddEditACHBatch.ISO_DestinationCurrencyCode}"/>
                    <ffi:getProperty name="AddEditACHBatch" property="ISO_DestinationCurrencyCode"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr>
            <tr>
                <td align="right">
        <span class="sectionsubhead"><!--L10NStart-->Foreign Exchange Method<!--L10NEnd--> </span>
                </td>
                <td class="columndata">
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${AddEditACHBatch.ForeignExchangeIndicator}"/>
                    <ffi:getProperty name="AddEditACHBatch" property="ForeignExchangeIndicator"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
                </td>

                <td class="sectionsubhead" colspan='3'></td>
            </tr> --%>
<% } %>
                                                <%-- <tr>
										<td class="sectionsubhead" align="right">
										<% if (!isTemplate) { %>
											<!--L10NStart-->Effective Date<!--L10NEnd-->
										<% } else { %>
											<!--L10NStart-->Template Scope<!--L10NEnd-->
										<%}%>
										</td>
										<td class="columndata">
										<% if (!isTemplate) { %>
	                                			<ffi:getProperty name="AddEditACHBatch" property="Date" />
										<% } else {%>
											<ffi:getProperty name="AddEditACHBatch" property="TemplateScope" />
										<%}%>
											 </td>
                                                </tr> --%>

		<%-- <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHHeaderCompanyName${AddEditACHBatch.StandardEntryClassCode}"/>
		<ffi:getProperty name="ACHResource" property="Resource" assignTo="headerTitle" />
<% if (headerTitle != null && headerTitle.length() > 0) { %>
                                    <tr>
										<td class="sectionsubhead" align="right">
											<%=	headerTitle %>
										</td>
										<td class="columndata">
											<ffi:getProperty name="AddEditACHBatch" property="HeaderCompName"/>
										</td>

										<td class="sectionsubhead" colspan='3'></td>
								    </tr>
<% } %>

									<% if (!isTemplate) { %>
									<ffi:cinclude value1="${AddEditACHBatch.DateHasChanged}" value2="true" >
										<tr>
											<td colspan="3" align="center" class="sectionsubhead columndata_error">
											<!--L10NStart-->Effective Date has changed to be a business day.<!--L10NEnd--><br>
											<!--L10NStart-->If Effective Date doesn't allow two business days, batch might be rejected.<!--L10NEnd-->
											</td>
										</tr>
									</ffi:cinclude>
									<% } %> --%>

									        </table>
									    </td>


									    <td width="40%">
									        <table>
<%-- SECOND column of information --%>
									            <%-- <tr>
										<% if (canRecurring) { %>
                                            <td class="sectionsubhead tbrd_r" rowspan="4" width="10">&nbsp;</td>
                                            <td class="sectionsubhead" rowspan="4" width="10">&nbsp;</td>
                                            <td class="sectionsubhead" align="right"><!--L10NStart-->Repeating<!--L10NEnd--></td>
                                            <td class="columndata"><ffi:getProperty name="AddEditACHBatch.Frequency"/></td>
										<% } %>
									            </tr>
									            <tr>
										<% if (canRecurring) { %>
										<td class="sectionsubhead" colspan='2' align="right"><input type="radio" name="OpenEnded" disabled value="true" border="0" <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999">checked</ffi:cinclude>></td>
										<td class="sectionsubhead"><!--L10NStart-->Unlimited<!--L10NEnd--></td>
										<% } %>
									            </tr>
									            <tr>
										<% if (canRecurring) { %>
										<td class="sectionsubhead" colspan='2' align="right"><input disabled type="radio" name="OpenEnded" value="false" border="0" <ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999" operator="notEquals" >checked</ffi:cinclude>></td>
										<td class="sectionsubhead"><!--L10NStart--># of Payments<!--L10NEnd--> <s:property value="%{#session.AddEditACHBatch.NumberPayments}" /></td>
										<td></td>
										<% } %>
									            </tr> --%>
									            <tr>
											<%-- <ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="equals">
<% if ("IAT".equals(classCode))
 {
// QTS 577543: If IAT, we don't support Batch Balanced or Entry Balanced batches
%>
                                        <td class="sectionsubhead" align="right" colspan='3'>
                                        <s:text name="ach.IAT.noOffsetAcct"/>
                                        </td>
 <% } else { %>
												<td class="sectionsubhead" align="right"><!--L10NStart-->Offset Account<!--L10NEnd--></td>
												<td class="columndata" colspan="2">
														<s:property value="#session.AddEditACHBatch.OffsetAccount.Number"/>&nbsp; - &nbsp;<s:property value="#session.AddEditACHBatch.OffsetAccount.Type"/>
												</td>
<% } %>
											</ffi:cinclude>
											<ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="notEquals">
												<td class="sectionsubhead" align="right">&nbsp;</td>
												<td colspan="2">&nbsp;</td>
											</ffi:cinclude>
									            </tr> --%>
									        </table>
									    </td>
									</tr>

									</table>



    								<table width="98%" border="0" cellspacing="0" cellpadding="3">
									<!-- <tr class="lightBackground">
										<td height="20"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="157" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="87" height="1" border="0"></td>
										<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="107" height="1" border="0"></td>
									</tr> -->
									<%-- <tr>
										<td class="tbrd_b" colspan="6">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
	    <td class="sectionhead" rowspan="2" width="36%"><span class="sectionhead">&gt;&nbsp;<!--L10NStart-->Entries<!--L10NEnd--><br></span></td>

		<td class="sectionsubhead" valign="center" width="16%">
			<!--L10NStart-->Total # Credits<!--L10NEnd-->
		</td>
		<td class="sectionsubhead" width="16%">
            <input type="text" disabled name="TotalNumberCredits" maxlength="6" size="8" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberCredits" />" class="ui-widget-content ui-corner-all txtbox">
        </td>
		<td class="sectionsubhead" width="16%">
			<!--L10NStart-->Total # Debits<!--L10NEnd-->
		</td>
		<td class="sectionsubhead" width="16%">
			<input type="text" disabled name="TotalNumberDebits" maxlength="6" size="8" value="<ffi:getProperty name="AddEditACHBatch" property="TotalNumberDebits" />" class="ui-widget-content ui-corner-all txtbox">
		</td>
		</tr>
		<tr>
		<td class="sectionsubhead" width="16%">
			<!--L10NStart-->Total Credits<!--L10NEnd--> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
		</td>
		<td class="sectionsubhead" width="16%">
            <input type="text" disabled name="TotalCreditAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalCreditAmount" />" class="ui-widget-content ui-corner-all txtbox">
		</td>
		<td class="sectionsubhead" width="16%">
			<!--L10NStart-->Total Debits<!--L10NEnd--> (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>)
		</td>
		<td class="sectionsubhead" width="16%">
			<input type="text" disabled name="TotalDebitAmount" maxlength="15" size="15" value="<ffi:getProperty name="AddEditACHBatch" property="TotalDebitAmount" />" class="ui-widget-content ui-corner-all txtbox">
		</td>
		</tr>
		</table>

										</td>
									</tr> --%>
	<!-- <tr>
		<td  colspan="6">
		</td>
	</tr> -->

<%-- adjust how many need to exist before the Search happens in csil.xml, ACHEntryDisplaySize --%>
<ffi:object name="com.ffusion.tasks.GetDisplayCount" id="GetDisplayCount"/>
<ffi:setProperty name="GetDisplayCount" property="SearchTypeName" value="<%= com.ffusion.tasks.GetDisplayCount.ACHENTRY %>"/>
<ffi:setProperty name="GetDisplayCount" property="DisplayCountName" value="achentryDisplaySize"/>
<ffi:process name="GetDisplayCount"/>
<ffi:removeProperty name="GetDisplayCount"/>



<%
	ACHBatch batch = (ACHBatch)session.getAttribute("AddEditACHBatch");
	session.setAttribute("ACHEntries",batch.getACHEntries());
    if (((com.ffusion.beans.ach.ACHEntries)session.getAttribute("ACHEntries")).nonFilteredSize() > Integer.parseInt((String)session.getAttribute("achentryDisplaySize")))
    {
        %>
            <ffi:setProperty name="ACHEntries" property="FilteredPageSize" value="${achentryDisplaySize}"/>
            <%-- repeat the last search OR do ALL search with paging --%>
            <ffi:setProperty name="ACHEntries" property="FilterPaging" value="paged"/>
            <ffi:cinclude value1="${ACHEntries.IsPageFiltered}" value2="true" operator="notEquals" >
                <ffi:setProperty name="ACHEntries" property="Filter" value="${ACHEntries.Filter}"/>
            </ffi:cinclude>
        <%
    }
	    if (request.getParameter("NextPage") != null )	// if they changed the SEC code, need to re-test default effective date
    	{
            String currentPage = ""+(Integer.valueOf(request.getParameter("NextPage")) + 1);
            %>
            <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="<%=currentPage%>"/>
            <ffi:setProperty name="ACHEntries" property="Filter" value="${ACHEntries.Filter}"/>
            <%
    	}

	    if (request.getParameter("PreviousPage") != null )	// if they changed the SEC code, need to re-test default effective date
    	{
            String currentPage = ""+(Integer.valueOf(request.getParameter("PreviousPage")) - 1);
            %>
            <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="<%=currentPage%>"/>
            <ffi:setProperty name="ACHEntries" property="Filter" value="${ACHEntries.Filter}"/>
            <%
    	}

	    if (request.getParameter("GoToPage") != null )	// if they changed the SEC code, need to re-test default effective date
    	{
            String currentPage = request.getParameter("pageNumber");
            %>
            <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="<%=currentPage%>"/>
            <ffi:setProperty name="ACHEntries" property="Filter" value="${ACHEntries.Filter}"/>
            <%
    	}
	    if (request.getParameter("DoSearch") != null )	// if they changed the SEC code, need to re-test default effective date
    	{
            %>
            <ffi:cinclude value1="${SearchString}" value2="" operator="notEquals">
                <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="0"/>
                <ffi:setProperty name="ACHEntries" property="Filter" value="PAYEE_NAME**${SearchString},PAYEE_NICKNAME**${SearchString},PAYEE_ACCOUNTNUMBER**${SearchString},PAYEE_ROUTINGNUM**${SearchString},PAYEE_USERACCOUNTNUMBER**${SearchString},AMOUNT**${SearchString}"/>
            </ffi:cinclude>
            <ffi:cinclude value1="${SearchString}" value2="" operator="equals">
                <ffi:setProperty name="ACHEntries" property="CurrentPageNumber" value="0"/>
                <ffi:setProperty name="ACHEntries" property="Filter" value="All"/>
            </ffi:cinclude>
            <%
    	}
%>
<%
boolean displayPageBlock = false;
if( displayPageBlock ) {
%>
<ffi:cinclude value1="${ACHEntries.IsPaged}" value2="true">
									<tr>

		<td colspan="6">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
	    <td class="sectionhead" width="26%">
        &nbsp;
	    </td>

		<td class="sectionsubhead" valign="center" width="34%" colspan="2">
            <input class="ui-widget-content ui-corner-all txtbox" type="text" id="SearchString" name="SearchString" value="<ffi:getProperty name="SearchString" />" size="25">
            &nbsp;
            <input class="submitbutton" type="button" value="Search" onClick="doSearch();return false;">
            <br>
            <s:text name="jsp.default_515"/>:<ffi:getProperty name="ACHEntries" property="FilteredSize"/>
        </td>
        <td class="sectionsubhead" width="40%">
        <ffi:cinclude value1="${ACHEntries.MaximumPageNumber}" value2="0" operator="notEquals">
            <input class="submitbutton" type="button" <ffi:cinclude value1="false" value2="${ACHEntries.HasPrevPage}">disabled</ffi:cinclude> value="PREV PAGE" onClick="document.forms['AddBatchForm'].action='<ffi:urlEncrypt url="${SecurePath}payments/achbatchaddeditconfirm.jsp?PreviousPage=${ACHEntries.CurrentPageNumber}"/>'; document.forms['AddBatchForm'].submit();return false;">
            &nbsp;
            <input class="submitbutton" type="button" <ffi:cinclude value1="false" value2="${ACHEntries.HasNextPage}">disabled</ffi:cinclude> value="NEXT PAGE" onClick="document.forms['AddBatchForm'].action='<ffi:urlEncrypt url="${SecurePath}payments/achbatchaddeditconfirm.jsp?NextPage=${ACHEntries.CurrentPageNumber}"/>'; document.forms['AddBatchForm'].submit();return false;">
            <br>
            <s:text name="jsp.common_112"/> <ffi:getProperty name="ACHEntries" property="CurrentPageNumber"/><!--L10NStart--> of <!--L10NEnd--><ffi:getProperty name="ACHEntries" property="MaximumPageNumber"/>
            &nbsp;&nbsp;
            <s:text name="jsp.default_490"/>:
            <select class="txtbox" name="pageNumber" onchange="gotoPage(this)">
<ffi:object id="Paging" name="com.ffusion.beans.util.Paging" scope="session"/>
<ffi:setProperty name="Paging" property="Pages" value="${ACHEntries.MaximumPageNumber}" />
<ffi:list collection="Paging" items="PageNum">
            <option value='<ffi:getProperty name="PageNum"/>' <ffi:cinclude value1="${ACHEntries.CurrentPageNumber}" value2="${PageNum}" >selected</ffi:cinclude> > <ffi:getProperty name="PageNum"/></option>
</ffi:list>
            </select>

        </ffi:cinclude>
        <ffi:cinclude value1="${ACHEntries.MaximumPageNumber}" value2="0" operator="equals">
            <s:text name="jsp.default_478"/>
        </ffi:cinclude>
		</td>
		</tr>
		</table>
		</td>
		</tr>
</ffi:cinclude>
<%} // end of if(displayPageBlock == true)%>
								</table>

<ffi:setProperty name="ACHEntries" property="FilterPaging" value="none"/>


							</td>
						</tr>
					</table>
</s:form>

<s:if test="%{#session.AddEditACHBatch.SameDay==1}">
<div id="achSameDayMessageConfirmDiv"> <b><s:text name="ach.payments.sameday.verify.message"/></b></div>
</s:if>

<div id="achEntryConfirmGridDiv">
	<s:include value="%{#session.PagesPath}/ach/achbatchaddeditentryconfirmgrid.jsp" />
</div>

 <div class="btn-row">
<%-- <sj:a id="verifyACHCancel"
             button="true"
	onClickTopics="cancelACHForm"
     	>
Cancel
</sj:a> --%>
<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
	<sj:a
              button="true"
               id="verifyACHCancel"
		summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHTemplateForm"
      	><s:text name="jsp.default_82" />
      	</sj:a>
</s:if>
<s:else>
	<sj:a
              button="true"
		summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHForm"
		 id="verifyACHCancel"
      	><s:text name="jsp.default_82" />
      	</sj:a>
</s:else>
	<%--
<input class="submitbutton" type="button" value="REVISE" onclick="document.location='<ffi:urlEncrypt url="${SecurePath}payments/achbatchaddedit.jsp?Revise=true"/>';return false;">
--%>
<sj:a id="verifyACHBack"
    						button="true"
	onClickTopics="ach_backToInput"
						>
Back
</sj:a>&nbsp;
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
	<%--
	<input class="submitbutton" type="button" name="submitButtonName" value="CONFIRM/SUBMIT TAX PAYMENT" onClick="AddBatchForm.submitButtonName.disabled=true;document.AddBatchForm.submit();">
	--%>
	<sj:a
		id="sendACHSubmit"
		formIds="AddBatchSendFormId"
                          targets="inputDiv"
                          button="true"
                          onBeforeTopics="beforeSendACHFormInMBT"
                          onSuccessTopics="sendACHFormSuccessInMBT"
                          onErrorTopics="errorSendACHFormInMBT"
                          onCompleteTopics="sendACHFormCompleteInMBT"
               				><s:text name="jsp.default_395" />
               				</sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
	<%--
	<input class="submitbutton" type="button" name="submitButtonName" value="CONFIRM/SUBMIT CHILD SUPPORT PAYMENT" onClick="AddBatchForm.submitButtonName.disabled=true;document.AddBatchForm.submit();">
	--%>
	<sj:a
		id="sendACHSubmit"
		formIds="AddBatchSendFormId"
                          targets="inputDiv"
                          button="true"
                          onBeforeTopics="beforeSendACHFormInMBT"
                          onSuccessTopics="sendACHFormSuccessInMBT"
                          onErrorTopics="errorSendACHFormInMBT"
                          onCompleteTopics="sendACHFormCompleteInMBT"
               				><s:text name="jsp.default_395" />
               				</sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
	<%--
	<input class="submitbutton" type="button" name="submitButtonName" value="CONFIRM/SUBMIT ACH PAYMENT" onClick="AddBatchForm.submitButtonName.disabled=true;document.AddBatchForm.submit();">
	--%>
	<sj:a
		id="sendACHSubmit"
		formIds="AddBatchSendFormId"
                          targets="inputDiv"
                          button="true"
                          onBeforeTopics="beforeSendACHFormInMBT"
                          onSuccessTopics="sendACHFormSuccessInMBT"
                          onErrorTopics="errorSendACHFormInMBT"
                          onCompleteTopics="sendACHFormCompleteInMBT"
               				><s:text name="jsp.default_395" />
               				</sj:a>
</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
<s:if test="%{!#session.AddEditACHBatch.isTemplateValue}">
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
<%--
<input class="submitbutton" type="button" name="submitButtonName" value="CONFIRM/SUBMIT TAX PAYMENT" onClick="AddBatchForm.submitButtonName.disabled=true;document.AddBatchForm.submit();">
--%>
<sj:a
	id="sendACHSubmit"
	formIds="AddBatchSendFormId"
                         targets="confirmDiv"
                         button="true"
                         onBeforeTopics="beforeSendACHForm"
                         onSuccessTopics="sendACHFormSuccess"
                         onErrorTopics="errorSendACHForm"
                         onCompleteTopics="sendACHFormComplete"
              				><s:text name="jsp.default_395" />
  				</sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
<%--
<input class="submitbutton" type="button" name="submitButtonName" value="CONFIRM/SUBMIT CHILD SUPPORT PAYMENT" onClick="AddBatchForm.submitButtonName.disabled=true;document.AddBatchForm.submit();">
--%>
<sj:a
	id="sendACHSubmit"
	formIds="AddBatchSendFormId"
                         targets="confirmDiv"
                         button="true"
                         onBeforeTopics="beforeSendACHForm"
                         onSuccessTopics="sendACHFormSuccess"
                         onErrorTopics="errorSendACHForm"
                         onCompleteTopics="sendACHFormComplete"
              				><s:text name="jsp.default_395" />
  				</sj:a>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
<%--
<input class="submitbutton" type="button" name="submitButtonName" value="CONFIRM/SUBMIT ACH PAYMENT" onClick="AddBatchForm.submitButtonName.disabled=true;document.AddBatchForm.submit();">
--%>
<sj:a
	id="sendACHSubmit"
	formIds="AddBatchSendFormId"
                         targets="confirmDiv"
                         button="true"
                         onBeforeTopics="beforeSendACHForm"
                         onSuccessTopics="sendACHFormSuccess"
                         onErrorTopics="errorSendACHForm"
                         onCompleteTopics="sendACHFormComplete"
              				><s:text name="jsp.default_395" />
  				</sj:a>
</ffi:cinclude>
&nbsp;
	<ffi:object name="com.ffusion.tasks.ach.CheckACHSECEntitlement" id="CheckACHSECEntitlement" scope="session"/>
<ffi:setProperty name="CheckACHSECEntitlement" property="ACHBatchName" value="AddEditACHBatch"/>
<ffi:setProperty name="CheckACHSECEntitlement" property="OperationName" value="<%= EntitlementsDefines.ACH_TEMPLATE %>"/>
<ffi:process name="CheckACHSECEntitlement" />
<%-- <ffi:cinclude value1="${CheckACHSECEntitlement.IsEntitledTo}" value2="TRUE" operator="equals">
<%
if (classCode != null && classCode.length() > 3)
	classCode = classCode.substring(0,3);
// RCK, POP, TEL, CIE, ARC, BOC are NOT allowed in templates
if (!"RCK".equals(classCode) && !"POP".equals(classCode) && !"BOC".equals(classCode) &&
	!"TEL".equals(classCode) && !"CIE".equals(classCode) && !"ARC".equals(classCode))
{
%>
<input onclick="openSaveTemplate('<ffi:getProperty name="SecurePath"/>payments/achbatchtempnewname.jsp')" type="hidden" name="Button" value="SAVE AS TEMPLATE" class="submitbutton">
     <sj:a
             button="true"
             validate="true"
             onClickTopics="clickSaveAsTemplateTopic"
             style="display:none"
       ><s:text name="jsp.default_366" />
     	</sj:a>
<% 	} %>
</ffi:cinclude> --%>
<ffi:removeProperty name="CheckACHSECEntitlement" />
</s:if>
<s:if test="%{#session.AddEditACHBatch.isTemplateValue}">
<sj:a
								id="sendACHSubmit"
								 button="true"
								formIds="AddBatchSendFormId"
                                targets="confirmDiv"
                                onBeforeTopics="beforeSendACHForm"
                                onSuccessTopics="sendACHTemplateFormSuccess"
                                onErrorTopics="errorSendACHForm"
                                onCompleteTopics="sendACHFormComplete"
                     				><s:text name="jsp.default_107" />
</sj:a>
</s:if>
</ffi:cinclude>
</div>

<ffi:setProperty name="OpenEnded" value="false"/>
</div>