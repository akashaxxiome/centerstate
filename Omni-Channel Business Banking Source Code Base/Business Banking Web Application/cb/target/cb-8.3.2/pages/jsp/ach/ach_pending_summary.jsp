<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="payments_ACHPendingSummary" className="moduleHelpClass"/>
	<ffi:setGridURL grid="GRID_pendingACH" name="ViewURL" url="/cb/pages/jsp/ach/viewAchBatch.action?ID={0}&BatchIsland=Scheduled&RecID={1}" parm0="ID" parm1="RecID"/>
	
	<!-- Decide Edit URL depending module type -->
	<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_CHILD)}">
		<ffi:setGridURL grid="GRID_pendingACH" name="EditURL" url="/cb/pages/jsp/ach/editChildSupportBatch_init.action?ID={0}&RecID={1}&OriginalDate={2}&EditRecurringModel=false" parm0="ID" parm1="RecID" parm2="Date" />
	</s:if>
	<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_TAX)}">
		<ffi:setGridURL grid="GRID_pendingACH" name="EditURL" url="/cb/pages/jsp/ach/editTaxBatch_init.action?ID={0}&RecID={1}&OriginalDate={2}&EditRecurringModel=false" parm0="ID" parm1="RecID" parm2="Date" />
	</s:if>
	<s:if test="%{#session.ACHType.equals(@com.ffusion.beans.ach.ACHDefines@ACH_TYPE_ACH)}">
		<ffi:setGridURL grid="GRID_pendingACH" name="EditURL" url="/cb/pages/jsp/ach/editACHBatch_init.action?ID={0}&RecID={1}&OriginalDate={2}&EditRecurringModel=false" parm0="ID" parm1="RecID" parm2="Date" />
		<ffi:setGridURL grid="GRID_pendingACH" name="EditRecURL" url="/cb/pages/jsp/ach/achbatchselectinstance.jsp?ID={0}&RecID={1}&OriginalDate={2}" parm0="ID" parm1="RecID" parm2="Date"/>
	</s:if>
	
	<ffi:setGridURL grid="GRID_pendingACH" name="DeleteURL" url="/cb/pages/jsp/ach/deleteACHBatch_init.action?ID={0}&RecID={1}&DeleteRecurringModel=false" parm0="ID" parm1="RecID" />
	<ffi:setGridURL grid="GRID_pendingACH" name="DeleteRecURL" url="/cb/pages/jsp/ach/deleterecurringbatchselectinstance.jsp?ID={0}&RecID={1}" parm0="ID" parm1="RecID"/>
	<ffi:setGridURL grid="GRID_pendingACH" name="SkipURL" url="/cb/pages/jsp/ach/skipACHBatch_init.action?ID={0}&RecID={1}&isSkipInstance=true&SkipRecurringModel=false" parm0="ID" parm1="RecID" />
	<ffi:setGridURL grid="GRID_pendingACH" name="InquireURL" url="/cb/pages/jsp/ach/sendACHInqMessage_initACHInquiry.action?FundsID={0}&recurringId={1}&CollectionName=PendingACHBatches&Subject=ACH Inquiry" parm0="ID" parm1="RecID"/>
	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getPagedACHbatchesAction_getPendingACHBatches.action?collectionName=PendingACHBatches&ACHType=${ACHType}&GridURLs=GRID_pendingACH" URLEncrypt="true"/>
    <s:url id="pendingACHUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
		id="pendingACHGridID"
		caption=""  
		sortable="true" 
		dataType="local"  
		href="%{pendingACHUrl}"
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		sortname="date"
		sortorder="asc"
		onGridCompleteTopics="addGridControlsEvents,pendingACHGridCompleteEvents"
		> 
		
        <sjg:gridColumn name="date" width="100" index="date" title="%{getText('ach.grid.effectiveDate')}" sortable="true" formatter="ns.ach.formatAchEffectiveDate" />
        <sjg:gridColumn name="name" width="60" index="name" title="%{getText('ach.grid.batchName')}" sortable="true" />
        <sjg:gridColumn name="companyID" width="100"  index="companyID" title="%{getText('ach.grid.companyID')}" sortable="true"/>
        <sjg:gridColumn name="coName" width="100"  index="coName" title="%{getText('ach.grid.companyName')}" sortable="true" />
        <sjg:gridColumn name="numberEntries" width="50" index="numberEntries" title="%{getText('ach.grid.entries')}" sortable="true"/>
        <ffi:cinclude value1="${ACHType}" value2="TaxPayment" operator="notEquals">
            <sjg:gridColumn name="frequency" width="80" index="frequency" title="%{getText('ach.grid.frequency')}" sortable="true" />
        </ffi:cinclude>
        <sjg:gridColumn name="status" width="90" index="status" title="%{getText('jsp.default_388')}" sortable="true"/>
        <sjg:gridColumn name="totalCreditAmountValue.currencyStringNoSymbol" width="100" index="totalCreditAmount" title="%{getText('ach.grid.totalCredits')}" align="right" sortable="true" formatter="ns.ach.formatCreditAmountColumn" formatoptions="{suffix:' USD'}"/>
        <sjg:gridColumn name="totalDebitAmountValue.currencyStringNoSymbol" width="100"  index="totalDebitAmount" title="%{getText('ach.grid.totalDebits')}" align="right" sortable="true" formatter="ns.ach.formatDebitAmountColumn" formatoptions="{suffix:' USD'}"/>
        <sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false" formatter="ns.ach.formatPendingACHActionLinks" width="90" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>

		<sjg:gridColumn name="recID" index="recID" title="recID" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="transactionType" index="transactionType" title="transactionType" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="TransactionIndexString" index="TransactionIndexString" title="TransactionIndexString" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="ACHType" index="ACHType" title="ACHType" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="canEdit" index="canEdit" title="Can Edit" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="canView" index="canView" title="Can View" hidden="true" hidedlg="true"/>
        <sjg:gridColumn name="canDelete" index="canDelete" title="Can Delete" hidden="true" hidedlg="true"/>
	</sjg:grid>
	
	<script type="text/javascript">
	<!--
		$("#pendingACHGridID").jqGrid('setColProp','ID',{title:false});
	//-->
	</script>	
