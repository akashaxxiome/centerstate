<%@ page import="com.ffusion.beans.ach.ACHPayee,
				 com.ffusion.beans.SecureUser,
				 com.ffusion.beans.ach.ACHEntry"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<script type="text/javascript"><!--
$(document).ready(function(){
    // if REVERSAL is active hide if no entries are selected
    if ($("input[id^='hold']:checkbox:checked").length > 0)
    {
        $("#reverseViewACHBatchLink").show();
    } else {
        $("#reverseViewACHBatchLink").hide();
    }
});
 //--></script>
<%-- ----------------------------------------
	this file is used to display the entries portion for the following files:
		achbatchdelete.jsp
		achbatchview.jsp
		achbatch-reverse.jsp
	if you make changes, test with all of the files
	it requires being inside of a form named ViewForm
	It also requires "ACHBatch" in the session
--------------------------------------------- --%>

<div id="ViewACHEntries">
<s:if test="%{#attr.isReverse}">
 <s:action name="viewAchBatchEntries" namespace="/pages/jsp/ach" executeResult="true">
 	<s:if test="%{#attr.isReverse}">
 		<s:param name="isReverse" value="true"></s:param>
 	</s:if>
 </s:action>
 </s:if>
 	
</div> 
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="tableData">
	<tr>
		<td colspan="10">
			<s:if test="%{#attr.auditHistoryRecords.size() > 0 && isVirtualInstance !='true'}">
				<s:include value="include-view-transaction-history.jsp"/>
    		</s:if>
    		<s:else>
    			&nbsp;
    		</s:else>
		</td>
	</tr>
</table>
<s:if test="%{#attr.isReverse}">
	<div align="center" class="ui-widget-header customDialogFooter reverseDialogWdt" >
			 
			    <sj:a
	                button="true"
					onClickTopics="closeDialog"
	        	><s:text name="jsp.default_82"/>
	        	</sj:a>
				<input class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" type="button" value="Send" onclick='ns.ach.reverseSend();'>
			
		   &nbsp;&nbsp;&nbsp;
	</div>
	</s:if>
<ffi:removeProperty name="SortURL"/>
