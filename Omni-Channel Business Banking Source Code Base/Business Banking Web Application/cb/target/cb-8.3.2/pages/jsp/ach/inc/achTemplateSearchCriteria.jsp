<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>

<div id="quicksearchcriteria" >
	<s:form action="/pages/jsp/ach/achTemplateSummary_verifyACHBatchTemplate.action" method="post" id="QuickSearchACHFormID1" name="QuickSearchTransfersForm" theme="simple">
		<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
		<div id="templatessearchcriteria" style="display:none">
			<div class="acntDashboard_masterHolder marginTop10">
				<div class="acntDashboard_itemHolder">
					<label><s:text name="ach.grid.templateName"/></label>
					<input id="viewForTemplateNameID" name="achBatchTemplateSearchCriteria.templateName" type="text" class="ui-widget-content ui-corner-all txtbox" size="18" maxlength="32" border="0" value=""/>
				</div>
			
				<div class="acntDashboard_itemHolder">
					<label><s:text name="ach.showQS"/></label>
					<s:select id="viewForACHCompanyId2" cssClass="txtbox" headerKey="" headerValue="%{getText('ach.allCompanyIDQS')}" list="companyList" name="achBatchTemplateSearchCriteria.companyID" listKey="companyID" listValue="%{companyName+'('+companyID+')'}"/>
				</div>
				<s:if test="%{consumerUser}">	
				<div class="acntDashboard_itemHolder">
					
					<input type="hidden" name="achBatchTemplateSearchCriteria.templateScope" value="USER"/>
	
				</div>
				</s:if>
				<s:else>
					<div class="acntDashboard_itemHolder">
						<label><s:text name="ach.grid.scope"/></label>
						<select id="viewForTemplateScopeID" class="txtbox" name="achBatchTemplateSearchCriteria.templateScope">
							
							<option value="BUSINESS"><!--L10NStart-->Business (General Use)<!--L10NEnd--></option>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
								<option value="USER"><!--L10NStart-->User (My Use)<!--L10NEnd--></option>
								<option value="" selected="selected"><!--L10NStart-->Both<!--L10NEnd--></option>
							</ffi:cinclude>
						</select>
					</div>
				</s:else>
			
				<div class="acntDashboard_itemHolder">
					<sj:a targets="quick" title="%{getText('jsp.default_6')}" id="quicksearchbutton2"  button="true" onCompleteTopics="quickSearchACHTemplatesComplete"  formIds="QuickSearchACHFormID1" >
						<s:text name="jsp.default_6"/>
					</sj:a><br>
				</div>
			</div>
		</div>
	</s:form>
	
	<s:form action="/pages/jsp/ach/achTemplateSummary_verifyACHBatchMultiTemplate.action" method="post" id="QuickSearchACHFormID2" name="QuickSearchTransfersForm" theme="simple">
		<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
		<div id="multitemplatessearchcriteria" style="display:none">
			<table>
				<tr>
					<td>
						<label for="">
							&nbsp;<s:text name="ach.grid.templateName"/>
						</label>
						<input id="multiACHTemplateNameID" name="achBatchMultiTemplateSearchCriteria.templateName" type="text" class="ui-widget-content ui-corner-all txtbox" size="18" maxlength="32" border="0" value=""/>
					</td>
					<td>
						<label for="">
							&nbsp;<s:text name="ach.grid.scope"/>
						</label>
						<select id="multiACHTemplateScopeID" class="txtbox" name="achBatchMultiTemplateSearchCriteria.templateScope">
							<option value="" selected="selected"><!--L10NStart-->Both<!--L10NEnd--></option>
							<option value="BUSINESS"><!--L10NStart-->Business (General Use)<!--L10NEnd--></option>
							<option value="USER"><!--L10NStart-->User (My Use)<!--L10NEnd--></option>
						</select>
					</td>
					<td>
						<sj:a targets="quick" title="%{getText('jsp.default_6')}" id="quicksearchbutton3"  button="true" onCompleteTopics="quickSearchACHTemplatesComplete" formIds="QuickSearchACHFormID2"><s:text name="jsp.default_6"  /></sj:a><br>
					</td>
				</tr>
			</table>
		</div>
	</s:form>	
</div>
<script type="text/javascript">
	$(function(){
		$("#viewForACHCompanyId2").selectmenu({width: 200});
		$("#viewForTemplateScopeID").selectmenu({width: 160});
		$("#multiACHTemplateScopeID").selectmenu({width: 160});
		
		ns.ach.currentTemplateSearchCriteria = "templatessearchcriteria";
	});
</script>