<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines,
                 com.ffusion.beans.ach.ACHOffsetAccount,
				 com.ffusion.beans.ach.ACHClassCode" %>
<%--
	This file is used by both Add/Edit ACH AND Add/Edit ACH Templates
	If you modify this file, make sure your change works for both please.
--%>
<ffi:help id="payments_achbatchaddedit" className="moduleHelpClass"/>
<s:action namespace="/pages/jsp/ach" name="ACHInitAction_initACHBatches" />

<%
	String subMenuSelected = "ach";
	boolean companySelected = false;
	boolean addEntryDisabled = false;
	boolean importedBatch = false;
	String secCodeDisplay = "";
	String selectedSECCode = "";
  	boolean isEdit = true;
	boolean isTemplate = false;
    boolean goToTemplatePage = false;
	boolean fromTemplate = false;
	boolean secChanged = false;			// need to redo effective Entry Date
	String frequencyValue = "";		// used in assignTo below

	boolean isScheduledACHBatch = false;
	if ((session.getAttribute("AddEditACHBatch") instanceof com.ffusion.tasks.ach.AddACHBatch)
		||(session.getAttribute("AddEditACHBatch") instanceof com.ffusion.tasks.ach.AddACHTaxPayment)
		||(session.getAttribute("AddEditACHBatch") instanceof com.ffusion.tasks.ach.AddChildSupportPayment)
		) {
		isEdit = false;
	}
	if (request.getParameter("Revise") != null) {
		%>
			<ffi:removeProperty name="AddEditACHTemplate" />
			<ffi:removeProperty name="ACHBatchTemplate" />
		<%
	}
	if (session.getAttribute("AddEditACHTemplate") != null)
	{
		isTemplate = true;
		isEdit = false;
		if (session.getAttribute("AddEditACHTemplate") instanceof com.ffusion.tasks.ach.ModifyACHTemplate){
			isEdit = true;
		}
        %>
        <ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="Add">
        <%
             isEdit = false;
        %>
        </ffi:cinclude>
        <%		
	}
    if (request.getParameter("AddEditACHBatch.CompanyID") != null)
    {
        String company_ID = (String)request.getParameter("AddEditACHBatch.CompanyID");
        %>
        <ffi:setProperty name="AddEditACHBatch" property="CompanyID" value='<%=company_ID%>'/>
    <%}
	if (request.getParameter("AddEditACHBatch.StandardEntryClassCode") != null)	// if they changed the SEC code, need to re-test default effective date
	{
		secChanged = true;
	%>
		<ffi:setProperty name="AddEditACHBatch" property="StandardEntryClassCode" value='<%=request.getParameter("AddEditACHBatch.StandardEntryClassCode") %>'/>
	<%
	}
%>
<ffi:cinclude value1="${AddEditACHBatch.Action}" value2="Add" >
    <%
         isEdit = false;
    %>
</ffi:cinclude>

<ffi:cinclude value1="${AddEditACHBatch.FromTemplate}" value2="true" >
	<% fromTemplate = true; %>
	<ffi:cinclude value1="${AddEditACHBatch.Date}" value2="" operator="equals">
		<% secChanged = true; %>
	</ffi:cinclude>
</ffi:cinclude>


<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
	<% subMenuSelected = "tax";
    	secCodeDisplay = "Tax Payment (CCD + TXP Credit)";
		selectedSECCode = EntitlementsDefines.CCD_TXP;
	%>
	<ffi:setProperty name="AddEditACHBatch" property="StandardEntryClassCode" value="<%= ACHClassCode.ACH_CLASSCODE_TEXT_CCD %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
	<% subMenuSelected = "child";
		secCodeDisplay = "Child Support Payment (CCD + DED Credit)";
        selectedSECCode = EntitlementsDefines.CCD_DED;
	%>
	<ffi:setProperty name="AddEditACHBatch" property="StandardEntryClassCode" value="<%= ACHClassCode.ACH_CLASSCODE_TEXT_CCD %>"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
	<% if (!fromTemplate)
        isTemplate = true;%>
</ffi:cinclude>

<%-- Load ACH Company according to the AddEditACHBatch.CompanyID after use selectes it --%>
<ffi:cinclude value1="${AddEditACHBatch.CompanyID}" value2="" operator="equals">
	<ffi:removeProperty name="ACHCOMPANY" />
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.StandardEntryClassCode}" value2="" operator="equals">
	<% secChanged = true; %>
</ffi:cinclude>

<ffi:cinclude value1="${AddEditACHBatch.CompanyID}" value2="" operator="notEquals">
<% String currentCompanyID = null;
   String batchCompanyID = null;
   companySelected = true;
   boolean reloadACHCompany = true; %>
	<ffi:getProperty name="AddEditACHBatch" property="CompanyID" assignTo="batchCompanyID"/>
	<ffi:cinclude value1="${ACHCOMPANY}" value2="" operator="notEquals">
		<ffi:setProperty name="AddEditACHBatch" property="CoID" value="${ACHCOMPANY.ID}" />
		<ffi:setProperty name="AddEditACHBatch" property="CoName" value="${ACHCOMPANY.CompanyName}" />
		<ffi:getProperty name="ACHCOMPANY" property="CompanyID" assignTo="currentCompanyID"/>
		<%
			if (currentCompanyID != null && currentCompanyID.equals(batchCompanyID))	// if already set to this company, don't do it again
				reloadACHCompany = false;
		%>
	</ffi:cinclude>
	<% if (reloadACHCompany) { %>
		<ffi:object id="SetACHCompany" name="com.ffusion.tasks.ach.SetACHCompany" scope="session"/>
		<ffi:setProperty name="SetACHCompany" property="CompanyID" value="${AddEditACHBatch.CompanyID}" />
		<ffi:process name="SetACHCompany" />
        <ffi:setProperty name="AddEditACHBatch" property="NewCompanyID" value="${AddEditACHBatch.CompanyID}" />
        <ffi:process name="AddEditACHBatch" />
        <%-- Set AddEditACHBatch.CoID from ACHCOMPANY--%>
        <ffi:setProperty name="AddEditACHBatch" property="CoID" value="${ACHCOMPANY.ID}" />
        <ffi:setProperty name="AddEditACHBatch" property="CoName" value="${ACHCOMPANY.CompanyName}" />

<% if (isTemplate) { %>
        <ffi:setProperty name="ACHCOMPANY" property="CurrentClassCodeTemplate" value="${AddEditACHBatch.StandardEntryClassCode}" />
<% } else { %>
        <ffi:setProperty name="ACHCOMPANY" property="CurrentClassCode" value="${AddEditACHBatch.StandardEntryClassCode}" />
<% } %>
        <%-- need to reload offset accounts if we changed ACH Companies --%>
        <ffi:removeProperty name="ACHOffsetAccounts" />
        <%	secChanged = true;			// if we changed ACH Companies, we need to recheck default effective entry date
	 } %>
</ffi:cinclude>
<ffi:cinclude value1="${ACHOffsetAccounts.CompanyID}" value2="${AddEditACHBatch.CoID}" operator="notEquals">
    <ffi:removeProperty name="ACHOffsetAccounts" />
</ffi:cinclude>

<ffi:cinclude value1="${AddEditACHBatch.ReturnToTemplatesPage}" value2="true">
    <% goToTemplatePage = true; %>
</ffi:cinclude>
<ffi:setProperty name="BackURL" value="${SecurePath}payments/achbatchaddedit.jsp" />
<ffi:setProperty name="CallerURL" value="${SecurePath}payments/achbatchaddedit.jsp"/>
<ffi:setProperty name="CallerForm" value="AddBatchForm"/>
<ffi:setProperty name="ProcessACHImport" property="ClearACHEntries" value="false"/> <%-- just in case our referring page is the cancel button of the achimportconfirm page --%>
<ffi:removeProperty name="ImportErrors" />

<ffi:object id="SearchACHPayees" name="com.ffusion.tasks.ach.SearchACHPayees" scope="session"/>
<ffi:removeProperty name="FILTERED_ACHPAYEES"/>

<%  boolean anyEntries = false;
	boolean canRecurring = true;
	String headerTitle = "";		// used in assignTo below
   	String headerValue = "";		// used in assignTo below
	String coEntryDesc = "";
	String classCode;
	String pageHeading = "";
	if (subMenuSelected.equals("tax"))
	{
		pageHeading = "<!--L10NStart-->Tax Payment<!--L10NEnd-->";
		canRecurring = false;
	} else
	if (subMenuSelected.equals("child"))
	{
		pageHeading = "<!--L10NStart-->Child Support Payment<!--L10NEnd-->";
	}
	else
	{
		pageHeading = "<!--L10NStart-->ACH Payment<!--L10NEnd-->";
	}
	session.setAttribute("PageHeading", (isEdit?"<!--L10NStart-->Edit<!--L10NEnd--> ":"Add ") + pageHeading + (isTemplate?" <!--L10NStart-->Template<!--L10NEnd-->":"") );
	if (request.getParameter("PayeeIDs") != null) { %>
	<ffi:cinclude value1="${CreateEntriesForPayees}" value2="" operator="equals">
		<ffi:object id="CreateEntriesForPayees" name="com.ffusion.tasks.ach.CreateEntriesForPayees" scope="session"/>
	</ffi:cinclude>
	<ffi:setProperty name="CreateEntriesForPayees" property="BatchName" value="AddEditACHBatch"/>
	<ffi:setProperty name="CreateEntriesForPayees" property="PayeeIDs" value="${PayeeIDs}" />
	<ffi:process name="CreateEntriesForPayees"/>
    <ffi:removeProperty name="PayeeIDs" />
<% }
if (request.getParameter("defaultTaxFormID") != null) {
	session.setAttribute("DefaultTaxFormID",request.getParameter("defaultTaxFormID"));
%>
    <ffi:cinclude value1="${AddEditACHBatch.DefaultTaxFormID}" value2="${DefaultTaxFormID}" operator="notEquals">
        <ffi:setProperty name="AddEditACHBatch" property="DefaultTaxFormID" value="${DefaultTaxFormID}"/>
        <ffi:process name="AddEditACHBatch"/>
    </ffi:cinclude>
    <ffi:removeProperty name="DefaultTaxFormID" />
<% } %>
<span id="PageHeading" style="display:none;"><%="" + session.getAttribute("PageHeading")%></span>
    
	<ffi:cinclude value1="${AddEditACHBatch.NumberPayments}" value2="999"><ffi:setProperty name="OpenEnded" value="true" /></ffi:cinclude>
    <ffi:setProperty name="NumberPayments" value="${AddEditACHBatch.NumberPayments}" />
    <ffi:setProperty name="Frequency" value="${AddEditACHBatch.Frequency}" />
<%
if (session.getAttribute("Initialize") != null || request.getParameter("Initialize")!= null) { %>
    <ffi:removeProperty name="Initialize" />
   

	<ffi:setProperty name="ACHCOMPANIES" property="SortedBy" value="CONAME" />
<% }
if (session.getAttribute("ACHOffsetAccounts") == null && session.getAttribute("ACHCOMPANY") != null) { %>
	<ffi:object id="GetACHOffsetAccounts" name="com.ffusion.tasks.ach.GetACHOffsetAccounts" scope="session"/>
	<ffi:process name="GetACHOffsetAccounts"/>
	<ffi:removeProperty name="GetACHOffsetAccounts"/>
	<ffi:setProperty name="ACHOffsetAccounts" property="SortedBy" value="OFFSETACCOUNTNUMBER" />
<% } %>

<ffi:cinclude value1="${OpenEnded}" value2="true" >
<ffi:setProperty name="NumberPayments" value="999"/>
<ffi:setProperty name="AddEditACHBatch" property="NumberPayments" value="${NumberPayments}"/>
</ffi:cinclude>
<ffi:object name="com.ffusion.tasks.ach.SetACHOffsetAccount" id="SetACHOffsetAccount" scope="session"/>
<ffi:setProperty name="SetACHOffsetAccount" property="OffsetAccountInSessionName" value="ACHOffsetAccount" />
<ffi:setProperty name="SetACHOffsetAccount" property="OffsetAccountsInSessionName" value="ACHOffsetAccounts" />
<ffi:cinclude value1="" value2="${SetACHOffsetAccount.OffsetAccountID}" operator="equals" >
	<ffi:setProperty name="ACHOffsetAccounts" property="Filter" value="All" />
	<ffi:list collection="ACHOffsetAccounts" items="OffsetAccount" startIndex="1" endIndex="1">
		<ffi:setProperty name="SetACHOffsetAccount" property="OffsetAccountID" value="${OffsetAccount.ID}" />
	</ffi:list>
</ffi:cinclude>

<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
<ffi:setProperty name="SortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="Reverse Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="ACHEntries"/>
<ffi:process name="SortImage"/>

<ffi:object name="com.ffusion.tasks.ach.ValidateACHBatch" id="ValidateACHBatch" scope="session"/>
<ffi:setProperty name="ValidateACHBatch" property="SuccessURL" value=""/>
<ffi:setProperty name="ValidateACHBatch" property="ValidationErrorsURL" value=""/>
<% if ("true".equalsIgnoreCase(request.getParameter("ValidateBatch"))) { %>
	<ffi:process name="ValidateACHBatch"/>
<% }

    if (request.getParameter("ToggleSortedBy") != null) {
  	String toggle=""; %>
	<ffi:getProperty name="ToggleSortedBy" assignTo="toggle"/>
  	<ffi:setProperty name="ACHEntries" property="ToggleSortedBy" value="<%= toggle %>" />
<% } %>

<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="classCode" />
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
	<% canRecurring = false;            // multiple batch templates - no recurring/repeating
    %>
</ffi:cinclude>

<% String tTrackingId = "";
   String tSrvrtId = ""; %>
<ffi:getProperty name="AddEditACHBatch" property="ID" assignTo="tSrvrtId"/>
<ffi:getProperty name="AddEditACHBatch" property="TrackingID"  assignTo="tTrackingId" />

<% if ( tTrackingId != null && !"".equals( tTrackingId )  ) {  %>
<ffi:object id="GetAuditTransferById" name="com.ffusion.tasks.banking.GetAuditTransferById" scope="session"/>
    <ffi:setProperty name="GetAuditTransferById" property="TrackingID" value='<%= tTrackingId %>'/>
    <ffi:setProperty name="GetAuditTransferById" property="ID" value='<%= tSrvrtId %>'/>
    <ffi:setProperty name="GetAuditTransferById" property="BeanSessionName" value="TransactionHistory"/>
    <ffi:setProperty name="GetAuditTransferById" property="Template" value="<%= String.valueOf(isTemplate) %>"/>
<ffi:process name="GetAuditTransferById"/>
<% }  %>


<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
<%
	    if (request.getParameter("SetACHBatchType") != null)	// if they changed the SEC code, need to re-test default effective date
    	{

            %>
            <ffi:setProperty name="AddEditACHBatch" property="ChangeToACHBatchType" value="true"/>
            <%
    	}
%>
</ffi:cinclude>

<%-- make sure totals for entries are correct --%>
<ffi:setProperty name="AddEditACHBatch" property="RecalculateTotals" value="true"/>
<ffi:process name="AddEditACHBatch"/>

<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
	<% subMenuSelected = "ach";
	%>
</ffi:cinclude>

<script type="text/javascript"><!--

ns.ach.changeACHClassCode = function() {
// we need to serialize the data so it doesn't get lost before changing class code
    var strutsActionName = '<ffi:getProperty name="strutsActionName"/>' + '_reloadProcessing.action';
	$.ajax({
		type: "POST",
        url: "/cb/pages/jsp/ach/" + strutsActionName,
        data: $("#AddBatchNew").serialize(),
        success: function(data) {
    		$('#inputDiv').html(data);
    		
    		$.publish("common.topics.tabifyDesktop");
    		if(accessibility){
    			$("#inputDiv").setFocus();
        }
        }
    });
};

ns.ach.changeACHCompany = function(companyID) {
	var strutsActionName = '<ffi:getProperty name="strutsActionName"/>' + '_reloadProcessing.action';
	$.ajax({
		type: "POST",
		url: "/cb/pages/jsp/ach/" + strutsActionName,
        data: $("#AddBatchNew").serialize(),
        success: function(data) {
    		$('#inputDiv').html(data);
    		
    		$.publish("common.topics.tabifyDesktop");
    		if(accessibility){
    			$("#inputDiv").setFocus();
    		}
        }
    });
};

function importACHEntries(isEntries)
{
    // we need to serialize the data so it doesn't get lost before importing entries
    var strutsActionName = '<ffi:getProperty name="strutsActionName"/>' + '_saveParameters.action';
	var importACHEntriesUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achimport.jsp?PartialImport=false"/>';
    ns.ach.doPartialImport = false;
    if (isEntries == false)
    {
        importACHEntriesUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achimport.jsp?PartialImport=true"/>';
        ns.ach.doPartialImport = true;
    }
    $.ajax({
        type: "POST",
        url: "/cb/pages/jsp/ach/" + strutsActionName,
        data: $("#AddBatchNew").serialize(),
        success: function(data) {
            ns.ach.importEntries(importACHEntriesUrl);
        }
    });
}

function addACHEntry() {
	var strutsActionName = '<ffi:getProperty name="strutsActionName"/>' + '_saveParameters.action';
    var addEditEntryUrl = "";
    <ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
        addEditEntryUrl = "/cb/pages/jsp/ach/addTaxChildACHEntryAction_init.action";
    </ffi:cinclude>
    <ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="equals" >
        addEditEntryUrl = "/cb/pages/jsp/ach/addACHEntryAction_init.action";
    </ffi:cinclude>
    var defaultTaxFormID = $("#defaultTaxFormID");
    if(defaultTaxFormID != 'undefined' && defaultTaxFormID != undefined) {
        if($("#defaultTaxFormID").val() != undefined) {
            addEditEntryUrl = addEditEntryUrl + "?defaultTaxFormID=" + $("#defaultTaxFormID").val();
        }
    }
	$.ajax({
        url: "/cb/pages/jsp/ach/" + strutsActionName,
		type: "POST",
        data: $("#AddBatchNew").serialize(),
        success: function(data) {
            ns.ach.addEditEntry(addEditEntryUrl);
        }
    });

}

function setAllAmounts () {
    var action = $("#AddBatchNew").attr("action");
	var len = action.indexOf("_");
	var urlStr = action.substr(0,len) + "_setAllAmountsInit.action";
	$.ajax({
		type: "POST",
        url: urlStr,
        data: $("#AddBatchNew").serialize(),
        success: function(data) {
    		$('#setAllAmountDialogID').html(data).dialog("open");
        }
    });
}

function setACHBatchType(urlString) {
	$.ajax({
        url: urlString,
		type: "POST",
        data: $("#AddBatchNew").serialize(),
        success: function(data) {
    		$('#inputDiv').html(data);
    		
    		$.publish("common.topics.tabifyDesktop");
    		if(accessibility){
    			$("#inputDiv").setFocus();
    		}
        }
    });
}

function changeChildspType() {
	if($("#defaultTaxFormID").val() == '') {
		$("#setACHBatchTypeId").addClass("ui-state-disabled");
		$("#addEntryButtonId").addClass("ui-state-disabled");
		document.getElementById("addEntryButtonId").disabled = true;
		document.getElementById("setACHBatchTypeId").disabled = true;
	} else {
		$("#setACHBatchTypeId").removeClass("ui-state-disabled");
		$("#addEntryButtonId").removeClass("ui-state-disabled");
		//$("#addEntryButtonId").removeAttr("disabled");
		document.getElementById("addEntryButtonId").disabled = false;
		document.getElementById("setACHBatchTypeId").disabled = false;
	}
	
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
		$("#inputDiv").setFocus();
	}
}

function changePaymentType(clearValue) {
	var secChanged = "false";
	<% if (secChanged) { %>
	    secChanged = "true";
	<%}%>
	
	if($("#paymentTypeID").length > 0){
		if($("#paymentTypeID").val() == '') {
	        $("#coEntryDesc").removeClass("ui-state-disabled");
	        if (clearValue == 'true')
	            $("#coEntryDesc").val("");
	        if (document.getElementById("coEntryDesc") != null)
	            document.getElementById("coEntryDesc").disabled = false;
	        if (document.getElementById("coEntryDescHidden") != null)
	            document.getElementById("coEntryDescHidden").disabled = true;
		} else if ($("#paymentTypeID").val() == 'Healthcare') {
	        $("#coEntryDesc").addClass("ui-state-disabled");
	        $("#coEntryDesc").val("HCCLAIMPMT");
	        if (document.getElementById("coEntryDesc") != null)
	            document.getElementById("coEntryDesc").disabled = true;
	        if (document.getElementById("coEntryDescHidden") != null)
	            document.getElementById("coEntryDescHidden").disabled = false;
	        $("#coEntryDescHidden").val("HCCLAIMPMT");
		}
	}else  if (secChanged == "true"){
		if ($("#coEntryDesc").val() == "HCCLAIMPMT")
			$("#coEntryDesc").val("");
        if (document.getElementById("coEntryDesc") != null)
          document.getElementById("coEntryDesc").disabled = false;
        if (document.getElementById("coEntryDescHidden") != null)
          document.getElementById("coEntryDescHidden").disabled = true;
	}
}

 function tempHistoryDisplay(show)
 {
     if (show)
     {
        document.getElementById("tempHistory").style.display = "inline";
        document.getElementById("viewTempHistory").style.display = "none";
        document.getElementById("hideTempHistory").style.display = "inline";
		$("#hideTempHistory").focus();
     }
     else
     {
        document.getElementById("tempHistory").style.display = "none";
        document.getElementById("viewTempHistory").style.display = "inline";
        document.getElementById("hideTempHistory").style.display = "none";
		 $("#viewTempHistory").focus();
     }
 }
function doSearch()
{
    document.AddBatchForm.action='<ffi:urlEncrypt url="${SecurePath}payments/achbatchaddedit.jsp?DoSearch=true"/>';
    document.forms['AddBatchForm'].submit();
    return false;
}
function gotoPage(page)
{
    document.AddBatchForm.action='<ffi:urlEncrypt url="${SecurePath}payments/achbatchaddedit.jsp?GoToPage=true"/>';
    document.forms['AddBatchForm'].submit();
    return false;
}
 //--></script>

<script language="JavaScript">
<!--

    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
    <ffi:process name="ACHResource" />
    var accountTypeList = null;

/* Mappings of banks to indices into the account lists.*/
var countries = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCodes" property="Size"/>);
var iatCurrencyList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCodes" property="Size"/>);
<% int countryIndex = 0;
%>
<ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
    countries[ <%= countryIndex%> ] = "<ffi:getProperty name="countryCode"/>";
    iatCurrencyList[ <%= countryIndex%> ] = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyList${countryCode}"/><ffi:getProperty name="ACHResource" property="Resource"/>";
    <% countryIndex++; %>
</ffi:list>

var iatForeignExchangeIndicatorFF = "<ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicatorFF"/><ffi:getProperty name="ACHResource" property="Resource"/>";
var iatForeignExchangeIndicatorFV = "<ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicatorFV"/><ffi:getProperty name="ACHResource" property="Resource"/>";
var iatForeignExchangeIndicatorVF = "<ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicatorVF"/><ffi:getProperty name="ACHResource" property="Resource"/>";

var iatSupportedCurrencyList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCurrencyList" property="Size"/>);
var iatSupportedCurrencyNamesList = new Array(<ffi:getProperty name="ACHIAT_DestinationCountryCurrencyList" property="Size"/>);
<% int currencyIndex = 0; %>
<ffi:list collection="ACHIAT_DestinationCountryCurrencyList" items="currencyCode">
    iatSupportedCurrencyList [ <%= currencyIndex%> ] = "<ffi:getProperty name="currencyCode"/>";
    iatSupportedCurrencyNamesList [ <%=currencyIndex%> ] = "<ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${currencyCode}"/><ffi:getProperty name="ACHResource" property="Resource"/>";
    <% currencyIndex++; %>
</ffi:list>

    function updateForeignExchangeMethod(currency)
    {
        // There is a one-to-one mapping of currency to Foreign Exchange Method
        var currencySel = document.getElementById("CURRENCY");
        var foreignSel = document.getElementById("FOREIGNEXCHANGE");
        var countrySel = document.getElementById("countryID");
        var addEntry = document.getElementById("addEntryButtonId");
        var importEntry = document.getElementById("IMPORTENTRIES");

        if (countrySel == null || currencySel == null || foreignSel == null || currency == null)
            return;

        //clear out entries in the lists
        while( foreignSel.options.length > 0 ){
            foreignSel.options[ 0 ] = null;
        }

        // assume FF for default
        var value = "FF";
        var text = value + "-" + iatForeignExchangeIndicatorFF;
        // for USA, support USD, USD (Same Day), USD (Next Day)
        if (currency.value.indexOf("USD") != 0 && currency.value.indexOf("USS") != 0 && currency.value.indexOf("USN") != 0)
        {
            value = "FV";
            text = value + "-" + iatForeignExchangeIndicatorFV;
        }
// VF not supported when USD is originating currency
<%--            if (supportedForeignExchange.indexOf("VF") == 0)--%>
<%--            {--%>
<%--                value = "VF";--%>
<%--                text = value + "-" + iatForeignExchangeIndicatorVF;--%>
<%--            }--%>
        if (currency.selectedIndex == 0)
        {
            value = "";
            text = currency.options[0].text;
            if (addEntry != null) {
                addEntry.disabled = true;
                $('#addEntryButtonId').addClass('ui-state-disabled');
            }
            if (importEntry != null) {
                importEntry.disabled = true;
                $('#IMPORTENTRIES').addClass('ui-state-disabled');
            }
        }
        var newOpt = new Option(text, value);
        foreignSel.options[ 0 ] = newOpt;
        $("#FOREIGNEXCHANGE").selectmenu('destroy').selectmenu({width: 160});

        if (currency.selectedIndex != 0 && countrySel.selectedIndex != 0)
        {
            if (addEntry != null){
                addEntry.disabled = false;
                $('#addEntryButtonId').removeClass('ui-state-disabled');
            }
            if (importEntry != null) {
                importEntry.disabled = false;
                $('#IMPORTENTRIES').removeClass('ui-state-disabled');
            }
        }
    }

    function updateCurrencyInfo(country)
    {
        var currencySel = document.getElementById("CURRENCY");
        var foreignSel = document.getElementById("FOREIGNEXCHANGE");
        var addEntry = document.getElementById("addEntryButtonId");
        var importEntry = document.getElementById("IMPORTENTRIES");
        var countrySel = document.getElementById("countryID");
        var truncatedText = document.getElementById("TruncatedText");
        if (currencySel == null || foreignSel == null || country == null)
            return;
        var supportedCurrencies = "";

        for (var i = 0; i < countries.length; i++)
        {
            if (countries[ i ] == country.value)
            {
                supportedCurrencies = iatCurrencyList[ i ];

                if (truncatedText != null)
                {
                    taxtitle = countrySel.options[ countrySel.selectedIndex ].title;
                    truncatedText.innerHTML = taxtitle;
                    if (taxtitle.length > 25)
                    {
                        truncatedText.style.visibility = "visible";
                        truncatedText.style.display = "";
                    }
                    else
                    {
                        truncatedText.style.visibility = "hidden";      // only show if too long for drop-down
                        truncatedText.style.display = "none";
                    }
                 }

                if (addEntry != null){
                    addEntry.disabled = false;
                    $('#addEntryButtonId').removeClass('ui-state-disabled');
                }
                if (importEntry != null) {
                    importEntry.disabled = false;
                    $('#IMPORTENTRIES').removeClass('ui-state-disabled');
                }

                var found = "";
                currencySel.selectedIndex = 0;
                while (true)
                {
                    for (var j = 0; j < iatSupportedCurrencyList.length; j++)
                    {
                        var value = iatSupportedCurrencyList[ j ];
                        var text = iatSupportedCurrencyNamesList[ j ];

                        if (value.length == 0 || supportedCurrencies.indexOf(value) == 0)
                        {
                            if (found == "")
                            {
                                found = "1";
                                currencySel.selectedIndex = j+1;        // "Select Currency" is first item
                                $('#CURRENCY').selectmenu('destroy').selectmenu({width: 160});
                                updateForeignExchangeMethod(currencySel);
                                break;
                            }
                        }
                    }
                    if (supportedCurrencies.length <= 3)
                        break;
                    supportedCurrencies = supportedCurrencies.substr(4);        // skip the first currency and comma
                }
                // we updated the ForeignExchangeMethod with the first currency displayed
                break;
            } // if found country
        }   // for loop
        if (supportedCurrencies == "")
        {
        	if (addEntry != null) {
                addEntry.disabled = true;
                $('#addEntryButtonId').addClass('ui-state-disabled');
            }
            if (importEntry != null) {
                importEntry.disabled = true;
                $('#IMPORTENTRIES').addClass('ui-state-disabled');
            }
            currencySel.selectedIndex = 0;      // "Select Country First"
            $('#CURRENCY').selectmenu('destroy').selectmenu({width: 160});
            if (foreignSel != null){
                //clear out entries in the lists
                while( foreignSel.options.length > 0 ){
                    foreignSel.options[ 0 ] = null;
                }
                var newOpt = new Option(currencySel.options[0].text, "");
                foreignSel.options[ 0 ] = newOpt;
                $("#FOREIGNEXCHANGE").selectmenu('destroy').selectmenu({width: 160});
            }
        }
    }

function popCalendar(){
	secCode = "CCD";        // the default for Tax and Child Support
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
	<% if (isEdit) { %>
		secCode = '<ffi:getProperty name="ACHCOMPANY" property="CurrentClassCode"/>';
	<% } else { %>
	if(document.AddBatchForm["AddEditACHBatch.StandardEntryClassCode"] != null){
		secCode = document.AddBatchForm["AddEditACHBatch.StandardEntryClassCode"].options[document.AddBatchForm["AddEditACHBatch.StandardEntryClassCode"].selectedIndex].value;
	}
	else
	{
	secCode='<ffi:getProperty name="ACHCOMPANY" property="CurrentClassCode"/>';
	}

	<% } %>
</ffi:cinclude>
	window.open("<ffi:getProperty name='SecurePath'/>calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=9&calDisplay=select&calForm=AddBatchForm&calTarget=AddEditACHBatch.Date&fromACHorTax=true&ACHCompanyID=${ACHCOMPANY.ID}&SECCode=" + secCode,"Calendar","width=350,height=300");
}

function toggleRecurring(){
	if(document.AddBatchForm.OpenEnded != null){
	if (document.AddBatchForm.OpenEnded[1].checked == true){
		if(document.AddBatchForm.numberPayments != null)
		document.AddBatchForm.numberPayments.disabled = false;
	} else {
		if(document.AddBatchForm.numberPayments != null)
		document.AddBatchForm.numberPayments.disabled = true;
	}
    }
}

function holdAll(holdCheckBox){
	var flag;
    if (holdCheckBox.checked) {
        flag = "true";
    } else {
        flag = "false";
    }
    var action = $("#AddBatchNew").attr("action");
	var len = action.indexOf("_");
	//var urlStr = action.substr(0,len) + "_holdAllEntries.action";
	var urlStr = action.substr(0,len) + "_holdAllEntries.action?holdAllEntries=" + flag;

    $.ajax({
        type: "POST",
        url: urlStr,
		data: $("#AddBatchNew").serialize(),
		//data: {holdAllEntries: flag,CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
        async: false,
        success: function(data) {
            // reload the Entry Grid DIV 
            $.get("/cb/pages/jsp/ach/achbatchaddeditentryheader.jsp", function(data) {
                $('#achEntryHeaderDiv').html(data);
            });
            $('#achEntriesGridId').trigger("reloadGrid");
	    }
    });
    return false;
}

function changeHold(holdCheckBox, ActiveID){
    var crdr = document.getElementById("crdr"+ActiveID).value;
    var frm = document.AddBatchForm;
    var tag = null;
	if (crdr.indexOf("Credit") >= 0)
	{
		tag = frm['TotalNumberCredits'];
	} else
	{
		tag = frm['TotalNumberDebits'];
	}
	var value = tag.value;
	var total = parseInt(value);
    var amtStr = "";
	if (holdCheckBox.checked == true){
		document.getElementById("active"+ActiveID).value = "false";
		total -= 1;			// subtract out previous count
		amtStr = "" + total;
		tag.value = amtStr;
	} else {
		document.getElementById("active"+ActiveID).value = "true";
		total += 1;			// add in previous count
		amtStr = "" + total;
		tag.value = amtStr;
	}
	totalThisAmount(ActiveID);
	
}

function modifyFrequency(){
<% if (!fromTemplate) {
   if (!isEdit) { %>

	if (document.AddBatchForm.frequency != null)
	{
		if (document.AddBatchForm.frequency.selectedIndex == 0){
			if(document.AddBatchForm.OpenEnded !=null)
			document.AddBatchForm.OpenEnded[0].disabled = true;
			if(document.AddBatchForm.OpenEnded !=null)
			document.AddBatchForm.OpenEnded[1].disabled = true;
			if(document.AddBatchForm.numberPayments !=null)
			document.AddBatchForm.numberPayments.disabled = true;
			document.AddBatchForm.numberPayments.value = "1";
		} else {
			if(document.AddBatchForm.OpenEnded !=null)
			document.AddBatchForm.OpenEnded[0].disabled = false;
			if(document.AddBatchForm.OpenEnded !=null)
			document.AddBatchForm.OpenEnded[1].disabled = false;
			if(document.AddBatchForm.numberPayments !=null)
			document.AddBatchForm.numberPayments.disabled = false;
			if ( document.AddBatchForm.numberPayments.value == "0" || document.AddBatchForm.numberPayments.value == "1")
				document.AddBatchForm.numberPayments.value = "2";
			toggleRecurring();
		}
	}
<% } else { %>
	if (document.AddBatchForm.frequency != null) {
			if(document.AddBatchForm.OpenEnded !=null)
			document.AddBatchForm.OpenEnded[0].disabled = false;
			if(document.AddBatchForm.OpenEnded !=null)
			document.AddBatchForm.OpenEnded[1].disabled = false;
			if(document.AddBatchForm.numberPayments !=null)
			document.AddBatchForm.numberPayments.disabled = false;
			toggleRecurring();
	}
<% }
 } %>
}

function totalThisAmount(ID) {
	var creditAmtTotal = 0.0;
	var debitAmtTotal = 0.0;
	var frm = document.AddBatchForm;
    var tag = null;
	var newAmount = document.getElementById("amount"+ID).value;
	while (newAmount.indexOf(',') > -1)
	{
		newAmount = newAmount.substr(0,newAmount.indexOf(',')) + newAmount.substr(newAmount.indexOf(',')+1);
	}
	var crdr = document.getElementById("crdr"+ID).value;
	var prevAmount = crdr.substr(0,crdr.indexOf(' '));
	while (prevAmount.indexOf(',') > -1)
	{
		prevAmount = prevAmount.substr(0,prevAmount.indexOf(',')) + prevAmount.substr(prevAmount.indexOf(',')+1);
	}
	if (crdr.indexOf("Credit") >= 0)
	{
		crdr = "Credit";
		tag = frm['TotalCreditAmount'];
	} else
	{
		crdr = "Debit";
		tag = frm['TotalDebitAmount'];
	}
	

    // task validation will catch an invalid amount
	//if (!checkACHAmount( frm["AddEditACHBatch.CurrentEntry="+ID+"&AddEditACHBatch.EntryAmount"]))
	//{
	//	newAmount = "0.00";
	//}
	if (document.getElementById("active"+ID).value == "false")
	{
		newAmount = "0.00";         // if inactive, set amount to "0.00" so it doesn't get added in.
	}
	document.getElementById("crdr"+ID).value = newAmount + ' ' + crdr ;
	var amt = parseFloat(prevAmount);
	if (isNaN(amt))
		amt = parseFloat("0.00");
	var newamt = parseFloat(newAmount);
	if (isNaN(newamt))
		newamt = parseFloat("0.00");

	var value = tag.value;
	while (value.indexOf(',') > -1)
	{
		value = value.substr(0,value.indexOf(',')) + value.substr(value.indexOf(',')+1);
	}
	var amtTotal = parseFloat(value);
	amtTotal -= amt;			// subtract out previous amount
	amtTotal += newamt;			// add in new amount

	amtTotal = Math.round(amtTotal*Math.pow(10,2))/Math.pow(10,2);
	var amtStr;
	if(amtTotal < 0) {
		amtStr = "0.00";
	} else {
		amtStr = "" + amtTotal;
	}
// WARNING.... Custom Code for US follows, Decimal = ".", grouping separator = ","
	if (amtStr.indexOf('.') == -1) { amtStr = amtStr+".0" } // Add .0 if a decimal point doesn't exist
	temp=(amtStr.length-amtStr.indexOf('.')); // Find out if the # ends in a tenth (ie 145.5)
	if (temp <= 2) { amtStr=amtStr+"0"; }; // if it ends in a tenth, add an extra zero to the string
// add in commas again
	var x = amtStr.indexOf('.') - 3;
	while (x > 0)
	{
		amtStr = amtStr.substr(0,x) + ',' + amtStr.substr(x, amtStr.length);
		x = amtStr.indexOf(',')-3;
	}
	tag.value = amtStr;
}
// --></script>
<s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />

<div >
<s:form id="AddBatchNew" namespace="/pages/jsp/ach" validate="false" action="%{#session.strutsActionName + '_verify'}" method="post" name="AddBatchForm" theme="simple">
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="tableData">
						<tr>
							<td class="columndata lightBackground">

                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<s:hidden id="ID" name="ID" value="%{ID}"/>
                    	<input type="hidden" id="batchTypeId" name="batchTypeh" value="<ffi:getProperty name='subMenuSelected'/>"/>
<ffi:setProperty name="ValidateACHBatch" property="ValidationErrorsURL" value="${SecurePath}payments/achbatchaddedit.jsp"/>
					<% if (isTemplate) { %>
                        <ffi:setProperty name="tempURL" value="${SecurePath}wait.jsp?target=payments/achtemplateaddeditsave.jsp" URLEncrypt="true"/>
<ffi:setProperty name="ValidateACHBatch" property="SuccessURL" value="${tempURL}"/>
                        <ffi:removeProperty name="tempURL"/>
					<% } else { %>
<ffi:setProperty name="ValidateACHBatch" property="SuccessURL" value="${SecurePath}payments/achbatchaddeditconfirm.jsp"/>
					<% } %>
							<div align="center">
								<div align="left">
								<table width="98%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td class="sectionhead lightBackground" colspan="7"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="692" height="1" border="0"></td>
									</tr>
									<tr valign="top">
								<%
									String title = "";
                                    String[] titleStrings;
	String[] achStrings = {
						      "<!--L10NStart-->Create ACH Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create ACH Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create ACH Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit ACH Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit ACH Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit ACH Batch (from template)<!--L10NEnd-->",
    };
	String[] taxStrings = {
						      "<!--L10NStart-->Create Tax Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create Tax Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create Tax Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit Tax Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Tax Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Tax Batch (from template)<!--L10NEnd-->",
    };
	String[] childStrings = {
						      "<!--L10NStart-->Create Child Support Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Create Child Support Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Create Child Support Batch (from template)<!--L10NEnd-->",
                              "<!--L10NStart-->Edit Child Support Batch<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Child Support Batch Template<!--L10NEnd-->",
						      "<!--L10NStart-->Edit Child Support Batch (from template)<!--L10NEnd-->",
    };
                                        int titleIndex = 0;
										if (subMenuSelected.equals("tax")) {
                                            titleStrings = taxStrings;
										} else if (subMenuSelected.equals("child")) {
                                            titleStrings = childStrings;
										} else {
                                            titleStrings = achStrings;
										}
    if (isEdit)
        titleIndex = 3;
//    if (isView)
//        titleIndex = 6;
//    if (isDelete)
//        titleIndex = 9;
    if (isTemplate)
        titleIndex ++;
    else
    if (fromTemplate)
        titleIndex += 2;
    title = titleStrings[titleIndex];
									String tempEntToCheck;
									if (isTemplate) {
										if (subMenuSelected.equals("tax")) {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_TEMPLATE );
										} else if (subMenuSelected.equals("child")) {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_TEMPLATE );
										} else {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_TEMPLATE );
										}
									} else
									{
										if (subMenuSelected.equals("tax")) {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.TAX_PAYMENTS, EntitlementsDefines.ACH_PAYMENT_ENTRY );
										} else if (subMenuSelected.equals("child")) {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.CHILD_SUPPORT, EntitlementsDefines.ACH_PAYMENT_ENTRY );
										} else {
											tempEntToCheck = com.ffusion.util.entitlements.util.EntitlementsUtil.getACHEntitlementName( EntitlementsDefines.ACH_BATCH, EntitlementsDefines.ACH_PAYMENT_ENTRY );
										}
									}
									%>
										<td class="sectionhead tbrd_b ltrow2_color" colspan="7"><span class="sectionhead">&gt; <%= title %><br>
										</span>

										</td>

									</tr>
<br/>
	<ffi:cinclude value1="${AddEditACHBatch.AnyEntries}" value2="true" operator="equals" >
	<% anyEntries = true; %>
	</ffi:cinclude>
									<tr class="lightBackground">
										
										<td id="achBatchCompanyLabel" width="249" align="right"><span class="sectionsubhead"><s:text name="ach.grid.companyName" /> </span><span class="required">*</span></td>
										<td class="columndata" colspan="5">
										<ffi:cinclude ifEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" checkParent="true">
											<select id="AddEditACHBatchCompanyID" class="txtbox" name="AddEditACHBatch.CompanyID" <%=anyEntries || isEdit ?" disabled " : " "%> onchange="ns.ach.changeACHCompany(this.value);">
<% if (!companySelected) {
	addEntryDisabled = true;
%>
											<option value='' ><s:text name="ach.select.an.ACH.company" /></option>
<% } %>
											<ffi:list collection="ACHCOMPANIES" items="ACHCompany">
											<% boolean include_company = true; %>
											    <ffi:cinclude ifEntitled="<%= tempEntToCheck %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCompany.CompanyID}" checkParent="true">
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
<%   if (!isTemplate) { %>
	<ffi:cinclude value1="${ACHCompany.ACHPaymentEntitled}" value2="FALSE">
		<% include_company = false; 		// there are no SEC Codes available for this ACH Company - don't display it %>
	</ffi:cinclude>
<% }
   if (anyEntries) {
   if (isTemplate) { %>
        <ffi:setProperty name="ACHCompany" property="CurrentClassCodeTemplate" value="${AddEditACHBatch.StandardEntryClassCode}" />
<% } else { %>
        <ffi:setProperty name="ACHCompany" property="CurrentClassCode" value="${AddEditACHBatch.StandardEntryClassCode}" />
<% } %>
    <ffi:cinclude value1="${ACHCompany.ClassCodeEntitled}" value2="TRUE" operator="notEquals">
		<% include_company = false; 		// there are no SEC Codes available for this ACH Company - don't display it %>
    </ffi:cinclude>
<% } %>
</ffi:cinclude>
<%
	if (include_company) {
%>
												<option value='<ffi:getProperty name="ACHCompany" property="CompanyID" />' <ffi:cinclude value1="${ACHCOMPANY.CompanyID}" value2="${ACHCompany.CompanyID}">selected</ffi:cinclude>><ffi:getProperty name="ACHCompany" property="CompanyName" />&nbsp;-&nbsp;<ffi:getProperty name="ACHCompany" property="CompanyID" /></option>
<% } %>
											    </ffi:cinclude>
											</ffi:list>
											</select>
										</ffi:cinclude>
										<div id="companyIDError" style="color:red;"></div>
										</td>
									</tr>
									<tr class="lightBackground">
										<td id="achBatchTypeLabel" align="right"><span class="sectionsubhead"><s:text name="jsp.default_67" /> </span><span class="required">*</span></td>
										<td class="columndata" colspan="5">
								<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
								<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
								<ffi:process name="ACHResource" />

<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
<% if (!anyEntries) { %>
											<select class="txtbox" id="standardEntryClassCodeId" name="AddEditACHBatch.StandardEntryClassCode" <%= companySelected ? "" : "disabled"%> onchange="ns.ach.changeACHClassCode();">
<% if (!companySelected) { %>
											<option value='' ><s:text name="ach.select.an.ACH.company"/></option>
<% } else { %>
												<ffi:object id="CheckACHSECEntitlement" name="com.ffusion.tasks.ach.CheckACHSECEntitlement" scope="session"/>
<% String validateClassCodes = ""; %>
    <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodes"/>
    <ffi:getProperty name="ACHResource" property="Resource" assignTo="validateClassCodes" />
												<ffi:list collection="ACHClassCodes" items="ACHClass">
												<ffi:setProperty name="CheckACHSECEntitlement" property="SECResourceCode" value="${ACHClass}"/>
												<ffi:setProperty name="CheckACHSECEntitlement" property="ACHCompanyID" value="${ACHCOMPANY.CompanyID}"/>
<%	String secCode = ""; String OperationName = "";%>
													<ffi:getProperty name="ACHClass" assignTo="secCode" />
<%	if (isTemplate) {
    OperationName = EntitlementsDefines.ACH_TEMPLATE_ENTRY + EntitlementsDefines.LIMIT_DEBIT + "," + EntitlementsDefines.ACH_TEMPLATE_ENTRY + EntitlementsDefines.LIMIT_CREDIT;
    if (secCode != null && secCode.length() > 3)
    {
        if (secCode.charAt(3)=='0')  // credit
            OperationName = EntitlementsDefines.ACH_TEMPLATE_ENTRY + EntitlementsDefines.LIMIT_CREDIT;
        else if (secCode.charAt(3)=='1')  // debit
            OperationName = EntitlementsDefines.ACH_TEMPLATE_ENTRY + EntitlementsDefines.LIMIT_DEBIT;
    }
%>
    <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodesTemplate"/>
    <ffi:getProperty name="ACHResource" property="Resource" assignTo="validateClassCodes" />
<% } else {
    OperationName = EntitlementsDefines.ACH_PAYMENT_ENTRY + EntitlementsDefines.LIMIT_DEBIT + "," + EntitlementsDefines.ACH_PAYMENT_ENTRY + EntitlementsDefines.LIMIT_CREDIT;
    if (secCode != null && secCode.length() > 3)
    {
        if (secCode.charAt(3)=='0')  // credit
            OperationName = EntitlementsDefines.ACH_PAYMENT_ENTRY + EntitlementsDefines.LIMIT_CREDIT;
        else if (secCode.charAt(3)=='1')  // debit
            OperationName = EntitlementsDefines.ACH_PAYMENT_ENTRY + EntitlementsDefines.LIMIT_DEBIT;
    }
   }
%>
												<ffi:setProperty name="CheckACHSECEntitlement" property="OperationName" value="<%= OperationName %>"/>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
    <ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodesMultiBatchTemplate"/>
    <ffi:getProperty name="ACHResource" property="Resource" assignTo="validateClassCodes" />
</ffi:cinclude>
												<ffi:process name="CheckACHSECEntitlement" />
												<ffi:cinclude value1="${CheckACHSECEntitlement.IsEntitledTo}" value2="TRUE" operator="equals" >
													<%
														if (secCode != null && secCode.length() > 3)
															secCode = secCode.substring(0,3);
														if (validateClassCodes != null && validateClassCodes.indexOf(secCode) >= 0)
														{
															if (selectedSECCode.length() == 0) { %>
																<ffi:getProperty name="ACHClass" assignTo="selectedSECCode" />
															<% } %>
														<option value="<ffi:getProperty name="ACHClass"/>"
														<ffi:cinclude value1="${ACHClass}" value2="${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}">
															selected
															<ffi:getProperty name="ACHClass" assignTo="selectedSECCode" />
														</ffi:cinclude>>
															<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${ACHClass}"/>
															<ffi:getProperty name="ACHResource" property="Resource"/>
														</option>
													<% } %>
												</ffi:cinclude>
												</ffi:list>
													<% if (selectedSECCode.length() == 0) {
														addEntryDisabled = true;%>
														<option value='' ><s:text name="jsp.ach.select.batch.type" /></option>
													<% } else {     // if the selectedSECCode is different than StandardEntryClassCode+DebitBatch, set it as the default
                                                    %>
                                                        <ffi:cinclude value1="<%=selectedSECCode%>" value2="${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}" operator="notEquals">
                                                              <ffi:setProperty name="AddEditACHBatch" property="StandardEntryClassCode" value="<%=selectedSECCode%>" />
                                                        </ffi:cinclude>
                                                    <% } %>
												<ffi:removeProperty name="CheckACHSECEntitlement" />
        <% if (isTemplate) { %>
                <ffi:setProperty name="ACHCOMPANY" property="CurrentClassCodeTemplate" value="${AddEditACHBatch.StandardEntryClassCode}" />
        <% } else { %>
                <ffi:setProperty name="ACHCOMPANY" property="CurrentClassCode" value="${AddEditACHBatch.StandardEntryClassCode}" />
        <% }
   } %>
                                            </select>
<% } else { %>
										<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${AddEditACHBatch.StandardEntryClassCode}${AddEditACHBatch.DebitBatch}"/>
										<ffi:getProperty name="ACHResource" property="Resource"/>
										<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="selectedSECCode" />
<% }
										// if can have addenda, need to know if they are only entitled to the + Addenda code
											if (selectedSECCode != null && selectedSECCode.length() > 3)
												selectedSECCode = selectedSECCode.substring(0,3);
											if ("ARC".equals(selectedSECCode) || "POP".equals(selectedSECCode) || 
												"RCK".equals(selectedSECCode) || "CIE".equals(selectedSECCode) || "BOC".equals(selectedSECCode))
													canRecurring = false;
											if ("CCD".equals(selectedSECCode) || "CTX".equals(selectedSECCode) || "IAT".equals(selectedSECCode) ||
												"CIE".equals(selectedSECCode) || "PPD".equals(selectedSECCode) || "WEB".equals(selectedSECCode)) {
													String secEnt = selectedSECCode + " + Addenda";
										%>

										<ffi:cinclude ifNotEntitled="<%= selectedSECCode %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCOMPANY.CompanyID}" checkParent="true">
											<ffi:cinclude ifEntitled="<%= secEnt %>" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCOMPANY.CompanyID}" checkParent="true">
													<% selectedSECCode = secEnt; %>
											</ffi:cinclude>
										</ffi:cinclude>
										<% } %>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
										<%= secCodeDisplay %>
</ffi:cinclude>
										</td>
    <ffi:getProperty name="AddEditACHBatch" property="FrequencyValue" assignTo="frequencyValue" />
<%
	if (isEdit && "0".equals(frequencyValue))
		canRecurring = false;
%>
									</tr>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
    <s:action namespace="/pages/jsp/ach" name="addTaxChildACHEntryAction_init_ChildSupportTaxForms" />
	<ffi:cinclude value1="${AddEditACHBatch.DefaultTaxForm}" value2="" >
		<% addEntryDisabled = true; %>
	</ffi:cinclude>
	<ffi:cinclude value1="${AddEditACHBatch.BatchImported}" value2="true" >
		<% importedBatch = true; %>
	</ffi:cinclude>
									<tr class="lightBackground">
										<td align="right"><span class="sectionsubhead"><s:text name="jsp.default_481" /></span><span class="required">*</span></td>
										<td class="columndata" colspan="5">
                                        <% boolean zeroSize = false; %>
										<ffi:setProperty name="TaxForms" property="filter" value="TYPE==CHILDSUPPORT"/>
	                                    <ffi:cinclude value1="${TaxForms.size}" value2="0" operator="equals" >
                                            <% zeroSize = true; %>
                                        </ffi:cinclude>
										<select class="txtbox" id="defaultTaxFormID" name="defaultTaxFormID" <%= !companySelected || zeroSize || (anyEntries && !importedBatch) ? "disabled" : ""%> onchange="changeChildspType();">
											<option value='-1' ><s:text name="ach.child.support.type" /></option>
											<% boolean anySelected = false; %>
											<ffi:list collection="TaxForms" items="TaxForm">
												<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CHILD_SUPPORT %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.STATE %>" objectId="${TaxForm.State}">
													<option value="<ffi:getProperty name='TaxForm' property='ID'/>"
														<ffi:cinclude value1="${TaxForm.ID}" value2="${AddEditACHBatch.DefaultTaxForm.ID}">selected <% anySelected = true; %></ffi:cinclude>>
														<ffi:cinclude value1="${TaxForm.type}" value2="CHILDSUPPORT"><ffi:getProperty name="TaxForm" property="state" /> State</ffi:cinclude>
														<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /></span></b> - <span class="sectionsubhead"><ffi:getProperty name="TaxForm" property="TaxFormDescription" /></span>
                                                        <ffi:cinclude value1="${TaxForm.BusinessID}" value2="" operator="notEquals">
                                                                         (<s:text name="jsp.reports_526" />)
                                                        </ffi:cinclude>
													</option>
												</ffi:cinclude>
											</ffi:list>
											<ffi:setProperty name="TaxForms" property="filter" value="All"/>
											<% if (!anySelected && isEdit) {
												String taxformid=""; %>
												<ffi:getProperty name="AddEditACHBatch" property="DefaultTaxForm.ID" assignTo="taxformid" />
												<% if(taxformid != null && !"".equals(taxformid.trim())) { %>
												<ffi:object name="com.ffusion.tasks.ach.GetTaxForm" id="GetTaxForm" scope="session"/>
												<ffi:setProperty name="GetTaxForm" property="ID" value="<%=taxformid%>" />
												<ffi:process name="GetTaxForm"/>
												<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CHILD_SUPPORT %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.STATE %>" objectId="${GetTaxForm.State}">
													<option value="<ffi:getProperty name='GetTaxForm' property='ID'/>"
														<ffi:cinclude value1="${GetTaxForm.ID}" value2="${AddEditACHBatch.DefaultTaxForm.ID}">selected</ffi:cinclude>>
														<ffi:cinclude value1="${GetTaxForm.type}" value2="CHILDSUPPORT"><ffi:getProperty name="GetTaxForm" property="state" /> <s:text name="jsp.default_386" /></ffi:cinclude>
														<ffi:getProperty name="GetTaxForm" property="IRSTaxFormNumber" /> - <span class="sectionsubhead"><ffi:getProperty name="GetTaxForm" property="TaxFormDescription" /></span>
													</option>
												</ffi:cinclude>
												<ffi:removeProperty name="GetTaxForm" />
												<% } %>
											<% } %>
										</select>
										<div id="defaultTaxFormIDError" style="color:red;"></div>
										</td>
									</tr>
</ffi:cinclude>
<%-- They don't want Default Tax Form, so when the first tax form is added, set it as the default tax form.
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
									<tr class="lightBackground">
										<td width="206" class="sectionsubhead" colspan="2" align="right">Default Tax Form</td>
										<td class="columndata" colspan="7">
										<select class="txtbox" name="defaultTaxFormID" <%= !companySelected || anyEntries ? "disabled" : ""%> onchange="document.AddBatchForm.action='<ffi:getProperty name="SecurePath"/>payments/achbatchaddedit.jsp'; document.AddBatchForm.submit();">
											<option value='' >Please select a Default Tax Form Type</option>
											<ffi:setProperty name="TaxForms" property="filter" value="TYPE==FEDERAL,TYPE==STATE,TYPE==OTHER"/>
											<ffi:list collection="TaxForms" items="TaxForm">
												<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TAX_PAYMENTS %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.STATE %>" objectId="${TaxForm.State}">
													<option value="<ffi:getProperty name='TaxForm' property='ID'/>"
														<ffi:cinclude value1="${TaxForm.ID}" value2="${AddEditACHBatch.DefaultTaxForm.ID}">selected</ffi:cinclude>>
														<ffi:cinclude value1="${TaxForm.type}" value2="STATE"><ffi:getProperty name="TaxForm" property="state" /> <!--L10NStart-->State<!--L10NEnd--></ffi:cinclude>
														<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /></span></b> - <span class="sectionsubhead"><ffi:getProperty name="TaxForm" property="TaxFormDescription" /></span>
													</option>
												</ffi:cinclude>
											</ffi:list>
											<ffi:setProperty name="TaxForms" property="filter" value="All"/>
										</select></td>
									</tr>
</ffi:cinclude>
--%>
<%-- Create a new table with two columns, the first column will be the left side,
the second column will be the right side (repeating and Offset Account) --%>
                                    <tr class="lightBackground">
                                        <td colspan="7" style="padding-left:55px" >
                                        <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                        <td >
                                        <table border="0" cellspacing="0" cellpadding="0">

									<tr class="lightBackground">
										<td id="achBatchNameLabel" width="200" align="right">


								<% if (!isTemplate) { %>
											<span class="sectionsubhead"><s:text name="jsp.default_66" /> </span><span class="required">*</span>
											</td>
										<td class="columndata" align="left"><input id="achBatchName" <%=fromTemplate?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="24" style="width:150px;" maxlength="32" border="0" name="name" value="<ffi:getProperty name="AddEditACHBatch" property="Name"/>">
											<div id="batchNameError"></div>
										</td>
								<% } else { %>
											<span class="sectionsubhead"><s:text name="jsp.default_416" /> </span><span class="required">*</span>
											<input type="hidden" name="template" value="true">
											</td>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
										<td class="columndata" align="left">
											<input id="AchBatchTemplateName" type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="32" style="width:150px;" border="0" name="templateName" value="<ffi:getProperty name="AddEditACHBatch" property="TemplateName"/>">
											<div id="templateNameError"></div>
										</td>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
										<td class="columndata" align="left">
											<input id="AchTemplateName" type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="32" style="width:150px;" border="0" name="templateName" value="<ffi:getProperty name="AddEditACHBatch" property="TemplateName"/>">
											<div id="templateNameError"></div>
										</td>
</ffi:cinclude>
								<% } %>
									</tr>
<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="equals" >
<% if (selectedSECCode != null && selectedSECCode.startsWith("CCD")) {%>
                            <ffi:cinclude ifEntitled="CCD + Healthcare" objectType="<%= EntitlementsDefines.ACHCOMPANY %>" objectId="${ACHCOMPANY.CompanyID}" checkParent="true">
									<tr class="lightBackground">
                                        <td align="right"><span class="sectionsubhead"><s:text name="ach.payment.type" /></span><span class="required">*</span></td>
                                        <td class="columndata" align="left">
                                        <select class="txtbox" id="paymentTypeID" name="paymentTypeID" <%= !companySelected || (anyEntries && !importedBatch) ? "disabled" : ""%> onchange="changePaymentType('true');">
                                            <option value='' ><s:text name="ach.Regular" /></option>
                                            <option value='Healthcare' <ffi:cinclude value1="HCCLAIMPMT" value2="${AddEditACHBatch.CoEntryDesc}">selected</ffi:cinclude>><s:text name="jsp.ach.Healthcare"/></s:text></option>
                                        </select>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                            </ffi:cinclude>

                                    <% } %>
</ffi:cinclude>
									<tr class="lightBackground">
										<td id="achBatchCompanyDescriptionLabel" align="right"><span class="sectionsubhead"><s:text name="ach.company.description" /> </span><span class="required">*</span></td>
										<td class="columndata" align="left">
<%	if (selectedSECCode == null)
		selectedSECCode = "";
	if (selectedSECCode.startsWith("XCK"))
		coEntryDesc = "NO CHECK";
	if (selectedSECCode.startsWith("RCK"))
		coEntryDesc = "REDEPCHECK";
	if (coEntryDesc.length() > 0)
	{ %>
		<ffi:setProperty name="coEntrySecureDesc" value="<%= coEntryDesc %>"/>
		<input type="Hidden" name="AddEditACHBatch.CoEntryDesc" value="<ffi:getProperty name="coEntrySecureDesc"/>">
		<input disabled type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="10" border="0" name="coEntryDesc" value="<ffi:getProperty name="coEntrySecureDesc"/>">
<% } else { %>
		<ffi:getProperty name="AddEditACHBatch" property="CoEntryDesc" assignTo="coEntryDesc" />
		<% boolean cannotChange = false;
			if ("NO CHECK".equals(coEntryDesc) || "REDEPCHECK".equals(coEntryDesc) || coEntryDesc == null)
				coEntryDesc = "";			// not XCK or RCK, so clear it out
            if ("HCCLAIMPMT".equals(coEntryDesc))
                cannotChange = true;
		%>
        <ffi:cinclude value1="${AddEditACHBatch.CanChangeCompanyDescription}" value2="false" operator="equals">
            <% cannotChange = true; %>
        </ffi:cinclude>
		<ffi:setProperty name="coEntrySecureDesc" value="<%= coEntryDesc %>"/>
        <input id="coEntryDescHidden" type="Hidden" <%=!(fromTemplate || cannotChange)?"disabled":""%> name="AddEditACHBatch.CoEntryDesc" value="<ffi:getProperty name="coEntrySecureDesc"/>">
		<input id="achBatchCompanyDescription" <%=fromTemplate || cannotChange?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="10" border="0" style="width:150px;" name="coEntryDesc" value="<ffi:getProperty name="coEntrySecureDesc"/>">
<% } %>

										<div id="coEntryDescError"></div>
										</td>
									</tr>
									<tr class="lightBackground">
<%-- Discretionary Data is fine for WEB, just at the ENTRY level, there is no Discretionary Data --%>
<% if (selectedSECCode.startsWith("IAT"))
{
    // IAT doesn't have Discretionary Data in the batch header
%>
										<td align="right" width="200">
											<span class="sectionsubhead"><s:text name="ach.originating.currency" />&nbsp;&nbsp;&nbsp;</span>
										</td>
										<td class="columndata">
											<input type="Hidden" class="ui-widget-content ui-corner-all" size="20" maxlength="20" border="0" name="AddEditACHBatch.CoDiscretionaryData" value="">
											<input type="Hidden" class="ui-widget-content ui-corner-all" size="20" maxlength="20" border="0" name="AddEditACHBatch.ISO_OriginatingCurrencyCode" value="USD">
                                            <s:text name="ach.USD.United.States.Dollar" />
										</td>
<% } else {
    boolean cannotChange = false;
%>

										<td id="achBatchDescretionaryDataLabel" align="right" width="200">
											<span class="sectionsubhead"><s:text name="ach.Discretionary.Data" /></span>
										</td>
										<td class="columndata" align="left">
                                            <ffi:cinclude value1="${AddEditACHBatch.CanChangeDiscretionaryData}" value2="false" operator="equals">
                                                <% cannotChange = true; %>
                                            </ffi:cinclude>
											<input id="achBatchDescretionaryData" <%=cannotChange?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="20" maxlength="20" style="width:150px;" border="0" name="coDiscretionaryData" value="<ffi:getProperty name="AddEditACHBatch" property="CoDiscretionaryData"/>">
                                            <div id="coDiscretionaryDataError"></div>
										</td>
<% } %>

									</tr>
<% headerTitle = "ACHHeaderCompanyName"+ (selectedSECCode.length() <= 3?selectedSECCode:selectedSECCode.substring(0,3));
   headerValue = "ACHHeaderCompanyNameValue"+ (selectedSECCode.length() <= 3?selectedSECCode:selectedSECCode.substring(0,3)); %>
		<ffi:setProperty name="ACHResource" property="ResourceID" value="<%=headerTitle%>"/>
		<ffi:getProperty name="ACHResource" property="Resource" assignTo="headerTitle" />
<% if (headerTitle != null && headerTitle.length() > 0) { %>
		<ffi:setProperty name="ACHResource" property="ResourceID" value="<%=headerValue%>"/>
		<ffi:getProperty name="ACHResource" property="Resource" assignTo="headerValue" />
		<input type="Hidden" name="HeaderTitle" value="<%= headerTitle %>">
									<tr class="lightBackground">
										<td id="achBatchPayeeLabel" align="right">
								<span class="sectionsubhead"><%= headerTitle %> </span><span class="required">*</span>
										</td>
										<td class="columndata">
<%	if (headerValue != null && headerValue.length() > 0) { %>
											<ffi:setProperty name="coHeaderCompName" value="<%= headerValue %>"/>
											<input type="Hidden" name="AddEditACHBatch.HeaderCompName" value="<ffi:getProperty name="coHeaderCompName"/>">
											<input disabled type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="16" border="0" name="HeaderCompName" value="<ffi:getProperty name="coHeaderCompName"/>">
<% } else { %>
		<ffi:getProperty name="AddEditACHBatch" property="HeaderCompName" assignTo="headerValue" />
		<%
			if ("CHECK DESTROYED".equals(headerValue) || headerValue == null)
				headerValue = "";			// not XCK
		%>
		<ffi:setProperty name="coHeaderCompName" value="<%= headerValue %>"/>
		<input id="achBatchPayee" <%=fromTemplate?"disabled":""%> type="text" class="ui-widget-content ui-corner-all txtbox" size="24" maxlength="16" border="0" style="width:150px;" name="AddEditACHBatch.HeaderCompName" value="<ffi:getProperty name="coHeaderCompName"/>">
		<div id="headerTitleError"></div>
<% } %>
										</td>

										<td class="sectionsubhead" colspan='3'></td>
									</tr>
<% } else { %>
								<input type="Hidden" name="AddEditACHBatch.HeaderCompName" value="">
<% } %>
								<ffi:removeProperty name="coHeaderCompName"/>

<%-- IAT specific items --%>
<% if (selectedSECCode.startsWith("IAT"))
 { %>
									<tr class="lightBackground">
										<td align="right">
								<span class="sectionsubhead"><s:text name="da.field.ach.destinationCountry" /> </span><span class="required">*</span>
										</td>
										<td class="columndata">
                                            <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                            <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                            <ffi:process name="ACHResource" />
                                            <% String crdr=""; %>
                                            <ffi:cinclude value1="${AddEditACHBatch.DebitBatch}" value2="1" operator="equals">
                                                <%-- this is a debit batch, check if country supports debits --%>
                                                <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryDebitList"/>
                                                <ffi:getProperty name="ACHResource" property="Resource" assignTo="crdr" />
                                            </ffi:cinclude>
                                            <ffi:cinclude value1="${AddEditACHBatch.DebitBatch}" value2="1" operator="notEquals">
                                                <%-- this is a credit batch, check if country supports credits --%>
                                                <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCreditList"/>
                                                <ffi:getProperty name="ACHResource" property="Resource" assignTo="crdr" />
                                            </ffi:cinclude>
                                            <select id="countryID" <%= anyEntries ? "disabled" : ""%> class="txtbox" name="iSO_DestinationCountryCode" onchange="updateCurrencyInfo(this);">
                                                <option title="" value="">(<s:text name="ach.countrySelect.defaultOption" />)</option>
                                                <ffi:list collection="ACHIAT_DestinationCountryCodes" items="countryCode">
                                                <% boolean useCountry = true; String countryCode1 = ""; %>
                                                <ffi:getProperty name="countryCode" assignTo="countryCode1"/>
                                                <% if (crdr != null && crdr.indexOf(countryCode1) == -1) useCountry = false;
                                                   if (useCountry) {
                                                String countryName =""; %>
                                                        <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${countryCode}"/>
                                                        <ffi:getProperty name="ACHResource" property="Resource" assignTo="countryName" />
                        <%
                            if (countryName == null) countryName = "";
                        %>
                                                    <option title="<%=countryName%>" value="<ffi:getProperty name="countryCode"/>"
                                                    <ffi:cinclude value1="${countryCode}" value2="${AddEditACHBatch.ISO_DestinationCountryCode}">
                                                        selected
                                                    </ffi:cinclude>>
                        <%
                            if (countryName.length() > 25)
                            {
                                countryName = countryName.substring(0, 25);
                            }
                        %>
                                                    <%=countryName%>
                                                    </option>
                                                <% } %>
                                                </ffi:list>
                                            </select>
                                            <div id="truncatedText"></div>
                                            <div id="destinationCountryError"></div>
										</td>

										<td class="sectionsubhead" colspan='3'></td>

    <ffi:cinclude value1="${AddEditACHBatch.ISO_DestinationCountryCode}" value2="" >
        <%
        addEntryDisabled = true;
        %>
    </ffi:cinclude>
									</tr>
									<tr class="lightBackground">
										<td align="right">
								<span class="sectionsubhead"><s:text name="ach.Destination.Currency"/> </span><span class="required">*</span>
										</td>
                                            <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                                            <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                                            <ffi:process name="ACHResource" />
										<td class="columndata">
                                            <select id="CURRENCY" <%= anyEntries ? "disabled" : ""%> class="txtbox" name="iSO_DestinationCurrencyCode" onchange="updateForeignExchangeMethod(this);">
                                                <option value="">(<s:text name="ach.currencySelect.defaultOption" />)</option>
                                                <ffi:list collection="ACHIAT_DestinationCurrencyCodes" items="CurrencyCode">
                                                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${CurrencyCode}"/>
                                                    <option value="<ffi:getProperty name="CurrencyCode" />" <ffi:cinclude value1="${CurrencyCode}" value2="${AddEditACHBatch.ISO_DestinationCurrencyCode}">selected</ffi:cinclude>><ffi:getProperty name="CurrencyCode" />-<ffi:getProperty name="ACHResource" property="Resource"/></option>
                                                </ffi:list>
                                            </select>
                                            <div id="destinationCountryCurrencyError"></div>
										</td>
    <ffi:cinclude value1="${AddEditACHBatch.ISO_DestinationCurrencyCode}" value2="" >
    <%
	addEntryDisabled = true;
    %>
    </ffi:cinclude>

										<td class="sectionsubhead" colspan='3'></td>
									</tr>
									<tr class="lightBackground">
										<td align="right">
								<span class="sectionsubhead"><s:text name="da.field.ach.foreignExchangeMethod" /> </span><span class="required">*</span>
										</td>
										<td class="columndata">
                                            <select id="FOREIGNEXCHANGE" <%= anyEntries ? "disabled" : ""%> class="txtbox" name="foreignExchangeIndicator">
                                                <ffi:cinclude value1="${AddEditACHBatch.ISO_DestinationCountryCode}" value2="" operator="equals" >
                                                <option value="">(<s:text name="ach.currencySelect.defaultOption" />)</option>
                                                </ffi:cinclude>
                                                <ffi:cinclude value1="${AddEditACHBatch.ISO_DestinationCountryCode}" value2="" operator="notEquals" >
                                                    <ffi:list collection="ACHIAT_ForeignExchangeIndicatorSupported" items="ForeignExchangeType">
                                                        <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${ForeignExchangeType}"/>
                                                        <ffi:cinclude value1="${ForeignExchangeType}" value2="${AddEditACHBatch.ForeignExchangeIndicator}">
                                                            <option value="<ffi:getProperty name="ForeignExchangeType"/>" selected><ffi:getProperty name="ForeignExchangeType"/>-<ffi:getProperty name="ACHResource" property="Resource"/></option>
                                                        </ffi:cinclude>
                                                    </ffi:list>
                                                </ffi:cinclude>
                                            </select>
                                            <div id="foreignExchangeError"></div>

										</td>
    <ffi:cinclude value1="${AddEditACHBatch.ForeignExchangeIndicator}" value2="" >
    <%
	addEntryDisabled = true;
    %>
    </ffi:cinclude>

										<td class="sectionsubhead" colspan='3'></td>
									</tr>
<% }
   boolean isMultipleBatchTemplate = false; %>
<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
    <% isMultipleBatchTemplate = true; %>
</ffi:cinclude>
<% if (!isMultipleBatchTemplate || fromTemplate) { %>
									<tr class="lightBackground">
										<td id="achBatchDateLabel" align="right">
										<% if (!isTemplate) { %>
											<span class="sectionsubhead"><s:text name="ach.grid.effectiveDate" /> </span><span class="required">*</span>
										<% } else {
										// NO Date required for Templates
										%>
											<span class="sectionsubhead"><s:text name="jsp.default_418" /> </span><span class="required">*</span>
										<% } %></td>
										<td class="columndata">
										<% if (!isTemplate) {
												// we only want to get default effective date if no entries yet
												if (!anyEntries || (fromTemplate && secChanged)) { %>
													<ffi:object name="com.ffusion.tasks.ach.GetDefaultEffectiveDate" id="GetDefaultEffectiveDate" scope="session"/>
													<ffi:setProperty name="GetDefaultEffectiveDate" property="DateFormat" value="${AddEditACHBatch.DateFormat}"/>
													<ffi:setProperty name="GetDefaultEffectiveDate" property="CompanyID" value=" ${AddEditACHBatch.CoID}"/>
													<ffi:setProperty name="GetDefaultEffectiveDate" property="StandardEntryClassCode" value="<%=selectedSECCode%>"/>
													<ffi:setProperty name="GetDefaultEffectiveDate" property="BatchName" value="AddEditACHBatch"/>
													<ffi:process name="GetDefaultEffectiveDate"/>
													<ffi:cinclude value1="${GetDefaultEffectiveDate.BatchDateBeforeEffectiveDate}" value2="true" >
														<% secChanged = true; %>
													</ffi:cinclude>
													<% if (secChanged == true) { %>
														<ffi:setProperty name="AddEditACHBatch" property="Date" value="${GetDefaultEffectiveDate.DefaultEffectiveDate}" />
													<% }
												} %>
											<sj:datepicker value="%{#session.AddEditACHBatch.Date}"
												id="AddEditACHBatchDateId" 
												name="date"
												label="Date"
												maxlength="10" 
												buttonImageOnly="true"
												cssClass="ui-widget-content ui-corner-all" />
											<div id="dateError"></div>
											<script>
												var secCode = "CCD";        // the default for Tax and Child Support
                                                var debitBatch = '<ffi:getProperty name="AddEditACHBatch" property="DebitBatch"/>';

                                                <ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" >
                                                    secCode = "TXP";        // the default for Tax Payment
                                                </ffi:cinclude>
                                                <ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ChildSupportPayment" >
                                                    secCode = "DED";        // the default for Child Support
                                                </ffi:cinclude>
												<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
													<% if (isEdit) { %>
														secCode = '<ffi:getProperty name="ACHCOMPANY" property="CurrentClassCode"/>' + debitBatch;
													<% } else { %>
													if(document.AddBatchForm["AddEditACHBatch.StandardEntryClassCode"] != null){
														secCode = document.AddBatchForm["AddEditACHBatch.StandardEntryClassCode"].options[document.AddBatchForm["AddEditACHBatch.StandardEntryClassCode"].selectedIndex].value;
													}
													else
													{
													secCode='<ffi:getProperty name="ACHCOMPANY" property="CurrentClassCode"/>' + debitBatch;
													}

													<% } %>
												</ffi:cinclude>

												var urlStr = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=9&calDisplay=select&calForm=AddBatchForm&calTarget=AddEditACHBatch.Date&fromACHorTax=true&ACHCompanyID=${ACHCOMPANY.ID}"/>';	
												ns.common.enableAjaxDatepicker("AddEditACHBatchDateId", urlStr + '&SECCode=' + secCode);
											</script>

										<% } else { %>
											<select id="AddEditACHBatchBatchScopeID" class="txtbox" name="batchScope">
												<option value="BUSINESS" <ffi:cinclude value1="BUSINESS" value2="${AddEditACHBatch.BatchScope}" operator="equals" >selected</ffi:cinclude>><s:text name="jsp.default_77" /></option>

												<%-- Start: Dual approval processing --%>
												<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
													<option value="USER" <ffi:cinclude value1="USER" value2="${AddEditACHBatch.BatchScope}" operator="equals" >selected</ffi:cinclude>><s:text name="jsp.default_454" /></option>
												</ffi:cinclude>
												<%-- End: Dual approval processing --%>
											</select>
										<% } %>
										<div id="effectiveDateError"></div>
										</td>

									</tr>


                                </td>
                                </tr>
<% } %>
                                </table>
                                </td>
                                <td >
                                <table border="0" cellspacing="0" cellpadding="3">

		<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="TaxPayment" operator="notEquals">

		<ffi:cinclude value1="${AddEditACHBatch.TransactionType}" value2="ACH" >

		<ffi:cinclude value1="${AddEditACHBatch.RecID}" value2="" operator="notEquals">

		<ffi:cinclude value1="${AddEditACHBatch.ID}" value2="${AddEditACHBatch.RecID}" operator="notEquals">

		<% isScheduledACHBatch= true; %>
		</ffi:cinclude>
		</ffi:cinclude>
		</ffi:cinclude>
		</ffi:cinclude>
									<tr class="lightBackground">
<% if (canRecurring) { %>
										<td class="sectionsubhead tbrd_r" rowspan="3" width="10">&nbsp;</td>
										<td class="sectionsubhead" rowspan="3" width="10">&nbsp;</td>
                                        <td id="achTemplateRepeatingLabel" class="sectionsubhead" align="right">
                                            <s:text name="jsp.default_351"/>
                                        </td>
                                        <td class="columndata" colspan='2'>
                                            <select <%=fromTemplate || isScheduledACHBatch?"disabled":""%> id="FrequencyID" name="frequency" class="txtbox" onchange="modifyFrequency();">
<% if (!isEdit) { %>
                                                <option value="None"><s:text name="jsp.ach.None.Selected"/></option>
<% } %>
                                                <ffi:list collection="ACHFrequencies" items="Freq1">
                                                    <option value="<ffi:getProperty name="Freq1"/>" <ffi:cinclude value1="${Freq1}" value2="${Frequency}">selected</ffi:cinclude>><ffi:getProperty name="Freq1"/></option>
                                                </ffi:list>
                                            </select>
                                        </td>
<% } %>
									</tr>
<% if(!isScheduledACHBatch) {%>
									<tr class="lightBackground">
<% if (canRecurring) { %>

                                        <td class="sectionsubhead" align="right"><input <%=fromTemplate ?"disabled":""%> onclick="toggleRecurring();" type="radio" name="OpenEnded" value="true" border="0" <ffi:cinclude value1="${OpenEnded}" value2="true">checked</ffi:cinclude>></td>
                                        <td class="sectionsubhead" colspan='2' align="left"><s:text name="jsp.default_446"/></td>
<% } %>
									</tr>
									<tr class="lightBackground">
<% if (canRecurring) { %>

                                        <td class="sectionsubhead" align="right"><input <%=fromTemplate ?"disabled":""%> onclick="toggleRecurring();" type="radio" name="OpenEnded" value="false" border="0" <ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals" >checked</ffi:cinclude>></td>
                                        <td class="sectionsubhead" align="left"><s:text name="jsp.default_2" /><input <%=fromTemplate ?"disabled":""%> type="text" name="numberPayments" value="<ffi:getProperty name="AddEditACHBatch" property="NumberPayments"/>" size="2" class="ui-widget-content ui-corner-all txtbox" maxlength="3"><span id="NumberPaymentsError"></span></td>
<% } %>
									</tr>
<% } %>

<% if(isScheduledACHBatch) {%>
									<tr class="lightBackground">
<% if (canRecurring) { %>

                                        <td class="sectionsubhead" align="right"><input disabled type="radio" name="StaticOpenEnded" value="true" border="0" <ffi:cinclude value1="${OpenEnded}" value2="true">checked</ffi:cinclude>></td>
                                        <td class="sectionsubhead" colspan='2'><s:text name="jsp.default_446"/></td>
<% } %>
									</tr>
									<tr class="lightBackground">
<% if (canRecurring) { %>

                                        <td class="sectionsubhead" align="right"><input disabled  type="radio" name="StaticOpenEnded" value="false" border="0" <ffi:cinclude value1="${OpenEnded}" value2="true" operator="notEquals" >checked</ffi:cinclude>></td>
                                        <td class="sectionsubhead"><s:text name="jsp.default_2" /> <ffi:getProperty name="AddEditACHBatch" property="NumberPayments"/></td>
<% } %>
									</tr>
<% } %>



									<tr class="lightBackground">
                                        <ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="equals">
<% if (selectedSECCode.startsWith("IAT"))
 {
// QTS 577543: If IAT, we don't support Batch Balanced or Entry Balanced batches
%>
									<tr class="lightBackground">
                                        <td class="sectionsubhead" align="right" colspan='4'>
                                        <s:text name="ach.IAT.noOffsetAcct" />
                                        </td>
									</tr>
 <% } else { %>
    										<td class="sectionsubhead tbrd_r" width="10">&nbsp;</td>
    										<td class="sectionsubhead" width="10">&nbsp;</td>
                                            <td align="right" nowrap><span class="sectionsubhead"><s:text name="jsp.default_494" /> </span><span class="required">*</span></td>
                                            <td class="columndata">
                                                <select id="AddEditACHBatchOffsetAccountID" class="txtbox" name="AddEditACHBatch.OffsetAccountID">
                                                    <ffi:cinclude value1="${ACHOffsetAccounts.Size}" value2="1" operator="notEquals" >
                                                    <option value="">(<s:text name="jsp.ach.select.offset.account" />)</option>
                                                    </ffi:cinclude>
                                                    <ffi:setProperty name="ACHOffsetAccounts" property="Filter" value="All" />
                                                    <ffi:list collection="ACHOffsetAccounts" items="OffsetAccount">

            <ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetAccountEntitlementObjectID" scope="request"/>
                <ffi:setProperty name="GetAccountEntitlementObjectID" property="ObjectType" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>'/>
                <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>'/>
                <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${OffsetAccount.Account.ID}"/>
                <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyName" value='<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>'/>
                <ffi:setProperty name="GetAccountEntitlementObjectID" property="CurrentPropertyValue" value="${OffsetAccount.RoutingNum}"/>
                <ffi:process name="GetAccountEntitlementObjectID"/>

            <% boolean isEntitled = false; String EntitlementToCheck = "";
                if (subMenuSelected.equals("tax")) {
                    EntitlementToCheck = EntitlementsDefines.TAX_PAYMENTS;
                } else if (subMenuSelected.equals("child")) {
                    EntitlementToCheck = EntitlementsDefines.CHILD_SUPPORT;
                } else {
                    EntitlementToCheck = EntitlementsDefines.ACH_BATCH;
                }
            %>
            <ffi:cinclude ifEntitled="<%= EntitlementToCheck %>" objectType="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACCOUNT %>" objectId="${GetAccountEntitlementObjectID.ObjectID}">
                <% isEntitled = true; %>
            </ffi:cinclude>
                        <% if (isEntitled) { %>


                                                        <option value="<ffi:getProperty name="OffsetAccount" property="ID"/>" <ffi:cinclude value1="${OffsetAccount.ID}" value2="${AddEditACHBatch.OffsetAccountID}">selected</ffi:cinclude>><ffi:getProperty name="OffsetAccount" property="Number"/>-<ffi:getProperty name="OffsetAccount" property="Type"/></option>
                        <% } %>
                                                    </ffi:list>
                                                </select>
                                                <div id="offsetAccountError"></div>

                                            </td>
<% } %>
                                        </ffi:cinclude>
                                        <ffi:cinclude value1="${AddEditACHBatch.BatchType}" value2="Batch Balanced" operator="notEquals">
                                            <td class="sectionsubhead" align="right">&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </ffi:cinclude>
									</tr>

                                </table>
                                </td>
                                </tr>
                                </table>
                                </td>
                                </tr>


<%-- end of table --%>
	<ffi:cinclude operator="equals" value1="${AddEditACHBatch.hasValidationErrors}" value2="true">

		<% String lineContent; String lineNumber; String recordNumber; %>
		<ffi:list collection="AddEditACHBatch.validationErrors" items="validationError">
			<ffi:getProperty name="AddEditACHBatch" property="lineContent" assignTo="lineContent"/>
			<ffi:getProperty name="AddEditACHBatch" property="lineNumber" assignTo="lineNumber"/>
			<ffi:getProperty name="AddEditACHBatch" property="recordNumber" assignTo="recordNumber"/>
		<tr>
			<td colspan="10" class="columndata_error columndata_bold"><ffi:getProperty name="validationError" property="title"/></td>
		</tr>
		<tr>
			<td colspan="10" class="columndata_error">
			<% if (lineNumber != null && recordNumber != null) { %>
      	<span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-1" parm0="${AddEditACHBatch.lineNumber}" parm1="${AddEditACHBatch.recordNumber}"/>:</span>
			<% }
			   if (lineNumber != null && recordNumber == null) { %>
        <span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-2" parm0="${AddEditACHBatch.lineNumber}"/>:</span>
			<% }
			   if (lineNumber == null && recordNumber != null) { %>
        <span class="columndata_bold"><ffi:getL10NString rsrcFile="cb" msgKey="jsp/payments/achbatchaddeditentries.jsp-3" parm0="${AddEditACHBatch.recordNumber}"/>:</span>
			<% } %>
        <ffi:getProperty name="validationError" property="message"/>
			</td>
		</tr>
			<% if (lineContent != null) { %>
			<tr>
				<td></td>
				<td colspan="9" class="columndata_error">
					<span class="columndata_bold"><s:text name="jsp.default_209" />:</span>
					<%= lineContent %>
				</td>
			</tr>
			<tr>
				<td><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="5" border="0"></td>
			</tr>
			<% } %>
		</ffi:list>
	</ffi:cinclude>

								<tr>
                                	<td colspan="10"><span id="formError"></span></td>
                                </tr>
								<tr>
									<td colspan="5"><div align="center"><span class="required">* <s:text name="jsp.default_240" /></span></div></td>
								</tr>
								<ffi:cinclude operator="equals" value1="true" value2="${AddEditACHBatch.EntryValidationErrors}">
									<tr>
										<td class="lightBackground" colspan="7" height="20"><img src="/cb/web/multilang/grafx/payments/spacer.gif" alt="" width="1" height="1" border="0"></td>
									</tr>
 									<tr>
										<td class="lightBackground mainfontbold errortext" colspan="7">(<s:text name="jsp.ach.errors.in.ACH.entries"/>)</td>
									</tr>
 									<tr>
										<td class="lightBackground mainfont" colspan="7"><s:text name="jsp.ach.error.messages.processing" /></td>
									</tr>
								</ffi:cinclude>

									<tr>
										
										<td class="sectionsubhead" colspan="7" align="left" nowrap>
										<hr>

<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
	<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" operator="notEquals" >
		<script>ns.ach.setACHBatchTypeUrl = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achbatchaddedit.jsp?SetACHBatchType=true"/>';</script>
		<input id="setACHBatchTypeId" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%=addEntryDisabled ?"ui-state-disabled":"" %>' type="button" <%= addEntryDisabled ?"disabled":"" %> value="SET ACHBATCH TYPE" onClick="setACHBatchType(ns.ach.setACHBatchTypeUrl);">
		&nbsp;
    </ffi:cinclude>
</ffi:cinclude>
		<input id="setAllAmountsButtonId" class='formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= addEntryDisabled || !anyEntries?"ui-state-disabled":"" %>' type="button" <%= addEntryDisabled || !anyEntries?"disabled":"" %> value="Set All Amounts" onClick="setAllAmounts();">
		&nbsp;
		<ffi:cinclude value1="${AddEditACHBatch.ACHType}" value2="ACHBatch" >
			<input id="importAmounts" class="formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= addEntryDisabled || !anyEntries?"ui-state-disabled":"" %>" type="button" <%= addEntryDisabled || !anyEntries?"disabled":"" %> name="Submit3" value="Import Amounts" onclick="importACHEntries(false);">&nbsp;

<% if (!fromTemplate) { %>

			<input id="IMPORTENTRIES" class='formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= addEntryDisabled?"ui-state-disabled":"" %>' type="button" <%= addEntryDisabled?"disabled":"" %> name="Submit3" value="Import Entries" onclick="importACHEntries(true);">&nbsp;
<% } %>
											</ffi:cinclude>
<% if (!fromTemplate) { %>

					<input id="addEntryButtonId" class='formSubmitBtn ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only <%= addEntryDisabled?"ui-state-disabled":"" %>' type="button" <%= addEntryDisabled?"disabled":"" %> name="addEntry" value="Add Entry" onclick="addACHEntry();">
<% } %>
										</td>
									</tr>


</table>


<%-- this is already done, so don't need to do again
<ffi:setProperty name="CallerURL" value="${SecurePath}payments/achbatchaddedit.jsp"/>
--%>



<ffi:setProperty name="ACHEntries" property="FilterPaging" value="none"/>


 										</div>
									</div>
								<br>
							</td>
						</tr>
</table>
<div id="achEntryHeaderDiv"  style="padding-left: 20px;padding-right: 25px;" class="sectionsubhead">
	<s:include value="%{#session.PagesPath}/ach/achbatchaddeditentryheader.jsp" />
</div>
<div id="achEntryGridDiv"  style="padding-left: 20px;padding-right: 25px; padding-top: 10px; clear:both;" class="sectionsubhead">
	<s:include value="%{#session.PagesPath}/ach/achbatchaddeditentrygrid.jsp" />
</div>
<table align="center" width="98%" border="0" cellspacing="0" cellpadding="1">
	<tr>
		<td>
			<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
                <ffi:cinclude value1="${AddEditMultiACHTemplate.Action}" value2="Add">
                <%
                     isEdit = false;
                %>
                </ffi:cinclude>
			</ffi:cinclude>
			                       <% if (isTemplate && isEdit) { %>

			<s:include value="%{#session.PagesPath}/ach/achbatchaddedit_lifecycle.jsp" />

					             <% } %>
		</td>
	</tr>
</table>
           <table width="98%" border="0" cellspacing="0" cellpadding="1">

				<tr>
					<td colspan="15" align="center">

			<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
								<sj:a
					                button="true"
									onClickTopics="cancelACHForm"
					        	><s:text name="jsp.default_82" /><!-- CANCEL -->
					        	</sj:a>
			</ffi:cinclude>
			<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
								<sj:a
	                                button="true"
	                                onClickTopics="backToACHMultiBatchForm"
			                        ><s:text name="jsp.default_82" /><!-- CANCEL -->
	                        	</sj:a>
			</ffi:cinclude>
							&nbsp;
    		<ffi:cinclude operator="notEquals" value1="true" value2="${AddEditACHBatch.EntryValidationErrors}">
					<ffi:cinclude operator="equals" value1="true" value2="<%= String.valueOf( isTemplate ) %>">
							<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="equals" >
								<sj:a
									id="verifyACHSubmit"
									formIds="AddBatchNew"
	                                targets="resultmessage"
	                                button="true"
	                                validate="true"
	                                validateFunction="ns.ach.batchCustomValidation"
	                                onBeforeTopics="beforeVerifyACHForm"
	                                onClickTopics="clickSaveToMultiTempForm"
	                                onSuccessTopics="completeSaveToMultiTempForm"
									onErrorTopics="errorAddACHTempForm"
			                        ><s:text name="jsp.default_366" />
	                        	</sj:a>
							</ffi:cinclude>


							<ffi:cinclude value1="${AddEditACHBatch.IsMultipleBatchTemplate}" value2="true" operator="notEquals" >
									<input type="hidden" name="isTemplateEdit" value='<ffi:getProperty name="isTemplateEdit"/>'>
									<sj:a
										id="verifyACHSubmit"
										formIds="AddBatchNew"
		                                targets="resultmessage"
		                                button="true"
		                                validate="true"
		                                validateFunction="ns.ach.batchCustomValidation"
		                                onBeforeTopics="beforeVerifyACHForm"
		                                onClickTopics="clickAddACHTempForm"
		                                onCompleteTopics="completeAddACHTempForm"
		                                onSuccessTopics="successAddACHTempForm"
		                                onErrorTopics="errorAddACHTempForm"
				                        ><s:text name="jsp.default_366" />
		                        	</sj:a>

							</ffi:cinclude>
					</ffi:cinclude>
					<ffi:cinclude operator="notEquals" value1="true" value2="<%= String.valueOf( isTemplate ) %>">
									<sj:a
										id="verifyACHSubmit"
										formIds="AddBatchNew"
		                                targets="verifyDiv"
		                                button="true"
		                                validate="true"
		                                validateFunction="ns.ach.batchCustomValidation"
		                                onBeforeTopics="beforeVerifyACHForm"
		                                onCompleteTopics="verifyACHFormComplete"
										onErrorTopics="errorVerifyACHForm"
		                                onSuccessTopics="successVerifyACHForm"
		                                onClickTopics="clickVerifyACHForm"
				                        ><s:text name="jsp.default_291" />
		                        	</sj:a>
					</ffi:cinclude>
			</ffi:cinclude>
            <ffi:cinclude operator="equals" value1="true" value2="${AddEditACHBatch.EntryValidationErrors}">
									<sj:a
										id="verifyCheckACHSubmit"
										formIds="AddBatchNew"
		                                targets="verifyDiv"
		                                button="true"
		                                validate="true"
		                                validateFunction="ns.ach.batchCustomValidation"
		                                onCompleteTopics="recheckACHFormComplete"
		                                onClickTopics="clickVerifyACHForm"
				                        ><s:text name="jsp.ach.Recheck.Validation" />
		                        	</sj:a>
		<script>
			ns.ach.showErrorEntriesURL = '<ffi:urlEncrypt url="/cb/pages/jsp/ach/achbatchaddedit.jsp?ShowErrors=true"/>';
 		</script>
									<sj:a
                                        id="showErrorRows"
                                        formIds="AddBatchNew"
                                        targets="inputDiv"
                                        button="true"
                                        onCompleteTopics="showErrorRowsACHFormComplete"
                                        onClickTopics="clickVerifyACHForm"
                                        ><s:text name="jsp.ach.Show.Error.Entries"/>
                                    </sj:a>
                                    <span
                                        id="showingErrorRows"
                                        style="display:none;"
                                        >&nbsp;<s:text name="jsp.ach.Showing.Only.Error.Entries" />
                                    </span>

            </ffi:cinclude>
					</td>
				</tr>
			</table>
		</s:form>
</div>
<script type="text/javascript">
<!--
	modifyFrequency();
    changePaymentType('false');
    $("#paymentTypeID").selectmenu({width: 120});
	$("#AddEditACHBatchCompanyID").selectmenu({width: 250});
	$("#standardEntryClassCodeId").selectmenu({width: 400});
	$("#defaultTaxFormID").selectmenu({width: 250});
	$("#AddEditACHBatchBatchScopeID").selectmenu({width: 200});
	$("#FrequencyID").selectmenu({width: 120});
	$("#AddEditACHBatchOffsetAccountID").selectmenu({width: 160});
	$("#FOREIGNEXCHANGE").selectmenu({width: 160});
	$("#countryID").selectmenu({width: 160});
	$("#CURRENCY").selectmenu({width: 160});
	ns.common.addHoverEffect("setACHBatchTypeId");
	ns.common.addHoverEffect("setAllAmountsButtonId");
	ns.common.addHoverEffect("importAmounts");
	ns.common.addHoverEffect("addEntryButtonId");
	ns.common.addHoverEffect("IMPORTENTRIES");
//-->
</script>

<% if (!isTemplate) { %>
<script type="text/javascript">
<!--
    ns.ach.setInputTabText('#normalInputTab');
    ns.ach.setVerifyTabText('#normalVerifyTab');
    ns.ach.setConfirmTabText('#normalConfirmTab');
    ns.ach.showInputDivSteps();
//-->
</script>
<% } %>
<% if (isTemplate) { %>
<script type="text/javascript">
<!--
    ns.ach.setInputTabText('#templateInputTab');
    ns.ach.setVerifyTabText('');
    ns.ach.setConfirmTabText('');
    ns.ach.showInputDivSteps();
//-->
</script>
<% } %>

<ffi:setProperty name="OpenEnded" value="false"/>
<ffi:removeProperty name="ModifyACHEntry" />
<ffi:removeProperty name="ACHEntryPayee" />
<ffi:removeProperty name="coEntrySecureDesc"/>
<ffi:removeProperty name="NumberPayments" />

