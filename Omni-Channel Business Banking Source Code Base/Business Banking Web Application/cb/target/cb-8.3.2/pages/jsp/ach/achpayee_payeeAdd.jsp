<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
<!--
    ns.ach.setInputTabText('#normalInputTab');
    ns.ach.setVerifyTabText('#normalVerifyTab');
    ns.ach.setConfirmTabText('#normalConfirmTab');
    ns.ach.showInputDivSteps();
//-->
</script>
<% String selectedSECCode=""; %>
<ffi:getProperty name="AddEditACHBatch" property="StandardEntryClassCode" assignTo="selectedSECCode" />
<%
    if (selectedSECCode != null && selectedSECCode.length() > 3)
        selectedSECCode = selectedSECCode.substring(0,3);
    if ("IAT".equals(selectedSECCode))
    {
        %>
            <ffi:setProperty name="AddEditACHPayee" property="PayeeType" value="2"/>
            <ffi:cinclude value1="${AddEditACHBatch.ISO_DestinationCountryCode}" value2="" operator="notEquals">
                <ffi:setProperty name="AddEditACHPayee" property="DestinationCountry" value="${AddEditACHBatch.ISO_DestinationCountryCode}"/>
                <ffi:setProperty name="AddEditACHPayee" property="BankCountryCode" value="${AddEditACHBatch.ISO_DestinationCountryCode}"/>
            </ffi:cinclude>
            <ffi:cinclude value1="${AddEditACHBatch.ISO_DestinationCurrencyCode}" value2="" operator="notEquals">
                <ffi:setProperty name="AddEditACHPayee" property="CurrencyCode" value="${AddEditACHBatch.ISO_DestinationCurrencyCode}"/>
            </ffi:cinclude>
            <ffi:cinclude value1="${AddEditACHBatch.ForeignExchangeIndicator}" value2="" operator="notEquals">
                <ffi:setProperty name="AddEditACHPayee" property="ForeignExchangeMethod" value="${AddEditACHBatch.ForeignExchangeIndicator}"/>
            </ffi:cinclude>
        <%
    }
 %>

<%
	if(request.getParameter("ManagedGridPayee") != null) {
		session.setAttribute("ManagedGridPayee", "true");
	} else session.removeAttribute("ManagedGridPayee");

	session.setAttribute("Action", request.getParameter("Action"));
%>
<span id="PageHeading" style="display:none;"><s:text name="ach.newPayeeTT" /></span>
<span class="shortcutPathClass" style="display:none;">pmtTran_ach,addACHPayeeLink</span>
<span class="shortcutEntitlementClass" style="display: none;">Manage ACH Participants</span>
    <ffi:removeProperty name="PayeeID"/>
    <div>
    <s:form id="ACHPayeeForm"  namespace="/pages/jsp/ach" action="addACHPayeeAction_verify" validate="false"  method="post" name="FormName" theme="simple" >
        <input type="hidden" name="isAdd" value="true"/>
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
                <div class="type-text">
                    <%-- using s:include caused buffer overflow --%>
                    <ffi:include page="/pages/jsp/ach/achpayeeaddedit.jsp" />
                </div>
         <div class="btn-row">
         <%if(session.getAttribute("ManagedGridPayee") != null) { %> 
	        <sj:a 
	                button="true"
	                onClick="ns.ach.gotoManagedPayees();"
	        ><s:text name="jsp.default_82" /></sj:a>
        <%} else { %>
        	<sj:a 
                button="true"
                summaryDivId="summary" buttonType="cancel" onClickTopics="showSummary,cancelACHPayeeForm"
        ><s:text name="jsp.default_82" /></sj:a>
        <%} %>
            <sj:a
            id="verifyACHPayeeSubmit2"
            targets="verifyDiv"
            button="true"
			formIds="ACHPayeeForm"
            validate="true"
            validateFunction="customValidation"
            onBeforeTopics="beforeVerifyACHPayeeForm"
            onCompleteTopics="verifyACHPayeeFormComplete"
            onErrorTopics="errorVerifyACHPayeeForm"
            onSuccessTopics="successVerifyACHPayeeForm"
			
			><s:text name="jsp.common.verifypayee" /></sj:a>
        <%--onClick="onSubmitCheck(false)"--%>
       </div>
    </s:form>
    </div>
