<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_achbatch-reverse-send" className="moduleHelpClass"/>
<%
    if (request.getParameter("SetCheckbox") != null )	// if they changed the SEC code, need to re-test default effective date
{
    String ID = request.getParameter("ActiveID");
    String checkBoxValue = request.getParameter("SetCheckbox");
    %>
    <ffi:setProperty name="ReverseACHBatch" property="CurrentEntry" value="<%=ID%>"/>
    <ffi:setProperty name="ReverseACHBatch" property="EntryActive" value="<%=checkBoxValue%>"/>
    <%
} else { %>

<ffi:setProperty name="ReverseACHBatch" property="Process" value="true" />
<ffi:process name="ReverseACHBatch" />
<s:include value="achbatch-reverse.jsp"/>

<% } %>