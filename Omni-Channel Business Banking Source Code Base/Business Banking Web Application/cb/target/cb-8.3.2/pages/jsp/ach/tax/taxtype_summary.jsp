<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<div id="taxtypeGridTabs" class="portlet gridPannelSupportCls" >
    <div class="portlet-header">
	    <div class="searchHeaderCls">
			<div class="summaryGridTitleHolder">
				<span id="selectedGridTab" class="selectedTabLbl" >
					<s:text name="ach.tax.state.tax.forms"/>
				</span>
				<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
				
				<div class="gridTabDropdownHolder">
					<span class="gridTabDropdownItem" id='stateTaxForm' onclick="ns.common.showGridSummary('stateTaxForm');" >
						<s:text name="ach.tax.state.tax.forms"/>
					</span>
					<span class="gridTabDropdownItem" id='federalTaxForm' onclick="ns.common.showGridSummary('federalTaxForm');">
						<s:text name="ach.tax.fed.forms"/>
					</span>
				</div>
			</div>
		</div>
    </div>
    
    <div class="portlet-content">
		<div id="gridContainer" class="summaryGridHolderDivCls">
			<div id="stateTaxFormSummaryGrid" gridId="taxStateTypesGridId" class="gridSummaryContainerCls" >
				<s:include value="/pages/jsp/ach/tax/taxtype_stategrid.jsp"/>
			</div>
			<div id="federalTaxFormSummaryGrid" gridId="taxFederalTypesGridId" class="gridSummaryContainerCls hidden" >
				<s:include value="/pages/jsp/ach/tax/taxtype_federalgrid.jsp"/>
			</div>
		</div>
    </div>
	
	<div id="taxTypeSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
       <ul class="portletHeaderMenu" style="background:#fff">
              <li><a href='#' onclick="ns.common.showDetailsHelp('taxtypeGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
       </ul>
	</div>
	
</div>
<input type="hidden" id="getTransID" value="<ffi:getProperty name='taxtypetabs' property='TransactionID'/>" />
	
<script>    
	//Initialize portlet with settings icon
	ns.common.initializePortlet("taxtypeGridTabs");
</script>
