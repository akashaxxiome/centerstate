<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

	<ffi:setProperty name="tempURL" value="/pages/jsp/ach/getACHEntriesAction.action?active=true" URLEncrypt="true"/>
    <s:url id="achConfirmEntryUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
		id="achEntriesConfirmGridId"
		caption=""
		sortable="true"
		dataType="json"
		href="%{achConfirmEntryUrl}"
		pager="true"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}"
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onGridCompleteTopics="addGridControlsEvents,ACHEntryConfirmGridCompleteEvents"
	>
		<sjg:gridColumn name="hold" index="hold" title="Hold" sortable="false" search="false" width="100" formatter="ns.ach.formatConfirmHoldLinks" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="achPayee.nickNameSecure" index="nickName" title="Nick Name" sortable="true" width="160" cssClass="datagrid_textColumn"/>
<s:if test="!#session.AddEditACHBatch.StandardEntryClassCodeDebitBatch.equals('WEB0')">
		<sjg:gridColumn name="achPayee.userAccountNumber" index="userAccountNumber" title="Participant ID" sortable="true" width="100"  cssClass="datagrid_textColumn"/>
</s:if>
<s:if test="#session.AddEditACHBatch.StandardEntryClassCodeDebitBatch.equals('WEB0')">
        <sjg:gridColumn name="achPayee.userAccountNumber" index="userAccountNumber" title="Sender" sortable="true" width="100"  cssClass="datagrid_textColumn"/>
</s:if>
		<sjg:gridColumn name="achPayee.routingNumberSecure" index="routingNumber" title="ABA #" sortable="true" width="100" cssClass="datagrid_numberColumn"/>
		<sjg:gridColumn name="achPayee.accountNumberSecure" index="accountNumber" title="Account #" sortable="true" width="100" cssClass="datagrid_numberColumn"/>
		<sjg:gridColumn name="achPayee.accountTypeSecure" index="accountType" title="Account Type" sortable="true" width="100" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="discretionaryData" index="discretionaryData" title="DD" sortable="true" width="100" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="amount" index="amount" title="Amount" sortable="true" width="100" formatter="ns.ach.formatConfirmAmountLinks" cssClass="datagrid_amountColumn"/>
		<sjg:gridColumn name="amountIsDebit" index="amountIsDebit" title="Debit/Credit" sortable="true" width="100" formatter="ns.ach.formatDebitLinks" cssClass="datagrid_amountColumn"/>
		<sjg:gridColumn name="active" index="active" title="Active" sortable="false" hidden="true" width="100"  hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="ID" index="ID" title="ID" sortable="false" hidden="true" width="100" hidedlg="true" cssClass="datagrid_textColumn"/>
		<sjg:gridColumn name="achPayee.map.DAStatus" index="daStatus" title="DAStatus" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>

		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="Amount" hidden="true" hidedlg="true" sortable="true" width="100" cssClass="datagrid_textColumn"/>

	</sjg:grid>

