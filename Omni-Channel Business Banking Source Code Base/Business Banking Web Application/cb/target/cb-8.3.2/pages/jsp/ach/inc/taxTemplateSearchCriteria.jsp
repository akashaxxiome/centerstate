<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%
    String viewAllText1 = "<!--L10NStart-->Business Tax<!--L10NEnd-->";
    String viewAllText2 = "<!--L10NStart-->Group Tax<!--L10NEnd-->";
    String viewMyText = "<!--L10NStart-->My Tax<!--L10NEnd-->";
%>
<s:form action="/pages/jsp/ach/taxTemplateSummary_verifyACHBatchTemplate.action" method="post" id="QuickSearchTaxFormID2" name="QuickSearchTransfersForm" theme="simple">
		<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
		<div id="templatessearchcriteria" class="quickSearchAreaCls">
			<div class="acntDashboard_masterHolder">
				<table>
                	<tr>
						<td>
							<label for="">&nbsp;Show:</label>
						</td>
						<td>
						  <s:select
								id="viewForTaxCompanyId2"
								cssClass="txtbox"
								headerKey="" headerValue="%{getText('ach.allCompanyIDQS')}"
								list="companyList" name="achBatchTemplateSearchCriteria.companyID"
								listKey="companyID" listValue="%{companyName+'('+companyID+')'}">
							</s:select> 		
                        </td>
						<td>
							<sj:a targets="quick" title="%{getText('jsp.default_6')}" 
								id="quicksearchbutton2" formIds="QuickSearchTaxFormID2" 
								button="true" 
								onCompleteTopics="quickSearchTaxTemplatesComplete">
								<s:text name="jsp.default_6" />
							</sj:a>
						</td>
                    </tr>
                </table>
			</div>
		</div>
</s:form>

<div id="multitemplatessearchcriteria" class="hidden marginTop10">
	No search criteria available for Multiple templates.
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#viewForTaxCompanyId2").selectmenu({width: 200});
});
</script>