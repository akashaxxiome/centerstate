<%@taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@taglib uri="/struts-jquery-tags" prefix="sj" %>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-jquery-grid-tags" prefix="sjg"%>
<%@page contentType="text/html; charset=UTF-8" %>

    <s:form id="ACHPayeeForm_Delete" namespace="/pages/jsp/ach" action="deleteACHPayeeAction"
        method="post" name="FormName" theme="simple" >
        <!-- <input type="hidden" name="taskName" value="DeleteACHPayee"/> --> 
        <input type="hidden" name="PayeeID" value="<ffi:getProperty name='PayeeID'/>"/>
        <input type="hidden" name="CSRF_TOKEN"
            value="<ffi:getProperty name='CSRF_TOKEN'/>" />
    </s:form>
        <div class="type-text">
            <%-- using s:include caused buffer overflow --%>
            <ffi:include page="/pages/jsp/ach/achpayeedelete.jsp" />
        </div>
<div class="ffivisible" style="height:50px;">&nbsp;</div>        
<div class="ui-widget-header customDialogFooter">
    <sj:a id="closeDeleteACHPayeeLink" button="true"
          onClickTopics="closeDeleteACHPayee"
          title="Cancel"><s:text name="jsp.default_82"/></sj:a>

    <sj:a id="DeleteACHSubmitButton"
          formIds="ACHPayeeForm_Delete"
          button="true"
          targets="resultmessage"
          title="Delete"
          onBeforeTopics="beforeDeleteACHPayeeForm"
          onErrorTopics="errorDeleteACHPayeeForm"
          onSuccessTopics="successDeleteACHPayeeForm"
		  effectDuration="1500"
          ><s:text name="jsp.default_162" /></sj:a>
</div>