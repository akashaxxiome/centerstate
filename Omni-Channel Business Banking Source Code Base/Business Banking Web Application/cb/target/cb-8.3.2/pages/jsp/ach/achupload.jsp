<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>



<s:set var="showNote" value="%{'false'}"/>

<ffi:cinclude value1="${SameDayACHEnabledForUser}" value2="true">

		<s:set var="showNote" value="%{'true'}"/>

</ffi:cinclude>

<ffi:cinclude value1="${SameDayChildSPEnabledForUser}" value2="true">

		<s:set var="showNote" value="%{'true'}"/>

</ffi:cinclude>

<ffi:cinclude value1="${SameDayTaxEnabledForUser}" value2="true">

		<s:set var="showNote" value="%{'true'}"/>

</ffi:cinclude>
 <%	String cutoffTime = "";
            String finalCutoffTime = "";
			String sameDayCutoffTime = "";
            String sameDayFinalCutoffTime = "";
 %>
<s:if test="%{#showNote=='true'}">

<ffi:object id="GetFileUploadNextCutOffTime" name="com.ffusion.tasks.affiliatebank.GetNextCutOffTime" scope="session" />
	<ffi:setProperty name="GetFileUploadNextCutOffTime" property="FIId" value="${SecureUser.BPWFIID}"/>
	<ffi:setProperty name="GetFileUploadNextCutOffTime" property="Category" value="<%=com.ffusion.beans.affiliatebank.CutOffDefines.PMT_GROUP_ACH_FILE_UPLOAD%>"/>
	<ffi:setProperty name="GetFileUploadNextCutOffTime" property="DateFormat" value="E hh:mm a (z)"/>
	<ffi:setProperty name="GetFileUploadNextCutOffTime" property="Locale" value="${UserLocale.Locale}"/>
    <ffi:setL10NProperty name="GetFileUploadNextCutOffTime" property="NotSetString" value="Not Available/Cut-off time not set"/>
	<ffi:setProperty name="GetFileUploadNextCutOffTime" property="InstructionType" value="ACHFILETRN"/>
<ffi:process name="GetFileUploadNextCutOffTime"/>


<ffi:object id="GetSameDayFileUploadNextCutOffTime" name="com.ffusion.tasks.affiliatebank.GetNextCutOffTime" scope="session" />
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime" property="FIId" value="${SecureUser.BPWFIID}"/>
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime" property="Category" value="<%=com.ffusion.beans.affiliatebank.CutOffDefines.PMT_GROUP_ACH_FILE_UPLOAD%>"/>
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime" property="DateFormat" value="E hh:mm a (z)"/>
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime" property="Locale" value="${UserLocale.Locale}"/>
    <ffi:setL10NProperty name="GetSameDayFileUploadNextCutOffTime" property="NotSetString" value="Not Available/Cut-off time not set"/>
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime" property="InstructionType" value="SAMEDAYACHFILETRN"/>
<ffi:process name="GetSameDayFileUploadNextCutOffTime"/>


</s:if>



<ffi:help id="payments_achupload" className="moduleHelpClass"/>
<span class="shortcutPathClass" style="display:none;">pmtTran_ach,ACHUploadLink</span>
<span class="shortcutEntitlementClass" style="display: none;">File Upload</span>
<span class="shortcutEntitlementClass" style="display: none;">ACH File Upload</span>

 <ffi:setProperty name="dontCleanupACHSession" value="true"/>
   <ffi:object id="FileUpload" name="com.ffusion.tasks.fileImport.FileUploadTask" scope="session"/>
   <ffi:setProperty name="FileUpload" property="FileType" value="ACH" />
    <ffi:setProperty name="fileType" value="ACH" />
    <ffi:setProperty name="FileUpload" property="AuthenticateURL" value="${SecurePath}payments/achupload.jsp?Authenticate=true" URLEncrypt="true" />

   <ffi:object id="ProcessACHFileUpload" name="com.ffusion.tasks.fileImport.ProcessACHFileUploadTask" scope="session"/>
    <ffi:setProperty name="FileUpload" property="SuccessURL" value="${SecureServletPath}ProcessACHFileUpload"/>
    <ffi:setProperty name="ProcessACHFileUpload" property="SuccessURL" value="${SecureServletPath}NewReportBase?NewReportBase.ReportName=${GetReportsByCategoryACH.ReportCategory}&NewReportBase.Target=${SecureServletPath}GenerateReportBase" URLEncrypt="true"/>
   <ffi:object id="NewReportBase" name="com.ffusion.tasks.reporting.NewReportBase"/>
   <ffi:object id="GenerateReportBase" name="com.ffusion.tasks.reporting.GenerateReportBase"/>
   <ffi:setProperty name="GenerateReportBase" property="Target" value="${SecurePath}reports/displayreport.jsp"/>

   <ffi:object id="GetReportsByCategoryACH" name="com.ffusion.tasks.reporting.GetReportsByCategory"/>
   <ffi:setProperty name="GetReportsByCategoryACH" property="ReportCategory" value="<%= com.ffusion.beans.ach.ACHReportConsts.RPT_TYPE_FILE_UPLOAD %>" />
   <ffi:setProperty name="GetReportsByCategoryACH" property="ReportsName" value="ReportsACH" />
   <ffi:process name="GetReportsByCategoryACH" />
	<ffi:setProperty name="ReportName" value="${GetReportsByCategoryACH.ReportCategory}" />
   <%
    session.setAttribute("FFIFileUpload", session.getAttribute("FileUpload"));
    session.setAttribute("FFIProcessACHFileUpload", session.getAttribute("ProcessACHFileUpload"));
    session.setAttribute("FFINewReportBase", session.getAttribute("NewReportBase"));
    session.setAttribute("FFIGenerateReportBase", session.getAttribute("GenerateReportBase"));
   %>


   	<script>
	$.subscribe('openFileUploadTopics', function(event,data){
		$("#openFileImportDialogID").dialog('open');
	});
	</script>
	  <div>
	  <div align="left">
	  <form id="FileTypeID" name="FileType" action="/cb/pages/fileupload/ACHFileUploadAction_openACHImportWidget.action" method="post" target="FileUpload">
          <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
          <input type="hidden" name="FileUpload.FileType" value="ACH">
          <input type="hidden" name="ProcessImportTask" value="ProcessACHFileUpload">
	  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableData">
	  <tr>
	  <td></td>
	  <td class="lightBackground" align="left">
	  	<s:if test="%{#showNote=='true'}">
		<ffi:getProperty name="GetFileUploadNextCutOffTime" property="NextCutOffTime" assignTo="cutoffTime" />
		<ffi:getProperty name="GetFileUploadNextCutOffTime" property="FinalCutOffTime" assignTo="finalCutoffTime" />
		<ffi:getProperty name="GetSameDayFileUploadNextCutOffTime" property="NextCutOffTime" assignTo="sameDayCutoffTime" />
		<ffi:getProperty name="GetSameDayFileUploadNextCutOffTime" property="FinalCutOffTime" assignTo="sameDayFinalCutoffTime" />

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td  class="columndata" colspan="3">
				<b><s:text name="jsp.payments.achupload3"/></b>
				</td>
			</tr>
			<tr>
				<td  class="columndata" colspan="3">
				<s:text name="jsp.payments.achupload1">
				<s:param><ffi:getProperty name='GetSameDayFileUploadNextCutOffTime' property='NextCutOffTime'/></s:param>
				</s:text><br/>
				</td>
			</tr>
			<% if (sameDayCutoffTime != null && !sameDayCutoffTime.equals(sameDayFinalCutoffTime)) { %>
				<tr>
				<td class="columndata" colspan="3" >
					<s:text name="jsp.payments.achupload5">
						<s:param><ffi:getProperty name='GetSameDayFileUploadNextCutOffTime' property='FinalCutOffTime'/></s:param>
					</s:text>
				</td>
				</tr>
			<% } %>
			<tr>
			<td  class="columndata" colspan="3">
				<s:text name="jsp.payments.achupload2">
				<s:param><ffi:getProperty name='GetFileUploadNextCutOffTime' property='NextCutOffTime'/></s:param>
				</s:text><br/>
				</td>

			</tr>
				<% if (cutoffTime != null && !cutoffTime.equals(finalCutoffTime)) { %>
				<tr>
				<td class="columndata" colspan="3" >
					<s:text name="jsp.payments.achupload6"> <s:param><ffi:getProperty name='GetFileUploadNextCutOffTime' property='FinalCutOffTime'/></s:param> </s:text>
				</td>
				</tr>
			<% } %>
			<tr>
				<td  colspan="3">
				<br/>
				</td>
			</tr>
			</table>
			</s:if>

	  <table width="98%" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<td class="columndata" width="480"><br><!--L10NStart-->Fusion Bank accepts electronic records from a variety of applications you may be using in your business. Uploading these records is easy and can save your business a significant amount of time and expense. Accepted file types are displayed in the column on the right.<br>
			<br>If you have questions about file upload, see the Help section, or call us at 1-800-000-0000.<!--L10NEnd-->
			<s:if test="%{#showNote=='true'}">
			<br/> <br/>
			<b>	<s:text name="jsp.payments.achupload4"/></b>
			</s:if>


			</td>
		</tr>



		<tr>
			<td class="warningField"><br><!--L10NStart-->ACH Record must be in NACHA format<!--L10NEnd--></td>
		</tr>
		<tr>
			<td colspan="3" align="center" nowrap>


<%-- 				<sj:a
					id="cancelFileImportTypeID"
					onclick="ns.ach.cancelAchFileUpload()"
                    button="true"
                    cssClass="cancelImportFormButton"
                ><s:text name="jsp.default_82" /><!-- CANCEL -->
           		</sj:a> --%>
				<sj:a
					id="fileImportTypeID"
					formIds="FileTypeID"
                    targets="openFileImportDialogID"
                    button="true"
					onclick="removeValidationErrors();"
					onErrorTopics="errorOpenFileUploadTopics"
					onSuccessTopics="openFileUploadTopics"
                >Upload File
           		</sj:a>
			</td>
		</tr>
		<tr>
	 </table>
	 </td>
	 <td></td>
	 </tr>
	 </table>
	 </form>
	 	</div>
	 </div>
<ffi:object id="FFIUpdateReportCriteria" name="com.ffusion.tasks.reporting.UpdateReportCriteria"/>
<ffi:setProperty name="FFIUpdateReportCriteria" property="ReportName" value="ReportData"/>
<ffi:setProperty name="actionNameNewReportURL" value="NewReportBaseAction.action?NewReportBase.ReportName=${ReportName}&flag=generateReport" URLEncrypt="true" />
<ffi:setProperty name="actionNameGenerateReportURL" value="GenerateReportBaseAction_display.action?doneCompleteDirection=${doneCompleteDirection}" URLEncrypt="true" />