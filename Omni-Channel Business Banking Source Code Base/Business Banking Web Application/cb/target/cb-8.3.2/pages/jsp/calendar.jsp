<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Copy over some necessary parameters --%>
<% String strTarget = null;		//required %>
 <ffi:setProperty name="tempVar" value="${calTarget}"/>
 <ffi:getProperty name="tempVar" assignTo="strTarget"/>
<% strTarget = com.ffusion.util.HTMLUtil.encode(strTarget); %>

<% String strForm = null;		//required %>
 <ffi:setProperty name="tempVar" value="${calForm}"/>
 <ffi:getProperty name="tempVar" assignTo="strForm"/>

<ffi:removeProperty name="tempVar"/>

<s:include value="/pages/jsp/calendar2.jsp" />