<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.opensymphony.xwork2.*" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<ffi:removeProperty name="CreateSession"/>

<ffi:setProperty name="BackURL" value="${SecurePath}invalidate-session.jsp" />
<ffi:task errorURL="${PathExt}invalid-input.jsp" />
<ffi:task serviceErrorURL="${PathExt}process-error.jsp" />
<ffi:task errorRedirectURL="${PathExt}error-redirect.jsp" />
<ffi:task taskRedirectURL="${PathExt}task-redirect.jsp" />

<%--
	String uri = request.getRequestURI();
	session.setAttribute("uriRoot", uri.substring(0, uri.indexOf("start-page.jsp")));
--%>


<%
ActionContext ctx = ActionContext.getContext();
ctx.put("__requestWrapper.getAttribute", new Boolean(true));
%>

<%-- Execute logic for menu tab display --%>
<s:include value="inc/init/menu-init.jsp"></s:include>

<%-- Hide Reporting tab for Consumer and Small Business --%>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="nav_showtop_reports" value="false" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>" operator="equals">
	<ffi:setProperty name="nav_showtop_reports" value="false" />
</ffi:cinclude>

<%-- Get the user's navigation settings --%>
<ffi:object name="com.ffusion.struts.utils.GetNavigationSettingsCustom" id="GetNavigationSettings" scope="session"/>
<%-- <ffi:object name="com.ffusion.tasks.user.GetNavigationSettings" id="GetNavigationSettings" scope="session"/> --%>
	<ffi:setProperty name="EntitledMenuTabs" value="home"/>
    <ffi:setProperty name="GetNavigationSettings" property="EntitledMenuTab" value="home"/>
    <ffi:cinclude value1="${nav_showtop_account}" value2="true">
        <ffi:setProperty name="GetNavigationSettings" property="EntitledMenuTab" value="acctMgmt"/>
		<ffi:setProperty name="EntitledMenuTabs" value="${EntitledMenuTabs},acctMgmt"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${nav_showtop_cash}" value2="true">
        <ffi:setProperty name="GetNavigationSettings" property="EntitledMenuTab" value="cashMgmt"/>
		<ffi:setProperty name="EntitledMenuTabs" value="${EntitledMenuTabs},cashMgmt"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${nav_showtop_payments}" value2="true">
        <ffi:setProperty name="GetNavigationSettings" property="EntitledMenuTab" value="pmtTran"/>
		<ffi:setProperty name="EntitledMenuTabs" value="${EntitledMenuTabs},pmtTran"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${nav_showtop_user}" value2="true">
        <ffi:setProperty name="GetNavigationSettings" property="EntitledMenuTab" value="admin"/>
		<ffi:setProperty name="EntitledMenuTabs" value="${EntitledMenuTabs},admin"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${nav_showtop_approvals}" value2="true">
        <ffi:setProperty name="GetNavigationSettings" property="EntitledMenuTab" value="approvals"/>
		<ffi:setProperty name="EntitledMenuTabs" value="${EntitledMenuTabs},approvals"/>
    </ffi:cinclude>
    <ffi:cinclude value1="${nav_showtop_reports}" value2="true">
        <ffi:setProperty name="GetNavigationSettings" property="EntitledMenuTab" value="reporting"/>
		<ffi:setProperty name="EntitledMenuTabs" value="${EntitledMenuTabs},reporting"/>
    </ffi:cinclude>
    
	<ffi:setProperty name="EntitledMenuTabs" value="${EntitledMenuTabs},contactus"/>
    
    <ffi:cinclude value1="${nav_showtop_servicecenter}" value2="true">
        <ffi:setProperty name="GetNavigationSettings" property="EntitledMenuTab" value="serviceCenter"/>
		<ffi:setProperty name="EntitledMenuTabs" value="${EntitledMenuTabs},serviceCenter"/>
    </ffi:cinclude>
 	<%-- Show preferences menu --%>
	<ffi:setProperty name="GetNavigationSettings" property="EntitledMenuTab" value="preferences"/>
	<ffi:setProperty name="EntitledMenuTabs" value="${EntitledMenuTabs},preferences"/>
	
	
<ffi:process name="GetNavigationSettings"/>

<script type="text/javascript">
	location.replace("<s:url value='/pages'/>/jsp/home/mainPage.jsp");
	//location.replace("<s:url value='/pages'/>/jsp/home/index.jsp");
</script>
