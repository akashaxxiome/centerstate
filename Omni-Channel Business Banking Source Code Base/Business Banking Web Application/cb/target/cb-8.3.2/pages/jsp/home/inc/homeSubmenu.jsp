<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%
	Map menuShortcuts = (Map)request.getAttribute("menuShortcuts");
%>

<div class="submenuHolderBgCls">
	<div class="formActionItemPlaceholder">
		<div class="cardHolder">
			<div class="subMenuCard">
				<span class="cardTitle">_______________</span>
				<div class="cardSummary">
					<span>__________________________<br>__________________________<br>________</span>
				</div>
				<div class="cardLinks">
					<span>___________</span>
					<span>___________</span>
				</div> 
			</div>
		</div>	
	</div> 
	<ul id="tab11" class="mainSubmenuItemsCls">
				<!-- <div class="submenuListBg"></div> -->
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MESSAGES %>" >
					<li id="menu_home_message" menuId="home_message">
					<ffi:help id="home_message" className="moduleHelpClass"/>
					<s:url id="messageMenuURL" value="/pages/jsp/message/index.jsp" escapeAmp="false"></s:url>
					<sj:a
							href="%{messageMenuURL}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_121"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_home_message')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{messageMenuURL}"/>','home_message','Message');"><s:text name="jsp.default.messageHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.messages"/> 
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{messageMenuURL}"/>','home_message','Message','inbox');"><s:text name="message.received"/></span>
										<span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{messageMenuURL}"/>','home_message','Message','sentbox');"><s:text name="message.sent" /></span>
									</div>
								</div>
							</div>	
							<%-- <span class="removeComma" onclick="ns.common.loadSubmenuSummary('<s:property value="%{messageMenuURL}"/>','home_message','Message','newMessageLink');"><s:text name="jsp.message_27" /></span><br>
							<span class="removeComma" onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{messageMenuURL}"/>','home_message','Message','inbox');"><s:text name="message.received"/></span><br>
							<span class="removeComma" onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{messageMenuURL}"/>','home_message','Message','sentbox');"><s:text name="message.sent" /></span> --%>
						</div>
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-msgs"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_105')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_121')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "home_message");
					%>
				</ffi:cinclude>
				
				<!-- alerts menu items -->
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ALERTS%>" >
					<li id="menu_home_alert" menuId="home_alert">
					<ffi:help id="home_alert" className="moduleHelpClass"/>
					<s:url id="alertMenuUrl" value="/pages/jsp/alert/initAlertsAction.action" escapeAmp="false"></s:url>
					<sj:a
							href="%{alertMenuUrl}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_29"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_home_alert')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','SubscribedAlerts');"><s:text name="jsp.default.alertsHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.alerts"/>
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','SubscribedAlerts','subscribeAlertsLink');"><s:text name="alerts.dashboard.button.addeditalerts" /></span>
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','AlertLogs');"><s:text name="alerts.dashboard.button.alertlogs" /></span>
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','ContactPoints');"><s:text name="alerts.dashboard.button.contactpointS"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.contactPoints"/>
											 
										</span>
									</div>
									<div class="cardLinks">
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','ContactPoints','addNewCPLink');"><s:text name="alerts.dashboard.button.addcontactpoint"/></span>
									</div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','MobileDevices');"><s:text name="alerts.dashboard.button.Mobile.Devices"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.mobileDevices"/>
											 
										</span>
									</div>
									<div class="cardLinks">
									<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MBANKING%>" >
										<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','MobileDevices','addNewDeviceLink');"><s:text name="alerts.dashboard.button.add.Mobile.Devices"/></span>
									</ffi:cinclude>	
									</div>
								</div>
							</div>
							
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','SubscribedAlerts');"><s:text name="alerts.dashboard.button.subscribedalerts" /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','SubscribedAlerts','subscribeAlertsLink');"><s:text name="alerts.dashboard.button.addeditalerts" /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','AlertLogs');"><s:text name="alerts.dashboard.button.alertlogs" /></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','ContactPoints');"><s:text name="alerts.dashboard.button.contactpointS"/></span>
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','ContactPoints','addNewCPLink');"><s:text name="alerts.dashboard.button.addcontactpoint"/></span>

							<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MBANKING%>" >
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','MobileDevices');"><s:text name="alerts.dashboard.button.Mobile.Devices"/></span>
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{alertMenuUrl}"/>','home_alert','MobileDevices','addNewDeviceLink');"><s:text name="alerts.dashboard.button.add.Mobile.Devices"/></span>
							</ffi:cinclude>	 --%>
						</div>
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-alerts"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_105')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_29')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "home_alert");
					%>
				</ffi:cinclude>

				
				<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
					<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_USERS%>" >
						<ffi:cinclude value1="${SecureUser.UsingEntProfiles}" value2="true" operator="notEquals">
							<li id="menu_home_manageUsers" menuId="home_manageUsers">
								<ffi:help id="menu_home_manageUsers" className="moduleHelpClass"/>
								<s:url id="manageUsersMenuURL" value="/pages/jsp/user/manage-users.jsp" escapeAmp="false">
									<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
								</s:url>
								<sj:a
										href="%{manageUsersMenuURL}"
										indicator="indicator"
										targets="notes"
										onCompleteTopics="subMenuOnCompleteTopics"
									><s:text name="jsp.user.preferences.manageUsersTabName"/></sj:a>
								<span onclick="ns.common.showMenuHelp('menu_home_manageUsers')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
								<div class="formActionItemHolder hidden">
									<div class="cardHolder">
										<div class="subMenuCard">
											<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{manageUsersMenuURL}"/>','home_manageUsers','ManageUsers','addSecondaryUserBtn');" ><s:text name="jsp.user_28"/></span>
											<div class="cardSummary">
												<span>This area provides brief information about this feature. Please click on help icon to know more. 
													 
												</span>
											</div>
											<div class="cardLinks borderNone">
											</div>
										</div>
									</div>	
									<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{manageUsersMenuURL}"/>','home_manageUsers','ManageUsers','addSecondaryUserBtn');" ><s:text name="jsp.user_28"/></span> --%>
								</div>
								<span class="ffiUiIco-menuItem ffiUiIco-menuItem-manageUsers"></span>
								<s:set var="tempMenuName" value="%{getText('jsp.home_105')}" scope="session" />
								<s:set var="tempSubMenuName" value="%{getText('jsp.user.preferences.manageUsersTabName')}" scope="session" />
								<% 
									menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "home_manageUsers");
								%>
							</li>
						</ffi:cinclude>
					</ffi:cinclude>
				</ffi:cinclude>
			</ul>
</div>