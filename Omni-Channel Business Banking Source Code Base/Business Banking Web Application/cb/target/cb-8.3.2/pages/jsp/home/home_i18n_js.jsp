<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%-- 
	NOTE: 
	This file is not being used now its code has been moved to home.js 
	TODO: DELETE THIS FILE	
	--%>
<script type="text/javascript">
	ns.home.initI18N = function(){
		// initialize system language global variable (zh_CN or en_US)
		ns.home.locale = '<ffi:getProperty name="UserLocale" property="Locale"/>';
    	var jqLocale = ns.home.getJQLocale();
    	$.struts2_jquery.gridLocal = jqLocale;
		$.struts2_jquery.require("i18n/grid.locale-" +	$.struts2_jquery.gridLocal + ".js",
				function() {
					$.jgrid.no_legacy_api = true;
					$.jgrid.useJSON = true;
				}, '/cb/struts/');
		$.struts2_jquery.require("js/plugins/jquery.jqGrid.js",'','/cb/struts/');
		//$.struts2_jquery.requireCss("themes/ui.jqgrid.css",'/cb/struts/');
		$.extend($.jgrid.defaults, { 
			altRows: true,
			altclass: 'ui-state-zebra',
			autowidth: true,
			gridview: true,
			loadui: 'disable',
			ajaxGridOptions: {error: function(){ns.common.resizeWidthOfGrids();}},
			
			});

		// load i18n properties file for datepicker, English is the default
		if(ns.home.locale != 'en_US'){
        	$.struts2_jquery.require('i18n/jquery.ui.datepicker-' + jqLocale + '.min.js', null, '/cb/struts/');
		}
		
		// load i18n properties file for date range picker, English is the default
		if(ns.home.locale != 'en_US'){
        	$.struts2_jquery.require("i18n/ux.extdaterangepicker.locale." + jqLocale + ".min.js", null , "/cb/web/js/common/widgets/");
		}
		
		// load i18n properties file for full calendar, English is the default
    	$.struts2_jquery.require('i18n/fullCalendar.locale-' + jqLocale + '.js', null, '/cb/web/js/calendar/');
		
		$.struts2_jquery.require("js/jqGridSortableColumns.min.js",'','/cb/web/');
		$.struts2_jquery.require("js/JsonXml.js",'','/cb/web/');
		$.struts2_jquery.require("js/grid.import.js",'','/cb/web/');
		$.struts2_jquery.require("js/plugins/grid.inlinedit.js",'','/cb/struts/');
		$.struts2_jquery.require("js/plugins/grid.formedit.js",'','/cb/struts/');
		
		
		
		

		(function($){
		$.jgrid.extend({
			destroy: function () {
	        	//$(document).unbind('mouseup'); //commented with jquery UI upgrade 1.10.3, as event is biding again once bind. 
				var events = $(window).data('events');
				if(events){
					var tempHandler = [], count = 0;
					$.each(events.unload,function(i,unloadObj){
						if(unloadObj.namespace==='') {tempHandler[count++]=unloadObj.handler;}
					})
					for (var i=0;i<count;i++){$(window).unbind('unload',tempHandler[i]);}
					delete tempHandler;
				}
				return this.each(function(){
	            	if ( this.grid ) { 
						var gid = this.id;
						try {
							$(this).jqGrid('clearBeforeUnload');
							$("#gbox_"+gid).find('.ui-jqgrid-resize-mark').removeClass().unbind().remove();
			                $("#gbox_"+gid).find('.jqgrid-overlay').removeClass().unbind().unbind().remove();;
			                $("#gbox_"+gid).find('.loading').removeClass().unbind().unbind().remove();
			                $.delAttrByRoot(this.id);
			                $("#gbox_"+gid).remove();
			                //alert('done');
			            } catch (_) {}
					}
				});
	        },
		
		clearBeforeUnload : function () {
            return this.each(function(){
            	var grid = this.grid;
            	var gboxID = "#gbox_"+this.id;
				
            	// Iterate over headers to release headers
                var i, j, l = grid.headers.length;
                var sortIconContainer = null;
                var searchRow = $(grid.hDiv).find('tr.ui-search-toolbar');
                var labelRow = $(grid.hDiv).find('tr.ui-jqgrid-labels');
                var searchRowTH = $(searchRow).find('th');
                var labelRowTH = $(labelRow).find('th');
                var spanArray = null;
                for (i = 0; i < l; i++) {
                	//Access sort image and Header separator span and release them
                	spanArray = $(grid.headers[i].el).find('span');
                	for(j = 0;j < spanArray.length; j++){
                		$(spanArray[j]).removeClass();
                		$(spanArray[j]).unbind();
                		spanArray[j].innerHTML = null;
                		spanArray[j] = null;
                	}
                	sortIconContainer = $(grid.headers[i].el).find('div.ui-jqgrid-sortable');
                	sortIconContainer.removeClass();
                	sortIconContainer.unbind();
                	sortIconContainer = null;
                	grid.headers[i].el = null;
                	$(searchRowTH).removeClass().unbind();
                	$(labelRowTH).removeClass().unbind();
                }
                
                // Remove header rows and thead
                $(searchRow).removeClass().unbind();
                if($(labelRow).data("ui-sortable") != undefined){
                	$(labelRow).sortable('destroy').removeClass().unbind();	
                }
                
				$(grid.hDiv).find('table').unbind().remove();
                
              //$(document).unbind("mouseup"); // TODO add namespace
              	grid.headers = null;
              	grid.cols = null;
                $(grid.hDiv).unbind("mousemove"); // TODO add namespace
               
                //Remove all rows.
               //$(grid.hDiv).remove();
               var mainPagerTable = null;
               var pagerTDcollection = null;
               var pagerTablecollection = null;
               var pagerNavigationTableTD = null;
               if ( this.p.pager ) { // if not part of grid
					mainPagerTable = $(gboxID).find('table.ui-pg-table').filter(":first");
					pagerNavigationTableTD = mainPagerTable.find('table.navtable').find('td');
					for(i = 0; i < pagerNavigationTableTD.length; i++){
						var spanCollection = $(pagerNavigationTableTD[i]).find('span')
						for(j = 0; j < spanCollection.length; j++){
							$(spanCollection[j]).removeClass().unbind();
							$(spanCollection[j]).innerHTML = null;
						}
						$(pagerNavigationTableTD[i]).find('ui-pg-div').removeClass().unbind();
						$(pagerNavigationTableTD[i]).removeClass().unbind().empty();
					}
					pagerTablecollection = mainPagerTable.find('table.ui-pg-table');
					for(i = 0; i < pagerTablecollection.length; i++){
					 	pagerTDcollection = $(pagerTablecollection[i]).find('td');
						for(j = 0; j < pagerTDcollection.length; j++){
							$(pagerTDcollection[j]).find('span').removeClass().unbind();
							$(pagerTDcollection[j]).find('select').removeClass().unbind();
							$(pagerTDcollection[j]).find('input').removeClass().unbind();							
							$(pagerTDcollection[j]).removeClass().unbind().empty();
						 	//pagerTDcollection[j].innerHTML = null;
						}
						
					}
					mainPagerTable.find('td').removeClass().unbind().html('');
					mainPagerTable.removeClass().unbind().remove();
					//$(this.p.pager).remove();
         			this.p.pager = null;
				}
           		
               var dataTable = $(gboxID).find('table.ui-jqgrid-btable');
               var anchorInsideDataTD = dataTable.find('td a');
               for(i = 0; i < anchorInsideDataTD.length; i++){
           			anchorInsideDataTD[i].onclick = '';
           			anchorInsideDataTD.innerHTML = null;           			
           		}
               
               //alert('using tbody approach tr unbind');
               /* var dataTds = dataTable.find('td');
               for(i = 0; i < dataTds.length; i++){
            	   $(dataTds[i]).removeClass().unbind().attr('aria-describedby','').attr('role','').attr('title','');
            	   dataTds[i].innerHTML = null;
               }
               dataTable.find('tr.ui-widget-content').unbind().remove(); */
               this.p.colModel = null;
               var tBody = $("tbody", grid.bDiv).filter(':first');
               $("td",tBody[0]).unbind().empty();
               $("tr",tBody[0]).removeClass().unbind().remove();
               
               /* var dataRows = $(gboxID).find('table.ui-jqgrid-btable').find('tr');
               for(i = 0; i < dataRows.length; i++){
            	   $(dataRows[i]).find('td').html('');
            	   $(dataRows[i]).removeClass().unbind().empty();
               }*/
               //$(gboxID).find('table.ui-jqgrid-btable tbody:first tr').remove();
               //$("tbody:first tr", parent).remove()
               this.p.colModel = null;
               dataTable.unbind().removeClass();
				$(grid.bDiv).unbind();
           		
           		if(grid.footers.length){
           			var footerTDs = $(gboxID).find('table.ui-jqgrid-ftable').find('td');
	           		for(i = 0; i < footerTDs.length; i++){
	           			$(footerTDs[i]).unbind().removeClass();
	           			footerTDs[i].innerHTML = null;           			
	           		}
           		}
           		//grid.footers.unbind().remove();
           		grid.footers = null;
           		$(gboxID).find('tr').unbind().removeClass().remove();
           		
				           	
               //$(grid.bDiv).remove();;
               $(grid.cDiv).unbind().remove();
               
               $(this).unbind();
                grid.dragEnd = null;
                grid.dragMove = null;
                grid.dragStart = null;
                grid.emptyRows = null;
                grid.populate = null;
                grid.populateVisible = null;
                grid.scrollGrid = null;
                grid.selectionPreserver = null;

                grid.bDiv = null;
                grid.cDiv = null;
                grid.hDiv = null;
                grid.cols = null;
                grid.footers=null;
				
                this.grid = null;
                this.formatCol = null;
                this.sortData = null;
                this.updatepager = null;
                this.refreshIndex = null;
                this.setHeadCheckBox = null;
                this.constructTr = null;
                this.formatter = null;
                this.addXmlData = null;
                this.addJSONData = null;
                this.p = null;                
            });
        }
       /*  emptyRows = function (scroll, locdata) {
            var firstrow;
            if (this.p.deepempty) {
                $(this.rows).slice(1).remove();
            } else {
                firstrow = this.rows.length > 0 ? this.rows[0] : null;
                $(this.firstChild).empty().append(firstrow);
            }
            if (scroll && this.p.scroll) {
                $(this.grid.bDiv.firstChild).css({height: "auto"});
                $(this.grid.bDiv.firstChild.firstChild).css({height: 0, display: "none"});
                if (this.grid.bDiv.scrollTop !== 0) {
                    this.grid.bDiv.scrollTop = 0;
                }
            }
            if(locdata === true && this.p.treeGrid) {
                this.p.data = []; this.p._index = {};
            }
        } */});
		})(jQuery);
	};
	
		
</script>
