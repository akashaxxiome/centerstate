<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<s:url id="authenticationUrl" value="%{#session.PagesPath}security-check.jsp" escapeAmp="false">
   		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
</s:url>
<sj:dialog id="authenticationInfoDialogID" title="%{getText('jsp.home_179')}" href="%{authenticationUrl}"
		modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="450">
</sj:dialog>
