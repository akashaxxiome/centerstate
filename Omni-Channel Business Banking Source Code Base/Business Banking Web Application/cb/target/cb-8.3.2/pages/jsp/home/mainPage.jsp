<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		
		<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8"/>
		<title><s:text name="jsp.home_90"/></title>
		<style type="text/css">
		   html, body, div,iframe{ margin:0; padding:0; height:100%; }
		   iframe { display:block; width:100%;}
		   .sessionNotificationDiv{height: 20px; width:100%; display:none; text-align:center; 
		   		font: 400; font-size: 12px;  font-family: Verdana,Arial,Helvetica,sans-serif;}   
		</style>
		
		<%-- Add jquery library --%>
		<s:url id="head" value="/web/css"/>
		<s:include value="include-theme.jsp" />	
		<%--
			JS and CSS files Inclusion
			NOTE: 
				-To improve page performance the JS and CSS files have been aggreagated during build, please refer to main-page.css and
				main-page.js configuration in pom.xml of war. 
				-Any new css or js file inclusion has to be defined in pom.xml
				-The order of files is important. To resolve dependency files are placed in right order in pom.xml

			Files included in main-page.css
				/web/css/showLoading.css
				/web/css/Consumer/jquery-ui.css
		--%>		
		<link type="text/css" rel="stylesheet" src="<s:url value='/web/css/main-page%{#session.minVersion}.css'/>"></link>

		<%--
			Files included in main-page.js
				/web/js/jquery.showLoading.js
				/web/js/jquery.history.js
				/web/js/jquery.i18n.properties.js
				/web/js/common/handleHistory.js
				/web/js/common/userNavigateAwayCheck.js
		--%>
		<script type="text/javascript" src="<s:url value='/web/js/main-page%{#session.minVersion}.js'/>"></script>
		
		<script type="text/javascript">
			var windowTitle="";
			$(document).ready(function() {
				jQuery.i18n.properties({
					name: 'MainPageJavaScript',
					path:'<s:url value="/web/js/i18n/" />',
					mode:'both',
					language: '<ffi:getProperty name="SecureUser" property="Locale"/>',
					cache:true,
					callback: function(){
					}
				});
				// Window title
				windowTitle = js_FFI_label;
			
			});
			
			//Text to be shown in java script alert pop-up, while navigating away from page.
			var logoutPopupText = '<s:text name="%{\'jsp.common_logoutPopup\' + #session.SecureUser.AffiliateID }"/>';
					
			// URL required to log out user.
			var logoutUrl = '/cb/pages/jsp/invalidate-session.jsp';
		
			// Set start page used to maintain start page after login by handlehistory.js
			var startingPage = '<ffi:getProperty name="startPage"/>';

	// Show loading image so that User will get feel of something is happening.
	var intervalId = '';
	$('#ifrm').showLoading({
		'addClass': 'loading-indicator-dripcircle',
	    'afterShow': function() {
	    		setTimeout("hideMainPageLoading()", 30000);
			}
		}
	);
	
	
	// Function to hide loading symbol once default loading period is over.
	function hideMainPageLoading(){
		jQuery('#ifrm').hideLoading();
	}
	</script>
    </head>
	<body style="overflow:hidden;">
		<div>
			<div id="sessionNotificationMsgDiv" class="ui-state-highlight sessionNotificationDiv"></div>
			<iframe id="ifrm" name="ifrm" src='index.jsp' 
			 width="100%" height="100%" frameborder="0" title="OCB Online"></iframe>
		</div>
	</body>	
</html>