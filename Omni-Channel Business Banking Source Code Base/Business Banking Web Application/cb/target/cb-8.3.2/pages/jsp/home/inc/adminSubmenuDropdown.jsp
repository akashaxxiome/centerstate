<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- selected module drop down items holder --%>
<ul class="moduleSubmenuDropdownHolder">
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.COMPANY_SUMMARY %>" >
		<li onclick="ns.shortcut.goToMenu('admin_summary');">
			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-adminSummary"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_197"/></span>
		</li>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN %>" >
		<li onclick="ns.shortcut.goToMenu('admin_company');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-adminCompany"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_58"/></span>
		</li>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
		<li onclick="ns.shortcut.goToMenu('admin_division');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-adminDivision"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_83"/></span>
		</li>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT %>" >
		<li onclick="ns.shortcut.goToMenu('admin_group');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-adminGroups"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_101"/></span>
		</li>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.USER_ADMIN %>" >
		<li onclick="ns.shortcut.goToMenu('admin_user');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-adminUsers"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_229"/></span>
		</li>
</ffi:cinclude>
</ul>	