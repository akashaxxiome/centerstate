<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:setProperty name="populateReversePositivePayPortletDataURL" value="/cb/pages/jsp/home/ReversePositivePayPortletAction_loadViewDetailsMode.action" URLEncrypt="true"/>

<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="SearchImage" scope="session"/>
<ffi:setProperty name="SearchImage" property="Init" value="true"/>
<ffi:process name="SearchImage"/>

<%
	session.setAttribute("FFISearchImage", session.getAttribute("SearchImage"));
%>

<div id="reversePositivePayPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>
<div id="reversePositivePayPortletNoDataDiv" style="text-align:center;"  class="marginBottom5">
<br><br><br><br><br>
<s:text name="jsp.default.no.pending.rppay.exceptions" />
</div>
<div id="reversePositivePayPortletDataDiv">
	<s:action name="ReversePositivePayPortletAction_loadViewDetailsMode" namespace="/pages/jsp/home" executeResult="true">
	 	<s:param name="portletId" value="%{#portletId}" />
	 </s:action>
</div>