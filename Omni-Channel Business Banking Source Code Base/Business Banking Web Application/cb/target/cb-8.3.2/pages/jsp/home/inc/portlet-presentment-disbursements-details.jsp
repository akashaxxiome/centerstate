<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript">
var prefix = ' ';
<ffi:cinclude value1="en_US" value2="${UserLocale.Locale}" operator="equals" >
	prefix = ' for ';
</ffi:cinclude>
</script>
<ffi:help id="home_portlet-presentment-disbursements"/>
<ffi:cinclude value1="${DisbursementPresentmentSettings.size}" value2="" operator="equals">
	<div align="center"><s:text name="jsp.default_552"/></div>
</ffi:cinclude>
<ffi:cinclude value1="${DisbursementPresentmentSettings.size}" value2="" operator="notEquals">
	<ffi:cinclude value1="${DisbursementPresentmentSettings.size}" value2="0" operator="equals">
		<div align="center"><s:text name="jsp.default_552"/></div>
	</ffi:cinclude>   
	<ffi:cinclude value1="${DisbursementPresentmentSettings.size}" value2="0" operator="notEquals">
	<%--URL Encryption Edit By Dongning --%>
		<script type="text/javascript">
			ns.home.initPresentDisPortlet('<s:property value="portletId"/>', 
									'<s:property value="portletId"/>_presentDis_datagrid',
									'<ffi:urlEncrypt url="/cb/pages/jsp/home/PresentmentDisbursementsPortletAction_updateColumnSettings.action?portletId=${portletId}"/>',
									'<s:property value="portletId"/>_grid_load_complete',
									'<s:property value="columnPerm"/>',
									'<s:property value="hiddenColumnNames"/>');
		</script>
		<%-- URL Encryption Edit By Dongning --%>
		<ffi:setProperty name="presentDisPortletTempURL" value="/pages/jsp/home/PresentmentDisbursementsPortletAction_loadPresentDisData.action?portletId=${portletId}" URLEncrypt="true"/>
		<s:url id="presentDis_data_action" value="%{#session.presentDisPortletTempURL}"/>
		<sjg:grid id="%{portletId}_presentDis_datagrid"
						sortable="true"  
						dataType="json" 
						href="%{presentDis_data_action}" 
						gridModel="gridModel"
						pager="true" 
						rowList="%{#session.StdGridRowList}" 
		 				rowNum="5"
		 				viewrecords="true"
		 				rownumbers="false"
	     				navigator="true"
		 				navigatorAdd="false"
		 				navigatorDelete="false"
						navigatorEdit="false"
						navigatorRefresh="false"
						navigatorSearch="false"
						navigatorView="false"
						shrinkToFit="true"					
						scroll="false"
						scrollrows="true"									 
						hoverrows="true"
						sortname="PRESENTMENT"
						onGridCompleteTopics="%{portletId}_grid_load_complete">         
				<sjg:gridColumn name="presentment" index="PRESENTMENT" title="%{getText('jsp.default_328')}" width="120" align="left" sortable="true" hidedlg="true"/>
				<sjg:gridColumn name="numItemsPending" index="NUMBERITEMSPENDING" title="%{getText('jsp.home_109')}" width="150" align="right" sortable="true" formatter="ns.home.ControlledDisbursementsSummaryFormatter"/>
				<sjg:gridColumn name="totalDebits.currencyCode" title="%{getText('jsp.default_125')}" width="70" align="right" sortable="false"/>
				<sjg:gridColumn name="totalDebits.currencyStringNoSymbol" index="TOTALDEBITS" title="%{getText('jsp.default_43')}" width="80" align="right" sortable="true"/>
		</sjg:grid>
		
		<div id="<s:property value="portletId"/>_presentDis_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('cashMgmt_disburs');"><s:text name="jsp.default.viewAllRecord" /></div>
		
		<script>
		if($("#<s:property value='portletId'/>_presentDis_datagrid tbody").data("ui-jqgrid") != undefined){
			$("#<s:property value='portletId'/>_presentDis_datagrid tbody").sortable("destroy");
		}
		</script>
		
	</ffi:cinclude>
</ffi:cinclude>