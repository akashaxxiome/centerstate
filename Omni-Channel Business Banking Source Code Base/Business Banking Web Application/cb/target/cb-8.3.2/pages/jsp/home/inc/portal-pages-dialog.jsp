<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<ffi:help id="home_portal-pages-dialog" />
<fieldset class="ui-widget-content ui-corner-all">
	<legend style="font-size:13px;"><s:text name="jsp.home_161"/></legend>
	<table border="0" class="tableData">
		<tr>
			<td colspan="2" align="left">
				<div id="deletePortalPageError" style="padding: 0pt 0.7em;text-align:left;display:none" class="ui-state-error ui-corner-all ui-widget ui-widget-content">
					<span class="ui-icon ui-icon-alert" style="float:left"></span>
					<span style="float:left" id="deletePortalPageErrorMsg"></span>
				</div>
			</td>
		</tr>
		<tr>
			<td><s:text name="jsp.home_68"/></td>
			<td><ffi:getProperty name="PortalPageName" /></td>
		</tr>
		<tr>
			<td><s:text name="jsp.home_162"/></td>
			<td>
				<select id="portalPagesSelect">
				<ffi:list items="pageName" collection="PortlaPageNames">
					<ffi:cinclude value1="${pageName}" value2="${PortalPageName}" operator="equals">
						<option value="<ffi:getProperty name="pageName"/>" selected="true">
							<ffi:getProperty name="pageName" />
						</option>
					</ffi:cinclude>
					<ffi:cinclude value1="${pageName}" value2="${PortalPageName}" operator="notEquals">
						<option value="<ffi:getProperty name="pageName"/>">
							<ffi:getProperty name="pageName" />
						</option>
					</ffi:cinclude>
				</ffi:list>
				</select>
			</td>
		</tr>
	</table>
</fieldset>