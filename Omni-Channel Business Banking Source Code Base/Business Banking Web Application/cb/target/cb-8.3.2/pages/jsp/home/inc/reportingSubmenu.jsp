<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%
	Map menuShortcuts = (Map)request.getAttribute("menuShortcuts");
%>
<div class="submenuHolderBgCls">
			<div class="formActionItemPlaceholder">
				<div class="cardHolder">
					<div class="subMenuCard">
						<span class="cardTitle">_______________</span>
						<div class="cardSummary">
							<span>__________________________<br>__________________________<br>________</span>
						</div>
						<div class="cardLinks">
							<span>___________</span>
							<span>___________</span>
						</div> 
					</div>
				</div>	
			</div> 
			<ul id="tab17" class="mainSubmenuItemsCls">
				<ffi:cinclude value1="${displayReportAcctMgmt}" value2="true" operator="equals">
					<li id="menu_reporting_acctmgt" menuId="reporting_acctmgt">
					<ffi:help id="reporting_acctmgt" className="moduleHelpClass"/>
					<s:url id="ajax" value="/pages/jsp/reports/index.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
						<s:param name="subpage" value="%{'acctmgmt'}"/>
					</s:url>
					<sj:a
							href="%{ajax}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_21"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_reporting_acctmgt')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_acctmgt','accounts','infoReport');"><s:text name="jsp.reports.information.reporting" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.InformationReporting"/> 
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
								
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_acctmgt','accounts','bankReport');"><s:text name="jsp.reports.bank.reporting" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.BankReporting"/>  
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</div>
							
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_acctmgt,infoReport');"  ><s:text name="jsp.reports.information.reporting" /></span><br/> --%>
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_acctmgt','accounts','infoReport');"><s:text name="jsp.reports.information.reporting" /></span><br/> --%>
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_acctmgt,bankReport');"><s:text name="jsp.reports.bank.reporting" /></span> --%>
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_acctmgt','accounts','bankReport');"><s:text name="jsp.reports.bank.reporting" /></span> --%>
						</div> 
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-report_accounts"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.default_355')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_21')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "reporting_acctmgt");
					%> 
				</ffi:cinclude>
				<ffi:cinclude value1="${displayReportCashMgmt}" value2="true" operator="equals">
					<li id="menu_reporting_cashmgt" menuId="reporting_cashmgt">
					<ffi:help id="reporting_cashmgt" className="moduleHelpClass"/>
					<s:url id="ajax" value="/pages/jsp/reports/index.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
						<s:param name="subpage" value="%{'cashmgmt'}"/>
					</s:url>
					<sj:a
							href="%{ajax}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_50"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_reporting_cashmgt')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_cashmgt','cashFlow','lockBoxContent');"  ><s:text name="jsp.default_268" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.LockBoxReporting"/>  
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
								<ffi:cinclude value1="${CashReportSubMenuPPay}" value2="TRUE" operator="equals">
									<div class="subMenuCard">
										<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_cashmgt','cashFlow','pPayContent');"><s:text name="jsp.default_327" /></span>
										<div class="cardSummary">
											<span><s:text name="jsp.default.PositivePayReporting"/>   
											</span>
										</div>
										<div class="cardLinks borderNone"></div>
									</div>
								</ffi:cinclude>
								<ffi:cinclude value1="${CashReportSubMenuRPPay}" value2="TRUE" operator="equals">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_cashmgt','cashFlow','rpPayContent');"><s:text name="jsp.home_263" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ReversePositivePayReporting"/> 
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
								</ffi:cinclude>
							</div>	
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_cashmgt,cashFlowContent');"><s:text name="jsp.default_87" /></span> --%>
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_cashmgt,lockBoxContent');"  ><s:text name="jsp.default_268" /></span><br/> --%>
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_cashmgt','cashFlow','lockBoxContent');"  ><s:text name="jsp.default_268" /></span><br/> --%>
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_cashmgt,pPayContent');"><s:text name="jsp.default_327" /></span> --%>
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_cashmgt','cashFlow','pPayContent');"><s:text name="jsp.default_327" /></span> --%>
						</div> 
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-report_cashflow"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.default_355')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_50')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "reporting_cashmgt");
					%> 
				</ffi:cinclude>
				<ffi:cinclude value1="${displayReportTransPay}" value2="true" operator="equals">
					<li id="menu_reporting_paytran" menuId="reporting_paytran">
					<ffi:help id="reporting_paytran" className="moduleHelpClass"/>
					<s:url id="ajax" value="/pages/jsp/reports/index.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
						<s:param name="subpage" value="%{'pandt'}"/>
					</s:url>
					<sj:a
							href="%{ajax}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home.label.menu.payments"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_reporting_paytran')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder hidden">
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_paytran,transferReport');"><s:text name="jsp.default_443" /></span> --%>
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_paytran,wireReport');"><s:text name="jsp.default_465" /></span>
							<span onclick="ns.shortcut.navigateToSubmenus('reporting_paytran,achReport');"  ><s:text name="jsp.default_22" /></span>
							<span onclick="ns.shortcut.navigateToSubmenus('reporting_paytran,taxReport');"><s:text name="jsp.default_405" /></span>
							<span onclick="ns.shortcut.navigateToSubmenus('reporting_paytran,childSupportReport');"  ><s:text name="jsp.default_97" /></span>
							<span onclick="ns.shortcut.navigateToSubmenus('reporting_paytran,achFileUploadReport');"><s:text name="jsp.reports_481" /></span>
							<span onclick="ns.shortcut.navigateToSubmenus('reporting_paytran,billPayReport');"  ><s:text name="jsp.default_74" /></span>
							<span onclick="ns.shortcut.navigateToSubmenus('reporting_paytran,cashConReport');"><s:text name="jsp.default_85" /></span> --%>
							
							<div class="cardHolder">
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TRANSFERS %>" >
									<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_paytran','payments','transferReport');"><s:text name="jsp.default_443" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Transfer"/>
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</ffi:cinclude>
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES %>" >
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_paytran','payments','wireReport');"><s:text name="jsp.default_465" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.WiresReporting"/> 
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</ffi:cinclude>
							<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACH_BATCH %>" >
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_paytran','payments','achReport');"  ><s:text name="jsp.default_22" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ACHReporting"/>
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</ffi:cinclude>
							<br/>
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.TAX_PAYMENTS %>" >
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_paytran','payments','taxReport');"><s:text name="jsp.default_405" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.TaxReporting"/>
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</ffi:cinclude>
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CHILD_SUPPORT %>" >
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_paytran','payments','childSupportReport');"  ><s:text name="jsp.default_97" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ChildSupportReporting"/>
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</ffi:cinclude>
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.ACH_FILE_UPLOAD %>" >
	            			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FILE_UPLOAD %>" >
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_paytran','payments','achFileUploadReport');"><s:text name="jsp.reports_481" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ACHFileUploadReporting"/> 
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</ffi:cinclude>
							</ffi:cinclude>
							<br/>
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PAYMENTS %>" >
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_paytran','payments','billPayReport');"  ><s:text name="jsp.default_74" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.BillPayReporting"/> 
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</ffi:cinclude>
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CASH_CON_REPORTING %>" >
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_paytran','payments','cashConReport');"><s:text name="jsp.default_85" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.CashConcentrationReporting"/> 
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
							</ffi:cinclude>
							</div>
						</div> 
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-transfer"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.default_355')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_152.1')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "reporting_paytran");
					%> 
				</ffi:cinclude>
				<ffi:cinclude value1="${displayReportAudit}" value2="true" operator="equals">
					<li id="menu_reporting_audit" menuId="reporting_audit">
					<ffi:help id="reporting_audit" className="moduleHelpClass"/>
					<s:url id="ajax" value="/pages/jsp/reports/index.jsp" escapeAmp="false">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
						<s:param name="Initialize" value="true"/>
						<s:param name="subpage" value="%{'audit'}"/>
					</s:url>
					<sj:a
							href="%{ajax}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.default_53"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_reporting_audit')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span>
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_audit','audit','permissionContent');"  ><s:text name="jsp.default_569" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.Permissions"/>  
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_audit','audit','historyContent');"  ><s:text name="jsp.reports_595" /></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.HistoryReporting"/>  
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_audit','audit','changesContent');"><s:text name="jsp.reports_532" /></span>
									<div class="cardSummary">
										<span>This area provides brief information about this feature. Please click on help icon to know more. 
										</span>
									</div>
									<div class="cardLinks borderNone"></div>
								</div>
								</ffi:cinclude>
							</div>	
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_audit,auditContent');"><s:text name="jsp.default_53" /></span> --%>
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_audit,permissionContent');"  ><s:text name="jsp.default_569" /></span><br/> --%>
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_audit','audit','permissionContent');"  ><s:text name="jsp.default_569" /></span><br/> --%>
							<%-- <span onclick="ns.shortcut.navigateToSubmenus('reporting_audit,historyContent');"  ><s:text name="jsp.reports_595" /></span> --%>
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_audit','audit','historyContent');"  ><s:text name="jsp.reports_595" /></span> --%>
							<%-- Start: Dual approval processing --%>
							<%-- <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
							<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{ajax}"/>','reporting_audit','audit','changesContent');"><s:text name="jsp.reports_532" /></span>
							</ffi:cinclude> --%>
							<%-- End: Dual approval processing --%>
						</div> 
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-audit"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.default_355')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.default_53')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "reporting_audit");
					%> 
				</ffi:cinclude>
			</ul>
</div>
