<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:help id="home_portlet-consolidated-balance"/>
<script type="text/javascript">
	function <s:property value="portletId"/>_save() {
		var idSelector = '#<s:property value="portletId"/>';
		
		var type = $(idSelector + '_combobox').val();
		//URL Encryption: Change "$.get" method to "$.post" method
		$.post('<s:url action="ConsolidatedBalancePortletAction_updateSettings.action"/>', {portletId: '<s:property value="portletId"/>',
			displayType:type, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'}, 
			function(data){
			 if(type=='pie'){
				 ns.home.changeConsolidatedBalancePortletView('donut');
         	   }else if(type=='bar'){
         		  ns.home.changeConsolidatedBalancePortletView('bar');
         	   }else if(type=='Table'){
         		  ns.home.changeConsolidatedBalancePortletView('dataTable');
         	   }
			$.publish("common.topics.tabifyNotes");	
			if(accessibility){
				$(idSelector).setFocus();
			}
		});
	}
	
	$('#<s:property value="portletId"/>_combobox').selectmenu({width:'99'});
	
	$('#<s:property value="portletId"/>_anchor').button({
		icons: {}
	});
</script>

<div>
	<span><b><s:text name="jsp.home_258"/></b></span>
	<select id="<s:property value="portletId"/>_combobox">
		<option value="pie" <ffi:cinclude value1="${ConsolidatedBalancesSummarySettings.displayType}" value2="pie" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_257"/></option>
		<option value="bar" <ffi:cinclude value1="${ConsolidatedBalancesSummarySettings.displayType}" value2="bar" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_43"/></option>
		<option value="Table" <ffi:cinclude value1="${ConsolidatedBalancesSummarySettings.displayType}" value2="Table" operator="equals">selected</ffi:cinclude>><s:text name="jsp.home_72"/></option>
	</select></br></br>
	
	<span><a id="<s:property value="portletId"/>_anchor" href="javascript:void(0)" onclick="<s:property value="portletId"/>_save()" ><s:text name="jsp.default_303"/></a></span>
</div>