<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<sj:dialog id="editHomeTransferDialogID" title="%{getText('jsp.transfers_38')}"
modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="900"
minHeight="350" maxHeight="520" onCloseTopics="closeEditPendingTransferDialog,clearDialogContent">
</sj:dialog>

<script type="text/javascript">
$(document).ready(function(){
	
	$("#editHomeTransferDialogID").dialog({
		   beforeClose: function(event, ui) { 
			   	return ns.pendingTransactionsPortal.checkExceptionBeforeClose(event, ui);
		   	}
	});
	
});
</script>
