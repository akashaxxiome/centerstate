<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<sj:dialog id="quickViewAlertDialogID" title="%{getText('jsp.alert_66')}" resizable="false" modal="true" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="500">
	<div id="quickViewAlertDlgBand1"></div>
	<div id="quickViewAlertDlgBand2"></div>
</sj:dialog>