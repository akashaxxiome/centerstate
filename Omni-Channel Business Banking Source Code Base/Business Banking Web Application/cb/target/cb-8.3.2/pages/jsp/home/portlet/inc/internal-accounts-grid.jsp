<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.accounts.AccountTypes"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummaries"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummary"%>
<%@ page import="com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.accounts.AssetAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.DepositAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.LoanAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.CreditCardAcctSummary"%>
<%@ page import="com.ffusion.beans.common.Currency"%>
<%@ page import="java.util.Collection"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
 <%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.HISTORY%>" >
	<ffi:setProperty name="isEntitledForHistory" value="true"/>
</ffi:cinclude>
<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.HISTORY%>" >
	<ffi:setProperty name="isEntitledForHistory" value="false"/>
</ffi:cinclude>

<script type="text/javascript">
	ns.home.initInternalAccountsGrid('<s:property value="portletId"/>_internalAccountsSummaryID', 'onInternalAccountsGridLoadComplete');
</script>

<ffi:setGridURL grid="GRID_ACCOUNT_EXPORT" name="ExportURL" url="/cb/pages/jsp/account/inc/account-history-export-common.jsp?TransactionSearch=false&SetAccountExportID={0}" parm0="ID"/>

<ffi:setProperty name="tempURL" value="/pages/jsp/home/AccountsSummaryPortletAction_loadInternalAccountsSummary.action?GridURLs=GRID_ACCOUNT_EXPORT" URLEncrypt="true"/>

<s:url id="internalAccountsSummaryUrl" value="%{#session.tempURL}"  escapeAmp="false"/>
		<sjg:grid  
			id="%{portletId}_internalAccountsSummaryID"  
			caption=""  
			dataType="json"  
			href="%{internalAccountsSummaryUrl}"  
			pager="true" 
			gridModel="mapListGridModel" 
			rowList="10,15,20" 
			rowNum="5" 
			shrinkToFit="true" 
			sortable="true" 
			scroll="false" 
			scrollrows="true" 
			viewrecords="true" 
			userDataOnFooter="false"
			onGridCompleteTopics="onInternalAccountsGridLoadComplete"
			> 

			<sjg:gridColumn name="consumerDisplayText" index="NICKNAME" title="%{getText('jsp.home.column.label.account')}" sortable="true" />
			<sjg:gridColumn name="bankName" index="BANKNAME" title="%{getText('jsp.home.column.label.bank')}" sortable="true" hidden="true" hidedlg="true" />
			<sjg:gridColumn name="type" index="TYPESTRING" title="%{getText('jsp.home.column.label.account.type')}" sortable="true" hidden="true" hidedlg="true" /> 
			<sjg:gridColumn name="currencyCode" index="CURRENCY_CODE" title="%{getText('jsp.home.column.label.currency')}" sortable="true" /> 
			<sjg:gridColumn name="displayClosingBalance" index="CLOSINGBALANCE" title="%{getText('jsp.home.column.label.prior.balance')}" sortable="true" hidden="true" hidedlg="true" /> 
			<sjg:gridColumn name="displayCurrentBalance" index="CurrentBalanceField" title="%{getText('jsp.home.column.label.curr.balance')}" sortable="true" formatter ="ns.home.formatCurrentBalance" /> 
			<sjg:gridColumn name="displayAvailableBalance" index="AvailableBalanceField" title="%{getText('jsp.home.column.label.avail.balance')}" sortable="true" hidden="true" hidedlg="true" /> 
			<sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.home.column.label.action')}" 
			formatter="ns.home.formatInternalAccountsActionLinks"
			formatoptions="{isEntitledForHistory : '%{#session.isEntitledForHistory}'}"
			sortable="false" hidden="true" hidedlg="true" cssClass="__gridActionColumn" />

			<sjg:gridColumn name="ExportURL" index="ExportURL" title="ExportURL" sortable="true" hidden="true" hidedlg="true"/>

			</sjg:grid> 
			<div id="<s:property value="portletId"/>_internalAccountsSummaryID_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('userPreferencesVirtualMenu,accountPreferencesID');"><s:text name="jsp.default.viewAllRecord" /></div>
		<script>
		if($("#<s:property value='portletId'/>_internalAccountsSummaryID tbody").data("ui-jqgrid") != undefined){
			$("#<s:property value='portletId'/>_internalAccountsSummaryID tbody").sortable("destroy");
		}
		</script>

		