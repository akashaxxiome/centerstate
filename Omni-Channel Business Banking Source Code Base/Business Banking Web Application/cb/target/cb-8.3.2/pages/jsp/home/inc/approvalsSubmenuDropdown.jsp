<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%-- selected module drop down items holder --%>

	<ul class="moduleSubmenuDropdownHolder">
		<ffi:cinclude value1="${show_approvalpending_menu}" value2="true" operator="equals">
			<li onclick="ns.shortcut.goToMenu('approvals_pending');">
				<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-approvalPending"></span>
	    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.approvals_12"/></span>
			</li>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.APPROVALS_ADMIN %>" >
			<li onclick="ns.shortcut.goToMenu('approvals_workflow');">
				<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-approvalWorkflow"></span>
	    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.approvals_24"/></span>
			</li>
		</ffi:cinclude>
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>" >
			<li onclick="ns.shortcut.goToMenu('approvals_group');">
				<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-approvalGroups"></span>
	    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.approvals_23"/></span>
			</li>
		</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
			<li onclick="ns.shortcut.goToMenu('approvals_group');">
				<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-approvalGroups"></span>
	    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.approvals_23"/></span>
			</li>
		</ffi:cinclude>
		
		
	</ul>