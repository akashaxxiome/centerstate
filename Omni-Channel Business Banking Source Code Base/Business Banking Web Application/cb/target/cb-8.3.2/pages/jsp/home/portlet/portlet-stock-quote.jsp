<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<div id="<s:property value="portletId"/>_stockSymbol"><s:property value="symbol"/></div>
<div id="<s:property value="portletId"/>_stockSymbolHiddenDiv" style="display: none;"><ffi:getProperty name="StockLookup" property="Symbol"/></div>
<%-- ================ MAIN CONTENT START ================ --%>
<ffi:help id="home_portlet-stock-quote"/>
<table border="0" cellspacing="0" cellpadding="0" wdith="100%" align="center">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td style="font-weight:bold;text-align: center">
			<ffi:getProperty name="StockLookup" property="CompanyName"/> (<ffi:getProperty name="StockLookup" property="Symbol"/>)
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="433px" border="0" cellspacing="0" cellpadding="0" align="center" class="ui-widget-content ui-corner-all">
				<tr>
					<td class="sectionsubhead ui-widget-header ui-corner-all" colspan="6"><ffi:getProperty name="StockLookup" property="CompanyName"/> (<ffi:getProperty name="StockLookup" property="Symbol"/>)</td>
				</tr>
				<tr>
					<td class="ltrow" height="16" width="100px"><s:text name="jsp.home_209"/></td>
					<td class="ltrow" align="center" width="80px"><ffi:getProperty name="StockLookup" property="LastSaleTime"/></td>
					<td class="ltrow" width="10px"></td>
					<td class="ltrow" width="3px"></td>
					<td class="ltrow"><s:text name="jsp.home_145"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="OpenPrice"/></td>
				</tr>
				<tr>
					<td class="dkrow" height="16"><s:text name="jsp.home_110"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="LastSalePrice"/></td>
					<td class="dkrow"></td>
					<td class="dkrow"></td>
					<td class="dkrow"><s:text name="jsp.home_74"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="HighPrice"/></td>
				</tr>
				<tr>
					<td class="ltrow" height="16"><s:text name="jsp.home_52"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="ChangePrice"/></td>
					<td class="ltrow"></td>
					<td class="ltrow"></td>
					<td class="ltrow"><s:text name="jsp.home_75"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="LowPrice"/></td>
				</tr>
				<tr>
					<td class="dkrow" height="16"><s:text name="jsp.home_2"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="ChangePercent"/></td>
					<td class="dkrow"></td>
					<td class="dkrow"></td>
					<td class="dkrow"><s:text name="jsp.home_44"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="BidPrice"/></td>
				</tr>
				<tr>
					<td class="ltrow" height="16"><s:text name="jsp.home_189"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="LastSaleSize"/></td>
					<td class="ltrow"></td>
					<td class="ltrow"></td>
					<td class="ltrow"><s:text name="jsp.home_39"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="AskPrice"/></td>
				</tr>
				<tr>
					<td class="dkrow" height="16"><s:text name="jsp.home_232"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="Volume"/></td>
					<td class="dkrow"></td>
					<td class="dkrow"></td>
					<td class="dkrow"><s:text name="jsp.home_16"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="YearHigh"/></td>
				</tr>
				<tr>
					<td class="ltrow" height="16">&nbsp;</td>
					<td class="ltrow">&nbsp;</td>
					<td class="ltrow"></td>
					<td class="ltrow"></td>
					<td class="ltrow" height="16"><s:text name="jsp.home_17"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="YearLow"/></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>
			<table width="433px" border="0" cellspacing="0" cellpadding="0" align="center" class="ui-widget-content ui-corner-all">
				<tr>
					<td class="sectionsubhead ui-widget-header ui-corner-all" colspan="6"><s:text name="jsp.home_95"/></td>
				</tr>
				<tr>
					<td class="ltrow" height="16" width="100px"><s:text name="jsp.home_231"/></td>
					<td class="ltrow" align="center" width="80px"><ffi:getProperty name="StockLookup" property="Volatility"/></td>
					<td class="ltrow" width="10px"></td>
					<td class="ltrow" width="3px"></td>
					<td class="ltrow"><s:text name="jsp.home_82"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="DividendFrequency"/></td>
				</tr>
				<tr>
					<td class="dkrow" height="16"><s:text name="jsp.home_147"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="PeRatio"/></td>
					<td class="dkrow"></td>
					<td class="dkrow"></td>
					<td class="dkrow"><s:text name="jsp.home_86"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="ExDividendDate"/></td>
				</tr>
				<tr>
					<td class="ltrow" height="16"><s:text name="jsp.home_85"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="EarningsPerShare"/></td>
					<td class="ltrow"></td>
					<td class="ltrow"></td>
					<td class="ltrow"><s:text name="jsp.home_120"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="MarketCap"/></td>
				</tr>
				<tr>
					<td class="dkrow" height="16"><s:text name="jsp.home_81"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="Dividend"/></td>
					<td class="dkrow"></td>
					<td class="dkrow"></td>
					<td class="dkrow"><s:text name="jsp.home_186"/></td>
					<td class="dkrow" align="center"><ffi:getProperty name="StockLookup" property="SharesOutstanding"/></td>
				</tr>
				<tr>
					<td class="ltrow" height="16"><s:text name="jsp.home_237"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="Yield"/></td>
					<td class="ltrow"></td>
					<td class="ltrow"></td>
					<td class="ltrow"><s:text name="jsp.home_87"/></td>
					<td class="ltrow" align="center"><ffi:getProperty name="StockLookup" property="ItemExchangeCode"/></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%-- ================= MAIN CONTENT END ================= --%>

<div id="stockPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick='<s:property value="portletId"/>_switchToEdit()'><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>