<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<style>
	#<s:property value="portletId"/> .ui-jqgrid-ftable td{
		border:none;
	}
</style>

<script type="text/javascript">
	//remove column chooser in edit mode
	$('#<s:property value="portletId"/>').portlet('bindCtrlEvent', '.edit', 'click', function(){
		ns.home.removeColumnChooser('<s:property value="portletId"/>');
	});

</script>
<ffi:help id="home_portlet-consolidated-balance"/>

<div id="ConsolidatedBalancePortletContainer" class="marginBottom5">

<input type="hidden" value="<s:property value="%{#session.ConsolidatedBalancesSummarySettings.displayType}"/>" id="selectedTab">
<div id="conBalanceTableContainer">

			<h3 id="0" onclick="loadAccountGroupData($(this));"></h3>
			<div id="content0">
			<s:include value="/pages/jsp/home/inc/portlet-account-deposit.jsp"/>
			</div>
			
			<h3 id="1" onclick="loadAccountGroupData($(this));"></h3>
			<div id="content1">
			<s:include value="/pages/jsp/home/inc/portlet-account-asset.jsp"/>
			</div>
			
			<h3 id="2" onclick="loadAccountGroupData($(this));"></h3>
			<div id="content2">
			<s:include value="/pages/jsp/home/inc/portlet-account-loan.jsp"/>
			</div>
			
			<h3 id="3" onclick="loadAccountGroupData($(this));"></h3>
			<div id="content3">
			<s:include value="/pages/jsp/home/inc/portlet-account-ccard.jsp"/>
			</div>
</div>
<s:hidden value="%{portletId}" id="portletIdconsolidated"></s:hidden>
<div id="conBalanceDonutChart" class="hidden" style="text-align: center;"></div>
<div id="conBalanceBarChart" class="hidden" style="text-align: center;"></div>
<div id="conBalanceSettings" class="hidden" style="margin:5px;"></div>  


</div>

<script type="text/javascript">
	ns.home.initConBalPortlet('<s:property value="portletId"/>',
						'<s:property value="portletId"/>_conbal_datagrid',
						'<ffi:urlEncrypt url="/cb/pages/jsp/home/ConsolidatedBalancePortletAction_updateColumnSettings.action?portletId=${portletId}"/>',
						'<s:property value="portletId"/>_grid_load_complete',
						'<s:property value="columnPerm"/>',
						'<s:property value="hiddenColumnNames"/>');
	if($("#<s:property value='portletId'/>_conbal_datagrid tbody").data("ui-jqgrid") != undefined){
		$("#<s:property value='portletId'/>_conbal_datagrid tbody").sortable("destroy");
	}

	function loadAccountGroupData(header){
		var contentLoaded = $(header).attr('contentLoaded');
		//Bypass the grid reload call if the Grid already has data
		if(contentLoaded != 'true'){
			var gridElement= $(header).next().find(".ui-jqgrid");
			gridId = $(gridElement).attr("id").substr(5);
			ns.common.reloadFirstGridPage('#'+gridId);
			ns.common.resizeWidthOfGrids();
			$(header).attr('contentLoaded','true');//Set the content as loaded, so as to avoid loading next time.
		}
	}

	$(document).ready(function(){
		$("#conBalanceTableContainer").accordion({
	        heightStyle: "content",
	        create: function( event, ui ) {
	        	ns.common.resizeWidthOfGrids();
	        },
	        activate: function( event, ui ) {
	        	ns.common.resizeWidthOfGrids();
	        }
		});
		
		// array which holds the divs which will show the charts
		var tabTypes = ["conBalanceDonutChart","conBalanceTableContainer","conBalanceBarChart","conBalanceSettings"];
		var selectedTab=$('#selectedTab').val().trim();
		if(selectedTab=='Table'){
			$('#consolidatedTable').addClass('selectedChart');
			renderConsolidateBalanceAccordion();
		}else if(selectedTab=='bar'){
			$('#consolidatedBar').addClass('selectedChart');
			renderConsolidateBalanceBarChart();
			displayChart("conBalanceBarChart");;
		}else if(selectedTab=='pie'){
			$('#consolidatedDonut').addClass('selectedChart');
			renderConsolidateBalanceDonutChart();
			displayChart("conBalanceDonutChart");;
		}

		//Reset the hash which will reload the charts view
		window.location.hash = "";

		
		function displayChart(chartType){
			for(var i=0;i<tabTypes.length;i++){
				if(tabTypes[i]== chartType){
					$("#"+tabTypes[i]).show();
				}else{
					$("#"+tabTypes[i]).hide();
				}
			}
		}
		
		ns.home.changeConsolidatedBalancePortletView = function(dataType) {
			if(dataType =='donut'){
				renderConsolidateBalanceDonutChart();
				displayChart("conBalanceDonutChart");;
			}else if(dataType =='bar'){
				renderConsolidateBalanceBarChart();
				displayChart("conBalanceBarChart");;
			}else if(dataType =='dataTable'){
				displayChart("conBalanceTableContainer");
				renderConsolidateBalanceAccordion();
			}else if(dataType =='chartSettings'){
				 displayChart("conBalanceSettings");
				 renderConsolidateBalanceSettings();
			}
		};
	});

	function renderConsolidateBalanceAccordion(){
		if($("#conBalanceTableContainer #accordianHolder").length == 0){//Load Account Group Summary balances only once
			getAccountGroupSummaryBalances();
		}
		
		$("#conBalanceTableContainer").accordion( "option", "active", 0);//Set first panel active
		loadAccountGroupData($("#conBalanceTableContainer #0.ui-accordion-header"));//Get the data for first accordion
	}

	function getAccountGroupSummaryBalances(){
		var portletId = $('#portletIdconsolidated').val();  
		var aConfig = {
			url : "/cb/pages/jsp/home/ConsolidatedBalancePortletAction_loadConBalData.action?portletId="+portletId,
			containerId: "ConsolidatedBalancePortletContainer",
			data: {CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
			async : false,
			success: function(data) {
				if(data!=null && data.gridModel!=null){
					var userObject=data.userdata; 
					for(var i=0;i<data.gridModel.length;i++){
						var object = data.gridModel[i];
						var bal = object.balance;
						if(object.balanceAmountValue !="" && object.balanceAmountValue != null && object.balanceAmountValue != undefined && parseFloat(object.balanceAmountValue)<0){
							bal = "<font color='red'>"+object.balance+"</font>";
						}
						$("#conBalanceTableContainer h3#"+i).html('<span id="accordianHolder" style="float:left;">' + object.accountsType + '</span><span class="floatRight marginRight10"> '+bal+"  "+userObject.currencyType+" </span>");
					}
				}
			}
		};

		//Portlet calls should be non blocking local ajax
		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}

	function loadAccountDetails(accountGroupID,portletId,i){
		var aConfig = {
			url: "/cb/pages/jsp/home/ConsolidatedBalancePortletAction_loadGroupAccounts.action",
			type:"POST",
			async : false,
			containerId: portletId,
			data: {portletId:portletId,groupID: accountGroupID, queryDataClass: 'P',CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
			success: function(data){
				$("#conBalanceTableContainer div#content"+i).html(data);
		   	}
		};

		//Portlet calls should be non blocking local ajax
		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}

	function renderConsolidateBalanceSettings(){
		// below step is done to remove the last loaded from thru sapui5 so that new view is loaded if anything changes through setting
		window.location.hash='';

		var aConfig = {
			url : "/cb/pages/jsp/home/ConsolidatedBalancePortletAction_loadEditMode.action",
			containerId :'<s:property value="%{portletId}"/>',
			data: {CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>',
				portletId:'<s:property value="%{portletId}"/>'},
			success: function(data) {
				$('#conBalanceSettings').html("");
				$('#conBalanceSettings').html(data);
			}
		}

		//Portlet calls should be non blocking local ajax
		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}

	function renderConsolidateBalanceDonutChart() {
		$.publish("sap-ui-renderChartsTopic",{
			chartRoute: "_consolidatedBalanceDonutChart",
			beforeNavigate : function(){
				// Place SAP UI5 layout element for each chart
				var donutChartLayout = sap.ui.getCore().byId("conBalanceUI5DonutContainer");
				if(!donutChartLayout){
					donutChartLayout = new sap.ui.layout.VerticalLayout("conBalanceUI5DonutContainer", {
						content : [],
					});
				}
				donutChartLayout.placeAt($("#conBalanceDonutChart")[0])
			}});
	}

	updateAmounts = function(data) {
		for(var i=0;i < data.length; i++) {
			var model = data[i];
			if(model.balance){
				var amountNoComma = model.balance.replace(/,/g, "");
				//var amountNoDot = amountNoComma.slice(0,amountNoComma.length-3);
				data[i].amountString = amountNoComma;
			}else{
				data[i].amountString = 0;
			}
		}
		return data;
	};

	function renderConsolidateBalanceBarChart() {
		$(document).publish("sap-ui-renderChartsTopic",{
			chartRoute: "_consolidatedBalanceBarChart",
			beforeNavigate : function(){
				var columnChartLayout = sap.ui.getCore().byId("conBalanceUI5ColContainer");
				// Place SAP UI5 layout element for each chart
				if(!columnChartLayout){
					columnChartLayout = new sap.ui.layout.VerticalLayout("conBalanceUI5ColContainer", {
						content :[]						
					});
				}
				columnChartLayout.placeAt($("#conBalanceBarChart")[0]);
			}});
	}

</script>