<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript">
<ffi:cinclude value1="en_US" value2="${UserLocale.Locale}" operator="equals" >
	ns.home.setHeaderDate('<s:property value="portletId"/>', '<s:url value="/pages/jsp/home/inc/portlet-header-date.jsp"/>', ' for ');
</ffi:cinclude>
<ffi:cinclude value1="en_US" value2="${UserLocale.Locale}" operator="notEqual" >
	ns.home.setHeaderDate('<s:property value="portletId"/>', '<s:url value="/pages/jsp/home/inc/portlet-header-date.jsp"/>', ' ');
</ffi:cinclude>
</script>
<ffi:help id="home_portlet-lockbox"/>
<ffi:cinclude value1="" value2="${LockboxSummarySettings}" operator="equals">
	<br><br><br><br><br><div height="20" align="center"><s:text name="jsp.default_551"/></div>
</ffi:cinclude>
<ffi:cinclude value1="" value2="${LockboxSummarySettings}" operator="notEquals">
	<ffi:cinclude value1="0" value2="${LockboxSummarySettings.size}" operator="equals">
		<br><br><br><br><br><div height="20" align="center"><s:text name="jsp.default_551"/></div>
	</ffi:cinclude>
	<ffi:cinclude value1="${LockboxSummarySettings.size}" value2="0" operator="notEquals">
		<%--URL Encryption Edit By Dongning --%>
		<script type="text/javascript">
			ns.home.initLockboxPortlet('<s:property value="portletId"/>', 
								'<s:property value="portletId"/>_lockbox_datagrid',
								'<ffi:urlEncrypt url="/cb/pages/jsp/home/LockboxPortletAction_updateColumnSettings.action?portletId=${portletId}"/>',
								'<s:property value="portletId"/>_grid_load_complete',
								'<s:property value="columnPerm"/>',
								'<s:property value="hiddenColumnNames"/>');
		</script>
		<%-- URL Encryption Edit By Dongning --%>
		<ffi:setProperty name="lockboxPortletTempURL" value="/pages/jsp/home/LockboxPortletAction_loadLockboxData.action?portletId=${portletId}" URLEncrypt="true"/>
		<s:url id="lockbox_data_action" value="%{#session.lockboxPortletTempURL}"/>
		<sjg:grid id="%{portletId}_lockbox_datagrid"
						sortable="true"  
						dataType="json" 
						href="%{lockbox_data_action}" 
						gridModel="gridModel" 
						pager="true" 
						rowList="%{#session.StdGridRowList}" 
		 				rowNum="5"
		 				viewrecords="true"
		 				rownumbers="false"
	     				navigator="true"
		 				navigatorAdd="false"
		 				navigatorDelete="false"
						navigatorEdit="false"
						navigatorRefresh="false"
						navigatorSearch="false"
						navigatorView="false"
						shrinkToFit="true"					
						scroll="false"
						scrollrows="true"			
						hoverrows="true"
						sortname="LOCKBOX_ACCT" 
						onGridCompleteTopics="%{portletId}_grid_load_complete">         
				<sjg:gridColumn name="accountDisplayText" index="LOCKBOX_ACCT" title="%{getText('jsp.default_15')}" width="200" align="left" sortable="true" hidedlg="true" cssClass="datagrid_textColumn"/>
				<sjg:gridColumn name="lockboxAccountNickname" index="NICKNAME" title="%{getText('jsp.default_293')}" width="180" align="left" sortable="true" cssClass="datagrid_textColumn"  formatter="ns.home.lockboxAccountNickNameFormatter" />
				<sjg:gridColumn name="totalLockboxCredits.currencyStringNoSymbol" index="TOTAL_CREDITS" title="%{getText('jsp.default_169')}" width="100" align="right" sortable="true" cssClass="datagrid_amountColumn"/>
				<sjg:gridColumn name="totalNumLockboxCredits" index="NUM_CREDITS" title="%{getText('jsp.default_123')}" width="50" align="right" sortable="true" formatter="ns.home.lockboxCreditsFormatter" cssClass="datagrid_amountColumn"/>
				<sjg:gridColumn name="immediateFloat.currencyStringNoSymbol" index="IMMEDIATE_FLOAT" title="%{getText('jsp.default_234')}" width="120" align="right" sortable="true" cssClass="datagrid_textColumn"/>
				<sjg:gridColumn name="oneDayFloat.currencyStringNoSymbol" index="ONE_DAY_FLOAT" title="%{getText('jsp.default_10')}" width="80" align="right" sortable="true" cssClass="datagrid_textColumn"/>
				<sjg:gridColumn name="twoDayFloat.currencyStringNoSymbol" index="TWO_DAY_FLOAT" title="%{getText('jsp.default_12')}" width="80" align="right" sortable="true" cssClass="datagrid_textColumn"/>
				<sjg:gridColumn name="lockboxAccount.displayText" index="displayText" title="%{getText('jsp.cash_53')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
		</sjg:grid>
		
		<div id="<s:property value="portletId"/>_lockbox_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('cashMgmt_lockbox');"><s:text name="jsp.default.viewAllRecord" /></div>
		
		
		<script>
		if($("#<s:property value='portletId'/>_lockbox_datagrid tbody").data("ui-jqgrid") != undefined){
			$("#<s:property value='portletId'/>_lockbox_datagrid tbody").sortable("destroy");
		}
		</script>
	</ffi:cinclude>
</ffi:cinclude>