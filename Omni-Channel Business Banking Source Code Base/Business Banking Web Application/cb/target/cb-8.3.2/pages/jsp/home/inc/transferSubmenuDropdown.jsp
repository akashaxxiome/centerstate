<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%-- selected module drop down items holder --%>
<ul class="moduleSubmenuDropdownHolder dropdownOptionHolder">
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSFERS%>" >
		<li class="dropdownMenuItem" onclick="ns.shortcut.goToMenu('pmtTran_transfer');" >
			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-transfer"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.transfers.transfers" /></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES %>" >
		<li class="dropdownMenuItem" onclick="ns.shortcut.goToMenu('pmtTran_wire');" >
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-wires"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.default_465" /></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${displayACH}" value2="true" operator="equals">
		<li class="dropdownMenuItem" onclick="ns.shortcut.goToMenu('pmtTran_ach');" >
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-ach"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.default_22" /></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CCD_DED %>"  >
		<li class="dropdownMenuItem" onclick="ns.shortcut.goToMenu('pmtTran_childsp');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-childSupport"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.default_97" /></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CCD_TXP %>" >
		<li class="dropdownMenuItem" onclick="ns.shortcut.goToMenu('pmtTran_tax');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-tax"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.default_405" /></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAYMENTS %>" >
		<li class="dropdownMenuItem" onclick="ns.shortcut.goToMenu('pmtTran_billpay');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-billPay"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.default_74" /></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.STOPS %>" >
		<li class="dropdownMenuItem" onclick="ns.shortcut.goToMenu('pmtTran_stops');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-stops"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.stops_17" /></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${displayCashConcentation}" value2="true" operator="equals">
		<li class="dropdownMenuItem" onclick="ns.shortcut.goToMenu('pmtTran_cashcon');" >
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-cashCon"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_48" /></span>
		</li>
	</ffi:cinclude>
</ul>
