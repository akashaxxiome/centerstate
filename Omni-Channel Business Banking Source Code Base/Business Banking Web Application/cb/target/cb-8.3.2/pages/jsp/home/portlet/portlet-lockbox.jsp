<%@page import="com.ffusion.tasks.banking.GetPagedTransactions"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="${LockboxSummariesForPortlet}" value2="" operator="equals">
		<ffi:object id="LockboxSummariesForPortlet" name="com.ffusion.tasks.lockbox.LockboxSummariesTask" scope="session"/>
</ffi:cinclude>
<ffi:setProperty name="LockboxSummariesForPortlet" property="StartDate" value=""/>
<ffi:setProperty name="LockboxSummariesForPortlet" property="EndDate" value=""/>
<ffi:setProperty name="LockboxSummariesForPortlet" property="DataClassification" value="<%=GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY %>"/>

<%
	session.setAttribute("FFILockboxSummariesForPortlet", session.getAttribute("LockboxSummariesForPortlet"));
%>


<ffi:setProperty name="populateLockBoxPortletDataURL" value="/cb/pages/jsp/home/LockboxPortletAction_loadViewDetailsMode.action" URLEncrypt="true"/>

<div id="lockBoxPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<div id="lockBoxPortletDataDiv" class="marginBottom5">
	<s:action name="LockboxPortletAction_loadViewDetailsMode" namespace="/pages/jsp/home" executeResult="true">
	 	<s:param name="portletId" value="%{#portletId}" />
	 </s:action>
</div>