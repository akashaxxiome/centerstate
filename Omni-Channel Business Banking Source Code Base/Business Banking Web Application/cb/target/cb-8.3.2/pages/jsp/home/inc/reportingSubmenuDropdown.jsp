<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- selected module drop down items holder --%>
<ul class="moduleSubmenuDropdownHolder">
	<ffi:cinclude value1="${displayReportAcctMgmt}" value2="true" operator="equals">
		<li onclick="ns.shortcut.goToMenu('reporting_acctmgt');">
			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-accountHistory"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_21"/></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${displayReportCashMgmt}" value2="true" operator="equals">
		<li onclick="ns.shortcut.goToMenu('reporting_cashmgt');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-cashFlow"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_50"/></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${displayReportTransPay}" value2="true" operator="equals">
		<li onclick="ns.shortcut.goToMenu('reporting_paytran');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-transfer"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_152"/></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${displayReportAudit}" value2="true" operator="equals">
		<li onclick="ns.shortcut.goToMenu('reporting_audit');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-audit"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.default_53"/></span>
		</li>
	</ffi:cinclude>
</ul>	