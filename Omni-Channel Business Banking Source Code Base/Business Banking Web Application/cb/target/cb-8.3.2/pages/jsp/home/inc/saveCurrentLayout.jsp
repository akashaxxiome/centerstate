<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

	<ffi:help id="home_portal-page-form" className="moduleHelpClass"/>
	<div id='savePortalPage' class="layoutSaveDialog" style="overflow:visible;">
		<table>
			<tr style="height:40px">
				<td width="100"><s:text name="jsp.home.currentLayout" />:</td>
				<td><span class="boldIt">&nbsp;<ffi:getProperty name="CURRENT_LAYOUT" property="name"/></span></td>
			</tr>
			<tr style="height:40px">
				<td><s:text name="jsp.home.saveAs" />:</td>
				<td>
					<span class="layoutSaveInputAutoComplete">
						<input name="inputSaveLayout" id="inputSaveLayout" onblur="ns.home.onChangeOfLookup()" onchange="ns.home.onChangeOfLookup()" onclick="ns.home.showCustomLayouts()" placeholder="Select or enter new name..." class="ui-widget-content ui-corner-all txtbox"  maxlength="30" style="width: 250px!important;"  />
					</span>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<span id="inputSaveLayoutError" class="hidden inputSaveLayoutError"><s:text name="jsp.home.layoutNameRequired" /></span>
					<span class="overwriteWarningLbl">
		 				<s:text name="jsp.home.overwritingLayout" />&nbsp;"<span id="duplicateLayoutName"></span>"
					</span>
					<div id="inputSaveLayoutOptHolderDiv"></div>
				</td>
			</tr>
		</table>
			
		<input name="replaceLayoutId" id="replaceLayoutId" type="hidden"/>
		<span class="layoutSetAsDefault" style="display:none;">
			<input type="checkbox" id="makeItDefault" checked="checked"/><s:text name="jsp.home.setAsHomePage"/>		
		</span>
	</div>
	<div align="center" class="ui-widget-header customDialogFooter">
		<sj:a id="cancelSavePortalPage" button="true"
			onClickTopics="cancelSaveLayoutTopic">
			<s:text name="jsp.default_82" />
		</sj:a>
		
		<sj:a id="savePortalPageBtn" 
			button="true"
			onClickTopics="clickSaveLayoutTopic"
			onSuccessTopics="successSaveLayoutTopic">
			<s:text name="jsp.default_366" />
		</sj:a>
	</div>
<script>
	$('#portalPageLayout').addHelp(function(){
		var helpFile = $('#portalPageLayout').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	
	// Adding class to parent div in order to show drop down values overlapping the dialog box boundries
	$("#savePortalPageDialogID").parent().css("overflow", "visible");
	
	$(document).ready(function() {
		
		ns.home.customLayouts = {};
		
		ns.home.getCustomLayouts = function(){
			$.ajax({
				url: '/cb/pages/jsp/home/customLayoutsLookupBox.action',
				data: {term: ''},
				global:false,
				success: function(data){
					if(data.optionsModel){
						$.each(data.optionsModel, function(i, record){
							//ns.home.customLayouts[(record.name).toLowerCase()] = record.id;
							//ns.home.customLayouts[record.id] = record.name;
							ns.home.customLayouts[(record.name).toLowerCase()] = record;
					   	});
					}
				}
			});
		};
		
		//preload the custom layouts for showing duplicate name warning.
		ns.home.getCustomLayouts();
		
		customLayoutSearchFunction = function(request, response){
			$.ajax({
				url: '/cb/pages/jsp/home/customLayoutsLookupBox.action',
				data: {term: request.term},
				global:false,
				success: function(data){
					 response($.map( data.optionsModel, function(item){
							var text = $.trim(item.name);	
						 	var textToReplace = text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "i"), "<strong>$1</strong>");
						 	return {
				 	              	label: textToReplace,
				 	              	id: item.id,
				 	              	name:item.name,
				 	              	appType:item.appType,
				 	              	type:item.type,
				 	              	isdefault:item.isdefault,
							}
			 	        }));
				}
			});
		};
		
		showDeleteCustomLayoutPage = function(layoutId,layoutName){
			$("#deleteLayoutId").val(layoutId);
			$("#deleteLayoutName").html(layoutName);
			$('#saveLayoutBand').slideUp();
			$('#deleteLayoutBand').slideDown();
		};
		
		$("#inputSaveLayoutOptHolderDiv").on("click", "li", function(event) {
			var layoutId = $(this).data("uiAutocompleteItem").id;
			var layoutName = $(this).data("uiAutocompleteItem").name;
			var isOverwrittenLayoutDefault = $(this).data("uiAutocompleteItem").isdefault;
			
			if($(event.target).hasClass("sapUiIconCls icon-delete")){
				showDeleteCustomLayoutPage(layoutId, layoutName);
	     	}else{
	     		$("#inputSaveLayout").val(layoutName);
		    	$("#duplicateLayoutName").html(layoutName);
		    	$(".overwriteWarningLbl").show();
		    	$("#inputSaveLayoutError").hide();
		    	$("#replaceLayoutId").val(layoutId);
		    	if(isOverwrittenLayoutDefault == 'true'){
		    		$("#makeItDefault").prop("checked",true);
		    	}else{
		    		$("#makeItDefault").prop("checked",false);
		    	}
		     	return false;
	     	}
		});
		
		$("#inputSaveLayout").autocomplete({
		    source: customLayoutSearchFunction,
		    appendTo:"#inputSaveLayoutOptHolderDiv",
		    minLength : "0",
		    create: function () {
		        $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
		        	var defaultLayoutActions = '<span class="sapUiIconCls icon-home" style="float:left;"></span>'; 
		        	var customLayoutActions = '';
		        	
		        	if(item.isdefault == 'false'){
		        		defaultLayoutActions = '<span class="paddingRight10" style="float:left;"></span>'; //Do not show Home icon to indicate Default layout, if it is not a default layout.
		        		customLayoutActions = '<div class="actionContainer"><span class="sapUiIconCls icon-delete" ></span></div>';//Show delete icon if layout is not default layout.
		        	}
		        	
		        	var li = '<li>';
		        	$(li).data(item);
		        	return $(li).append('<a class="shortcutHolderAnchor">' + defaultLayoutActions + item.label + customLayoutActions +' </a>')
		            .appendTo(ul);
		        	
		        };
		    },
		    select: function(event, ui){
		    	$("#inputSaveLayout").val(ui.item.name);
		     	return false;
		    },
		    focus: function( event, ui ) {
		    	return false;
		    },
		    open: function() { $('#inputSaveLayoutOptHolderDiv .ui-menu').width(253);
                $('#inputSaveLayoutOptHolderDiv .ui-menu').css("overflow","auto");
            }
		    
		});
		
		ns.home.onChangeOfLookup = function(){
			var duplicateLayoutName = '';
			var replaceLayoutId = '';
			var isDuplicateLayoutName = false;
			var isOverwrittenLayoutDefault = false;
			var layoutName = $("#inputSaveLayout").val().trim();
			
			//Check if user entered layout name is already present in existing layouts
			var searchForDuplicateLayout = ns.home.customLayouts[layoutName.toLowerCase().trim()];
			if(searchForDuplicateLayout != undefined){
				duplicateLayoutName = searchForDuplicateLayout.name;
				replaceLayoutId = searchForDuplicateLayout.id;
				//the layout will be treated as duplicate case in-sensitive
				
				if(duplicateLayoutName != undefined && duplicateLayoutName != '' && duplicateLayoutName.toLowerCase() == layoutName.toLowerCase() && duplicateLayoutName.trim()==layoutName.trim() && duplicateLayoutName.toLowerCase().trim() == layoutName.toLowerCase().trim()){
					layoutName=duplicateLayoutName;
					isDuplicateLayoutName = true;
					if(searchForDuplicateLayout.isdefault == 'true'){
						isOverwrittenLayoutDefault = true;
					}
				}
			}
			
			if(layoutName == ''){
				$("#inputSaveLayout").val('');
				$("#replaceLayoutId").val('');
				$("#duplicateLayoutName").html('');
		    	$(".overwriteWarningLbl").hide();
		    	$("#makeItDefault").prop("checked",false);
			}else if(isDuplicateLayoutName){
				$("#inputSaveLayoutError").hide();
				$("#duplicateLayoutName").html(duplicateLayoutName);//Show original layout name to show duplicate warning, which is case in-sensitive
		    	$(".overwriteWarningLbl").show();
		    	$("#replaceLayoutId").val(replaceLayoutId);
		    	if(isOverwrittenLayoutDefault){
		    		$("#makeItDefault").prop("checked",true);
		    	}else{
		    		$("#makeItDefault").prop("checked",false);
		    	}
			}else{
				$("#inputSaveLayoutError").hide();
				$("#replaceLayoutId").val('');
				$("#duplicateLayoutName").html('');
		    	$(".overwriteWarningLbl").hide();
			}
		};
		
		ns.home.showCustomLayouts = function(shortcutID,shortCutName){
			var userInput = $("#inputSaveLayout").val();
			$("#inputSaveLayout").autocomplete("search", userInput);
		};
		
		$.subscribe('clickSaveLayoutTopic', function(event,data){
			var newLayoutName = $("#inputSaveLayout").val().trim();
			var replaceLayoutId = $("#replaceLayoutId").val();
			
			if(newLayoutName !=''){
				ns.layout.saveLayoutHandler(event,newLayoutName,replaceLayoutId);
				$.publish("closeDialog");
			}else{
				$("#inputSaveLayoutError").show();
			}
		});
	
		$.subscribe('successSaveLayoutTopic', function(event,data){
			
		});
		
		$.subscribe('cancelSaveLayoutTopic', function(event,data){
			$("#inputSaveLayout").val('');
			$("#duplicateLayoutName").html('');
	    	$(".overwriteWarningLbl").hide();
	    	$("#replaceLayoutId").val('');
			$.publish("closeDialog");
		});
		
		$.subscribe('cancelDeleteLayout', function(event,data){
			$("#inputSaveLayout").val("");
			$("#replaceLayoutId").val('');
			$("#duplicateLayoutName").html("");
			$(".overwriteWarningLbl").hide();
			$("#deleteLayoutName").html("");
			$('#saveLayoutBand').slideDown();
			$('#deleteLayoutBand').slideUp();
		});


		$.subscribe('deleteLayoutClick', function(event,data){
			var deleteLayoutURL = "/cb/pages/jsp/home/deleteLayout.action";
			$.ajax({
				url : deleteLayoutURL,
				data: {layoutId: $("#deleteLayoutId").val()},
				global:false,
				success : function(data) {
					$.publish("closeDialog");
					ns.common.showStatus(data.response.data);
					//preload the custom layouts for showing duplicate name warning.
					ns.home.getCustomLayouts();
				}
			});
		});

		$.subscribe('deleteLayoutComplete', function(event,data){
			$.log('deleteLayoutComplete ');
		});

		$.subscribe('errorDeleteLayout', function(event,data){
			$.log('errorDeleteLayout ');
		});

		$.subscribe('successDeleteLayout', function(event,data){
			$.log('successDeleteLayout ');
		});
		
		
	});
</script>