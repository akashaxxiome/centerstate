<!DOCTYPE html>

<%@page import="com.sap.banking.common.constants.BankingConstants"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page
	import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>


<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
	operator="equals">
	<ffi:object id="GetBusinessEmployeeCustom"
		name="com.ffusion.tasks.user.GetBusinessEmployeeCustom" scope="session" />
	<ffi:setProperty name="GetBusinessEmployeeCustom" property="ProfileId"
		value="${SecureUser.ProfileID}" />
	<ffi:process name="GetBusinessEmployeeCustom" />
</ffi:cinclude>

<!-- Check AppType of user -->
<ffi:removeProperty name="IsConsumerUser" />
<ffi:setProperty name="IsConsumerUser" value="false" />
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}"
	value2="<%=EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>"
	operator="equals">
	<ffi:setProperty name="IsConsumerUser" value="true" />
</ffi:cinclude>

<html>
<head>
<!-- <meta http-equiv="X-UA-Compatible" content="IE=8"> -->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<%--Show loading symbol/gif if we refreshed whole page from User-> Preferences -> User Profile / Menu --%>
<s:if test="#session.TabToBeSelected != null">
	<script type="text/javascript">
		//Check if parent Iframe is available or not
		if(window.parent.ifrm){
			window.parent.$('#ifrm').showLoading({
				'addClass': 'loading-indicator-dripcircle'
			});
		}
		</script>
</s:if>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title><s:text name="jsp.home_91" /></title>

<%--
		JS and CSS files Inclusion
		NOTE: 
			-To improve page performance the JS and CSS files have been aggreagated during build, please refer to home-page.css and
			homde-page.js configuration in pom.xml of war. 
			-Any new css or js file inclusion has to be defined in pom.xml
			-The order of files is important. To resolve dependency files are placed in right order in pom.xml

		Files included in login-page.css
			/web/css/ui.selectmenu.css
			/web/css/ui.multiselect.css
			/web/css/home/jquery/layout.css
			/web/css/home/jquery/jquery.themes.css
			/web/css/home/jquery/fg.menu.css
			/web/css/home/home.css
			/web/css/jalert.css
			/web/css/showLoading.css
			/web/css/FF.css
			/web/css/desktop.css
			/web/css/ux.lookupbox.css
			/web/css/ui.daterangepicker.css
			/web/css/ui.daterangepicker.overrides.css
			/web/css/ui.custom.config.css
			/web/css/ui.jqgrid.custom.css
			/web/css/toolbar.css
			/web/css/notifications.css
			/web/css/SAPIconLib.css
			/web/css/toggles.css
			/web/css/toggles-dark.css
			/web/css/topBar.css
			/web/css/customSpriteUI.css
			/web/css/menuBar.css
			/web/css/dashboard.css
			/web/css/message/message.css
			/web/css/approvals/approvals.css
			/web/css/ubuntu-font-family-0.80/ubuntuFontLib.css
			/web/css/miscIconSprite.css
			/web/css/menuIconSprite.css
			/web/css/ui.jqgrid.css
			/web/css/visualize.css
			/web/css/visualize-light.css
	--%>
<link type="text/css" rel="stylesheet" media="all"
	href="<s:url value='/web/css/home-page%{#session.minVersion}.css'/>" />
<link type="text/css" rel="stylesheet" media="all"
	href="<s:url value='/web/css/home/home-page-custom.css'/>" />
<link type="text/css" rel="stylesheet" media="all"
	href="<s:url value='/web/css/cssPatch/Dev_Favor/ui-patch%{#session.minVersion}.css'/>" />

<s:url id="head" value="/web/css" />
<s:include value="include-theme.jsp" />



<!-- MEDIA style, excluding from bundle inclusion-->
<link type="text/css" rel="stylesheet"
	media="only screen and (min-device-width: 768px) and (max-device-width: 1024px)"
	href="<s:url value='/web/css/deviceStyle%{#session.minVersion}.css'/>" />


<script>
		$(document).ready(function(){
		ns.home.StdGridRowNum = "<s:property value='#session.StdGridRowNum' />";

		if (document.createStyleSheet)
		{
		    document.createStyleSheet("/cb/web/css/ie.custom.css");
		}
		});
		


		
		// Initializing I18N plugin should be on document ready as this plugin is conflicting with something and 
		// blank page is shown in Firefox. CR #699922, #703360
		$(document).ready(function(){
			// Need to load properties first because some custom widgets may depend on it, e.g. combobox. 
			// Initialize system language global variable (zh_CN or en_US).
			ns.home.locale = '<ffi:getProperty name="UserLocale" property="Locale"/>';
	
			// load i18n properties files for js file
			jQuery.i18n.properties({
				name: 'JavaScript',
				path:'<s:url value="/web/js/i18n/" />',
				mode:'both',
				language: ns.home.locale,
				cache:true,
				callback: function(){
				}
			});
			var windowTitle="";
			jQuery.i18n.properties({
					name: 'MainPageJavaScript',
					path:'<s:url value="/web/js/i18n/" />',
					mode:'both',
					language: ns.home.locale,
					cache:true,
					callback: function(){
					}
				});
			
			windowTitle = js_FFI_label;
			window.parent.document.title = windowTitle;
			
			
			// Now once we loaded all resource bundle messages in javascript variable using I18N plugin,
			// re-assign them to control.
			$.extend($.ui.multiselect, {
				locale: {
					addAll: js_multiSelect_addAll,
					removeAll: js_multiSelect_removeAll,
					itemsCount: js_multiSelect_itemsCount
				}
			});
			
			$.extend($.ui.combobox.prototype, {
				options: {
					hint: js_combobox_hint
				}
			});
			
			
		// Global variable used in js for checking application user type
		ns.home.appUserType = '<ffi:getProperty name="SecureUser" property="AppType"/>';
		ns.home.menuRefreshed = '<ffi:getProperty name="MenuRefreshed"/>';
			
		});
		
		
	</script>

<ffi:cinclude value1="${MenuRefreshed}" value2="true" operator="equals">
	<s:include value="/pages/jsp/home/inc/reloadAccounts.jsp" />
	<%-- Execute logic for menu tab display --%>
	<s:include value="/pages/jsp/inc/init/menu-init.jsp"></s:include>

	<%-- Hide Reporting tab for Consumer and Small Business --%>
	<ffi:cinclude value1="${SecureUser.AppType}"
		value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
		operator="equals">
		<ffi:setProperty name="nav_showtop_reports" value="false" />
	</ffi:cinclude>
	<ffi:cinclude value1="${SecureUser.AppType}"
		value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
		operator="equals">
		<ffi:setProperty name="nav_showtop_reports" value="false" />
	</ffi:cinclude>

	<%-- Get the user's navigation settings --%>
	<ffi:object name="com.ffusion.tasks.user.GetNavigationSettings"
		id="GetNavigationSettings" scope="session" />
	<ffi:setProperty name="GetNavigationSettings"
		property="EntitledMenuTab" value="home" />
	<ffi:cinclude value1="${nav_showtop_account}" value2="true">
		<ffi:setProperty name="GetNavigationSettings"
			property="EntitledMenuTab" value="acctMgmt" />
	</ffi:cinclude>
	<ffi:cinclude value1="${nav_showtop_cash}" value2="true">
		<ffi:setProperty name="GetNavigationSettings"
			property="EntitledMenuTab" value="cashMgmt" />
	</ffi:cinclude>
	<ffi:cinclude value1="${nav_showtop_payments}" value2="true">
		<ffi:setProperty name="GetNavigationSettings"
			property="EntitledMenuTab" value="pmtTran" />
	</ffi:cinclude>
	<ffi:cinclude value1="${nav_showtop_user}" value2="true">
		<ffi:setProperty name="GetNavigationSettings"
			property="EntitledMenuTab" value="admin" />
	</ffi:cinclude>
	<ffi:cinclude value1="${nav_showtop_approvals}" value2="true">
		<ffi:setProperty name="GetNavigationSettings"
			property="EntitledMenuTab" value="approvals" />
	</ffi:cinclude>
	<ffi:cinclude value1="${nav_showtop_reports}" value2="true">
		<ffi:setProperty name="GetNavigationSettings"
			property="EntitledMenuTab" value="reporting" />
	</ffi:cinclude>
	<ffi:setProperty name="GetNavigationSettings"
		property="EntitledMenuTab" value="contactus" />
	<ffi:cinclude value1="${nav_showtop_servicecenter}" value2="true">
		<ffi:setProperty name="GetNavigationSettings"
			property="EntitledMenuTab" value="serviceCenter" />
	</ffi:cinclude>
	<%-- Show preferences menu --%>
	<ffi:setProperty name="GetNavigationSettings"
		property="EntitledMenuTab" value="preferences" />
	<ffi:process name="GetNavigationSettings" />
	<ffi:removeProperty name="MenuRefreshed" />
</ffi:cinclude>

<script>
		// There are few properties which required to be initialized as 
		// these are required by combobox.js and selectmenu.js
		js_multiSelect_addAll='';
		js_multiSelect_removeAll='';
		js_multiSelect_itemsCount='';
		js_combobox_hint='';

	</script>

<%--			
		Files included in home-page.js, to add new file to this bundle include in its corresponding aggregation in pom.xml
			/web/js/namespace.js
			/web/js/jquery.i18n.properties.js
			/web/jquery/home/jquery-migrate-1.2.1.js
			/web/js/home/pendingTransactionPortalGrid.js
			/web/js/home/pendingTransactionPortal.js
			/web/js/toggles.js
			/web/js/common/widgets/ux.uisettings.js
			/web/js/jquery.browser.js
			/web/jquery/home/fg.menu.js
			/web/jquery/home/jquery.themes.js
			/web/jquery/home/layout.latest.js
			/web/js/custom.validation.js
			/web/js/jquery.progressbar.js
			/web/js/jquery.chili-2.2.js
			/web/js/jquery.showLoading.js
			/web/js/common.js
			/web/js/common/jq.overrides.js
			/web/js/common/jq.extensions.js
			/web/js/common/sjq.overrides.js
			/web/js/jquery.ui.portlet.js
			/web/js/jquery.ui.pane.js
			/web/js/jquery.ui.panegroup.js
			/web/js/custom/widget/jquery.jalert.js
			/web/js/combobox.js
			/web/js/common/widgets/ux.lookupbox.js
			/web/js/common/widgets/ux.lookup-grid.js
			/web/js/portal.js
			/web/js/calendar/fullcalendar.js
			/web/js/jquery.editinplace.js
			/web/js/home/shortcuts/shortcut.js
			/web/js/jquery.ui.selectmenu.js
			/web/js/jquery.PrintPlugin.js
			/web/js/ui.multiselect.js
			/web/js/home/home.js
			/web/js/callHelp.js
			/web/js/uiroller.js
			/web/js/tabConfig.js
			/web/js/jquery.tabify.js
			/web/js/common/accessibility.js
			/web/js/applicationController.js
			/web/js/custom/date.js
			/web/js/custom/widget/daterangepicker.jQuery.js
			/web/js/common/widgets/ux.extdaterangepicker.js
			/web/js/home/layout.js
			/web/js/home/layoutcontroller.js
			/web/js/common/widgets/datagrid.js
			/web/js/home/toolbarcontroller.js
			/web/js/jquery.Guid.js
			/web/js/home/portal.js
			/web/js/home/portlets.js
			/web/js/excanvas.js
			/web/js/visualize.jQuery.js
	--%>
<script type="text/javascript"
	src="<s:url value='/web/js/home-page%{#session.minVersion}.js'/>"></script>

<%
	com.ffusion.beans.user.UserLocale userLocale = (com.ffusion.beans.user.UserLocale)session.getAttribute( "UserLocale" );
        com.ffusion.beans.SecureUser user = (com.ffusion.beans.SecureUser)session.getAttribute( "SecureUser" );
        com.ffusion.beans.DateTime date = (com.ffusion.beans.DateTime)user.get( com.ffusion.efs.adapters.profile.constants.ProfileDefines.LAST_ACTIVE_DATE );
  		if(date != null){
                date.setFormat( "EEEEEEEEE, "+ userLocale.getDateTimeFormat() );
                date.setLocale( userLocale.getLocale() );
                session.setAttribute( "lastDate", date.toString() );
        } else {
                session.setAttribute( "lastDate", "" );
        }
  
  		//Set login date to fetch the Event updates at the later time		
  		Calendar loginCalendar = Calendar.getInstance();
  		SimpleDateFormat dateFormatter = new SimpleDateFormat(BankingConstants.DATE_TIME_FORMAT_MMDDYYYY_HHMMSS_SSS);
  		String loginDateStr = dateFormatter.format(loginCalendar.getTime());
%>

<% String tempDisplayText = null; %>

<ffi:object id="GetCurrentDate"
	name="com.ffusion.tasks.util.GetCurrentDate" scope="page" />
<ffi:process name="GetCurrentDate" />
<%-- Format and display the current date/time --%>
<ffi:setProperty name="GetCurrentDate" property="DateFormat"
	value="EEEEEEEEE, ${UserLocale.DateTimeFormat}" />

<ffi:cinclude value1="${lastDate}" value2="" operator="notEquals">
	<ffi:getL10NString rsrcFile="cb" msgKey="jsp/index.jsp-1"
		parm0="${fullName}" parm1="${GetCurrentDate.Date}" parm2="${lastDate}"
		assignTo="tempDisplayText" />
	<s:set var="tmpI18nStr" value="%{getText('jsp.home_lastLogin')}"
		scope="request" />
	<ffi:setProperty name='PageText'
		value='${GetCurrentDate.Date}  |  ${tmpI18nStr}: ${lastDate} ' />

	<ffi:setProperty name='lastLoginDateWithLocale'
		value='${tmpI18nStr}: ${lastDate} ' />
</ffi:cinclude>

<ffi:cinclude value1="${lastDate}" value2="" operator="equals">
	<ffi:getL10NString rsrcFile="cb" msgKey="jsp/index.jsp-2"
		parm0="${fullName}" parm1="${GetCurrentDate.Date}"
		assignTo="tempDisplayText" />
	<ffi:setProperty name='PageText' value='${GetCurrentDate.Date}' />
</ffi:cinclude>
<ffi:removeProperty name="lastDate" />

<ffi:cinclude value1="${UserLocale.Locale}" value2="en_US"
	operator="notEquals">
	<ffi:setProperty name="ImgExt" value="${UserLocale.Locale}" />
</ffi:cinclude>
<ffi:cinclude value1="${UserLocale.Locale}" value2="en_US"
	operator="equals">
	<ffi:setProperty name="ImgExt" value="" />
</ffi:cinclude>

</head>

<%-- The display:none style is to avoid showing page content in plain text before jQuery stuff is executed.
	 It will be turn on in home.js before call $('body').layout() --%>
<body class="custom ui-widget-content" style="display: none;">
	<span id="helpSubMenuName" style="display: none"></span>
	<span id="helpFileName" style="display: none"></span>
	<s:include value="/pages/jsp/inc/init/portal-init.jsp" />
	<ffi:process name="PortalRefreshAll" />
	<div class="reloadIE"></div>

	<div class="overlayBg"></div>

	<div class="request-alert-message"></div>

	<div id="operationresult" class="serverResponseHolderDiv hidden">
		<div id="resultmessage" role="alert" aria-live="assertive">
			<s:text name="jsp.default_498" />
		</div>
		<span class="serverMsgCloseBtn sapUiIconCls icon-decline"
			onclick="ns.common.hideStatus();"></span>
	</div>

	<div id="outer-north" class="reloadIE resetHeaderPadding">

		<s:include value="/pages/jsp/home/inc/topbar.jsp" />

	</div>


	<div id="page-loading">
		<div class="ui-autocomplete-loading loading" style="font-size: 1px"></div>
	</div>

	<div id="outer-center" class="ui-layout-center hidden">

		<div id="middle-north" class="middle-north"></div>

		<div id="tabpanels">
			<div id="tab1" class="mainMenuBgCls tab ui-tabs-hide">

				<div id="main-menu"
					class="ui-layout-north ui-widget mainMenubarHolderCls"
					role="navigation">
					<%-- <div class="menuFileHolder">
					<s:include value="/pages/jsp/home/inc/menu.jsp" />
				</div> --%>
				</div>

				<div class="ui-layout-center ui-widget-content mainContainer">
					<div id="loadingstatus">
						<%-- for displaying error messages which occur when loading application desktop --%>
					</div>

					<div id="reinitializesessionmessage" style="display: none;">
						<%-- for displaying message when session is reloaded after change in entitlements  --%>
						<s:text name="jsp.home_255" />
					</div>

					<!-- home page layout container -->
					<div id="desktopContainer" class="portal-page">
						<div class='desktopLayoutDialogHeaderDiv hidden'>
							<s:text name="jsp.home_105" />
							<span style='float: right' class='sapUiIconCls icon-decline'
								onclick='ns.home.hideDesktopLayout()'></span>
						</div>
						<div id="desktopDashboard">
							<s:include value="/pages/jsp/home/inc/homeDashboard.jsp" />
						</div>
						<div id="desktopContent" class="desktopContentCls"></div>
					</div>
					<div id="notes" class="ui-widget-content" style="border: none"
						role="main" aria-live="polite" aria-relevant="additions removals">
					</div>
					<div id="blankDiv" style="height: 10px; height: 0;">&nbsp;</div>
				</div>

				<div class="ui-layout-south ui-widget" id="footerHolderDiv">
					<div id="footerDivID" class="toolbar copyrightContainer"
						style="padding: 0px;">
						<div id="sapGoldFooterBar"></div>
						<div class="lastLoginSection">
							<s:text name="jsp.default_428" />
							:&nbsp;</span>
							<ffi:getProperty name='PageText' encode="false" />
						</div>

						<p class="copyrightText">
							<s:text name="jsp.home_66.2" />
						</p>
						<%-- <br><s:text name="jsp.home_226.2"/><br><br> --%>

						<div class="footerLinksSec">
							<sj:a id="languageButton"
								onclick="ns.common.openDialog('language');" href="#"
								cssClass="hidden">
								<img class="countryFlagImg" width="32" height="32"
									src="<s:url value='/web' />/multilang/grafx/<ffi:getProperty name="UserLocale" property="Locale" />_flag.png" />
								<span class="languageSelectionSpan"><s:text
										name="language" /></span>
							</sj:a>

							<sj:a id="contactUsButton"
								onclick="ns.common.openDialog('contactUs');"
								indicator="indicator" href="#">
								<s:text name="contactUs" />
							</sj:a>

							<ffi:cinclude value1="${IsConsumerUser}" value2="true"
								operator="notEquals">
								<ffi:cinclude value1="${User.PRIMARY_ADMIN}" value2="0"
									operator="notEquals">
									<sj:a id="contactMyAdminButton"
										onclick="ns.common.openDialog('contactMyAdmin');"
										indicator="indicator" href="#">
										<s:text name="jsp.default_text_contact_my_admin" />
									</sj:a>
								</ffi:cinclude>
							</ffi:cinclude>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="globalMsgDisplayHolder" class="globalMsgDisplayHolderCls"
		style="display: none !important;">
		<div id="globalMsgDisplayBox"
			class="globalMsgDisplayBoxCls globalMsgFontProp">
			<div id="globalMsgCloseBtn" class="globalMsgCloseBtnCls">
				<span class="sapUiIconCls icon-decline customTheme"></span>
			</div>
			<span id="globalMsgTxtHolderID" class="globalMsgTxtHolder"> </span>
		</div>
	</div>

	<!-- Mail notification window -->
	<div id="msgDisplayHolder" class="notificationDisplayHolderCls">
		<div id="titleBar" class="titleBarCls">
			<span class="titleCls"><s:text name="jsp.message_27" /></span> <span
				class="turnOffNotificationCls"><span
				id="mailNotificationWinCloseBtn"
				onclick="ns.common.hideImmediately('msgDisplayHolder');"
				class="notificationCloseBtnCls ffiUiIcoSmall ffiUiIco-icon-close"
				title="<s:text name='jsp.default_102' />"></span></span>
		</div>
		<div id="msgContentArea" class="contentAreaCls"
			onclick="ns.common.quickViewMessage()">
			<div id="msgDetailsHolder" class="msgDetailsHolderCls"
				onclick="ns.common.quickViewMessage()">
				<input name="recievedMsgID" id="recievedMsgID" type="hidden"
					value="">
				<!-- From & date area -->
				<div
					class="notificationDetailBlock marginTop10 marginBottom10 headerTextColor">
					<span id="msgFrom" class="msgDetailLblTitle">message from
						brown John</span> <span id="msgDate" class="msgDetailLblDesc floatRight">1/1/2014</span>
				</div>

				<!-- Subject area -->
				<div class="notificationDetailBlock nonHeaderItem">
					<span id="msgSubject" class="msgDetailLblDesc">Message
						subject to appear here</span>
				</div>

				<!-- Message body -->
				<div class="notificationDetailBlock nonHeaderItem">
					<span id="msgBody" class="msgBodyLblDesc">Message body to
						appear here</span>
				</div>
				<%-- <!-- Case No. area -->
			<span id="msgCaseNoLabel" class="msgDetailLblTitle">Case No.:</span>
			<span id="msgCaseNo" class="msgDetailLblDesc"></span> --%>
			</div>
			<div class="notificationActionbarCls">
				<span
					class="notificationCloseBtnCls ffiUiIcoSmall ffiUiIco-icon-medium-delete"
					onclick="deleteMessage()" title="<s:text name='jsp.default_162' />"></span>
				<span
					class="notificationCloseBtnCls ffiUiIcoSmall ffiUiIco-icon-reply"
					title="test" onclick="replyMessage()"> </span>
			</div>
		</div>
	</div>

	<!-- Alert notification window -->
	<div id="alertDisplayHolder" class="notificationDisplayHolderCls">
		<div id="titleBar" class="titleBarCls">
			<span class="titleCls"><s:text name="alert.new.alert" /></span> <span
				class="turnOffNotificationCls"><span
				id="alertNotificationWinCloseBtn"
				onclick="ns.common.hideImmediately('alertDisplayHolder');"
				class="notificationCloseBtnCls ffiUiIcoSmall ffiUiIco-icon-close"
				title="<s:text name='jsp.default_102' />"></span></span>
		</div>
		<div id="alertContentArea" class="contentAreaCls"
			onclick="ns.common.quickViewAlert()">
			<div id="alertDetailsHolder" class="msgDetailsHolderCls">
				<input name="recievedAlertID" id="recievedAlertID" type="hidden"
					value="">
				<!-- Subject & date area -->
				<div
					class="notificationDetailBlock marginTop10 marginBottom10 headerTextColor">
					<span id="alertSubject" class="msgDetailLblDesc">Alert
						subject area</span> <span id="alertDate"
						class="msgDetailLblDesc floatRight">2/2/2014</span>
				</div>
				<!-- Alert body -->
				<div class="notificationDetailBlock nonHeaderItem">
					<span id="alertBody" class="msgBodyLblDesc">Alert body to
						appear here...</span>
				</div>
			</div>

			<div class="notificationActionbarCls">
				<span
					class="notificationCloseBtnCls ffiUiIcoSmall ffiUiIco-icon-medium-delete"
					onclick="deleteAlert()" title="<s:text name='jsp.default_162' />"></span>
			</div>
		</div>
	</div>

	<!-- Approval notification window -->
	<div id="approvalDisplayHolder" class="notificationDisplayHolderCls">
		<div id="titleBar" class="titleBarCls">
			<span class="titleCls" id="approvalTitle"><s:text
					name='jsp.approvals.new.approval.request' /></span> <span
				class="turnOffNotificationCls"><span
				id="approvalNotificationWinCloseBtn"
				onclick="ns.common.hideImmediately('approvalDisplayHolder');"
				class="notificationCloseBtnCls ffiUiIcoSmall ffiUiIco-icon-close"
				title="<s:text name='jsp.default_102' />"></span></span>
		</div>
		<div id="approvalContentArea" class="contentAreaCls"
			onclick="ns.common.visitApprovalsArea()">
			<div id="approvalDetailsHolder"
				class="msgDetailsHolderCls marginTop10">
				<input name="approvalItemType" id="approvalItemType" type="hidden"
					value=""> <br />
				<!-- Subject & date area -->
				<div class="notificationDetailBlock marginTop10 headerTextColor">
					Submitted By: <span id="approvalSubmittedBy"
						class="msgDetailLblDesc">Submitted by area</span><br /> Submission
					Date: <span id="approvalDate" class="msgDetailLblDesc">2/2/2014</span>
				</div>

				<!-- Amount -->
				<div id="approvalAmountArea"
					class="notificationDetailBlock headerTextColor">
					Amount :<span id="approvalAmount" class="msgDetailLblDesc">Approval
						amount to appear here</span>
				</div>
			</div>
			<div class="notificationActionbarCls">
				<span
					class="approvalActionbarSpan ffiUiIcoMedium ffiUiIco-icon-check"
					title="<s:text name='jsp.default_6' />"></span>
			</div>
		</div>
	</div>

	<iframe id='ifrmPrint' style="width: 0px; height: 0px; opacity: 0"></iframe>


	<%-- Timeout interval will be refreshed upon successful Ajax request. Refer to common.js. --%>
	<s:include value="/pages/jsp/inc/timeout.jsp" />

	<%-- Check View only OBO flag of agent --%>
	<ffi:setProperty name="ViewOnlyOBO"
		value="${SecureUser.Agent.ViewOnlyOBO}" />

	<%-- Define the commonly used variables which are derived from server --%>
	<script type='text/javascript'>
	// initialize system language global variable (zh_CN or en_US)
    ns.home.locale = '<ffi:getProperty name="UserLocale" property="Locale"/>';
	ns.common.landingMenuId = '<ffi:getProperty name="StartPage"/>';
	
	// Set User agent ViewOnlyOBOFlag
	ns.common.isViewOnlyOBO = '<ffi:getProperty name="ViewOnlyOBO" />';
	
	ns.billpay.loadDashBoardURL = "<ffi:urlEncrypt url='/cb/pages/jsp/billpay/billpay_dashboard.jsp' />";
	ns.billpay.getAllPaymentsURL = "<ffi:urlEncrypt url='/cb/pages/jsp/billpay/inc/getPayments.jsp' />";
	ns.home.getSessionTokenVarForGlobalMessage = '<ffi:getProperty name="CSRF_TOKEN"/>';
	$('#blankDiv').css("height","22px");

	ns.common.loginDate = '<%=loginDateStr %>';

	JS_INVALIDATE_SESSION_URL = "<ffi:getProperty name='FullPagesPath'/>invalidate-session.jsp";

	//Set user's locale.It is used in UI5.
	JS_LOCALE_LANGUAGE = "<ffi:getProperty name='UserLocale' property='Language'/>";
	JS_LOCALE_NAME = "<ffi:getProperty name='UserLocale' property='LocaleName'/>";

	jQuery(document).ready(function (){ 
		ns.home.executeAfterDomReady();
		if(applicationController){
			applicationController.init();
		}
	});	
</script>

	<ffi:removeProperty name="ViewOnlyOBO" />
</body>
</html>