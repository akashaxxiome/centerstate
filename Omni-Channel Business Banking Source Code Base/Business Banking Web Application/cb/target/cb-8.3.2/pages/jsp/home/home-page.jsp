<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="request"/>
<ffi:process name="GetCurrentDate"/> 
<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateFormat}" />
<input id="currentDateId" type="hidden"  value='<ffi:getProperty name="GetCurrentDate" property="Date" />'>

<div id="portalIconOperationResult">
		<div id="resultmsg" align="center"></div>
</div><!-- result -->

<s:include value="/pages/jsp/home/inc/portal-page-form.jsp" />

<div id="layoutContent"></div>
<div id="blankLayoutNotice" class="hidden noPortletMsgCls" align="center">
	<a class="anchorText" href="#" onclick="$('#addPortletBtnLink').click()"><s:text name="jsp.default.blankLayoutNotice"/></a>
</div>

<script>
	$.subscribe("successAddLayout",function(e,d){
		console.info("successAddLayout");
	});

	$.subscribe("errorAddlayout",function(e,d){
		console.info("errorAddlayout");
	});
</script>



<div id="removeConfirmDialog" title="<s:text name="jsp.home_246"/>" style="display:none" class="staticContentDialog" >
	<span>
		<s:text name="jsp.home_36"/>
	</span>
</div>

<script type="text/javascript">
//URL Encryption
ns.common.resetDialogState(); // On portal page reset home-page is loaded and not index.jsp
ns.home.currentPortalPage = '';
//ns.home.loadPortalPage('<s:url action="LoadPortalPageSettings"/>');

function loadPortletContent(portlet, action, aMode) {
	//URL Encryption: Change "$.get" method to "$.post" method
	var aConfig = {
	  type: "POST",
	  containerId: portlet.attr('id'),
	  url: '<s:url action="' + action + '"/>',
	  data: {portletId: portlet.attr('id'), CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
	  success: function(data){
		//portlet.show();
		portlet.html(data).portlet('setTitleWidth');
		ns.home.bindPortletHoverToTable(portlet);
		
		if(aMode === "EDIT"){
			portlet.slideToggle('slow');
			if(accessibility){
				$(portlet).setFocus();
			}		
		}
		$.publish("common.topics.tabifyNotes");
		$.publish("common.topics.portletContentLoaded",{portletId:portlet.attr('id')});
	  },
	  error: function(error){
	  	portlet.html(js_portlet_failed);
	  }
	};

	var localAjaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
	$.ajax(localAjaxSettings);
}

function updatePortalPageLayout() {
	var columnNum = $('.column:visible').length;
	isPortalModified = true;
	$.ajax({
		url: '<s:url action="UpdatePortalPageLayout.action"></s:url>',
		type: "POST",
		data:{portalPageLayout: ns.home.getPortalPageLayout(), columnCount: columnNum, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
		success: function(data) {
			var layoutPortletsCount = $('#layoutContent .portlet .innerPortletsContainer').length;
			if(layoutPortletsCount != undefined && layoutPortletsCount == 0){
				ns.layout.toggleBlankLayoutNotice(true);
			}else{
				ns.layout.toggleBlankLayoutNotice(false);
			}
		}
	});
}
 
function onDeletePortalPageConfirm(){
	var selectedPageName = $('#portalPagesSelect').val();
	$.ajax({
		url: '<s:url action="DeletePortalPage.action"></s:url>',
		type: "POST",
		data: {portalPageName: selectedPageName, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
		dataType:'json',
		success: function(data) {
			if(data.errorMsg) {
				$('#deletePortalPageError').show();
				$('#deletePortalPageErrorMsg').html(data.errorMsg);
			} else {
				var config = ns.common.dialogs["portalOperationNotice"];
				var beforeDialogOpen = function(){			
					var msg = jQuery.i18n.prop('js_home_DeletePortalSuccessMsg', [selectedPageName]);
					$('#portalOperationNoticeDialogID').html(msg);
				}
				
				config.beforeOpen = beforeDialogOpen;
				ns.common.openDialog("portalOperationNotice");
				//$('#portalOperationNoticeDialogID').html(jQuery.i18n.prop('js_home_DeletePortalSuccessMsg', [selectedPageName])).dialog('open');
				
				// CR : 758019 - C4:Home portal page_delete page success msg popup not getting closed by 'X' icon.
				$('#deletePortalSettingNoticeDialogID').dialog('close');
				
				
				$('#portalPagesDialog').dialog('close');
				$('#portal-container .column').html('');
				ns.home.loadPortalPage('<s:url action="LoadPortalPageSettings.action"/>');
			}
		}
	});
	
}

function onDeletePortalPage() {
	var selectedPageName = $('#portalPagesSelect').val();
	var config = ns.common.dialogs["deletePortalSettingsNotice"];
	var beforeDialogOpen = function(){			
		$('#deletePortalSettingNoticeDialogID').html(jQuery.i18n.prop('js_home_DeletePortalAlertMsg', [selectedPageName]))		
	}
	config.beforeOpen = beforeDialogOpen;
	ns.common.openDialog("deletePortalSettingsNotice");
}

function onLoadPortalPage() {
	if( isPortalModified) {
		var config = ns.common.dialogs["loadPortalSettingsNotice"];
		var beforeDialogOpen = function(){			
			$('#loadPortalSettingNoticeDialogID').html(js_home_loadPortalPageNotice);			
		}
		config.beforeOpen = beforeDialogOpen;
		ns.common.openDialog("loadPortalSettingsNotice");
			
	} else {
		var selectedPageName = $('#portalPagesSelect').val();
		ns.home.changePortalPage(selectedPageName, '<s:url action="ChangePortalPage.action"></s:url>');
	}
}

function onLoadPortalPageWithPopup(){
	var selectedPageName = $('#portalPagesSelect').val();
	ns.home.changePortalPage(selectedPageName, '<s:url action="ChangePortalPage.action"></s:url>');
}

function onSavePortalPage(pageName, isDefaultPage, saveMode) {
	//if(pageName) {
		var action = "";
		if(saveMode == 1) {
			action = '<s:url action="SavePortalPage.action"></s:url>';
		} else if(saveMode == 2) {
			action = '<s:url action="UpdatePortalPage.action"></s:url>';
		} else if(saveMode == 3) {
			action = '<s:url action="ResetDefaultPortalPage.action"></s:url>';
		}
		$.ajax({
			url: action,
			type: "POST",
			data: {portalPageName: pageName, isDefault: isDefaultPage, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
			dataType:'json',
			success: function(data) {
				if(data.errorMsg) {
					$('#confirmSavePortalPageError').show();
					$('#confirmSavePortalPageErrorMsg').html(data.errorMsg);
				} else {
					isPortalModified = false;
					var msg = ""
					if(saveMode == 1) {
						msg = jQuery.i18n.prop('js_home_SavePortalSuccessMsg', [pageName]);
					} else if(saveMode == 2) {
						msg = jQuery.i18n.prop('js_home_UpdatePortalSuccessMsg', [pageName]);
					} else if(saveMode == 3) {
						msg = jQuery.i18n.prop('js_home_ResetPortalSuccessMsg', [pageName]);
					}
					//Assign callback for onbeforeclose 
					var closeDialogHandler = function(event, ui) { 
						ns.home.changePortalPage(pageName, '<s:url action="ChangePortalPage.action"></s:url>');		 
					};
					
					var config = ns.common.dialogs["portalOperationNotice"];
					var beforeDialogOpen = function(){									
						$('#portalOperationNoticeDialogID').html(msg);
					}
					config.beforeOpen = beforeDialogOpen;
					config.close = closeDialogHandler;
					ns.common.openDialog("portalOperationNotice");
				
					if(accessibility){
						$("#portalOperationNoticeDialogID").siblings(".ui-dialog-buttonpane").setFocus();						
					}
				}
			}
	});
	//}
}

function resetToPreviousPage() {
	$.ajax({
		url: '<s:url action="ResetPreviousPortalPage.action"></s:url>',
		type: "POST",
		data: {CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
		dataType:'json',
		success: function(data) {
			if(data.errorMsg){
				ns.home.showPortalOperationResult(data.errorMsg);
			} else {
				isPortalModified = false;
				ns.home.changePortalPage(data.pageName, '<s:url action="ChangePortalPage.action"></s:url>');
			}
		}
	});
}



function resetPortalPageNotice(){
		var mode = $('input[name="portalPageResetMode"]:checked').val();
		if(mode=='1'){
			$('#resetPortalNoticeSpan').html(js_ResavePortalAlert1);
		}
		if(mode=='2'){
			action = '<s:url action="ResetDefaultPortalPage.action"></s:url>';
			$('#resetPortalNoticeSpan').html(js_ResavePortalAlert2);
		}
}
	
function resetToDefaultPage(pageName, isDefaultPage){
	$.ajax({
		url: '<s:url action="ResetDefaultPortalPage.action"></s:url>',
		type: "POST",
		dataType:'json',
		data: {portalPageName: pageName, isDefault: isDefaultPage, CSRF_TOKEN: ns.home.getSessionTokenVarForGlobalMessage},
		success: function(data) {
			if(data.errorMsg){
				ns.home.showPortalOperationResult(data.errorMsg);
			} else {
				isPortalModified = false;
				ns.home.changePortalPage(pageName, '<s:url action="ChangePortalPage.action"></s:url>');
			}
		}
	});
}

function resetPortalPage(){
		var mode = $('input[name="portalPageResetMode"]:checked').val();
		if(mode=='1'){
			resetToPreviousPage();
		}
		if(mode=='2'){
			var pageName = ns.home.currentPortalPage;
			if(pageName === undefined){
				pageName = ns.home.newPortalPageName;
			}
			var isDefaultPage = $('#newPortalPageSetDefault').is(':checked');
			resetToDefaultPage(pageName, isDefaultPage);
		}
}

$(document).ready(function(){
	if(layoutController){
		layoutController.init();
		window.CSRF_TOKEN = '<ffi:getProperty name="CSRF_TOKEN"/>';
	}
});

</script>