<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
		<div class="layoutChooserDiv">
			<s:iterator value="%{systemLayouts}" status="listStatus" var="systemLayout">
				<div class="layoutOptionDiv">
					<span class="layoutNameBar"> <s:property value="%{#systemLayout.title}"/></span>
					<ffi:cinclude value1="${CURRENT_LAYOUT.id}" value2="${systemLayout.id}" operator="equals">
						<span class='<s:property value="%{#systemLayout.iconClass}"/> selectedLayoutstyle' data-layout-id='<s:property value="%{#systemLayout.id}"/>' />
					</ffi:cinclude>
					<ffi:cinclude value1="${CURRENT_LAYOUT.id}" value2="${systemLayout.id}" operator="notEquals">
						<span class='<s:property value="%{#systemLayout.iconClass}"/>' data-layout-id='<s:property value="%{#systemLayout.id}"/>' />
					</ffi:cinclude>
				</div>
			</s:iterator>
			
			<div class="layoutOptionDiv">
				<span class="layoutNameBar"><s:text name="label.home.layout.custom" /></span>
				<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="custom" operator="equals">
					<span class="ui-state-default sapUiIconCls icon-employee layoutTileOption selectedLayoutstyle" data-layout-id="CUSTOM_LAYOUT" />
				</ffi:cinclude>
				<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="custom" operator="notEquals">
					<span class="ui-state-default sapUiIconCls icon-employee layoutTileOption" data-layout-id="CUSTOM_LAYOUT" />
				</ffi:cinclude>
			</div>
		</div>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="custom" operator="equals">
			<div class="layoutSearchBoxDiv">
				<input name="layoutSearchInput" id="layoutSearchInput" value='<ffi:getProperty name="CURRENT_LAYOUT" property="name"/>' placeholder="Select layout to load..." onclick="ns.home.showAllCustomLayouts()" class="ui-widget-content ui-corner-all txtbox layoutSearchInputCls"/>
				<div id="layoutOptHolderDiv"></div>
			</div>
		</ffi:cinclude>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="custom" operator="notEquals">
			<div class="layoutSearchBoxDiv hidden">
				<input name="layoutSearchInput" id="layoutSearchInput" placeholder="Select layout to load..." onclick="ns.home.showAllCustomLayouts()" class="ui-widget-content ui-corner-all txtbox layoutSearchInputCls"/>
				<div id="layoutOptHolderDiv"></div>
			</div>
		</ffi:cinclude>
		<div align="center" class="ui-widget-header customDialogFooter" id="userprofileButtonID">
			<sj:a id="cancelLoadPortalPage" button="true"
				onClickTopics="cancelLoadLayoutTopic">
				<s:text name="jsp.default_82" />
			</sj:a>
		</div>
<script>
	$('#portalPageLayout').addHelp(function(){
		var helpFile = $('#portalPageLayout').find('.moduleHelpClass').html();
		callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
	});
	
	$(document).ready(function() {
		
		// Adding class to parent div in order to show drop down values overlapping the dialog box boundries
		$("#portalPageLayout").parent().css("overflow", "visible");

		$.subscribe('cancelLoadLayoutTopic', function(event,data){
			$("#layoutSearchInput").val('');
			$.publish("closeDialog");
		});

		$(".layoutTileOption").click(function(e){			
			$("span.selectedLayoutstyle").removeClass("selectedLayoutstyle");
			$(this).addClass("selectedLayoutstyle");
			var $el = $(e.currentTarget);
			var layoutId = $el.attr("data-layout-id");
			if(layoutId === "CUSTOM_LAYOUT"){
				$(".layoutSearchBoxDiv").removeClass("hidden");
			}else{
				$.publish("closeDialog");
				ns.layout.changeLayout(layoutId);
				$(".layoutSearchBoxDiv").addClass("hidden");
			}
		});
		
		customLayoutSearchFunction = function(request, response){
			$.ajax({
				url: '/cb/pages/jsp/home/customLayoutsLookupBox.action',
				data: {term: request.term},
				global:false,
				success: function(data){
					 response($.map( data.optionsModel, function(item){
							var text = $.trim(item.name);	
						 	var textToReplace = text.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(request.term) + ")(?![^<>]*>)(?![^&;]+;)", "i"), "<strong>$1</strong>");
						 	return {
				 	              	label: textToReplace,
				 	              	id: item.id,
				 	              	name:item.name,
				 	              	appType:item.appType,
				 	              	type:item.type,
				 	              	isdefault:item.isdefault,
							}
			 	        }));
				}
			});
		};
		
		$("#layoutSearchInput").autocomplete({
		    source: customLayoutSearchFunction,
		    appendTo:"#layoutOptHolderDiv",
		    minLength : "0",
		    create: function () {
		        $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
		        	
		        	/* var li = '<li>';
		        	$(li).data(item);
		        	return $(li).append('<a>' + item.label + ' </a>')
		            .appendTo(ul); */
		            
		        	var defaultLayoutActions = '<span class="sapUiIconCls icon-home" style="float:left;"></span>'; 
		        	if(item.isdefault == 'false'){
		        		defaultLayoutActions = '<span class="paddingRight10" style="float:left;"></span>';
		        	}
		        	var li = '<li>';
		        	$(li).data(item);
		        	return $(li).append('<a>' + defaultLayoutActions + item.label + ' </a>')
		            .appendTo(ul);
		        	
		        };
		    },
		    select: function(event, ui){
		    	$("#layoutSearchInput").val(ui.item.name);
		    	$.publish("closeDialog");
		    	ns.layout.changeLayout(ui.item.id);
		     	return false;
		    },
		    focus: function( event, ui ) {
		    	return false;
		    },
		    open: function() { $('#layoutOptHolderDiv .ui-menu').width(253);
                $('#layoutOptHolderDiv .ui-menu').css("overflow","auto");
            }
		    
		});
		
		ns.home.showAllCustomLayouts = function(){
			$("#layoutSearchInput").autocomplete("search", "");
			$("#layoutSearchInput").focus();
		};
		
	});
</script>