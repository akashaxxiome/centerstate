<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
 <%@ page contentType="text/html; charset=UTF-8" %>

<%-- <ffi:setGridURL grid="GRID_PENDING_PAYMENTS" name="DeleteURL"
	url="/cb/pages/jsp/billpay/billpaydeletebill.jsp?Collection=PendingPayments&ID={0}&fromPortalPage=true" parm0="ID"/>
<ffi:setGridURL grid="GRID_PENDING_PAYMENTS" name="EditURL" url="/cb/pages/jsp/home/portlet/inc/pending-billpayments-edit.jsp?ID={0}&Collection=PendingPayments&isPending=true" parm0="ID"/>
 --%>
 <ffi:setGridURL grid="GRID_PENDING_PAYMENTS" name="DeleteURL" url="/cb/pages/jsp/billpay/cancelBillpayAction_init.action?ID={0}&PaymentType={1}&RecPaymentID={2}&isPortalPage='true" parm2="RecPaymentID" parm1="PaymentType" parm0="ID"/>
 <ffi:setGridURL grid="GRID_PENDING_PAYMENTS" name="EditURL" url="/cb/pages/jsp/billpay/editBillpayAction_init.action?ID={0}&RecPaymentID={1}&PaymentType={2}&isPortalPage='true'" parm1="RecPaymentID" parm0="ID" parm2="PaymentType"/>
<script type="text/javascript">
	ns.pendingTransactionsPortal.initPendingPaymentsGrid('pendingPaymentsSummaryID', 'onPendingPaymentsGridLoadComplete');
</script>

<ffi:setProperty name="tempURL" value="/pages/jsp/home/PendingBillpayPortletAction_getPayments.action?GridURLs=GRID_PENDING_PAYMENTS" URLEncrypt="true"/>
<s:url id="pendingPaymentsSummaryUrl" value="%{#session.tempURL}"  escapeAmp="false"/>
<sjg:grid
	id="pendingPaymentsSummaryID"
	caption=""
	dataType="json"
	href="%{pendingPaymentsSummaryUrl}"
	pager="true"
	gridModel="gridModel"
	rowNum="5"
	shrinkToFit="true"
	sortable="true"
	scroll="false"
	scrollrows="true"
	viewrecords="true"
	footerrow="false"
	userDataOnFooter="false"
	onGridCompleteTopics="onPendingPaymentsGridLoadComplete"
	cssClass="portletRenderer">

	<sjg:gridColumn name="payDate" index="payDate" title="%{getText('jsp.default_137')}" sortable="true" width="75"/>

	<sjg:gridColumn name="account.accountDisplayText" index="Accounts" title="%{getText('jsp.default_598')}" sortable="false" width="250"  formatter="ns.pendingTransactionsPortal.formaAccountAndPayee"/>
	
	<sjg:gridColumn name="map.accountText" id="accountText" index="accountNickName"
		title="%{getText('jsp.billpay_account')}" sortable="true" hidden="true" width="100"/>

	<sjg:gridColumn name="account.consumerMenuDisplayText" id="NICKNAME" index="consumerDisplayText"
		title="%{getText('jsp.billpay_6')}" sortable="true" hidden="true" hidedlg="true" width="100"/>
		
	<sjg:gridColumn name="account.consumerDisplayText" id="accountDisplayText" index="accountNickName" hidden="true" hidedlg="false"
		title="%{getText('jsp.billpay_account')}" sortable="true" width="100"/>

	<sjg:gridColumn name="payeeName" index="payeeName" title="%{getText('jsp.default_313')}" sortable="true" width="100" hidden="true" hidedlg="false"/>

	<sjg:gridColumn name="map.Frequency" index="FREQUENCY" title="%{getText('jsp.default_215')}" formatter="ns.pendingTransactionsPortal.formatBillpayFrequencyColumn" 
	hidden="true" hidedlg="false" sortable="false" width="60"/>

	<sjg:gridColumn name="numberPayments" index="numberPayments" title="#" hidden="true" hidedlg="false"
		formatter="ns.pendingTransactionsPortal.formatNumberPayments" sortable="false" width="40"/>

	<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_43')}"
		formatter="ns.pendingTransactionsPortal.formatAmountColumn" sortable="true" width="60" />

	<sjg:gridColumn name="account.displayText" index="accountDisplayText" title="%{getText('jsp.billpay_5')}"
		search="false" sortable="false" width="120" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="account.currencyCode" index="accountCurrencyCode" title="%{getText('jsp.billpay_39')}"
		search="false" sortable="false" width="120" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="account.number" index="accountNumber" title="%{getText('jsp.default_19')}" width="100"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="amount" index="amount" title="%{getText('jsp.default_43')}"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="status" index="statusName" title="%{getText('jsp.default_388')}" formatter="ns.pendingTransactionsPortal.formatBillpayStatusColumn" sortable="true" 
	hidden="true" hidedlg="false" width="30"/>	

	<sjg:gridColumn name="" index="ID" title="%{getText('jsp.default_27')}" width="40" sortable="false" hidden="true" hidedlg="false"
		search="false" formatter="ns.pendingTransactionsPortal.formatPendingBillpayActionLink" cssClass="__gridActionColumn"/>

	<sjg:gridColumn name="EditURL" index="EditURL" title="%{getText('jsp.default_181')}" search="false"
		sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="EditRecURL" index="EditRecURL" title="%{getText('jsp.default_181')}"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="DeleteRecURL" index="DeleteRecURL" title="%{getText('jsp.default_167')}"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="map.accountText" index="accountText" title="accountText" search="false"
		sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="map.isEntitled" index="isEntitled" title="isEntitled"
		search="false" sortable="false" hidden="true" hidedlg="true"/>
</sjg:grid>

<div id="pendingPaymentsSummaryID_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('pmtTran_billpay,pending');"><s:text name="jsp.default.viewAllRecord" /></div>


<script>
if($("#pendingPaymentsSummaryID tbody").data("ui-jqgrid") != undefined){
	$("#pendingPaymentsSummaryID tbody").sortable("destroy");
}
</script>