<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_save() {
		var idSelector = '#<s:property value="portletId"/>';
		
		var counts = $(idSelector + '_combobox').val();

		//URL Encryption: Change "$.get" method to "$.post" method
		var aConfig = {
				type: "POST",
				url: '<s:url action="NewsPortletAction_updateSettings.action"/>',
				data: {portletId: '<s:property value="portletId"/>', newsType:'<s:property value="newsType"/>', itemCounts:counts, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
				success: function(data){
					ns.home.switchToView($(idSelector));
					$(idSelector).html(data);
					isPortalModified = true;
					ns.home.bindPortletHoverToTable($(idSelector));
					
					$.publish("common.topics.tabifyNotes");	
					if(accessibility){
						$(idSelector).setFocus();
					}
				}
			};

			var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
			$.ajax(ajaxSettings);	
	}
	$('#<s:property value="portletId"/>_combobox').selectmenu({width:'140px'});
	
	$('#<s:property value="portletId"/>_anchor').button({
		icons: {}
	});
</script>

<ffi:help id="home_portlet-news-edit"/>
<table class="marginBottom5">
	<tr>
		<td nowrap>
			<span style="font-weight:bold;">
				<s:text name="jsp.home_79"/>
			</span>
		</td>
		<td>
			<s:if test="%{newsType == 1}">
				<ffi:setProperty name="Compare" property="Value1" value="${NewsSportsSettings.MaxHeadlines}"/>
				<ffi:setProperty name="ValidateEnumerationValue" property="Type" value="NewsSportsSettings_MaxHeadlines" />
			</s:if>
			<s:if test="%{newsType == 2}">
				<ffi:setProperty name="Compare" property="Value1" value="${NewsBusinessSettings.MaxHeadlines}"/>
				<ffi:setProperty name="ValidateEnumerationValue" property="Type" value="NewsBusinessSettings_MaxHeadlines" />
			</s:if>
			<s:if test="%{newsType == 3}">
				<ffi:setProperty name="Compare" property="Value1" value="${NewsFrontpageSettings.MaxHeadlines}"/>
				<ffi:setProperty name="ValidateEnumerationValue" property="Type" value="NewsFrontpageSettings_MaxHeadlines" />
			</s:if>
			
			<ffi:setProperty name="ValidateEnumerationValue" property="Action" value="<%=com.ffusion.tasks.ValidateEnumerationValueTask.ACTION_DISPLAY%>" />
			<ffi:process name="ValidateEnumerationValue" />
			
			<select class="txtbox" id="<s:property value="portletId"/>_combobox">
				<ffi:list collection="<%=com.ffusion.tasks.ValidateEnumerationValueTask.ENUMERATION_DATA_LIST%>" items="Item">
				<ffi:setProperty name="Compare" property="Value2" value="${Item.ID}" />
				<ffi:setProperty name="tmpI18nStr" value="${Item.NAME}" />
				<option value='<ffi:getProperty name="Item" property="ID" />' <ffi:getProperty name="selected${Compare.Equals}"/>> <ffi:getProperty name="Item" property="ID" /> <s:text name="%{#session.tmpI18nStr}" /> </option>
				</ffi:list>
			</select>
		</td>
		<td>
			<a id="<s:property value="portletId"/>_anchor" href="javascript:void(0)" onclick="<s:property value="portletId"/>_save()" ><s:text name="jsp.default_303"/></a>
		</td>
	</tr>
	
</table>

<div id="newsPortletEditDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>
