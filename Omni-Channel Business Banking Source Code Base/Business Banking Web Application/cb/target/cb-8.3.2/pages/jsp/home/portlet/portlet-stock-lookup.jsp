<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<div id="<s:property value="portletId"/>_stockSymbol"><s:text name="jsp.home_116"/></div>
<script type="text/javascript">
	$('#<s:property value="portletId"/>_lookup_searchButton').button({
		icons: {}
	});
	
	function <s:property value="portletId"/>_lookup_stock_search() {
		var showType = '<s:property value="lookupType"/>';
		var symbol = $('#<s:property value="portletId"/>_lookup_symbolCompany').val();
		if(showType == "chart") 
			lookupStockChart(symbol, 'company');
		else if(showType == "quote") 
			lookupStockQuote(symbol, 'company');
	}
</script>

<%-- ================ MAIN CONTENT START ================ --%>
			
			<ffi:help id="home_portlet-stock-lookup"/>
            <table>
                <tr>
                    <form name="frmPortfolio" action="<ffi:getProperty name="SecurePath"/>inc/portal/portal-portfolio-save-wait.jsp" method="POST">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                        <td>
                            <ffi:setProperty name="firstempty" value=""/>
                            <ffi:list collection="StocksEdit" items="Stock1">
                                <ffi:cinclude value1="" value2="${Stock1.Symbol}" operator="equals" >
                                    <ffi:cinclude value1="" value2="${firstempty}" operator="equals" >
                                        <ffi:setProperty name="firstempty" value="${Stock1.Counter}"/>
                                    </ffi:cinclude>
                                </ffi:cinclude>
                                <input type="Hidden" name="StocksEdit.CurrentStock=<ffi:getProperty name="Stock1" property="Counter"/>&StocksEdit.Symbol" value="<ffi:getProperty name="Stock1" property="Symbol"/>">
                                <input type="Hidden" name="StocksEdit.CurrentStock=<ffi:getProperty name="Stock1" property="Counter"/>&StocksEdit.Shares" value="<ffi:getProperty name="Stock1" property="Shares"/>">
                                <input type="Hidden" name="StocksEdit.CurrentStock=<ffi:getProperty name="Stock1" property="Counter"/>&StocksEdit.PurchasePrice" value="<ffi:getProperty name="Stock1" property="PurchasePrice"/>">
                            </ffi:list>
                        </td>
                    </form>
                </tr>
            </table>

			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="sectionsubhead"><s:text name="jsp.default_536"/></td>
					<td style="width: 5"></td>
					<td class="sectionsubhead"><s:text name="jsp.default_517"/></td>
					<td style="width: 5"></td>
					<td align="center" class="sectionsubhead"><s:text name="jsp.home_54"/></td>
					<td style="width: 5"></td>
					<td align="center" class="sectionsubhead" nowrap><s:text name="jsp.home_26"/></td>
				</tr>
				<ffi:setProperty name="Math" property="Value1" value="3" />
				<ffi:setProperty name="Math" property="Value2" value="1" />
				
				<ffi:setProperty name="localePath" value="${UserLocale.LocaleName}/"/>
				<ffi:cinclude value1="en_US" value2="${UserLocale.LocaleName}" operator="equals" >
					<ffi:setProperty name="localePath" value=""/>
				</ffi:cinclude>
				<ffi:list collection="PortalStockSymbols" items="Symbol">
					<tr>
						<td class="columndata">&nbsp;<ffi:getProperty name="Symbol" property="CompanyName"/></td>
						<td style="width: 5"></td>
						<td class="columndata"><a href="javascript:void(0)" onclick="lookupStockQuote('<ffi:getProperty name="Symbol" property="Symbol"/>', 'Symbol')"><ffi:getProperty name="Symbol" property="Symbol"/></a></td>
						<td style="width: 5"></td>
						<td class="columndata" align="center" ><a href="javascript:void(0)" onclick="lookupStockChart('<ffi:getProperty name="Symbol" property="Symbol"/>', 'Symbol')"><img src="<s:url value="/web"/>/multilang/grafx/chart.gif" width="12" height="12" alt="<s:text name="jsp.home_230"/>" border="0" style="text-align: center"/></a></td>
						<td style="width: 5"></td>
						
						<td class="columndata" align="center"><a href="javascript:void(0)" onclick="<s:property value="portletId"/>_addSymbol('<ffi:getProperty name="Symbol" property="Symbol"/>')"><img src="<s:url value="/web"/>/grafx/<ffi:getProperty name="localePath" />button_add.gif" width="31" height="14" alt="<s:text name="jsp.default_30"/>" border="0"></a></td>
					</tr>
					<ffi:setProperty name="Math" property="Value2" value="${Math.Subtract}" />
				</ffi:list>
			</table>
            <br>

            <table border="0" style="font-size:10px">
                <tr>
                	<td>
						<span class="mainfontbold"><s:text name="jsp.home_117"/> </span>
                        <input id="<s:property value="portletId"/>_lookup_symbolCompany" style="overflow: hidden; vertical-align: middle; font-size: 12px; padding: 2px 6px;" class="ui-widget ui-widget-content" size="32" maxlength="100">&nbsp;
                        <a id="<s:property value="portletId"/>_lookup_searchButton" href="javascript:void(0)" onclick="<s:property value="portletId"/>_lookup_stock_search()"><s:text name="jsp.default_548"/></a>
                    </td>
                </tr>
            </table>
<%-- ================= MAIN CONTENT END ================= --%>

<div id="stockPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick='<s:property value="portletId"/>_switchToEdit()'><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>