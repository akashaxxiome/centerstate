<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<style>
	#<s:property value="portletId"/>_colbal_summary_detail .ui-jqgrid-ftable td{
		border:none;
	}
</style>

<script type="text/javascript">
	//ns.home.initAccountOtherGrid('<s:property value="portletId"/>_conBal_other_datagrid', 'onOtherGridLoadComplete');
</script>
<s:include value="/pages/jsp/home/inc/portlet-con-bal-options.jsp" />
<div id="otherAccountsPortletID" style="padding:10px;overflow:auto;">
	<%-- URL Encryption Edit By Dongning --%>
	<ffi:setProperty name="tempURL" value="/pages/jsp/home/ConsolidatedBalancePortletAction_loadOtherData.action?queryDataClass=${queryDataClass}&portletId=${portletId}" URLEncrypt="true"/>
	<s:url id="account_other_data_action" value="%{#session.tempURL}"/>
		<sjg:grid id="%{portletId}_conBal_other_datagrid"
					dataType="json" 
					href="%{account_other_data_action}" 
					pager="true" 
					rowList="%{#session.StdGridRowList}" 
	 				rowNum="%{#session.StdGridRowNum}"  
					gridModel="summaries" 
					hoverrows="false" 
					footerrow="true" 
					userDataOnFooter="true"
					sortable="true"
					rownumbers="false"
     				navigator="true"
	 				navigatorAdd="false"
	 				navigatorDelete="false"
					navigatorEdit="false"
					navigatorRefresh="false"
					navigatorSearch="false"
					navigatorView="false"
					shrinkToFit="true"					
					scroll="false"
					viewrecords="true"
					scrollrows="true"
					onGridCompleteTopics="onOtherGridLoadComplete,enableGridAutoWidth">         
			<sjg:gridColumn name="nameNickNameloan" index="NUMBER" title="%{getText('jsp.default_15')}" width="150" align="left" sortable="true"  formatter="ns.home.formatAccountColumn"/>
			<sjg:gridColumn name="account.nickName" index="ACCNICKNAME" title="%{getText('jsp.default_293')}" width="280" align="left" sortable="true" formatter="ns.home.customToAccountNickNameColumn"/>
			<sjg:gridColumn name="account.bankName" index="BANKNAME" title="%{getText('jsp.default_61')}" width="100" align="left" sortable="true"/>
			<sjg:gridColumn name="account.type" index="TYPESTRING" title="%{getText('jsp.default_444')}" width="100" align="left" sortable="true"/>
			<sjg:gridColumn name="summaryDateDisplay" index="SummaryDateDisplay" title="%{getText('jsp.default_51')}" width="90" align="center" sortable="true"/>
			<sjg:gridColumn name="displayOpeningLedger" index="OPENING_LEDGER" title="%{getText('jsp.default_305')}" width="120" align="right" sortable="true"/>
			<sjg:gridColumn name="totalDebits" index="TOTAL_DEBITS" title="%{getText('jsp.default_434')}" width="120" align="right" sortable="true"/>
			<sjg:gridColumn name="totalCredits" index="TOTAL_CREDITS" title="%{getText('jsp.default_433')}" width="120" align="right" sortable="true"/>
			<sjg:gridColumn name="account.displayClosingBalance" index="CLOSINGBALANCE" title="%{getText('jsp.default_104')}" width="120" align="right" sortable="true"/>
			<sjg:gridColumn name="account.displayIntradayCurrentBalance.amountValue.currencyStringNoSymbol" index="INTRADAYCURRENTBALANCE" title="%{getText('jsp.default_129')}" width="120" align="right" sortable="true"/>
		</sjg:grid>
		
		<div id="<s:property value="portletId"/>_conBal_other_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('acctMgmt_consobal');"><s:text name="jsp.default.viewAllRecord" /></div>
			<script>
			$(document).ready(function(){
				if(ns.common.isInitialized($("#<s:property value='portletId'/>_conBal_other_datagrid",'ui-jqgrid'))){
					$("#<s:property value='portletId'/>_conBal_other_datagrid tbody").sortable("destroy");
				}
				
				ns.common.addGridControls("#<s:property value='portletId'/>_conBal_datagrid", false);
			})
			</script>
</div>