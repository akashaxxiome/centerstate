<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<div class="hidden">
<sj:dialog
    	id="saveThemeDialogID"
    	buttons="{
			 '%{getText(\"jsp.default_82\")}':function() {
    			$(this).dialog('close');
    		 },
			 '%{getText(\"jsp.default_366\")}':function() {
				ns.home.saveTheme();
    			$(this).dialog('close');
    		 }
    		}"
    	autoOpen="false"
    	modal="true"
    	title="%{getText('jsp.home_177')}"
		showEffect="fold"
   		hideEffect="clip"
		width="320"
		height="200"
		cssClass="saveThemeDialog staticContentDialog"
    ><s:text name="jsp.home_199"/>
</sj:dialog>
</div>
