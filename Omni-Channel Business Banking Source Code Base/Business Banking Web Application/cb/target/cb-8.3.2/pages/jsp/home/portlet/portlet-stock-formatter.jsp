<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<s:set var="value" value="#parameters.value[0]" scope="request" />
<s:if test='%{"GainLoss".equals(#parameters.type[0])}'>
	<ffi:object id="Currency" name="com.ffusion.beans.common.Currency" scope="request"/>
	<ffi:setProperty name="Currency" property="Format" value="CURRENCY" />
	<ffi:setProperty name="Currency" property="Amount" value="${value}" />
	<ffi:setProperty name="Currency" property="Format" value="#.00" />
	<ffi:setProperty name="FloatMath" property="Value1" value="${Currency.String}" />
	<ffi:setProperty name="FloatMath" property="Value2" value="0" />
	<span style="color:<ffi:getProperty name="negative${FloatMath.Less}" encode="false"/>;"><ffi:getProperty name="value"/></span>
</s:if>

<s:if test='%{"DayChange".equals(#parameters.type[0])}'>
	<ffi:setProperty name="StringUtil" property="Find" value="-" />
	<ffi:setProperty name="StringUtil" property="Value1" value="${value}" />
	<ffi:setProperty name="StringUtil" property="Replace" value="" />
	<ffi:setProperty name="Compare" property="Value1" value="${value}" />
	<ffi:setProperty name="Compare" property="Value2" value="${StringUtil.String}" />
	<span style="color:<ffi:getProperty name="positive${Compare.Equals}"/>;"><ffi:getProperty name="value"/></span>
</s:if>