<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<ffi:object id="GetLanguageList" name="com.ffusion.tasks.util.GetLanguageList" scope="session" />
<ffi:process name="GetLanguageList" />
    <script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown img.flag").addClass("flagvisibility");

            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
            });
                      
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a span").html(text);
                $(".dropdown dd ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
                $("#requestLocale").val(getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
            });
            $(".dropdown img.flag").toggleClass("flagvisibility");
            $.subscribe('changeLanguageCloseTopic', function(event,ui) {
            	$('#aLanChooser').removeClass('ui-state-focus');
    		});                
        });
    </script>
    		<div class="ui-widget">
				<div style="padding: 0pt 0.7em;" class="ui-state-error ui-corner-all"> 
					<p><span style="float: left; margin-right: 0.3em;" class="ui-icon ui-icon-alert"></span> 
					<strong><s:text name="jsp.default_521"/></strong> <s:text name="jsp.home_30"/></p>
				</div>
			</div>
 <dl id="sample" class="dropdown">
        <dt>
        	<sj:a 
				href="#"	
				indicator="indicator" 
				button="true" 
				buttonIcon="ui-icon-triangle-1-s"	
			><s:text name="jsp.home_4"/></sj:a>	
        </dt>
        <dd>
            <ul style="display: none;" class="ui-widget ui-widget-content ui-corner-all">       
            	<ffi:list collection="GetLanguageList.LanguagesList" items="language">
	            	<ffi:cinclude value1="${language.Language}" value2="zh_CN" operator="equals" >
						<li><a href="#"><s:text name="language.zh_CN"/><img class="flag" src="/cb/web/images/<ffi:getProperty name='language' property='Language' />.small.png" alt=""><span class="value"><ffi:getProperty name='language' property='Language' /></span></a></li>
					</ffi:cinclude>
					<ffi:cinclude value1="${language.Language}" value2="en_US" operator="equals" >
						<li><a href="#"><s:text name="language.en_US"/><img class="flag" src="/cb/web/images/<ffi:getProperty name='language' property='Language' />.small.png" alt=""><span class="value"><ffi:getProperty name='language' property='Language' /></span></a></li>
					</ffi:cinclude>
				</ffi:list>
            </ul>
        </dd>
    </dl>
    <s:form id="changeLocaleForm" namespace="/pages/jsp/home" action="locale">
     	<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
    	<s:hidden id="requestLocale" name="request_locale" value="%{#session.locale}"></s:hidden> 
    </s:form>