<%@ page import="com.ffusion.beans.banking.TransferDefines,
				 com.ffusion.beans.banking.Transfer"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- grid starts --%>
<ffi:setGridURL grid="GRID_PENDING_TRANSFERS" name="EditURL" url="/cb/pages/jsp/transfers/editTransferAction_input.action?transferID={0}&transferType={1}&recTransferID={2}&fromPortal=true&fromPendingTransfers=true" parm0="ID" parm1="TransferType" parm2="RecTransferID"  />
<ffi:setGridURL grid="GRID_PENDING_TRANSFERS" name="DeleteURL" url="/cb/pages/jsp/transfers/deleteTransferAction_init.action?transferID={0}&transferType={1}&recTransferID={2}&fromPortalPage=true" parm0="ID" parm1="TransferType" parm2="RecTransferID" />
<ffi:setGridURL grid="GRID_PENDING_TRANSFERS" name="ViewURL" url="/cb/pages/jsp/transfers/viewTransferAction_init.action?transferID={0}&transferType={1}&recTransferID={2}&fromPortalPage=true" parm0="ID" parm1="TransferType" parm2="RecTransferID" />

<script type="text/javascript">
	ns.pendingTransactionsPortal.initPendingTransfersGrid('Grid_pendingTransfersSummaryID', 'onPendingTransfersGridLoadComplete');
</script>

<ffi:setProperty name="pendingTransfersPortletTempURL" value="/pages/jsp/home/PendingTransfersPortletAction_execute.action?GridURLs=GRID_PENDING_TRANSFERS" URLEncrypt="true"/>
<s:url id="pendingTransfersSummaryUrl" value="%{#session.pendingTransfersPortletTempURL}"/>
<sjg:grid
	id="Grid_pendingTransfersSummaryID"
	caption=""
	dataType="json"
	href="%{pendingTransfersSummaryUrl}"
	pager="true"
	gridModel="gridModel"
	hoverrows="true"
	rowNum="5"
	navigator="true"
	navigatorAdd="false"
	navigatorDelete="false"
	navigatorEdit="false"
	navigatorRefresh="false"
	navigatorSearch="false"
	navigatorView="false"
	shrinkToFit="true"
	sortable="true"
	scroll="false"
	scrollrows="true"
	viewrecords="true"
	footerrow="false"
	userDataOnFooter="false"
	onGridCompleteTopics="onPendingTransfersGridLoadComplete">
	<sjg:gridColumn name="date" index="DATE_TO_POST" title="%{getText('jsp.default_137')}" sortable="true" width="75"/>

	<sjg:gridColumn name="combinedAccount" index="Accounts" title="%{getText('jsp.home_21')}" sortable="false" width="270" 
	formatter="ns.pendingTransactionsPortal.formatCombinedAccount"/>
	
	<ffi:cinclude value1="${UserType}" value2="Corporate" operator="equals">
			<sjg:gridColumn name="fromAccount.accountDisplayText" index="fromAccountNickName" title="%{getText('jsp.transfers_46')}" 
			sortable="true" width="150" hidden="true" hidedlg="false"/>
			
			<sjg:gridColumn name="toAccount.accountDisplayText" index="toAccountNickName" title="%{getText('jsp.transfers_77')}" 
			sortable="true" width="150" hidden="true" hidedlg="false"/>
	</ffi:cinclude>
	
	<ffi:cinclude value1="${UserType}" value2="Consumer" operator="equals">
			<sjg:gridColumn name="consumerFromAccountDisplayText" index="consumerFromAccount" title="%{getText('jsp.transfers_46')}"
			sortable="true" width="120" hidden="true" hidedlg="false"/>

			<sjg:gridColumn name="consumerToAccountDisplayText" index="consumerToAccount" title="%{getText('jsp.transfers_77')}"
			sortable="true" width="120" hidden="true" hidedlg="false"/>
	</ffi:cinclude>
		
	

	<sjg:gridColumn name="map.transactionDestinationCustom" index="type" title="%{getText('jsp.default_444')}"
		 sortable="true" width="40" hidden="true" hidedlg="false"/>

	<sjg:gridColumn name="map.Frequency" index="mapFrequencyValue" title="%{getText('jsp.default_215')}"
		formatter="ns.pendingTransactionsPortal.formatTransferFrequencyColumn" sortable="false" width="60" hidden="true" hidedlg="false"/>

	<sjg:gridColumn name="numberTransfers" index="numberTransfers" title="#"
		formatter="ns.pendingTransactionsPortal.formatTransferNumberColumn" sortable="false" width="40" hidden="true" hidedlg="false"/>
	
	<s:if test="%{#DisplayToAmount != #trueFlag}">
		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol" title="%{getText('jsp.default_489')}"
			formatter="ns.pendingTransactionsPortal.formatFromAmountColumn" sortable="true" width="80" hidden="true" hidedlg="false"/>
	</s:if>
	
	<s:elseif test="%{#DisplayToAmount== #trueFlag}">
		<sjg:gridColumn name="combinedAmount" index="Amount" title="%{getText('jsp.default_43')}" sortable="true" width="120" formatter="ns.pendingTransactionsPortal.formatCombinedAmount"/>
	   <sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amountValueCurrencyStringNoSymbol"
	   		title="%{getText('jsp.default_489')}" formatter="ns.pendingTransactionsPortal.formatFromAmountColumn" sortable="true" width="80" hidden="true"/>

		<sjg:gridColumn name="toAmountValue.currencyStringNoSymbol" index="toAmountValueCurrencyStringNoSymbol"
			title="%{getText('jsp.default_512')}" formatter="ns.pendingTransactionsPortal.formatToAmountColumn" sortable="true" width="60" hidden="true" hidedlg="true" />
	</s:elseif>

	<sjg:gridColumn name="status" index="statusName" title="%{getText('jsp.default_388')}" formatter="ns.pendingTransactionsPortal.formatTransferStatusColumn" sortable="true" 
	hidden="true" hidedlg="false" width="30"/>

	<sjg:gridColumn name="ID" index="action" title="%{getText('jsp.default_27')}" sortable="false"
		formatter="ns.pendingTransactionsPortal.formatPendingTransfersActionLinks" width="40" search="false" hidden="true" hidedlg="false"  cssClass="__gridActionColumn"/>

	<%-- HIDDEN COLUMNS USED FOR DATA HOLDING PURPOSE --%>
	<sjg:gridColumn name="date" index="DATE_TO_POST" title="%{getText('jsp.default_137')}" hidden="true"
		hidedlg="true" sortable="true" width="55"/>

	<sjg:gridColumn name="isAmountEstimated" index="isAmountEstimated" title="" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="isToAmountEstimated" index="isToAmountEstimated" title="" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="trackingID" index="trackingID" title="" width="80" hidden="true" hidedlg="true"/>

	
	<sjg:gridColumn name="fromAccount.displayText" index="fromAccountDisplayText" title="" width="80" hidden="true" hidedlg="true" />

	<sjg:gridColumn name="fromAccount.currencyCode" index="fromAccountCurrencyCode" title="" width="80" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="amount" index="amount" title="" sortable="true" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="toAccount.displayText" index="toAccountDisplayText" title="" width="80" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="toAccount.currencyCode" index="" title="%{getText('jsp.transfers_78')}" width="80" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="transactionIndexString" index="transactionIndexString" title="TransactionIndexString" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="transferDestination" index="transferDestination" title="" width="80" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="transferType" index="transferType" title="" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="canEdit" index="canEdit" title="" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="canDelete" index="canDelete" title="" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="frequency" index="frequency" title="" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="map.fromAccountText" index="fromAccountText" title="fromAccountText"
		search="false" sortable="false" hidden="true" hidedlg="true"/>
	
	<sjg:gridColumn name="map.isFromEntitled" index="isFromEntitled" title="isFromEntitled"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="map.toAccountText" index="toAccountText" title="toAccountText" search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="map.isToEntitled" index="isToEntitled" title="isToEntitled"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="map.enableButtons" index="enableButtons" title="enableButtons"
		search="false" sortable="false" hidden="true" hidedlg="true"/>

	<sjg:gridColumn name="fromAccount.nickName" index="fromAccountNickName"
		hidden="true" hidedlg="true" title="%{getText('jsp.transfers_46')}" sortable="false" width="150"/>

	<sjg:gridColumn name="toAccount.nickName" index="toAccountNickName" title="%{getText('jsp.transfers_77')}"
		hidden="true" hidedlg="true" sortable="false" width="150"/>

</sjg:grid>

<div id="Grid_pendingTransfersSummaryID_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('pmtTran_transfer,pending');"><s:text name="jsp.default.viewAllRecord" /></div>


<script>
$(document).ready(function(){
	if($("#Grid_pendingTransfersSummaryID tbody").data("ui-jqgrid") != undefined){
		$("#Grid_pendingTransfersSummaryID tbody").sortable("destroy");
	}
});

</script>
