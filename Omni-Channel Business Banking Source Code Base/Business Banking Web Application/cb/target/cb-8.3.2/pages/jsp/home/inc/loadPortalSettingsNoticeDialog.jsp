<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%-- this dialog can be reused when you want to display a notice message in portal page module --%>
<sj:dialog id="loadPortalSettingNoticeDialogID" cssClass="homeDialog" title="%{getText('jsp.home_142')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="350" buttons="{'%{getText(\"jsp.default_303\")}':function() { onLoadPortalPageWithPopup(); ns.common.closeDialog(); }, '%{getText(\"jsp.default_82\")}':function() { ns.common.closeDialog();} }">
</sj:dialog>