<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.accounts.AccountTypes"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummaries"%>
<%@ page import="com.ffusion.beans.accounts.AccountSummary"%>
<%@ page import="com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.beans.accounts.AssetAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.DepositAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.LoanAcctSummary"%>
<%@ page import="com.ffusion.beans.accounts.CreditCardAcctSummary"%>
<%@ page import="com.ffusion.beans.common.Currency"%>
<%@ page import="java.util.Collection"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
 <%@ page contentType="text/html; charset=UTF-8" %>

				<ffi:cinclude value1="${savedSort}" value2="" operator="notEquals">
					<ffi:setProperty name="BankingAccounts" property="SortedBy" value="${savedSort}"/>
				</ffi:cinclude>
				<ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,AND" />

<script type="text/javascript">
	ns.home.initExternalAccountsGrid('<s:property value="portletId"/>_externalAccountsSummaryID', 'onExternalAccountsGridLoadComplete');
</script>

<ffi:setProperty name="tempURL" value="/pages/jsp/home/AccountsSummaryPortletAction_loadExternalAccountsSummary.action" URLEncrypt="true"/>

<s:url id="externalAccountsSummaryUrl" value="%{#session.tempURL}"  escapeAmp="false"/>
		<sjg:grid
			id="%{portletId}_externalAccountsSummaryID"
			caption=""
			dataType="local"
			href="%{externalAccountsSummaryUrl}"
			pager="true"
			gridModel="mapListExternalGridModel"
			rowList="10,15,20"
			rowNum="5"
			shrinkToFit="true"
			sortable="true"
			scroll="false"
			scrollrows="true"
			viewrecords="true"
			footerrow="false"
			userDataOnFooter="false" 
			onGridCompleteTopics="onExternalAccountsGridLoadComplete"
			>

			<sjg:gridColumn name="consumerDisplayText" index="NICKNAME" title="%{getText('jsp.home.column.label.account')}" sortable="true" />
			<sjg:gridColumn name="bankName" index="BANKNAME" title="%{getText('jsp.home.column.label.bank')}" sortable="true"  hidden="true" hidedlg="true" />
			<sjg:gridColumn name="type" index="TYPESTRING" title="%{getText('jsp.home.column.label.account.type')}" sortable="true" hidden="true" hidedlg="true" />
			<sjg:gridColumn name="currencyCode" index="CURRENCY_CODE" title="%{getText('jsp.home.column.label.currency')}" sortable="true"  />
			<sjg:gridColumn name="displayClosingBalance" index="displayClosingBalance" title="%{getText('jsp.home.column.label.prior.balance')}" sortable="true" hidden="true" hidedlg="true" />
			<sjg:gridColumn name="displayCurrentBalance" index="CurrentBalanceField" title="%{getText('jsp.home.column.label.curr.balance')}" formatter ="ns.home.formatCurrentBalance"  sortable="true" />
			<sjg:gridColumn name="displayAvailableBalance" index="displayAvailableBalance" title="%{getText('jsp.home.column.label.avail.balance')}" sortable="true" hidden="true" hidedlg="true" />
			<sjg:gridColumn name="" index="" title="%{getText('jsp.home.column.label.action')}" hidden="true" sortable="false" />
			</sjg:grid>
			<div id="<s:property value="portletId"/>_externalAccountsSummaryID_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('userPreferencesVirtualMenu,accountPreferencesID');"><s:text name="jsp.default.viewAllRecord" /></div>
		<script>
		if($("#<s:property value='portletId'/>_externalAccountsSummaryID tbody").data("ui-jqgrid") != undefined){
			$("#<s:property value='portletId'/>_externalAccountsSummaryID tbody").sortable("destroy");
		}
		</script>

