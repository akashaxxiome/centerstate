<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>


<script type="text/javascript">
	$('#<s:property value="portletId"/>').portlet('bindCtrlEvent', '.edit', 'click', function(){
		ns.home.removeColumnChooser('<s:property value="portletId"/>');
	});
	function <s:property value="portletId"/>_switchToEdit() {
		$('#<s:property value="portletId"/>').portlet('edit');
	}
	
	<ffi:cinclude value1="en_US" value2="${UserLocale.Locale}" operator="equals" >
		ns.home.setHeaderDate('<s:property value="portletId"/>', '<s:url value="/pages/jsp/home/inc/portlet-header-date.jsp"/>', ' for ');
	</ffi:cinclude>
	<ffi:cinclude value1="en_US" value2="${UserLocale.Locale}" operator="notEqual" >
		ns.home.setHeaderDate('<s:property value="portletId"/>', '<s:url value="/pages/jsp/home/inc/portlet-header-date.jsp"/>', ' ');
	</ffi:cinclude>

</script>

<ffi:help id="home_portlet-cashflow"/>

<div id="cashFlowPortletContainer">


<div id="cashFlowTableContainer">

<%--There are no accounts so we should display a message to the user --%>
<ffi:cinclude value1="${CashFlowAccountSummaries.size}" value2="0" operator="equals">
	<script type="text/javascript">
		ns.home.removeColumnChooser('<s:property value="portletId"/>');
		ns.home.removeEditButton('<s:property value="portletId"/>');
	</script>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" nowrap>
					<span align="center">
						<s:text name="jsp.default_549"/>
					</span>
				</td>
			</tr>
	</table>
</ffi:cinclude> 


<ffi:cinclude value1="${CashFlowAccountSummaries.size}" value2="0" operator="notEquals">
<!-- this holds which tabs was saved to be opened on default load.
	corresponding tab will be selected i.e. selectedChart class will be applied -->
	<input type="hidden" value="<s:property value="%{#session.CashFlowSummarySettings.displayType}"/>" id="cashFlowSelectedTab">
	<ffi:cinclude value1="${CashFlowSummarySettings.displayType}" value2="Table" operator="notEquals">

		<s:if test='%{chartAccountIDs != null && !"".equals(chartAccountIDs)}'>
		 	<span class="<s:property value="portletId"/>_chart_area">
				<span id="<s:property value="portletId"/>_chart_data" style="display:none"></span>
				<div align="center" id="<s:property value="portletId"/>_no_data" style="display:none"><s:text name="jsp.home_140"/></div>
			</span>
			
		</s:if>
	</ffi:cinclude>
			
		<style>
			#<s:property value="portletId"/> .ui-jqgrid-ftable td{
				border:none;
			}
		</style>
		
		<%-- URL Encryption Edit By Dongning --%>
		<ffi:setProperty name="cashflowPortletTempURL" value="/pages/jsp/home/CashFlowPortletAction_loadCashFlowTableData.action?portletId=${portletId}&&displayType='Table'" URLEncrypt="true"/>
		<s:url id="cashflow_data_action" value="%{#session.cashflowPortletTempURL}"/>
		<sjg:grid id="%{portletId}_cashflow_datagrid"
						sortable="true"  
						dataType="local" 
						href="%{cashflow_data_action}" 
						hiddengrid="true"
						gridModel="gridModel" 
						pager="true" 
						rowList="%{#session.StdGridRowList}" 
		 				rowNum="5"
		 				rownumbers="false"
	     				navigator="true"
		 				navigatorAdd="false"
		 				navigatorDelete="false"
						navigatorEdit="false"
						navigatorRefresh="false"
						navigatorSearch="false"
						navigatorView="false"
						shrinkToFit="true"					
						scroll="false"
						scrollrows="true"					
						hoverrows="true"
						sortname="accountNum"
						viewrecords="true"
						footerrow="false" 
						userDataOnFooter="false"
						onGridCompleteTopics="%{portletId}_grid_load_complete">
						<sjg:gridColumn name="account.ID" index="ID" title="%{getText('jsp.account_12')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
			<sjg:gridColumn name="account.bankID" index="BANKID" title="%{getText('jsp.account_39')}" sortable="false" hidden="true" hidedlg="true" />
			<sjg:gridColumn name="account.routingNum" index="ROUTINGNUM" title="%{getText('jsp.account_183')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>	
				<%-- <sjg:gridColumn name="accountNum" index="accountNum" title="%{getText('jsp.default_15')}" width="180" align="left" sortable="true" formatter="ns.home.formatAccountColumn"/> --%>
				<sjg:gridColumn name="account.accountDisplayText" index="" title="Account" width="500" align="center" sortable="false" formatter="ns.home.formatCashflowAcctColumn" cssClass="datagrid_actionColumn" />
				<%-- <sjg:gridColumn name="nickName" index="nickName" title="%{getText('jsp.default_294')}" width="180" align="left" sortable="true" formatter="ns.home.customToAccountNickNameColumn" /> --%>
				<sjg:gridColumn name="" index="" title="Account" width="280" align="center" formatter="ns.home.customAccountAndNickNameColumn" sortable="false" hidden="true"/>
				<sjg:gridColumn name="openingBalString" index="openBal" title="%{getText('jsp.default_304')}" width="140" align="left" sortable="true" formatter="ns.home.openBalFormatter" hidden="false"/>
				<sjg:gridColumn name="itemsCount" index="itemsCount" title="%{getText('jsp.default_1')}" width="120" align="center" sortable="true" formatter="ns.home.openItemsFormatter" hidden="false"/>
				<sjg:gridColumn name="totalDebits" index="totalDebits" title="%{getText('jsp.default_434')}" width="90" align="right" sortable="true" formatter="ns.home.totalCreditsFormatter" hidden="true"/>
				<sjg:gridColumn name="totalCredits" index="totalCredits" title="%{getText('jsp.default_433')}" width="80" align="right" sortable="true" formatter="ns.home.totalCreditsFormatter" hidden="true"/>
				<sjg:gridColumn name="closingBalString" index="closeBal" title="%{getText('jsp.default_103')}" width="120" align="left" sortable="true" formatter="ns.home.closeBalFormatter"/>	
				
			</sjg:grid>
			
			<div class="sectionsubhead ltrow2_color hidden">
				<br/><s:text name="jsp.cash_total_from_all_pages" />
			</div>
			
			<div id="<s:property value="portletId"/>_cashflow_datagrid_GotoModule" class="portletGoToLinkDivHolder hidden" onclick="ns.shortcut.navigateToSubmenus('cashMgmt_cashflow');"><s:text name="jsp.default.viewAllRecord" /></div>
			
			
</ffi:cinclude>
</div>
<s:hidden value="%{portletId}" id="portletIdCashFlow"></s:hidden>
<div id="cashFlowDonutChart" class="hidden" style="text-align: center;"></div>
<div id="cashFlowBarChart" class="hidden" style="text-align: center;"></div>
<div id="cashFlowSettings" class="hidden marginBottom10"></div>  

</div>


<script type="text/javascript">
$(document).ready(function(){
	// array which holds the divs which will show the charts
	var tabTypes = ["cashFlowDonutChart","cashFlowBarChart","cashFlowSettings","cashFlowTableContainer"];
	var selectedTab=$('#cashFlowSelectedTab').val().trim();
	if(selectedTab=='Table'){
		$('#cashflowTable').addClass('selectedChart');
		var portletId = $('#portletIdCashFlow').val();
		ns.common.reloadFirstGridPage('#'+portletId+'_cashflow_datagrid');
		ns.common.resizeWidthOfGrids();
	}else if(selectedTab=='bar'){
		$('#cashflowBar').addClass('selectedChart');
		renderCashFlowBarChart();
		displayCashFlowChart("cashFlowBarChart");;
	}else if(selectedTab=='pie'){
		$('#cashflowDonut').addClass('selectedChart');
		renderCashFlowDonutChart();
		displayCashFlowChart("cashFlowDonutChart");;
	}
	
	function displayCashFlowChart(chartType){
		for(var i=0;i<tabTypes.length;i++){
			if(tabTypes[i]== chartType){
				$("#"+tabTypes[i]).show();
			}else{
				$("#"+tabTypes[i]).hide();
			}
		}
	}
	
	ns.home.changeCashFlowPortletView = function(dataType) {
		if(dataType =='donut'){
			renderCashFlowDonutChart();
			displayCashFlowChart("cashFlowDonutChart");
		}else if(dataType =='bar'){
			renderCashFlowBarChart();
			displayCashFlowChart("cashFlowBarChart");
		}else if(dataType =='dataTable'){
			$("#cashFlowTableContainer").show();
			displayCashFlowChart("cashFlowTableContainer");
			var portletId = $('#portletIdCashFlow').val();
			ns.common.reloadFirstGridPage('#'+portletId+'_cashflow_datagrid');
			ns.common.resizeWidthOfGrids();
		}else if(dataType =='chartSettings'){
			displayCashFlowChart("cashFlowSettings");
			 renderCashFlowSettings();
		}
	}
	
	function renderCashFlowBarChart() {
		$.publish("sap-ui-renderChartsTopic",{chartRoute: "_cashFlowSummaryBarChart"});
	}
	
	function renderCashFlowDonutChart() {
		$.publish("sap-ui-renderChartsTopic",{chartRoute: "_cashFlowSummaryDonutChart"});
	}
	
	function renderCashFlowSettings(){
		window.location.hash='';
		var aConfig = {
			url : "/cb/pages/jsp/home/CashFlowPortletAction_loadEditMode.action",
			containerId: '<s:property value="portletId"/>',
			data: {CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>',portletId:'<s:property value="portletId"/>'},
			success: function(data) {
				$('#cashFlowSettings').html("");
				$('#cashFlowSettings').html(data);
			}
		};

		//Portlet calls should be non blocking local ajax
		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}
});	
</script>