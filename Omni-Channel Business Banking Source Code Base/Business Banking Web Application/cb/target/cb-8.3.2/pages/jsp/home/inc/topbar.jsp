<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page
	import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<div id="usernameDiv" class="usernameDiv">


	<ffi:setProperty name="ShowAccountTab" value="False" />
	<ffi:cinclude value1="${SecureUser.AppType}"
		value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>"
		operator="equals">
		<ffi:setProperty name="ShowAccountTab" value="True" />
	</ffi:cinclude>
	<ffi:cinclude value1="${SecureUser.AppType}"
		value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>"
		operator="equals">
		<ffi:setProperty name="ShowAccountTab" value="True" />
	</ffi:cinclude>
	<ffi:cinclude value1="${SecureUser.AppType}"
		value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>"
		operator="equals">
		<ffi:setProperty name="ShowAccountTab" value="True" />
	</ffi:cinclude>
	<ffi:cinclude value1="${ShowAccountTab}" value2="True"
		operator="equals">
		<ffi:cinclude ifNotEntitled="<%= EntitlementsDefines.ACCOUNT_SETUP%>">
			<ffi:setProperty name="ShowAccountTab" value="False" />
		</ffi:cinclude>
	</ffi:cinclude>

	<!-- div to show user name -->
	<div class="topbarCenterDiv">
		<%-- <span class="UbuntuFont userFullName"> <ffi:cinclude
				value1="${SecureUser.AppType}"
				value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
				operator="equals">
				<s:property value="%{#session.BusinessEmployee.fullName}" />
			</ffi:cinclude> <ffi:cinclude value1="${SecureUser.AppType}"
				value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
				operator="notEquals">
				<ffi:getProperty name="User" property="PreferredGreeting" />
			</ffi:cinclude> <span class="usernameDownArrow ffiUiIcoSmall ffiUiIco-icon-downArrow">&nbsp;</span>
		</span> --%>
	</div>
	<ffi:object id="GetShortcuts"
		name="com.ffusion.tasks.util.GetShortcuts" scope="request" />
	<ffi:process name="GetShortcuts" />
	<!-- div to hold logo and bookmark input box -->
	<div class="topbarLeftDiv">
		<!-- to wrap bookmark / shortcut search bar -->
		<%-- <span class="topBarBankLogo"></span> --%>
		<div class="profileInfoContainer">
			<span class="avatar"></span> <span class="UbuntuFont userFullName">
				<ffi:cinclude value1="${SecureUser.AppType}"
					value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
					operator="equals">
					<s:property value="%{#session.BusinessEmployee.fullName}" />
				</ffi:cinclude> <ffi:cinclude value1="${SecureUser.AppType}"
					value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
					operator="notEquals">
					<ffi:getProperty name="User" property="PreferredGreeting" />
				</ffi:cinclude> <%-- <span class="usernameDownArrow ffiUiIcoSmall ffiUiIco-icon-downArrow">&nbsp;</span> --%>
			</span>

			<div class="profileActivityContent">
				<%-- <span onclick="ns.common.openDialog('updateProfile');">Update
					Profile</span> --%>
				<ffi:cinclude value1="${SecureUser.AppType}"
					value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
					operator="notEquals">
					<ffi:cinclude value1="${ShowAccountTab}" value2="True"
						operator="equals">
						<span onclick="ns.common.openDialog('accountPrefs');">Account
							Preferences</span>
					</ffi:cinclude>
				</ffi:cinclude>
				<span onclick="ns.common.openDialog('otherSetting');">Other
					Settings</span> <span onclick="ns.common.openDialog('siteNavigation');">Site
					Navigation</span>
				<ffi:cinclude value1="${SecureUser.AppType}"
					value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
					operator="equals">
					<span onclick="ns.common.openDialog('sessionActivityReport');">Session
						Activity</span>
				</ffi:cinclude>
				<ffi:cinclude value1="${SecureUser.AppType}"
					value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
					operator="notEquals">
					<ffi:cinclude
						ifEntitled="<%= EntitlementsDefines.SESSION_RECEIPT %>">
						<span onclick="ns.common.openDialog('sessionActivityReport');">Session
							Activity</span>
					</ffi:cinclude>
				</ffi:cinclude>

				<span id="bankLookupDivID"
					onclick="ns.common.openDialog('bankLookup');"><s:text
						name="jsp.default_554" /></span> <span id="fxConverterID"
					onclick="ns.common.openDialog('fx');"><s:text
						name="jsp.home_3" /></span> <span id="printerIcon"
					onclick="ns.common.printWholePage();">Print page</span>

				<%-- <div>
						<span onclick="ns.common.openDialog('updateProfile');"></span>
						<span>
							<span onclick="ns.common.openDialog('updateProfile');">Update Profile</span>
							<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
								<ffi:cinclude value1="${ShowAccountTab}" value2="True" operator="equals">
									<span onclick="ns.common.openDialog('accountPrefs');">Account Preferences</span>
								</ffi:cinclude>
							</ffi:cinclude>
						</span>
					</div> --%>
				<%-- <div>
						<span onclick="ns.common.openDialog('otherSetting');">Settings</span>
						<span onclick="ns.common.openDialog('siteNavigation');">Site Navigation</span>
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
							<span onclick="ns.common.openDialog('sessionActivityReport');">Session Activity</span>
						</ffi:cinclude>
						<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
							<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SESSION_RECEIPT %>" >
								<span onclick="ns.common.openDialog('sessionActivityReport');">Session Activity</span>
							</ffi:cinclude>
						</ffi:cinclude>
						
					</div> --%>
				<%-- <div>
						<span>Sign Out</span>
					</div> --%>
			</div>
		</div>

		<div class="menuFileHolder">
			<s:include value="/pages/jsp/home/inc/menu.jsp" />
		</div>
	</div>

	<!-- div to hold right side items e.g. global msgs / messages icon etc. -->
	<div class="topbarRightDiv">

		<div class="bookmarkBoxHolder">
			<input name="shortcutSearchInput"
				onclick="ns.shortcut.showAllShortcuts()" id="shortcutSearchInput"
				placeholder="Select shortcut..."
				class="ui-widget-content ui-corner-all bookmarkInputCls" /> <span
				onclick="ns.shortcut.showAllShortcuts()"
				class="bookmarkSearchIconHolder ffiUiIcoLarge ffiUiIco-icon-bookmark-search-icon">&nbsp;</span>
		</div>
		<div id="shortcutOptHolderDiv"></div>

		<!-- printer icon -->
		<li id="printerIcon"
			class="globalMsgIcoCls ffiUiIcoMedium ffiUiIco-icon-printer"
			onclick="ns.common.printWholePage();">&nbsp;</li>

		<!-- global messages icon -->
		<li id="globalMsgIcon"
			class="globalMsgIcoCls ffiUiIcoMedium ffiUiIco-icon-cloud">&nbsp;</li>

		<!-- approval icon -->
		<ffi:cinclude value1="${SecureUser.AppType}"
			value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
			operator="equals">
			<% boolean displayApprovalSmallPanel = false; %>
			<ffi:cinclude value1="${IsApprover}" value2="true" operator="equals">
				<% displayApprovalSmallPanel = true; %>
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>">
				<% displayApprovalSmallPanel = true; %>
			</ffi:cinclude>
			<ffi:cinclude
				ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>">
				<% displayApprovalSmallPanel = true; %>
			</ffi:cinclude>
			<ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES %>">
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES_RELEASE %>">
					<% displayApprovalSmallPanel = true; %>
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="${Business.dualApprovalMode}"
				value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				<% displayApprovalSmallPanel = true; %>
			</ffi:cinclude>

			<% if (displayApprovalSmallPanel) { %>
			<s:url id="approvalSmallPanelAction"
				value="/pages/jsp/approvals/approvalsPanelViewAction.action"
				escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<li class="approvalDropdownMainCls "
				data-sap-banking-type="topbarapprovals"><a
				class="globalMsgIcoCls ffiUiIcoMedium ffiUiIco-icon-approval"
				href="javascript:void(0);">
					<div id="approvalMsgIndicator" class="newNotifiationIndicatorCls"></div>
			</a>
				<ul>
					<sj:div id="approvalSmallPanelPortlet1"
						listenTopics="refreshApprovalSmallPanel"
						href="%{approvalSmallPanelAction}" deferredLoading="true"
						reloadTopics="home-page-topbarApprovalsTopic"
						onBeforeTopics="ns-common-localAjaxBeforeTopic"
						onCompleteTopics="ns-common-localAjaxCompleteTopic">
					</sj:div>
				</ul></li>
			<% } %>
		</ffi:cinclude>
		<!-- alert area -->
		<s:url id="alertsPanelViewAction"
			value="/pages/jsp/alert/alertsPanelViewAction.action"
			escapeAmp="false">
			<s:param name="messageBody" value="true" />
			<s:param name="messageSearchCriteria.unReadOnly" value="true" />
			<s:param name="messageSearchCriteria.readOnly" value="false" />
			<s:param name="messageSearchCriteria.pageInfo.pageSize" value="5" />
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>

		<!-- alert drop down menu -->
		<li class="alertDropdownMainCls" data-sap-banking-type="topbaralerts">
			<a class="globalMsgIcoCls ffiUiIcoMedium ffiUiIco-icon-alert"
			onclick="ns.home.openAlertDT()" href="javascript:void(0);">
				<div id="alertMsgIndicator" class="newNotifiationIndicatorCls"></div>
		</a>
			<ul class="alertFileHolder">
				<sj:div id="alertSmallPanelPortletTopBar"
					listenTopics="refreshAlertSmallPanel"
					href="%{alertsPanelViewAction}" deferredLoading="true"
					reloadTopics="home-page-topbarAlertsTopic"
					onBeforeTopics="ns-common-localAjaxBeforeTopic"
					onCompleteTopics="ns-common-localAjaxCompleteTopic">
				</sj:div>
			</ul>
		</li>

		<!-- message area -->
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MESSAGES %>">
			<s:url id="messagesAction"
				value="/pages/jsp/message/NotificationMessagesAction.action"
				escapeAmp="false">
				<s:param name="receivedItems" value="true" />
				<s:param name="messageBody" value="true" />
				<s:param name="messageSearchCriteria.unReadOnly" value="true" />
				<s:param name="messageSearchCriteria.readOnly" value="false" />
				<s:param name="messageSearchCriteria.pageInfo.pageSize" value="5" />
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}" />
			</s:url>

			<!-- message drop down menu -->
			<li class="msgDropdownMainCls"><a id="openMessages"
				class="globalMsgIcoCls ffiUiIcoMedium ffiUiIco-icon-mail"
				onclick="ns.home.openMessageDT()"
				data-sap-banking-type="topbarmessage" href="javascript:void(0);">
					<div id="mailMsgIndicator" class="newNotifiationIndicatorCls"></div>
			</a>
				<ul class="mailMsgFileHolder">
					<sj:div id="messagesListHolder"
						listenTopics="refreshMessageSmallPanel" href="%{messagesAction}"
						deferredLoading="true" reloadTopics="home-page-topbarMessageTopic"
						onBeforeTopics="ns-common-localAjaxBeforeTopic"
						onCompleteTopics="ns-common-localAjaxCompleteTopic">
					</sj:div>
				</ul></li>
		</ffi:cinclude>

		<!-- Logout button: check to validate whether to show activity report on logout -->
		<ffi:cinclude value1="${DISPLAY_SESSION_SUMMARY_REPORT}" value2="YES"
			operator="equals">
			<div class="headerBarLinks">
				<a id="logoutLink" class="logoutLinkCls" href="javascript:void(0)"
					onclick="ns.common.openDialog('sessionActivity'); return false;"
					style="float: right;"> <span><s:text name="signOut" /></span>
				</a>
			</div>
		</ffi:cinclude>
		<ffi:cinclude value1="${DISPLAY_SESSION_SUMMARY_REPORT}" value2="YES"
			operator="notEquals">
			<div class="headerBarLinks">
				<a id="logoutLink" class="logoutLinkCls" href="#"
					onclick="ns.common.logout();ns.common.onPageUnload();"
					style="float: right;"> <span><s:text name="signOut" /></span>
				</a>
			</div>
		</ffi:cinclude>

	</div>

	<div id="globalMsgIndicatorTxtHolder"
		class="globalMsgIndicatorTxtHolderCls">
		<span id="gmTxtHolder" onclick="ns.common.showGlobalMsg();">
			<!-- span to hold global message indicator text -->
		</span> <span class="gmIndicatorHolderCloseBtn sapUiIconCls icon-decline"
			onclick="ns.common.hideCloudNotification();"></span>
	</div>
</div>

<script type="text/javascript">

$(document).ready(function(){
	//ns.home.loadUnReadMessages(5);
	//ns.home.loadUnReadAlerts(5);
	if(toolBarController){
		toolBarController.init();
	}   
});

$(".mailMsgFileHolder").click(function(e) {
    e.stopPropagation();
});

$(".alertFileHolder").click(function(e) {
    e.stopPropagation();
});
</script>

