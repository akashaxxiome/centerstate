<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- selected module drop down items holder --%>
<ul class="moduleSubmenuDropdownHolder">
	<ffi:cinclude value1="${displayAccountHistory}" value2="true" operator="equals">
		<li onclick="ns.shortcut.goToMenu('acctMgmt_history');">
			<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-accountHistory"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_20"/></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${displayConsolidatedBalance}" value2="true" operator="equals">
		<li onclick="ns.shortcut.goToMenu('acctMgmt_consobal');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-conBalance"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_60"/></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${displayAccountBalance}" value2="true" operator="equals">
		<li onclick="ns.shortcut.goToMenu('acctMgmt_balance');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-accountBalance"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_19"/></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${displayTransactionSearch}" value2="true" operator="equals" >
		<li onclick="ns.shortcut.goToMenu('acctMgmt_search');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-tranSearch"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_221"/></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD %>" >
		<li onclick="ns.shortcut.goToMenu('acctMgmt_fileupload');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-fileUpload"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_92"/></span>
		</li>
	</ffi:cinclude>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CHECK_IMAGING %>" >
		<li onclick="ns.shortcut.goToMenu('acctMgmt_imagesearch');">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-imageSearch"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home_254"/></span>
		</li>
	</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ONLINE_STATEMENT %>" >
		<li onclick="$('li[menuId=acctMgmt_onlineStatements]').find('a').eq(0).click();">
			<span class="moduleDropdownMenuIcon ffiUiIcoSmall ffiUiIco-icon-medium-onlineStatement"></span>
    		<span class="moduleSubmenuDropdownLbl"><s:text name="jsp.home.onlineStatements.menu"/></span>
		</li>
	</ffi:cinclude>
	</ffi:cinclude>	
</ul>	