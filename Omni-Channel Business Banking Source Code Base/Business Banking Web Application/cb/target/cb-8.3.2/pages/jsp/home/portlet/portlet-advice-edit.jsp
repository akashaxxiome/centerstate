<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_save() {
		var idSelector = '#<s:property value="portletId"/>';
		
		var checkedItems = '';
		$('input[name="<s:property value='portletId'/>_checkbox"]').each(function(){
			if($(this).attr("checked")){
				if(checkedItems) {
					checkedItems += ",";
				}
				checkedItems += $(this).val();
			}
		});
		
		//URL Encryption: Change "$.get" method to "$.post" method
		var aConfig = {
			type: "POST",
			url: '<s:url action="AdvicePortletAction_updateSettings.action"/>',
			data: {portletId: '<s:property value="portletId"/>', advices: checkedItems,  CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
			success: function(data){
				ns.home.switchToView($(idSelector));
				$(idSelector).html(data);
				isPortalModified = true;
				ns.home.bindPortletHoverToTable($(idSelector));
				$.publish("common.topics.tabifyNotes");	
				if(accessibility){
					$(idSelector).setFocus();
				}
			}
		};
		
		var localAjaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(localAjaxSettings);
	}
	
	$('#<s:property value="portletId"/>_anchor').button({
		icons: {}
	});

</script>

<%-- ================ MAIN CONTENT START ================ --%>

			<ffi:help id="home_portlet-advice-edit" />

			<table border="0" cellspacing="0" cellpadding="0" align="center" class="marginBottom5">
				<tr>
					<td colspan="5">
						<span style="font-weight:bold;"><s:text name="jsp.home_187"/></span>
					</td>
				</tr>
				<tr><td colspan="5">&nbsp;</td></tr>
				<ffi:list collection="AdvisorsMaster" items="Advisor1,Advisor2">
					<tr valign="top">
						<ffi:setProperty name="Advisors" property="ContainsAdvisor" value="${Advisor1.Category}" />
							<td>
								<input type="checkbox" name="<s:property value="portletId"/>_checkbox" value="<ffi:getProperty name="Advisor1" property="Category"/>" <ffi:getProperty name="checked${Advisors.ContainsAdvisor}"/>>
							</td>
							<td>
								<ffi:getProperty name="Advisor1" property="Name"/>
							</td>
						<ffi:cinclude value1="" value2="${Advisor2}" operator="notEquals" >
							<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<ffi:setProperty name="Advisors" property="ContainsAdvisor" value="${Advisor2.Category}" />
							<td>
								<input type="checkbox" name="<s:property value="portletId"/>_checkbox" value="<ffi:getProperty name="Advisor2" property="Category"/>" <ffi:getProperty name="checked${Advisors.ContainsAdvisor}"/>>
							</td>
							<td>
								<ffi:getProperty name="Advisor2" property="Name"/>
							</td>
						</ffi:cinclude>
					</tr>
				</ffi:list>
				<tr><td colspan="5">&nbsp;</td></tr>
				<tr align="center">
					<td colspan="5">
						<a id="<s:property value="portletId"/>_anchor" href="javascript:void(0)" onclick="<s:property value="portletId"/>_save()" ><s:text name="jsp.default_303"/></a>
					</td>
				</tr>
			</table>
	

<%-- ================= MAIN CONTENT END ================= --%>

<div id="advicePortletEditDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>
