<%-- This page is used to show notifications panel in notifications portlet--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="home_portlet-notifications"/>

<div id="notificationsContainer-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<br>
<div id="notificationsContainer" class="ui-widget-content notificationBarStyles" style="padding:0px; height: 550px;">					
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MESSAGES %>" >
	<%-- Message Small Panel --%>
	   <%-- <s:url id="messagesAction" value="/pages/jsp/message/NotificationMessagesAction.action" escapeAmp="false">
		<s:param name="receivedItems" value="true"/>
		<s:param name="messageBody" value="false"/>
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
	   </s:url> --%>
	   
	<sj:div id="messageSmallPanelPortlet" cssClass="notificationBarStyles" onclick="ns.home.loadAllMessages();">
	</sj:div>
	</ffi:cinclude>
	<%-- Alert Small Panel --%>
	   <s:url id="alertsPanelViewAction" value="/pages/jsp/alert/alertsPanelViewAction.action" escapeAmp="false">
		<s:param name="messageBody" value="false"/>
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	   </s:url>
	<sj:div id="alertSmallPanelPortlet" listenTopics="refreshAlertSmallPanel" href="%{alertsPanelViewAction}">
	</sj:div>

	<%-- Approval Small Panel --%>
	<% boolean displayApprovalSmallPanel = false; %>
	<ffi:cinclude value1="${IsApprover}" value2="true" operator="equals">
		<% displayApprovalSmallPanel = true; %>
	    </ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>">
		<% displayApprovalSmallPanel = true; %>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>">
		<% displayApprovalSmallPanel = true; %>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES %>">
	    <ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRES_RELEASE %>">
			<% displayApprovalSmallPanel = true; %>
	    </ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<% displayApprovalSmallPanel = true; %>
	</ffi:cinclude>

	  <% if (displayApprovalSmallPanel) { %>
		    <s:url id="approvalSmallPanelAction" value="/pages/jsp/approvals/approvalsPanelViewAction.action" escapeAmp="false">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		   </s:url>
	   <sj:div id="approvalSmallPanelPortlet" listenTopics="refreshApprovalSmallPanel" href="%{approvalSmallPanelAction}">
	   </sj:div>
	<% } %>
</div>
<script type="text/javascript">

$(document).ready(function(){
//	ns.home.loadAllMessages(5);
})

</script>
<script type="text/javascript">

	(function($){

		$('#messageSmallPanelPortlet').pane({
			title: js_message_pane_title,
			expand: true,
			minmax: false,
			afterMinmax: function(pane){
				if(pane.expanded){
					$('#messageSmallPanelPortlet').css("overflow-y", "auto");
					//$.publish('refreshMessageSmallPanel');
					ns.home.loadAllMessages(5);
				}
			},
			displayLeftIcon: false,
			useSAPIcons : true,
			customIcons: ['icon-synchronize','icon-notification-2','icon-search'],
			customTips: [js_refresh,js_send_message,"read"],
			customCallbacks: [
			 	function(event){
			 		ns.home.loadAllMessages(5);
				},
			 	function(){
					//URL Encryption Edit By Dongning
					ns.home.quickSendMessageURL = '<ffi:urlEncrypt url="/cb/pages/jsp/message/SendMessageAction_init.action?initResponse=quickNewMessage"/>';
					ns.home.quickSendMessage(ns.home.quickSendMessageURL);
				},
			 	function(){
					ns.home.loadUnReadMessages(5);
				}]
		});

		$('#alertSmallPanelPortlet').pane({
			title: js_label_alerts,
			expand: false,
			minmax: false,
			help:false,
			url: '<ffi:urlEncrypt url="/cb/pages/jsp/alert/alerts.jsp"/>',
			afterMinmax: function(pane){
				if(pane.expanded){
					$('#alertSmallPanelPortlet').css("overflow-y", "auto");
					$.publish('refreshAlertSmallPanel');
				}
			},
			displayLeftIcon: false,
			useSAPIcons : true,
			customIcons: ['icon-synchronize'],
			customTips: [js_refresh],
			customCallbacks: [
			 	function(event){
					$.publish('refreshAlertSmallPanel');
				}]
		});

		$('#approvalSmallPanelPortlet').pane({
			title: js_approval_title,
			expand: false,
			minmax: false,
			url: '<ffi:urlEncrypt url="/cb/pages/jsp/home/inc/leftbar_approvalsmallpanel.jsp"/>',
			afterMinmax: function(pane){
				if(pane.expanded){
					$('#approvalSmallPanelPortlet').css("overflow-y", "auto");
					$.publish('refreshApprovalSmallPanel');
				}
			},
			displayLeftIcon: false,
			useSAPIcons : true,
			customIcons: ['icon-synchronize'],
			customTips: [js_refresh],
			customCallbacks: [
 			 	function(event){
 			 		$.publish('refreshApprovalSmallPanel');
 				}]
		});

		$('#notificationsContainer').panegroup();
		$('body').layout().resizeAll();
		
		var unreadMsgs = <s:property value="unreadMsgs"/>;
		var unreadAlerts = <s:property value="unreadAlerts"/>;
		var unreadApprovals = <s:property value="unreadApprovals"/>;
		var totalUnreadItems = <s:property value="totalUnreadItems"/>;
		
		if(unreadMsgs == undefined || unreadMsgs == ''){
			unreadMsgs = 0;
		}
		if(unreadAlerts == undefined || unreadAlerts == ''){
			unreadAlerts = 0;
		}
		if(unreadApprovals == undefined || unreadApprovals == ''){
			unreadApprovals = 0;
		}
		if(totalUnreadItems == undefined || totalUnreadItems == ''){
			totalUnreadItems = 0;
		}
		
		var unreadMsgsInt = parseInt(unreadMsgs);
		var unreadAlertsInt = parseInt(unreadAlerts);
		var unreadApprovalsInt = parseInt(unreadApprovals);
		
		if(totalUnreadItems>0){
			var totalItemsInt = parseInt(totalUnreadItems);
			$("#notificationCountIndicator").html(totalItemsInt).fadeIn();
		}else{
			var totalItemsInt = parseInt(totalUnreadItems);
			$("#notificationCountIndicator").html(totalItemsInt).fadeOut();
		}
		
		var $messageCountContainer = $('<span/>').attr('id', 'totalNewMsgsCount').addClass("newNotificationCounterCls totalNewMsgsCountCls").html(unreadMsgsInt);
		$('#messageSmallPanelPortlet').pane('addCustomContainer', $messageCountContainer);
		
		var $alertCountContainer = $('<span/>').attr('id', 'totalNewAlertsCount').addClass("newNotificationCounterCls totalNewAlertsCountCls").html(unreadAlertsInt);
		$('#alertSmallPanelPortlet').pane('addCustomContainer', $alertCountContainer);
		
		var $approvalCountContainer = $('<span/>').attr('id', 'totalNewApprovalsCount').addClass("newNotificationCounterCls totalNewApprovalsCountCls").html(unreadApprovalsInt);
		$('#approvalSmallPanelPortlet').pane('addCustomContainer', $approvalCountContainer);
		
	})(jQuery)
</script>