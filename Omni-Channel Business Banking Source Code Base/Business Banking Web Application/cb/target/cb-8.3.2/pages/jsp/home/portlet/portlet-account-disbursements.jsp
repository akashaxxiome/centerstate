<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Initializing tasks required to populate data --%>
<ffi:object id="GetDisbursementSummariesForAccountDisbursementPortlet" name="com.ffusion.tasks.disbursement.GetSummaries" scope="session"/>
<ffi:setProperty name="GetDisbursementSummariesForAccountDisbursementPortlet" property="DataClassification" value="<%=com.ffusion.tasks.portal.Task.DATA_CLASSIFICATION_PREVIOUSDAY %>"/>
<ffi:setProperty name="GetDisbursementSummariesForAccountDisbursementPortlet" property="BatchSize" value="50"/>
 
<%
  session.setAttribute("FFIGetDisbursementSummariesForAccountDisbursementPortlet", session.getAttribute("GetDisbursementSummariesForAccountDisbursementPortlet"));
%>
<%-- Initializing tasks required to populate data ends--%>

<ffi:setProperty name="populateAccDisbursementURL" value="/cb/pages/jsp/home/AccountDisbursementsPortletAction_loadViewDetailsMode.action" URLEncrypt="true"/>

<div id="accountDisbursementDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>
<div id="accountDisbursementDataDiv">
	<s:action name="AccountDisbursementsPortletAction_loadViewDetailsMode" namespace="/pages/jsp/home" executeResult="true">
	 	<s:param name="portletId" value="%{#portletId}" />
	 </s:action>
</div>