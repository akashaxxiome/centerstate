<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_ok() {
		var idSelector = '#<s:property value="portletId"/>';
		
		var rssUrlValue = $(idSelector + ' input').val();
		$.post('<s:url action="RssPortletAction_updateSettings.action"/>', {portletId: '<s:property value="portletId"/>', rssUrl: rssUrlValue, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'}, function(data){
			ns.home.switchToView($(idSelector));
			$(idSelector).html(data);
			isPortalModified = true;
			ns.home.bindPortletHoverToTable($(idSelector));
		});
	}
	
	$('#<s:property value="portletId"/>_anchor').button({
		icons: {}
	});

</script>

	<span style="font-size: 12px; font-weight: bold;"><s:text name="jsp.home_159"/> </span><input id="rssUrlInputId" class="ui-widget-content ui-corner-all txtbox" type="text" style="width: 60%;" />
	<a id="<s:property value='portletId'/>_anchor" href="javascript:void(0)" onclick="<s:property value='portletId'/>_ok()" ><s:text name="jsp.default_303"/></a>

