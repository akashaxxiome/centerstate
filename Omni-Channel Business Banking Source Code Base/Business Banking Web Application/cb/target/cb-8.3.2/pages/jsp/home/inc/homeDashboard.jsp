<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<div class="dashboardUiCls borderNone">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-home"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.home_105" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/homeSubmenuDropdown.jsp" />
    	</div>
    	
        
        <!-- dashboard items listing for transfers -->
        <div id="dbHomeSummary" class="dashboardSummaryHolderDiv">
	        <!-- Home summary link -->
			<sj:a id="homeSummaryBtnLink" button="true" cssClass="summaryLabelCls" title="%{getText('jsp.home_197')}">
				<s:text name="jsp.home_197"/>
			</sj:a>
			
			<!-- Add portlet link -->
			<sj:a id="addPortletBtnLink" onclick="ns.home.onOpenLayoutPortletsDialog('/cb/pages/jsp/home/inc/portal-portlets-dialog.jsp', '%{#session.CSRF_TOKEN}');" button="true" title="%{getText('jsp.home_248')}">
				<s:text name="jsp.home_248" />
			</sj:a>
			
			 <!-- Choose layout link -->
			<sj:a id="chooseLayoutBtnLink" onclick="ns.common.openDialog('portalLayoutDialog');" button="true" title="%{getText('jsp.home.label.select.layout')}">
				<s:text name="jsp.home.label.select.layout" />
			</sj:a>
			
			 <!-- Save layout link -->
			<sj:a id="saveLayoutBtnLink" onclick="ns.home.openSaveLayoutPage()" button="true" title="%{getText('jsp.home.label.save.layout')}">
				<s:text name="jsp.home.label.save.layout" />
			</sj:a>
		</div>
 </div>
   
<script>
$(document).ready(function(){
	$("#homeSummaryBtnLink").find("span").addClass("dashboardSelectedItem");
});
</script>