<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<sj:dialog
	id="helpMe"
	buttons="{
		'%{getText(\"jsp.default_102\")}':function() {
			$(this).dialog('close');
		 }
		}"
	autoOpen="false"
	modal="true"
	title="%{getText('jsp.home_103')}"
	cssClass="staticContentDialog"
	>
	<s:text name="jsp.home_103"/>
</sj:dialog>