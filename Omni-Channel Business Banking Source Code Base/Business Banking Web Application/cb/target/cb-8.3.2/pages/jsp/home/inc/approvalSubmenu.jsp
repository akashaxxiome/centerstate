<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@page import="java.util.Map"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%
	Map menuShortcuts = (Map)request.getAttribute("menuShortcuts");
%>
	<div class="submenuHolderBgCls">
			<div class="formActionItemPlaceholder">
				<div class="cardHolder">
					<div class="subMenuCard">
						<span class="cardTitle">_______________</span>
						<div class="cardSummary">
							<span>__________________________<br>__________________________<br>________</span>
						</div>
						<div class="cardLinks">
							<span>___________</span>
							<span>___________</span>
						</div> 
					</div>
				</div>	
			</div> 
			<ul id="tab16" class="mainSubmenuItemsCls">
				<ffi:cinclude value1="${show_approvalpending_menu}" value2="true" operator="equals">
					<li id="menu_approvals_pending" menuId="approvals_pending">
						<ffi:help id="approvals_pending" className="moduleHelpClass"/>
						<s:url id="pendingApprovalMenuURL" value="/pages/jsp/approvals/pendingApprovalIndex.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
							<s:param name="subpage" value="%{'pending'}"/>
						</s:url><sj:a
							href="%{pendingApprovalMenuURL}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_153"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_approvals_pending')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder">
							<div class="cardHolder">
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{pendingApprovalMenuURL}"/>','approvals_pending','PendingApprovals');"><s:text name="jsp.default.PendingApprovalsHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.PendingApprovals"/> 
										</span>
									</div>
									<div class="cardLinks">
							<span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{pendingApprovalMenuURL}"/>','approvals_pending','PendingApprovals','pendingTransactions');">Pending Transactions</span>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
											<span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{pendingApprovalMenuURL}"/>','approvals_pending','PendingApprovals','pendingPayees');">Pending Payees</span>
										</ffi:cinclude>
									</div>
								</div>
							</div>
							
							<%-- <span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{pendingApprovalMenuURL}"/>','approvals_pending','PendingApprovals','pendingTransactions');">Pending Transactions</span>
							<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
								<br/><span onclick="ns.common.loadSubmenuSummaryGrid('<s:property value="%{pendingApprovalMenuURL}"/>','approvals_pending','PendingApprovals','pendingPayees');">Pending Payees</span>
							</ffi:cinclude> --%>
						</div>
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-pendingApproval"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_34')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_153')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "approvals_pending");
					%> 
				</ffi:cinclude>
				<ffi:cinclude ifEntitled="<%= EntitlementsDefines.APPROVALS_ADMIN %>" >
					<li id="menu_approvals_workflow" menuId="approvals_workflow">
						<ffi:help id="approvals_workflow" className="moduleHelpClass"/>
						<s:url id="approvalWorkflowMenuURL" value="/pages/jsp/approvals/approvalWorkflowIndex.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
							<s:param name="subpage" value="%{'workflow'}"/>
						</s:url><sj:a
							href="%{approvalWorkflowMenuURL}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_33"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_approvals_workflow')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">	
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{approvalWorkflowMenuURL}"/>','approvals_workflow','ApprovalWorkflow');" ><s:text name="jsp.default.ApprovalWorkflowHeader"/> </span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ApprovalWorkflow"/>  
										</span>
									</div>
									<div class="cardLinks borderNone">
									</div>
								</div>
							</div>	
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{approvalWorkflowMenuURL}"/>','approvals_workflow','ApprovalWorkflow');" ><s:text name="SUMMARY"/></span> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-apprWorkflow"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_34')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_33')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "approvals_workflow");
					%> 
					<li id="menu_approvals_group" menuId="approvals_group">
						<ffi:help id="approvals_group" className="moduleHelpClass"/>
						<s:url id="approvalGroupsMenuURL" value="/pages/jsp/approvals/approvalGroupsIndex.jsp" escapeAmp="false">
							<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
							<s:param name="Initialize" value="true"/>
							<s:param name="subpage" value="%{'groups'}"/>
						</s:url><sj:a
							href="%{approvalGroupsMenuURL}"
							indicator="indicator"
							targets="notes"
							onCompleteTopics="subMenuOnCompleteTopics"
						><s:text name="jsp.home_32"/></sj:a>
						<span onclick="ns.common.showMenuHelp('menu_approvals_group')" class="sapUiIconCls icon-sys-help-2 submenuHelpIcon">&nbsp;</span> 
						<div class="formActionItemHolder hidden">
							<div class="cardHolder">	
								<div class="subMenuCard">
									<span class="cardTitle" onclick="ns.common.loadSubmenuSummary('<s:property value="%{approvalGroupsMenuURL}"/>','approvals_group','ApprovalGroups');" ><s:text name="jsp.default.ApprovalGroupHeader"/></span>
									<div class="cardSummary">
										<span><s:text name="jsp.default.ApprovalGroup"/> 
										</span>
									</div>
									<div class="cardLinks">
							<ffi:cinclude ifEntitled="Approvals Workflow Admin">
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{approvalGroupsMenuURL}"/>','approvals_group','ApprovalGroups','addGroupLink');" ><s:text name="jsp.approvals_21"/></span>
							</ffi:cinclude>
									</div>
								</div>
							</div>	
							<%-- <span onclick="ns.common.loadSubmenuSummary('<s:property value="%{approvalGroupsMenuURL}"/>','approvals_group','ApprovalGroups');" ><s:text name="SUMMARY"/></span> --%>
							<%-- <ffi:cinclude ifEntitled="Approvals Workflow Admin">
								<span onclick="ns.common.loadSubmenuSummary('<s:property value="%{approvalGroupsMenuURL}"/>','approvals_group','ApprovalGroups','addGroupLink');" ><s:text name="jsp.approvals_21"/></span>
							</ffi:cinclude> --%>
						</div>  
						<span class="ffiUiIco-menuItem ffiUiIco-menuItem-apprGroups"></span>
					</li>
					<s:set var="tempMenuName" value="%{getText('jsp.home_34')}" scope="session" />
					<s:set var="tempSubMenuName" value="%{getText('jsp.home_32')}" scope="session" />
					<% 
						menuShortcuts.put(session.getAttribute("tempMenuName")+" :: "+session.getAttribute("tempSubMenuName"), "approvals_group");
					%> 
				</ffi:cinclude>
			</ul>
</div>
