<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<%@ page import="com.ffusion.beans.accounts.Account"%>
<%@ page import="com.ffusion.beans.reporting.ExportFormats"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
 <%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="account-history-export.jsp"/>
<ffi:setProperty name="topMenu" value="accounts"/>
<ffi:setProperty name="subMenu" value="history"/>
<ffi:setProperty name="sub2Menu" value="viewhistory"/>
<ffi:setL10NProperty name="PageTitle" value="Account History Export"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
 <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">

	<title><!--L10NStart-->Financial Fusion<!--L10NEnd--></title>
	<%--
	<ffi:include page="${PathExt}inc/timeout.jsp" />
	<link rel="stylesheet" type="text/css"  href="<ffi:getProperty name="ServletPath" />FFretail.css">
	--%>
</head>

<script>
$("#dateSelectBox").selectmenu({width: 250});


function setRadioButton( fieldName, formName ) {
	//alert('setRadioButton=>'+fieldName);
	document.formExportHistory["date1"].checked=false;
	document.formExportHistory["date2"].checked=false;
	document.formExportHistory["date3"].checked=false;
	formName[fieldName].checked = true;
	formExport();
}

function formExport() {
	if(document.formExportHistory["date1"].checked ) {
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>&ExportHistory.SearchCriteriaValue"].value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>";
	} else if(document.formExportHistory["date2"].checked ) {
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>&ExportHistory.SearchCriteriaValue"].value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_SINCE_LAST_EXPORT %>";
	} else {
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>&ExportHistory.SearchCriteriaValue"].value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>";
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_START_DATE %>&ExportHistory.SearchCriteriaValue"].value = document.formExportHistory["ExportHistory.StartDate"].value;
		document.formExportHistory["ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_END_DATE %>&ExportHistory.SearchCriteriaValue"].value = document.formExportHistory["ExportHistory.EndDate"].value;
	}

	//document.formExportHistory.submit();
}

function formExport2() {
	document.formExportHistory2.submit();
}
</script>

<body>

<%session.setAttribute("advancedSearch",request.getParameter("advancedSearch"));%>
<%session.setAttribute("SetAccountExportID",request.getParameter("SetAccountExportID"));%>

<ffi:cinclude value1="${advancedSearch}" value2="true" operator="notEquals">
	<% session.removeAttribute("FFIExportHistory");%>
	<% session.removeAttribute("ExportHistory");%>
</ffi:cinclude>



	<ffi:cinclude value1="${ExportHistory}" value2="" operator="equals">
		<ffi:object name="com.ffusion.tasks.banking.ExportHistory" id="ExportHistory" scope="session" />
		<ffi:setProperty name="ExportHistory" property="IsConsumerBanking" value="true"/>
		<ffi:setProperty name="<%= com.ffusion.tasks.reporting.GenerateReportBase.RPT_STYLESHEET_NAME %>" value="FFretail.css"/>
		<ffi:setProperty name="ExportHistory" property="bankingServiceName" value="com.ffusion.services.banking.interfaces.BankingService"/>
	    <ffi:setProperty name="ExportHistory" property="MaxDaysInDateRange" value="180"/>
        <ffi:setProperty name="ExportHistory" property="EnforceMaxDateRangeCheck" value="true"/>
        <ffi:setProperty name="ExportHistory" property="EnforceNoFutureDatesCheck" value="true"/>
        <ffi:setProperty name="ExportHistory" property="DataClassification" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_ALL %>"/>
		<ffi:setProperty name="ExportHistory" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION %>"/>
		<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DATA_CLASSIFICATION_ALL %>"/>


		<ffi:cinclude value1="${advancedSearch}" value2="true" operator="notEquals">
			<ffi:cinclude value1="${GetPagedTransactions.StartDate}" value2="" operator="notEquals">
				<ffi:setProperty name="ExportHistory" property="StartDate" value="${GetPagedTransactions.StartDate}" />
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_START_DATE %>"/>
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.StartDate}"/>
			</ffi:cinclude>

			<ffi:cinclude value1="${GetPagedTransactions.EndDate}" value2="" operator="notEquals">
				<ffi:setProperty name="ExportHistory" property="EndDate" value="${GetPagedTransactions.EndDate}" />
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_END_DATE %>"/>
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.EndDate}"/>
			</ffi:cinclude>

			<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
			<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>" />
			<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}" />
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="equals">
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${GetPagedTransactions}" value2="" operator="equals">
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>"/>
			</ffi:cinclude>
		</ffi:cinclude>

	</ffi:cinclude>

	<%
		session.setAttribute("FFIExportHistory", session.getAttribute("ExportHistory"));
	%>


	<ffi:cinclude value1="${coreAccounts}" value2="" operator="notEquals">
		<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
		<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${coreAccounts}" />
	</ffi:cinclude>

	<table width="741" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="1%">
			<%--
			<img src="/efs/efs/multilang/grafx/clear.gif" width="10" height="20">
			--%>
			</td>
			<td width="98%" valign="top">
				<div align="center">
				<ffi:cinclude value1="${advancedSearch}" value2="true" operator="equals">
				<%--
					<form action="<ffi:urlEncrypt url="${SecureServletPath}ExportHistory"/>" method="post" name="formExportHistory2">
				--%>
				<form action="/cb/pages/jsp/account/ConsumerExportHistoryAction.action" method="post" name="formExportHistory2">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<%-- set default export format --%>
				<ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="" operator="equals">
					<ffi:setProperty name="ExportHistory" property="ExportFormat" value="<%= ExportFormats.FORMAT_COMMA_DELIMITED %>" />
				</ffi:cinclude>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="header" colspan="2"><!--L10NStart-->Export History<!--L10NEnd--></td>
							<td class="header" width="1%">
								<div align="right">
								<%--
								<img src="/efs/efs/multilang/grafx/header_corner.gif" width="21" height="19" align="absmiddle">
								--%>
								</div>
							</td>
						</tr>
						<tr>
							<td class="filter_table" width="25%" >&nbsp;</td>
							<td class="filter_table" width="20%" >&nbsp;</td>
							<td class="filter_table" width="55%" >&nbsp;</td>
						</tr>
						<tr>
							<td class="filter_table">&nbsp;</td>
							<td class="filter_table" valign="top" >
								<div align="right" class="txt_normal"><!--L10NStart-->Account:<!--L10NEnd--></div>
							</td>
							<td class="filter_table">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<ffi:list collection="CheckedAccounts" items="checkedAccount1">
										<ffi:setProperty name="SetAccount" property="ID" value="${checkedAccount1}" />
										<ffi:process name="SetAccount"/>

										<ffi:cinclude value1="${getExternalAccountValue}" value2="${Account.CoreAccount}" operator="equals">
											<%-- If this is an external account, use aggregration--%>
											<ffi:setProperty name="accountType" value="${aggregation}"/>
										</ffi:cinclude>
										<ffi:cinclude value1="${getCoreAccountValue}" value2="${Account.CoreAccount}" operator="equals">
											<%-- If this is an internal account, use accounts --%>
											<ffi:setProperty name="accountType" value="${accounts}"/>
										</ffi:cinclude>

										<ffi:cinclude value1="${accountType}" value2="${accounts}" operator="equals">
											<tr>
												<td class="filter_table">
													<ffi:getProperty name="Account" property="ConsumerDisplayText"/>&nbsp;
												</td>
											</tr>
										</ffi:cinclude>
									</ffi:list>
								</table>
							</td>
						</tr>

						<%-- Determine whether the user has entered a relative --%>
						<%-- or absolute date range. --%>
						<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
						<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>" operator="equals">
							<tr>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table" >
									<div align="right" class="txt_normal"><!--L10NStart-->Date Range:<!--L10NEnd--></div>
								</td>
								<td nowrap valign="top" class="filter_table">
									<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
									<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="equals">
										(<!--L10NStart-->None Selected<!--L10NEnd-->)&nbsp;
									</ffi:cinclude>
									<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
										<ffi:getProperty name="GetPagedTransactions" property="SearchCriteriaValue"/>&nbsp;
									</ffi:cinclude>
								</td>
							</tr>
						</ffi:cinclude>
						<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
						<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>" operator="notEquals">
							<tr>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table" >
									<div align="right" class="txt_normal"><!--L10NStart-->Start:<!--L10NEnd--></div>
								</td>
								<td nowrap valign="top" class="filter_table">
									<ffi:cinclude value1="${GetPagedTransactions.StartDate}" value2="" operator="equals">
										(<!--L10NStart-->None Selected<!--L10NEnd-->)&nbsp;
									</ffi:cinclude>
									<ffi:cinclude value1="${GetPagedTransactions.StartDate}" value2="" operator="notEquals">
										<ffi:getProperty name="GetPagedTransactions" property="StartDate"/>&nbsp;
									</ffi:cinclude>
								</td>
							</tr>
							<tr>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table">
									<div align="right" class="txt_normal"><!--L10NStart-->End:<!--L10NEnd--></div>
								</td>
								<td nowrap valign="top" class="filter_table">
									<ffi:cinclude value1="${GetPagedTransactions.EndDate}" value2="" operator="equals">
										(<!--L10NStart-->None Selected<!--L10NEnd-->)&nbsp;
									</ffi:cinclude>
									<ffi:cinclude value1="${GetPagedTransactions.EndDate}" value2="" operator="notEquals">
										<ffi:getProperty name="GetPagedTransactions" property="EndDate"/>&nbsp;
									</ffi:cinclude>
								</td>
							</tr>
						</ffi:cinclude>

						<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>"/>
							<tr>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table">
									<div align="right" class="txt_normal"><!--L10NStart-->Transaction Type:<!--L10NEnd--></div>
								</td>
								<td nowrap valign="top" class="filter_table">
								<ffi:cinclude value1="${GetTransactionTypes}" value2="" operator="equals">
	<ffi:object name="com.ffusion.tasks.util.GetTransactionTypes" id="GetTransactionTypes" scope="request" />
	<ffi:setProperty name="GetTransactionTypes" property="Application" value="<%= com.ffusion.tasks.util.GetTransactionTypes.APPLICATION_CONSUMER %>" />
	<ffi:process name="GetTransactionTypes" />
</ffi:cinclude>
	<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="equals">
<!--L10NStart-->All Transactions<!--L10NEnd-->
	</ffi:cinclude>
	<ffi:list collection="GetTransactionTypes.TypeList" items="Type">

	<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="${Type.Key}" operator="equals">
	<ffi:getProperty name="Type" property="Value"/>

	</ffi:cinclude>

	</ffi:list>&nbsp;
								</td>
							</tr>

						<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_NUM %>"/>
						<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
							<tr>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table">
									<div align="right" class="txt_normal"><!--L10NStart-->Reference Number:<!--L10NEnd--></div>
								</td>
								<td nowrap valign="top" class="filter_table">
									<ffi:getProperty name="GetPagedTransactions" property="SearchCriteriaValue"/>&nbsp;
								</td>
							</tr>
						</ffi:cinclude>

						<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>"/>
						<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
							<tr>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table">
									<div align="right" class="txt_normal"><!--L10NStart-->Minimum Amount:<!--L10NEnd--></div>
								</td>
								<td nowrap valign="top" class="filter_table">
									<ffi:getProperty name="GetPagedTransactions" property="SearchCriteriaValue"/>&nbsp;
								</td>
							</tr>
						</ffi:cinclude>

						<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>"/>
						<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
							<tr>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table">
									<div align="right" class="txt_normal"><!--L10NStart-->Maximum Amount:<!--L10NEnd--></div>
								</td>
								<td nowrap valign="top" class="filter_table">
									<ffi:getProperty name="GetPagedTransactions" property="SearchCriteriaValue"/>&nbsp;
								</td>
							</tr>
						</ffi:cinclude>

						<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DESCRIPTION %>"/>
						<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
							<tr>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table">
									<div align="right" class="txt_normal"><!--L10NStart-->Description:<!--L10NEnd--></div>
								</td>
								<td nowrap valign="top" class="filter_table">
									<ffi:getProperty name="GetPagedTransactions" property="SearchCriteriaValue"/>&nbsp;
								</td>
							</tr>
						</ffi:cinclude>
				</table>

						<ffi:cinclude value1="${GetPagedTransactions.StartDate}" value2="" operator="notEquals">
							<ffi:setProperty name="ExportHistory" property="StartDate" value="${GetPagedTransactions.StartDate}" />
							<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_START_DATE %>"/>
							<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.StartDate}"/>
						</ffi:cinclude>

						<ffi:cinclude value1="${GetPagedTransactions.EndDate}" value2="" operator="notEquals">
							<ffi:setProperty name="ExportHistory" property="EndDate" value="${GetPagedTransactions.EndDate}" />
							<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_END_DATE %>"/>
							<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.EndDate}"/>
						</ffi:cinclude>


					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>" />
					<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}" />
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>"/>
					</ffi:cinclude>

					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>" />
					<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}"/>
					</ffi:cinclude>

					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>"/>
					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>" />
					<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}" />
					</ffi:cinclude>
					<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="equals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_VALUE_ALL_TRANS_TYPE %>" />
					</ffi:cinclude>

					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_NUM %>"/>
					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_NUM %>" />
					<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}" />
					</ffi:cinclude>

					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>"/>
					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>" />
					<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}" />
					</ffi:cinclude>

					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>"/>
					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>" />
					<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}" />
					</ffi:cinclude>

					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DESCRIPTION %>"/>
					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DESCRIPTION %>" />
					<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="" operator="notEquals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}" />
					</ffi:cinclude>

					<%-- Sort, may not need all these --%>
					<ffi:setProperty name="ExportHistory" property="SortCriteriaName" value="${GetPagedTransactions.SortCriteriaName}" />
					<ffi:setProperty name="ExportHistory" property="SortCriteriaOrdinal" value="${GetPagedTransactions.SortCriteriaOrdinal}" />
					<ffi:setProperty name="ExportHistory" property="CompareSortCriterion" value="${GetPagedTransactions.CompareSortCriterion}" />
					<ffi:setProperty name="ExportHistory" property="SortedBy" value="${GetPagedTransactions.SortedBy}" />

					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
					<ffi:cinclude value1="${coreAccounts}" value2="" operator="notEquals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${coreAccounts}" />
					</ffi:cinclude>

					<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SHOW_MEMO %>"/>
					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_SHOW_MEMO %>" />
					<ffi:cinclude value1="${GetPagedTransactions.SearchCriteriaValue}" value2="true" operator="equals">
						<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${GetPagedTransactions.SearchCriteriaValue}"/>
					</ffi:cinclude>
				</ffi:cinclude>


				<ffi:cinclude value1="${advancedSearch}" value2="true" operator="notEquals">
				<form  action="/cb/pages/jsp/account/ConsumerExportHistoryAction.action"  method="post" name="formExportHistory">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<input type="hidden" name="ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>&ExportHistory.SearchCriteriaValue" value="">
				<input type="hidden" name="ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_START_DATE %>&ExportHistory.SearchCriteriaValue" value="">
				<input type="hidden" name="ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_END_DATE %>&ExportHistory.SearchCriteriaValue" value="">
					<%-- set default export format --%>
					<ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="" operator="equals">
						<ffi:setProperty name="ExportHistory" property="ExportFormat" value="<%= ExportFormats.FORMAT_QIF %>" />
					</ffi:cinclude>

					<ffi:setProperty name="ExportHistory" property="SortCriteriaOrdinal" value="1"/>
					<ffi:setProperty name="ExportHistory" property="SortCriteriaName" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SORT_CRITERIA_DATE %>"/>
					<ffi:setProperty name="ExportHistory" property="SortCriteriaAsc" value="False"/>

					<ffi:cinclude value1="${SetAccountExport}" value2="" operator="equals" >
						<ffi:object id="SetAccountExport" name="com.ffusion.tasks.accounts.SetAccount" scope="session"/>
						<ffi:setProperty name="SetAccountExport" property="AccountsName" value="BankingAccounts" />
						<ffi:setProperty name="SetAccountExport" property="AccountName" value="Account" />
					</ffi:cinclude>

					<ffi:setProperty name="SetAccountExport" property="ID" value="${SetAccountID}"/>

					<%-- come from portal page --%>
					<ffi:cinclude value1="${SetAccountExport.ID}" value2="" operator="equals" >
						<ffi:setProperty name="SetAccountExport" property="ID" value="${SetAccountExportID}"/>
					</ffi:cinclude>

					<ffi:cinclude value1="${SetAccountExport.ID}" value2="" operator="equals" >
						<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />
						<ffi:list collection="BankingAccounts" items="Account1" startIndex="1" endIndex="1">
							<ffi:setProperty name="SetAccountExport" property="ID" value="${Account1.ID}" />
						</ffi:list>
					</ffi:cinclude>

					<ffi:process name="SetAccountExport"/>

					<ffi:cinclude value1="${getExternalAccountValue}" value2="${Account.CoreAccount}" operator="equals">
						<%-- If this is an external account, use aggregration--%>
						<ffi:setProperty name="accountType" value="${aggregation}"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${getCoreAccountValue}" value2="${Account.CoreAccount}" operator="equals">
						<%-- If this is an internal account, use accounts --%>
						<ffi:setProperty name="accountType" value="${accounts}"/>
					</ffi:cinclude>

					<%-- if coming from portal page the account string must be set --%>
					<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
					<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="" operator="equals">
							<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" 					value="${Account.ID}:${Account.BankID}:${Account.RoutingNum}:${Account.NickName}:${Account.CurrencyCode}" />
					</ffi:cinclude>

					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="header" colspan="2"><!--L10NStart-->Export History<!--L10NEnd--></td>
							<td class="header" width="1%">
								<div align="right">
								<%--
								<img src="/efs/efs/multilang/grafx/header_corner.gif" width="21" height="19" align="absmiddle">
								--%>
								</div>
							</td>
						</tr>
						<tr>
							<td class="filter_table" width="25%" >&nbsp;</td>
							<td class="filter_table" width="10%" >&nbsp;</td>
							<td class="filter_table" width="65%" >&nbsp;</td>
						</tr>
						<tr>
							<td class="filter_table" >
								<div align="right" class="txt_normal"><!--L10NStart-->Account:<!--L10NEnd--></div>
							</td>
							<td class="filter_table" colspan="2" ><ffi:getProperty name="Account" property="ConsumerDisplayText"/>&nbsp;</td>
						</tr>
						<tr>
							<td class="filter_table" width="30%" >
								<div align="right" class="txt_normal"><!--L10NStart-->Date:<!--L10NEnd--></div>
							</td>
							<td class="filter_table" width="10%" >
								<input type="radio" name="date1"
									<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_RELATIVE %>" operator="equals">
										checked
									</ffi:cinclude>
									onClick="setRadioButton( 'date1', document.formExportHistory );" >
							</td>
							<td class="filter_table" width="60%" >
								<select class="form_elements" id="dateSelectBox" name="ExportHistory.SearchCriteriaKey=<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>&ExportHistory.SearchCriteriaValue" style="width:105px;" onChange="setRadioButton( 'date1', document.formExportHistory );">
									<%-- We want to retrieve the last selected item in the drop box
											It is stored in the task's "criteria" keyed to this name --%>
									<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>

									<option value=""
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Select...<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_DAY %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_DAY %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Today<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_YESTERDAY %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_YESTERDAY %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Yesterday<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_WEEK %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_WEEK %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Current Week<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_WEEK %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_WEEK %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Last Week<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_7_DAYS %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_7_DAYS %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Last 7 Days<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_30_DAYS %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_30_DAYS %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Last 30 Days<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_60_DAYS %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_60_DAYS %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Last 60 Days<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_90_DAYS %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_90_DAYS %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Last 90 Days<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_MONTH %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_CURRENT_MONTH %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Current Month<!--L10NEnd-->
									</option>

									<option value="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_MONTH %>"
									<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_VALUE_LAST_MONTH %>" operator="equals"> Selected </ffi:cinclude> >
										<!--L10NStart-->Last Month<!--L10NEnd-->
									</option>
								</select>
							</td>
						</tr>
						<ffi:object name="com.ffusion.tasks.banking.GetLastExportedDate" id="GetLastExportedDate" scope="session" />
						<ffi:setProperty name="GetLastExportedDate" property="ReportType" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY %>"/>
						<ffi:setProperty name="GetLastExportedDate" property="DateFormat" value="${UserLocale.DateTimeFormat}"/>
						<ffi:process name="GetLastExportedDate"/>
						<tr>
							<td class="filter_table" >
								<div class="txt_normal"> </div>
							</td>
							<td class="filter_table" >
								<input type="radio" name="date2" onClick="setRadioButton( 'date2', document.formExportHistory );" <ffi:cinclude value1="${GetLastExportedDate.LastExportedDate}" value2="" operator="equals"> disabled </ffi:cinclude>>
							</td>
							<td class="filter_table" ><!--L10NStart-->Since Last Export<!--L10NEnd--></td>
						</tr>

						<tr>
							<td class="filter_table">&nbsp;</td>
							<td class="filter_table" >&nbsp;</td>
							<td class="filter_table" ><span class="txt_copyright"><!--L10NStart-->Last Export Date:<!--L10NEnd--></span>&nbsp;&nbsp;
								<ffi:cinclude value1="" value2="${GetLastExportedDate.LastExporteddate}" operator="equals">
									<!--L10NStart-->Not Previously Exported<!--L10NEnd-->
								</ffi:cinclude>
								<ffi:cinclude value1="" value2="${GetLastExportedDate.LastExporteddate}" operator="notEquals">
									<ffi:getProperty name="GetLastExportedDate" property="LastExportedDate"/>
								</ffi:cinclude>
							</td>
						</tr>
						<tr>
							<td class="filter_table">&nbsp; </td>
							<td class="filter_table" nowrap>
							<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE %>"/>
								<input type="radio" name="date3" <ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE_TYPE_ABSOLUTE %>" operator="equals"> checked </ffi:cinclude> onClick="setRadioButton( 'date3', document.formExportHistory );">
							</td>
							<td class="filter_table" nowrap><!--L10NStart-->Date Range<!--L10NEnd--></td>
						</tr>
						<tr>
							<td class="filter_table">&nbsp; </td>
							<td class="filter_table" >
								<div align="right" class="txt_normal"><!--L10NStart-->Start:<!--L10NEnd--></div>
							</td>
							<td nowrap valign="top" class="filter_table">

						<%
							String startDate = "";
							com.ffusion.tasks.banking.ExportHistory exHistory= (com.ffusion.tasks.banking.ExportHistory)session.getAttribute("ExportHistory");
							if(exHistory!=null)
							{
								startDate = exHistory.getStartDate();
								if(startDate==null || "".equals(startDate))
								{
									com.ffusion.beans.user.UserLocale userLocale = (com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale");
									startDate = userLocale.getDateFormat();
								}
							}

							request.setAttribute("startDate",startDate);
						%>

							<%--
								<input type="text" class="form_fields" name="ExportHistory.StartDate" size="12" <ffi:cinclude value1="${ExportHistory.StartDate}" value2="" operator="equals"> value="<ffi:getProperty name="UserLocale" property="DateFormat"/>" </ffi:cinclude> <ffi:cinclude value1="${ExportHistory.StartDate}" value2="" operator="notEquals"> value="<ffi:getProperty name="ExportHistory" property="StartDate"/>" </ffi:cinclude> onFocus="setRadioButton( 'date3', document.formExportHistory );">&nbsp;
							--%>

								<div id="testFocusDiv" onFocus="setRadioButton( 'date3', document.formExportHistory );">
									<sj:datepicker title="%{getText('Choose_start_date')}"
									value="%{#attr.startDate}"
									id="ExportHistory.StartDate"
									name="ExportHistory.StartDate"
									label="Start Date"
									maxlength="10"
									onFocusTopics=""
									onChangeTopics=""
									buttonImageOnly="true"
									cssClass="ui-widget-content ui-corner-all"/>
								</div>

							</td>
						</tr>
						<tr>
							<td class="filter_table">&nbsp; </td>
							<td class="filter_table">
								<div align="right" class="txt_normal"><!--L10NStart-->End:<!--L10NEnd--></div>
							</td>
							<td nowrap valign="top" class="filter_table">

							<%
							String endDate = "";
							com.ffusion.tasks.banking.ExportHistory exHistory2= (com.ffusion.tasks.banking.ExportHistory)session.getAttribute("ExportHistory");
							if(exHistory2!=null)
							{
								endDate = exHistory2.getEndDate();
								if(endDate==null || "".equals(endDate))
								{
									com.ffusion.beans.user.UserLocale userLocale = (com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale");
									endDate = userLocale.getDateFormat();
								}
							}

							request.setAttribute("endDate",endDate);
						%>

							<%--
							<input type="text" class="form_fields" name="ExportHistory.EndDate" size="12" <ffi:cinclude value1="${ExportHistory.EndDate}" value2="" operator="equals"> value="<ffi:getProperty name="UserLocale" property="DateFormat"/>" </ffi:cinclude> <ffi:cinclude value1="${ExportHistory.EndDate}" value2="" operator="notEquals"> value="<ffi:getProperty name="ExportHistory" property="EndDate"/>" </ffi:cinclude> onFocus="setRadioButton( 'date3', document.formExportHistory );">&nbsp;
							--%>

							<div id="testFocusDiv2" onFocus="setRadioButton( 'date3', document.formExportHistory );">
									<sj:datepicker title="%{getText('Choose_to_date')}"
									value="%{#attr.endDate}"
									id="ExportHistory.EndDate"
									name="ExportHistory.EndDate"
									label="End Date"
									maxlength="10"
									buttonImageOnly="true"
									cssClass="ui-widget-content ui-corner-all"/>
							</div>

							</td>
						</tr>
					</table>

				</ffi:cinclude>


					<br>
					<table width="282" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td class="txt_normal" colspan="5"><!--L10NStart-->Select a format:<!--L10NEnd--></td>
						</tr>
						<ffi:cinclude value1="${advancedSearch}" value2="true" operator="notEquals">
							<tr>
								<td class="filter_table" width="10%">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_QIF %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_QIF %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" nowrap><!--L10NStart-->Quicken (QIF)<!--L10NEnd--></td>
								<td class="filter_table">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_PDF %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_PDF %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" nowrap><!--L10NStart-->PDF<!--L10NEnd--></td>
							</tr>
							<tr>
								<td class="filter_table">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_QFX %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_QFX %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" nowrap><!--L10NStart-->Quicken (Web Connect)<!--L10NEnd--></td>
								<td class="filter_table">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_TEXT %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_TEXT %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" nowrap><!--L10NStart-->Plain Text<!--L10NEnd--></td>
							</tr>
							<tr>
								<td class="filter_table">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_OFX %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_OFX %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" nowrap><!--L10NStart-->Microsoft Money<!--L10NEnd--></td>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table" nowrap>&nbsp;</td>
							</tr>
							<tr>
								<td class="filter_table">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_COMMA_DELIMITED %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_COMMA_DELIMITED %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" nowrap>
									<div align="left"><!--L10NStart-->Comma Delimited<!--L10NEnd--></div>
								</td>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table" nowrap>&nbsp;</td>
							</tr>
							<tr>
								<td class="filter_table" width="10%">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_TAB_DELIMITED %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_TAB_DELIMITED %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" width="40%" nowrap><!--L10NStart-->Tab Delimited<!--L10NEnd--></td>
								<td class="filter_table">&nbsp;</td>
								<td class="filter_table" nowrap>&nbsp;</td>
							</tr>
						</ffi:cinclude>
						<ffi:cinclude value1="${advancedSearch}" value2="true" operator="equals">
							<tr>
								<td class="filter_table">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_COMMA_DELIMITED %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_COMMA_DELIMITED %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" nowrap>
									<div align="left"><!--L10NStart-->Comma Delimited<!--L10NEnd--></div>
								</td>
								<td class="filter_table">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_PDF %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_PDF %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" nowrap><!--L10NStart-->PDF<!--L10NEnd--></td>
							</tr>
							<tr>
								<td class="filter_table" width="10%">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_TAB_DELIMITED %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_TAB_DELIMITED %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" width="40%" nowrap><!--L10NStart-->Tab Delimited<!--L10NEnd--></td>
								<td class="filter_table">
									<div align="right">
										<input type="radio" name="ExportHistory.ExportFormat" value="<%= ExportFormats.FORMAT_TEXT %>" <ffi:cinclude value1="${ExportHistory.ExportFormat}" value2="<%= ExportFormats.FORMAT_TEXT %>" operator="equals"> checked </ffi:cinclude>>
									</div>
								</td>
								<td class="filter_table" nowrap><!--L10NStart-->Plain Text<!--L10NEnd--></td>
							</tr>
						</ffi:cinclude>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="5" class="totals_row">&nbsp;</td>
						</tr>
					</table>
					<br>
					<p></p>
					<sj:a button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" >
						<s:text name="jsp.default_82" />
					</sj:a>

					<ffi:cinclude value1="${advancedSearch}" value2="true" operator="notEquals">
						<sj:submit
							id="exportHistorySubmit"
							button="true"
							buttonIcon="ui-icon-refresh"
							value="%{getText('jsp.default_195')}"
						/>

					</ffi:cinclude>
					<ffi:cinclude value1="${advancedSearch}" value2="true" operator="equals">
						<sj:submit
							id="exportHistorySubmit"
							button="true"
							buttonIcon="ui-icon-refresh"
							value="%{getText('jsp.default_195')}"
						/>

					</ffi:cinclude>
				</div>
				</table>
				<div align="center"><br>
				</div>
			</td>
			<td width="1%">
			</td>
		</tr>
	</table>
	</form>

<div id="resultMessageDiv">

</div>


</body>
</html>

<ffi:setProperty name="BackURL" value="${SecurePath}account-history-export.jsp"/>
<ffi:removeProperty name="GetLastExportedDate"/>
<ffi:removeProperty name="SetAccountID"/>
<ffi:removeProperty name="coreAccounts"/>
<ffi:removeProperty name="GetTransactionTypes"/>



