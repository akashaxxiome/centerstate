<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript">
	$('#<s:property value="portletId"/>_stock_symbol').selectmenu({width:'125px'});
	$('#<s:property value="portletId"/>_show_type').selectmenu({width:'125px'});
	$('#<s:property value="portletId"/>_searchButton').button({
		icons: {}
	});
	$('#<s:property value="portletId"/>_anchor').button({
		icons: {}
	});
	
	function <s:property value="portletId"/>_stock_search() {
		var symbolType = $('#<s:property value="portletId"/>_stock_symbol').val();
		var showType = $('#<s:property value="portletId"/>_show_type').val();
		var symbol = $('#<s:property value="portletId"/>_SymbolCompany').val();
		if(showType == "chart") 
			lookupStockChart(symbol, symbolType);
		else if(showType == "quote") 
			lookupStockQuote(symbol, symbolType);
	}
	
	function <s:property value="portletId"/>_save(mode) {
		var idSelector = '#<s:property value="portletId"/>';
		var symbolTable = $('#<s:property value="portletId"/>_symbol_table');
		var symbolForm = symbolTable.find('tr[name="<s:property value="portletId"/>_symbol_form"]');
		var stocksParam = '';
		symbolForm.each(function(){
			var symbol1 = $(this).find('input[name="stock_symbol1"]').val();
			var counter1 = $(this).find('input[name="stock_counter1"]').val();
			var shares1 = $(this).find('input[name="stock_shares1"]').val();
			var price1 = $(this).find('input[name="stock_price1"]').val();
			var symbol2 = $(this).find('input[name="stock_symbol2"]').val();
			var counter2 = $(this).find('input[name="stock_counter2"]').val();
			var shares2 = $(this).find('input[name="stock_shares2"]').val();
			var price2 = $(this).find('input[name="stock_price2"]').val();
			stocksParam += 'counter=' + counter1 + ',' +
							'symbol=' + symbol1 + ',' +
							'shares=' + shares1 + ',' +
							'purchasePrice=' + price1 + ';' +
							'counter=' + counter2 + ',' +
							'symbol=' + symbol2 + ',' +
							'shares=' + shares2 + ',' +
							'purchasePrice=' + price2 + ';';
		});

		var aConfig = {
			type: "POST",
			url: '<s:url action="StockPortletAction_updateSettings.action"/>',
			data: {portletId: '<s:property value="portletId"/>', stockParams: stocksParam, displayMode: mode, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
			success: function(data){
				if(!mode || mode != "editMode") {
					ns.home.switchToView($(idSelector));
				}
				$(idSelector).html(data);			
				$.publish("common.topics.tabifyNotes");	
				if(accessibility){
					$(idSelector).setFocus();
				}

				isPortalModified = true;
				ns.home.bindPortletHoverToTable($(idSelector));
			}
		};

		var ajaxSettings = ns.common.applyLocalAjaxSettings(aConfig);
		$.ajax(ajaxSettings);
	}
	
	function <s:property value="portletId"/>_addSymbol(symbol) {
		var symbolTable = $('#<s:property value="portletId"/>_symbol_table');
		var symbolForm = symbolTable.find('tr[name="<s:property value="portletId"/>_symbol_form"]');
		var hasEmpty = false;
		for(var i=0;i < symbolForm.length; i++) {
			var symbolLine = symbolForm.eq(i);
			if(symbolLine) {
				var symbol1 = symbolLine.find('input[name="stock_symbol1"]');
				if(symbol1) {
					if(!symbol1.val()) {
						symbol1.val(symbol);
						hasEmpty = true;
						break;
					}
				}
				var symbol2 = symbolLine.find('input[name="stock_symbol2"]');
				if(symbol2) {
					if(!symbol2.val()) {
						symbol2.val(symbol);
						hasEmpty = true;
						break;
					}
				}
			}
		}
		if(hasEmpty)
			<s:property value="portletId"/>_save('editMode');
	}
</script>

<%-- ================ MAIN CONTENT START ================ --%>
<ffi:help id="home_portlet-stock-edit"/>
<!-- <table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr>
		<td align="center"> -->
			<table border="0" cellspacing="3" cellpadding="3" width="100%" id="dvcStockPortfolio">
				<tr>
					<td valign="top">
							<table id="<s:property value="portletId"/>_symbol_table" border="0" cellspacing="0" cellpadding="2"  width="100%">
								<tr>
									<td colspan="6"></td>
								</tr>
								<tr align="center">
										<td align="center" class="sectionsubhead"><s:text name="jsp.home_208"/></td>
										<td align="center" class="sectionsubhead"><s:text name="jsp.home_1"/></td>
										<td align="center" class="sectionsubhead"><s:text name="jsp.home_167"/></td>
										<td align="center" class="sectionsubhead"><s:text name="jsp.home_208"/></td>
										<td align="center" class="sectionsubhead"><s:text name="jsp.home_1"/></td>
										<td align="center" class="sectionsubhead"><s:text name="jsp.home_167"/></td>
								</tr>
								<ffi:list collection="StocksEdit" items="Stock1,Stock2">
									<tr align="center" name="<s:property value="portletId"/>_symbol_form">
										<td align="center">
											<input style="overflow: hidden;" class="txtbox ui-widget-content ui-corner-all" type="text" name="stock_symbol1" value="<ffi:getProperty name="Stock1" property="Symbol"/>" size="5" maxlength="10">
											<input type="hidden" name="stock_counter1" value="<ffi:getProperty name="Stock1" property="Counter"/>"/>
										</td>
										<td align="center"><input style="overflow: hidden;" class="txtbox ui-widget-content ui-corner-all" type="text" name="stock_shares1" value="<ffi:getProperty name="Stock1" property="Shares"/>" size="5" maxlength="10"></td>
										<td align="center"><input style="overflow: hidden;" class="txtbox ui-widget-content ui-corner-all" type="text" name="stock_price1" value="<ffi:getProperty name="Stock1" property="PurchasePrice"/>" size="5" maxlength="10"></td>
										<td align="center">
											<input style="overflow: hidden;" class="txtbox ui-widget-content ui-corner-all" type="text" name="stock_symbol2" value="<ffi:getProperty name="Stock2" property="Symbol"/>" size="5" maxlength="10">
											<input type="hidden" name="stock_counter2" value="<ffi:getProperty name="Stock2" property="Counter"/>"/>
										</td>
										<td align="center"><input style="overflow: hidden;" class="txtbox ui-widget-content ui-corner-all" type="text" name="stock_shares2" value="<ffi:getProperty name="Stock2" property="Shares"/>" size="5" maxlength="10"></td>
										<td align="center"><input style="overflow: hidden;" class="txtbox ui-widget-content ui-corner-all" type="text" name="stock_price2" value="<ffi:getProperty name="Stock2" property="PurchasePrice"/>" size="5" maxlength="10"></td>
									</tr>
								</ffi:list>
							</table>
					</td>
				</tr>

				<tr>
					<td colspan="6" align="center">
							<table border="0" cellspacing="0" cellpadding="2" style="font-size:10px">
								<tr>
									<td>
										<select id="<s:property value="portletId"/>_stock_symbol" class="txtbox buttonSizeReset" name="SymbolCompanyFlag" style="vertical-align: middle">
											<option value="company" selected><s:text name="jsp.default_536"/></option>
											<option value="symbol"><s:text name="jsp.home_195"/></option>
										</select>
									</td>
									<td>&nbsp;<input class="ui-widget-content ui-corner-all" id="<s:property value="portletId"/>_SymbolCompany" size="25" maxlength="100">&nbsp;</td>
									<td>
										<select id="<s:property value="portletId"/>_show_type" class="txtbox buttonSizeReset" name="StockLookup" style="vertical-align: middle">
											<option value="quote"><s:text name="jsp.home_168"/></option>
											<option value="chart"><s:text name="jsp.home_192"/></option>
										</select>
									</td>
									<td>
										<a id="<s:property value="portletId"/>_searchButton" href="javascript:void(0)" onclick="<s:property value="portletId"/>_stock_search()"><s:text name="jsp.default_548"/></a>
									</td>
								</tr>
								<tr>
									<td class="columndata" colspan="4" align="center">
										<s:text name="jsp.home_71"/><ffi:setProperty name="GetCurrentDate" property="DateFormat" value="M/d/yyyy h:mm a (z)" />
										<ffi:getProperty name="GetCurrentDate" property="Date"/>.
									</td>
								</tr>
							</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<center>
							<a id="<s:property value="portletId"/>_anchor" href="javascript:void(0)" onclick="<s:property value="portletId"/>_save()" ><s:text name="jsp.default_303"/></a>
						</center>
					</td>
				</tr>
			</table>
		<!-- </td>
	</tr>
	
</table> -->
<%-- ================= MAIN CONTENT END ================= --%>
	
<div id="stockPortletEditDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>