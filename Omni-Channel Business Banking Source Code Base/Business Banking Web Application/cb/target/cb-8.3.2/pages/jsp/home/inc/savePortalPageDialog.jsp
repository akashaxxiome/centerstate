<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<sj:dialog id="savePortalPageDialogID" title="%{getText('jsp.home_176')}" position="['center','middle']" 
	hideEffect="clip" autoOpen="false" showEffect="fold" width="430" height="220" modal="true" resizable="false" overlayColor="#000"
	onOpenTopics="onSavePortalPageDialogOpen" overlayOpacity="0.7" cssClass="staticContentDialog homeDialog" cssStyle="overflow:visible;">
	<div id="saveLayoutBand"></div>
	<div id="deleteLayoutBand" class="hidden layoutSaveDialog" style="text-align:center; margin-top: 30px;">
		<br/>
		<span><s:text name="jsp.home.layoutDeleteConfirm" />&nbsp;<strong>'<span id="deleteLayoutName"></span>'</strong>?</span>
		<input type="hidden" name="deleteLayoutId" id="deleteLayoutId" />
		<div class="ui-widget-header customDialogFooter">
			<sj:a button="true" 
				  onClickTopics="cancelDeleteLayout"
	        ><s:text name="jsp.default_82"/></sj:a>
        
           <sj:a
			id="deleteLayoutBtn"
            button="true" 
            onClickTopics="deleteLayoutClick"
            onCompleteTopics="deleteLayoutComplete"
			onErrorTopics="errorDeleteLayout"
            onSuccessTopics="successDeleteLayout"
         	>
         		<s:text name="jsp.default_162"/>
         	</sj:a>
    	</div>     
	</div>
</sj:dialog>