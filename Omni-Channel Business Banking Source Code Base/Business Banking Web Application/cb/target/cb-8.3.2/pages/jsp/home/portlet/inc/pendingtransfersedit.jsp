<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>

<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/transfers/transfer%{#session.minVersion}.css'/>"></link>
<div id="operationresult">
	<div id="resultmessage"><s:text name="jsp.default_498"/></div>
</div><!-- result -->


<%-- Added to resolve Transfer Portlet edit issue.  --%>
<ffi:setProperty name="Transfer" property="EditURL" value=""/>
<ffi:setProperty name="Transfer" property="DeleteURL" value=""/>


<%-- This JSP is responsible for  --%>
<s:include value="/pages/jsp/home/portlet/inc/pendingTransactionNavigation.jsp"></s:include>

