<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<script type="text/javascript">
	function <s:property value="portletId"/>_switchToEdit() {
		$('#<s:property value="portletId"/>').portlet('edit');
	}
</script>

<%-- Since Forecasts does not support the Size property, we use the following to determine if it's empty --%>
<ffi:setProperty name="hasForecastItems" value="false"/>
<ffi:list collection="Forecasts" items="item">
	<ffi:setProperty name="hasForecastItems" value="true"/>
</ffi:list>

<div id="WeatherPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick='<s:property value="portletId"/>_switchToEdit()'><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

<table border="0" cellspacing="0" cellpadding="2" width="100%">
<%-- IF Forecasts COLLECTION IS EMPTY --%>
<ffi:cinclude value1="false" value2="${hasForecastItems}" operator="equals">

		<tr>
			<td align="center" height="20">
				<br><br><br><br><br>
				<s:text name="jsp.home_129"/><br>
				<s:text name="jsp.home_56"/> <a href='javascript:void(0)' class="sapUiLnk" onclick='<s:property value="portletId"/>_switchToEdit()'><s:text name="jsp.default_179"/></a> <s:text name="jsp.home_217"/>
			</td>
		</tr>
</ffi:cinclude>

<ffi:help id="home_portlet-weather" />
<%-- IF Forecasts COLLECTION IS NOT EMPTY --%>
<ffi:cinclude value1="true" value2="${hasForecastItems}" operator="equals">
    <tr>
		<td>
			<table border="0"  border="0" cellspacing="0" cellpadding="2" >
			
			
			<% 
				java.util.HashMap stateCities = (java.util.HashMap) config.getServletContext().getAttribute(com.ffusion.tasks.portal.Task.STATE_CITY_LIST_CONTEXT); 
				java.util.HashMap countryCities = (java.util.HashMap) config.getServletContext().getAttribute(com.ffusion.tasks.portal.Task.COUNTRY_CITY_LIST_CONTEXT); 
				String cityAbbr = null;
				int index = -1;
				String stateName = null;
				String countryName = null;
				String cityName = null;
				com.ffusion.beans.portal.GeographicUnit city = null;
				java.util.HashMap cityFullNameMap = new java.util.HashMap(); 
			%>
			<%-- Init cityFullNameMap --%>
			<ffi:list collection="ForecastSettings" items="setting">
				<ffi:getProperty name="setting" property="City" assignTo="cityAbbr" />
				<%
				if(cityAbbr == null){
					cityAbbr = "";
				}else{
					if (cityAbbr.endsWith(" US") ) {
						index = cityAbbr.lastIndexOf(" US");
						stateName = cityAbbr.substring(index - 2, index);
						cityName = cityAbbr.substring(0, index - 2 ) + "," + stateName;
					} else {
						index = cityAbbr.lastIndexOf(' ');
						countryName = cityAbbr.substring(index + 1);
						cityName = cityAbbr.substring(0, index + 1);
					}
					cityFullNameMap.put( cityName, cityAbbr );
				}
				%>
			</ffi:list>
			<ffi:list collection="Forecasts" items="Forecast">
				<ffi:getProperty name="Forecast" property="City" assignTo="cityAbbr" />
				<% 
					com.ffusion.beans.portal.GeographicUnits cities = null;
					cityAbbr = (String) cityFullNameMap.get(cityName);
					
					if(cityAbbr == null){
						cityAbbr = "";
					}else{
						if (cityAbbr.endsWith(" US") ) {
							// this is a US city. find the state
							
							index = cityAbbr.lastIndexOf(" US");
							stateName = cityAbbr.substring(index - 2, index);
							cities = (com.ffusion.beans.portal.GeographicUnits) stateCities.get(stateName);
						} else {
							index = cityAbbr.lastIndexOf(' ');
							countryName = cityAbbr.substring(index + 1);
							cities = (com.ffusion.beans.portal.GeographicUnits) countryCities.get(countryName);
						}
						
						for( int i = 0 ; i < cities.size() ; i++ ) {
							city = (com.ffusion.beans.portal.GeographicUnit) cities.get(i);
							if( city.getKey().equals(cityAbbr) )
								break;
							city = null;
						}
					}
				%>
			<tr>
				<td colspan="5" style="font-weight:bold">
					<span class="ui-icon-carat-1-e ui-icon" style="float:left"></span>
					<%= city != null ? city.getName() : "" %>
				</td>
			</tr>
			<tr>
				
				<ffi:list collection="Forecast" items="ForecastDay1">
					<td style="width: 20%; padding:0 20px" align="center"><ffi:getProperty name="ForecastDay1" property="Day"/><br>
						<img src="<s:url value="/web" />/grafx/<ffi:getProperty name="ImgExt"/>/<ffi:getProperty name="ForecastDay1" property="SkyCondition"/>.gif"><br>
						<span style="color: #A51029">
							<s:text name="jsp.home_104"/> <ffi:getProperty name="ForecastDay1" property="High"/>
							<ffi:cinclude value1="${ForecastSettingsTemp.Scale}" value2="C" operator="notEquals" >&ordm;F</ffi:cinclude>
							<ffi:cinclude value1="${ForecastSettingsTemp.Scale}" value2="C" operator="equals" >&ordm;C</ffi:cinclude>
						</span><br>
						<span style="color: #101884"><s:text name="jsp.home_119"/> <ffi:getProperty name="ForecastDay1" property="Low"/> 
							<ffi:cinclude value1="${ForecastSettingsTemp.Scale}" value2="C" operator="notEquals" >&ordm;F</ffi:cinclude>
							<ffi:cinclude value1="${ForecastSettingsTemp.Scale}" value2="C" operator="equals" >&ordm;C</ffi:cinclude>
						</span>
					</td>
				</ffi:list>
			</tr>
			<tr>
				<td colspan="5">&nbsp;</td>
			</tr>
			
			</ffi:list>
	</table> 
	</td>
</tr>
    <ffi:removeProperty name="CityDisplay"/>
</ffi:cinclude>
</table>
