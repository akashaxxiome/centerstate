<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<div id="<s:property value="portletId"/>_stockSymbol">nomatch</div>
<%-- ================ MAIN CONTENT START ================ --%>
<ffi:help id="home_portlet-stock-no-match"/>
<table border="0" cellspacing="0" cellpadding="0" >
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr align="center">
		<td>
			<%-- Sorry, no stock symbols for companies named <b><s:property value="symbol"/></b> were found.  Please try again.--%>
			<s:text name="jsp.home_190"/> 
		</td>
	</tr>
</table>
<%-- ================= MAIN CONTENT END ================= --%>
<div id="stockPortletDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	<ul class="portletHeaderMenu">
		<li><a href='#' onclick='<s:property value="portletId"/>_switchToEdit()'><span class="sapUiIconCls icon-technical-object" style="float:left;"></span><s:text name='jsp.home.settings' /></a></li>
		<ffi:cinclude value1="${CURRENT_LAYOUT.type}" value2="system" operator="notEquals">
			<li><a href='#' onclick="ns.home.removePortlet('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-cancel-2" style="float:left;"></span><s:text name='jsp.home.closePortlet' /></a></li>
		</ffi:cinclude>
		<li><a href='#' onclick="ns.home.showPortletHelp('<s:property value="portletId"/>')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	</ul>
</div>

