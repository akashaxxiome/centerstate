<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<sj:dialog id="accessKeysViewDialogID"  cssStyle="display:none;"
               hideEffect="fade" title="%{getText('jsp.common.accessKeyDialog.accessKeyDialogTitle')}" autoOpen="true"
               width="400" modal="true" resizable="false"
			   closeOnEscape="true" buttons="{'%{getText('jsp.default_102')}': function() { $(this).dialog('close'); } }" cssClass="staticContentDialog"
			   > 
<div align="center">
	<table cellspacing="0" cellpadding="3" width="400px;" class="tableData shortcutKeysWin">
		<tr>
			<th width="300px;" align="left" class="ui-state-default ui-th-column ui-th-ltr"><span style="padding-left: 7px;"><s:text name="jsp.common.accessKeyDialog.shortCutKeys" /></span></th>
			<th width="700px;" class="ui-state-default ui-th-column ui-th-ltr"><span><s:text name="jsp.common.accessKeyDialog.description" /><span></th>
		</tr>
		<tr class="ui-widget-content jqgrow ui-row-ltr">
			<td><s:text name="jsp.common.accessKeyDialog.alt_zero" /></th>
			<td><s:text name="jsp.common.accessKeyDialog.alt_zero_desc" /></th>
		</tr>
		<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
			<td><s:text name="jsp.common.accessKeyDialog.alt_one" /></th>
			<td><s:text name="jsp.common.accessKeyDialog.alt_one_desc" /></th>
		</tr>
		<tr class="ui-widget-content jqgrow ui-row-ltr">
			<td><s:text name="jsp.common.accessKeyDialog.alt_two" /></th>
			<td><s:text name="jsp.common.accessKeyDialog.alt_two_desc" /></th>
		</tr>
		<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
			<td><s:text name="jsp.common.accessKeyDialog.alt_four" /></th>
			<td><s:text name="jsp.common.accessKeyDialog.alt_four_desc" /></th>
		</tr>
		<tr class="ui-widget-content jqgrow ui-row-ltr">
			<td><s:text name="jsp.common.accessKeyDialog.alt_six" /></th>
			<td><s:text name="jsp.common.accessKeyDialog.alt_six_desc" /></th>
		</tr>
		<!-- <tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
			<td><s:text name="jsp.common.accessKeyDialog.alt_five" /></th>
			<td><s:text name="jsp.common.accessKeyDialog.alt_five_desc" /></th>
		</tr> -->
		<%--
		<tr class="ui-widget-content jqgrow ui-row-ltr">
			<td><s:text name="jsp.common.accessKeyDialog.alt_up_arrow" /></th>
			<td><s:text name="jsp.common.accessKeyDialog.alt_up_arrow_desc" /></th>
		</tr>
		<tr class="ui-widget-content jqgrow ui-row-ltr ui-state-zebra">
			<td><s:text name="jsp.common.accessKeyDialog.alt_down_arrow" /></th>
			<td><s:text name="jsp.common.accessKeyDialog.alt_down_arrow_desc" /></th>
		</tr>
		--%>
	</table>
</div>
</sj:dialog>