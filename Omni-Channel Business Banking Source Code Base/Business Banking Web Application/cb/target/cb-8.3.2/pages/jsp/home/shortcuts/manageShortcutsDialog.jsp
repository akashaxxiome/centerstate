<%@page import="java.util.Iterator"%>
<%@page import="java.util.Map"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%

String opeartion = request.getParameter("opeartion");
String shortcutID = request.getParameter("shortcutID");
String shortCutName = request.getParameter("shortCutName");

request.setAttribute("opeartion", opeartion);
request.setAttribute("shortcutID", shortcutID);
request.setAttribute("shortCutName", shortCutName);
%>

<div id='manageShortcut' align="center">
		<input class="ui-widget-content" type="hidden" id="originalShortcutName" name="originalShortcutName" value="<s:property value='%{#attr.shortCutName}'/>" />
		<input class="ui-widget-content" type="hidden" id="shortcutID" name="shortcutID" value="<s:property value='%{#attr.shorcutID}'/>" />
		
		<s:if test="%{#attr.opeartion == 'modify'}" >
			<table border="0">
				<tr>
					<td align="left"><s:text name="jsp.default.label.new.shortcut.name" /></td>
					<td>
						<input class="ui-widget-content ui-corner-all" type="text" id="newShortcutName" name="newShortcutName" value="<s:property value='%{#attr.shortCutName}'/>"  maxlength="20" />
					</td>
				</tr>
				<tr height="15px"></tr>
				<tr>
					<td id="blankShortcutNameWarning" colspan="2" class="required hidden" align="center"><s:text name="jsp.default.validation.blank.shortcut.name" />
					</td>
				</tr>
				<tr height="15px"></tr>
				<tr align="center">
					<td colspan="2">
					  	<sj:a id="cancelShortcutForm" button="true"
							onClickTopics="closeDialog">
							<s:text name="jsp.default_82" /></sj:a>
							
						<sj:a 
							href="#"
							id="submitEditForm"
			                button="true"
			                onClickTopics="editShortcutFormSubmit" 
			                onCompleteTopics="editShortcutFormComplete"
							onErrorTopics="errorEditShortcutForm"
			                onSuccessTopics="successEditShortcutForm"
			               ><s:text name="jsp.default_366" /></sj:a>
					</td>
				</tr>
			</table>
		</s:if>
		
		<s:if test="%{#attr.opeartion == 'delete'}" >
			<table border="0">
				<tr>
					<%-- <td align="left">The shortcut <strong>'<s:property value='%{#attr.shortCutName}'/>'</strong> will be deleted, do you wish to proceed with this operation?</td> --%>
					<td align="left">
						<s:text name="jsp.default.text.confirm.shortcut.delete.1" /><strong>'<s:property value='%{#attr.shortCutName}'/>'</strong> <s:text name="jsp.default.text.confirm.shortcut.delete.2" />
					</td>
				</tr>
				<tr height="15px"></tr>
				<tr align="center">
					<td>
					 	<sj:a id="cancelShortcutForm" button="true"
							onClickTopics="closeDialog">
							<s:text name="jsp.default_82" /></sj:a>
							
						<sj:a 
							href="#"
							id="submitDeleteForm"
			                button="true" 
			                onClickTopics="deleteShortcutFormSubmit"
			                onCompleteTopics="deleteShortcutFormComplete"
							onErrorTopics="errorDeleteShortcutForm"
			                onSuccessTopics="successDeleteShortcutForm"
			               ><s:text name="jsp.default_162"/></sj:a>
					</td>
	               
				</tr>
			</table>
		</s:if>
</div>

