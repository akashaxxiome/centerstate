<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:object id="GetShortcuts" name="com.ffusion.tasks.util.GetShortcuts" scope="request"/>
<ffi:process name="GetShortcuts"/>
<ffi:help id="home_leftbar_shortcuts" />
<div id="shortCutsQuickLinksDivID" class="shortcutQuickLinksArea">
  <div id="shortcutErrorMsgID" style='text-align:center;vertical-align:middle;color:red;'></div>
  <ffi:cinclude value1="0" value2="${GetShortcuts.Size}" operator="equals">
  	<p class="columndata" align="center"><s:text name="jsp.home_206"/></p>
  </ffi:cinclude>
 
	<ffi:list collection="UserShortcuts" items="shortCut">
			 <ffi:cinclude value1="NO_ENTITLEMENT_CHECK" value2="${shortCut.entitlementSetting}" operator="equals">
			 	<s:include value="%{#session.PagesPath}/home/shortcuts/inc/include-shortcuts-list.jsp"/>
			 </ffi:cinclude>
			 <ffi:cinclude value1="NO_ENTITLEMENT_CHECK" value2="${shortCut.entitlementSetting}" operator="notEquals">
				 <ffi:cinclude ifEntitled="${shortCut.entitlementSetting}">
				    <s:include value="%{#session.PagesPath}/home/shortcuts/inc/include-shortcuts-list.jsp"/>
				</ffi:cinclude>
			</ffi:cinclude>
	</ffi:list>
</div>