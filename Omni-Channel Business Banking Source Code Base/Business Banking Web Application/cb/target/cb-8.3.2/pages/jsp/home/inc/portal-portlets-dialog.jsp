<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="home_portal-portlets-dialog" className="moduleHelpClass" />

<% 
		boolean isPaymentsEntitled = false;
		boolean isTransfersEntitled = false;
	%>
<ffi:cinclude
	ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.REC_PAYMENTS %>">
	<% isPaymentsEntitled = true; %>
</ffi:cinclude>

<ffi:cinclude
	ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.REC_TRANSFERS %>">
	<% isTransfersEntitled = true; %>
</ffi:cinclude>

<div>
	<table border="0" width="100%" cellpadding="3" cellspacing="0"
		id="portletList"
		class="tableData tableAlerternateRowColor tdWithPaddingAllSides">
		<ffi:cinclude value1="${SecureUser.AppType}"
			value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>"
			operator="notEquals">
			<tr>
				<td><span class="portletListName"> <s:text
							name="jsp.home.label.accounts.summary" />
				</span> <span class="portletListAdd"> <a id="ACCOUNTSSUMMARY"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('ACCOUNTSSUMMARY', ns.home.createPortletId('ACCOUNTSSUMMARY'), js_accountSummary_portlet_title, 'AccountsSummaryPortletAction_loadViewMode', '', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
		</ffi:cinclude>

		<% if( isPaymentsEntitled || isTransfersEntitled ) { %>
		<tr>
			<td><span class="portletListName"> <s:text
						name="jsp.home.label.pending.payments" />
			</span> <span class="portletListAdd"> <a id="PENDINGPAYMENTS"
					href="javascript:void(0)" class="ui-button"
					onclick="ns.layout.togglePortlet('PENDINGPAYMENTS',ns.home.createPortletId('PENDINGPAYMENTS'), js_pendingPayments_portlet_title,'PendingBillpayPortletAction_loadViewMode','', true)">
						<span class="ui-icon ui-icon-close" />
				</a>

			</span></td>
		</tr>

		<tr>
			<td><span class="portletListName"> <s:text
						name="jsp.home.label.pending.transfers" />
			</span> <span class="portletListAdd"> <a id="PENDINGTRANSFERS"
					href="javascript:void(0)" class="ui-button"
					onclick="ns.layout.togglePortlet('PENDINGTRANSFERS', ns.home.createPortletId('PENDINGTRANSFERS'), js_pendingTransfers_portlet_title, 'PendingTransfersPortletAction_loadViewMode', '', true)">
						<span class="ui-icon ui-icon-close" />
				</a>
			</span></td>
		</tr>

		<% } %>
		<ffi:cinclude
			ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.CONSOLIDATED_BALANCE %>">
			<tr>
				<td><span class="portletListName"> <s:text
							name="Consolidated_Balances_Summary" />
				</span> <span class="portletListAdd"> <a id="CONSOLIDATEDBALANCE"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('CONSOLIDATEDBALANCE', ns.home.createPortletId('CONSOLIDATEDBALANCE'), js_consolidatedBalancesSummary_portlet_title, 'ConsolidatedBalancePortletAction_loadViewMode', 'ConsolidatedBalancePortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
		</ffi:cinclude>
		<ffi:cinclude
			ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.CASH_FLOW %>">
			<tr>
				<td><span class="portletListName"> <s:text
							name="Cash_Flow_Summary" />
				</span> <span class="portletListAdd"> <a id="CASHFLOW"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('CASHFLOW',ns.home.createPortletId('CASHFLOW'), js_cashFlowSummary_portlet_title, 'CashFlowPortletAction_loadViewMode', 'CashFlowPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
		</ffi:cinclude>
		<%--
		<ffi:cinclude
			ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.PAY_ENTITLEMENT %>">
			<tr>
				<td><span class="portletListName"> <s:text
							name="Positive_Pay_Exception_Summary" />
				</span> <span class="portletListAdd"> <a id="POSITIVEPAY"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('POSITIVEPAY',ns.home.createPortletId('POSITIVEPAY'), js_positivePayExceptionSummary_portlet_title,'PositivePayPortletAction_loadViewMode','', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
		</ffi:cinclude>
		<ffi:cinclude
			ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>">
			<tr>
				<td><span class="portletListName"> <s:text
							name="Reverse_Positive_Pay_Summary" />
				</span> <span class="portletListAdd"> <a id="REVERSEPOSITIVEPAY"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('REVERSEPOSITIVEPAY',ns.home.createPortletId('REVERSEPOSITIVEPAY'), js_reversePositivePayExceptionSummary_portlet_title,'ReversePositivePayPortletAction_loadViewMode','', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
		</ffi:cinclude>
		<ffi:cinclude
			ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.LOCKBOX %>">
			<tr>
				<td><span class="portletListName"> <s:text
							name="Lockbox_Deposit_Availability_Summary" />
				</span> <span class="portletListAdd"> <a id="LOCKBOX"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('LOCKBOX', ns.home.createPortletId('LOCKBOX'), js_lockboxDepositAvailabilitySummary_portlet_title,'LockboxPortletAction_loadViewMode','', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
		</ffi:cinclude>
		<ffi:cinclude
			ifEntitled="<%=com.ffusion.csil.core.common.EntitlementsDefines.DISBURSEMENTS %>">
			<tr>
				<td><span class="portletListName"> <s:text
							name="Controlled_Disbursements_by_Presentment" />
				</span> <span class="portletListAdd"> <a
						id="PRESENTMENTDISBURSEMENTS" href="javascript:void(0)"
						class="ui-button"
						onclick="ns.layout.togglePortlet('PRESENTMENTDISBURSEMENTS', ns.home.createPortletId('PRESENTMENTDISBURSEMENTS'), js_controlledDisbursementsPresentment_portlet_title,'PresentmentDisbursementsPortletAction_loadViewMode','', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
			<tr>
				<td><span class="portletListName"> <s:text
							name="Controlled_Disbursements_by_Account" />
				</span> <span class="portletListAdd"> <a id="ACCOUNTDISBURSEMENTS"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('ACCOUNTDISBURSEMENTS',ns.home.createPortletId('ACCOUNTDISBURSEMENTS'), js_controlledDisbursementsAccount_portlet_title,'AccountDisbursementsPortletAction_loadViewMode','', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
		</ffi:cinclude> --%>
		<ffi:cinclude value1="${SecureUser.AppType}"
			value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>">
			<tr>
				<td><span class="portletListName"> <s:text
							name="Favorite_Accounts" />
				</span> <span class="portletListAdd"> <a id="FAVORITEACCOUNTS"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('FAVORITEACCOUNTS', ns.home.createPortletId('FAVORITEACCOUNTS'), js_favoriteAccounts_portlet_title,'FavoriteAccountsPortletAction_loadViewMode','FavoriteAccountsPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
		</ffi:cinclude>
		<%--
		<ffi:cinclude
			ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.PORTAL%>">
			<tr>
				<td><span class="portletListName"> <s:text
							name="Financial_Advice" />
				</span> <span class="portletListAdd"> <a id="ADVISORS"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('ADVISORS', ns.home.createPortletId('ADVISORS'), js_financialAdvice_portlet_title,'AdvicePortletAction_loadViewMode','AdvicePortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
			<tr>
				<td><span class="portletListName"> <s:text
							name="Business_News" />
				</span> <span class="portletListAdd"> <a id="NEWSBUSINESS"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('NEWSBUSINESS', ns.home.createPortletId('NEWSBUSINESS'), js_businessNews_portlet_title,'NewsPortletAction_loadViewMode','NewsPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
			<tr>
				<td><span class="portletListName"> <s:text
							name="Headline_News" />
				</span> <span class="portletListAdd"> <a id="NEWSFRONTPAGE"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('NEWSFRONTPAGE', ns.home.createPortletId('NEWSFRONTPAGE'), js_headlineNews_portlet_title,'NewsPortletAction_loadViewMode','NewsPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
			<tr>
				<td><span class="portletListName"> <s:text
							name="Stock_Portfolio" />
				</span> <span class="portletListAdd"> <a id="STOCKS"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('STOCKS', ns.home.createPortletId('STOCKS'), js_stockPortfolio_portlet_title,'StockPortletAction_loadViewMode','StockPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
			<tr>
				<td><span class="portletListName"> <s:text
							name="Sports_News" />
				</span> <span class="portletListAdd"> <a id="NEWSSPORTS"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('NEWSSPORTS', ns.home.createPortletId('NEWSSPORTS'),js_sportsNews_portlet_title,'NewsPortletAction_loadViewMode','NewsPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
			<tr>
				<td><span class="portletListName"> <s:text
							name="Weather" />
				</span> <span class="portletListAdd"> <a id="FORECASTS"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('FORECASTS', ns.home.createPortletId('FORECASTS'), js_weather_portlet_title,'ForecastPortletAction_loadViewMode','ForecastPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
			<tr>
				<td><span class="portletListName"> <s:text
							name="Calculators" />
				</span> <span class="portletListAdd"> <a id="CALCULATORS"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('CALCULATORS', ns.home.createPortletId('CALCULATORS'), js_calculators_portlet_title,'CalcPortletAction_loadViewMode','CalcPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
			<tr>
				<td><span class="portletListName"> <s:text
							name="Today's_Markets" />
				</span> <span class="portletListAdd"> <a id="STOCKINDEXES"
						href="javascript:void(0)" class="ui-button"
						onclick="ns.layout.togglePortlet('STOCKINDEXES', ns.home.createPortletId('STOCKINDEXES'), js_todaysMarkets_portlet_title,'MarketPortletAction_loadViewMode','MarketPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-close" />
					</a>
				</span></td>
			</tr>
		</ffi:cinclude>
		 --%>
		<!--
			<tr>
				<td>
					<span class="portletListName">
						<s:text name="Generic_Portlet"/>
					</span>
					<span class="portletListAdd">
						<a id="GENERICPORTLET" href="javascript:void(0)" onclick="ns.layout.togglePortlet(ns.home.createPortletId('GENERICPORTLET'), '<s:text name="Generic_Portlet"/>','RssPortletAction_loadViewMode','RssPortletAction_loadEditMode', true)">
							<span class="ui-icon ui-icon-circle-plus"/>
						</a>
					</span>
				</td>
			</tr>
			-->

	</table>

	<div class="ui-widget-header customDialogFooter">
		<sj:a id="cancelFormButton" href="#" button="true"
			onClickTopics="closeDialog">
			<s:text name="jsp.default_102.1" />
		</sj:a>
	</div>
</div>