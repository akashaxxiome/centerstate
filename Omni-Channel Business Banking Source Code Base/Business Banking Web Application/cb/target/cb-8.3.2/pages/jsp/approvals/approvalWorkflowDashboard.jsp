<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>


	<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-approvalWorkflow"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.approvals_24" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/approvalsSubmenuDropdown.jsp" />
    	</div>
   	    <div id="dbApprovalWFSummary" class="dashboardSummaryHolderDiv">
   	
       		<sj:a
				id="approvalWFSummaryBtn"
				indicator="indicator"
				cssClass="summaryLabelCls"
				button="true"
				onclick="ns.shortcut.goToMenu('approvals_workflow');"
				title="%{getText('SUMMARY')}"
				><s:text name="SUMMARY"/></sj:a>
		</div>
    </div>
<script>
	$("#approvalWFSummaryBtn").find("span").addClass("dashboardSelectedItem");
</script>           