<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<div id="detailsPortlet" class="portlet">
	<div class="portlet-header">
		<span class="portlet-title"><s:text name="jsp.approvals_11" /></span>
	</div>
	<div id="detailsDiv" class="portlet-content">
        <sj:tabbedpanel id="TransactionWizardTabs" >

            <sj:tab id="inputTab" target="inputDiv" label="Step 1: Edit"/>
            <sj:tab id="verifyTab" target="verifyDiv" label="Step 2: Verify"/>
            <sj:tab id="confirmTab" target="confirmDiv" label="Step 3: Confirm"/>
            <div id="inputDiv" style="border:1px">
                <s:text name="jsp.user_149" />
                <br><br>
            </div>
            <div id="verifyDiv" style="border:1px">
                <s:text name="jsp.approvals_15"/>
                <br><br>
            </div>
            <div id="confirmDiv" style="border:1px">
                <s:text name="jsp.approvals_4"/>
                <br><br>
            </div>
        </sj:tabbedpanel>
    </div>
    <div id="approvalsWorkFlowDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
      <ul class="portletHeaderMenu" style="background:#fff">
             <li><a href='#' onclick="ns.common.showDetailsHelp('detailsPortlet')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
      </ul>
	</div>
</div>

<script>	
		ns.common.initializePortlet("detailsPortlet");
</script>
