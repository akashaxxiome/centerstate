<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="approvals_corpadmincoapwfdeletelev" />

<%-- set the approvals constants --%>
<s:include value="%{#session.PagesPath}/inc/include-cb-approval-constants.jsp" />

<div align="center">
	<s:include value="%{#session.PagesPath}/common/include-approval-delete-level.jsp" />
</div>
