<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>

<script src="<s:url value='/web/js'/>/ui.supermultiselect.js"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web'/>/css/ui.supermultiselect.css" /> 
<%-- Remove pending WIre and ACH payee list from session --%>
<ffi:removeProperty name="<%=SessionNames.APPROVAL_ITEMS_DA%>"/>

<%-- set the approvals constants --%>
<s:include value="%{#session.PagesPath}/inc/include-cb-approval-constants.jsp" />

<ffi:help id="approvals_corpadmincoapwf" />
<span class="shortcutPathClass" style="display:none;">approvals_workflow</span>
<span class="shortcutEntitlementClass" style="display:none;">Approvals Admin</span>

<div align="center">
	<s:include value="%{#session.PagesPath}/common/include-approval-workflows.jsp" />
 	<br>
</div>
