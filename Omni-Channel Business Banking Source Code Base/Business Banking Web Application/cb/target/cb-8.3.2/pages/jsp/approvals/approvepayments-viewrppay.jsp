<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- Include approval include-approval-view-details-init.jsp page --%>
<ffi:include page="${PagesPath}/common/include-approval-view-details-init.jsp"/>
<ffi:removeProperty name="ItemID"/>

<ffi:cinclude value1="${ApprovalsBackPage}" value2="approvepayments.jsp" operator="equals" >
	<ffi:setProperty name='PageHeading' value='Pending Approvals'/>
</ffi:cinclude>

<ffi:help id="approvals_approvepayments-viewrppay" />
<div class="approvalDialogHt">
	<span id="PageHeading" style="display:none"><s:text name="jsp.approvals_27"/></span>
    <ffi:include page="${PagesPath}/common/include-approval-view-rppay-details.jsp"/>
</div>
