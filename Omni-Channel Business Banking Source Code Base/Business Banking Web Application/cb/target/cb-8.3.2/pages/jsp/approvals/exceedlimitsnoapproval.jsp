<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.common.Currency,
				 com.ffusion.beans.util.AccountUtil,
				 com.ffusion.beans.util.UtilException,
				 com.ffusion.csil.core.common.PaymentEntitlementsDefines" %>
<%@ page import="java.util.Locale" %>

<%-- set the properties required by this page --%>
<s:set var="tmpI18nStr" value="%{getText('jsp.approvals_6')}" scope="request" /><ffi:setProperty name='PageHeading' value="${tmpI18nStr}"/>
<ffi:setProperty name='PageText' value=''/>
<s:set var="tmpI18nStr" value="%{getText('jsp.approvals_6')}" scope="request" /><ffi:setProperty name="ApprovalsHeading" value="${tmpI18nStr}"/>
<% String SavedSubmenu = "";	// required for ACH Payments to know which menu to display (ACH/Tax/Child Support)
   String SavedSessionCleanupCurrTab = "";
%>
<ffi:getProperty name="subMenuSelected" assignTo="SavedSubmenu" />
<%-- we don't want any clean-up to happen if we are just coming back from ExceedLimitsnoApproval.jsp
So save the current clean-up value and restore it below --%>
<ffi:getProperty name="SessionCleanupCurrTab" assignTo="SavedSessionCleanupCurrTab"/>
<ffi:setProperty name="subMenuSelected" value="approvals"/>

<ffi:object id="GetLimitBaseCurrency" name="com.ffusion.tasks.util.GetLimitBaseCurrency" scope="session"/>
<ffi:process name="GetLimitBaseCurrency" />

<%-- include the approvals constants page --%>
<s:include value="/pages/jsp/inc/include-cb-approval-constants.jsp"/>


<%

	String operationName = "";
	String operationNameDisplay = "";
	String objectType = "";
	String objectID = "";
	String locationLimit = "";
	String periodOfLimit = "";
	String groupName = "";
	String exceedLimitsComment = "";
	boolean toggle = false;

%>
     <div align="center">

	<table width="750" border="0" cellspacing="0" cellpadding="0">
			<tr><td><br></td></tr>
   			<tr class="plainsubhead" wrap>
    				<td><s:text name="jsp.approvals_14"/></td>
			</tr>
			<tr><td><br></td></tr>
	<%
	com.ffusion.csil.beans.entitlements.Limits limitsNoApproval =
	    ( com.ffusion.csil.beans.entitlements.Limits )session.getAttribute( com.ffusion.approvals.constants.IApprovalsConsts.EXCEEDED_LIMITS_NO_APPROVAL );
	if ( limitsNoApproval != null ) {
		int size = limitsNoApproval.size();
		for( int i = 0; i < size; i++ ) {
	    	com.ffusion.csil.beans.entitlements.Limit limit =
			(com.ffusion.csil.beans.entitlements.Limit )limitsNoApproval.get(i);
		    com.ffusion.csil.beans.entitlements.Entitlement ent = limit.getEntitlement();

	    	//populate variables with relavent information
	    	operationName 	= ent.getOperationName();
	    	objectType 		= ent.getObjectType();
	    	objectID		= ent.getObjectId();

		if ( operationName != null )
		{
			if ( operationName.equals(PaymentEntitlementsDefines.PAYMENTS) ) {
				operationNameDisplay = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_321", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
			} else if ( operationName.equals(PaymentEntitlementsDefines.TRANSFERS) ) {
				operationNameDisplay = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_443", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
			} else {
				operationNameDisplay = operationName;
			}
		}
		
            boolean useOperationName = (operationName != null && !operationName.equals( "" ));
            boolean useObjectTypeID = ( objectType != null && !objectType.equals( "" ) );

			if (PaymentEntitlementsDefines.ACCOUNT.equals( objectType ) && objectID != null)
			{
				com.ffusion.beans.SecureUser user = (com.ffusion.beans.SecureUser)session.getAttribute( "SecureUser" );
				// need to format objectID
				try {
					objectID = AccountUtil.buildAccountDisplayText( objectID.substring( objectID.indexOf( '-' ) + 1 ), user.getLocale() );
				} catch( UtilException utilE ) {
					// Intentionally eaten... we want to default to displaying just the
					// entitlement object ID
				}
			} else if (PaymentEntitlementsDefines.ACCOUNT_GROUP.equals( objectType ) && objectID != null) {
				
				com.ffusion.beans.accountgroups.BusinessAccountGroup accountGroup = null;
				
				%>
				<ffi:object id="GetAccountGroup" name="com.ffusion.tasks.accountgroups.GetAccountGroup" scope="session" />
				<ffi:setProperty name="GetAccountGroup" property="AccountId" value="<%= objectID %>"/>
	    		<ffi:process name="GetAccountGroup"/>
	    		<%
	    			
	    		Object tempObject = session.getAttribute(com.ffusion.tasks.accountgroups.AccountGroupsTask.ACCOUNTGROUP);
	    			
	    		if ( tempObject instanceof com.ffusion.beans.accountgroups.BusinessAccountGroup ) {
	    			accountGroup = (com.ffusion.beans.accountgroups.BusinessAccountGroup) tempObject;
	    		}
	    			
	    		if ( accountGroup != null ) {
	    			objectID = com.ffusion.util.HTMLUtil.encode(accountGroup.getName())+" - "+accountGroup.getAcctGroupId();
	    		}
	    	
			} else if ( PaymentEntitlementsDefines.WIRE_TEMPLATE.equals( objectType ) && objectID != null ) {
				%>
				<ffi:object id="GetWireTemplateById" name="com.ffusion.tasks.wiretransfers.GetWireTransferById" scope="session" />
				<ffi:setProperty name="GetWireTemplateById" property="BeanSessionName" value="ExceededLimitWireTemplate"/>
				<ffi:setProperty name="GetWireTemplateById" property="ID" value="<%= objectID %>"/>
	    		<ffi:process name="GetWireTemplateById"/>
	    		<%
	    		
	    		com.ffusion.beans.wiretransfers.WireTransfer wireTemplate = 
	    		(com.ffusion.beans.wiretransfers.WireTransfer)session.getAttribute("ExceededLimitWireTemplate");
	    		
	    		if( wireTemplate != null ) {
	    			objectID = wireTemplate.getWireName();
	    		}
	    		%>
	    		<ffi:removeProperty name="ExceededLimitWireTemplate"/>
	    		<ffi:removeProperty name="GetWireTemplateById"/>    		
	    		<%
				
			}

	    	String memberType = limit.getMemberType();
	    	exceedLimitsComment = null;
	    	locationLimit = null;

            //String amount = String.valueOf(limit.getAmount());
            String amount = limit.getData();

	    	int period = limit.getPeriod();

	    	//checking the type of period
	    	if ( period == com.ffusion.csil.beans.entitlements.Limit.PERIOD_TRANSACTION ) {
	    		periodOfLimit = "jsp/approvals/exceedlimitsnoapproval.jsp-transaction";
				if (operationName != null && operationName.indexOf(PaymentEntitlementsDefines.ACH_PAYMENT_ENTRY) >=0 )
				{
		    		periodOfLimit = "jsp/approvals/exceedlimitsnoapproval.jsp-batch";
				}
	    	} else if ( period == com.ffusion.csil.beans.entitlements.Limit.PERIOD_DAY ) {
	    		periodOfLimit = "jsp/approvals/exceedlimitsnoapproval.jsp-daily";
	    	} else if ( period == com.ffusion.csil.beans.entitlements.Limit.PERIOD_WEEK ) {
	    		periodOfLimit = "jsp/approvals/exceedlimitsnoapproval.jsp-weekly";
	    	} else if ( period == com.ffusion.csil.beans.entitlements.Limit.PERIOD_MONTH ) {
	    		periodOfLimit = "jsp/approvals/exceedlimitsnoapproval.jsp-monthly";
	    	}
            %>
<ffi:getL10NString rsrcFile='cb' msgKey="<%=periodOfLimit%>" assignTo="periodOfLimit" />
            <%
            boolean bankLimit = false;

	    	if ( memberType != null )
	    	{
                %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-user" assignTo="locationLimit" />
                <%
	    	} else {

        	%>

	    	<ffi:object id="GetEntitlementGroup" name="com.ffusion.efs.tasks.entitlements.GetEntitlementGroup" scope="session" />
			<ffi:setProperty name="GetEntitlementGroup" property="GroupId" value="<%= Integer.toString( limit.getGroupId() )%>"/>
	    	<ffi:process name="GetEntitlementGroup"/>

        	<%
	    		com.ffusion.csil.beans.entitlements.EntitlementGroup entGroup =
	    			( com.ffusion.csil.beans.entitlements.EntitlementGroup )session.getAttribute( com.ffusion.efs.tasks.entitlements.Task.ENTITLEMENT_GROUP );
	    		if ( entGroup.getEntGroupType().equals( com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_DIVISION ) ) {
	    			groupName = entGroup.getGroupName();
                %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-division" assignTo="locationLimit" parm0="<%=groupName%>" />
                <%
	    		} else if ( entGroup.getEntGroupType().equals( com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_GROUP ) ) {
	    			groupName = entGroup.getGroupName();
                %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-group" assignTo="locationLimit" parm0="<%=groupName%>" />
                <%
	    		} else if ( entGroup.getEntGroupType().equals( com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS ) ) {
                %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-business" assignTo="locationLimit" />
                <%
		    	} else {
                    bankLimit = true;
	    		}

	    	}

	    	if ( bankLimit ) {
                if (!useOperationName)
                {
                    %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-bank1" assignTo="exceedLimitsComment" parm0="<%=periodOfLimit%>" parm1="<%=amount%>" parm2="${BaseCurrency}" />
                    <%
                } else {
                    if (!useObjectTypeID)
                    {
                    %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-bank2" assignTo="exceedLimitsComment" parm0="<%=periodOfLimit%>" parm1="<%=amount%>" parm2="<%=operationNameDisplay%>" parm3="${BaseCurrency}" />
                    <%
                    } else {
                        String limitOn;
                    %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-limiton" assignTo="limitOn" parm0="<%=objectType%>" parm1="<%=objectID%>" />
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-bank3" assignTo="exceedLimitsComment" parm0="<%=periodOfLimit%>" parm1="<%=amount%>" parm2="<%=operationNameDisplay%>" parm3="<%=limitOn%>" parm4="${BaseCurrency}" />
                    <%
                    }
                }
            } else {

                if (!useOperationName)
                {
                %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-1" assignTo="exceedLimitsComment" parm0="<%=periodOfLimit%>" parm1="<%=amount%>" parm2="<%=locationLimit%>" parm3="${BaseCurrency}" />
                <%
                } else {
                    if (!useObjectTypeID)
                    {
                    %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-2" assignTo="exceedLimitsComment" parm0="<%=periodOfLimit%>" parm1="<%=amount%>" parm2="<%=locationLimit%>" parm3="<%=operationNameDisplay%>" parm4="${BaseCurrency}" />
                    <%
                    } else {
                        String limitOn;
                    %>

					<%-- Check if objectType is Location. If it is, get location name according to objectID --%>
					<%if("Location".equals(objectType)){ %>
						<ffi:object id="GetLocations" name="com.ffusion.tasks.cashcon.GetLocations"/>
						<ffi:process name="GetLocations"/>
						<ffi:removeProperty name="GetLocations"/>
					<%
						com.ffusion.beans.cashcon.LocationSearchResults locs = (com.ffusion.beans.cashcon.LocationSearchResults)session.getAttribute("Locations");
						com.ffusion.beans.cashcon.LocationSearchResult loc = null;	
						if(locs != null){	
							 loc = locs.getByBPWID(objectID);
						}
						if(loc != null){
							objectID = loc.getLocationName();
						}
					} else if("ACHCompany".equals(objectType)){ %>
						<ffi:object id="GetACHCompanies" name="com.ffusion.tasks.ach.GetACHCompanies" scope="session"/>
						<ffi:setProperty name="GetACHCompanies" property="CustID" value="${SecureUser.BusinessID}" />
						<ffi:setProperty name="GetACHCompanies" property="Reload" value="true" />
						<ffi:setProperty name="GetACHCompanies" property="FIID" value="${SecureUser.BankID}" />
						<ffi:setProperty name="GetACHCompanies" property="LoadCompanyEntitlements" value="false"/>
						<ffi:process name="GetACHCompanies"/>
						<ffi:removeProperty name="GetACHCompanies"/>
					<%
						com.ffusion.beans.ach.ACHCompanies companies = (com.ffusion.beans.ach.ACHCompanies)session.getAttribute("ACHCOMPANIES");
						com.ffusion.beans.ach.ACHCompany company = null;
						if(companies != null){
							company = companies.getByCompanyID(objectID);
						}
						if(company != null){
							objectID = company.getCompanyName();
						}
					} else if("Wire Template".equals(objectType)){ %>
						<ffi:object id="GetWireTemplates" name="com.ffusion.tasks.wiretransfers.GetWireTemplates" scope="session"/>
							<ffi:setProperty name="GetWireTemplates" property="CollectionSessionName" value="WireTemplatesUser"/>
							<ffi:setProperty name="GetWireTemplates" property="WireScope" value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_SCOPE_USER %>"/>
							<ffi:process name="GetWireTemplates"/>
							<%
							com.ffusion.beans.wiretransfers.WireTransfers templates = (com.ffusion.beans.wiretransfers.WireTransfers)session.getAttribute("WireTemplatesUser");
							com.ffusion.beans.wiretransfers.WireTransfer template = null;
							if(templates != null){
								template = (com.ffusion.beans.wiretransfers.WireTransfer)templates.getByID(objectID);
							}
							if(template != null){
								objectID = template.getWireName();
							} else {
							%>
								<ffi:setProperty name="GetWireTemplates" property="CollectionSessionName" value="WireTemplatesBusiness"/>
								<ffi:setProperty name="GetWireTemplates" property="WireScope" value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_SCOPE_BUSINESS %>"/>
								<ffi:process name="GetWireTemplates"/>
								<%
								templates = (com.ffusion.beans.wiretransfers.WireTransfers)session.getAttribute("WireTemplatesBusiness");
								if(templates != null){
									template = (com.ffusion.beans.wiretransfers.WireTransfer)templates.getByID(objectID);
								}
								if(template != null){
									objectID = template.getWireName();
								} else {
								%>
									<ffi:setProperty name="GetWireTemplates" property="CollectionSessionName" value="WireTemplatesBank"/>
									<ffi:setProperty name="GetWireTemplates" property="WireScope" value="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_SCOPE_BANK %>"/>
									<ffi:process name="GetWireTemplates"/>
									<%
									templates = (com.ffusion.beans.wiretransfers.WireTransfers)session.getAttribute("WireTemplatesBusiness");
									if(templates != null){
										template = (com.ffusion.beans.wiretransfers.WireTransfer)templates.getByID(objectID);
									}
									if(template != null){
										objectID = template.getWireName();
									} 
									%>
				
								<%} %>
							<%} %>
							<ffi:removeProperty name="GetWireTemplates"/>
					<%
					} //Wire Template
					%>
				

<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-limiton" assignTo="limitOn" parm0="<%=objectType%>" parm1="<%=objectID%>" />
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-3" assignTo="exceedLimitsComment" parm0="<%=periodOfLimit%>" parm1="<%=amount%>" parm2="<%=locationLimit%>" parm3="<%=operationNameDisplay%>" parm4="<%=limitOn%>" parm5="${BaseCurrency}" />
                    <%
                    }
                }
	    	}

	    toggle = !toggle;

	%>
	    		<tr class="<%= toggle ? "ltrow1" : "dkrow" %>">
	                	<td class="columndata" nowrap width="730">
			    		<div align="left"><%= exceedLimitsComment%></div>
				</td>
	    		</tr>
	<%
	}
	} else {
                %>
<ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/exceedlimitsnoapproval.jsp-NotAvail" assignTo="exceedLimitsComment" />
                <%
	%>

			<tr class="<%= toggle ? "ltrow1" : "dkrow" %>">
	                	<td class="columndata" nowrap width="730">
			    		<div align="left"><%= exceedLimitsComment%></div>
				</td>
	    		</tr>
	<%
	}
	session.removeAttribute( com.ffusion.approvals.constants.IApprovalsConsts.EXCEEDED_LIMITS_NO_APPROVAL );
	%>
	</table>
     </div>

<ffi:setProperty name="subMenuSelected" value="<%=SavedSubmenu%>" />
<ffi:setProperty name="SessionCleanupCurrTab" value="<%=SavedSessionCleanupCurrTab%>" />
