<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>


	<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-approvalPending"></span>
    		<span class="moduleSubmenuLbl">
    			<s:text name="jsp.approvals_12" />
    		</span>
    		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
			
			<!-- dropdown menu include -->    		
    		<s:include value="/pages/jsp/home/inc/approvalsSubmenuDropdown.jsp" />
    	</div>
    	<div id="dbPendingApprovalSummary" class="dashboardSummaryHolderDiv">
    	
	       		<sj:a
					id="pendingApprovalSummaryBtn"
					indicator="indicator"
					cssClass="summaryLabelCls"
					button="true"
					onclick="ns.shortcut.goToMenu('approvals_pending');"
					title="%{getText('jsp.pending_approval_lbl_summary')}"
					><s:text name="jsp.pending_approval_lbl_summary"/></sj:a>
		</div>
		
    	
    </div>
<script>
	$("#pendingApprovalSummaryBtn").find("span").addClass("dashboardSelectedItem");
</script>       