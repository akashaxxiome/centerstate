<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%

for (int i = 0; ; i++) {
	String currKey = "selectedEntity" + i;
	String currValue = request.getParameter(currKey);
	if (currValue == null) 
	    break;

	session.setAttribute(currKey, currValue);
}
%>

<ffi:removeProperty name="SortSelectedApprovers"/>
<ffi:object name="com.ffusion.tasks.approvals.SortSelectedApprovers" id="SortSelectedApprovers" scope="session"/>
<ffi:process name="SortSelectedApprovers"/>

<s:include value="%{#session.PagesPath}/common/include-approval-view-permissions.jsp"/>

