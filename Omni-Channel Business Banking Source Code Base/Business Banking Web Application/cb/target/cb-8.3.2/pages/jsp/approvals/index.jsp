<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>



<ffi:cinclude value1="${SameDayCashConEnabled}" value2="" >
	<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
	<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayCashConEnabled"/>
	<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.cashcon.sameday.support"/>
	<ffi:process name="GetBPWProperty"/>
</ffi:cinclude>


<ffi:cinclude value1="${SameDayExtTransferEnabled}" value2="" >
	<ffi:object id="GetBPWProperty" name="com.ffusion.tasks.util.GetBPWProperty" scope="session" />
	<ffi:setProperty name="GetBPWProperty" property="SessionName" value="SameDayExtTransferEnabled"/>
	<ffi:setProperty name="GetBPWProperty" property="PropertyName" value="bpw.transfer.external.sameday.support"/>
	<ffi:process name="GetBPWProperty"/>
</ffi:cinclude>

<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/css/approvals/approvals%{#session.minVersion}.css'/>"></link>
<script type="text/javascript" src="<s:url value='/web/js/approvals/approvals%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/approvals/approvals_grid%{#session.minVersion}.js'/>"></script>

<div id="desktop" align="center">

    <div id="operationresult">
        <div id="resultmessage"><s:text name="jsp.default_498"/></div>
    </div>

	<div id="appdashboard">
		<s:include value="apprs_dashboard.jsp"/>
	</div>

	<div id="details" style="display:none;">
		<s:include value="apprs_details.jsp"/>
	</div>

	<script type="text/javascript">
		$('#appdashboard').pane({
			title: js_dashboard,
			minmax: true,
			close: false
		});
		$('#appdashboard').pane('hide'); //for later use
	</script>

<style type="text/css">
#targetGroups {
	padding: 0;
}

#targetPending {
	padding: 0;
}

</style>
<div id="summary">
<sj:tabbedpanel id="approvalTabs" collapsible="false" cssClass="portlet">
<%
	String subpage = request.getParameter("subpage");

	if( subpage != null && subpage.equals("groups") ) {
%>
	<div id="targetGroups">
	<sj:tab id="groups" target="targetGroups" onclick="hideDetails();" label="%{getText('jsp.approvals_23')}"/>
    <s:include
		value="corpadminapprovalgroup.jsp?InitPage=true" /></div>
<% } else if( subpage != null && subpage.equals("pending")) { %>
	<sj:tab id="pending" target="targetPending" onclick="hideDetails();" label="%{getText('jsp.approvals_12')}"/>
	<div id="targetPending"><s:include
		value="approvepayments.jsp?InitPage=true" /></div>
	<div id="targetApprovePaymentsVerify" style="display:none"></div>
<% } else { %>
 	<sj:tab id="workflow" target="targetWorkflow" onclick="hideDetails();" label="%{getText('jsp.approvals_24')}"/>
	<div id="targetWorkflow"><s:include
		value="corpadmincoapwf.jsp?InitPage=true" /></div>
<% } %>
</sj:tabbedpanel>
	<script type="text/javascript">
	$('#approvalTabs').portlet({
		bookmark: true,
		helpCallback: function(){
		 	var helpFile;

		 	if(document.getElementById('targetWorkflow') != null )
		 		helpFile = $('#targetWorkflow').find('.moduleHelpClass').html();
		 	else if(document.getElementById('targetGroups') != null )
		 		helpFile = $('#targetGroups').find('.moduleHelpClass').html();
		 	else {	// pending here
			 	if($('#targetPending').css('display').toLowerCase() == "block" )
			 		helpFile = $('#targetPending').find('.moduleHelpClass').html();
			 	else
			 		helpFile = $('#targetApprovePaymentsVerify').find('.moduleHelpClass').html();
		 	}
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		},
		bookMarkCallback: function(){
			var path = $('#summary').find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.shortcutPathClass').html();
			var ent  = $('#summary').find('.ui-tabs-panel:not(.ui-tabs-hide)').find('.shortcutEntitlementClass').html();
			ns.shortcut.openShortcutWindow( path, ent );
		}
	});
 	</script>
</div>
</div>


    <sj:dialog id="enableCascadingDialogID" cssClass="approvalsDialog" title="%{getText('jsp.default_185')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>

    <s:url id="displayStallConditionUrl" value="%{#session.PagesPath}/%{#session.approval_display_stall_condition_jsp}" escapeAmp="false">
	   		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	</s:url>

	<sj:dialog id="stallConditionDialogID" cssClass="approvalsDialog" title="%{getText('jsp.default_385')}" href="%{displayStallConditionUrl}"
			modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="650">
	</sj:dialog>

	<sj:dialog id="viewPermissionDialogID" cssClass="approvalsDialog" title="%{getText('jsp.default_463')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>

	<sj:dialog id="deleteApprovalLevelDialogID" cssClass="approvalsDialog" title="%{getText('jsp.approvals_5')}" modal="true" resizable="false" autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" width="570">
	</sj:dialog>
