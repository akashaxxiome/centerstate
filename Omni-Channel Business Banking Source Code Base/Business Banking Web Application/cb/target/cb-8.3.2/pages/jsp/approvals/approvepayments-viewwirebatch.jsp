<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name='PageHeading' value="${ApprovalsHeading}"/>

<%-- Include approval include-approval-view-details-init.jsp page --%>
<ffi:include page="${PagesPath}/common/include-approval-view-details-init.jsp"/>
<ffi:removeProperty name="ItemID"/>

<ffi:help id="approvals_approvepayments-viewwirebatch"/>
<div align="center">
	<span id="PageHeading" style="display:none"><s:text name="jsp.approvals_19"/></span>
	
		<ffi:include page="${PagesPath}/common/include-approval-view-details-header.jsp"/>
		<div align="center">
		<ffi:include page="${PagesPath}/common/include-view-wirebatch.jsp"/></div>
    </table>								
</div>