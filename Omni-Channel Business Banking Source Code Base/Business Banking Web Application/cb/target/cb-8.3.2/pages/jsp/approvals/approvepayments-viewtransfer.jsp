<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name='PageHeading' value='${ApprovalsHeading}' />

<%-- Include approval include-approval-view-details-init.jsp page --%>
<ffi:include page="${PagesPath}/common/include-approval-view-details-init.jsp"/>
<ffi:removeProperty name="ItemID"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<ffi:help id="approvals_approvepayments-viewtransfer" />
<span id="PageHeading" style="display:none"><ffi:getL10NString rsrcFile='cb' msgKey="jsp/approvals/approve-payments-view.jsp" parm0="${ApprovalsItem.Type}" /></span>
<div class="approvalDialogHt">
<ffi:include page="${PagesPath}/common/include-approval-view-details-header.jsp"/>
<div>
	<ffi:include page="${PagesPath}/common/include-view-accounttransfer-details.jsp"/> 
</div>
												
</div>		
