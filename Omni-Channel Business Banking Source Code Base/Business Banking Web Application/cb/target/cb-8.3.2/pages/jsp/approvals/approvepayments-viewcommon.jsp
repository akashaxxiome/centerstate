<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<s:set name="Type" value="#parameters.Type[0]" scope="request"/>
<s:set name="ItemID" value="#parameters.ItemID[0]" scope="session"/>
<s:set name="CollectionName" value="#parameters.CollectionName[0]" scope="session"/>

<ffi:include page="${PagesPath}/common/include-approval-view-details-init.jsp"/>
<ffi:include page="${PagesPath}/inc/include-cb-approval-constants.jsp"/>

<%-- Based on the type, go to the appropriate page.  Note that the types are all from TWTransactionTypes.  Since <%= %> doesn't work in s:tags, the string value of the defines are used here. --%>
<s:if test="%{#request.Type == 'Account Transfer' || #request.Type == 'Recurring Account Transfer'}">
	<ffi:include page="${PagesPath}${approval_view_transfer_jsp}"/>
</s:if>
<s:elseif test="%{#request.Type == 'Payment' || #request.Type == 'Recurring Payment'}">
	<ffi:include page="${PagesPath}${approval_view_billpay_jsp}"/>
</s:elseif>
<s:elseif test="%{#request.Type == 'Wire Transfer' || #request.Type == 'Recurring Wire Transfer' || #request.Type == 'Wire Template' || #request.Type == 'Recurring Wire Template'}">
	<ffi:include page="${PagesPath}${approval_view_wire_jsp}"/>
</s:elseif>
<s:elseif test="%{#request.Type == 'Wire Batch'}">
	<ffi:include page="${PagesPath}${approval_view_wirebatch_jsp}"/>
</s:elseif>
<s:elseif test="%{#request.Type == 'ACH Batch' || #request.Type == 'Recurring ACH Batch' || #request.Type == 'Tax Payment' ||
 				#request.Type == 'Recurring Tax Payment' || #request.Type == 'Child Support Payment' || #request.Type == 'Recurring Child Support Payment'}">
	<ffi:include page="${PagesPath}${approval_view_ach_jsp}"/>
</s:elseif>
<s:elseif test="%{#request.Type == 'Cash Concentration Deposit Entry' || #request.Type == 'Cash Concentration Disbursement Request'}">
	<ffi:include page="${PagesPath}${approval_view_cashcon_jsp}"/>
</s:elseif>
<s:elseif test="%{#request.Type == 'Positive Pay'}">
	<ffi:include page="${PagesPath}${approval_view_positive_pay_jsp}"/>
</s:elseif>
<s:elseif test="%{#request.Type == 'Reverse Positive Pay'}">
	<ffi:include page="${PagesPath}${approval_view_reverse_positive_pay_jsp}"/>
</s:elseif>

