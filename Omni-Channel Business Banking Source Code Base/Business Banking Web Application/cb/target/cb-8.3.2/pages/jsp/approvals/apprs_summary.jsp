<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>


<style type="text/css">
#targetGroups{
padding: 0;
}
#targetPending{
padding: 0;
}
</style>
<sj:tabbedpanel id="approvalTabs" collapsible="false" cssClass="portlet">
    <sj:tab id="workflow" target="targetWorkflow" onclick="hideDetails();" key="Approval Workflow"/>
    <sj:tab id="groups" target="targetGroups" onclick="hideDetails();" key="Approval Groups"/>
    <sj:tab id="pending" target="targetPending" onclick="hideDetails();" key="Pending Approvals"/>

    <div id="targetWorkflow"><s:include value="corpadmincoapwf.jsp?InitPage=true" /></div>
    <div id="targetGroups"><s:include value="corpadminapprovalgroup.jsp?InitPage=true"/>
    </div>
    <div id="targetPending"><s:include value="approvepayments.jsp?InitPage=true"/>
    </div>
</sj:tabbedpanel>

