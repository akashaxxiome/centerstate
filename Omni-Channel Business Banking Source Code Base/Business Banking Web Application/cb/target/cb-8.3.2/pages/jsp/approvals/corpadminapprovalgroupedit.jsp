<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>


<ffi:help id="approvals_corpadminapprovalgroupedit" />
<style>
#formerrors{display:none;}
</style>
<div align="center">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2">
		<div align="center" class="marginBottom20"><span class="sectionsubhead"> <ffi:getProperty
			name="Business" property="BusinessName" />
		</span></div>
		</td>
	</tr>
</table>
<s:form id="GroupEdit" namespace="/pages/jsp/approvals" validate="false"
	action="editApprovalGroup" method="post" name="GroupEdit"
	theme="simple" onsubmit="$('#saveEditGroupLink1').click();return false">
	<input type="hidden" name="CSRF_TOKEN"
		value="<ffi:getProperty name='CSRF_TOKEN'/>" />
	<div align="center">
	<table width="99%" border="0" cellspacing="0" cellpadding="3"
		align="center">
		<tr>
			<td colspan="2" class="adminBackground sectionsubhead"><span
				id="GroupEdit_OriginalName" style="display: none"><s:property value="%{#session.editApprovalsGroup.GroupName}" /></span>
			<div align="center" class="marginBottom10"><s:text name="jsp.approvals_7"/> <s:textfield
				name="groupName" size="32" maxlength="20"
				cssClass="ui-widget-content ui-corner-all"
				value="%{groupName}" /></div>
			</td>
		</tr>
		<tr>
			<td colspan="2" valign="top" align="center" height="30">&nbsp;<span id="_groupNameError"></span></td>
		</tr>
	
		<s:set var="isFirstRecord" value="%{'true'}" />
		<s:iterator value="approvalsGroupMemberNames">
				<tr>
				<td class="adminBackground sectionsubhead" align="right">
				<s:if test="%{#isFirstRecord=='true'}">
					<s:text name="jsp.approvals_22"/>
					<s:set var="isFirstRecord" value="%{'false'}" />
				</s:if>
				</td>
				<td align="left" width="55%"><span class="columndata"><s:property/></span></td>
			</tr>
		</s:iterator>
		
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" valign="top">
			<div class="ui-widget-header customDialogFooter">
				<sj:a id="cancelEditGroupLink1"
					button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_82')}"><s:text name="jsp.default_82"/></sj:a>
				<sj:a id="resetEditGroupLink1" button="true"
					onClickTopics="resetGroupEditForm" title="%{getText('jsp.default_358'}"><s:text name="jsp.default_358" /></sj:a> 
				<sj:a
					id="saveEditGroupLink1" formIds="GroupEdit" targets="resultmessage"
					button="true"
					title="%{getText('jsp.default_366'}"
					validate="true"
					validateFunction="customValidation"
					onCompleteTopics="editApprovalGroupComplete"
					onSuccessTopics="editApprovalGroupSuccessTopics"
					onErrorTopics="editApprovalGroupError" 
					effectDuration="1500"><s:text name="jsp.default_366" /></sj:a></div>
			</td>
		</tr>
	</table>
	</div>
</s:form></div>
