<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%-- set the approvals constants --%>
<s:include value="%{#session.PagesPath}/inc/include-cb-approval-constants.jsp" />
<%-- call the processing page --%>
<s:include value="%{#session.PagesPath}/common/include-approval-edit-level-process.jsp" />
