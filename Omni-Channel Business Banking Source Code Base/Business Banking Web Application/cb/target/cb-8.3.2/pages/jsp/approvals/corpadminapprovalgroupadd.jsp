<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<style>
#formerrors{display:none;}
</style>
<ffi:help id="approvals_corpadminapprovalgroupadd" />
<div align="center">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="2">
			<div align="center" class="marginBottom20"><span class="sectionsubhead"> <ffi:getProperty
				name="Business" property="BusinessName" /></span>
			</div>
		</td>
	</tr>
</table>
<s:form id="GroupNew" namespace="/pages/jsp/approvals" validate="false"
	action="addApprovalGroup" method="post" name="GroupNew" theme="simple" onsubmit="$('#addAddGroupLink1').click();return false">
	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
	<table width="99%" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<td class="adminBackground sectionsubhead">
			<div align="center" class="marginBottom10"><s:text name="jsp.approvals_7"/> <s:textfield
				name="groupName" size="32" maxlength="20"
				cssClass="ui-widget-content ui-corner-all" /></div>
			</td>
		</tr>
		<tr>
			<td valign="top" align="center" height="50">&nbsp;<span id="_groupNameError"></span>&nbsp;</td>
		</tr>
		<tr>
			<td valign="top">
			<div class="ui-widget-header customDialogFooter">
				<sj:a id="cancelAddGroupLink1"
					button="true" onClickTopics="closeDialog" title="%{getText('jsp.default_82')}"><s:text name="jsp.default_82"/></sj:a>
				<sj:a id="resetAddGroupLink1" button="true"
					onClickTopics="resetAddGroupDialog" title="%{getText('jsp.default_358'}"><s:text name="jsp.default_358" /></sj:a> 
				<sj:a
					id="addAddGroupLink1" formIds="GroupNew" targets="resultmessage"
					button="true"
					title="Add Group"
					validate="true"
					validateFunction="customValidation"
					onCompleteTopics="addApprovalGroupComplete"
					onSuccessTopics="addApprovalGroupSuccessTopics"
					onErrorTopics="addApprovalGroupErrorTopics"
					effectDuration="1500"><s:text name="jsp.approvals_21" /></sj:a>
			</div>
			</td>
		</tr>
	</table>
</s:form>
</div>
