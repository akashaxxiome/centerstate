<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:cinclude value1="${GetAuditWireTransferById.Template}" value2="true" operator="notEquals">
    <ffi:object id="StatusResource" name="com.ffusion.tasks.util.Resource" scope="request"/>
        <ffi:setProperty name="StatusResource" property="ResourceFilename" value="com.ffusion.beansresources.reporting.wire_status" />
    <ffi:process name="StatusResource"/>
</ffi:cinclude>

<ffi:cinclude value1="${GetAuditWireTransferById.Template}" value2="true">
    <ffi:object id="StatusResource" name="com.ffusion.tasks.util.Resource" scope="request"/>
        <ffi:setProperty name="StatusResource" property="ResourceFilename" value="com.ffusion.beansresources.reporting.template_status" />
    <ffi:process name="StatusResource"/>
</ffi:cinclude>

<%-- ---- TRANSACTION HISTORY ---- --%>
<table width="750" border="0" cellspacing="0" cellpadding="3" class="tableAlerternateRowColor tdWithPadding">
    <tr>
	    <td align="left" colspan="4" class="sectionhead"><strong>
            <ffi:cinclude value1="${GetAuditWireTransferById.Template}" value2="true" operator="notEquals">
                <s:text name="jsp.default_437"/>
            </ffi:cinclude>
            <ffi:cinclude value1="${GetAuditWireTransferById.Template}" value2="true">
                <s:text name="jsp.default_411"/>
            </ffi:cinclude>
       </strong> </td>
    </tr>
    <tr class="resetTableRowBGColor">
        <td align="left" class="sectionsubhead" nowrap><s:text name="jsp.default_142"/></td>
        <td align="left" class="sectionsubhead" nowrap>
            <ffi:cinclude value1="${GetAuditWireTransferById.Template}" value2="true" operator="notEquals">
                <s:text name="jsp.default_438"/>
            </ffi:cinclude>
            <ffi:cinclude value1="${GetAuditWireTransferById.Template}" value2="true">
                <s:text name="jsp.default_419"/>
            </ffi:cinclude>
        </td>
        <td align="left" class="sectionsubhead" nowrap><s:text name="jsp.default_333"/></td>
        <td align="left" class="sectionsubhead" nowrap><s:text name="jsp.default_170"/></td>
    </tr>
    <ffi:list collection="WireTransferHistory" items="historyItem">
	<ffi:setProperty name="StatusResource" property="ResourceID" value="${historyItem.TranState}"/>
	<ffi:setProperty name="stateDisplay" value="${StatusResource.Resource}"/>
	<ffi:cinclude value1="${stateDisplay}" value2="" operator="equals">
		<ffi:setProperty name="stateDisplay" value="${historyItem.TranState}"/>
	</ffi:cinclude>
    <tr>
        <td align="left" class="columndata"><ffi:getProperty name="historyItem" property="LogDate"/></td>
        <td align="left" class="columndata"><ffi:getProperty name="stateDisplay"/></td>
        <td align="left" class="columndata"><ffi:getProperty name="historyItem" property="ProcessedBy"/></td>
        <td align="left" class="columndata"><div class="description"><ffi:getProperty name="historyItem" property="Comment"/></div></td>
    </tr>
    </ffi:list>
</table>
<br>
<ffi:removeProperty name="stateDisplay"/>
<ffi:removeProperty name="GetAuditWireTransferById"/>
