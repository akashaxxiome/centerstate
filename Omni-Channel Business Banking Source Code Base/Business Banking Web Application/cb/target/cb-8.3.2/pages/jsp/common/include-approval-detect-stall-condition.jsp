<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.Locale" %>	
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.ffusion.beans.approvals.*" %>
<%@ page import="com.ffusion.approvals.constants.IApprovalsConsts" %>
<%@ page import="com.ffusion.tasks.approvals.*" %>
<%@ page import="com.ffusion.beans.common.Currency" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines" %>
<%@ page import="com.ffusion.struts.ConstantDefine" %>


<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;
if( session.getAttribute( "APPROVALS_BC" ) != null ) {
	String isBCStr = ( String )session.getAttribute( "APPROVALS_BC" );
	if( isBCStr.equalsIgnoreCase( "true" ) ) {
		_isBC = true;
	}
}

// determines the destination i.e which page should be refreshed when 
// value of the combo box containg the operation types is changed
String _destination = ( String )session.getAttribute( "SecurePath" ) + 
	( String )session.getAttribute( "approval_display_workflow_jsp" );
if( _isBC ) {
	_destination = ( String )session.getAttribute( "BusinessApprovalWorkflowsPath" );
}

String _opType = ( String )session.getAttribute( "approvalsLevelsOperationType" );
String _setOperationType = "false";
if( _opType != null && _opType.length() > 0  ) {
    _setOperationType = "true";
}

String _accountID = ( String )session.getAttribute( "approvalsLevelsAccountID" );
String _setAccountID = "false";
if( _accountID != null && _accountID.length() > 0  ) {
    _setAccountID = "true";
}


 if( _isBC ) { %>
    <ffi:object id="GetApprovalsEntitledBankEmployees" name="com.ffusion.tasks.approvals.GetApprovalsEntitledBankEmployees" scope="session"/>
    <ffi:setProperty name="GetApprovalsEntitledBankEmployees" property="AffiliateID" value="${ModifyBusiness.AffiliateBankID}" />
    <ffi:setProperty name="GetApprovalsEntitledBankEmployees" property="Type" value="<%=com.ffusion.tasks.approvals.GetApprovalsEntitledBankEmployees.CORPORATE%>" />
    <ffi:cinclude value1="${srvPkgId}" value2="" operator="notEquals">
	    <ffi:setProperty name="GetApprovalsEntitledBankEmployees" property="AffiliateID" value="${ApprovalServicesPackage.AffiliateBankId}" />
            <ffi:cinclude value1="${ServicePackagesCollectionName}" value2="UserServicesPackages" operator="equals">
    		    <ffi:setProperty name="GetApprovalsEntitledBankEmployees" property="Type" value="<%=com.ffusion.tasks.approvals.GetApprovalsEntitledBankEmployees.CONSUMER%>" />						
    		</ffi:cinclude>		    
	</ffi:cinclude>    
    <ffi:process name="GetApprovalsEntitledBankEmployees"/>
<% } else { 
    SecureUser su = ( SecureUser )session.getAttribute( "SecureUser" );
    BusinessEmployee be = new BusinessEmployee();
    be.set( ProfileDefines.BANK_ID, su.getBankID() );
    be.setBusinessId( su.getBusinessID() );
    session.setAttribute( "BusinessEmployee", be ); %>
    
    <ffi:object id="GetBusinessEmployees" name="com.ffusion.tasks.user.GetBusinessEmployees" scope="session"/>
    <ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="BusinessEmployee"/>
    <ffi:process name="GetBusinessEmployees"/>
<% } %>
<ffi:object id="SetWorkflowChainItems" name="com.ffusion.tasks.approvals.SetWorkflowChainItems" scope="session"/>
<% if( _isBC ) { %>
    <ffi:setProperty name="SetWorkflowChainItems" property="AffiliateID" value="${ModifyBusiness.AffiliateBankID}" />
    <ffi:cinclude value1="${srvPkgId}" value2="" operator="notEquals">
	    <ffi:setProperty name="SetWorkflowChainItems" property="AffiliateID" value="${ApprovalServicesPackage.AffiliateBankId}" />
	</ffi:cinclude>        
<% } %>
<ffi:process name="SetWorkflowChainItems"/>

<% String _stallConditionFound = ( String )session.getAttribute( IApprovalsTask.APPROVALS_STALL_CONDITIONS );
   if( _stallConditionFound != null && _stallConditionFound.equalsIgnoreCase( "true" ) ) { 
	   request.setAttribute(ConstantDefine.SERVER_EXCEPTION, String.valueOf(ConstantDefine.INTERVAL_VALIDATION_ERROR));
		
       //<meta http-equiv="refresh" content='0;URL=<ffi:getProperty name="SecurePath"/><ffi:getProperty name="approval_display_stall_condition_jsp"/>'>
   } else { %>
	<ffi:cinclude value1="${GoToApprovalsAddLevelPage}" value2="false" operator="equals">
	    <ffi:object id="UpdateWorkflowLevels" name="com.ffusion.tasks.approvals.UpdateWorkflowLevels" scope="session"/>
	    <ffi:process name="UpdateWorkflowLevels"/>
	    <s:text name="jsp.approvals_level_changed"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${GoToApprovalsAddLevelPage}" value2="false" operator="notEquals">
	    <s:text name="jsp.approvals_level_added"/>
	</ffi:cinclude>
    
	<ffi:object id="IsApproverObj" name="com.ffusion.tasks.approvals.IsApprover" scope="session"/>
	<ffi:process name="IsApproverObj"/>
	    
	<ffi:removeProperty name="SetWorkflowChainItems" />
	
	<%-- 
	<ffi:cinclude value1="<%=_setOperationType %>" value2="true" operator="equals">
		<ffi:cinclude value1="<%=_setAccountID %>" value2="true" operator="equals">
			<meta http-equiv="refresh" content="0;URL=<%= _destination %>?OpType=<%=_opType%>">&AccountID=<%=_accountID%>
		</ffi:cinclude>
		<ffi:cinclude value1="<%=_setAccountID %>" value2="true" operator="notEquals">
			<meta http-equiv="refresh" content="0;URL=<%= _destination %>?OpType=<%=_opType%>">
		</ffi:cinclude>		
	</ffi:cinclude>
	
	<ffi:cinclude value1="<%=_setOperationType %>" value2="true" operator="notEquals">
	    <meta http-equiv="refresh" content="0;URL=<%= _destination %>">
	</ffi:cinclude>
	--%>
<% } %>
