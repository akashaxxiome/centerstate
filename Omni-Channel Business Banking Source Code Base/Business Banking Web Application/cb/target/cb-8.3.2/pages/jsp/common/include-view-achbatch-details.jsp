<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;
if( session.getAttribute( "APPROVALS_BC" ) != null ) {
	String isBCStr = ( String )session.getAttribute( "APPROVALS_BC" );
	if( isBCStr.equalsIgnoreCase( "true" ) ) {
		_isBC = true;
	}
}
%>



<%-- <ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
	<ffi:cinclude value1="${APPROVALS_CB_PAYMENTS}" value2="true" operator="notEquals">
		<table class="ltrow4_color" width="720" border="0" cellspacing="0" cellpadding="3">
	</ffi:cinclude>
	<ffi:cinclude value1="${APPROVALS_CB_PAYMENTS}" value2="true" operator="equals">
		<table class="ltrow2_color" width="720" border="0" cellspacing="0" cellpadding="3">
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
	<table width="720" border="0" cellspacing="0" cellpadding="3">
</ffi:cinclude> --%>
<form action='<ffi:urlEncrypt url="${ach_details_jsp}"/>' method="post" name="ViewForm" id="ViewACHForm">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>

    <%-- include batch header --%>
    <ffi:include page="${PagesPath}common/include-view-achbatch-header.jsp" />
    
    <%-- get the currency code for BC --%>
    <% if( _isBC ) { %>
	<ffi:object id="GetCurrencyCode" name="com.ffusion.tasks.GetCurrencyCode" scope="session"/>
	<ffi:setProperty name="GetCurrencyCode" property="BusinessID" value="${Business.Id}"/>
	<ffi:process name="GetCurrencyCode" />
    <% } %>
<div class="blockWrapper label150">
	<div  class="blockHead"><s:text name="ach.grid.entries"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_513" />:
				</span>
				<span>
				 <ffi:getProperty name="ACHBatch" property="TotalNumberCredits"/>
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_433" /> (<% if( _isBC ) { %><ffi:getProperty name="GetCurrencyCode" property="CurrencyCode" /><% } else { %><ffi:getProperty name="SecureUser" property="BaseCurrency"/><% } %>)
				:</span>
				<span>
					<ffi:getProperty name="ACHBatch" property="TotalCreditAmount" />&nbsp;&nbsp;&nbsp;
				</span>
			</div>
		</div>	
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_514" />
				:</span>
				<span> <ffi:getProperty name="ACHBatch" property="TotalNumberDebits"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_434"/> (<% if( _isBC ) { %><ffi:getProperty name="GetCurrencyCode" property="CurrencyCode" /><% } else { %><ffi:getProperty name="SecureUser" property="BaseCurrency"/><% } %>)
				:</span>
				<span><ffi:getProperty name="ACHBatch" property="TotalDebitAmount" /></span>
			</div>
		</div>
		
					<%-- we only want to reverse ACH batches, not TAX or CHILD SUPPORT --%>
					<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ACHBatch" operator="equals">
					<%-- we don't want to reverse FAILED payments, only status of completed, we also don't want to reverse REVERSAL batches --%>
						<ffi:cinclude value1="${ACHBatch.Status}" value2="Completed" >
							<ffi:cinclude value1="${ACHBatch.CoEntryDesc}" value2="REVERSAL" operator="notEquals">
		    					<ffi:cinclude value1="${ACHBatch.StandardEntryClassCode}" value2="IAT" operator="notEquals">
		                            <div class="blockRow"><ffi:object id="ReverseACHBatch" name="com.ffusion.tasks.ach.ReverseACHBatch" />
		                                <ffi:setProperty name="ReverseACHBatch" property="CheckDate" value="true" />
		                                <ffi:setProperty name="ReverseACHBatch" property="BatchName" value="ACHBatch" />
		                                <ffi:setProperty name="ReverseACHBatch" property="DateFormat" value="${UserLocale.DateFormat}"/>
		                            <ffi:process name="ReverseACHBatch" />
		                            <ffi:cinclude value1="${ReverseACHBatch.DateWithinRangeForReverse}" value2="true">
		        					    <input class="submitbutton" type="button" value="<s:text name="jsp.default_361"/>" onclick="document.location='<ffi:urlEncrypt url="${SecurePath}payments/achbatch-reverse.jsp?Initialize=true"/>';return false;">
		                            </ffi:cinclude>
		                            <ffi:removeProperty name="ReverseACHBatch" /></div>
		    					</ffi:cinclude>
							</ffi:cinclude>
						</ffi:cinclude>
					</ffi:cinclude>
		<ffi:cinclude value1="${ACHBatch.BatchType}" value2="Entry Balanced" operator="equals">
		<ffi:list collection="ACHBatch.OffsetAccounts" items="offsetAcct">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_495"/></span>
				<span>
				<ffi:getProperty name="offsetAcct" property="RoutingNum"/>:<ffi:getProperty name="offsetAcct" property="Number"/>:<ffi:getProperty name="offsetAcct" property="NickName"/>(<ffi:getProperty name="offsetAcct" property="Type"/>)
				</span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_513"/> <ffi:getProperty name="offsetAcct" property="TotalNumberCredits"/>
				</span>
				<span>
					<s:text name="jsp.default_433"/> (<% if( _isBC ) { %><ffi:getProperty name="GetCurrencyCode" property="CurrencyCode" /><% } else { %><ffi:getProperty name="SecureUser" property="BaseCurrency"/><% } %>)
					<ffi:getProperty name="offsetAcct" property="TotalCreditAmount" /></span>
			</div></div>
			<div class="blockRow">
				<div class="inlineBlock" style="width: 50%">
					<span class="sectionsubhead sectionLabel">
						<s:text name="jsp.default_514"/> 
					</span>
					<span><ffi:getProperty name="offsetAcct" property="TotalNumberDebits"/></span>
				</div>
				<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel">
					<s:text name="jsp.default_434"/> (<% if( _isBC ) { %><ffi:getProperty name="GetCurrencyCode" property="CurrencyCode" /><% } else { %><ffi:getProperty name="SecureUser" property="BaseCurrency"/><% } %>)
				</span>
				<span><ffi:getProperty name="offsetAcct" property="TotalDebitAmount" />
				</span>
    		</div></div>
	</ffi:list>
	</ffi:cinclude>
	<ffi:cinclude value1="${ACHBatch.BatchType}" value2="Batch Balanced" operator="equals">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
		   		 <span class="sectionsubhead sectionLabel"><s:text name="jsp.default_496"/></span>
		   		 <span>
					<ffi:getProperty name="ACHBatch" property="OffsetAccount.RoutingNum"/>:<ffi:getProperty name="ACHBatch" property="OffsetAccount.Number"/>:<ffi:getProperty name="ACHBatch" property="OffsetAccount.NickName"/>(<ffi:getProperty name="ACHBatch" property="OffsetAccount.Type"/>)
				</span>
			</div>
		<div class="inlineBlock">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_513"/></span>
			<span><ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalNumberCredits"/></span>
		</div></div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_433"/> (<% if( _isBC ) { %><ffi:getProperty name="GetCurrencyCode" property="CurrencyCode" /><% } else { %><ffi:getProperty name="SecureUser" property="BaseCurrency"/><% } %>)
				</span>
				<span><ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalCreditAmount" /></span>
			</div>
		<div class="inlineBlock">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_514"/> <ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalNumberDebits"/></span>
			<span>
			<s:text name="jsp.default_434"/> (<% if( _isBC ) { %><ffi:getProperty name="GetCurrencyCode" property="CurrencyCode" /><% } else { %><ffi:getProperty name="SecureUser" property="BaseCurrency"/><% } %>)
			<ffi:getProperty name="ACHBatch" property="OffsetAccount.TotalDebitAmount" /></span></div></div>
	</ffi:cinclude>
	</div>
</div>
<div class="marginTop10"></div>
</form>

<%-- include batch entries --%>
<ffi:include page="${PagesPath}common/include-view-achbatch-entries.jsp" />
