<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setProperty name="viewTaxDoneURL" value="${PagesPath}payments/taxpayments.jsp"/>
<% if( session.getAttribute( "tax_details_post_jsp" ) != null ) { %>
    <ffi:setProperty name="viewTaxDoneURL" value="${tax_details_post_jsp}"/>
<% } %>

<% if (session.getAttribute("TaxPaymentView") != null) { %>
	<ffi:object name="com.ffusion.tasks.ach.GetAuditHistory" id="GetAuditHistory" scope="session"/>
		<ffi:setProperty name='GetAuditHistory' property='BeanSessionName' value='TaxPayment'/>
		<ffi:setProperty name="GetAuditHistory" property="AuditLogSessionName" value="TransactionHistory" />
	<ffi:process name="GetAuditHistory"/>

	<ffi:object name="com.ffusion.tasks.ach.ExportACHBatch" id="ExportACHBatch" scope="session"/>
		<ffi:setProperty name='ExportACHBatch' property='BatchName' value='TaxPayment'/>
<% } %>

<table class='<ffi:getProperty name="tax_details_background_color"/>' border="0" cellspacing="0" cellpadding="3" width="716">
	<tr>
		<td class="tbrd_b" colspan="4"><span class="sectionhead">
			<ffi:cinclude value1="${TaxPayment.TaxType}" value2="FEDERAL">&gt; <s:text name="jsp.default_488"/></ffi:cinclude>
			<ffi:cinclude value1="${TaxPayment.TaxType}" value2="STATE">&gt; <s:text name="jsp.default_507"/></ffi:cinclude>
			<ffi:cinclude value1="${TaxPayment.TaxType}" value2="OTHER">&gt; <s:text name="jsp.default_501"/></ffi:cinclude>
		</span></td>
	</tr>
	<tr>
	</tr>
	<tr>
		<td colspan="4"><b>
<span class="sectionsubhead">
<ffi:cinclude value1="${TaxPayment.taxType}" value2="FEDERAL"><s:text name="jsp.default_487"/></ffi:cinclude>
<ffi:cinclude value1="${TaxPayment.taxType}" value2="STATE"><s:text name="jsp.default_506"/> <ffi:getProperty name="TaxPayment" property="taxState" /></ffi:cinclude>
<ffi:cinclude value1="${TaxPayment.taxType}" value2="OTHER"><s:text name="jsp.default_500"/></ffi:cinclude>
<ffi:getProperty name="TaxPayment" property="TaxForm.IRSTaxFormNumber" /></span></b> - <span class="sectionsubhead"><ffi:getProperty name="TaxPayment" property="TaxForm.TaxFormDescription" /></span>
		</td>
	</tr>
	<tr>
		<td width="300"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" width="300" height="1" border="0"></td>
		<td width="220"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" width="220" height="1" border="0"></td>
		<td nowrap><img src='<ffi:getProperty name="spacer_gif"/>' alt="" width="165" height="1" border="0"></td>
		<td nowrap></td>
	</tr>
	<ffi:cinclude value1="${TaxPayment.BatchType}" value2="Unbalanced" operator="notEquals">
	<tr>
		<td class="sectionsubhead " align="right"><s:text name="jsp.default_494"/></td>
		<td class="columndata" colspan="2">
			<ffi:getProperty name="TaxPayment" property="OffsetAccountNumber"/>-<ffi:getProperty name="TaxPayment" property="OffsetAccountType"/>
		</td>
		<td nowrap></td>
	</tr>
	<tr>
		<td colspan="4">
			<div align="center"><img src='<ffi:getProperty name="spacer_gif"/>' width="710" height="1" vspace="3"></div>
		</td>
	</tr>
	</ffi:cinclude>
	<tr>
		<td><span class="sectionsubhead"><s:text name="jsp.common_152"/></span></td>
		<td><span class="sectionsubhead"><s:text name="jsp.common_150"/></span></td>
		<td><span class="sectionsubhead"><s:text name="jsp.common_151"/></span></td>
		<td nowrap></td>
	</tr>
	<tr>
		<td><span class="columndata"><ffi:getProperty name="TaxPayment" property="TaxForm.BankRoutingNumber"/></span></td>
		<td><span class="columndata"><ffi:getProperty name="TaxPayment" property="TaxForm.BankAccountNumber"/></span></td>
		<td><span class="columndata"><ffi:getProperty name="TaxPayment" property="TaxForm.BankAccountType"/></span></td>
		<td nowrap></td>
	</tr>
	<tr>
		<td colspan="4">
			<div align="center"><img src='<ffi:getProperty name="spacer_gif"/>' width="710" height="1" vspace="3"></div>
		</td>
	</tr>

	<ffi:list collection="TaxPayment.taxForm.fields" items="field">
		<ffi:cinclude value1="${field.editable}" value2="TRUE">
		<tr>
			<td width="300">
				<div align="right" class="sectionsubhead"><ffi:getProperty name="field" property="name" /></div>
			</td>
			<td nowrap width="220" class="columndata"><ffi:getProperty name="TaxPayment" property="${field.Fieldname}" />
			</td>
			<td nowrap>&nbsp;</td>
			<td nowrap>&nbsp;</td>
		</tr>
		</ffi:cinclude>
	</ffi:list>
	<tr>
		<td width="300">
			<div align="right" class="sectionsubhead"><s:text name="jsp.default_182"/></div>
		</td>
		<td class="columndata" nowrap width="220">
			<ffi:getProperty name="TaxPayment" property="Date" />
		</td>
	</tr>
	<tr>
		<td align="right" class="columndata">
			<div align="right" class="sectionsubhead"><s:text name="jsp.default_477"/></div>
		</td>
		<td colspan="2" class="columndata">
			<input class="txtbox" type="text" name="textfield5" value="<ffi:getProperty name="TaxPayment" property="TaxAddendaString"/>" disabled size="80">		</td>
	</tr>
	<tr>
		<td align="right" class="columndata">
			<div align="right" class="sectionsubhead"><s:text name="jsp.common_160"/></div>
		</td>
		<td colspan="2" class="columndata">
			<ffi:getProperty name="TaxPayment" property="TotalCreditAmount"/>
		</td>
	</tr>
	<tr>
		<td colspan="4">
			<div align="center"><img src='<ffi:getProperty name="spacer_gif"/>' width="710" height="1" vspace="3"></div>
		</td>
	</tr>
	<tr>
		<td colspan="4">
<% if (session.getAttribute("TaxPaymentView") != null) { %>
			<%-- include batch Transaction History --%>
			<ffi:include page="${FullPagesPath}common/include-view-transaction-history.jsp" />
<% } else { %>
			<br>
<% } %>
		</td>
	</tr>
</table>
<% if (session.getAttribute("TaxPaymentDelete") != null) { %>
		<br>
		<input class="submitbutton" type="button" value="<s:text name="jsp.default_82"/>" onclick='document.location="<%= ( String )session.getAttribute( "viewTaxDoneURL" ) %>"; return false;' >
		&nbsp;&nbsp;&nbsp;
		<input class="submitbutton" type="submit" value="<s:text name="jsp.common_68"/>"></td>
		<br>
<% } else { %>
		<br>
<% if (session.getAttribute("TaxPaymentView") != null) { %>
		&nbsp;&nbsp;&nbsp;
		<input class="submitbutton" type="button" value="<s:text name="jsp.common_77"/>" onclick="document.forms['TaxPaymentForm'].action='<ffi:getProperty name="SecureServletPath"/>ExportACHBatch'; document.forms['TaxPaymentForm'].submit();">
<% } %>
		<br>
<% } %>
</div>

<ffi:removeProperty name="viewTaxDoneURL" />