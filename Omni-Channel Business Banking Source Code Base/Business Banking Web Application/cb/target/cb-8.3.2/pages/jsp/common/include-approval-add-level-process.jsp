<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.approvals.*" %>
<%@ page import="com.ffusion.beans.common.Currency" %>
<%@ page import="com.ffusion.approvals.constants.IApprovalsConsts" %>
<%@ page import="com.ffusion.tasks.approvals.IApprovalsTask" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>

<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;
if( session.getAttribute( "APPROVALS_BC" ) != null ) {
	String isBCStr = ( String )session.getAttribute( "APPROVALS_BC" );
	if( isBCStr.equalsIgnoreCase( "true" ) ) {
		_isBC = true;
	}
}
    String addMinAmount = ( String )request.getParameter( "approvalsLevelsMinAmount" );
    String addMaxAmount = ( String )request.getParameter( "approvalsLevelsMaxAmount" );
    String opType = ( String )request.getParameter( "approvalsLevelsOperationType" );
	String accountID = ( String )request.getParameter( "approvalsLevelsAccountID" );
	String workflowID = ( String )request.getParameter( "approvalsLevelsWorkflowID" );
	String workflowName = ( String )request.getParameter( "approvalsLevelsWorkflowName" );
    // the approval level was already placed into the session
    // so we will now put the chainItems into the session

    // the value of the chainItems is: user or group string - usertype - groupOrUser ID
    String[] chainItemsData	= request.getParameterValues( "chainItemList" );
    String[] chainItemsNames    = new String[ chainItemsData.length ];
    HashMap groupNames = new HashMap();
    HashMap approvalGroupNames = new HashMap();
    boolean isApprovalsGroup = false;
    ApprovalsChainItems chainItems = new ApprovalsChainItems( Locale.getDefault() );
    // create the chain items from the posted data
    // the previous page validated that at least one chain item is present
    
    for( int i = 0 ; i < chainItemsData.length ; i++ ) {
	// create a decision object
	ApprovalsChainItem chainItem = chainItems.add();
	String itemData = chainItemsData[ i ];

	int firstIndex = itemData.indexOf( '-' );
	int middleIndex = itemData.indexOf( '-',  firstIndex + 1 );
	int lastIndex = itemData.indexOf( '-', middleIndex + 1 );
	
	boolean isUsingUser = itemData.substring( 0, firstIndex ).equalsIgnoreCase( "user" );
	if ( !_isBC ) {
		isApprovalsGroup = itemData.substring( 0, firstIndex ).equalsIgnoreCase( "apprgroup" );
	}
	int userType = Integer.parseInt( itemData.substring( firstIndex + 1, middleIndex ) );
	int groupOrUserID = Integer.parseInt( itemData.substring( middleIndex + 1, lastIndex ) );	
	chainItemsNames[ i ]  = itemData.substring ( lastIndex + 1 ).trim();
	
	System.out.println("chainItemName: " + chainItemsNames[i]);
	
	if ( !_isBC && isApprovalsGroup ) {
		approvalGroupNames.put( new Integer( groupOrUserID ), itemData.substring( lastIndex + 1 ) );
	} else {
		groupNames.put( new Integer( groupOrUserID ), itemData.substring( lastIndex + 1 ) );
	}

	chainItem.setGroupOrUserID( groupOrUserID );
	chainItem.setUsingUser( isUsingUser );
	chainItem.setUserType( userType );
	if ( !_isBC ) {
		chainItem.setIsApprovalsGroup( isApprovalsGroup );	
	}
    }

    // put the data into the session
    session.setAttribute( "ApprovalsChainItemsNames", chainItemsNames );
    session.setAttribute( "ApprovalsChainItemsGroupNames", groupNames );
    if ( !_isBC ) {
    	session.setAttribute( "ApprovalsChainItemsApprovalGroupNames", approvalGroupNames );
    }
    session.setAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_CHAIN_ITEMS, chainItems );    

%>

<ffi:setProperty name="addApprovalsLevelMinAmount" value="<%= addMinAmount %>"/>
<ffi:setProperty name="addApprovalsLevelMaxAmount" value="<%= addMaxAmount %>"/>
<ffi:object id="AddApprovalsLevelObject" name="com.ffusion.tasks.approvals.GetApprovalsLevel" scope="session"/>
<ffi:setProperty name="AddApprovalsLevelObject" property="NewLevel" value="true"/>
<% if( _isBC ) { %>
    <ffi:setProperty name="AddApprovalsLevelObject" property="LevelType" value="<%= com.ffusion.approvals.constants.IApprovalsConsts.LEVEL_TYPE_BANK_DEFINED %>"/>
    <ffi:cinclude value1="${srvPkgId}" value2="" operator="equals">
        <ffi:setProperty name="AddApprovalsLevelObject" property="BusinessID" value="${ModifyBusiness.Id}"/>
    </ffi:cinclude>    
    <ffi:cinclude value1="${srvPkgId}" value2="" operator="notEquals">
	    <ffi:setProperty name="AddApprovalsLevelObject" property="BusinessID" value="${ApprovalServicesPackage.Id}" />		
	    <ffi:setProperty name="AddApprovalsLevelObject" property="ObjectType" value="<%=String.valueOf(com.ffusion.approvals.constants.IApprovalsConsts.APPROVAL_OBJECT_TYPE_SERVICE_PACKAGE)%>" />		
	</ffi:cinclude>    
<% } %>
<ffi:cinclude value1="<%= addMinAmount %>" value2="" operator="notEquals">
	<ffi:setProperty name="AddApprovalsLevelObject" property="MinAmount" value="<%= addMinAmount %>"/>
</ffi:cinclude>
<ffi:cinclude value1="<%= addMinAmount %>" value2="" operator="equals">
	<ffi:setProperty name="AddApprovalsLevelObject" property="MinAmount" value="<%= com.ffusion.approvals.constants.IApprovalsConsts.MIN_AMOUNT %>"/>
</ffi:cinclude>
<ffi:cinclude value1="<%= addMaxAmount %>" value2="" operator="notEquals">
	<ffi:setProperty name="AddApprovalsLevelObject" property="MaxAmount" value="<%= addMaxAmount %>"/>
</ffi:cinclude>
<ffi:cinclude value1="<%= addMaxAmount %>" value2="" operator="equals">
	<ffi:setProperty name="AddApprovalsLevelObject" property="MaxAmount" value="<%= com.ffusion.approvals.constants.IApprovalsConsts.MAX_AMOUNT %>"/>
</ffi:cinclude>
<% if( opType != null && opType.length() > 0 ) { %>
    <ffi:setProperty name="AddApprovalsLevelObject" property="OperationType" value="<%= opType %>"/>
<% } %>
<% if( accountID != null && accountID.length() > 0 ) { %>

		<ffi:object id="SearchAcctsByNameNumType" name="com.ffusion.tasks.accounts.SearchAcctsByNameNumType" scope="session"/>
		<ffi:setProperty name="SearchAcctsByNameNumType" property="AccountsName" value="ParentsAccounts"/>
		<ffi:setProperty name="SearchAcctsByNameNumType" property="Id" value="<%=accountID%>"/>
		<ffi:process name="SearchAcctsByNameNumType"/>	
		<ffi:object name="com.ffusion.tasks.accounts.SetAccount" id="SetAccount" scope="request"/>
		<ffi:setProperty name="SetAccount" property="AccountsName" value="FilteredAccounts"/>
		<ffi:setProperty name="SetAccount" property="ID" value="<%=accountID%>"/>
		<ffi:setProperty name="SetAccount" property="AccountName" value="SelectedAccount"/>
		<ffi:process name="SetAccount"/>
		<ffi:setProperty name="AddApprovalsLevelObject" property="AccountID" value="<%= accountID %>"/>
<% } %>
<% if( workflowID != null && workflowID.length() > 0 ) { %>
    <ffi:setProperty name="AddApprovalsLevelObject" property="ApprovalWorkflowId" value="<%= workflowID %>"/>
<% } %>
<% if( workflowName != null && workflowName.length() > 0 ) { %>
    <ffi:setProperty name="AddApprovalsLevelObject" property="ApprovalWorkflowName" value="<%= workflowName %>"/>
<% } %>
<ffi:process name="AddApprovalsLevelObject"/>
<ffi:object id="AddWorkflowLevel" name="com.ffusion.tasks.approvals.AddWorkflowLevel" scope="session"/>
<ffi:process name="AddWorkflowLevel"/>

<ffi:setProperty name="GoToApprovalsAddLevelPage" value="true"/>
<s:include value="%{#session.PagesPath}common/include-approval-detect-stall-condition.jsp"/>
