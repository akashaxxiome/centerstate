<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.Locale" %>	
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.ffusion.beans.approvals.*" %>
<%@ page import="com.ffusion.csil.beans.entitlements.*" %>
<%@ page import="com.ffusion.approvals.constants.IApprovalsConsts" %>
<%@ page import="com.ffusion.tasks.approvals.*" %>
<%@ page import="com.ffusion.beans.common.Currency" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployees" %>
<%@ page import="com.ffusion.beans.user.BusinessEmployee" %>
<%@ page import="com.ffusion.efs.adapters.profile.constants.ProfileDefines" %>

<%
    boolean _isBC = false;
    if( session.getAttribute( "APPROVALS_BC" ) != null ) {
	    String isBCStr = ( String )session.getAttribute( "APPROVALS_BC" );
	    if( isBCStr.equalsIgnoreCase( "true" ) ) {
		    _isBC = true;
	    }
    }
    // read other reqd values from the session
    String[] chainItemsNames = ( String[] )session.getAttribute( "ApprovalsChainItemsNames" );
    HashMap groupNames = ( HashMap )session.getAttribute( "ApprovalsChainItemsGroupNames" );
    HashMap approvalGroupNames = ( HashMap )session.getAttribute( "ApprovalsChainItemsGroupNames" );
    
    ApprovalsGroups apprGroups = null;
    EntitlementGroups entGroups = null;
    
    session.removeAttribute( "ApprovalsChainItemsNames" );
    session.removeAttribute( "ApprovalsChainItemsGroupNames" );
    
    //set variables to be used for displaying stall condition error page
    ArrayList stallMemberList = ( ArrayList )session.getAttribute( IApprovalsTask.APPROVALS_STALL_CONDITIONS_MEMBERLIST );    
    ArrayList entitlementGroupList = ( ArrayList )session.getAttribute( IApprovalsTask.APPROVALS_STALL_CONDITIONS_GROUPID );
    ArrayList approvalGroupList = ( ArrayList )session.getAttribute( IApprovalsTask.APPROVALS_STALL_CONDITIONS_APPROVALGROUPIDS );
    Integer stallNumber = ( Integer )session.getAttribute( IApprovalsTask.APPROVALS_STALL_CONDITIONS_STALLNUMBER );
        
    //String stallGroupName = ( String )groupNames.get( stallGroupID );       
    
    session.removeAttribute( IApprovalsTask.APPROVALS_STALL_CONDITIONS_MEMBERLIST );
    session.removeAttribute( IApprovalsTask.APPROVALS_STALL_CONDITIONS );
    session.removeAttribute( IApprovalsTask.APPROVALS_STALL_CONDITIONS_STALLNUMBER );
    session.removeAttribute( IApprovalsTask.APPROVALS_STALL_CONDITIONS_GROUPID );

%>

<% if ( _isBC ) { %>
	<ffi:object id="GetApprovalsEntitledBankGroups" name="com.ffusion.tasks.approvals.GetApprovalsEntitledBankGroups" scope="session"/>
	<ffi:setProperty name="GetApprovalsEntitledBankGroups" property="AffiliateID" value="${ModifyBusiness.AffiliateBankID}" />
	<ffi:setProperty name="GetApprovalsEntitledBankGroups" property="Type" value="<%=com.ffusion.tasks.approvals.GetApprovalsEntitledBankGroups.CORPORATE%>" />
    <ffi:cinclude value1="${srvPkgId}" value2="" operator="notEquals">
	    <ffi:setProperty name="GetApprovalsEntitledBankGroups" property="AffiliateID" value="${ApprovalServicesPackage.AffiliateBankId}" />
        <ffi:cinclude value1="${ServicePackagesCollectionName}" value2="UserServicesPackages" operator="equals">
		    <ffi:setProperty name="GetApprovalsEntitledBankGroups" property="Type" value="<%=com.ffusion.tasks.approvals.GetApprovalsEntitledBankGroups.CONSUMER%>" />						
		</ffi:cinclude>		    
	</ffi:cinclude>        	
	<ffi:process name="GetApprovalsEntitledBankGroups"/>
	
<%	entGroups = ( EntitlementGroups )session.getAttribute( IApprovalsTask.APPROVALS_ENTITLED_BANK_GROUPS ); %>
<% } else { %>
	<ffi:object id="GetApprovalGroups" name="com.ffusion.tasks.approvals.GetApprovalGroups" scope="session"/>
	<ffi:process name="GetApprovalGroups" />

	<ffi:object id="GetAncestorGroupByType" name="com.ffusion.tasks.admin.GetAncestorGroupByType" scope="session"/>
	<ffi:setProperty name="GetAncestorGroupByType" property="GroupID" value="${SecureUser.EntitlementID}" />
	<ffi:setProperty name="GetAncestorGroupByType" property="GroupType" value="Business" />
	<ffi:setProperty name="GetAncestorGroupByType" property="EntGroupSessionName" value="_groupAncestor" />
	<ffi:process name="GetAncestorGroupByType"/>
			
	<%-- now get the children of that business --%>
	<ffi:object id="GetDescendantsByType" name="com.ffusion.efs.tasks.entitlements.GetDescendantsByType" scope="session"/>
	<ffi:setProperty name="GetDescendantsByType" property="GroupID" value="${_groupAncestor.GroupId}" />
	<ffi:setProperty name="GetDescendantsByType" property="AttributeName" value="_groupList" />
	<ffi:process name="GetDescendantsByType"/>
	
	<%
		apprGroups = ( ApprovalsGroups )session.getAttribute( IApprovalsTask.APPROVALS_GROUPS );
		entGroups = ( EntitlementGroups )session.getAttribute( "_groupList" );		
	%>
<% } %>

<script>
function gotoAddEdit()
{
    <ffi:cinclude value1="${GoToApprovalsAddLevelPage}" value2="true" operator="equals">
    	window.location='<ffi:getProperty name="SecurePath"/><ffi:getProperty name="approval_display_add_edit_workflow_jsp"/>?Edit=false&errorPage=true';
    </ffi:cinclude>
    <ffi:cinclude value1="${GoToApprovalsAddLevelPage}" value2="false" operator="equals">
    	window.location='<ffi:getProperty name="SecurePath"/><ffi:getProperty name="approval_display_add_edit_workflow_jsp"/>?Edit=true&errorPage=true';
    </ffi:cinclude>
}
</script>




<FORM name="stallconditionform" action="javascript:gotoAddEdit()" method="post" >

<table width="100%" border="0">
	<tr>
	    <td align="left" class="sectionhead"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="11" border="0">
		     <ffi:setProperty name="imgTag" value='<img src="${spacer_gif}" alt="" height="1" width="11" border="0">'/>
		     <ffi:getL10NString rsrcFile="common" msgKey="jsp/include-approval-display-stall-condition.jsp-1" parm0="${imgTag}" encode="false" encodeParm0="false"/>
	    </td>
	</tr>
	<tr>
	    <td><img src='<ffi:getProperty name="spacer_gif"/>' height="10" width="1" border="0" alt=""></td>
	</tr>
	<tr>
	    <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		<s:text name="jsp.common_7"/>
		<br><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		<ffi:setProperty name="imgTag" value='<img src="${spacer_gif}" alt="" height="1" width="23" border="0">'/>
		<ffi:getL10NString rsrcFile="common" msgKey="jsp/include-approval-display-stall-condition.jsp-2" parm0="${imgTag}" encode="false" encodeParm0="false"/>		
	    </td>
	</tr>
	<tr>
	    <td><img src='<ffi:getProperty name="spacer_gif"/>' height="10" width="1" border="0" alt=""></td>
	</tr>
	<tr>
	    <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		<s:text name="jsp.common_148"/>
		<br><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		<s:text name="jsp.common_8"/>
		<br><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		&nbsp;&nbsp;&nbsp;&nbsp;(<s:text name="jsp.common_81"/>)
		<br><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		b) <s:text name="jsp.common_108"/>
	    </td>
	</tr>
	<tr>
	    <td><img src='<ffi:getProperty name="spacer_gif"/>' height="10" width="1" border="0" alt=""></td>
	</tr>
	<tr>
	    <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		<s:text name="jsp.common_158"/>
		<br><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		<s:text name="jsp.common_126"/><b> <%if ( stallNumber != null ) { %><%=stallNumber.intValue()%><% } %></b> <s:text name="jsp.common_92"/>
		<br><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		<s:text name="jsp.common_156"/>
	    </td>
	</tr>	
	<tr>
	    <td><img src='<ffi:getProperty name="spacer_gif"/>' height="10" width="1" border="0" alt=""></td>
	</tr>
	<tr>
	    <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
	    <span class="sectionsubhead"><s:text name="jsp.common_147"/></span></td>
	</tr>
	<% if ( chainItemsNames != null ) { %>
	<tr>
	    <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
	<select name = "approvalChainItem" class = "txtbox" multiple size = <%=chainItemsNames.length%> style="width:300px">
	      <% for ( int a = 0; a < chainItemsNames.length; a++ ) { %>
		 	 <option><%=a + 1%>. <%=chainItemsNames[ a ]%></option>		 
	      <% } %>
	</select>
	</td></tr>
	<% } %>
	<tr>
	    <td><img src='<ffi:getProperty name="spacer_gif"/>' height="1" width="1" border="0" alt=""></td>
	</tr>
	<tr>
	    <td>
	       <table width = "100%" border = "0">
	       	   <% if ( stallMemberList != null && stallMemberList.size() != 0 ) { %>
		       <tr>
			   <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
			   <span class="sectionsubhead"><s:text name="jsp.common_53"/>: </span></td>			
		       </tr>
		       	   <%		           
		              String memberName;
		           %>
			   <% for ( int a = 0; a < stallMemberList.size(); a++ ) {
			       memberName = ( String )stallMemberList.get( a );
			       if ( memberName != null || memberName == "" ) { %>
				   <tr>
				       <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
				       <span class = "columndata"><%=memberName%></span></td>
				   </tr>
			       <% } %>
			   <% } %>
		   <% } %>
	       	   <% if ( entitlementGroupList != null && entitlementGroupList.size() != 0 ) { %>
		       <tr>
		           <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		           <span class="sectionsubhead"><s:text name="jsp.common_52"/>: </span></td>
		       </tr>
		       <% EntitlementGroup entGroup = null;
		          String groupName;
		          Integer tempInt;
		       %>
		       <% for ( int a = 0; a < entitlementGroupList.size(); a++ ) {	       	       
		       	       tempInt = ( Integer )entitlementGroupList.get( a );
		       	       entGroup = entGroups.getGroupById( tempInt.toString() ) ;
			       groupName = entGroup.getGroupName();
			       if ( groupName != null || groupName == "" ) { %>
				   <tr>
				       <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
				       <span class = "columndata"><%=groupName%></span></td>
				   </tr>
			       <% } %>
			   <% } %>
		   <% } %>
		   <% if ( !_isBC && approvalGroupList != null && approvalGroupList.size() != 0 ) { %>
		       <tr>
		           <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
		           <span class="sectionsubhead"><s:text name="jsp.common_51"/>: </span></td>
		       </tr>
		       <% ApprovalsGroup apprGroup = null; 
		          String groupName;
		          Integer tempInt;
		       %>
		       <% for ( int a = 0; a < approvalGroupList.size(); a++ ) {		       	       
		               tempInt = ( Integer )approvalGroupList.get( a );
		       	       apprGroup = apprGroups.getByID( tempInt.intValue() );		       	       
			       groupName = apprGroup.getGroupName();
			       if ( groupName != null || groupName == "" ) { %>
				   <tr>
				       <td nowrap align="left" class="submain"><img src='<ffi:getProperty name="spacer_gif"/>' alt="" height="1" width="23" border="0">
				       <span class = "columndata"><%=groupName%></span></td>
				   </tr>
			       <% } %>
			   <% } %>
		   <% } %>
		</table>
	    </td>
	</tr>
	<tr>
	    <td><img src='<ffi:getProperty name="spacer_gif"/>' height="10" width="1" border="0" alt=""></td>
	</tr>
	<tr>
	    <td><div align = "center">
			<sj:a button="true"	onClickTopics="closeDialog" title="%{getText('jsp.default_83')}" >
				<s:text name="jsp.default_82"/>
			</sj:a>
	    </div></td>
	</tr>
</table>

</FORM>

<ffi:cinclude value1="${GoToApprovalsAddLevelPage}" value2="true" operator="equals">
    <ffi:object name="com.ffusion.tasks.approvals.RemoveWorkflowLevel" id = "RemoveWorkflowLevel" />
    <ffi:process name="RemoveWorkflowLevel" />
</ffi:cinclude>
<ffi:removeProperty name="SetWorkflowChainItems" />
<ffi:removeProperty name="_groupList"/>
<ffi:removeProperty name="_groupAncestor"/>
