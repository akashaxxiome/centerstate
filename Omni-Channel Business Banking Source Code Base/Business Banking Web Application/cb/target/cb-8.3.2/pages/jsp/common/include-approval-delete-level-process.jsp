<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;


// determines the destination i.e which page should be refreshed when 
// value of the combo box containg the operation types is changed
String _destination = ( String )session.getAttribute( "SecurePath" ) + 
	( String )session.getAttribute( "approval_display_workflow_jsp" );

String levelID = com.ffusion.util.HTMLUtil.encode(request.getParameter( "LevelID" ));
String workflowID = com.ffusion.util.HTMLUtil.encode(request.getParameter( "WorkflowID" ));
String opType = com.ffusion.util.HTMLUtil.encode(request.getParameter( "OperationType" ));
%>
	<%if(workflowID !=null && workflowID.length()>0){%>
	<ffi:object id="DeleteApprovalsLevelObject" name="com.ffusion.tasks.approvals.SetWorkflowApprovalLevels" scope="session"/>
    <ffi:setProperty name="DeleteApprovalsLevelObject" property="WorkflowId" value="<%=workflowID %>"/>
	<ffi:process name="DeleteApprovalsLevelObject"/>
	<ffi:object id="RemoveWorkflowLevels" name="com.ffusion.tasks.approvals.RemoveWorkflowLevels" scope="session"/>
    <ffi:process name="RemoveWorkflowLevels"/>
	
	<ffi:removeProperty name="DeleteApprovalsLevelObject"/>
	<ffi:removeProperty name="RemoveWorkflowLevels"/>
	<%}else{%>
    <ffi:object id="DeleteApprovalsLevelObject" name="com.ffusion.tasks.approvals.GetApprovalsLevel" scope="session"/>
    <ffi:setProperty name="DeleteApprovalsLevelObject" property="LevelID" value="<%= levelID %>"/>
	<ffi:process name="DeleteApprovalsLevelObject"/>
	<ffi:object id="RemoveWorkflowLevel" name="com.ffusion.tasks.approvals.RemoveWorkflowLevel" scope="session"/>
    <ffi:process name="RemoveWorkflowLevel"/>
	<ffi:removeProperty name="DeleteApprovalsLevelObject"/>
	<ffi:removeProperty name="RemoveWorkflowLevel"/>
	<%}%>
   
	

    
    <%-- invoke the IsApprover task as the user could have become an approver --%>
    <ffi:object id="IsApproverObj" name="com.ffusion.tasks.approvals.IsApprover" scope="session"/>
    <ffi:process name="IsApproverObj"/>
    
    <ffi:removeProperty name="DeleteApprovalsLevelID"/>
    

	<%if(workflowID !=null && workflowID.length()>0){%>
		Approvals Workflow deleted
	<%}else{%>
	<s:text name="jsp.approvals_level_deleted"/>
	<%}%>
<%-- 
<html>
<head>
<% if( opType != null && opType.length() > 0 ) { %>
<meta http-equiv="refresh" content="0;URL=<%= _destination %>?OpType=<%= opType %>">
<% } else { %>
<meta http-equiv="refresh" content="0;URL=<%= _destination %>">
<% } %>
</head>
</html>
--%>