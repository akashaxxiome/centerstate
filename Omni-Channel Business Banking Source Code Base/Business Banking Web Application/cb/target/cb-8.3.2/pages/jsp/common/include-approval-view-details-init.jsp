<%@ page import="com.ffusion.beans.approvals.ApprovalsItems" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;
boolean _isInit = true;
if( session.getAttribute( "APPROVALS_BC" ) != null ) {
	String isBCStr = ( String )session.getAttribute( "APPROVALS_BC" );
	if( isBCStr.equalsIgnoreCase( "true" ) ) {
		_isBC = true;
	}
}
    // QTS 637465 - if sorting, don't get a new batch object
    if (request.getParameter("ACHEntries.ToggleSortedBy") != null)  // if sorting, we don't want a new object
        _isInit = false;
    
%>

<%-- clean up the session --%>
<ffi:removeProperty name="IncludeViewPayment"/>
<ffi:removeProperty name="IncludeViewTransfer"/>
<ffi:removeProperty name="WireTransfer"/>
<ffi:cinclude value1="${wireInBatch}" value2="true" operator="notEquals">
	<ffi:removeProperty name="WireBatch"/>
	<ffi:removeProperty name="Wires"/>
	<ffi:removeProperty name="wire_details_post_jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireInBatch}" value2="true" operator="equals">
	<ffi:setProperty name="wire_details_post_jsp" value="${ApprovalsBackPage}"/>
</ffi:cinclude>
<ffi:removeProperty name="collectionToShow"/>
<%
    if (_isInit) { %>
<ffi:removeProperty name="ACHBatch"/>
<ffi:removeProperty name="TaxPayment"/>
<ffi:removeProperty name="TaxForm"/>
<ffi:removeProperty name="TaxFields"/>
<ffi:removeProperty name="ach_details_post_jsp"/>
<ffi:removeProperty name="tax_details_post_jsp"/>
<% } %>
<ffi:removeProperty name="payment_details_post_jsp"/>
<ffi:removeProperty name="transfer_details_post_jsp"/>
<ffi:removeProperty name="wirebatch_details_post_jsp"/>
<ffi:removeProperty name="cashcon_details_post_jsp"/>
<ffi:removeProperty name="ApprovalsViewTransactionDetails"/>

<%-- get the approvals item --%>
<ffi:removeProperty name="ApprovalsItem" />
<ffi:object name="com.ffusion.tasks.util.SaveFilteredItem" id="SaveFilteredItem" scope="request"/>
	<ffi:setProperty name="SaveFilteredItem" property="Collection" value="<%=com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ITEMS%>"/>
	<ffi:setProperty name="SaveFilteredItem" property="Filter" value="ITEMID=${ItemID}"/>
	<ffi:setProperty name="SaveFilteredItem" property="Item" value="<%=com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ITEM%>"/>
<ffi:process name="SaveFilteredItem"/>

<ffi:setProperty name="ApprovalsItem" property="Item.SubmissionDateTime.Format" value="${UserLocale.DateFormat}" />
<ffi:setProperty name="ApprovalsItem" property="Item.Transaction.DateFormat" value="${UserLocale.DateFormat}" />
<% 
com.ffusion.beans.approvals.ApprovalsItem item = ( com.ffusion.beans.approvals.ApprovalsItem )session.getAttribute( "ApprovalsItem" );
com.ffusion.beans.tw.TWTransaction transaction = ( com.ffusion.beans.tw.TWTransaction )item.getItem();
int transactionType = transaction.getTransactionType();
%>
<%
if( transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_BILL_PAYMENT || 
    transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_RECURRING_BILL_PAYMENT ) {
    	com.ffusion.beans.billpay.Payment payment = ( com.ffusion.beans.billpay.Payment )transaction.getTransaction();
	session.setAttribute( "IncludeViewPayment", payment ); 
%>
	<ffi:setProperty name="payment_details_post_jsp" value="${ApprovalsBackPage}"/>
<%
} else if( transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_ACCOUNT_TRANSFER || 
	   transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_RECURRING_ACCOUNT_TRANSFER ) {
    	com.ffusion.beans.banking.Transfer transfer = ( com.ffusion.beans.banking.Transfer )transaction.getTransaction();
	session.setAttribute( "IncludeViewTransfer", transfer ); 
%>
	<ffi:setProperty name="transfer_details_post_jsp" value="${ApprovalsBackPage}"/>
<%
	
} else if( transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_WIRE_TRANSFER || 
	   transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_RECURRING_WIRE_TRANSFER ) {
    	com.ffusion.beans.wiretransfers.WireTransfer wire = ( com.ffusion.beans.wiretransfers.WireTransfer )transaction.getTransaction();
	session.setAttribute( "WireTransfer", wire ); 
%>
	<ffi:setProperty name="wire_details_post_jsp" value="${ApprovalsBackPage}"/>
<%	
} else if( transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_ACH_BATCH || 
	  	transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_RECURRING_ACH_BATCH ||
		transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_TAX_PAYMENT ||
	  	transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_REC_TAX_PAYMENT ||
		transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_CHILD_SUPPORT_PAYMENT ||
	  	transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_REC_CHILD_SUPPORT_PAYMENT) {
        if (_isInit)
        {
        	com.ffusion.beans.ach.ACHBatch ach = ( com.ffusion.beans.ach.ACHBatch )transaction.getTransaction();

            session.setAttribute( "ACHBatch", ach );
            session.setAttribute( "ACHEntries", ach.getACHEntries() );
            // QTS 591019
            // because ACH is stored completely in BPW, we want to get the batch from BPW
            // so that the entire batch isn't stored in the Transaction Warehouse
            // if BatchID is null, it uses BatchName (which is ACHBatch) to retrieve the batch
            // from the backend.
            %>
            <ffi:object name="com.ffusion.tasks.ach.GetACHBatch" id="GetACHBatch" scope="session"/>
            <ffi:process name="GetACHBatch"/>
            <ffi:removeProperty name="GetACHBatch"/>

       <% } %>

	<ffi:setProperty name="ach_details_post_jsp" value="${ApprovalsBackPage}"/>
<%
	
} else if( transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_WIRE_BATCH ) { 
    	com.ffusion.beans.wiretransfers.WireBatch wire = ( com.ffusion.beans.wiretransfers.WireBatch )transaction.getTransaction();
	session.setAttribute( "WireBatch", wire ); 
	session.setAttribute( "Wires", wire.getWires() ); 
	session.setAttribute( "collectionToShow", "Wires" ); 
%>
	<ffi:setProperty name="wirebatch_details_post_jsp" value="${ApprovalsBackPage}"/>
<%	
	
} else if( transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_CASHCON ) {
	com.ffusion.beans.cashcon.CashCon cashCon = ( com.ffusion.beans.cashcon.CashCon )transaction.getTransaction();
	session.setAttribute( "AddEditCashCon", cashCon );
%>
	<% if( _isBC ) { %>
	    <ffi:object name="com.ffusion.tasks.affiliatebank.GetAffiliateBankByID" id="GetAffiliateBankByID"  scope="session"/>
	    <ffi:setProperty name="GetAffiliateBankByID" property="AffiliateBankID" value="${Business.AffiliateBankID}"/>
	    <ffi:setProperty name="GetAffiliateBankByID" property="EntitlementBypass" value="true" />
	    <ffi:process name="GetAffiliateBankByID" />
	    <ffi:object name="com.ffusion.tasks.cashcon.GetCashConCompanies" id="GetCashConCompanies"  scope="session"/>
	    <ffi:setProperty name="GetCashConCompanies" property="CompId" value="${Business.Id}"/>
	    <ffi:setProperty name="GetCashConCompanies" property="FIId" value="${AffiliateBank.FIBPWID}"/>
	    <ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
	    <ffi:process name="GetCashConCompanies" />
	<% } else {  %>
	    <ffi:object name="com.ffusion.tasks.cashcon.GetCashConCompanies" id="GetCashConCompanies"  scope="session"/>
	    <ffi:setProperty name="GetCashConCompanies" property="CompId" value="${User.BUSINESS_ID}"/>
	    <ffi:setProperty name="GetCashConCompanies" property="FIId" value="${User.BANK_ID}"/>
	    <ffi:setProperty name="GetCashConCompanies" property="EntitlementGroupId" value="${Business.EntitlementGroupId}"/>
	    <ffi:process name="GetCashConCompanies" />
	<% } %>
	<ffi:setProperty name="cashcon_details_post_jsp" value="${ApprovalsBackPage}"/>
	<ffi:object name="com.ffusion.tasks.cashcon.SetCashConCompany" id="SetCashConCompany" scope="session"/>
	<ffi:setProperty name="SetCashConCompany" property="CollectionSessionName" value="CashConCompanies"/>
	<ffi:setProperty name="SetCashConCompany" property="CompanySessionName" value="CashConCompany"/>
	<ffi:setProperty name="SetCashConCompany" property="BPWID" value="${AddEditCashCon.CompanyID}"/>
	<ffi:process name="SetCashConCompany" />
<%
}else if( transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_POSITIVE_PAY_DECISION ) {
	com.ffusion.beans.positivepay.PPayDecision ppayDecision = ( com.ffusion.beans.positivepay.PPayDecision )transaction.getTransaction();
	session.setAttribute( "VeiwPPayDecision", ppayDecision );
	%>
	<ffi:setProperty name="ppay_details_post_jsp" value="${ApprovalsBackPage}"/>
	<%
} else if( transactionType == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_REVERSE_POSITIVE_PAY ) {
	com.ffusion.beans.positivepay.PPayDecision ppayDecision = ( com.ffusion.beans.positivepay.PPayDecision )transaction.getTransaction();
	session.setAttribute( "ViewRPPayDecision", ppayDecision );
	%>
	<ffi:setProperty name="rppay_details_post_jsp" value="${ApprovalsBackPage}"/>
	<%
}
%>


<ffi:setProperty name="ApprovalsViewTransactionDetails" value="true" />