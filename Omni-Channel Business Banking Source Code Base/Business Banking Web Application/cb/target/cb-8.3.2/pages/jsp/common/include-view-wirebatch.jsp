<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- Default the page links to DOMESTIC/INTERNATIONAL wires --%>
<% if( session.getAttribute( "wirebatch_details_post_jsp") == null ) {  %>
    <ffi:setProperty name="wireBatchDoneURL" value="${PagesPath}payments/wiretransfers.jsp"/>
<% } else { %>
    <ffi:setProperty name="wireBatchDoneURL" value="${wirebatch_details_post_jsp}"/>
<% } 
String returnURL = ( String )session.getAttribute( "wireBatchDoneURL" );
%>
<ffi:setProperty name="WireBatch" property="DateFormat" value="${UserLocale.DateFormat}"/>
<div class="blockWrapper" role="form" aria-labelledby="batchInfoHeader">
	<div  class="blockHead"><h2 id="batchInfoHeader"><s:text name="jsp.default_65"/></h2></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireBatchNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_66"/></span>
                <span id="viewWireBatchNameValue" class="columndata"><ffi:getProperty name="WireBatch" property="BatchName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewWireBatchTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_67"/></span>
                <% String batchDest = ""; %>
                <ffi:getProperty name="WireBatch" property="BatchDestination" assignTo="batchDest"/>
                <span id="viewWireBatchTypeValue" class="columndata"><%-- <ffi:setProperty name="WireDestinations" property="Key" value="<%= batchDest %>"/> --%>
                <ffi:getProperty name="WireBatch" property="BatchDestination"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireBatchApplicationTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_47"/></span>
                <span id="viewWireBatchApplicationTypeValue" class="columndata"><ffi:getProperty name="WireBatch" property="BatchType"/></span>
			</div>
			<div class="inlineBlock">
				<span id="viewWireBatchValueDateLabel" width="115" class="sectionsubhead sectionLabel">
					<ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_HOST %>" operator="notEquals">Value Date</ffi:cinclude>
					<ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_HOST %>" operator="equals">Processing Date</ffi:cinclude>
				</span>
                <span id="viewWireBatchValueDateValue"  width="220" class="columndata"><ffi:getProperty name="WireBatch" property="DueDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewWireBatchDateOfPostLabel"  class="sectionsubhead sectionLabel"><s:text name="jsp.default_141"/></span>
                <span id="viewWireBatchDateOfPostValue" class="columndata"><ffi:getProperty name="WireBatch" property="DateToPost"/></span>
			</div>
			<ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_HOST %>" operator="equals">
				<div class="inlineBlock">
					<span id="viewWireBatchSettlementDateLabel"  class="sectionsubhead sectionLabel"><s:text name="jsp.default_380"/></span>
                    <span id="viewWireBatcSettelementDateValue"  class="columndata"><ffi:getProperty name="WireBatch" property="SettlementDate"/></span>
				</div>
			</ffi:cinclude>
		</div>
	</div>
</div>

<div class="blockWrapper marginTop20"  role="form" aria-labelledby="includeBatchesHeader">
	<div  class="blockHead"><h2 id="includeBatchesHeader"><s:text name="jsp.default_239"/></h2></div>
</div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr class="header">
                <td id="viewWireBatchBeneficiaryLabel" align="center" class="sectionsubhead">
                <% if (batchDest.equals(WireDefines.WIRE_HOST)) { %><s:text name="jsp.default_230"/>
                <% } else if (batchDest.equals(WireDefines.WIRE_DRAWDOWN)) { %><s:text name="jsp.default_486"/>
                <% } else { %><s:text name="jsp.default_68"/>
                <% } %>
                </td>
                <td id="viewWireBatchStatusLabel" class="sectionsubhead" align="left"><s:text name="jsp.default_388"/></td>
                <td id="viewWireBatchAmountLabel" class="sectionsubhead" align="left" colspan="2"><s:text name="jsp.default_43"/></td>
            </tr>
			 <% int idCount=0; %>
            <ffi:list collection="WireBatch.Wires" items="wire" >
            <% if( session.getAttribute( "wirebatch_details_post_jsp") == null ) {  %>
            <ffi:setProperty name="wireViewPageURL" value="/cb/pages/jsp/wires/viewWireTransferAction.action?ID=${wire.ID}&transType=SINGLE"/>
            <%  } %>
            <% if( session.getAttribute( "ApprovalsViewTransactionDetails") != null ) { %>
		    <ffi:setProperty name="wireViewPageURL" value="/cb/pages/jsp/wires/${PagesPath}${approval_view_wire_jsp}?ID=${wire.ID}&trackingID=${wire.TrackingID}&collectionName=${collectionToShow}&wireInBatch=true"/>    
			<% } %>
            <tr>
            <ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
                <td align="center" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
            </ffi:cinclude>
            <ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
                <td id="viewWireBatchBeneficiaryNameValue<%=idCount %>" align="center" class="columndata" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
                    <ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
            </ffi:cinclude>
            
                <td id="viewWireBatchStatusValue<%=idCount %>"" class="columndata" align="left" nowrap>
                    <ffi:getProperty name="wire" property="StatusName"/>
                </td>

                <td id="viewWireBatchAmountValue<%=idCount++ %>"" class="columndata" align="right" nowrap><ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/></td>
                <td class="columndata" align="right" nowrap>
                    <%-- <ffi:link url="${wireViewPage}?ID=${wire.ID}&trackingID=${wire.TrackingID}&collectionName=${collectionToShow}&wireInBatch=true"><img src='<ffi:getProperty name="view_details_gif"/>' alt="View Info" height="14" width="19" border="0" hspace="3"></ffi:link>--%>
					<ffi:cinclude value1="${wire.CanView}" value2="true" operator="equals">
                	<a class='ui-button' title='View' href='#' onClick="ns.wire.viewWireTransferFromBatch('<ffi:urlEncrypt url="${wireViewPageURL}"/>')">
                	<span class='ui-icon ui-icon-info'></span></a>
                	</ffi:cinclude>
                </td>
            </tr>
            
			<ffi:cinclude value1="${wire.Status}" value2="<%= String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_REJECTED) %>" operator="equals" >
				<tr>
					<td colspan=4>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">

							<tr>
							<td width="25%" class="sectionsubhead" align="right" ><s:text name="jsp.common_138"/>&nbsp;</td>
							<td align="left" width="32%" class="columndata">
								<div class="description">
									<ffi:getProperty name="wire" property="RejectReason"/>
									<ffi:cinclude value1="${wire.RejectReason}" value2="" operator="equals"><s:text name="jsp.common_103"/></ffi:cinclude>
								</div>
							</td>
							<ffi:cinclude value1="${wire.ApproverName}" value2="" operator="notEquals" >
								<td width="18%" class="sectionsubhead" align="right"><s:text name="jsp.common_141"/></td>
								<td align="left" width="25%" class="columndata">
								<ffi:getProperty name="wire" property="ApproverName" />
								<ffi:cinclude value1="true" value2="${wire.ApproverIsGroup}" >(<s:text name="jsp.common_82"/>)</ffi:cinclude>
								</td>
							</ffi:cinclude>
							</tr>
						</table>
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude value1="${wire.Status}" value2="<%= String.valueOf(WireDefines.WIRE_STATUS_PENDING_APPROVAL) %>" operator="equals" >
				<ffi:cinclude value1="${wire.ApproverName}" value2="" operator="notEquals" >
					<tr>
						<td colspan=4>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
								<td width="33%" class="sectionsubhead" align="right"><s:text name="jsp.common_26"/>&nbsp;</td>
								<td align="left" width="67%" class="columndata">
								<ffi:getProperty name="wire" property="ApproverName" />
								<ffi:cinclude value1="true" value2="${wire.ApproverIsGroup}" >(<s:text name="jsp.common_82"/>)</ffi:cinclude>
								</td>
								</tr>
							</table>
						</td>
					</tr>
				</ffi:cinclude>
                <ffi:cinclude value1="${wire.ApproverName}" value2="" operator="equals" >
                <tr>
                    <td class="sectionsubhead" nowrap colspan=4>
                        <div align="right">
                                <s:text name="jsp.common_149"/>&nbsp;
                        </div>
                    </td>
                </tr>
                </ffi:cinclude>
			</ffi:cinclude>

            </ffi:list>
            
            <tr>
                <td class="columndata">&nbsp;</td>
                <td id="viewWireBatchTotalAmountLabel" class="columndata" align="right" valign="bottom" nowrap>Total&nbsp;(<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                <td id="viewWireBatchTotalAmountValue" class="tbrd_t sectionsubhead" width="1%" align="right" valign="bottom" nowrap><ffi:getProperty name="WireBatch" property="TotalOrigAmount"/></td>
                <td class="columndata" width="1%" nowrap>&nbsp;</td>
            </tr>
        </table>
	</div>
</div>
<table width="750" cellpadding="0" cellspacing="0" border="0">
    <td class='<ffi:getProperty name="wirebatch_details_background_color"/>' align="center">
        <%-- <table width="98%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td id="viewWireBatchInfoLabel" align="left" class="tbrd_b sectionhead">&gt; <s:text name="jsp.default_65"/></td>
            </tr>
        </table> --%>
        <%-- <table width="715" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="357" valign="top">
                    <table width="355" cellpadding="3" cellspacing="0" border="0">
                        <tr>
                            <td id="viewWireBatchNameLabel" align="left" width="115" class="sectionsubhead"><s:text name="jsp.default_66"/></td>
                            <td id="viewWireBatchNameValue" align="left" width="228" class="columndata"><ffi:getProperty name="WireBatch" property="BatchName"/></td>
                        </tr>
                        <tr>
                            <td id="viewWireBatchTypeLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_67"/></td>
                            <% String batchDest = ""; %>
                            <ffi:getProperty name="WireBatch" property="BatchDestination" assignTo="batchDest"/>
                            <td id="viewWireBatchTypeValue" align="left" class="columndata"><ffi:setProperty name="WireDestinations" property="Key" value="<%= batchDest %>"/><ffi:getProperty name="WireDestinations" property="Value"/></td>
                        </tr>
                        <tr>
                            <td id="viewWireBatchApplicationTypeLabel" align="left" class="sectionsubhead"><s:text name="jsp.default_47"/></td>
                            <td id="viewWireBatchApplicationTypeValue" align="left" class="columndata"><ffi:getProperty name="WireBatch" property="BatchType"/></td>
                        </tr>
                    </table>
                </td>
                <td class="tbrd_l" width="10"><img src='<ffi:getProperty name="spacer_gif"/>' width="1" height="1" alt=""></td>
                <td width="348" valign="top">
                    <table width="347" cellpadding="3" cellspacing="0" border="0">
                        <tr>
                            <td id="viewWireBatchValueDateLabel" align="left" width="115" class="sectionsubhead">
								<ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_HOST %>" operator="notEquals">Value Date</ffi:cinclude>
								<ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_HOST %>" operator="equals">Processing Date</ffi:cinclude>
							</td>
                            <td id="viewWireBatchValueDateValue" align="left"  width="220" class="columndata"><ffi:getProperty name="WireBatch" property="DueDate"/></td>
                        </tr>
                        <ffi:cinclude value1="${WireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_HOST %>" operator="equals">
						<tr>
                            <td id="viewWireBatchSettlementDateLabel" align="left"  class="sectionsubhead"><s:text name="jsp.default_380"/></td>
                            <td id="viewWireBatcSettelementDateValue" align="left"  class="columndata"><ffi:getProperty name="WireBatch" property="SettlementDate"/></td>
                        </tr>
						</ffi:cinclude>
                        <tr>
                            <td id="viewWireBatchDateOfPostLabel" align="left"  class="sectionsubhead"><s:text name="jsp.default_141"/></td>
                            <td id="viewWireBatchDateOfPostValue" align="left" class="columndata"><ffi:getProperty name="WireBatch" property="DateToPost"/></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table> --%>
        <%-- <br>
        <table width="98%" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td id="viewWireBatchIncludeBatchesLabel" align="left" class="tbrd_b sectionhead">&gt; <s:text name="jsp.default_239"/></td>
            </tr>
        </table> --%>
        <%-- <table width="450" border="0" cellspacing="0" cellpadding="3">
            <tr>
                <td id="viewWireBatchBeneficiaryLabel" align="left" class="sectionsubhead">
                <% if (batchDest.equals(WireDefines.WIRE_HOST)) { %><s:text name="jsp.default_230"/>
                <% } else if (batchDest.equals(WireDefines.WIRE_DRAWDOWN)) { %><s:text name="jsp.default_486"/>
                <% } else { %><s:text name="jsp.default_68"/>
                <% } %>
                </td>
                <td id="viewWireBatchStatusLabel" class="sectionsubhead" align="left"><s:text name="jsp.default_388"/></td>
                <td id="viewWireBatchAmountLabel" class="sectionsubhead" align="right"><s:text name="jsp.default_43"/></td>
            </tr>
			 <% int idCount=0; %>
            <ffi:list collection="WireBatch.Wires" items="wire" >
            <% if( session.getAttribute( "wirebatch_details_post_jsp") == null ) {  %>
            <ffi:setProperty name="wireViewPageURL" value="/cb/pages/jsp/wires/viewWireTransferAction.action?ID=${wire.ID}&transType=SINGLE"/>
            <%  } %>
            <% if( session.getAttribute( "ApprovalsViewTransactionDetails") != null ) { %>
		    <ffi:setProperty name="wireViewPageURL" value="/cb/pages/jsp/wires/${PagesPath}${approval_view_wire_jsp}?ID=${wire.ID}&trackingID=${wire.TrackingID}&collectionName=${collectionToShow}&wireInBatch=true"/>    
			<% } %>
            <tr>
            <ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="equals" >
                <td align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="HostID"/></td>
            </ffi:cinclude>
            <ffi:cinclude value1="${WireBatch.BatchType}" value2="HOST" operator="notEquals" >
                <td id="viewWireBatchBeneficiaryNameValue<%=idCount %>" align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
                    <ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
            </ffi:cinclude>
            
                <td id="viewWireBatchStatusValue<%=idCount %>"" class="columndata" align="left" nowrap>
                    <ffi:getProperty name="wire" property="StatusName"/>
                </td>

                <td id="viewWireBatchAmountValue<%=idCount++ %>"" class="columndata" align="right" nowrap><ffi:getProperty name="wire" property="AmountValue.CurrencyStringNoSymbol"/></td>
                <td class="columndata" align="right" nowrap>
                    <ffi:link url="${wireViewPage}?ID=${wire.ID}&trackingID=${wire.TrackingID}&collectionName=${collectionToShow}&wireInBatch=true"><img src='<ffi:getProperty name="view_details_gif"/>' alt="View Info" height="14" width="19" border="0" hspace="3"></ffi:link>
									
                	<a class='ui-button' title='View' href='#' onClick="ns.wire.viewWireTransferFromBatch('<ffi:urlEncrypt url="${wireViewPageURL}"/>')">
                	<span class='ui-icon ui-icon-info'></span></a>
                </td>
            </tr>
            
			<ffi:cinclude value1="${wire.Status}" value2="<%= String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_REJECTED) %>" operator="equals" >
				<tr>
					<td colspan=4>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">

							<tr>
							<td width="25%" class="sectionsubhead" align="right" ><s:text name="jsp.common_138"/>&nbsp;</td>
							<td align="left" width="32%" class="columndata">
								<div class="description">
									<ffi:getProperty name="wire" property="RejectReason"/>
									<ffi:cinclude value1="${wire.RejectReason}" value2="" operator="equals"><s:text name="jsp.common_103"/></ffi:cinclude>
								</div>
							</td>
							<ffi:cinclude value1="${wire.ApproverName}" value2="" operator="notEquals" >
								<td width="18%" class="sectionsubhead" align="right"><s:text name="jsp.common_141"/></td>
								<td align="left" width="25%" class="columndata">
								<ffi:getProperty name="wire" property="ApproverName" />
								<ffi:cinclude value1="true" value2="${wire.ApproverIsGroup}" >(<s:text name="jsp.common_82"/>)</ffi:cinclude>
								</td>
							</ffi:cinclude>
							</tr>
						</table>
					</td>
				</tr>
			</ffi:cinclude>
			<ffi:cinclude value1="${wire.Status}" value2="<%= String.valueOf(WireDefines.WIRE_STATUS_PENDING_APPROVAL) %>" operator="equals" >
				<ffi:cinclude value1="${wire.ApproverName}" value2="" operator="notEquals" >
					<tr>
						<td colspan=4>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
								<td width="33%" class="sectionsubhead" align="right"><s:text name="jsp.common_26"/>&nbsp;</td>
								<td align="left" width="67%" class="columndata">
								<ffi:getProperty name="wire" property="ApproverName" />
								<ffi:cinclude value1="true" value2="${wire.ApproverIsGroup}" >(<s:text name="jsp.common_82"/>)</ffi:cinclude>
								</td>
								</tr>
							</table>
						</td>
					</tr>
				</ffi:cinclude>
                <ffi:cinclude value1="${wire.ApproverName}" value2="" operator="equals" >
                <tr>
                    <td class="sectionsubhead" nowrap colspan=4>
                        <div align="right">
                                <s:text name="jsp.common_149"/>&nbsp;
                        </div>
                    </td>
                </tr>
                </ffi:cinclude>
			</ffi:cinclude>

            </ffi:list>
            
            <tr>
                <td class="columndata">&nbsp;</td>
                <td id="viewWireBatchTotalAmountLabel" class="columndata" align="right" valign="bottom" nowrap>Total&nbsp;(<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                <td id="viewWireBatchTotalAmountValue" class="tbrd_t sectionsubhead" width="1%" align="right" valign="bottom" nowrap><ffi:getProperty name="WireBatch" property="TotalOrigAmount"/></td>
                <td class="columndata" width="1%" nowrap>&nbsp;</td>
            </tr>
        </table> --%>
        <br>
        <form method="post">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
        <%--<input type="button" class="submitbutton" value="DONE" onclick='document.location="<%= returnURL %>";'>&nbsp;--%>
        </form>
        </td>
    </tr>
</table>
<ffi:removeProperty name="wireViewPage"/>
<ffi:removeProperty name="wireBatchDoneURL"/>
