<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%-- 
parameters need to be prepared:

fileType: 
actionName: 	
ProcessImportTask

uploadingID: default to ns.common.uploadingID defined in common.js 

--%>

<script language="JavaScript" type="text/javascript">
<!--

// This function is used to check event of enter key.
$("#uploaderID").keypress(function(e) {
	if (e.which == 13) {  
		//$("#uploadFileButton").click();
		return false;
	}   
});	


//--></script>

<div align="center">
<fieldset id="fileUploadSelectFileFieldSetID" class="dialogueData">
<legend><s:text name="jsp.default_211"/></legend>
			<span class="progressbar" style="display:none;" id="uploadprogressbar"></span>
			<div id="showImportFileNameID"></div>
			<div id="importedFileFormDivID">
			<table width="282" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="columndata_grey" colspan="2">
						<div align="left">
							<span class="columndata"><s:text name="jsp.default_187"/></span></div>
					</td>
				</tr>
				<tr>
					<td class="columndata_grey" colspan="2">
						<s:form id="uploaderID" name="FileForm" enctype="multipart/form-data" action="%{#request.actionName}" method="post" theme="simple">
			                <div align="center">
			                <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" id="CSRF_TOKEN"/>
							<input type="text" id="fakeImportFileID" readonly="true" class="ui-widget-content ui-corner-all"/>
							<span class="fileUploadSpan" style="height:auto;">
								<sj:a
									id="browseFileButton"
									cssClass="fakeUploadLink"
									button="true" 
									onClickTopics=""
								><s:text name="jsp.default.label.browse"/></sj:a>
								<s:file id="importFileInputID" name="file" accept="text/plain" onchange="ns.common.updateFilePath();"/>
							</span><span id="FileError"></span>
							<br/><span id="errorFileType"></span>
							<input type="hidden" name="overWrite" value="true">
							<input type="hidden" name="uploadingID" value="TellurideUpload"/>
							<input type="hidden" name="processImportTask" value="<ffi:getProperty name="ProcessImportTask"/>">
							<input type="hidden" name="fileType" value='<ffi:getProperty name="fileType"/>'/>
							<input type="hidden" name="transactionsOnly" value='<ffi:getProperty name="TransactionsOnly"/>'/>
							<input type="hidden" name="multipleBatches" value='<ffi:getProperty name="MultipleBatches"/>'/>
							<input type="hidden" name="strictACHValidation" value='<ffi:getProperty name="StrictACHValidation"/>'/>
							<input type="hidden" name="isFromFileUpload" value='true'/>
							</div>
						</s:form>
					</td>
				</tr>
				<tr>
					<td class="columndata lightBackground" colspan="2" align="center">
						<sj:a 
                                button="true"
                                onClickTopics="closeUploadFileWindowTopic,tabifyNotes" 
		                        ><s:text name="jsp.default_82"/></sj:a>
						<sj:a 
								id="uploadFileButton"
								formIds="uploaderID"
								targets="checkFileImportResultsDialogID"
								timeout="0"
                                button="true" 
								onclick="ns.common.updateRequestParams('uploaderID');"
                                onBeforeTopics="beforeUploadingFile"
								onErrorTopics="errorUploadingFile"
	                            onSuccessTopics="successUploadingFile"
								onCompleteTopics="completeUploadingFile,tabifyNotes"								
		                        ><s:text name="jsp.default_452"/></sj:a>
					</td>
				</tr>
			</table>
			<p></p>
			</div>
	<div id="uploadingStatus2" ></div>
	<iframe style="display: none;" name="progressFrame"></iframe>
	</fieldset>
</div>
