<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.HashSet" %>

<%
int bandCtr = 0;
String backgroundColour = "";
String darkRow = "";
String lightRow = "";
%>

<ffi:getProperty name="approval_view_permissions_table_dark_row" assignTo="darkRow"/>
<ffi:getProperty name="approval_view_permissions_table_light_row" assignTo="lightRow"/>

<ffi:list collection="${UsersCollectionName}" items="User">
    <ffi:setProperty name="DisplayedData" value="true"/>
    <ffi:setProperty name="DisplayedUserData" value="true"/>
    <ffi:setProperty name="UserHasData" value="false"/>
    <%
    bandCtr = (bandCtr + 1) % 2;
    backgroundColour = (bandCtr == 0) ? darkRow : lightRow;
    %>
    <ffi:setProperty name="UserFirstName" value=""/>
    <ffi:setProperty name="UserLastName" value=""/>
    <ffi:setProperty name="UserName" value=""/>
    <ffi:setProperty name="FoundEmployee" value="false"/>

    <ffi:object name="com.ffusion.tasks.user.GetBusinessEmployeesByEntGroupId"
	    id="GetBusinessEmployeesByEntGroupId"
	    scope="session"/>
    <ffi:setProperty name="GetBusinessEmployeesByEntGroupId" property="EntitlementGroupId" value="${User.EntitlementGroupId}"/>
    <ffi:setProperty name="GetBusinessEmployeesByEntGroupId" property="BusinessEmployeesSessionName" value="TempBusEmployees"/>
    <ffi:process name="GetBusinessEmployeesByEntGroupId"/>

    <ffi:list collection="TempBusEmployees" items="TempEmployee">
	<ffi:cinclude value1="${FoundEmployee}" value2="false" operator="equals">
	    <ffi:setProperty name="IDMatched" value="false"/>
	    <ffi:setProperty name="TypeMatched" value="false"/>
	    <ffi:setProperty name="SubTypeMatched" value="false"/>

	    <ffi:cinclude value1="${TempEmployee.EntitlementGroupMember.Id}" value2="${User.Id}" operator="equals">
		<ffi:setProperty name="IDMatched" value="true"/>
	    </ffi:cinclude>
	    <ffi:cinclude value1="${TempEmployee.EntitlementGroupMember.MemberType}" value2="${User.MemberType}" operator="equals">
		<ffi:setProperty name="TypeMatched" value="true"/>
	    </ffi:cinclude>
	    <ffi:cinclude value1="${TempEmployee.EntitlementGroupMember.MemberSubType}" value2="${User.MemberSubType}" operator="equals">
		<ffi:setProperty name="SubTypeMatched" value="true"/>
	    </ffi:cinclude>

	    <ffi:cinclude value1="${IDMatched}" value2="true" operator="equals">
		<ffi:cinclude value1="${TypeMatched}" value2="true" operator="equals">
		    <ffi:cinclude value1="${SubTypeMatched}" value2="true" operator="equals">
			<ffi:setProperty name="FoundEmployee" value="true"/>

			<ffi:setProperty name="UserFirstName" value="${TempEmployee.FirstName}"/>
			<ffi:setProperty name="UserLastName" value="${TempEmployee.LastName}"/>
			<ffi:setProperty name="UserName" value="${TempEmployee.UserName}"/>
		    </ffi:cinclude>
		</ffi:cinclude>
	    </ffi:cinclude>
	</ffi:cinclude>
    </ffi:list>
	    <tr>
		<td colspan="7" style="text-align: center; height: 20px;" class="tbrd_full ui-widget-content">
		    <span class="sectionhead"><ffi:getProperty name="UserFirstName"/> <ffi:getProperty name="UserLastName"/> (<ffi:getProperty name="UserName"/>)</span>
		</td>
	    </tr>
	    
	<%-- please not that we are using jsp:include instead of ffi:include because ffi:include was not rendering the page correctly --%>
	    <jsp:include page="include-approval-view-permissions-user-ach-cross-ach-company.jsp" />
	    <jsp:include page="include-approval-view-permissions-user-ach-per-ach-company.jsp" />

    <ffi:cinclude value1="${DisplayedHeader}" value2="true" operator="equals">
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_dark_row'/>">&nbsp;</td>
	    </tr>
	<ffi:setProperty name="UserHasData" value="true"/>
    </ffi:cinclude>

    <ffi:cinclude value1="${UserHasData}" value2="false" operator="equals">
	    <tr>
		<td colspan="7" class="<%= backgroundColour %>">&nbsp;</td>
	    </tr>
	    <tr>
		<td colspan="7" style="<ffi:getProperty name='ErrorMsgStyle'/>" class="<%= backgroundColour %>">
		    <span class="sectionsubhead">
		    <ffi:getProperty name="UserFirstName"/> <ffi:getProperty name="UserLastName"/> <ffi:getL10NString rsrcFile="common" msgKey="jsp/include-approval-view-permissions-user-ach.jsp-1" parm0="${OpType}"/>
		    </span>
		</td>
	    </tr>
	    <tr>
		<td colspan="7" class="<%= backgroundColour %>">&nbsp;</td>
	    </tr>
	    <tr>
		<td colspan="7" class="<ffi:getProperty name='approval_view_permissions_table_dark_row'/>">&nbsp;</td>
	    </tr>
    </ffi:cinclude>

</ffi:list>
