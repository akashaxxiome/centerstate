<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

		<ffi:setProperty name="fileImportErrorsURL" value="/pages/fileupload/GetFileImportErrorsAction.action?collectionName=ImportErrors" URLEncrypt="true"/>
    	<s:url id="getFileImportErrorsURL" value="%{#session.fileImportErrorsURL}" escapeAmp="false"/>
		
		<sjg:grid
		id="getImportErrorsGrid"
		caption=""
		sortable="true"
		dataType="json"
		href="%{getFileImportErrorsURL}"
		pager="true"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="true"
		navigator="true"
	    navigatorAdd="false"
	    sortname="date"
	    navigatorDelete="false"
	    navigatorEdit="false"
	    navigatorRefresh="false"
	    navigatorSearch="false"
	    navigatorView="false"
	    viewrecords="true"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		onGridCompleteTopics="addGridControlsEvents"
		>

		<sjg:gridColumn name="title" index="title" title="%{getText('jsp.default.label.title')}" sortable="false" width="65"/>
		<sjg:gridColumn name="lineNumber" index="lineNumber" title="%{getText('jsp.default.label.line')}" sortable="false" width="30"/>
		<sjg:gridColumn name="recordNumber" index="recordNumber" title="%{getText('jsp.default.label.record')}" sortable="false" width="30"/>
		<sjg:gridColumn name="message" index="message" title="%{getText('jsp.default.label.message')}" sortable="false" width="150"/>
		<sjg:gridColumn name="lineContent" index="lineContent" title="%{getText('jsp.default_136')}" sortable="false" width="150"/>
	</sjg:grid>
	<br/>	
		 
