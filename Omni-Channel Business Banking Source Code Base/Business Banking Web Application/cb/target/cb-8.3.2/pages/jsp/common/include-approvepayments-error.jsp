<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.common.Currency" %>
<%@ page import="java.util.Locale" %>

<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;
if( session.getAttribute( "APPROVALS_BC" ) != null ) {
	String isBCStr = ( String )session.getAttribute( "APPROVALS_BC" );
	if( isBCStr.equalsIgnoreCase( "true" ) ) {
		_isBC = true;
	}
}
%>
<style type="text/css">

.ui-jqgrid tr.jqgrow td{
	white-space: normal !important;
	height:auto;
	vertical-align:text-top;
}

</style>
<script src="<s:url value='/web'/>/js/common/include-approvepayments_grid.js" type="text/javascript"></script>

	<%
	com.ffusion.beans.approvals.ApprovalsItemErrors itemErrors = 
	    ( com.ffusion.beans.approvals.ApprovalsItemErrors ) 
	    session.getAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ITEM_ERRORS );
	    com.ffusion.beans.SecureUser secureUser = (com.ffusion.beans.SecureUser)session.getAttribute( com.ffusion.efs.tasks.SessionNames.SECURE_USER );
	com.ffusion.beans.approvals.ApprovalsItems items = new com.ffusion.beans.approvals.ApprovalsItems( secureUser.getLocale() );	    
	java.util.HashMap itemsToDecisions = (java.util.HashMap)session.getAttribute( "APPROVALS_ItemDecisionMap" );
	int size = itemErrors.size();
	boolean toggle = false;
	for( int i = 0; i < size; i++ ) {
	    com.ffusion.beans.approvals.ApprovalsItemError itemError = 
		(com.ffusion.beans.approvals.ApprovalsItemError)itemErrors.get(i);
	    com.ffusion.beans.approvals.ApprovalsItem item = itemError.getApprovalItem();
	    item.setLocale( secureUser.getLocale() );
	    com.ffusion.beans.approvals.ApprovalsDecision decision = (com.ffusion.beans.approvals.ApprovalsDecision)(itemsToDecisions.get( item ));
	    com.ffusion.beans.tw.TWTransaction transaction = 
		(com.ffusion.beans.tw.TWTransaction)item.getItem();
	    toggle = !toggle;
	    String _strAmount = ( ( com.ffusion.beans.FundsTransaction )transaction.getTransaction() ).getAmountValue().getCurrencyStringNoSymbol();
	    String _strType = transaction.getTypeAsString();	    
	    String _strDisplayType = transaction.getDisplayTypeAsString();	   
	    if( _strType.equals( com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_WIRE_TEMPLATE_STRING ) || 
		_strType.equals( com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_RECURRING_WIRE_TEMPLATE_STRING ) ) {
			_strAmount = ( ( com.ffusion.beans.wiretransfers.WireTransfer )transaction.getTransaction() ).getDisplayAmount();
			if( !_strAmount.equals( com.ffusion.beans.wiretransfers.WireDefines.NO_LIMIT ) ) {
	    		     _strAmount = ( ( com.ffusion.beans.FundsTransaction )transaction.getTransaction() ).getAmountValue().getCurrencyStringNoSymbol();
			}
	    }	    
	    items.add( item );
	    
		itemError.set("Date", String.valueOf( transaction.getSubmissionDateTime() ) );
		itemError.set("DisplayType", _strDisplayType);
		itemError.set("UserName", item.getSubmittingUserName() );
		itemError.set("Amount", _strAmount );
	
	 	String tempDecision = decision.getDecision();
	 	
	   	if( com.ffusion.approvals.constants.IApprovalsConsts.DECISION_APPROVED.equals( tempDecision ) ) { 
	   		itemError.set("Decision", "Approved");
	   	} else if( com.ffusion.approvals.constants.IApprovalsConsts.DECISION_REJECTED.equals( tempDecision ) ) {  
	   		itemError.set("Decision", "Rejected");
	   	} else if( com.ffusion.approvals.constants.IApprovalsConsts.DECISION_HOLD.equals( tempDecision ) ) {   
	   		itemError.set("Decision", "Hold");
	   	} else if( com.ffusion.approvals.constants.IApprovalsConsts.DECISION_RELEASE.equals( tempDecision ) ) { 
	   		itemError.set("Decision", "Release");
		} 
		    	
	    	if( !_isBC ) {  
		        session.setAttribute( "CURRENT_DECISION", decision );
		        session.setAttribute( "CURRENT_ERROR", itemError );
		    %>
				<%-- include the approvals constants page --%>
				<ffi:include page="${PagesPath}approvals/inc/approval_error_message.jsp"/>
		    <% } 
		    String errorPage = null; 
		    if( transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_BILL_PAYMENT || 
		       transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_RECURRING_BILL_PAYMENT ) {
			    errorPage = "approval_view_billpay_jsp";
		    }
		    if( transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_ACCOUNT_TRANSFER || 
		       transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_RECURRING_ACCOUNT_TRANSFER ) {
			    errorPage = "approval_view_transfer_jsp";
		    }
		    if( transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_WIRE_TRANSFER || 
		       transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_RECURRING_WIRE_TRANSFER ) {
			    errorPage = "approval_view_wire_jsp";
		    }
		    if( transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_ACH_BATCH || 
		       transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_RECURRING_ACH_BATCH ||
				transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_TAX_PAYMENT ||
				transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_REC_TAX_PAYMENT ||
				transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_CHILD_SUPPORT_PAYMENT ||
				transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_REC_CHILD_SUPPORT_PAYMENT
			) {
			    errorPage = "approval_view_ach_jsp";
		    }
		    if( transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_WIRE_BATCH ) {
			    errorPage = "approval_view_wirebatch_jsp";
		    }
		    if( transaction.getTransactionType() == com.ffusion.tw.interfaces.TWTransactionTypes.TYPE_CASHCON ) {
			    errorPage = "approval_view_cashcon_jsp";
		    }
		    StringBuffer urlBuffer = new StringBuffer( "http://" );
		    urlBuffer.append(request.getHeader("Host")).append(session.getAttribute("FullPagesPath"))
		    	.append( session.getAttribute( errorPage ) ).append( "?idx=" ).append( i );
		    itemError.set("ErrorPage", urlBuffer.toString() );    
	}
	items.setLocale( secureUser.getLocale() );
	session.setAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ITEMS, items );
	%>
	<div>
	<script type="text/javascript">
	$.subscribe("approvePaymentsErrorGridComplete", function(event,data) {
		ns.common.resizeWidthOfGrids();
	});
	</script>
	
	
<ffi:setGridURL grid="GRID_approvePaymentsError" name="ViewURL" url="${FullPagesPath}approvals/approvepayments-viewcommon.jsp?CollectionName=ApprovalsItems&Type={0}&ItemID={1}&SubmittingUserName={2}" parm0="ApprovalItem.Item.TypeAsString" parm1="ApprovalItem.itemID" parm2="ApprovalItem.submittingUserName"/>

<ffi:setProperty name="tempURL" value="/pages/jsp/approvals/getApprovePaymentsErrors.action?GridURLs=GRID_approvePaymentsError" URLEncrypt="true"/>
<s:url id="getApprovePaymentsErrorsUrl" value="%{#session.tempURL}" escapeAmp="false"/>

<sjg:grid
	id="approvePaymentsErrorsGrid" caption="" dataType="json"
	sortable="true"  
	href="%{getApprovePaymentsErrorsUrl}" pager="false" gridModel="gridModel"
	rowList="%{#session.StdGridRowList}" 
	rowNum="%{#session.StdGridRowNum}" 
	rownumbers="false" shrinkToFit="true"
	scroll="false" scrollrows="true" onGridCompleteTopics="approvePaymentsErrorGridComplete">
	<sjg:gridColumn name="map.Date" index="mapDate" title="%{getText('jsp.default_137')}" sortable="false" width="80" />
	<sjg:gridColumn name="map.DisplayType" index="mapDisplayType" title="%{getText('jsp.default_444')}" sortable="false" width="120" />
	<sjg:gridColumn name="map.UserName" index="mapUserName" title="%{getText('jsp.default_398')}" sortable="false" width="100" />
	<sjg:gridColumn name="map.Amount" index="mapAmount" title="%{getText('jsp.default_43')}" sortable="false" width="90" />
	<sjg:gridColumn name="map.Decision" index="mapDecison" title="%{getText('jsp.default_160')}" sortable="false" width="80" />
	<sjg:gridColumn name="errorMessage" index="errorMessage" title="%{getText('jsp.default_189')}" sortable="false" width="260" />
	<sjg:gridColumn name="map.ErrorPage" index="mapErrorPage" title="%{getText('jsp.default_27')}" sortable="false" width="60" formatter="ns.approval.formatApprovePaymentActionLinks" />
	<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default_464')}" sortable="true" width="60" hidden="true" hidedlg="true"/>

    <sjg:gridColumn name="approvalItem.submittingUserName" index="SUBMITTINGUSERNAME" title="%{getText('jsp.default_398')}" hidden="true" hidedlg="true" sortable="false" />
    <sjg:gridColumn name="approvalItem.item.typeAsString" index="TypeString" title="%{getText('jsp.common_9')}" hidden="true" hidedlg="true" sortable="false"/>
    <sjg:gridColumn name="approvalItem.itemID" index="itemID" title="%{getText('jsp.common_95')}" hidden="true" hidedlg="true" sortable="false" formatter="ns.approval.formatItemID" />

</sjg:grid>
	<div align="center"><br/><s:url id="ajax" value="/pages/jsp/approvals/approvepayments.jsp">	
	<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
	</s:url><sj:a 
			href="%{ajax}" 
			button="true"
			indicator="indicator" 
			targets="approvalGroupSummary"
			onBeforeTopics="ApprovePamentsErrorOKClickBefore"
			><s:text name="jsp.common_4"/></sj:a>
	</div>
	<br/><br/>
	<sj:dialog id="viewApprovePaymentDialogID"
		cssClass="includeApprovalDialog" 
		title="%{getText('jsp.common_167')}" 
		modal="true" 
		resizable="false"
		autoOpen="false" 
		showEffect="fold" 
		hideEffect="clip" 
		width="800"
   		buttons="{ 
    		'DONE':function() { ns.common.closeDialog(); } 
   		}"	>
	</sj:dialog>
	<script>
		$('#viewApprovePaymentDialogID').addHelp(function(){
			var helpFile = $('#viewApprovePaymentDialogID').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		});
	</script>
	
	</div>