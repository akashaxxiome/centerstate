<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- include the transaction details constants --%>
<s:include value="/pages/jsp/common/include-view-transaction-details-constants.jsp"/>

<%-- set the values for BC/CB --%>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
    <%-- core approval pages --%>
    <ffi:setProperty name="approval_verify_jsp" value="approvals/approvepayments-verify.jsp"/>
    <ffi:setProperty name="approval_payment_jsp" value="approvals/approvepayments.jsp"/>
    <ffi:setProperty name="approval_error_jsp" value="approvals/approvepayments-error.jsp"/>
    <ffi:setProperty name="approval_confirm_jsp" value="approvals/approvepayments-confirm.jsp"/>

    <%-- approval admin pages --%>
    <ffi:setProperty name="approval_display_workflow_jsp" value="approvals/corpadmincoapwf.jsp" />
    <ffi:setProperty name="approval_display_add_edit_workflow_jsp" value="approvals/corpadmincoapwfaddedit.jsp" />
    <ffi:setProperty name="approval_display_delete_workflow_jsp" value="approvals/corpadmincoapwfdeletelev.jsp" />
    <ffi:setProperty name="approval_display_stall_condition_jsp" value="approvals/corpadmincoapwfstallcondition.jsp" />
    <ffi:setProperty name="approval_add_workflow_jsp" value="approvals/corpadmincoapwfaddlev.jsp" />
    <ffi:setProperty name="approval_edit_workflow_jsp" value="approvals/corpadmincoapwfeditlev.jsp" />
    <ffi:setProperty name="approval_view_permissions_jsp" value="approvals/corpadmincoapwfviewpermissions.jsp"/>

    <%-- view transaction pages --%>
    <ffi:setProperty name="approval_view_transfer_jsp" value="approvals/approvepayments-viewtransfer.jsp"/>
    <ffi:setProperty name="approval_view_wire_jsp" value="approvals/approvepayments-viewwire.jsp"/>
    <ffi:setProperty name="approval_view_wirebatch_jsp" value="approvals/approvepayments-viewwirebatch.jsp"/>
    <ffi:setProperty name="approval_view_billpay_jsp" value="approvals/approvepayments-viewbillpay.jsp"/>
    <ffi:setProperty name="approval_view_ach_jsp" value="approvals/approvepayments-viewach.jsp"/>
    <ffi:setProperty name="approval_view_cashcon_jsp" value="approvals/approvepayments-viewcashcon.jsp"/>
    <ffi:setProperty name="approval_view_positive_pay_jsp" value="approvals/approvepayments-viewppay.jsp"/>
	<ffi:setProperty name="approval_view_reverse_positive_pay_jsp" value="approvals/approvepayments-viewrppay.jsp"/>

    <%-- constants for gifs used on pages --%>
    <ffi:setProperty name="approval_payments_table_light_row" value="ltrow4"/>
    <ffi:setProperty name="approval_payments_table_dark_row" value="ltrow5"/>
    <ffi:setProperty name="approval_view_permissions_table_light_row" value="ltrow3"/>
    <ffi:setProperty name="approval_view_permissions_table_dark_row" value="ltrow5"/>
    <ffi:setProperty name="approval_view_permissions_table_header_style"
	    value="background: url('/cb/web/multilang/grafx/approvals/sechdr_blank.gif'); width: 750px; height: 14px; text-align: left; vertical-align: middle"/>
    <ffi:setProperty name="approval_view_permissions_table_footer_style"
	    value="background: url('/cb/web/multilang/grafx/approvals/sechdr_btm.gif'); width: 750px; height: 11px; line-height: 11px;"/>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
    <%-- core approval pages --%>
    <ffi:setProperty name="approval_verify_jsp" value="business/business_exceedlimits_verify.jsp"/>
    <ffi:setProperty name="approval_payment_jsp" value="business/business_exceedlimits.jsp"/>
    <ffi:setProperty name="approval_error_jsp" value="business/business_exceedlimits_error.jsp"/>
    <ffi:setProperty name="approval_confirm_jsp" value="business/business_exceedlimits_done.jsp"/>
    <ffi:setProperty name="approval_business_home_jsp" value="home.jsp"/>

    <%-- approval admin pages --%>
    <ffi:setProperty name="approval_display_workflow_jsp" value="business/business_approval_workflows.jsp" />
    <ffi:setProperty name="approval_display_add_edit_workflow_jsp" value="business/business_approval_add_edit_level.jsp" />
    <ffi:setProperty name="approval_display_delete_workflow_jsp" value="business/business_approval_delete_level.jsp" />
    <ffi:setProperty name="approval_display_stall_condition_jsp" value="business/business_approval_stall_condition.jsp" />
    <ffi:setProperty name="approval_add_workflow_jsp" value="business/business_approval_add_level_process.jsp" />
    <ffi:setProperty name="approval_edit_workflow_jsp" value="business/business_approval_edit_level_process.jsp" />

    <%-- view transaction pages --%>
    <ffi:setProperty name="approval_view_transfer_jsp" value="business/business_exceedlimits_viewtransfer.jsp"/>
    <ffi:setProperty name="approval_view_wire_jsp" value="business/business_exceedlimits_viewwire.jsp"/>
    <ffi:setProperty name="approval_view_wirebatch_jsp" value="business/business_exceedlimits_viewwirebatch.jsp"/>
    <ffi:setProperty name="approval_view_billpay_jsp" value="business/business_exceedlimits_viewbillpay.jsp"/>
    <ffi:setProperty name="approval_view_ach_jsp" value="business/business_exceedlimits_viewach.jsp"/>
    <ffi:setProperty name="approval_view_cashcon_jsp" value="business/business_exceedlimits_viewcashcon.jsp"/>

    <%-- constants for gifs used on pages --%>
    <ffi:setProperty name="approval_payments_table_light_row" value="ltrow"/>
    <ffi:setProperty name="approval_payments_table_dark_row" value="dkrow"/>
    
    <%-- constants for BC consumer approvals --%>
    <ffi:cinclude value1="${APPROVALS_BC_CONSUMER}" value2="true" operator="equals">
	<ffi:setProperty name="approval_verify_jsp" value="customer/customer_approval_transactions_verify.jsp"/>
	<ffi:setProperty name="approval_payment_jsp" value="customer/customer_approval_transactions_view.jsp"/>
	<ffi:setProperty name="approval_error_jsp" value="customer/customer_approval_transactions_error.jsp"/>
	<ffi:setProperty name="approval_confirm_jsp" value="customer/customer_approval_transactions_done.jsp"/>
	<ffi:setProperty name="approval_business_home_jsp" value="home.jsp"/>
    
	<%-- view transaction pages --%>
	<ffi:setProperty name="approval_view_transfer_jsp" value="customer/customer_approval_transactions_viewtransfer.jsp"/>
	<ffi:setProperty name="approval_view_billpay_jsp" value="customer/customer_approval_transactions_viewbillpay.jsp"/>
    </ffi:cinclude> 
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC_SRV_PKG}" value2="true" operator="equals">
    <%-- approval admin pages --%>
    <ffi:setProperty name="approval_display_workflow_jsp" value="admin/service_package_approval_workflows.jsp" />
    <ffi:setProperty name="approval_display_add_edit_workflow_jsp" value="admin/service_package_approval_add_edit_level.jsp" />
    <ffi:setProperty name="approval_display_delete_workflow_jsp" value="admin/service_package_approval_delete_level.jsp" />
    <ffi:setProperty name="approval_display_stall_condition_jsp" value="admin/service_package_approval_stall_condition.jsp" />
    <ffi:setProperty name="approval_add_workflow_jsp" value="admin/service_package_approval_add_level_process.jsp" />
    <ffi:setProperty name="approval_edit_workflow_jsp" value="admin/service_package_approval_edit_level_process.jsp" />
    <%-- constants for gifs used on pages --%>
    <ffi:setProperty name="approval_payments_table_light_row" value="ltrow"/>
    <ffi:setProperty name="approval_payments_table_dark_row" value="dkrow"/>
</ffi:cinclude>
