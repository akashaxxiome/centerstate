<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript">
<!--

/*
	amountField - contains the amount as a string
	FromAcccountID - contains the accountID of the account we care about. We use this to match up the account in the list of accounts to find the currency code of the account. If this is null, and useBaseCurrency is true then this paramter contains the specified currency code
	useBaseCurrenccy - false means that we are using the accounts collection to find the currency code, true means we are using the user's base currency OR the passed in currencycode under FromAccountID.

*/
function checkAmount(amountField, FromAccountID, useBaseCurrency) {

var currencyCode;
var amountField = "" + amountField;
var currencyRe;
//alert(amountField);
//alert(FromAccountID);

if (useBaseCurrency) {
	if (FromAccountID == null) {
	<%
		com.ffusion.beans.SecureUser sUser = (com.ffusion.beans.SecureUser)session.getAttribute(com.ffusion.efs.tasks.SessionNames.SECURE_USER);
	%>

		currencyCode = '<%=sUser.getBaseCurrency()%>';
	}
	else {
		currencyCode = FromAccountID;
	}

} else {
// Find the currency code for the selected From Account ID
<ffi:list collection="${checkAmount_acctCollectionName}" items="Accounts">
	if (FromAccountID == '<ffi:getProperty name="Accounts" property="ID"/>') {
		currencyCode = '<ffi:getProperty name="Accounts" property="CurrencyCode"/>';
	}
	//alert('<ffi:getProperty name="Accounts" property="CurrencyCode"/>');
</ffi:list>
}

// Check the amount field to make sure the currency symbol matches the currency code

if (amountField.length == 0) {
	alert(js_amount_enter_alert);
	return false;
}
//alert(currencyCode);
if (currencyCode == 'USD' || currencyCode == 'CDN' ) {
	currencyRe = /^(|\+|-)(|\$)(|[1-9]((\d)*|(|\d|\d\d)(,\d\d\d)*)|0)(|\.\d(|\d))$/;

	if ( !currencyRe.test ( amountField ) ) {
		alert ( amountField + " <s:text name="jsp.default_253"/>" );
		return false;
	} else {
		return true;
	}
} else if (currencyCode == 'JPY') {
	currencyRe = /^(|\+|-)(|¥)([1-9]((\d)*|(|\d|\d\d)(,\d\d\d)*)|0)$/;

	if ( !currencyRe.test ( amountField ) ) {
		alert ( amountField + " <s:text name="jsp.common_94"/>" );
		return false;
	} else {
		return true;
	}
} else {
	return true;
}

return true;

}

function matchValidCharacters(amountField, validChars) {

	var chr;
	var valid;
	for (var i=0; i < amountField.length; i++) {
	// loop through the amount string and check each character against the valid characters
		chr = amountField.charAt(i);

		valid = false;
		for (var j=0; j < validChars.length; j++) {
		// go through the characters and set a flag that the character is valid
			if (chr == validChars[j]) {
				valid = true;
				break;
			}
		}
		if (!valid) {
		// if none of the characters are valid then alert the user and don't submit
			alert("<s:text name="jsp.common_155"/>: '"+chr+"'.");
			return false;
		}
	}
	return true;
}

function checkACHAmount (item) {

	var amt = item.value;

	//If amount has been left blank, set it to 0.00

	if (amt.length == 0) {

		amt = "0.00";
		return true;


	}

	//Check for Yen
	if (amt.indexOf('¥') != -1) {

			alert("<s:text name="jsp.common_155"/>: '¥'.");
			item.focus();
			return false;
	}

	//Check for negative amount
	var negIdx = amt.indexOf('-');
	if (negIdx != -1) {

		if (negIdx == 0) {

			alert("<s:text name="jsp.common_100"/>");
		} else {
			alert("<s:text name="jsp.common_155"/>: '-'.");

		}

		item.focus();
		return false;
	}

	//Invoke common validations
	if (!checkAmount(amt, null, true)) {

		item.focus();
		return false;

	}

	//Before checking for 8 digit integer and 10 digit total constraints, we need to remove
	//commas

	var tempAmt = amt.replace(/[^0-9|\.]/g,"");

	var decIdx = tempAmt.indexOf('.');
	var intPartLen = 0;
	var decPartLen = 0;

	if (decIdx != -1) {
		intPartLen = tempAmt.substring(0, decIdx).length;
		decPartLen = tempAmt.substring(decIdx+1, tempAmt.length).length;
	} else {
		intPartLen = tempAmt.length;
	}

	//Check 8 digit integer constraint
	if (intPartLen > 8) {
		alert("<s:text name="jsp.common_19"/>");
		item.focus();
		return false;
	}

	//Check total 10 digit constraint
	if ((intPartLen + decPartLen) > 10) {
		alert("<s:text name="jsp.common_18"/>");
		item.focus();
		return false;

	}

	// WARNING.... Custom Code for US follows, Decimal = ".", grouping separator = ","
	var amtStr = "" + item.value.replace(/[^0-9|\.]/g,"");
    var amt = parseFloat(amtStr);

    if (isNaN(amt)) {
		alert("<s:text name="jsp.common_124"/>");
		item.focus();
		return false;
	}

	amtStr = "" + amt;
	if (amtStr.indexOf('.') == -1) { amtStr = amtStr+".0" } // Add .0 if a decimal point doesn't exist
	temp=(amtStr.length-amtStr.indexOf('.')); // Find out if the # ends in a tenth (ie 145.5)
	if (temp <= 2) { amtStr=amtStr+"0"; }; // if it ends in a tenth, add an extra zero to the string
	// add in commas again
	if (amtStr.length <= 9)		// don't do it if amount > 7 digits before decimal
	{
		var x = amtStr.indexOf('.') - 3;
		while (x > 0)
		{
			amtStr = amtStr.substr(0,x) + ',' + amtStr.substr(x, amtStr.length);
			x = amtStr.indexOf(',')-3;
		}
	}
	item.value = amtStr;

	return true;

}

// -->
</script>
