<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:include page="${PagesPath}common/include-approval-view-details-init.jsp"/>

<%-- set the header depnding on the type of cashcon --%>
<ffi:cinclude value1="${AddEditCashCon.Type}" value2="<%= String.valueOf( com.ffusion.beans.FundsTransaction.FUNDS_TYPE_CASH_CONCENTRATION ) %>" operator="equals" >
	<ffi:setProperty name='CashConHeading' value='View Cash Concentration - Deposit Entry Details'/>
</ffi:cinclude>
<ffi:cinclude value1="${AddEditCashCon.Type}" value2="<%= String.valueOf( com.ffusion.beans.FundsTransaction.FUNDS_TYPE_CASH_CONCENTRATION ) %>" operator="notEquals" >
	<ffi:setProperty name='CashConHeading' value='View Cash Concentration - Disbursement Request Details'/>
</ffi:cinclude>

<div class="nameSubTitle"><ffi:getProperty name="CashConHeading"/></div>
	<ffi:removeProperty name="CashConHeading"/>
<ffi:include page="${PagesPath}common/include-approval-view-details-header.jsp"/>
  <ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
	</ffi:cinclude>
	<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
	</ffi:cinclude>
<ffi:include page="${PagesPath}common/include-view-cashcon-details.jsp"/>
