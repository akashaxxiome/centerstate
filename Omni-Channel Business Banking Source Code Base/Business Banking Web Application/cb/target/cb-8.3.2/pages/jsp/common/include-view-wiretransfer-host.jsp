<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:setProperty name="WireTransfer" property="DateFormat" value="${UserLocale.DateFormat}"/>
<%-- ---- CONFIRMATION NUMBERS ---- --%>
<div class="confirmationDetailsWrapper">
<div class="confirmationDetails" style="width:50%; float:left; text-align:left">
	<span><s:text name="jsp.default_435"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="TrackingID"/></span>
</div>
<div class="confirmationDetails" style="width:49%; float:left; text-align:left">
	<span><s:text name="jsp.default_46"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="ReferenceNumber"/></span>
</div>
<div class="confirmationDetails" style="width:50%; float:left; text-align:left">
	<span><s:text name="jsp.default_229"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="ConfirmationNum"/></span>
</div>
<div class="confirmationDetails" style="width:49%; float:left; text-align:left">
	<span><s:text name="jsp.common_79"/>: </span>
	<span><ffi:getProperty name="WireTransfer" property="FedConfirmationNum"/></span>
</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><s:text name="jsp.common_83"/></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_230"/></span>
		    	<span class="columndata"><ffi:getProperty name="WireTransfer" property="HostID"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_43"/></span>
		    	<span class="columndata"><ffi:getProperty name="WireTransfer" property="Amount"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_457"/></span>
		    <span class="columndata"><ffi:getProperty name="WireTransfer" property="DateToPost"/></span>
		</div>
	</div>
</div>
<div class="marginTop20"></div>
<s:include value="%{#session.PagesPath}common/include-view-wire-history.jsp"/>
<form method="post">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<% if( session.getAttribute( "wire_details_post_jsp" ) == null ) { %>
	<ffi:setProperty name="wireViewDoneURL" value="payments/wiretransfers.jsp"/>
	<ffi:cinclude value1="${collectionName}" value2="WiresRelease" operator="equals">
		<ffi:setProperty name="wireViewDoneURL" value="payments/wiresrelease.jsp?DontInitialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${collectionName}" value2="WireReportRecords" operator="equals">
		<ffi:setProperty name="wireViewDoneURL" value="payments/wiretransferreport.jsp?haveReport=false&save=true&mod=false&DontInitializeReport=true" URLEncrypt="true"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${collectionName}" value2="WireBatch.Wires" operator="equals">
		<ffi:setProperty name="wireViewDoneURL" value="payments/wirebatchview.jsp?DontInitialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${collectionName}" value2="WireTransfersByBatchId" operator="equals">
		<ffi:setProperty name="wireViewDoneURL" value="payments/wirebatchview.jsp?DontInitialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
	<%
	    StringBuffer tempURLBuf = new StringBuffer( ( String )session.getAttribute( "SecurePath" ) );
	    tempURLBuf.append( ( String )session.getAttribute( "wireViewDoneURL" ) );
	    session.setAttribute( "wireViewDoneURL", tempURLBuf.toString() );
	%>
    <% } else { %>
	<ffi:setProperty name="wireViewDoneURL" value="${wire_details_post_jsp}" />
    <% } %>
    <% if( session.getAttribute( "WireBatch" ) != null && 
		session.getAttribute( "ApprovalsViewTransactionDetails") != null ) { %>
	    <ffi:setProperty name="wireViewDoneURL" value="${SecurePath}${approval_view_wirebatch_jsp}"/>
    <% } %>
    <%-- 
    <input type="Button" value="DONE" class="submitbutton" onClick="javascript:document.location='<ffi:getProperty name="wireViewDoneURL"/>'">
    --%>
    <div align="center">
		 <%-- <sj:a button="true" title="DONE" onClickTopics="closeDialog">DONE</sj:a> --%>
	</div>
						
    <ffi:removeProperty name="wireViewDoneURL"/>
    <ffi:removeProperty name="collectionName"/>
</form>