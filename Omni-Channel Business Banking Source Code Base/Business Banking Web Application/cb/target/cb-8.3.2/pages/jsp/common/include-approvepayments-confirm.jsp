<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.approvals.*" %>
<%@ page import="com.ffusion.beans.tw.*" %>
<%@ page import="com.ffusion.beans.business.*" %>
<%@ page import="java.util.Locale" %>
<%@ page import="com.ffusion.approvals.constants.IApprovalsConsts" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%
try{
Integer startIndex = new Integer( (String) session.getAttribute("startIndex") );
int indexOffset = startIndex.intValue() - 1;

String approvalDecisions[] = ( String[] ) session.getAttribute( "Approvals_approvalDecisions" );
String isSelected[] = ( String[] ) session.getAttribute( "Approvals_isSelected" );
String itemIndex[] = ( String[] ) session.getAttribute( "Approvals_itemIndex" );
String reason[] = ( String[] ) session.getAttribute( "Approvals_reason" );

// the previous page validated that a decision was made
ApprovalsDecisions decisions = new ApprovalsDecisions( Locale.getDefault() );
ApprovalsItems items = (ApprovalsItems)session.getAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ITEMS );

// values required for the BC pages
boolean notifyPrimaryContact[] = null;
PendingTransactions pendingTransactions = null;
PendingTransactions approvalPendingTransactions = null;
%>

<%
StringBuffer confirmation = new StringBuffer();
HashMap itemToDecision = new HashMap();
// create the approvals decisions from the items that have actions set to non-select
for( int i = 0 ; i < approvalDecisions.length ; i++ ) {
	if( isSelected[ i ].equalsIgnoreCase( "true" ) ) {
		ApprovalsItem item = (ApprovalsItem)items.get( Integer.valueOf( itemIndex[i] ).intValue() + indexOffset );
		%>

		<%
		// create a decision object
		ApprovalsDecision decision = decisions.add();
		if( approvalDecisions[i].equals( IApprovalsConsts.DECISION_APPROVED ) ) {
			decision.setDecision( IApprovalsConsts.DECISION_APPROVED );
			item.setApprovalItemProperty( IApprovalsConsts.ITEM_PROPERTIES_APPROVAL_REASON, reason[i] );
		} else if( approvalDecisions[i].equals( IApprovalsConsts.DECISION_REJECTED ) ) {
			decision.setDecision( IApprovalsConsts.DECISION_REJECTED );
			item.setApprovalItemProperty( IApprovalsConsts.ITEM_PROPERTIES_REJECT_REASON, reason[i] );
		} else if( approvalDecisions[i].equals( IApprovalsConsts.DECISION_RELEASE ) ) {
			decision.setDecision( IApprovalsConsts.DECISION_RELEASE );
		} else if( approvalDecisions[i].equals( IApprovalsConsts.DECISION_HOLD ) ) {
			decision.setDecision( IApprovalsConsts.DECISION_HOLD );
		} else {
			decision.setDecision( approvalDecisions[ i ] );
		}
		decision.setApprovalItem( item );
		itemToDecision.put( item, decision );
	}
}

// put the data into the session
session.setAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_DECISIONS, decisions );
session.setAttribute( "APPROVALS_ItemDecisionMap", itemToDecision );
%>


	<ffi:object id="submitDecisions" name="com.ffusion.tasks.approvals.SubmitDecisions" scope="page"/>
	<ffi:process name="submitDecisions"/>

	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%= IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:object name="com.ffusion.beans.user.BusinessEmployee" id="SearchBusinessEmployee" scope="session" />
		<ffi:setProperty name="SearchBusinessEmployee" property="BusinessId" value="${Business.Id}"/>
		<ffi:setProperty name="SearchBusinessEmployee" property="<%= com.ffusion.efs.adapters.profile.constants.ProfileDefines.BANK_ID %>" value="${Business.BankId}"/>

		<ffi:object name="com.ffusion.tasks.user.GetBusinessEmployees" id="GetBusinessEmployees" scope="session" />
		<ffi:setProperty name="GetBusinessEmployees" property="SearchBusinessEmployeeSessionName" value="SearchBusinessEmployee"/>
		<ffi:setProperty name="GetBusinessEmployees" property="SearchEntitlementProfiles" value="TRUE"/>
		<ffi:process name="GetBusinessEmployees"/>
		<ffi:setProperty name="GetBusinessEmployees" property="BusinessEmployeesSessionName" value="entitlementProfiles"/>
		<ffi:process name="GetBusinessEmployees"/>



		<ffi:removeProperty name="GetBusinessEmployees"/>
		<ffi:removeProperty name="SearchBusinessEmployee"/>

	</ffi:cinclude>



<meta http-equiv="content-type" content="text/html;charset=UTF-8">
<%
// we assume that the task resource has been called and is available in the
// session to retrieve the error messages beased on the error code
com.ffusion.beans.approvals.ApprovalsItemErrors itemErrors =
    ( com.ffusion.beans.approvals.ApprovalsItemErrors )
    session.getAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_ITEM_ERRORS );
if( itemErrors != null && itemErrors.size() > 0 ) {

%>
<ffi:include page="${PagesPath}${approval_error_jsp}"/>
<%
} else {

	// cleanup the session
	session.removeAttribute( "Approvals_approvalDecisions" );
	session.removeAttribute( "Approvals_isSelected" );
	session.removeAttribute( "Approvals_itemIndex" );
	session.removeAttribute( "Approvals_reason" );
	session.removeAttribute( "Approvals_notifyPrimaryContact" );
	session.removeAttribute( "Approvals_itemIDSet" );
%>



<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
	<ffi:include page="${PagesPath}${approval_payment_jsp}"></ffi:include>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
alert("bc = true");
   	 <%--<META HTTP-EQUIV="Refresh" CONTENT="0; URL='<ffi:getProperty name="SecurePath"/><ffi:getProperty name="approval_business_home_jsp"/>'"> --%>
</ffi:cinclude>
<%
}
%>

<%
}catch(Exception e){
%>
	<ffi:object id="ThrowException" name="com.ffusion.tasks.DummyTask" scope="request" />
	<ffi:setProperty name="ThrowException" property="Error" value="<%=String.valueOf(com.ffusion.csil.CSILException.SERVICE_ERROR)%>"/>
	<ffi:setProperty name="ThrowException" property="ErrorType" value="<%=com.ffusion.tasks.DummyTask.TASK_ERROR%>"/>
	<ffi:process name="ThrowException"/>
<%
}
 %>