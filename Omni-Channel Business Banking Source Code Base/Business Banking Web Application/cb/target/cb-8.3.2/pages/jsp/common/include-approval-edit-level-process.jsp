<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.Locale" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="com.ffusion.beans.approvals.*" %>
<%@ page import="com.ffusion.beans.common.Currency" %>
<%@ page import="com.ffusion.beans.SecureUser" %>
<%@ page import="com.ffusion.approvals.constants.IApprovalsConsts" %>
<%@ page import="com.ffusion.tasks.approvals.IApprovalsTask" %>

<%
// constant that determines if BC/CB is using the page
//----------------------------------------------------
boolean _isBC = false;
if( session.getAttribute( "APPROVALS_BC" ) != null ) {
	String isBCStr = ( String )session.getAttribute( "APPROVALS_BC" );
	if( isBCStr.equalsIgnoreCase( "true" ) ) {
		_isBC = true;
	}
}

// get the parameters reflecting the approval transaction value range
String minAmount = request.getParameter( "approvalsLevelsMinAmount" );
String maxAmount = request.getParameter( "approvalsLevelsMaxAmount" );
String opType = request.getParameter( "approvalsLevelsOperationType" );
String levelID = request.getParameter( "approvalsLevelsID" );

// get the levels object currently in the session (to obtain the locale)
ApprovalsLevels levels = ( ApprovalsLevels )session.getAttribute( "ApprovalsLevels" );
Locale locale = levels.getLocale();
ApprovalsLevels newLevels = new ApprovalsLevels( locale );

int busID = -1;
if( _isBC ) {
    if( session.getAttribute( "srvPkgId" ) == null ) {
        busID = ( (com.ffusion.beans.business.Business)session.getAttribute( "ModifyBusiness" ) ).getIdValue();
    } else {
        busID = Integer.parseInt((String)session.getAttribute( "srvPkgId" ));
    }
} else {
    busID = ( (SecureUser)session.getAttribute( "SecureUser" ) ).getBusinessID();
}

// need to wrap the single ApprovalsLevel in an ApprovalsLevel object, as the UpdateWorkflowLevels task expects
// the collection rather than the single value.

int approvalsLevelID = -1; 

try{
	approvalsLevelID = Integer.valueOf( levelID ).intValue();
}catch(Exception e){
	//ignore the exception because incorrect level id will be validated in the tasks. 
}

ApprovalsLevel newLevel = new ApprovalsLevel( locale );

Currency minAmountCurr = null;

if( !( minAmount.equals("") ) ) {
	minAmountCurr = new Currency( minAmount, locale );
	newLevel.setMinAmount( minAmountCurr );
} else {
	minAmountCurr = new Currency( String.valueOf(com.ffusion.approvals.constants.IApprovalsConsts.MIN_AMOUNT), locale );
	newLevel.setMinAmount( minAmountCurr );
}

Currency maxAmountCurr = null;

if( !( maxAmount.equals("") ) ) {
	maxAmountCurr = new Currency( maxAmount, locale );
	newLevel.setMaxAmount( maxAmountCurr );
} else {
	maxAmountCurr = new Currency( String.valueOf( com.ffusion.approvals.constants.IApprovalsConsts.MAX_AMOUNT), locale );
	newLevel.setMaxAmount( maxAmountCurr );
}

if( _isBC ) {
    newLevel.setLevelType( IApprovalsConsts.LEVEL_TYPE_BANK_DEFINED );
    if( session.getAttribute( "srvPkgId" ) == null ) {
        newLevel.setObjectType( com.ffusion.approvals.constants.IApprovalsConsts.APPROVAL_OBJECT_TYPE_SERVICE_PACKAGE );
    } else {
	newLevel.setObjectType( com.ffusion.approvals.constants.IApprovalsConsts.APPROVAL_OBJECT_TYPE_BUSINESS );
    }
} else {
    newLevel.setLevelType( IApprovalsConsts.LEVEL_TYPE_BUSINESS_DEFINED );
    newLevel.setObjectType( com.ffusion.approvals.constants.IApprovalsConsts.APPROVAL_OBJECT_TYPE_BUSINESS );
}
newLevel.setLevelID( approvalsLevelID );
newLevel.setBusinessID( busID );
newLevels.add( newLevel );

session.removeAttribute( "ApprovalsLevels" );
session.setAttribute( "ApprovalsLevels", newLevels );

// the approval level was already placed into the session
// so we will now put the chainItems into the session

// the value of the chainItems is: user or group string - usertype - groupOrUser ID
String[] chainItemsData	= request.getParameterValues( "chainItemList" );
ApprovalsChainItems chainItems = new ApprovalsChainItems( Locale.getDefault() );
String[] chainItemsNames    = new String[ chainItemsData.length ];
HashMap groupNames = new HashMap();
HashMap approvalGroupNames = new HashMap();
boolean isApprovalsGroup = false;

// create the chain items from the posted data
// the previous page validated that at least one chain item is present
for( int i = 0 ; i < chainItemsData.length ; i++ ) {
	// create a decision object
	ApprovalsChainItem chainItem = chainItems.add();
	String itemData = chainItemsData[ i ];

	int firstIndex = itemData.indexOf( '-' );
	int middleIndex = itemData.indexOf( '-',  firstIndex + 1 );
	//lastIndex now used to get User/Group Names for Stall Condition error page
	int lastIndex = itemData.indexOf( '-', middleIndex + 1 );

	boolean isUsingUser = itemData.substring( 0, firstIndex ).equalsIgnoreCase( "user" );
	if ( !_isBC ) {
		isApprovalsGroup = itemData.substring( 0, firstIndex ).equalsIgnoreCase( "apprgroup" );
	}
	int userType = Integer.parseInt( itemData.substring( firstIndex + 1, middleIndex ) );
	int groupOrUserID = Integer.parseInt( itemData.substring( middleIndex + 1, lastIndex ) );
	chainItemsNames[ i ]  = itemData.substring ( lastIndex + 1 ).trim();
	if ( !_isBC && isApprovalsGroup ) {
		approvalGroupNames.put( new Integer( groupOrUserID ), itemData.substring( lastIndex + 1 ) );
	} else {
		groupNames.put( new Integer( groupOrUserID ), itemData.substring( lastIndex + 1 ) );
	}
	    

	chainItem.setGroupOrUserID( groupOrUserID );
	chainItem.setUsingUser( isUsingUser );
	chainItem.setUserType( userType );
	if ( !_isBC ) {
		chainItem.setIsApprovalsGroup( isApprovalsGroup );
	}
}

// put the data into the session
session.setAttribute( "ApprovalsChainItemsNames", chainItemsNames );
session.setAttribute( "ApprovalsChainItemsGroupNames", groupNames );
if ( !_isBC ) {
	session.setAttribute( "ApprovalsChainItemsApprovalGroupNames", approvalGroupNames );
}
session.setAttribute( com.ffusion.tasks.approvals.IApprovalsTask.APPROVALS_CHAIN_ITEMS, chainItems );

%>

<ffi:setProperty name="GoToApprovalsAddLevelPage" value="false"/>
<s:include value="%{#session.PagesPath}common/include-approval-detect-stall-condition.jsp"/>
