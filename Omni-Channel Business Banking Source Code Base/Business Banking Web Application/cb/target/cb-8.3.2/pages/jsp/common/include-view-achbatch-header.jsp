<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%-- ----------------------------------------
	this file is used to display the top portion, batch-related items for the following files:
		achbatchdelete.jsp
		achbatchview.jsp
		achbatch-reverse.jsp
	if you make changes, test with all of the files
--------------------------------------------- --%>

    <ffi:setProperty name="OpenEnded" value="false" />
    <ffi:cinclude value1="${ACHBatch.NumberPayments}" value2="999"><ffi:setProperty name="OpenEnded" value="true" /></ffi:cinclude>
    <ffi:setProperty name="NumberPayments" value="${ACHBatch.NumberPayments}" />
    <ffi:setProperty name="Frequency" value="${ACHBatch.Frequency}" />

<%-- ACH Company is now needed to display the batch properly --%>
	<ffi:object id="SetACHCompany" name="com.ffusion.tasks.ach.SetACHCompany" scope="session"/>
	<ffi:setProperty name="SetACHCompany" property="CompanyID" value="${ACHBatch.CompanyID}" />
	<ffi:process name="SetACHCompany" />
	<ffi:removeProperty name="SetACHCompany"/>

<%
	boolean isApproval = false;
	boolean isTemplate = false;
	boolean canRecurring = true;
	String classCode = "";
	if (session.getAttribute("ApprovalsViewTransactionDetails") != null) {
		isApproval = true;
	}
	if (session.getAttribute("ViewTemplateBatch") != null) {
		isTemplate = true;
	}

	if( !isApproval && session.getAttribute("ACHOffsetAccounts") == null ){ %>
		<ffi:object id="GetACHOffsetAccounts" name="com.ffusion.tasks.ach.GetACHOffsetAccounts" scope="session"/>
		<ffi:process name="GetACHOffsetAccounts"/>
		<ffi:removeProperty name="GetACHOffsetAccounts"/>
		<ffi:setProperty name="ACHOffsetAccounts" property="SortedBy" value="OFFSETACCOUNTNUMBER" />
<% } %>
<ffi:cinclude value1="${ACHBatch.TemplateScope}" value2="" operator="notEquals">
    <ffi:cinclude value1="${ACHBatch.TemplateID}" value2="" operator="equals">
        <% isTemplate = true; %>
    </ffi:cinclude>
</ffi:cinclude>

</table>
<%-- <table width="720" border="0" cellspacing="0" cellpadding="3">
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
<tr>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
<tr class="lightBackground">
</ffi:cinclude> --%>
<div class="blockWrapper label150">
	<div  class="marginTop10"></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewAchBatchcompanyNameLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_482"/>:</span>
				<span id="viewAchBatchCompanuNamevlaue" class="columndata">
					<ffi:getProperty name="ACHCOMPANY" property="CompanyName" />&nbsp;-&nbsp;<ffi:getProperty name="ACHCOMPANY" property="CompanyID" />
				</span>
			</div>
			<div class="inlineBlock">
					<span id="viewAchBatchTypeLabel" class="sectionsubhead sectionLabel"><s:text name="jsp.default_67"/>:</span>
					<span id="viewAchBatchTypeValue" class="columndata">
						<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ACHBatch" operator="equals">
							<ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
							<ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
							<ffi:process name="ACHResource" />
							<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHClassCodeDesc${ACHBatch.StandardEntryClassCode}${ACHBatch.DebitBatch}"/>
							<ffi:getProperty name="ACHResource" property="Resource"/>
							<ffi:getProperty name="ACHBatch" property="StandardEntryClassCode" assignTo="classCode" />
				<%
					if (classCode == null)
						classCode = "";
					classCode = classCode.toUpperCase();
					if ("ARC".equals(classCode) || "POP".equals(classCode) || 
						"BOC".equals(classCode) || "RCK".equals(classCode) || "CIE".equals(classCode))
							canRecurring = false;
				%>
						</ffi:cinclude>
						<ffi:cinclude value1="${ACHBatch.ACHType}" value2="TaxPayment" operator="equals">
							<s:text name="jsp.default_509"/>
							<% canRecurring = false; %>
						</ffi:cinclude>
						<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ChildSupportPayment" operator="equals">
							<s:text name="jsp.default_480"/>
						</ffi:cinclude>
					</span>
			</div>
		</div>
		<div class="blockRow">
				<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ChildSupportPayment" >
					<div class="inlineBlock" style="width: 50%">
						<span class="sectionsubhead sectionLabel"><s:text name="jsp.default_481"/>:</span>
						<span>
						<% String taxformid; %>
						<ffi:getProperty name="ACHBatch" property="TaxFormID" assignTo="taxformid" />
						<ffi:object name="com.ffusion.tasks.ach.GetTaxForm" id="TaxForm" scope="session"/>
						<ffi:setProperty name="TaxForm" property="ID" value="<%=taxformid%>" />
						<ffi:process name="TaxForm"/>
							<ffi:cinclude value1="${TaxForm.type}" value2="CHILDSUPPORT"><ffi:getProperty name="TaxForm" property="State" /> State</ffi:cinclude>
							<ffi:getProperty name="TaxForm" property="IRSTaxFormNumber" /> - <ffi:getProperty name="TaxForm" property="TaxFormDescription" />
						</span>
					</div>
				</ffi:cinclude>
			<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true">
				<% canRecurring = false; %>
			</ffi:cinclude>
			<ffi:cinclude value1="${ViewCompletedBatch}" value2="true">
				<% canRecurring = false; // QTS 569058 - completed batches don't belong to recurring anymore %>
			</ffi:cinclude>
			
				<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
					<div class="inlineBlock">
							<span id="viewAchBatchBatchNameLabel" class="sectionsubhead sectionLabel">
						<%
							if (!isTemplate) {
						%>
								Batch Name
						<% } else { %>
								Template Name
						<% } %>
							:</span>
						
							<span id="viewAchBatchNameValue" class="columndata">
						<%
							if (!isTemplate) {
						%>
								<ffi:getProperty name="ACHBatch" property="Name"/>
						<% } else { %>
								<ffi:getProperty name="ACHBatch" property="TemplateName"/>
						<% } %>
							</span>
					</div>
				</ffi:cinclude>
				<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="equals" >
					<div class="inlineBlock">
                            <span class="sectionsubhead sectionLabel">
                                Template Name:
                            </span>
                            <span class="columndata">
                                <ffi:getProperty name="ACHBatch" property="TemplateName"/>
                            </span>
                    </div>
				</ffi:cinclude>
			</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="viewAchBatchCompanyDescriptionLabel" class="sectionsubhead sectionLabel">Company Description:</span>
				<span id="viewAchBatchCompanyDescriptionValue" class="columndata">
					<ffi:getProperty name="ACHBatch" property="CoEntryDesc"/>
				</span>
			</div>
				<% if (!"IAT".equals(classCode)) { %>
				    <div class="inlineBlock">
					<span id="viewAchBatchDiscretionaryDataLabel" class="sectionsubhead sectionLabel">
							Discretionary Data:
					</span>
					<span id="viewAchBatchDiscretionaryDataValue" class="columndata">
							<ffi:getProperty name="ACHBatch" property="CoDiscretionaryData"/>
					</span>
					</div>
				<% } else {
				// IAT doesn't have Discretionary Data
				%>
				    <div class="inlineBlock">
					<span class="sectionsubhead sectionLabel">
							Originating Currency:
					</span>
					<span class="columndata">
							USD - United States Dollar
					</span>
					</div>
				<% } %>	
		</div>
		
				<% if (!"IAT".equals(classCode)) { %>
				<% } else {
				// IAT doesn't have Discretionary Data
				%>
           <div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
        			<span class="sectionsubhead sectionLabel">Destination Country:</span>
	                <span class="columndata">
	                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
	                    <ffi:process name="ACHResource" />
	
	                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryName${ACHBatch.ISO_DestinationCountryCode}"/>
	                    <ffi:getProperty name="ACHResource" property="Resource"/>
	
	                </span>
            </div>
            <div class="inlineBlock">
        		<span class="sectionsubhead sectionLabel">Destination Currency:</span>
                    <ffi:object id="ACHResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
                    <ffi:setProperty name="ACHResource" property="ResourceFilename" value="com.ffusion.beansresources.ach.resources" />
                    <ffi:process name="ACHResource" />
                <span class="columndata">
                    <ffi:setProperty name="ACHResource" property="ResourceID" value="DestinationCountryCurrencyName${ACHBatch.ISO_DestinationCurrencyCode}"/>
                    <ffi:getProperty name="ACHBatch" property="ISO_DestinationCurrencyCode"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
                </span>
            </div></div>
<% } %>
		
				<% if (!"IAT".equals(classCode)) { %>
				<% } else {
				// IAT doesn't have Discretionary Data
				%>
            	<div class="blockRow">
					<div class="inlineBlock" style="width: 50%">
	        			<span class="sectionsubhead sectionLabel">Foreign Exchange Method:</span>
		                <span class="columndata">
		                    <ffi:setProperty name="ACHResource" property="ResourceID" value="ForeignExchangeIndicator${ACHBatch.ForeignExchangeIndicator}"/>
		                    <ffi:getProperty name="ACHBatch" property="ForeignExchangeIndicator"/>-<ffi:getProperty name="ACHResource" property="Resource"/>
		                </span>
					</div>
					<div class="inlineBlock">
						<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
							<span id="viewAchBatchEffectiveDateLabel" class="sectionsubhead sectionLabel">
									<%
										if (!isTemplate) {
									%>
											Effective Date:
									<% } else { %>
											Template Scope:
									<% } %>
										</span>
										<span id="viewAchBatchEffectiveDateValue" class="columndata">
									<%
										if (!isTemplate) {
									%>
									        <ffi:getProperty name="ACHBatch" property="Date" />
									<% } else { %>
									        <ffi:getProperty name="ACHBatch" property="BatchScope" />
									<% } %>
							</span>
						</ffi:cinclude>
					</div>
				</div>
				<% } %>
			<div class="blockRow">
				<%
					String headerTitle = "";
				%>
				<ffi:setProperty name="ACHResource" property="ResourceID" value="ACHHeaderCompanyName${ACHBatch.StandardEntryClassCode}"/>
				<ffi:getProperty name="ACHResource" property="Resource" assignTo="headerTitle" />
				<% if (headerTitle != null && headerTitle.length() > 0) { %>
						<div class="inlineBlock" style="width: 50%">
							<span id="viewAchBatchOriginalPayeeNameLabel" class="sectionsubhead sectionLabel">
								<%=	headerTitle %>:
							</span>
							<span id="viewAchBatchOriginalPayeeNameValue" class="columndata">
								<ffi:getProperty name="ACHBatch" property="HeaderCompName"/>
							</span>
						</div>
				<% } %>
				<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
					<div class="inlineBlock">
						<span id="viewAchBatchStatusLabel" class="sectionsubhead sectionLabel" >Status:</span>
						<span id="viewAchBatchStatusValue" class="columndata"><ffi:getProperty name="ACHBatch" property="Status" /></span>
					</div>
				</ffi:cinclude>
			</div>
			<div class="blockRow">
				<ffi:cinclude value1="${ViewMultipleBatchTemplate}" value2="true" operator="notEquals" >
					<div class="inlineBlock" style="width: 50%">
						<span id="viewAchTrackingIdLabel"  class="sectionsubhead sectionLabel" >Tracking ID:</span>
						<span id="viewAchTrackingIdValue" class="columndata"><ffi:getProperty name="ACHBatch" property="TrackingID" /></span>
					</div>
				</ffi:cinclude>
				<div class="inlineBlock">
					<span id="viewAchBatchRepeatingLabel" class="sectionsubhead " style="width:100px">
						<% if (canRecurring) { %>
							Repeating:
						<% } %>
					</span>
					<span id="viewAchBatchrepeatingValue" class="columndata">
						<% if (canRecurring) { %>
							<ffi:getProperty name="Frequency"/>
						<% } %>
					</span>
					<span id="viewAchBatchCheckedLabel" class="sectionsubhead " colspan='2'>
						<% if (canRecurring) { %>
							<input id="viewAchBatchCheckedValue" type="radio" name="OpenEnded" disabled value="true" border="0" <ffi:cinclude value1="${NumberPayments}" value2="999">checked</ffi:cinclude>>
						<% } %>
					</span>
					<span id="viewAchBatchUnlimitedLabel" class="sectionsubhead">
						<% if (canRecurring) { %>
							Unlimited
						<% } %>
					</span>
					<span class="sectionsubhead " colspan='2'>
						<% if (canRecurring) { %>
							<input id="viewAchBatchUnlimitedValue" disabled type="radio" name="OpenEnded" value="false" border="0" <ffi:cinclude value1="${NumberPayments}" value2="999" operator="notEquals" >checked</ffi:cinclude>>
						<% } %>
					</span>
					<span id="viewAchBatchofPaymentLabel" class="sectionsubhead">
						<% if (canRecurring) { %>
							# of Payments <ffi:getProperty name="NumberPayments"/>
						<% } %>
					</span>
				</div>
			</div>
			
			<ffi:cinclude value1="${ACHBatch.BatchType}" value2="Batch Balanced" operator="equals">
				<div class="blockRow">
					<% if( !isApproval ) { %>
					<span class="sectionsubhead ">Offset Account</span>
					<span class="columndata">
			            <ffi:object id="GetACHOffsetAccounts" name="com.ffusion.tasks.ach.GetACHOffsetAccounts" scope="session"/>
			            <ffi:process name="GetACHOffsetAccounts"/>
			            <ffi:removeProperty name="GetACHOffsetAccounts"/>
						<ffi:setProperty name="ACHOffsetAccounts" property="Filter" value="ID=${ACHBatch.OffsetAccountID}" />
						<ffi:list collection="ACHOffsetAccounts" items="OffsetAccount">
							<ffi:getProperty name="OffsetAccount" property="Number"/>-<ffi:getProperty name="OffsetAccount" property="Type"/>
						</ffi:list>
						<ffi:setProperty name="ACHOffsetAccounts" property="Filter" value="All" />
					</span>
					<% } else { %>
					<% } %>
				</div>
			</ffi:cinclude>
	
</div></div>
	
<%-- <ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
<tr>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
<tr class="lightBackground">
</ffi:cinclude> --%>
<ffi:cinclude value1="${ACHBatch.ACHType}" value2="ChildSupportPayment" >
	<%-- <ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
	<tr>
	</ffi:cinclude>
	<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
	<tr class="lightBackground">
	</ffi:cinclude> --%>
</ffi:cinclude>

<%-- </table>
<table width="720" border="0" cellspacing="0" cellpadding="3">
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
<tr>
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
<tr class="lightBackground">
</ffi:cinclude> --%>

<%--- first column of information --%>
<%--- SECOND column of information --%>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="notEquals">
</ffi:cinclude>
<ffi:cinclude value1="${APPROVALS_BC}" value2="true" operator="equals">
</ffi:cinclude>
