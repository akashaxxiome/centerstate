<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%--
This page contains all of the field and button labels used in wire transfers
It is included by all wire transfer pages using an include directive, instead
of an ffi:include, so that it is compiled as part of the page.

NOTE:  Changes made to this file are not automatically recompiled
unless the parent file is modified.

To make the changes recompile otherwise, you should delete the files
in the WebSphere temp directory (no need to restart the app server).
--%>

<%
/*****************
 *  FORM LABELS  *
 *****************/

// Template labels
String LABEL_TEMPLATE_NAME = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_416", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_TEMPLATE_NICKNAME = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_417", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_TEMPLATE_CATEGORY = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_410", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_TEMPLATE_SCOPE = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_418", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_TEMPLATE_ID = 			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_412", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());

// Beneficiary labels
String LABEL_BENEFICIARY_NAME = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_71", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_DRAWDOWN_ACCOUNT_NAME = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_149", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // drawdown

String LABEL_NICKNAME = 			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_294", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_ADDRESS_1 = 			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_36", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_ADDRESS_2 = 			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_37", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_CITY = 				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_101", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_STATE_PROVINCE = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_387", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_STATE = 				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_386", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_ZIP_POSTAL_CODE = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_473", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_ZIP_CODE = 			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_197", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_COUNTRY = 				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_115", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_CONTACT_PERSON = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_110", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_ACCOUNT_NUMBER = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_19", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_DRAWDOWN_ACCOUNT_NUMBER = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_64", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // drawdown
String LABEL_ACCOUNT_TYPE = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_20", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_DRAWDOWN_ACCOUNT_TYPE = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_65", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // drawdown

String LABEL_BENEFICIARY_TYPE = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_73", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_BENEFICIARY_SCOPE = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_72", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_DRAWDOWN_ACCOUNT_SCOPE = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_150", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // drawdown

String LABEL_SELECT_BENEFICIARY = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_145", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_SELECT_DRAWDOWN_ACCOUNT = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_146", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // drawdown

// Beneficiary Bank Labels
String LABEL_BANK_NAME = 			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_63", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_DRAWDOWN_BANK_NAME = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_154", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // drawdown
String LABEL_AFFILIATE_BANKS = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_14", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // book

String LABEL_BANK_ADDRESS_1 = 		LABEL_ADDRESS_1;
String LABEL_BANK_ADDRESS_2 = 		LABEL_ADDRESS_2;
String LABEL_BANK_CITY = 			LABEL_CITY;
String LABEL_BANK_STATE_PROVINCE = 	LABEL_STATE_PROVINCE;
String LABEL_BANK_STATE = 			LABEL_STATE;
String LABEL_BANK_ZIP_POSTAL_CODE = LABEL_ZIP_POSTAL_CODE;
String LABEL_BANK_ZIP_CODE = 		LABEL_ZIP_CODE;
String LABEL_BANK_COUNTRY = 		LABEL_COUNTRY;

String LABEL_FED_ABA = 				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_80", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_DRAWDOWN_BANK_ABA = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_151", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // drawdown
String LABEL_SWIFT = 				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_401", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_CHIPS = 				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_99", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_NATIONAL = 			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_289", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_IBAN = 			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_88", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER =  	 com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_54", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());

// Payment labels
String LABEL_FROM_ACCOUNT =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_147", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_CREDIT_ACCOUNT =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_121", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());   // drawdown
String LABEL_AMOUNT =				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_43", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_ORIG_AMOUNT =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_111", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // international
String LABEL_USD_AMOUNT =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_17", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());    // international
String LABEL_PAYMENT_AMOUNT =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_318", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());   // international
String LABEL_TEMPLATE_LIMIT =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_415", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_FREQUENCY =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_351", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_UNLIMITED =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_446", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_NUMBER_OF_PAYMENTS =	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_2", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_DUE_DATE =				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_457", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_REQ_DUE_DATE =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_357", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_ACTUAL_DUE_DATE =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_194", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_PROCESSING_DATE =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_334", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // international & host
String LABEL_REQ_PROCESSING_DATE =	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_356", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // international
String LABEL_ACTUAL_PROCESSING_DATE = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_193", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // international
String LABEL_SETTLEMENT_DATE =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_380", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // international
String LABEL_DEBIT_CURRENCY = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_155", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());   // international
String LABEL_PAYMENT_CURRENCY = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_319", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()); // international
String LABEL_EXCHANGE_RATE =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_156", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // international
String LABEL_MATH_RULE =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_278", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());        // international
String LABEL_CONTRACT_NUMBER =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_113", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());  // international

// By Order Of labels
String LABEL_BOO_NAME =				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_283", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_BOO_ADDRESS_1 = 		LABEL_ADDRESS_1;
String LABEL_BOO_ADDRESS_2 = 		LABEL_ADDRESS_2;
String LABEL_BOO_ADDRESS_3 = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_38", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_BOO_ACCOUNT_NUMBER =	LABEL_ACCOUNT_NUMBER;

// Other labels
String LABEL_HOST_ID =				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_230", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_TRACKING_ID =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_435", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String LABEL_APPLICATION_ID =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_46", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());


/*****************
 *  BUTTON TEXT  *
 *****************/

String BUTTON_CANCEL = 					com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_82", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_BACK = 					com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_57", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_DONE = 					com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_175", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_CONTINUE = 				com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_111", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_LOAD_TEMPLATE =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_264", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_SEARCH = 					com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_373", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_SUBMIT_WIRE = 			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_397", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_CONFIRM_SUBMIT_WIRE =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_108", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_SUBMIT_TEMPLATE = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_371", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_CONFIRM_SUBMIT_TEMPLATE =	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_49", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_SAVE_AS_TEMPLATE = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_368", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_MANAGE_BENEFICIARY = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_270", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_ADD_INTER_OR_REC_BANK =	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_12", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_ADD_INTERMEDIARY_BANK =	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_11", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_ADD_RECEIVING_BANK = 		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_13", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_VIEW_AMOUNTS =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_166", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_SAVE_BENEFICIARY =		com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_369", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BUTTON_CONFIRM_SAVE_BENEFICIARY = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_48", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());


/*********************
 *  COLUMN HEADINGS  *
 *********************/

// Intermediary Banks
String COLUMN_INTERMEDIARY_BANK = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_251", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String COLUMN_FED_ABA = 			LABEL_FED_ABA;
String COLUMN_SWIFT = 				LABEL_SWIFT;
String COLUMN_CHIPS = 				LABEL_CHIPS;
String COLUMN_NATIONAL =			LABEL_NATIONAL;
String COLUMN_IBAN = 			LABEL_IBAN;
String COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER =  	 LABEL_CORRESPONDENT_BANK_ACCOUNT_NUMBER;


/**************************
 *  DEFAULT FORM OPTIONS  *
 **************************/

String DEFAULT_TEMPLATE =				"- " + com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_NoTemplate", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) + " -";
String DEFUALT_BENEFICIARY =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_55", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String DEFUALT_DRAWDOWN_BENEFICIARY = 	com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_56", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String DEFUALT_UNMANAGED_BENE =			com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_109", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String DEFUALT_UNMANAGED_DRAWDOWN_BENE = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_110", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String DEFAULT_AFFILIATE_BANK = 		"- " + com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_SelectAffiliateBank", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) + " -";
String DEFAULT_CREDIT_ACCOUNT = 		"- " + com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.billpay_96", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale()) + " -";

/*******************
 *  OTHER STRINGS  *
 *******************/

String DOMESTIC = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_70", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String INTERNATIONAL = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_93", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String BOOK = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_75", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String DRAWDOWN = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_176", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String FED = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.common_78", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
String HOST = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.default_229", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());
%>