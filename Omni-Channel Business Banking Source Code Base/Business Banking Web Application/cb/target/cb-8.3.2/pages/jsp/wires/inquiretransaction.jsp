<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<script type="text/javascript" src="<s:url value='/web/js/custom/widget/ckeditor/ckeditor.js'/>"></script>
<link type="text/css" rel="stylesheet"  media="all" href="<s:url value='/web/js/custom/widget/ckeditor/contents.css'/>"/>

<ffi:help id="payments_wiretransferInquiry" className="moduleHelpClass"/>
<%-- <ffi:cinclude value1="${messaging_init_touched}" value2="true" operator="notEquals" >
	<s:include value="%{#session.PagesPath}inc/init/messaging-init.jsp" />
</ffi:cinclude> --%>

<%
	
	String Subject = com.opensymphony.xwork2.util.LocalizedTextUtil.findDefaultText("jsp.message_34", ((com.ffusion.beans.user.UserLocale)session.getAttribute("UserLocale")).getLocale());	
%>


<div align="center">
			<table width="650" border="0" cellspacing="0" cellpadding="0">				
				<tr>
					<td class="columndata lightBackground" colspan="2">
						<div align="center">
							<span class="sectionsubhead"><s:text name="jsp.default.inquiry.message.RTE.label" /><span class="required">*</span></span>
						</div>
					</td>					
				</tr>
				<tr>
					<td class="columndata lightBackground" colspan="2">
						<s:form id="inquiryForm" name="form4" theme="simple" method="post" action="/pages/jsp/wires/sendFundsTransMessageAction_sendWireInquiry.action" style="text-align: left;">												
                    		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    		<input type="hidden" name="fundTransactionMessage.subject" value="<%=Subject%>"/>
							<input type="hidden" name="fundTransactionMessage.fundsID" value="<ffi:getProperty name='FundsID'/>"/>													
							<input type="hidden" name="paymentType" value="<ffi:getProperty name='paymentType'/>"/>
							<input type="hidden" name="recurringId" value="<ffi:getProperty name='recurringId'/>"/>
							<input type="hidden" name="fundTransactionMessage.from" value="<ffi:getProperty name='userId'/>"/>
							<div>
								<p><textarea class="txtbox ui-widget-content ui-corner-all" id="sendWireInquiry" name="fundTransactionMessage.memo" rows="10" cols="75" wrap="virtual" style="overflow-y: auto;"></textarea>
								</p>
							</div>
						</s:form>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="50">&nbsp;</td>
				</tr>
				<tr>
					<td class="ui-widget-header customDialogFooter" colspan="2" align="center">
						<sj:a id="closeInquireTransferLink" button="true" 
							  onClickTopics="closeDialog" 
							  title="Cancel"><s:text name="jsp.default_82" /></sj:a>
							  
						<sj:a id="cancelTransferSubmitButton" 
							  formIds="inquiryForm" 
							  targets="resultmessage" 
							  button="true" 
							  validate="true"
							  validateFunction="customValidation" 
							  title="Send Inquiry"
							  onSuccessTopics="sendInquiryWiresTransferFormSuccess"><s:text name="jsp.default_378" /></sj:a>
					</td>
				</tr>
				<br>
			</table>
			<p></p>
			
		</div>
		
<script>
	setTimeout(function(){ns.common.renderRichTextEditor('sendWireInquiry','600',true);}, 600);
</script>		