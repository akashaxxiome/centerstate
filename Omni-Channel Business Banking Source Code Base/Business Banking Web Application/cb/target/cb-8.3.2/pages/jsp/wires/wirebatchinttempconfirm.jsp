<%--
This is the template international wire batch confirmation page. Note it is only
for international wire batches.  All other wire types use wirebatchtempconfirm.jsp

Pages that request this page
----------------------------
wirebatchtemp.jsp
	SUBMIT WIRE BATCH button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
BACK requests wirebatchtemp.jsp
CONFIRM/SEND BATCH requests wirebatchsend.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<ffi:help id="payments_wirebatchinttempconfirm" className="moduleHelpClass"/>
<% String itemsPerPageStr = ""; String pageStr = ""; String pagesStr = "";  int mismatchCount=0; %>
<ffi:getProperty name="wireBatchItemsPerPage" assignTo="itemsPerPageStr"/>
<ffi:getProperty name="templatePage" assignTo="pageStr"/>
<ffi:getProperty name="templatePages" assignTo="pagesStr"/>


	<%-- Copy DateToPost to SettlementDate if SettlementDate is empty --%>
	<% String settleDate = ""; %>
	<ffi:getProperty name="wireBatchObject" property="SettlementDate" assignTo="settleDate"/>
	<%
	if (settleDate == null) settleDate = "";
	if (settleDate.equals("")) {
		String postDate = "";
	%>
		<ffi:getProperty name="wireBatchObject" property="DateToPost" assignTo="postDate"/>
		<ffi:setProperty name="wireBatchObject" property="SettlementDate" value="<%= postDate %>"/>
	<%
	}
	%>
<div align="center" class="label150">
<span id="wireBatchResultMessage" style="display:none">Your wire batch has been saved.</span>
<div class="marginTop20">
	<s:if test="%{isDueDateChanged && nonBusinessDay}">
        <%-- Both proc window is closed and tomorrow is not a biz day --%>
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
            <!--L10NStart-->NOTE: The International wire processing window for today has closed,
            so the Requested Processing Date has been changed to tomorrow's date.
            Since tomorrow is not a business day, the Expected Processing Date has been changed
            to the next business day.<!--L10NEnd--><br><br>
        </div>
    </s:if>
    <s:elseif test="%{isDueDateChanged}">
        <%-- DueDate changed because processing window is closed --%>
        <div class="sectionsubhead" style="color:red" align="center">
            <!--L10NStart-->NOTE: The International wire processing window for today has closed.
            The Requested Processing Date has been changed to tomorrow's date.<!--L10NEnd--><br><br>
        </div>
     </s:elseif>
    <s:elseif test="%{nonBusinessDay}">
        <%-- DueDate changed because its not a business day --%>
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
            <!--L10NStart-->NOTE: The Requested Processing Date is not a business day.
            The Expected Processing Date has been changed to the next business day.<!--L10NEnd--><br><br>
        </div>
  </s:elseif>

    <%-- Show warning count since there is more than one --%>
    <% int index = 0; %>

   <s:if test="%{duplicateWire}">
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
					<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchinttempconfirm.jsp-2" parm0="<%= String.valueOf(++index) %>" />
        </div>
    </s:if>

    <%-- Mismatched currency type warning --%>
    <ffi:cinclude value1="<%= String.valueOf(mismatchCount) %>" value2="0" operator="notEquals">
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
					<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchinttempconfirm.jsp-1" parm0="<%= String.valueOf(++index) %>" parm1="<%= String.valueOf(mismatchCount) %>" />
					<br><br>
        </div>
    </ffi:cinclude>
</div>
<div class="blockWrapper" role="form" aria-labelledby="batchInfoHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="batchInfoHeader">Batch Information</h2><!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Batch Name<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Application Type<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchType"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Requested Processing Date<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="wireBatchObject" property="DueDate"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Settlement Date<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="wireBatchObject" property="SettlementDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Expected Processing Date<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="wireBatchObject" property="DateToPost"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Debit Currency<!--L10NEnd--></span>
				<span class="columndata">
				<ffi:cinclude value1="${wireBatchObject.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_MOD %>" operator="notEquals">
					<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${wireBatchObject.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_MOD %>" operator="equals">
					<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>
				</ffi:cinclude>
				</span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Payment Currency<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="wireBatchObject" property="PayeeCurrencyType"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Exchange Rate<!--L10NEnd--></span>
										<span class="columndata"><ffi:getProperty name="wireBatchObject" property="ExchangeRate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Math Rule<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="wireBatchObject" property="MathRule"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Contract Number<!--L10NEnd--></span>
				<span class="columndata"><ffi:getProperty name="wireBatchObject" property="ContractNumber"/></span>
			</div>
		</div>
	</div>
</div>

<div class="blockWrapper marginTop20" role="form" aria-labelledby="includeBatchesHeader">
	<div  class="blockHead"><h2 id="includeBatchesHeader">Included Batches</h2></div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
						<tr class="header">
							<td align="center" class="sectionsubhead" colspan="2"><!--L10NStart-->Beneficiary<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead"><!--L10NStart-->Amount<!--L10NEnd--></td>
						</tr>
						<ffi:list collection="wireBatchObject.Wires" items="wire">
							<s:if test="%{#request.wire.Action != 'del' && #request.wire.Status != 3}">
						<tr>
							<td align="center" class="columndata" colspan="2" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
								<ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
							<td align="left" class="columndata" nowrap>
							
							
							<s:if test="%{wireBatchObject.Action == null || (wireBatchObject.Action != null && !wireBatchObject.Action == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_ACTION_MOD)}">							
								<ffi:getProperty name="wire" property="AmountForBPW"/> (<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
							</s:if>							
							<s:else>	
								<ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="equals">
									<ffi:getProperty name="wire" property="AmountForBPW"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
								</ffi:cinclude>
								<ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
									<ffi:getProperty name="wire" property="OrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
								</ffi:cinclude>
							</s:else>
							</td>
						</tr>
						</s:if>
						</ffi:list>
						
						<s:if test="%{wireBatchObject.paymentAmount == null}">
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Original Total:<!--L10NEnd--></td>
							<td align="left" class="tbrd_t sectionsubhead" width="20%" valign="bottom" nowrap>
							<s:if test="%{!isModifyAction}">
								<ffi:getProperty name="wireBatchObject" property="TotalAmountForBPW"/> (<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
							</s:if>
							<s:else>
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
							</s:else>
							</td>
						</tr>
							<s:if test="%{wireBatchAmountUrl == 'modifyWireBatchTemplateAction_getBatchAmount.action'}">
								<script>
								//QTS680696
 									ns.wire.viewTotalsInBatchConfirmURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/modifyWireBatchTemplateAction_getBatchAmount.action"/>' ;
 								</script>
							</s:if>
							<s:else>
								<script>
								//QTS680696
 									ns.wire.viewTotalsInBatchConfirmURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireBatchTemplateAction_getBatchAmount.action"/>' ;
 								</script>
							</s:else>
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Total in USD:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="20%" valign="bottom" nowrap><%-- <ffi:link url="wirebatchinttempconfirm.jsp?GetAmounts=true"><!--L10NStart-->View Totals<!--L10NEnd--></ffi:link>--%>
								<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm()">View Totals</a>
							</td>
						</tr>
								
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Total Payment:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="20%" valign="bottom" nowrap><%--<ffi:link url="wirebatchinttempconfirm.jsp?GetAmounts=true"><!--L10NStart-->View Totals<!--L10NEnd--></ffi:link>--%>
								<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm()">View Totals</a>
							</td>
						</tr>
						</s:if>
						<s:else>
						
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Original Total:<!--L10NEnd--></td>
							<td align="left" class="tbrd_t sectionsubhead" width="20%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>
								<s:if test="%{!isModifyAction}">
									(<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
								</s:if>
								<s:else>
									(<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
								</s:else>
							</td>
						</tr>
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Total in USD:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="20%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="TotalDebit"/>
							</td>
						</tr>
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Total Payment:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="20%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="paymentAmount"/> (<ffi:getProperty name="wireBatchObject" property="PayeeCurrencyType"/>)
							</td>
						</tr>
						</s:else>
						
					</table>
	</div>
</div>
            <%--  <table width="750" cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td class="columndata ltrow2_color" align="center">
                    <table width="715 border="0" cellspacing="0" cellpadding="3">
   Show alert if date was changed --%>
   
      <%-- <s:if test="%{isDueDateChanged && nonBusinessDay}">
    <tr>
        Both proc window is closed and tomorrow is not a biz day
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
            <!--L10NStart-->NOTE: The International wire processing window for today has closed,
            so the Requested Processing Date has been changed to tomorrow's date.
            Since tomorrow is not a business day, the Expected Processing Date has been changed
            to the next business day.<!--L10NEnd--><br><br>
        </td>
    </tr>
    </s:if>
    <s:elseif test="%{isDueDateChanged}">
    <tr>
        DueDate changed because processing window is closed
        <td class="sectionsubhead" style="color:red" align="center">
            <!--L10NStart-->NOTE: The International wire processing window for today has closed.
            The Requested Processing Date has been changed to tomorrow's date.<!--L10NEnd--><br><br>
        </td>
    </tr>
     </s:elseif>
    <s:elseif test="%{nonBusinessDay}">
    <tr>
        DueDate changed because its not a business day
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
            <!--L10NStart-->NOTE: The Requested Processing Date is not a business day.
            The Expected Processing Date has been changed to the next business day.<!--L10NEnd--><br><br>
        </td>
    </tr>
  </s:elseif>

    Show warning count since there is more than one
    <% int index = 0; %>

   <s:if test="%{duplicateWire}">
    <tr>
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
					<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchinttempconfirm.jsp-2" parm0="<%= String.valueOf(++index) %>" />
        </td>
    </tr>
    </s:if>

    Mismatched currency type warning
    <ffi:cinclude value1="<%= String.valueOf(mismatchCount) %>" value2="0" operator="notEquals">
    <tr>
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
					<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchinttempconfirm.jsp-1" parm0="<%= String.valueOf(++index) %>" parm1="<%= String.valueOf(mismatchCount) %>" />
					<br><br>
        </td>
    </tr>
    </ffi:cinclude> 
                        <tr>
							<td align="left" class="tbrd_b sectionhead"><!--L10NStart-->Batch Information<!--L10NEnd--></td>
						</tr>
					</table>
					<br>--%>
					<%-- <table width="715" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="357" valign="top">
								<table width="355" cellpadding="3" cellspacing="0" border="0">
									<tr>
										<td align="left" width="160" class="sectionsubhead"><!--L10NStart-->Batch Name<!--L10NEnd--></td>
										<td align="left" width="183" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchName"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Application Type<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchType"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Requested Processing Date<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="DueDate"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Settlement Date<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="SettlementDate"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Expected Processing Date<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="DateToPost"/></td>
									</tr>
								</table>
							</td>
							<td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
							<td width="348" valign="top">
								<table width="347" cellpadding="3" cellspacing="0" border="0">
									<tr>
										<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Debit Currency<!--L10NEnd--></td>
										<td align="left" width="220" class="columndata">
										<ffi:cinclude value1="wireBatchObject" value2="ModifyWireBatch" operator="notEquals">
											<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>
										</ffi:cinclude>
										<ffi:cinclude value1="wireBatchObject" value2="ModifyWireBatch" operator="equals">
											<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>
										</ffi:cinclude>
										</td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Payment Currency<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="PayeeCurrencyType"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Exchange Rate<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="ExchangeRate"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Math Rule<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="MathRule"/></td>
									</tr>
									<tr>
										<td align="left" class="sectionsubhead"><!--L10NStart-->Contract Number<!--L10NEnd--></td>
										<td align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="ContractNumber"/></td>
									</tr>
								</table>
							</td>
						</tr>
					</table> --%>
					<!-- <br>
					<table width="98%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td align="left" class="tbrd_b sectionhead">L10NStartIncluded BatchesL10NEnd</td>
						</tr>
					</table> -->
					<%-- <table width="50%" border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td align="left" class="sectionsubhead" colspan="2"><!--L10NStart-->Beneficiary<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead"><!--L10NStart-->Amount<!--L10NEnd--></td>
						</tr>
						<ffi:list collection="wireBatchObject.Wires" items="wire">
							<s:if test="%{#request.wire.Action != 'del' && #request.wire.Status != 3}">
						<tr>
							<td align="left" class="columndata" colspan="2" nowrap><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
								<ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
							<td align="left" class="columndata" nowrap>
							
							<s:if test="%{!isModifyAction}">
								<ffi:getProperty name="wire" property="AmountForBPW"/> (<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
								</s:if>
							<s:else>	
								<ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="equals">
									<ffi:getProperty name="wire" property="AmountForBPW"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
								</ffi:cinclude>
								<ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
									<ffi:getProperty name="wire" property="OrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
								</ffi:cinclude>
							</s:else>
							</td>
						</tr>
						</s:if>
						</ffi:list>
						
						<s:if test="%{wireBatchObject.paymentAmount == null}">
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Original Total:<!--L10NEnd--></td>
							<td align="left" class="tbrd_t sectionsubhead" width="1%" valign="bottom" nowrap>
							<s:if test="%{!isModifyAction}">
								<ffi:getProperty name="wireBatchObject" property="TotalAmountForBPW"/> (<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
							</s:if>
							<s:else>
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
							</s:else>
							</td>
						</tr>
							<s:if test="%{wireBatchAmountUrl == 'modifyWireBatchTemplateAction_getBatchAmount.action'}">
								<script>
								//QTS680696
 									ns.wire.viewTotalsInBatchConfirmURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/modifyWireBatchTemplateAction_getBatchAmount.action"/>' ;
 								</script>
							</s:if>
							<s:else>
								<script>
								//QTS680696
 									ns.wire.viewTotalsInBatchConfirmURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireBatchTemplateAction_getBatchAmount.action"/>' ;
 								</script>
							</s:else>
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Total in USD:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap><ffi:link url="wirebatchinttempconfirm.jsp?GetAmounts=true"><!--L10NStart-->View Totals<!--L10NEnd--></ffi:link>
								<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm()">View Totals</a>
							</td>
						</tr>
								
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Total Payment:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap><ffi:link url="wirebatchinttempconfirm.jsp?GetAmounts=true"><!--L10NStart-->View Totals<!--L10NEnd--></ffi:link>
								<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm()">View Totals</a>
							</td>
						</tr>
						</s:if>
						<s:else>
						
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Original Total:<!--L10NEnd--></td>
							<td align="left" class="tbrd_t sectionsubhead" width="1%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>
								<s:if test="%{!isModifyAction}">
									(<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
								</s:if>
								<s:else>
									(<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
								</s:else>
							</td>
						</tr>
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Total in USD:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="TotalDebit"/>
							</td>
						</tr>
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Total Payment:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="paymentAmount"/> (<ffi:getProperty name="wireBatchObject" property="PayeeCurrencyType"/>)
							</td>
						</tr>
						</s:else>
						
					</table> --%>
					<%-- <s:form id="sendWireBacthFormID" method="post" name="FormName" namespace="/pages/jsp/wires" action="%{#session.wirebatch_confirm_form_url}" theme="simple">
					<form action="wirebatchsend.jsp" method="post" name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
					<sj:a id="cancelFormButtonOnBatchVerify"
						button="true" 
						onClickTopics="cancelWireTransferForm"
					><s:text name="jsp.default_82" />
					</sj:a>
					<sj:a id="backFormButton" 
						  button="true" 
						  onClickTopics="backToInput"
					><s:text name="jsp.default_57" />
					</sj:a>
                     <sj:a id="sendWireBacthSubmit"
						formIds="sendWireBacthFormID" 
						targets="confirmDiv" 
						button="true" 
						onBeforeTopics="sendWireBacthFormBeforeTopics"
						onSuccessTopics="sendWireBacthFormSuccessTopic"
						onErrorTopics="errorSendWireBatchForm"
						onCompleteTopics="sendWireBacthFormCompleteTopic"><s:text name="jsp.default_CONFIRM_SEND_BATCH" /></sj:a>
                    </form>
                    </s:form> 
					</td>
				</tr>
			</table>--%>
<div class="btn-row">
<s:form id="sendWireBacthFormID" method="post" name="FormName" namespace="/pages/jsp/wires" action="%{#session.wirebatch_confirm_form_url}" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<sj:a id="cancelFormButtonOnBatchVerify"
		button="true" summaryDivId="summary" buttonType="cancel"
		onClickTopics="showSummary,cancelWireBatchForm"
	><s:text name="jsp.default_82" />
	</sj:a>
	<sj:a id="backFormButton" 
		  button="true" 
		  onClickTopics="backToInput"
	><s:text name="jsp.default_57" />
	</sj:a>
                 <sj:a id="sendWireBacthSubmit"
		formIds="sendWireBacthFormID" 
		targets="confirmDiv" 
		button="true" 
		onBeforeTopics="sendWireBacthFormBeforeTopics"
		onSuccessTopics="sendWireBacthFormSuccessTopic"
		onErrorTopics="errorSendWireBatchForm"
		onCompleteTopics="sendWireBacthFormCompleteTopic"><s:text name="jsp.default_CONFIRM_SEND_BATCH" /></sj:a>
</s:form>
</div>
</div>

