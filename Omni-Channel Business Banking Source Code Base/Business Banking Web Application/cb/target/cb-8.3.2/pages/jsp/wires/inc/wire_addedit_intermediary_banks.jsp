<%--
This file contains the intermediary bank list and ADD INTERMEDIARY BANK button.

It is included on the following add/edit wire transfer and add/edit wire beneficiary
pages:
	wireaddpayee.jsp
	wirefed.jsp
	wiretransfernew.jsp

It includes the following file:
common/wire_labels.jsp
	The labels for fields and buttons
--%>
<%@page import="com.ffusion.struts.wires.WiresDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ include file="../../common/wire_labels.jsp"%>

<ffi:cinclude value1="${disableEdit}" value2="disabled">
<script>
	$("#addIntermediaryBankButtonID").fadeTo('slow', 0.5);
	$("#addIntermediaryBankButtonID").attr('onclick','');
	$("#wireIntermediaryBanksID").fadeTo('slow', 0.5);
	$("#wireIntermediaryBanksID").attr('onclick','');
</script>
</ffi:cinclude>

<% String payeeDest = ""; boolean showInternational = false; String intBankSize = "";
	boolean showFed = false; %>
<%@page import="com.ffusion.beans.wiretransfers.WireDefines"%>
<ffi:getProperty name="destination" assignTo="payeeDest"/>
<%-- Remove an intermediary bank from the IntermediaryBanks collection if a delete button was clicked --%>
<% 
	if( request.getParameter("DontInitialize") != null){ session.setAttribute("DontInitialize", request.getParameter("DontInitialize")); } 
	if( request.getParameter("delIntBank") != null){ session.setAttribute("delIntBank", request.getParameter("delIntBank")); } 
%>
<ffi:cinclude value1="${delIntBank}" value2="" operator="notEquals">
	<ffi:setProperty name="RemoveIntermediaryBank" property="BankIndex" value="${delIntBank}"/>
	<ffi:process name="RemoveIntermediaryBank"/>
	<%--ffi:setProperty name="${wireTask.Error}" value="0" / --%>
</ffi:cinclude>
<ffi:removeProperty name="delIntBank"/>
<%--
Show the international fields if:
    A) This is included in add/edit wire transfer, and the current wire destination is Domestic or International or Fed, or
    B) This is included in add/edit wire payee, and the payee destination is Regular.
	NOTE:  payeeTask == WireTransferPayee if this is being included in add/edit wire transfer.
--%>
	
	<% if (payeeDest.equals(WireDefines.WIRE_DOMESTIC) || payeeDest.equals(WireDefines.WIRE_INTERNATIONAL)) showInternational = true; %>
	<% if (payeeDest.equals(WireDefines.WIRE_FED)) {
			showFed = true;
		} %>
	<% if (payeeDest.equals(WireDefines.PAYEE_TYPE_REGULAR)) showInternational = true; %>


<%-- ------ INTERMEDIARY BANKS ------ --%>
<%-- Verify CODE
<ffi:cinclude value1="${disableEdit}" value2="disabled" operator="notEquals">
    <ffi:process name="CheckBanks"/>
</ffi:cinclude>
--%>
<ffi:cinclude value1="${disableEdit}" value2="disabled" operator="equals">
    <ffi:setProperty name="buttonName" value="ADD INTERMEDIARY BANK"/>
</ffi:cinclude>

<ffi:setProperty name="${payeeTask}" property="IntermediaryBanks.Filter" value="ACTION!!del"/>
<ffi:getProperty name="${payeeTask}" property="IntermediaryBanks.Size" assignTo="intBankSize"/>



<%
if (intBankSize == null) intBankSize = "0";
int intBanks = Integer.parseInt(intBankSize);
if (intBanks < com.ffusion.beans.wiretransfers.WireDefines.INTERMEDIARY_BANKS_MAX_SIZE) {
%>
<div align="center" class="marginBottom10">
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
 	<% //Due to sj:a button doesn't support disable feature, old button widget has to be used here.
       //replace the CSS of old search button with the new one, so that it looks like a new button widget.
       StringBuffer interBanksButtonClassString = new StringBuffer("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
       if((String)(session.getAttribute("disableEdit")) == "disabled")
    	   interBanksButtonClassString.append(" ui-state-disabled");
    %>
	
	<sj:a id="addIntermediaryBankButtonID" 
		button="true" 
		onclick="selectInterBank();"
		> 
		<s:text name="jsp.common_11" />**
	</sj:a>
</ffi:cinclude>
</div>
<%
}
%>

<% int daIntBanks = 0;
%>

<%-- Start: Dual approval processing --%>
<div align="center">
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	
	<% //Due to sj:a button doesn't support disable feature, old button widget has to be used here.
       //replace the CSS of old search button with the new one, so that it looks like a new button widget.
       StringBuffer interBanksButtonClassString = new StringBuffer("ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only");
       if((String)(session.getAttribute("disableEdit")) == "disabled")
    	   interBanksButtonClassString.append(" ui-state-disabled");
    %>
	
	<%
		daIntBanks = intBanks - daIntBanks;
		if (daIntBanks < com.ffusion.beans.wiretransfers.WireDefines.INTERMEDIARY_BANKS_MAX_SIZE) {
	%>
		
		<sj:a id="wireIntermediaryBanksID" 
			button="true" 
			onclick="selectInterBank();"
			cssStyle="margin:0 0 10px"> 
			<s:text name="jsp.common_11" />**
		</sj:a>
	
	<%
	}
	%>	
	
	<ffi:setProperty name="cssClass" value="sectionsubhead"/>
	<%-- Highlight corresponding bank account no only when the page gets loaded from beneficiary home page --%>	
	<ffi:cinclude value1="${IsPending}" value2="" operator="notEquals">
		<%--Get corresponding bank no if it is modified for any intermediary bank.  --%>
		<ffi:object id="GetDACategoryDetails" name="com.ffusion.tasks.dualapproval.GetDACategoryDetails"/>
			<ffi:setProperty name="GetDACategoryDetails" property="fetchMutlipleCategories" value="true"/>
			<ffi:setProperty name="GetDACategoryDetails" property="MultipleCategoriesSessionName" value="InterMultipleCategories"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemId" value="${ID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="categoryType" value="<%=IDualApprovalConstants.CATEGORY_WIRE_INTERMEDIARY_BANK%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY%>"/>
			<ffi:setProperty name="GetDACategoryDetails" property="businessId" value="${SecureUser.BusinessID}"/>
			<ffi:setProperty name="GetDACategoryDetails" property="daItemId" value="${daItemId}"/>
		<ffi:process name="GetDACategoryDetails"/>
		<ffi:setProperty name="HighlightCorrespondentBnkAccNo" value="false"/>
		<ffi:list collection="InterMultipleCategories" items="category">
			<ffi:cinclude value1="${category.userAction}" value2="<%=IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<%--Set the value only if it is false.Else dont overwrite the value while iterating in the list.  --%>
				<ffi:cinclude value1="${HighlightCorrespondentBnkAccNo}" value2="true" operator="notEquals">
					<ffi:setProperty name="HighlightCorrespondentBnkAccNo" value="true"/>
				</ffi:cinclude>
			</ffi:cinclude>
		</ffi:list>
		<ffi:cinclude value1="${HighlightCorrespondentBnkAccNo}" value2="true">
			<ffi:setProperty name="cssClass" value="sectionheadDA"/>
		</ffi:cinclude>
	</ffi:cinclude>
<div class="blockWrapper">

		<% int idx = 0; int counter = 0; %>
		<% String wireTaskName = ""; String errMessage = "";  %>
		<ffi:getProperty name="payeeTask" assignTo="wireTaskName" />
		<ffi:cinclude value1='<%=wireTaskName%>' value2="WireTransferPayee">
			<ffi:getProperty name="wireTask" assignTo="wireTaskName" />
		</ffi:cinclude>
		<ffi:getProperty name="<%=wireTaskName%>" property="ErrorMessage" assignTo="errMessage"/>
		<ffi:list collection="${payeeTask}.IntermediaryBanks" items="Bank1">	
		
		<ffi:setProperty name="displayCss" value="columndata"/>
			<!-- Highlight the intermediary banks only when displaying the page for the first time. -->			
			<ffi:cinclude value1="${IsPending}" value2="" operator="notEquals">
				<!-- Highlight the intermediary banks only when editing an existing beneficiary from pending island. -->
				<ffi:cinclude value1="${payeeTask}" value2="WireTransferPayee">
					<ffi:cinclude value1="${Bank1.action}" value2="<%=WireDefines.WIRE_ACTION_ADD %>">
						<ffi:setProperty name="displayCss" value="sectionheadDA"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Bank1.action}" value2="<%=WireDefines.WIRE_ACTION_MOD %>">
						
						We need to get the corresponding bank account no to display as a label. 
						<ffi:list collection="InterMultipleCategories" items="category">
							<ffi:list collection="category.daItems" items="DaItemDetail">
								<ffi:cinclude value1="${DaItemDetail.fieldName}" value2="ID" >
									<ffi:cinclude value1="${DaItemDetail.newValue}" value2="${Bank1.ID}" >
										<ffi:setProperty name="BankFound" value="true"/>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${BankFound}" value2="true" >
									<ffi:list collection="category.daItems" items="detail">
										<ffi:cinclude value1="${detail.fieldName}" value2="correspondentBankAccountNumber" >
											<ffi:setProperty name="OldCorrespondentBankAccNo" value="${detail.oldValue}"/>
										</ffi:cinclude>
									</ffi:list>
								</ffi:cinclude>
							</ffi:list>
						</ffi:list>
						<ffi:removeProperty name="BankFound"/>
					</ffi:cinclude>
				</ffi:cinclude>
			</ffi:cinclude>
			
<div  class="blockHead" style="font-size:22px; font-weight:normal">Intermediary Bank Details  
<ffi:cinclude value1="${Bank1.DeleteIcon}" value2="true" operator="notEquals">
	<ffi:cinclude value1="${disableEdit}" value2="disabled" operator="notEquals">
		<a href="javascript:deleteInterBank('<ffi:getProperty name="CallerURL"/>','<ffi:getProperty name="Bank1" property="ID"/>');"><span class="sapUiIconCls icon-delete" style="font-size:16px;"></span></a>
	</ffi:cinclude>
</ffi:cinclude>	
</div>
<div class="blockContent">
<% if (showInternational) { %>
<div class="blockRow">
	<div class="inlineBlock" style="width: 50%">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %>: </span>
		<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="BankName"/></span>
	</div>
	<div class="inlineBlock">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %>: </span>
		<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
	</div>
</div>	
<div class="blockRow">
	<div class="inlineBlock" style="width: 50%">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_SWIFT %>: </span>
		<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingSwift"/></span>
	</div>
	<div class="inlineBlock">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_CHIPS %>: </span>
		<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingChips"/></span>
	</div>
</div>	
<div class="blockRow">
	<div class="inlineBlock" style="width: 50%">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_NATIONAL %>: </span>
		<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingOther"/></span>
	</div>
	<div class="inlineBlock">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
		<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="IBAN"/></span>
	</div>
</div>	
<div class="blockRow">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
		<span class="<ffi:getProperty name='displayCss'/>">
			<ffi:cinclude value1="${Bank1.DeleteIcon}" value2="true" operator="notEquals">
				<input id="CurrentWireIntermediaryBank<%=counter%>"  name="<ffi:getProperty name="payeeTask"/>.CurrentWireIntermediaryBank=<ffi:getProperty name="Bank1" property="ID"/>&<ffi:getProperty name="payeeTask"/>.CorrespondentBankAccountNumber" value="<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="25" style="width:100" <ffi:getProperty name="disableEdit"/>>
				<%-- <span id="CurrentWireIntermediaryBank<%=counter%>"> <ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/></span> --%>
				<span class="sectionhead_greyDA"><ffi:getProperty name="OldCorrespondentBankAccNo"/></span>
				<ffi:removeProperty name="OldCorrespondentBankAccNo"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${Bank1.DeleteIcon}" value2="true">
				<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
			</ffi:cinclude>		
		</span>
</div>

<% } else if(showFed) { %>
	<td class="columndata" width="5%"><span class="sectionsubhead">&nbsp;</span></td>
			<div class="blockRow">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %></span>
				<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="BankName"/></span>
				
			</div>
			<div class="blockRow">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %></span>
				<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
			</div>
			<div class="blockRow">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %></span>
				<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="IBAN"/></span>
			</div>
			<div class="blockRow">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></span>
				<span class="<ffi:getProperty name='displayCss'/>">
					<ffi:cinclude value1="${Bank1.DeleteIcon}" value2="true" operator="notEquals">
						<input id="CurrentWireIntermediaryBank<%=counter%>"  name="<ffi:getProperty name="payeeTask"/>.CurrentWireIntermediaryBank=<ffi:getProperty name="Bank1" property="ID"/>&<ffi:getProperty name="payeeTask"/>.CorrespondentBankAccountNumber" value="<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="25" style="width:100" <ffi:getProperty name="disableEdit"/>>
						<%-- <span id="CurrentWireIntermediaryBank<%=counter%>"> <ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/></span> --%>
						<span class="sectionhead_greyDA"><ffi:getProperty name="OldCorrespondentBankAccNo"/></span>
						<ffi:removeProperty name="OldCorrespondentBankAccNo"/>
					</ffi:cinclude>
				</span>
				<ffi:cinclude value1="${Bank1.DeleteIcon}" value2="true">
					<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
				</ffi:cinclude>	
			</div>
<% } else { %>
			<div class="blockRow">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %></span>
				<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="BankName"/></span>
			</div>
			<div class="blockRow">
				<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %></span>
				<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
			</div>
<% } %>
</div>	
<%	counter++; %>
	</ffi:list>
	
</div>
<ffi:removeProperty name="InterMultipleCategories"/>
<!-- end block added here -->
<%--Even if there are no intermediary banks on screen still we need to show the labels from pending table. --%>
<div class="blockWrapper">
				<ffi:list collection="${payeeTask}.MasterPayee.IntermediaryBanks" items="Bank1">
					<ffi:cinclude value1="${Bank1.Action}" value2="<%=WireDefines.WIRE_ACTION_DEL %>">
<div class="blockContent">
					<% if (showInternational) { %>
						<div class="blockRow">
							<div class="inlineBlock" style="width: 50%">
								<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %>: </span>
								<span><ffi:getProperty name="Bank1" property="BankName"/></span>
							</div>
							<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %>: </span>
								<span><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
							</div>
						</div>
						<div class="blockRow">
							<div class="inlineBlock" style="width: 50%">
								<span class="sectionsubhead sectionLabel"><%= COLUMN_SWIFT %>: </span>
								<span><ffi:getProperty name="Bank1" property="RoutingSwift"/></span>
							</div>
							<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel"><%= COLUMN_CHIPS %>: </span>
								<span><ffi:getProperty name="Bank1" property="RoutingChips"/></span>
							</div>
						</div>
						<div class="blockRow">
							<div class="inlineBlock" style="width: 50%">
								<span class="sectionsubhead sectionLabel"><%= COLUMN_NATIONAL %>: </span>
								<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingOther"/></span>
							</div>
							<div class="inlineBlock">
								<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
								<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="IBAN"/></span>
							</div>
						</div>
						<div class="blockRow">
							<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
							<span>
								<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
							</span>
						</div>
						<% } else if(showFed) { %>
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %>: </span>
									<span><ffi:getProperty name="Bank1" property="BankName"/></span>
								</div>
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %>: </span>
									<span><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
								</div>
							</div>
							<div class="blockRow" style="width: 50%">
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
									<span class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="IBAN"/></span>
								</div>
								<div class="inlineBlock">
									<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
								</div>
							</div>
						<% } else { %>
							<div class="blockRow">
								<div class="inlineBlock" style="width: 50%">
									<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %>: </span>
									<span><ffi:getProperty name="Bank1" property="BankName"/></span>
								</div>
								<div class="inlineBlock">
									<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %>: </span>
									<span><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
								</div>
							</div>
						<% } %>
</div>
					</ffi:cinclude>
				</ffi:list>
</div>
		<%-- </ffi:cinclude>--%>
		<ffi:removeProperty name="DELETED_WIRE_TRANSFER_BANKS"/>
<!-- end block added here -->
	<%-- <table width="98%" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<% if (showInternational) { %>
			<td class="columndata" width="5%"><span class='sectionsubhead'>&nbsp;</span></td>
			<td class="columndata" width="30%"><span class='sectionsubhead'><%= COLUMN_INTERMEDIARY_BANK %></span></td>
			<td class="columndata" width="12%"><span class='sectionsubhead'><%= COLUMN_FED_ABA %></span></td>
			<td class="columndata" width="12%"><span class='sectionsubhead'><%= COLUMN_SWIFT %></span></td>
			<td class="columndata" width="11%"><span class='sectionsubhead'><%= COLUMN_CHIPS %></span></td>
			<td class="columndata" width="10%"><span class='sectionsubhead'><%= COLUMN_NATIONAL %></span></td>
			<td class="columndata" width="10%"><span class='sectionsubhead'><%= COLUMN_IBAN %></span></td>
			<td class="columndata" width="10%"><span class='<ffi:getProperty name="cssClass"/>'>
													<%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></span></td>
			<% } else if(showFed) { %>
			<td class="columndata" width="5%"><span class='sectionsubhead'>&nbsp;</span></td>
			<td class="columndata" width="35%"><span class='sectionsubhead'><%= COLUMN_INTERMEDIARY_BANK %></span></td>
			<td class="columndata" width="20%"><span class='sectionsubhead'><%= COLUMN_FED_ABA %></span></td>
			<td class="columndata" width="20%"><span class='sectionsubhead'><%= COLUMN_IBAN %></span></td>
			<td class="columndata" width="20%"><span class='<ffi:getProperty name="cssClass"/>'>
															<%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></span></td>
			<% } else { %>
			<td class="columndata" width="5%"><span class='sectionsubhead'>&nbsp;</span></td>
			<td class="columndata" width="45%"><span class='sectionsubhead'><%= COLUMN_INTERMEDIARY_BANK %></span></td>
			<td class="columndata" width="50%"><span class='sectionsubhead'><%= COLUMN_FED_ABA %></span></td>
			<% } %>
		</tr>
	
		<% int idx = 0; int counter = 0; %>
		<% String wireTaskName = ""; String errMessage = "";  %>
		<ffi:getProperty name="payeeTask" assignTo="wireTaskName" />
		<ffi:cinclude value1='<%=wireTaskName%>' value2="WireTransferPayee">
			<ffi:getProperty name="wireTask" assignTo="wireTaskName" />
		</ffi:cinclude>
		<ffi:getProperty name="<%=wireTaskName%>" property="ErrorMessage" assignTo="errMessage"/>
		
		<ffi:list collection="${payeeTask}.IntermediaryBanks" items="Bank1">	
		
			<ffi:setProperty name="displayCss" value="columndata"/>
			Highlight the intermediary banks only when displaying the page for the first time.			
			<ffi:cinclude value1="${IsPending}" value2="" operator="notEquals">
				Highlight the intermediary banks only when editing an existing beneficiary from pending island.
				<ffi:cinclude value1="${payeeTask}" value2="WireTransferPayee">
					<ffi:cinclude value1="${Bank1.action}" value2="<%=WireDefines.WIRE_ACTION_ADD %>">
						<ffi:setProperty name="displayCss" value="sectionheadDA"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Bank1.action}" value2="<%=WireDefines.WIRE_ACTION_MOD %>">
						
						We need to get the corresponding bank account no to display as a label. 
						<ffi:list collection="InterMultipleCategories" items="category">
							<ffi:list collection="category.daItems" items="DaItemDetail">
								<ffi:cinclude value1="${DaItemDetail.fieldName}" value2="ID" >
									<ffi:cinclude value1="${DaItemDetail.newValue}" value2="${Bank1.ID}" >
										<ffi:setProperty name="BankFound" value="true"/>
									</ffi:cinclude>
								</ffi:cinclude>
								<ffi:cinclude value1="${BankFound}" value2="true" >
									<ffi:list collection="category.daItems" items="detail">
										<ffi:cinclude value1="${detail.fieldName}" value2="correspondentBankAccountNumber" >
											<ffi:setProperty name="OldCorrespondentBankAccNo" value="${detail.oldValue}"/>
										</ffi:cinclude>
									</ffi:list>
								</ffi:cinclude>
							</ffi:list>
						</ffi:list>
						<ffi:removeProperty name="BankFound"/>
					</ffi:cinclude>
				</ffi:cinclude>
			</ffi:cinclude>
			<tr>
				<td class="columndata">
				<ffi:cinclude value1="${Bank1.DeleteIcon}" value2="true" operator="notEquals">
					<ffi:cinclude value1="${disableEdit}" value2="disabled" operator="notEquals">
						<a href="javascript:deleteInterBank('<ffi:getProperty name="CallerURL"/>','<ffi:getProperty name="Bank1" property="ID"/>');"><img src="/cb/web/multilang/grafx/payments/i_trash.gif" alt="Delete" height="14" width="14" border="0" hspace="3"></a>
					</ffi:cinclude>
				</ffi:cinclude>		
				</td>
				<td class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</td>
				<td class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</td>
				<% if (showInternational) { %>
				<td class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</td>
				<td class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</td>
				<td class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</td>
				<% } %>
				<% if (showInternational || showFed) { %>
				<td class="<ffi:getProperty name='displayCss'/>"><ffi:getProperty name="Bank1" property="IBAN"/>
				</td>
				<td class="<ffi:getProperty name='displayCss'/>">
				<% if(counter == 0) { %>
				&nbsp;
				<% } else { %>
					<ffi:cinclude value1="${Bank1.DeleteIcon}" value2="true" operator="notEquals">
						<input id="CurrentWireIntermediaryBank<%=counter%>"  name="<ffi:getProperty name="payeeTask"/>.CurrentWireIntermediaryBank=<ffi:getProperty name="Bank1" property="ID"/>&<ffi:getProperty name="payeeTask"/>.CorrespondentBankAccountNumber" value="<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="25" style="width:100" <ffi:getProperty name="disableEdit"/>>
						<span class="sectionhead_greyDA"><ffi:getProperty name="OldCorrespondentBankAccNo"/></span>
						<ffi:removeProperty name="OldCorrespondentBankAccNo"/>
					</ffi:cinclude>
					<ffi:cinclude value1="${Bank1.DeleteIcon}" value2="true">
						<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>
					</ffi:cinclude>				
				<% } %>
				&nbsp;</td>
				<% }
				%>		
		    </tr>
		<%	counter++; %>
	</ffi:list>
</table> --%>
</ffi:cinclude>
</div>



<%-- End: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
<% if (intBanks != 0) { %>

<% int idx = 0; int counter = 0; %>
		<% String wireTaskName = ""; String errMessage = "";  %>
		<ffi:getProperty name="payeeTask" assignTo="wireTaskName" />
		<ffi:cinclude value1='<%=wireTaskName%>' value2="WireTransferPayee">
			<ffi:getProperty name="wireTask" assignTo="wireTaskName" />
		</ffi:cinclude>
		<ffi:getProperty name="<%=wireTaskName%>" property="ErrorMessage" assignTo="errMessage"/>
		
		<ffi:list collection="${payeeTask}.IntermediaryBanks" items="Bank1">	
<div  class="blockHead" style="font-size:22px; font-weight:normal">Intermediary Bank Details  
<ffi:cinclude value1="${disableEdit}" value2="disabled" operator="notEquals">
	<a class="" title='Delete' href='#' onClick="deleteInterBank('<ffi:getProperty name="CallerURL"/>','<ffi:getProperty name="Bank1" property="ID"/>')"><span class="sapUiIconCls icon-delete" style="font-size:16px;"></span></a>
</ffi:cinclude>
</div>		
<% if (showInternational) { %>
<div class="blockContent">

<div class="blockRow">
	<div class="inlineBlock" style="width: 50%">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %>: </span>
		<span><ffi:getProperty name="Bank1" property="BankName"/></span>
	</div>
	<div class="inlineBlock">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %>: </span>
		<span><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
	</div>
</div>	
<div class="blockRow">
	<div class="inlineBlock" style="width: 50%">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_SWIFT %>: </span>
		<span><ffi:getProperty name="Bank1" property="RoutingSwift"/></span>
	</div>
	<div class="inlineBlock">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_CHIPS %>: </span>
		<span><ffi:getProperty name="Bank1" property="RoutingChips"/></span>
	</div>
</div>	
<div class="blockRow">
	<div class="inlineBlock" style="width: 50%">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_NATIONAL %>: </span>
		<span><ffi:getProperty name="Bank1" property="RoutingOther"/></span>
	</div>
	<div class="inlineBlock">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %>: </span>
		<span><ffi:getProperty name="Bank1" property="IBAN"/></span>
	</div>
</div>	
<div class="blockRow">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %>: </span>
		<span>
			<% if(counter == 0) { %>
			<% } else { %>
				<input id="CurrentWireIntermediaryBank<%=counter%>"  name="<ffi:getProperty name="payeeTask"/>.CurrentWireIntermediaryBank=<ffi:getProperty name="Bank1" property="ID"/>&<ffi:getProperty name="payeeTask"/>.CorrespondentBankAccountNumber" value="<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="25" style="width:100" <ffi:getProperty name="disableEdit"/>>
			<% } %>
		</span>
</div>
<ffi:cinclude value1='<%=errMessage%>' value2="error" operator="notEquals">
			<!-- Validation Error occurred -->
			<% String currentFieldName = "IntermediaryBankIBAN"+counter; %>
			
			<ffi:cinclude value1="<%=wireTaskName%>" value2="EditWireTransferPayee">
			<ffi:setProperty name="EditWireTransferPayee" property="CurrentFieldName" value="<%=currentFieldName%>" />
				<ffi:cinclude value1="${EditWireTransferPayee.HasValidationErrors}" value2="true">
					<ffi:cinclude value1="${EditWireTransferPayee.CurrentFieldHasValidationErrors}" value2="true">
						<ffi:list collection="EditWireTransferPayee.CurrentFieldValidationErrors" items="validationError">
							<div class="blockRow columndata_error columndata_bold">
									<!--L10NStart--><ffi:getProperty name="validationError" property="message"/><!--L10NEnd-->
									<ffi:setProperty name="<%=wireTaskName%>" property="ErrorMessage" value="error" />
							</div>
						</ffi:list>
					</ffi:cinclude>
				<ffi:setProperty name="EditWireTransferPayee" property="CurrentFieldName" value="" />
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="<%=wireTaskName%>" value2="AddWireTransferPayee">
				<ffi:setProperty name="AddWireTransferPayee" property="CurrentFieldName" value="<%=currentFieldName%>" />
				<ffi:cinclude value1="${AddWireTransferPayee.HasValidationErrors}" value2="true">
					<ffi:cinclude value1="${AddWireTransferPayee.CurrentFieldHasValidationErrors}" value2="true">
					<ffi:list collection="AddWireTransferPayee.CurrentFieldValidationErrors" items="validationError">
						<div class="blockRow columndata_error columndata_bold">
								<!--L10NStart--><ffi:getProperty name="validationError" property="message"/><!--L10NEnd-->
									<ffi:setProperty name="<%=wireTaskName%>" property="ErrorMessage" value="error" />
						</div>
					</ffi:list>
					</ffi:cinclude>
				<ffi:setProperty name="AddWireTransferPayee" property="CurrentFieldName" value="" />
				</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="<%=wireTaskName%>" value2="ModifyWireTransfer">
					<ffi:setProperty name="ModifyWireTransfer" property="CurrentFieldName" value="<%=currentFieldName%>" />
					<ffi:cinclude value1="${ModifyWireTransfer.HasValidationErrors}" value2="true">
						<ffi:cinclude value1="${ModifyWireTransfer.CurrentFieldHasValidationErrors}" value2="true">
						<ffi:list collection="ModifyWireTransfer.CurrentFieldValidationErrors" items="validationError">
							<div class="blockRow columndata_error columndata_bold">
									<!--L10NStart--><ffi:getProperty name="validationError" property="message"/><!--L10NEnd-->
									<ffi:setProperty name="<%=wireTaskName%>" property="ErrorMessage" value="error" />
							</div>
						</ffi:list>
						</ffi:cinclude>
					<ffi:setProperty name="ModifyWireTransfer" property="CurrentFieldName" value="" />
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="<%=wireTaskName%>" value2="AddWireTransfer">abc
					<ffi:setProperty name="AddWireTransfer" property="CurrentFieldName" value="<%=currentFieldName%>" />
					<ffi:cinclude value1="${AddWireTransfer.HasValidationErrors}" value2="true">
						<ffi:cinclude value1="${AddWireTransfer.CurrentFieldHasValidationErrors}" value2="true">
						<ffi:list collection="AddWireTransfer.CurrentFieldValidationErrors" items="validationError">
							<div class="blockRow columndata_error columndata_bold">
									<!--L10NStart--><ffi:getProperty name="validationError" property="message"/><!--L10NEnd-->
									<ffi:setProperty name="<%=wireTaskName%>" property="ErrorMessage" value="error" />
							</div>
						</ffi:list>
						</ffi:cinclude>
					<ffi:setProperty name="AddWireTransfer" property="CurrentFieldName" value="" />
					</ffi:cinclude>
				</ffi:cinclude>			
	   </ffi:cinclude>
</div>
<% } else if(showFed) { %>
	<div class="blockRow">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %></span>
		<span><ffi:getProperty name="Bank1" property="BankName"/></span>
	</div>	
	<div class="blockRow">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %></span>
		<span><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
	</div>
	<div class="blockRow">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_IBAN %></span>
		<span><ffi:getProperty name="Bank1" property="IBAN"/></span>
	</div>
<% } else { %>
	<div class="blockRow">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_INTERMEDIARY_BANK %></span>
		<span><ffi:getProperty name="Bank1" property="BankName"/></span>
	</div>
	<div class="blockRow">
		<span class="sectionsubhead sectionLabel"><%= COLUMN_FED_ABA %></span>
		<span><ffi:getProperty name="Bank1" property="RoutingFedWire"/></span>
	</div>
<% } %>
<%	counter++; %>
</ffi:list>

	<table width="98%" border="0" cellspacing="0" cellpadding="3">
		<%-- <tr>
			<% if (showInternational) { %>
			<td class="columndata" width="5%"><span class="sectionsubhead">&nbsp;</span></td>
			<td class="columndata" width="30%"><span class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></span></td>
			<td class="columndata" width="12%"><span class="sectionsubhead"><%= COLUMN_FED_ABA %></span></td>
			<td class="columndata" width="12%"><span class="sectionsubhead"><%= COLUMN_SWIFT %></span></td>
			<td class="columndata" width="11%"><span class="sectionsubhead"><%= COLUMN_CHIPS %></span></td>
			<td class="columndata" width="10%"><span class="sectionsubhead"><%= COLUMN_NATIONAL %></span></td>
			<td class="columndata" width="10%"><span class="sectionsubhead"><%= COLUMN_IBAN %></span></td>
			<td class="columndata" width="10%">
			<span class="sectionsubhead">
			<%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></span></td>
			<% } else if(showFed) { %>
			<td class="columndata" width="5%"><span class="sectionsubhead">&nbsp;</span></td>
			<td class="columndata" width="35%"><span class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></span></td>
			<td class="columndata" width="20%"><span class="sectionsubhead"><%= COLUMN_FED_ABA %></span></td>
			<td class="columndata" width="20%"><span class="sectionsubhead"><%= COLUMN_IBAN %></span></td>
			<td class="columndata" width="20%">
			<span class="sectionsubhead">
			<%= COLUMN_CORRESPONDENT_BANK_ACCOUNT_NUMBER %></span></td>
			<% } else { %>
			<td class="columndata" width="5%"><span class="sectionsubhead">&nbsp;</span></td>
			<td class="columndata" width="45%"><span class="sectionsubhead"><%= COLUMN_INTERMEDIARY_BANK %></span></td>
			<td class="columndata" width="50%"><span class="sectionsubhead"><%= COLUMN_FED_ABA %></span></td>
			<% } %>
		</tr> --%>
	
		<%-- <% int idx = 0; int counter = 0; %>
		<% String wireTaskName = ""; String errMessage = "";  %>
		<ffi:getProperty name="payeeTask" assignTo="wireTaskName" />
		<ffi:cinclude value1='<%=wireTaskName%>' value2="WireTransferPayee">
			<ffi:getProperty name="wireTask" assignTo="wireTaskName" />
		</ffi:cinclude>
		<ffi:getProperty name="<%=wireTaskName%>" property="ErrorMessage" assignTo="errMessage"/> --%>
		
<%-- 		<ffi:list collection="${payeeTask}.IntermediaryBanks" items="Bank1">	
			<tr>
			<td class="columndata">
			<ffi:cinclude value1="${disableEdit}" value2="disabled" operator="notEquals">
				<a class='ui-button' title='Delete' href='#' onClick="deleteInterBank('<ffi:getProperty name="CallerURL"/>','<ffi:getProperty name="Bank1" property="ID"/>')"><span class='ui-icon ui-icon-trash'></span></a>
			</ffi:cinclude>
			</td>
			<td class="columndata"><ffi:getProperty name="Bank1" property="BankName"/>&nbsp;</td>
			<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingFedWire"/>&nbsp;</td>
			<% if (showInternational) { %>
			<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingSwift"/>&nbsp;</td>
			<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingChips"/>&nbsp;</td>
			<td class="columndata"><ffi:getProperty name="Bank1" property="RoutingOther"/>&nbsp;</td>
			<% } %>
			<% if (showInternational || showFed) { %>
			<td class="columndata"><ffi:getProperty name="Bank1" property="IBAN"/>
			</td>
			<td class="columndata">
			<% if(counter == 0) { %>
			&nbsp;
			<% } else { %>
				<input id="CurrentWireIntermediaryBank<%=counter%>"  name="<ffi:getProperty name="payeeTask"/>.CurrentWireIntermediaryBank=<ffi:getProperty name="Bank1" property="ID"/>&<ffi:getProperty name="payeeTask"/>.CorrespondentBankAccountNumber" value="<ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="25" style="width:100" <ffi:getProperty name="disableEdit"/>>
			<% } %>
			&nbsp;</td>
			<% }
			%>		
	    </tr>
	
		<ffi:cinclude value1='<%=errMessage%>' value2="error" operator="notEquals">
			Validation Error occurred
			<% String currentFieldName = "IntermediaryBankIBAN"+counter; %>
			
			<ffi:cinclude value1="<%=wireTaskName%>" value2="EditWireTransferPayee">
			<ffi:setProperty name="EditWireTransferPayee" property="CurrentFieldName" value="<%=currentFieldName%>" />
				<ffi:cinclude value1="${EditWireTransferPayee.HasValidationErrors}" value2="true">
					<ffi:cinclude value1="${EditWireTransferPayee.CurrentFieldHasValidationErrors}" value2="true">
						<ffi:list collection="EditWireTransferPayee.CurrentFieldValidationErrors" items="validationError">
							<tr>
								<td class="columndata" colspan="6">&nbsp;</td>
								<td class="columndata_error columndata_bold" colspan="2" >
									<!--L10NStart--><ffi:getProperty name="validationError" property="message"/><!--L10NEnd-->
									<ffi:setProperty name="<%=wireTaskName%>" property="ErrorMessage" value="error" />
								</td>
							 </tr>
						</ffi:list>
					</ffi:cinclude>
				<ffi:setProperty name="EditWireTransferPayee" property="CurrentFieldName" value="" />
				</ffi:cinclude>
			</ffi:cinclude>
			<ffi:cinclude value1="<%=wireTaskName%>" value2="AddWireTransferPayee">
				<ffi:setProperty name="AddWireTransferPayee" property="CurrentFieldName" value="<%=currentFieldName%>" />
				<ffi:cinclude value1="${AddWireTransferPayee.HasValidationErrors}" value2="true">
					<ffi:cinclude value1="${AddWireTransferPayee.CurrentFieldHasValidationErrors}" value2="true">
					<ffi:list collection="AddWireTransferPayee.CurrentFieldValidationErrors" items="validationError">
						<tr>
							<td class="columndata" colspan="6">&nbsp;</td>
							<td class="columndata_error columndata_bold" colspan="2" >
								<!--L10NStart--><ffi:getProperty name="validationError" property="message"/><!--L10NEnd-->
									<ffi:setProperty name="<%=wireTaskName%>" property="ErrorMessage" value="error" />
							</td>
						 </tr>
					</ffi:list>
					</ffi:cinclude>
				<ffi:setProperty name="AddWireTransferPayee" property="CurrentFieldName" value="" />
				</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="<%=wireTaskName%>" value2="ModifyWireTransfer">
					<ffi:setProperty name="ModifyWireTransfer" property="CurrentFieldName" value="<%=currentFieldName%>" />
					<ffi:cinclude value1="${ModifyWireTransfer.HasValidationErrors}" value2="true">
						<ffi:cinclude value1="${ModifyWireTransfer.CurrentFieldHasValidationErrors}" value2="true">
						<ffi:list collection="ModifyWireTransfer.CurrentFieldValidationErrors" items="validationError">
							<tr>
								<td class="columndata" colspan="6">&nbsp;</td>
								<td class="columndata_error columndata_bold" colspan="2" >
									<!--L10NStart--><ffi:getProperty name="validationError" property="message"/><!--L10NEnd-->
									<ffi:setProperty name="<%=wireTaskName%>" property="ErrorMessage" value="error" />
								</td>
							 </tr>
						</ffi:list>
						</ffi:cinclude>
					<ffi:setProperty name="ModifyWireTransfer" property="CurrentFieldName" value="" />
					</ffi:cinclude>
				</ffi:cinclude>
				<ffi:cinclude value1="<%=wireTaskName%>" value2="AddWireTransfer">abc
					<ffi:setProperty name="AddWireTransfer" property="CurrentFieldName" value="<%=currentFieldName%>" />
					<ffi:cinclude value1="${AddWireTransfer.HasValidationErrors}" value2="true">
						<ffi:cinclude value1="${AddWireTransfer.CurrentFieldHasValidationErrors}" value2="true">
						<ffi:list collection="AddWireTransfer.CurrentFieldValidationErrors" items="validationError">
							<tr>
								<td class="columndata" colspan="6">&nbsp;</td>
								<td class="columndata_error columndata_bold" colspan="2" >
									<!--L10NStart--><ffi:getProperty name="validationError" property="message"/><!--L10NEnd-->
									<ffi:setProperty name="<%=wireTaskName%>" property="ErrorMessage" value="error" />
								</td>
							 </tr>
						</ffi:list>
						</ffi:cinclude>
					<ffi:setProperty name="AddWireTransfer" property="CurrentFieldName" value="" />
					</ffi:cinclude>
				</ffi:cinclude>			
	   </ffi:cinclude>
		<%	counter++; %>
	</ffi:list> --%>
	</table>
	<% } %>
</ffi:cinclude>




<%--Even if there are no intermediary banks on screen still we need to show the labels from pending table. --%>
<%-- <ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<table width="98%" border="0" cellspacing="0" cellpadding="3">
				<ffi:list collection="${payeeTask}.MasterPayee.IntermediaryBanks" items="Bank1">
					<ffi:cinclude value1="${Bank1.Action}" value2="<%=WireDefines.WIRE_ACTION_DEL %>">
					<tr>
					<% if (showInternational) { %>
						<td class="columndata" width="5%">&nbsp;</td>
						<td class="columndata" width="30%"><ffi:getProperty name="Bank1" property="BankName"/></td>
						<td class="columndata" width="12%"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></td>
						<td class="columndata" width="12%"><ffi:getProperty name="Bank1" property="RoutingSwift"/></td>
						<td class="columndata" width="11%"><ffi:getProperty name="Bank1" property="RoutingChips"/></td>
						<td class="columndata" width="10%"><ffi:getProperty name="Bank1" property="RoutingOther"/></td>
						<td class="columndata" width="10%"><ffi:getProperty name="Bank1" property="IBAN"/></td>
						<td class="columndata" width="10%"><ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/></td>
						<% } else if(showFed) { %>
						<td class="columndata" width="5%">&nbsp;</td>
						<td class="columndata" width="35%"><ffi:getProperty name="Bank1" property="BankName"/></td>
						<td class="columndata" width="20%"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></td>
						<td class="columndata" width="20%"><ffi:getProperty name="Bank1" property="IBAN"/></td>
						<td class="columndata" width="20%"><ffi:getProperty name="Bank1" property="CorrespondentBankAccountNumber"/></td>
						<% } else { %>
						<td class="columndata" width="5%">&nbsp;</td>
						<td class="columndata" width="45%"><ffi:getProperty name="Bank1" property="BankName"/></td>
						<td class="columndata" width="50%"><ffi:getProperty name="Bank1" property="RoutingFedWire"/></td>
						<% } %>
					</tr>
					</ffi:cinclude>
				</ffi:list>
			</table>
			<br/>
		<ffi:removeProperty name="DELETED_WIRE_TRANSFER_BANKS"/>
</ffi:cinclude> --%>

<div class="columndata marginTop10">
	<span class="required">** If the beneficiary bank does not have a routing number, at least one intermediary bank with routing number is required</span>
</div>
<ffi:removeProperty name="displayCss"/>
