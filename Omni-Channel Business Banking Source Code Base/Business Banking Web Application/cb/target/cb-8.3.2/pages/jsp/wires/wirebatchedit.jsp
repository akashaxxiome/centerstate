<%--
This file is a wrapper that will include the appropriate edit wire batch page
based on the type of batch the user selected.

Pages that request this page
----------------------------
wire_transfers_list.jsp (Included in wiretransfers.jsp, the wire transfer summary page)
	Edit button next to a wire batch.

Pages this page requests
------------------------
See included files

Pages included in this page
---------------------------
payments/inc/wire_set_wire.jsp
	Sets a wire transfer or wire batch with a given ID into the session
payments/wirebatchedithost.jsp (included only if the batch type is Host)
	The edit host wire batch page
payments/wirebatchedittemplate.jsp (included only if the batch type is Template)
	The edit template wire batch page
payments/wirebatcheditff.jsp (included only if the batch type is Freeform)
	The edit freeform wire batch page
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireBatch,
				 com.ffusion.beans.wiretransfers.WireTransfers"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatchedit" className="moduleHelpClass"/>
<%
if( request.getParameter("ID") != null ){ session.setAttribute("ID", request.getParameter("ID")); }
if( request.getParameter("collectionName") != null ){ session.setAttribute("collectionName", request.getParameter("collectionName")); }
%>
<%-- Set the wire batch into the session 
<s:include value="%{#session.PagesPath}/wires/inc/wire_set_wire.jsp"/>
--%>
<%-- following line to make sure Confirm page works right for Wire Batch HOST 
<ffi:removeProperty name="AddWireBatch"/>
--%>


<ffi:cinclude value1="${wireBatchObject.BatchType}" value2="<%=com.ffusion.beans.wiretransfers.WireDefines.WIRE_BATCH_TYPE_HOST%>" operator="equals">
	<s:include value="%{#session.PagesPath}/wires/wirebatchedithost.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireBatchObject.BatchType}" value2="<%=com.ffusion.beans.wiretransfers.WireDefines.WIRE_BATCH_TYPE_TEMPLATE%>" operator="equals">
	<s:include value="%{#session.PagesPath}/wires/wirebatchedittemplate.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireBatchObject.BatchType}" value2="<%=com.ffusion.beans.wiretransfers.WireDefines.WIRE_BATCH_TYPE_FREEFORM%>" operator="equals">
	<s:include value="%{#session.PagesPath}/wires/wirebatcheditff.jsp"/>
</ffi:cinclude>
