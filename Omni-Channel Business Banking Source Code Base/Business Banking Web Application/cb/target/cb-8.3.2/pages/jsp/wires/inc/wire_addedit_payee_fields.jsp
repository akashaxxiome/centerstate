<%--
This file contains the wire beneficiary name and address fields.

It is included on all add/edit wire transfer and add/edit wire beneficiary
pages
    wireaddpayee.jsp
    wireaddpayeebook.jsp
    wireaddpayeedrawdown.jsp
    wirebook.jsp
    wiredrawdown.jsp
    wirefed.jsp
    wiretransfernew.jsp

It includes the following file:
common/wire_labels.jsp
    The labels for fields and buttons
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ include file="../../common/wire_labels.jsp"%>
<ffi:cinclude value1="${disableEdit}" value2="disabled" operator="notEquals">
<style>
/* input#beneficiaryAccountNo, input#beneficiaryName, input#beneficiaryNickName, input#beneficiaryAddress1, input#beneficiaryAddress2, input#beneficiaryCity, input#beneficiaryContact{width:23.5em} */
</style>
<script>
$(function(){
/* 	$("#selectStateProvinceID").selectmenu({width:'24em'});
	$("#selectBeneficiaryCountryID").selectmenu({width:'24em'});
	$("#selectBeneficiaryScopeID").selectmenu({width:'13em'});
	$("#selectAccountTypeID").selectmenu({width:'24em'});
	$("#selectPayeeDestinationID").selectmenu({width:'13em'});

	$("#selectBeneficiaryCountryID_autoComplete").css("width", "22em"); */
});
</script>
</ffi:cinclude>
<%-- <ffi:cinclude value1="${disableEdit}" value2="disabled"> --%>
<script>
$(function(){
	$("#selectStateProvinceID").selectmenu({width:'13em'});
	$("#selectBeneficiaryCountryID").selectmenu({width:'13em'});
	$("#selectBeneficiaryScopeID").selectmenu({width:'13em'});
	$("#selectAccountTypeID").selectmenu({width:'13em'});
	$("#selectPayeeDestinationID").selectmenu({width:'13em'});

	$("#selectBeneficiaryCountryID_autoComplete").css("width", "13.6em");
});
</script>
<%-- </ffi:cinclude> --%>
<% String payeeDest = ""; String wireDest = ""; boolean showInternational = false; %>
<ffi:getProperty name="destination" assignTo="payeeDest"/>


<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	 
	<ffi:setProperty name="WireBeneficiaryCategory" value="<%=IDualApprovalConstants.CATEGORY_WIRE_PAYEE%>"/>
	
	<%--When user clicks Add Beneficiary button then ID is null as user is going to create a new record.
	When user clicks Edit button for newly added beneficiary from pending island then ID is not null and 
	payee task is still AddWireTransferPayee.  --%>
	
	<ffi:cinclude value1="${ID}" value2="" operator="notEquals">
	<ffi:cinclude value1="${IS_DA_WIRE_TRANSFER}" value2="true" operator="notEquals">
		<ffi:setProperty name="${payeeTask}" property="ID" value="${ID}"/>
	</ffi:cinclude>
	</ffi:cinclude>
	<ffi:cinclude value1="${validateAction}" value2="addWireBeneficiaryAction_verify" operator="notEquals">
		<s:set var="disablePayeeType" value="'disabled'" />
	</ffi:cinclude>
	<s:if test="%{WireTransferPayee.action == 'Added'}">
		<s:set var="disablePayeeType" value="''" />
	</s:if>
	<ffi:cinclude value1="${payeeTask}" value2="AddWireTransferPayee">
		<ffi:removeProperty name="<%=IDualApprovalConstants.CATEGORY_BEAN%>"/>
		<ffi:removeProperty name="<%=IDualApprovalConstants.OLD_DA_OBJECT%>"/>
	</ffi:cinclude>
	<ffi:cinclude value1="${payeeDest}" value2="<%=WireDefines.PAYEE_TYPE_DRAWDOWN%>">
		<ffi:setProperty name="${payeeTask}" property="IntermediaryBanks" value=""/>
	</ffi:cinclude>
	<ffi:cinclude value1="${payeeDest}" value2="<%=WireDefines.PAYEE_TYPE_BOOK%>">
		<ffi:setProperty name="${payeeTask}" property="IntermediaryBanks" value=""/>
	</ffi:cinclude>	
</ffi:cinclude>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<ffi:removeProperty name="disablePayeeType"/>
</ffi:cinclude>
<%-- End: Dual approval processing--%>

<%--
Show the international fields if:
    A) This is included in add/edit wire transfer, and the current wire destination is Domestic or International, or
    B) This is included in add/edit wire payee, and the payee destination is Regular.
    NOTE:  payeeTask == WireTransferPayee if this is being included in add/edit wire transfer.
--%>
<%
    	if (payeeDest.equals(WireDefines.WIRE_DOMESTIC)
    				|| payeeDest.equals(WireDefines.WIRE_INTERNATIONAL) || WireDefines.PAYEE_TYPE_REGULAR.equals(payeeDest))
    			showInternational = true;
%>
<%--
The following processing retrieves a list of valid States based
on the Country chosen. This was done for localiztion.
--%>
<%
	String selectedDBBLCountry = "";
	String selectedDBISOCountry = "";
%>
<ffi:getProperty name="WireTransferPayee" property="Country" assignTo="selectedDBBLCountry"/>
<ffi:cinclude value1="${selectedDBBLCountry}" value2="" operator="equals">
    <ffi:setProperty name="selectedDBBLCountry" value="<%= com.ffusion.banklookup.beans.FinancialInstitution.PREFERRED_COUNTRY %>"/>
</ffi:cinclude>
<table cellpadding="3" cellspacing="0" border="0" style="margin:10px 0 0" class="tableData" width="100%">
   <%--  <tr>
        <td class="sectionhead" colspan="5" style="font-size:16px"><%= WireDefines.PAYEE_TYPE_DRAWDOWN.equals(payeeDest) ? "<!--L10NStart-->Debit Account<!--L10NEnd-->" : "<!--L10NStart-->Beneficiary<!--L10NEnd-->" %> <!--L10NStart-->Info<!--L10NEnd--></td>
    </tr> --%>
    
        <%-- Determine if the scope needs to be disabled.  It should be disabled if disableSemi=="disabled", or
         if we're editing a payee (payeeTask=="EditWireTransferPayee"), and the payeeScope=="BUSINESS".  --%>
    <ffi:setProperty name="disablePayeeScope" value="${disableSemi}"/>
    <%
    	String curScope = "";
    %>
    <ffi:getProperty name="${payeeTask}" property="PayeeScope" assignTo="curScope"/>
      <ffi:cinclude value1="${validateAction}" value2="modifyWirebeneficiary_verify.action">
        <ffi:cinclude value1="<%= curScope %>" value2="BUSINESS" operator="equals">
        <ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_ADDED %>" operator="notEquals">
            <ffi:setProperty name="disablePayeeScope" value="disabled"/>
        </ffi:cinclude>
        </ffi:cinclude>
    </ffi:cinclude>
    <ffi:setProperty name="showPayeeScopeBiz" value="false"/>
    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BENEFICIARY_MANAGEMENT %>">
        <ffi:setProperty name="showPayeeScopeBiz" value="true"/>
    </ffi:cinclude>
    <ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BENEFICIARY_MANAGEMENT %>">
        <ffi:cinclude value1="<%= curScope %>" value2="<%= WireDefines.PAYEE_SCOPE_BUSINESS %>" operator="equals">
            <ffi:setProperty name="showPayeeScopeBiz" value="true"/>
        </ffi:cinclude>
    </ffi:cinclude>
    
    <tr>
    	<td width="25%" id="beneficiaryNameLabel"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.PayeeName}" oldFieldValue="${WireTransferPayee.MasterPayee.PayeeName}" 
        	daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=WireDefines.PAYEE_TYPE_DRAWDOWN.equals(payeeDest) ? LABEL_DRAWDOWN_ACCOUNT_NAME
			: LABEL_BENEFICIARY_NAME%></span><span class="required">*</span>
		</td>
		<s:if test="%{!IsConsumerUser}">
		<td width="25%" id="beneficiaryScopeLabel"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.PayeeScope}" oldFieldValue="${WireTransferPayee.MasterPayee.PayeeScope}" 
	        daUserAction="${WireTransferPayeeDA.Action}" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>'><%=LABEL_BENEFICIARY_SCOPE%></span><span class="required">*</span></td>
		</s:if>
        <td id="beneficiaryAccountNoLabel" width="25%">
			<span
			class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.AccountNum}" oldFieldValue="${WireTransferPayee.MasterPayee.AccountNum}" daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=LABEL_ACCOUNT_NUMBER%></span>
			<span class="required">*</span>
		</td>
		<td id="beneficiaryAccountTypeLabel" width="25%"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.AccountType}" oldFieldValue="${WireTransferPayee.MasterPayee.AccountType}" 
        	daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA"  defaultcss="sectionsubhead"/>'><%=LABEL_ACCOUNT_TYPE%></span><span class="required">*</span>
        </td>
    </tr>
    <tr>
		<td>
        	<input id="beneficiaryName" name="WireTransferPayee.PayeeName" value="<ffi:getProperty name="WireTransferPayee" property="PayeeName"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="128" size="20" <ffi:getProperty name="disableEdit"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.PayeeName}" value2="${WireTransferPayee.MasterPayee.PayeeName}" operator="notEquals">
         		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.PayeeName" /></span>
         	</ffi:cinclude>
			</ffi:cinclude>
         	
        </td>
        <s:if test="%{!IsConsumerUser}">
        <td>
        	<%-- Start: Dual approval processing --%>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				<%-- If it is wire transfer / wire template transfer with DA then don't show user and business scope --%>
				<ffi:cinclude value1="${IS_DA_WIRE_TRANSFER}" value2="true">
					<ffi:cinclude value1="${currentPayeeId}" value2="Create New Beneficiary">
						<% curScope = WireDefines.PAYEE_SCOPE_UNMANAGED; %>
					</ffi:cinclude>
					<ffi:setProperty name="disablePayeeScope" value="disabled"/>
				</ffi:cinclude>
				<ffi:removeProperty name="IS_DA_WIRE_TRANSFER"/>				
			</ffi:cinclude>
			<%-- End: Dual approval processing--%>
	
			<%-- execute javascript onchange event only when adding/editing a wire (payeeTask == WireTransferPayee) --%>
			<div id="selectBeneficiaryScopeIDforDisplay" class="sectionsubhead">
		
			<select id="selectBeneficiaryScopeID" name="<ffi:getProperty name="payeeTask"/>.PayeeScope" class="txtbox" <ffi:getProperty name="disablePayeeScope"/> <s:if test="%{!isBeneficiary}">  onFocus="saveSelect(this.selectedIndex)" onchange="setScope(this.options[this.selectedIndex].value, true)"</s:if>>
				<ffi:removeProperty name="disablePayeeScope"/>
				<% if (curScope == null) curScope = ""; %>
				<%-- Start: Dual approval processing --%>
							
					<option value="<%= WireDefines.PAYEE_SCOPE_USER %>" <%= curScope.equalsIgnoreCase(WireDefines.PAYEE_SCOPE_USER) ? "selected" : "" %>><ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_USER %>"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></option>
				
				<%-- End: Dual approval processing--%>

				<%-- show business option only when entitled to Wire Beneficiary Management --%>
				<ffi:cinclude value1="${showPayeeScopeBiz}" value2="true" operator="equals">
				
				<option value="<%= WireDefines.PAYEE_SCOPE_BUSINESS %>" <%= curScope.equalsIgnoreCase(WireDefines.PAYEE_SCOPE_BUSINESS) ? "selected" : "" %>><ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_BUSINESS %>"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></option>
				
				</ffi:cinclude>
				<ffi:removeProperty name="showPayeeScopeBiz"/>

				<%-- show unmanaged payee option only when adding/editing a wire (payeeTask == WireTransferPayee) --%>
				<s:if test="%{!isBeneficiary}">
					<option value="<%= WireDefines.PAYEE_SCOPE_UNMANAGED %>" <%= curScope.equalsIgnoreCase(WireDefines.PAYEE_SCOPE_UNMANAGED) ? "selected" : "" %>><ffi:setProperty name="WirePayeeScopes" property="Key" value="<%= WireDefines.PAYEE_SCOPE_UNMANAGED %>"/><ffi:getProperty name="WirePayeeScopes" property="Value"/></option>
				</s:if>
       	 	</select>
			
       	 	<ffi:cinclude value1="${WireTransferPayee.PayeeScope}" value2="${WireTransferPayee.MasterPayee.PayeeScope}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.PayeeScope" /></span>
        	</ffi:cinclude>
       	 	<script>
       	 	$(document).ready(function(){
       	 		ns.wire.origSelect = $("#selectBeneficiaryScopeID").val();       	 		
   	 		});</script>
       	 	</div>   
					
        </td>
		</s:if>
			<s:else>
				<ffi:cinclude value1="${WireTransferPayee.PayeeScope}" value2="<%= WireDefines.PAYEE_SCOPE_UNMANAGED %>" operator="equals">
					<input id="selectBeneficiaryScopeID" name="<ffi:getProperty name="payeeTask"/>.PayeeScope" value="<ffi:getProperty name="WireTransferPayee" property="PayeeScope" />" type="hidden"  maxlength="128" size="20">
				</ffi:cinclude>
				<ffi:cinclude value1="${WireTransferPayee.PayeeScope}" value2="<%= WireDefines.PAYEE_SCOPE_UNMANAGED %>" operator="notEquals">
					<input id="selectBeneficiaryScopeID" name="<ffi:getProperty name="payeeTask"/>.PayeeScope" value="<%= WireDefines.PAYEE_SCOPE_USER %>" type="hidden"  maxlength="128" size="20">
				</ffi:cinclude>
				<script>
				$(document).ready(function(){
				$("#selectBeneficiaryScopeID").selectmenu('destroy');
				});</script>
			</s:else>	
        <td>
			<input id="beneficiaryAccountNo" name="WireTransferPayee.AccountNum" value="<ffi:getProperty name="WireTransferPayee" property="AccountNum"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="35" size="20" <ffi:getProperty name="disableEdit"/>>		
			<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.AccountNum}" value2="${WireTransferPayee.MasterPayee.AccountNum}" operator="notEquals">
	        	<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.AccountNum" /></span>
	        </ffi:cinclude>
			</ffi:cinclude>
        </td>
        <td><select id="selectAccountTypeID" class="txtbox" name="WireTransferPayee.AccountType" <ffi:getProperty name="disableEdit"/>>
                <s:iterator value="accountTypeList">
                     	<option value="<s:text name="key"/>" <s:if test="%{WireTransferPayee.AccountType == key}">selected</s:if>><s:text name="value"/></option>
                </s:iterator>
                </select>
                <ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${WireTransferPayee.AccountType}" value2="${WireTransferPayee.MasterPayee.AccountType}" operator="notEquals">
                	<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.AccountType" /></span>
                </ffi:cinclude>
				</ffi:cinclude>
        </td>
        
    </tr>
    <tr>
    	<td colspan="2">
    	<span id="WireTransferPayee.PayeeNameError"></span> 
		<span id="payeeNameError"></span>
    	<span id="PayeeScopeError"></span></td>
    	<%-- <td><span id="PayeeScopeError"></span></td> --%>
    	<td colspan="2">
    		<span id="WireTransferPayee.AccountNumError"></span>
    		<span id="accountNumError"></span>
    	</td>
    </tr>
    <tr>
    <td id="beneficiaryNickNameLabel" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.NickName}" oldFieldValue="${WireTransferPayee.MasterPayee.NickName}" 
         daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=LABEL_NICKNAME%></td>
    <td id="beneficiaryContactLabel" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.ContactName}" oldFieldValue="${WireTransferPayee.MasterPayee.ContactName}" 
          daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=LABEL_CONTACT_PERSON%>
    </td>
     <td id="beneficiaryAddress1Label" colspan="2"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.Street}" oldFieldValue="${WireTransferPayee.MasterPayee.Street}" 
        	daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=LABEL_ADDRESS_1%></span><span class="required">*</span></td>
	</tr>
	<tr>
		<td>
	        <input id="beneficiaryNickName" name="WireTransferPayee.NickName" value="<ffi:getProperty name="WireTransferPayee" property="NickName"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="64" size="20" <ffi:getProperty name="disableEdit"/>>
			<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.NickName}" value2="${WireTransferPayee.MasterPayee.NickName}" operator="notEquals">	        
	        	<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.NickName" /></span>
	        </ffi:cinclude>
			</ffi:cinclude>
       </td>
       <td>
        	<input id="beneficiaryContact" name="WireTransferPayee.ContactName" value="<ffi:getProperty name="WireTransferPayee" property="ContactName"/>" class="ui-widget-content ui-corner-all txtbox"  maxlength="64" size="20" <ffi:getProperty name="disableEdit"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.ContactName}" value2="${WireTransferPayee.MasterPayee.ContactName}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.ContactName" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
        </td>
        <td colspan="2">
        	<input id="beneficiaryAddress1" name="WireTransferPayee.Street" value="<ffi:getProperty name="WireTransferPayee" property="Street"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="128" size="45" <ffi:getProperty name="disableEdit"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.Street}" value2="${WireTransferPayee.MasterPayee.Street}" operator="notEquals">	
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.Street" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
		</td>	
	</tr>
	<tr>
		<td><span id="WireTransferPayee.NickNameError"></span></td>
		<td><span id="WireTransferPayee.ContactNameError"></span></td>
		<td colspan="2">
			<span id="WireTransferPayee.StreetError"></span>
			<span id="streetError"></span>
		</td>  
    </tr>
    <tr>
        <td id="beneficiaryAddress2Label" colspan="2" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.Street2}" oldFieldValue="${WireTransferPayee.MasterPayee.Street2}" 
        	daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=LABEL_ADDRESS_2%></td>
    	<td id="beneficiaryCityLabel"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.City}" oldFieldValue="${WireTransferPayee.MasterPayee.City}" 
            daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=LABEL_CITY%></span><span class="required">*</span></td>
    	<td id="beneficiaryStateLabel" class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.State}" oldFieldValue="${WireTransferPayee.MasterPayee.State}" 
          daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=showInternational ? LABEL_STATE_PROVINCE : LABEL_STATE%></td>
    </tr>
    <tr>
        <td colspan="2">
        	<input id="beneficiaryAddress2" name="WireTransferPayee.Street2" value="<ffi:getProperty name="WireTransferPayee" property="Street2"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="128" size="45" <ffi:getProperty name="disableEdit"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.Street2}" value2="${WireTransferPayee.MasterPayee.Street2}" operator="notEquals">	
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.Street2" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
			
        </td>
        <td>
        	<input id="beneficiaryCity" name="WireTransferPayee.City" value="<ffi:getProperty name="WireTransferPayee" property="City"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="32" size="20" <ffi:getProperty name="disableEdit"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.City}" value2="${WireTransferPayee.MasterPayee.City}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.City" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>
        </td>
        <td class="sectionsubhead">
            <%
            	String payeeState = "";
            %>
            <ffi:getProperty name="WireTransferPayee" property="State" assignTo="payeeState" />
			<div id="wireStateSelectedId">
				<ffi:cinclude value1="${stateList}" value2="null" operator="notEquals">
					<select id="selectStateProvinceID" class="txtbox" <ffi:getProperty name="disableEdit"/>
						name="WireTransferPayee.State"
						<ffi:getProperty name="m_editBeneficiaryInfoDisabled"/>>
						<option value=""><s:text name="jsp.default_376"/></option>
						<ffi:list collection="StateList" items="SPDefn">
							<option
								<ffi:cinclude value1="<%= payeeState %>" value2="${SPDefn.StateKey}">selected</ffi:cinclude>
								value="<ffi:getProperty name="SPDefn" property="StateKey"/>"><ffi:getProperty
								name="SPDefn" property="Name" /></option>
						</ffi:list>
					</select> 
				</ffi:cinclude>				
			</div>
			<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
				<ffi:cinclude value1="${WireTransferPayee.State}" value2="${WireTransferPayee.MasterPayee.State}" operator="notEquals">
					<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.State" /></span>
				</ffi:cinclude>
			</ffi:cinclude>
        </td>
    </tr>
    <tr>
    	<td colspan="2"><span id="WireTransferPayee.Street2Error"></span></td>
    	<td>
    		<span id="WireTransferPayee.CityError"></span>
    		<span id="cityError"></span>
    	</td>
    </tr>
    <s:if test="%{!StatesExistsForPayeeCountry}">
    	<ffi:setProperty name="WireTransferPayee" property="State" value="" />
    </s:if>
	
	<tr>
        <td id="beneficiaryZipCodeLabel"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.ZipCode}" oldFieldValue="${WireTransferPayee.MasterPayee.ZipCode}" 
        daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=showInternational ? LABEL_ZIP_POSTAL_CODE 	: LABEL_ZIP_CODE%></span><span class="required">*</span></td>
       
        <td id="beneficiaryCountryLabel">
        	<%
				if (showInternational) {
			%>
			<span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.Country}" oldFieldValue="${WireTransferPayee.MasterPayee.Country}" 
	        daUserAction="${WireTransferPayeeDA.Action}" dacss="sectionheadDA" defaultcss="sectionsubhead"/>'><%=LABEL_COUNTRY%></span><span class="required">*</span>
	         <%
				}
			%>
		</td>
        <td></td>
       
         <%-- Only show Beneficiary Type when adding/editing a payee (payeeTask == WireTransferPayee when adding/editing a wire) --%>
   		 <s:if test="%{isBeneficiary}">
   		 	<td id="beneficiaryTypeLabel"><span class='<ffi:daPendingStyle newFieldValue="${WireTransferPayee.PayeeDestination}" oldFieldValue="${WireTransferPayee.MasterPayee.PayeeDestination}" 
              daUserAction="${WireTransferPayeeDA.Action}" defaultcss="sectionsubhead"  dacss="sectionheadDA"/>'><%=LABEL_BENEFICIARY_TYPE%></span><span class="required">*</span>
            </td>
   		 </s:if>
	</tr>
	
    <tr>
        <td>
        	<input id="beneficiaryZipCode" name="WireTransferPayee.ZipCode" value="<ffi:getProperty name="WireTransferPayee" property="ZipCode"/>" type="text" class="ui-widget-content ui-corner-all txtbox" maxlength="32" size="20" <ffi:getProperty name="disableEdit"/>>
        	<ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.ZipCode}" value2="${WireTransferPayee.MasterPayee.ZipCode}" operator="notEquals">
        		<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.ZipCode" /></span>
        	</ffi:cinclude>
			</ffi:cinclude>	
        </td>
       
        <td class="sectionsubhead">
        	 <%
				if (showInternational) {
			%>
            <%
            	String tempDefaultValue = "";
            		String tempCountry = "";
            %>
            <ffi:getProperty name="WireTransferPayee" property="Country" assignTo="tempDefaultValue"/>
            <%
            	if (tempDefaultValue == null)
            			tempDefaultValue = "";
            %>
            <%
            	tempDefaultValue = (tempDefaultValue == "" ? com.ffusion.banklookup.beans.FinancialInstitution.PREFERRED_COUNTRY
            				: tempDefaultValue);
            %>
            <div class="selectBoxHolder">
	            <select id="selectBeneficiaryCountryID" name="<ffi:getProperty name="payeeTask"/>.Country" style="width: 200;"  class="txtbox" <ffi:getProperty name="disableEdit"/> onChange="updateDBStateInformation(this),ns.common.adjustSelectMenuWidth('selectBeneficiaryCountryID')">
				<ffi:list collection="WireCountryList" items="country">
	            <ffi:getProperty name="country" property="BankLookupCountry" assignTo="tempCountry"/>
	            <option value="<ffi:getProperty name='tempCountry'/>" <%=(tempDefaultValue.equals(tempCountry) ? "selected"
												: "")%>><ffi:getProperty name="country" property="Name"/></option>
	            </ffi:list>
	            </select>
	        </div>
            <ffi:cinclude value1="${WireTransferPayeeDA.Action}" value2="<%= IDualApprovalConstants.USER_ACTION_MODIFIED %>">
			<ffi:cinclude value1="${WireTransferPayee.Country}" value2="${WireTransferPayee.MasterPayee.Country}" operator="notEquals">
            	<span class="sectionhead_greyDA"><ffi:getProperty name="WireTransferPayee" property="MasterPayee.Country" /></span>
            </ffi:cinclude>
			</ffi:cinclude>		
			 <%
				}
			 %>
       </td>
       
       <td></td>
       
        <%-- Only show Beneficiary Type when adding/editing a payee (payeeTask == WireTransferPayee when adding/editing a wire) --%>
        <s:if test="%{isBeneficiary}">
        	<td class="sectionsubhead"><select id="selectPayeeDestinationID" name="<ffi:getProperty name="payeeTask"/>.PayeeDestination" class="txtbox" <ffi:getProperty name="disablePayeeType"/> onchange="changeType(this.value);">
                <%
                	String curDest = "";
                %>
                <ffi:getProperty name="${payeeTask}" property="PayeeDestination" assignTo="curDest"/>
                
                <%-- Start: Dual approval processing 
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
					 <ffi:cinclude value1="${benficiaryType}" value2="" operator="notEquals">
						<% curDest = (String)session.getAttribute("benficiaryType"); %>
						<ffi:setProperty name="${payeeTask}" property="PayeeDestination" value="${benficiaryType}"/>						
					 </ffi:cinclude>
				</ffi:cinclude>--%>
				<%-- End: Dual approval processing --%>
				
                <% if (curDest == null) curDest = ""; %>
                <option value="<%= WireDefines.PAYEE_TYPE_REGULAR %>" <%= curDest.equalsIgnoreCase(WireDefines.PAYEE_TYPE_REGULAR) ? "selected" : "" %>><!--L10NStart-->Regular<!--L10NEnd--></option>
				<option value="<%= WireDefines.PAYEE_TYPE_DRAWDOWN %>" <%= curDest.equalsIgnoreCase(WireDefines.PAYEE_TYPE_DRAWDOWN) ? "selected" : "" %>><!--L10NStart-->Drawdown<!--L10NEnd--></option>
				<option value="<%= WireDefines.PAYEE_TYPE_BOOK %>" <%= curDest.equalsIgnoreCase(WireDefines.PAYEE_TYPE_BOOK) ? "selected" : "" %>><!--L10NStart-->Book<!--L10NEnd--></option>				
        	 </select>       	
     		</td>
        </s:if>
    </tr>
    <tr>
    	<td colspan="2"><span id="WireTransferPayee.ZipCodeError"></span> <span id="WireTransferPayee.CountryError"></span></td>
    	<%
			if (showInternational) {
		%>
    	<%-- <td><span id="WireTransferPayee.CountryError"></span></td> --%>
    	<td></td>
    	
    	<s:if test="%{isBeneficiary}">
    		<td><span id="PayeeDestinationError"></span></td>
    	</s:if>
    	<%
			}
		%>
    </tr>
    <tr>
    	<td>
	    	<span id="zipCodeError"></span>
    	</td>
    </tr>



    	
</table>

<ffi:removeProperty name="disablePayeeType"/>

<script>
$(function(){
	ns.common.adjustSelectMenuWidth('selectBeneficiaryCountryID');
});
</script>
