<%--
This is the release wires page.

Pages that request this page
----------------------------
wire_buttons.jsp (included in many pages)
	RELEASE WIRES top button
index.jsp (CB home page - in cb root directory)
	"Please release x new wires" link
wiresreleaseconfirm.jsp
	BACK button

Pages this page requests
------------------------
CANCEL requests one of the following:
	wiretransfers.jsp (wire summary page)
	index.jsp (CB home page - in cb root directory)
SUBMIT requests wiresreleaseconfirm.jsp
Calendar button requests calendar.jsp
REFRESH VIEW requests wiresrelease.jsp (itself)
View button requests one of the following:
	wiretransferview.jsp
	wirebookview.jsp
	wiredrawdownview.jsp
	wirefedview.jsp
	wirehostview.jsp
Sort buttons request wiresrelease.jsp (itself)

Pages included in this page
---------------------------
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%-- <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> --%>

<ffi:setProperty name="subMenuSelected" value="wires"/>
<ffi:setProperty name='PageHeading' value='Release Wires'/>

<ffi:setProperty name="DimButton" value="releasewires"/>


<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="GetSortImage" scope="session"/>
<ffi:setProperty name="SortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="Reverse Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="WiresRelease"/>
<ffi:process name="SortImage"/>

	<div id="releaseWiresGridDivID" class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" >
        <div class="portlet-header ui-widget-header ui-corner-all" role="section" aria-labelledby="summaryHeader">
        <h1 id="summaryHeader" class="portlet-title"><s:text name="jsp.wire.wire_release_summary"/></h1>
        	<%-- <span class="portlet-title">Release Wires</span>--%>
			<div class="searchHeaderCls">
					<span class="searchPanelToggleAreaCls" onclick="$('#quicksearchWireReleaseCriteria').slideToggle();">
						<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
					</span>
					<div class="summaryGridTitleHolder">
						<span id="selectedGridTab" class="selectedTabLbl">
							Release Wires
						</span>
					</div>
			</div>
		</div> 	
        <div class="portlet-content">
        	<div class="searchDashboardRenderCls">
				<s:include value="/pages/jsp/wires/inc/wire_release_quicksearch.jsp"/>
			</div>
		<s:form id="frmReleaseID" name="frmRelease" namespace="/pages/jsp/wires"  validate="true" action="/pages/jsp/wires/initReleaseWireDashBoard_confirm.action" method="post" theme="simple">
		<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
		<input type="hidden" name="ID" value=""/>
		<input type="hidden" name="recID" value=""/>
		<input type="hidden" name="collectionName" value=""/>
		<input type="hidden" name="wireReleaseDetails" id="wireReleaseDetails">
		<input type="hidden" name="releaseAll" id="releaseAll">
		<input type="hidden" name="rejectAll" id="rejectAll">
			<div id="gridContainer" class="summaryGridHolderDivCls">
			<ffi:help id="payments_wiresrelease" className="moduleHelpClass"/>
			<ffi:setGridURL grid="GRID_wireRelease" name="ViewURL" url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID={0}&collectionName=WiresRelease&Date={1}&transType=SINGLE" parm0="ID" parm1="DateToPost"/>
			<ffi:setGridURL grid="GRID_wireRelease" name="ViewURLHost" url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID={0}&collectionName=WiresRelease&Date={1}&transType=SINGLE" parm0="ID" parm1="DateToPost"/>
			<ffi:setGridURL grid="GRID_wireRelease" name="ViewURLBook" url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID={0}&collectionName=WiresRelease&Date={1}&transType=SINGLE" parm0="ID" parm1="DateToPost"/>
			<ffi:setGridURL grid="GRID_wireRelease" name="ViewURLDrawdown" url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID={0}&collectionName=WiresRelease&Date={1}&transType=SINGLE" parm0="ID" parm1="DateToPost"/>
			<ffi:setGridURL grid="GRID_wireRelease" name="ViewURLFED" url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID={0}&collectionName=WiresRelease&Date={1}&transType=SINGLE" parm0="ID" parm1="DateToPost"/>
	
			<ffi:setProperty name="tempURL" value="/pages/jsp/wires/initReleaseWireDashBoard.action?GridURLs=GRID_wireRelease" URLEncrypt="true"/>
			<s:url id="releaseWiresUrl" value="%{#session.tempURL}" escapeAmp="false"/>
			<sjg:grid
				id="releaseWiresGridID"
				dataType="json"
				href="%{releaseWiresUrl}"
				pager="true"
				gridModel="gridModel"
				rowList="%{#session.StdGridRowList}"
				rowNum="%{#session.StdGridRowNum}"
				rownumbers="false"
				shrinkToFit="true"
				navigator="true"
				navigatorAdd="false"
				navigatorDelete="false"
				navigatorEdit="false"
				navigatorRefresh="false"
				navigatorSearch="false"
				navigatorView="false"
				sortable="true"
				scroll="false"
				scrollrows="true"
				viewrecords="true"
				sortname="dateToPost"
				sortorder="asc"
				onGridCompleteTopics="addGridControlsEvents,releaseWiresGridCompleteEvents"
				>
				<sjg:gridColumn name="dateToPost" index="date" title="%{getText('jsp.wire.Date')}" sortable="true" width="65"/>
				<sjg:gridColumn name="map.beneficiary" index="beneficiary" title="%{getText('jsp.wire.Beneficiary')}" sortable="true" width="75"/>
				<sjg:gridColumn name="map.accountNickname" index="accountNickname" title="%{getText('jsp.wire.Account_Nickname')}" sortable="true" width="120"/>
				<sjg:gridColumn name="map.destination" index="destination" title="%{getText('jsp.wire.Type')}" sortable="true" width="55"/>
				<sjg:gridColumn name="frequency" index="frequency" title="%{getText('jsp.wire.Frequency')}" sortable="true" width="55"/>
				<sjg:gridColumn name="map.origStatusName" index="status" title="%{getText('jsp.wire.Status')}" sortable="true" width="55"/>
				<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amount" title="%{getText('jsp.wire.Amount')}" sortable="true" width="55"/>
				<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="%{getText('jsp.default.ViewUrl')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
				<sjg:gridColumn name="map.canRelease" index="canRelease" title="%{getText('jsp.default.canRelease')}" search="false" sortable="false" hidden="true" hidedlg="true"/>
				<sjg:gridColumn name="ID" index="IDindex" title="%{getText('jsp.wire.Action')}" sortable="false" formatter="ns.wire.formatReleaseWiresActionLinks" hidedlg="true" cssStyle="white-space:normal" hidden="" cssClass=""/>
				
	
			</sjg:grid></div></s:form>
		</div>
		<div id="wireDetailsDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	       		  <li><a href='#' onclick="ns.common.bookmarkThis('releaseWiresGridDivID')"><span class="sapUiIconCls icon-create-session" style="float:left;"></span>Bookmark</a></li>
	       		  <li><a href='#' onclick="ns.common.showDetailsHelp('releaseWiresGridDivID')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>
	</div>

	<script>
		//Initialize portlet with settingsicon
		ns.common.initializePortlet("releaseWiresGridDivID");
		$("#releaseWiresGridID").jqGrid('setColProp','map.action',{title:false}).addClass("skipActionHolderDivAppend");
	</script>

	<div class="submitButtonsDiv">
		<s:url id="releaseWiresCancelURL" value="/pages/jsp/wires/wires_summary.jsp">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a
			id="cancelFromWiresReleaseLink"
			href="%{releaseWiresCancelURL}"
			targets="summary"
			indicator="indicator"
			button="true"
			onCompleteTopics="cancelFromWiresReleaseOnCompleteTopics"
			><s:text name="jsp.default_82" /><!-- CANCEL -->
		</sj:a>

		<sj:a
			id="releaseAllActionLink"
			button="true"
			onclick="ns.wire.setAll(1);"
			><s:text name="jsp.default_Release_All" /><!-- RELEASE ALL -->
		</sj:a>

		<sj:a
			id="rejectAllActionLink"
			button="true"
			onclick="ns.wire.setAll(2);"
			><s:text name="jsp.default_Reject_All" /><!-- REJECT ALL -->
		</sj:a>

		<sj:a
			id="submitWiresReleaseLink"
			formIds="frmReleaseID"
			targets="wiresReleaseConfirmDiv"
			button="true"
			validate="false"
			onClickTopics="submitWiresReleaseOnClickTopics"
			onCompleteTopics="submitWiresReleaseOnCompleteTopics"
			><s:text name="jsp.default_395" /><!-- SUBMIT -->
		</sj:a>
	</div>
	<div style="clear:both"></div>
<s:set var="tmpI18nStr" value="%{getText('jsp.wire.releaseAll.confirm')}" scope="request" />
<input type="hidden" id="releaseWireMsg" value="<ffi:getProperty name="tmpI18nStr"/>"/>

<s:set var="tmpI18nStr" value="%{getText('jsp.wire.rejectAll.confirm')}" scope="request" />
<input type="hidden" id="rejectWireMsg" value="<ffi:getProperty name="tmpI18nStr"/>"/>
