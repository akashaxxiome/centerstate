<%--
This is the wire batch confirmation page.  This is for non-international wires
only.  International wires use wirebatchintffconfirm.jsp

Pages that request this page
----------------------------
wirebatchff.jsp
	SUBMIT WIRE BATCH button
wirebatcheditff.jsp
	SUBMIT WIRE BATCH button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
BACK requests one of the following
	wirebatchff.jsp
	wirebatcheditff.jsp
CONFIRM/SEND BATCH requests wirebatchsend.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatchffconfirm" className="moduleHelpClass"/>
<ffi:setL10NProperty name='PageHeading' value='Confirm Wire Batch'/>
<ffi:setProperty name='PageText' value=''/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<ffi:setProperty name="AddEditTask" value="wireBatchObject" />

<ffi:setProperty name="${AddEditTask}" property="DateFormat" value="${UserLocale.DateFormat}"/>
<div align="center" class="">
<span id="wireBatchResultMessage" style="display:none">Your wire batch has been saved.</span>
<div class="marginTop20">
<% String batchDest, batchDestDisplay = ""; %>
    <%-- <ffi:getProperty name="${AddEditTask}" property="BatchDestination" assignTo="batchDest"/>
    <ffi:setProperty name="WireDestinations" property="Key" value="<%= batchDest %>"/>--%>
    <ffi:getProperty name="batchType"  assignTo="batchDestDisplay"/>
    <%-- Show alert if date was changed --%>
    <s:if test="%{nonBusinessDay}">
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
					<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchffconfirm.jsp-1" parm0="<%= batchDestDisplay %>" />
					<br><br>
        </div>
    </s:if>

    <%-- Duplicate Wire warning --%>
     <s:if test="%{duplicateWire}">
        <div class="sectionsubhead" colspan="3" style="color:red" align="center">
						<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchffconfirm.jsp-3" parm0="<%= batchDestDisplay %>" />
        </div>
    </s:if>
</div>
<div class="leftPaneWrapper" role="form" aria-labelledby="batchSummaryHeader"><div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="batchSummaryHeader">Wire Batch Summary</h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyWireBeneficiaryLabel">Batch Name: </span>
				<span class="inlineSection floatleft labelValue" id="verifyWireBeneficiaryValue"><ffi:getProperty name="${AddEditTask}" property="BatchName"/></span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection">Batch Type: </span>
				<span class="inlineSection floatleft labelValue"><s:property value="batchType"/></span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection">Application Type: </span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="${AddEditTask}" property="BatchType"/></span>
		</div>
	</div>
</div></div>
<div class="confirmPageDetails">
<div class="blockWrapper" role="form" aria-labelledby="batchInfoHeader">
	<div  class="blockHead"><!--L10NStart--><h2 id="batchInfoHeader">Batch Information</h2><!--L10NEnd--></div>
	<div class="blockContent label140">
		<%-- <div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyWireBatchNameLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Batch Name<!--L10NEnd--></span>
                <span id="verifyWireBatchNameValue" class="columndata"><ffi:getProperty name="${AddEditTask}" property="BatchName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyWireBatchTypeLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Batch Type<!--L10NEnd--></span>
                <span id="verifyWireBatchTypeValue" class="columndata"><s:property value="batchType"/></span>
			</div>
		</div> --%>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="verifyRequestedDateLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Requested Value Date:<!--L10NEnd--></span>
                <span id="verifyRequestedDateValue" class="valueCls"><ffi:getProperty name="${AddEditTask}" property="DueDate"/></span>
			</div>
			<div class="inlineBlock">
				<span id="verifyExpectedDateLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Expected Value Date:<!--L10NEnd--></span>
                <span id="verifyExpectedDateValue" class="valueCls"><ffi:getProperty name="${AddEditTask}" property="DateToPost"/></span>
			</div>
		</div>
	</div>
</div>

<div class="blockWrapper marginTop20" role="form" aria-labelledby="includedBatchesHeader">
	<div  class="blockHead"><h2 id="includedBatchesHeader">Included Batches</h2></div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
              <tr class="header">
                  <td id="verifyWireBeneficiaryLabel" align="center" class="sectionsubhead" colspan="2"><s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DRAWDOWN}">Drawdown Account</s:if> <s:else><!--L10NStart-->Beneficiary<!--L10NEnd--></s:else></td>
                  <td id="verifyWireAmountLabel" class="sectionsubhead" align="left"><!--L10NStart-->Amount<!--L10NEnd--></td>
              </tr>
              <% int idCount=0; %>
              <ffi:setProperty name="${AddEditTask}" property="Wires.Filter" value="ACTION!!del,Status!!3,AND" />
              <s:iterator value="%{wireBatchObject.Wires}" var="wire" > 
                    <tr>
                        <td id="verifyWireBeneficiaryValue<%=idCount %>>" align="center" class="columndata" colspan="2" nowrap><s:property  value="WirePayee.PayeeName"/>
                        	   	<s:if test="%{WirePayee.NickName != null && WirePayee.NickName != ''}"> (<s:property  value="WirePayee.NickName"/>)</s:if>                        	
						</td>
                        <td id="verifyWireAmountValue<%=idCount %>>" align="left" class="columndata" nowrap>
                 
                  			<s:if test="%{Action ==  @com.ffusion.beans.wiretransfers.WireDefines@WIRE_ACTION_ADD}">
                  				<s:property  value="AmountValue.CurrencyStringNoSymbol"/>                                    
                            </s:if>
                            <s:else>
                            	<ffi:object id="Currency" name="com.ffusion.beans.common.Currency"/>
					<ffi:setProperty name="Currency" property="Amount" value="${wire.OrigAmount}"/>
					<ffi:setProperty name="Currency" property="CurrencyCode" value="${wire.OrigCurrency}"/>
                                <ffi:getProperty name="Currency" property="CurrencyStringNoSymbol"/>
                            </s:else>
                     
                        </td>
                        <% idCount++; %>
                    </tr>
              </s:iterator>
              <ffi:setProperty name="${AddEditTask}" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
			  <s:if test="%{wireBatchObject.batchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
			  <s:if test="%{wireBatchObject.paymentAmount == null}">
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Original Total:<!--L10NEnd--></td>
							<td align="left" class="tbrd_t sectionsubhead" width="20%" valign="bottom" nowrap>
							<s:if test="%{!isModifyAction}">
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
							</s:if>
							<s:else>
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
							</s:else>
							</td>
						</tr>
						<s:if test="%{wireBatchAmountUrl == 'modifyWireBatchAction_getBatchAmount.action'}">
								<script>
								//QTS680696
 									ns.wire.viewTotalsInBatchConfirmURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/modifyWireBatchAction_getBatchAmount.action"/>' ;
 								</script>
							</s:if>
							<s:else>
								<script>
								//QTS680696
 									ns.wire.viewTotalsInBatchConfirmURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireBatchAction_getBatchAmount.action"/>' ;
 								</script>
							</s:else>
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Total in USD:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="20%" valign="bottom" nowrap><%-- <ffi:link url="wirebatchinttempconfirm.jsp?GetAmounts=true"><!--L10NStart-->View Totals<!--L10NEnd--></ffi:link>--%>
								<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm()">View Totals</a>
							</td>
						</tr>
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Total Payment:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="20%" valign="bottom" nowrap><%--<ffi:link url="wirebatchinttempconfirm.jsp?GetAmounts=true"><!--L10NStart-->View Totals<!--L10NEnd--></ffi:link>--%>
								<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm()">View Totals</a>
							</td>
						</tr>
						</s:if>
						<s:else>
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Original Total:<!--L10NEnd--></td>
							<td align="left" class="tbrd_t sectionsubhead" width="20%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>
								<s:if test="%{!isModifyAction}">
									(<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
								</s:if>
								<s:else>
									(<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
								</s:else>
							</td>
						</tr>
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Total in USD:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="20%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="TotalDebit"/>
							</td>
						</tr>
						<tr>
							<td align="center" class="columndata" width="60%">&nbsp;</td>
							<td align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap><!--L10NStart-->Total Payment:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="20%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="paymentAmount"/> (<ffi:getProperty name="${AddEditTask}" property="PayeeCurrencyType"/>)
							</td>
						</tr>
						</s:else>		
						</s:if>
						<s:else>
						 <tr>
                            <td align="center" class="columndata" width="60%">&nbsp;</td>
                            <td id="totalLabel" align="right" class="columndata" width="20%" align="right" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                            <td id="totalValue" align="left" class="tbrd_t sectionsubhead" width="20%" valign="bottom" nowrap>
                            <ffi:cinclude value1="${AddEditTask}" value2="AddWireBatch" operator="equals">
                                <ffi:getProperty name="${AddEditTask}" property="Amount"/>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${AddEditTask}" value2="AddWireBatch" operator="notEquals">
                                <ffi:getProperty name="${AddEditTask}" property="TotalOrigAmount"/>
                            </ffi:cinclude>
                            </td>
                        </tr>
				</s:else>
               </table>
	</div>
</div>
<div class="btn-row">
 <s:form id="sendWireBacthFormID" method="post" name="FormName" namespace="/pages/jsp/wires" action="%{#session.wirebatch_confirm_form_url}" theme="simple">
 <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<sj:a id="cancelFormButtonOnBatchVerify"
	button="true" summaryDivId="summary" buttonType="cancel"
	onClickTopics="showSummary,cancelWireBatchForm"
	><s:text name="jsp.default_82" />
	</sj:a>
	<sj:a id="backFormButton" 
	button="true" 
	onClickTopics="backToInput"
	><s:text name="jsp.default_57" />
	</sj:a>
	<sj:a id="sendWireBacthSubmit"
	formIds="sendWireBacthFormID" 
	targets="confirmDiv" 
	button="true" 
	onBeforeTopics="sendWireBacthFormBeforeTopics"
	onSuccessTopics="sendWireBacthFormSuccessTopic"
	onErrorTopics="errorSendWireBatchForm"
	onCompleteTopics="sendWireBacthFormCompleteTopic"><s:text name="jsp.default_395" /></sj:a>
 </s:form>
</div>
</div></div>
           <%--     <table width="750" cellpadding="0" cellspacing="0" border="0">
     
                <tr>
                <td class="columndata ltrow2_color" align="center">
                 <table width="98%" border="0" cellspacing="0" cellpadding="3">
    <% String batchDest, batchDestDisplay = ""; %>
    <ffi:getProperty name="${AddEditTask}" property="BatchDestination" assignTo="batchDest"/>
    <ffi:setProperty name="WireDestinations" property="Key" value="<%= batchDest %>"/>
    <ffi:getProperty name="batchType"  assignTo="batchDestDisplay"/>
    Show alert if date was changed
    <s:if test="%{nonBusinessDay}">
    <tr>
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
					<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchffconfirm.jsp-1" parm0="<%= batchDestDisplay %>" />
					<br><br>
        </td>
    </tr>
    </s:if>


    Duplicate Wire warning
     <s:if test="%{duplicateWire}">
    <tr>
        <td class="sectionsubhead" colspan="3" style="color:red" align="center">
						<ffi:getL10NString rsrcFile='cb' msgKey="jsp/payments/wirebatchffconfirm.jsp-3" parm0="<%= batchDestDisplay %>" />
            <br><br>
        </td>
    </tr>
    </s:if>

    <tr>
        <td align="left" class="tbrd_b sectionhead">&gt; <!--L10NStart-->Batch Information<!--L10NEnd--></td>
    </tr>
    </table> --%>
                    <%-- <table width="715" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="357" valign="top">
                                <table width="355" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td id="verifyWireBatchNameLabel" align="left" width="115" class="sectionsubhead"><!--L10NStart-->Batch Name<!--L10NEnd--></td>
                                        <td id="verifyWireBatchNameValue" align="left" width="228" class="columndata"><ffi:getProperty name="${AddEditTask}" property="BatchName"/></td>
                                    </tr>
                                    <tr>
                                        <td id="verifyWireBatchTypeLabel" align="left" class="sectionsubhead"><!--L10NStart-->Batch Type<!--L10NEnd--></td>
                                        <td id="verifyWireBatchTypeValue" align="left" class="columndata"><s:property value="batchType"/></td>
                                    </tr>
                                    <tr>
                                        <td id="verifyApplicationTypeLabel" align="left" class="sectionsubhead"><!--L10NStart-->Application Type<!--L10NEnd--></td>
                                        <td id="verifyApplicationTypeValue" align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="BatchType"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                            <td width="348" valign="top">
                                <table width="347" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td id="verifyRequestedDateLabel" align="left" width="135" class="sectionsubhead"><!--L10NStart-->Requested Value Date<!--L10NEnd--></td>
                                        <td id="verifyRequestedDateValue" align="left" width="200" class="columndata"><ffi:getProperty name="${AddEditTask}" property="DueDate"/></td>
                                    </tr>
                                    <tr>
                                        <td id="verifyExpectedDateLabel" align="left" class="sectionsubhead"><!--L10NStart-->Expected Value Date<!--L10NEnd--></td>
                                        <td id="verifyExpectedDateValue" align="left" class="columndata"><ffi:getProperty name="${AddEditTask}" property="DateToPost"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table> --%>
                    <!-- <table width="98%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td id="verifyIncludeBatchLabel" align="left" class="tbrd_b sectionhead">&gt; L10NStartIncluded BatchesL10NEnd</td>
                        </tr>
                    </table> -->
                    <%-- <table width="375" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td id="verifyWireBeneficiaryLabel" align="left" class="sectionsubhead" colspan="2"><s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DRAWDOWN}">Drawdown Account</s:if> <s:else><!--L10NStart-->Beneficiary<!--L10NEnd--></s:else></td>
                            <td id="verifyWireAmountLabel" class="sectionsubhead" align="right"><!--L10NStart-->Amount<!--L10NEnd--></td>
                        </tr>
                        <% int idCount=0; %>
                        <ffi:setProperty name="${AddEditTask}" property="Wires.Filter" value="ACTION!!del,Status!!3,AND" />
                        
                       
                        <s:iterator value="%{wireBatchObject.Wires}" var="wire" > 
                        <tr>
                            <td id="verifyWireBeneficiaryValue<%=idCount %>>" align="left" class="columndata" colspan="2" nowrap><s:property  value="WirePayee.PayeeName"/>
                            	   	<s:if test="%{WirePayee.NickName != ''}"> (<s:property  value="WirePayee.NickName"/>)</s:if>                        	
							</td>
                            <td id="verifyWireAmountValue<%=idCount %>>" align="left" class="columndata" nowrap>
                     
                      			<s:if test="%{Action ==  @com.ffusion.beans.wiretransfers.WireDefines@WIRE_ACTION_ADD}">
                      				<s:property  value="AmountValue.CurrencyStringNoSymbol"/>                                    
                                </s:if>
                                <s:else>
                                	<ffi:object id="Currency" name="com.ffusion.beans.common.Currency"/>
									<ffi:setProperty name="Currency" property="Amount" value="${wire.OrigAmount}"/>
									<ffi:setProperty name="Currency" property="CurrencyCode" value="${wire.OrigCurrency}"/>
                                    <ffi:getProperty name="Currency" property="CurrencyStringNoSymbol"/>
                                </s:else>
                         
                            </td>
                            <% idCount++; %>
                        </tr>
                        </s:iterator>
                       
                        
                        <ffi:setProperty name="${AddEditTask}" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
						
						<s:if test="%{wireBatchObject.batchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
						<s:if test="%{wireBatchObject.paymentAmount == null}">
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Original Total:<!--L10NEnd--></td>
							<td align="left" class="tbrd_t sectionsubhead" width="1%" valign="bottom" nowrap>
							<s:if test="%{!isModifyAction}">
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
							</s:if>
							<s:else>
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/> (<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
							</s:else>
							</td>
						</tr>
						<s:if test="%{wireBatchAmountUrl == 'modifyWireBatchAction_getBatchAmount.action'}">
								<script>
								//QTS680696
 									ns.wire.viewTotalsInBatchConfirmURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/modifyWireBatchAction_getBatchAmount.action"/>' ;
 								</script>
							</s:if>
							<s:else>
								<script>
								//QTS680696
 									ns.wire.viewTotalsInBatchConfirmURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireBatchAction_getBatchAmount.action"/>' ;
 								</script>
							</s:else>
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Total in USD:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap><ffi:link url="wirebatchinttempconfirm.jsp?GetAmounts=true"><!--L10NStart-->View Totals<!--L10NEnd--></ffi:link>
								<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm()">View Totals</a>
							</td>
						</tr>
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Total Payment:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap><ffi:link url="wirebatchinttempconfirm.jsp?GetAmounts=true"><!--L10NStart-->View Totals<!--L10NEnd--></ffi:link>
								<a href="#" onClick="ns.wire.viewTotalsInBatchConfirm()">View Totals</a>
							</td>
						</tr>
						</s:if>
						<s:else>
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Original Total:<!--L10NEnd--></td>
							<td align="left" class="tbrd_t sectionsubhead" width="1%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>
								<s:if test="%{!isModifyAction}">
									(<ffi:getProperty name="wireBatchObject" property="AmtCurrencyType"/>)
								</s:if>
								<s:else>
									(<ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>)
								</s:else>
							</td>
						</tr>
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Total in USD:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="TotalDebit"/>
							</td>
						</tr>
						<tr>
							<td align="left" class="columndata" width="98%">&nbsp;</td>
							<td align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap><!--L10NStart-->Total Payment:<!--L10NEnd--></td>
							<td align="left" class="sectionsubhead" width="1%" valign="bottom" nowrap>
								<ffi:getProperty name="wireBatchObject" property="paymentAmount"/> (<ffi:getProperty name="${AddEditTask}" property="PayeeCurrencyType"/>)
							</td>
						</tr>
						</s:else>		
						</s:if>
						<s:else>
						 <tr>
                            <td align="left" class="columndata" width="98%">&nbsp;</td>
                            <td id="totalLabel" align="left" class="columndata" width="1%" align="right" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                            <td id="totalValue" align="left" class="tbrd_t sectionsubhead" width="1%" valign="bottom" nowrap>
                            <ffi:cinclude value1="${AddEditTask}" value2="AddWireBatch" operator="equals">
                                <ffi:getProperty name="${AddEditTask}" property="Amount"/>
                            </ffi:cinclude>
                            <ffi:cinclude value1="${AddEditTask}" value2="AddWireBatch" operator="notEquals">
                                <ffi:getProperty name="${AddEditTask}" property="TotalOrigAmount"/>
                            </ffi:cinclude>
                            </td>
                        </tr>
						</s:else>
                       </table> 
                    </td>
                </tr>
            </table>--%>
