<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<% 
boolean isBatch = false;
boolean isTemplate = false;
if(request.getParameter("isBatch")!=null){
	isBatch = Boolean.valueOf(request.getParameter("isBatch"));
}

if(request.getParameter("isTemplate")!=null){
	isTemplate = Boolean.valueOf(request.getParameter("isTemplate"));
}
%>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="sectionhead">		
			<br><ffi:getL10NString rsrcFile="cb" msgKey="jsp.wiretypechange.confirmation"/><br>
		</td>
	</tr>
	
	<tr>
		<td align="center"  class="ui-widget-header customDialogFooter">
			<sj:a 
				id="cancelWireTemplateLink" 
				button="true" 
				onClickTopics="closeConfirmationDialog" 
				title="Cancel"
				><s:text name="jsp.default_82"/>
			</sj:a>
			
			<% if(isBatch){	%>
			   <sj:a 
					id="changeWireBatchType" 
					button="true"   
					title="%{getText('jsp.default_303')}"
					onClickTopics="changeWireBatchType" 
					><s:text name="jsp.default_303"/>
				</sj:a>
			<% } else if(isTemplate){ %>
				<sj:a 
					id="changeWireTypeTemplate" 
					button="true"   
					title="%{getText('jsp.default_303')}"
					onClickTopics="changeWireTypeTemplate" 
					><s:text name="jsp.default_303"/>
				</sj:a>
			<% } else {	%>
				<sj:a 
					id="changeWireType" 
					button="true"   
					title="%{getText('jsp.default_303')}"
					onClickTopics="changeWireType" 
					><s:text name="jsp.default_303"/>
				</sj:a>
			<% } %>
		</td>
	</tr>
</table>