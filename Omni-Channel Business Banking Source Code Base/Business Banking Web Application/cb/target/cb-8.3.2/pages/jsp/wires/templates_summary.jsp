<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%-- <s:include value="%{#session.PagesPath}/wires/inc/wire_templates_pre.jsp"/>--%>


	<ffi:object id="wiretemptabs" name="com.ffusion.tasks.util.TabConfig"/>
	<ffi:setProperty name="wiretemptabs" property="TransactionID" value="WireTemplate"/>

	<ffi:setProperty name="wiretemptabs" property="Tab" value="user"/>
	<ffi:setProperty name="wiretemptabs" property="Href" value="/cb/pages/jsp/wires/initWireTemplatesSummaryAction.action?collectionName=WireTemplatesUser"/>
	<ffi:setProperty name="wiretemptabs" property="LinkId" value="user"/>
	<ffi:setProperty name="wiretemptabs" property="Title" value="User Template List"/>
	
	<ffi:setProperty name="wiretemptabs" property="Tab" value="business"/>
	<ffi:setProperty name="wiretemptabs" property="Href" value="/cb/pages/jsp/wires/initWireTemplatesSummaryAction.action?collectionName=WireTemplatesBusiness"/>
	<ffi:setProperty name="wiretemptabs" property="LinkId" value="business"/>
	<ffi:setProperty name="wiretemptabs" property="Title" value="Business Template List"/>
	
	<ffi:setProperty name="wiretemptabs" property="Tab" value="bank"/>
	<ffi:setProperty name="wiretemptabs" property="Href" value="/cb/pages/jsp/wires/initWireTemplatesSummaryAction.action?collectionName=WireTemplatesBank"/>
	<ffi:setProperty name="wiretemptabs" property="LinkId" value="bank"/>
	<ffi:setProperty name="wiretemptabs" property="Title" value="Bank Template List"/>
	
	<ffi:process name="wiretemptabs"/>

	<div id="wireTemplateGridTabs" class="portlet gridPannelSupportCls" >
	
		<div class="portlet-header" role="section" aria-labelledby="summaryHeader">
		<h1 id="summaryHeader" class="portlet-title"><s:text name="jsp.wire.wire_template_summary"/></h1>
		    <div class="searchHeaderCls">
			    <span class="searchPanelToggleAreaCls" onclick="$('#quicksearchTemplatecriteria').slideToggle();">
					<span class="gridSearchIconHolder sapUiIconCls icon-search"></span>
				</span>
				<div class="summaryGridTitleHolder">
					<span id="selectedGridTab" class="selectedTabLbl">
						User Template List
					</span>
					<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow floatRight"></span>
					
					<div class="gridTabDropdownHolder">
						<span class="gridTabDropdownItem" id='userTemplates' onclick="ns.common.showGridSummary('userTemplates')" >
							User Template List
						</span>
					     <ffi:cinclude value1="${SecureUser.BusinessID}" value2="0" operator="notEquals">
							<span class="gridTabDropdownItem" id='businessTemplates' onclick="ns.common.showGridSummary('businessTemplates')">
								Business Template List
							</span>
						</ffi:cinclude>
						
						<span class="gridTabDropdownItem" id='bankTemplates' onclick="ns.common.showGridSummary('bankTemplates')">
							Bank Template List
						</span>
					</div>
				</div>
			</div>
		</div>
	
		<div class="portlet-content">
	    	<div class="searchDashboardRenderCls">
				<s:include value="/pages/jsp/wires/inc/wireTemplateSearchCriteria.jsp"/>
			</div>
			<div id="gridContainer" class="summaryGridHolderDivCls">
				<div id="userTemplatesSummaryGrid" gridId="wireTemplatesUserGridID" class="gridSummaryContainerCls" >
					<s:action namespace="/pages/jsp/wires" name="initWireTemplatesSummaryAction" executeResult="true">
						<s:param name="collectionName">WireTemplatesUser</s:param>
					</s:action>
				</div>
				<div id="businessTemplatesSummaryGrid" gridId="wireTemplatesBusinessGridID" class="gridSummaryContainerCls hidden" >
					<s:action namespace="/pages/jsp/wires" name="initWireTemplatesSummaryAction" executeResult="true">
						<s:param name="collectionName">WireTemplatesBusiness</s:param>
					</s:action>
				</div>
				<div id="bankTemplatesSummaryGrid" gridId="wireTemplatesBankGridID" class="gridSummaryContainerCls hidden" >
					<s:action namespace="/pages/jsp/wires" name="initWireTemplatesSummaryAction" executeResult="true">
						<s:param name="collectionName">WireTemplatesBank</s:param>
					</s:action>
				</div>
			</div>
		</div>
		
		<div id="wireTemplateSummaryDataDiv-menu" class="portletHeaderMenuContainer" style="display:none;">
	       <ul class="portletHeaderMenu" style="background:#fff">
	              <li><a href='#' onclick="ns.common.showDetailsHelp('wireTemplateGridTabs')"><span class="sapUiIconCls icon-sys-help-2" style="float:left;"></span><s:text name='jsp.home_103' /></a></li>
	       </ul>
		</div>		
	</div>
	
<input type="hidden" id="getTransID" value="<ffi:getProperty name='wiretemptabs' property='TransactionID'/>" />	
	
<script>    
$.ajax({
    type: "POST",
   	url: "/cb/pages/jsp/wires/getWireTransferTemplatesAction_verify.action",
	data: $("#QuickSearchFrmTempID").serialize(),
    success: function(data) {
       
    }
});


//Initialize portlet with settings icon
ns.common.initializePortlet("wireTemplateGridTabs");	
</script>
