<%--
This is the final sent/saved page for import wire transfers.

Pages that request this page
----------------------------
wiretransferimportsave.jsp
	Javascript auto-refresh

Pages this page requests
------------------------
DONE requests wiretransfers.jsp

Pages included in this page
---------------------------
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireTransfers" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_wiretransferimportresults" className="moduleHelpClass"/>
<ffi:setL10NProperty name='PageHeading' value='Wire Transfers Import'/>

<ffi:setProperty name="DimButton" value="fileimport"/>
<s:include value="%{#session.PagesPath}/wires/inc/wire_buttons.jsp"/>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wiretransfers.jsp?DontInitialize=true" URLEncrypt="true"/>
<%
String sortDatas = request.getParameter("ImportWireTransfers.ToggleSortedBy") ;
if(  sortDatas != null && session.getAttribute("ImportWireTransfers") != null)
{
	((WireTransfers)session.getAttribute("ImportWireTransfers")).setToggleSortedBy( sortDatas );
}
%>

		<div align="center">
			
			<table width="750" border="0" cellspacing="0" cellpadding="0">

				<tr>
					<td class="columndata ltrow2_color">
						<div align="center">
							<table width="720" border="0" cellspacing="0" cellpadding="3">

								<tr>
									<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <!--L10NStart-->Wire Import Results<!--L10NEnd--></span><span class="sectionhead"><br>
										</span></td>
								</tr>
								<tr>
									<td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead"><!--L10NStart-->The following wires were submitted for payment.<br>Please check the status of each wire.<!--L10NEnd--></span><span class="sectionhead"><br>
										</span></td>
								</tr>

<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
<ffi:setProperty name="SortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="Reverse Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="ImportWireTransfers"/>
<ffi:process name="SortImage"/>
<ffi:setProperty name="SortImage" property="Compare" value="DueDate"/>
								<tr>
									<td class="sectionsubhead" width="80">
										<ffi:setProperty name="SortURL" value="wiretransferimportresults.jsp?ImportWireTransfers.ToggleSortedBy=DueDate" URLEncrypt="false"/>
										<!--L10NStart-->Date<!--L10NEnd--><a onclick="ns.wire.sortImportedResults('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
									</td>
				<ffi:setProperty name="SortImage" property="Compare" value="WirePayee.PayeeName"/>
									<td class="sectionsubhead" width="220">
										<ffi:setProperty name="SortURL" value="wiretransferimportresults.jsp?ImportWireTransfers.ToggleSortedBy=WirePayee.PayeeName" URLEncrypt="false"/>
										<!--L10NStart-->Beneficiary<!--L10NEnd--><a onclick="ns.wire.sortImportedResults('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
									</td>
				<ffi:setProperty name="SortImage" property="Compare" value="FromAccountNum"/>
									<td class="sectionsubhead" width="180">
										<ffi:setProperty name="SortURL" value="wiretransferimportresults.jsp?ImportWireTransfers.ToggleSortedBy=FromAccountNum" URLEncrypt="false"/>
										<!--L10NStart-->Account<!--L10NEnd--><a onclick="ns.wire.sortImportedResults('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
									</td>
				<ffi:setProperty name="SortImage" property="Compare" value="Status"/>
									<td class="sectionsubhead" width="85">
										<ffi:setProperty name="SortURL" value="wiretransferimportresults.jsp?ImportWireTransfers.ToggleSortedBy=Status" URLEncrypt="false"/>
										<!--L10NStart-->Status<!--L10NEnd--><a onclick="ns.wire.sortImportedResults('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
									</td>
				<ffi:setProperty name="SortImage" property="Compare" value="AMOUNT"/>
									<td class="sectionsubhead" align="right" width="85">
										<ffi:setProperty name="SortURL" value="wiretransferimportresults.jsp?ImportWireTransfers.ToggleSortedBy=AMOUNT" URLEncrypt="false"/>
										<!--L10NStart-->Amount<!--L10NEnd--><a onclick="ns.wire.sortImportedResults('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
									</td>
									<td class="sectionsubhead" width="100">&nbsp;</td>
								</tr>

								<ffi:list collection="ImportWireTransfers" items="wireTransfer">
									<tr>
										<td class="columndata"><ffi:getProperty name="wireTransfer" property="DueDate"/></td>
										<td class="columndata">
										<ffi:getProperty name="wireTransfer" property="WirePayee.PayeeName"/>
										</td>
										<td class="columndata">

                                                <%-- check to see if the account is in the Accounts collection, so we can show its NickName --%>
                                                <ffi:setProperty name="BankingAccounts" property="Filter" value="ID=${wireTransfer.FromAccountID}"/>
                                                <ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="notEquals">
                                                    <ffi:list collection="BankingAccounts" items="acct" startIndex="1" endIndex="1">
                                                        <ffi:getProperty name="acct" property="DisplayText"/> -
                                                        <ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
                                                            <ffi:getProperty name="acct" property="NickName"/> -
                                                        </ffi:cinclude>
                                                        <ffi:getProperty name="acct" property="CurrencyCode"/>
                                                    </ffi:list>
                                                </ffi:cinclude>
                                                <%-- if the account is not in the Accounts collection, it's not entitled.  Show the account type instead. --%>
                                                <ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="equals">
                                                    <ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="session"/>
                                                    <ffi:setProperty name="AccountDisplayTextTask" property="AccountID" value="${wireTransfer.FromAccountID}"/>
                                                    <ffi:process name="AccountDisplayTextTask"/>
                                                    <ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
                                                    <ffi:removeProperty name="AccountDisplayTextTask"/>
                                                </ffi:cinclude>
                                                <ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>

										</td>
										<td class="columndata"><ffi:getProperty name="wireTransfer" property="StatusName"/></td>
										<td class="columndata" align="right"><ffi:getProperty name="wireTransfer" property="Amount"/></td>
										<td class="columndata" align="right" nowrap>
										</td>
									</tr>
								</ffi:list>
								<tr>
									<td colspan="6"><br></td>
								</tr>
								<tr>
									<td colspan="6" align="center" nowrap>
										<%-- 
										<input type="button" class="submitbutton" value="DONE" onclick="document.location='<ffi:urlEncrypt url="wiretransfers.jsp?Refresh=true" />';return false;">
										--%>
										<sj:a 
											id = "wireFileUploadResultDoneButton"
			                                button="true" 
			                                onClickTopics="wireUploadDoneOnClickTopics"
					                        ><s:text name="jsp.default_175" /><!-- DONE -->
					            		</sj:a>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>

			</table>
		</div>
