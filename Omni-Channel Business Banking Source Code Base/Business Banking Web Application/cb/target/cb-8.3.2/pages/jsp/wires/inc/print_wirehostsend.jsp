<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../common/wire_labels.jsp"%>

<ffi:setProperty name='PageHeading' value='Host Wire Transfer Print'/>
<ffi:help id="payments_wirehostconfirm" className="moduleHelpClass"/>
<%
String task = "wireTransfer";
//if (task == null) task = "AddHostWire";
%>

    <div align="center">
        <table width="670" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="left" class="tbrd_b">&nbsp;</td>
            </tr>
            <tr>
                <td class="tbrd_lrb" width="670">
                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td colspan="4" height="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                        </tr>
                        <tr>
                            <td align="left" width="5" class="sectionsubhead">&nbsp;</td>
                            <td align="left" width="325" class="sectionsubhead">Host Wire Transfer Confirm</td>
                            <td align="left" colspan="2" class="sectionsubhead">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" class="sectionsubhead">&nbsp;</td>
                            <td align="left" class="sectionsubhead"><ffi:getProperty name="SecureUser" property="BusinessName"/></td>
                            <td align="left" width="90" class="sectionsubhead"><%= LABEL_TRACKING_ID %>:</td>
                            <td align="left" width="215" class="sectionsubhead"><ffi:getProperty name="<%= task %>" property="TrackingID"/></td>
                        </tr>
                        <tr>
                            <td align="left" class="sectionsubhead">&nbsp;</td>
                            <td align="left" class="sectionsubhead">
                                <ffi:process name="GetCurrentDate"/>
                                <ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateTimeFormat}"/>
                                <ffi:getProperty name="GetCurrentDate" property="Date"/>
                            </td>
                            <td align="left" class="sectionsubhead"><%= LABEL_APPLICATION_ID %>:</td>
                            <td align="left" class="sectionsubhead"><ffi:getProperty name="<%= task %>" property="ID"/></td>
                        </tr>
                        <tr>
                            <td align="left" colspan="4" height="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" class="tbrd_lr">&nbsp;</td>
            </tr>
            <tr>
                <td class="tbrd_lr">
                    <div align="center">
                        <table border="0" cellspacing="0" cellpadding="2" width="635">
                            <tr>
                                <td align="left" width="60" class="sectionsubhead">Host ID</td>
                                <td align="left" width="100" class="columndata"><ffi:getProperty name="<%= task %>" property="HostID"/></td>
                                <td align="left" width="170" class="sectionsubhead">Requested Processing Date</td>
                                <td align="left" width="285" class="columndata"><ffi:getProperty name="<%= task %>" property="DueDate"/></td>
                            </tr>
                            <tr>
                                <td align="left" class="sectionsubhead">Amount</td>
                                <td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="Amount"/></td>
                                <td align="left" class="sectionsubhead">Settlement Date</td>
                                <td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="SettlementDate"/></td>
                            </tr>
                            <tr>
                                <td align="left" class="sectionsubhead">&nbsp;</td>
                                <td align="left" class="columndata">&nbsp;</td>
                                <td align="left" class="sectionsubhead">Expected Processing Date</td>
                                <td align="left" class="columndata"><ffi:getProperty name="<%= task %>" property="DateToPost"/></td>
                            </tr>
                        </table>
                    </div>
                </td>
            <tr>
                <td align="left" class="tbrd_lr">&nbsp;</td>
            </tr>
            <tr>
                <td class="tbrd_ltr sectionsubhead" height="20" width="670">
                    <table border="0" cellspacing="0" cellpadding="2" width="100%">
                    <tr>
                        <td align="left" width="5" class="sectionsubhead">&nbsp;</td>
                        <td align="left" width="665" class="sectionsubhead">Confidential</td>
                    </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="tbrd_t">&nbsp;</td>
            </tr>
        </table>
    </div>
