<%--
This page will add a templated wire to a batch being edited.

Pages that request this page
----------------------------
wirebatcheditaddtemplate.jsp
	ADD WIRE button

Pages this page requests
------------------------
Javascript auto-refreshes to wirebatchedittemplate.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%
if( request.getParameter("templateID") != null ){ session.setAttribute("templateID", request.getParameter("templateID")); }
if( request.getParameter("amount") != null ){ session.setAttribute("amount", request.getParameter("amount")); }
if( request.getParameter("beneficiaryName") != null ){ session.setAttribute("beneficiaryName", request.getParameter("beneficiaryName")); }
%>
<%-- Load a wire from selected template --%>
<ffi:object id="LoadWireTemplate" name="com.ffusion.tasks.wiretransfers.LoadWireTemplate" scope="session"/>
        <ffi:setProperty name="LoadWireTemplate" property="TemplateId" value="${templateID}"/>
        <ffi:setProperty name="LoadWireTemplate" property="TemplatesSessionName" value="WireTemplates"/>
        <ffi:setProperty name="LoadWireTemplate" property="BeanSessionName" value="WireTransfer"/>
<ffi:process name="LoadWireTemplate"/>
<ffi:removeProperty name="LoadWireTemplate"/>
<%-- Create an AddWireTransfer object and copy loaded wire --%>
<ffi:object id="AddWireTransfer" name="com.ffusion.tasks.wiretransfers.AddWireTransfer" scope="session"/>
        <ffi:setProperty name="AddWireTransfer" property="BeanSessionName" value="WireTransfer"/>
        <ffi:setProperty name="AddWireTransfer" property="LoadFromTemplate" value="true"/>
        <ffi:setProperty name="AddWireTransfer" property="Initialize" value="true"/>
<ffi:process name="AddWireTransfer"/>
<%-- Copy over the amount into AddWireTransfer --%>
<ffi:setProperty name="AddWireTransfer" property="Amount" value="${amount}"/>
<%-- Validate the loaded wire --%>
<ffi:setProperty name="AddWireTransfer" property="ValidateAmount" value="true"/>
<ffi:process name="AddWireTransfer"/>
<%-- Add the wire to batch if validation succeded --%>
<ffi:object id="AddWireTransferToBatch" name="com.ffusion.tasks.wiretransfers.AddWireTransferToBatch"/>
        <ffi:setProperty name="AddWireTransferToBatch" property="CollectionSessionName" value="ModifyWireBatch"/>
        <ffi:setProperty name="AddWireTransferToBatch" property="BeanSessionName" value="AddWireTransfer"/>
<ffi:process name="AddWireTransferToBatch"/>
<%-- <html><head><script>location.replace("<ffi:getProperty name='wireBackURL'/>");</script></head><body></body></html>--%>
