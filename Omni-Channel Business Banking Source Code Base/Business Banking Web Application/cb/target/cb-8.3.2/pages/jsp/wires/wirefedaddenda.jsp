<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object name="com.ffusion.tasks.wiretransfers.ImportWireAddenda" id="ImportAddenda" scope="session"/>
<%
    session.setAttribute("FFIImportAddendaTask", session.getAttribute("ImportAddenda"));
%>

<div align="center">
    <table width="282" border="0" cellspacing="0" cellpadding="0" style="margin-top: 15px;">
        <tr>
            <td class="columndata lightBackground" colspan="2">
                <div align="left">
                    <span class="columndata"><!--L10NStart-->Enter file path or click browse and navigate to file<!--L10NEnd--></span></div>
            </td>
        </tr>
        <tr>
            <td class="columndata lightBackground" colspan="2">

                <s:actionerror />
                <s:fielderror />
                <s:actionmessage />
                <s:form id="WireImportAddendaForm" validate="false" name="FileForm" enctype="multipart/form-data" theme="simple" namespace="/pages/jsp/wires" action="FedWireImportAddendaAction" method="post">
                    <s:file class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" name="userImage" value="" maxlength="255" size="40" accept="text/*" />
                </s:form>
				<div class="submitButtonsDiv">
					<sj:a id="closeWireImportAddendaLink" button="true"
						  onClickTopics="closeWireImportAddenda"
						  title="Cancel">
						  	<s:text name="jsp.default_82" />
					</sj:a>
					<sj:submit id="WireImportAddendaButton" button="true"
						  formIds="WireImportAddendaForm"
						  targets="ImportAddendaResult"
						  title="Import Addenda"
						  onBeforeTopics="beforeWireImportAddendaForm"
						  onErrorTopics="errorWireImportAddendaForm"
						  onSuccessTopics="successWireImportAddendaForm"
						  value="Import Addenda"/>
				</div>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
	$("#ImportAddendaResult").html("");     // clear previous contents
</script>