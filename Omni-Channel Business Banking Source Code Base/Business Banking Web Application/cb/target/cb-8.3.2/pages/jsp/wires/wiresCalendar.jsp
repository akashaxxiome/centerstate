<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.util.DateConsts"%>		
	<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
    <ffi:process name="GetCurrentDate"/>			
    <%-- Format and display the current date/time --%>
	<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="<%= DateConsts.FULL_CALENDAR_DATE_FORMAT %>"/>
	
	<%-- <ffi:setGridURL grid="WIRES_Calendar_Event_URL" name="WIRES_Calendar_Event_URL" url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID={0}&transType={1}&recurringId={2}&collectionName=PendingWireTransfers" parm0="ID" parm1="transType" parm2="RecurringID"/> --%>
	
	<ffi:setGridURL grid="WIRES_Calendar_Event_URL" name="ViewURL" url="/cb/pages/jsp/wires/viewWireTransferAction_calendarData.action?ID={0}&transType={1}&recurringId={2}" parm0="tranId" parm1="wireTransferType" parm2="recWireTransferId"/>
	<%-- <ffi:setGridURL grid="WIRES_Calendar_Event_URL" name="ViewURL" url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID={0}&transType={1}&recurringId={2}" parm0="tranId" parm1="paymentType" parm2="recPaymentId"/> --%>
	<ffi:setGridURL grid="WIRES_Calendar_Event_URL" name="ViewURLInternational" url="/cb/pages/jsp/wires/viewWireBatch.action?batchId={0}" parm0="tranId" />
	<ffi:setGridURL grid="WIRES_Calendar_Event_URL" name="ViewURLNotInternational" url="/cb/pages/jsp/wires/viewWireBatch.action?batchId={0}" parm0="tranId" />
	<%-- <ffi:setGridURL grid="WIRES_Calendar_Event_URL" name="ViewURLInternational" url="/cb/pages/jsp/wires/viewWireBatch.action?batchId={0}&collectionName=PendingApprovalWireTransfers" parm0="ID" />
	<ffi:setGridURL grid="WIRES_Calendar_Event_URL" name="ViewURLNotInternational" url="/cb/pages/jsp/wires/viewWireBatch.action?batchId={0}&collectionName=PendingApprovalWireTransfers" parm0="ID" /> --%>	
	
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar-extra.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.print.css' media='print' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.overrides.css' />
	
	<!-- <script type='text/javascript' src='/cb/web/js/calendar/fullcalendar.js'></script> -->
	<style type="text/css">
		.fc td.fc-sat{
			width: 122px;
			}
	</style>
	<script type='text/javascript'>
		$(document).ready(function() {
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		var dateArray=[];
		$('#wiresCalendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},			
			editable: true,
			disableDragging:true,
			events: "<ffi:urlEncrypt url='/cb/pages/jsp/calendar/getCalendarEvents.action?moduleName=WiresCalendar&GridURLs=WIRES_Calendar_Event_URL' />",
			eventClick: function(calEvent, jsEvent, view){
				if(view.name == "month"){
					moveToDay(calEvent.start);
				}else if(view.name == "basicDay"){
					var temp = $(this);
					var isLoaded = false;						
					for(var i =0;i<$.parseHTML(this.innerHTML).length;i++){
						if(($.parseHTML(this.innerHTML)[i].localName =="table"
								&& $.parseHTML(this.innerHTML)[i].className.indexOf("tableAlerternateRowColor")!="-1") ||
								($.parseHTML(this.innerHTML)[i].localName =="div" && $.parseHTML(this.innerHTML)[i].id =="wireBatchView")){
							isLoaded = true;
							break;
						}						
					}
					if(!isLoaded){
						clearData();
					 /* if(temp.html().indexOf('tableAlerternateRowColor')=="-1"){ */
						 if(calEvent.wireTransferType=="SINGLE" || "RECURRING"==calEvent.wireTransferType){
								transactionSummaryUrl = calEvent.ViewURL;
						}else if(calEvent.wireTransferType=="BATCH"){
							if(calEvent.destination="International"){
								transactionSummaryUrl  = calEvent.ViewURLInternational;
							}else{
								transactionSummaryUrl = calEvent.ViewURLNotInternational;
							}
						}
						 $.ajax({
							   url: transactionSummaryUrl,
							   success: function(data){
								   temp.append(data);
								   var lHeight = $(temp).parent().height();
								   $("div .fc-content").height(lHeight+50);
								}
						});
					 }
				}
			},
					
			eventRender: function(event, element) {
				//removing css class for events o
				element.removeClass("fc-event fc-event-skin");
				element.find("div").removeClass("fc-event fc-event-skin");
				var view  =  $('#wiresCalendar').fullCalendar('getView');
				if(view.name == "month"){
					var y = event.start;
					var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
					var cal = $("#wiresCalendar")[0];
					var data = $.data(cal,d.toString());
					if(dateArray.indexOf(d)=='-1'){
						dateArray.push(d);
						var holderDiv = "<div>";
						var text = '';
						for (var key in event.calendarDateData) {
							  if (event.calendarDateData.hasOwnProperty(key)) {
								 /*  text = text + holderDiv;
								  text= text+" " +event.calendarDateData[key] + " "+ key; */
								  if(event.calendarDateData[key] > 0) {
									  if(key == "Pending" || key == "Skipped") {
										  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarPendingApprovalRecordCls sapUiIconCls icon-history'>";
									  } else if(key == "Pending Approval") {
										  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarPendingRecordCls sapUiIconCls icon-pending'>";
									  } else if (key == "Completed") {
										  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
									  }
	
									  text = text + holderDiv;
									  text = text+" " +event.calendarDateData[key] + " "+ key;
									  text += "</div>";
								  }
							  }
						}
						$(".completedevent").css("position", "absolute");
						element.find('.fc-event-title').html(text);
						$.data(cal,d.toString(),data);
						}
					else{
						return false;
					}
				}
				if(view.name == "basicWeek") {
					text = "";
					for (var key in event.calendarDateData) {
						  if (event.calendarDateData.hasOwnProperty(key)) {
							  if(event.calendarDateData[key] > 0) {
								  if(key == "Pending" || key == "Skipped") {
									  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewPendingApprovalRecordCls sapUiIconCls icon-history'>";
								  } else if(key == "Pending Approval") {
									  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewPendingRecordCls sapUiIconCls icon-pending'>";
								  } else if (key == "Completed") {
									  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
								  }

								  text = text + holderDiv;
								  text = text+" " +event.title;
								  text += "</div>";
							  }	  
						  }
					}
					$(".completedevent").css("position", "absolute");
					element.find('.fc-event-title').html(text);	
				}
				if(view.name == "basicDay"){
					text = "";
					var statusName = ""; 
					if(this.statusName.trim() == "Pending" || this.statusName.trim() == "Scheduled" || this.statusName.trim() ==="Created"  || this.statusName.trim() == "Skipped") {
						statusName = "<div class='marginRight20 marginleft10 floatleft'>"+this.statusName+"<span class='marginleft5 calendarPendingApprovalRecordCls sapUiIconCls icon-history'></span></div>";
					} else if(this.statusName.trim() == "Pending Approval") {
						statusName = "<div class='marginRight20 floatleft'>"+this.statusName+"<span class='marginleft5 calendarPendingApprovalRecordCls sapUiIconCls icon-history'></span></div>";
					} else if (this.statusName.trim() == "Completed") {
						statusName = "<div class='marginRight20 floatleft'>"+this.statusName+"<span class='marginleft5 calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'></span></div>";
					}
					var referenceNumber = "<div class='marginRight20 floatleft heading'>"+js_reference_number+": <span class='boldIt'>"+this.referenceNumber+"</span></div>";
					var displayText= "<span class='marginleft10 sapUiIconCls icon-arrow-right'></span>";
					if(this.wireTransferType =="BATCH"){
						displayText = this.wireTransferType;
					}
					var account = "<div class='marginRight20 floatleft'>"+js_account+":  <span class='boldIt'>"+this.fromAccount+
					displayText
					+this.payeeName+"</span></div>";
					var amount = "<div class='marginRight20 floatleft'>"+js_amount+":  <span class='boldIt'>"+this.amount+ " " +event.currencyCode +"</span></div>";
					
					var existingTitle = this.title;
					
					var newTitle = "<span class='heading'>"+statusName+referenceNumber+account+amount+"<span>";
					
					//$(".completedevent").addClass("completedeventDayView");
					$("div[class*='fc-view-basicDay'] div .fc-event-hori").addClass("completedeventDayView");
					element.find('.fc-event-title').html(newTitle);
				}
			},
			
			eventAfterRender :function( event, element, view ) { 
				var y = event.start;
				var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
				var cal = $("#wiresCalendar")[0];
				$.removeData(cal,d.toString());		
				dateArray.length=0;
			},
			
			windowResize: function(view) {
				/* $('#wiresCalendar').fullCalendar('refetchEvents');
				$('#wiresCalendar').fullCalendar('render'); */
			}
			
		});	
		});
		function moveToDay(date){
			var toDate = new Date(date);
			$('#wiresCalendar').fullCalendar( 'changeView', 'basicDay' );
			$('#wiresCalendar').fullCalendar( 'gotoDate', toDate );
		}
				
		function getServerDate(){
		    var date = new Date('<ffi:getProperty name="GetCurrentDate" property="Date" />');
			return date;
		} 
		
		function clearData(){
			$('.tableAlerternateRowColor').each(function(){
				$(this).slideToggle();
				$(this).remove();
			});
		}
	</script>

<style type='text/css'>

		label {
			margin-top: 40px;
			text-align: left;
			font-size: 14px;
			font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
			}

		#wiresCalendar {
			width: 95%;
			margin: 0 auto;
			}
			
		/* style added to increase the height of the content when a transaction is expanded for DayView */
		
		.fc-view-basicDay {
			height: 100%;
		}
	
		.fc-border-separate {
		     height: 100%;
		}	
		
		.tableAlerternateRowColor{
			cursor: default;
		}
	</style>
	<div id='wiresCalendar'></div>
