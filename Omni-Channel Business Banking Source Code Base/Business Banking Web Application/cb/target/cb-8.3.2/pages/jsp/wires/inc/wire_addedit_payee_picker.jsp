<%--
This file contains the Select Beneficiary drop-down list used on the add/edit
wire transfer pages

It is included on all add/edit wire transfer pages (except Host)
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp
	wiretransfernew.jsp

It includes the following file:
common/wire_labels.jsp
	The labels for fields and buttons
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../common/wire_labels.jsp"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames" %>
<%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<% String wireDest = ""; String wpid = ""; String wpscope = ""; String beneficiaryType = ""; %>
<ffi:getProperty name="${wireTask}" property="WireDestination" assignTo="wireDest"/>
<ffi:getProperty name="${beneficiaryType}" property="beneficiaryType" assignTo="beneficiaryType"/>
<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="wpid"/>
<ffi:getProperty name="WireTransferPayee" property="PayeeScope" assignTo="wpscope"/>
<style type="text/css">
.beneficiaryComboHolder .combobox{width:80%; margin:0 0 10px !important;}
</style>

<%
if (wpid == null) wpid = "";
if (wpscope == null) wpscope = "";
%>
<%-- ------ BENEFICIARY PICK LIST: WirePayeeID ------ --%>
<table border="0" cellspacing="0" cellpadding="3" width="100%">

	<%--<tr>
	<% if( !wireDest.equals(WireDefines.WIRE_INTERNATIONAL ) ) { %>
		 <td colspan="2" class="tbrd_b sectionhead">&gt; <%= wireDest.equals(WireDefines.WIRE_DRAWDOWN) ? "<!--L10NStart-->Debit Bank Information<!--L10NEnd-->" : "<!--L10NStart-->Send Wire To<!--L10NEnd-->" %></td> 
	<% } else { %> 
	<%-- <td colspan="1" class="tbrd_b sectionhead"><!--L10NStart-->Send Wire To<!--L10NEnd--></td>
	<td  class="sectionhead tbrd_b ltrow2_color" align="right"> 
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FOREIGN_EXCHANGE_UTILITY %>">
					&nbsp;
					<sj:a 
						tooltip="Foreign Exchange Rate"
				    	onclick="ns.common.openDialog('fx');"
				    	button="true"
						>
						<img src="/cb/web/multilang/grafx/common/i_fx.gif" alt="" width="33" height="14" border="0">
				    </sj:a>
			</ffi:cinclude>
	</td> <% } %> </tr>--%>
		
	
				<%String payeeID= "";%> 
		
				<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="payeeID"/> 
				
				<ffi:setProperty name="WireTransferPayees" property="Filter" value="ID=${payeeID}"/>

				<ffi:cinclude value1="${WireTransferPayees.Size}" value2="0" operator="equals">
				<ffi:setProperty name="disableManagePayee" value="disabled"/>
				</ffi:cinclude>
				<ffi:cinclude value1="${WireTransferPayees.Size}" value2="0" operator="notEquals">
				<ffi:setProperty name="disableManagePayee" value=""/>
				</ffi:cinclude>
                <ffi:cinclude value1="${disableSemi}" value2="disabled">
                    <ffi:setProperty name="disableManagePayee" value="disabled"/>
                </ffi:cinclude>

				<ffi:setProperty name="WireTransferPayees" property="Filter" value="All"/>
				
	
	
	<%-- Start: Dual approval processing --%>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">			
		<ffi:setProperty name="IS_DA_TRANSFER" value="true"/>		

		<%-- If DA is enabled keep manage beneficiary button disabled --%>
		<ffi:setProperty name="disableManagePayee" value="disabled"/>		
		
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:setProperty name="IS_DA_TRANSFER" value="false"/>
		</ffi:cinclude>			
	</ffi:cinclude>	
	<%-- End: Dual approval processing --%>

	<tr>
		<%-- <td class="sectionsubhead" width="12%">
			<%=  wireDest.equals(WireDefines.WIRE_DRAWDOWN) ? LABEL_SELECT_DRAWDOWN_ACCOUNT : "Beneficiary :" %>
		</td> --%>
		
		<td width="75%">
			<%-- <% String curPayee = ""; String thisPayee = ""; String payeeName=""; String nickName="";String accountNum="";String wirePayeeID="";%>
			<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="curPayee"/>
			<ffi:getProperty name="WireTransferPayee" property="PayeeName" assignTo="payeeName" encodeAssign="true"/>
			<ffi:getProperty name="WireTransferPayee" property="NickName" assignTo="nickName" encodeAssign="true"/>
			<ffi:getProperty name="WireTransferPayee" property="AccountNum" assignTo="accountNum" encodeAssign="true"/>
			<ffi:getProperty name="WireTransferPayee" property="ID" assignTo="wirePayeeID"/>

			<% if (curPayee == null) curPayee = ""; %>

			Set current payee Id in session
			<ffi:setProperty name="currentPayeeId" value="<%=curPayee%>"/>
			<ffi:cinclude value1="" value2="${currentPayeeId}" operator="equals">
				<ffi:setProperty name="currentPayeeId" value="<%= WireDefines.CREATE_NEW_BENEFICIARY%>"/>			
			</ffi:cinclude>
				<div class="selectBoxHolder beneficiaryComboHolder">
			<s:if test="%{beneficiaryType=='managed' || beneficiaryType==null}">
				<select class="txtbox" id="selectWirePayeeID" name="<ffi:getProperty name="wireTask"/>.WirePayeeID" size="1" onChange="setPayee(this.options[this.selectedIndex].value)" <ffi:getProperty name="disableSemi"/>>
						<%
						if(curPayee!=null && !curPayee.isEmpty() && !curPayee.equals(WireDefines.WIRE_LOOKUPBOX_DEFAULT_VALUE) 
							&& !curPayee.equals(WireDefines.CREATE_NEW_BENEFICIARY) && !curPayee.equals(WireDefines.CREATE_NEW_DEBIT_ACCOUNT)){
							String label = payeeName;
							if(nickName!=null && !nickName.equalsIgnoreCase("")){
								label = label +" (" + nickName + " - "+ accountNum + ")";
							}else{
								label = label +" (" + accountNum + ")";
							}
							
						%>
						<option value="<%=curPayee%>" selected="selected"><%=label %></option>
						<%
						} else { 
						String label = WireDefines.CREATE_NEW_BENEFICIARY;
						%>
						<option value="<%=label%>" selected="selected"></option>
						<% } %>
			</select>
			</s:if>
			<s:else>
				<select class="txtbox" id="selectWirePayeeID" name="<ffi:getProperty name="wireTask"/>.WirePayeeID" size="1" onChange="setPayee(this.options[this.selectedIndex].value)" <ffi:getProperty name="disableSemi"/>>
						<%
						if(curPayee!=null && !curPayee.isEmpty() && !curPayee.equals(WireDefines.WIRE_LOOKUPBOX_DEFAULT_VALUE) 
							&& !curPayee.equals(WireDefines.CREATE_NEW_BENEFICIARY) && !curPayee.equals(WireDefines.CREATE_NEW_DEBIT_ACCOUNT)){
							String label = payeeName;
							if(nickName!=null && !nickName.equalsIgnoreCase("")){
								label = label +" (" + nickName + " - "+ accountNum + ")";
							}else{
								label = label +" (" + accountNum + ")";
							}
							
						%>
						<option value="<%=curPayee%>" selected="selected"><%=label %></option>
						<%
						} else { 
						String label = wireDest.equals(WireDefines.WIRE_DRAWDOWN) ? WireDefines.CREATE_NEW_DEBIT_ACCOUNT : WireDefines.CREATE_NEW_BENEFICIARY;
						%>
						<option value="<%=label%>" selected="selected"><%=label%></option>
						<% } %>
			</select>
			</s:else>
			</div> 
			<ffi:cinclude value1="${WireTransferPayee.PayeeScope}" value2="<%= WireDefines.PAYEE_SCOPE_UNMANAGED %>" operator="equals">
				<ffi:cinclude value1="<%= wpid %>" value2="" operator="notEquals">
					<input type="Hidden" name="<ffi:getProperty name="wireTask"/>.Action" value="mod">
					<input type="Hidden" name="WireTransferPayee.Action" value="mod">
				</ffi:cinclude>
			</ffi:cinclude>--%>
			<!-- <input type="button" id="openBeneficiaryDetails" onclick="openBeneficiaryDetail();" value="Show Details" disabled/> -->
			<%-- <span class="sapUiIconCls icon-drill-down" id="expand" onclick="openBeneficiaryDetail();" title="Show Beneficiary Details">Expand</span> --%>
			<div class="selecteBeneficiaryMessage">
				<span><s:text name="jsp.wire.No_beneficiary_selected" /></span>
				<%-- <sj:a button="true"
				id="disbaledExpandBtn"
				disabled="true"
				cssStyle="float:right"
				><span class="sapUiIconCls icon-positive"></span>Expand
			</sj:a> --%>
			</div>
			<a id="expand" onclick="openBeneficiaryDetail()" class="hidden nameTitle" title="Show Beneficiary Details">
				<ffi:getProperty name="WireTransferPayee" property="PayeeName"/> 
					<span class="nameSubTitle"> (<s:if test='%{WireTransferPayee.nickName!=""}'><ffi:getProperty name="WireTransferPayee" property="nickName"/> -</s:if>
					<ffi:getProperty name="WireTransferPayee" property="AccountNum"/>) <span class="sapUiIconCls icon-positive"></span></span>
			</a>
			<a id="collapse" onclick="openBeneficiaryDetail()" class="hidden nameTitle" title="Hide Beneficiary Details">
				<ffi:getProperty name="WireTransferPayee" property="PayeeName"/> 
					<span class="nameSubTitle">(<s:if test='%{WireTransferPayee.nickName!=""}'><ffi:getProperty name="WireTransferPayee" property="nickName"/> -</s:if>
					<ffi:getProperty name="WireTransferPayee" property="AccountNum"/>) <span class="sapUiIconCls icon-negative"></span></span>
			</a>
			<%-- <sj:a id="collapse" 
				button="true"
				onclick="openBeneficiaryDetail()"
				cssClass="hidden"
				cssStyle="display:none; float:right"
				title="Hide Beneficiary Details"
				><span class="sapUiIconCls icon-up"></span>Collapse
			</sj:a> --%>
					
			<span class="sapUiIconCls icon-drill-up hidden" id="collapse" onclick="openBeneficiaryDetail();" title="Hide Beneficiary Details"></span>
			<!-- <input type="button" name="payeeDetails" value="Add New" onclick="loadBeneficiaryForm('Create New Beneficiary','freeForm')" id="freeForm" style="margin-bottom:20px"/> -->
			<!-- <input type="button" name="payeeDetails" value="Add New" onclick="loadBeneficiaryForm()" id="freeForm" style="margin-bottom:20px"/> -->
			<%-- <sj:a name="payeeDetails" id="freeForm" onclick="loadBeneficiaryForm()" button="true" cssStyle="margin-left:10px">
				<span class="sapUiIconCls icon-add"></span>Add New
			</sj:a> --%>
		</td>
	
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BENEFICIARY_MANAGEMENT %>">
		<% if (!wpid.equals("") && !wpscope.equals(WireDefines.PAYEE_SCOPE_UNMANAGED)) { %>
		<td align="right" width="25%" id="manageBeneficiaryButtonIDHolder"><%-- <input type="button" name="wireBankSearch" class="submitbutton" value="MANAGE BENEFICIARY" <ffi:getProperty name="disableManagePayee" />--%>
			<ffi:cinclude value1="${disableManagePayee}" value2="disabled" operator="notEquals">
					<sj:a id="manageBeneficiaryButtonID" 
						button="true"
						name="wireBankSearch" 										
						onClickTopics="manageBeneficiaryClickTopic"
						cssClass="floatRight"
					><s:text name="jsp.default_270" /><!-- MANAGE BENEFICIARY -->
					</sj:a>
			</ffi:cinclude>
			<ffi:cinclude value1="${disableManagePayee}" value2="disabled" operator="equals">	
				<input id="manageBeneficiaryButtonID" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-disabled" type="Button"  value="MANAGE BENEFICIARY" >
			</ffi:cinclude>
			<% String curPayeeforEncryption = ""; String returnPage = ""; %>
			<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="curPayeeforEncryption"/>			
			<ffi:setProperty name="currentPayeeIdforEncryption" value="<%=curPayeeforEncryption%>"/>	
			<input type="hidden" id="urlIDForJS" value='<ffi:urlEncrypt url="modifyWirebeneficiary_init.action?payeeId=${currentPayeeIdforEncryption}&payeeDestination=${wireDest}&returnPage=${returnPage}&manageBeneficiary=true"/>'/>
		</td>	
		<% } else { %>		
		<% } %>
		</ffi:cinclude>
		<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_BENEFICIARY_MANAGEMENT %>">
		<% if (!wpid.equals("") && !wpscope.equals(WireDefines.PAYEE_SCOPE_UNMANAGED) && !wpscope.equals(WireDefines.PAYEE_SCOPE_BUSINESS)) { %>	
		<td align="right" width="25%">	
			<ffi:cinclude value1="${disableManagePayee}" value2="disabled" operator="notEquals">
					<sj:a id="manageBeneficiaryButtonID" 
						button="true"
						name="wireBankSearch" 										
						onClickTopics="manageBeneficiaryClickTopic"
						cssClass="floatRight"
					>
						MANAGE BENEFICIARY
					</sj:a>
			</ffi:cinclude>
			<ffi:cinclude value1="${disableManagePayee}" value2="disabled" operator="equals">	
				<input id="manageBeneficiaryButtonID" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-disabled" type="Button"  value="MANAGE BENEFICIARY" >	
			</ffi:cinclude>
			<% String curPayeeforEncryption = ""; String returnPage = ""; %>
			<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="curPayeeforEncryption"/>			
			<ffi:setProperty name="currentPayeeIdforEncryption" value="<%=curPayeeforEncryption%>"/>
			<input type="hidden" id="urlIDForJS" value='<ffi:urlEncrypt url="modifyWirebeneficiary_init.action?payeeId=${currentPayeeIdforEncryption}&payeeDestination=${wireDest}&returnPage=${returnPage}&manageBeneficiary=true"/>'/>
		</td>	
		<% } else if(!wpid.equals("")){ %>		
			<td align="right" width="25%">			
			<sj:a id="manageBeneficiaryButtonID" 
				button="true"
				name="wireBankSearch" 				
				onClickTopics=""
				cssClass="disabledBtnStyle floatRight"
				disabled="true"
			>
				MANAGE BENEFICIARY
			</sj:a>
			</td>
		<% }else{ %>
		<% } %>
		</ffi:cinclude>
	</tr>
</table>
<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<s:if test="%{#request.PayeePendingInDA}"> 
		<div class="errorLabel" style="margin:0 0 10px">&nbsp;&nbsp;&nbsp;&nbsp;* <s:property value="%{#request.PayeePendingInDAMessage}"/></div>
	</s:if>	
</ffi:cinclude>
<%-- End: Dual approval processing --%>
<%-- 

<script>
	var wireDestination = "<ffi:getProperty name="${wireTask}" property="WireDestination"/>";
	var PAYEE_DESTINATION_DA = "<ffi:getProperty name="PAYEE_DESTINATION_DA" />";
	var isSimplified = $('#beneficiaryType').val();
	var value = "";	
	
	/* if (wireDestination == "DRAWDOWN") {
		value = {value:"Create New Debit Account", label:js_create_new_debit_account};		
	} else if(isSimplified){
		// do nothings
	}
	else {
	value = {value:"Create New Beneficiary", label:js_create_new_beneficiary};		
	} */
	
	$("#selectWirePayeeID").lookupbox({
		"source":"/cb/pages/jsp/wires/wireTransferPayeeLookupBoxAction.action?wireDestination="+wireDestination,
		"controlType":"server",
		"size":"35",
		"module":"wireLoadPayee",
		"hint":"Select Beneficiary"
	});
</script>
 --%>
<ffi:removeProperty name="PAYEE_DESTINATION_DA"/>
