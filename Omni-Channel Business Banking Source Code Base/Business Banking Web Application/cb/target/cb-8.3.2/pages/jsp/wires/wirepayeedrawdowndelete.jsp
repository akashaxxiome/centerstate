<%--
This is the delete drawdown wire beneficiary page.

Pages that request this page
----------------------------
wirepayees.jsp

Pages this page requests
------------------------
CANCEL requests wirepayees.jsp
DELETE BENEFICIARY requests wirepayeedeletesend.jsp

Pages included in this page
---------------------------
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>

<ffi:help id="payments_wirepayeedrawdowndelete" className="moduleHelpClass"/>

<ffi:setProperty name="DimButton" value="beneficiary"/>
<%--<ffi:include page="${PathExt}payments/inc/wire_buttons.jsp"/>--%>

	<ffi:object id="DeleteWireTransferPayee" name="com.ffusion.tasks.wiretransfers.DeleteWireTransferPayee" scope="session"/>
		
	<%
		session.setAttribute("FFIDeleteWireTransferPayee", session.getAttribute("DeleteWireTransferPayee"));
	%>
    

<%-- Start: Dual approval processing --%>

<%-- End: Dual approval processing --%>
<s:form action="deleteWireBeneficiary_delete" method="post" name="FormName" id="deleteWireBeneficiaryFormId" namespace="/pages/jsp/wires">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="payeeId" value="<ffi:getProperty name="WireTransferPayee" property="ID"/>"/>
<div class="blockWrapper label130">
	<div  class="blockHead"><!--L10NStart-->Debit Account Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel">Debit Account Name:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Name"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel">Nickname:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="NickName"/></span>
			</div>
		</div>
	<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel">Address 1:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel">Address 2:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Street2"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel">City:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="City"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel">State:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="State"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel">ZIP/Postal Code:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="ZipCode"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel">Contact Person:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="Contact"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel">Beneficiary Type:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeDestination"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel">Beneficiary Scope:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="PayeeScope"/></span>
			</div>
		</div>
		
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel">Account Number:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountNum"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel">Account Type:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="AccountTypeDisplayName"/></span>
			</div>
		</div>
</div>
</div>
<div class="blockWrapper label130">
	<div  class="blockHead"><!--L10NStart-->Debit Bank Info<!--L10NEnd--></div>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel">Debit Bank Name:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.BankName"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel">Address 1:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel">Address 2:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.Street2"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel">City:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.City"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span  class="sectionsubhead sectionLabel">State:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.State"/></span>
			</div>
			<div class="inlineBlock">
				<span  class="sectionsubhead sectionLabel">ZIP/Postal Code:</span>
				<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.ZipCode"/></span>
			</div>
		</div>
		<div class="blockRow">
			<span  class="sectionsubhead sectionLabel">Debit Bank ABA:</span>
			<span  class="columndata"><ffi:getProperty name="WireTransferPayee" property="DestinationBank.RoutingFedWire"/></span>
		</div>
	</div>
</div>
<div class="blockWrapper">
	<div  class="blockHead"><!--L10NStart-->Comments<!--L10NEnd--></div>
<div class="blockContent">
		<div class="blockRow">
			<ffi:getProperty name="WireTransferPayee" property="Memo" />
		</div>
	</div>
</div>
<div align="center" class="marginTop10"><!--L10NStart-->Are you sure you want to delete this drawdown account?<!--L10NEnd--></div>
<div class="btn-row">
<sj:a 
	id="cancelWireBeneficiaryLink" 
	button="true" 
	onClickTopics="closeDialog" 
	title="Cancel"
	><s:text name="jsp.default_82" />
</sj:a>

<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<sj:a 
		id="deleteWireBeneficiaryLink" 
		formIds="deleteWireBeneficiaryFormId" 
		targets="resultmessage" 
		button="true"   
		title="Delete Beneficiary" 
		onSuccessTopics="daDeleteWireBeneficiaryCompleteTopics"
		onCompleteTopics="refreshWireBeneficiariesGridSuccessTopics" 
		effectDuration="1500" 
		><s:text name="jsp.default_DELETE_BENEFICIARY" /><!-- DELETE BENEFICIARY -->
	</sj:a>									
</ffi:cinclude>
<%-- End: Dual approval processing --%>

<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
	<sj:a 
		id="deleteWireBeneficiaryLink" 
		formIds="deleteWireBeneficiaryFormId" 
		targets="resultmessage" 
		button="true"   
		title="Delete Beneficiary" 
		onCompleteTopics="deleteWireBeneficiarySuccessTopics" 
		onSuccessTopics="deleteWireBeneficiaryCompleteTopics" 
		effectDuration="1500" 
		><s:text name="jsp.default_DELETE_BENEFICIARY" /><!-- DELETE BENEFICIARY -->
	</sj:a>									
</ffi:cinclude>
<%-- End: Dual approval processing --%>	
</div>
</s:form>