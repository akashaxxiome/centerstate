<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

		<span class="sectionhead">&gt; <s:text name="jsp.default.label.wire.import.results"/></span><br/><br/>
		<span class="sectionhead"><s:text name="jsp.default_421"/><br/><br/></span>
		
		<ffi:cinclude value1="${ImportErrors}" value2="" operator="notEquals">
		
			<s:include value="%{#session.PagesPath}/common/fileImportErrorsGrid.jsp"/>
	
		</ffi:cinclude>
	
		<span class="sectionhead"> <s:text name="jsp.default.label.fix.errors"/></span><br/><br/>
	<div valign="bottom">
		<div align="center">
			<sj:a 
			button="true" 
			onClickTopics="closeDialog"
			><s:text name="jsp.default_111"/></sj:a>
		</div>
	</div>
	<br/>
	
		
		 
