<%--
This page processes the delete wire transfer request

Pages that request this page
----------------------------
wiretransferdelete.jsp
wirebookdelete.jsp
wiredrawdowndelete.jsp
wirefreddelete.jsp
	DELETE TRANSFER button

Pages this page requests
------------------------
Javasript auto-refreshes to one of the following:
	wiretransfers.jsp
	wiretemplates.jsp
	wirebatchff.jsp
	wirebatcheditff.jsp
	wirebatchedittemplate.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object id="DeleteWireTransfer" name="com.ffusion.tasks.wiretransfers.DeleteWireTransfer" scope="session"/>
<ffi:process name="DeleteWireTransfer"/>

<%--
<ffi:removeProperty name="DeleteWireTransfer"/>
<ffi:removeProperty name="ID"/>
<ffi:removeProperty name="recID"/>
--%>
<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="notEquals">
	<ffi:setProperty name="tmp_url" value="${SecurePath}payments/wiretransfers.jsp?Refresh=true" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:cinclude value1="${deleteTemplate}" value2="true" operator="equals">
	<ffi:setProperty name="tmp_url" value="${SecurePath}payments/wiretemplates.jsp" URLEncrypt="true"/>
</ffi:cinclude>
<ffi:removeProperty name="deleteTemplate"/>

<script>document.location='<ffi:getProperty name="tmp_url"/>'</script>
<ffi:removeProperty name="tmp_url"/>

