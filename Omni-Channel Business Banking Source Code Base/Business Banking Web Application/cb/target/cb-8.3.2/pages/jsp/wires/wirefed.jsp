<%--
This page is used for the following operations:
	Add and Edit fed wire transfer
	Add and Edit fed wire transfer to batch
	Add and Edit fed wire transfer template

Pages that request this page
----------------------------
wiretransfers.jsp (wire transfer summary)
	NEW WIRE button (when fed wire is selected in Wire Type)
wire_transfers_list.jsp (included in wiretransfers.jsp)
	Edit button next to a fed wire transfer
wiretransferconfirm.jsp (wire transfer confirm)
	BACK button
wirebatchff.jsp (add freeform wire batch)
	ADD WIRE button
	Edit button next to a wire transfer
wirebatcheditff.jsp (edit freeform wire batch)
	ADD WIRE button
	Edit button next to a wire transfer
wirebatchtemp.jsp (add template batch)
	Edit button next to a wire transfer
wirebatchedittemplate.jsp (edit template batch)
	Edit button next to a wire transfer
wire_templates_list.jsp (included in wiretemplates.jsp)
	Load button next to a fed wire template
	Edit button next to a fed wire template
wire_type_select.jsp (included in wire_common_init.jsp, which is in turn included
	in wiretransfernew.jsp, wirebook.jsp, wiredrawdown.jsp,	and wirefed.jsp)
	Changing the Wire Type to triggers a javascript function called setDest() in
	wire_scripts_js.jsp
wireaddpayee.jsp
	CANCEL button

Pages this page requests
------------------------
LOAD TEMPLATE requests wirefed.jsp (itself)
MANAGE BENEFICIARY requests wireaddpayee.jsp
SEARCH requests one of the following:
	wirebanklist.jsp
	wirebanklistselect.jsp
ADD INTERMEDIARY/RECEIVING BANK requests wirebanklist.jsp
CANCEL requests one of the following:
	wiretransfers.jsp
	wirebatchff.jsp
	wirebatchtemp.jsp
	wirebatcheditff.jsp
	wirebatchtemplateedit.jsp
	wiretemplates.jsp
SUBMIT WIRE/SUBMIT TEMPLATE requests wiretransferconfirm.jsp
Choose Wire Type requests one of the following:
	wiretransfernew.jsp
	wirebook.jsp
	wiredrawdown.jsp
	wirehost.jsp
Select Beneficiary requests wirefed.jsp (itself)
Calendar button requests calendar.jsp

Pages included in this page
---------------------------
payments/inc/wire_common_init.jsp
	Common task initialization for all wire types
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/wire_scripts_js.jsp
	Common javascript functions used for all wire types
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_template_picker.jsp
	Template drop-down list
payments/inc/wire_template_fields.jsp
	Template fields, used when creating wire templates
payments/inc/wire_addedit_template_info.jsp
	Read-only template info, used when loading a template
payments/inc/wire_addedit_payee_picker.jsp
	Select Beneficiary drop-down list and MANAGE BENEFICIARY button
payments/inc/wire_addedit_payee_fields.jsp
	Beneficiary name & address fields
payments/inc/wire_addedit_bank_fields.jsp
	Beneficiary destination bank name & address fields
payments/inc/wire_addedit_intermediary_banks.jsp
	Beneficiary intermediary bank list and ADD BENEFICIARY BANK button
payments/inc/wire_addedit_payment_fields.jsp
	Wire amount, date, and recurring fields
payments/inc/wire_addedit_comment_fields.jsp
	Wire transfer comment and By Order Of feilds
common/checkAmount_js.jsp
	A common javascript that validates an amount is formatted properly
--%>
<%@ page import="com.ffusion.tasks.wiretransfers.ModifyWireTransfer" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/wire_labels.jsp"%>

<ffi:help id="payments_wirefed" className="moduleHelpClass"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%-- ----------------------------------------------------------------------------------------------- --
NOTE:  This page is for ADD and MODIFY wire transfers, ADD and MODIFY wire transfer templates, ADD and
MODIFY wire transfers in a new batch, and ADD and MODIFY wire transfers in an existing batch.
The object it's utilizing is in the session called "wireTask":
	"AddWireTransfer" for add single wire.
	"ModifyWireTransfer" for edit single wire.
	"WireTransfer" for add and edit wires part of a batch.
If a string is in the session called "templateTask", and it's one of the following values, it will
treat the wire transfer as a wire transfer template:
	"add" for add wire template.
	"edit" for edit wire template.
If a string is in the session called "wireBatch", and it's one of the following values, it will
treat the wire transfer as a part of a batch:
	"AddWireBatch" for wires included in a new batch.
	"ModifyWireBatch" for wires included in an existing batch.
---------------------------------------------------------------------------------------------------- --%>

<%
if( request.getParameter("notTemplate") != null ) { session.removeAttribute("templateTask"); }
if( request.getParameter("wireTask") != null && request.getParameter("wireTask").length() > 0 ){ session.setAttribute("wireTask", request.getParameter("wireTask")); }
if( request.getParameter("wireDestination") != null && request.getParameter("wireDestination").length() > 0 ){ session.setAttribute("wireDestination", request.getParameter("wireDestination")); }
if( request.getParameter("DA_WIRE_PRESENT") != null && request.getParameter("DA_WIRE_PRESENT").length() > 0 ){ session.setAttribute("DA_WIRE_PRESENT", request.getParameter("DA_WIRE_PRESENT")); }
if( request.getParameter("DontInitialize") != null && request.getParameter("DontInitialize").length() > 0 ){ session.setAttribute("DontInitialize", request.getParameter("DontInitialize")); }
if( request.getParameter("setPayee") != null && request.getParameter("setPayee").length() > 0 ){ session.setAttribute("setPayee", request.getParameter("setPayee")); }
if( request.getParameter("delIntBank") != null && request.getParameter("delIntBank").length() > 0 ){ session.setAttribute("delIntBank", request.getParameter("delIntBank")); }
if( request.getParameter("selectedCountry") != null && request.getParameter("selectedCountry").length() > 0 ){ session.setAttribute("selectedCountry", request.getParameter("selectedCountry")); }
if( request.getParameter("templateTask") != null && request.getParameter("templateTask").length() > 0 ){ session.setAttribute("templateTask", request.getParameter("templateTask")); }
//***Fix QTS:661548 & 662132.Start***
//CR Descriptions:Wire template use button does not work correct, works as clone.
//The session should be removed when it not used.
else if(request.getParameter("userTemplate") != null){ session.removeAttribute("templateTask");}
//***Fix QTS:661548 & 662132.End***
if( request.getParameter("ID") != null && request.getParameter("ID").length() > 0 ){ session.setAttribute("ID", request.getParameter("ID")); }
if( request.getParameter("recID") != null && request.getParameter("recID").length() > 0 ){ session.setAttribute("recID", request.getParameter("recID")); }
if( request.getParameter("collectionName") != null && request.getParameter("collectionName").length() > 0 ){ session.setAttribute("collectionName", request.getParameter("collectionName")); }
if( request.getParameter("wireInBatch") != null && request.getParameter("wireInBatch").length() > 0 ){ session.setAttribute("wireInBatch", request.getParameter("wireInBatch")); }
if( request.getParameter("LoadFromTransferTemplate") != null && request.getParameter("LoadFromTransferTemplate").length() > 0 ){ session.setAttribute("LoadFromTransferTemplate", request.getParameter("LoadFromTransferTemplate")); }
if( request.getParameter("wireBatch") != null ){ session.setAttribute("wireBatch", request.getParameter("wireBatch")); }
if( request.getParameter("wireIndex") != null ){ session.setAttribute("wireIndex", request.getParameter("wireIndex")); }
if( request.getParameter("DontInitializeBatch") != null ){ session.setAttribute("DontInitializeBatch", request.getParameter("DontInitializeBatch")); }
if( request.getParameter("editWireTransfer") != null ){ session.setAttribute("editWireTransfer", request.getParameter("editWireTransfer")); }
%>

<%-- hints for bookmark --%>
<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
	<span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTransferLink,newSingleWiresTransferID</span>
	<span class="shortcutEntitlementClass" style="display:none;">Wires</span>
</ffi:cinclude>
<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
	<span class="shortcutPathClass" style="display:none;">pmtTran_wire,quickNewWireTemplateLink,newSingleWiresTemplateID</span>
	<span class="shortcutEntitlementClass" style="display:none;">Wires</span>
</ffi:cinclude>
<ffi:setProperty name="CallerForm" value="WireNew"/>
<ffi:setProperty name="CallerURL" value="wirefed.jsp?DontInitialize=true" URLEncrypt="true"/>

<%-- Initialize the Wire Transfer, common strings, & other objects.  Plus other common code (like the set payee code) --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_common_init.jsp"/>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wirefed.jsp?DontInitialize=true" URLEncrypt="true" />

<%-- Set TaskErrorURL for appropriate task so that this page gets reloaded to display task errors --%>
<ffi:cinclude value1="${wireTask}" value2="AddWireTransfer">
    <ffi:setProperty name="AddWireTransfer" property="TaskErrorURL" value="${BackURL}"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireTask}" value2="ModifyWireTransfer">
    <ffi:setProperty name="ModifyWireTransfer" property="TaskErrorURL" value="${BackURL}"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireTask}" value2="WireTransfer">
    <ffi:setProperty name="WireTransferTask" property="TaskErrorURL" value="${BackURL}"/>
</ffi:cinclude>

<ffi:removeProperty name="LoadFromTransferTemplate" />

<%-- Start: Dual approval processing --%>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:setProperty name="PAYEE_DESTINATION_DA" value="<%= WireDefines.PAYEE_TYPE_REGULAR %>"/>
</ffi:cinclude>

<%-- End: Dual approval processing --%>

		<s:include value="%{#session.PagesPath}/wires/inc/nav_menu_top_js.jsp" />
		<s:include value="%{#session.PagesPath}/wires/inc/wire_scripts_js.jsp" />
		
		<div align="center"><ul id="formerrors"></ul>
<div class="leftPaneWrapper">
<%-- ------ LOAD TEMPLATE PICK LIST ------ --%>
<!-- below if condition added to hide load template page when coming from load/clone template or edit wire -->
<s:if test='%{#session.wireTransfer.ID=="" && #session.wireTransfer.wireType!="TEMPLATE"}'>
	<div id="wireTemplateDiv">
	<s:include value="%{#session.PagesPath}/wires/inc/wire_template_picker.jsp" />
	<input type="hidden" id="TEMP_CSRF_TOKEN" name="TEMP_CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	<input type="hidden" id="isManagePayeeModified" name="isManagePayeeModified" />
	</div>
</s:if>
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
<div class="leftPaneInnerWrapper" role="form" aria-labelledby="addWireBeneficiaryHeader">
<div class="header"><h2 id="addWireBeneficiaryHeader">Add Wire Beneficiary*</h2></div>
<div class="leftPaneInnerBox">

<script>
	var wireDestination = "<ffi:getProperty name="${wireTask}" property="WireDestination"/>";
	var PAYEE_DESTINATION_DA = "<ffi:getProperty name="PAYEE_DESTINATION_DA" />";
	var isSimplified = $('#beneficiaryType').val();
	var value = "";	
	
	/* if (wireDestination == "DRAWDOWN") {
		value = {value:"Create New Debit Account", label:js_create_new_debit_account};		
	} else if(isSimplified){
		// do nothings
	}
	else {
	value = {value:"Create New Beneficiary", label:js_create_new_beneficiary};		
	} */
	
	$("#selectWirePayeeID").lookupbox({
		"source":"/cb/pages/jsp/wires/wireTransferPayeeLookupBoxAction.action?wireDestination="+wireDestination,
		"controlType":"server",
		"size":"35",
		"module":"wireLoadPayee"
	});
</script>
<%-- ------ BENEFICIARY PICK LIST: WirePayeeID ------ --%>
	<% String curPayee = ""; String thisPayee = ""; String payeeName=""; String nickName="";String accountNum="";String wirePayeeID="";%>
	<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="curPayee"/>
	<ffi:getProperty name="WireTransferPayee" property="PayeeName" assignTo="payeeName" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="NickName" assignTo="nickName" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="AccountNum" assignTo="accountNum" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="ID" assignTo="wirePayeeID"/>

	<% if (curPayee == null) curPayee = ""; %>

	<%-- Set current payee Id in session --%>
	<ffi:setProperty name="currentPayeeId" value="<%=curPayee%>"/>
	<ffi:cinclude value1="" value2="${currentPayeeId}" operator="equals">
		<ffi:setProperty name="currentPayeeId" value="<%= WireDefines.CREATE_NEW_BENEFICIARY%>"/>			
	</ffi:cinclude>
	<div class="selectBoxHolder beneficiaryComboHolder">
	<s:if test="%{beneficiaryType=='managed' || beneficiaryType==null}">
		<select class="txtbox" id="selectWirePayeeID" name="" size="1" onChange="setPayee(this.options[this.selectedIndex].value)" <ffi:getProperty name="disableSemi"/>>
				<%
				if(curPayee!=null && !curPayee.isEmpty() && !curPayee.equals(WireDefines.WIRE_LOOKUPBOX_DEFAULT_VALUE) 
					&& !curPayee.equals(WireDefines.CREATE_NEW_BENEFICIARY) && !curPayee.equals(WireDefines.CREATE_NEW_DEBIT_ACCOUNT)){
					String label = payeeName;
					if(nickName!=null && !nickName.equalsIgnoreCase("")){
						label = label +" (" + nickName + " - "+ accountNum + ")";
					}else{
						label = label +" (" + accountNum + ")";
					}
					
				%>
				<option value="<%=curPayee%>" selected="selected"><%=label %></option>
				<%
				} else { 
				String label = WireDefines.CREATE_NEW_BENEFICIARY;
				%>
				<option value="<%=label%>" selected="selected"></option>
				<% } %>
	</select>
	</s:if>
	</div>
	<% String wireType = null; 
		String isRecModel = null;
	%>

	<ffi:getProperty name="${wireTask}" property="WireType" assignTo="wireType"/>
	<ffi:getProperty name="isRecModel" assignTo="isRecModel"/>
	<div align="center">
		<div style="margin: 0 0 5px;">OR</div>
		<sj:a name="payeeDetails" id="freeForm" onclick="loadBeneficiaryForm()" button="true">
			<s:text name="jsp.wire.freeForm"/>
		</sj:a>	</div>
</div>	</div>
		<%if (WireDefines.WIRE_TYPE_RECURRING.equals(wireType) && !"true".equals(isRecModel)) { %>
		<ffi:cinclude value1="${IsModify}" value2="true">
			<div class="marginTop20 floatleft fxActionBtnCls"><div class="leftPaneInnerWrapper" style="border:none !important;"><div align="center"><sj:a id="loadRecModelFormButton"
			                button="true"
			                onclick="selectRecModel();" 
			            ><s:text name="jsp.default.edit_orig_trans"/></sj:a></div></div></div>
			            </ffi:cinclude>
		<% } %>

	</ffi:cinclude>
</div>
<div class="rightPaneWrapper">
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
				
				<tr>
					<td class="ltrow2_color" align="center">
						<%--
						<form action="wiretransferconfirm.jsp" method="post" name="WireNew">
						--%>
						<s:form id="newWiresTransferFormID" namespace="/pages/jsp/wires" validate="false" action="%{#attr.ValidateAction}" method="post" name="WireNew" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<input id="isRecModel" name="isRecModel" value="<ffi:getProperty name="isRecModel"/>" type="hidden"/>
						<input id="recurringId" name="recurringId" value="<ffi:getProperty name="recurringId"/>" type="hidden"/>
						<input id="ID" name="ID" value="<ffi:getProperty name="ID"/>" type="hidden"/>
						<input id="transType" name="transType" value="<ffi:getProperty name="transType"/>" type="hidden"/>
						<input type="hidden" name="DontInitialize" value="true"/>
						<input type="hidden" name="BatchEntry" value="<ffi:getProperty name='isBatchEntry'/>"/>
						<input type="hidden" name="isFEDWIRE" value="FEDWIRE" id="isFEDWIRE"/>
						<input type="hidden" id="returnPage" value="<ffi:getProperty name='returnPage'/>"/>
						<input type="hidden" name ="bankLookupRaturnPage" id="bankLookupRaturnPage" value="<ffi:getProperty name='returnPage'/>"/>
						<input type="hidden" id="setPayeeAction" value="<ffi:getProperty name='setPayeeAction'/>"/>
						<input type="hidden" id="deleteIntermediaryBankAction" value="<ffi:getProperty name='deleteIntermediaryBankAction'/>"/>
               			<input type="hidden" name="delIntBank" value=""/>
               			<input type="hidden" name="selectedCountry" value=""/>
						<input type="hidden" name="<ffi:getProperty name="wireTask"/>.Type" value="<ffi:getProperty name="${wireTask}" property="Type"/>">
						<input type="hidden" name="<ffi:getProperty name="wireTask"/>.WireType" value="<ffi:getProperty name="${wireTask}" property="WireType"/>">
						<input type="hidden" name="<ffi:getProperty name="wireTask"/>.WireDestination" value="<ffi:getProperty name="${wireTask}" property="WireDestination"/>">
						<input type="hidden" name="WireTransferPayee.PayeeDestination" value="<%= WireDefines.PAYEE_TYPE_REGULAR %>">
						<input type="hidden" name="WireTransferPayee.Country" value="UNITED STATES">
						<input type="hidden" name="WireTransferPayee.DestinationBank.Country" value="UNITED STATES">
						<input type="hidden" name="disableRec" value="<ffi:getProperty name="disableRec" />"/>
						<input type="hidden" name="disableBat" value="<ffi:getProperty name="disableBat" />"/>
						<input type="hidden" name="disableDates" value="<ffi:getProperty name="disableDates" />"/>
						<input type="hidden" name="isWireBatch" value="<ffi:getProperty name="isWireBatch" />"/>
						<input type="hidden" name="wireBatchUrl" value="<ffi:getProperty name="wireBatchUrl" />"/>
						<input type="hidden" name="<ffi:getProperty name="wireTask"/>.WirePayeeID" id="payeeID">

    <%-- Display task error if one exists --%>
    <ffi:cinclude value1="${wireTask}" value2="AddWireTransfer">
        <% request.setAttribute("ErrorTask", session.getAttribute("AddWireTransfer")); %>
		<%--
        <ffi:include page="${PathExt}inc/page-error.jsp"/>
		--%>
    </ffi:cinclude>
    <ffi:cinclude value1="${wireTask}" value2="ModifyWireTransfer">
        <% request.setAttribute("ErrorTask", session.getAttribute("ModifyWireTransfer")); %>
		<%--
        <ffi:include page="${PathExt}inc/page-error.jsp"/>
		--%>
    </ffi:cinclude>
    <ffi:cinclude value1="${wireTask}" value2="WireTransfer">
	 <% request.setAttribute("ErrorTask", session.getAttribute("WireTransferTask")); %>
	 <%--
	 <ffi:include page="${PathExt}inc/page-error.jsp"/>
	 --%>
    </ffi:cinclude>
    

    
	 <%    String wireSource = "";    %>
	<ffi:getProperty name="${wireTask}" property="wireSource" assignTo="wireSource"/>
	<ffi:cinclude value1="${wireSource}" value2="TEMPLATE" operator="equals">
		<div class="paneWrapper" role="form" aria-labelledby="wireTemplateHeader">
			<div  class="header"><h2 id="wireTemplateHeader">Template Details</h2></div>
		    <div class="paneInnerWrapper">
		        <div class="paneContentWrapper">
		        	<%-- ------ TEMPLATE (READ-ONLY) WireName, NickName, TemplateID, DisplayWireLimit, & WireScope ------ --%>
					<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_template_info.jsp"/>
				</div>
			</div>
		</div>
		<div class="marginTop10"></div>
	</ffi:cinclude>
	<%-- ------ BENEFICIARY PICK LIST: WirePayeeID ------ --%>
<div class="paneWrapper" style="margin:0 0 10px !important" role="form" aria-labelledby="wireBeneficiaryHeader">
       	<div class="paneInnerWrapper">
      		<div class="header"><h2 id="wireBeneficiaryHeader"><s:text name="jsp.default_70" /></h2></div>
       		<div class="paneContentWrapper">
				<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_picker.jsp"/>
				<table width="100%" cellpadding="0" cellspacing="0" border="0" id="fedwireHiddenJsp" class="hidden">
					<tr>
						<td width="357" valign="top">
							<%-- ------ PAYEE FIELDS: PayeeName, Street, Street2, City, State, ZipCode, Country, ContactName, PayeeScope, AccountNum, AccountType ------ --%>
							<ffi:setProperty name="WireTransferPayee" property="PayeeDestination" value="<%= WireDefines.PAYEE_TYPE_REGULAR %>"/>
							<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_fields.jsp"/>
						</td>
						</tr>
						<tr><td valign="top"><hr class="hidden thingrayline lineBreak" /></td></tr>
						<tr>
							<td width="348" valign="top">
								<%-- ------ BENEFICIARY BANK FIELDS (WireTransferPayee.DestinationBank):  BankName, Street, Street2, City, State, ZipCode, Country, RoutingFedWire, RoutingSwift, RoutingChips, RoutingOther, IBAN, Correspondent Bank Account Number ------ --%>
								<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_bank_fields.jsp"/>
							</td>
						</tr>
					</table>
						<%-- ------ INTERMEDIARY BANKS LIST ------ --%>
						<div id="intermediaryBanksDivID" class="hidden">
							<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_intermediary_banks.jsp"/>
						</div>
		</div>
	</div>
</div>

<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
	<%-- ------ TEMPLATE FIELDS: WireName, NickName, WireCategory, & WireScope ------ --%>
	<s:include value="%{#session.PagesPath}/wires/inc/wire_template_fields.jsp"/>
</ffi:cinclude>
<div class="paneWrapper" role="form" aria-labelledby="wirePaymentDetailHeader">
  	<div class="paneInnerWrapper">
  		<div class="header"><h2 id="wirePaymentDetailHeader">Payment Details</h2></div>
  		<div class="paneContentWrapper">	
			<%-- ------ DOMESTIC PAYMENT & REPEATING FIELDS:  FromAccountID, Amount/OrigAmount, DueDate, Frequency, NumberTransfers ------ --%>
			<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payment_fields.jsp"/>
		</div>
	</div>
</div>

	<div class="marginTop10"></div>
	<%-- ------ COMMENT FIELDS:  Comment, OrigToBeneficiaryInfo1 - OrigToBeneficiaryInfo4, BankToBankInfo1 - BankToBankInfo6 ------ --%>
	<div align="left"><s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_comment_fields.jsp"/></div>
								<span class="required">&nbsp;<br>
								* <!--L10NStart-->indicates a required field<!--L10NEnd--><br>
								</span>

	<ffi:setProperty name="checkAmount_acctCollectionName" value="WiresAccounts"/>
	<%-- include javascript for checking the amount field. Note: Make sure you include this file after the collection has been populated.--%>
	<s:include value="%{#session.PagesPath}/common/checkAmount_js.jsp" />
	<div class="btn-row">
								<%--
								<input type="button" class="submitbutton" value="CANCEL" onclick="document.location='<ffi:getProperty name="wireBackURL"/>'">
								--%>								
								<s:if test="%{!isWireBatch}">
									<ffi:cinclude value1="${wireSource}" value2="TEMPLATE">
										<sj:a id="cancelFormButtonOnInput" 
											button="true" summaryDivId="summary" buttonType="cancel" 
											onClickTopics="showSummary,cancelWireTransferForm"
											>
											<s:text name="jsp.default_82"/>
										</sj:a>		
									</ffi:cinclude>
									<ffi:cinclude value1="${wireSource}" value2="TEMPLATE" operator="notEquals">
										<sj:a id="cancelFormButtonOnInput" 
											button="true" summaryDivId="summary" buttonType="cancel" 
											onClickTopics="showSummary,cancelWireTransferForm"
											>
											<s:text name="jsp.default_82"/>
										</sj:a>									
									</ffi:cinclude>										
								</s:if>				
								
								<script>									
									ns.wire.cancelWireURLFromWireBatch = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/${wireBackURL}"/>' ;
								</script>
								<s:if test="%{isWireBatch}">
									<s:hidden id="backToBatchURLForJS" value="%{#session.wireBackURL}"></s:hidden>
	   								<sj:a id="cancelAddWireOnBatchFormID" 
										button="true" 
										onClickTopics="cancelAddWireOnBatchFormClickTopics"
									>
										<s:text name="jsp.default_82"/>
									</sj:a>
								</s:if>
								<s:if test="%{#request.templateTask ==''}">
									<s:set name="WireButtonLabel" value="%{getText('jsp.default_395')}"/>
								</s:if>	
								<s:else>
									<s:set name="WireButtonLabel" value="%{getText('jsp.default_366')}"/>
								</s:else>
								<%-- Start: Dual approval processing --%>

								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">	
									<s:if test="%{#request.PayeePendingInDA}"> 
										<input id="verifyDAWiresTransferNoSubmit" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-disabled" type="Button"  value='<s:property value="%{#WireButtonLabel}" />' >
									</s:if>	
									<s:else>
										<sj:a
												id="verifyDAWiresTransferSubmit"
												formIds="newWiresTransferFormID"
				                                targets="verifyDiv"
				                                button="true"
				                                validate="true"
				                                validateFunction="customValidation"
				                                onBeforeTopics="beforeVerifyWireTransferForm"
				                                onCompleteTopics="verifyWireTransferFormComplete"
												onErrorTopics="errorVerifyWireTransferForm"
				                                onSuccessTopics="successVerifyWireTransferForm"
												onClick="SelectAmount()"
						                        ><s:property value="%{#WireButtonLabel}" />
						            	</sj:a>
									</s:else>																	
								</ffi:cinclude>

								<%-- End: Dual approval processing--%>
								
								<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">

									 <%--
									 <input type="button" class="submitbutton" value="<ffi:getProperty name="wireSaveButton"/>" onclick="if (SelectAmount()) document.WireNew.submit();">
									 --%>	
									 <sj:a 
												id="verifyWiresTransferSubmit"
												formIds="newWiresTransferFormID"
				                                targets="verifyDiv" 
				                                button="true" 
				                                validate="true" 
				                                validateFunction="customValidation"
				                                onBeforeTopics="beforeVerifyWireTransferForm"
				                                onCompleteTopics="verifyWireTransferFormComplete"
												onErrorTopics="errorVerifyWireTransferForm"
				                                onSuccessTopics="successVerifyWireTransferForm"
												onclick="SelectAmount()"
						                        ><s:property value="%{#WireButtonLabel}" />
						            </sj:a>

								</ffi:cinclude>
		</div>
							</s:form>
					</td>
				</tr>
			</table>
</ffi:cinclude>
		</div>
</div>
<ffi:object id="GetStateProvinceDefnForState" name="com.ffusion.tasks.util.GetStateProvinceDefnForState" scope="session"/>
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
<script>
	$(document).ready(function(){
		$("input[name='isBeneficiary']").each( function(){$(this).remove();} );
		var isRecModel = '<ffi:getProperty name="isRecModel" />';
		if(isRecModel == 'true') {
			enableRepeating(<ffi:getProperty name="${wireTask}" property="FrequencyValue"/>);
		}
		if($("#selectWirePayeeID").val()!=null && $("#selectWirePayeeID").val()!="Create New Beneficiary"){
			$(".selecteBeneficiaryMessage").toggle();
			$('#expand').toggle();
		}
	});
	
	$( "#disbaledExpandBtn" ).button({ disabled: true });
	
	function openBeneficiaryDetail(){
		$('#fedwireHiddenJsp').slideToggle();
		$('#collapse').toggle();
		$('#expand').toggle();
	}
	
	function loadBeneficiaryForm(){
		var optionsStr = $("<option value='Create New Beneficiary'></option>");
		var firstOption = $('#selectWirePayeeID').find("option:first");
		$(optionsStr).insertBefore(firstOption);
		$('#selectWirePayeeID').find("option[value='Create New Beneficiary']").attr("selected","selected");
		setPayee('Create New Beneficiary');
		$('.lineBreak').slideToggle();
		$('#intermediaryBanksDivID').slideToggle();
		$('#collapse').toggle();
		$('#expand').toggle();
	}

		
</script>

</ffi:cinclude>
