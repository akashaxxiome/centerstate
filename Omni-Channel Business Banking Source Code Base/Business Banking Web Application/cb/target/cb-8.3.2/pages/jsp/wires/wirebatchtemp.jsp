<%--
This page is for adding a template wire batch

Pages that request this page
----------------------------
wirebatch.jsp (add wire batch)
	As an included file
wirebatchtempconfirm.jsp (template wire batch confirm (non-international))
wirebatchinttempconfirm.jsp (template international wire batch confirm)
	BACK button
wiretransfernew.jsp (add/edit domestic and international wire)
wirebook.jsp (add/edit book wire)
wiredrawdown.jsp (add/edit drawdown wire)
wirefed.jsp (add/edit fed wire)
wiretransferconfirm.jsp (add/edit wire transfer confirm)
	CANCEL button
wiretransfersend.jsp (add/edit wire transfer final send/save page)
	Javascript auto-refresh

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
SUBMIT WIRE BATCH requests one of the following:
	wirebatchtempconfirm.jsp (for template batches (non-international))
	wirebatchinttempconfirm.jsp (for freeform international batches)
NEXT PAGE requests wirebatchtemp.jsp (itself)
PREVIOUS PAGE requests wirebatchtemp.jsp (itself)
Edit button next to wires requests one of the following:
	wiretransfernew.jsp (domestic and international wire)
	wirefed.jsp (fed wire)
	wiredrawdown.jsp (drawdown wire)
	wirebook.jsp (book wire)
Calendar button requests calendar.jsp

Pages included in this page
---------------------------
payments/inc/wire_batch_select.jsp
	Creates the Choose Wire Batch Type drop-down
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_batch_fields.jsp
	edit fields for a non-international wire
payments/inc/wire_batch_int_fields.jsp
	edit fields for an international wire
--%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.ffusion.beans.wiretransfers.WireDefines"%>
<%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatchtemp" className="moduleHelpClass"/>
<%
Map map = request.getParameterMap(); 
Iterator it = (Iterator) map.entrySet().iterator();
while (it.hasNext()) {
	Map.Entry entry =(Entry) it.next();
	String key =  (String)entry.getKey();
	String[] value = (String[])entry.getValue();
	session.setAttribute(key, value[0]);
}
%>
<ffi:setL10NProperty name='PageHeading' value='New Batch Wire Transfer'/>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/addWireBatchTemplateAction_reload.action" URLEncrypt="true"  />

<ffi:setProperty name="wireBackURL" value="addWireBatchTemplateAction_reload.action?DontInitializeBatch=true" URLEncrypt="true" />
<ffi:setL10NProperty name="wireSaveButton" value="ADD TO BATCH"/>
<ffi:setProperty name="wireBatchItemsPerPage" value="10"/>
<ffi:setProperty name="wirebatch_confirm_form_url" value="addWireBatchTemplateAction"/>

<% String task = "AddWireBatch"; String itemsPerPageStr = ""; String pageStr = ""; String pagesStr = "";
   String nxtPage = ""; String prePage = ""; String maxPageTraversed = ""; %>
<ffi:getProperty name="wireBatchItemsPerPage" assignTo="itemsPerPageStr"/>
<ffi:getProperty name="templatePage" assignTo="pageStr"/>
<ffi:getProperty name="templatePages" assignTo="pagesStr"/>
<ffi:getProperty name="nextPage" assignTo="nxtPage"/>
<ffi:getProperty name="prevPage" assignTo="prePage"/>
<ffi:getProperty name="maxPgTraversed" assignTo="maxPageTraversed"/>
<%

if (nxtPage == null) nxtPage = "";
if (prePage == null) prePage = "";
int itemsPerPage = Integer.parseInt(itemsPerPageStr);

// get current page number
if (pageStr == null) pageStr = "0";
int pageNum = Integer.parseInt(pageStr);

// get total pages
if (pagesStr == null) pagesStr = "0";
int pagesNum = Integer.parseInt(pagesStr);

// calculate starting and ending wire for this current page
int startNum = pageNum * itemsPerPage;
int endNum = startNum + itemsPerPage;
int pageDelta = 0;
int count;

// if next or previous was clicked, adjust the page number
if (nxtPage.equals("true")) pageDelta = 1;
if (prePage.equals("true")) pageDelta = -1;

// if next or previous was clicked, and remember the checkboxes and adjust the page number
// Put checked checkboxes in the session with a value of "true", unchecked ones "false"
if (pageDelta != 0) {
	for (count = startNum; count < endNum; count++) {
		String checkbox = "checkbox_" + String.valueOf(count);
		if (request.getParameter(checkbox) != null) { %>
			<ffi:setProperty name="<%= checkbox %>" value="true"/>
<%		} else { %>
			<ffi:setProperty name="<%= checkbox %>" value="false"/>
<%		}
	}
	pageNum += pageDelta;
	if (pageNum > pagesNum) pagesNum = pageNum;
	startNum = pageNum * itemsPerPage;
	endNum = startNum + itemsPerPage;
	
	int maxPageTraversedInt = 0;
	if(maxPageTraversed!=null && !maxPageTraversed.equals("")){
		maxPageTraversedInt = Integer.parseInt(maxPageTraversed);	
	}
	
	if(maxPageTraversedInt<pageNum){
		maxPageTraversedInt = pageNum;
	}
%>
	<ffi:setProperty name="templatePage" value="<%= String.valueOf(pageNum) %>"/>
	<ffi:setProperty name="templatePages" value="<%= String.valueOf(pagesNum) %>"/>
	<ffi:setProperty name="maxPgTraversed" value="<%= String.valueOf(maxPageTraversedInt) %>"/>
<%
}
%>
<ffi:removeProperty name="nextPage"/>
<ffi:removeProperty name="prevPage"/>

	<%-- <ffi:cinclude value1="${GetWireTemplates.Type}" value2="${wireDestination}" operator="notEquals">
	<%-- Get the templates -- --%>
	<ffi:object id="GetWireTemplates" name="com.ffusion.tasks.wiretransfers.GetWireTemplates" scope="session"/>
		<ffi:setProperty name="GetWireTemplates" property="PageSize" value="100"/>
		<ffi:setProperty name="GetWireTemplates" property="Type" value="DOMESTIC"/>
		<ffi:setProperty name="GetWireTemplates" property="CollectionSessionName" value="WireTemplatesUser"/>
		<ffi:setProperty name="GetWireTemplates" property="WireScope" value="<%= WireDefines.WIRE_SCOPE_USER %>"/>
	<ffi:process name="GetWireTemplates"/>

<%-- :::::::::::::::::::::::::: --%>
<%-- :: Initialize the batch :: --%>
<%-- :::::::::::::::::::::::::: --%>
<ffi:cinclude value1="${DontInitializeBatch}" value2="" operator="equals">
	<%
	int maxPageTraversedInt = 0;
	int lastRecordCount = 0;
	if(maxPageTraversed!=null && !maxPageTraversed.equals("")){
		maxPageTraversedInt = Integer.parseInt(maxPageTraversed);	
		maxPageTraversedInt = maxPageTraversedInt + 1; //get actual page count
		lastRecordCount = maxPageTraversedInt * itemsPerPage;
	}
	
	//This fix was needed in order to remove previous session entries beyond page1
	if(endNum<lastRecordCount){
		endNum = lastRecordCount;
	}
		
	// delete any abandoned session variables we'll need to be using
	for (count = 0; count < endNum; count ++) {
		session.removeAttribute("checkbox_" + String.valueOf(count));
		session.removeAttribute("amount_" + String.valueOf(count));
		session.removeAttribute("beneficiaryName_" + String.valueOf(count));
		session.removeAttribute("daBeneficiaryStatus_" + String.valueOf(count));	
		session.removeAttribute("templateID_" + String.valueOf(count));
		session.removeAttribute("currencyMismatch_" + String.valueOf(count));
		session.removeAttribute("showMismatch_" + String.valueOf(count));
		session.removeAttribute("pmtCurrency_" + String.valueOf(count));
		session.removeAttribute("amtCurrency_" + String.valueOf(count));
		session.removeAttribute("templateDisplayText_" + String.valueOf(count));
		session.removeAttribute("templateDisplayTextID_" + String.valueOf(count));
	}
	startNum = 0;
	endNum = 10;
	pageNum=0;
	%>
	<ffi:setProperty name="templatePage" value="0"/>
	<ffi:setProperty name="templatePages" value="0"/>	
</ffi:cinclude>

<ffi:setProperty name="AddWireBatch" property="ClearWiresInBatch" value="true"/>

<ffi:cinclude value1="${cancelFromBatch}" value2="" operator="notEquals">
	<ffi:setProperty name="AddWireBatch" property="DeleteWireFromBatch" value="${cancelFromBatch}"/>
</ffi:cinclude>

<ffi:setProperty name="wireBatchConfirmURL" value="wirebatchtempconfirm.jsp"/>
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_DOMESTIC %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wiretransfernew.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New Domestic Wire Transfer Batch"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wiretransfernew.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New International Wire Transfer Batch"/>
	<ffi:setProperty name="wireBatchConfirmURL" value="wirebatchinttempconfirm.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_FED %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wirefed.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New FED Wire Transfer Batch"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wiredrawdown.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New Drawdown Wire Transfer Batch"/>
</ffi:cinclude>
<ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_BOOK %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wirebook.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="New Book Wire Transfer Batch"/>
</ffi:cinclude>

<span id="PageHeading" style="display:none;"><ffi:getProperty name='PageHeading'/></span>
<%--<ffi:include page="${PathExt}payments/inc/wire_batch_select.jsp"/>--%>


<ffi:removeProperty name="DontInitialize"/>
<ffi:removeProperty name="editWireTransfer"/>
<ffi:removeProperty name="WireTransfer"/>
<ffi:removeProperty name="WireTransferPayee"/>
<ffi:removeProperty name="loadFromTemplate"/>
<ffi:removeProperty name="cancelFromBatch"/>



<script>
   $(function(){
	   <%for (int i = startNum; i < endNum; i++) { %>
			
		   $("#templateID_<%= i %>").lookupbox({
				"source":"/cb/pages/jsp/wires/wireTransferTemplateLookupBoxAction.action?wireDestination=<ffi:getProperty name='wireDestination'/>",
				"controlType":"server",
				"size":"25",
				"module":"wireLoadTemplateForBatch"
			});
			
			
			
		<%} %>
	});
</script>
<script>

function changeForm(dest) {
	var _batchDestination = "<%= ((String)request.getAttribute("WireDestination"))%> "
	if (dest == "<%= WireDefines.WIRE_BATCH_TYPE_TEMPLATE %>") page = "addWireBatchTemplateAction_initTemplate.action?wireDestination=" + _batchDestination;
	if (dest == "<%= WireDefines.WIRE_BATCH_TYPE_FREEFORM %>") page = "addWireBatchAction_init.action?wireDestination=" + _batchDestination;
	
	
	document.frmBatch.DontInitializeBatch.value = "";
	document.frmBatch.wireDestination.value = dest;
	$.ajax({  
		  type: "POST",   
		  data: $("#frmBatchID").serialize(),    
		  url: "/cb/pages/jsp/wires/" + page,     
		  success: function(data) {  	  
			 $("#inputDiv").html(data);
			 
			 $.publish("common.topics.tabifyNotes");
			 if(accessibility){
				$("#wireBatchTypeSelectID-menu").focus();
			 }
			 
			 var heading = $('#PageHeading').html();
				var $title = $('#details .portlet-title');
				$title.html(heading);
		  }   
		});
	
}

function setDest(dest) {
	if (dest == "<%= WireDefines.WIRE_HOST %>") page = "wirebatchhost.jsp";
	else page = "wirebatch.jsp";
	document.frmBatch.DontInitializeBatch.value = "";
	document.frmBatch.wireDestination.value = dest;
	document.frmBatch.action = page;
	document.frmBatch.submit();
}

function check(state) {
	numBoxes = <%= itemsPerPage %>;     //Number of checkboxes per page
	frm = document.frmBatch;
	for (count = <%= startNum %>; count < <%= endNum %>; count++) {
		frm.elements["checkbox_" + count].checked = (state == "true" ? true : false);
	}
}

// The tempInfo array is set up as follows:  tempInfo[templateID] = ["beneficiaryName","WireLimit"]
tempInfo = new Array();
<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">	
	<ffi:list collection="WireTemplates" items="template">
		tempInfo["<ffi:getProperty name='template' property='TemplateID'/>"] = ["<ffi:getProperty name='template' property='WirePayee.PayeeName'/><ffi:cinclude value1="${template.WirePayee.NickName}" value2="" operator="notEquals"> (<ffi:getProperty name="template" property="WirePayee.NickName"/>)</ffi:cinclude>","<ffi:cinclude value1="${template.WireLimitValue.AmountValue}" value2="0.0" operator="notEquals"><ffi:getProperty name='template' property='WireLimitValue.CurrencyStringNoSymbol'/></ffi:cinclude>","<ffi:getProperty name='template' property='PayeeCurrencyType'/>","<ffi:getProperty name='template' property='AmtCurrencyType'/>", "", ""]		
	</ffi:list>
</ffi:cinclude>

<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >	
	<ffi:list collection="WireTemplates" items="template">
		<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
		<ffi:setProperty name="GetDAItem" property="itemId" value="${template.WirePayee.ID}"/>
		<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY %>"/>
		<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.BusinessID}"/>
		<ffi:process name="GetDAItem" />
		
		<ffi:cinclude value1="${DAItem.Status}" value2="<%=IDualApprovalConstants.STATUS_TYPE_PENDING_APPROVAL %>" operator="notEquals">	
		tempInfo["<ffi:getProperty name='template' property='TemplateID'/>"] = ["<ffi:getProperty name='template' property='WirePayee.PayeeName'/><ffi:cinclude value1="${template.WirePayee.NickName}" value2="" operator="notEquals"> (<ffi:getProperty name="template" property="WirePayee.NickName"/>)</ffi:cinclude>","<ffi:cinclude value1="${template.WireLimitValue.AmountValue}" value2="0.0" operator="notEquals"><ffi:getProperty name='template' property='WireLimitValue.CurrencyStringNoSymbol'/></ffi:cinclude>","<ffi:getProperty name='template' property='PayeeCurrencyType'/>","<ffi:getProperty name='template' property='AmtCurrencyType'/>","<ffi:getProperty name='DAItem' property='Status'/>", ""];			
		</ffi:cinclude>
		<ffi:cinclude value1="${DAItem.Status}" value2="<%=IDualApprovalConstants.STATUS_TYPE_PENDING_APPROVAL %>">	
		tempInfo["<ffi:getProperty name='template' property='TemplateID'/>"] = ["<ffi:getProperty name='template' property='WirePayee.PayeeName'/><ffi:cinclude value1="${template.WirePayee.NickName}" value2="" operator="notEquals"> (<ffi:getProperty name="template" property="WirePayee.NickName"/>)</ffi:cinclude>","<ffi:cinclude value1="${template.WireLimitValue.AmountValue}" value2="0.0" operator="notEquals"><ffi:getProperty name='template' property='WireLimitValue.CurrencyStringNoSymbol'/></ffi:cinclude>","<ffi:getProperty name='template' property='PayeeCurrencyType'/>","<ffi:getProperty name='template' property='AmtCurrencyType'/>","* Beneficiary has been modified and is pending approval. Please select a different beneficiary.", "disabled"];
		</ffi:cinclude>
		<ffi:cinclude value1="${DAItem.Status}" value2="<%=IDualApprovalConstants.STATUS_TYPE_REJECTED %>">	
		tempInfo["<ffi:getProperty name='template' property='TemplateID'/>"]  = ["<ffi:getProperty name='template' property='WirePayee.PayeeName'/><ffi:cinclude value1="${template.WirePayee.NickName}" value2="" operator="notEquals"> (<ffi:getProperty name="template" property="WirePayee.NickName"/>)</ffi:cinclude>","<ffi:cinclude value1="${template.WireLimitValue.AmountValue}" value2="0.0" operator="notEquals"><ffi:getProperty name='template' property='WireLimitValue.CurrencyStringNoSymbol'/></ffi:cinclude>","<ffi:getProperty name='template' property='PayeeCurrencyType'/>","<ffi:getProperty name='template' property='AmtCurrencyType'/>","* Beneficiary has been modified and is pending approval. Please select a different beneficiary.", "disabled"];
		</ffi:cinclude>
	</ffi:list>
</ffi:cinclude>
<%-- End: Dual approval processing--%>

function decode(toDecode) {
    if (toDecode != null) {
        var temp = toDecode.replace(/&lt;/g,"<");
        temp = temp.replace(/&gt;/g,">");
        temp = temp.replace(/&#34;/g,"\"");
        temp = temp.replace(/&#39;/g,"'");
        temp = temp.replace(/&amp;/g,"&");        
        return temp;
    }
}

<%-- When the selection changed event is fired on the template name list box, this method gets called. It
populates the values for beneficiary name, amount and currency mismatch indicator for the selected element --%>

function magic(tid) {
	frm = document.frmBatch;
	tid_prefix = "templateID_"; // the static portion of the templateID tag's name
	bid_prefix = "beneficiaryName_"; // the static portion of the beneficiaryName's tag's name
	da_beneficiary_prefix = "daBeneficiaryStatus_"; // the static portion of the daBeneficiaryStatus's tag's name
	chk_prefix = "checkbox_"; // the static portion of the checkbox's tag's name
	amt_prefix = "amount_"; // the static portion of the amount's tag's name
    mismatch_prefix = "currencyMismatch_" //static portion of the hidden field storing mismatch value
    mismatch_indicator_prefix = "showMismatch_"; // static portion of mismatch indicator
    pmtCurrency_prefix = "pmtCurrency_" //static portion of the hidden field storing pmt currency value
    amtCurrency_prefix = "amtCurrency_" //static portion of the hidden field storing amt currency value
    templateDisplayText_prefix = "templateDisplayText_"; //store display text of selected options for redisplay when user navigates through pages
    templateDisplayTextID_prefix = "templateDisplayTextID_"; //store display ID of selected options for redisplay when user navigates through pages
	rowID = tid.name;
	rowID = rowID.substring(tid_prefix.length);
	buttonLayer = document.getElementById("button_" + rowID);
	tempID = $(tid).val();
	if (tempID != -1) {
		// check the checkbox if selecting a template
		frm[chk_prefix + rowID].checked = true;

		// put beneficiary name into it's field
		frm[bid_prefix + rowID].value = decode ( tempInfo[tempID][0] );

		// put wire limit into amount field
		frm[amt_prefix + rowID].value = decode ( tempInfo[tempID][1] );

		// put payment currency
		frm[pmtCurrency_prefix + rowID].value = tempInfo[tempID][2];

		// put amount currency
		frm[amtCurrency_prefix + rowID].value = tempInfo[tempID][3];

		//put selected template's value in order to retain if we move from page to page
		frm[templateDisplayText_prefix + rowID].value =tid.options[tid.selectedIndex].innerHTML;

		//put selected template ID value in order to retain if we move from page to page
		frm[templateDisplayTextID_prefix + rowID].value = tempID;

		// put dual approval beneficiary status
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >
		frm[da_beneficiary_prefix + rowID].value = tempInfo[tempID][4];
		</ffi:cinclude>

		// show button
		buttonLayer.style.display = "block";

		// Disable submit button
		if (tempInfo[tempID][5] != "") {
			frm.SubmitBatch.disabled = true;
			// hide button
			buttonLayer.style.display = "none";
		}

		// move focus to amount field
		//frm[amt_prefix + rowID].focus();
		//frm[amt_prefix + rowID].select();

        // show the mismatch indicator if currency fields do not match

        if ( isCurrencyDoNotMatch(rowID) ) {
            frm[mismatch_prefix + rowID].value = "true"
            star = document.getElementById(mismatch_indicator_prefix + rowID);
            star.style.display="inline";
            msg = document.getElementById("intlMessage");
            msg.style.display="inline";
        }
        else
        {
            frm[mismatch_prefix + rowID].value = "false"
            star = document.getElementById(mismatch_indicator_prefix + rowID);
            star.style.display="none";

        }

	} else {
		// remove beneficiary name from it's field
		frm[bid_prefix + rowID].value = "";
       // initialise payment currency
        frm[pmtCurrency_prefix + rowID].value = "";
        // initialise amount currency
        frm[amtCurrency_prefix + rowID].value = "";
        frm[mismatch_prefix + rowID].value = "false"

		// uncheck the checkbox if deselecting a template
		frm[chk_prefix + rowID].checked = false;

		// hide button
		buttonLayer.style.display = "none";

        //hide mismatch indicator
        star = document.getElementById(mismatch_indicator_prefix + rowID);
        star.style.display="none";
	}
}

<%-- This method returns true if there is a currency mismatch at the specified column --%>
function  isCurrencyDoNotMatch(rowID)
{
    frm = document.frmBatch;

    if ( frm["wireBatchObject.AmtCurrencyType"] == null ) {
            return false;
    }
    if ( frm["wireBatchObject.PayeeCurrencyType"] == null ) {
            return false;
    }
    batchAmtCurrency = frm["AddWireBatch.AmtCurrencyType"].value;
    batchPmtCurrency = frm["AddWireBatch.PayeeCurrencyType"].value;
    amtCurrency = frm["amtCurrency_" + rowID].value;
    pmtCurrency = frm["pmtCurrency_" + rowID].value;

    if ( batchAmtCurrency != null && amtCurrency != null ) {
        if (batchAmtCurrency != amtCurrency )
            return true;
    }
    if ( batchPmtCurrency != null && pmtCurrency != null ) {
        if (batchPmtCurrency != pmtCurrency )
            return true;
    }
    return false;
}

<%-- This method refreshes the currency mismatch indicator for all elements in the batch --%>
function  refreshCurrencyMismatches()
{
    numBoxes = <%= itemsPerPage %>;     //Number of checkboxes per page
	frm = document.frmBatch;
    mismatch_prefix = "currencyMismatch_" //static portion of the hidden field storing mismatch value
    mismatch_indicator_prefix = "showMismatch_"; // static portion of mismatch indicator
    tid_prefix = "templateID_"; // the static portion of the templateID tag's name
	for (count = <%= startNum %>; count < <%= endNum %>; count++) {
        templateElement = document.getElementById(tid_prefix + count);
        tempID = -1;
        if (templateElement != null) tempID = templateElement.selectedIndex - 1;
        if (tempID != -1)
        {
            if (isCurrencyDoNotMatch(count))
            {
                frm[mismatch_prefix + count].value = "true"
                star = document.getElementById(mismatch_indicator_prefix + count);
                star.style.display="inline";
                msg = document.getElementById("intlMessage");
                msg.style.display="inline";
            }
            else
            {
                frm[mismatch_prefix + count].value = "false"
                star = document.getElementById(mismatch_indicator_prefix + count);
                star.style.display="none";
            }
        }
    }
}


function editWire(rowID,tID,amt) {
	frm = document.frmBatch;
	// strip all formatting from amount
	re = new RegExp("[0-9\.]");
	a = "";
	for (i = 0; i < amt.length; i++) {
		x = amt.substring(i,i+1)
		if(re.test(x)) a += x;
	}
	if (a == "") a = 0;
	frm.loadFromTemplate.value =  rowID + ";" + tID + ";" + a;
	$.ajax({ 
		  type: "POST",   
		  data: $("#frmBatchID").serialize(),     
		  url: "/cb/pages/jsp/wires/addWireBatchTemplateAction_editWireTransfer.action",     
		  success: function(data) {  	  
			 $("#inputDiv").html(data);
			 
			 $.publish("common.topics.tabifyNotes");
			 if(accessibility){
				$("#inputDiv").setFocus();
			 }
		  }   
		});
}

function addPage() {
	$.ajax({ 
	  type: "POST",   
	  data: $("#frmBatchID").serialize(),     
	  url: "/cb/pages/jsp/wires/addWireBatchTemplateAction_addPage.action",     
	  success: function(data) {  	  
		 $("#inputDiv").html(data);
		 
		 $.publish("common.topics.tabifyNotes");
		 if(accessibility){
			$("#inputDiv").setFocus();
		 }
	  }   
	});
}

function backPage() {
	$.ajax({ 
		  type: "POST",   
		  data: $("#frmBatchID").serialize(),     
		  url: "/cb/pages/jsp/wires/addWireBatchTemplateAction_backPage.action",     
		  success: function(data) {  	  
			 $("#inputDiv").html(data);
			 
			 $.publish("common.topics.tabifyNotes");
			 if(accessibility){
				$("#inputDiv").setFocus();
			 }
		  }   
		});
}
refreshCurrencyMismatches();
</script>
<span id="formerrors"></span>
<s:form  name="frmBatch"  id="frmBatchID" action="addWireBatchTemplateAction_verify" namespace="/pages/jsp/wires"  method="post" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="DontInitializeBatch" value="true">
<input type="hidden" name="loadFromTemplate" value=""/>
<input type="hidden" name="wireDestination" value=""/>
<div class="leftPaneWrapper" role="form" aria-labelledby="batchEntryHeader">
	<div class="leftPaneInnerWrapper">
		<div class="header" id=""><h2 id="batchEntryHeader">Wire Batch Entry Details</h2></div>
		<div class="leftPaneInnerBox">
			<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="notEquals">
				<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_fields.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
				<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_int_fields.jsp"/>
			</ffi:cinclude>
		</div>
	</div>
</div>
<div class="confirmPageDetails" id="wireBatTemplate" style="padding:0px !important; width:70%">
<%-- CONTENT START --%>
			<div class="paneWrapper">
				<div class="paneInnerWrapper">
					<table width="100%" cellpadding="3" cellspacing="0" border="0" class="tableData">
					<tr class="header">
						<td class="sectionsubhead" width="4%" align="right">
							<%-- <sj:a button="true" onclick="check('true');"><s:text name="jsp.default_93" /></sj:a>
							<sj:a button="true" class="" onclick="check('false');"><s:text name="jsp.default_96" /></sj:a> --%>
							<input type="Checkbox" name="check_all" onclick="check('true');" id="check_all">
							<input type="Checkbox" name="check_none" onclick="check('false');" id="check_none" class="hidden">
						</td>
	          			<td class="sectionsubhead" width="1%">&nbsp;</td>
						<td class="sectionsubhead" width="35%" align="left" nowrap><!--L10NStart-->Template Name<!--L10NEnd--></td>
						<td class="sectionsubhead" width="35%" align="left" nowrap><!--L10NStart-->Beneficiary Name<!--L10NEnd--></td>
						<td class="sectionsubhead" width="10%"><!--L10NStart-->Amount<!--L10NEnd--></td>
						<td class="sectionsubhead" width="5%">&nbsp;</td>
						<td class="sectionsubhead" width="10%">Action</td>
					</tr>
					<%
						for (int i = startNum; i < endNum; i++) {
						String checkbox = "checkbox_" + String.valueOf(i);
						String name = "beneficiaryName_" + String.valueOf(i);
						String daBeneficiaryStatus = "daBeneficiaryStatus_" + String.valueOf(i);
						String amount = "amount_" + String.valueOf(i);
						String template = "templateID_" + String.valueOf(i);
						String mismatchField = "currencyMismatch_" + String.valueOf(i);
						String pmtCurrency = "pmtCurrency_" + String.valueOf(i);
						String amtCurrency = "amtCurrency_" + String.valueOf(i);
						String templateDisplayText = "templateDisplayText_" + String.valueOf(i);
						String templateDisplayTextID = "templateDisplayTextID_" + String.valueOf(i);
						String templateDisplayTextValue = "";
						String templateDisplayTextIDValue = "";
						
						String DontInitializeBatchVar = (String)session.getAttribute("DontInitializeBatch");
						if(DontInitializeBatchVar!=null && "true".equals(DontInitializeBatchVar)){
							templateDisplayTextValue = session.getAttribute(templateDisplayText)==null?"":(String)session.getAttribute(templateDisplayText);
							templateDisplayTextIDValue = session.getAttribute(templateDisplayTextID)==null?"":(String)session.getAttribute(templateDisplayTextID);
						}
	
						String curTemplate = "";
						String thisTemplate = "";
						String amt = "";
						String chk = "";
						String mismatchValue = "";
        			%>
				<tr>
					<ffi:getProperty name="<%= checkbox %>" assignTo="chk"/>
					<% if (chk == null) chk = "false"; else if(chk.equalsIgnoreCase("on") ) chk = "true";%>
					<td style="padding:10px 0" align="right"><input type="Checkbox" name="checkbox_<%= i %>" <%= chk.equals("true") ? "checked" : "" %>></td>
					<td style="padding:10px 0" class="columndata">
                         <ffi:getProperty name="<%= mismatchField %>" assignTo="mismatchValue"/>
                         <% if (mismatchValue == null) mismatchValue = "false"; %>
                        <div id="showMismatch_<%= i %>"  style=<%= mismatchValue.equals("true") ? "display:inline" : "display:none" %>>#</div>
					</td>
                    <td style="display:none">
                        <input type="hidden" name="currencyMismatch_<%= i %>" value="<ffi:getProperty name='<%= mismatchField %>'/>" />
                        <input type="hidden" name="pmtCurrency_<%= i %>" value="<ffi:getProperty name='<%= pmtCurrency %>'/>"/>
                        <input type="hidden" name="amtCurrency_<%= i %>" value="<ffi:getProperty name='<%= amtCurrency %>'/>"/>
                        <input type="hidden" name="templateDisplayText_<%= i %>" value="<ffi:getProperty name='<%= templateDisplayText %>'/>"/>
                        <input type="hidden" name="templateDisplayTextID_<%= i %>" value="<ffi:getProperty name='<%= templateDisplayTextID %>'/>"/>
                    </td>
                    <td style="padding:10px 0">
					
					<%
					curTemplate = (String)session.getAttribute(template);
					if(curTemplate==null) curTemplate = "";
					%>
					
					<select id="templateID_<%= i %>" name="templateID_<%= i %>" class="txtbox" onchange="magic(this);">
						<%
						if(templateDisplayTextValue!=null && !templateDisplayTextValue.isEmpty()){
						%>
						<option value="<%=templateDisplayTextIDValue%>"><%=templateDisplayTextValue%></option>
						<%} %> 
					</select>
					
					</td>
					<td style="padding:10px 0"><input type="Text" name="beneficiaryName_<%= i %>" class="txtbox lightBackground" value="<ffi:getProperty name="<%= name %>"/>" size="35" readonly style="border-style:none;"></td>
					<ffi:getProperty name="<%= amount %>" assignTo="amt"/>
					<% 
						if(amt == null)
							amt="";
					%>
			<%-- 		<ffi:setProperty name="Currency" property="Amount" value="<%= amt %>"/> --%>
					<td style="padding:10px 0"><input type="Text" name="amount_<%= i %>" class="ui-widget-content ui-corner-all txtbox" value="<s:if test="%{amt != ''}"><%=amt %></s:if>" size="10">
					<span id="amount_<%=i%>Error"></span>
					</td>
					<td style="padding:10px 0" class="columndata" colspan="2" align="center">
						<div id="button_<%= i %>" style="display:<%= (curTemplate.equals("") || curTemplate.equals("-1") ) ? "none" : "block" %>;"><a href="#" onclick="javascript:editWire(<%= i %>,document.frmBatch.templateID_<%= i %>.options[document.frmBatch.templateID_<%= i %>.selectedIndex].value,document.frmBatch.amount_<%= i %>.value);" class="ui-button"><span class='sapUiIconCls icon-wrench'></span></a></div>
					</td>
				</tr>
				
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >	
					<tr>
						<td colspan="2" style="padding:10px 0">&nbsp;</td>
						<td colspan="5" style="padding:10px 0"><!--L10NStart-->
							<input type="Text" name="daBeneficiaryStatus_<%= i %>" class="txtbox lightBackground" value="<ffi:getProperty name="<%= daBeneficiaryStatus %>"/>" readonly style="border-style:none;width:96%;color:red;"><!--L10NEnd-->
						</td>
					<tr>
				</ffi:cinclude>
				<% } %>
				<%-- <tr>
					<td>&nbsp;</td>
					<td colspan="3">
					<sj:a button="true" onclick="check('true');"><s:text name="jsp.default_93" /></sj:a>
					<sj:a button="true" onclick="check('false');"><s:text name="jsp.default_96" /></sj:a>
					</td>
				</tr> --%>
				</table>
			 </div>
			</div>
			<%-- <table width="100%" cellpadding="3" cellspacing="0" border="0">
				<tr>
					<td class="sectionsubhead" width="10%">&nbsp;</td>
          			<td class="sectionsubhead" width="12%">&nbsp;</td>
					<td class="sectionsubhead" width="34%" align="left" nowrap><!--L10NStart-->Template Name<!--L10NEnd--></td>
					<td class="sectionsubhead" width="1%" align="left" nowrap><!--L10NStart-->Beneficiary Name<!--L10NEnd--></td>
					<td class="sectionsubhead" width="1%"><!--L10NStart-->Amount<!--L10NEnd--></td>
					<td class="sectionsubhead" width="20%">&nbsp;</td>
				</tr>
				<%
				for (int i = startNum; i < endNum; i++) {
					String checkbox = "checkbox_" + String.valueOf(i);
					String name = "beneficiaryName_" + String.valueOf(i);
					String daBeneficiaryStatus = "daBeneficiaryStatus_" + String.valueOf(i);
					String amount = "amount_" + String.valueOf(i);
					String template = "templateID_" + String.valueOf(i);
					String mismatchField = "currencyMismatch_" + String.valueOf(i);
					String pmtCurrency = "pmtCurrency_" + String.valueOf(i);
					String amtCurrency = "amtCurrency_" + String.valueOf(i);
					String templateDisplayText = "templateDisplayText_" + String.valueOf(i);
					String templateDisplayTextID = "templateDisplayTextID_" + String.valueOf(i);
					String templateDisplayTextValue = "";
					String templateDisplayTextIDValue = "";
					
					String DontInitializeBatchVar = (String)session.getAttribute("DontInitializeBatch");
					if(DontInitializeBatchVar!=null && "true".equals(DontInitializeBatchVar)){
						templateDisplayTextValue = session.getAttribute(templateDisplayText)==null?"":(String)session.getAttribute(templateDisplayText);
						templateDisplayTextIDValue = session.getAttribute(templateDisplayTextID)==null?"":(String)session.getAttribute(templateDisplayTextID);
					}

					String curTemplate = "";
					String thisTemplate = "";
					String amt = "";
					String chk = "";
					String mismatchValue = "";
        			%>
				<tr>
					<ffi:getProperty name="<%= checkbox %>" assignTo="chk"/>
					<% if (chk == null) chk = "false"; else if(chk.equalsIgnoreCase("on") ) chk = "true";%>
					<td align="right"><input type="Checkbox" name="checkbox_<%= i %>" <%= chk.equals("true") ? "checked" : "" %>></td>
					<td class="columndata">
                         <ffi:getProperty name="<%= mismatchField %>" assignTo="mismatchValue"/>
                         <% if (mismatchValue == null) mismatchValue = "false"; %>
                        <div id="showMismatch_<%= i %>"  style=<%= mismatchValue.equals("true") ? "display:inline" : "display:none" %>>#</div>
					</td>
                    <td style="display:none">
                        <input type="hidden" name="currencyMismatch_<%= i %>" value="<ffi:getProperty name='<%= mismatchField %>'/>" />
                        <input type="hidden" name="pmtCurrency_<%= i %>" value="<ffi:getProperty name='<%= pmtCurrency %>'/>"/>
                        <input type="hidden" name="amtCurrency_<%= i %>" value="<ffi:getProperty name='<%= amtCurrency %>'/>"/>
                        <input type="hidden" name="templateDisplayText_<%= i %>" value="<ffi:getProperty name='<%= templateDisplayText %>'/>"/>
                        <input type="hidden" name="templateDisplayTextID_<%= i %>" value="<ffi:getProperty name='<%= templateDisplayTextID %>'/>"/>
                    </td>
                    <td>
					
					<%
					curTemplate = (String)session.getAttribute(template);
					if(curTemplate==null) curTemplate = "";
					%>
					
					<select id="templateID_<%= i %>" name="templateID_<%= i %>" class="txtbox" onchange="magic(this);">
						<%
						if(templateDisplayTextValue!=null && !templateDisplayTextValue.isEmpty()){
						%>
						<option value="<%=templateDisplayTextIDValue%>"><%=templateDisplayTextValue%></option>
						<%} %> 
					</select>
					
					</td>
					<td><input type="Text" name="beneficiaryName_<%= i %>" class="txtbox lightBackground" value="<ffi:getProperty name="<%= name %>"/>" size="40" readonly style="border-style:none;"></td>
					<ffi:getProperty name="<%= amount %>" assignTo="amt"/>
					<% 
						if(amt == null)
							amt="";
					%>
					<ffi:setProperty name="Currency" property="Amount" value="<%= amt %>"/>
					<td><input type="Text" name="amount_<%= i %>" class="ui-widget-content ui-corner-all txtbox" value="<s:if test="%{amt != ''}"><%=amt %></s:if>" size="20">
					<span id="amount_<%=i%>Error"></span>
					</td>
					<td class="columndata">
						<div id="button_<%= i %>" style="display:<%= (curTemplate.equals("") || curTemplate.equals("-1") ) ? "none" : "block" %>;"><a href="#" onclick="javascript:editWire(<%= i %>,document.frmBatch.templateID_<%= i %>.options[document.frmBatch.templateID_<%= i %>.selectedIndex].value,document.frmBatch.amount_<%= i %>.value);" class="ui-button"><span class='ui-icon ui-icon-wrench'></span></a></div>
					</td>
				</tr>
				
				<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" >	
					<tr>
						<td colspan="2">&nbsp;</td>
						<td colspan="4"><!--L10NStart-->
							<input type="Text" name="daBeneficiaryStatus_<%= i %>" class="txtbox lightBackground" value="<ffi:getProperty name="<%= daBeneficiaryStatus %>"/>" size="110" readonly style="border-style:none;color:red;"><!--L10NEnd-->
						</td>
					<tr>
				</ffi:cinclude>
				<% } %>
				<tr>
					<td>&nbsp;</td>
					<td colspan="3">
					<sj:a button="true" onclick="check('true');"><s:text name="jsp.default_93" /></sj:a>
					<sj:a button="true" onclick="check('false');"><s:text name="jsp.default_96" /></sj:a>
					</td>
				</tr>
			</table> --%>
			&nbsp;<br>
            <div align="center" class="required">* <!--L10NStart-->indicates a required field<!--L10NEnd--></div>
			<div class="btn-row">
			<sj:a id="cancelFormButtonOnVerify" 
				button="true" summaryDivId="summary" buttonType="cancel"
				onClickTopics="showSummary,cancelWireTransferForm"
			 ><s:text name="jsp.default_82" />
			 </sj:a>
			
			 <sj:a id="verifyWireBatchID"
				formIds="frmBatchID"  
			 	targets="verifyDiv" 
			 	validate="true" 
				validateFunction="customValidation"
				button="true" 
				onBeforeTopics="verifyWireBatchBeforeTopics"
				onSuccessTopics="verifyWireBatchSuccessTopics"><s:text name="jsp.default_395" /></sj:a>
			<% if (pageNum > 0) { %>
				<sj:a button="true" onclick="backPage('true');"><s:text name="jsp.default_PREVIOUS_PAGE" /></sj:a>
			<% } %>
			<sj:a button="true" onclick="addPage('true');"><s:text name="jsp.default_NEXT_PAGE" /></sj:a></div>
            <ffi:cinclude value1="${AddWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
                <table border="0" cellspacing="0" cellpadding="3" width="98%" class="marginTop20">
                    <tr>
                        <td class="columndata">
                            <div id="intlMessage" style="display:none"><!--L10NStart--># indicates the template's debit/payment currency don't match the batch's debit/payment currencies. Upon submission, the batch currencies will override the template's currencies.<!--L10NEnd--></div>
                        </td>
                    </tr>
                </table>
			</ffi:cinclude>
            
<%-- CONTENT END --%>
</div></s:form>
<script type="text/javascript">
	$(document).ready(function(){
		$( "#check_all" ).click(function(){ 
				$(this).hide();
				$("#check_none").show();
				$("#check_none").prop('checked', true);
		});
		$( "#check_none" ).click(function(){ 
				$(this).hide();
				$("#check_all").show();
				$("#check_all").prop('checked', false);
		});
	});
</script>