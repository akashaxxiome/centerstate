<%--
This file contains common javascript functions used on the add/edit wire transfer
pages

It is included in the following files:
    wirebook.jsp
    wiredrawdown.jsp
    wirefed.jsp
    wirehost.jsp
    wiretransfernew.jsp

--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setProperty name="wireTaskHost" value="true"/>
<ffi:cinclude value1="${wireTask}" value2="AddHostWire" operator="notEquals">
    <ffi:cinclude value1="${wireTask}" value2="ModifyHostWire" operator="notEquals">
        <ffi:setProperty name="wireTaskHost" value="false"/>
    </ffi:cinclude>
</ffi:cinclude>

<input type="hidden" name="wireDestinationForJS" id="wireDestinationIDForJS" value="<ffi:getProperty name='${wireTask}' property='WireDestination'/>"/>


<script>
<ffi:cinclude value1="${wireTaskHost}" value2="true" operator="equals">
    wireDest = "<%= WireDefines.WIRE_HOST %>";
</ffi:cinclude>
<ffi:cinclude value1="${wireTaskHost}" value2="true" operator="notEquals">
    wireDest = "<ffi:getProperty name='${wireTask}' property='WireDestination'/>";
</ffi:cinclude>
WIRE_DOMESTIC = "<%= WireDefines.WIRE_DOMESTIC %>";
WIRE_INTERNATIONAL = "<%= WireDefines.WIRE_INTERNATIONAL %>";
WIRE_HOST = "<%= WireDefines.WIRE_HOST %>";
WIRE_DRAWDOWN = "<%= WireDefines.WIRE_DRAWDOWN %>";
WIRE_FED = "<%= WireDefines.WIRE_FED %>";
WIRE_BOOK = "<%= WireDefines.WIRE_BOOK %>";
WIRE_CREATE_NEW_BENEFICIARY = "<%= WireDefines.CREATE_NEW_BENEFICIARY %>";
WIRE_CREATE_NEW_DEBIT_ACCOUNT = "<%=WireDefines.CREATE_NEW_DEBIT_ACCOUNT%>";

<%-- /*
* Do not load all but the necessary javascripts if the user is not entitled to free-form create, and they haven't
* selected a template to load yet. (and they're not working on a template or a wire in a batch)
*/ --%>
<ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
function SelectAmount() {
    frm = document.WireNew;
    if (wireDest != WIRE_HOST) {
		frm["WireTransferPayee.PayeeScope"].disabled = false;
	}
    <ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
        frm['<ffi:getProperty name="wireTask"/>.WireLimitValue'].value = frm['<ffi:getProperty name="wireTask"/>.AmountValue'].value;
    </ffi:cinclude>

    if (wireDest != WIRE_HOST) {
        if (frm.openEnded[0].checked == true) {
            frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].disabled = false;
            frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value = "999";
        }

    }
    return true;
}
/*
* Additional Functions
*/

//Submit the form again and for setting the payee selected.
ns.wire.submmitWireTransferForm = function(urlString, setPayeeVar, DontInitializeVar, preProcessType){	
	var action = $("#returnPage").val();
	var url = "/cb/pages/jsp/wires/"+action;
	
	    $.ajax({
	        type: "POST",
	        url: url,
	        data: $("#newWiresTransferFormID").serialize(),
	        success: function(data) {                
				$("#inputDiv").html(data);
				ns.wire.beneScope = $('#selectBeneficiaryScopeID')[0].selectedIndex;	
						
				$.publish("common.topics.tabifyDesktop");
				if(accessibility){
			        $("#details").setInitialFocus();
			    }
	       }
	        
	    });
    
    
	    
}

function setPayee(id) {
	$('#payeeID').val(id);
	// this hold payee/beneficiary/debit account ID
	var payeeId = $('#payeeID').val();
	// this holds credit account id
	var creditAccountId = $('#accountID').val();
    frm = document.WireNew;
	var preProcessType = "preProcess";
	if (wireDest == WIRE_DOMESTIC || wireDest == WIRE_INTERNATIONAL) { pageName = "wiretransfernew.jsp"; preProcessType = "preProcess"; }
	if (wireDest == WIRE_BOOK) { pageName = "wirebook.jsp"; preProcessType = "preProcessBookWire"; }
	if (wireDest == WIRE_DRAWDOWN) { pageName = "wiredrawdown.jsp"; preProcessType = "preProcessDrawDownWire"; }
	if (wireDest == WIRE_FED) { pageName = "wirefed.jsp"; preProcessType = "preProcessFedWire"; }
	var url = pageName;
	var setPayeeVar = id;
	var DontInitializeVar = "true";

	<%-- clear the WireTransferPayee field names, so that none of the elements get submitted.
	Otherwise, their values will override the values in the selected beneficiary. --%>
	for (i = 0; i < frm.elements.length; i++) {    	
		if (frm.elements[i].name.substring(0, 17) == "WireTransferPayee") {
			frm.elements[i].name = "";
		}
	}
	
	if (setPayeeVar == 'Create New Beneficiary' || setPayeeVar == 'Create New Debit Account') {
		setPayeeVar = "";
	} 

	var action = $("#setPayeeAction").val();
	var url = "/cb/pages/jsp/wires/"+action;
	if(url.indexOf("add") > -1) {
		url= url + "?isAddTransfer=true";
	}
	
    $.ajax({
        type: "POST",
        url: url,
        data: $("#newWiresTransferFormID").serialize(),
        success: function(data) {                
			$("#inputDiv").html(data);
			ns.wire.beneScope = $('#selectBeneficiaryScopeID')[0].selectedIndex;
			
			$.publish("common.topics.tabifyDesktop");
			if(accessibility){
		        $("#details").setInitialFocus();
		    }
       },
       // below complete is added to show hide details in case of simplifed wires
       complete : function(jqXHR,textStatus){
    	   if (setPayeeVar == "") {
    		    $('#wireAddeditPayeeFields').slideToggle();
    			$('#wireAddeditBankFields').slideToggle();
    			$('.lineBreak').slideToggle();
    			$('#intermediaryBanksDivID').slideToggle();
    			$('#bookWireHiddenJsp').slideToggle();
    			$('#drawdownHiddenJsp').slideToggle();
    			$('#fedwireHiddenJsp').slideToggle();
    			// below code for cr:765155 to hide to load template when free form clicked
    			if('<ffi:getProperty name="wireBatchObject" property="BatchType"/>'=='FREEFORM'){
    				$('#loadWireTransferTemplateFormID').parent('div.leftPaneInnerBox').parent('div.leftPaneInnerWrapper').hide();
    			}
    	   }
    	   // below if for drawdown to set credit account/debit account id's
    	   if(payeeId!=null && $.trim(payeeId)!=""){
    		   $('#payeeID').val(payeeId);
    	   }
    	   if(creditAccountId!=null && $.trim(creditAccountId)!=""){
    		   $('#accountID').val(creditAccountId);
    	   }
    	   // id added 
    	   $(".selecteBeneficiaryMessage").html("<div class='sectionhead nameTitle' id='freeFormBeneLoaded' style='cursor:auto'>Beneficiary Info</div> ");
       }         
    });
	
	if(id==""){
		ns.wire.showScopeWarning = false;
	} else {
		ns.wire.showScopeWarning = true;
	}
	
	if ($("#selectWirePayeeID").length > 0 && $("#selectWirePayeeID").val() != js_wirelookup_default_value 
			&& $("#selectWirePayeeID").val() != 'Create New Beneficiary' && $("#selectWirePayeeID").val() != 'Create New Debit Account') {		
			$("#searchDestBank").button("option", "disabled", true);
	}	
	
	$.publish("common.topics.tabifyDesktop");
	if(accessibility){
        $("#details").setInitialFocus();
    }
}

/* kaijie: ns.wire.isChangeBeneScope is a global variable returned by the "changeBeneScopeDialog" popup dialog */
ns.wire.isChangeBeneScope = false;
ns.wire.showScopeWarning = true;
ns.wire.currentSelect = 0;
ns.wire.origSelect = 0;

function setScope(scope, changeBeneScope) {
	<% String payeeid = ""; %>
	<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="payeeid"/>
	<ffi:cinclude value1="<%= payeeid %>" value2='Create New Beneficiary' operator="notEquals">
	<ffi:cinclude value1="<%= payeeid %>" value2="" operator="notEquals">
	frm = document.WireNew;
	ns.wire.isChangeBeneScope = changeBeneScope;
	if (ns.wire.showScopeWarning) {
		if (ns.wire.isChangeBeneScope){
			ns.wire.callOnCloseTopic = true;
			$('#changeBeneScopeDialog').dialog('open').html('Changing the beneficiary scope will create a new beneficiary.<br/>Click OK to create a new beneficiary.<br/>Click CANCEL to return to the existing beneficiary on the current page.');
			ns.wire.currentSelect = $("#selectBeneficiaryScopeID").val();        	
			return false;
		} else {
			<%-- Start: Dual approval processing --%>
			<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
				$("#selectBeneficiaryScopeID").val("UNMANAGED");
				$("#selectBeneficiaryScopeID")
				$("#selectBeneficiaryScopeID").attr("disabled",disabled);
				$("#selectBeneficiaryScopeID").selectmenu('destroy').selectmenu({width:'10em'});
			</ffi:cinclude>
			<%-- End: Dual approval processing--%>
		}
		ns.wire.showScopeWarning = false;
	}    
		</ffi:cinclude>	
	</ffi:cinclude>	
}
ns.wire.changeBeneScope = function(fieldValue)
{	
	frm = document.WireNew;
	disableFlag = false;	
	if( ns.wire.isChangeBeneScope == false ) {
		disableFlag = true;
	}
	var selectmenuStatusFlag = "";
	
	if(disableFlag == false){
		selectmenuStatusFlag="enable";
	} else {
		selectmenuStatusFlag="disable";
	}
	
	//if (scope != "<%= WireDefines.PAYEE_SCOPE_UNMANAGED %>") disableFlag = true;
	frm["WireTransferPayee.PayeeName"].disabled = disableFlag;
	frm["WireTransferPayee.NickName"].disabled = disableFlag;
	frm["WireTransferPayee.Street"].disabled = disableFlag;
	frm["WireTransferPayee.Street2"].disabled = disableFlag;
	frm["WireTransferPayee.City"].disabled = disableFlag;
	frm["WireTransferPayee.State"].disabled = disableFlag;
	$("#selectStateProvinceID").selectmenuStatusFlag;
	frm["WireTransferPayee.ZipCode"].disabled = disableFlag;
	frm["WireTransferPayee.ContactName"].disabled = disableFlag;
	frm["WireTransferPayee.AccountNum"].disabled = disableFlag;
	frm["WireTransferPayee.AccountType"].disabled = disableFlag;
	$("#selectAccountTypeID").selectmenuStatusFlag;
	frm["<ffi:getProperty name="wireTask"/>.WirePayeeID"].selectedIndex = 0;
	$("#selectStateProvinceID").selectmenu('destroy').selectmenu({width:'14.5em'});
	
	if (ns.wire.isChangeBeneScope) {
		$("#selectWirePayeeID").lookupbox("refresh");		
		$("#searchDestBank").button("option", "disabled", false);
		var selectedValue = WIRE_CREATE_NEW_BENEFICIARY;
		if(wireDest == "DRAWDOWN") {
			selectedValue = WIRE_CREATE_NEW_DEBIT_ACCOUNT;
		}
		$("#selectWirePayeeID").lookupbox('widget').val(selectedValue);
		$("#selectWirePayeeID_autoComplete").val(selectedValue);
		$("#selectWirePayeeID_autoComplete").removeAttr('style');
	} else {
		$("#selectBeneficiaryScopeID").val(fieldValue);
	}
	
	var payeeScopeValue = $("#selectBeneficiaryScopeID").val();	
	if (payeeScopeValue == "USER") {
		payeeScopeValue = "User";
	} else if (payeeScopeValue == "BUSINESS") {
		payeeScopeValue = "Business";
	} else if (payeeScopeValue == "UNMANAGED") {
		payeeScopeValue = "One-Off";	
	}	
	$("#selectBeneficiaryScopeID-menu").find("span.ui-selectmenu-status").text(payeeScopeValue);
	
	// Enable or disable the IBAN, Correspondent Bank Account Number fields
	var interBankSize = "<ffi:getProperty name='WireTransferPayee' property='IntermediaryBanks.Size'/>";
	if (wireDest == WIRE_DOMESTIC || wireDest == WIRE_INTERNATIONAL || wireDest == WIRE_FED) {
		frm["WireTransferPayee.DestinationBank.IBAN"].disabled = disableFlag;
		if (parseInt(interBankSize) >= 1) {
			frm["WireTransferPayee.DestinationBank.CorrespondentBankAccountNumber"].disabled = disableFlag;
			for (var i=1;i<interBankSize;i++) {
				document.getElementById("CurrentWireIntermediaryBank"+i).disabled = disableFlag;
			}
		}
	}

	if (wireDest != WIRE_BOOK) {
	if (frm["wireBankSearch"] != null) {
			document.getElementById("wireBankSearchID").disabled = disableFlag;
			$('#wireBankSearchID').attr('class','ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only');
		}     
	frm["WireTransferPayee.DestinationBank.BankName"].disabled = disableFlag;
	frm["WireTransferPayee.DestinationBank.Street"].disabled = disableFlag;
	frm["WireTransferPayee.DestinationBank.Street2"].disabled = disableFlag;
	frm["WireTransferPayee.DestinationBank.City"].disabled = disableFlag;
	frm["WireTransferPayee.DestinationBank.State"].disabled = disableFlag;
	frm["WireTransferPayee.DestinationBank.ZipCode"].disabled = disableFlag;
	frm["WireTransferPayee.DestinationBank.RoutingFedWire"].disabled = disableFlag;

	if (wireDest == WIRE_DOMESTIC || wireDest == WIRE_INTERNATIONAL) {
	    frm["WireTransferPayee.Country"].disabled = disableFlag;
			$("#selectBeneficiaryCountryID").selectmenu('destroy').selectmenu({width:'14.5em'}).selectmenuStatusFlag;
	    frm["WireTransferPayee.DestinationBank.Country"].disabled = disableFlag;
	    $("#selectBankCountryID").selectmenu('destroy').selectmenu({width:'15em'}).selectmenuStatusFlag;
	    frm["WireTransferPayee.DestinationBank.RoutingSwift"].disabled = disableFlag;
	    frm["WireTransferPayee.DestinationBank.RoutingChips"].disabled = disableFlag;
	    frm["WireTransferPayee.DestinationBank.RoutingOther"].disabled = disableFlag;
	}
	if (wireDest != WIRE_DRAWDOWN) {
	    if ($("#addIntermediaryBankButtonID") != null) {
		//frm["wireIntermediaryBanks"].disabled = disableFlag;
		$("#addIntermediaryBankButtonID").removeAttr("style");
		$("#addIntermediaryBankButtonID").bind("click",selectInterBank);
		$("#addIntermediaryBankButtonID").attr("disbaled", disableFlag);
		$("#selectAffiliateBankID").selectmenu(selectmenuStatusFlag);
	    }        
	}
	} else {
		frm["affBank"].disabled = disableFlag;
	}
}

function saveSelect(idx) {
    ns.wire.origSelect = idx;
}

ns.wire.numberOfTransfers = 2;
function neverending(flag) {
    frm = document.WireNew;
    if(frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'] != null & frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value != "") {	
    	    	ns.wire.numberOfTransfers = frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value;
    }
    if (flag == "true") {
        frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value="";
        frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].disabled=true;
    } else {
        frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value = ns.wire.numberOfTransfers;
        frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].disabled=false;
    }
}

function enableRepeating(freq) {
	
    frm = document.WireNew;
    if (freq != <%= com.ffusion.beans.Frequencies.FREQ_NONE %>) {
	
        //if (frm.openEnded[0].disabled == true) {
            frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].disabled=false;           
            if(frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value === ''){
            	frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value="2";
            } 
           
            frm.openEnded[0].disabled=false;
            frm.openEnded[1].disabled=false;
			
            frm['<ffi:getProperty name="wireTask"/>.Type'].value="<%= com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER %>";
<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
            frm['<ffi:getProperty name="wireTask"/>.WireType'].value="<%= WireDefines.WIRE_TYPE_RECURRING %>";
</ffi:cinclude>
<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
            frm['<ffi:getProperty name="wireTask"/>.WireType'].value="<%= WireDefines.WIRE_TYPE_RECTEMPLATE %>";
</ffi:cinclude>
		var obj = document.getElementById('selectFrequencyID1');
		
		if(obj != null) {
			frm.selectFrequencyID1.disabled=false;
			$("#selectFrequencyID1").selectmenu("enable");
		}
		obj = document.getElementById('selectFrequencyID');
		if(obj != null) {
			frm.selectFrequencyID.disabled=false;
			$("#selectFrequencyID").selectmenu("enable");
		} 
		obj = document.getElementById('selectPaymentFrequencyID1');
		if(obj != null) {
			frm.selectPaymentFrequencyID1.disabled=false;
		} 
		obj = document.getElementById('selectPaymentFrequencyID');
		if(obj != null) {
			frm.selectPaymentFrequencyID.disabled=false;
		} 
		document.getElementById('isRecModel').value="true";
        //}
    } else {
        frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value="";
        frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].disabled=true;
        frm.openEnded[1].checked=true;
        frm.openEnded[0].disabled=true;
        frm.openEnded[1].disabled=true;
        frm['<ffi:getProperty name="wireTask"/>.Type'].value="<%= com.ffusion.beans.FundsTransactionTypes.FUNDS_TYPE_WIRE_TRANSFER %>";
<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
        frm['<ffi:getProperty name="wireTask"/>.WireType'].value="<%= WireDefines.WIRE_TYPE_SINGLE %>";
</ffi:cinclude>
<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
        frm['<ffi:getProperty name="wireTask"/>.WireType'].value="<%= WireDefines.WIRE_TYPE_TEMPLATE %>";
</ffi:cinclude>
    }
}

function checkRepeating() {
    frm = document.WireNew;
    if (frm['<ffi:getProperty name="wireTask"/>.Frequency'].options[frm['<ffi:getProperty name="wireTask"/>.Frequency'].selectedIndex].value != <%= com.ffusion.beans.Frequencies.FREQ_NONE %>) {
        <ffi:cinclude value1="${disableRec}" value2="" operator="equals">
            frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].disabled=false;
            frm.openEnded[0].disabled=false;
            frm.openEnded[1].disabled=false;
        </ffi:cinclude>
        if (frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value=="999") {
            frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].value="";
            <ffi:cinclude value1="${disableRec}" value2="" operator="equals">
                frm['<ffi:getProperty name="wireTask"/>.NumberTransfers'].disabled=true;
            </ffi:cinclude>
            frm.openEnded[0].checked=true;
        } else {
            frm.openEnded[1].checked=true;
        }
    }
}

function selectDestBank() {
    frm = document.WireNew;
    rtSwift = false;
    rtOther = false;
    rtChips = false;
    iban = false;

    bankName = frm["WireTransferPayee.DestinationBank.BankName"].value == "" ? false : true;
    bankCity = frm["WireTransferPayee.DestinationBank.City"].value == "" ? false : true;
    bankState = frm["WireTransferPayee.DestinationBank.State"].value == "" ? false : true;
    bankCountry = frm["WireTransferPayee.DestinationBank.Country"].value == "" ? false : true;
    rtFed = frm["WireTransferPayee.DestinationBank.RoutingFedWire"].value == "" ? false : true;
    if (wireDest == WIRE_DOMESTIC || wireDest == WIRE_INTERNATIONAL) {
        rtSwift = frm["WireTransferPayee.DestinationBank.RoutingSwift"].value == "" ? false : true;
        rtOther = frm["WireTransferPayee.DestinationBank.RoutingOther"].value == "" ? false : true;
        rtChips = frm["WireTransferPayee.DestinationBank.RoutingChips"].value == "" ? false : true;
    }
    
    // Setting IBAN for Domestic/International/Fed wire bank lookup
	if (wireDest == WIRE_DOMESTIC || wireDest == WIRE_INTERNATIONAL || wireDest == WIRE_FED) {
		iban = frm["WireTransferPayee.DestinationBank.IBAN"].value == "" ? false : true;
	}
    
	if (bankName || rtSwift || rtOther || rtFed || rtChips || bankCity || bankState || bankCountry || iban) {
    	frm["isBankSearch"].value="false";
    	frm["Inter"].value="no";    	
    } else {
    	frm["isBankSearch"].value="true";
    	frm["Inter"].value="no";    	
    }

    if (bankName || rtSwift || rtOther || rtFed || rtChips || bankCity || bankState || bankCountry || iban) destURL = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/wirebanklistselect.jsp?Inter=no&Edit=false&currentTask=${wireTask}'/>";
    else destURL = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/wirebanklist.jsp?Inter=no&Edit=false&currentTask=${wireTask}'/>";
    var preProcessType = "preProcess";
    if (wireDest == WIRE_DOMESTIC || wireDest == WIRE_INTERNATIONAL) { preProcessType = "preProcess"; }
    if (wireDest == WIRE_BOOK) { preProcessType = "preProcessBookWire"; }
    if (wireDest == WIRE_DRAWDOWN) { preProcessType = "preProcessDrawDownWire"; }
    if (wireDest == WIRE_FED) { preProcessType = "preProcessFedWire"; }
    
    actionURL = "/cb/pages/jsp/wires/addWireTransferAction_" + preProcessType + ".action";
    destURL = "/cb/pages/jsp/wires/getWireTransferBanksAction_init.action";
	ns.wire.searchDestinationBankForm(destURL, actionURL);
}

function selectInterBank() {
    frm = document.WireNew;
    actionURL = "/cb/pages/jsp/wires/getWireTransferBanksAction_init.action";
    targetURL = "<ffi:urlEncrypt url='/cb/pages/jsp/wires/getWireTransferBanksAction_init.action?Inter=yes&isBankSearch=true' />";
	ns.wire.searchDestinationBankForm(targetURL,actionURL);
}

function deleteInterBank(page,idx) {
	var action = $("#deleteIntermediaryBankAction").val();
	var url = "/cb/pages/jsp/wires/"+action;
	$.ajax({   
		type: "POST", 		
		url: url+"?delIntBank=" + idx,
		data: $("#newWiresTransferFormID").serialize(),		
		success: function(data) {
			$('#intermediaryBanksDivID').html(data);
		}   
	});
}

function selectRecModel(){
	var url = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/modifyWireTransferAction_init.action?isRecModel=true&recurringId=${recurringId}&ID=${ID}&transType=${transType}" />';
	ns.wire.editWireTransfers(url);
	
}

var theChild = null;
var theTarget = null;
var theAction = null;

function closeDestBank() {
    frm = document.WireNew;
	if (theChild != null)
	{
		try
		{
			if (!theChild.closed)
				theChild.close();
		} catch(e) {}
		theChild = null;
		frm.target = theTarget;
		frm.action = theAction;
	}
}

<%-- // End of the block not to be loaded if not entitled to free-form create. --%>
</ffi:cinclude>

function setDest(dest) {
    if (dest == WIRE_DOMESTIC) document.location = "<ffi:urlEncrypt url='wiretransfernew.jsp?wireTask=AddWireTransfer&wireDestination=DOMESTIC' />";
    if (dest == WIRE_INTERNATIONAL)  document.location = "<ffi:urlEncrypt url='wiretransfernew.jsp?wireTask=AddWireTransfer&wireDestination=INTERNATIONAL' />";
    if (dest == WIRE_HOST) document.location = "<ffi:urlEncrypt url='wirehost.jsp?wireTask=AddHostWire' />";
    if (dest == WIRE_DRAWDOWN) document.location = "<ffi:urlEncrypt url='wiredrawdown.jsp?wireTask=AddWireTransfer&wireDestination=DRAWDOWN' />";
    if (dest == WIRE_FED) document.location = "<ffi:urlEncrypt url='wirefed.jsp?wireTask=AddWireTransfer&wireDestination=FEDWIRE' />";
    if (dest == WIRE_BOOK) document.location = "<ffi:urlEncrypt url='wirebook.jsp?wireTask=AddWireTransfer&wireDestination=BOOKTRANSFER' />";
}

<ffi:cinclude value1="${wireTaskHost}" value2="true" operator="notEquals">
<%-- /*
* Do not attempt the onload and onfocus event javascript if the user is not entitled to free-form create, and they haven't
* selected a template to load yet. (and they're not working on a template or a wire in a batch)
*/ --%>
    <ffi:cinclude value1="${wireShowForm}" value2="true" operator="equals">
        
        window.onfocus=closeDestBank
    </ffi:cinclude>
</ffi:cinclude>

function updateDBStateInformation(obj) {
	var urlString = '/cb/pages/jsp/wires/addWireBeneficiaryAction_getStateForCountry.action';
	$.ajax({
		   type: "POST",
		   url: urlString,
		   data: {countryName: obj.value, CSRF_TOKEN: '<ffi:getProperty name="CSRF_TOKEN"/>'},
		   success: function(data){
				$("#wireStateSelectedId").html('<span></span>');
				$("#wireStateSelectedId").html(data);		   	
		   }
	});
}

// process input 
function roundExchangeRate() { 
  frm = document.WireNew;	
  var num = frm['<ffi:getProperty name="wireTask"/>.ExchangeRate'].value 
  frm['<ffi:getProperty name="wireTask"/>.ExchangeRate'].value = roundTo7DecimalPlaces(num) 
} 

// Round to 7 decimal places 
function roundTo7DecimalPlaces(n) { 

  ans = (Math.round(n * 10000000))/10000000 + "" 
  dot = ans.indexOf(".",0) 
  if (dot == -1) {ans = "1.0"} 
  else if (dot == ans.length - 7) {ans = ans + "0"} 
  return ans 
} 
</script>
<ffi:removeProperty name="isRecModel" />