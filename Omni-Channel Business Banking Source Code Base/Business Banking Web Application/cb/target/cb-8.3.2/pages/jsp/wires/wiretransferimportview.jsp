<%--
This is the import wire transfer confirmation page.

Pages that request this page
----------------------------
wiretransferimportwait.jsp
	Javascript auto-refresh

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
SUBMIT requests wiretransferimportsave.jsp

Pages included in this page
---------------------------
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireTransfers" %>
<ffi:help id="payments_wiretransferimportview" className="moduleHelpClass"/>

<%-- 
	In case of file upload this file will be loaded in Iframe used to upload file.
	In that case it will show javascript error as JQuery won't be available.
	Added following functionality to make sure error won't be shown.
 --%>
<s:include value="%{#session.PagesPath}/common/commonUploadIframeCheck_js.jsp" />

<ffi:setProperty name="DimButton" value="fileimport"/>
<s:include value="%{#session.PagesPath}/wires/inc/wire_buttons.jsp"/>


<%
String sortDatas = request.getParameter("ImportWireTransfers.ToggleSortedBy") ;
if(  sortDatas != null && session.getAttribute("ImportWireTransfers") != null)
{
	((WireTransfers)session.getAttribute("ImportWireTransfers")).setToggleSortedBy( sortDatas );
}
%>

        <div align="center">

            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="columndata ltrow2_color">
                        <div align="center">
                            <table width="720" border="0" cellspacing="0" cellpadding="3">

                            <%-- Show them warnings or notes --%>
                            <% String dateChanged = "";  %>
                            <ffi:getProperty name="AddWireTransfersFromImport" property="DateChanged" assignTo="dateChanged"/>

                            <% String nonBizDay = ""; %>
                            <ffi:getProperty name="AddWireTransfersFromImport" property="NonBusinessDay" assignTo="nonBizDay"/>

                            <% if ("true".equals(dateChanged) && "true".equals(nonBizDay)) { %>
                            <tr>
                                <%-- Both proc window is closed and tomorrow is non-biz day --%>
                                <td class="sectionsubhead" colspan="6" style="color:red" align="center">
                                <!--L10NStart-->NOTE: The wire processing window for today has closed,
                                so the Requested Date has been changed to tomorrow's date for
                                one or more wires. Since tomorrow is not a business day,
                                the Expected Value Date has been changed to the next business day.<!--L10NEnd--><br><br>
                                </td>
                            </tr>
                            <% } else if ("true".equals(dateChanged)) { %>
                            <tr>
                                <%-- DueDate changed because processing window is closed --%>
                                <td class="sectionsubhead" colspan="6" style="color:red" align="center">
                                <!--L10NStart-->NOTE: The wire processing window for today has closed.
                                The Requested Date has been changed to tomorrow's date
                                for one or more wires.<!--L10NEnd--><br><br>
                                </td>
                            </tr>
                            <% } else if ("true".equals(nonBizDay)) { %>
                            <tr>
                                <%-- DueDate changed because its not a business day --%>
                                <td class="sectionsubhead" colspan="6" style="color:red" align="center">
                                <!--L10NStart-->NOTE: The Requested Date is not a business day.
                                The Expected Value Date has been changed to the next business day
                                for one or more wires.<!--L10NEnd--><br><br>
                                </td>
                            </tr>
                            <% } %>

                            <%-- Duplicate Wire warning --%>
                            <% String dupeWire = ""; %>
                            <ffi:getProperty name="AddWireTransfersFromImport" property="DuplicateWire" assignTo="dupeWire"/>
                            <ffi:cinclude value1="<%= dupeWire %>" value2="true" operator="equals">
                            <tr>
                                <td class="sectionsubhead" colspan="6" style="color:red" align="center">
                                    <!--L10NStart-->WARNING: One or more duplicate wires exist for this
                                    business with the same Amount, Value Date, and Beneficiary.<!--L10NEnd--><br><br>
                                </td>
                            </tr>
                            </ffi:cinclude>

                                <tr>
                                    <td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead">&gt; <!--L10NStart-->Wire Import View<!--L10NEnd--></span><span class="sectionhead"><br>
                                        </span></td>
                                </tr>
                                <tr>
                                    <td class="tbrd_b ltrow2_color" colspan="6"><span class="sectionhead"><!--L10NStart-->Verify the wire payments for import.  When finished, click SUBMIT.<!--L10NEnd--></span><span class="sectionhead"><br>
                                        </span></td>
                                </tr>

<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="SortImage" scope="session"/>
<ffi:setProperty name="SortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="Reverse Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="Sort" width="24" height="8" border="0">'/>
<ffi:setProperty name="SortImage" property="Collection" value="ImportWireTransfers"/>
<ffi:process name="SortImage"/>
<ffi:setProperty name="SortImage" property="Compare" value="DateToPost"/>
                                <tr>
                                    <td span class="sectionsubhead" width="80">
                                    	<ffi:setProperty name="SortURL" value="wiretransferimportview.jsp?ImportWireTransfers.ToggleSortedBy=DateToPost" URLEncrypt="false"/>
                                       <!--L10NStart-->Date<!--L10NEnd--><a onclick="ns.wire.sortImportedResults('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
                                    </td>
                <ffi:setProperty name="SortImage" property="Compare" value="WirePayee.PayeeName"/>
                                    <td span class="sectionsubhead" width="220">
                                    	<ffi:setProperty name="SortURL" value="wiretransferimportview.jsp?ImportWireTransfers.ToggleSortedBy=WirePayee.PayeeName" URLEncrypt="false"/>
                                        <!--L10NStart-->Beneficiary<!--L10NEnd--><a onclick="ns.wire.sortImportedResults('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
                                    </td>
                <ffi:setProperty name="SortImage" property="Compare" value="FromAccountNum"/>
                                    <td span class="sectionsubhead" width="180">
                                    	<ffi:setProperty name="SortURL" value="wiretransferimportview.jsp?ImportWireTransfers.ToggleSortedBy=FromAccountNum" URLEncrypt="false"/>
                                        <!--L10NStart-->Account<!--L10NEnd--><a onclick="ns.wire.sortImportedResults('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
                                    </td>
                                    <td span class="sectionsubhead" width="85">
                                        <!--L10NStart-->Status<!--L10NEnd-->
                                    </td>
                <ffi:setProperty name="SortImage" property="Compare" value="AMOUNT"/>
                                    <td span class="sectionsubhead" align="right" width="85">
                                    	<ffi:setProperty name="SortURL" value="wiretransferimportview.jsp?ImportWireTransfers.ToggleSortedBy=AMOUNT" URLEncrypt="false"/>
                                        <!--L10NStart-->Amount<!--L10NEnd--><a onclick="ns.wire.sortImportedResults('<ffi:getProperty name="SortURL"/>');"><ffi:getProperty name="SortImage" encode="false"/></a>
                                    </td>
                                    <td span class="sectionsubhead" width="100">&nbsp;</td>
                                </tr>

                                <ffi:list collection="ImportWireTransfers" items="wireTransfer">
                                    <tr>
                                        <td class="columndata"><ffi:getProperty name="wireTransfer" property="DateToPost"/></td>
                                        <td class="columndata">
                                        <ffi:getProperty name="wireTransfer" property="WirePayee.PayeeName"/>
                                        </td>
                                        <td class="columndata">

                                                <%-- check to see if the account is in the Accounts collection, so we can show its NickName --%>
                                                <ffi:setProperty name="BankingAccounts" property="Filter" value="ID=${wireTransfer.FromAccountID}"/>
                                                <ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="notEquals">
                                                    <ffi:list collection="BankingAccounts" items="acct" startIndex="1" endIndex="1">
                                                        <ffi:getProperty name="acct" property="DisplayText"/> -
                                                        <ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
                                                            <ffi:getProperty name="acct" property="NickName"/> -
                                                        </ffi:cinclude>
                                                        <ffi:getProperty name="acct" property="CurrencyCode"/>
                                                    </ffi:list>
                                                </ffi:cinclude>
                                                <%-- if the account is not in the Accounts collection, it's not entitled.  Show the account type instead. --%>
                                                <ffi:cinclude value1="${BankingAccounts.Size}" value2="0" operator="equals">
                                                    <ffi:object id="AccountDisplayTextTask" name="com.ffusion.tasks.util.GetAccountDisplayText" scope="session"/>
                                                    <ffi:setProperty name="AccountDisplayTextTask" property="AccountID" value="${wireTransfer.FromAccountID}"/>
                                                    <ffi:process name="AccountDisplayTextTask"/>
                                                    <ffi:getProperty name="AccountDisplayTextTask" property="AccountDisplayText"/>
                                                    <ffi:removeProperty name="AccountDisplayTextTask"/>
                                                </ffi:cinclude>
                                                <ffi:setProperty name="BankingAccounts" property="Filter" value="All"/>

                                        </td>
                                        <td class="columndata"><!--L10NStart-->Pending Submit<!--L10NEnd--></td>
                                        <td class="columndata" align="right"><ffi:getProperty name="wireTransfer" property="Amount"/></td>
                                        <td class="columndata" align="right" nowrap>
                                        </td>
                                    </tr>
                                </ffi:list>

                                <tr>
                                    <td colspan="6"><br></td>
                                </tr>
                                <tr>
                                <td colspan="6" align="center" nowrap>
                                	<%-- <form id="submitFileImportFormID" action="wiretransferimportsave.jsp" method="post" name="FormName"> --%>
                                    <s:form id="submitFileImportFormID" action="WireFileUploadAction_save" method="post" name="FormName" namespace="/pages/fileupload">
                    					<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                                    </s:form>
                                </td>
                                    
                                </tr>
                                <tr>
                                	<td colspan="6" align="center" nowrap>
                                	    <%-- This button is jqueryfied by ns.common.jqueryWireImportView in common.js --%>
                                	    <sj:a id="anchor_wireupload_view_cancel" href="#" button="true" onClickTopics="wireUploadCancelOnClickTopics"><s:text name="jsp.default_82"></s:text></sj:a>                             			
                             			<sj:a id="submitFileImportFormButtonID" href="#" 
                             			formIds="submitFileImportFormID" 
                             			button="true" 
                             			targets="checkFileImportResultsDialogID"
                             			onErrorTopics="errorImportingWiresFileFormTopics"><s:text name="jsp.default_395"></s:text></sj:a>

                                	</td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
