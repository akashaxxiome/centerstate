<%--
This is the final sent/saved page for wire batches.

Pages that request this page
----------------------------
wirebatchffconfirm.jsp
	SUBMIT WIRE BATCH button
wirebatchhostconfirm.jsp
	SUBMIT WIRE BATCH button
wirebatchintffconfirm.jsp
	SUBMIT WIRE BATCH button
wirebatchinttempconfirm.jsp
	SUBMIT WIRE BATCH button
wirebatchtempconfirm.jsp
	SUBMIT WIRE BATCH button

Pages this page requests
------------------------
DONE requests wiretransfers.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../common/wire_labels.jsp"%>
<%--=================== PAGE SECURITY CODE - Part 1 Start ====================--%>

<ffi:help id="payments_wirebatchsend" className="moduleHelpClass"/>
<%	session.setAttribute( "Credential_Op_Type", ""+ com.ffusion.beans.authentication.AuthConsts.OPERATION_TYPE_TRANSACTION) ; %>
<ffi:setProperty name="LoadURL" value="payments/wirebatchsend.jsp"/>
<ffi:setProperty name="securityMessageKey" value="jsp/payments/security-check.jsp-6"/>

<ffi:setProperty name="securitySubMenu" value="${subMenuSelected}"/>
<ffi:setProperty name='securityPageHeading' value='${PageHeading}'/>

<% if( !( session.getAttribute( "UserSecurityKey" ) instanceof com.ffusion.tasks.util.ImmutableString) )  { %>
	<script>
		//window.location = "<ffi:getProperty name="SecurePath"/>payments/security-check-wait.jsp";
	</script>
<%} %>
<ffi:cinclude value1="${UserSecurityKey.ImmutableString}" value2="${verified}" operator="notEquals">
	<script>
		//window.location = "<ffi:getProperty name="SecurePath"/>payments/security-check-wait.jsp";
	</script>
</ffi:cinclude>
<% if( session.getAttribute( "UserSecurityKey" ) instanceof com.ffusion.tasks.util.ImmutableString )  { %>
<%-- 
<ffi:cinclude value1="${UserSecurityKey.ImmutableString}" value2="${verified}" operator="equals">
--%>
<%--=================== PAGE SECURITY CODE - Part 1 End ======================--%>
	<ffi:removeProperty name="verified"/>


<%-- 
<ffi:cinclude value1="${AddWireBatch}" value2="" operator="notEquals" >
	<ffi:setProperty name="AddWireBatch" property="Process" value="true"/>--%>
	<%--<ffi:process name="AddWireBatch"/>
</ffi:cinclude>
<ffi:cinclude value1="${ModifyWireBatch}" value2="" operator="notEquals" >
	<ffi:setProperty name="ModifyWireBatch" property="Process" value="true"/>--%>
	<%--<ffi:process name="ModifyWireBatch"/>
</ffi:cinclude>
--%>
<%-- String batchDest = "";
<ffi:getProperty name="${AddEditTask}" property="BatchDestination" assignTo="batchDest"/>
<%
String wireType = "";
if (batchDest.equals(WireDefines.WIRE_DOMESTIC)) wireType = DOMESTIC;
if (batchDest.equals(WireDefines.WIRE_INTERNATIONAL)) wireType = INTERNATIONAL;
if (batchDest.equals(WireDefines.WIRE_BOOK)) wireType = BOOK;
if (batchDest.equals(WireDefines.WIRE_DRAWDOWN)) wireType = DRAWDOWN;
if (batchDest.equals(WireDefines.WIRE_FED)) wireType = FED;
if (batchDest.equals(WireDefines.WIRE_HOST)) wireType = HOST;
%> 
<ffi:setProperty name="${AddEditTask}" property="DateFormat" value="${UserLocale.DateFormat}"/>
--%>
<div class="leftPaneWrapper" role="form" aria-labelledby="wireBatchSummaryHeader"><div class="leftPaneInnerWrapper">
	<div class="header"><h2 id="wireBatchSummaryHeader">Wire Batch Summary</h2></div>
	<div class="summaryBlock"  style="padding: 15px 10px;">
		<div style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection" id="verifyWireBeneficiaryLabel">Batch Name: </span>
				<span class="inlineSection floatleft labelValue" id="verifyWireBeneficiaryValue"><ffi:getProperty name="wireBatchObject" property="BatchName"/></span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection">Batch Type: </span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="batchType"/></span>
		</div>
		<div class="marginTop10 clearBoth floatleft" style="width:100%">
				<span class="sectionsubhead sectionLabel floatleft inlineSection">Application Type: </span>
				<span class="inlineSection floatleft labelValue"><ffi:getProperty name="wireBatchObject" property="BatchType"/></span>
		</div>
	</div>
</div></div>
<div class="confirmPageDetails">
<ffi:list collection="wireBatchObject.Wires" items="wire">
	<ffi:cinclude value1="${wire.Status}" value2="<%=String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_NOT_ALLOWED)%>" operator="equals">
		<ffi:setProperty name="ApprovalError" value="true"/>
	</ffi:cinclude>
</ffi:list>
<%-- this isn't a real error - the batch was added and saved, but some entried exceeded approvals.  Display it here. --%>
<ffi:cinclude value1="${ApprovalError}" value2="true" operator="equals">
	<tr>
		<td colspan="4" class="sectionhead errorcolor" align="center"><s:text name="wire.batchApprovalError"/></td>
	</tr>
</ffi:cinclude>
<ffi:removeProperty name="ApprovalError"/>
<div class="confirmationDetails" style="width:50%; float:left">
	<span><!--L10NStart-->Tracking ID<!--L10NEnd-->: </span>
	<span><ffi:getProperty name="wireBatchObject" property="TrackingID"/></span>
</div>
<div class="confirmationDetails" style="width:49%; float:left">
	<span><!--L10NStart-->Application ID<!--L10NEnd-->: </span>
	<span><ffi:getProperty name="wireBatchObject" property="ID"/></span>
</div>
<div class="marginTop10 clearBoth floatleft" style="width:100%"></div>
<div class="blockWrapper" role="form" aria-labelledby="wireBatchInfoHeader">
	<div  class="blockHead"><h2 id="wireBatchInfoHeader">Batch Information</h2></div>
	<s:if test="%{wireBatchObject.batchDestination != @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
	<div class="blockContent label140">
		<%-- <div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" id="confirmWireBatchNameLabel">Batch Name: </span>
                <span class="columndata" id="confirmWireBatchNameValue"><ffi:getProperty name="wireBatchObject" property="BatchName"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel" id="confirmWireBatchTypeLabel">Batch Type: </span>
                <span class="columndata" id="confirmWireBatchTypeValue"><ffi:getProperty name="batchType"/></span>
			</div>
		</div> --%>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span class="sectionsubhead sectionLabel" id="confirmRequestedDateLabel"><s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_HOST}"> Requested Processing Date</s:if><s:else> <!--L10NStart-->Requested Value Date<!--L10NEnd--></s:else>: </span>
                <span class="columndata" id="confirmRequestedDateValue"><ffi:getProperty name="wireBatchObject" property="DueDate"/></span>
			</div>
			<s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_HOST}">
				<div class="inlineBlock">
	                  <span id="confirmSettledDateLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Settlement Date: <!--L10NEnd--></span>
	                  <span id="confirmSettledDateValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="SettlementDate"/></span>
	            </div>
			</s:if>
		</div>
		<div class="blockRow">
				<span id="confirmExpectedDateLabel" class="sectionsubhead sectionLabel"><s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_HOST}"><!--L10NStart-->Expected Processing Date<!--L10NEnd--></s:if>  <s:else><!--L10NStart-->Expected Value Date<!--L10NEnd--></s:else>: </span>
                <span id="confirmExpectedDateValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="DateToPost"/></span>
		</div>
	</div>
	</s:if>
	<s:else>
	 <%--  Show international wire info --%>
	<div class="blockContent">
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmWireBatchNameLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Batch Name<!--L10NEnd--></span>
                <span id="confirmWireBatchNameValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchName"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmApplicationTypeLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Application Type<!--L10NEnd--></span>
                <span id="confirmApplicationTypeValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchType"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmRequestedDateLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Requested Processing Date<!--L10NEnd--></span>
                <span id="confirmRequestedDateValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="DueDate"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmSettledDateLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Settlement Date<!--L10NEnd--></span>
                <span id="confirmSettledDateValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="SettlementDate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmExpectedDateLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Expected Processing Date<!--L10NEnd--></span>
                <span id="confirmExpectedDateValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="DateToPost"/></span>
			</div>
			<div class="inlineBlock">
				<span class="sectionsubhead sectionLabel"><!--L10NStart-->Debit Currency<!--L10NEnd--></span>
                <span class="columndata"><ffi:getProperty name="wireBatchObject" property="OrigCurrency"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmPayementCurrencyLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Payment Currency<!--L10NEnd--></span>
                <span id="confirmPayementCurrencyValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="PayeeCurrencyType"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmExchangeRateLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Exchange Rate<!--L10NEnd--></span>
                <span id="confirmExchangeRateValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="ExchangeRate"/></span>
			</div>
		</div>
		<div class="blockRow">
			<div class="inlineBlock" style="width: 50%">
				<span id="confirmMathRuleLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Math Rule<!--L10NEnd--></span>
                <span id="confirmMathRuleValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="MathRule"/></span>
			</div>
			<div class="inlineBlock">
				<span id="confirmContractNoLabel" class="sectionsubhead sectionLabel"><!--L10NStart-->Contract Number<!--L10NEnd--></span>
                <span id="confirmContractNoValue" class="columndata"><ffi:getProperty name="wireBatchObject" property="ContractNumber"/></span>
			</div>
		</div>
	</div>
</s:else>
</div>

<div class="blockWrapper marginTop20" role="form" aria-labelledby="wireBatchIncludeHeader">
	<div  class="blockHead"><h2 id="wireBatchIncludeHeader">Included Batches</h2></div>
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
                        <tr class="header">
                            <td id="confirmWireBeneficiaryLabel" align="left" class="sectionsubhead" width="30%"><s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DRAWDOWN}">Drawdown Account</s:if> <s:else>Beneficiary</s:else></td>
                            <td id="confirmWireStatusLabel" align="left" class="sectionsubhead" width="20%"><!--L10NStart-->Status<!--L10NEnd--></td>
                            <td id="confirmWireTrackIdLabel" align="left" class="sectionsubhead" width="40%"><!--L10NStart-->Tracking ID<!--L10NEnd--></td>
                            <td id="confirmWireAmountLabel" align="left" class="sectionsubhead" width="10%"><!--L10NStart-->Amount<!--L10NEnd--></td>
                        </tr>
                        <% int idCount=0; %>
                        <ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="ACTION!!del,Status!!3,AND" />
                        
                          <s:iterator value="wireBatchObject.Wires" var="wire" > 
                        <tr>
                            <td id="confirmWireBeneficiaryValue<%=idCount %>" align="left" class="columndata" nowrap><s:property  value="WirePayee.PayeeName"/>

                                	<s:if test="%{WirePayee.NickName != null && WirePayee.NickName != ''}"> (<s:property  value="WirePayee.NickName"/>)</s:if>      
                            </td>

							<td id="confirmWireStausValue<%=idCount %>" class="<ffi:cinclude value1='${wire.Status}' value2='<%=String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_NOT_ALLOWED)%>' operator='notEquals'>columndata</ffi:cinclude><ffi:cinclude value1='${wire.Status}' value2='<%=String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_NOT_ALLOWED)%>' operator='equals'>columndata_error</ffi:cinclude>" align="left" nowrap> <s:property  value="StatusName"/></td>
                            <td id="confirmWireTrackIdValue<%=idCount %>" align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="TrackingID"/></td>

                            <td id="confirmWireAmountValue<%=idCount++ %>" align="left" class="columndata" nowrap>
                         	 <s:if test="%{Action ==  @com.ffusion.beans.wiretransfers.WireDefines@WIRE_ACTION_ADD}">
                         	 	<s:if test="%{wireBatchObject.batchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
                         	 		<s:property  value="AmountValue.CurrencyStringNoSymbol"/>
                         	 	</s:if><s:else>
                         	 		<s:property value="OrigAmount"/>                         	 			
                         	 	</s:else>
                      			                                
                                </s:if>
                				 <s:else>
                				 <s:if test="%{wireBatchObject.batchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
                         	 		<s:property  value="AmountValue.CurrencyStringNoSymbol"/>
                         	 	</s:if><s:else>
                				 	<s:property value="OrigAmount"/>*
                				</s:else>                                	
                                </s:else>
                            </td>
                        </tr>
                       </s:iterator>
                        
                        <ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
                        
                        <tr>
                            <td align="left" class="columndata" colspan="2">&nbsp;</td>
                            <td id="totalLabel" align="left" class="columndata" align="right" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                            <td id="totalValue" align="left" class="tbrd_t sectionsubhead" valign="bottom" nowrap>
                             <s:if test="%{!isModifyAction}">
                                <ffi:getProperty name="wireBatchObject" property="Amount"/>
                             </s:if> <s:else>
                                
								<s:if test="%{wireBatchObject.batchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
	                           	  <ffi:getProperty name="wireBatchObject" property="TotalDebit"/>
	                            </s:if> <s:else>	                             
	                                <ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>	                             
	                             </s:else>
                            </s:else>
                            </td>
                        </tr>
                        <ffi:removeProperty name="GetAmounts"/>
                    </table>
	</div>
</div>
            <table width="750" cellpadding="0" cellspacing="0" border="0">
                <tr>
                <td class="columndata ltrow2_color" align="center">
                   <%--  <table width="715" border="0" cellspacing="0" cellpadding="3">
						<ffi:list collection="wireBatchObject.Wires" items="wire">
							<ffi:cinclude value1="${wire.Status}" value2="<%=String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_NOT_ALLOWED)%>" operator="equals">
								<ffi:setProperty name="ApprovalError" value="true"/>
							</ffi:cinclude>
						</ffi:list>
						this isn't a real error - the batch was added and saved, but some entried exceeded approvals.  Display it here.
						<ffi:cinclude value1="${ApprovalError}" value2="true" operator="equals">
							<tr>
								<td colspan="4" class="sectionhead errorcolor" align="center"><s:text name="wire.batchApprovalError"/></td>
							</tr>
						</ffi:cinclude>
						<ffi:removeProperty name="ApprovalError"/> 
						<tr>
							<td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Tracking ID<!--L10NEnd--></td>
							<td align="left" width="239" class="columndata"><ffi:getProperty name="wireBatchObject" property="TrackingID"/></td>
							<td align="left" width="135" class="sectionsubhead"><!--L10NStart-->Application ID<!--L10NEnd--></td>
							<td align="left" width="202" class="columndata"><ffi:getProperty name="wireBatchObject" property="ID"/></td>
						</tr>
					</table>
					<br>--%>
                    <%-- <table width="98%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td align="left" class="tbrd_b sectionhead">&gt; <!--L10NStart-->Batch Information<!--L10NEnd--></td>
                        </tr>
                    </table>
                    <br>
                    <s:if test="%{wireBatchObject.batchDestination != @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
                    <table width="715" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="357" valign="top">
                                <table width="355" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td id="confirmWireBatchNameLabel" align="left" width="115" class="sectionsubhead"><!--L10NStart-->Batch Name<!--L10NEnd--></td>
                                        <td id="confirmWireBatchNameValue" align="left" width="228" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchName"/></td>
                                    </tr>
                                    <tr>
                                        <td id="confirmWireBatchTypeLabel" align="left" class="sectionsubhead"><!--L10NStart-->Batch Type<!--L10NEnd--></td>
                                        <td id="confirmWireBatchTypeValue" align="left" class="columndata"><ffi:getProperty name="batchType"/></td>
                                    </tr>
                                    <tr>
                                        <td id="confirmApplicationTypeLabel" align="left" class="sectionsubhead"><!--L10NStart-->Application Type<!--L10NEnd--></td>
                                        <td id="confirmApplicationTypeValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchType"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                            <td width="348" valign="top">
                                <table width="347" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td id="confirmRequestedDateLabel" align="left" width="135" class="sectionsubhead"> <s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_HOST}"> Requested Processing Date</s:if><s:else> <!--L10NStart-->Requested Value Date<!--L10NEnd--></s:else></td>
                                        <td id="confirmRequestedDateValue" align="left" width="200" class="columndata"><ffi:getProperty name="wireBatchObject" property="DueDate"/></td>
                                    </tr>
                                  <s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_HOST}">
									<tr>
                                        <td id="confirmSettledDateLabel" align="left" class="sectionsubhead"><!--L10NStart-->Settlement Date<!--L10NEnd--></td>
                                        <td id="confirmSettledDateValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="SettlementDate"/></td>
                                    </tr>
									</s:if>
                                    <tr>
                                        <td id="confirmExpectedDateLabel" align="left" class="sectionsubhead"><s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_HOST}"><!--L10NStart-->Expected Processing Date<!--L10NEnd--></s:if>  <s:else><!--L10NStart-->Expected Value Date<!--L10NEnd--></s:else></td>
                                        <td id="confirmExpectedDateValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="DateToPost"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </s:if>
                    <s:else>
			  Show international wire info
					<table width="715" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="357" valign="top">
                                <table width="355" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td id="confirmWireBatchNameLabel" align="left" width="160" class="sectionsubhead"><!--L10NStart-->Batch Name<!--L10NEnd--></td>
                                        <td id="confirmWireBatchNameValue" align="left" width="183" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchName"/></td>
                                    </tr>
                                    <tr>
                                        <td id="confirmApplicationTypeLabel" align="left" class="sectionsubhead"><!--L10NStart-->Application Type<!--L10NEnd--></td>
                                        <td id="confirmApplicationTypeValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="BatchType"/></td>
                                    </tr>
                                    <tr>
                                        <td id="confirmRequestedDateLabel" align="left" class="sectionsubhead"><!--L10NStart-->Requested Processing Date<!--L10NEnd--></td>
                                        <td id="confirmRequestedDateValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="DueDate"/></td>
                                    </tr>
                                    <tr>
                                        <td id="confirmSettledDateLabel" align="left" class="sectionsubhead"><!--L10NStart-->Settlement Date<!--L10NEnd--></td>
                                        <td id="confirmSettledDateValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="SettlementDate"/></td>
                                    </tr>
                                    <tr>
                                        <td id="confirmExpectedDateLabel" align="left" class="sectionsubhead"><!--L10NStart-->Expected Processing Date<!--L10NEnd--></td>
                                        <td id="confirmExpectedDateValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="DateToPost"/></td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
                            <td width="348" valign="top">
                                <table width="347" cellpadding="3" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" width="115" class="sectionsubhead"><!--L10NStart-->Debit Currency<!--L10NEnd--></td>
                                        <td align="left" width="220" class="columndata">
                                            <ffi:getProperty name="wireBatchObject" property="OrigCurrency"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td id="confirmPayementCurrencyLabel" align="left" class="sectionsubhead"><!--L10NStart-->Payment Currency<!--L10NEnd--></td>
                                        <td id="confirmPayementCurrencyValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="PayeeCurrencyType"/></td>
                                    </tr>
                                    <tr>
                                        <td id="confirmExchangeRateLabel" align="left" class="sectionsubhead"><!--L10NStart-->Exchange Rate<!--L10NEnd--></td>
                                        <td id="confirmExchangeRateValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="ExchangeRate"/></td>
                                    </tr>
                                    <tr>
                                        <td id="confirmMathRuleLabel" align="left" class="sectionsubhead"><!--L10NStart-->Math Rule<!--L10NEnd--></td>
                                        <td id="confirmMathRuleValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="MathRule"/></td>
                                    </tr>
                                    <tr>
                                        <td id="confirmContractNoLabel" align="left" class="sectionsubhead"><!--L10NStart-->Contract Number<!--L10NEnd--></td>
                                        <td id="confirmContractNoValue" align="left" class="columndata"><ffi:getProperty name="wireBatchObject" property="ContractNumber"/></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
</s:else>
					
                    <br> 
                    <table width="98%" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td id="confirmIncludeBatchesLabel" align="left" class="tbrd_b sectionhead">&gt; <!--L10NStart-->Included Batches<!--L10NEnd--></td>
                        </tr>
                    </table>--%>
                    <%-- <table width="450" border="0" cellspacing="0" cellpadding="3">
                        <tr>
                            <td id="confirmWireBeneficiaryLabel" align="left" class="sectionsubhead" width="30%"><s:if test="%{batchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DRAWDOWN}">Drawdown Account</s:if> <s:else>Beneficiary</s:else></td>
                            <td id="confirmWireStatusLabel" align="left" class="sectionsubhead" width="20%"><!--L10NStart-->Status<!--L10NEnd--></td>
                            <td id="confirmWireTrackIdLabel" align="left" class="sectionsubhead" width="40%"><!--L10NStart-->Tracking ID<!--L10NEnd--></td>
                            <td id="confirmWireAmountLabel" align="left" class="sectionsubhead" width="10%"><!--L10NStart-->Amount<!--L10NEnd--></td>
                        </tr>
                        <% int idCount=0; %>
                        <ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="ACTION!!del,Status!!3,AND" />
                        
                          <s:iterator value="wireBatchObject.Wires" var="wire" > 
                        <tr>
                            <td id="confirmWireBeneficiaryValue<%=idCount %>" align="left" class="columndata" nowrap><s:property  value="WirePayee.PayeeName"/>

                                	<s:if test="%{WirePayee.NickName != ''}"> (<s:property  value="WirePayee.NickName"/>)</s:if>      
                            </td>

							<td id="confirmWireStausValue<%=idCount %>" class="<ffi:cinclude value1='${wire.Status}' value2='<%=String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_NOT_ALLOWED)%>' operator='notEquals'>columndata</ffi:cinclude><ffi:cinclude value1='${wire.Status}' value2='<%=String.valueOf(WireDefines.WIRE_STATUS_APPROVAL_NOT_ALLOWED)%>' operator='equals'>columndata_error</ffi:cinclude>" align="left" nowrap> <s:property  value="StatusName"/></td>
                            <td id="confirmWireTrackIdValue<%=idCount %>" align="left" class="columndata" nowrap><ffi:getProperty name="wire" property="TrackingID"/></td>

                            <td id="confirmWireAmountValue<%=idCount++ %>" align="left" class="columndata" nowrap>
                         	 <s:if test="%{Action ==  @com.ffusion.beans.wiretransfers.WireDefines@WIRE_ACTION_ADD}">
                      				<s:property  value="AmountValue.CurrencyStringNoSymbol"/>                                
                                </s:if>
                				 <s:else>
                				 	<s:property value="OrigAmount"/>*                                	
                                </s:else>
                            </td>
                        </tr>
                       </s:iterator>
                        
                        <ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
                        
                        <tr>
                            <td align="left" class="columndata" colspan="2">&nbsp;</td>
                            <td id="totalLabel" align="left" class="columndata" align="right" valign="bottom" nowrap>Total (<ffi:getProperty name="SecureUser" property="BaseCurrency"/>):</td>
                            <td id="totalValue" align="left" class="tbrd_t sectionsubhead" valign="bottom" nowrap>
                             <s:if test="%{!isModifyAction}">
                                <ffi:getProperty name="wireBatchObject" property="Amount"/>
                             </s:if> <s:else>
                                <ffi:getProperty name="wireBatchObject" property="TotalOrigAmount"/>
                            </s:else>
                            </td>
                        </tr>
                        <ffi:removeProperty name="GetAmounts"/>
                    </table> --%>
                    <%-- <form action="wiretransfers.jsp" method="post" name="FormName">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
											<input type="Hidden" name="Refresh" value="true">
	                    <sj:a id="sendFormButtonOnConfirm" 
							button="true" 
							onClickTopics="cancelWireBatchForm"
						><s:text name="jsp.default_175" /><!-- DONE -->
						</sj:a>
                    </form> --%>
                    </td>
                </tr>
            </table>
<form action="wiretransfers.jsp" method="post" name="FormName">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="Hidden" name="Refresh" value="true">
	<div class="btn-row">
                <sj:a id="sendFormButtonOnConfirm" 
				button="true" summaryDivId="summary" buttonType="done" 
				onClickTopics="showSummary,cancelWireBatchForm"
			><s:text name="jsp.default_175" /><!-- DONE -->
			</sj:a>
	</div>
</form>
</div>

<ffi:removeProperty name="AddWireBatch"/>
<ffi:removeProperty name="ModifyWireBatch"/>
<ffi:removeProperty name="DontInitializeBatch"/>
<ffi:removeProperty name="wireDestinationType"/>


<%--=================== PAGE SECURITY CODE - Part 2 Start ====================--%>
<%}%>
<%--=================== PAGE SECURITY CODE - Part 2 End =====================--%>
