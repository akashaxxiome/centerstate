	<%--
This file contains the batch-specific fields for a non-international wire batch

It is included on all add wire batch pages
	wirebatchff.jsp
	wirebatchhost.jsp
	wirebatchtemp.jsp
--%>
<%@page import="com.ffusion.beans.wiretransfers.WireBatch"%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<script>
	$(function(){
		$("#wireBatchTypeSelectID").selectmenu({width:'260px'});
	});
</script>
<%
String task = "WireBatchObject";
 %>
					<div id="wireBatchNameLabel" ><span class="sectionsubhead"><!--L10NStart--><label for="wireBatchName">Batch Name</label><!--L10NEnd--></span><span class="required" title="required">*</span></div>
					<div class="marginTop10"><input id="wireBatchName" type="Text" name="<%= task %>.BatchName" value="<ffi:getProperty name="<%= task %>" property="BatchName"/>" class="ui-widget-content ui-corner-all txtbox" size="35" maxlength="128" ></div>
					<div class="marginTop10"><span id="BatchNameError"></span></div>
					<s:if test="%{WireBatchObject.BatchDestination != @com.ffusion.beans.wiretransfers.WireDefines@WIRE_HOST}">
						<div class="marginTop10" id="wireBatchApplicationTypeLabel"><span class="sectionsubhead"><!--L10NStart--><label for="wireBatchTypeSelectID">Application Type</label><!--L10NEnd--></span><span class="required" title="required">*</span></div>
						<div class="marginTop10">
							<select name="<%= task %>.BatchType" class="txtbox" onchange="changeForm(this.options[this.selectedIndex].value);"  <s:if test="%{!(isFreeFormEntitled && isTemplateEntitled)}">disabled</s:if> id="wireBatchTypeSelectID" >
								<s:if test="%{isFreeFormEntitled}"><option value="<s:property value="@com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_FREEFORM" />"  <s:if test="%{WireBatchObject.BatchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_FREEFORM}">selected</s:if>><!--L10NStart-->Free-form<!--L10NEnd--></option> </s:if>
								<s:if test="%{isTemplateEntitled}"><option value="<s:property value="@com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_TEMPLATE"/>" <s:if test="%{WireBatchObject.BatchType == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_BATCH_TYPE_TEMPLATE}">selected</s:if>><!--L10NStart-->Template<!--L10NEnd--></option></s:if>						
							</select>
						</div>
					</s:if>
					<div class="marginTop10" id="wireBatchDateLabel">
						<s:if test="%{WireBatchObject.BatchDestination != @com.ffusion.beans.wiretransfers.WireDefines@WIRE_HOST}"> <span class="sectionsubhead"><!--L10NStart--><label for="Wire_Batch_FieldsDateId">Value Date</label><!--L10NEnd--></span><span class="required" title="required">*</span> </s:if>
						<s:else> <span class="sectionsubhead"> <!--L10NStart--><label for="Wire_Batch_FieldsDateId">Processing Date</label><!--L10NEnd--></span><span class="required" title="required">*</span> </s:else>
					</div>
					<div class="marginTop10"><%-- <input type="Text" name="<%= task %>.DueDate" class="txtbox" value="<ffi:getProperty name="<%= task %>" property="DueDate"/>" size="11">
					<a onclick="window.open('<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.DueDate&wireDest=${AddWireBatch.BatchDestination}"/>','Calendar','width=350,height=300');" href="#"><img src="/cb/web/multilang/grafx/i_calendar.gif" alt="" width="19" height="16" align="absmiddle" border="0"></a>--%>
					<sj:datepicker
						value="%{WireBatchObject.DueDate}"
						id="Wire_Batch_FieldsDateId"
						name="WireBatchObject.DueDateString"
						label="Date"
						maxlength="10"
						cssStyle="width:110px"
						buttonImageOnly="true"
						cssClass="ui-widget-content ui-corner-all" />
					<script>
					var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.DueDate&wireDest=${AddWireBatch.BatchDestination}"/>';
                    ns.common.enableAjaxDatepicker("Wire_Batch_FieldsDateId", tmpUrl);
                    $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>');
					</script>
					<div class="marginTop10"><span id="DueDateError"></span></div>
					</div>
					<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_HOST}">
						<div class="sectionsubhead marginTop10" ><!--L10NStart--><label for="SettlementDate">Settlement Date</label><!--L10NEnd--></div>
						<div class="marginTop10"><%--<input type="Text" name="<%= task %>.SettlementDate" class="txtbox" value="<ffi:getProperty name="<%= task %>" property="SettlementDate"/>" size="11" class="ui-widget-content ui-corner-all txtbox">
						<a onclick="window.open('<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.SettlementDate&wireDest=${AddWireBatch.BatchDestination}"/>','Calendar','width=350,height=300');" href="#"><img src="/cb/web/multilang/grafx/i_calendar.gif" alt="" width="19" height="16" align="absmiddle" border="0"></a> --%>
						
						<sj:datepicker
							value="%{WireBatchObject.SettlementDate}"
							id="SettlementDate"
							name="WireBatchObject.SettlementDateString"
							label="Date"
							maxlength="10"
							cssStyle="width:110px"
							buttonImageOnly="true"
							cssClass="ui-widget-content ui-corner-all"/>
						<script>
							var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=14&calDisplay=select&calForm=frmBatch&calTarget=AddWireBatch.SettlementDate&wireDest=${AddWireBatch.BatchDestination}"/>';
		                    ns.common.enableAjaxDatepicker("SettlementDate", tmpUrl);
		                    $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>');
						</script>
						</div>
						<div class="marginTop10"><span id="SettlementDateError"></span></div>
					</s:if>
