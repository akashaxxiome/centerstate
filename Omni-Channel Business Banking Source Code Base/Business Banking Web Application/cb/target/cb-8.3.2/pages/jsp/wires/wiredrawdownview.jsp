<%--
This is the view drawdown wire page.

This page is not much more than a wrapper.  The main contents of the view
drawdown wire page can be found in the common include file
common/include-view-wiretransfer-drawdown.jsp

Pages that request this page
----------------------------
wire_transfers_list.jsp (included in wiretransfers.jsp, wire summary page)
	View button in a row containing a drawdown wire
wire_templates_list.jsp (included in wiretransfers.jsp, wire summary page)
	View button in a row containing a drawdown wire
wiresrelease.jsp
	View button in a row containing a drawdown wire

Pages included in this page
---------------------------
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
payments/inc/wire_set_wire.jsp
	Sets a wire transfer or wire batch with a given ID into the session
payments/include-view-transaction-details-constants.jsp
	does nothing other than include common/include-view-transaction-details-constants.jsp
	which sets values in the session used in displaying the view page
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
common/include-view-wiretransfer-drawdown.jsp
	A common view page for drawdown wires used by many parts of CB and BC
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:help id="payments_wiredrawdownview" className="moduleHelpClass"/>

<ffi:setProperty name="DimButton" value=""/>
<%--<ffi:include page="${PathExt}payments/inc/wire_buttons.jsp"/>--%>

<s:include value="%{#session.PagesPath}/wires/inc/include-view-transaction-details-constants.jsp"/>
        <div align="center">
            <table width="750" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="ltrow2_color">
                        <div align="center">
			<s:include value="%{#session.PagesPath}/common/include-view-wiretransfer-drawdown.jsp"/>
                        </div>
            </td>
        </tr>
    </table>
        </div>
