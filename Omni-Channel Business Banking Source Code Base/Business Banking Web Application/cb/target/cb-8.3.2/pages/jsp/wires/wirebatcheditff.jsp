<%--
This page is for editing a freeform wire batch.

Pages that request this page
----------------------------
wirebatchedit.jsp (edit wire batch)
	As an included file
wirebatchffconfirm.jsp (add/edit freeform batch confirm)
	BACK button
wiretransfernew.jsp (add/edit domestic and international wire)
wirebook.jsp (add/edit book wire)
wiredrawdown.jsp (add/edit drawdown wire)
wirefed.jsp (add/edit fed wire)
wiretransferconfirm.jsp (add/edit wire transfer confirm)
	CANCEL button
wiretransfersend.jsp (add/edit wire transfer final send/save page)
	Javascript auto-refresh
wiretransferdelete.jsp (domestic and international wire)
wirefeddelete.jsp (fed wire)
wiredrawdowndelete.jsp (drawdown wire)
wirebookdelete.jsp (book wire)
	CANCEL button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
SUBMIT WIRE BATCH requests one of the following:
	wirebatchffconfirm.jsp (for freeform batches (non-international))
	wirebatchintffconfirm.jsp (for freeform international batches)
ADD WIRE requests one of the following:
	wiretransfernew.jsp (domestic and international wire)
	wiredrawdown.jsp (drawdown wire)
	wirebook.jsp (book wire)
Edit button next to wires requests one of the following:
	wiretransfernew.jsp (domestic and international wire)
	wiredrawdown.jsp (drawdown wire)
	wirebook.jsp (book wire)
Delete button next to wires requests one of the following:
	wiretransferdelete.jsp (domestic and international wire)
	wirefeddelete.jsp (fed wire)
	wiredrawdowndelete.jsp (drawdown wire)
	wirebookdelete.jsp (book wire)
Calendar button requests calendar.jsp

Pages included in this page
---------------------------
payments/inc/wire_set_wire.jsp
	Sets a wire transfer or wire batch with a given ID into the session
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_batch_edit_fields.jsp
	edit fields for a non-international wire
payments/inc/wire_batch_edit_int_fields.jsp
	edit fields for an international wire
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireBatch,
				 com.ffusion.beans.wiretransfers.WireTransfers,
				 com.ffusion.beans.wiretransfers.WireDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatcheditff" className="moduleHelpClass"/>
<%
if( request.getParameter("DontInitializeBatch") != null ){ session.setAttribute("DontInitializeBatch", request.getParameter("DontInitializeBatch")); }
%>
<ffi:setL10NProperty name='PageHeading' value='Edit Batch Wire Transfer'/>
<ffi:setProperty name='PageText' value=''/>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wirebatcheditff.jsp?DontInitializeBatch=true" URLEncrypt="true" />
<%--<ffi:setProperty name="wireBackURL" value="${SecurePath}payments/wirebatcheditff.jsp?DontInitializeBatch=true" URLEncrypt="true" /> --%>
<ffi:setProperty name="wireBackURL" value="modifyWireBatchAction_reload.action"/>
<ffi:setProperty name="wireSaveButton" value="SAVE TO BATCH"/>
<ffi:setProperty name="wirebatch_confirm_form_url" value="modifyWireBatchAction"/>
<ffi:setProperty name="wireBatchConfirmURL" value="wirebatchffconfirm.jsp"/>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DOMESTIC}">
	<ffi:setL10NProperty name="PageHeading" value="Edit Domestic Wire Transfer Batch"/>
</s:if>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_INTERNATIONAL}">
	<ffi:setL10NProperty name="PageHeading" value="Edit International Wire Transfer Batch"/>
</s:if>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_FED}">
	<ffi:setL10NProperty name="PageHeading" value="Edit FED Wire Transfer Batch"/>
</s:if>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_DRAWDOWN}">
	<ffi:setL10NProperty name="PageHeading" value="Edit Drawdown Wire Transfer Batch"/>
</s:if>

<s:if test="%{WireBatchObject.BatchDestination == @com.ffusion.beans.wiretransfers.WireDefines@WIRE_BOOK}">
	<ffi:setL10NProperty name="PageHeading" value="Edit Book Wire Transfer Batch"/>
</s:if>


<span id="PageHeading" style="display:none;"><ffi:getProperty name='PageHeading'/></span>
<%-- 
<ffi:removeProperty name="DontInitializeBatch"/>
<ffi:removeProperty name="DontInitialize"/>
<ffi:removeProperty name="editWireTransfer"/>
<ffi:removeProperty name="WireTransfer"/>
<ffi:removeProperty name="WireTransferPayee"/>
--%>


<script>
function addWire() {
	var targetAction="/cb/pages/jsp/wires/modifyWireBatchAction_addWireTransfer.action";
	$("#frmBatchID").attr("action",targetAction); 
}
function editWire(idx) {
	frm = document.frmBatch;
	frm.wireIndex.value=idx;
	$.ajax({  
	  type: "POST",   
	  data: $("#frmBatchID").serialize(),    
	  url: "/cb/pages/jsp/wires/modifyWireTransferAction_editWireTransferFromBatch.action",     
	  success: function(data) {  	  
		 $("#inputDiv").html(data);
	  }   
	});
}
function deleteWire(idx) {
	frm = document.frmBatch;
	frm.wireIndex.value=idx;
	$.ajax({  
		  type: "POST",   
		  data: $("#frmBatchID").serialize(),    
		  url: "/cb/pages/jsp/wires/modifyWireBatchAction_deleteWireTransfer.action",     
		  success: function(data) {  	  
			 $("#deleteWireInBatchyDialogID").html(data).dialog('open');
		  }   
		});
}
</script>

<div align="center">
<%-- CONTENT START --%>
<span id="formerrors"></span>
<s:form  name="frmBatch"  id="frmBatchID" action="modifyWireBatchAction_verify" namespace="/pages/jsp/wires"  method="post" theme="simple">
<%-- <form name="frmBatch" action="<ffi:urlEncrypt url="${wireBatchConfirmURL}" />" method="post">--%>
                	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                	<input type="hidden" id="isManagePayeeModified" name="isManagePayeeModified" />
                 	<input type="hidden" name="wireIndex" value=""/>
<input type="hidden" name="DontInitializeBatch" value="true">
<input type="hidden" name="ResetBatch" value="">
<input type="hidden" name="isModifyAction" value=""/>
<ffi:setProperty name="wireBatchPage" value="nonrepetetive"/>
<div class="leftPaneWrapper" role="form" aria-labelledby="editBatchEntryHeader">
	<div class="leftPaneInnerWrapper">
		<div class="header" id=""><h2 id="editBatchEntryHeader">Edit Wire Batch Entry Details</h2></div>
		<div class="leftPaneInnerBox">
			<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="notEquals">
			<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_edit_fields.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
			<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_edit_int_fields.jsp"/>
			</ffi:cinclude>
		</div>
	</div>
</div>
<div class="confirmPageDetails">
<div class="paneWrapper">
	<div class="paneInnerWrapper">
		<table width="100%" cellpadding="3" cellspacing="0" border="0">
				<tr class="header">
					<td class="sectionsubhead" width="25%">&nbsp;</td>
					<td align="left" class="sectionsubhead" width="25%">
					<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="notEquals"><!--L10NStart-->Beneficiary Name<!--L10NEnd--></ffi:cinclude>
					<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="equals"><!--L10NStart-->Drawdown Account Name<!--L10NEnd--></ffi:cinclude>
					</td>
					<td align="left" class="sectionsubhead" width="25%" align="right"><!--L10NStart-->Amount<!--L10NEnd--></td>
					<td class="sectionsubhead" width="25%">&nbsp;</td>
				</tr>
				<% int count = 0; %>
				<ffi:setProperty name="wireBatchObject" property="Wires.SortedBy" value="WirePayee.PayeeName,Amount"/>
				<ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
				<ffi:list collection="wireBatchObject.Wires" items="wire">
					<s:if test="%{#request.wire.Action != 'del' && #request.wire.Status != 3}">
						<tr>
							<td class="columndata">&nbsp;</td>
							<td align="left"  class="columndata"><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
								<ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
							<td class="columndata" align="left">
							<ffi:cinclude value1="${wire.Action}" value2="<%= WireDefines.WIRE_ACTION_ADD %>" operator="equals">
								<ffi:getProperty name="wire" property="AmountForBPW"/>
							</ffi:cinclude>
							<ffi:cinclude value1="${wire.Action}" value2="<%= WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
								<ffi:getProperty name="wire" property="OrigAmount"/>
							</ffi:cinclude>
							</td>
							<td align="left" class="columndata">
							<ffi:cinclude value1="${wire.Action}" value2="<%= WireDefines.WIRE_ACTION_ADD %>" operator="equals">
								<a class='ui-button' title='Edit' href='#' onClick='editWire(<%= count %>)'><span class='sapUiIconCls icon-wrench'></span></a>
								<a class='ui-button' title='Delete' href='#' onClick='deleteWire(<%= count %>)'><span class='sapUiIconCls icon-delete'></span></a>
							</ffi:cinclude>
							<ffi:cinclude value1="${wire.Action}" value2="<%= WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
								<ffi:cinclude value1="${wire.CanEdit}" value2="true" operator="equals">
									<%--<a href="javascript:editWire(<%= count %>)"><img src="/cb/web/multilang/grafx/payments/i_edit.gif" width="19" height="14" alt="Edit" border="0"></a>--%>
									<a class='ui-button' title='Edit' href='#' onClick='editWire(<%= count %>)'><span class='sapUiIconCls icon-wrench'></span></a>
								</ffi:cinclude>
								<ffi:cinclude value1="${wire.CanEdit}" value2="true" operator="notEquals">
									<%--<img src="/cb/web/multilang/grafx/payments/i_edit_dim.gif" width="19" height="14" alt="Cannot Edit" border="0">--%>
									<%--<a class='ui-button' title='Cannot Edit'><span class='sapUiIconCls icon-wrench'></span></a>--%>
								</ffi:cinclude>
								<ffi:cinclude value1="${wire.CanDelete}" value2="true" operator="equals">
									<%--<a href="javascript:deleteWire(<%= count %>)"><img src="/cb/web/multilang/grafx/payments/i_trash.gif" width="14" height="14" alt="Delete" border="0"></a>--%>
									<a class='ui-button' title='Delete' href='#' onClick='deleteWire(<%= count %>)'><span class='sapUiIconCls icon-delete'></span></a>
								</ffi:cinclude>
								<ffi:cinclude value1="${wire.CanDelete}" value2="true" operator="notEquals">
									<%--<img src="/cb/web/multilang/grafx/payments/i_trash_dim.gif" width="14" height="14" alt="Cannot Delete" border="0">--%>
									<%--<a class='ui-button' title='Cannot Delete'><span class='sapUiIconCls icon-delete'></span></a>--%>
								</ffi:cinclude>
							</ffi:cinclude>
							</td>
						</tr>
					</s:if>
					<% count++; %>
				</ffi:list>
			</table>
	</div>
</div>
<div class="btn-row marginTop20">		
			<sj:a id="cancelFormButtonOnVerify" 
				button="true" 
				onClickTopics="cancelWireTransferForm"
			 >
				Cancel
			 </sj:a>
			
			 <sj:a id="verifyWireBatchID"
				formIds="frmBatchID"  
			 	targets="verifyDiv" 
			 	validate="true" 
				validateFunction="customValidation"
				button="true" 
				onBeforeTopics="verifyWireBatchBeforeTopics"
				onSuccessTopics="verifyWireBatchSuccessTopics"><s:text name="jsp.default_395" /></sj:a>
				
			 <sj:a targets="inputDiv" 
				id="addWireOnNewBatchEditID" 
				formIds="frmBatchID" 
				onClickTopics="addWireClickTopic"
				button="true" 
				onCompleteTopics="addWireOnBatchCompleteTopic"><s:text name="jsp.default_29" /></sj:a>
</div>	</div>
</s:form>
<%-- CONTENT END --%>
</div>
<ffi:removeProperty name="isManagePayeeModified"/>
<%-- session.setAttribute("FFIModifyWireBatch", session.getAttribute("wireBatchObject")); --%>	