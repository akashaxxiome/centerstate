<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines"%>

<div class="searchDashboardRenderCls">
	<div id="quicksearchWireReleaseCriteria" class="quickSearchAreaCls">
		<s:form id="releaseWireSearchFormId" name="releaseWireSearchForm" action="/pages/jsp/wires/initReleaseWireDashBoard_verify.action" validate="true" method="post" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	       <table cellpadding="0" cellspacing="5" border="0" class="tblFlex">
	           <tr>
	          	 <td>
	        		<input type="text" id="releaseWiresDateRangeBox" />
					<input value="<ffi:getProperty name='wireReleaseSearchCriteria' property='startDate' />" id="releaseWireStartDateID" name="WireReleaseSearchCriteria.StartDate" type="hidden" />
					<input value="<ffi:getProperty name='wireReleaseSearchCriteria' property='endDate' />" id="releaseWireEndDateID" name="WireReleaseSearchCriteria.EndDate" type="hidden" />
					<sj:a
						id="searchReleaseWireId"
						formIds="releaseWireSearchFormId"
						targets="resultmessage"
						button="true" 
						validate="true"
						validateFunction="customValidation"
						cssStyle="margin-left:10px;"
						onCompleteTopics="releseWireSearchComplete">
						<s:text name="jsp.default_6" />
					</sj:a>
					
					<%-- <sj:a
						id="dummySearchButton"
						button="true"
						onclick="$('#searchReleaseWireId').click()"
						><s:text name="jsp.default_5" />
					</sj:a> --%>
				 </td>
	           </tr>
	       </table>
      	 		
   		</s:form>
	</div>
</div>   

<script>
$(function(){
	var aConfig = {
			startDateBox:$("#releaseWireStartDateID"),
			endDateBox:$("#releaseWireEndDateID")
		};
	$("#releaseWiresDateRangeBox").extdaterangepicker(aConfig);
});
</script>