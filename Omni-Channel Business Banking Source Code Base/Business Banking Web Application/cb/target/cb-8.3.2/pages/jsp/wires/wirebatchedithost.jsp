<%--
This page is for editing a host wire batch.

Pages that request this page
----------------------------
wirebatchedit.jsp (edit wire batch)
	As an included file
wirebatchhostconfirm.jsp (add/edit host wire batch confirm)
	BACK button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
SUBMIT WIRE BATCH requests wirebatchhostconfirm.jsp
ADD WIRE requests wirebatchedithost.jsp (itself)
Delete button next to wires requests wirebatchedithost.jsp (itself)
Calendar buttons request calendar.jsp

Pages included in this page
---------------------------
payments/inc/wire_set_wire.jsp
	Sets a wire transfer or wire batch with a given ID into the session
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_batch_edit_fields.jsp
	edit fields for a non-international wire
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireBatch,
				 com.ffusion.beans.wiretransfers.WireTransfers"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatchedithost" className="moduleHelpClass"/>
<%
if( request.getParameter("deleteWireFromBatch") != null ){ session.setAttribute("deleteWireFromBatch", request.getParameter("deleteWireFromBatch")); }
if( request.getParameter("DontInitializeBatch") != null ){ session.setAttribute("DontInitializeBatch", request.getParameter("DontInitializeBatch")); }
%>
<ffi:setL10NProperty name='PageHeading' value='Edit Batch Wire Transfer'/>
<span id="PageHeading" style="display:none;"><ffi:getProperty name='PageHeading'/></span>
<ffi:setProperty name='PageText' value=''/>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wirebatchedithost.jsp?DontInitialize=true" URLEncrypt="true"/>
<ffi:setProperty name="wireBackURL" value="wirebatchedithost.jsp?DontInitializeBatch=true" URLEncrypt="true"/>
<ffi:setL10NProperty name="wireSaveButton" value="SAVE TO BATCH"/>
<ffi:setProperty name="wirebatch_confirm_form_url" value="modifyHostWireBatchAction"/>


<script>

function check(state) {
	firstBox = 3;      //Form element number of the first checkbox
	numBoxes = 20;     //Number of checkboxes per page
	numElemPerRow = 3; //Number of form elements per row (including the checkbox)
	frm = document.frmBatch;
	for (i = 0; i < numBoxes; i++) {
		frm.elements[(i * numElemPerRow) + firstBox].checked = (state == "true" ? true : false);
	}
}

function addWire() {
	$.ajax({   
		  type: "POST",   
		  data: $("#frmBatchID").serialize(),   
		  url: "/cb/pages/jsp/wires/modifyHostWireBatchAction_addHostWire.action",
		  success: function(data) {
			$("#inputDiv").html(data);
		  }   
		});   
}
function delWire(countStr) {
	frm = document.frmBatch;
	frm.wireIndex.value=countStr;
	$.ajax({ 
		  type: "POST",   
		  data: $("#frmBatchID").serialize(),     
		  url: "/cb/pages/jsp/wires/modifyHostWireBatchAction_deleteHostWire.action",
		  success: function(data) {
			$("#inputDiv").html(data);
		  }   
		}); 
}
</script>

<div align="center">
<%-- CONTENT START --%>
<span id="formerrors"></span>
<s:form  name="frmBatch"  id="frmBatchID" action="modifyHostWireBatchAction_verify" namespace="/pages/jsp/wires"  method="post" theme="simple">
<%--<form name="frmBatch" action="wirebatchhostconfirm.jsp" method="post">--%>
                 	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
          <input type="hidden" name="wireIndex" value=""/>
<input type="hidden" name="DontInitializeBatch" value="true">
<input type="hidden" name="ResetBatch" value="">
<ffi:setProperty name="wireBatchPage" value="nonrepetetive"/>
<div class="leftPaneWrapper" role="form" aria-labelledby="editBatchEntryHeader">
	<div class="leftPaneInnerWrapper">
		<div class="header" id=""><h2 id="editBatchEntryHeader">Edit Wire Batch Entry Details</h2></div>
		<div class="leftPaneInnerBox">
			<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_edit_fields.jsp"/>
		</div>
	</div>
</div>
<div class="confirmPageDetails">
<div class="paneWrapper">
	<div class="paneInnerWrapper">
			<table width="100%" cellpadding="3" cellspacing="0" border="0" class="tableData">
				<tr class="header">
					<td class="sectionsubhead" width="25%">&nbsp;</td>
					<td class="sectionsubhead" width="25%"><!--L10NStart-->Host ID<!--L10NEnd--></td>
					<td class="sectionsubhead" width="25%" align="center"><!--L10NStart-->Amount<!--L10NEnd--></td>
					<td class="sectionsubhead" width="25%" align="center">Action</td>
				</tr>
				<% int count = 0;
				String countStr = ""; %>
				<ffi:setProperty name="wireBatchObject" property="Wires.SortedBy" value="ID,Amount"/>
				<ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
				<ffi:list collection="wireBatchObject.Wires" items="wire">
					<s:if test="%{#request.wire.Action != 'del' && #request.wire.Status != 3}">
						<% countStr = "" + count; %>
						<tr>
							<ffi:setProperty name="ModifyWireBatch" property="CurrentWireIndex" value="<%= countStr %>"/>
							<td class="columndata">&nbsp;</td>
							<ffi:cinclude value1="${wire.HostID}" value2="" operator="notEquals">
							<td class="columndata"><ffi:getProperty name="wire" property="HostID"/></td>
							</ffi:cinclude>
							<ffi:cinclude value1="${wire.HostID}" value2="" operator="equals">
							<td class="columndata">

								<input type="Text" name="WireHostID_<%= countStr %>" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="wire" property="HostID"/>" size="40" maxlength="32">
							</td>
							</ffi:cinclude>
							<ffi:cinclude value1="${wire.OrigAmount}" value2="" operator="notEquals">
							<td class="columndata" align="center">
								<input type="Text" name="WireAmount_<%= countStr %>" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="wire" property="OrigAmount"/>" size="40">
							</td>
							</ffi:cinclude>
							<ffi:cinclude value1="${wire.OrigAmount}" value2="" operator="equals">
							<td class="columndata" align="center">
								<input type="Text" name="WireAmount_<%= countStr %>" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="wire" property="AmountValue.AmountValue"/>" size="40">
							</td>
							</ffi:cinclude>
							<td class="columndata" align="center">
							<ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="equals">
								<%-- <a href="#" onclick="delWire(<%=countStr%>); return false;"><img src="/cb/web/multilang/grafx/payments/i_trash.gif" width="14" height="14" alt="Delete" border="0"></a>--%>
								<a class='ui-button' title='Delete' href='#' onClick='delWire(<%=countStr%>); return false;'><span class='sapUiIconCls icon-delete'></span></a>
							</ffi:cinclude>
							<ffi:cinclude value1="${wire.Action}" value2="<%= com.ffusion.beans.wiretransfers.WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
								<ffi:cinclude value1="${wire.CanDelete}" value2="true" operator="equals">
									<%-- <a href="#" onclick="delWire(<%=countStr%>); return false;"><img src="/cb/web/multilang/grafx/payments/i_trash.gif" width="14" height="14" alt="Delete" border="0"></a>--%>
									<a class='ui-button' title='Delete' href='#' onClick='delWire(<%=countStr%>); return false;'><span class='sapUiIconCls icon-delete'></span></a>
								</ffi:cinclude>
								<ffi:cinclude value1="${wire.CanDelete}" value2="true" operator="notEquals">
									<%-- <img src="/cb/web/multilang/grafx/payments/i_trash_dim.gif" width="14" height="14" alt="Cannot Delete" border="0">--%>
									<a class='ui-button' title='Cannot Delete'><span class='sapUiIconCls icon-delete'></span></a>
								</ffi:cinclude>
							</ffi:cinclude>
							</td>
						</tr>
					</s:if>
					<% count++; %>
				</ffi:list>
			</table>
	</div>
</div>
<div class="btn-row">
<sj:a id="cancelFormButtonOnVerify" 
	button="true" summaryDivId="summary" buttonType="cancel"
	onClickTopics="showSummary,cancelWireTransferForm"
 ><s:text name="jsp.default_82" />
</sj:a>

<sj:a id="verifyWireBatchID"
formIds="frmBatchID"  
	targets="verifyDiv"
	validate="true" 
validateFunction="customValidation" 
button="true" 
onBeforeTopics="verifyWireBatchBeforeTopics"
onSuccessTopics="verifyWireBatchSuccessTopics"><s:text name="jsp.default_395" /></sj:a>

<sj:a onclick="addWire()" button="true" ><s:text name="jsp.add.label" /></sj:a>
</div></div>
</s:form>
<%-- CONTENT END --%>
</div>

