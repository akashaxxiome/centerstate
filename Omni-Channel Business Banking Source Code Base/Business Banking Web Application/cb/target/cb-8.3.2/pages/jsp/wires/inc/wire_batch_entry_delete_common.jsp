<%--
This file contains common initialization code used on all delete wire from batch
confirmation pages

It is included on all delete wire confirmation pages (except Host)
	wirebookdelete.jsp
	wiredrawdowndelete.jsp
	wirefeddelete.jsp
	wiretransferdelete.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="wireDeleteURL" value="wirebatchentrydeletesend.jsp"/>
<ffi:setProperty name="wireDeleteButtonName" value="DELETE TRANSFER FROM BATCH"/>

<ffi:cinclude value1="${wireBatch}" value2="ModifyWireBatch" operator="notEquals">
	<ffi:setProperty name="AddWireBatch" property="WireFreeFormFlag" value="true"/>
	<ffi:setProperty name="AddWireBatch" property="WireParameter" value="${wireIndex}"/>
	<ffi:setProperty name="AddWireBatch" property="WireSessionName" value="WireTransfer"/>
	<ffi:process name="AddWireBatch"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireBatch}" value2="ModifyWireBatch" operator="equals">
	<ffi:object id="SetWireTransferFromBatch" name="com.ffusion.tasks.wiretransfers.SetWireTransferFromBatch"/>
	<ffi:setProperty name="SetWireTransferFromBatch" property="Index" value="${wireIndex}"/>
	<ffi:setProperty name="SetWireTransferFromBatch" property="CollectionSessionName" value="ModifyWireBatch"/>
	<ffi:setProperty name="SetWireTransferFromBatch" property="BeanSessionName" value="WireTransfer"/>
	<ffi:process name="SetWireTransferFromBatch"/>
	<ffi:setProperty name="ModifyWireBatch" property="DeleteWireFromBatch" value="${wireIndex}"/>	
</ffi:cinclude>
