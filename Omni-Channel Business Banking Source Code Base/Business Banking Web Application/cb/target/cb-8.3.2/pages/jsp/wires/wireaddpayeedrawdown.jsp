<%--
This page is used to add or edit a DRAWDOWN wire transfer beneficiary. REGULAR
beneficiaries use wireaddpayee.jsp and BOOK beneficiaries use
wireaddpayeebook.jsp

Pages that request this page
----------------------------
wirepayee.jsp (wire beneficiary summary page)
	ADD BENEFICIARY button, or by clicking the edit icon next to a DRAWDOWN
	beneficiary.
wiredrawdown.jsp (add/edit drawdown wire)
	MANAGE BENEFICIARY button

Pages this page requests
------------------------
CANCEL requests one of the following:
	wirepayee.jsp
	wiretransfernew.jsp
SAVE BENEFICIARY requests wirepayeeaddconfirm.jsp

Pages included in this page
---------------------------
common/wire_labels.jsp
	The labels for fields and buttons
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
payments/inc/wire_common_payee_init.jsp
	Initialization common to all add/edit wire beneficiary pages
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/wire_scripts_payee_js.jsp
	Javascript functions common to all add/edit wire beneficiary pages
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_addedit_payee_fields.jsp
	The beneficiary name and address fields - shared by all add/edit wire
	transfer and all add/edit wire beneficiary pages.
payments/inc/wire_addedit_bank_fields.jsp
	The beneficiary bank name and address fields and SEARCH button - shared by
	all add/edit wire transfer and all add/edit wire beneficiary pages.
payments/inc/wire_addedit_intermediary_banks.jsp
	The intermediary bank list and ADD INTERMEDIARY BANK button - shared by all
	add/edit wire transfer and all add/edit wire beneficiary pages.
payments/inc/wire_addedit_payee_comment.jsp
	The beneficiary comment field - shared by all add/edit wire beneficiary
	pages. (Not used by add/edit wire transfers.)
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<head>
<script>
function spanValidation(){
	var beneficiary_type = $("#selectPayeeDestinationID option:selected")[0].value;

			if(beneficiary_type === 'DRAWDOWN'){
				if ($('span[customId=destinationBankRoutingFedWireError]').length > 0 && $('span[customId=destinationBankRoutingFedWireError]').html()==""){
					$('#columnId').attr('colspan','' +'2');
				}
			}
			if(beneficiary_type === 'DRAWDOWN'){
				if($('span[customId=wireTransferBankInvalidRoutingFedWireError]').length > 0 && $('span[customId=wireTransferBankInvalidRoutingFedWireError]').html()!=""){
					$('#columnId').attr('colspan','' +'1');
				}
			}
}
</script>
</head>

<ffi:help id="payments_wireaddpayeedrawdown" className="moduleHelpClass"/>
<ffi:removeProperty name="isBeneficiary"/>
<ffi:removeProperty name="IsPending"/>
<% if (request.getParameter("Initialize") != null) { %>
<ffi:removeProperty name="benficiaryType"/>
<%}%>
<%
String validation_action ="";
%>

 <ffi:getProperty name="validateAction" assignTo="validation_action"/>

<ffi:setL10NProperty name="PageHeading" value="Add Drawdown Wire Account"/>
<ffi:cinclude value1="${validateAction}" value2="addWireBeneficiaryAction_verify" operator="notEquals">
	<ffi:setL10NProperty name="PageHeading" value="Edit Drawdown Wire Account"/>
</ffi:cinclude>

<span id="PageHeading" style="display:none;"><ffi:getProperty name="PageHeading"/></span>

<ffi:setProperty name="DimButton" value="beneficiary"/>
<%-- <ffi:include page="${PathExt}payments/inc/wire_buttons.jsp"/>--%>

<ffi:setProperty name="BackURL" value="${SecurePath}wires/wireaddpayeedrawdown.jsp" />
<ffi:setProperty name="CallerForm" value="WireNew"/>
<ffi:setProperty name="CallerURL" value="wireaddpayeedrawdown.jsp"/>

<%--

<% if (request.getParameter("Initialize") != null) { %>
	<s:include value="%{#session.PagesPath}/wires/inc/wire_common_payee_init.jsp"/>
<% } else { %>
	<%-- Check the destinations bank's country.  If it doesn't match the preferred country, clear it.
	<% String bankCountry = ""; %>
	<ffi:getProperty name="${payeeTask}" property="DestinationBank.Country" assignTo="bankCountry"/>
	<% if (bankCountry == null) bankCountry = ""; %>

	<ffi:cinclude value1="<%= bankCountry %>" value2="<%= com.ffusion.banklookup.beans.FinancialInstitution.PREFERRED_COUNTRY %>" operator="notEquals">
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<ffi:setProperty name="${payeeTask}" property="ClearDestinationBank" value="true"/>
		</ffi:cinclude>
		<ffi:process name="${payeeTask}"/>
	</ffi:cinclude>
<% } %>

 --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_scripts_payee_js.jsp"/>

			<div align="center">
			<ul id="formerrors"></ul>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td></td>
					<td class="ltrow2_color" align="left">
						<s:form name="WireNew" id="newBeneficiaryFormID" namespace="/pages/jsp/wires"  validate="false"  
								action="%{#attr.validation_action}" method="post" theme="simple">
						<%-- <form action="wirepayeeaddconfirm.jsp" method="post" name="WireNew">--%>
						<input type="hidden" name="delIntBank" value=""/>
						<input type="hidden" name="selectedCountry" value=""/>
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
                    	<input type="hidden" name="validateAction" value="<ffi:getProperty name='validateAction'/>"/>
						<input type="hidden" name="<ffi:getProperty name="payeeTask"/>.Country" value="UNITED STATES">
						<input type="hidden" name="<ffi:getProperty name="payeeTask"/>.DestinationBank.Country" value="UNITED STATES">
						<input type="hidden" name ="returnPage" id="returnPage" value="<ffi:getProperty name='returnPage'/>"/>
						<input type="hidden" id="manageBeneficiary" name="manageBeneficiary" value="<ffi:getProperty name='manageBeneficiary'/>"/>
						<s:if test="%{validateAction == 'addWireBeneficiaryAction_verify'}">
                    		<input type="hidden" name ="bankLookupRaturnPage" id="bankLookupRaturnPage" value="addWireBeneficiaryAction_preProcess.action"/>
                    	</s:if>
                    	<s:else>
                    		<input type="hidden" name ="bankLookupRaturnPage" id="bankLookupRaturnPage" value="modifyWirebeneficiary_preProcess.action"/>
                    	</s:else>
						
								<table cellpadding="0" cellspacing="0" border="0">
						<input type="hidden" name="<ffi:getProperty name="payeeTask"/>.ID" value="<ffi:getProperty name="${payeeTask}" property="ID"/>"/>
						
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td valign="top">
<%-- ------ PAYEE FIELDS: PayeeName, Street, Street2, City, State, ZipCode, Country, ContactName, PayeeScope, AccountNum, AccountType ------ --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_fields.jsp"/>
										</td>
									</tr>
									<tr><td valign="top"><hr class="thingrayline lineBreak" /></td></tr>
									<tr>
										<td valign="top">
<%-- ------ BENEFICIARY BANK FIELDS:  BankName, Street, Street2, City, State, ZipCode, Country, RoutingFedWire, RoutingSwift, RoutingChips, RoutingOther ------ --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_bank_fields.jsp"/>
										</td>
									</tr>
								</table>
								<span class="columndata">&nbsp;<br></span>
<%-- ------ PAYEE COMMENTS ------ --%>
<s:include value="%{#session.PagesPath}/wires/inc/wire_addedit_payee_comment.jsp"/>
								<span class="columndata">&nbsp;<br>&nbsp;<br></span>
								<%-- 
								<input type="button" class="submitbutton" value="CANCEL" onclick="document.location='<ffi:getProperty name="returnPage"/>'">
								<input type="button" class="submitbutton" value="SAVE DRAWDOWN ACCOUNT" onclick="document.WireNew.submit();">
								--%>
								<input type="hidden" id="returnPageIDForJS" value="<ffi:getProperty name='returnPage'/>"/>
                    			<s:hidden name="isBeneficiary" value="true" id="isBeneficiary"/>
								<s:hidden name="IsPending" value="%{#session.IsPending}" id="IsPending"/>
                    			<%--if isHaveReturnPageParamIDForJS is not null,
                    				means this page from click manage beneficiary button from wire or template --%>
								<input type="hidden" id="isHaveReturnPageParamIDForJS" value="<ffi:getProperty name='returnPage'/>"/>
								<div class="submitButtonsDiv">
								<sj:a id="cancelFormButtonOnInput" 
									button="true" summaryDivId="summary" buttonType="cancel" 
									onClickTopics="showSummary"
									onClick="ns.wire.cancelWireBeneficiaryFormOnClick();"
								><s:text name="jsp.default_82" />
								</sj:a>
								<sj:a 
									id="verifyBeneficiarySubmit"
									formIds="newBeneficiaryFormID"
	                                targets="verifyDiv" 
	                                button="true" 
	                                validate="true" 
	                                validateFunction="customValidation,spanValidation();"
	                                onBeforeTopics="beforeVerifyBeneficiaryForm"
	                                onCompleteTopics="verifyBeneficiaryFormComplete"
									onErrorTopics="errorVerifyBeneficiaryForm"
	                                onSuccessTopics="successVerifyBeneficiaryForm"
			                        ><s:text name="jsp.default_395" />
						            </sj:a>
								</div>
							<%--</form> --%>
							</s:form>	
					</td>
					<td align="right"></td>
				</tr>
			</table>
				<br>
			</div>




