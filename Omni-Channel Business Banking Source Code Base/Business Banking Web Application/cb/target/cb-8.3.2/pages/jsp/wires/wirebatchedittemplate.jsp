<%--
This page is for editing a template wire batch.

Pages that request this page
----------------------------
wirebatchedit.jsp (edit wire batch)
	As an included file
wirebatcheditaddtemplatesend.jsp
wirebatchtempconfirm.jsp (add/edit template batch confirm)
	BACK button
wiretransfernew.jsp (add/edit domestic and international wire)
wirebook.jsp (add/edit book wire)
wiredrawdown.jsp (add/edit drawdown wire)
wirefed.jsp (add/edit fed wire)
wiretransferconfirm.jsp (add/edit wire transfer confirm)
	CANCEL button
wiretransfersend.jsp (add/edit wire transfer final send/save page)
	Javascript auto-refresh
wiretransferdelete.jsp (domestic and international wire)
wirefeddelete.jsp (fed wire)
wiredrawdowndelete.jsp (drawdown wire)
wirebookdelete.jsp (book wire)
	CANCEL button

Pages this page requests
------------------------
CANCEL requests wiretransfers.jsp
SUBMIT WIRE BATCH requests one of the following:
	wirebatchffconfirm.jsp (for freeform batches (non-international))
	wirebatchintffconfirm.jsp (for freeform international batches)
ADD WIRE requests wirebatcheditaddtemplate.jsp
Edit button next to wires requests one of the following:
	wiretransfernew.jsp (domestic and international wire)
	wirefed.jsp (fed wire)
	wiredrawdown.jsp (drawdown wire)
	wirebook.jsp (book wire)
Delete button next to wires requests one of the following:
	wiretransferdelete.jsp (domestic and international wire)
	wirefeddelete.jsp (fed wire)
	wiredrawdowndelete.jsp (drawdown wire)
	wirebookdelete.jsp (book wire)
Calendar button requests calendar.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
payments/inc/wire_batch_edit_fields.jsp
	edit fields for a non-international wire
payments/inc/wire_batch_edit_int_fields.jsp
	edit fields for an international wire
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireBatch,
				 com.ffusion.beans.wiretransfers.WireTransfers,
				 com.ffusion.beans.wiretransfers.WireDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatchedittemplate" className="moduleHelpClass"/>
<% 
if( request.getParameter("DontInitializeBatch") != null ){ session.setAttribute("DontInitializeBatch", request.getParameter("DontInitializeBatch")); }
if( request.getParameter("wireBatch") != null ){ session.setAttribute("wireBatch", request.getParameter("wireBatch")); }
if( request.getParameter("SetActionFlag") != null ){ session.setAttribute("SetActionFlag", request.getParameter("SetActionFlag")); }
if( request.getParameter("wireIndex") != null ){ session.setAttribute("wireIndex", request.getParameter("wireIndex")); }
%>

<ffi:setL10NProperty name='PageHeading' value='Edit Batch Wire Transfer'/>
<ffi:setProperty name='PageText' value=''/>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wirebatchedittemplate.jsp?DontInitializeBatch=true" URLEncrypt="true" />
<%-- <ffi:setProperty name="wireBackURL" value="${SecurePath}payments/wirebatchedittemplate.jsp?DontInitializeBatch=true" URLEncrypt="true" />--%>
<ffi:setProperty name="wireBackURL" value="modifyWireBatchTemplateAction_reload.action"/>
<ffi:setProperty name="wireSaveButton" value="SAVE TO BATCH"/>
<ffi:setProperty name="wirebatch_confirm_form_url" value="modifyWireBatchTemplateAction"/>

<ffi:cinclude value1="${DontInitializeBatch}" value2="true" operator="notEquals">
	
	
</ffi:cinclude>

<ffi:setProperty name="wireBatchConfirmURL" value="wirebatchffconfirm.jsp"/>
<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_DOMESTIC %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wiretransfernew.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wiretransferdelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="Edit Domestic Wire Transfer Batch"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wiretransfernew.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wiretransferdelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="Edit International Wire Transfer Batch"/>
	<ffi:setProperty name="wireBatchConfirmURL" value="wirebatchintffconfirm.jsp"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_FED %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wirefed.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wirefeddelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="Edit FED Wire Transfer Batch"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wiredrawdown.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wiredrawdowndelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="Edit Drawdown Wire Transfer Batch"/>
</ffi:cinclude>
<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_BOOK %>" operator="equals">
	<ffi:setProperty name="wireBatchAddTransferURL" value="wirebook.jsp"/>
	<ffi:setProperty name="wireBatchDeleteTransferURL" value="wirebookdelete.jsp"/>
	<ffi:setL10NProperty name="PageHeading" value="Edit Book Wire Transfer Batch"/>
</ffi:cinclude>

<span id="PageHeading" style="display:none;"><ffi:getProperty name='PageHeading'/></span>

<ffi:removeProperty name="DontInitializeBatch"/>
<ffi:removeProperty name="DontInitialize"/>
<ffi:removeProperty name="editWireTransfer"/>
<ffi:removeProperty name="WireTransfer"/>
<ffi:removeProperty name="WireTransferPayee"/>

<script>
function validateForm() {
	frm = document.frmBatch;

	return true;
}
function popCalendar(calTarget) {
		window.open("<ffi:getProperty name='SecurePath'/>calendar.jsp?calDirection=back&calForm=frmBatch&calTarget=" + calTarget,"","width=350,height=300,resizable=false,scrollbars=false,menubar=false,toolbar=false,directories=false,location=false,status=false");
}
<%--
function addWireTempalteInBacth() {
	$.ajax({    
	  url: "/cb/pages/jsp/wires/wirebatcheditaddtemplate.jsp",
	  success: function(data) {
		 $('#wireBatchEditAddTemplateDialogID').html(data).dialog('open');
	  }   
	});   
}	
--%>

<%-- URL Encrption Edit By Dongning --%>
function addWireTempalteInBacth() {
	ns.wire.wireBatchTemplateAddWireURL = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/modifyWireBatchTemplateAction_initAddWireTemplate.action"/>' ;
	$.ajax({    
	  url: ns.wire.wireBatchTemplateAddWireURL,
	  success: function(data) {
		 //$('#wireBatchEditAddTemplateDialogID').html(data).dialog('open');
		  $('#wireBatchEditAddTemplateWrapper').html(data);
	  }   
	});   
}

$("#addNewTemplate").trigger('click');
function editWire(idx) {
	frm = document.frmBatch;
	frm.wireIndex.value=idx;
	$.ajax({  
		  type: "POST",  
		  url: "/cb/pages/jsp/wires/modifyWireTransferAction_editWireTransferTemplateFromBatch.action",
		  data: $("#frmBatchID").serialize(), 
		  success: function(data) {
			$("#inputDiv").html(data);
		  }   
		}); 
}
function deleteWireTemplateInBacth(idx) {
	frm = document.frmBatch;
	frm.wireIndex.value=idx;
	$.ajax({  
		  type: "POST",  
		  url: "/cb/pages/jsp/wires/modifyWireBatchTemplateAction_deleteWireTemplate.action",
		  data: $("#frmBatchID").serialize(), 
		  success: function(data) {
			$("#deleteWireInBatchyDialogID").html(data).dialog('open');
		  }   
		}); 
}

<%-- This method returns true if there is a currency mismatch at the specified column --%>
function  isCurrencyDoNotMatch(rowID)
{
    frm = document.frmBatch;

    if ( frm["ModifyWireBatch.OrigCurrency"] == null ) {
            return false;
    }
    if ( frm["ModifyWireBatch.PayeeCurrencyType"] == null ) {
            return false;
    }
    batchAmtCurrency = frm["ModifyWireBatch.OrigCurrency"].value;
    batchPmtCurrency = frm["ModifyWireBatch.PayeeCurrencyType"].value;

    amtCurrency = frm["amtCurrency_" + rowID].value;
    pmtCurrency = frm["pmtCurrency_" + rowID].value;

    if ( batchAmtCurrency != null && amtCurrency != null ) {
        if (batchAmtCurrency != amtCurrency )
            return true;
    }
    if ( batchPmtCurrency != null && pmtCurrency != null ) {
        if (batchPmtCurrency != pmtCurrency )
            return true;
    }
    return false;
}

<%-- This method refreshes the currency mismatch indicator for all elements in the batch --%>
function  refreshCurrencyMismatches()
{
    frm = document.frmBatch;
    mismatch_indicator_prefix = "showMismatch_"; // static portion of mismatch indicator
    mismatch_prefix = "currencyMismatch_" //static portion of the hidden field storing mismatch value
    count=0;
    star = document.getElementById(mismatch_indicator_prefix + count);
    while (star != null)
    {
        if (isCurrencyDoNotMatch(count)) {
            frm[mismatch_prefix + count].value = "true";
            star.style.display="inline";
            msg = document.getElementById("intlMessage");
            msg.style.display="inline";
        }
        else {
            frm[mismatch_prefix + count].value = "false";
            star.style.display="none"; }

        count++;
        star = document.getElementById(mismatch_indicator_prefix + count);
    }
}

<%-- This method submits the form after validation --%>
function  submitForm(dest)
{
    if (dest != null && dest == "<%= WireDefines.WIRE_INTERNATIONAL %>"){
       <ffi:setProperty name="wireBatchConfirmURL" value="wirebatchinttempconfirm.jsp"/>
    }
    if ( validateForm() == true ){
    	 <ffi:setProperty name="wireBatchConfirmURL" value="wirebatchtempconfirm.jsp"/>
    }
}

refreshCurrencyMismatches();
</script>

<div align="center">

<%-- CONTENT START --%>
<span id="formerrors"></span>
<s:form  name="frmBatch"  id="frmBatchID" action="modifyWireBatchTemplateAction_verify" namespace="/pages/jsp/wires"  method="post" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" id="isManagePayeeModified" name="isManagePayeeModified" />
<input type="hidden" name="wireIndex" value=""/>
<input type="hidden" name="DontInitializeBatch" value="true">
<input type="hidden" name="ResetBatch" value="">
<ffi:setProperty name="wireBatchPage" value="nonrepetetive"/>
<div class="leftPaneWrapper" role="form" aria-labelledby="batchEntryHeader">
	<div class="leftPaneInnerWrapper">
		<div class="header" id=""><h2 id="batchEntryHeader">Edit Wire Batch Entry Details</h2></div>
		<div class="leftPaneInnerBox">
			<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="notEquals">
			<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_edit_fields.jsp"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
			<s:include value="%{#session.PagesPath}/wires/inc/wire_batch_edit_int_fields.jsp"/>
			</ffi:cinclude>
		</div>
	</div>
</div>	
<div class="confirmPageDetails">
	<div class="paneWrapper">
		<div class="paneInnerWrapper">
			<table width="100%" cellpadding="3" cellspacing="0" border="0">
					<tr class="header">
						<td class="sectionsubhead" width="18%">Template Name</td>
	                    <td class="sectionsubhead" width="1%">&nbsp;</td>
						<td class="sectionsubhead" width="32%" align="center">
						<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="notEquals"><!--L10NStart-->Beneficiary Name<!--L10NEnd--></ffi:cinclude>
						<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="equals"><!--L10NStart-->Drawdown Account Name<!--L10NEnd--></ffi:cinclude>
						 </td>
						<td class="sectionsubhead" width="25%" align="right"><!--L10NStart-->Amount<!--L10NEnd--></td>
						<td class="sectionsubhead" width="25%" align="center">Action</td>
					</tr>
					<% int count = 0; %>
					<ffi:setProperty name="wireBatchObject" property="Wires.SortedBy" value="WirePayee.PayeeName,Amount"/>
					<ffi:setProperty name="wireBatchObject" property="Wires.Filter" value="<%=com.ffusion.util.Filterable.ALL_FILTER%>" />
					<ffi:list collection="wireBatchObject.Wires" items="wire">
						<s:if test="%{#request.wire.Action != 'del' && #request.wire.Status != 3}">
							<tr>
								<td class="columndata">&nbsp;</td>
								<td class="columndata">
									<div id="showMismatch_<%= count %>"  style="display:none">#</div>
								</td>
								<td style="display:none">
									<% String mismatchField = "currencyMismatch_" + count;
									   String mismatchValue = "";
									%>
									<ffi:getProperty name="<%= mismatchField %>" assignTo="mismatchValue"/>
									<% if (mismatchValue == null) mismatchValue = "false"; %>
									<input type="hidden" name="currencyMismatch_<%= count %>" value="<ffi:getProperty name='<%= mismatchField %>'/>" />
									<input type="hidden" name="pmtCurrency_<%= count %>" value="<ffi:getProperty name='wire' property='PayeeCurrencyType'/>"/>
									<input type="hidden" name="amtCurrency_<%= count %>" value="<ffi:getProperty name='wire' property='AmtCurrencyType'/>"/>
								</td>
								<td class="columndata" align="center"><ffi:getProperty name="wire" property="WirePayee.PayeeName"/>
									<ffi:cinclude value1="${wire.WirePayee.NickName}" value2="" operator="notEquals">(<ffi:getProperty name="wire" property="WirePayee.NickName"/>)</ffi:cinclude></td>
								<td class="columndata" align="right">
								<ffi:cinclude value1="${wire.Action}" value2="<%= WireDefines.WIRE_ACTION_ADD %>" operator="equals">
									<ffi:getProperty name="wire" property="AmountForBPW"/>
								</ffi:cinclude>
								<ffi:cinclude value1="${wire.Action}" value2="<%= WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
									<ffi:getProperty name="wire" property="OrigAmount"/>
								</ffi:cinclude>
								</td>
								<td class="columndata" align="center">
								<ffi:cinclude value1="${wire.Action}" value2="<%= WireDefines.WIRE_ACTION_ADD %>" operator="equals">
									<a class='ui-button' title='Edit' href='#' onClick='editWire(<%= count %>)'><span class='sapUiIconCls icon-wrench'></span></a>
									<a class='ui-button' title='Delete' href='#' onClick='deleteWireTemplateInBacth(<%= count %>)'><span class='sapUiIconCls icon-delete'></span></a>
								</ffi:cinclude>
								<ffi:cinclude value1="${wire.Action}" value2="<%= WireDefines.WIRE_ACTION_ADD %>" operator="notEquals">
									<ffi:cinclude value1="${wire.CanEdit}" value2="true" operator="equals">
										<a class='ui-button' title='Edit' href='#' onClick='editWire(<%= count %>)'><span class='sapUiIconCls icon-wrench'></span></a>
									</ffi:cinclude>
									<ffi:cinclude value1="${wire.CanEdit}" value2="true" operator="notEquals">
										<%--<a class='ui-button' title='Cannot Edit'><span class='ui-icon ui-icon-wrench'></span></a>--%>
									</ffi:cinclude>
									<ffi:cinclude value1="${wire.CanDelete}" value2="true" operator="equals">
										<a class='ui-button' title='Delete' href='#' onClick='deleteWireTemplateInBacth(<%= count %>)'><span class='sapUiIconCls icon-delete'></span></a>
									</ffi:cinclude>
									<ffi:cinclude value1="${wire.CanDelete}" value2="true" operator="notEquals">
										<%--<a class='ui-button' title='Cannot Delete'><span class='ui-icon ui-icon-trash'></span></a>--%>
									</ffi:cinclude>
								</ffi:cinclude>
								</td>
							</tr>
						</s:if>
						<% count++; %>
					</ffi:list>
					
					<tr>
						<td colspan="5" id="wireBatchEditAddTemplateWrapper"></td>
					</tr>
					<tr>
						<td colspan="5" align="right"><sj:a cssStyle="display:none" onclick="addWireTempalteInBacth()" button="true" id="addNewTemplate"><span class="sapUiIconCls icon-add"></span><s:text name="jsp.default_29" /></sj:a></td>
					</tr>
				</table>
		</div>
	</div>
	<div class="btn-row marginTop20">
	            <sj:a id="cancelFormButtonOnVerify" 
					button="true" summaryDivId="summary" buttonType="cancel"
					onClickTopics="showSummary,cancelWireTransferForm"
				 ><s:text name="jsp.default_82" />
				 </sj:a>
	
				 <sj:a id="verifyWireBatchID"
					formIds="frmBatchID"  
				 	targets="verifyDiv" 
				 	validate="true" 
					validateFunction="customValidation"
					button="true" 
					onclick="submitForm('%{#session.ModifyWireBatch.BatchDestination}')"
					onBeforeTopics="verifyWireBatchBeforeTopics"
					onSuccessTopics="verifyWireBatchSuccessTopics"><s:text name="jsp.default_395" /></sj:a>
					
				 
	</div>				
<ffi:cinclude value1="${wireBatchObject.BatchDestination}" value2="<%= WireDefines.WIRE_INTERNATIONAL %>" operator="equals">
 <table border="0" cellspacing="0" cellpadding="3" width="98%" class="marginTop20">
     <tr>
         <td class="columndata">
             <div id="intlMessage" style="display:none"><!--L10NStart--># indicates the template's debit/payment currency don't match the batch's debit/payment currencies. Upon submission, the batch currencies will override the template's currencies.<!--L10NEnd--></div>
         </td>
     </tr>
 </table>
</ffi:cinclude>
</div>			</s:form>
<%-- CONTENT END --%>
</div>

<%-- session.setAttribute("FFIModifyWireBatch", session.getAttribute("ModifyWireBatch")); --%>	