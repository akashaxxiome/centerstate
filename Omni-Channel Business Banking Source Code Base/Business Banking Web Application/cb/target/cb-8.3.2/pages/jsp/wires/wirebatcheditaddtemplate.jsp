<%--
This page is used to select a wire template and amount to add to a wire batch
currently being edited.

Pages that request this page
----------------------------
wirebatchedittemplate.jsp (Included in wirebatchedit.jsp, edit wire batch)
	ADD WIRE button

Pages this page requests
------------------------
ADD WIRE requests wirebatcheditaddtemplatesend.jsp
CANCEL requests wirebatchedittemplate.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:help id="payments_wirebatcheditaddtemplate" className="moduleHelpClass"/>
<script>
	/* $(function(){
		$("#templateIDSelectID").combobox({'size':'18'});
		
	}); */
</script>
<ffi:setL10NProperty name='PageHeading' value='Edit Wire Transfer Batch'/>
<ffi:setProperty name='PageText' value=''/>

	<%-- Start: Dual approval processing --%>
	<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
		<ffi:setProperty name="GetWireTransferPayees" property="Reload" value="true"/>
		<ffi:process name="GetWireTransferPayees"/>
	</ffi:cinclude>
<script>
// The tempInfo array is set up as follows:  tempInfo[templateID] = ["beneficiaryName","WireLimit"]
tempInfo = new Array();
<ffi:list collection="WireTemplates" items="template">
	
<%-- 	<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
	<ffi:setProperty name="GetDAItem" property="itemId" value="${template.WirePayee.ID}"/>
	<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY %>"/>
	<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.BusinessID}"/>
	<ffi:process name="GetDAItem" /> --%>
	
	<ffi:cinclude value1="${DAItem.Status}" value2="<%=IDualApprovalConstants.STATUS_TYPE_PENDING_APPROVAL %>" operator="notEquals">	
	<ffi:cinclude value1="${DAItem.Status}" value2="<%=IDualApprovalConstants.STATUS_TYPE_REJECTED %>" operator="notEquals">
	
		tempInfo["<ffi:getProperty name='template' property='TemplateID'/>"] = ["<ffi:getProperty name='template' property='WirePayee.PayeeName'/><ffi:cinclude value1="${template.WirePayee.NickName}" value2="" operator="notEquals"> (<ffi:getProperty name="template" property="WirePayee.NickName"/>)</ffi:cinclude>","<ffi:cinclude value1="${template.WireLimitValue.AmountValue}" value2="0.0" operator="notEquals"><ffi:getProperty name='template' property='WireLimit'/></ffi:cinclude>"]
	</ffi:cinclude>
	</ffi:cinclude>
</ffi:list>

function decode(toDecode) {
    if (toDecode != null) {
        var temp = toDecode.replace(/&lt;/g,"<");
        temp = temp.replace(/&gt;/g,">");
        temp = temp.replace(/&#34;/g,"\"");
        temp = temp.replace(/&#39;/g,"'");
        temp = temp.replace(/&amp;/g,"&");        
        return temp;
    }
}

function magicOnAddTemplate(tid) {
	
	frm = document.frmBatchOnAddTemplate;
	tempID = tid.value;
	if (tempID != -1) {
		// put beneficiary name into it's field
		frm["beneficiaryName"].value = decode ( tempInfo[tempID][0] );
		// put wire limit into amount field
		frm["amount"].value = decode ( tempInfo[tempID][1] );
		// move focus to amount field
		//frm["amount"].focus();
		//frm["amount"].select();
	} else {
		// remove beneficiary name from it's field
		frm["beneficiaryName"].value = "";
	}
}
</script>

<div align="center">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td class="lightBackground" align="center">
<%-- CONTENT START --%>
		<s:form id="frmBatchOnAddTemplateID" method="post" name="frmBatchOnAddTemplate" namespace="/pages/jsp/wires" action="modifyWireBatchTemplateAction_addWireTemplate" theme="simple">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<table width="100%" cellpadding="3" cellspacing="0" border="0" class="tableData">
				<tr>
					<td class="sectionsubhead" width="18%"><!--L10NStart--><!-- Template Name --><!--L10NEnd--></td>
				 	<td class="sectionsubhead" width="1%">&nbsp;</td>
					<td class="sectionsubhead" width="32%"  align="center">
					<%-- <ffi:cinclude value1="${ModifyWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="notEquals"><!--L10NStart-->Beneficiary Name<!--L10NEnd--></ffi:cinclude>
					<ffi:cinclude value1="${ModifyWireBatch.BatchDestination}" value2="<%= WireDefines.WIRE_DRAWDOWN %>" operator="equals"><!--L10NStart-->Drawdown Account Name<!--L10NEnd--></ffi:cinclude> --%>
					 </td>
					<td class="sectionsubhead" width="25%"  align="right"><!--L10NStart--><!-- Amount --><!--L10NEnd--></td>
					<td class="sectionsubhead" width="25%"  align="center">&nbsp;</td>
				</tr>
				<tr>
					<td>
					<%-- <select id="templateIDSelectID" name="templateID" class="txtbox" onchange="magicOnAddTemplate(this);">
						<option value=""><!--L10NStart-->Select Template<!--L10NEnd--></option>
						<ffi:list collection="WireTemplates" items="template">
							<ffi:object id="GetDAItem" name="com.ffusion.tasks.dualapproval.GetDAItem" />
							<ffi:setProperty name="GetDAItem" property="itemId" value="${template.WirePayee.ID}"/>
							<ffi:setProperty name="GetDAItem" property="itemType" value="<%=IDualApprovalConstants.ITEM_TYPE_WIRE_BENEFICIARY %>"/>
							<ffi:setProperty name="GetDAItem" property="businessId" value="${SecureUser.BusinessID}"/>
							<ffi:process name="GetDAItem" />
							
							<ffi:cinclude value1="${DAItem.Status}" value2="<%=IDualApprovalConstants.STATUS_TYPE_PENDING_APPROVAL %>" operator="notEquals">	
							<ffi:cinclude value1="${DAItem.Status}" value2="<%=IDualApprovalConstants.STATUS_TYPE_REJECTED %>" operator="notEquals">
								<option value="<ffi:getProperty name="template" property="TemplateID"/>"><ffi:getProperty name="template" property="WireName"/><ffi:cinclude value1="${template.NickName}" value2="" operator="notEquals"> (<ffi:getProperty name="template" property="NickName"/>)</ffi:cinclude></option>
							</ffi:cinclude>
							</ffi:cinclude>
							<ffi:removeProperty name="DAItem"/>
						</ffi:list>
					</select> --%>
				 	<select id="templateIDSelectID" name="templateID" class="txtbox"  onChange="magicOnAddTemplate(this);">
	                </select>
					</td>
					<td class="columndata">&nbsp;</td>
					<td align="center"><input type="Text" name="beneficiaryName" class="txtbox lightBackground" value="" readonly style="border-style:none; text-align:center; padding:3px"></td>
					<td align="right"><input type="Text" name="amount" class="ui-widget-content ui-corner-all txtbox" value="" size="20"></td>
					<td align="center">
						<%-- <a 
							button="true" 
							onclick="ns.common.closeDialog('wireBatchEditAddTemplateDialogID')"
						 >s:text name="jsp.default_82" /><span class="sapUiIconCls icon-decline"></span>
						 </a> --%>
						 <script>
							 ns.wire.getwirebatchedittemplateUrl = function(){
								var stringGetwirebatchedittemplateUrl='<ffi:urlEncrypt url="/cb/pages/jsp/wires/wirebatchedittemplate.jsp?DontInitializeBatch=true"/>';					
								return stringGetwirebatchedittemplateUrl;
							}
						</script> 		 
						 <sj:a id="wireBatchAddTemplateSubmitID"
						 	formIds="frmBatchOnAddTemplateID"  
						 	targets="inputDiv" 
							button="true" 
							onClickTopics="wireBatchAddTemplateSubmitClickTopics"
							onBeforeTopics="wireBatchAddTemplateSubmitBeforeTopics"
							onSuccessTopics="wireBatchAddTemplateSubmitSuccessTopics"><s:text name="jsp.default_29" /></sj:a>	
					</td>
				</tr>

			</table>
			</s:form>
<%-- CONTENT END --%>
		</td>
	</tr>
</table>
&nbsp;
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#wireBatchEditAddTemplateDialogID").parent().addClass("overflowVisible");
});

$("#templateIDSelectID").lookupbox({
	"source":"/cb/pages/jsp/wires/wireTransferTemplateLookupBoxAction.action?wireDestination=<ffi:getProperty name='wireDestination'/>",
	"controlType":"server",
	"size":"30",
	"module":"wireLoadTemplateForBatch",
	"hint": "Select Template",
});
</script>



