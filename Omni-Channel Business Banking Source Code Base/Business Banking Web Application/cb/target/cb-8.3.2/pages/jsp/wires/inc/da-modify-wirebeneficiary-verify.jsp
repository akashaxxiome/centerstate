<%--
This page is a dialog page for modifying wire beneficiary pending changes.

Pages that request this page
----------------------------
Discard and Cancel button

Pages this page requests
------------------------
DONE button requests wire_beneficiary_summary.jsp

Pages included in this page
---------------------------

--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<ffi:setL10NProperty name='PageHeading' value='Verify Modify Wire Beneficiary'/>

<%
if( request.getParameter("itemId") != null ) { session.setAttribute("itemId", request.getParameter("itemId")); }
if( request.getParameter("itemType") != null ) { session.setAttribute("itemType", request.getParameter("itemType")); }
if( request.getParameter("userAction") != null ) { session.setAttribute("userAction", request.getParameter("userAction")); }
if( request.getParameter("Nickname") != null ) { session.setAttribute("Nickname", request.getParameter("Nickname")); }
%>

<ffi:object name="com.ffusion.tasks.dualapproval.ModifyDAItemStatus" id="ModifyDAItemStatus" />
<ffi:setProperty name="ModifyDAItemStatus" property="itemId" value="${itemId}"/>
<ffi:setProperty name="ModifyDAItemStatus" property="itemType" value="${itemType}"/>
<ffi:setProperty name="ModifyDAItemStatus" property="userAction" value="${userAction}" />

<% session.setAttribute("FFICommonDualApprovalTask", session.getAttribute("ModifyDAItemStatus"));%>

<div align="center">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td class="columndata ltrow2_color">
			<div align="center">
				<table width="100%" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td width="100%" valign="top">
							<table width="100%" cellpadding="3" cellspacing="0" border="0">
								<tr>
									<td class="sectionsubhead" align="center"><s:text name="da.message.wirebeneficiary.modify">
									<s:param name="value" value="%{#session.Nickname}"/>
									</s:text>
									</td>
									<td class="columndata">&nbsp;</td>
								</tr>									
							</table>					
						</td>
					</tr>
				 </table>
				 </div>
			 </td>
		 </tr>
		 <tr><td>&nbsp;</td></tr>
 		 <tr><td>&nbsp;</td></tr>
		 <tr>
			<td class="columndata ltrow2_color">
			<div align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="columndata" colspan="6" nowrap align="center">
								<form method="post">
									<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
									<s:url id="modifyURL" escapeAmp="false"  value="/pages/dualapproval/dualApprovalAction-modifyDAChanges.action?daAction=In-Process&module=WireBeneficiary">
										<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>								
									</s:url>
									<sj:a 
										button="true"   
										title="Modify Beneficiary" 
										targets="resultmessage" 
										href="%{modifyURL}"
										onClickTopics="modifyWireBeneficiaryCompleteTopics" 
										onCompleteTopics="refreshWireBeneficiariesGridSuccessTopics" 
										effectDuration="1500" 
										><s:text name="jsp.default_467"/>
									</sj:a>
									<sj:a button="true" title="No" onClickTopics="closeDialog"><s:text name="jsp.default_295"/></sj:a>
								</form>
								</td>
							</tr>
						</table>
				 </div>
			 </td>
		 </tr>
		<br>
	</div>
</div>

