<%--
This is the view international wire batch page.  This is for International wire
batches only.  All other wire batches use wirebatchview.jsp.

This page is not much more than a wrapper.  The main contents of the view
international wire batch can be found in the common include file
common/include-view-wirebatch-international.jsp

Pages that request this page
----------------------------
wire_transfers_list.jsp (included in wiretransfers.jsp, wire summary page)
	View button in a row containing an international batch

Pages included in this page
------------------------
payments/inc/wire_buttons.jsp
	The top buttons (Reporting, Beneficiary, Release Wires, etc)
payments/inc/wire_set_wire.jsp
	Sets a wire transfer or wire batch with a given ID into the session
payments/include-view-transaction-details-constants.jsp
	does nothing other than include common/include-view-transaction-details-constants.jsp
	which sets values in the session used in displaying the view page
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
common/include-view-wirebatch-international.jsp
	A common view page for international wire batches used by many parts of CB
	and BC
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<ffi:help id="payments_wirebatchintview" className="moduleHelpClass"/>
<%
if( request.getParameter("ID") != null ){ session.setAttribute("ID", request.getParameter("ID")); }
if( request.getParameter("collectionName") != null ){ session.setAttribute("collectionName", request.getParameter("collectionName")); }
%>
<ffi:setL10NProperty name='PageHeading' value='View Wire Batch'/>

<ffi:setProperty name="DimButton" value=""/>
<%-- <ffi:include page="${PathExt}payments/inc/wire_buttons.jsp"/>--%>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wirebatchintview.jsp?DontInitialize=true" URLEncrypt="true"/>
<%-- 
<% if (request.getParameter("DontInitialize") == null) { %>
    <%-- Set the wire batch into the session 
    <s:include value="%{#session.PagesPath}/wires/inc/wire_set_wire.jsp"/>
    
    <ffi:setProperty name="collectionToShow" value="WireBatch.Wires"/>
        <ffi:object id="GetWiresByBatchId" name="com.ffusion.tasks.wiretransfers.GetWiresByBatchId" scope="session" />
            <ffi:setProperty name="GetWiresByBatchId" property="BatchId" value="${WireBatch.BatchID}"/>
            <ffi:setProperty name="GetWiresByBatchId" property="CollectionSessionName" value="WireTransfersByBatchId"/>
        <ffi:process name="GetWiresByBatchId"/>
        <ffi:setProperty name="collectionToShow" value="WireTransfersByBatchId"/>
<% } %>
<ffi:removeProperty name="DontInitialize"/>
--%>
<s:include value="%{#session.PagesPath}/common/include-view-transaction-details-constants.jsp"/>


        <div align="center">
            <div align="center">
            <table width="750" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td class="columndata ltrow2_color" align="center">
                     <s:include value="%{#session.PagesPath}/common/include-view-wirebatch-international.jsp"/>
                    </td>
                </tr>
            </table>
            </div>
        </div>

