<%--
This page processes the add wire from import request.  Wires are just created
at this point, but not saved to the back-end.  That happens after the wires are
confirmed.

Pages that request this page
----------------------------
wiretransferimport.jsp
	This happens in a somewhat roundabout way.  Wiretransferimport.jsp makes
	the request, but the request is initiated by uploadpprecord.jsp (a pop-up)
	to a task defined on wiretransferimport.jsp (FileUpload).  FileUpload, in
	turn, requests this page.
	
Pages this page requests
------------------------
Javascript auto-refreshes to wiretransferimportview.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:object id="AddWireTransfersFromImportDA" name="com.ffusion.tasks.dualapproval.wiretransfers.AddWireTransfersFromImportDA" />
	<ffi:process name="AddWireTransfersFromImportDA"/>
</ffi:cinclude>
<%-- End: Dual approval processing --%>

<ffi:object id="AddWireTransfersFromImport" name="com.ffusion.tasks.wiretransfers.AddWireTransfersFromImport" />
<ffi:setProperty name="AddWireTransfersFromImport" property="Initialize" value="true"/>
<ffi:process name="AddWireTransfersFromImport"/>
<%-- perform the validation on the wires to be imported --%>
<%-- add back in WIRE_TRANSFER_BANK --%>
<ffi:setProperty name="AddWireTransfersFromImport" property="Validate" value="FROM_ACCOUNT_ID,AMOUNT,WIRE_PAYEE,WIRE_TRANSFER_BANK,BY_ORDER_OF" />
<ffi:process name="AddWireTransfersFromImport" />

<%-- <script>document.location='<ffi:getProperty name="SecurePath" />payments/wiretransferimportview.jsp'</script> --%>
<s:include value="%{#session.PagesPath}/wires/wiretransferimportview.jsp"/>