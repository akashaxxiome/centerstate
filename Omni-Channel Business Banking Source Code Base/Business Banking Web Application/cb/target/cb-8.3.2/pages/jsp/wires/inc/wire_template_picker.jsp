<%--
This file contains the template drop-down list and the LOAD TEMPLATE button

It is included on all add/edit wire transfer pages (except Host)
    wirebook.jsp
    wiredrawdown.jsp
    wirefed.jsp
    wiretransfernew.jsp

It includes the following file:
common/wire_labels.jsp
    The labels for fields and buttons
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@page import="com.ffusion.tasks.util.BuildIndex"%>
<%@page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ include file="../../common/wire_labels.jsp"%>
<script>
	$("#LoadFromTransferTemplateID_autoComplete").keydown(function(){ 
		if(event.keyCode==13){
			return false;
		}
	}) 
</script>
<script>
    /* This script will clear out the new wire form upon user selecting
     * the 'No Template' option from the template picklist.
     * This method simply reloads the new wire screen for the
     * respective wireType.
     * We could have submitted the form instead of changing window location.
     * If we want to pass the elements of the form to the caller page,
     * then we should consider doing a form submit. In this case, there
     * isn't any value from the form that needs to be passed to the caller page.
     * So we chose to change the window location.
     */
    ns.wire.clearWireForm = function(urlString) {
        $.ajax({
			type: "POST",
            url: urlString,
            data: {CSRF_TOKEN: '<ffi:getProperty name='CSRF_TOKEN'/>'},
            success: function(data) {
                $('#inputDiv').html(data);
            }
        });
    };

    $.subscribe('beforeTopics', function(event,data) {
        frm = document.WiresTemplate;
        if (frm.LoadFromTransferTemplate.options[frm.LoadFromTransferTemplate.selectedIndex].value == -1) {
            event.originalEvent.options.submit = false;
        }
    });
	
    function enableLoadButton(selectedvalue)
	{
		$('#loadWireTransferTemplateID').trigger('click');
	}
    
</script>								
<style type="text/css">
#loadWireTransferTemplateFormID input.combobox{width:80%}
</style>			
 <s:if test="%{#request.ShowWireTemplatePicker}"> 
<div class="leftPaneInnerWrapper" role="form" aria-labelledby="loadTemplateHeader">
<div class="header"><h2 id="loadTemplateHeader"><s:text name='jsp.default_264' /></h2></div>
<div class="leftPaneInnerBox">
	<s:form id="loadWireTransferTemplateFormID" name="WiresTemplate" action="/pages/jsp/wires/addWireTransferAction_loadTemplate.action">
	    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
	        <input type="hidden" name="ALL_TEMPLATES_FLAG" value="true"/>
	            	<select id="LoadFromTransferTemplateID" name="LoadFromTransferTemplate" class="txtbox"  onChange="enableLoadButton(this.value)">
	            		<s:if test='%{#parameters.LoadFromTransferTemplate != null || #parameters.loadFromWireTemplateSummaryGrid != null}'>
		            		<option selected="selected">
		            			<ffi:getProperty name="${wireTask}" property="WireName"/> (<ffi:getProperty name="${wireTask}" property="NickName"/>)
		            		</option>
	            		</s:if>
	                </select>
	                <sj:a
	                    id="loadWireTransferTemplateID"
	                    formIds="loadWireTransferTemplateFormID"
	                    targets="inputDiv"
	                    button="true"
	                    onBeforeTopics="beforeTopics"
						onCompleteTopics="loadWireTempCompleteTopic"
						 cssStyle="display:none;"
	                    ><s:text name="jsp.default_264" /><!-- LOAD TEMPLATE -->
	                </sj:a>
	</s:form>
</div>
</div>
</s:if> 

<script type="text/javascript">
<!--
		
		$("#LoadFromTransferTemplateID").lookupbox({
			"source":"/cb/pages/jsp/wires/wireTransferTemplateLookupBoxAction.action?wireDestination=<ffi:getProperty name='wireDestination'/>",
			"controlType":"server",
			"size":"40",
			"module":"wireLoadTemplate",
		});
		
if('<ffi:getProperty name="wireBatchObject" property="BatchType"/>'=='FREEFORM'){
	$('#loadWireTransferTemplateFormID').parent('div.leftPaneInnerBox').parent('div.leftPaneInnerWrapper').hide();
}
//-->
</script>	