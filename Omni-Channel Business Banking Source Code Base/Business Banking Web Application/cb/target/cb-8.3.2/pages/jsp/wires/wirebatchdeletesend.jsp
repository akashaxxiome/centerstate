<%--
This file processes the DeleteWireBatch task, which deletes a wire batch.
It is used to delete all types of wire batch.

Pages that request this page
----------------------------
wirebatchdelete.jsp (delete wire batch page (non-international))
	DELETE BATCH button
wirebatchintdelete.jsp (delete international wire batch page)
	DELETE BATCH button

Pages this page requests
------------------------
Javascript auto-refreshes to wiretransfers.jsp
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:object id="DeleteWireBatch" name="com.ffusion.tasks.wiretransfers.DeleteWireBatch" scope="session" />
        <ffi:setProperty name="DeleteWireBatch" property="BeanSessionName" value="WireBatch"/>
<ffi:process name="DeleteWireBatch"/>
