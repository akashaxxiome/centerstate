<%@ page import="com.ffusion.beans.accounts.Accounts"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>
 <%--
This file contains the wire transfer payment fields, such as Amount, Date, and
Recurring info

It is included on all add/edit wire transfer pages (except Host)
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp
	wiretransfernew.jsp

It includes the following file:
common/wire_labels.jsp
	The labels for fields and buttons
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../common/wire_labels.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@page import="com.ffusion.beans.wiretransfers.WireTransfer"%>
<style type="text/css">
.selectPaymentFromAccountHolder input{width:365px;}
</style>
<script>
var wireDestination = "<ffi:getProperty name="${wireTask}" property="WireDestination"/>";
var wireSource = "<ffi:getProperty name="${wireTask}" property="WireSource"/>";
var templateTask = "<ffi:getProperty name="templateTask"/>";
	$(function(){
		$("#selectFrequencyID").selectmenu({'width':'120px'});
		$("#selectFrequencyID1").selectmenu({'width':'120px'});
		$("#selectPaymentFromAccountIDID").lookupbox({
			"controlType":"server",
			"collectionKey":"1",
			"module":"Wires",
			"size":"30",
			"source":"/cb/pages/jsp/wires/WireAccountsLookupBoxAction.action?wireDestination="+wireDestination+"&templateTask="+templateTask+"&wireSource="+wireSource
		});
	});

$("#Wire_Addedit_Payment_FieldsDateId").css('width','110px')
</script>



<% String wireDest = ""; %>
<ffi:getProperty name="${wireTask}" property="WireDestination" assignTo="wireDest"/>
<%-- ------ DOMESTIC ------ --%>
<input type="hidden" name="<ffi:getProperty name="wireTask"/>.<ffi:getProperty name="wireCurrencyField"/>" value="USD">
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="tableData">
	<%-- <tr>
		<td colspan="3" class="sectionhead">
		<%= wireDest.equals(WireDefines.WIRE_DRAWDOWN) ? 
				"<!--L10NStart-->Payment Information<!--L10NEnd-->" : 
					"<!--L10NStart--><!--L10NEnd-->" %> 
	</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr> --%>
	<tr>
		<td width="50%" valign="top">
<%-- ------ DOMESTIC PAYMENT INFO ------ --%>
			<table cellpadding="3" cellspacing="0" border="0" class="tableData" width="100%">
				<%-- <% if (!wireDest.equals(WireDefines.WIRE_DRAWDOWN)) { %> --%>
				<%-- <ffi:cinclude value1="${templateTask}" value2="" operator="equals">
					<ffi:object id="GetWiresAccounts" name="com.ffusion.tasks.wiretransfers.GetWiresAccounts" scope="session"/>
						<ffi:setProperty name="GetWiresAccounts" property="Reload" value="true"/>
						<ffi:setProperty name="GetWiresAccounts" property="Type" value="<%= wireDest %>"/>
						<ffi:setProperty name="GetWiresAccounts" property="Source" value="${wireSource}"/>
					<ffi:process name="GetWiresAccounts"/>
					<ffi:removeProperty name="GetWiresAccounts"/>
				</ffi:cinclude>

				<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
					<ffi:object id="GetTemplatesAccounts" name="com.ffusion.tasks.wiretransfers.GetTemplatesAccounts" scope="session"/>
						<ffi:setProperty name="GetTemplatesAccounts" property="Reload" value="true"/>
					<ffi:process name="GetTemplatesAccounts"/>
					<ffi:removeProperty name="GetTemplatesAccounts"/>
				</ffi:cinclude> --%>
				
				<tr>
					<% if (!wireDest.equals(WireDefines.WIRE_DRAWDOWN)) { %><td width="50%"><span class="sectionsubhead"><label for="selectPaymentFromAccountIDID_autoComplete"><%= LABEL_FROM_ACCOUNT %></label></span><span class="required" title="required">*</span></td><% } %>
					<td id="wireAmountLabel"><span class="sectionsubhead">
						<ffi:cinclude value1="${templateTask}" value2="" operator="equals"><label for="AMOUNT"><%= LABEL_AMOUNT %></label></span><span class="required" title="required">*</span></ffi:cinclude>
						<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals"><label for="AMOUNT"><%= LABEL_TEMPLATE_LIMIT %></label></span></ffi:cinclude>
					</td>
					<ffi:cinclude value1="${templateTask}" value2="" operator="equals"><td id="wireDateLabel"><span class="sectionsubhead"><label for="Wire_Addedit_Payment_FieldsDateId"><%= LABEL_DUE_DATE %></label></span><span class="required" title="required">*</span></td></ffi:cinclude>
				</tr>
				<%-- <% } %> --%>
				<tr>
					<% if (!wireDest.equals(WireDefines.WIRE_DRAWDOWN)) { %>
						<td>
							<div class="selectBoxHolder selectPaymentFromAccountHolder">
								<select id="selectPaymentFromAccountIDID" class="txtbox" style="width: 200px;" name="<ffi:getProperty name="wireTask"/>.FromAccountID" <ffi:getProperty name="disableSemi"/>>
									
									<% String curAcct = ""; String thisAcct = ""; %>
									<ffi:getProperty name="${wireTask}" property="FromAccountID" assignTo="curAcct"/>
									<% if (curAcct == null) curAcct = ""; %>
									<ffi:setProperty name="selectedAcct"  value="<%=curAcct%>"/>
									
									<ffi:cinclude value1="${selectedAcct}" value2="" operator="notEquals">
									<ffi:cinclude value1="${selectedAcct}" value2="-1" operator="notEquals">
										<option value="<ffi:getProperty name="selectedAcct" />" selected><ffi:getProperty name="${wireTask}" property="FromAccountNumberDisplayText" /></option>
									</ffi:cinclude>
									</ffi:cinclude>
								</select>
							  </div>
						</td>
					<% } %>
					<% String wireAmount = ""; %>
					<ffi:getProperty name="${wireTask}" property="${wireAmountField}" assignTo="wireAmount"/>
					<ffi:object id="Currency" name="com.ffusion.beans.common.Currency" scope="request" />
					<ffi:setProperty name="Currency" property="Amount" value="<%= wireAmount %>"/>
					<ffi:setProperty name="Currency" property="Format" value="0.00"/>
					<td><input id="AMOUNT" name="<ffi:getProperty name="wireTask"/>.<ffi:getProperty name="wireAmountField"/>Value" value="<ffi:getProperty name="Currency" property="CurrencyStringNoSymbol"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="15" maxlength="16">
					<input type="hidden" name="CurrentWireAmountField" value="<ffi:getProperty name="wireAmountField"/>"/>
					</td>
					<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
						<td>
							<%-- <input name="<ffi:getProperty name="wireTask"/>.DueDateValue" value="<ffi:getProperty name="${wireTask}" property="DueDate"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="11" <ffi:getProperty name="disableDates"/>>
							--%>
							<ffi:cinclude value1="${disableDates}" value2="disabled" operator="notEquals">
								<%-- <a onclick="window.open('<ffi:urlEncrypt url="${SecurePath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=${wireTask}.DueDateValue&wireDest=${wireDest}"/>','Calendar','width=350,height=300');" href="#"><img src="/cb/web/multilang/grafx/i_calendar.gif" alt="" width="19" height="16" align="absmiddle" border="0"></a>
								--%>
								<% 	//This is for datepicker								
									String dueDateForDP=((WireTransfer)session.getAttribute("wireTransfer")).getDueDate();
									session.setAttribute("dueDateForDP",dueDateForDP);
								%>
								<sj:datepicker
								id="Wire_Addedit_Payment_FieldsDateId"
								value="%{#session.dueDateForDP}"
								name="%{#request.wireTask}.DueDateValue"
								maxlength="10"
							
								buttonImageOnly="true"
								cssClass="ui-widget-content ui-corner-all" />
							<script>
		                        var tmpUrl = '<ffi:urlEncrypt url="${FullPagesPath}calendar.jsp?calDirection=forward&calOneDirectionOnly=true&calTransactionType=5&calDisplay=select&calForm=WireNew&calTarget=${wireTask}.DueDateValue&wireDest=${wireDest}"/>';
		                        ns.common.enableAjaxDatepicker("Wire_Addedit_Payment_FieldsDateId", tmpUrl);
		                        $('img.ui-datepicker-trigger').attr('title','<s:text name="jsp.default_calendarTitleText"/>'); 
		                         </script>
	
							</ffi:cinclude>
							<ffi:cinclude value1="${disableDates}" value2="disabled" operator="equals">
								<input name="<ffi:getProperty name="wireTask"/>.DueDate" value="<ffi:getProperty name="${wireTask}" property="DueDate"/>" type="text" class="ui-widget-content ui-corner-all txtbox" size="15" <ffi:getProperty name="disableDates"/>>
	                        </ffi:cinclude>
						</td>
					</ffi:cinclude>
				</tr>
				<tr>
					<% if (!wireDest.equals(WireDefines.WIRE_DRAWDOWN)) { %><td valign="top"><span id="<ffi:getProperty name="wireTask"/>.FromAccountIDError"></span></td><% } %>
					<td valign="top"><span id="<ffi:getProperty name="wireTask"/>.<ffi:getProperty name="wireAmountField"/>Error"></span></td>
					<td valign="top"><span id="<ffi:getProperty name="wireTask"/>.DueDateError"></span></td>
				</tr>
				<tr>
					<td id="wireFrequencyLabel" class="sectionsubhead"><label for="selectFrequencyID"><%= LABEL_FREQUENCY %></label></td>
					<td class="sectionsubhead" colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2"><select id="selectFrequencyID" name="<ffi:getProperty name="wireTask"/>.FrequencyValue" class="txtbox" onchange="enableRepeating(this.options[this.selectedIndex].value);" <s:if test="%{isWireBatch}">disabled</s:if> <s:else> <ffi:getProperty name="disableRec"/></s:else>>
						<ffi:getProperty name="freqNone" encode="false"/>
						<% String curFreq = ""; String thisFreq = ""; %>
						<ffi:getProperty name="${wireTask}" property="FrequencyValue" assignTo="curFreq"/>
						<% if (curFreq == null) curFreq = ""; %>
						<ffi:list collection="wireFrequencyList" items="wireFrequency">						
							<ffi:getProperty name="wireFrequency"  property ="key" assignTo="thisFreq"/>							
							<option value="<ffi:getProperty name="wireFrequency" property="key"/>" <%= curFreq.equals(thisFreq) ? "selected" : "" %>><ffi:getProperty name="wireFrequency" property="value"/></option>						
						</ffi:list>
						</select>
					<input id="wireUnlimited" disabled type="radio" name="openEnded" value="true" onclick="neverending(this.value)"  <ffi:getProperty name="disableRec"/>><span class="columndata"> <%= LABEL_UNLIMITED %></span>
					<% String numXfers = ""; Integer freqNoneInt = new Integer(com.ffusion.beans.Frequencies.FREQ_NONE); %>
					<ffi:getProperty name="${wireTask}" property="NumberTransfers" assignTo="numXfers"/>
					<% if (numXfers == null) numXfers = ""; %>
					<input id="noOfWireTransfer" disabled type="radio" name="openEnded" value="false" onclick="neverending(this.value)" checked  <ffi:getProperty name="disableRec"/>><span class="columndata"> <%= LABEL_NUMBER_OF_PAYMENTS %> </span><input type="text" name="<ffi:getProperty name="wireTask"/>.NumberTransfers" size="2" maxlength="3" class="ui-widget-content ui-corner-all txtbox" value="<%= numXfers.equals(freqNoneInt.toString()) ? "" : numXfers %>" <ffi:getProperty name="disableRec"/>>
					</td>
					
					<td class="sectionsubhead">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="3">
						<span id="<ffi:getProperty name="wireTask"/>.FrequencyValueError"></span>
						<span id="<ffi:getProperty name="wireTask"/>.NumberTransfersError"></span>
					</td>
				</tr>
			</table>
		</td>
			<%-- <td class="tbrd_l" width="10"><img src="/cb/web/multilang/grafx/payments/spacer.gif" width="1" height="1" alt=""></td>
		<td width="348" valign="top">
------ DOMESTIC REPEATING INFO ------
			<table width="347" cellpadding="3" cellspacing="0" border="0" class="tableData">
				<tr>
					<td id="wireFrequencyLabel" class="sectionsubhead"><%= LABEL_FREQUENCY %></td>
					<td><select id="selectFrequencyID" name="<ffi:getProperty name="wireTask"/>.FrequencyValue" class="txtbox" onchange="enableRepeating(this.options[this.selectedIndex].value);" <s:if test="%{isWireBatch}">disabled</s:if> <s:else> <ffi:getProperty name="disableRec"/></s:else>>
						<ffi:getProperty name="freqNone" encode="false"/>
						<% String curFreq = ""; String thisFreq = ""; %>
						<ffi:getProperty name="${wireTask}" property="FrequencyValue" assignTo="curFreq"/>
						<% if (curFreq == null) curFreq = ""; %>
						<ffi:list collection="wireFrequencyList" items="wireFrequency">						
							<ffi:getProperty name="wireFrequency"  property ="key" assignTo="thisFreq"/>							
							<option value="<ffi:getProperty name="wireFrequency" property="key"/>" <%= curFreq.equals(thisFreq) ? "selected" : "" %>><ffi:getProperty name="wireFrequency" property="value"/></option>						
						</ffi:list>
						</select>
					</td><span id="<ffi:getProperty name="wireTask"/>.FrequencyValueError"></span>
				</tr>
				<tr>
					<td class="sectionsubhead" style="padding-top:0px; padding-bottom:0px;">&nbsp;</td>
					<td style="padding-top:0px; padding-bottom:0px;"><input id="wireUnlimited" type="radio" name="openEnded" value="true" onclick="neverending(this.value)"  <ffi:getProperty name="disableRec"/>><span class="columndata"> <%= LABEL_UNLIMITED %></span></td>
				</tr>
				<tr>
					<td class="sectionsubhead" style="padding-top:0px; padding-bottom:0px;">&nbsp;</td>
					<% String numXfers = ""; Integer freqNoneInt = new Integer(com.ffusion.beans.Frequencies.FREQ_NONE); %>
					<ffi:getProperty name="${wireTask}" property="NumberTransfers" assignTo="numXfers"/>
					<% if (numXfers == null) numXfers = ""; %>
					<td style="padding-top:0px; padding-bottom:0px;" nowrap><input id="noOfWireTransfer" type="radio" name="openEnded" value="false" onclick="neverending(this.value)" checked  <ffi:getProperty name="disableRec"/>><span class="columndata"> <%= LABEL_NUMBER_OF_PAYMENTS %> </span><input type="text" name="<ffi:getProperty name="wireTask"/>.NumberTransfers" size="2" maxlength="3" class="ui-widget-content ui-corner-all txtbox" value="<%= numXfers.equals(freqNoneInt.toString()) ? "" : numXfers %>" disabled></td>
					<span id="<ffi:getProperty name="wireTask"/>.NumberTransfersError"></span>
				</tr>
			</table>
		</td> --%>
	</tr>
</table>
<ffi:removeProperty name="amountPostfix"/>
<ffi:removeProperty name="dueDateForDP"/>
<ffi:removeProperty name="isRecModel" />


