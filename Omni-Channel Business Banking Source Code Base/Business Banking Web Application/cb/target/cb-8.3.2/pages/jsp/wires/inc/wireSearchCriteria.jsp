<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines"%>

<div class="searchDashboardRenderCls">
		<div id="quicksearchcriteria" class="quickSearchAreaCls" style="display:none; clear:both;">	
            <s:form action="/pages/jsp/wires/getWireTransfersAction_verify.action" method="post" id="QuickSearchWiresFormID" name="QuickSearchWiresForm" theme="simple">
				<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
				<table border="0" class="tableData">
 					<tr>
						<td nowrap>
							<span class="sectionsubhead dashboardLabelMargin" style="display:block;"><s:text name="jsp.default.label.date.range"/></span>
							<input type="text" id="wiresDateRangeBox" />
							<input value="<ffi:getProperty name='wireSearchCriteria' property='startDate' />" id="StartDateID" name="StartDate" type="hidden" />
							<input value="<ffi:getProperty name='wireSearchCriteria' property='endDate' />" id="EndDateID" name="EndDate" type="hidden" />
						</td>
						<!-- <td width="5"></td> -->
						<td nowrap>
						<!-- <label for="">Show</label> -->
						<span class="sectionsubhead dashboardLabelMargin" style="display:block;"><s:text name="jsp.default_381" /></span>
						<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_ADMIN_VIEW %>">
							<select name="viewAll" id="viewAllID" class="txtbox">
								<option value="false" <ffi:cinclude value1="${viewAll}" value2="true" operator="notEquals">selected</ffi:cinclude>>My Wires</option>
								<option value="true" <ffi:cinclude value1="${viewAll}" value2="true" operator="equals">selected</ffi:cinclude>>
									<s:if test="%{isGroupEntitleme}">
										<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>">Business Wires</ffi:cinclude>
										<ffi:cinclude ifNotEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.VIEW_TRANSACTIONS_ACCROSS_GROUPS %>">Group Wires</ffi:cinclude>
									</s:if>
									<s:else>Business Wires</s:else>									
									
								</option>
							</select>
						</ffi:cinclude>
						<% String curType = ""; %>
						<ffi:getProperty name="showWireType" assignTo="curType"/>
						<% if (curType == null) curType = ""; %>
						<select id="showWireTypeID" name="showWireType" class="txtbox" style="width:100px;">
							<option value="">All Wire Types</option>
							<option value="<%= WireDefines.WIRE_DOMESTIC %>" <%= curType.equals(WireDefines.WIRE_DOMESTIC) ? "selected" : "" %>><ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_DOMESTIC %>"/><ffi:getProperty name="WireDestinations" property="Value"/></option>
							<option value="<%= WireDefines.WIRE_INTERNATIONAL %>" <%= curType.equals(WireDefines.WIRE_INTERNATIONAL) ? "selected" : "" %>><ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_INTERNATIONAL %>"/><ffi:getProperty name="WireDestinations" property="Value"/></option>
							<option value="<%= WireDefines.WIRE_BOOK %>" <%= curType.equals(WireDefines.WIRE_BOOK) ? "selected" : "" %>><ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_BOOK %>"/><ffi:getProperty name="WireDestinations" property="Value"/></option>
                            				<option value="<%= WireDefines.WIRE_DRAWDOWN %>" <%= curType.equals(WireDefines.WIRE_DRAWDOWN) ? "selected" : "" %>><ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_DRAWDOWN %>"/><ffi:getProperty name="WireDestinations" property="Value"/></option>
                            				<option value="<%= WireDefines.WIRE_FED %>" <%= curType.equals(WireDefines.WIRE_FED) ? "selected" : "" %>><ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_FED %>"/><ffi:getProperty name="WireDestinations" property="Value"/></option>
                           				<option value="<%= WireDefines.WIRE_HOST %>" <%= curType.equals(WireDefines.WIRE_HOST) ? "selected" : "" %>><ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_HOST %>"/><ffi:getProperty name="WireDestinations" property="Value"/></option>
                           			</select>
						</td>
						<!-- <td width="5"></td> -->
						<td nowrap>
						<span class="sectionsubhead dashboardLabelMargin" style="display:block;">&nbsp;</span>
						<sj:a targets="quick"
							id="quicksearchbutton"
							formIds="QuickSearchWiresFormID"
							button="true" 
							validate="true"
							validateFunction="customValidation"
							onclick="removeValidationErrors();"
							onCompleteTopics="quickSearchWiresComplete">
							<s:text name="jsp.default_6" />
						</sj:a>
					</td>
					</tr>
					<tr>
					<td colspan="3">
						<span id="startDateError"></span>
						<span id="endDateError"></span>
						<span id="dateRangeValueError"></span>
   					</td>
   				</tr>
				</table>
            </s:form>
        </div>
</div>

<script>
$(function(){
	$("#viewAllID").selectmenu({width:'14em'});
	$("#showWireTypeID").selectmenu({width:'14em'});
	
	var aConfig = {
			startDateBox:$("#StartDateID"),
			endDateBox:$("#EndDateID")
		};
	$("#wiresDateRangeBox").extdaterangepicker(aConfig);
});
</script>