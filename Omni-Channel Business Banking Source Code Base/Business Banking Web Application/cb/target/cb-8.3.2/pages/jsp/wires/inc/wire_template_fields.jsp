<%--
This file contains the fields used when adding and editing a wire transfer
template, like template name, scope, etc.

It is included on all add/edit wire transfer pages (except Host)
	wirebook.jsp
	wiredrawdown.jsp
	wirefed.jsp
	wiretransfernew.jsp

It includes the following file:
common/wire_labels.jsp
	The labels for fields and buttons
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<%@ include file="../../common/wire_labels.jsp"%>
<script>
$(function(){
	$("#selectWireCategoryID").selectmenu({width:'22em'});;
	$("#selectWireScopeID").selectmenu({width:'22em'});;
});
</script>
<%
String task = "WireTransfer";
%>

<%-- ------ TEMPLATE INFO ------ --%>
<input type="hidden" name="<%= task %>.WireLimitValue" value="<ffi:getProperty name="<%= task %>" property="WireLimit"/>">
<%
String curCat = "";
String curScope = "";
%>
<ffi:getProperty name="<%= task %>" property="WireCategory" assignTo="curCat"/>
<ffi:getProperty name="<%= task %>" property="WireScope" assignTo="curScope"/>
<%
if (curCat == null) curCat = "";
if (curScope == null) curScope = "";
%>
<div class="marginTop10" role="form" aria-labelledby="templateInfoHeader">
	<div class="paneWrapper">
	  	<div class="paneInnerWrapper">
	  		<div class="header"><h2 id="templateInfoHeader">Template Information</h2></div>
	  		<div class="paneContentWrapper">
			<table width="100%" cellpadding="0" cellspacing="0" border="0" class="formTable tableData">
				<tr>
					<td id="wireTemplateNameLabel" width="50%"><span class="sectionsubhead"><label for="wireTemplateName"><%= LABEL_TEMPLATE_NAME %></label></span><span class="required" title="required">*</span></td>
					<td id="wireTemplateNickNameLabel" width="50%" class="sectionsubhead"><label for="wireTemplateNickName"><%= LABEL_TEMPLATE_NICKNAME %></label></td>
				</tr>
				<tr>
					<td><input id="wireTemplateName" type="text" name="<%= task %>.WireName" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="WireName"/>" size="40" maxlength="32" style="width: 22em"></td>
					<td><input id="wireTemplateNickName" type="text" name="<%= task %>.NickName" class="ui-widget-content ui-corner-all txtbox" value="<ffi:getProperty name="<%= task %>" property="NickName"/>" size="40" maxlength="32" style="width: 22em"></td>
				</tr>
				<tr>
					<td><span id="<ffi:getProperty name="wireTask"/>.WireNameError"></span></td>
					<td></td>
				</tr>
				<tr>
					<td id="wireTemplateCategoryLabel"><span class="sectionsubhead"><label for="selectWireCategoryID"><%= LABEL_TEMPLATE_CATEGORY %></label></span><span class="required">*</span></td>
					<s:if test="%{!IsConsumerUser}">
					<td id="wireTemplateScopeLabel" ><span class="sectionsubhead"><label for="selectWireScopeID"><%= LABEL_TEMPLATE_SCOPE %></label></span><span class="required">*</span></td>
					</s:if>
				</tr>
				<tr>
			<%-- ------ TEMPLATE CATEGORY AND SCOPE ------ --%>
					<td><select id="selectWireCategoryID" name="<%= task %>.WireCategory" class="txtbox">
						<option value="<%= WireDefines.WIRE_CATEGORY_FREEFORM %>" <%= curCat.equals(WireDefines.WIRE_CATEGORY_FREEFORM) ? "selected" : "" %>><!--L10NStart-->Free-form<!--L10NEnd--></option>
						<option value="<%= WireDefines.WIRE_CATEGORY_REPETITIVE %>" <%= curCat.equals(WireDefines.WIRE_CATEGORY_REPETITIVE) ? "selected" : "" %>><!--L10NStart-->Repetitive<!--L10NEnd--></option>
						<option value="<%= WireDefines.WIRE_CATEGORY_SEMIREPETITIVE %>" <%= curCat.equals(WireDefines.WIRE_CATEGORY_SEMIREPETITIVE) ? "selected" : "" %>><!--L10NStart-->Semi-Repetitive<!--L10NEnd--></option>
						</select>
					</td>
					<s:if test="%{!IsConsumerUser}">
					<td>
						<select id="selectWireScopeID" name="<%= task %>.WireScope" class="txtbox">
							<option value="<%= WireDefines.WIRE_SCOPE_BUSINESS %>" <%= curScope.equals(WireDefines.WIRE_SCOPE_BUSINESS) ? "selected" : "" %>><!--L10NStart-->Business
							(General Use)<!--L10NEnd--></option>
							<option value="<%= WireDefines.WIRE_SCOPE_USER %>" <%= curScope.equals(WireDefines.WIRE_SCOPE_USER) ? "selected" : "" %>><!--L10NStart-->User (My Use)<!--L10NEnd--></option>
						</select>
					</td>
					</s:if>
					<s:else>
					<input id="selectWireScopeID" name="<%= task %>.WireScope" value="<%= WireDefines.WIRE_SCOPE_USER %>" type="hidden"  maxlength="128" size="20">
					<script>
					$(document).ready(function(){
					$("#selectWireScopeID").selectmenu('destroy');
					});</script>
					</s:else>
				</tr>
			</table>
			</div>
		</div>
	</div>
</div>