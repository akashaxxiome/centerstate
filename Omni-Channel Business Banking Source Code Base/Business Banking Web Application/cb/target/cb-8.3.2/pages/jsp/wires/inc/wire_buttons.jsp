<%--
This file contains the top buttons used in the wire transfer pages.
It is included in all wire transfer pages that require the top buttons.
	wireaddpayee.jsp
	wireaddpayeebook.jsp
	wireaddpayeedrawdown.jsp
	wirebatchintview.jsp
	wirebatchview.jsp
	wirebookview.jsp
	wiredrawdownview.jsp
	wirefedview.jsp
	wirehostview.jsp
	wirepayeebookdelete.jsp
	wirepayeedelete.jsp
	wirepayeedrawdowndelete.jsp
	wirepayees.jsp
	wirepayeeview.jsp
	wiresrelease.jsp
	wiretemplates.jsp
	wiretransferimportresults.jsp
	wiretransferimportview.jsp
	wiretransfers.jsp
	wiretransferview.jsp

If a string called DimButton is in the session, it will dim the specified button.
DimButton values can be "reporting", "beneficiary", "releasewires", "templates",
and "fileimport".
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:setProperty name="PageText" value=""/>
<% String dimButton = ""; %>
<ffi:getProperty name="DimButton" assignTo="dimButton"/>
<% if (dimButton == null) dimButton = ""; %>

<ffi:cinclude value1="${PaymentReportsDenied}" value2="false" operator="equals">
	<% if (dimButton.equalsIgnoreCase("reporting")) { %>
	    <ffi:setProperty name='PageText' value='${PageText}<img src="/cb/pages/${ImgExt}grafx/i_reporting_dim.gif" alt="" width="68" height="16" border="0" hspace="3">'/>
	<% } else { %>
	    <ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}reports/payments_list.jsp"><img src="/cb/pages/${ImgExt}grafx/i_reporting.gif" alt="" width="68" height="16" border="0" hspace="3"></a>'/>
	<% } %>
</ffi:cinclude>

<% if (dimButton.equalsIgnoreCase("beneficiary")) { %>
    <ffi:setProperty name='PageText' value='${PageText}<img src="/cb/pages/${ImgExt}grafx/payments/i_addeditbene_dim.gif" alt="" width="85" height="16" border="0" hspace="3">'/>
<% } else { %>
	<ffi:setProperty name='PageText1' value='${SecurePath}payments/wirepayees.jsp?DA_WIRE_PRESENT=true' URLEncrypt="true" />
	<ffi:setProperty name='PageText' value='${PageText}<a href="${PageText1}"><img src="/cb/pages/${ImgExt}grafx/payments/i_addeditbene.gif" alt="" width="85" height="16" border="0" hspace="3"></a>'/>
	<ffi:removeProperty name="PageText1"/>
<% } %>

<ffi:cinclude value1="${GetSupportRelease.SupportRelease}" value2="true" operator="equals">
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_RELEASE %>">
		<% if (dimButton.equalsIgnoreCase("releasewires")) { %>
		    <ffi:setProperty name='PageText' value='${PageText}<img src="/cb/pages/${ImgExt}grafx/payments/i_release_dim.gif" width="91" height="16" border="0" hspace="3" alt="">'/>
		<% } else { %>
		    <ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}payments/wiresrelease.jsp"><img src="/cb/pages/${ImgExt}grafx/payments/i_release.gif" width="91" height="16" border="0" hspace="3" alt=""></a>'/>
		<% } %>
	</ffi:cinclude>
</ffi:cinclude>

<% if (dimButton.equalsIgnoreCase("templates")) { %>
   	<ffi:setProperty name='PageText' value='${PageText}<img src="/cb/pages/${ImgExt}grafx/payments/i_templates_dim.gif" alt="" border="0" hspace="3">'/>
<% } else { %>
    <ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}payments/wiretemplates.jsp"><img src="/cb/pages/${ImgExt}grafx/payments/i_templates.gif" alt="" border="0" hspace="3"></a>'/>
<% } %>

<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>">
    <ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FILE_UPLOAD %>">
		<% if (dimButton.equalsIgnoreCase("fileimport")) { %>
    	    <ffi:setProperty name='PageText' value='${PageText}<img src="/cb/pages/${ImgExt}grafx/payments/i_fileimport_dim.gif" alt="" width="70" height="16" hspace="3" border="0">'/>
		<% } else { %>
    	    <ffi:setProperty name='PageText' value='${PageText}<a href="${SecurePath}payments/wiretransferimport.jsp"><img src="/cb/pages/${ImgExt}grafx/payments/i_fileimport.gif" alt="" width="70" height="16" hspace="3" border="0"></a>'/>
		<% } %>
    </ffi:cinclude>
</ffi:cinclude>

<ffi:removeProperty name="DimButton"/>
