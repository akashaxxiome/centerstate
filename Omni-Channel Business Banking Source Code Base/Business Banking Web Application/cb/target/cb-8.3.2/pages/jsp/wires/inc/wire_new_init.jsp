<%--
This file contains initialization code used on all wire transfer pages specific
to adding a wire transfer (not in a batch)

It is included in wire_common_init.jsp, which is included on all add/edit wire
transfer pages

It includes the following files:
common/wire_labels.jsp
	The labels for fields and buttons
--%>
<%@ page import="com.ffusion.beans.FundsTransactionTypes"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ include file="../../common/wire_labels.jsp"%>

<%
String templateTask = (String)session.getAttribute("templateTask");
if (templateTask == null) templateTask = "";
if (!(templateTask.equals("add") || templateTask.equals("edit"))) templateTask = "";
%>
<ffi:cinclude value1="${templateTask}" value2="" operator="equals">
	<ffi:setProperty name="wireBackURL" value="wiretransfers.jsp?Refresh=true" URLEncrypt="true"/>
	<ffi:setProperty name="wireSaveButton" value="<%= BUTTON_SUBMIT_WIRE %>"/>
</ffi:cinclude>

<ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
	<ffi:setProperty name="wireBackURL" value="wiretemplates.jsp"/>
	<ffi:setProperty name="wireSaveButton" value="<%= BUTTON_SUBMIT_TEMPLATE %>"/>
</ffi:cinclude>

<ffi:setProperty name="disableRec" value=""/>
<ffi:setProperty name='freqNone' value='<option value="0"><!--L10NStart-->None<!--L10NEnd--></option>'/>
<ffi:setProperty name="wireAmountField" value="Amount"/>
<ffi:setProperty name="wireCurrencyField" value="AmtCurrencyType"/>

<ffi:removeProperty name="WireTransfer"/>
<ffi:removeProperty name="WireTransferPayee"/>
<ffi:removeProperty name="WireTransferBank"/>
<ffi:removeProperty name="AddWireTransfer"/>
<ffi:removeProperty name="FFIWireTransferPayee"/>
<ffi:removeProperty name="FFIWireTransfer"/>
<ffi:removeProperty name="FFIAddWireTransfer"/>
<ffi:removeProperty name="FFIWireTransferBank"/>

<ffi:object id="WireTransferPayee" name="com.ffusion.beans.wiretransfers.WireTransferPayee" scope="session"/>
<%
session.setAttribute("FFIWireTransferPayee", session.getAttribute("WireTransferPayee"));
%>
<ffi:object id="WireTransferBank" name="com.ffusion.beans.wiretransfers.WireTransferBank" scope="session"/>

<ffi:setProperty name="GetWireTransferPayees" property="Reload" value="true"/>
<ffi:process name="GetWireTransferPayees"/>
		
<% String tid = ""; String collectionName = ""; %>
<ffi:getProperty name="LoadFromTransferTemplate" assignTo="tid"/>
<%
if (tid == null) tid = "-1";
if (tid.equals("")) tid = "-1";
if (tid.equals("-1")) {
%>
	
<%
} else {
	String cat = tid.substring(0,4);
	String templateID = tid.substring(5);
	if (cat.equals("USER")) collectionName = "WireTemplatesUser";
	if (cat.equals("BUSI")) collectionName = "WireTemplatesBusiness";
	if (cat.equals("BANK")) collectionName = "WireTemplatesBank";
	/**
	 * This session name is created in GetWireTemplatesAction.java(see changelist 497202 and 492196) for loading templates, and used by templates summary grid.
	 * This collection name 'collectionName + "ForLoadTempalte"' holds all types of templates.
	 * If the flag ALL_TEMPLATES_FLAG is null, that means the request reqires all templates in the session. 
	 * The session object collectionName + "ForLoadTempalte" is populated in the action GetWiresTemplatesAction.java
	*/
	if( request.getParameter("ALL_TEMPLATES_FLAG") == null ) {
		collectionName = collectionName + "ForLoadTempalte";
	}	
%>
	<ffi:object id="LoadWireTemplate" name="com.ffusion.tasks.wiretransfers.LoadWireTemplate" scope="session"/>
		<ffi:setProperty name="LoadWireTemplate" property="TemplateId" value="<%= templateID %>"/>
		<ffi:setProperty name="LoadWireTemplate" property="TemplatesSessionName" value="<%= collectionName %>"/>
		<ffi:setProperty name="LoadWireTemplate" property="BeanSessionName" value="WireTransfer"/>
	<ffi:process name="LoadWireTemplate"/>
	<ffi:removeProperty name="LoadWireTemplate"/>

	<ffi:object id="AddWireTransfer" name="com.ffusion.tasks.wiretransfers.AddWireTransfer" scope="session"/>
		<ffi:setProperty name="AddWireTransfer" property="BeanSessionName" value="WireTransfer"/>
		<ffi:setProperty name="AddWireTransfer" property="LoadFromTemplate" value="true"/>
		<ffi:setProperty name="AddWireTransfer" property="Initialize" value="true"/>
	<ffi:process name="AddWireTransfer"/>

    <ffi:object id="GetPayeeFromWireTransfer" name="com.ffusion.tasks.wiretransfers.GetPayeeFromWireTransfer" scope="session"/>
		<ffi:setProperty name="GetPayeeFromWireTransfer" property="PayeeSessionName" value="WireTransferPayee"/>
		<ffi:setProperty name="GetPayeeFromWireTransfer" property="WireSessionName" value="AddWireTransfer"/>
	<ffi:process name="GetPayeeFromWireTransfer"/>

	<ffi:setProperty name="AddWireTransfer" property="SettlementDate" value="${AddWireTransfer.DueDate}"/>
	<ffi:setProperty name="AddWireTransfer" property="WireSource" value="TEMPLATE"/>

    <ffi:cinclude value1="${templateTask}" value2="">
        <ffi:cinclude value1="${AddWireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_TEMPLATE %>" operator="equals">
            <ffi:setProperty name="AddWireTransfer" property="WireType" value="<%= WireDefines.WIRE_TYPE_SINGLE %>"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${AddWireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_RECTEMPLATE %>" operator="equals">
            <ffi:setProperty name="AddWireTransfer" property="WireType" value="<%= WireDefines.WIRE_TYPE_RECURRING %>"/>
        </ffi:cinclude>
    </ffi:cinclude>
    <ffi:cinclude value1="${templateTask}" value2="" operator="notEquals">
        <ffi:cinclude value1="${AddWireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_SINGLE %>" operator="equals">
            <ffi:setProperty name="AddWireTransfer" property="WireType" value="<%= WireDefines.WIRE_TYPE_TEMPLATE %>"/>
        </ffi:cinclude>
        <ffi:cinclude value1="${AddWireTransfer.WireType}" value2="<%= WireDefines.WIRE_TYPE_RECURRING %>" operator="equals">
            <ffi:setProperty name="AddWireTransfer" property="WireType" value="<%= WireDefines.WIRE_TYPE_RECTEMPLATE %>"/>
        </ffi:cinclude>
        <ffi:setProperty name="AddWireTransfer" property="WireName" value="${AddWireTransfer.WireName} (CLONE)"/>
        <ffi:setProperty name="AddWireTransfer" property="ID" value=""/>
    </ffi:cinclude>

    <ffi:setProperty name="disableRec" value="disabled"/>
	<ffi:setProperty name='freqNone' value='<option value="0"><!--L10NStart-->None<!--L10NEnd--></option>'/>
	<%
	Integer fundsTypeRec = new Integer(FundsTransactionTypes.FUNDS_TYPE_REC_WIRE_TRANSFER);
	String ft = fundsTypeRec.toString();
	%>
	<ffi:cinclude value1="${AddWireTransfer.Type}" value2="<%= ft %>" operator="equals">
		<ffi:setProperty name="disableRec" value=""/>
		<ffi:setProperty name='freqNone' value=''/>
	</ffi:cinclude>
<%
		session.setAttribute("FFIWireTransferPayee", session.getAttribute("WireTransferPayee"));
	}
%>

<%
	session.setAttribute("FFIModifyWireTransfer", session.getAttribute("AddWireTransfer"));
%>

<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.dateFormat}"/>
<ffi:setProperty name="AddWireTransfer" property="DateFormat" value="${UserLocale.dateFormat}"/>
<ffi:setProperty name="AddWireTransfer" property="DueDate" value="${GetCurrentDate.Date}"/>
<ffi:setProperty name="AddWireTransfer" property="SettlementDate" value="${GetCurrentDate.Date}"/>

<ffi:object id="SetWireTransferPayee" name="com.ffusion.tasks.wiretransfers.SetWireTransferPayee" scope="session"/>

<ffi:object id="RemoveIntermediaryBank" name="com.ffusion.tasks.wiretransfers.RemoveIntermediaryBank" scope="session" />
	<ffi:setProperty name="RemoveIntermediaryBank" property="PayeeSessionName" value="WireTransferPayee"/>

<ffi:removeProperty name="isRecModel" />
