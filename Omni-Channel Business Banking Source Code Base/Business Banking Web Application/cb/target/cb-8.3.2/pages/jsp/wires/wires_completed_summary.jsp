<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:help id="payments_wiretransferCompletedSummary" className="moduleHelpClass"/>
	<ffi:setGridURL grid="GRID_wireCompleted" name="InqueryURL"    url="/cb/pages/jsp/wires/sendFundsTransMessageAction_initWireInquiry.action?FundsID={0}&paymentType={1}&recurringId={2}" parm0="ID" parm1="transType" parm2="RecurringID"/>
	
	<ffi:setGridURL grid="GRID_wireCompleted" name="ViewURL" url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID={0}&transType={1}&recurringId={2}&collectionName=CompletedWireTransfers" parm0="ID" parm1="transType" parm2="RecurringID"/>
	<ffi:setGridURL grid="GRID_wireCompleted" name="ViewURLInternational" url="/cb/pages/jsp/wires/viewWireBatch.action?batchId={0}&collectionName=CompletedWireTransfers" parm0="ID" />
	<ffi:setGridURL grid="GRID_wireCompleted" name="ViewURLNotInternational" url="/cb/pages/jsp/wires/viewWireBatch.action?batchId={0}&collectionName=CompletedWireTransfers" parm0="ID" />
	
	<ffi:setGridURL grid="GRID_wireCompleted" name="EditURL" url="/cb/pages/jsp/wires/modifyWireTransferAction_init.action?ID={0}&transType={1}&recurringId={2}&collectionName=CompletedWireTransfers" parm0="ID" parm1="transType" parm2="RecurringID"/>
	<ffi:setGridURL grid="GRID_wireCompleted" name="EditURLHost" url="/cb/pages/jsp/wires/modifyHostWireAction_init.action?ID={0}&collectionName=PendingWireTransfers&pendApproval=" parm0="ID" />
	<ffi:setGridURL grid="GRID_wireCompleted" name="EditURLInternational" url="/cb/pages/jsp/wires/wirebatchedit.jsp?ID={0}&collectionName=CompletedWireTransfers&DontInitializeBatch=false" parm0="ID" />
	<ffi:setGridURL grid="GRID_wireCompleted" name="EditURLNotInternational" url="/cb/pages/jsp/wires/wirebatchedit.jsp?ID={0}&collectionName=CompletedWireTransfers&DontInitializeBatch=false" parm0="ID" />
	
	<ffi:setGridURL grid="GRID_wireCompleted" name="SkipURL" url="/cb/pages/jsp/wires/skipWireTransferAction_init.action?isSkip=true&ID={0}&transType={1}&recurringId={2}&collectionName=CompletedWireTransfers" parm0="ID" parm1="transType" parm2="RecurringID" />
	<ffi:setGridURL grid="GRID_wireCompleted" name="SkipURLInternational" url="/cb/pages/jsp/wires/skipWireBatch_init.action?isSkip=true&batchId={0}&collectionName=CompletedWireTransfers" parm0="ID" />
	<ffi:setGridURL grid="GRID_wireCompleted" name="SkipURLNotInternational" url="/cb/pages/jsp/wires/skipWireBatch_init.action?isSkip=true&batchId={0}&collectionName=CompletedWireTransfers" parm0="ID" />
	
	<ffi:setGridURL grid="GRID_wireCompleted" name="DeleteURL" url="/cb/pages/jsp/wires/deleteWireTransferAction_init.action?ID={0}&transType={1}&recurringId={2}&collectionName=CompletedWireTransfers" parm0="ID" parm1="transType" parm2="RecurringID" />
	<ffi:setGridURL grid="GRID_wireCompleted" name="DeleteURLInternational" url="/cb/pages/jsp/wires/deleteWireBatch_init.action?batchId={0}&collectionName=CompletedWireTransfers" parm0="ID" />
	<ffi:setGridURL grid="GRID_wireCompleted" name="DeleteURLNotInternational" url="/cb/pages/jsp/wires/deleteWireBatch_init.action?batchId={0}&collectionName=CompletedWireTransfers" parm0="ID" />
	
	<ffi:setProperty name="tempURL" value="/pages/jsp/wires/getWireTransfersAction.action?collectionName=CompletedWireTransfers&wiresAccountsName=WiresAccounts&GridURLs=GRID_wireCompleted" URLEncrypt="true"/>
    <s:url id="completedWiresUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid  
		id="completedWiresGridID"  
		caption=""  
		dataType="local"  
		href="%{completedWiresUrl}"  
		pager="true"  
		gridModel="gridModel" 
		rowList="%{#session.StdGridRowList}" 
		rowNum="%{#session.StdGridRowNum}" 
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		sortable="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		sortname="date"
		sortorder="desc"
		onGridCompleteTopics="addGridControlsEvents,completedWiresGridCompleteEvents"
		> 
		
		<sjg:gridColumn name="date" index="date" title="%{getText('jsp.wire.Date')}" sortable="true" width="65"/>
		<sjg:gridColumn name="map.transType" index="transType" title="%{getText('jsp.wire.Transaction')}" sortable="true" width="75"/>
		<sjg:gridColumn name="map.beneficiary" index="beneficiary" title="%{getText('jsp.wire.Beneficiary')}" sortable="true" width="75"/>
		<sjg:gridColumn name="map.accountNickname" index="accountNickname" title="%{getText('jsp.wire.Account_Nickname')}" sortable="true" width="120"/>
		<sjg:gridColumn name="map.destination" index="destination" title="%{getText('jsp.wire.Type')}" sortable="true" width="55"/>
		<sjg:gridColumn name="statusName" index="status" title="%{getText('jsp.wire.Status')}" sortable="true" width="55"/>
		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amount" title="%{getText('jsp.wire.Amount')}" sortable="true" width="55" formatter="ns.wire.formatWireAmount"/>
		<sjg:gridColumn name="transaction.totalOrigAmount" index="amount" title="Amount" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="ID" index="ID" title="Action" sortable="false" width="100" formatter="ns.wire.formatCompletedWiresActionLinks" search="false" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
		<sjg:gridColumn name="canEdit" index="canEdit" title="canEdit" sortable="true" width="55" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="canDelete" index="canDelete" title="canDelete" sortable="true" width="55" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="ViewURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.InqueryURL" index="InqueryURL" title="InqueryURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.EditURL" index="EditURL" title="EditURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="DeleteURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
	</sjg:grid>
	
	<script>
		$("#completedWiresGridID").jqGrid('setColProp','ID',{title:false});
	</script>
