<%--
This file contains the initialization of tasks and beans required for all
add/edit wire beneficiary pages.

It is included in the following files:
	wireaddpayee.jsp
	wireaddpayeebook.jsp
	wireaddpayeedrawdown.jsp
--%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>

<ffi:removeProperty name="Initialize" />
<ffi:removeProperty name="WirePayee" />
<ffi:object id="WireTransferBank" name="com.ffusion.beans.wiretransfers.WireTransferBank" scope="session"/>


<%-- Remove from the session, just in case it's lingering from add/edit wire transfer --%>
<ffi:removeProperty name="disableEdit"/>

<%-- Start: Dual approval processing --%>
<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
	<ffi:cinclude value1="${payeeTask}" value2="AddWireTransferPayee">
		<ffi:cinclude value1="${ID}" value2="" operator="notEquals">
			<ffi:object id="SetWireTransferPayeeDA" name="com.ffusion.tasks.dualapproval.wiretransfers.SetWireTransferPayeeDA" scope="session"/>
				<ffi:setProperty name="SetWireTransferPayeeDA" property="ID" value="${ID}"/>
			<ffi:process name="SetWireTransferPayeeDA"/>
			<ffi:removeProperty name="SetWireTransferPayeeDA" />
			<ffi:setProperty name="AddWireTransferPayee" property="Initialize" value="true"/>
			<ffi:process name="AddWireTransferPayee"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>
<%-- End: Dual approval processing--%>

	