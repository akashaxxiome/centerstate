<script type="text/javascript"><!--

function newImage(arg) {
	if (document.images) {
		rslt = new Image();
		rslt.src = arg;
		return rslt;
	}
}

userAgent = window.navigator.userAgent;
browserVers = parseInt(userAgent.charAt(userAgent.indexOf("/")+1),10);
mustInitImg = true;

function initImgID() {
	di = document.images;
	if (mustInitImg && di) {
		for (var i=0; i<di.length; i++) {
			if (!di[i].id) di[i].id=di[i].name;
		}
		mustInitImg = false;
	}
}

function findElement(n,ly) {
	d = document;
	if (browserVers < 4)		return d[n];
	if ((browserVers >= 6) && (d.getElementById)) {initImgID; return(d.getElementById(n))};
	var cd = ly ? ly.document : d;
	var elem = cd[n];
	if (!elem) {
		for (var i=0;i<cd.layers.length;i++) {
			elem = findElement(n,cd.layers[i]);
			if (elem) return elem;
		}
	}
	return elem;
}

function changeImages() {
	d = document;
	if (d.images) {
		var img;
		for (var i=0; i<changeImages.arguments.length; i+=2) {
			img = null;
			if (d.layers) {img = findElement(changeImages.arguments[i],0);}
			else {img = d.images[changeImages.arguments[i]];}
			if (img) {img.src = changeImages.arguments[i+1];}
		}
	}
}


var preloadFlag = false;

function preloadImages() {
	if (document.images) {
		over_hdr_nav_acctmng_b = newImage(/*URL*/'/cb/web/multilang/grafx/hdr_menu_arrow.gif');
		preloadFlag = true;
	}
}

// --></script>
