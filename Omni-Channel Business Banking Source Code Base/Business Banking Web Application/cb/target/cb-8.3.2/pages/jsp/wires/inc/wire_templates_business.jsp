<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:help id="payments_wireTemplateBusinessSummary" className="moduleHelpClass"/>
	<ffi:setGridURL grid="GRID_wireTemplateBusiness" name="EditURL" url="/cb/pages/jsp/wires/modifyWireTemplateAction_init.action?ID={0}&wireTemplateType={1}&collectionName={2}" parm0="ID" parm1="WireType" parm2="collectionName"/>
	<ffi:setGridURL grid="GRID_wireTemplateBusiness" name="DeleteURL" url="/cb/pages/jsp/wires/deleteWireTemplateAction_init.action?ID={0}&wireTemplateType={1}&collectionName={2}" parm0="ID" parm1="WireType" parm2="collectionName"/>
	<ffi:setGridURL grid="GRID_wireTemplateBusiness" name="ViewURL" url="/cb/pages/jsp/wires/viewWireTemplateAction.action?ID={0}&wireTemplateType={1}&collectionName={2}" parm0="ID" parm1="WireType" parm2="collectionName"/>
	<ffi:setGridURL grid="GRID_wireTemplateBusiness" name="LoadURL" url="/cb/pages/jsp/wires/addWireTransferAction_loadTemplate.action?ID={0}&wireTemplateType={1}&collectionName={2}&loadFromWireTemplateSummaryGrid=true" parm0="ID" parm1="WireType" parm2="collectionName"/>
	<ffi:setGridURL grid="GRID_wireTemplateBusiness" name="CloneURL" url="/cb/pages/jsp/wires/addWireTemplateAction_cloneTemplate.action?ID={0}&wireTemplateType={1}&collectionName={2}" parm0="ID" parm1="WireType" parm2="collectionName"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/wires/getWireTransferTemplatesAction.action?collectionName=WireTemplatesBusiness&GridURLs=GRID_wireTemplateBusiness" URLEncrypt="true"/>
    <s:url id="templatesWiresUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
		id="wireTemplatesBusinessGridID"
		caption=""
		dataType="local"
		href="%{templatesWiresUrl}"
		pager="true"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}"
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="false"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		sortable="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		sortorder="asc"
		onGridCompleteTopics="addGridControlsEvents,wireTemplatesBusinessGridCompleteEvents"
		>
		<sjg:gridColumn name="templateName" index="templateName" title="%{getText('jsp.default.Template_Name')}" sortable="true" width="90" formatter="ns.wire.customWireNameColumnForTemplate"/>
		<sjg:gridColumn name="wireDestination" index="wireDestination" title="%{getText('jsp.wire.Type')}" sortable="true" width="65"/>
		<sjg:gridColumn name="map.displayFromAccoutNickName" index="map.displayFromAccoutNickName" title="%{getText('jsp.default.From_Accout_Nickname')}" sortable="true" width="135"/>
		<sjg:gridColumn name="wirePayee.payeeName" index="wirePayee.payeeName" title="%{getText('jsp.wire.Beneficiary')}" sortable="true" width="55"/>
		<sjg:gridColumn name="statusName" index="Status" title="%{getText('jsp.wire.Status')}" sortable="true" width="50"/>
		<sjg:gridColumn name="displayAmount" index="displayAmount" title="%{getText('jsp.wire.Limit')}" sortable="true" width="55"/>

		<sjg:gridColumn name="canLoad" index="canLoad" title="" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="canDelete" index="canDelete" title="canDelete" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="canEdit" index="canEdit" title="" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="recurringID" index="recurringID" title="" sortable="true" hidden="true" hidedlg="true"/>

		<sjg:gridColumn name="status" index="status" title="%{getText('jsp.wire.Status')}" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="nickName" index="nickName" title="" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="wireName" index="wireName" title="" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.allowEdit" index="map.allowEdit" title="" sortable="true"  hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.wireViewPage" index="map.wireViewPage" title="" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.wireDeletePage" index="wireDeletePage" title="" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.wireEditPage" index="wireEditPage" title="" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.wireEditTask" index="wireEditTask" title="" sortable="true" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverInfos" index="approverInfos" title="" hidden="true" hidedlg="true" formatter="ns.wire.formatApproversInfo" />
		<sjg:gridColumn name="ID" index="ID" title="Action" sortable="false" width="135" formatter="ns.wire.formatWireTemplatesActionLinks" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
		<sjg:gridColumn name="resultOfApproval" index="resultOfApproval" title="ResultOfApproval" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="rejectReason" index="rejectReason" title="RejectReason" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverName" index="approverName" title="ApproverName" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverIsGroup" index="approverIsGroup" title="ApproverIsGroup" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="userName" index="userName" title="UserName" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.EditURL" index="EditURL" title="EditURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="DeleteURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="ViewURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.LoadURL" index="LoadURL" title="LoadURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.CloneURL" index="CloneURL" title="CloneURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.containPendingPayee" index="payeeStatus" title="payeeStatus" search="false" sortable="false" hidden="true" hidedlg="true"/>

	</sjg:grid>

	<script>
		$("#wireTemplatesBusinessGridID").jqGrid('setColProp','ID',{title:false});
	</script>
