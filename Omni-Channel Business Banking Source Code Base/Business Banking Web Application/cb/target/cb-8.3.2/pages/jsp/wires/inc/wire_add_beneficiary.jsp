<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<script>
	var wireDestination = "<ffi:getProperty name="${wireTask}" property="WireDestination"/>";
	var PAYEE_DESTINATION_DA = "<ffi:getProperty name="PAYEE_DESTINATION_DA" />";
	var isSimplified = $('#beneficiaryType').val();
	var value = "";	
	
	/* if (wireDestination == "DRAWDOWN") {
		value = {value:"Create New Debit Account", label:js_create_new_debit_account};		
	} else if(isSimplified){
		// do nothings
	}
	else {
	value = {value:"Create New Beneficiary", label:js_create_new_beneficiary};		
	} */
	
	$("#selectWirePayeeID").lookupbox({
		"source":"/cb/pages/jsp/wires/wireTransferPayeeLookupBoxAction.action?wireDestination="+wireDestination,
		"controlType":"server",
		"size":"35",
		"module":"wireLoadPayee"
	});
</script>
<%-- ------ BENEFICIARY PICK LIST: WirePayeeID ------ --%>
	<% String curPayee = ""; String thisPayee = ""; String payeeName=""; String nickName="";String accountNum="";String wirePayeeID="";%>
	<ffi:getProperty name="${wireTask}" property="WirePayeeID" assignTo="curPayee"/>
	<ffi:getProperty name="WireTransferPayee" property="PayeeName" assignTo="payeeName" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="NickName" assignTo="nickName" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="AccountNum" assignTo="accountNum" encodeAssign="true"/>
	<ffi:getProperty name="WireTransferPayee" property="ID" assignTo="wirePayeeID"/>

	<% if (curPayee == null) curPayee = ""; %>

	<%-- Set current payee Id in session --%>
	<ffi:setProperty name="currentPayeeId" value="<%=curPayee%>"/>
	<ffi:cinclude value1="" value2="${currentPayeeId}" operator="equals">
		<ffi:setProperty name="currentPayeeId" value="<%= WireDefines.CREATE_NEW_BENEFICIARY%>"/>			
	</ffi:cinclude>
	<div class="selectBoxHolder beneficiaryComboHolder" style="margin: 0 0 10px;">
	<s:if test="%{beneficiaryType=='managed' || beneficiaryType==null}">
		<select class="txtbox" id="selectWirePayeeID" name="" size="1" onChange="setPayee(this.options[this.selectedIndex].value)" <ffi:getProperty name="disableSemi"/>>
				<%
				if(curPayee!=null && !curPayee.isEmpty() && !curPayee.equals(WireDefines.WIRE_LOOKUPBOX_DEFAULT_VALUE) 
					&& !curPayee.equals(WireDefines.CREATE_NEW_BENEFICIARY) && !curPayee.equals(WireDefines.CREATE_NEW_DEBIT_ACCOUNT)){
					String label = payeeName;
					if(nickName!=null && !nickName.equalsIgnoreCase("")){
						label = label +" (" + nickName + " - "+ accountNum + ")";
					}else{
						label = label +" (" + accountNum + ")";
					}
					
				%>
				<option value="<%=curPayee%>" selected="selected"><%=label %></option>
				<%
				} else { 
				String label = WireDefines.CREATE_NEW_BENEFICIARY;
				%>
				<option value="<%=label%>" selected="selected"></option>
				<% } %>
	</select>
	</s:if>
	</div>
	<div align="center">
		<sj:a name="payeeDetails" id="freeForm" onclick="loadBeneficiaryForm()" button="true">
			Add New
		</sj:a>
	</div>