<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines"%>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants" %>
<div class="dashboardUiCls">
	<div class="moduleSubmenuItemCls">
		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-wires"></span>
			<span class="moduleSubmenuLbl">
				<s:text name="jsp.default_465" />
			</span>
			<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>

			<!-- dropdown menu include -->    		
			<s:include value="/pages/jsp/home/inc/transferSubmenuDropdown.jsp" />
	</div>
	
	<!-- dashboard items listing for wires -->
    <div id="dbWireSummary" class="dashboardSummaryHolderDiv">
    	<s:url id="WiresUrl" value="/pages/jsp/wires/initWireAction_summary.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a
			id="wiresLink"
			href="%{WiresUrl}"
			targets="summary"
			indicator="indicator"
			button="true"
			cssClass="summaryLabelCls"
			onClickTopics="beforeWiresSummaryOnClickTopics,setDisableOnChange"
			onSuccessTopics="wiresSummaryLoadedSuccess"
			><s:text name="jsp.home_197" /><!-- WIRES -->
		</sj:a>
		
		 <sj:a id="goBackSummaryLink" 
    		button="true" 
    		cssStyle="display:none"
			onClick="reloadWires()">
			<s:text name="Summary" />
		</sj:a>
		
		
		<div id="newWireOrTampButtonPanel" style="display: inline-block; " >
			 <div id="newWireTransferCriteria" class="dashboardCustomDiv">
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>">
					<ffi:removeProperty name="disableNewOptions"/>
					<ffi:cinclude value1="${GetEntitledWireTypes.EntitledWireTypes.Size}" value2="0" operator="equals">
						<ffi:setProperty name="disableNewOptions" value="disabled"/>
							<span class="sectionsubhead" style="color:red;" align="center"><!--L10NStart-->You do not have any available wire types.  Please contact your system administration to correct this condition.<!--L10NEnd--></span>
					</ffi:cinclude>
					<s:form name="WireTransfer" action="/pages/jsp/wires/wiretransfernew.jsp" id="SelectWiresTypeFormID" theme="simple">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<span class="sectionsubhead marginRight5 dashboardText"><s:text name="jsp.default_581" /> : </span>
						<select name="dests" id="wiresTypesID" class="txtbox" <ffi:getProperty name="disableNewOptions"/> onchange="ns.wire.newWiresTypesOnChange()">
							<s:iterator value="wireTypeList">
									<option value="<ffi:getProperty name="key"/>"><ffi:getProperty name="value" /></option>
							</s:iterator>
							<ffi:cinclude value1="${disableNewOptions}" value2="disabled" operator="equals">
								<option value=""><!--L10NStart-->None Available<!--L10NEnd--></option>
							</ffi:cinclude>
						</select>
						<ffi:cinclude value1="${GetEntitledWireTypes.EntitledWireTypes.Size}" value2="0" operator="notEquals">
							<sj:a targets="inputDiv"
								id="newSingleWiresTransferID"
								formIds="SelectWiresTypeFormID"
								button="true"
								onClickTopics="setDestinationTopic,beforeLoadWiresTransferForm"
								onCompleteTopics="selectWiresTypeCompleteTopic"
								cssStyle=" margin-left:25px;">
								<s:text name="jsp.default_590" /><!-- NEW WIRE -->
								</sj:a>
							<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_BATCH_CREATE %>">
								<sj:a targets="inputDiv"
									id="newBatchWiresTransferID"
									formIds="SelectWiresTypeFormID"
									button="true"
									onClickTopics="setBatchDestinationTopic,beforeLoadWireBatchForm"
									onCompleteTopics="selectWiresTypeCompleteTopic"
									>
									<s:text name="jsp.default_591" /><!-- NEW BATCH -->
									</sj:a>
							</ffi:cinclude>
						</ffi:cinclude>
					</s:form>
				</ffi:cinclude>
			</div>
		</div>
        
	</div>
	
	<!-- dashboard items listing for templates -->
    <div id="dbTemplateSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
    		<s:url id="templatesUrl" value="/pages/jsp/wires/initWireTemplatesSummaryAction_init.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			</s:url>
			<sj:a
				id="templatesLink"
				href="%{templatesUrl}"
				targets="summary"
				indicator="indicator"
				button="true"
				cssClass="summaryLabelCls"
				onClickTopics="beforeWireTemplatesOnClickTopics,setDisableOnChange"
				onSuccessTopics="wireTemplatesSummaryLoadedSuccess"
				><s:text name="jsp.default_5" /><!-- TEMPLATES -->
			</sj:a>
			
			
			<div id="newWireTemplateCriteria" style="display: inline-block;" class="dashboardCustomDiv">
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRE_TEMPLATES_CREATE %>">
					<s:form name="WireTemplate" action="/pages/jsp/wires/wiretransfernew.jsp" id="SelectWiresTemplateTypeFormID" theme="simple">
						<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<span class="sectionsubhead marginRight5 dashboardText" style="line-height:30px;"><s:text name="jsp.default_581" /> :  </span>
						<select name="dests" id="wiresTemplateTypesID" class="txtbox marginRight10" onchange="ns.wire.newWiresTypesOnChange()">
							<option selected="selected" value="DOMESTIC">Domestic</option>
							<option value="INTERNATIONAL">International</option>
							<option value="BOOKTRANSFER">Book</option>
							<option value="DRAWDOWN">Drawdown</option>
							<option value="FEDWIRE">FED Wire</option>							
						</select>
						<sj:a targets="inputDiv" id="newSingleWiresTemplateID" formIds="SelectWiresTemplateTypeFormID" 
							button="true" onClickTopics="setTemplateDestinationTopic,clickNewWireTemplateTopic" 
							onCompleteTopics="selectWiresTypeCompleteTopic,prepareTemplateJsp">
							<s:text name="NEW_TEMPLATE" /><!-- NEW TEMPLATE -->
						</sj:a>
					</s:form>
				</ffi:cinclude>
			</div>			
        
	</div>
	
	<!-- dashboard items listing for release wires -->
    <div id="dbReleaseWireSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
		<s:if test="%{isSupportRelease}">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_RELEASE %>">
				<s:url id="releaseWiresUrl" value="/pages/jsp/wires/initReleaseWireDashBoard_init.action">
					<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				</s:url>
				<sj:a
					id="releaseWiresLink"
					href="%{releaseWiresUrl}"
					targets="wiresReleaseDiv"
					title="Release Wires"
					indicator="indicator"
					button="true"
					cssClass="summaryLabelCls"
					onClickTopics="releaseWiresOnClickTopics"
					onCompleteTopics="releaseWiresonCompleteTopics"
					><s:text name="jsp.default_589" /><!-- RELEASE WIRES -->
				</sj:a>
			</ffi:cinclude>
		</s:if>
	</div>
	
	<!-- dashboard items listing for wires beneficiaries -->
    <div id="dbPayeeSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
        <s:url id="ManageBeneficiaryUrl" value="/pages/jsp/wires/wireBeneficiaryDashBoard_init.action">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a
			id="beneficiaryLink"
			href="%{ManageBeneficiaryUrl}"
			targets="summary"
			title="Manage Beneficiary"
			indicator="indicator"
			button="true"
			cssClass="summaryLabelCls"
			onClickTopics="beforeBeneficiaryOnClickTopics"
			onSuccessTopics="beneficicariesSummaryLoadedSuccess"
			><s:text name="jsp.default_68" /><!-- BENEFICIARY -->
		</sj:a>
		
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>">
			<s:url id="createBeneficiary" value="/pages/jsp/wires/addWireBeneficiaryAction_init.action" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="Initialize" value="true"/>
				<s:param name="payeeTask" value="%{'AddWireTransferPayee'}"/>
				<s:param name="DA_WIRE_PRESENT" value="false"/>
				<s:param name="isBeneficiary" value="true"/>
			</s:url>
		    <sj:a id="createBeneficiaryLink" href="%{createBeneficiary}" targets="inputDiv"  button="true" title="Create Beneficiary"
		    	onClickTopics="beforeLoadBeneficiaryForm" onCompleteTopics="loadBeneficiaryFormComplete" onErrorTopics="errorLoadBeneficiaryForm">
			  	<s:text name="jsp.default_592" /><!-- NEW BENEFICIARY -->
		    </sj:a>
		</ffi:cinclude>
		<ffi:cinclude value1="${Business.dualApprovalMode}" value2="<%=IDualApprovalConstants.BASIC_DUAL_APPROVAL %>" operator="notEquals">
			<s:url id="createBeneficiary" value="/pages/jsp/wires/addWireBeneficiaryAction_init.action" escapeAmp="false">
				<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
				<s:param name="Initialize" value="true"/>
				<s:param name="payeeTask" value="%{'AddWireTransferPayee'}"/>
				<s:param name="DA_WIRE_PRESENT" value="false"/>
				<s:param name="isBeneficiary" value="true"/>
			</s:url>
		    <sj:a id="createBeneficiaryLink" href="%{createBeneficiary}" targets="inputDiv"  button="true" title="Create Beneficiary"
		    	onClickTopics="beforeLoadBeneficiaryForm" onCompleteTopics="loadBeneficiaryFormComplete" onErrorTopics="errorLoadBeneficiaryForm">
			  	<s:text name="jsp.default_592" /><!-- NEW BENEFICIARY -->
		    </sj:a>
		</ffi:cinclude>
		
	</div>
	
	<!-- dashboard items listing for file upload -->
    <div id="dbFileUploadSummary" class="dashboardSummaryHolderDiv hideDbOnLoad">
    	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FILE_UPLOAD %>">
					<s:url id="FileImportUrl" value="/pages/jsp/wires/wiretransferimport.jsp">
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
					</s:url>
					<sj:a
						id="FileImportEntryLink"
						href="%{FileImportUrl}"
						targets="selectImportFormatOfFileUploadID"
						title="Import wires transfer file"
						indicator="indicator"
						button="true"
						cssClass="summaryLabelCls"
						onClickTopics="onClickUploadWireFormTopics,fileUploadClickTopic"
						onCompleteTopics="onImportWireFormCompleteTopics"
						onErrorTopics="errorImportWireFileOnErrorTopics"
						><s:text name="jsp.default_539" /><!-- FILE IMPORT -->
					</sj:a>
					
			        <s:url id="CustomMappingsUrl" value="/pages/jsp/fileuploadcustom.jsp?Section=WIRE" escapeAmp="false">
			             <s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
			        </s:url>
					<sj:a
						id="customMappingsLink"
						href="%{CustomMappingsUrl}"
						targets="mappingDiv"
						title="Custom Mappings"
						indicator="indicator"
						button="true"
						onCompleteTopics="customMappingsOnCompleteTopics"
						><s:text name="CUSTOM_MAPPINGS" /><!-- CUSTOM MAPPINGS -->
					</sj:a>
					
					<s:url id="addCustomMappingUrl" value="/pages/jsp/custommapping.jsp" escapeAmp="false">
						<s:param name="SetMappingDefinitionID" value="0"/>
						<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
					</s:url>
					<sj:a
						id="addCustomMappingsLink"
						href="%{addCustomMappingUrl}"
						targets="inputCustomMappingDiv"
						title="%{getText('jsp.default_135')}"
						indicator="indicator"
						button="true"
						onClickTopics="beforeCustomMappingsOnClickTopics,onWireCustomMappingClickTopics" 
						onCompleteTopics="addCustomMappingsOnCompleteTopics" 
						onErrorTopics="errorCustomMappingsOnErrorTopics"
						style="display:none;"
					>
						<s:text name="jsp.default_34" />
					</sj:a> 
			</ffi:cinclude>
		</ffi:cinclude>
	</div>
	
	<!-- More list option listing -->
	<div class="dashboardSubmenuItemCls">
		<span class="dashboardSubmenuLbl"><s:text name="jsp.default.more" /></span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
	
		<!-- UL for rendering tabs -->
		<ul class="dashboardSubmenuItemList">
			<li class="dashboardSubmenuItem"><a id="showdbWireSummary" onclick="ns.common.refreshDashboard('showdbWireSummary',true)"><s:text name="jsp.home_235"/></a></li>
		
			<li class="dashboardSubmenuItem"><a id="showdbTemplateSummary" onclick="ns.common.refreshDashboard('showdbTemplateSummary',true)"><s:text name="jsp.default_5"/></a></li>
			
			<s:if test="%{isSupportRelease}">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_RELEASE %>">
				<li class="dashboardSubmenuItem"><a id="showdbReleaseWireSummary" onclick="ns.common.refreshDashboard('showdbReleaseWireSummary',true)"><s:text name="jsp.default_589"/></a></li>
			</ffi:cinclude>
			</s:if>
			<li class="dashboardSubmenuItem"><a id="showdbPayeeSummary" onclick="ns.common.refreshDashboard('showdbPayeeSummary',true)"><s:text name="jsp.default_68"/></a></li>
			
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>">
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FILE_UPLOAD %>">
					<li class="dashboardSubmenuItem"><a id="showdbFileUploadSummary" onclick="ns.common.refreshDashboard('showdbFileUploadSummary',true)"><s:text name="jsp.default_539"/></a></li>
				</ffi:cinclude>
			</ffi:cinclude>
		</ul>
	</div>
	<ffi:cinclude value1="${SecureUser.BusinessID}" value2="0" operator="notEquals">
		<span id="isConsumerUserLoggedIn" />
	</ffi:cinclude>
<%-- <ul class="dashboardTabsCls">
		<li><a onclick="openWires()" href="#appdashboard-left" class="sapUiIconCls icon-simple-payment"><s:text name="jsp.home_235" /><!-- WIRES --></a>	</li>
	   	 
	   	<li><a onclick="openTemplates()" href="#appdashboard-left" class="sapUiIconCls icon-survey"><s:text name="jsp.default_5" /><!-- TEMPLATES --></a></li>
	   	
		<li><a onclick="openBeneficiary()" href="#appdashboard-left" class="sapUiIconCls icon-person-placeholder"><s:text name="jsp.default_68" /><!-- Beneficiary 	 --></a></li>
		
		
		<s:if test="%{isSupportRelease}">
			<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_RELEASE %>">
				<li><a onclick="releaseWires()" href="#appdashboard-left" class="sapUiIconCls icon-payment-approval"><s:text name="jsp.default_589" /></a></li>
			</ffi:cinclude>
		</s:if><!-- Release Wires -->
			
		<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.WIRES_CREATE %>">
				<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.FILE_UPLOAD %>">
					<li>
				   		<a id="fileImportTabWires" href="#appdashboard-left" onclick="openFileImport()" class="sapUiIconCls icon-home-share"><s:text name="jsp.default_539" /></a>
				   	</li>
				</ffi:cinclude>
		</ffi:cinclude>
</ul> --%>



<%-- 
	
    <div id="appdashboard-left">
    

		<sj:a
			id="quickSearchLink"
			button="true"
			onclick="quickSearchOnclick()"			
		 	><span class="sapUiIconCls icon-search"></span><s:text name="jsp.default_4" /><!-- QUICK SEARCH -->
		</sj:a>
		<sj:a
			id="quickSearchTemplateLink"
			button="true"
        	onClickTopics="clearWireTemplateQuickSearchTopic"            		
			><span class="sapUiIconCls icon-search"></span><s:text name="jsp.default_4" /><!-- QUICK SEARCH -->
		</sj:a>
		<sj:a
			id="quicksearchReleaseWireLink"
			button="true"
            onclick="quickSearchReleaseWireOnclick()"
			><span class="sapUiIconCls icon-search"></span><s:text name="jsp.default_4" /><!-- QUICK SEARCH -->
		</sj:a>
		




        

 --%>
 
 
 
 <script>
	ns.wire.enableOnChange = false;
	ns.wire.isTemplate = false;
	ns.wire.isBatch = false;

	//QTS:759216 Added to maintain orginal selection of the wire type in case of wire and wire template
	ns.wire.oldWireType = "";
	ns.wire.oldWireTemplateType = "";
	
	ns.wire.newWiresTypesOnChange = function(){
		if(ns.wire.enableOnChange){
			if(!ns.wire.isTemplate){
				if(!ns.wire.isBatch){
					ns.wire.changeWireTypeConfirmation(false);
				} else {
					ns.wire.changeWireTypeConfirmation(true);
				}
			} else {
				ns.wire.changeWireTypeConfirmationForTemplate();
			}
		}else{
			ns.wire.oldWireType = $("#wiresTypesID").val();
			ns.wire.oldWireTemplateType = $("#wiresTemplateTypesID").val(); 
		}
	}

	$(function(){
		ns.wire.oldWireType = $("#wiresTypesID").val();
		ns.wire.oldWireTemplateType = $("#wiresTemplateTypesID").val(); 
		
		if (ns.wire.isSelectMenuAvailable("#wiresTypesID")) {
			$("#wiresTypesID").selectmenu({width:'9em'});
		}
		
		
		if (ns.wire.isSelectMenuAvailable("#wiresTemplateTypesID")) {
			$("#wiresTemplateTypesID").selectmenu({width:'12em'});
		}
	});
	
	$("#newSingleWiresTransferID, #newBatchWiresTransferID, #newSingleWiresTemplateID, #createBeneficiaryLink").click(function(){
		hideDashboard();	
	});
	
	$('body').on('click', '#cancelFormButtonOnInput,#cancelFormButtonOnVerify,#sendFormButtonOnConfirm', function () {
		showDashboard();
		$('#wiresFileImportHolder').hide();
	});
	
	
	$.subscribe('setDestinationTopic', function(event,data) {
		frm = document.WireTransfer;
		dest = frm.dests.options[frm.dests.selectedIndex].value;

		var targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTransferAction_init.action"/>';
		if (dest == "DOMESTIC")	{
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTransferAction_init.action?wireDestination=DOMESTIC"/>';
		} else if ( dest == "INTERNATIONAL" ){
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTransferAction_init.action?wireDestination=INTERNATIONAL"/>';
		} else if ( dest == "HOST" ){
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addHostWireAction_init.action"/>';
		} else if ( dest == "DRAWDOWN" ){
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTransferAction_init.action?wireDestination=DRAWDOWN"/>';
		} else if ( dest == "FEDWIRE" ){
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTransferAction_init.action?wireDestination=FEDWIRE"/>';
		} else if ( dest == "BOOKTRANSFER" ){
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTransferAction_init.action?wireDestination=BOOKTRANSFER"/>';
		}
		$("#SelectWiresTypeFormID").attr("action",targetAction);

		ns.wire.enableOnChange = true;
		ns.wire.isTemplate = false;
		ns.wire.isBatch = false;

		//$('#newSingleWiresTransferID').hide();
		//$('#newBatchWiresTransferID').hide();
		//$('#wiresLink').show();
		//hideCalendar();
});
$.subscribe('setTemplateDestinationTopic', function(event,data) {
		frm = document.WireTemplate;
		dest = frm.dests.options[frm.dests.selectedIndex].value;
		var targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTemplateAction_init.action"/>';
		if (dest == "DOMESTIC")	{					
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTemplateAction_init.action?wireDestination=DOMESTIC"/>';
		} else if ( dest == "INTERNATIONAL" ){
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTemplateAction_init.action?wireDestination=INTERNATIONAL"/>';
		} else if ( dest == "DRAWDOWN" ){
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTemplateAction_init.action?wireDestination=DRAWDOWN"/>';
		} else if ( dest == "FEDWIRE" ){
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTemplateAction_init.action?wireDestination=FEDWIRE"/>';
		} else if ( dest == "BOOKTRANSFER" ){
			targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireTemplateAction_init.action?wireDestination=BOOKTRANSFER"/>';
		}
		$("#SelectWiresTemplateTypeFormID").attr("action",targetAction);

		ns.wire.enableOnChange = true;
		ns.wire.isTemplate = true;

		//$('#newSingleWiresTemplateID').hide();				
		//$('#templatesLink').show();
});

$.subscribe('setBatchDestinationTopic', function(event,data) {
	frm = document.WireTransfer;
	dest = frm.dests.options[frm.dests.selectedIndex].value;
	var targetAction = "";
	if (dest == "HOST"){
		targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addHostWireBatchAction_init.action?DontInitializeBatch=&wireDestination=HOST"/>';
	}else if (dest == "DOMESTIC"){
		targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireBatchAction_init.action?DontInitializeBatch=&wireDestination=DOMESTIC&DA_WIRE_PRESENT=true"/>';
	}else if ( dest == "INTERNATIONAL" ){
		targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireBatchAction_init.action?DontInitializeBatch=&wireDestination=INTERNATIONAL&DA_WIRE_PRESENT=true"/>';
	}else if ( dest == "DRAWDOWN" ){
		targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireBatchAction_init.action?DontInitializeBatch=&wireDestination=DRAWDOWN&DA_WIRE_PRESENT=true"/>';
	}else if ( dest == "FEDWIRE" ){
		targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireBatchAction_init.action?DontInitializeBatch=&wireDestination=FEDWIRE&DA_WIRE_PRESENT=true"/>';
	}else if ( dest == "BOOKTRANSFER" ){
		targetAction = '<ffi:urlEncrypt url="/cb/pages/jsp/wires/addWireBatchAction_init.action?DontInitializeBatch=&wireDestination=BOOKTRANSFER&DA_WIRE_PRESENT=true"/>';
	}
	$("#SelectWiresTypeFormID").attr("action",targetAction);

	ns.wire.enableOnChange = true;
	ns.wire.isTemplate = false;
	ns.wire.isBatch = true;

	//$('#newSingleWiresTransferID').hide();
	//$('#newBatchWiresTransferID').hide();
	//$('#wiresLink').show();
	//hideCalendar();
});

$("#wiresLink").find("span").addClass("dashboardSelectedItem");
	
</script>