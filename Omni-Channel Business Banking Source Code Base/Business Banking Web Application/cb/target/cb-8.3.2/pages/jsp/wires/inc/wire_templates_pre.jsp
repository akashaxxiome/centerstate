<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ page import="com.sap.banking.dualapprovalbackend.constants.IDualApprovalConstants"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<ffi:setL10NProperty name='PageHeading' value='Wire Transfer Templates'/>

<ffi:setProperty name="DimButton" value="templates"/>

<ffi:setProperty name="BackURL" value="${SecurePath}payments/wiretemplates.jsp" />

<ffi:removeProperty name="wireTask"/>
<ffi:removeProperty name="templateTask"/>
<ffi:removeProperty name="DontInitialize"/>

<ffi:cinclude value1="${templateStartDate}" value2="" operator="equals">
	<ffi:setProperty name="templateStartDate" value="01/01/2000" />
</ffi:cinclude>
<ffi:cinclude value1="${templateEndDate}" value2="" operator="equals">
	<ffi:setProperty name="templateEndDate" value="01/01/2050" />
</ffi:cinclude>

<%
if( request.getParameter("showTemplateType") != null){
	if("-1".equals(request.getParameter("showTemplateType"))){
		session.setAttribute("showTemplateType", "");
	}else{
		session.setAttribute("showTemplateType", request.getParameter("showTemplateType")); 
	}
}

String templateName = request.getParameter("SearchTemplateName");
String searchAccount = request.getParameter("searchAcct");
if(searchAccount != null && searchAccount.equals("All Accounts")){
	searchAccount="";
}
String beneficiaryName = request.getParameter("SearchBeneficiaryName");
String beneficiaryAccountNumber = request.getParameter("SearchBeneficiaryAccountNumber");
String beneficiaryRoutingNumber = request.getParameter("SearchBeneficiaryRoutingNumber");
%>

<ffi:setProperty name="SearchTemplateName" value="<%=templateName%>" />
<ffi:setProperty name="searchAcct" value="<%=searchAccount%>" />
<ffi:setProperty name="SearchBeneficiaryName" value="<%=beneficiaryName%>" />
<ffi:setProperty name="SearchBeneficiaryAccountNumber" value="<%=beneficiaryAccountNumber%>" />
<ffi:setProperty name="SearchBeneficiaryRoutingNumber" value="<%=beneficiaryRoutingNumber%>" />

<%
if (request.getParameter("GetWireTemplatesUser.SortedBy") == null &&
    request.getParameter("GetWireTemplatesUser.PreviousPage") == null &&
    request.getParameter("GetWireTemplatesUser.NextPage") == null &&
    request.getParameter("GetWireTemplatesBusiness.SortedBy") == null &&
    request.getParameter("GetWireTemplatesBusiness.PreviousPage") == null &&
    request.getParameter("GetWireTemplatesBusiness.NextPage") == null &&
    request.getParameter("GetWireTemplatesBank.SortedBy") == null &&
    request.getParameter("GetWireTemplatesBank.PreviousPage") == null &&
    request.getParameter("GetWireTemplatesBank.NextPage") == null) {
		// reset the sort name if showTemplateType is not on the request (the user did not click Refresh View)
		boolean resetSort = (request.getParameter("showTemplateType") == null);
		String currentSort = "";
%>
		<ffi:getProperty name="GetWireTemplatesUser" property="SortCriteriaName" assignTo="currentSort"/>
		<% if (currentSort == null) currentSort = ""; %>
		<ffi:object id="GetWireTemplatesUser" name="com.ffusion.tasks.wiretransfers.GetPagedWireTemplates" scope="session"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="DateFormat" value="${UserLocale.DateFormat}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="FirstPage" value="true"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="PageSize" value=""/>
			<ffi:setProperty name="GetWireTemplatesUser" property="StartDate" value="${templateStartDate}" />
			<ffi:setProperty name="GetWireTemplatesUser" property="EndDate" value="${templateEndDate}" />
			<ffi:setProperty name="GetWireTemplatesUser" property="CollectionSessionName" value="WireTemplatesUser" />
			<ffi:setProperty name="GetWireTemplatesUser" property="Type" value="${showTemplateType}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="WireScope" value="<%= WireDefines.WIRE_SCOPE_USER %>" />
			<ffi:setProperty name="GetWireTemplatesUser" property="PendingApproval" value="true"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="Status" value=""/>
			<ffi:setProperty name="GetWireTemplatesUser" property="ClearSortCriteria" value=""/>
			<ffi:setProperty name="GetWireTemplatesUser" property="SortCriteriaOrdinal" value="1"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="SortCriteriaName" value="<%= resetSort ? WireDefines.SORT_CRITERIA_WIRENAME : currentSort %>"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="SortCriteriaAsc" value="true"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="NoSortImage" value="${noSortImage}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="AscendingSortImage" value="${ascSortImage}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="DescendingSortImage" value="${descSortImage}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="TemplateName" value="${SearchTemplateName}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="DebitAccountID" value="${searchAcct}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="BeneficiaryName" value="${SearchBeneficiaryName}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="BeneficiaryAccountNumber" value="${SearchBeneficiaryAccountNumber}"/>
			<ffi:setProperty name="GetWireTemplatesUser" property="BeneficiaryBankRoutingNumber" value="${SearchBeneficiaryRoutingNumber}"/>
		<%--<ffi:process name="GetWireTemplatesUser"/>--%>

	<ffi:getProperty name="GetWireTemplatesBusiness" property="SortCriteriaName" assignTo="currentSort"/>
	<% if (currentSort == null) currentSort = ""; %>
	<ffi:object id="GetWireTemplatesBusiness" name="com.ffusion.tasks.wiretransfers.GetPagedWireTemplates" scope="session"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="FirstPage" value="true"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="PageSize" value=""/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="StartDate" value="${templateStartDate}" />
		<ffi:setProperty name="GetWireTemplatesBusiness" property="EndDate" value="${templateEndDate}" />
		<ffi:setProperty name="GetWireTemplatesBusiness" property="CollectionSessionName" value="WireTemplatesBusiness" />
		<ffi:setProperty name="GetWireTemplatesBusiness" property="Type" value="${showTemplateType}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="WireScope" value="<%= WireDefines.WIRE_SCOPE_BUSINESS %>" />
		<ffi:setProperty name="GetWireTemplatesBusiness" property="PendingApproval" value="true"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="Status" value=""/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="ClearSortCriteria" value=""/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="SortCriteriaOrdinal" value="1"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="SortCriteriaName" value="<%= resetSort ? WireDefines.SORT_CRITERIA_WIRENAME : currentSort %>"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="SortCriteriaAsc" value="true"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="NoSortImage" value="${noSortImage}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="AscendingSortImage" value="${ascSortImage}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="DescendingSortImage" value="${descSortImage}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="TemplateName" value="${SearchTemplateName}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="DebitAccountID" value="${searchAcct}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="BeneficiaryName" value="${SearchBeneficiaryName}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="BeneficiaryAccountNumber" value="${SearchBeneficiaryAccountNumber}"/>
		<ffi:setProperty name="GetWireTemplatesBusiness" property="BeneficiaryBankRoutingNumber" value="${SearchBeneficiaryRoutingNumber}"/>
	<%--<ffi:process name="GetWireTemplatesBusiness"/>--%>

	<ffi:getProperty name="GetWireTemplatesBank" property="SortCriteriaName" assignTo="currentSort"/>
	<% if (currentSort == null) currentSort = ""; %>
	<ffi:object id="GetWireTemplatesBank" name="com.ffusion.tasks.wiretransfers.GetPagedWireTemplates" scope="session"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="DateFormatParam" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="DateFormat" value="${UserLocale.DateFormat}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="FirstPage" value="true"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="PageSize" value=""/>
		<ffi:setProperty name="GetWireTemplatesBank" property="StartDate" value="${templateStartDate}" />
		<ffi:setProperty name="GetWireTemplatesBank" property="EndDate" value="${templateEndDate}" />
		<ffi:setProperty name="GetWireTemplatesBank" property="CollectionSessionName" value="WireTemplatesBank" />
		<ffi:setProperty name="GetWireTemplatesBank" property="Type" value="${showTemplateType}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="WireScope" value="<%= WireDefines.WIRE_SCOPE_BANK %>" />
		<ffi:setProperty name="GetWireTemplatesBank" property="PendingApproval" value="true"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="Status" value=""/>
		<ffi:setProperty name="GetWireTemplatesBank" property="ClearSortCriteria" value=""/>
		<ffi:setProperty name="GetWireTemplatesBank" property="SortCriteriaOrdinal" value="1"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="SortCriteriaName" value="<%= resetSort ? WireDefines.SORT_CRITERIA_WIRENAME : currentSort %>"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="SortCriteriaAsc" value="true"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="NoSortImage" value="${noSortImage}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="AscendingSortImage" value="${ascSortImage}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="DescendingSortImage" value="${descSortImage}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="TemplateName" value="${SearchTemplateName}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="DebitAccountID" value="${searchAcct}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="BeneficiaryName" value="${SearchBeneficiaryName}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="BeneficiaryAccountNumber" value="${SearchBeneficiaryAccountNumber}"/>
		<ffi:setProperty name="GetWireTemplatesBank" property="BeneficiaryBankRoutingNumber" value="${SearchBeneficiaryRoutingNumber}"/>
	<%--<ffi:process name="GetWireTemplatesBank"/>--%>

<%
} else if (request.getParameter("GetWireTemplatesUser.PreviousPage") != null ||
           request.getParameter("GetWireTemplatesUser.NextPage") != null) { %>
	<%--<ffi:process name="GetWireTemplatesUser"/>--%>
<%
} else if (request.getParameter("GetWireTemplatesBusiness.PreviousPage") != null ||
           request.getParameter("GetWireTemplatesBusiness.NextPage") != null) { %>
	<%--<ffi:process name="GetWireTemplatesBusiness"/>--%>
<%
} else if (request.getParameter("GetWireTemplatesBank.PreviousPage") != null ||
           request.getParameter("GetWireTemplatesBank.NextPage") != null) { %>
	<%--<ffi:process name="GetWireTemplatesBank"/>--%>
<%
} else if (request.getParameter("GetWireTemplatesUser.SortedBy") != null) { %>
	<ffi:setProperty name="GetWireTemplatesUser" property="FirstPage" value="true"/>
	<%--<ffi:process name="GetWireTemplatesUser"/>--%>
<%
} else if (request.getParameter("GetWireTemplatesBusiness.SortedBy") != null) { %>
	<ffi:setProperty name="GetWireTemplatesBusiness" property="FirstPage" value="true"/>
	<%--<ffi:process name="GetWireTemplatesBusiness"/>--%>
<%
} else if (request.getParameter("GetWireTemplatesBank.SortedBy") != null) { %>
	<ffi:setProperty name="GetWireTemplatesBank" property="FirstPage" value="true"/>
	<%--<ffi:process name="GetWireTemplatesBank"/>--%>
<%
} %>
<ffi:cinclude value1="${WireDestinations}" value2="" operator="equals">
	<ffi:object id="WireDestinations" name="com.ffusion.beans.util.StringTable" scope="session" />
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_DOMESTIC %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="Domestic"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_INTERNATIONAL %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="International"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_FED %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="FED Wire"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_DRAWDOWN %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="Drawdown"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_BOOK %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="Book"/>
	<ffi:setProperty name="WireDestinations" property="Key" value="<%= WireDefines.WIRE_HOST %>"/>
	<ffi:setProperty name="WireDestinations" property="Value" value="Host"/>
</ffi:cinclude>

<%
	session.setAttribute("FFIGetWireTemplatesUser", session.getAttribute("GetWireTemplatesUser"));
	session.setAttribute("FFIGetWireTemplatesBusiness", session.getAttribute("GetWireTemplatesBusiness"));
	session.setAttribute("FFIGetWireTemplatesBank", session.getAttribute("GetWireTemplatesBank"));
%>
