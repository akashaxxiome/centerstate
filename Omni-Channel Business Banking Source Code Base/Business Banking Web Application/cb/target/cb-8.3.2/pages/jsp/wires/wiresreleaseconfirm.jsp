<%--
This is the release wires confirmation page.

Pages that request this page
----------------------------
wiresrelease.jsp (wires release page)
	SUBMIT button

Pages this page requests
------------------------
CANCEL requests one of the following:
	wiretransfers.jsp (wire summary page)
	index.jsp (CB home page - in cb root directory)
BACK requests wiresrelease.jsp
CONFIRM/RELEASE WIRES requests wiresreleasesave.jsp

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
payments/inc/nav_menu_top_js.jsp
	Javascript functions used by the top menu
payments/inc/nav_header.jsp
	The top menu shared by all of Payments & Transfers
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:setProperty name='PageHeading' value='Release Wires Confirm'/>
<ffi:setProperty name='PageText' value=''/>





 <div id="releaseWiresConfirmGridDivID">
	<ffi:help id="payments_wiresreleaseconfirm" className="moduleHelpClass"/>
	<ffi:setProperty name="tempURL" value="/pages/jsp/wires/initReleaseWireDashBoard_confirm.action?reload=false" URLEncrypt="true"/>
	<s:url id="releaseWiresConfirmUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
		id="releaseWiresConfirmGridID"
		dataType="json"
		href="%{releaseWiresConfirmUrl}"
		sortable="false"
		pager="true"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}"
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="true"
		shrinkToFit="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		onCompleteTopics="releaseWiresConfirmGridCompleteEvents"
		>
		<sjg:gridColumn name="ID" index="ID" title="%{getText('jsp.wire.ID')}" sortable="false" width="65" hidden="true"/>
		<sjg:gridColumn name="dateToPost" index="date" title="%{getText('jsp.wire.Date')}" sortable="false" width="65"/>
		<sjg:gridColumn name="map.beneficiary" index="beneficiary" title="%{getText('jsp.wire.Beneficiary')}" sortable="false" width="75"/>
		<sjg:gridColumn name="map.accountNickname" index="accountNickname" title="%{getText('jsp.wire.Account_Nickname')}" sortable="false" width="120"/>
		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amount" title="%{getText('jsp.wire.Amount')}" sortable="false" width="55"/>
		<sjg:gridColumn name="statusName" index="status" title="%{getText('jsp.wire.Status')}" sortable="false" width="55"/>
	</sjg:grid>

	<script>
		$('#releaseWiresConfirmGridDivID').portlet({
			title: 'Release Wires Confirm',
			generateDOM: true,
			helpCallback: function(){
				var helpFile = $('#releaseWiresConfirmGridDivID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
		});
	</script>
</div> 

<s:form id="FormNameID" name="FormName" validate="true" action="/pages/jsp/wires/initReleaseWireDashBoard_save.action" method="post" theme="simple">
	<div id="wireReleaseConfirmButtonsDiv" class="marginTop10">
		<s:hidden name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
		<s:url id="releaseWiresConfirmCancelURL" value="/pages/jsp/wires/wires_summary.jsp">
			<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>
		</s:url>
		<sj:a
			id="cancelFromWiresReleaseConfirmLink"
			href="%{releaseWiresConfirmCancelURL}"
			targets="summary"
			indicator="indicator"
			button="true"
			onClickTopics="cancelReleaseWireForm"
			onCompleteTopics="cancelFromWiresReleaseConfirmOnCompleteTopics"
			><s:text name="jsp.default_82" />
		</sj:a>

		<sj:a
			id="backFromWiresReleaseConfirmLink"
			button="true"
			onClickTopics="backFromWiresReleaseConfirmOnCompleteTopics"
			><s:text name="jsp.default_57" />
		</sj:a>
		<%-- <input type="submit" class="submitbutton" value="CONFIRM/RELEASE WIRES"> --%>
		<sj:a
			id="submitWiresReleaseConfirmLink"
			formIds="FormNameID"
			targets="summary"
			button="true"
			validate="false"
			onCompleteTopics="submitWiresReleaseConfirmOnCompleteTopics"
			><s:text name="jsp.default_CONFIRM_RELEASE_WIRES" />
		</sj:a>
	</div>
	<div style="clear:both"></div>
</s:form>

