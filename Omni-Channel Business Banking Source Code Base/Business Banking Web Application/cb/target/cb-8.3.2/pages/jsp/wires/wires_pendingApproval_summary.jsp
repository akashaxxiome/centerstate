<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>

<ffi:help id="payments_wiretransferSubmitedApprovalOrRejectedSummary" className="moduleHelpClass"/>
	<ffi:setGridURL grid="GRID_wireApproval" name="InqueryURL"    url="/cb/pages/jsp/wires/sendFundsTransMessageAction_initWireInquiry.action?FundsID={0}&paymentType={1}&recurringId={2}" parm0="ID" parm1="transType" parm2="RecurringID"/>

	<ffi:setGridURL grid="GRID_wireApproval" name="ViewURL" url="/cb/pages/jsp/wires/viewWireTransferAction.action?ID={0}&transType={1}&recurringId={2}&collectionName=PendingApprovalWireTransfers&ViewRecurringModel={3}" parm0="ID" parm1="transType" parm2="RecurringID" parm3="RecModel"/>
	<ffi:setGridURL grid="GRID_wireApproval" name="ViewURLInternational" url="/cb/pages/jsp/wires/viewWireBatch.action?batchId={0}&collectionName=PendingApprovalWireTransfers&ViewRecurringModel={1}" parm0="ID" parm1="RecModel"/>
	<ffi:setGridURL grid="GRID_wireApproval" name="ViewURLNotInternational" url="/cb/pages/jsp/wires/viewWireBatch.action?batchId={0}&collectionName=PendingApprovalWireTransfers&ViewRecurringModel={1}" parm0="ID" parm1="RecModel"/>

	<ffi:setGridURL grid="GRID_wireApproval" name="EditURL" url="/cb/pages/jsp/wires/modifyWireTransferAction_init.action?ID={0}&transType={1}&recurringId={2}&collectionName=PendingApprovalWireTransfers&isRecModel={3}" parm0="ID" parm1="transType" parm2="RecurringID" parm3="RecModel"/>
	<ffi:setGridURL grid="GRID_wireApproval" name="EditURLHost" url="/cb/pages/jsp/wires/modifyHostWireAction_init.action?ID={0}&collectionName=PendingApprovalWireTransfers&pendApproval=true&isRecModel={1}" parm0="ID" parm1="RecModel"/>
	<ffi:setGridURL grid="GRID_wireApproval" name="EditURLInternational" url="/cb/pages/jsp/wires/modifyWireBatchAction_initedit.action?ID={0}&collectionName=PendingApprovalWireTransfers&DontInitializeBatch=false&isRecModel={1}" parm0="ID" parm1="RecModel"/>
	<ffi:setGridURL grid="GRID_wireApproval" name="EditURLNotInternational" url="/cb/pages/jsp/wires/modifyWireBatchAction_initedit.action?ID={0}&collectionName=PendingApprovalWireTransfers&DontInitializeBatch=false&isRecModel={1}" parm0="ID" parm1="RecModel"/>

	<ffi:setGridURL grid="GRID_wireApproval" name="DeleteURL" url="/cb/pages/jsp/wires/deleteWireTransferAction_init.action?ID={0}&transType={1}&recurringId={2}&collectionName=PendingApprovalWireTransfers&isRecModel={3}" parm0="ID" parm1="transType" parm2="RecurringID" parm3="RecModel"/>
	<ffi:setGridURL grid="GRID_wireApproval" name="SkipURL" url="/cb/pages/jsp/wires/skipWireTransferAction_init.action?isSkip=true&ID={0}&transType={1}&recurringId={2}&collectionName=PendingApprovalWireTransfers" parm0="ID" parm1="transType" parm2="RecurringID" />
	<ffi:setGridURL grid="GRID_wireApproval" name="SkipURLInternational"  url="/cb/pages/jsp/wires/skipWireBatch_init.action?isSkip=true&batchId={0}&collectionName=PendingWireTransfers" parm0="ID" />
	<ffi:setGridURL grid="GRID_wireApproval" name="SkipURLNotInternational" url="/cb/pages/jsp/wires/skipWireBatch_init.action?isSkip=true&batchId={0}&collectionName=PendingWireTransfers" parm0="ID" />

	<ffi:setGridURL grid="GRID_wireApproval" name="DeleteURLInternational" url="/cb/pages/jsp/wires/deleteWireBatch_init.action?batchId={0}&collectionName=PendingApprovalWireTransfers&isRecModel={1}" parm0="ID" parm1="RecModel"/>
	<ffi:setGridURL grid="GRID_wireApproval" name="DeleteURLNotInternational" url="/cb/pages/jsp/wires/deleteWireBatch_init.action?batchId={0}&collectionName=PendingApprovalWireTransfers&isRecModel={1}" parm0="ID" parm1="RecModel"/>

	<ffi:setProperty name="tempURL" value="/pages/jsp/wires/getWireTransfersAction.action?collectionName=PendingApprovalWireTransfers&wiresAccountsName=WiresAccounts&GridURLs=GRID_wireApproval" URLEncrypt="true"/>
    <s:url id="pendingApprovalWiresUrl" value="%{#session.tempURL}" escapeAmp="false"/>
	<sjg:grid
		id="pendingApprovalWiresGridID"
		caption=""
		dataType="json"
		href="%{pendingApprovalWiresUrl}"
		pager="true"
		gridModel="gridModel"
		rowList="%{#session.StdGridRowList}"
		rowNum="%{#session.StdGridRowNum}"
		rownumbers="true"
		shrinkToFit="true"
		navigator="true"
		navigatorAdd="false"
		navigatorDelete="false"
		navigatorEdit="false"
		navigatorRefresh="false"
		navigatorSearch="false"
		navigatorView="false"
		sortable="true"
		scroll="false"
		scrollrows="true"
		viewrecords="true"
		sortname="date"
		sortorder="asc"
		onGridCompleteTopics="addGridControlsEvents,pendingApprovalWiresGridCompleteEvents"
		>
		<sjg:gridColumn name="date" index="date" title="%{getText('jsp.wire.Date')}" sortable="true" width="65"/>
		<sjg:gridColumn name="map.transType" index="transType" title="%{getText('jsp.wire.Transaction')}" sortable="true" width="75"/>
		<sjg:gridColumn name="map.beneficiary" index="beneficiary" title="%{getText('jsp.wire.Beneficiary')}" sortable="true" width="75"/>
		<sjg:gridColumn name="map.accountNickname" index="accountNickname" title="%{getText('jsp.wire.Account_Nickname')}" sortable="true" width="120"/>
		<sjg:gridColumn name="map.destination" index="destination" title="%{getText('jsp.wire.Type')}" sortable="true" width="55"/>
		<sjg:gridColumn name="statusName" index="status" title="%{getText('jsp.wire.Status')}" sortable="true" width="55"/>
		<sjg:gridColumn name="amountValue.currencyStringNoSymbol" index="amount" title="%{getText('jsp.wire.Amount')}" sortable="true" width="55" formatter="ns.wire.formatWireAmount"/>
		<sjg:gridColumn name="transaction.totalOrigAmount" index="amount" title="Amount" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="ID" index="ID" title="Action" sortable="false" width="100" formatter="ns.wire.formatPendingApprovalWiresActionLinks" search="false" hidden="true" hidedlg="true" cssClass="__gridActionColumn"/>
		<sjg:gridColumn name="canEdit" index="canEdit" title="canEdit" sortable="true" width="55" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="canDelete" index="canDelete" title="canDelete" sortable="true" width="55" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="canSkip" index="canSkip" title="canSkip" sortable="true" width="55" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="status" index="status" title="Status" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="rejectReason" index="rejectReason" title="RejectReason" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="resultOfApproval" index="resultOfApproval" title="Submitted For" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverName" index="approverName" title="ApproverName" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverIsGroup" index="approverIsGroup" title="ApproverIsGroup" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="userName" index="userName" title="UserName" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.ViewURL" index="ViewURL" title="ViewURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.InqueryURL" index="InqueryURL" title="InqueryURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.EditURL" index="EditURL" title="EditURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="DeleteURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="map.SkipURL" index="SkipURL" title="SkipURL" search="false" sortable="false" hidden="true" hidedlg="true"/>
		<sjg:gridColumn name="approverInfos" index="approverInfos" title="" hidden="true" hidedlg="true" formatter="ns.wire.formatApproversInfo" />
	</sjg:grid>

	<script>
		$("#pendingApprovalWiresGridID").jqGrid('setColProp','ID',{title:false});
	</script>
