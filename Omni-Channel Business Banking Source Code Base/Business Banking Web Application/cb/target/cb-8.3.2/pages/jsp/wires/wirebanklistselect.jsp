<%--
This page is the wire bank search page which shows you the search results
of the banks found based on the criteria entered in the previous page, which
may be wirebanklist.jsp (if no criteria was specified on the add/edit page),
or the add/edit wire transfer/beneficiary in which a SEARCH button was clicked
(if criteria was entered on the add/edit page)

Pages that request this page
----------------------------
wirebanklist.jsp (bank lookup search criteria)
	SEARCH button
wire_addedit_bank_fields.jsp (Included on the add/edit wire transfer pages and the add/edit wire beneficiary pages.)
	SEARCH button (when criteria has been entered on the page)

Pages this page requests
------------------------
SEARCH AGAIN requests wirebanklist.jsp
CLOSE does not request any page.  It just closes the pop-up window.

Pages included in this page
---------------------------
inc/timeout.jsp
	The meta tag that will redirect to invalidate-session.jsp after a
	predetermined amount of time.
--%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.beans.wiretransfers.WireDefines" %>

<ffi:help id="payments_wirebanklistselect" className="moduleHelpClass"/>
<%-- 
<ffi:getProperty name="${wireTask}" property="WireDestination" assignTo="destination"/>

<% 
if( WireDefines.WIRE_DRAWDOWN.equals( destination ) || WireDefines.WIRE_FED.equals( destination ) || WireDefines.WIRE_BOOK.equals( destination )){
	filteredSwift = "true";
}
%>
<ffi:cinclude value1="<%=filteredSwift %>" value2="true">
	<ffi:setProperty name="WireTransferBanks" property="Filter" value="ROUTING_SWIFT=''" />
</ffi:cinclude>
--%>
<script type="text/javascript">
	function bankListRowsIDClick(urlString){
		ns.wire.searchDestinationBankForm(urlString);
	}
</script>

		<div align="center" class="approvalDialogHt" style="margin-bottom:40px;">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="ltrow2_color tableData tableAlerternateRowColor">
				<tr>
					<td class="columndata" align="center" class="ltrow2_color">
						<form action="wirebanklist.jsp" method="post" name="BankForm" id="researchWireBankListFormID">
                    	<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
						<input type="hidden" name="isBankSearch" value="true"/>
						<input type="hidden" name="Inter" value="<ffi:getProperty name='Inter'/>"/>
						<input type="hidden" name="bankLookupRaturnPage" value="<ffi:getProperty name='bankLookupRaturnPage'/>"/>
						<input type="hidden" id="manageBeneficiary" name="manageBeneficiary" value="<ffi:getProperty name='manageBeneficiary'/>"/>
                    	<input type="hidden" name ="returnPage" id="returnPage" value="<ffi:getProperty name='returnPage'/>"/>
						<table width="100%" border="0" cellspacing="0" cellpadding="3" class="ltrow2_color">
							<!-- <tr>
								<td colspan="2"><span class="sectionhead">&gt; L10NStartBank Lookup ResultsL10NEnd</span></td>
							</tr> -->
							<tr>
		                    	<th align="left"><s:text name="jsp.default_61" /></th>
		                    	<th align="left"><s:text name="jsp.default_35" /></th>
		                    	<th align="left"><s:text name="jsp.default_401" /></th>
		                    	<th align="left"><s:text name="jsp.default_201" /></th>
		                    </tr>
							<% int wtbCount = 0;%>
							<ffi:list collection="WireTransferBanks" items="Bank1">
							<% wtbCount++; %>
							<tr id="bankListRowsID">
								<input id="bank1idID<%= wtbCount%>" type="hidden" value="<ffi:getProperty name='Bank1' property='ID'/>">
								<input id="interID<%= wtbCount%>" type="hidden" value="<ffi:getProperty name='Inter'/>">
								
								<td class="columndata"><a href="javascript:void(0)" class="link anchorText" onclick="bankListRowsIDClick('<ffi:urlEncrypt url='/cb/pages/jsp/wires/getWireTransferBanksAction_selectBank.action?bankId=${Bank1.ID}&Inter=${Inter}&manageBeneficiary=${manageBeneficiary}&bankLookupRaturnPage=${bankLookupRaturnPage}&returnPage=${returnPage}'/>')"><ffi:getProperty name="Bank1" property="BankName"/></a></td>
								<td class="columndata">
                                    					<ffi:cinclude value1="${Bank1.Street}" value2="" operator="notEquals">
                                        					<ffi:getProperty name="Bank1" property="Street"/><br>
                                    					</ffi:cinclude>
                                    					<ffi:cinclude value1="${Bank1.Street2}" value2="" operator="notEquals">
                                        					<ffi:getProperty name="Bank1" property="Street2"/><br>
                                    					</ffi:cinclude>
                                    					<ffi:getProperty name="Bank1" property="City"/>,&nbsp;
                                    					<ffi:cinclude value1="${Bank1.State}" value2="" operator="notEquals" ><ffi:getProperty name="Bank1" property="State" /></ffi:cinclude>
                                    					<ffi:cinclude value1="${Bank1.State}" value2="" operator="equals" ><ffi:getProperty name="Bank1" property="StateCode" /></ffi:cinclude>
                                    					<br>
                                    					<ffi:getProperty name="Bank1" property="Country"/>
								</td>
								<td class="columndata">
									<ffi:cinclude value1="${Bank1.RoutingSwift}" value2="" operator="equals">
                       					N/A
                   					</ffi:cinclude>
                   					<ffi:cinclude value1="${Bank1.RoutingSwift}" value2="" operator="notEquals">
                       					<ffi:getProperty name="Bank1" property="RoutingSwift"/>
                   					</ffi:cinclude>
									
								</td>
								<td class="columndata">
									<ffi:cinclude value1="${Bank1.RoutingFedWire}" value2="" operator="equals">
                       					N/A
                   					</ffi:cinclude>
                   					<ffi:cinclude value1="${Bank1.RoutingFedWire}" value2="" operator="notEquals">
                       					<ffi:getProperty name="Bank1" property="RoutingFedWire"/>
                   					</ffi:cinclude>
								</td>
							</tr>
							</ffi:list>
							<tr class="resetTableRowBGColor">
								<td colspan="4" class="columndata"><br>
								<!--L10NStart-->*If you don't find the bank, please refine your search and try again<!--L10NEnd--></td>
							</tr>
							<tr>
								<td colspan="4" height="30">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" align="center"  class="ui-widget-header customDialogFooter">
								<sj:a 											
									id="researchBankListID"
									button="true" 
									onClickTopics="researchBankListOnClickTopics"
						        	> <s:text name="jsp.default_374"/>
								</sj:a>
								<sj:a id="closeSerachBankListID" 
									button="true" 
									onClickTopics="closeSerachBankListOnClickTopics"
								> <s:text name="jsp.default_102"/>
								</sj:a>
                                </td>
							</tr>
						</table>
						</form>
					</td>
				</tr>
			</table>
		</div>

