<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:help id="custommapping_delete_confirm" />

<s:set var="tmpI18nStr" value="%{getText('jsp.default_133')}" scope="request" /><ffi:setProperty name="PageHeading" value="${tmpI18nStr}"/>
<ffi:setProperty name="PageText" value=""/>
<ffi:setProperty name="BackURL" value="${SecurePath}account/Mapping.jsp"/>

<%
	session.setAttribute("MappingDefinitionID", request.getParameter("MappingDefinitionID"));
%>



<ffi:cinclude value1="${MappingDefinitionID}" value2="" operator="notEquals">
    <ffi:setProperty name="SetMappingDefinition" property="ID" value="${MappingDefinitionID}"/>
    <ffi:removeProperty name="MappingDefinitionID"/>
</ffi:cinclude>
<ffi:process name="SetMappingDefinition"/>


<script type="text/javascript"><!--
function goto(formatControl)
{
	var val = formatControl.options[formatControl.selectedIndex].value;

	if (val == "1")
		document.Mapping.action = "custommapping_fixed.jsp";

	document.Mapping.submit();
}

// --></script>

<%-- include javascripts for top navigation menu bar
Since Payments, Transfers, ACH and Wire all are in the SubDirectory payments, just make that the default. --%>
<% request.setAttribute("SubDirectory", "payments"); %>
<ffi:cinclude value1="${Section}" value2="PPAY">
	<% request.setAttribute("SubDirectory", "cash"); %>
</ffi:cinclude>

		<div align="center">

<ffi:flush/>

			<s:form name="Mapping" id="MappingDeleteFormID" action="/pages/jsp/custommapping_delete.jsp" method="post" theme="simple">
							<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					
					<tr>
						<td align="center" class"sectionhead_grey">
							<table width="100%" border="0" cellspacing="0" cellpadding="3">
								<tr>
									<td colspan="2" align="center" class="sectionhead_grey"><span class="sectionhead"><s:text name="jsp.default_50"/></span></td>
								</tr>
								<tr>
									<td class="columndata" nowrap align="right" width="40%">
									<span class="sectionsubhead labelCls"><s:text name="jsp.default_287.2"/> </span>
									</td>
									<td class="sectionsubhead valueCls">
										<ffi:getProperty name="MappingDefinition" property="Name"/>
									</td>
								</tr>
								<tr>
									<td class="columndata" nowrap align="right" width="40%">
									<span class="sectionsubhead labelCls"><s:text name="jsp.default_171"/> </span>
									</td>
									<td class="sectionsubhead valueCls">
										<ffi:getProperty name="MappingDefinition" property="Description"/>
									</td>
								</tr>
								<tr>
									<td colspan="2" height="50">&nbsp;</td>
								</tr>
								<tr>
								<td colspan="2" align="center" class="ui-widget-header customDialogFooter">
									<sj:a 
									id="cancelFormButton2" 
									button="true" 
									onClickTopics="closeDialog"
								><s:text name="jsp.default_82"/></sj:a>
								<sj:a 
									id="deleteMappingID"
									targets="resultmessage" 
									formIds="MappingDeleteFormID"
									button="true" 
									onCompleteTopics="common.topics.deleteMappingComplete"
								><s:text name="jsp.default_162"/></sj:a>
								</td>
								</tr>
	
							</table>
						</td>
						</td>
					</tr>
			
				</table>
			</s:form>
		</div>
