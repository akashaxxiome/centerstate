<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>

<%-- Remove items from the session that are created and used exclusively by the Account Management tab. --%>
<ffi:removeProperty name="FileUpload"/>
<ffi:removeProperty name="CheckPPayImport"/>
<ffi:removeProperty name="ProcessACHFileUpload"/>
<ffi:removeProperty name="GetReportsByCategoryACH"/>
<ffi:removeProperty name="ReportsACH"/>

<ffi:removeProperty name="Account"/>
<ffi:removeProperty name="Transactions"/>
<ffi:removeProperty name="SearchImage"/>
<ffi:removeProperty name="SendMessage"/>
<ffi:removeProperty name="tmp_url"/>


<ffi:removeProperty name="GetPagedTransactions"/>
<ffi:removeProperty name="AccountDisplayCurrencyCode"/>
<ffi:removeProperty name="DefaultStartDate"/>
<ffi:removeProperty name="DefaultEndDate"/>

<%-- Reomve the dropdown indexes from session when moved from tab --%>
<ffi:removeProperty name="<%= SessionNames.ENTITLEMENTACCOUNTS_INDEX %>" />
<ffi:removeProperty name="<%= SessionNames.ENTITLEMENTACCOUNTS_INDEX_BY_TYPE %>" />
<ffi:removeProperty name="<%= SessionNames.ENTITLEMENTACCOUNTS_INDEX_BY_CURRENCY %>" />