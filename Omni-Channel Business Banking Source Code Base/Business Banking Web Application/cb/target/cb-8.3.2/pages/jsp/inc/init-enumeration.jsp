<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="java.util.Locale" %>
<%@ page import="com.ffusion.tasks.ValidateEnumerationValueTask" %>
<%
	Locale locale = (Locale)session.getAttribute(com.ffusion.tasks.Task.LOCALE);
	if(locale == null) {
		locale = com.ffusion.util.LocaleUtil.getDefaultLocale();
	}
	if(!ValidateEnumerationValueTask.initialized(true,locale)) {
		synchronized(ValidateEnumerationValueTask.class) {
			if(!ValidateEnumerationValueTask.initialized(true,locale)) {
				//begin initialization the enumeration type
				try{
					ValidateEnumerationValueTask.startInit(true,locale);
%>
					<s:include value="init-type-4-user-preference.jsp"></s:include>
<%
				} finally{
					ValidateEnumerationValueTask.endInit(true);
				}
			}
		}
	}

	ValidateEnumerationValueTask validateTask = new ValidateEnumerationValueTask();
	validateTask.setAction(ValidateEnumerationValueTask.ACTION_INIT);
	session.setAttribute("ValidateEnumerationValue",validateTask);
%>
<ffi:process name="ValidateEnumerationValue"/>

<s:include value="init-type-4-user-preference-per-user.jsp"></s:include>

