<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%-- Remove items from the session that are created and used exclusively by the Payments & Transfers tab. --%>

<% System.out.println("xxx cleanup pmtTran"); %>
<%-- remove the Transaction Security flag for Bill Pay Payees --%>
<ffi:removeProperty name="manageBillPayPayees" />

<%-- remove the ACH Batch items
 Need to check if in File Upload - it does a report after, so REPORTING menu is switched
 --%>
<%
    // if we are going to show exceeded limits, don't clean up session
com.ffusion.csil.beans.entitlements.Limits limitsNoApproval =
    ( com.ffusion.csil.beans.entitlements.Limits )session.getAttribute( com.ffusion.approvals.constants.IApprovalsConsts.EXCEEDED_LIMITS_NO_APPROVAL );
if ( limitsNoApproval != null ) {
    %>
<ffi:setProperty name="dontCleanupACHSession" value="true"/>
<%}%>

<ffi:cinclude value1="${dontCleanupACHSession}" value2="" operator="equals">
    <ffi:removeProperty name="ACHBatch,ACHEntries,AddEditACHBatch,MultipleACHBatches,SortImage"/>
    <ffi:removeProperty name="AddEditMultiACHTemplate,ACHMultiBatchTemplates,ACHBatchTemplates"/>
    <ffi:removeProperty name="crdr" startsWith="true"/>
    <ffi:removeProperty name="GetApprovalACHBatches,GetCompletedACHBatches,GetPendingACHBatches"/>
    <ffi:removeProperty name="GetMultipleACHBatchTemplates,GetACHBatchTemplates,FundsTransactionTemplate"/>
    <ffi:removeProperty name="CompletedACHBatches,PendingApprovalACHBatches,PendingACHBatches,ValidateACHBatch"/>
</ffi:cinclude>
<ffi:removeProperty name="dontCleanupACHSession"/>

<%-- Reomve the dropdown indexes from session when moved from tab --%>

<ffi:removeProperty name="<%= SessionNames.TRANSFERACCOUNTS_INDEX %>" />
<ffi:removeProperty name="<%= SessionNames.TRANSFERACCOUNTS_INDEX_BY_TYPE %>" />
<ffi:removeProperty name="<%= SessionNames.TRANSFERACCOUNTS_INDEX_BY_CURRENCY %>" />

<ffi:removeProperty name="<%= SessionNames.BILLPAY_PAYEES_INDEX %>"  />
<ffi:removeProperty name="<%= SessionNames.BILLPAY_ACCOUNTS_INDEX %>"  />
<ffi:removeProperty name="<%= SessionNames.BILLPAY_ACCOUNTS_INDEX_BY_TYPE %>"  />
<ffi:removeProperty name="<%= SessionNames.BILLPAY_ACCOUNTS_INDEX_BY_CURRENCY%>"  />

<ffi:removeProperty name="<%= SessionNames.WIRESACCOUNTS_INDEX %>" />
<ffi:removeProperty name="<%= SessionNames.WIREACCOUNTS_INDEX_BY_CURRENCY %>" />
<ffi:removeProperty name="<%= SessionNames.WIREACCOUNTS_INDEX_BY_TYPE %>" />

<ffi:removeProperty name="<%= SessionNames.STOPS_ACCOUNTS_INDEX %>" />
<ffi:removeProperty name="<%= SessionNames.STOPS_ACCOUNTS_INDEX_BY_TYPE %>" />
<ffi:removeProperty name="<%= SessionNames.STOPS_ACCOUNTS_INDEX_BY_CURRENCY %>" />
