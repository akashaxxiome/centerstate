<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="extend-session-popup.jsp"/>

<script type="text/javascript">

  var timer = 30; //seconds to count down
	<% if (request.getParameter("Timer") != null) { %>
        timer = <%=com.ffusion.util.HTMLUtil.encode(request.getParameter("Timer")) %>;
    <% } else { %>
        <ffi:object name="com.ffusion.tasks.affiliatebank.GetAffiliateBankByID" id="GetAffiliateBankByIDTimer"/>
        <ffi:setProperty name="GetAffiliateBankByIDTimer" property="AffiliateBankID" value="${SecureUser.AffiliateID}"/>
        <ffi:setProperty name="GetAffiliateBankByIDTimer" property="AffiliateBankSessionName" value="AffiliateBankTimer"/>
        <ffi:process name="GetAffiliateBankByIDTimer"/>
        <ffi:removeProperty name="GetAffiliateBankByIDTimer"/>
        timer = <ffi:getProperty name="AffiliateBankTimer" property="TimeoutCountdownSeconds"/>;
        <ffi:removeProperty name="AffiliateBankTimer"/>
    <% } %>

  //display seconds remaining (countdown)  
  ns.common.setTimer = function()
  {
  	try {
  		if (ns.common.timerGoing != 'true') {
  			$.publish('closeDialog');
  		}
  		if (timer == 0){
  			if (ns.common.timerGoing == 'true') {
      			ns.common.invalidateSession();
  			}
  		} else if (timer > 0){
  			document.getElementById('ticker').innerHTML=timer;
  		}
  	} catch (er){
  		$.publish('closeDialog');
  	}
  }
  
  ns.common.countDown = function() {
  	timer--;
    ns.common.setTimer();
  }


  ns.common.invalidateSession = function() {
    ns.common.timerGoing = 'false';
    location.href="<ffi:urlEncrypt url='/cb/pages/jsp/invalidate-session.jsp'/>";
    $.publish('closeDialog');
  }

  ns.common.extendSession = function() {
    ns.common.timerGoing = 'false';
    clearInterval(ns.common.timerIntervalId);
    ns.common.closeDialog('extendsessionpopup');    
  };

</script>


<script>ns.common.timerIntervalId = setInterval("ns.common.countDown()",1000)</script>

<ffi:cinclude value1="${LogoFile}" value2="" operator="notEquals">
    <table width="300" height="40" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td height="40">
                    <img src="<ffi:getProperty name="LogoFile"/>">
            </td>
        </tr>
    </table>
</ffi:cinclude>

<table align="top" width="100%" cellpadding="1" cellspacing="0" border="0">
	<tr class="subaccountmenu3">
		<td colspan="2" align="center"><s:text name="jsp.extendSession.message.part1"/> <span id=ticker>30</span>&nbsp;<s:text name="jsp.extendSession.message.part2"/></td><br>
	</tr>
	<tr>
		<td colspan="2" align="left"><img src="/cb/web/multilang/grafx/spacer.gif" width="1" height="1" border="0" alt=""></td>
	</tr>
  <tr>
    <td align="center" colspan="2">
        <sj:a button="true" onclick="javascript:ns.common.invalidateSession();"><s:text name="jsp.default_269"/></sj:a>&nbsp;&nbsp;
        <sj:a button="true" onclick="javascript:ns.common.extendSession();"><s:text name="jsp.default_389"/></sj:a>
        <%-- <input class="ui-state-default ui-corner-all" type="button" name="Submit3" value="" >
        <input class="ui-state-default ui-corner-all" type="button" name="Submit4" value="<s:text name="jsp.default_389"/>" onclick="javascript:ns.common.extendSession();"> --%>
    </td>
  </tr>
</table>

<script type="text/javascript">
	ns.common.setTimer();
</script>
