<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="java.util.Locale" %>
<%@ page import="com.ffusion.tasks.ValidateEnumerationValueTask" %>
<%
	int max = 0;
	int min = 1;

	


	/*---------------------------------------------------------------------------------------------*/
	ValidateEnumerationValueTask.createType("WideContentOrdinal");
	min = 1;
	max = 7;
	ValidateEnumerationValueTask.addValue("","-");
	for (int i = min; i <= max; i++) {
		ValidateEnumerationValueTask.addValue(Integer.toString(i),Integer.toString(i));
	}

	/*---------------------------------------------------------------------------------------------*/
	ValidateEnumerationValueTask.createType("NarrowContentOrdinal");
	min = 1;
	max = 2;
	ValidateEnumerationValueTask.addValue("","-");
	for (int i = min; i <= max; i++) {
		ValidateEnumerationValueTask.addValue(Integer.toString(i),Integer.toString(i));
	}

	/*---------------------------------------------------------------------------------------------*/
	ValidateEnumerationValueTask.createType("TransactionSearchMaximumMatches");
	ValidateEnumerationValueTask.addValue("100","100");
	ValidateEnumerationValueTask.addValue("200","200");
	ValidateEnumerationValueTask.addValue("250","250");
	ValidateEnumerationValueTask.addValue("300","300");
	ValidateEnumerationValueTask.addValue("400","400");
	ValidateEnumerationValueTask.addValue("500","500");

	/*---------------------------------------------------------------------------------------------*/
	ValidateEnumerationValueTask.createType("NewsBusinessSettings_MaxHeadlines");
	ValidateEnumerationValueTask.addValue("1","jsp.home.newsLink");
	max = 10;
	for (int i = 2; i <= max; i++) {
		ValidateEnumerationValueTask.addValue(Integer.toString(i), "jsp.home.newsLinks");
	}

	/*---------------------------------------------------------------------------------------------*/
	ValidateEnumerationValueTask.createType("NewsFrontpageSettings_MaxHeadlines");
	ValidateEnumerationValueTask.addValue("1", "jsp.home.newsLink");
	max = 10;
	for (int i = 2; i <= max; i++) {
		ValidateEnumerationValueTask.addValue(Integer.toString(i), "jsp.home.newsLinks");
	}

	/*---------------------------------------------------------------------------------------------*/
	ValidateEnumerationValueTask.createType("NewsSportsSettings_MaxHeadlines");
	ValidateEnumerationValueTask.addValue("1", "jsp.home.newsLink");
	max = 10;
	for (int i = 2; i <= max; i++) {
		ValidateEnumerationValueTask.addValue(Integer.toString(i), "jsp.home.newsLinks");
	}

%>
