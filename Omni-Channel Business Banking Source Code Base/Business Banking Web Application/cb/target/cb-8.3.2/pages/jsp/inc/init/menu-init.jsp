<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:setProperty name="menu_init_touched" value="true"/>
<ffi:setProperty name="touchedvar" value="menu_init_touched"/>

<%-- <script language="JavaScript1.2" src="inc\callHelp.js" type="text/javascript"></script>
<script type="text/javascript"><!--
<ffi:include page="${PathExt}inc/callHelp.js"/>
// --></script> --%>
<%-- 
<ffi:setProperty name="SelectedImageTag" value='<IMG STYLE="VERTICAL-ALIGN:middle;" SRC="/cb/web/multilang/grafx/hdr_subnav_selected.gif" WIDTH="8" HEIGHT="21" BORDER="0" alt="">'/>
<ffi:setProperty name="BarImageTag" value='<IMG STYLE="VERTICAL-ALIGN:middle;" SRC="/cb/web/multilang/grafx/hdr_subnav_bar.gif" WIDTH="10" HEIGHT="21" alt="">'/>
--%>
<%--create the top left menu containing the contact, help, and logout links
<ffi:setProperty name="top-left-menu" value=""/>
<ffi:setProperty name="TagToAdd" value='${BarImageTag}'/>
--%>
<%-- 
<ffi:cinclude value1="${User.PRIMARY_ADMIN}" value2="0" operator="notEquals">
    <ffi:setProperty name="openWindow" value="window.open('${SecurePath}contact_my_admin.jsp','ContactMyAdmin','width=775,height=350');"/>
    <ffi:setProperty name="top-left-menu" value='
        <a class="menuSecondaryContainer" onclick="${openWindow}" href="#">
            <span class="menuSecondaryText">Contact My Admin</span>
        </a>'/>
    <%--add bar picture between contact and help links
    <ffi:setProperty name="top-left-menu" value='${top-left-menu}${TagToAdd}'/>
</ffi:cinclude>
--%>
<%--
	String uri = request.getRequestURI();
	String uriRoot = (String)session.getAttribute( "uriRoot" );
	String tempURI = uri.substring( uriRoot.length(), uri.length() - 3 );

	String helpURL = (String)session.getAttribute( "HelpPath" ) + tempURI.replace('/', '_') + "html";
	pageContext.setAttribute( "helpURL", helpURL );

--%>
<%--
<ffi:setProperty name="HelpJSFunction" value="javascript:callHelp('${HelpPath}', '${helpURL}')"/>
<ffi:setProperty name="top-left-menu" value='${top-left-menu}
    <A class="menuSecondaryContainer" HREF="${HelpJSFunction}"><span class="menuSecondaryText">Help</span></A>'/>
add bar picture between help and logout links
<ffi:setProperty name="top-left-menu" value='${top-left-menu}${TagToAdd}'/>

<ffi:cinclude value1="${DISPLAY_SESSION_SUMMARY_REPORT}" value2="YES" operator="equals">
    <ffi:setProperty name="top-left-menu" value='${top-left-menu}
	<a class="menuSecondaryContainer" href="${SecurePath}session-activity.jsp">
	    <span class="menuSecondaryText">Log Out</span>
	</a>'/>
</ffi:cinclude>
<ffi:cinclude value1="${DISPLAY_SESSION_SUMMARY_REPORT}" value2="YES" operator="notEquals">
    <ffi:setProperty name="top-left-menu" value='${top-left-menu}
	<a class="menuSecondaryContainer" href="${SecurePath}invalidate-session.jsp">
	    <span class="menuSecondaryText">Log Out</span>
	</a>'/>
</ffi:cinclude>
--%>

<%-- get the default sub-menu option for the home directory --%>
<ffi:setProperty name="nav_default_home" value="${SecurePath}index.jsp"/>

<%-- Computing entitlement for consolidated balance display --%>
<ffi:setProperty name="displayConsolidatedBalance" value="false"/>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CONSOLIDATED_BALANCE%>" >
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="displayConsolidatedBalance" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
		<ffi:setProperty name="displayConsolidatedBalance" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>
</ffi:cinclude> 
<%-- Computing entitlement for account balance display --%>
<ffi:setProperty name="displayAccountBalance" value="false"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_BALANCE %>" >
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="displayAccountBalance" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
		<ffi:setProperty name="displayAccountBalance" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<%-- Computing entitlement for account history display --%>
<ffi:setProperty name="displayAccountHistory" value="false"/>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.HISTORY %>" >
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
		<ffi:setProperty name="displayAccountHistory" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
		<ffi:setProperty name="displayAccountHistory" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.HISTORY %>" >
		<ffi:setProperty name="displayAccountHistory" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>


<%-- Computing entitlement for transaction display --%>
<ffi:setProperty name="displayTransactionSearch" value="false"/>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.TRANSACTION_SEARCH%>" >
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
		<ffi:setProperty name="displayTransactionSearch" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
		<ffi:setProperty name="displayTransactionSearch" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.HISTORY %>" >
		<ffi:setProperty name="displayTransactionSearch" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>

<%-- Computing entitlement for cash flow summary display --%>
<ffi:setProperty name="displayCashFlowSummary" value="false"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CASH_FLOW %>" >
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="displayCashFlowSummary" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
		<ffi:setProperty name="displayCashFlowSummary" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<%-- Computing entitlement for Disbursements display --%>
<ffi:setProperty name="displayDisbursements" value="false"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.DISBURSEMENTS %>" >
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="displayDisbursements" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
		<ffi:setProperty name="displayDisbursements" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
		<ffi:setProperty name="displayDisbursements" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
		<ffi:setProperty name="displayDisbursements" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<%-- Computing entitlement for Lockbox display --%>
<ffi:setProperty name="displayLockbox" value="false"/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.LOCKBOX %>" >
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_SUMMARY %>">
		<ffi:setProperty name="displayLockbox" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_INTRA_DAY_SUMMARY %>">
		<ffi:setProperty name="displayLockbox" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_PREVIOUS_DAY_DETAIL %>">
		<ffi:setProperty name="displayLockbox" value="true"/>
	</ffi:cinclude>
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.INFO_REPORTING_INTRA_DAY_DETAIL %>">
		<ffi:setProperty name="displayLockbox" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>


<%-- get the default sub-menu option for the account directory--%>
<ffi:setProperty name="nav_default_account" value=""/>
<ffi:cinclude value1="${displayAccountHistory}" value2="true" operator="equals">
	<ffi:cinclude value1="${nav_default_account}" value2="" operator="equals" >
	    <ffi:setProperty name="nav_default_account" value="pages/jsp/account/InitAccountHistoryAction.action?TransactionSearch=false" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${displayConsolidatedBalance}" value2="true" operator="equals">
	<ffi:cinclude value1="${nav_default_account}" value2="" operator="equals" >
	    <ffi:setProperty name="nav_default_account" value="pages/jsp/account/consolidatedbalance_index.jsp"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${displayAccountBalance}" value2="true" operator="equals">
	<ffi:cinclude value1="${nav_default_account}" value2="" operator="equals" >
	    <ffi:setProperty name="nav_default_account" value="pages/jsp/account/accountbalance_index.jsp"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${displayTransactionSearch}" value2="true" operator="equals" >
	<ffi:cinclude value1="${nav_default_account}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_account" value="pages/jsp/account/index.jsp?TransactionSearch=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
	<ffi:cinclude value1="${nav_default_account}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_account" value="pages/jsp/account/accountregister_index.jsp"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FILE_UPLOAD%>" >
	<ffi:cinclude value1="${nav_default_account}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_account" value="pages/jsp/account/file_upload_index.jsp"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:cinclude value1="${nav_default_account}" value2="" operator="equals" >
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.CHECK_IMAGING %>" >
			<ffi:setProperty name="nav_default_account" value="pages/jsp/imagesearch/index.jsp" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:cinclude value1="${nav_default_account}" value2="" operator="equals" >
		<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ONLINE_STATEMENT %>" >
			<ffi:setProperty name="nav_default_account" value="pages/jsp/onlinestatements/onlinestatements_index.jsp" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
</ffi:cinclude>

<s:include value="menu-init2.jsp"></s:include>
<s:include value="menu-init3.jsp"></s:include>
