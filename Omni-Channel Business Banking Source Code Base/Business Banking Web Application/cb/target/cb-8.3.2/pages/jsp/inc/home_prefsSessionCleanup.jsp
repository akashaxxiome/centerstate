<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<ffi:author page="inc/nav/home_prefsSessionCleanup.jsp"/>

<%-- 
	This page is used to remove all objects from session that are used in the "Preferences" sub section
	of EFS.  This page only gets invoked if the user leaves the Pay a Bill Tab and goes to another
	tab in the application.
--%>
<ffi:removeProperty name="verified"/>
