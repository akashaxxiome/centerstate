<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<%-- get the default sub-menu option for the cash directory --%>
<ffi:setProperty name="nav_default_cash" value=""/>
<ffi:cinclude value1="${displayCashFlowSummary}" value2="true" operator="equals">
	<ffi:cinclude value1="${nav_default_cash}" value2="" operator="equals" >
	    <ffi:setProperty name="nav_default_cash" value="pages/jsp/cash/InitCashFlowAction.action?" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.PAY_ENTITLEMENT %>" >
	<ffi:cinclude value1="${nav_default_cash}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_cash" value="pages/jsp/positivepay/initPositivePayAction.action?positivepayReload=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.REVERSE_POSITIVE_PAY_ENTITLEMENT %>" >
	<ffi:cinclude value1="${nav_default_cash}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_cash" value="pages/jsp/reversepositivepay/initReversePositivePayAction.action?reversepositivepayReload=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${displayDisbursements}" value2="true">
	<ffi:cinclude value1="${nav_default_cash}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_cash" value="pages/jsp/cash/cashdisbursement.jsp?positivepayReload=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${displayLockbox}" value2="true">
	<ffi:cinclude value1="${nav_default_cash}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_cash" value="pages/jsp/cash/cashlockbox.jsp?positivepayReload=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>

<s:include value="submenu-pmt-init.jsp"></s:include>
<s:include value="submenu-rpt-init.jsp"></s:include>

<%-- get the default sub-menu option for the user directory --%>
<ffi:setProperty name="nav_default_user" value=""/>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.COMPANY_SUMMARY %>" >
	<ffi:cinclude value1="${nav_default_user}" value2="" operator="equals">
	    <ffi:setProperty name="nav_default_user" value="pages/jsp/user/admin_summaryindex.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.BUSINESS_ADMIN %>" >
	<ffi:cinclude value1="${nav_default_user}" value2="" operator="equals">
	    <ffi:setProperty name="nav_default_user" value="pages/jsp/user/admin_companyindex.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.DIVISION_MANAGEMENT %>" >
	<ffi:cinclude value1="${nav_default_user}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_user" value="pages/jsp/user/admin_divisionsindex.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.GROUP_MANAGEMENT %>" >
	<ffi:cinclude value1="${nav_default_user}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_user" value="pages/jsp/user/admin_groupsindex.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.USER_ADMIN %>" >
	<ffi:cinclude value1="${nav_default_user}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_user" value="pages/jsp/user/admin_usersindex.jsp?Initialize=true" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<%--
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.AUDIT_REPORTING %>" >
<ffi:cinclude value1="${AuditReportsDenied}" value2="false" operator="equals">
	<ffi:cinclude value1="${nav_default_user}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_user" value="xxx"/>
	</ffi:cinclude>
</ffi:cinclude>
</ffi:cinclude>
--%>

<%-- get the default sub-menu option for the approvals directory --%>
<ffi:setProperty name="nav_default_approvals" value=""/>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.APPROVALS_ADMIN %>" >
	<ffi:setProperty name="show_approvalpending_menu" value="true"/>
</ffi:cinclude>

<ffi:cinclude value1="${IsApprover}" value2="true" operator="equals">
	<ffi:setProperty name="show_approvalpending_menu" value="true"/>
</ffi:cinclude>


<ffi:cinclude ifEntitled="<%= EntitlementsDefines.WIRE_BENEFICIARY_MANAGEMENT %>">
	<ffi:setProperty name="show_approvalpending_menu" value="true"/>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.MANAGE_ACH_PARTICIPANTS %>">
	<ffi:setProperty name="show_approvalpending_menu" value="true"/>
</ffi:cinclude>

<ffi:cinclude value1="${show_approvalpending_menu}" value2="true" operator="equals">
		<ffi:cinclude value1="${nav_default_approvals}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_approvals" value="pages/jsp/approvals/pendingApprovalIndex.jsp?Initialize=true&subpage=pending" URLEncrypt="true"/>
		</ffi:cinclude>
</ffi:cinclude>

<ffi:cinclude ifEntitled="<%= EntitlementsDefines.APPROVALS_ADMIN %>" >
	<ffi:cinclude value1="${nav_default_approvals}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_approvals" value="pages/jsp/approvals/approvalWorkflowIndex.jsp?Initialize=true&subpage=workflow" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.APPROVALS_ADMIN %>" >
	<ffi:cinclude value1="${nav_default_approvals}" value2="" operator="equals" >
		<ffi:setProperty name="nav_default_approvals" value="pages/jsp/approvals/pendingApprovalIndex.jsp?Initialize=true&subpage=groups" URLEncrypt="true"/>
	</ffi:cinclude>
</ffi:cinclude>

<%-- get the default sub-menu option for Service Center display --%>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="nav_default_servicecenter" value=""/>

	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SERVICES %>" >
		<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_servicecenter" value="pages/jsp/servicecenter/index.jsp?Initialize=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>

	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FAQ %>" >
		<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_servicecenter" value="pages/jsp/servicecenter/faq.jsp?Initialize=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>

	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SESSION_RECEIPT %>" >
		<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_servicecenter" value="pages/jsp/servicecenter/session-receipt.jsp?Initialize=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
	
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_OMNI_CHANNEL%>" operator="equals">
	<ffi:setProperty name="nav_default_servicecenter" value=""/>

	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SERVICES %>" >
		<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_servicecenter" value="pages/jsp/servicecenter/index.jsp?Initialize=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>

	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FAQ %>" >
		<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_servicecenter" value="pages/jsp/servicecenter/faq.jsp?Initialize=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>

	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SESSION_RECEIPT %>" >
		<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_servicecenter" value="pages/jsp/servicecenter/session-receipt.jsp?Initialize=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
	
</ffi:cinclude>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_SMALLBUSINESS%>" operator="equals">
	<ffi:setProperty name="nav_default_servicecenter" value=""/>

	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SERVICES %>" >
		<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_servicecenter" value="pages/jsp/servicecenter/index.jsp?Initialize=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
	
	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.FAQ %>" >
		<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_servicecenter" value="pages/jsp/servicecenter/faq.jsp?Initialize=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>

	<ffi:cinclude ifEntitled="<%= EntitlementsDefines.SESSION_RECEIPT %>" >
		<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="equals" >
			<ffi:setProperty name="nav_default_servicecenter" value="pages/jsp/servicecenter/session-receipt.jsp?Initialize=true" URLEncrypt="true"/>
		</ffi:cinclude>
	</ffi:cinclude>
	
</ffi:cinclude>
<%-- set flags for the top-level menus --%>
<ffi:setProperty name="nav_showtop_account" value="false"/>
<ffi:cinclude value1="${nav_default_account}" value2="" operator="notEquals" >
	<ffi:setProperty name="nav_showtop_account" value="true"/>
</ffi:cinclude>

<ffi:setProperty name="nav_showtop_cash" value="false"/>
<ffi:cinclude value1="${nav_default_cash}" value2="" operator="notEquals" >
	<ffi:setProperty name="nav_showtop_cash" value="true"/>
</ffi:cinclude>

<ffi:setProperty name="nav_showtop_payments" value="false"/>
<ffi:cinclude value1="${nav_default_payments}" value2="" operator="notEquals" >
	<ffi:setProperty name="nav_showtop_payments" value="true"/>
</ffi:cinclude>

<ffi:setProperty name="nav_showtop_reports" value="false"/>
<ffi:cinclude value1="${nav_default_reports}" value2="" operator="notEquals" >
	<ffi:setProperty name="nav_showtop_reports" value="true"/>
</ffi:cinclude>

<ffi:setProperty name="nav_showtop_user" value="false"/>
<ffi:cinclude value1="${nav_default_user}" value2="" operator="notEquals" >
	<ffi:setProperty name="nav_showtop_user" value="true"/>
</ffi:cinclude>

<ffi:setProperty name="nav_showtop_approvals" value="false"/>
<ffi:cinclude value1="${nav_default_approvals}" value2="" operator="notEquals" >
	<ffi:setProperty name="nav_showtop_approvals" value="true"/>
</ffi:cinclude>

<%-- set flags for the top-level menus --%>
<ffi:setProperty name="nav_showtop_servicecenter" value="false"/>
<ffi:cinclude value1="${nav_default_servicecenter}" value2="" operator="notEquals" >
	<ffi:setProperty name="nav_showtop_servicecenter" value="true"/>
</ffi:cinclude>

