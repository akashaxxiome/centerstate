<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="inc/timeout.jsp"/>
<ffi:setProperty name="timeouttrue" value="540"/>
<ffi:setProperty name="timeoutfalse" value="${User.Timeout}"/>
<ffi:setProperty name="Compare" property="Value1" value="${User.Timeout}" />
<ffi:setProperty name="Compare" property="Value2" value="" />

<script type="text/javascript">
	ns.common.sessionIntervalId = '';
    ns.common.timerGoing = 'false';
    var desiredTimeOut = <ffi:getProperty name="timeout${Compare.Equals}"/>;
    <ffi:object name="com.ffusion.tasks.affiliatebank.GetAffiliateBankByID" id="GetAffiliateBankByIDTimer"/>
    <ffi:setProperty name="GetAffiliateBankByIDTimer" property="AffiliateBankID" value="${SecureUser.AffiliateID}"/>
    <ffi:setProperty name="GetAffiliateBankByIDTimer" property="AffiliateBankSessionName" value="AffiliateBankTimer"/>
    <ffi:process name="GetAffiliateBankByIDTimer"/>
    <ffi:removeProperty name="GetAffiliateBankByIDTimer"/>
    var timer = <ffi:getProperty name="AffiliateBankTimer" property="TimeoutCountdownSeconds"/>;
    if (desiredTimeOut > timer)
        desiredTimeOut = desiredTimeOut - timer;
    if (timer > desiredTimeOut)
    {
        // we need to swap timer and desiredTimeOut because we don't want timer to get reset
        var saved = timer;
        timer = desiredTimeOut;
        desiredTimeOut = saved;
    }
    ns.common.timeoutMS = "" + desiredTimeOut + "000";
    ns.common.timerExpired = function() {
        //If there are active ajax requests going extend the timeout
        if(ns.common.isAjaxRequestActive()){
            ns.common.extendTimeout();
            return;
        }

        if (timer == 0)
        {
            // invalidate session immediately if no timer
            location.href="<ffi:getProperty name="FullPagesPath"/>invalidate-session.jsp?TimedOut=true";
        } else {
            ns.common.timerGoing = 'true';
            $('#extendsessionpopup').dialog('open');
        }
    }

</script>
<script>ns.common.sessionIntervalId = setInterval("ns.common.timerExpired",ns.common.timeoutMS)</script>

	<s:url id="extendsessionpopupurl" value="/pages/jsp/inc/extend-session-popup.jsp" escapeAmp="false">
		<s:param name="Timer" value="%{#session.AffiliateBankTimer.timeoutCountdownSeconds}"></s:param>
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"></s:param>		
	</s:url>
	<sj:dialog id="extendsessionpopup" title="%{getText('jsp.default_200')}" modal="true" resizable="false" 
				autoOpen="false" closeOnEscape="true" showEffect="fold" hideEffect="clip" 
				href="%{extendsessionpopupurl}" width="300">
	</sj:dialog>

<ffi:removeProperty name="AffiliateBankTimer"/>

<script type="text/javascript">
	//This is used to compute the difference of server date and client date so that we can disable dates 
	//in datepicker based on server date. sj:datepicker doesn't support an absolute minDate, but an issue
	//has been raised and accepted for this. 
	//!found no way to set minDate in client side for sj:datepicker, this is pending...
	<% java.util.Date date = new java.util.Date(); %>
	var serverDate = new Date(<%=date.getYear()+1900%>,<%=date.getMonth()%>,<%=date.getDate()%>);
	var clientDate = new Date();
	clientDate.setHours(0);
	clientDate.setMinutes(0);
	clientDate.setSeconds(0);
	clientDate.setMilliseconds(0);
	var dateOffset = (serverDate.getTime() - clientDate.getTime())/(24*60*60*1000);
</script>


