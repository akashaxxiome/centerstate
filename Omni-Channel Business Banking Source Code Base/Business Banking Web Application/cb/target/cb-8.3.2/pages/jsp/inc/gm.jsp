<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<ffi:cinclude value1="${SecureUser.LocaleLanguage}" value2="" operator="equals">
	<%-- The "locale" variable will be set on the login screen, but SecureUser.LocaleLanguage will not! --%>
	<ffi:setProperty name="language" value="${locale}"/>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.LocaleLanguage}" value2="" operator="notEquals">
	<ffi:setProperty name="language" value="${SecureUser.LocaleLanguage}"/>
</ffi:cinclude>


<ffi:cinclude value1="${currentMsgType}" value2="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.MSG_TYPE_LOGIN ) %>" operator="equals" >
<ffi:object id="GetGlobalLoginMessages" name="com.ffusion.tasks.messages.GetGlobalLoginMessages" scope="session"/>
<ffi:setProperty name="GetGlobalLoginMessages" property="BankId" value="${BankId}"/>
<ffi:setProperty name="GetGlobalLoginMessages" property="AffiliateBankId" value="4"/> <%-- HARD-CODED because we do not know the real ID at this point --%>
<s:if test="%{#session.LoginAppType.equals('CB')}">
	<ffi:setProperty name="GetGlobalLoginMessages" property="SuiteType" value="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.SUITE_CB) %>"/>
</s:if>
<s:else>
<ffi:setProperty name="GetGlobalLoginMessages" property="SuiteType" value="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.SUITE_CEFS) %>"/>
</s:else>
<ffi:setProperty name="GetGlobalLoginMessages" property="SearchLanguage" value="${language}"/>
<ffi:process name="GetGlobalLoginMessages" />
<ffi:setProperty name="table_width" value="522"/>
<%-- in login page only occupy one column span --%>
<ffi:setProperty name="span_width" value="1"/>
<ffi:setProperty name="IconWidthPct" value="5"/>
<ffi:setProperty name="MessageWidthPct" value="95"/>
</ffi:cinclude>

<ffi:cinclude value1="${currentMsgType}" value2="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.MSG_TYPE_LOGIN ) %>" operator="notEquals" >
<ffi:object id="SearchGlobalMessages" name="com.ffusion.tasks.messages.SearchGlobalMessages" scope="session"/>
<ffi:object id='SearchMessageInSession' name='com.ffusion.beans.messages.GlobalMessageSearchCriteria' scope='session'/>
<ffi:setProperty name="SearchMessageInSession" property="SearchLanguage" value="${language}"/>
<ffi:setProperty name='SearchMessageInSession' property='BankId' value='${BankId}'/>
<ffi:setProperty name="SearchMessageInSession" property="AddAffiliateBank" value="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.ALL_AFFILIATE_BANKS ) %>" />
<ffi:setProperty name="SearchMessageInSession" property="AddAffiliateBank" value="${SecureUser.AffiliateID}" />

<%-- search for messages that should be shown on this page and this date --%>
<ffi:setProperty name='SearchMessageInSession' property='AddMsgTypes' value='${currentMsgType}'/>
<ffi:setProperty name='GetCurrentDate' property='DateFormat' value='<%= com.ffusion.util.DateFormatUtil.DATE_FORMAT_MMDDYYYY %>' />
<ffi:setProperty name='SearchMessageInSession' property='DisplayDate' value='${GetCurrentDate.Date}' />
<ffi:setProperty name='SearchMessageInSession' property='RecordType' value='<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.RECORD_TYPE_MESSAGE ) %>' />
<ffi:setProperty name='SearchMessageInSession' property='AddStatus' value='<%=String.valueOf(com.ffusion.efs.adapters.profile.constants.ProfileDefines.GLOBAL_MESSAGE_APPROVED)%>'/>

<%-- need to include the toGroup information to ensure that we do not get back "consumer" messages --%>
<ffi:setProperty name="ToGroupType" value="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.ALL_CUSTOMERS ) %>" />
<ffi:setProperty name="SearchMessageInSession" property="AddToGroupPair" value="${ToGroupType},0" />
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="equals">
	<ffi:setProperty name="ToGroupType" value="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.ALL_CONSUMER_CUSTOMERS ) %>" />
	<ffi:setProperty name="SearchMessageInSession" property="AddToGroupPair" value="${ToGroupType},0" />
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_CONSUMERS%>" operator="notEquals">
	<ffi:setProperty name="ToGroupType" value="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.ALL_BUSINESS_CUSTOMERS ) %>" />
	<ffi:setProperty name="SearchMessageInSession" property="AddToGroupPair" value="${ToGroupType},0" />
</ffi:cinclude>
<ffi:setProperty name="ToGroupType" value="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.ALL_SERVICE_PKG ) %>" />
<s:if test="%{#session.LoginAppType.equals('CB')}">
	<ffi:setProperty name="SearchMessageInSession" property="AddToGroupPair" value="${ToGroupType},${Business.ServicesPackageId}" />
</s:if>
<s:else>
	<ffi:setProperty name="SearchMessageInSession" property="AddToGroupPair" value="${ToGroupType},${User.ServicesPackageId}" />
</s:else>

<ffi:setProperty name="SearchGlobalMessages" property="SearchBean" value='SearchMessageInSession' />
<ffi:process name="SearchGlobalMessages" />
<ffi:setProperty name="table_width" value="750"/>
<%-- in other pages only occupy 3 column span --%>
<ffi:setProperty name="span_width" value="3"/>
<ffi:setProperty name="IconWidthPct" value="2"/>
<ffi:setProperty name="MessageWidthPct" value="98"/>
</ffi:cinclude>

<% String msgSize = ""; %>
<ffi:getProperty name="<%= com.ffusion.tasks.messages.Task.GLOBAL_MESSAGES %>" property="Size" assignTo="msgSize"/>
			<ffi:cinclude value1="<%= msgSize %>" value2="0" operator="equals">
				<script type="text/javascript">typeof jQuery != 'undefined'? $("#globalmessage").hide(): '';</script>
			</ffi:cinclude>
			<ffi:cinclude value1="<%= msgSize %>" value2="0" operator="notEquals">
				<% 	int band = 0;
                    String currentMsgType="";
                    String currentMsgResourceId=""; %>
<ffi:cinclude value1="${currentMsgType}" value2="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.MSG_TYPE_LOGIN ) %>" operator="notEquals" >
    <table class="<ffi:getProperty name="BackGround"/>" width="<ffi:getProperty name="table_width"/>" border="0" cellspacing="0" cellpadding="0">
	<!--
	<tr>
	    <td width="11"><img src="/cb/web/multilang/grafx/corner_al.gif" alt="" border="0" width="11" height="7"></td>
	    <td colspan="<ffi:getProperty name='span_width'/>"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0"></td>
	    <td width="11"><img src="/cb/web/multilang/grafx/corner_ar.gif" alt="" border="0" width="11" height="7"></td>
	</tr>
	-->
</ffi:cinclude>
	<tr class="<ffi:getProperty name="BackGround"/>">
	   <!-- <td class="corner_cellbg" width="11"></td> -->
	    <td colspan="<ffi:getProperty name='span_width'/>" >
	    <table width="100%" class="<ffi:getProperty name="BackGround"/>">
	    <ffi:setProperty name="<%= com.ffusion.tasks.messages.Task.GLOBAL_MESSAGES %>" property="SortedBy" value="Priority,REVERSE" />
			<ffi:list collection="<%= com.ffusion.tasks.messages.Task.GLOBAL_MESSAGES %>" items="message">
			<ffi:setProperty name="message" property="CurrentLanguage" value="${language}"/>
			<tr class="<ffi:getProperty name="BackGround"/>">

                <% 
		   String colorInt;
		   String colorStr="black"; //default font color black
		%>
                <ffi:getProperty name="message" property="Color" assignTo="colorInt"/>		
		
		<%
		  	switch( Integer.parseInt( colorInt ) ){ 
				case com.ffusion.beans.messages.GlobalMessageConsts.COLOR_BLACK:
					colorStr = "black";
					break;
				case com.ffusion.beans.messages.GlobalMessageConsts.COLOR_RED:
					colorStr = "red";
					break;
				case com.ffusion.beans.messages.GlobalMessageConsts.COLOR_BLUE:
					colorStr = "blue";
					break;
				case com.ffusion.beans.messages.GlobalMessageConsts.COLOR_GREEN:
					colorStr = "green";
					break;
				case com.ffusion.beans.messages.GlobalMessageConsts.COLOR_YELLOW:
					colorStr = "yellow";
					break;
			} 
		%>
				<%-- <td width='<ffi:getProperty name="IconWidthPct" />%' align="left" class="<ffi:getProperty name="BackGround"/>"><span class="columndata">
					<ffi:cinclude value1="${message.Priority}" value2="<%=String.valueOf(com.ffusion.beans.messages.GlobalMessageConsts.PRIORITY_INFORMATIONAL)%>" operator="equals"><a class='ui-button' style="cursor: default;"><span class='ui-icon ui-icon-help'></span></a></ffi:cinclude>
					<ffi:cinclude value1="${message.Priority}" value2="<%=String.valueOf(com.ffusion.beans.messages.GlobalMessageConsts.PRIORITY_HIGH)%>" operator="equals"><a class='ui-button' style="cursor: default;"><span class='ui-icon ui-icon-notice'></span></a></ffi:cinclude>
					<ffi:cinclude value1="${message.Priority}" value2="<%=String.valueOf(com.ffusion.beans.messages.GlobalMessageConsts.PRIORITY_NORMAL)%>" operator="equals"><a class='ui-button' style="cursor: default;"><span class='ui-icon ui-icon-radio-off'></span></a></ffi:cinclude>
				</span></td> --%>
				<td class="sectionsubhead <ffi:getProperty name="BackGround"/>" width='<ffi:getProperty name="MessageWidthPct" />%' align="left" style="color:<%= colorStr %>"><ffi:getProperty name="message" property="MsgText"/></td>
	    </tr>
			<% band++; %>
			</ffi:list>
	    </table>
	    <!-- <td class="corner_cellbg" width="11"></td> -->
	</tr>
<ffi:cinclude value1="${currentMsgType}" value2="<%= String.valueOf( com.ffusion.beans.messages.GlobalMessageConsts.MSG_TYPE_LOGIN ) %>" operator="notEquals" >
	<!--
	<tr>
	    <td width="11"><img src="/cb/web/multilang/grafx/corner_bl.gif" alt="" border="0" width="11" height="7"></td>
	    <td colspan="<ffi:getProperty name='span_width'/>"><img src="/cb/web/multilang/grafx/spacer.gif" alt="" border="0"></td>
	    <td width="11"><img src="/cb/web/multilang/grafx/corner_br.gif" alt="" border="0" width="11" height="7"></td>
	</tr>
	-->
    </table>
	<br>
	<br>
</ffi:cinclude>
		</ffi:cinclude>
<ffi:removeProperty name="IconWidthPct" />
<ffi:removeProperty name="MessageWidthPct" />
