<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="portal_init_touched" value="true"/>
<ffi:setProperty name="touchedvar" value="portal_init_touched"/>

<%-- 
<ffi:object name="com.ffusion.tasks.portal.ContentProviderInitService" id="PortalContentProviderInitService" scope="session"/>
	<ffi:setProperty name="PortalContentProviderInitService" property="ClassName" value="com.ffusion.services.portal.NewsAlertProvider"/>
<ffi:process name="PortalContentProviderInitService"/>
--%>
<%-- 
<ffi:object id="PortalInit" name="com.ffusion.tasks.portal.PortalInit" scope="session"/>
<ffi:process name="PortalInit" />
<ffi:removeProperty name="PortalInit"/>
--%>
<%-- Reset the country/state selection to "none". --%>
<ffi:setProperty name="ForecastStatesContext" property="Selected" value="UN"/>
<ffi:setProperty name="ForecastCountriesContext" property="Selected" value="UN"/>

<%--
<ffi:object name="com.ffusion.tasks.portal.PortalRefresh" id="PortalRefresh" scope="session"/>
	<ffi:setProperty name="PortalRefresh" property="SuccessURL" value="${SecurePath}PortalRefreshAll"/> 

<ffi:object name="com.ffusion.tasks.portal.PortalRefreshAll" id="PortalRefreshAll" scope="session"/>--%>
