<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%@ page import="java.util.Enumeration" %>

<%-- 	This scriptlet will iterate through every object in session and clean those having following prefixes. --%>
<%
	Enumeration enumCleanup = session.getAttributeNames();
	while( enumCleanup.hasMoreElements() ) {
		String name = (String)(enumCleanup.nextElement());

		if( name.startsWith("transaction_limit") ||
			name.startsWith("day_limit") ||
			name.startsWith("week_limit") ||
			name.startsWith("month_limit") ||
			name.startsWith("transaction_exceed") ||
			name.startsWith("day_exceed") ||
			name.startsWith("week_exceed") ||
			name.startsWith("month_exceed") ||
			name.startsWith("perBatchDebit") ||
			name.startsWith("perBatchCredit") ||
			name.startsWith("dailyDebit") ||
			name.startsWith("dailyCredit") ||
			name.startsWith("approveDaily") ||
			name.startsWith("approvePerBatch") ||
			name.startsWith("limit_error"))
		{
			session.removeAttribute(name);
		} else if (	name.startsWith("admin") )
		{
			try {
				if (name.length() > 5) {
					Integer.parseInt(name.substring(5));
					session.removeAttribute(name);
				}
			} catch (NumberFormatException nfe) {}
		} else if (	name.startsWith("account_group") )
		{
			try {
				if (name.length() > 13) {
					Integer.parseInt(name.substring(13));
					session.removeAttribute(name);
				}
			} catch (NumberFormatException nfe) {}
		} else if (	name.startsWith("account") )
		{
			try {
				if (name.length() > 7) {
					Integer.parseInt(name.substring(7));
					session.removeAttribute(name);
				}
			} catch (NumberFormatException nfe) {}
		} else if (	name.startsWith("entitlement") )
		{
			try {
				if (name.length() > 11) {
					Integer.parseInt(name.substring(11));
					session.removeAttribute(name);
				}
			} catch (NumberFormatException nfe) {}
		} else if (	name.startsWith("WireTemplateNumber") )
		{
			try {
				if (name.length() > 18) {
					Integer.parseInt(name.substring(18));
					session.removeAttribute(name);
				}
			} catch (NumberFormatException nfe) {}
		}
	}
%>