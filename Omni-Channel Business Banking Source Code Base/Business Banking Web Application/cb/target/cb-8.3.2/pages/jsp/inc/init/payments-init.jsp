<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<ffi:setProperty name="payments_init_touched" value="true"/>
<ffi:setProperty name="touchedvar" value="payments_init_touched"/>

<ffi:object id="BillpaySignOn" name="com.ffusion.tasks.billpay.SignOn" scope="session"/>
    <ffi:setProperty name="BillpaySignOn" property="UserName" value="${Business.BusinessName}" />
    <ffi:setProperty name="BillpaySignOn" property="Password" value="${Business.BusinessCIF}" />
<ffi:process name="BillpaySignOn" />

<ffi:cinclude value1="${PaymentFrequencies}" value2="" operator="equals">
	<ffi:object id="PaymentFrequencies" name="com.ffusion.tasks.util.ResourceList" scope="session"/>
		<ffi:setProperty name="PaymentFrequencies" property="ResourceFilename" value="com.ffusion.beansresources.billpay.resources" />
		<ffi:setProperty name="PaymentFrequencies" property="ResourceID" value="RecPaymentFrequencies" />
	<ffi:process name="PaymentFrequencies" />
	<ffi:getProperty name="PaymentFrequencies" property="Resource"/>
</ffi:cinclude>

<ffi:object id="PaymentFreqResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="PaymentFreqResource" property="ResourceFilename" value="com.ffusion.beansresources.billpay.resources" />
<ffi:process name="PaymentFreqResource" />

<ffi:setProperty name="BankingAccounts" property="Filter" value="All" />

<ffi:object id="GetBillPayAccounts" name="com.ffusion.tasks.billpay.GetAccounts" scope="session"/>
	<ffi:setProperty name="GetBillPayAccounts" property="UseAccounts" value="BankingAccounts" />
<ffi:process name="GetBillPayAccounts" />

<ffi:object id="GetPayees" name="com.ffusion.tasks.billpay.GetExtPayees" scope="session"/>
<ffi:process name="GetPayees" />

<ffi:object id="SetPayment" name="com.ffusion.tasks.billpay.SetPayment" scope="session"/>

<ffi:object id="SetRecPayment" name="com.ffusion.tasks.billpay.SetRecPayment" scope="session"/>

<ffi:cinclude value1="${StateList}" value2="">
	<ffi:object id="StateList" name="com.ffusion.tasks.util.ResourceList" scope="session"/>
		<ffi:setProperty name="StateList" property="ResourceFilename" value="com.ffusion.utilresources.states" />
		<ffi:setProperty name="StateList" property="ResourceID" value="StateList" />
	<ffi:process name="StateList"/>
	<ffi:getProperty name="StateList" property="Resource"/>
</ffi:cinclude>

<ffi:cinclude value1="${CountryList}" value2="">
	<ffi:object name="com.ffusion.tasks.util.GetCountryList" id="GetCountryList" scope="session"/>
	<ffi:setProperty name="GetCountryList" property="CollectionSessionName" value="CountryList"/>
	<ffi:process name="GetCountryList"/>
	<ffi:removeProperty name="GetCountryList"/>
</ffi:cinclude>

<ffi:object id="CountryResource" name="com.ffusion.tasks.util.Resource" scope="session"/>
	<ffi:setProperty name="CountryResource" property="ResourceFilename" value="com.ffusion.utilresources.states" />
<ffi:process name="CountryResource" />

<%-- ------------------------ register integration ----------------------- --%>
<ffi:cinclude ifEntitled="<%= EntitlementsDefines.ACCOUNT_REGISTER%>" >
	<ffi:object id="SetRegisterAccountsData" name="com.ffusion.tasks.register.SetRegisterAccountsData"/>
	<ffi:process name="SetRegisterAccountsData"/>

	<ffi:object name="com.ffusion.tasks.register.TransferDefaultCategories" id="TransferDefaultCategories" scope="session"/>
	<ffi:process name="TransferDefaultCategories"/>

	<ffi:object name="com.ffusion.tasks.register.GetRegisterCategories" id="GetRegisterCategories" scope="session"/>
    <ffi:process name="GetRegisterCategories"/>
	<% 
	   session.setAttribute("FFIGetRegisterCategories", session.getAttribute("GetRegisterCategories"));
	%>
</ffi:cinclude>
<%-- ------------------------ register integration ----------------------- --%>