<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.*"%>
<%@ page import="com.ffusion.beans.reporting.Report"%>
<%@ page import="com.ffusion.beans.reporting.ReportCriteria"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<ffi:help id="account_saveSearch" />
<%-- Prepare the SaveReport --%>
<ffi:object name="com.ffusion.beans.reporting.Report" id="ReportData" scope="session" />

<%-- Initialize the Report for Saving --%>
<%
	Properties reportOptions = new Properties();
	reportOptions.setProperty( ReportCriteria.OPT_RPT_TYPE, com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY);
	reportOptions.setProperty( ReportCriteria.OPT_RPT_CATEGORY, com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY);
	Report reportData = (Report) session.getAttribute( "ReportData" );
	ReportCriteria rptCriteria = new ReportCriteria();
 	rptCriteria.setReportOptions( reportOptions );
 	reportData.setReportCriteria( rptCriteria );
 	session.setAttribute( "ReportData", reportData );
%>

<%-- <s:include value="/pages/jsp/account/inc/saveSearchInit.jsp"/> --%>
<s:include value="/pages/jsp/account/inc/save-search-init.jsp"/>

<div align="center">
<s:if test="%{#parameters.id!='' && #parameters.id!='undefined'}">
<s:set var="tempUrl" value="'/pages/jsp/account/accounthistory_dashboard.jsp?editSavedSearch=true&TransactionSearch=true'"/>
</s:if>
<s:else>
<s:set var="tempUrl" value="'AddTransactionSearchAction'"/>
</s:else>
	<s:form action="AddTransactionSearchAction" method="post" name="addTransactionSearchForm" namespace="/pages/jsp/account" id="addTransactionSearchForm" validate="false" theme="simple">
              <input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
		<table width="100%" border="0" cellspacing="3" cellpadding="3">
			<tr>
				<td class="tbrd_b" align="left" colspan="2" nowrap><span class="sectionhead"><s:text name="jsp.save_search"/></span></td>
			</tr>
			<tr>
				<td colspan="2">
				        <!-- A List for Global Error Messages -->
				    <ul id="formerrors" class="errorMessage"></ul>
				</td>
			</tr>
			<tr>
				<td align="right" class="sectionsubhead" width="35%"><s:text name="jsp.default_283.1"/>&nbsp;</td>
				<td align="left">
			     <!-- required="true" -->	<sj:textfield
						id="name"
						name="name"
						label="Name"
						onchange="updateName()"
						value="%{#parameters.name}"
						
					/><span id="nameError"></span>
				</td>
			</tr>
			<tr>
				<td align="right" class="sectionsubhead"><s:text name="jsp.default_170"/>&nbsp;</td>
				<td align="left">
			     	<sj:textfield
						id="description"
						name="description"
						label="Description"
						onchange="updateDescription()"
						value="%{#parameters.description}"
					/><span id="descriptionError"></span>
				</td>
			</tr>
			<tr>
				<td height="60" colspan="2" align="center" valign="top">
					<div class="hidden" id="editSavedSearch">
		 				Overwriting Saved Search&nbsp;"<span id="duplicateLayoutName">
		 				<s:property value="%{#parameters.name}"/></span>"
					</div>
				</td>
			</tr>
			<tr>
				<td align="center" nowrap colspan="2" class="ui-widget-header customDialogFooter">
					
					<sj:a button="true" onClickTopics="closeDialog"><s:text name="jsp.default_82"/></sj:a>

					<sj:a
						id="triggerAddOrUpdate"
						button="true"
						onclick="triggerAddOrUpdate()"
						onSuccessTopics="closeDialogSave"
						><s:text name="jsp.default_366"/></sj:a>
						
					<sj:a
						id="deleteSavedSearch"
						button="true"
						cssStyle="display:none"
						onclick="triggerDelete()"
						onSuccessTopics="closeDialogSave"
						>
						Delete
						</sj:a>
						
					<sj:a
						id="addTransactionSearchFormSubmit"
						cssStyle="display:none"
						formIds="addTransactionSearchForm"
						targets="resultmessage"
						button="true"
						buttonIcon="ui-icon-check"
						validate="true"
						validateFunction="customValidation"
						onBeforeTopics="beforeSaveSearchCriteria"
						onSuccessTopics="successSaveSearchCriteria"
						onErrorTopics="errorSaveSearchCriteria"
						onCompleteTopics="completeSaveSearchCriteria"><s:text name="jsp.default_366"/></sj:a>
                </td>
			</tr>
		</table>
		
		<input type="hidden" name="id" value="<s:property value="%{#parameters.id}"/>"/>
		
	</s:form>

</div>

<script type="text/javascript">
	function updateDescription(){
		var description = $('#description').val();
		$('#accountCriteriaForm [name="description"]').val(description);
	}
	
	function updateName(){
		var name = $('#name').val();
		$('#accountCriteriaForm [name="name"]').val(name);
	}

	function triggerAddOrUpdate(){
		var updatedName = $.trim($('#addTransactionSearchForm [name="name"]').val());
		var updateModifyFormName = $.trim($('#accountCriteriaForm [name="oldReportName"]').val());
		var isUpdateFlag ='<s:property value="%{#parameters.id}"/>';
		
		if(isUpdateFlag != '') {
			$('#modifySearchID').trigger('click');
		} else {
			if(updatedName == ''){
				$('#addTransactionSearchFormSubmit').trigger('click');
			}
			else if(updatedName != updateModifyFormName){
				$('#addTransactionSearchFormSubmit').trigger('click');
			}else{
				$('#modifySearchID').trigger('click');
			}
		}
		$('#saveSearchDialog').dialog('close');
		
	}
	
	function triggerDelete(){
		var reportID = '<s:property value="%{#parameters.id}"/>';
		var reportName = '<s:property value="%{#parameters.name}"/>';
		var description = '<s:property value="%{#parameters.description}"/>';
		var urlString  ="/cb/pages/jsp/account/savedSearchConfirmDelete.jsp?reportID="+reportID+"&reportName="+reportName+"&reportDesc="+description;
 		ns.account.deleteSearch(urlString);
	}
	
	$(document).ready(function(){
		var t ='<s:property value="%{#parameters.id}"/>';
		if(t!=''){
			$('#editSavedSearch').show();	
			$('#deleteSavedSearch').show();
			$('#triggerAddOrUpdate span').html('Update');
		}
	});	
	
</script>