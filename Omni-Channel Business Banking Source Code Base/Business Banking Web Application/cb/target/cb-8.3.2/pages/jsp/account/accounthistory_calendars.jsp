<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ taglib prefix="ffi" uri="/WEB-INF/ffi.tld" %>
<%@ page import="com.ffusion.util.DateConsts"%>	
<ffi:object id="GetCurrentDate" name="com.ffusion.tasks.util.GetCurrentDate" scope="page"/>
<ffi:process name="GetCurrentDate"/>			
<%-- Format and display the current date/time --%>
<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="<%= DateConsts.FULL_CALENDAR_DATE_FORMAT %>"/>
<s:set name="AccountGroupID" value="accountHistorySearchCriteria.Account.AccountGroup"/>	
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar-extra.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.css' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.print.css' media='print' />
	<link rel='stylesheet' type='text/css' href='/cb/web/css/calendar/fullcalendar.overrides.css' />
	<div class="marginTop10 ieDisplayFlex" align="center" style="display: inline-flex;">
	
	<s:if test="%{#session.SecureUser.AppType =='Business'}">
		<div id="account" class="marginRight10">
			<select class="ui-widget-content ui-corner-all textbox" name="accountGroup" id="accountGroupIDCalendar" onChange="reloadCalendarAccountType();">
				<option value="1"<s:if test="%{#AccountGroupID == 1}">selected</s:if>><s:text name="jsp.account_76" /></option>
				<option value="2" <s:if test="%{#AccountGroupID == 2}">selected</s:if>><s:text name="jsp.account_30" /></option>
				<option value="4" <s:if test="%{#AccountGroupID == 4}">selected</s:if>><s:text name="jsp.account_58" /></option>
				<option value="3" <s:if test="%{#AccountGroupID == 3}">selected</s:if>><s:text name="jsp.account_116" /></option>
				<option value="5" <s:if test="%{#AccountGroupID == 5}">selected</s:if>><s:text name="jsp.account_155" /></option>
			</select>
		</div>
	</s:if>
	
	<s:form id="accHistoryCalendarForm">
		<div id="accountId">
		<s:if test="%{#session.SecureUser.AppType =='Business'}">
				<select class="ui-widget-content ui-corner-all" name="accountHistorySearchCriteria.selectedAccountOption" id="corporateAccountIDCalendar"
				onChange="reloadCalendarForAccount();">
					<ffi:list collection="accountsList" items="AccountList" startIndex="1" endIndex="1">
						<ffi:setProperty name="AccountDisplayText" value="${AccountList.AccountDisplayText}" />
						<ffi:setProperty name="selectAccountValue" value="${AccountList.ID},${AccountList.BankID},${AccountList.RoutingNum},${AccountList.AccountGroup},${TransactionSearch}" />
					</ffi:list>
					<option  value="<ffi:getProperty name='selectAccountValue'/>" ><ffi:getProperty name="AccountDisplayText"/> </option>
				</select> 
				
		</s:if>
			
			<s:if test="%{#session.SecureUser.AppType !='Business'}">
			<div id="consumerOnly" style="<ffi:getProperty name="consumerFieldStyle" />" class="floatleft">
				<table border="0" cellspacing="1" cellpadding="1">
					<tr>
						<td>
							<table border="0" cellspacing="1" cellpadding="1">
								<tr>
										<% String strURLConsumerValue = "";%>
										<ffi:getProperty name="URL1" assignTo="strURLConsumerValue" />
										<td width="85%" valign="top">
											<span class="sectionsubhead"><s:text name="jsp.default_21" /></span>
											<select class="ui-widget-content ui-corner-all" name="accountHistorySearchCriteria.selectedAccountOption"
												id="consumerAccountsDropDownCalendar"
												onChange="ns.account.reloadAccount(),ns.common.adjustSelectMenuWidth('consumerAccountsDropDownCalendar'),reloadCalendarForAccount();">
												<ffi:list collection="accountsList" items="AccountList1">
													<ffi:cinclude value1="1" value2="${AccountList1.CoreAccount}" operator="equals">
														<ffi:setProperty name="AccountDisplayText1" 
															value="${AccountList1.AccountDisplayText}" />
														<ffi:setProperty name="selectAccountValue" value="${AccountList1.ID},${AccountList1.BankID},${AccountList1.RoutingNum},${AccountList1.AccountGroup},${TransactionSearch}" />
														<option  value="<ffi:getProperty name='selectAccountValue' />" ><ffi:getProperty name="AccountDisplayText1"/> </option>
													</ffi:cinclude>
												</ffi:list>
												<%-- <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,AND" /> --%>
												<ffi:cinclude value1="0" value2="${accountsList.Size}" operator="notEquals">
													<% int count = 0; %>
													<ffi:list collection="accountsList" items="AccountList1">
														<ffi:cinclude value1="0" value2="${AccountList1.CoreAccount}" operator="equals">
														<% if (count < 1){ %>
																<option disabled="disabled" value="separator"><ffi:getProperty name="separator" /></option>
															<% }
															count++;
															%>
															<ffi:setProperty name="selectAccountValue" value="${AccountList1.ID},${AccountList1.BankID},${AccountList1.RoutingNum},${AccountList1.AccountGroup},${TransactionSearch}" />
															<option  value="<ffi:getProperty name='selectAccountValue' />" ><ffi:getProperty name="AccountList1" property="ConsumerMenuDisplayText" /> </option>
														</ffi:cinclude>
													</ffi:list>
													<% count = 0; %>
												</ffi:cinclude>
												<%-- <ffi:setProperty name="BankingAccounts" property="Filter" value="HIDE=0,COREACCOUNT=0,COREACCOUNT=1" /> --%>
											</select>
										</td>
								</tr>
								<span id="selectedAccountOptionError"></span>
							</table>
						</td>
					</tr>
				</table>
			</div> 
		</s:if>
		</div>
		<s:hidden name="accountHistorySearchCriteria.account.ID" value="" />
		<s:hidden name="accountHistorySearchCriteria.account.bankID" value="" />
		<s:hidden name="accountHistorySearchCriteria.account.routingNum" value="" />
		</s:form>
	</div>
	
	<ffi:setGridURL grid="GRID_ACCOUNTSCALENDAR_EVENT_URL" name="ViewURL" url="/cb/pages/jsp/account/transactiondetail.jsp?collectionName=Transactions&tranIndex={0}" parm0="tranId"/>
	<%-- <ffi:setGridURL grid="GRID_ACCOUNTSCALENDAR_EVENT_URL" name="ViewURL" url="/cb/pages/jsp/account/transactiondetail.jsp?collectionName=Transactions&tranIndex={0}" parm0="tranId"/>
	<ffi:setGridURL grid="GRID_ACCOUNTSCALENDAR_EVENT_URL" name="ViewURL" url="/cb/pages/jsp/account/transactiondetail.jsp?collectionName=Transactions&tranIndex={0}" parm0="tranId"/>
	<ffi:setGridURL grid="GRID_ACCOUNTSCALENDAR_EVENT_URL" name="ViewURL" url="/cb/pages/jsp/account/transactiondetail.jsp?collectionName=Transactions&tranIndex={0}" parm0="tranId"/> --%>
	 <!-- <script type='text/javascript' src='/cb/web/js/calendar/fullcalendar.js'></script> -->
	<style type="text/css">
		.fc td.fc-sat{
			width: 122px;
			}
	</style>
	<script type='text/javascript'>
	
	function reloadCalendarAccountType(){
		var accountGrpIDCalendar = $("#accountGroupIDCalendar").val() || "";
		$("#corporateAccountIDCalendar").lookupbox("option","source","/cb/pages/jsp/account/AccountHistoryLookupBoxAction.action?accountGroupId="+accountGrpIDCalendar);
		$("#corporateAccountIDCalendar").lookupbox("refresh");
		$('#accHistoryCalendar').fullCalendar('refetchEvents');
		
	}
	
	function reloadCalendarForAccount(){
		ns.common.populateAccountDetails();
		$('#accHistoryCalendar').fullCalendar('refetchEvents');
		
	}

	$("#accountGroupIDCalendar").selectmenu({
		width : 130
	});
	
	var accountGrpID = $("#accountGroupIDCalendar").val() || "";
	
	 $("#corporateAccountIDCalendar").lookupbox({
		"source":"/cb/pages/jsp/account/AccountHistoryLookupBoxAction.action?accountGroupId="+accountGrpID,
		"controlType":"server",
		"collectionKey":"1",
		"size":"35",
		"module":"accounthistory",
		"dataReceived":function(){
            $('#corporateAccountIDCalendar option:eq(1)').selected();
            var optLabel= $('#corporateAccountIDCalendar option:selected').text();
            var optValue= $('#corporateAccountIDCalendar option:selected').val();
            var optionValue = {value:optValue, label:optLabel};
            $('#corporateAccountIDCalendar').lookupbox("value",optionValue);
			var optValueTest= $('#corporateAccountIDCalendar').val();
			if(optValueTest < 0) 
			{
				$('#accHistoryCalendar').find('.fc-event-title').html("");
				return true;
			}
            
		}
	}); 
	 
		$(document).ready(function() {
			
		$("#consumerAccountsDropDownCalendar").selectmenu({
			width : 350
		});
			
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		var dateArray=[];
		var testJson =function(){
			return{
				'accountHistorySearchCriteria.selectedAccountOption' :$("#corporateAccountIDCalendar").val() || $('#consumerAccountsDropDownCalendar').val(),
				'accountHistorySearchCriteria.account.ID' :$('input[name="accountHistorySearchCriteria.account.ID"]').val(),
				'accountHistorySearchCriteria.account.bankID' :$('input[name="accountHistorySearchCriteria.account.bankID"]').val(),
				'accountHistorySearchCriteria.account.routingNum' :$('input[name="accountHistorySearchCriteria.account.routingNum"]').val()
			}
		};
		$('#accHistoryCalendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},			
			editable: true,
			disableDragging:true,
			events:	{
				url: '<ffi:urlEncrypt url="/cb/pages/jsp/account/getCalendarEvents.action?moduleName=AccHistoryCalendar&GridURLs=GRID_ACCOUNTSCALENDAR_EVENT_URL"/>',
		        		   data: testJson
			},
			eventClick: function(calEvent, jsEvent, view){
				if(view.name == "month"){
					moveToDay(calEvent.start);	
				}else if(view.name == "basicDay"){
					var temp = $(this);
					var isLoaded = false;						
					for(var i =0;i<$.parseHTML(this.innerHTML).length;i++){
						if($.parseHTML(this.innerHTML)[i].localName =="div"
								&& $.parseHTML(this.innerHTML)[i].className.indexOf("calendarData")!="-1"){
							isLoaded = true;
							break;
						}
					}
					if(!isLoaded){
						clearData();
						$.ajax({
						   url: "/cb/pages/jsp/account/transactiondetail.jsp?collectionName=Transactions",
						   global : false,
						   data:{
							   tranIndex:calEvent.tranId,
							   isCalendar:true
						   },
						   success: function(data){
								   temp.append(data);
								   var lHeight = $(temp).parent().height();
								   $("div .fc-content").height(lHeight+50);
							},
						});
					}else{
					}
				}else{
					moveToDay(calEvent.start);	
				}
			},
					
			eventRender: function(event, element) {
				//removing css class for events o
				element.removeClass("fc-event fc-event-skin");
				element.find("div").removeClass("fc-event fc-event-skin");
				
				var view  =  $('#accHistoryCalendar').fullCalendar('getView');
				if(view.name == "month"){				
					var y = event.start;
					var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
					var cal = $("#accHistoryCalendar")[0];
					var data = $.data(cal,d.toString());
					if(dateArray.indexOf(d)=='-1'){
						dateArray.push(d);
						var holderDiv = "<div>";
						var text = '';
						for (var key in event.calendarDateData) {
							  if (event.calendarDateData.hasOwnProperty(key)) {
								 /*  text = text + holderDiv;
								  text= text+" " +event.calendarDateData[key] + " "+ key; */
								  if(event.calendarDateData[key] > 0) {
									  holderDiv = "<div class='calendarDayTaskLblHolderCls calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
									  text = text + holderDiv;
									  text = text+" " +event.calendarDateData[key] + " "+ jQuery.i18n.prop("accounthistory_calendar_complete");
									  text += "</div>";
								  }
							  }
						}
						var optValueTest= $('#corporateAccountIDCalendar').val();
						if(optValueTest < 0) 
							element.find('.fc-event-title').html("");
						else
							element.find('.fc-event-title').html(text);
						$.data(cal,d.toString(),data);
						}
					else{
						return false;
						
					}
				}
				if(view.name == "basicWeek") {
					text = "";
					for (var key in event.calendarDateData) {
						  if (event.calendarDateData.hasOwnProperty(key)) {
							  if(event.calendarDateData[key] > 0) {
								  holderDiv = "<div class='calendarWeekTaskLblHolderCls weekViewCompletedApprovalRecordCls sapUiIconCls icon-accept'>";
								  text = text + holderDiv;
								  text = text+" " +this.title+ "<br>Debit Amount "+ this.debitAmount+ "<br>Credit Amount "+ this.creditAmount; 
								  text += "</div>";
							  }	  
						  }
					}
					$(".completedevent").css("position", "absolute");
					element.find('.fc-event-title').html(text);	
				}
				if(view.name == "basicDay"){
					text = "";
					var id = event.tranId;
					statusName = "<div class='marginRight20 floatleft'>Completed<span class='marginleft5 calendarCompletedApprovalRecordCls sapUiIconCls icon-accept'></span></div>";
					var referenceNumber = "<div class='marginRight20 floatleft'>"+"<span class='boldIt' id='"+ id  +"'>"+this.title+"<span class='marginleft10 sapUiIconCls hidden icon-arrow-right'></span></span></div>";
					var debitAmount = "<div class='marginRight20 floatleft'> Debit Amount : <span class='boldIt'> "+this.debitAmount+"</span></div>";
					var creditAmount = "<div class='marginRight20 floatleft'> Credit Amount : <span class='boldIt'> "+this.creditAmount +"</span></div>";
					
					var amount="";
					if(this.debitAmount!=null){
						amount=debitAmount;
					}
					if(this.creditAmount!=null){
						amount+=creditAmount;
					}
					var newTitle = "<span class='heading'>"+statusName+referenceNumber+amount+"</span>";
					
					/* $(".completedevent").css("position", "relative"); */
					$(".completedevent").addClass("completedeventDayView");
					$("div[class*='fc-view-basicDay'] div .fc-event-hori").addClass("completedeventDayView");
					element.find('.fc-event-title').html(newTitle);
				}
			},
			
			eventAfterRender :function( event, element, view ) { 
				var y = event.start;
				var d = y.getMonth().toString() + y.getDate().toString() + y.getFullYear().toString();
				var cal = $("#accHistoryCalendar")[0];
				$.removeData(cal,d.toString());		
				dateArray.length=0;
			},
			
			windowResize: function(view) {
					/* $('#accHistoryCalendar').fullCalendar('refetchEvents');
					$('#accHistoryCalendar').fullCalendar('render'); */
			}
			
		});	
		});
		function moveToDay(date){
			var toDate = new Date(date);
			$('#accHistoryCalendar').fullCalendar( 'changeView', 'basicDay' );
			$('#accHistoryCalendar').fullCalendar( 'gotoDate', toDate );
		}
				
		function getServerDate(){
		    var date = new Date('<ffi:getProperty name="GetCurrentDate" property="Date" />');
			return date;
		} 
		
		function clearData(){
			$('.calendarData').each(function(){
				$(this).slideToggle();
				$(this).remove();
			});
		}
	</script>

<style type='text/css'>

		label {
			margin-top: 40px;
			text-align: left;
			font-size: 14px;
			font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
			}

		#accHistoryCalendar {
			width: 95%;
			margin: 0 auto;
			}
			
		/* style added to increase the height of the content when a transaction is expanded for DayView */
		
		.fc-view-basicDay {
			height: 100%;
		}
	
		.fc-border-separate {
		     height: 100%;
		}
		
		.transactionDetailHolderCls{
			cursor: default;
		}
		
	</style>
	<div id='accHistoryCalendar'></div>
