<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<div align="center">
		
<ffi:setProperty name="subMenuSelected" value="account history"/>
<%-- include page header --%>
<ffi:include page="${PathExt}account/inc/nav_header.jsp" />

<table width="750" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td colspan="8" align="left" width="750" class="sectiontitle" height="14" background="/cb/web/multilang/grafx/account/sechdr_blank.gif">
			&nbsp;&gt; <s:text name="jsp.account_8"/>
		</td>
	</tr>
	<tr class="ltrow2_color">
         	<td class="columndata" align="center" colspan="8"><s:text name="jsp.account_199"/></td>
        </tr>
	<tr>
		<td colspan="8"><img src="/cb/web/multilang/grafx/account/sechdr_acct_btm.gif" alt="" width="750" height="11" border="0"></td>
	</tr>
</table>
</div>

<ffi:setProperty name="BackURL" value="${SecurePath}account/accounthistory.jsp"/>
