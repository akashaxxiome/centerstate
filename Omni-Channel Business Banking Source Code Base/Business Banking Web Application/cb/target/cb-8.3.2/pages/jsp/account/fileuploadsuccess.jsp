<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript">
function CSClickReturn () {
	var bAgent = window.navigator.userAgent;
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
}
CSStopExecution=false;
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
	var result;
	for (var i=0;i<array.length;i++) {
		if(CSStopExecution) return false;
		var aa = fct[array[i]];
		if (aa == null) return false;
		var ta = new Array;
		for(var j=1;j<aa.length;j++) {
			if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
				if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
				else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
				else ta[j]=aa[j];}
			} else ta[j]=aa[j];
		}
		result=aa[0](ta);
	}
	return result;
}
CSAct = new Object;
function CSOpenWindow(action) {
	var wf = "";
	wf = wf + "width=" + action[3];
	wf = wf + ",height=" + action[4];
	wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
	wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
	wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
	wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
	wf = wf + ",directories=" + (action[9] ? "yes" : "no");
	wf = wf + ",location=" + (action[10] ? "yes" : "no");
	wf = wf + ",status=" + (action[11] ? "yes" : "no");
	window.open(action[1],action[2],wf);
}

		function popNav()
		{
			var optionValue;
			optionValue = document.navForm.popNav.options[document.navForm.popNav.selectedIndex].value;
			document.location = optionValue;
		}
</script>

<%-- 
	Check if this is IFrame or not. If Iframe then set Jquery reference from parent frame.
	In case of file upload this file will be loaded in Iframe used to upload file.
 --%>
<s:include value="%{#session.PagesPath}/common/commonUploadIframeCheck_js.jsp" />

<div align="center">

	<form name="FileType" action="" method="post">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
				<table width="400" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td><img width="11"></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><br>
							<br>
						</td>
						<td align="center" class="sectionhead_grey">
							<table width="683" border="0" cellspacing="0" cellpadding="3">
                <tr>
									<td class="columndata" align="center" colspan="3"><s:text name="jsp.account_89"/></td>
                </tr>
							<tr>
								<td colspan="3" align="center" nowrap>
							    <sj:a 
									button="true" 
									onClickTopics="closeDialog"
									><s:text name="jsp.default_175"/></sj:a>
								</td>
							</tr>
							</table>
						</td>
						<td><br>
							<br>
						</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>
			</table>
	</form>
</div>
