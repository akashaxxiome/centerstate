<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<ffi:object name="com.ffusion.tasks.reporting.GetReportsByCategory" id="GetSavedSearches" scope="session"/>
<ffi:setProperty name="GetSavedSearches" property="ReportsName" value="SavedSearches" />
<ffi:setProperty name="GetSavedSearches" property="ReportCategory" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY %>" />

<%
	session.setAttribute("FFIGetSavedSearches", session.getAttribute("GetSavedSearches"));
%>

<ffi:setProperty name="tempURL" value="/pages/jsp/account/GetSavedSearchAction.action" URLEncrypt="true"/>

<s:url id="savedSearchUrl" value="%{#session.tempURL}"  escapeAmp="false"/>
<sjg:grid
	id="savedSearchId"
	caption=""
	dataType="json"
	href="%{savedSearchUrl}"
	pager="true"
	gridModel="mapListGridModel"
	rowList="10,15,20"
	rowNum="10"
	shrinkToFit="true"
	sortable="true"
	scroll="false"
	viewrecords="true"
	footerrow="false"
	userDataOnFooter="false"
	>
	<sjg:gridColumn name="reportName"  index="name" title="Name" sortable="false" cssClass="datagrid_textColumn"/>
	<sjg:gridColumn name="reportDescription" index="description" title="Description" sortable="false" cssClass="datagrid_textColumn"/>
	<sjg:gridColumn name="reportID" index="action" title="Action" sortable="true" cssClass="datagrid_textColumn"/>
</sjg:grid>
