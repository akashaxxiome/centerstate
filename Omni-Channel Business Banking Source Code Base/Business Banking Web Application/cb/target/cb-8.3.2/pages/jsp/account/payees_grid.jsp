<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%
   String filter = request.getParameter("filter");
   request.setAttribute("filter",filter);
%>

<div id="account_payees_gridID">
<ffi:help id="account_register-payees" />
<ffi:setGridURL grid="GRID_accountRegisterPayees" name="EditURL" url="/cb/pages/jsp/account/register-payees.jsp?SetEditRegisterPayeeId={0}" parm0="Id"/>
<ffi:setGridURL grid="GRID_accountRegisterPayees" name="DeleteURL" url="/cb/pages/jsp/account/register-payee-delete.jsp?SetRegisterPayeeId={0}" parm0="Id"/>

<ffi:setProperty name="tempURL" value="/pages/jsp/account/GetRegisterPayeesAction.action?filter=${filter}&GridURLs=GRID_accountRegisterPayees" URLEncrypt="true"/>
<s:url id="payeeSummaryURL" value="%{#session.tempURL}" escapeAmp="false"/>
<sjg:grid
     id="payeeSummaryID"
	 sortable="true" 
     dataType="json" 
     href="%{payeeSummaryURL}"
     pager="true"
     gridModel="gridModel"
	 rowList="%{#session.StdGridRowList}" 
	 rowNum="%{#session.StdGridRowNum}" 
	 rownumbers="false"
	 shrinkToFit="true"
	 scroll="false"
	 scrollrows="true"
     onGridCompleteTopics="addGridControlsEvents,payeeSummaryEvent"
     >
     
     <sjg:gridColumn name="name" index="name" title="%{getText('jsp.default_313')}" sortable="true" width="230" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="map.cate" index="map.cate" title="%{getText('jsp.account_52')}" sortable="false" width="400" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="id" index="id" title="%{getText('jsp.default_27')}" sortable="false" width="100" align="right" formatter="ns.account.payeeActionFormatter"/>
     <sjg:gridColumn name="map.EditURL" index="EditURL" title="%{getText('jsp.default_181')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
	 <sjg:gridColumn name="map.DeleteURL" index="DeleteURL" title="%{getText('jsp.default_167')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/>
</sjg:grid>

</div>
<script>	
					
		$('#account_payees_gridID').portlet({
			generateDOM: true,
			helpCallback: function(){
				
				var helpFile = $('#account_payees_gridID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}
			
		});

		$('#account_payees_gridID').portlet('title', js_acc_payee_portlet_title);

		$("#payeeSummaryID").jqGrid('setColProp','id',{title:false});

</script>