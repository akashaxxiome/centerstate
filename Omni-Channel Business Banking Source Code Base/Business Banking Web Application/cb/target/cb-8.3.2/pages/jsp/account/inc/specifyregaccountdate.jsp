<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%
   String refrenceNum = request.getParameter("GetRegisterTransactions.ReferenceNumber");
   if(refrenceNum!=null){
	   request.setAttribute("refrenceNum",refrenceNum);
	   %>
	   <ffi:setProperty name="GetRegisterTransactions" property="ReferenceNumber" value="<%=refrenceNum%>"/>
	   <%
          }
   String fromDate = request.getParameter("GetRegisterTransactions.FromDate");
   if(fromDate!=null){
	   request.setAttribute("fromDate",fromDate);
	   %>
	   <ffi:setProperty name="GetRegisterTransactions" property="FromDate" value="<%=fromDate%>"/>
	   <%
        }
   String toDate = request.getParameter("GetRegisterTransactions.ToDate");
   if(toDate!=null){
	   request.setAttribute("toDate",toDate);
	   %>
	   <ffi:setProperty name="GetRegisterTransactions" property="ToDate" value="<%=toDate%>"/>
	<%
   }
%>

<%-- Process tasks --%>
<ffi:cinclude value1="${RunFilter}" value2="true" operator="equals">
	<ffi:setProperty name="RunFilter" value=""/>
	<ffi:process name="GetRegisterTransactions"/>
</ffi:cinclude>
<%-- ObjectID for current account --%>
<%
String objectID = null;
String acctID = null;
String bankID=null;
String rtn=null;
%>
<ffi:getProperty name="<%= com.ffusion.tasks.accounts.Task.ACCOUNT %>" property="ID" assignTo="acctID"/>
<ffi:getProperty name="<%= com.ffusion.tasks.accounts.Task.ACCOUNT %>" property="BankID" assignTo="bankID"/>
<ffi:getProperty name="<%= com.ffusion.tasks.accounts.Task.ACCOUNT %>" property="RoutingNum" assignTo="rtn"/>
<%-- Get entitlement object ID --%>
<ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetEntitlementObjectID"/>
<ffi:setProperty name="GetEntitlementObjectID" property="ObjectType" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyName" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyValue" value="<%= acctID %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyName" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_BANK_ID %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyValue" value="<%= bankID %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyName" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>" />
<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyValue" value="<%= rtn %>" />
<ffi:process name="GetEntitlementObjectID" />
<ffi:getProperty name="GetEntitlementObjectID" property="ObjectID" assignTo="objectID" />

<ffi:setProperty name="activeMemofalse" value="false"/>
<ffi:setProperty name="activeMemotrue" value="${showRegMemos}"/>
<ffi:setProperty name="Compare" property="Value1" value="${showRegMemos}"/>
<ffi:setProperty name="Compare" property="Value2" value=""/>
<% String s = ""; %>
<ffi:getProperty name="activeMemo${Compare.NotEquals}" assignTo="s"/>
<ffi:setProperty name="showRegMemos" value="<%= s %>"/>

<SCRIPT type=text/javascript>
$(function(){
	$("#SetDefaultRegisterAccountID").selectmenu({width: 440});
});

  // view button for view account select box
   ns.account.viewAccount = function(){
      var urlString ="/cb/pages/jsp/account/register-wait.jsp";
		$.ajax({
			url: urlString,
			type: "POST",
			data: $("#changeAccount").serialize(),
			success: function(data){
			$('#accountRegisterDashbord').html(data);
			}
		});
	  }
  //show hide memos
   ns.account.showHideMemos = function(flag){
      if(flag=='hide'){
    	  document.changeAccount.showRegMemos.value='false';
          }else{
        	  document.changeAccount.showRegMemos.value='true';
          }
	  }
<!--
   ns.account.changeSuccess = function()
{
	// This function changes the form action to go to SetDefaultRegisterAccount if the user changes accounts in the drop down box.
	document.changeAccount.ChangeAccount.value="true";
}

   ns.account.checkDate = function()
   {
	// This function checks the ToDate on the form to make sure that the user has not left it blank.  If the user
	// HAS left it blank, we're assuming that they want a list of transactions through today's date, therefore
	// we are populating the box accordingly when they submit the form.
	if (document.changeAccount["GetRegisterTransactions.ToDate"].value == "")
	{
		<ffi:setProperty name="GetCurrentDate" property="DateFormat" value="${UserLocale.DateFormat}"/>
		document.changeAccount["GetRegisterTransactions.ToDate"].value = "<ffi:getProperty name="GetCurrentDate" property="Date"/>";
		return true;
	}
   }
-->
</SCRIPT>

<%-- This include jsp expects to be called from the an register.jsp --%>
<%-- This include jsp requires Account object to be set --%>
			<%-- AccountGroupID: AccountGroup to show in drop-down menus --%>
			<%-- Default is the AccountGroup for the current account --%>
			<ffi:setProperty name="StringUtil" property="Value1" value="${AccountGroupID}" />
			<ffi:setProperty name="StringUtil" property="Value2" value="${Account.AccountGroup}" />
			<ffi:setProperty name="AccountGroupID" value="${StringUtil.NotEmpty}" />

			<s:form id="changeAccount" method="post" name="changeAccount" action="/pages/jsp/account/register.jsp" theme="simple">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
			<input type="hidden" name="RunFilter" value="true">
			<input type="hidden" name="ChangeAccount">
			<input type="hidden" name="showRegMemos" value="<ffi:getProperty name="showRegMemos"/>">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right" width="25%"><span class="sectionsubhead"><s:text name="jsp.default_21"/></span></td>
					<td width="75%">
						<div align="left">
							<table border="0" cellspacing="4" cellpadding="0" width="100%">
								<tr>
									<td width="50%">
										<select id="SetDefaultRegisterAccountID" class="txtbox" name="SetDefaultRegisterAccountID" onChange="ns.account.changeSuccess()">
											<%-- StringUtil element to set selected option attribute --%>
											<ffi:setProperty name="StringUtil" property="Value1" value="${Account.ID}" />
											<ffi:setProperty name="<%= com.ffusion.tasks.banking.Task.ACCOUNTS %>" property="Filter" value="REG_ENABLED=true"/>
											<ffi:list collection="<%= com.ffusion.tasks.banking.Task.ACCOUNTS %>" items="acct">
												<ffi:cinclude value1="${acct.REG_ENABLED}" value2="true">
													<ffi:cinclude value1="${acct.TypeValue}" value2="14" operator="notEquals">
														<ffi:setProperty name="StringUtil" property="Value2" value="${acct.ID}" />
														<option <ffi:getProperty name="StringUtil" property="SelectedIfEquals" /> value='<ffi:getProperty name="acct" property="ID"/>'>
															<ffi:cinclude value1="${acct.RoutingNum}" value2="" operator="notEquals">
																<ffi:getProperty name="acct" property="RoutingNum"/>:</ffi:cinclude><ffi:getProperty name="acct" property="DisplayText"/><ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >- <ffi:getProperty name="acct" property="NickName"/></ffi:cinclude>- <ffi:getProperty name="acct" property="CurrencyCode"/>
														</option>
													</ffi:cinclude>
												</ffi:cinclude>
											</ffi:list>
										</select>
									</td>

							   <td align="left" nowrap width="50%">
							   <sj:a
                                  button="true"
                                  onclick="ns.account.viewAccount()"><s:text name="jsp.default_459"/></sj:a></td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<%-- StringUtil element to display text in date fields --%>
					<td align="right" nowrap><span class="sectionsubhead"><s:text name="jsp.account_175"/></span></td>
					<td>
						<div align="left">
							<table border="0" width="100%">
								<tr>
									<td align="left" nowrap><input class="ui-widget-content ui-corner-all" type="text" name="GetRegisterTransactions.ReferenceNumber" value="<ffi:getProperty name="GetRegisterTransactions" property="ReferenceNumber"/>" size="10" maxlength="20">&nbsp;</td>
									<ffi:setProperty name="StringUtil" property="Value2" value="${UserLocale.DateFormat}" />
									<td align="center" nowrap><span class="sectionsubhead"><s:text name="jsp.default_143"/></span></td>
								    <ffi:setProperty name="StringUtil" property="Value1" value="${GetRegisterTransactions.FromDate}" />

									<td align="left" nowrap>
									<sj:datepicker value="%{#session.StringUtil.NotEmpty}" id="startDate" name="GetRegisterTransactions.FromDate" label="%{getText('jsp.default_137')}" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
									</td>
									<td align="center" nowrap><p class="sectionsubhead"><s:text name="jsp.default_423.1"/></p></td>

									<ffi:setProperty name="StringUtil" property="Value1" value="${GetRegisterTransactions.ToDate}" />
									<td align="left" nowrap>
									<sj:datepicker value="%{#session.StringUtil.NotEmpty}" id="endDate" name="GetRegisterTransactions.ToDate" label="%{getText('jsp.default_137')}" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/>
									</td>
									<td align="left" nowrap width="40%">
                                    <sj:a id="viewCheckdate"
                                       formIds="changeAccount"
                                       button="true"
                                       targets="accountRegisterDashbord"
                                       onclick="ns.account.checkDate()"><s:text name="jsp.default_459"/></sj:a>
                                    </td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table border="0" cellspacing="4" cellpadding="0" width="100%">
							<tr>
								<td class="columndata" nowrap><img src="/cb/web/multilang/grafx/account/i_in_process.gif" border="0" width="16" height="16" alt="<s:text name="jsp.account_174"/>">&nbsp;<s:text name="jsp.default_237"/></td>
								<td class="columndata" nowrap><img src="/cb/web/multilang/grafx/account/gen-checkmark.gif" border="0" width="16" height="16" alt="<s:text name="jsp.account_174"/>">&nbsp;<s:text name="jsp.account_174"/></td>
								<td class="columndata" nowrap>&nbsp;&nbsp;<img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/account/gen-checkmarkauto.gif" border="0" width="16" height="16" alt="<s:text name="jsp.account_139"/>">&nbsp;<s:text name="jsp.account_137"/></td>
								<td class="columndata" nowrap>&nbsp;&nbsp;<img src="/cb/web/grafx/<ffi:getProperty name="ImgExt"/>/account/gen-checkmarkautodisc.gif" border="0" width="16" height="16" alt="<s:text name="jsp.account_140"/>">&nbsp;<s:text name="jsp.account_138"/></td>
								<td align="right">
									<table border="0" cellspacing="4" cellpadding="0" width="100%">
									<tr>
										<td align="right">
											<%--<!-- Show appropriate Show/Hide Memos button depending on value of showRegMemos -->--%>
											<ffi:cinclude value1="${showRegMemos}" value2="true">
												 <sj:a id="hideMemos"
												       formIds="changeAccount"
				                                       button="true"
				                                       targets="accountRegisterDashbord"
				                                       onclick="ns.account.showHideMemos('hide')"><s:text name="jsp.account_95"/></sj:a>
											</ffi:cinclude>
											<ffi:cinclude value1="${showRegMemos}" value2="false">
												 <sj:a id="showMemos"
												       formIds="changeAccount"
				                                       button="true"
				                                       targets="accountRegisterDashbord"
				                                       onclick="ns.account.showHideMemos('show')"><s:text name="jsp.account_190"/></sj:a>
											</ffi:cinclude>
										</td>
									</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			</s:form>
