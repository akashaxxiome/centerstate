<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<script>
	$(function(){
		$("#debitCreditOptionID").selectmenu({width: 200});
		$("#exportFormatID").selectmenu({width: 200});
   });
</script>

<ffi:object id="ExportHistory" name="com.ffusion.tasks.banking.ExportHistory" scope="session"/>
<ffi:setProperty name="ExportHistory" property="DateFormat" value="${UserLocale.DateFormat}"/>

	<%
		session.setAttribute("FFIExportHistory", session.getAttribute("ExportHistory"));
	%>

<%-- Added this condition in order to always display export option for consumer & after checking entitlement for corporate --%>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:setProperty name="displayExportButtons" value="false"/>
	<ffi:cinclude ifEntitled="<%= com.ffusion.csil.core.common.EntitlementsDefines.EXPORT_REPORT %>">
		<ffi:setProperty name="displayExportButtons" value="true"/>
	</ffi:cinclude>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:setProperty name="displayExportButtons" value="true"/>
	<ffi:setProperty name="ExportHistory" property="IsConsumerBanking" value="true"/>
	<ffi:setProperty name="ExportHistory" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
		<ffi:cinclude value1="${ExportHistory.SearchCriteriaValue}" value2="" operator="equals">
			<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${Account.ID}:${Account.BankID}:${Account.RoutingNum}:${Account.NickName}:${Account.CurrencyCode}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
				<ffi:setProperty name="ExportHistory" property="SearchCriteriaValue" value="${ExportAccountHistoryValues}" />
			</ffi:cinclude>
		</ffi:cinclude>
</ffi:cinclude>




<%-- <ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals"> --%>
				<ffi:cinclude value1="${displayExportButtons}" value2="true" operator="equals">
				<br>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<div align="center">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>

										<td align="center" class="sectionhead_grey">
											<s:form method="post" action="ExportHistoryAction"  namespace="/pages/jsp/account" name="exportform" id="exportformID" theme="simple" target="_blank">
			<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
												<input type="hidden" name="ExportHistory.startDate" value='<ffi:getProperty name="GetPagedTransactions" property="StartDate"/>'>
												<input type="hidden" name="ExportHistory.endDate" value='<ffi:getProperty name="GetPagedTransactions" property="EndDate"/>'>
												<input type="hidden" name="fromAccountHistory" value='true'>
												<input type="hidden" name="ExportHistory.dataClassification" value='<ffi:getProperty name="GetPagedTransactions" property="DataClassification"/>'>
												<table width="418" border="0" cellspacing="0" cellpadding="3" class="sectionhead_grey">
													<tr>
														<td nowrap class="sectionhead_grey" width="107"><img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="9" height="6" border="0"><span class="sectionsubhead"><s:text name="jsp.account_87"/></span></td>
														<td nowrap class="sectionhead_grey" width="108">
															<select class="txtbox" name="ExportHistory.debitCreditOption" id="debitCreditOptionID">
																	<option value=<%= com.ffusion.tasks.banking.ExportHistory.ALL_LINES %> selected><s:text name="jsp.default_39"/></option>
																	<option value=<%= com.ffusion.tasks.banking.ExportHistory.DEBIT_LINES %>><s:text name="jsp.account_69"/></option>
																	<option value=<%= com.ffusion.tasks.banking.ExportHistory.CREDIT_LINES %>><s:text name="jsp.account_59"/></option>
															</select>
														</td>
														<td nowrap class="sectionhead_grey" width="107"><img src="/cb/web/multilang/grafx/account/spacer.gif" alt="" width="9" height="6" border="0"><span class="sectionsubhead"><s:text name="jsp.account_86"/></span></td>
														<td nowrap class="sectionhead_grey" width="108">
															<select class="txtbox" name="ExportHistory.exportFormat" id="exportFormatID">
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_HTML%>><s:text name="jsp.default_231"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_COMMA_DELIMITED%>><s:text name="jsp.default_105"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TAB_DELIMITED%>><s:text name="jsp.default_402"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_PDF%>><s:text name="jsp.default_323"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_BAI2%>><s:text name="jsp.default_59"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_TEXT%>><s:text name="jsp.default_325"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QIF%>><s:text name="jsp.account_172"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QFX%>><s:text name="jsp.account_171"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_QBO%>><s:text name="jsp.account_170"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_IIF%>><s:text name="jsp.account_100"/></option>
																<option value=<%= com.ffusion.beans.reporting.ExportFormats.FORMAT_CSV%>><s:text name="jsp.default_124"/></option>
															</select>
														</td>
														<td nowrap class="sectionhead_grey" width="78">
														<%--
														changed sj:submit button to sj:a, as it was submitting two requests to server, which was failing for pdf export
														 --%>
														<sj:a
															id="exportHistorySubmit"
															button="true"
															buttonIcon="ui-icon-refresh"
															onclick="exportReport()"
														><s:text name="jsp.default_195"/></sj:a>
														</td>
													</tr>
												</table>
											</s:form>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
				</table>
				<br>
				</ffi:cinclude>
<%-- 			</ffi:cinclude> --%>

	<script>
		//submitting form using js in order to avoid double posting due to use of sj:submit button
		function exportReport()
		{
			var formDom = $('#exportformID')[0];
			formDom.target = "ifrmDownload";
			formDom.submit();
		}
	</script>





