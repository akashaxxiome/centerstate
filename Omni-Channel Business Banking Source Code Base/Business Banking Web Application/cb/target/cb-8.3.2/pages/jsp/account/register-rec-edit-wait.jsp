<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%
    session.setAttribute("SetRegisterTransactionId",request.getParameter("SetRegisterTransactionId"));
    session.setAttribute("CollectionSessionName",request.getParameter("CollectionSessionName"));
    String collectionName=request.getParameter("CollectionSessionName");
%>
<ffi:setProperty name="SetRegisterTransaction" property="CollectionSessionName" value="<%=collectionName %>"/>
<% String v153_4a = ""; String v153_2b = ""; %>

<ffi:cinclude value1="${registerInitTouched}" value2="true" operator="notEquals" >
	<s:include value="/pages/jsp/inc/init/register-init.jsp" />
</ffi:cinclude>

<ffi:object name="com.ffusion.tasks.register.SetRegisterTransaction" id="SetRegisterTransaction" scope="session"/>
	<ffi:setProperty name="SetRegisterTransaction" property="Id" value="${SetRegisterTransactionId}" />
	<ffi:setProperty name="SetRegisterTransaction" property="CollectionSessionName" value="${CollectionSessionName}" />
<ffi:process name="SetRegisterTransaction"/>

<ffi:setProperty name="RegisterCategories" property="Filter" value="All" />

<ffi:object name="com.ffusion.tasks.register.EditRegisterTransactionCB" id="EditRegisterTransaction" scope="session"/>
<ffi:setProperty name="EditRegisterTransaction" property="CollectionSessionName" value="${SetRegisterTransaction.CollectionSessionName}" />
<ffi:setProperty name="EditRegisterTransaction" property="TotalAmount" value="0" />

<ffi:object id="SetRegisterPayee" name="com.ffusion.tasks.register.SetRegisterPayee" scope="session"/>
	<ffi:setProperty name="SetRegisterPayee" property="Id" value="" />
<ffi:process name="SetRegisterPayee"/>

<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
	<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
	<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
<ffi:process name="CopyCollection"/>

<ffi:cinclude value1="${RegisterTransaction.MultipleCategories}" value2="true" operator="equals">
	<ffi:setProperty name="whichCat" value="multi"/>
</ffi:cinclude>
<ffi:cinclude value1="${RegisterTransaction.MultipleCategories}" value2="true" operator="notEquals">
	<ffi:setProperty name="whichCat" value="single"/>
</ffi:cinclude>

<ffi:setProperty name="NewPayee" value=""/>
<ffi:setProperty name="ExistingPayee" value=""/>
<ffi:setProperty name="NewChecked" value="false"/>

<s:include value="/pages/jsp/account/register-rec-edit.jsp"/>
