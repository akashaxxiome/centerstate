<%@page import="com.ffusion.util.HTMLUtil"%>
<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines" %>

<s:include value="/pages/jsp/account/inc/accounthistory_common.jsp"/>

<%-- ADDED BY TCG FOR CHECKIMAGING --%>

<%-- Flag to check whether to retrieve image or not --%>
<ffi:setProperty name="get-image-flag" value="true"/>

<%-- INITIALIZING TRANSACTION SEARCH IMAGE TASK --%>
<ffi:object name="com.ffusion.tasks.checkimaging.SearchImage" id="SearchImage" scope="session"/>
<ffi:setProperty name="SearchImage" property="Init" value="true"/>
<ffi:process name="SearchImage"/>
<ffi:setProperty name="SearchImage" property="SuccessURL" value="${SecurePath}account/checkimage.jsp" />

<script type="text/javascript"><!--
function CSClickReturn () {
	var bAgent = window.navigator.userAgent;
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
}
CSStopExecution=false;
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
	return CSAction3(fct, array, null);
}
function CSAction3(fct, array, param) {
	var result;
	for (var i=0;i<array.length;i++) {
		if(CSStopExecution) return false;
		var aa = fct[array[i]];
		if (aa == null) return false;
		var ta = new Array;
		for(var j=1;j<aa.length;j++) {
			if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
				if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
				else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
				else ta[j]=aa[j];}
			} else ta[j]=aa[j];
		}
		result=aa[0](ta, param);
	}
	return result;
}
CSAct = new Object;
number = -1;
function CSOpenWindow(action, param) {
	var wf = "";
	wf = wf + "width=" + action[3];
	wf = wf + ",height=" + action[4];
	wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
	wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
	wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
	wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
	wf = wf + ",directories=" + (action[9] ? "yes" : "no");
	wf = wf + ",location=" + (action[10] ? "yes" : "no");
	wf = wf + ",status=" + (action[11] ? "yes" : "no");
	if (param == null) {
		window.open(action[1], action[2], wf);
	} else {
		window.open(action[1] + "?" + param, action[2], wf);
	}
}


<%-- added by TCG for checkimaging --%>
function CSOpenImageWindow(url) {

	window.open(url, "CheckImage", "width=600, height=550, location=no, menubar=no, status=no, toolbar=no, scrollbars=no, resizable=no");
}

function setTranNumber( num )
{
	CSAct[/*CMP*/ 'B9D1DAD41'] = new Array(CSOpenWindow,/*URL*/ '<ffi:getProperty name="SecurePath"/>account/transactiondetail.jsp?tranIndex=' + num,'',518,455,false,false,false,false,false,false,false);
}


CSAct[/*CMP*/ 'B9D1DAD40'] = new Array(CSOpenWindow,/*URL*/ '<ffi:getProperty name="SecurePath"/>account/inquirehistory.jsp','',300,350,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E611E8165'] = new Array(CSOpenWindow,/*URL*/ '<ffi:getProperty name='SecurePath'/>calendar.jsp?calDirection=back&calForm=FormName&calTarget=GetPagedTransactions.StartDate','',350,300,false,false,false,false,false,false,false);
CSAct[/*CMP*/ 'B9E611E8166'] = new Array(CSOpenWindow,/*URL*/ '<ffi:getProperty name='SecurePath'/>calendar.jsp?calDirection=back&calForm=FormName&calTarget=GetPagedTransactions.EndDate','',350,300,false,false,false,false,false,false,false);

		function popNav()
		{
			var optionValue;
			optionValue = document.navForm.popNav.options[document.navForm.popNav.selectedIndex].value;
			document.location = optionValue;
		}

// --></script>

		<div align="left">
		
			<%-- set up to fetch the Transactions collection --%>
			<% String zbaFlag=null; %>
			<ffi:getProperty name="TSZBADisplay" assignTo="zbaFlag"/>
			<ffi:cinclude value1="<%= zbaFlag %>" value2="" operator="equals">
				<ffi:getProperty name="Account" property="ZBAFlag" assignTo="zbaFlag"/>
			</ffi:cinclude>
			<ffi:cinclude value1="<%= zbaFlag %>" value2="" operator="equals">
				<%
					zbaFlag = com.ffusion.efs.adapters.profile.constants.ProfileDefines.ZBA_BOTH;
				%>
			</ffi:cinclude>
			<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ZBA_DISPLAY %>" />
			<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="<%= zbaFlag %>" />

<ffi:setProperty name="subMenuSelected" value="account history"/>

			<%-- Prepare Sorting Functionality --%>
			<ffi:cinclude value1="${GetTransactionsSortImage}" value2="" operator="equals">
	    			<ffi:object name="com.ffusion.tasks.util.GetSortImage" id="GetTransactionsSortImage" scope="session"/>
	    			<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetTransactionsSortImage" property="NoSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_off.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
	    			<s:set var="tmpI18nStr" value="%{getText('jsp.default_362')}" scope="request" /><ffi:setProperty name="GetTransactionsSortImage" property="AscendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_asc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
	    			<s:set var="tmpI18nStr" value="%{getText('jsp.default_384')}" scope="request" /><ffi:setProperty name="GetTransactionsSortImage" property="DescendingSort" value='<img src="/cb/web/multilang/grafx/payments/i_sort_desc.gif" alt="${tmpI18nStr}" width="24" height="8" border="0">'/>
	    			<ffi:setProperty name="GetTransactionsSortImage" property="Collection" value="Transactions"/>
    			</ffi:cinclude>

			<%-- fetch Transactions collection --%>
			<ffi:cinclude value1="${ProcessTransactions}" value2="true" operator="equals">
				<ffi:setProperty name="Transactions" property="SortedBy" value="DATE"/>
				<ffi:setProperty name="ProcessTransactions" value="false"/>
				<%-- Process the Transactions collection for sort imaging --%>
			</ffi:cinclude>

			<%-- ObjectID for current account --%>
			<%
			com.ffusion.beans.accounts.Account currAccount = (com.ffusion.beans.accounts.Account)session.getAttribute( com.ffusion.tasks.accounts.Task.ACCOUNT );
			String accountDisplayText = null;
			String objectID = null;
			%>
			<%-- Get entitlement object ID --%>
			<ffi:object name="com.ffusion.tasks.util.GetEntitlementObjectID" id="GetEntitlementObjectID"/>
			<ffi:setProperty name="GetEntitlementObjectID" property="ObjectType" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.OBJECT_TYPE_ACCOUNT %>" />
			<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyName" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ACCOUNT_ID %>" />
			<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyValue" value="<%= currAccount.getID() %>" />
			<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyName" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_BANK_ID %>" />
			<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyValue" value="<%= currAccount.getBankID() %>" />
			<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyName" value="<%= com.ffusion.tasks.util.GetEntitlementObjectID.KEY_ROUTING_NUMBER %>" />
			<ffi:setProperty name="GetEntitlementObjectID" property="CurrentPropertyValue" value="<%= currAccount.getRoutingNum() %>" />
			<ffi:process name="GetEntitlementObjectID" />
			<ffi:getProperty name="GetEntitlementObjectID" property="ObjectID" assignTo="objectID" />
			
				<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
					<% if(currAccount != null) { 
						accountDisplayText = HTMLUtil.encode(currAccount.getDisplayTextRoutingNumNickNameCurrency()); 
					} %>
					<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
						<script>
						    ns.account.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accounthistory_other_grid.jsp' />";
							$(function(){
								ns.account.refreshSummaryForm(ns.account.refreshSummaryFormURL);
							});
						</script>
					</ffi:cinclude>
					<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
						<script>
							ns.account.gridContainerID = "<ffi:getProperty name='accountHistoryGridID${accountIndex}'/>";
						    ns.account.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accounthistory_other_grid.jsp?accountIndex=${accountIndex}&setAccountID=${setAccountID}'/>";
							$(function(){
								var gridID = "<ffi:getProperty name="accountHistoryGridID${accountIndex}"/>";
								ns.account.refreshSummaryForm(ns.account.refreshSummaryFormURL, gridID);
							});
						</script>
					</ffi:cinclude>
				</ffi:cinclude>
			<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
				<% if(currAccount != null) {
						accountDisplayText = currAccount.getConsumerAccountDisplayTextWithType(); 
				} %>
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="notEquals">
					<script>
					    ns.account.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accounthistory_other_grid.jsp' />";
						$(function(){
							ns.account.refreshSummaryForm(ns.account.refreshSummaryFormURL);
						});
					</script>
				</ffi:cinclude>
				<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
					<script>
						ns.account.gridContainerID = "<ffi:getProperty name='accountHistoryGridID${accountIndex}'/>";
					    ns.account.refreshSummaryFormURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/accounthistory_other_grid.jsp?accountIndex=${accountIndex}&setAccountID=${setAccountID}'/>";
						$(function(){
							var gridID = "<ffi:getProperty name="accountHistoryGridID${accountIndex}"/>";
							ns.account.refreshSummaryForm(ns.account.refreshSummaryFormURL, gridID);
						});
					</script>
				</ffi:cinclude>
			</ffi:cinclude>
		</div>

<%-- Since this search is OK, store those values here. --%>
<ffi:setProperty name="unsuccessfulSearch" value="false"/>

<ffi:setProperty name="LastGoodTSTransactionType" value="${TSTransactionType}"/>
<ffi:setProperty name="LastGoodTSZBADisplay" value="${TSZBADisplay}"/>
<ffi:setProperty name="LastGoodTSReferenceStart" value="${TSReferenceStart}"/>
<ffi:setProperty name="LastGoodTSReferenceEnd" value="${TSReferenceEnd}"/>
<ffi:setProperty name="LastGoodTSMinimumAmount" value="${TSMinimumAmount}"/>
<ffi:setProperty name="LastGoodTSMaximumAmount" value="${TSMaximumAmount}"/>
<ffi:setProperty name="LastGoodTSDescription" value="${TSDescription}"/>
<ffi:setProperty name="LastGoodStartDate" value="${GetPagedTransactions.StartDate}"/>
<ffi:setProperty name="LastGoodEndDate" value="${GetPagedTransactions.EndDate}"/>
<ffi:setProperty name="LastGoodDataClassification" value="${GetPagedTransactions.DataClassification}"/>
<%
	session.setAttribute( "LastGoodAccount", session.getAttribute( "Account" ) );
%>

<ffi:removeProperty name="TSView"/>
<ffi:removeProperty name="TransactionTypes" />
<ffi:removeProperty name="GetEntitlementObjectID" />
<ffi:removeProperty name="DateRangeValue"/>

<ffi:setProperty name="BackURL" value="${SecurePath}account/accounthistory_other.jsp"/>
