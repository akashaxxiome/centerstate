<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<% // From single record page to  this page and write the select info for the first select option
   String RegisterCategoryId = request.getParameter("RegisterTransaction.Current=0&RegisterTransaction.RegisterCategoryId"); 
  
   String select1 = request.getParameter("RegisterTransaction.Current=1&RegisterTransaction.RegisterCategoryId");
   String select2 = request.getParameter("RegisterTransaction.Current=2&RegisterTransaction.RegisterCategoryId");
   String select3 = request.getParameter("RegisterTransaction.Current=3&RegisterTransaction.RegisterCategoryId");
   String select4 = request.getParameter("RegisterTransaction.Current=4&RegisterTransaction.RegisterCategoryId");
   String amount0 = request.getParameter("RegisterTransaction.Current=0&RegisterTransaction.Amount");
   String amount1 = request.getParameter("RegisterTransaction.Current=1&RegisterTransaction.Amount");
   String amount2 = request.getParameter("RegisterTransaction.Current=2&RegisterTransaction.Amount");
   String amount3 = request.getParameter("RegisterTransaction.Current=3&RegisterTransaction.Amount");
   String amount4 = request.getParameter("RegisterTransaction.Current=4&RegisterTransaction.Amount");
   //to get the request from single category or multiple select on change method
   String flag = request.getParameter("flag");
%>
<script><!--
      $(document).ready(function(){
           $("#multipleDIV").find("select")
           .each(function(){
               $(this).selectmenu({width: 250});
               });  
        });

      ns.account.selectMultipleCategoriesOnchange = function(){
       //for multiple selected
  	   var urlString ="/cb/pages/jsp/account/register-rec-add.jsp?flag=multiOnchange&whichCat=multi";
    	  $.ajax({   
	  		  type: "post",   
	  		  url: urlString,   
	  		  data: $('#AddTransaction').serialize(), 
	  		  success: function(data) { 
	  			$('#accountRegisterDashbord').html(data);
	  		  }   
	  	});
        }
   
multiField = true;
        
var categoryID = new Array(5);
var categoryIDmappedtype = new Array(5);

<ffi:setProperty name="RegisterCategories" property="Filter" value="All" />
<ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />

<ffi:object id="Paging" name="com.ffusion.beans.util.Paging" scope="session"/>
<ffi:setProperty name="Paging" property="Pages" value="5" />
<ffi:list collection="Paging" items="PageNum">
<ffi:setProperty name="Math" property="Value1" value="${PageNum}" />
<ffi:setProperty name="Math" property="Value2" value="1" />
<ffi:setProperty name="CurrentID" value="${Math.Subtract}"/>
<%String Current1 = null; %>
<ffi:getProperty name="CurrentID" assignTo="Current1"/>

<ffi:setProperty name="RegisterTransaction" property="Current" value="${CurrentID}" />
<%if(Current1.equalsIgnoreCase("1")){%>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select1 %>" />
<%}else if(Current1.equalsIgnoreCase("2")){ %>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select2 %>" />
<%}else if(Current1.equalsIgnoreCase("3")){ %>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select3 %>" />
<%}else if(Current1.equalsIgnoreCase("4")){ %>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select4 %>" />
<%}else{ %>
<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=RegisterCategoryId %>" />
<%} %>
<ffi:setProperty name="Compare" property="Value1" value="${RegisterTransaction.RegisterCategoryId}" />

categoryID[<ffi:getProperty name="CurrentID"/>] = "<ffi:getProperty name="RegisterTransaction" property="RegisterCategoryId"/>";

	<ffi:list collection="RegisterCategories" items="Category">
		<ffi:setProperty name="Compare" property="Value2" value="${Category.Id}" />
		<ffi:cinclude value1="${Compare.Equals}" value2="true" operator="equals">
			categoryIDmappedtype[<ffi:getProperty name="CurrentID"/>] = "<ffi:getProperty name="Category" property="Type"/>";
		</ffi:cinclude>
	</ffi:list>
</ffi:list>

function stripChars(str) {
	// This function will strip everything but numbers and decimal points from a given string, then
	// convert the string to a Float Math object before returning the value.
	myTemp = str.replace(/[^0-9|\.|\-]/g,"");
	if (myTemp == "") { myTemp = 0 }
	var amt = parseFloat(myTemp);
	
	if( isNaN( amt ) ) {
		return NaN;
	}
	
	return amt;
}

function addCurrency( theForm, setTotalAmount ) {
	var totalAmt = 0.0;
	var currAmt;
	var deborcred;
	var amtStr;
	var currencyType;
	currencyType = getCurrencyforAccount( document.AddTransaction["AddRegisterTransaction.AccountID"].value );
	<%
	String fieldName=null;
	for ( int i=0; i<5; i++ ) {
		fieldName = "RegisterTransaction.Current=" + i + "&RegisterTransaction.Amount";
	%>
		amtStr = theForm["<%= fieldName %>" ].value;
		if( amtStr!=null && amtStr.length>0 ) {
			currAmt = stripChars( amtStr );
			if( isNaN( currAmt ) ) {

			} else if ( currAmt<=0 ) {

			} else {
				if( categoryIDmappedtype[<%= i %>] == 2 ) { // If it is an income
					totalAmt -= currAmt;
				}
				else if ( categoryIDmappedtype[<%= i %>] == 3 ) { //If it is a debit
					totalAmt += currAmt;
				}
				else {
					// Check if it is the unassigned type
					if( categoryID[<%= i %>] == "0" ) {
						totalAmt += currAmt;
					}
				}
				
			}
		}
	<%
	}
	%>

	deborcred = "Debit";
	if( totalAmt < 0 ) {
		deborcred = "Credit";
		totalAmt *= -1;
	}
	
	if( currencyType == 'JPY' ) {
		totalAmt = Math.round(totalAmt);
		totalValue = totalAmt.toString();
	} else {
		totalAmt = Math.round( totalAmt*100)/100;
		totalValue = totalAmt.toString();

		if (totalValue.indexOf('.') == -1) { totalValue = totalValue+".0" } // Add .0 if a decimal point doesn't exist
		temp=(totalValue.length-totalValue.indexOf('.')) // Find out if the # ends in a tenth (ie 145.5)
		if (temp <= 2) { totalValue=totalValue+"0"; } // if it ends in a tenth, add an extra zero to the string
	}

	theForm.multiTotal.value = totalValue;
	if (setTotalAmount == 'true') {
  	    theForm["AddRegisterTransaction.TotalAmount"].value = totalValue;
	}
}

-->
</script>

<ffi:object id="CopyCollection" name="com.ffusion.tasks.util.CopyCollection" scope="session"/>
	<ffi:setProperty name="CopyCollection" property="CollectionSource" value="RegisterCategories" />
	<ffi:setProperty name="CopyCollection" property="CollectionDestination" value="RegisterCategoriesCopy" />
<ffi:process name="CopyCollection"/>

<ffi:list collection="RegisterCategories" items="Category">
	<ffi:cinclude value1="${Category.Id}" value2="0" operator="equals">
		<ffi:setProperty name="Category" property="Locale" value="${UserLocale.Locale}" />
	</ffi:cinclude>
</ffi:list>
<ffi:setProperty name="RegisterCategories" property="Filter" value="PARENT_CATEGORY=-1,ID!0,AND" />
<ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />

<ffi:setProperty name="RegisterCategoriesCopy" property="Filter" value="PARENT_CATEGORY!-1" />
<ffi:setProperty name="RegisterCategoriesCopy" property="FilterSortedBy" value="NAME" />
<div id="multipleDIV">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td class="mainfontBold" width="1%" align="left"><s:text name="jsp.account_46"/></td>
		<td width="1%">&nbsp;</td>
		<td class="mainfontBold" width="1%"><s:text name="jsp.account_218"/></td>
		<td width="1%">&nbsp;</td>
		<td class="mainfontBold" width="96%" align="left"><s:text name="jsp.default_45"/></td>
	</tr>
<ffi:object id="Paging" name="com.ffusion.beans.util.Paging" scope="session"/>
<ffi:setProperty name="Paging" property="Pages" value="5" />
<ffi:list collection="Paging" items="PageNum">
<ffi:setProperty name="Math" property="Value1" value="${PageNum}" />
<ffi:setProperty name="Math" property="Value2" value="1" />
<ffi:setProperty name="CurrentID" value="${Math.Subtract}"/>
<%String Current = null; %>
<ffi:getProperty name="CurrentID" assignTo="Current"/>
	<tr>
		<td height="20" align="left">

		<ffi:setProperty name="RegisterTransaction" property="Current" value="${CurrentID}" />
		<%if(Current.equalsIgnoreCase("1")){%>
		<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select1 %>" />
		<%}else if(Current.equalsIgnoreCase("2")){ %>
		<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select2 %>" />
		<%}else if(Current.equalsIgnoreCase("3")){ %>
		<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select3 %>" />
		<%}else if(Current.equalsIgnoreCase("4")){ %>
		<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=select4 %>" />
		<%}else{ %>
		<ffi:setProperty name="RegisterTransaction" property="RegisterCategoryId" value="<%=RegisterCategoryId %>" />
		<%} %>
        <ffi:setProperty name="Compare" property="Value1" value="${RegisterTransaction.RegisterCategoryId}" />

			<select class="txtbox" name="RegisterTransaction.Current=<ffi:getProperty name="CurrentID"/>&RegisterTransaction.RegisterCategoryId" onchange="ns.account.selectMultipleCategoriesOnchange()">
				<option value="-1"><s:text name="jsp.account_185"/></option>
				<ffi:setProperty name="Compare" property="Value2" value="0" />

				<option value="0" <ffi:getProperty name="selected${Compare.Equals}"/>><s:text name="jsp.default_445"/></option>
                <ffi:cinclude value1="${Compare.Equals}" value2="true">
	             <ffi:setProperty name="SelectedType" value="3"/>
                </ffi:cinclude>
				<ffi:setProperty name="RegisterCategories" property="Filter" value="PARENT_CATEGORY=-1,ID!0,AND" />
                <ffi:setProperty name="RegisterCategories" property="FilterSortedBy" value="NAME" />
               <ffi:list collection="RegisterCategories" items="Category">
               <ffi:setProperty name="Compare" property="Value2" value="${Category.Id}" />

			   <option value="<ffi:getProperty name="Category" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="Category" property="Name"/></option>
               <ffi:cinclude value1="${Compare.Equals}" value2="true">
	           <ffi:setProperty name="SelectedType" value="${Category.Type}"/>
               </ffi:cinclude>
				<ffi:setProperty name="RegisterCategoriesCopy" property="Filter" value="PARENT_CATEGORY=${Category.Id}" />
               <ffi:setProperty name="RegisterCategoriesCopy" property="FilterSortedBy" value="NAME" />
               <ffi:list collection="RegisterCategoriesCopy" items="Category1">
				<ffi:setProperty name="Compare" property="Value2" value="${Category1.Id}" />
				<ffi:cinclude value1="${Compare.Equals}" value2="true">
					<ffi:setProperty name="SelectedType" value="${Category1.Type}"/>
				</ffi:cinclude>

				<option value="<ffi:getProperty name="Category1" property="Id"/>"<ffi:getProperty name="selected${Compare.Equals}"/>>&nbsp;&nbsp;<ffi:getProperty name="Category" property="Name"/>: <ffi:getProperty name="Category1" property="Name"/></option>
				</ffi:list>
              </ffi:list>

			</select>
		</td>
		<td class="sectionsubhead">&nbsp;</td>
		<td class="mainfont">
			<ffi:cinclude value1="${SelectedType}" value2="2" operator="equals">
				<s:text name="jsp.account_106"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${SelectedType}" value2="3" operator="equals">
				<s:text name="jsp.account_84"/>
			</ffi:cinclude>
			<ffi:cinclude value1="${SelectedType}" value2="" operator="equals">
				<s:text name="jsp.default_296"/>
			</ffi:cinclude>
			<ffi:removeProperty name="SelectedType"/>
		</td>
		<td class="sectionsubhead">&nbsp;</td>
		<td class="mainfont" align="left">
			<%-- Add the amount entry from single category page into the first category. Only do this if the category amount has not been set yet! --%>
			<ffi:cinclude value1="${CurrentID}${RegisterTransaction.Amount}" value2="0" operator="equals">
				<ffi:setProperty name="totalAmt" value="${AddRegisterTransaction.TotalAmount}" />
			</ffi:cinclude>
			<ffi:cinclude value1="${CurrentID}${RegisterTransaction.Amount}" value2="0" operator="notEquals">
				<ffi:cinclude value1="${AddRegisterTransaction.TotalAmountCurrency}" value2="" operator="notEquals">
			
					<%-- Set the currencycode so it displays the number correctly on refresh --%>
				
					<ffi:setProperty name="RegisterTransaction" property="AmountValue.CurrencyCode" value="${AddRegisterTransaction.TotalAmountCurrency}" />
				</ffi:cinclude>
				<ffi:setProperty name="totalAmt" value="${RegisterTransaction.AmountValue.CurrencyStringNoSymbolNoComma}" />
			</ffi:cinclude>
			<%if(Current.equalsIgnoreCase("0")){ 
			     if(flag!=null&&flag.equalsIgnoreCase("multiOnchange")){%>
			<ffi:setProperty name="totalAmt" value="<%=amount0 %>"/>
		    <%}}else if(Current.equalsIgnoreCase("1")){%>
		    <ffi:setProperty name="totalAmt" value="<%=amount1 %>"/>
		    <%}else if(Current.equalsIgnoreCase("2")){ %>
		    <ffi:setProperty name="totalAmt" value="<%=amount2 %>"/>
		    <%}else if(Current.equalsIgnoreCase("3")){ %>
		    <ffi:setProperty name="totalAmt" value="<%=amount3 %>"/>
		    <%}else if(Current.equalsIgnoreCase("4")){ %>
		    <ffi:setProperty name="totalAmt" value="<%=amount4 %>"/>
		    <%} %>
			<input class="ui-widget ui-widget-content ui-corner-all" type="text" name="RegisterTransaction.Current=<ffi:getProperty name="CurrentID"/>&RegisterTransaction.Amount" size="22" onChange="addCurrency(document.AddTransaction,'true')" value="<ffi:getProperty name='totalAmt'/>" >
			<ffi:removeProperty name="totalAmt"/>
		</td>
	</tr>
	<tr><td colspan="3" height="5"></td></tr>
</ffi:list>

	<tr>
		<td width="1%"><input type="hidden" name="categoryspan"><span id="categoryspanError"></span></td>
		<td width="1%">&nbsp;</td>
		<td width="1%">&nbsp;</td>
		<td class="sectionsubhead" align="right"width="1%" ><s:text name="jsp.default_431"/>&nbsp;</td>
		<td width="96%" align="left"><input class="ui-widget ui-widget-content ui-corner-all" type="text" name="multiTotal" size="22" onFocus="blur();" disabled></td>
	</tr>
</table>
</div>
<input type="hidden" name="AddRegisterTransaction.SplitCategories" value="true">
<script>addCurrency(document.AddTransaction,'true');</script>
