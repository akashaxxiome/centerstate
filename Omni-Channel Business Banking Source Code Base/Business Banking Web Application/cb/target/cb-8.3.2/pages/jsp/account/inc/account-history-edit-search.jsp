<%@ page import="java.util.*"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<%
	session.setAttribute("ReportName", request.getParameter("name"));
	session.setAttribute("ReportDesc", request.getParameter("description"));
%>

<ffi:object name="com.ffusion.tasks.reporting.ModifyReport" id="ModifyReport" scope="session" />
<ffi:setProperty name="ModifyReport" property="ReportGenerated" value="false"/>
<ffi:setProperty name="ModifyReport" property="IdentificationID" value="${ReportID}"/>
<ffi:setProperty name="ModifyReport" property="IdentificationName" value="${ReportName}"/>
<ffi:setProperty name="ModifyReport" property="IdentificationDesc" value="${ReportDesc}"/>

<ffi:setProperty name="ModifyReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_TYPE %>" />
<ffi:setProperty name="ModifyReport" property="ReportOptionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY %>" />
<ffi:setProperty name="ModifyReport" property="ReportOptionName" value="<%= com.ffusion.beans.reporting.ReportCriteria.OPT_RPT_CATEGORY %>" />
<ffi:setProperty name="ModifyReport" property="ReportOptionValue" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.RPT_TYPE_CONSUMER_ACCT_HISTORY %>" />

<%
	session.setAttribute("FFIModifyReport", session.getAttribute("ModifyReport"));
%>

<s:include value="/pages/jsp/account/inc/save-search-init.jsp"/>
