<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

<%
   session.setAttribute("whichCat",request.getParameter("whichCat"));
   session.setAttribute("newTrans",request.getParameter("newTrans"));
   session.setAttribute("ExistingPayee",request.getParameter("ExistingPayee"));
   session.setAttribute("NewPayee",request.getParameter("NewPayee"));
   session.setAttribute("NewChecked",request.getParameter("checkbox1"));

   String accountID = request.getParameter("AddRegisterTransaction.AccountID");
   String regiType = request.getParameter("RegisterTransaction.Type");
   String refNo = request.getParameter("RegisterTransaction.ReferenceNumber");
   String dateIssued = request.getParameter("RegisterTransaction.DateIssued");
   String memo = request.getParameter("RegisterTransaction.Memo");
   String totalAmount = request.getParameter("AddRegisterTransaction.TotalAmount");

%>

<ffi:setProperty name="AddRegisterTransaction" property="AccountID" value="<%=accountID %>"/>
<ffi:setProperty name="RegisterTransactionRegisterType" value="<%=regiType %>"/>
<ffi:setProperty name="RegisterTransaction" property="ReferenceNumber" value="<%=refNo %>"/>
<ffi:setProperty name="RegisterTransaction" property="DateIssued" value="<%=dateIssued %>"/>
<ffi:setProperty name="RegisterTransaction" property="Memo" value="<%=memo %>"/>
<ffi:setProperty name="RegisterTransaction" property="Type" value="<%=regiType %>"/>
<ffi:setProperty name="AddRegisterTransaction" property="TotalAmount" value="<%=totalAmount%>"/>
<ffi:object name="com.ffusion.tasks.register.GetRegisterTransactionTypes" id="GetRegisterTransactionTypes" scope="session"/>
<ffi:process name="GetRegisterTransactionTypes"/>

<%--
page property "whichCat" only set when the user clicks on "Single/Multiple" button
This means for every time the user enters add record page a new AddRegisterTransaction is created
--%>
<ffi:cinclude value1="" value2="whichCat" operator="equals">
	<%-- if the new Trans variable is set to false, we should not load a new transaction --%>
	<ffi:cinclude value1="newTrans" value2="false" operator="notEquals">
		<ffi:object name="com.ffusion.tasks.register.AddRegisterTransaction" id="AddRegisterTransaction" scope="session"/>
	</ffi:cinclude>
</ffi:cinclude>

<ffi:removeProperty name="newTrans"/>

<script type="text/javascript" language="javascript">

elementNum=14; // the element number of the subcategory select box

function preselect(selectedIdx) {
	selectedPayee=document.AddTransaction.ExistingPayee.options[selectedIdx].value;
	payees=new Array();
	<ffi:setProperty name="RegisterPayees" property="Filter" value="All" />
<ffi:list collection="RegisterPayees" items="RegisterPayee1">
	payees[payees.length]=new Array(["<ffi:getProperty name="RegisterPayee1" property="Name"/>"],["<ffi:getProperty name="RegisterPayee1" property="DefaultCategory"/>"]);
	</ffi:list>
	catToSelect=0;
	for (i=0; i<payees.length; i++) {
		if (selectedPayee==payees[i][0]) {
			catToSelect=payees[i][1];
		}
	}
	for (i=0; i<document.AddTransaction.elements[elementNum + 1].length; i++) {
		if (document.AddTransaction.elements[elementNum + 1].options[i].value==catToSelect) {
			document.AddTransaction.elements[elementNum + 1].selectedIndex=i;
		}
	}
}

function setAction(action) {
	var urlString = action;
	   $.ajax({
	  		  type: "post",
	  		  url: urlString,
	  		  data: $('#AddTransaction').serialize(),
	  		  success: function(data) {
	  			$('#accountRegisterDashbord').html(data);
	  		  }
	  	});
}

function setPayeeName() {
	if (document.AddTransaction.checkbox1.checked == true) {
		document.AddTransaction["RegisterPayee.Name"].value = document.AddTransaction["RegisterTransaction.PayeeName"].value;
	}
}

function clearPayee(which) {
	// This function clears either the existing Payee or New Payee field, depending on which one is being changed.
	// It ALSO copies the value of whichever box we are using into the hidden form value for the name we are
	// submitting.
	if (which == "NewPayee") {
		document.AddTransaction.checkbox1.checked=false;
		document.AddTransaction["RegisterTransaction.PayeeName"].value = document.AddTransaction.ExistingPayee.options[document.AddTransaction.ExistingPayee.selectedIndex].value;
		document.AddTransaction.NewPayee.value = "";
	} else {
		document.AddTransaction["RegisterTransaction.PayeeName"].value = document.AddTransaction.NewPayee.value;
		$("#ExistingPayee").selectmenu("value","Select Payee or Payor");
	}
	setPayeeName();
	flipCheck();
}

function flipCheck() {
	/*
		This function changes the value of NewChecked to true or false depending on the state of the checkbox.
		We do this so we can keep track of the state if we have to reload the page.
	*/
	if (document.AddTransaction.checkbox1.checked == true) {
		document.AddTransaction.NewChecked.value = "true";
	} else {
		document.AddTransaction.NewChecked.value = "false";
	}
}

function checkDate() {
	if (document.AddTransaction["RegisterTransaction.DateIssued"].value == "") {
		document.AddTransaction["RegisterTransaction.DateIssued"].value = "<ffi:getProperty name="GetCurrentDate" property="Date"/>";
	}
}

function getCurrencyforAccount( FromAccountID ) {
// Find the currency code for the selected From Account ID
<ffi:list collection="BankingAccounts" items="Accounts">
	if (FromAccountID == '<ffi:getProperty name="Accounts" property="ID"/>') {
		return '<ffi:getProperty name="Accounts" property="CurrencyCode"/>';
	}
</ffi:list>
	return 'USD';
}

function setAccountCurrencyType( ) {

	document.AddTransaction["AddRegisterTransaction.TotalAmountCurrency"].value = getCurrencyforAccount( document.AddTransaction["AddRegisterTransaction.AccountID"].value );
}

CSStopExecution=false;
function CSAction(array) {return CSAction2(CSAct, array);}
function CSAction2(fct, array) {
	var result;
	for (var i=0;i<array.length;i++) {
		if(CSStopExecution) return false;
		var aa = fct[array[i]];
		if (aa == null) return false;
		var ta = new Array;
		for(var j=1;j<aa.length;j++) {
			if((aa[j]!=null)&&(typeof(aa[j])=="object")&&(aa[j].length==2)){
				if(aa[j][0]=="VAR"){ta[j]=CSStateArray[aa[j][1]];}
				else{if(aa[j][0]=="ACT"){ta[j]=CSAction(new Array(new String(aa[j][1])));}
				else ta[j]=aa[j];}
			} else ta[j]=aa[j];
		}
		result=aa[0](ta);
	}
	return result;
}
CSAct = new Object;
function CSClickReturn () {
	var bAgent = window.navigator.userAgent;
	var bAppName = window.navigator.appName;
	if ((bAppName.indexOf("Explorer") >= 0) && (bAgent.indexOf("Mozilla/3") >= 0) && (bAgent.indexOf("Mac") >= 0))
		return true; // dont follow link
	else return false; // dont follow link
}


CSAct = new Object;
function CSOpenWindow(action, param) {
	var wf = "";
	wf = wf + "width=" + action[3];
	wf = wf + ",height=" + action[4];
	wf = wf + ",resizable=" + (action[5] ? "yes" : "no");
	wf = wf + ",scrollbars=" + (action[6] ? "yes" : "no");
	wf = wf + ",menubar=" + (action[7] ? "yes" : "no");
	wf = wf + ",toolbar=" + (action[8] ? "yes" : "no");
	wf = wf + ",directories=" + (action[9] ? "yes" : "no");
	wf = wf + ",location=" + (action[10] ? "yes" : "no");
	wf = wf + ",status=" + (action[11] ? "yes" : "no");
	if (param == null) {
		window.open(action[1], action[2], wf);
	} else {
		window.open(action[1] + "?" + param, action[2], wf);
	}
}

CSAct[/*CMP*/ 'B9E611E8165'] = new Array(CSOpenWindow,/*URL*/ '<ffi:getProperty name='SecurePath'/>calendar.jsp?calDirection=back&calForm=AddTransaction&calTarget=RegisterTransaction.DateIssued','',350,300,false,false,false,false,false,false,false);

    $(function(){
    	$("#AddRegisterTransactionAccountID").selectmenu({width: 'auto'});
    	$("#RegisterTransactionType").selectmenu({width: 'auto'});
     	$("#ExistingPayee").selectmenu({width: 'auto'});
        });
    ns.account.muiltiButtionSubmit = function(whichCat){
        document.AddTransaction.action="/cb/pages/jsp/account/register-rec-add.jsp?whichCat="+whichCat;
        }
    // whent the input text is disabled , it can not be submitted
    ns.account.fillTotalAmount = function(){
        var a = $("#AddRegisterTransactionTotalAmount").val();
        $("#multiTotalAmount").val(a);
        }
</script>

<div id="register_rec_addID" align=center>
<ffi:help id="account_register-rec-add-wait" />
<% String v152_4a = ""; String v152_2b = ""; %>

<ffi:setProperty name="RecordType" value="Add"/>
<ffi:setProperty name="RecButtonType" value="add"/>

<s:form id="AddTransaction" name="AddTransaction" method="post" action="AddRegisterTransactionAction"  namespace="/pages/jsp/account" theme="simple">
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<input type="hidden" name="obsolete">
<input type="hidden" name="RegisterTransaction.PayeeName" value="<ffi:getProperty name="RegisterTransaction" property="PayeeName"/>">
<input type="hidden" name="RegisterPayee.Name" value="<ffi:getProperty name="RegisterPayee" property="Name"/>">
<input type="hidden" name="RegisterPayee.DefaultCategory" value="<ffi:getProperty name="RegisterPayee" property="DefaultCategory"/>">
<input type="hidden" name="NewChecked" value="<ffi:getProperty name="NewChecked"/>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center" class="sectionhead_grey">

		<table width="96%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<ffi:cinclude value1="${LoadFromTransferTemplate}" value2="" operator="notEquals">
				&nbsp;(<s:text name="jsp.account_115"/> &quot;<ffi:getProperty name="LoadFromTransferTemplate"/>&quot;)
				</ffi:cinclude>
			</tr>
			<tr>
				<td colspan="4" class="sectionhead" height="10">&nbsp;</td>
			</tr>
			<%-- Set default selected account --%>
			<% String acctID=null; %>
			<ffi:getProperty name="AddRegisterTransaction" property="AccountID" assignTo="acctID"/>
			<%
			if( acctID==null || acctID.length()<=0 ){
			%>
				<%-- No account in object AddRegisterTransaction, then look at default account for Account Register --%>
				<ffi:getProperty name="<%= com.ffusion.tasks.register.Task.ACCOUNT %>" property="ID" assignTo="acctID"/>
			<%
			}
			%>
			<tr>
				<td class="sectionhead" colspan="4" height="20" align="left">
					<s:text name="jsp.default_15"/>
					&nbsp;
					<select id="AddRegisterTransactionAccountID" class="txtbox" name="AddRegisterTransaction.AccountID" onchange="setAccountCurrencyType(); <ffi:cinclude value1="${whichCat}" value2="multi">addCurrency( document.AddTransaction, 'true' );</ffi:cinclude>">
						<ffi:list collection="BankingAccounts" items="acct">
							<ffi:cinclude value1="${acct.TypeValue}" value2="14" operator="notEquals">
								<option <ffi:cinclude value1="<%= acctID %>" value2="${acct.ID}" operator="equals">selected</ffi:cinclude> value='<ffi:getProperty name="acct" property="ID"/>'>
									<ffi:getProperty name="acct" property="RoutingNum"/> :
									<ffi:getProperty name="acct" property="DisplayText"/>
									<ffi:cinclude value1="${acct.NickName}" value2="" operator="notEquals" >
										 - <ffi:getProperty name="acct" property="NickName"/>
									</ffi:cinclude>
									 - <ffi:getProperty name="acct" property="CurrencyCode"/>
								</option>
							</ffi:cinclude>
						</ffi:list>
					</select>
				</td>
			</tr>
			<tr>
				<td class="sectionhead" align="left"><s:text name="jsp.default_444"/><span class="required">*</span></td>
				<td class="sectionhead"><s:text name="jsp.default_347"/></td>
				<td class="sectionhead"><s:text name="jsp.account_110"/><span class="required">*</span></td>
				<td class="sectionhead"><s:text name="jsp.account_70"/></td>
				<td class="sectionhead"><s:text name="jsp.default_43"/><span class="required">*</span></td>
			</tr>
			<tr>
				<td align="left">
					<ffi:setProperty name="RegisterTransactionTypes" property="SortedBy" value="TYPE_NAME" />
					<ffi:setProperty name="CategoryChangeURL" value="/cb/pages/jsp/account/register-rec-add.jsp?newTrans=false"/>
					<select id="RegisterTransactionType" class="txtbox" name="RegisterTransaction.Type" onchange="setAction('<ffi:getProperty name="CategoryChangeURL"/>');">
						<ffi:removeProperty name="CategoryChangeURL"/>
						<ffi:setProperty name="Compare" property="Value1" value="${RegisterTransactionRegisterType}" />
						<ffi:list collection="RegisterTransactionTypes" items="Type">
							<ffi:setProperty name="Compare" property="Value2" value="${Type.TYPE_ID}" />
						    <option value="<ffi:getProperty name="Type" property="TYPE_ID"/>" <ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="Type" property="TYPE_NAME"/> (<ffi:getProperty name="Type" property="TYPE_ABBR"/>)</option>
						</ffi:list>
					</select>
				</td>
				<td>
					<input class="ui-widget-content ui-corner-all" type="text" size="10" maxlength="20" name="RegisterTransaction.ReferenceNumber" value="<ffi:getProperty name="RegisterTransaction" property="ReferenceNumber"/>"><span id="RegisterTransaction.ReferenceNumberError"></span>
				</td>
				<td><ffi:setProperty name="RegisterTransaction" property="DateFormat" value="${UserLocale.DateFormat}" />
				<sj:datepicker value="%{#session.RegisterTransaction.DateIssued}" name="RegisterTransaction.DateIssued" maxlength="10" buttonImageOnly="true" cssClass="ui-widget-content ui-corner-all"/><span id="RegisterTransaction.DateIssuedError"></span>
				</td>
				<td class='mainfont'>
					<% com.ffusion.beans.register.RegisterTransaction regTran = (com.ffusion.beans.register.RegisterTransaction)session.getAttribute( "RegisterTransaction" ); %>
					<% if ( com.ffusion.beans.util.RegisterUtil.isTransactionCreditType( regTran.getRegisterTypeValue() ) ) { %>
						<s:text name="jsp.default_120"/>
					<% } else {%>
						<s:text name="jsp.default_146"/>
					<% } %>
				</td>
				<td class="columndata">
					<input id="AddRegisterTransactionTotalAmount" class="ui-widget-content ui-corner-all" type="text" name="AddRegisterTransaction.TotalAmount" maxlength="53" value="<ffi:getProperty name="AddRegisterTransaction" property="TotalAmount"/>" <ffi:cinclude value1="${whichCat}" value2="multi">disabled</ffi:cinclude>><span id="AddRegisterTransaction.TotalAmountError"></span>
					<%--when it is disabled , it can not be submitted to the action  --%>
					<input type="hidden" id="multiTotalAmount" name="multiTotalAmount"/>
					<input type="hidden" name="AddRegisterTransaction.TotalAmountCurrency" value="<ffi:cinclude value1="${AddRegisterTransaction.TotalAmountCurrency}" value2="" operator="equals"><ffi:getProperty name="Account" property="CurrencyCode"/></ffi:cinclude><ffi:cinclude value1="${AddRegisterTransaction.TotalAmountCurrency}" value2="" operator="notEquals"><ffi:getProperty name="AddRegisterTransaction" property="TotalAmountCurrency"/></ffi:cinclude>">

				</td>
			</tr>
			<tr>
				<td colspan="4" class="sectionhead" align="left"><s:text name="jsp.default_279"/></td>
			</tr>
			<tr>
				<td colspan="4" align="left">
					<input class="ui-widget-content ui-corner-all" type="text" size="75" maxlength="255" name="RegisterTransaction.Memo" value="<ffi:getProperty name="RegisterTransaction" property="Memo"/>"><span id="RegisterTransaction.MemoError"></span>
				</td>
			</tr>
			<tr>
				<td class="sectionhead" align="left"><s:text name="jsp.account_83"/></td>
				<td class="sectionhead" colspan="3" align="left"><s:text name="jsp.account_145"/></td>
			</tr>
			<tr>
				<td class="mainfont" align="left">
				<ffi:setProperty name="Compare" property="Value1" value="${ExistingPayee}" />
                    <div id="changeSelectStatus">
					<select id="ExistingPayee" class="txtbox" name="ExistingPayee" onChange="clearPayee('NewPayee');preselect(this.selectedIndex);">
						<option value=""><s:text name="jsp.account_187"/></option>
						<ffi:setProperty name="RegisterPayees" property="Filter" value="All" />
		<ffi:setProperty name="RegisterPayees" property="SortedBy" value="NAME" />
		<ffi:list collection="RegisterPayees" items="RegisterPayee1">
		<ffi:setProperty name="Compare" property="Value2" value="${RegisterPayee1.Name}" />

						<option value="<ffi:getProperty name="RegisterPayee1" property="Name"/>" <ffi:getProperty name="selected${Compare.Equals}"/>><ffi:getProperty name="RegisterPayee1" property="Name"/></option>
						</ffi:list>

					</select>
					</div>
				</td>
				<td class="columndata" colspan="3" align="left">
				<ffi:setProperty name="checkedtrue" value="checked"/>
				<ffi:setProperty name="checkedfalse" value=""/>
				<ffi:setProperty name="Compare" property="Value1" value="${NewChecked}" />
				<ffi:setProperty name="Compare" property="Value2" value="checked" />
					<input class="ui-widget-content ui-corner-all" type="text" size="30" maxlength="40" name="NewPayee" onChange="clearPayee('ExistingPayee')" value="<ffi:getProperty name="NewPayee"/>"><span id="NewPayeeError"></span>
					<input type="checkbox" name="checkbox1" onClick="setPayeeName();flipCheck()" value="true" <ffi:getProperty name="checked${NewChecked}"/>> <s:text name="jsp.account_14"/>
			</td>
			</tr>
			<ffi:setProperty name="whichCatfalse" value="single"/>
			<ffi:setProperty name="whichCattrue" value="${whichCat}"/>
			<ffi:setProperty name="StringUtil" property="Value1" value="${whichCat}" />
			<ffi:setProperty name="StringUtil" property="Value2" value="" />
			<ffi:getProperty name="whichCat${StringUtil.NotEquals}" assignTo="v152_4a"/>
			<ffi:setProperty name="whichCat" value="<%= v152_4a %>"/>
			<tr>
				<td class="columndata" colspan="4">
					<s:include value="/pages/jsp/account/inc/register-rec-add-%{#session.whichCat}.jsp" />
				</td>
			</tr>
			<tr>
				<td class="colspan" colspan="4" valign="top" height="20" align="left">

				<ffi:cinclude value1="${whichCat}" value2="multi">
				     <sj:a
                       formIds="AddTransaction"
                       button="true"
                       targets="accountRegisterDashbord"
                       onclick="ns.account.muiltiButtionSubmit('single#cat')"
                       ><s:text name="jsp.account_221"/></sj:a>
				</ffi:cinclude>
				<ffi:cinclude value1="${whichCat}" value2="single">
				     <sj:a
                       formIds="AddTransaction"
                       button="true"
                       targets="accountRegisterDashbord"
                       onclick="ns.account.muiltiButtionSubmit('multi#cat')"
                       ><s:text name="jsp.account_220"/></sj:a>
				</ffi:cinclude>
				</td>
			</tr>
			<tr>
				<td class="sectionhead" colspan="4" align="center" height="20">&nbsp;</td>
			</tr>

    <ffi:setProperty name="checkAmount_acctCollectionName" value="BankingAccounts"/>
			<tr>
				<td align="center" colspan="4"><span class="required">* <s:text name="jsp.default_240"/></span></td>
			</tr>
			<tr>
				<td colspan="4"><br></td>
			</tr>
			<tr>
				<td class="sectionhead" colspan="4" align="center">
					<script>
						ns.account.cancleButtonRedirectRegisteURL = "<ffi:urlEncrypt url='/cb/pages/jsp/account/register-wait.jsp'/>";
					</script>
					 <sj:a
					    id="cancelrecReport"
						button="true"
						onclick="ns.account.cancleButtonRedirectRegiste(ns.account.cancleButtonRedirectRegisteURL)"
						><s:text name="jsp.default_82"/></sj:a>
				&nbsp;&nbsp;
				 <sj:a
                       formIds="AddTransaction"
                       button="true"
                       targets="accountRegisterDashbord"
                       onclick="ns.account.fillTotalAmount()"
                       validateFunction="customValidation"
                       validate="true"
                       ><s:text name="jsp.default_29"/></sj:a>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
</s:form>
<ffi:setProperty name="RegisterCategories" property="Filter" value="All" />

<%-- ================= MAIN CONTENT END ================= --%>
<script>checkDate()</script>
</div>
    <%
		session.setAttribute("FFIAddRegisterTransaction", session.getAttribute("AddRegisterTransaction"));
	%>

<script>

		$('#register_rec_addID').portlet({
			generateDOM: true,
			helpCallback: function(){

				var helpFile = $('#register_rec_addID').find('.moduleHelpClass').html();
				callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
			}

		});
			$('#register_rec_addID').portlet('title', js_acc_addrecord_portlet_title);
</script>
