<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<script type="text/javascript" language="javascript">
   $(function(){
      $("#navigatorButton").hide();
	   });

</script>
<div align=center>

<s:form name="AddTransaction" method="post" action='<ffi:getProperty name="SecurePath"/>account/register-rec-add-save-wait.jsp'>
<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="11"><br>
			<br>
		</td>
		<td align="center" class="sectionhead_grey">

		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td class="sectionhead tbrd_b" colspan="4">&gt; <s:text name="jsp.account_148"/></td>
			</tr>
			<tr>
				<td colspan="4" class="sectionhead" height="10">&nbsp;</td>
			</tr>
			<tr>
				<td class="columndata" colspan="4" height="20">
					<s:text name="jsp.account_150"/>
				</td>
			</tr>
		</table>
		</td>
		<td align="right" width="11"><br>
			<br>
		</td>
	</tr>

</table>
</s:form>
</div>