<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>
<%@ page import="com.ffusion.tasks.util.GetDatesFromDateRange"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<%@ page import="com.ffusion.efs.tasks.SessionNames"%>
<%@ page import="com.ffusion.tasks.util.BuildIndex"%>
<%@ page import="com.sap.banking.accountconfig.constants.AccountConstants"%>

 <s:include value="/pages/jsp/account/inc/accounthistory_common.jsp" />  

<ffi:setProperty name="separator" value="-----------------" />

<%
	session.setAttribute("loadSavedSearch",request.getParameter("loadSavedSearch"));
	session.setAttribute("editSavedSearch",request.getParameter("editSavedSearch"));
	session.setAttribute("CashFlowStartDate",request.getParameter("StartDate"));
	session.setAttribute("CashFlowEndDate",request.getParameter("EndDate"));
%>
<s:set name="dataPrevDay" value="@com.sap.banking.accountconfig.constants.AccountConstants@DATA_CLASSIFICATION_PREVIOUSDAY"/>
<s:set name="dataIntraDay" value="@com.sap.banking.accountconfig.constants.AccountConstants@DATA_CLASSIFICATION_INTRADAY"/>
<!-- setting account type properties -->
<s:set name="depositAccounts" value="@com.sap.banking.accountconfig.constants.AccountConstants@DEPOSIT_ACCOUNTS"/>
<s:set name="assetAccounts" value="@com.sap.banking.accountconfig.constants.AccountConstants@ASSET_ACCOUNTS"/>
<s:set name="creditCards" value="@com.sap.banking.accountconfig.constants.AccountConstants@CREDIT_CARDS"/>
<s:set name="loanAccounts" value="@com.sap.banking.accountconfig.constants.AccountConstants@LOAN_ACCOUNTS"/>
<s:set name="otherAccounts" value="@com.sap.banking.accountconfig.constants.AccountConstants@OTHER_ACCOUNTS"/>
<!-- setting ZBA Flag property. -->
<s:set name="zbaNoneConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@ZBA_NONE"/>
<s:set name="zbaCreditConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@ZBA_CREDIT_ONLY"/>
<s:set name="zbaDebitConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@ZBA_DEBIT_ONLY"/>
<s:set name="zbaBothConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@ZBA_BOTH"/>
<s:set name="businessAppConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@ENT_GROUP_BUSINESS"/>
<!-- Date Range Constants. -->
<s:set name="todayConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@TODAY"/>
<s:set name="currentDayConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@CURRENT_DAY"/>
<s:set name="yesterdayConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@YESTERDAY"/>
<s:set name="currentWeekConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@CURRENT_WEEK"/>
<s:set name="lastWeekConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@LAST_WEEK"/>
<s:set name="sevenDaysConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@LAST_7_DAYS"/>
<s:set name="thirtyDaysConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@LAST_30_DAYS"/>
<s:set name="sixtyDaysConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@LAST_60_DAYS"/>
<s:set name="ninetyDaysConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@LAST_90_DAYS"/>
<s:set name="monthToDateConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@THIS_MONTH_TO_DATE"/>
<s:set name="currentMonthConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@SEARCH_CRITERIA_VALUE_CURRENT_MONTH"/>
<s:set name="lastMonthConst" value="@com.sap.banking.accountconfig.constants.AccountConstants@LAST_MONTH"/>
<s:set name="defaultTransType" value="@com.sap.banking.accountconfig.constants.AccountConstants@ALL_TRANS_TYPE"/>
<s:set name="allTrans" value="@com.sap.banking.accountconfig.constants.AccountConstants@ALL_TRANS_TYPE"/>
<s:set name="allCreditTrans" value="@com.sap.banking.accountconfig.constants.AccountConstants@ALL_CREDIT_TRANSACTIONS"/>
<s:set name="allDebitsTrans" value="@com.sap.banking.accountconfig.constants.AccountConstants@ALL_DEBIT_TRANSACTIONS"/>

<ffi:cinclude value1="${loadSavedSearch}" value2="true"	operator="equals">
	<s:include value="/pages/jsp/account/inc/account-history-load-search.jsp" />
</ffi:cinclude>
<ffi:cinclude value1="${editSavedSearch}" value2="true"	operator="equals">
	<s:include value="/pages/jsp/account/inc/account-history-load-search.jsp" />
</ffi:cinclude>

<style>

#startDate {
	width: 50px;
}

#endDate {
	width: 50px;
}
</style>

<script type="text/javascript">
<!--
	$(function() {
		$("#reportOn").selectmenu({
			width : 130
		});
		$("#accountGroupID").selectmenu({
			width : 130
		});
		// $("#corporateAccountID").combobox({size:38});
		
		var accountGrpID = $("#accountGroupID").val() || "";
		
		 $("#corporateAccountID").lookupbox({
			"source":"/cb/pages/jsp/account/AccountHistoryLookupBoxAction.action?accountGroupId="+accountGrpID,
			"controlType":"server",
			"collectionKey":"1",
			"size":"35",
			"module":"accounthistory",
			"dataReceived":function(){
                $('#corporateAccountID option:eq(1)').selected();
                var optLabel= $('#corporateAccountID option:selected').text();
                var optValue= $('#corporateAccountID option:selected').val();
                var optionValue = {value:optValue, label:optLabel};
                $('#corporateAccountID').lookupbox("value",optionValue);
			}
		}); 

		$("#zbaDisplay").selectmenu({
			width : 100
		});
		$("#consumerAccountsDropDown").selectmenu({
			width : 350
		});
		$("#dateRangeValue").selectmenu({
			width : 130
		});
		$("select[multiple!='multiple']").removeClass("ui-widget-content");
	});
	

//-->
</script>
<%  session.setAttribute("FFIGetPagedTransactions", session.getAttribute("GetPagedTransactions"));%>
<ffi:setProperty name="urlPrefix" value="/cb"/>
<ffi:cinclude value1="${TransactionSearch}" value2="true" operator="equals">
	<ffi:setProperty name="urlPrefix" value=""/>
</ffi:cinclude>

<ffi:setProperty name="URL1" value="${urlPrefix}/pages/jsp/account/accounthistory_deposit.jsp" />
<ffi:setProperty name="URL2" value="${urlPrefix}/pages/jsp/account/accounthistory_asset.jsp" />
<ffi:setProperty name="URL3" value="${urlPrefix}/pages/jsp/account/accounthistory_loan.jsp" />
<ffi:setProperty name="URL4" value="${urlPrefix}/pages/jsp/account/accounthistory_ccard.jsp" />
<ffi:setProperty name="URL5" value="${urlPrefix}/pages/jsp/account/accounthistory.jsp" /> 
<ffi:removeProperty name="urlPrefix"/>


<div id="search_dashboard">
	<s:url id="exportHistoryUrl" value="/pages/jsp/account/inc/account-history-export-common.jsp">
		<s:param name="CSRF_TOKEN" value="%{#session.CSRF_TOKEN}"/>
	</s:url>
	
	<ffi:setProperty name="ActionName" value="getSearchResults"/>
	<s:if test="%{#session.TransactionSearch !='true'}">
		<ffi:setProperty name="ActionName" value="validateAccountHistory_verify" />
	</s:if>
	<s:form id="accountCriteriaForm" namespace="/pages/jsp/account" validate="false" action="%{#session.ActionName}" method="post" name="FormName" theme="simple" >
		<input type="hidden" name="CSRF_TOKEN" value="<ffi:getProperty name='CSRF_TOKEN'/>" />
		<input type="hidden" name="TSView" value="true" />
		<input type="hidden" name="ResetTSZBADisplay" value="false" />
		<input type="hidden" id="operationFlag" name="operationFlag" value="" />
		<s:hidden id="isTransactionSearch" name="isTransactionSearch" value="%{#session.TransactionSearch}" />
		<s:hidden id="exportHistoryUrlValue" name="exportHistoryUrlValue" value="%{#exportHistoryUrl}" />
		<s:if test="%{#session.TransactionSearch !='true'}">
			<s:hidden id="accountHistorySearchCriteria.account.ID" name="accountHistorySearchCriteria.account.ID" value="" />
			<s:hidden id="accountHistorySearchCriteria.account.bankID" name="accountHistorySearchCriteria.account.bankID" value="" />
			<s:hidden id="accountHistorySearchCriteria.account.routingNum" name="accountHistorySearchCriteria.account.routingNum" value="" />
		</s:if>
		<span id="formerrors"></span>

		<%-- <s:if test="%{#session.editSavedSearch =='true'}"> --%>
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="display:none">
			<tr>
				<td align="right"><span id="savedSearchNameLabelID"
					class="sectionsubhead"><s:text name="jsp.default_283" />:&nbsp;</span>
				</td>
				<td align="left"><input class="ui-widget-content ui-corner-all"
					type="hidden" name="name" size="25" maxlength="40" border="0"
					value="<s:property value="#parameters.reportName"/>">
				</td>
				<input type="hidden" name="oldReportName" value="<s:property value="#parameters.reportName"/>"/>
				<td align="right"><span id="savedSearchDescLabelID"
					class="sectionsubhead"><s:text name="jsp.default_170" />:&nbsp;</span>
				</td>
				<td align="left"><input class="ui-widget-content ui-corner-all"
					type="hidden" name="description" size="25" maxlength="255" border="0"
					value="<s:property value="#parameters.reportDesc"/>">
				</td>
			</tr>
			</table>
			<input type="hidden" name="id" value="<s:property value="#parameters.reportID"/>"/>
		<%-- </s:if> --%>
		
		<ffi:cinclude
			value1="${CheckPerAccountReportingEntitlements.EntitledToDetailPrev}"
			value2="true" operator="notEquals">
			<ffi:cinclude value1="${GetPagedTransactions.DataClassification}"
				value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY%>"
				operator="equals">
				<ffi:setProperty name="GetPagedTransactions"
					property="DataClassification"
					value="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY %>" />
			</ffi:cinclude>
		</ffi:cinclude>
		<ffi:cinclude
			value1="${CheckPerAccountReportingEntitlements.EntitledToDetailIntra}"
			value2="true" operator="notEquals">
			<ffi:cinclude value1="${GetPagedTransactions.DataClassification}"
				value2="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_INTRADAY%>"
				operator="equals">
				<ffi:setProperty name="GetPagedTransactions"
					property="DataClassification"
					value="<%= com.ffusion.tasks.banking.GetPagedTransactions.DATA_CLASSIFICATION_PREVIOUSDAY %>" />
			</ffi:cinclude>
		</ffi:cinclude>

		<s:if test="%{#session.SecureUser.AppType == 'Business'}">
			<ffi:setProperty name="corporateFieldStyle" value="display:block" />
			<ffi:setProperty name="consumerFieldStyle" value="display:none" />
		</s:if> 
		<s:if test="%{#session.SecureUser.AppTyp != 'Business'}">
			<ffi:setProperty name="corporateFieldStyle" value="display:none" />
			<ffi:setProperty name="consumerFieldStyle" value="display:block" />
		</s:if>
		
		<%-- TRANSACTION SERACH INCLUDE. --%>
		<s:if test="%{#session.TransactionSearch=='true'}">
	 		<s:include value="/pages/jsp/account/transaction-dashboard.jsp" />
		</s:if>
		
		<%-- ACCOUNT HISTORY SEARCH INCLUDE. --%>
		<s:if test="%{#session.TransactionSearch !='true'}">
			<s:include value="/pages/jsp/account/account-history-dashboard.jsp" />
		</s:if>
</s:form>
</div>
<script type="text/javascript">
<!--
$(document).ready(function(){
	
	if ($("#viewAccountHistoryID1").length > 0){
		setTimeout(function(){$('#viewAccountHistoryID1').click();}, 50);
	}
	if ($("#viewAccountHistoryID2").length > 0){
		setTimeout(function(){$('#viewAccountHistoryID2').click();}, 50);
	}
	
	var editSavedSearch = '<s:property value="#attr.editSavedSearch"/>';
	var loadSavedSearch = '<s:property value="#attr.loadSavedSearch"/>';
	var reportName = '<s:property value="#parameters.reportName"/>';
	//var reportDesc = '<s:property value="#parameters.reportDesc"/>';
	if(reportName !='undefined' && reportName!=''){
		$('#savedSearchGridId').val(reportName);
	}
	//Description of saved Trxn search dialog & same of criteria of Trxn Search are two different things altogether.Commented below section of code.
	/*if(reportDesc !='undefined' && reportDesc!=''){
		$('input[name="transactionHistorySearchCriteria.description"]').val(reportDesc);
	}*/
	if((editSavedSearch  !='undefined' && editSavedSearch !='') || (loadSavedSearch !='undefined' && loadSavedSearch!='')){
		$('#transactionSearchToggle').trigger('click');
	}
});
//-->
</script>


