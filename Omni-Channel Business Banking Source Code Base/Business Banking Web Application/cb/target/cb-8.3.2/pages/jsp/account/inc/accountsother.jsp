<%@ page import="com.ffusion.beans.user.UserLocale" %>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ page language="java" import="com.ffusion.beans.accounts.Accounts" %>
<%@ page language="java" import="com.ffusion.beans.accounts.AccountSummaries" %>
<script>
	$(function()
	{
		var flag = "<ffi:getProperty name="flag"/>";
		var urlString = "<ffi:urlEncrypt url='/cb/pages/jsp/account/consolidatedbalance_other_grid.jsp?classfication=${AccountBalancesDataClassification}&displayCurrencyCode=${AccountDisplayCurrencyCode}'/>";
		if (flag == "accountbalance") {
			ns.account.refreshOtherAccountSummary(urlString);
		} else {
			ns.account.refreshConsolidatedSummaryForm(urlString);
		}

	});
</script>
<br>
