<%@ page import="com.ffusion.csil.core.common.EntitlementsDefines"%>
<%@ page import="com.ffusion.beans.reporting.ReportConsts"%>
<%@ page import="com.ffusion.beans.reporting.ReportCriteria"%>
<%@ page import="com.ffusion.beans.reporting.ReportIdentifications"%>
<%@ page import="com.ffusion.beans.reporting.ReportIdentification"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
 <%@ page contentType="text/html; charset=UTF-8" %>
<ffi:author page="account-history-load-search.jsp"/>
<ffi:setProperty name="topMenu" value="accounts"/>
<ffi:setProperty name="subMenu" value="history"/>
<ffi:setProperty name="sub2Menu" value="advancedsearch"/>
<ffi:setL10NProperty name="PageTitle" value="Advanced Search"/>

<%
	session.setAttribute("ReportID", request.getParameter("reportID"));
	session.setAttribute("ReportName", request.getParameter("reportName"));
	session.setAttribute("ReportDesc", request.getParameter("reportDesc"));
%>

<%-- <ffi:object id="GetReportFromReports" name="com.ffusion.tasks.reporting.GetReportFromReports"/> --%>
<%-- <ffi:setProperty name="GetReportFromReports" property="ReportsName" value="SavedSearches" /> --%>
<%-- <ffi:setProperty name="GetReportFromReports" property="ReportName" value="ReportData" /> --%>
<%-- <ffi:setProperty name="GetReportFromReports" property="ReportName" value="SavedSearchReport" /> --%>
<%-- <ffi:setProperty name="GetReportFromReports" property="ID" value="${ReportID}" /> --%>
<%-- <ffi:process name="GetReportFromReports" /> --%>
<%-- <ffi:removeProperty name="GetReportFromReports"/> --%>

<ffi:object id="GetReportFromName" name="com.ffusion.tasks.reporting.GetReportFromName"/>
<ffi:setProperty name="GetReportFromName" property="IDInSessionName" value="ReportName" />
<ffi:setProperty name="GetReportFromName" property="ReportsIDsName" value="${ReportName}"/>
<ffi:setProperty name="GetReportFromName" property="ReportName" value="ReportData"/>
<ffi:process name="GetReportFromName" />
<ffi:removeProperty name="GetReportFromName"/>

<ffi:object id="DateConverter" name="com.ffusion.tasks.util.ConvetDateAsPerCurrentLocale"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.util.DateConsts.SEARCH_CRITERIA_DATE_FORMAT%>" />
<ffi:setProperty name="DateConverter" property="formatBeforeConversion" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.util.DateConsts.SEARCH_CRITERIA_START_DATE %>" />
<ffi:setProperty name="DateConverter" property="dateToBeConverted" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
<ffi:process name="DateConverter"/>
<ffi:setProperty name="GetPagedTransactions" property="StartDate" value="${DateConverter.convertedDate}" />
<ffi:setProperty name="DisplayStartDate" value="${GetPagedTransactions.StartDate}"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.util.DateConsts.SEARCH_CRITERIA_END_DATE %>" />
<ffi:setProperty name="DateConverter" property="dateToBeConverted" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}"/>
<ffi:process name="DateConverter"/>
<ffi:setProperty name="GetPagedTransactions" property="EndDate" value="${DateConverter.convertedDate}" />
<ffi:setProperty name="DisplayEndDate" value="${GetPagedTransactions.EndDate}"/>
<ffi:removeProperty name="DateConverter"/>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= ReportConsts.SEARCH_CRITERIA_DATE_RANGE %>" />
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
<ffi:setProperty name="GetPagedTransactions" property="DateRangeValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />


<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_TYPE %>" />
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
<ffi:setProperty name="DisplayTSTransactionType" value="${GetPagedTransactions.SearchCriteriaValue}"/>

<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="equals">
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MIN%>"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MIN %>" />
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
	<ffi:setProperty name="DisplayTSReferenceStart" value="${GetPagedTransactions.SearchCriteriaValue}"/>

	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MAX%>"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_CUST_REF_MAX %>" />
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
	<ffi:setProperty name="DisplayTSReferenceEnd" value="${GetPagedTransactions.SearchCriteriaValue}"/>
</ffi:cinclude>
<ffi:cinclude value1="${SecureUser.AppType}" value2="<%=com.ffusion.csil.core.common.EntitlementsDefines.ENT_GROUP_BUSINESS%>" operator="notEquals">
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MIN%>"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MIN %>" />
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
	<ffi:setProperty name="DisplayTSReferenceStart" value="${GetPagedTransactions.SearchCriteriaValue}"/>
	
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MAX%>"/>
	<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_TRANS_REF_MAX %>" />
	<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
	<ffi:setProperty name="DisplayTSReferenceEnd" value="${GetPagedTransactions.SearchCriteriaValue}"/>
	
</ffi:cinclude>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MIN %>" />
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
<ffi:setProperty name="DisplayTSMinimumAmount" value="${GetPagedTransactions.SearchCriteriaValue}"/>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_AMOUNT_MAX %>" />
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
<ffi:setProperty name="DisplayTSMaximumAmount" value="${GetPagedTransactions.SearchCriteriaValue}"/>

<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaKey" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DESCRIPTION %>"/>
<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_DESCRIPTION %>" />
<ffi:setProperty name="GetPagedTransactions" property="SearchCriteriaValue" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
<ffi:setProperty name="DisplayTSDescription" value="${GetPagedTransactions.SearchCriteriaValue}"/>

<ffi:setProperty name="ReportData" property="ReportCriteria.CurrentSearchCriterion" value="<%= com.ffusion.beans.accounts.GenericBankingRptConsts.SEARCH_CRITERIA_ACCOUNTS %>" />
<ffi:setProperty name="tempAccounts" value="${ReportData.ReportCriteria.CurrentSearchCriterionValue}" />
<%
	String accountList = (String) session.getAttribute ( "tempAccounts" );
	for (StringTokenizer st = new StringTokenizer(accountList, ","); st.hasMoreTokens();) {
	    String aCode = st.nextToken();
	   	StringTokenizer st1 = new StringTokenizer(aCode, ":");
	   	if (st1.hasMoreTokens()) {
	    	String accountID = st1.nextToken();
	    	session.setAttribute( "SearchAccount_" + accountID , "checked" );
	    }
	}
%>

<ffi:removeProperty name="tempAccounts"/>
<ffi:removeProperty name="SavedSearches"/>
<ffi:removeProperty name="deleteSavedSearch"/>



