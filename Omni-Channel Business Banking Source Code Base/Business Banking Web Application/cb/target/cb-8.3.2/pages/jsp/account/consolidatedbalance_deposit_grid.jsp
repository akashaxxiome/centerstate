<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags" %>
<%@ taglib prefix="sjg" uri="/struts-jquery-grid-tags"%>

<%
   String dataClassification = request.getParameter("classfication");
   String displayCurrencyCode = request.getParameter("displayCurrencyCode");
   request.setAttribute("dataClassification",dataClassification);
   request.setAttribute("displayCurrencyCode",displayCurrencyCode);
%>
<div id="consolidatedBalance_deposit_gridID">
<ffi:help id="accountsbygroup" />

<ffi:setGridURL grid="GRID_consolidatedvalanceDeposit" name="LinkURL" url="/cb/pages/jsp/account/InitAccountHistoryAction.action?AccountGroupID=1&AccountID={0}&AccountBankID={1}&AccountRoutingNum={2}&DataClassification={3}&redirection=true&ProcessAccount=true&TransactionSearch=false" parm0="Account.ID" parm1="Account.BankID" parm2="Account.RoutingNum" parm3="DataClassification"/>
<ffi:setProperty name="consolidatedBalanceDepositSummaryGridTempURL" value="/pages/jsp/account/GetConsolidatedBalanceDepositAction.action?collectionName=DepositAcctConsolidatedBalanceSummaries&dataClassification=${dataClassification}&displayCurrencyCode=${displayCurrencyCode}&GridURLs=GRID_consolidatedvalanceDeposit" URLEncrypt="true"/>
<s:url id="depositSummaryURL" value="%{#session.consolidatedBalanceDepositSummaryGridTempURL}" escapeAmp="false"/>
<sjg:grid
     id="consolidatedBalanceDepositSummaryID"
	 sortable="true" 
     dataType="json" 
     href="%{depositSummaryURL}"
     pager="true"
     gridModel="gridModel"
	 rowList="%{#session.StdGridRowList}" 
	 rowNum="%{#session.StdGridRowNum}" 
	 rownumbers="false"
     navigator="true"
	 navigatorAdd="false"
	 navigatorDelete="false"
	 navigatorEdit="false"
	 navigatorRefresh="false"
	 navigatorSearch="false"
	 navigatorView="false"
	 shrinkToFit="true"
	 footerrow="true"
	 userDataOnFooter="true"
	 scroll="false"
	 scrollrows="true"
	 viewrecords="true"
     onGridCompleteTopics="addGridControlsEvents,consolidatedBalanceDepositSummaryEvent"
     >
     <sjg:gridColumn name="account.accountDisplayText" index="NUMBER" title="%{getText('jsp.default_15')}" sortable="true" width="150" cssClass="datagrid_actionColumn" formatter="ns.account.formatAccountColumn"/>
     <sjg:gridColumn name="nameNickNamedeposit" index="ACCNICKNAME" title="%{getText('jsp.default_293')}" sortable="true" width="280" formatter="ns.account.customToAccountNickNameColumn" cssClass="datagrid_textColumn"/> 
     <sjg:gridColumn name="account.bankName" index="BANKNAME" title="%{getText('jsp.default_61')}" sortable="true" width="95"  formatter="ns.account.nameFormatter" cssClass="datagrid_textColumn"/> 
     <sjg:gridColumn name="account.type" index="TYPESTRING" title="%{getText('jsp.default_444')}" sortable="true" width="65" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="summaryDateDisplay" index="SummaryDateDisplay" title="%{getText('jsp.default_51')}" sortable="true" width="60" cssClass="datagrid_dateColumn"/>
     <sjg:gridColumn name="displayOpeningLedger" index="currencyOpeningLedger" title="%{getText('jsp.default_305')}" sortable="true" width="150"  formatter="ns.account.openingLedgerDepositFormatter" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="totalDebits" index="totlaDebitsForSorting" title="%{getText('jsp.default_434')}" sortable="true" width="90"  formatter="ns.account.totalDebitsFormatter" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="totalCredits" index="totlaCreditsForSorting" title="%{getText('jsp.default_433')}" sortable="true" width="90"  formatter="ns.account.totalCreditsFormatter" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="displayClosingAmountValue" index="CLOSINGBALANCE" title="%{getText('jsp.default_104')}" sortable="true" width="100" formatter="ns.account.closingLedgerDepositeGridFormatter" cssClass="datagrid_amountColumn"  />
     <sjg:gridColumn name="account.displayClosingBalance.amountValue.currencyStringNoSymbol" index="displayClosingBalance.amountValue.currencyStringNoSymbol" title="%{getText('jsp.default_104')}" hidden="true" hidedlg="true" formatter="ns.account.closingLedgerDepositeGridFormatter" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="account.displayIntradayCurrentBalance.amountValue.currencyStringNoSymbol" index="displayIntradayCurrentBalance.amountValue.currencyStringNoSymbol" title="%{getText('jsp.default_129')}" hidden="true" hidedlg="true" formatter="ns.account.currentLedgerDepositeFormatter" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="displayClosingAmountValue1" index="displayClosingAmountValue" title="%{getText('jsp.account_129')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
	 <sjg:gridColumn name="displayOpeningLedgerAmountValue" index="displayOpeningLedgerAmountValue" title="%{getText('jsp.account_131')}" formatter="ns.account.openingLedgerDepositFormatter"  hidden="true" hidedlg="true" />
     <sjg:gridColumn name="displayIntradayCurrentBalanceAmount" index="displayIntradayCurrentBalanceAmount" title="%{getText('jsp.account_130')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="displayOpeningLedgerTotal" index="displayOpeningLedgerTotal" title="%{getText('jsp.account_132')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="displayAccountsClosingBalanceTotal" index="displayAccountsClosingBalanceTotal" title="%{getText('jsp.account_127')}"  hidden="true" hidedlg="true" formatter="ns.account.closingLedgerDepositeGridFormatter"  cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="displayAccountsIntralBalanceTotal" index="displayAccountsIntralBalanceTotal" title="%{getText('jsp.account_128')}"  hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="account.displayText" index="account.displayText" title="%{getText('jsp.account_77')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.nickName" index="NICKNAME" title="%{getText('jsp.account_146')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.currencyCode" index="CURRENCYCODE" title="%{getText('jsp.account_60')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.ID" index="ID" title="%{getText('jsp.account_12')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="account.bankID" index="BANKID" title="%{getText('jsp.account_39')}" sortable="false" hidden="true" hidedlg="true" />
     <sjg:gridColumn name="account.routingNum" index="ROUTINGNUM" title="%{getText('jsp.account_183')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>
     <sjg:gridColumn name="displayCurrencyCode" index="displayCurrencyCode" title="%{getText('jsp.default_173')}" sortable="true" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="dataClassification" index="dataClassification" title="%{getText('jsp.account_64')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_textColumn"/>
     <sjg:gridColumn name="totalOpeningLedgerTotal" index="totalOpeningLedgerTotal" title="%{getText('jsp.account_210')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="totalDebitsTotal" index="totalDebitsTotal" title="%{getText('jsp.account_209')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_numberColumn"/>
     <sjg:gridColumn name="totalCreditsTotal" index="totalCreditsTotal" title="%{getText('jsp.account_208')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="accountsClosingBalanceTotal" index="accountsClosingBalanceTotal" title="%{getText('jsp.account_10')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="accountsIntradayCurrentBalanceTotal" index="accountsIntradayCurrentBalanceTotal" title="%{getText('jsp.account_11')}" sortable="false" hidden="true" hidedlg="true" cssClass="datagrid_amountColumn"/>
     <sjg:gridColumn name="map.LinkURL" index="LinkURL" title="%{getText('jsp.default_262')}" sortable="true" width="60" hidden="true" hidedlg="true" cssClass="datagrid_actionIcons"/> 
	 
	 
</sjg:grid>

</div>
<script>
$(document).ready(function(){
	/* $('#consolidatedBalance_deposit_gridID').portlet({
		generateDOM: true,
		helpCallback: function(){
			
			var helpFile = $('#consolidatedBalance_deposit_gridID').find('.moduleHelpClass').html();
			callHelp('/cb/web/help/', '/cb/web/help/' + helpFile);
		}
		
	});

	$('#consolidatedBalance_deposit_gridID').portlet('title', js_acc_deposite_portlet_title); */	
});
					
	
</script>
<style>
	#consolidatedBalance_deposit_gridID .ui-jqgrid-ftable td{
		border:none;
	}
</style>
