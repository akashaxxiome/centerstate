<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi" %>
<script type="text/javascript" src="<s:url value='/web/js/account/account%{#session.minVersion}.js'/>"></script>
<script type="text/javascript" src="<s:url value='/web/js/account/account_grid%{#session.minVersion}.js'/>"></script>

<div class="dashboardUiCls">
    	<div class="moduleSubmenuItemCls">
    		<span class="moduleDropdownMenuIcon ffiUiIcoMedium ffiUiIco-icon-medium-accountBalance"></span>
   		<span class="moduleSubmenuLbl">
   			<s:text name="jsp.home_19" />
   		</span>
   		<span class="moduleSubmenuLblDownArrow sapUiIconCls icon-navigation-down-arrow"></span>
		
		<!-- dropdown menu include -->    		
   		<s:include value="/pages/jsp/home/inc/accountSubmenuDropdown.jsp" />
   	</div>
</div> 
