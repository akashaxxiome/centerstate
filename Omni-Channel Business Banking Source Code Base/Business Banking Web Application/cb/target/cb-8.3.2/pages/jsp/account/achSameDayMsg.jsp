<%@ taglib uri="/WEB-INF/ffi.tld" prefix="ffi"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page contentType="text/html; charset=UTF-8"%>





<s:set var="showNote" value="%{'false'}" />

<ffi:cinclude value1="${SameDayACHEnabledForUser}" value2="true">
		<s:set var="showNote" value="%{'true'}" />
</ffi:cinclude>


<%
	String cutoffTime = "";
	String finalCutoffTime = "";
	String sameDayCutoffTime = "";
	String sameDayFinalCutoffTime = "";
%>
<s:if test="%{#showNote=='true'}">

	<ffi:object id="GetFileUploadNextCutOffTime"
		name="com.ffusion.tasks.affiliatebank.GetNextCutOffTime"
		scope="session" />
	<ffi:setProperty name="GetFileUploadNextCutOffTime" property="FIId"
		value="${SecureUser.BPWFIID}" />
	<ffi:setProperty name="GetFileUploadNextCutOffTime" property="Category"
		value="<%=com.ffusion.beans.affiliatebank.CutOffDefines.PMT_GROUP_ACH_FILE_UPLOAD%>" />
	<ffi:setProperty name="GetFileUploadNextCutOffTime"
		property="DateFormat" value="E hh:mm a (z)" />
	<ffi:setProperty name="GetFileUploadNextCutOffTime" property="Locale"
		value="${UserLocale.Locale}" />
	<ffi:setL10NProperty name="GetFileUploadNextCutOffTime"
		property="NotSetString" value="Not Available/Cut-off time not set" />
	<ffi:setProperty name="GetFileUploadNextCutOffTime"
		property="InstructionType" value="ACHFILETRN" />
	<ffi:process name="GetFileUploadNextCutOffTime" />


	<ffi:object id="GetSameDayFileUploadNextCutOffTime"
		name="com.ffusion.tasks.affiliatebank.GetNextCutOffTime"
		scope="session" />
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime"
		property="FIId" value="${SecureUser.BPWFIID}" />
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime"
		property="Category"
		value="<%=com.ffusion.beans.affiliatebank.CutOffDefines.PMT_GROUP_ACH_FILE_UPLOAD%>" />
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime"
		property="DateFormat" value="E hh:mm a (z)" />
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime"
		property="Locale" value="${UserLocale.Locale}" />
	<ffi:setL10NProperty name="GetSameDayFileUploadNextCutOffTime"
		property="NotSetString" value="Not Available/Cut-off time not set" />
	<ffi:setProperty name="GetSameDayFileUploadNextCutOffTime"
		property="InstructionType" value="SAMEDAYACHFILETRN" />
	<ffi:process name="GetSameDayFileUploadNextCutOffTime" />

</s:if>
<span id="achCutoffMessage"> <s:if test="%{#showNote=='true'}">
		<ffi:getProperty name="GetFileUploadNextCutOffTime"
			property="NextCutOffTime" assignTo="cutoffTime" />
		<ffi:getProperty name="GetFileUploadNextCutOffTime"
			property="FinalCutOffTime" assignTo="finalCutoffTime" />
		<ffi:getProperty name="GetSameDayFileUploadNextCutOffTime"
			property="NextCutOffTime" assignTo="sameDayCutoffTime" />
		<ffi:getProperty name="GetSameDayFileUploadNextCutOffTime"
			property="FinalCutOffTime" assignTo="sameDayFinalCutoffTime" />

		<s:text
							name="ach.payments.nextcutoff">
							<s:param>
								<ffi:getProperty name="GetFileUploadNextCutOffTime"
									property="NextCutOffTime" />
							</s:param>
						</s:text>


			<%
				if (finalCutoffTime != null && !finalCutoffTime.equals(cutoffTime)) {
			%>
			<br/>
			<s:text name="ach.payments.finalcutoff">
				<s:param>
					<ffi:getProperty name="GetFileUploadNextCutOffTime"
						property="FinalCutOffTime" />
				</s:param>
			</s:text>

			<%
				}
			%>
				<br/>
				<s:text name="ach.payments.samedaynextcutoff">
					<s:param>
						<ffi:getProperty name="GetSameDayFileUploadNextCutOffTime"
							property="NextCutOffTime" />
					</s:param>
				</s:text>



				<%
					if (sameDayCutoffTime != null && !sameDayCutoffTime.equals(sameDayFinalCutoffTime)) {
				%>
				<br/>
				<s:text name="ach.payments.samedayfinalcutoff">
					<s:param>
						<ffi:getProperty name="GetSameDayFileUploadNextCutOffTime"
							property="FinalCutOffTime" />
					</s:param>
				</s:text>

				<%
					}
				%>
<br/>
<s:if test="%{#showNote=='true'}">

			<b>	<s:text name="jsp.payments.achupload4"/></b>
</s:if>

	</s:if>
</span>